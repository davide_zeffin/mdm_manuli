/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object.imp;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.ISOCodeConversionProperties;
import com.sap.tc.logging.Location;


public abstract class DefaultIPCSession implements IPCSession {

	//Used by application level caching
	protected boolean cacheIsDirty = true;
    
    protected DefaultPricingConverter _pricingConverter;

    protected IPCClientObjectFactory factory = IPCClientObjectFactory.getInstance();
    // my parent object. Since my items have to create configs, and Configs need to know their
	// IPCClient object, we need to store it here.
	protected DefaultIPCClient _ipcClient;
	protected Hashtable     _documentTable;
	protected Hashtable     _configTable = new Hashtable();
	/**
	 * Holds all values of _documentTable.
	 * Will be only set on request, and reset to null whenever _documentTable changes.
	 */
	protected IPCDocument[] _documents;
	protected Hashtable     _descriptionForProcedure;
	protected Hashtable     _descriptionForCurrency;
	protected String        _userName;
	protected String        _userLocation;
	protected String        _mandt;
	protected String        _parameterNaming;
	/**
	 * By default (ISA) don't enable retrieval of extended data (performance)
	 */
	protected boolean       _isExtendedData = false;
    protected static final Location location = ResourceAccessor.getLocation(DefaultIPCSession.class);

	public abstract IClient getClient();


	public IPCClient getIPCClient() {
        location.entering("getIPCClient()");
        if (location.beDebug()) {
            if (_ipcClient!=null){
                location.debugT("return _ipcClient " + _ipcClient.toString());
            }
            else location.debugT("return _ipcClient null");
        }
        location.exiting();
		return _ipcClient;
	}


	/**
	 * Creates a new IPCDocument for the given parameters, and adds it to this session.
	 */
    public IPCDocument newDocument(IPCDocumentProperties props) throws IPCException {
        location.entering("newDocument(IPCDocumentProperties props)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (props!=null){
                location.debugT("IPCDocumentProperties props " + props.toString());
            }
            else location.debugT("IPCDocumentProperties props null");
        }
		if (!(props instanceof DefaultIPCDocumentProperties))
			throw new IllegalClassException(props, DefaultIPCDocumentProperties.class);

		IPCDocument document = factory.newIPCDocument(this, (DefaultIPCDocumentProperties)props);
		_documentTable.put(document.getId(), document);
		_documents = null;
        if (location.beDebug()) {
            location.debugT("return document " + document.getId());
        }
        location.exiting();
		return document;
    }


    public IPCDocument getDocument(String documentId) {
        location.entering("getDocument(String documentId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String documentId " + documentId);
        }
        IPCDocument ipcDocument = (IPCDocument)_documentTable.get(documentId);
        if (location.beDebug()) {
            if (ipcDocument != null){ 
               location.debugT("return ipcDocument " + ipcDocument.getId());
            }
            else location.debugT("Couldn't find document with id " +documentId);
        }
        location.exiting();
		return ipcDocument;
    }

    public Configuration getConfiguration(String configId) {
        location.entering("getConfiguration(String configId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String configId " + configId);
        }
        Configuration configuration = (Configuration)_configTable.get(configId);
        if (location.beDebug()) {
            location.debugT("return configuration " + configuration.getId());
        }
        location.exiting();
		return configuration;
    }


    public String getPricingProcedureDescription(String procedureName) {
        location.entering("getPricingProcedureDescription(String procedureName)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String procedureName " + procedureName);
        }
        String pricingProcedureDescription = (String)_descriptionForProcedure.get(procedureName);
        if (location.beDebug()) {
            location.debugT("return pricingProcedureDescription " + pricingProcedureDescription);
        }
        location.exiting();
        return pricingProcedureDescription;
    }

    public abstract void removeDocument(IPCDocument document) throws IPCException;

    public IPCDocument[] getDocuments() {
        location.entering("getDocuments()");
		if (_documents == null) {
			_documents = new IPCDocument[_documentTable.size()];
			int i=0;
			for (Enumeration e=_documentTable.elements(); e.hasMoreElements();) {
				_documents[i] = (IPCDocument)e.nextElement();
				i++;
			}
		}
        if (location.beDebug()){
            if (_documents != null) {
                location.debugT("return _documents");
                for (int i = 0; i<_documents.length; i++){
                    location.debugT("  _documents["+i+"] "+_documents[i].getId());
                }
            }
        }
        location.exiting();
		return _documents;
    }


    public abstract String[] getAvailableCurrencyUnits() throws IPCException;


    public String getUserName() {
        location.entering("getUserName()");
        if (location.beDebug()) {
        location.debugT("return _userName " + _userName);
        }
        location.exiting();
		return _userName;
    }

    public String getUserLocation() {
        location.entering("getUserLocation()");
        if (location.beDebug()) {
        location.debugT("return _userLocation " + _userLocation);
        }
        location.exiting();
		return _userLocation;
    }

    public String getUserSAPClient() {
        location.entering("getUserSAPClient()");
        if (location.beDebug()) {
        location.debugT("return _mandt " + _mandt);
        }
        location.exiting();
		return _mandt;
    }

	public String getParameterNaming() {
        location.entering("getParameterNaming()");
        if (location.beDebug()) {
        location.debugT("return _parameterNaming " + _parameterNaming);
        }
        location.exiting();
		return _parameterNaming;
	}


	public void setExtendedDataEnabled(boolean enabled) {
        location.entering("setExtendedDataEnabled(boolean enabled)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean enabled " + enabled);
        }
		_isExtendedData = enabled;
        location.exiting();
	}


	public boolean isExtendedDataEnabled() {
        location.entering("isExtendedDataEnabled()");
        if (location.beDebug()) {
        location.debugT("return _isExtendedData " + _isExtendedData);
        }
        location.exiting();
		return _isExtendedData;
	}

	public void determineUnitsOfMeasurement(ISOCodeConversionProperties[] props,
										    Locale locale, String groupSeparator, String decimalSeparator) throws IPCException {
		location.entering("determineUnitsOfMeasurement(ISOCodeConversionProperties[] props, " +                           "Locale locale, String groupSeparator, String decimalSeparator");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("ISOCodeConversionProperties[] props");
            if (props != null) {
                location.debugT("return props");
                for (int i = 0; i<props.length; i++){
                    location.debugT("  props["+i+"] "+props[i].toString());
                }
            }
            if (locale!=null){
                location.debugT("Locale locale " + locale.toString());
            }
            else location.debugT("Locale locale null");
            location.debugT("String groupSeparator " + groupSeparator);
            location.debugT("String decimalSeparator " + decimalSeparator);
        }
        determineDimensionalUnits(props, locale, groupSeparator, decimalSeparator, false);
        location.exiting();
	}

	public void determineCurrencyUnits(ISOCodeConversionProperties[] props,
											Locale locale, String groupSeparator, String decimalSeparator) throws IPCException {
        location.entering("determineCurrencyUnits(ISOCodeConversionProperties[] props, " +
                           "Locale locale, String groupSeparator, String decimalSeparator");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("ISOCodeConversionProperties[] props");
            if (props != null) {
                location.debugT("return props");
                for (int i = 0; i<props.length; i++){
                    location.debugT("  props["+i+"] "+props[i].toString());
                }
            }
            if (locale!=null){
                location.debugT("Locale locale " + locale.toString());
            }
            else location.debugT("Locale locale null");
            location.debugT("String groupSeparator " + groupSeparator);
            location.debugT("String decimalSeparator " + decimalSeparator);
        }
        determineDimensionalUnits(props, locale, groupSeparator, decimalSeparator, true);
        location.exiting();
	}

	protected abstract void determineDimensionalUnits(ISOCodeConversionProperties[] props,
											Locale locale, String groupSeparator, String decimalSeparator, boolean currencyUnit) throws IPCException;

	public abstract void close();

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String result = super.toString();
		result += "mandt:"
			+ _mandt
			+ "\nuserName:"
			+ _userName
			+ "\nuserLocation:"
			+ _userLocation
			+ "\nparameterNaming:"
			+ _parameterNaming
			+ "\nisExtendedData:"
			+ _isExtendedData;
		return result;
	}

	/**
	 * @return
	 */
	public String get_mandt() {
        location.entering("get_mandt()");
        if (location.beDebug()) {
            location.debugT("return _mandt " + _mandt);
        }
        location.exiting();
		return _mandt;
	}

	/**
	 * @param string
	 */
	public void set_mandt(String string) {
        location.entering("set_mandt(String string)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String string " + string);
        }
		_mandt = string;
        location.exiting();
	}

	/**
	 * Sets sessions Cache as diety and all the session documents cache also dirty.
	 * Note: Recommend not to use this API as you are interested to make your own 
	 * document dirty and by calling session dirty method, other documents in this session
	 * also becoming dirty which is a performance hit. Instead get your document from this 
	 * session and call dirty method on that document.
	 */
	public abstract void setCacheDirty();
		
	/**
	 * Synchronizes the session with server.
	 */
	public abstract void syncWithServer()throws IPCException;
	
	/**
	 * For IPC Client Object Layer Use only
	 */
	public void _setSessionFlagToDirty() {
        location.entering("_setSessionFlagToDirty()");
		cacheIsDirty = true;
        location.exiting();
	}
	
	/**
	 * For IPC Client Object Layer Use only
	 */
	protected void _setSessionDocsFlagToDirty() throws IPCException {
		if (_documentTable != null && _documentTable.size() > 0){
			//Set all documents cache as dirty.
			Enumeration docIds = _documentTable.keys();
			String documentId = null;
			while (docIds.hasMoreElements()){
				documentId = (String)docIds.nextElement();
				DefaultIPCDocument doc = (DefaultIPCDocument)_documentTable.get(documentId);
				doc.setCacheDirty();
			}
		}
	}
    
    public DefaultPricingConverter getPricingConverter(){
        return _pricingConverter; 
    }
}
