package com.sap.spc.remote.client.object.imp.rfc;

import java.util.ArrayList;
import java.util.Properties;

import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.FieldIterator;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ClientLogListener;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;


/**
 * @author I026584
 */
public abstract class RFCDefaultClient extends BackendBusinessObjectBaseSAP implements IClient {

	protected String language;
	protected String client; //mandt
	protected String connectionKey;
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(RFCDefaultClient.class);
    protected boolean firstCall = true;

	protected ArrayList listeners = new ArrayList();

	public RFCDefaultClient()throws ClientException{
		super();
	}

	/**
	 * Adds a new client log listener to this client. The client will inform it about
	 * all relevant events.
	 */
	public void addLogListener(ClientLogListener l) {
		if (!listeners.contains(l))
			listeners.add(l);
	}

	/**
	 * Removes a clinet log listener from this client. Doesn't perform any operation if l
	 * has not previously been added by addLogListener.
	 */
	public void removeLogListener(ClientLogListener l) { 
		listeners.remove(l);
	}

	public JCO.Function getFunction(String functionName)throws ClientException{
		    location.entering("getFunction(String functionName)");
		    if (location.beDebug()) {
		    	location.debugT("Parameters:");
		    	location.debugT("String functionName" + functionName);
		    }
			JCO.Function function = null;
			try {
				function = getDefaultIPCJCoConnection().getJCoFunction(functionName);
			} catch (BackendException e) {
				throw new ClientException("500", "Exception while getting the JCO.Function for function '"+functionName+"'. Message: " + e.getMessage());
			}
			if (function == null) throw new ClientException("500", "Function Module should not be null for function: "+functionName);
			location.exiting();
			return function;
	}	

	public void execute(JCO.Function function)throws ClientException{
		//location.entering(category, "RFCDefaultClient.execute(...)");
		JCoConnection conn = getDefaultIPCJCoConnection();
		try{
			if (location.beDebug()) {
				location.debugT(printImportParameters(function));
			}
			conn.execute(function);
			if (location.beDebug()) {
				location.debugT(printExportParameters(function));
			}
		}catch(JCO.Exception e){
            location.errorT(printExceptions(function) + printStackOfJCOException(e));  
			throw new ClientException("500","JCO Exception: " + e.getMessage() + printImportParameters(function));
		} catch (BackendException e) {
			throw new ClientException("500","BackendException Exception: " + e.getMessage() + printImportParameters(function));
		}finally{
			conn.close();
		}
	}
	
	public void executeStateless(JCO.Function function, String language) throws ClientException {
		location.entering("executeStateless(JCO.Function function, String language)");
		if (location.beDebug()) {
			location.debugT("Parameters:");
			location.debugT("JCO.Function function" + function);
			location.debugT("String language" + language);

		}
		
		JCoConnection ipcJcoCon = getIPCStatelessConnection(language);
		try {
			ipcJcoCon.execute(function);
		} catch (BackendException e) {
			throw new IPCException(e);
		}
		location.exiting();
	}

    public String printExceptions(JCO.Function function){
        if (function == null){
            return "";
        }
        StringBuffer result = new StringBuffer("\nJCO.Exception in " + function.getName());
        result.append("\nList of AbapExceptions: \n");
        JCO.AbapException exceptions[] = function.getExceptionList();
        if (exceptions != null){
            for (int i=0; i<exceptions.length; i++){
                JCO.AbapException abapEx = exceptions[i];
                result.append(i+1 + ": " + abapEx + "\n");
            }
        }
        else {
            result.append("No AbapExceptions.");
        }
        return result.toString();
    }
    
    public String printStackOfJCOException(JCO.Exception ex){
        if (ex == null){
            return "";
        }
        StringBuffer result = new StringBuffer("\n" + ex.toString());
        StackTraceElement stack[] = ex.getStackTrace();
        if (stack != null){
            for (int i=0; i<stack.length; i++){
                StackTraceElement ste = stack[i];
                result.append("\n\tat " + ste.toString());
                // show only the first 20 elements
                if (i==19){
                    result.append("\n\t... " + (stack.length - 20) + " more\n");
                    break;
                }
            }
        }
        else {
            result.append("\nNo stacktrace available.");
        }
        return result.toString();        
    }

    public String printImportParameters(JCO.Function function){
        StringBuffer result = new StringBuffer("\nFunction: " + function.getName());
        result.append("\nImport Parameters: \n");
        JCO.ParameterList imp = function.getImportParameterList();
        if (imp != null)
	        printParameterList(result, imp);
        
        return result.toString();
    }

    public String printExportParameters(JCO.Function function){
        StringBuffer result = new StringBuffer("\nFunction: " + function.getName());
        result.append("\nExport Parameters: \n");
        JCO.ParameterList exp = function.getExportParameterList();
        if (exp != null)
	        printParameterList(result, exp);
        
        return result.toString();
    }
    
    

    protected void printParameterList(StringBuffer result, JCO.ParameterList paramList) {
        result.append(paramList);
        FieldIterator flds = paramList.fields();
        if (flds != null){
			printRecord(result, flds);
        }
    }

    protected void printRecord(StringBuffer result, FieldIterator flds) {
        while (flds.hasMoreFields()){
            if (result.length() > 1048576){
               result.setLength(1048576);
               result.append("\nLength of parameter list is greater than 1048576 characters. Therefore it has been truncated.");
               break;
           }            
           Object el = flds.nextElement();
           JCO.Field fld = (JCO.Field)el;
           int type = fld.getType();
           if (type == JCO.TYPE_STRING) {
           	   result.append("\n");
               result.append(fld.getString());
           }
           if (type == JCO.TYPE_STRUCTURE){
               JCO.Structure strc = fld.getStructure();
               result.append("\n");
               result.append(strc);
               JCO.FieldIterator iter = strc.fields();
			   if (iter != null)
               	  printRecord(result, iter);
           }
           if (type == JCO.TYPE_TABLE){
               JCO.Table tab = fld.getTable();
               result.append("\n");
               result.append(tab);
               int numRows = tab.getNumRows();
               for (int i=0; i<numRows; i++){
                   tab.setRow(i);
                   JCO.FieldIterator iter = tab.fields();
				   if (iter != null)
                   		printRecord(result, iter);
               }
           }
        }
    }



	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IClient#getItemReference()
	 */
	public IPCItemReference getItemReference() {
		IPCItemReference reference = new RFCIPCItemReference();
		return reference;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IClient#getConfigReference()
	 */
	public IPCConfigReference getConfigReference() {
		IPCConfigReference reference = new RFCIPCConfigReference();
		return reference ;
	}

	/**
	 * 
	 */
	public void close() {
		getDefaultIPCJCoConnection().close();
	}
	
//	public void registerIPCClientAtBackendContext(IPCClient ipcClient){
//		getContext().setAttribute(IPCClient.DEFAULT_CLIENT, ipcClient);
//	}
//
	/* (non-Javadoc)
	 * @see com.sap.isa.core.eai.BackendBusinessObject#initBackendObject(java.util.Properties, com.sap.isa.core.eai.BackendBusinessObjectParams)
	 */
	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {
		super.initBackendObject(props, params);
		if (params != null){		
			IPCClientBackendBOParams IPCparams = (IPCClientBackendBOParams)params;
			this.language = IPCparams.getLanguage();
			this.client = IPCparams.getClient();
			this.connectionKey = IPCparams.getConnectionKey();
		}
	}
	
	abstract boolean isInitialized(JCoConnection connection);

	/* (non-Javadoc)
	 * @see com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP#getDefaultJCoConnection()
	 */
	public abstract JCoConnection getDefaultIPCJCoConnection();
	
	public abstract JCoConnection getIPCStatelessConnection(String language);
		
    /**
     * @return language of client
     */
    public String getLanguage() {
        return language;
    }
}
