package com.sap.spc.remote.client.object.imp.tcp;

import java.util.Map;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TcpDefaultIPCItemProperties
	extends DefaultIPCItemProperties
	implements IPCItemProperties {

	//Constructors
	protected TcpDefaultIPCItemProperties() {
		_productGuid 			= /*new LazyValue()*/ null;
		_productId   			= /*new LazyValue()*/ null;
		_productType            = /*new LazyValue()*/ null;
		_productLogSys          = /*new LazyValue()*/ null;
		_date		 			= /*new LazyValue()*/ null;

		_headerAttributeNames  	= /*new LazyValue()*/ new String[0];
		_headerAttributeValues 	= /*new LazyValue()*/ new String[0];
		_itemAttributeNames    	= /*new LazyValue()*/ new String[0];
		_itemAttributeValues   	= /*new LazyValue()*/ new String[0];

		_statistical 			= /*new LazyValue()*/ null;
		_performPricingAnalysis = /*new LazyValue()*/ false;
        _pricingRelevant        = new Boolean(true);
        _decoupleSubitems       = false;
		_salesQuantity			= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_netWeight				= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_grossWeight			= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_externalId				= /*new LazyValue()*/ null;
		_productDescription		= /*new LazyValue()*/ null;
        _configChanged          = true; // initially set to true because the config is not known at the client
		_configurable			= /*new LazyValue()*/ null;
		_productVariant         = /*new LazyValue()*/ null;
		_netValue				= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_netValueWithoutFreight	= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_taxValue				= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_grossValue				= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_totalGrossValue		= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_totalNetValue	    	= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_totalTaxValue  		= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_itemReturn				= /*new LazyValue()*/ null;
		_baseQuantity			= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_volume					= new com.sap.spc.remote.client.object.imp.DimensionalValue();
		_subItemIds             = /*new LazyValue()*/ new String[0];
        _config                 = factory.newConfigValue();
		_contextKeys            = /*new LazyValue()*/ new String[0];
		_contextValues          = /*new LazyValue()*/ new String[0];
		_exchangeRate           = /*new LazyValue()*/ null;
		_kbLogSys               = /*new LazyValue()*/ null;
		_kbName                 = /*new LazyValue()*/ null;
		_kbVersion              = /*new LazyValue()*/ null;
		_kbProfile              = /*new LazyValue()*/ null;
		_highLevelItemId		= /*new LazyValue()*/ null;
	}


	protected TcpDefaultIPCItemProperties(IPCItemProperties initialData) throws IPCException {
		this();
        if (initialData == null){
            return;
        }
        if (initialData.getItemId() != null && !initialData.getItemId().equals("")) {
			_itemId      = initialData.getItemId();       	
        }
		_productGuid = initialData.getProductGuid();
		_productId = initialData.getProductId();
        _productType = initialData.getProductType();
        _productLogSys = initialData.getProductLogSys();    
        _kbLogSys = initialData.getKbLogSys();
        _kbName = initialData.getKbName();
        _kbVersion = initialData.getKbVersion();
        _kbProfile = initialData.getKbProfile();
        _highLevelItemId = initialData.getHighLevelItemId();                    
		_date = initialData.getDate();
		_statistical = initialData.isStatistical() ? "Y" : "N";
        _pricingRelevant = new Boolean(initialData.isPricingRelevant().booleanValue()); 
        _performPricingAnalysis = initialData.getPerformPricingAnalysis();
        _decoupleSubitems = initialData.isDecoupleSubitems();
        _exchangeRateType = initialData.getExchangeRateType();
        _exchangeRateDate = initialData.getExchangeRateDate();

		_salesQuantity.setValue(initialData.getSalesQuantity());
        _baseQuantity.setValue(initialData.getBaseQuantity());
		_netWeight.setValue(initialData.getNetWeight());
		_itemReturn = initialData.isItemReturn() ? "Y" : "N";
		_volume.setValue(initialData.getVolume());

        _config = factory.newConfigValue(initialData.getConfig(null));

        Map initialContext = initialData.getContext();
        if (initialContext != null){
            this.setContext(initialContext);
        }

        Map initialHeaderAttributes = initialData.getHeaderAttributes();
        if (initialHeaderAttributes != null){
            this.setHeaderAttributes(initialHeaderAttributes);
        }

        Map initialItemAttributes = initialData.getItemAttributes();
        if (initialItemAttributes != null){
            this.setItemAttributes(initialItemAttributes);
        }

        String initialSubItemIds[] = initialData.getSubItemIds();
        if (initialSubItemIds != null){
            int length = initialSubItemIds.length;
            _subItemIds = new String[length];
            for (int i=0; i<length; i++){
                _subItemIds[i] = initialSubItemIds[i];
            }
        }

        Map initialConditionTimestamps = initialData.getConditionTimestamps();
        if (initialConditionTimestamps != null){
            this.setConditionTimestamps(initialConditionTimestamps);            
        }

        DefaultIPCItemProperties defaultInitialData = (DefaultIPCItemProperties)initialData;
        _externalId = defaultInitialData.getExternalId();
        _productDescription = defaultInitialData.getProductDescription();
        _configurable = defaultInitialData.isConfigurable() ? "Y" : "N";
        _productVariant = defaultInitialData.isProductVariant() ? "Y" : "N";
        _grossWeight.setValue(defaultInitialData.getGrossWeight());      
        _netValue.setValue(defaultInitialData.getNetValue());
        _netValueWithoutFreight.setValue(defaultInitialData.getNetValueWithoutFreight());

        _taxValue.setValue(defaultInitialData.getTaxValue());
        _grossValue.setValue(defaultInitialData.getGrossValue());
        _totalGrossValue.setValue(defaultInitialData.getTotalGrossValue());
        _totalNetValue.setValue(defaultInitialData.getTotalNetValue());
        _totalTaxValue.setValue(defaultInitialData.getTotalTaxValue());
	}


		//********Implementation for Abstract methods starts from here!********

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties#initializeValues(com.sap.spc.remote.client.object.imp.DefaultIPCSession, java.lang.String, java.lang.String, boolean, boolean)
	 */
	 //No caching client code but set initialize config..
	public void initializeValues(
		DefaultIPCSession session,
		String documentId,
		String itemId,
		boolean isValueFormatting,
		boolean isExtendedData) 
		{
		   _config.initializeValues(session, documentId, itemId);
	   }

}
