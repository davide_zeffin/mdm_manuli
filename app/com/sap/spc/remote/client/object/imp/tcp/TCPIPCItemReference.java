/*
 * Created on Mar 15, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import com.sap.spc.remote.client.object.IPCItemReference;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TCPIPCItemReference extends IPCItemReference {
	protected String host;
	protected String port;
	protected String encoding;
	protected boolean dispatcher;


	public TCPIPCItemReference() {
	}
	
	/**
	 * @param host
	 * @param port
	 * @param dispatcher
	 * @param encoding
	 * @param sessionId
	 * @param documentId
	 * @param itemId
	 */
	public TCPIPCItemReference(String host, String port, boolean dispatcher, String encoding, String sessionId, String documentId, String itemId) {
		this.host = host;
		this.port = port;
		this.dispatcher = dispatcher;
		this.encoding = encoding;
		this.documentId = documentId;
		this.itemId = itemId;		
	}
	public String getHost() {
		return host;
	}
	public void setHost(String newHost) {
		host = newHost;
	}
	public void setPort(String newPort) {
		port = newPort;
	}
	public String getPort() {
		return port;
	}
	public void setEncoding(String newEncoding) {
		encoding = newEncoding;
	}
	public String getEncoding() {
		return encoding;
	}
//	public void setSessionId(String newSessionId) {
//		sessionId = newSessionId;
//	}
//	public String getSessionId() {
//		return sessionId;
//	}
//	public void setDocumentId(String newDocumentId) {
//		documentId = newDocumentId;
//	}
//	public String getDocumentId() {
//		return documentId;
//	}
//	public void setItemId(String newItemId) {
//		itemId = newItemId;
//	}
//	public String getItemId() {
//		return itemId;
//	}
	public void setDispatcher(boolean newDispatcher) {
		dispatcher = newDispatcher;
	}
	public boolean isDispatcher() {
		return dispatcher;
	}
    
	public String toString(){
		return
		("host:" + host +
		"\nport:" + port +
		"\nencoding:" + encoding +
		"\ndispatcher:" + dispatcher +
		"\ndocumentId:" + documentId +
		"\nitemId:" + itemId);
	}
    
	public static void main(String[] args) {
		TCPIPCItemReference itemReference = new TCPIPCItemReference();
		itemReference.setHost("host");
		itemReference.setPort("port");
		itemReference.setEncoding("UnicodeLittle");
		itemReference.setDispatcher(true);
		itemReference.setDocumentId("documentId");
		itemReference.setItemId("itemId");
		System.out.println(itemReference.toString());
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItemReference#init(com.sap.spc.remote.client.object.IPCItemReference)
     */
    public void init(IPCItemReference reference) {
        TCPIPCItemReference ref = (TCPIPCItemReference)reference;
        this.host = ref.getHost();
        this.port = ref.getPort();
        this.dispatcher = ref.isDispatcher();
        this.encoding = ref.getEncoding();
        this.documentId = ref.getDocumentId();
        this.itemId = ref.getItemId();           
    }

}
