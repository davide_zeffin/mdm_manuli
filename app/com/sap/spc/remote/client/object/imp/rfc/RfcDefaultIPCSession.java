package com.sap.spc.remote.client.object.imp.rfc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.ISOCodeConversionProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCClient;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.rfc.function.PrcCnvCurrConvIso;
import com.sap.spc.remote.client.rfc.function.PrcCnvUnitConvIso;
import com.sap.spc.remote.client.rfc.function.SpcBindConfiguration;
import com.sap.spc.remote.client.rfc.function.SpcGetDocumentInfo;
import com.sap.spc.remote.client.rfc.function.SpcLockConfiguration;
import com.sap.spc.remote.client.rfc.function.SpcRemoveDocument;
import com.sap.spc.remote.client.rfc.function.SpcRepublishConfiguration;
import com.sap.spc.remote.client.rfc.function.SpcUnlockConfiguration;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

/**
 * @author I026584
 *
 */
public class RfcDefaultIPCSession
	extends DefaultIPCSession
	implements IPCSession {
		
	protected String tokenId;
	protected String configId;
	protected boolean _locked;

	/**
	 * @param client
	 * @param rfcClientSupport
	 * @param tokenId
	 */
	public RfcDefaultIPCSession(RfcDefaultIPCClient client, BackendBusinessObject rfcClientSupport, String tokenId, String configId) {
		_ipcClient = (DefaultIPCClient)client;
		_client = (RFCDefaultClient)rfcClientSupport;
        _pricingConverter = factory.newPricingConverter(this);
		_documentTable = new Hashtable();
		_documents = null;
		_descriptionForProcedure = new Hashtable();
		_descriptionForCurrency  = new Hashtable();		
		if (IPCConfigReference.isConfigOnly(configId)) {
			initializeConfigs(tokenId, configId);
		}else {
			initializeDocuments(tokenId, configId);		
		}
	}

	/**
     * @param ipcClient
     * @param reference
     */
    public RfcDefaultIPCSession(DefaultIPCClient ipcClient, IPCItemReference reference) {
        
        this(ipcClient, reference, new Boolean(false));
    }


    /**
     * @param ipcClient
     * @param reference
     * @param boolean1
     */
    public RfcDefaultIPCSession(DefaultIPCClient ipcClient, IPCItemReference reference, Boolean isExtendedData) {
        if (_client == null){
            throw new IPCException("BackendBusinessObject rfcClientSupport not yet initialized. This has to be done by call IPCBOManager.createIPCClient() before.");
        }

        _ipcClient = ipcClient;
        
        _pricingConverter = factory.newPricingConverter(this);

        _documentTable = new Hashtable();
        _documents = null;
        _descriptionForProcedure = new Hashtable();
        _descriptionForCurrency = new Hashtable();
        _mandt = "???";
        _userName = "unknown";
        _userLocation = "unknown";
        _isExtendedData = isExtendedData.booleanValue();

        initializeDocuments();

    }


    /**
	 * @param ipcClient
	 * @param rfcClientSupport
	 */
	public RfcDefaultIPCSession(IPCClient ipcClient, BackendBusinessObject rfcClientSupport) {
		_ipcClient = (DefaultIPCClient)ipcClient;
		_client = (RFCDefaultClient)rfcClientSupport;
        _pricingConverter = factory.newPricingConverter(this);
        _documentTable = new Hashtable();
        _documents = null;
        _descriptionForProcedure = new Hashtable();
        _descriptionForCurrency  = new Hashtable();		
        //Not sure whether this call is required or not. As its required in TCP impl, safe side adding here also
		syncWithServer();         
	}

	//		protected CachingClient _client;
		protected RFCDefaultClient _client;
		
		private static final Category category = ResourceAccessor.category;
		private static final Location location = ResourceAccessor.getLocation(RfcDefaultIPCSession.class);






	protected RfcDefaultIPCSession(RfcDefaultIPCClient ipcClient) {

//	   25.06.01 mk added the ipcClient
			_ipcClient = ipcClient;

            _pricingConverter = factory.newPricingConverter(this);            

			_documentTable = new Hashtable();
			_documents = null;
			_descriptionForProcedure = new Hashtable();
			_descriptionForCurrency  = new Hashtable();
			//Not sure whether this call is required or not. As its required in TCP impl, safe side adding here also
			syncWithServer();
		}


		/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCSession#initializeDocuments()
	 */
	protected void initializeDocuments() throws IPCException {
		try{
            JCO.Function functionSpcGetDocumentInfo = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_DOCUMENT_INFO);
			JCO.ParameterList ex = functionSpcGetDocumentInfo.getExportParameterList();
  	        
            category.logT(Severity.PATH, location, "executing RFC SPC_GET_DOCUMENT_INFO");
			((RFCDefaultClient)_client).execute(functionSpcGetDocumentInfo); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_GET_DOCUMENT_INFO");              

            JCO.Table exSpcgetdocumentinfo_etDocumentInfo = ex.getTable(SpcGetDocumentInfo.ET_DOCUMENT_INFO);
			for (int exSpcgetdocumentinfo_etDocumentInfoCounter=0; exSpcgetdocumentinfo_etDocumentInfoCounter<exSpcgetdocumentinfo_etDocumentInfo.getNumRows(); exSpcgetdocumentinfo_etDocumentInfoCounter++) {
                exSpcgetdocumentinfo_etDocumentInfo.setRow(exSpcgetdocumentinfo_etDocumentInfoCounter);
                String documentId = exSpcgetdocumentinfo_etDocumentInfo.getString(SpcGetDocumentInfo.DOCUMENT_ID);
                IPCDocument document = factory.newIPCDocument(this, documentId);
                _documentTable.put(documentId, document);
            }
			_documents = null;  // requires recalc
				   
        } catch (ClientException e) {
            throw new IPCException(e);
        }
    }

    protected synchronized void bindSharedData(String tokenId, String configId) {
		this.tokenId = tokenId;
		this.configId = configId;
		try{
			JCO.Function functionSpcBindConfiguration = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_BIND_CONFIGURATION );
			JCO.ParameterList im = functionSpcBindConfiguration.getImportParameterList();
			JCO.ParameterList ex = functionSpcBindConfiguration.getExportParameterList();
			
			im.setValue(tokenId, SpcBindConfiguration.TOKEN_ID);
			im.setValue(configId, SpcBindConfiguration.CONFIGURATION_ID);
  	        

			category.logT(Severity.PATH, location, "executing " + IFunctionGroup.SPC_BIND_CONFIGURATION);
			((RFCDefaultClient)_client).execute(functionSpcBindConfiguration); // does not define ABAP Exceptions
			category.logT(Severity.PATH, location, "done with " + IFunctionGroup.SPC_BIND_CONFIGURATION);              

				   
		} catch (ClientException e) {
			throw new IPCException(e);
		}
    }
    
    
	protected void initializeDocuments(String tokenId, String configId) throws IPCException {
		bindSharedData(tokenId, configId);
		initializeDocuments();
	}
	
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCSession#initializeConfigs()
	 */
	protected void initializeConfigs(String tokenId, String configId) throws IPCException {
		bindSharedData(tokenId, configId);
		Configuration config = factory.newConfiguration(this, configId);
		_configTable.put(configId, config);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#getAvailablePricingProcedureNames(java.lang.String)
	 */
    // is not available anymore, returning empty array
	public String[] getAvailablePricingProcedureNames(String application)
		throws IPCException {
            location.entering("getAvailablePricingProcedureNames(String application)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String application " + application);
            }
    		String procedures[] = new String[0];
            if (location.beDebug()){
                if (procedures != null) {
                    location.debugT("return procedures");
                    for (int i = 0; i<procedures.length; i++){
                        location.debugT("  procedures["+i+"] "+procedures[i]);
                    }
                }
            }
	       	return procedures;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#getAvailableCurrencyUnits()
	 */
	public String[] getAvailableCurrencyUnits() throws IPCException {
        throw new UnsupportedOperationException("getAvailableCurrencyUnits is not supported by VMC function modules.");
	}


	/**
	 * Supports multiple ISOCodeProperties with the same key.
	 * Entries have the type ISOCodeConversionPropertyList.
	 */
	class ISOCodeConversionPropertyHashMap implements Map {
		Hashtable isoCodes = new Hashtable();

		/* (non-Javadoc)
		 * @see java.util.Map#size()
		 */
		public int size() {
			// TODO Auto-generated method stub
			return isoCodes.size();
		}

		/* (non-Javadoc)
		 * @see java.util.Map#clear()
		 */
		public void clear() {
			isoCodes.clear();
		}

		/* (non-Javadoc)
		 * @see java.util.Map#isEmpty()
		 */
		public boolean isEmpty() {
			return isoCodes.isEmpty();
		}

		/* (non-Javadoc)
		 * @see java.util.Map#containsKey(java.lang.Object)
		 */
		public boolean containsKey(Object key) {
			return isoCodes.containsKey(key);
		}

		/* (non-Javadoc)
		 * @see java.util.Map#containsValue(java.lang.Object)
		 */
		public boolean containsValue(Object value) {
			for ( Iterator i = isoCodes.values().iterator(); i.hasNext(); ) {
				List entries = (List)i.next();
				for (Iterator ii = entries.iterator(); ii.hasNext(); ) {
					Object entry = (Object)ii.next();
					if (entry.equals(value)) {
						return true;
					}
				}
			}
			return false;
		}

		/* (non-Javadoc)
		 * returns the concatenated list of all ISOCodeConversionProperties
		 */
		public Collection values() {
			ArrayList returnValues = new ArrayList();
			for (Iterator i = isoCodes.values().iterator(); i.hasNext(); ) {
				ISOCodeConversionPropertyList list = (ISOCodeConversionPropertyList) i.next();
				returnValues.addAll(list.entries);
			}
			return returnValues;
		}

		/* (non-Javadoc)
		 * @see java.util.Map#putAll(java.util.Map)
		 */
		public void putAll(Map t) {
			isoCodes.putAll(t);
		}

		/* (non-Javadoc)
		 * @see java.util.Map#entrySet()
		 */
		public Set entrySet() {
			return isoCodes.entrySet();
		}

		/* (non-Javadoc)
		 * @see java.util.Map#keySet()
		 */
		public Set keySet() {
			return isoCodes.keySet();
		}

		/* (non-Javadoc)
		 * @see java.util.Map#get(java.lang.Object)
		 */
		public Object get(Object key) {
			return isoCodes.get(key);
			
		}

		/* (non-Javadoc)
		 * @see java.util.Map#remove(java.lang.Object)
		 */
		public Object remove(Object key) {
			return isoCodes.remove(key);
		}

		/* (non-Javadoc)
		 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
		 */
		public Object put(Object key, Object value) {
			ISOCodeConversionPropertyList entries = (ISOCodeConversionPropertyList)isoCodes.get(key);
			if (entries == null) {
				entries = new ISOCodeConversionPropertyList();
				entries.add(value);
				isoCodes.put(key, entries);
				return null;
			}else {
				entries.add(value);
				return entries;
			}
		}
	}
	
	/**
	 * Is a list of ISOCodeProperties. All operations will be performed
	 * on all members of the list.
	 */
	class ISOCodeConversionPropertyList implements List {
		
		ArrayList entries = new ArrayList(); 
		/**
		 * Sets the iso code of this object.
		 * This code must (!) be set for a successful
		 * call of IPCSession.determineUnitsOfMeasurement, either this way or in the constructor.
		 */
		public void setIsoCode(String newIsoCode) {
			for (Iterator i = entries.iterator(); i.hasNext(); ) {
				ISOCodeConversionProperties prop = (ISOCodeConversionProperties)i.next();
				prop.setIsoCode(newIsoCode);
			}
		}

		/**
		 * Internal. This method is used by the server to update this object. It should not
		 * be used by callers. If you set the error message, it may change at any time to
		 * reflect the server's state.
		 */
		public void setErrorMessage(String newErrorMessage) {
			for (Iterator i = entries.iterator(); i.hasNext(); ) {
				ISOCodeConversionProperties prop = (ISOCodeConversionProperties)i.next();
				prop.setErrorMessage(newErrorMessage);
			}
		}

		/**
		 * Internal. This method is used by the server to update this object. It should not
		 * be used by callers. If you set the external unit, it may change at any time to
		 * reflect the server's state.
		 */
		public void setExternalUnit(String newExternalUnit) {
			for (Iterator i = entries.iterator(); i.hasNext(); ) {
				ISOCodeConversionProperties prop = (ISOCodeConversionProperties)i.next();
				prop.setExternalUnit(newExternalUnit);
			}
		}

		/**
		 * Internal. This method is used by the server to update this object. It should not
		 * be used by callers. If you set the internal unit, it may change at any time to
		 * reflect the server's state.
		 */
		public void setInternalUnit(String newInternalUnit) {
			for (Iterator i = entries.iterator(); i.hasNext(); ) {
				ISOCodeConversionProperties prop = (ISOCodeConversionProperties)i.next();
				prop.setInternalUnit(newInternalUnit);
			}
		}

		/* (non-Javadoc)
		 * @see java.util.List#size()
		 */
		public int size() {
			return entries.size();
		}

		/* (non-Javadoc)
		 * @see java.util.List#clear()
		 */
		public void clear() {
			entries.clear();
		}

		/* (non-Javadoc)
		 * @see java.util.List#isEmpty()
		 */
		public boolean isEmpty() {
			return entries.isEmpty();
		}

		/* (non-Javadoc)
		 * @see java.util.List#toArray()
		 */
		public Object[] toArray() {
			return entries.toArray();
		}

		/* (non-Javadoc)
		 * @see java.util.List#get(int)
		 */
		public Object get(int index) {
			return entries.get(index);
		}

		/* (non-Javadoc)
		 * @see java.util.List#remove(int)
		 */
		public Object remove(int index) {
			return entries.remove(index);
		}

		/* (non-Javadoc)
		 * @see java.util.List#add(int, java.lang.Object)
		 */
		public void add(int index, Object element) {
			entries.add(index, element);
		}

		/* (non-Javadoc)
		 * @see java.util.List#indexOf(java.lang.Object)
		 */
		public int indexOf(Object o) {
			return entries.indexOf(o);
		}

		/* (non-Javadoc)
		 * @see java.util.List#lastIndexOf(java.lang.Object)
		 */
		public int lastIndexOf(Object o) {
			return entries.lastIndexOf(o);
		}

		/* (non-Javadoc)
		 * @see java.util.List#add(java.lang.Object)
		 */
		public boolean add(Object o) {
			return entries.add(o);
		}

		/* (non-Javadoc)
		 * @see java.util.List#contains(java.lang.Object)
		 */
		public boolean contains(Object o) {
			return entries.contains(o);
		}

		/* (non-Javadoc)
		 * @see java.util.List#remove(java.lang.Object)
		 */
		public boolean remove(Object o) {
			return entries.remove(o);
		}

		/* (non-Javadoc)
		 * @see java.util.List#addAll(int, java.util.Collection)
		 */
		public boolean addAll(int index, Collection c) {
			return entries.addAll(index, c);
		}

		/* (non-Javadoc)
		 * @see java.util.List#addAll(java.util.Collection)
		 */
		public boolean addAll(Collection c) {
			return entries.addAll(c);
		}

		/* (non-Javadoc)
		 * @see java.util.List#containsAll(java.util.Collection)
		 */
		public boolean containsAll(Collection c) {
			return entries.containsAll(c);
		}

		/* (non-Javadoc)
		 * @see java.util.List#removeAll(java.util.Collection)
		 */
		public boolean removeAll(Collection c) {
			return entries.removeAll(c);
		}

		/* (non-Javadoc)
		 * @see java.util.List#retainAll(java.util.Collection)
		 */
		public boolean retainAll(Collection c) {
			return entries.retainAll(c);
		}

		/* (non-Javadoc)
		 * @see java.util.List#iterator()
		 */
		public Iterator iterator() {
			return entries.iterator();
		}

		/* (non-Javadoc)
		 * @see java.util.List#subList(int, int)
		 */
		public List subList(int fromIndex, int toIndex) {
			return entries.subList(fromIndex, toIndex);
		}

		/* (non-Javadoc)
		 * @see java.util.List#listIterator()
		 */
		public ListIterator listIterator() {
			return entries.listIterator();
		}

		/* (non-Javadoc)
		 * @see java.util.List#listIterator(int)
		 */
		public ListIterator listIterator(int index) {
			return entries.listIterator(index);
		}

		/* (non-Javadoc)
		 * @see java.util.List#set(int, java.lang.Object)
		 */
		public Object set(int index, Object element) {
			return entries.set(index, element);
		}

		/* (non-Javadoc)
		 * @see java.util.List#toArray(java.lang.Object[])
		 */
		public Object[] toArray(Object[] a) {
			return entries.toArray(a);
		}

	}
	
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCSession#determineDimensionalUnits(com.sap.spc.remote.client.object.ISOCodeConversionProperties[], java.util.Locale, java.lang.String, java.lang.String, boolean)
	 */
	protected void determineDimensionalUnits(
		ISOCodeConversionProperties[] props,
		Locale locale,
		String groupSeparator,
		String decimalSeparator,
		boolean currencyUnit)
		throws IPCException {

            // needed to determine the failed conversions later
			ISOCodeConversionPropertyHashMap listOfIsoCodeProperties = new ISOCodeConversionPropertyHashMap();
			ISOCodeConversionPropertyHashMap listOfnotUsedIsoCodeProperties = new ISOCodeConversionPropertyHashMap();
            for (int i=0; i<props.length; i++) {
                String isoCode = props[i].getIsoCode();
                listOfIsoCodeProperties.put(isoCode, props[i]);
                listOfnotUsedIsoCodeProperties.put(isoCode, props[i]);
            }

            String[] failedIsoUnits = null;
            String[] conversionErrorMessages = null;
            String[] internalUnits = null;
            String[] isoUnits = null;           
            RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)this.getPricingConverter(); 

            if (currencyUnit){
                // conversion for currency
                try {
                    JCO.Function functionPrcCnvCurrConvIso = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.PRC_CNV_CURR_CONV_ISO);
                    JCO.ParameterList im = functionPrcCnvCurrConvIso.getImportParameterList();
                    JCO.ParameterList ex = functionPrcCnvCurrConvIso.getExportParameterList();
                    JCO.ParameterList tbl = functionPrcCnvCurrConvIso.getTableParameterList();
                    JCO.Table imPrccnvcurrconviso_itIsoCurrencyUnit = im.getTable(PrcCnvCurrConvIso.IT_ISO_CURRENCY_UNIT);
                    for (int i=0; i<props.length; i++) {
                        imPrccnvcurrconviso_itIsoCurrencyUnit.appendRow();
                        String isoCode = props[i].getIsoCode();
                        imPrccnvcurrconviso_itIsoCurrencyUnit.setValue(isoCode, "");     // CHAR, ISO Currency Unit
                    }
                    
                    category.logT(Severity.PATH, location, "executing RFC PRC_CNV_CURR_CONV_ISO");
                    ((RFCDefaultClient)_client).execute(functionPrcCnvCurrConvIso); // does not define ABAP Exceptions
                    category.logT(Severity.PATH, location, "done with RFC PRC_CNV_CURR_CONV_ISO");

                    // process table exPrccnvcurrconviso_etMapping
                    // TABLE, Mapping Between ISO and Internal Currency Units
                    JCO.Table exPrccnvcurrconviso_etMapping = ex.getTable(PrcCnvCurrConvIso.ET_MAPPING);
                    int mappingNumRow = exPrccnvcurrconviso_etMapping.getNumRows();
                    internalUnits = new String[mappingNumRow];
                    isoUnits = new String[mappingNumRow];

                    for (int exPrccnvcurrconviso_etMappingCounter=0; exPrccnvcurrconviso_etMappingCounter<exPrccnvcurrconviso_etMapping.getNumRows(); exPrccnvcurrconviso_etMappingCounter++) {
                        exPrccnvcurrconviso_etMapping.setRow(exPrccnvcurrconviso_etMappingCounter);
                        isoUnits[exPrccnvcurrconviso_etMappingCounter] = exPrccnvcurrconviso_etMapping.getString(PrcCnvCurrConvIso.ISO_UNIT);
                        internalUnits[exPrccnvcurrconviso_etMappingCounter] = exPrccnvcurrconviso_etMapping.getString(PrcCnvCurrConvIso.UNIT);
                    }
                    
                    // process table exPrccnvcurrconviso_etMessage
                    // TABLE, Messages From Conversion
                    JCO.Table exPrccnvcurrconviso_etMessage = ex.getTable(PrcCnvCurrConvIso.ET_MESSAGE);
                    int messageNumRow = exPrccnvcurrconviso_etMessage.getNumRows();
                    failedIsoUnits = new String[messageNumRow];
                    conversionErrorMessages = new String[messageNumRow];
                    
                    for (int exPrccnvcurrconviso_etMessageCounter=0; exPrccnvcurrconviso_etMessageCounter<exPrccnvcurrconviso_etMessage.getNumRows(); exPrccnvcurrconviso_etMessageCounter++) {
                        exPrccnvcurrconviso_etMessage.setRow(exPrccnvcurrconviso_etMessageCounter);
                        failedIsoUnits[exPrccnvcurrconviso_etMessageCounter] = exPrccnvcurrconviso_etMessage.getString(PrcCnvCurrConvIso.OBJECT);
                        conversionErrorMessages[exPrccnvcurrconviso_etMessageCounter] = exPrccnvcurrconviso_etMessage.getString(PrcCnvCurrConvIso.MESSAGE);
                    }
                }
                catch(ClientException ce){
                    throw new IPCException(ce); 
                }    
                
                // update currency units
                for (int i=0; i<isoUnits.length; i++){
                    String isoUnit = isoUnits[i];
					ISOCodeConversionPropertyList p = (ISOCodeConversionPropertyList)listOfIsoCodeProperties.get(isoUnit);
                    p.setInternalUnit(internalUnits[i]);
                    // there is no difference between internal and external currencies units
                    p.setExternalUnit(internalUnits[i]);
					listOfnotUsedIsoCodeProperties.remove(isoUnit);
                }
                
                //update values
                for ( Iterator i = listOfIsoCodeProperties.values().iterator(); i.hasNext(); ) {
                	ISOCodeConversionProperties p = (ISOCodeConversionProperties)i.next();
                    String internalValue = p.getValue();
                    if (internalValue == null) {
                        internalValue = "0";
                    }
                    BigDecimal internalValueBD = new BigDecimal(internalValue);
                    String convertedValue = pc.convertCurrencyValueInternalToExternal(internalValueBD, p.getInternalUnit(), decimalSeparator, groupSeparator);
                    p.setFormattedValue(convertedValue);
                    p.setErrorMessage(null);
                    // remove property from list
                }           
            }
            else {
                // conversion for UOM
                try {
                    JCO.Function functionPrcCnvUnitConvIso = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.PRC_CNV_UNIT_CONV_ISO);
                    JCO.ParameterList im = functionPrcCnvUnitConvIso.getImportParameterList();
                    JCO.ParameterList ex = functionPrcCnvUnitConvIso.getExportParameterList();
                    JCO.ParameterList tbl = functionPrcCnvUnitConvIso.getTableParameterList();
    
                    JCO.Table imPrccnvunitconviso_itIsoQuantityUnit = im.getTable(PrcCnvUnitConvIso.IT_ISO_QUANTITY_UNIT);
                    for (int i=0; i<props.length; i++) {
                        imPrccnvunitconviso_itIsoQuantityUnit.appendRow();
                        String isoCode = props[i].getIsoCode();
                        imPrccnvunitconviso_itIsoQuantityUnit.setValue(isoCode, "");     // CHAR, ISO Quantity Unit
                    }
                    
                    category.logT(Severity.PATH, location, "executing RFC PRC_CNV_UNIT_CONV_ISO");
                    ((RFCDefaultClient)_client).execute(functionPrcCnvUnitConvIso); // does not define ABAP Exceptions
                    category.logT(Severity.PATH, location, "done with RFC PRC_CNV_UNIT_CONV_ISO");
                    
                    // process table exPrccnvunitconviso_etMapping
                    // TABLE, ISO Quantity Unit
                    JCO.Table exPrccnvunitconviso_etMapping = ex.getTable(PrcCnvUnitConvIso.ET_MAPPING);
                    int mappingNumRow = exPrccnvunitconviso_etMapping.getNumRows();
                    internalUnits = new String[mappingNumRow];
                    isoUnits = new String[mappingNumRow];
                    for (int exPrccnvunitconviso_etMappingCounter=0; exPrccnvunitconviso_etMappingCounter<mappingNumRow; exPrccnvunitconviso_etMappingCounter++) {
                        exPrccnvunitconviso_etMapping.setRow(exPrccnvunitconviso_etMappingCounter);
                        isoUnits[exPrccnvunitconviso_etMappingCounter] = exPrccnvunitconviso_etMapping.getString(PrcCnvUnitConvIso.ISO_UNIT);
                        internalUnits[exPrccnvunitconviso_etMappingCounter] = exPrccnvunitconviso_etMapping.getString(PrcCnvUnitConvIso.UNIT);
                    }
                    
                    // process table exPrccnvunitconviso_etMessage
                    // TABLE, Messages From Conversion
                    JCO.Table exPrccnvunitconviso_etMessage = ex.getTable(PrcCnvUnitConvIso.ET_MESSAGE);
                    int messageNumRow = exPrccnvunitconviso_etMessage.getNumRows();
                    failedIsoUnits = new String[messageNumRow];
                    conversionErrorMessages = new String[messageNumRow];
                    for (int exPrccnvunitconviso_etMessageCounter=0; exPrccnvunitconviso_etMessageCounter<messageNumRow; exPrccnvunitconviso_etMessageCounter++) {
                        exPrccnvunitconviso_etMessage.setRow(exPrccnvunitconviso_etMessageCounter);
                        failedIsoUnits[exPrccnvunitconviso_etMessageCounter] = exPrccnvunitconviso_etMessage.getString(PrcCnvUnitConvIso.OBJECT);
                        conversionErrorMessages[exPrccnvunitconviso_etMessageCounter] = exPrccnvunitconviso_etMessage.getString(PrcCnvUnitConvIso.MESSAGE);
                    }
                }
                catch(ClientException ce){
                    throw new IPCException(ce); 
                }
                
                // update properties
                for (int i=0; i<isoUnits.length; i++){
                    String isoUnit = isoUnits[i];
                    ISOCodeConversionPropertyList p = (ISOCodeConversionPropertyList)listOfIsoCodeProperties.get(isoUnit);
                    p.setInternalUnit(internalUnits[i]);
                    String convertedUnit = pc.convertUOMInternalToExternal(internalUnits[i]);
                    if (convertedUnit != null){
                        p.setExternalUnit(convertedUnit);
                    }
					// remove property from list
					listOfnotUsedIsoCodeProperties.remove(isoUnit);
                }
                
                for (Iterator i = listOfIsoCodeProperties.values().iterator(); i.hasNext(); ) {
                	ISOCodeConversionProperties p = (ISOCodeConversionProperties)i.next();
                    String internalValue = p.getValue();
                    if (internalValue == null) {
                        internalValue = "0";
                    }
                    BigDecimal internalValueBD = new BigDecimal(internalValue);
                    String convertedValue = pc.convertValueInternalToExternal(internalValueBD, p.getInternalUnit(), decimalSeparator, groupSeparator);
                    p.setFormattedValue(convertedValue);
                    p.setErrorMessage(null);
                }
            }

            for (int i=0; i<failedIsoUnits.length; i++){
                String isoUnit = failedIsoUnits[i];
				ISOCodeConversionPropertyList p = (ISOCodeConversionPropertyList)listOfIsoCodeProperties.get(isoUnit);
                if (p != null){
                    p.setErrorMessage(conversionErrorMessages[i]);
                }
                listOfIsoCodeProperties.remove(isoUnit);
            }
            // set error message to remaining properties
            for (Iterator iter=listOfnotUsedIsoCodeProperties.keySet().iterator(); iter.hasNext();) {
                String key = (String)iter.next();
				ISOCodeConversionPropertyList p = (ISOCodeConversionPropertyList)listOfnotUsedIsoCodeProperties.get(key);
                p.setErrorMessage("Conversion Error");
            }              
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#close()
	 */
	public void close() {
        location.entering("close()");
        if (_client != null) {
        	//_client.cmd(com.sap.sxe.socket.shared.command.CommandConstants.CLOSE_SESSION, new String[0]);
        	_client.close();
        	_client = null;
        }else {
        	//the connection is already closed dont have to do anything
        }
    	_documents = null;
        location.exiting();
	}

	public void removeDocument(IPCDocument document) throws IPCException {
        location.entering("removeDocument(IPCDocument document)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCDocument document " + document.getId());
        }
		String documentId = document.getId();
		//No need to call a sync, which was a performance hit. just remove from data strecture
		_documentTable.remove(documentId);
		_documents = null;
		try{
		    JCO.Function functionSpcRemoveDocument = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_REMOVE_DOCUMENT);
		    JCO.ParameterList im = functionSpcRemoveDocument.getImportParameterList();
		    JCO.ParameterList ex = functionSpcRemoveDocument.getExportParameterList();
		    JCO.ParameterList tbl = functionSpcRemoveDocument.getTableParameterList();
  		    im.setValue(documentId, SpcRemoveDocument.IV_DOCUMENT_ID);		// BYTE, Pricing Document GUID

            category.logT(Severity.PATH, location, "executing RFC SPC_REMOVE_DOCUMENT");
		    ((RFCDefaultClient)_client).execute(functionSpcRemoveDocument); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_REMOVE_DOCUMENT");
            location.exiting();
		} catch(ClientException e){
            throw new IPCException(e);
        }
	 }
	 		
		//direct method from protocol layer
	 public IClient getClient() {
         location.entering("getClient()");
         if (location.beDebug()) {
             if (_client!=null){
                 location.debugT("return _client " + _client.toString());
             }
             else location.debugT("return _client null");
         }
         location.exiting();
		 return _client;
	 }
	 /**
	  * Sets Cache as dirty.
	  */
	 public void setCacheDirty() {
        location.entering("setCacheDirty()");
		if (!cacheIsDirty){
			category.log(Severity.DEBUG,location,"Setting session cache as Dirty.");
			_setSessionDocsFlagToDirty();
			_setSessionFlagToDirty();
		}
        location.exiting();
	 }

	 public void syncWithServer() throws IPCException{
        location.entering("syncWithServer()");
	 	//If cache is not dirty just return
	 	if (!cacheIsDirty) {
             location.exiting();
             return; 
        } 
	 	
		category.log(Severity.DEBUG,location,"Session cache is dirty. Syncing session with Server....");
	 	
		//get Documents from server
		try{
			JCO.Function functionSpcGetDocumentInfo = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_DOCUMENT_INFO);
			JCO.ParameterList im = functionSpcGetDocumentInfo.getImportParameterList();
			JCO.ParameterList ex = functionSpcGetDocumentInfo.getExportParameterList();
			JCO.ParameterList tbl = functionSpcGetDocumentInfo.getTableParameterList();
  	        
			category.logT(Severity.PATH, location, "executing RFC SPC_GET_DOCUMENT_INFO");
			((RFCDefaultClient)_client).execute(functionSpcGetDocumentInfo); // does not define ABAP Exceptions
			category.logT(Severity.PATH, location, "done with RFC SPC_GET_DOCUMENT_INFO");              

			JCO.Table exSpcgetdocumentinfo_etDocumentInfo = ex.getTable(SpcGetDocumentInfo.ET_DOCUMENT_INFO);
			for (int exSpcgetdocumentinfo_etDocumentInfoCounter=0; exSpcgetdocumentinfo_etDocumentInfoCounter<exSpcgetdocumentinfo_etDocumentInfo.getNumRows(); exSpcgetdocumentinfo_etDocumentInfoCounter++) {
				exSpcgetdocumentinfo_etDocumentInfo.setRow(exSpcgetdocumentinfo_etDocumentInfoCounter);
				String documentId = exSpcgetdocumentinfo_etDocumentInfo.getString(SpcGetDocumentInfo.DOCUMENT_ID);
				if (getDocument(documentId) == null){
					IPCDocument document = factory.newIPCDocument(this, documentId);
					_documentTable.put(documentId, document);
				}
			}
			_documents = null;  // requires recalc
				   
		} catch (ClientException e) {
			throw new IPCException(e);
		}
        
		cacheIsDirty = false;
        location.entering();
	 }


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#getSessionId()
	 */
	public String getSessionId() {
        location.entering("getSessionId()");
        if (location.beDebug()) {
            location.debugT("return \"\"");
        }
        location.exiting();
		return "";
	}
	
	public synchronized void lock() throws IPCException {
        location.entering("lock()");
		String locked = "";
		int lockAttempts = 0;
		try {
			JCO.Function functionSpcLockConfiguration = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_LOCK_CONFIGURATION);
			JCO.ParameterList im = functionSpcLockConfiguration.getImportParameterList();
			JCO.ParameterList ex = functionSpcLockConfiguration.getExportParameterList();
			
			im.setValue(this.configId, SpcLockConfiguration.CONFIGURATION_ID);
			
			while (!locked.equals("X") && lockAttempts <= 2) {
			    ((RFCDefaultClient)_client).execute(functionSpcLockConfiguration);		
			    locked = ex.getString(SpcLockConfiguration.LOCKED);
			    lockAttempts++;
			    if (!locked.equals("X")) try {
					wait(100);
				} catch (InterruptedException e1) {
					location.warningT(e1.toString());
				} //lock could not acquired yet, wait for 
			}
			if (!locked.equals("X")) {
				this._locked = false;
				throw new IPCException("Cannot acquire lock of shared container with configuration id:" + configId);
			}else {
				this._locked = true;
			}
            location.exiting();
		} catch (ClientException e) {
			throw new IPCException(e);
		}
	}
	
	public synchronized void unlock() {
        location.entering("unlock()");

//		   implementation
//		   get access to function
		try {
					JCO.Function functionSpcUnlockConfiguration = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_UNLOCK_CONFIGURATION);
					JCO.ParameterList im = functionSpcUnlockConfiguration.getImportParameterList();
					JCO.ParameterList ex = functionSpcUnlockConfiguration.getExportParameterList();
					JCO.ParameterList tbl = functionSpcUnlockConfiguration.getTableParameterList();
			//		   fill import parameters from internal members
					im.setValue(this.configId, SpcUnlockConfiguration.CONFIGURATION_ID);		// CHAR
			
			
					((RFCDefaultClient)_client).execute(functionSpcUnlockConfiguration); // does not define ABAP Exceptions
					this._locked = false;
                    location.exiting();
		} catch (ClientException e) {
			throw new IPCException(e);
		}

	}
	
	
	public void publish() throws IPCException {
        location.entering("publish");
		try {
			JCO.Function functionSpcRepublishConfiguration = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_REPUBLISH_CONFIGURATION);
			JCO.ParameterList im = functionSpcRepublishConfiguration.getImportParameterList();
//			   fill import parameters from internal members
			im.setValue(configId, SpcRepublishConfiguration.CONFIGURATION_ID);		// CHAR//			   fill import parameters from internal members


			((RFCDefaultClient)_client).execute(functionSpcRepublishConfiguration); // does not define ABAP Exceptions
		    location.exiting();
        }catch(ClientException e) {
			throw new IPCException(e);
		}
	}
	
	
	
	/**
	 * @return
	 */
	public boolean isLocked() {
        location.entering("isLocked()");
        if (location.beDebug()) {
            location.debugT("return _locked " + _locked);
        }
        location.exiting();
		return _locked;
	}

	/**
	 * @return
	 */
	public String getLanguage() {
        location.entering("getLanguage()");
		String lang;
		JCO.Attributes attributes = ((RFCDefaultClient)getClient()).getDefaultIPCJCoConnection().getAttributes();
		if (attributes != null){
			lang = attributes.getISOLanguage();
		}
		else {
			category.logT(Severity.WARNING, location, "Language of JCOConnection returned null. Try to use language of RfcDefaultClient.");
			lang = ((RFCDefaultClient)getClient()).getLanguage();
		}
		if (lang == null){
			category.logT(Severity.ERROR, location, "Neither JCOConnection nor RFCDefaultClient returned a language. Set language to \"EN\"");
			lang = "EN";
		}
        if (location.beDebug()) {
            location.debugT("return lang " + lang);
        }
        location.exiting();
		return lang;
	}


}
