package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.TTEBusinessPartner;
import com.sap.spc.remote.client.object.TTEConstants;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.spc.remote.client.object.TTEItem;
import com.sap.spc.remote.client.object.TTEProduct;
import com.sap.tc.logging.Location;

public abstract class DefaultTTEDocument extends BusinessObjectBase implements TTEDocument, TTEConstants {

    protected static final Location location = ResourceAccessor.getLocation(DefaultTTEDocument.class);
    public static final String TRACEMODE_DEFAULT         = "01";
    public static final String TRACEMODE_03              = "03";
    public static final String BUS_TRANS_ORDER           = "ORD";
    public static final String PROC_MODE_DEFAULT         = "01";
    public static final String BP_TAXABILITY_DEFAULT     = "100";
    

    // attributes 
    protected DefaultIPCSession session;
    protected String documentId;
    protected String processingMode;
    protected String traceMode;
    protected boolean textOutput;
    protected String language;
    protected String systemId1;
    protected String ownBusinessPartnerId;
    protected String documentCurrencyUnit;
    protected String businessTransaction;
    protected ArrayList businessPartners;
    protected ArrayList tteItems;    
    protected ArrayList products;
    protected HashMap productsById;

    // arrays of attribute names
    protected String[] tax_type_bp; // taxTypeBusinessPartner;
    protected String[] tax_dep_ty_bp; //taxTypeDepartBusinessPartner;
    protected String[] tax_group_bp; // taxGroupBusinessPartner;
    protected String[] tax_dep_gr_bp; //taxGroupDepartBusinessPartner;
    protected static HashMap tteConstants;

    protected DefaultTTEDocument(DefaultIPCSession session, IPCDocument document) {
        super(new TechKey(document.getId()));
        this.session = session;
        initAttributeArrays();
        initAttributes(document);
    }
    
    protected DefaultTTEDocument(){
        initAttributes();
    }

    private void initAttributes(IPCDocument document){
        HashMap headerAttributes = (HashMap) document.getHeaderAttributes();
        IPCDocumentProperties docProps = ((DefaultIPCDocument)document)._getDocumentProperties(false);
        this.documentId = document.getId();
        this.processingMode = PROC_MODE_DEFAULT; 
        this.traceMode = TRACEMODE_DEFAULT;
        // if the pricing analysis is enabled we set the trace mode explicitly to 03
        if (docProps.getPerformPricingAnalysis()){
            this.traceMode = TRACEMODE_03;
        }
        this.textOutput = true;
        this.language = document.getLanguage();
        this.systemId1 = getHeaderAttributeValue(headerAttributes, TTE_LOGSYS);
        this.ownBusinessPartnerId = getHeaderAttributeValue(headerAttributes, SALES_ORG);
        this.documentCurrencyUnit = docProps.getDocumentCurrency();
        this.businessTransaction = BUS_TRANS_ORDER;
        this.businessPartners = createBusinessPartners(headerAttributes);
        this.products = new ArrayList();
        this.productsById = new HashMap();
        this.tteItems = new ArrayList(); // at this point in time the list of TTE items is empty
    }

    private void initAttributes(){
        this.documentId = "";
        this.processingMode = PROC_MODE_DEFAULT; 
        this.traceMode = TRACEMODE_03;
        this.textOutput = true;
        this.language = "";
        this.systemId1 = "";
        this.ownBusinessPartnerId = "";
        this.documentCurrencyUnit = "";
        this.businessTransaction = BUS_TRANS_ORDER;
        this.businessPartners = new ArrayList();
        this.products = new ArrayList();
        this.productsById = new HashMap();
        this.tteItems = new ArrayList(); 
    }

    /**
     * @param headerAttributes document header attributes
     * @return list of business partners (actually: ship-from and ship-to)
     */
    private ArrayList createBusinessPartners(HashMap headerAttributes){
        ArrayList bpList = new ArrayList();        
        // create ship-from business partner
        TTEBusinessPartner businessPartnerSF = createBusinessPartnerSF(headerAttributes);
        bpList.add(businessPartnerSF);
        // create ship-to business partner
        TTEBusinessPartner businessPartnerST = createBusinessPartnerST(headerAttributes);
        bpList.add(businessPartnerST);
        return bpList;
    }

    /**
     * @param headerAttributes document header attributes
     * @return ship-from business partner
     */
    private TTEBusinessPartner createBusinessPartnerSF(HashMap headerAttributes) {
        String ownBusinessPartnerId = getHeaderAttributeValue(headerAttributes, SALES_ORG);
        String bpCountry = getHeaderAttributeValue(headerAttributes, TAX_DEP_CTY_ISO);
        String bpCompanyCode = getHeaderAttributeValue(headerAttributes, COMPANY_CODE);
        String bpRegion = getHeaderAttributeValue(headerAttributes, TAX_DEPART_REG);
        String bpTaxability = BP_TAXABILITY_DEFAULT;
        String bpJurisdictionCode = getHeaderAttributeValue(headerAttributes, TAXJURCODE_FROM);
        
        // bp tax numbers        
        ArrayList bpTaxNumbers = new ArrayList();
        bpTaxNumbers.add(getHeaderAttributeValue(headerAttributes, VAT_REG_NO_FROM));
        ArrayList bpTaxNumberTypes = new ArrayList();
        bpTaxNumberTypes.add(getHeaderAttributeValue(headerAttributes, TAX_DEP_CTY_ISO) + "0");
        
        //bp tax classifications
        ArrayList bpTaxGroups = getHeaderAttributeValues(headerAttributes, tax_dep_gr_bp);
        ArrayList bpTaxTypes = getHeaderAttributeValues(headerAttributes, tax_dep_ty_bp);
        TTEBusinessPartner businessPartnerSF = IPCClientObjectFactory.getInstance().newTTEBusinessPartner(
            ownBusinessPartnerId,
            bpCountry,
            bpRegion,
            "", // county
            "", // city,
            "", // postalCode
            "", // geoCode
            bpJurisdictionCode,
            "", // exemptedRegion
            bpCompanyCode,
            bpTaxability,
            bpTaxTypes,
            bpTaxGroups,
            bpTaxNumbers,
            bpTaxNumberTypes);        
        return businessPartnerSF;
    }

    /**
     * @param headerAttributes document header attributes
     * @return ship-to business partner
     */
    private TTEBusinessPartner createBusinessPartnerST(HashMap headerAttributes) {
        String businessPartnerId = getHeaderAttributeValue(headerAttributes, TTE_PARTNER_ID);
        String bpCountry = getHeaderAttributeValue(headerAttributes, TAX_DEST_CTY_ISO);
        String bpRegion = getHeaderAttributeValue(headerAttributes, TAX_DEST_REG);
        String bpTaxability = BP_TAXABILITY_DEFAULT;
        String bpJurisdictionCode = getHeaderAttributeValue(headerAttributes, TAXJURCODE);
        
        // bp tax numbers        
        ArrayList bpTaxNumbers = new ArrayList();
        bpTaxNumbers.add(getHeaderAttributeValue(headerAttributes, VAT_REG_NO));
        ArrayList bpTaxNumberTypes = new ArrayList();
        bpTaxNumberTypes.add(getHeaderAttributeValue(headerAttributes, TAX_DEST_CTY_ISO) + "0");
        
        //bp tax classifications
        ArrayList bpTaxGroups = getHeaderAttributeValues(headerAttributes, tax_group_bp);
        ArrayList bpTaxTypes = getHeaderAttributeValues(headerAttributes, tax_type_bp);
        TTEBusinessPartner businessPartnerST = IPCClientObjectFactory.getInstance().newTTEBusinessPartner(
            businessPartnerId,
            bpCountry,
            bpRegion,
            "", // county
            "", // city,
            "", // postalCode
            "", // geoCode
            bpJurisdictionCode,
            "", // exemptedRegion
            "", // legal entity
            bpTaxability,
            bpTaxTypes,
            bpTaxGroups,
            bpTaxNumbers,
            bpTaxNumberTypes);        
        return businessPartnerST;
    }

    private String getHeaderAttributeValue(HashMap headerAttributes, String attributeName) {
        String attributeValue = (String) headerAttributes.get(attributeName);
        if (attributeValue == null) {
            attributeValue = "";
        }
        return attributeValue;
    }
    
    private ArrayList getHeaderAttributeValues(HashMap headerAttributes, String[] attributeNames) {
        int max = 9;
        ArrayList values = new ArrayList();
        for (int i = 0; i < max; i++){
            values.add(getHeaderAttributeValue(headerAttributes, attributeNames[i]));
        }
        return values;
    }
    

    protected void changeAttributes(HashMap documentHeaderAttributes){
        this.systemId1 = getHeaderAttributeValue(documentHeaderAttributes, TTE_LOGSYS);
        this.ownBusinessPartnerId = getHeaderAttributeValue(documentHeaderAttributes, SALES_ORG);
        // overwrites the business partner list
        this.businessPartners = createBusinessPartners(documentHeaderAttributes);
    }
    
    /**
     * Calls FM AP_TTE_CALCULATE_TAXES for document creation on server.
     */
    protected abstract void createDocument();
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getId()
     */
    public String getId() {
        return this.documentId;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getBusinessPartners()
     */
    public ArrayList getBusinessPartners() {
        return businessPartners;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getBusinessTransaction()
     */
    public String getBusinessTransaction() {
        return businessTransaction;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#changeDocumentCurrencyUnit(java.lang.String)
     */
    public abstract void changeDocumentCurrencyUnit(String currencyUnit) throws IPCException;

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getDocumentCurrencyUnit()
     */
    public String getDocumentCurrencyUnit() {
        return documentCurrencyUnit;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getLanguage()
     */
    public String getLanguage() {
        return language;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getOwnBusinessPartnerId()
     */
    public String getOwnBusinessPartnerId() {
        return ownBusinessPartnerId;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getProcessingMode()
     */
    public String getProcessingMode() {
        return processingMode;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getSystemId1()
     */
    public String getSystemId1() {
        return systemId1;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getTraceMode()
     */
    public String getTraceMode() {
        return traceMode;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getTteItems()
     */
    public ArrayList getTteItems() {
        return tteItems;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#isTextOutput()
     */
    public boolean isTextOutput() {
        return textOutput;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#changeDocument(java.util.HashMap)
     */
    public abstract void changeDocument(HashMap documentHeaderAttributes) throws IPCException;

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#removeItem(TTEItem)
     */
    public void removeItem(TTEItem tteItem) {
        tteItems.remove(tteItem);
    }

    /**
     * Fills the arrays with the attribute names
     */
    private void initAttributeArrays(){        
        tax_type_bp = new String[9];
        tax_type_bp[0] = TAX_TYPE_BP_01;
        tax_type_bp[1] = TAX_TYPE_BP_02;
        tax_type_bp[2] = TAX_TYPE_BP_03;
        tax_type_bp[3] = TAX_TYPE_BP_04;
        tax_type_bp[4] = TAX_TYPE_BP_05;
        tax_type_bp[5] = TAX_TYPE_BP_06;
        tax_type_bp[6] = TAX_TYPE_BP_07;
        tax_type_bp[7] = TAX_TYPE_BP_08;
        tax_type_bp[8] = TAX_TYPE_BP_09;
        
        tax_dep_ty_bp = new String[9];
        tax_dep_ty_bp[0] = TAX_DEP_TY_BP_01;
        tax_dep_ty_bp[1] = TAX_DEP_TY_BP_02;
        tax_dep_ty_bp[2] = TAX_DEP_TY_BP_03;
        tax_dep_ty_bp[3] = TAX_DEP_TY_BP_04;
        tax_dep_ty_bp[4] = TAX_DEP_TY_BP_05;
        tax_dep_ty_bp[5] = TAX_DEP_TY_BP_06;
        tax_dep_ty_bp[6] = TAX_DEP_TY_BP_07;
        tax_dep_ty_bp[7] = TAX_DEP_TY_BP_08;
        tax_dep_ty_bp[8] = TAX_DEP_TY_BP_09;
                            
        tax_group_bp = new String[9];
        tax_group_bp[0] = TAX_GROUP_BP_01;
        tax_group_bp[1] = TAX_GROUP_BP_02;
        tax_group_bp[2] = TAX_GROUP_BP_03;
        tax_group_bp[3] = TAX_GROUP_BP_04;
        tax_group_bp[4] = TAX_GROUP_BP_05;
        tax_group_bp[5] = TAX_GROUP_BP_06;
        tax_group_bp[6] = TAX_GROUP_BP_07;
        tax_group_bp[7] = TAX_GROUP_BP_08;
        tax_group_bp[8] = TAX_GROUP_BP_09;
        
        tax_dep_gr_bp = new String[9];
        tax_dep_gr_bp[0] = TAX_DEP_GR_BP_01;
        tax_dep_gr_bp[1] = TAX_DEP_GR_BP_02;
        tax_dep_gr_bp[2] = TAX_DEP_GR_BP_03;
        tax_dep_gr_bp[3] = TAX_DEP_GR_BP_04;
        tax_dep_gr_bp[4] = TAX_DEP_GR_BP_05;
        tax_dep_gr_bp[5] = TAX_DEP_GR_BP_06;
        tax_dep_gr_bp[6] = TAX_DEP_GR_BP_07;
        tax_dep_gr_bp[7] = TAX_DEP_GR_BP_08;
        tax_dep_gr_bp[8] = TAX_DEP_GR_BP_09;
        
        // create a HashMap with all TTE relevant constants
        if (tteConstants == null){
            tteConstants = new HashMap();
            tteConstants.put(COMPANY_CODE,"");
            tteConstants.put(SOLD_TO_PARTY,"");
            tteConstants.put(TAX_DEST_CTY,"");
            tteConstants.put(TAX_DEST_REG,"");
            tteConstants.put(VAT_REG_NO,"");
            tteConstants.put(VAT_REG_NO_FROM,"");
            tteConstants.put(TAXJURCODE,"");
            tteConstants.put(TAXJURCODE_FROM,"");
            tteConstants.put(INCOTERMS1,"");
            tteConstants.put(INCOTERMS2,"");
            tteConstants.put(TAX_DEPART_REG,"");
            tteConstants.put(TAX_TYPE_P_01,""); 
            tteConstants.put(TAX_TYPE_P_02,"");
            tteConstants.put(TAX_TYPE_P_03,"");
            tteConstants.put(TAX_TYPE_P_04,"");
            tteConstants.put(TAX_TYPE_P_05,"");
            tteConstants.put(TAX_TYPE_P_06,"");
            tteConstants.put(TAX_TYPE_P_07,"");
            tteConstants.put(TAX_TYPE_P_08,"");
            tteConstants.put(TAX_TYPE_P_09,"");
            tteConstants.put(TAX_GROUP_P,"");
            tteConstants.put(TAX_GROUP_P_01,"");
            tteConstants.put(TAX_GROUP_P_02,"");
            tteConstants.put(TAX_GROUP_P_03,"");
            tteConstants.put(TAX_GROUP_P_04,"");
            tteConstants.put(TAX_GROUP_P_05,"");
            tteConstants.put(TAX_GROUP_P_06,"");
            tteConstants.put(TAX_GROUP_P_07,"");
            tteConstants.put(TAX_GROUP_P_08,"");
            tteConstants.put(TAX_GROUP_P_09,"");
            for (int i=0;i<tax_type_bp.length; i++){
                tteConstants.put(tax_type_bp[i],"");
            }
            for (int i=0;i<tax_dep_ty_bp.length; i++){
                tteConstants.put(tax_dep_ty_bp[i],"");
            }
            for (int i=0;i<tax_group_bp.length; i++){
                tteConstants.put(tax_group_bp[i],"");
            }
            for (int i=0;i<tax_dep_gr_bp.length; i++){
                tteConstants.put(tax_dep_gr_bp[i],"");
            }        
            tteConstants.put(TAX_DEPT_TYPE_P_01,"");
            tteConstants.put(TAX_DEPT_TYPE_P_02,"");
            tteConstants.put(TAX_DEPT_TYPE_P_03,"");
            tteConstants.put(TAX_DEPT_TYPE_P_04,"");
            tteConstants.put(TAX_DEPT_TYPE_P_05,"");
            tteConstants.put(TAX_DEPT_TYPE_P_06,"");
            tteConstants.put(TAX_DEPT_TYPE_P_07,"");
            tteConstants.put(TAX_DEPT_TYPE_P_08,"");
            tteConstants.put(TAX_DEPT_TYPE_P_09,"");
            tteConstants.put(TAX_DEPT_GROUP_P_01,"");
            tteConstants.put(TAX_DEPT_GROUP_P_02,"");
            tteConstants.put(TAX_DEPT_GROUP_P_03,"");
            tteConstants.put(TAX_DEPT_GROUP_P_04,"");
            tteConstants.put(TAX_DEPT_GROUP_P_05,"");
            tteConstants.put(TAX_DEPT_GROUP_P_06,"");
            tteConstants.put(TAX_DEPT_GROUP_P_07,"");
            tteConstants.put(TAX_DEPT_GROUP_P_08,"");
            tteConstants.put(TAX_DEPT_GROUP_P_09,"");
            tteConstants.put(TAX_DEP_CTY_ISO,"");
            tteConstants.put(TAX_DEST_CTY_ISO,"");
            tteConstants.put(TTE_PARTNER_ID,"");
            tteConstants.put(TTE_LOGSYS,"");
            tteConstants.put(PROD_TAXABILITY_TYPE,"");
            tteConstants.put(SALES_ORG,"");  
        }      
    }
    
    /**
     * Check the document header attributes whether TTE document creation
     * is wanted.
     * @param headerAttributes document header attributes
     * @return true if "TTE_DOC_CREATE" exists AND is set to "X"
     */
    public static boolean isDocumentCreationEnabled(HashMap headerAttributes){
        boolean documentCreationEnabled = false;
        String attributeValue = (String) headerAttributes.get(TTE_DOC_CREATE);
        if ((attributeValue != null) && (attributeValue.equals(TTE_DOC_CREATE_FLAG))) {
            documentCreationEnabled = true;
        }
        return documentCreationEnabled; 
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#addItem(com.sap.spc.remote.client.object.TTEItem)
     */
    public void addItem(TTEItem tteItem){
        if (tteItem != null){
            tteItems.add(tteItem);
        }
        else {
            location.errorT("Passed tteItem is null. It has not been added to the TTEDocument!");
        }
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#modifyDocumentOnServer()
     */
    public abstract void modifyDocumentOnServer();

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#addProduct(com.sap.spc.remote.client.object.TTEProduct)
     */
    public void addProduct(TTEProduct tteProduct) {
        String key = tteProduct.getProductId();
        if (productsById.containsKey(key)){
            TTEProduct productToBeRemoved = (TTEProduct)productsById.remove(key);
            products.remove(productToBeRemoved);
        }   
        productsById.put(key, tteProduct);
        products.add(tteProduct);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#containsProduct(java.lang.String)
     */
    public boolean containsProduct(String productId) {
        if (productsById.containsKey(productId)){
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getProducts()
     */
    public ArrayList getProducts() {
        return products;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getProductsById()
     */
    public HashMap getProductsById() {
        return productsById;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#removeProduct(java.lang.String)
     */
    public void removeProduct(String productId) {
        TTEProduct productToBeRemoved = (TTEProduct)productsById.remove(productId);
        products.remove(productToBeRemoved);
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#removeItemsOnServer(com.sap.spc.remote.client.object.IPCItem[])
     */
    public abstract void removeItemsOnServer(List ipcItems);

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getTTEAnalysisDataXml()
     */
    public abstract String[] getTTEAnalysisDataXml() throws IPCException;

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getTTEAnalysisData()
     */
    public abstract String[] getTTEAnalysisData() throws IPCException;
    

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#setTraceMode(java.lang.String)
     */
    public void setTraceMode(String traceMode) {
        this.traceMode = traceMode;
    }

    /**
     * This helper method checks whether the TTE attributes in the Map and the names/values arrays
     * differ in the following ways:<br>
     * 1. Values have been changed<br>
     * 2. New attributes have been added<br>
     * Only TTE constants are taken into account!
     * @param existing Map with existing attributes
     * @param names array with new names
     * @param values array with new values
     * @return
     */
    public boolean checkForChangedAndAddedTTEAttributeValues(Map existing, String[] names, String[] values){
        boolean changed = false;
         
        for (int i=0; i<names.length; i++){
            // check whether it is a TTE constant
            if (tteConstants.get(names[i]) == null){
                // it is not in the HashMap of TTE constants -> skip it
                continue;
            }
            // check for existance in old values
            String oldValue = (String) existing.get(names[i]);
            if (oldValue == null){
                // not existing -> has been added
                if (values[i] != null){
                    // set changed-flag only if the new value is not null
                    changed = true;
                    break;
                }
            }
            else {
                // if key exists already, check for equality
                if (!oldValue.equals(values[i])){
                    changed = true;
                    break;
                }
            }
        }
        
        return changed;
    }
}
