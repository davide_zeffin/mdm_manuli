package com.sap.spc.remote.client.object.imp;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.spc.remote.client.object.Delta;

public class DefaultDelta extends BusinessObjectBase implements Delta {

    private String fieldName;
    private String value1;
    private String value2;

    /**
     * @param fieldName name of the field, where the delta occurs
     * @param val1 the old value of the field
     * @param val2 the new value of the field
     */
    public DefaultDelta(String fieldName, String val1, String val2) {
        super();
        this.fieldName = fieldName;
        this.value1 = val1;
        this.value2 = val2;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Delta#getFieldName()
     */
    public String getFieldName() {
        return fieldName;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Delta#getValue1()
     */
    public String getValue1() {
        return value1;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Delta#getValue2()
     */
    public String getValue2() {
        return value2;
    }

}
