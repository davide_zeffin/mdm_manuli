/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCException;


/**
 * A DimensionalValue is any value that can be expressed by a (floating point)
 * number and a unit of some kind. This data type is used to represent sizes,
 * volumes, prices and so on. No conversion of units is supported in any way.
 */
public class DimensionalValue implements com.sap.spc.remote.client.object.DimensionalValue
{
	private String     _value;
	private String     _unit;

	private IClient _client;
	private String        _command;
	private String[]      _params;
	private String        _valueParam;
	private String        _unitParam;

	public DimensionalValue() {
		_value = null;
		_unit  = null;
	}

	/**
	 * Creates a dimensional value.
	 * @deprecated typically, DimensionalValues can't be converted into numbers, since
	 * all data communicated with IPC are language dependent, so the numbers might be
	 * formatted in any way.
	 * @param	value	the value
	 * @param	unit	the unit of measurement of this value. (eg. PC, USD).
	 */
    public DimensionalValue(double value, String unit) {
		this();
		setUnitAndValue(value, unit);
    }

	/**
	 * Creates a new DimensionalValue.
	 */
	public DimensionalValue(String value, String unit) {
		this();
		setUnitAndValue(value, unit);
	}

	public void setValue(com.sap.spc.remote.client.object.DimensionalValue other) throws IPCException {
		_unit = other.getUnit();
		_value = other.getValueAsString();
	}


	/**
	 * Returns the numeric value of this value.
	 * @deprecated
	 */
	public double getValue() throws IPCException {
		try {
		    return Double.parseDouble(getValueAsString());
		}
		catch(NumberFormatException e) {
			throw new IPCException(e);
		}
	}

	/**
	 * Returns the value as a (formatted) string.
	 */
	public String getValueAsString() throws IPCException {
	    return _value/*.getSingleContent()*/;
	}

	/**
	  * Returns the unit of this value.
	 */
	public String getUnit() throws IPCException {
		return _unit/*.getSingleContent()*/;
	}


	/**
	 * Changes unit and value of this DimensionalValue.
	 */
	public void setUnitAndValue(double value, String unit) {
		setUnitAndValue(Double.toString(value), unit);
    }


	/**
	 * Changes unit and value of this DimensionalValue.
	 */
	public void setUnitAndValue(String value, String unit) {
		_value = value;
		_unit = unit;
	}


	/**
	 * Returns a simple string representation of this dimensional value
	 * (value <space> unit), or null, if communication to the server
	 * was needed and failed.
	 */
	public String toString() {
		try {
			return _value/*.getSingleContent()*/+" "+_unit/*.getSingleContent()*/;
		}
		catch(IPCException e) {
			return null;
		}
	}

	void setPredeterminedValue(Object cacheKey, String value, String unit) {
		_value = value;
		_unit = unit;
	}
}