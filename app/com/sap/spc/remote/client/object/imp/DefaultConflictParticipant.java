package com.sap.spc.remote.client.object.imp;

import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.FrameworkConfigManager.BusinessObject;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ConflictParticipant;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.shared.command.GetFacts;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;


public abstract class DefaultConflictParticipant extends BusinessObject implements ConflictParticipant {

    protected IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
    protected IClient cachingClient;
    protected IPCClient ipcClient;
    protected Conflict conflict;
    protected boolean closed;
    protected String id;
    protected String text;
    protected int solutionRate;
    protected boolean causedByUser;
    protected boolean causedByDefault;
    protected boolean textAdjusted;
    protected boolean keyAdjusted;
    protected String factKey;
    protected String key;
    protected boolean hasTheHighestSolutionRate;
    protected boolean isTheDefaultToDelete;

    // logging
    protected static final Category category =
	ResourceAccessor.category;
    protected static final Location location =
		ResourceAccessor.getLocation(DefaultConflictParticipant.class);

    public String getId() {
        return this.id;
    }
    
    public Conflict getConflict() {
        return this.conflict;
    }
    
    public String getText(Locale locale, MessageResources resources) {
        if (this.textAdjusted) {
            return this.text;
        }
        return adjustText(this.text, locale, resources);
    }

    public String getKeyWithId() {
        return this.factKey;
    }
    
    public String getKey() {
        if (keyAdjusted)
            return this.key;
        String keys[] = tokenizeKey(factKey);
        return keys[1] + keys[2] + keys[3] + keys[4];
    }

    protected void setKey(String key) {
        this.key = key;
        this.keyAdjusted = true;
    }
    
    protected String[] tokenizeKey(String factKey) {
        StringTokenizer factKeyTokenizer =
            new StringTokenizer(
                factKey,
                GetFacts.FACT_KEY_DELIMITER,
                false);
        String[] keys = new String[5];
        int i = 0;
        while (factKeyTokenizer.hasMoreTokens()) {
            if (i == 5)
                break;
            keys[i] = factKeyTokenizer.nextToken();
            i++;
        }
        return keys;
    }
    
    public String adjustText(
        String text,
        Locale locale,
        MessageResources resources) {
        int indexStart;
        int indexEnd;
        if (locale == null)
            locale = Locale.getDefault();

        String keys[] = tokenizeKey(factKey);
        String instId = keys[GetFacts.FACT_KEY_INST_ID];
        String observType = keys[GetFacts.FACT_KEY_OBSERV_TYPE];
        String observable = keys[GetFacts.FACT_KEY_OBSERVABLE];
        String binding = keys[GetFacts.FACT_KEY_BINDING];
        setKey(
            GetFacts.FACT_KEY_PLACEHOLDER
                + GetFacts.FACT_KEY_DELIMITER
                + instId
                + GetFacts.FACT_KEY_DELIMITER
                + observType
                + GetFacts.FACT_KEY_DELIMITER
                + observable
                + GetFacts.FACT_KEY_DELIMITER
                + binding);

        text =
            observable
                + ": "
                + ExplanationTool.VALUE_LAYOUT_TAG_BEGIN
                + binding
                + ExplanationTool.VALUE_LAYOUT_TAG_END;
        //  IS_DEFAULT
        if (isCausedByDefault())
            text = text + " " + resources.getMessage(locale, "ipc.conflict.is.default");
        this.textAdjusted = true;
        return text;
    }

    public boolean isCausedByUser() {
        return causedByUser;
    }
    
    public boolean isCausedByDefault() {
        return causedByDefault;
    }

    public void setSolutionRate(int solutionRate) {
        this.solutionRate = solutionRate;
    }
    
    public int getSolutionRate() {
        return solutionRate;
    }
    
    public void setHasTheHighestSolutionRate(boolean value) {
        this.hasTheHighestSolutionRate = value;
    }
    
    public boolean hasTheHighestSolutionRate() {
        return hasTheHighestSolutionRate;
    }
    
    public void setIsTheDefaultToDelete(boolean value) {
        this.isTheDefaultToDelete = value;
    }
    
    public boolean isTheDefaultToDelete() {
        return isTheDefaultToDelete;
    }
    
    public boolean isSuggestedForDeletion() {
        //  only one participant in the conflict
        if (conflict.countParticipants() == 1) {
            return true;
        }
        //  has the highes solution rate
        if (hasTheHighestSolutionRate) {
            return true;
        }
        if (isTheDefaultToDelete) {
            return true;
        }
        return false;
    }

    public abstract void remove();

    public boolean isRelevant(String commandName) {
        // we don't use the cache
        return true;
    }
    
    public boolean isClosed() {
        return closed;
    }
    
    public void close() {
        this.closed = true;
    }
    
    protected String getConfigId() {
        return conflict.getExplanationTool().getConfiguration().getId();
    }
}
