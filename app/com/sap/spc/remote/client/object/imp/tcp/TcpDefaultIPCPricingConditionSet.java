/*
 * Created on Dec 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import java.util.*;

import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.ValueDescriptionPairs;
import com.sap.spc.remote.client.object.imp.*;
import com.sap.spc.remote.shared.command.*;
import com.sap.msasrv.constants.ServerConstants;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.tcp.ServerResponse;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TcpDefaultIPCPricingConditionSet
	extends DefaultIPCPricingConditionSet
	implements IPCPricingConditionSet {
	
	//Constructors
	/**
	* Creates a set of pricing conditions
	*/
	protected TcpDefaultIPCPricingConditionSet(IPCDocument document){
		this(document, null, true);
	}

	protected TcpDefaultIPCPricingConditionSet(IPCItem item){
		this(item.getDocument(), item, false);
	}

	protected TcpDefaultIPCPricingConditionSet(IPCDocument document, IPCItem item, boolean isHeader){
		_document = document;
		_item = item;
		_session = document.getSession();
		// 20020315-kha: new "all or nothing" client.object implementation to clean up API (cf IPCClient internal comment)
		if (!(_session instanceof DefaultIPCSession))
			throw new IllegalClassException(_session, DefaultIPCSession.class);
		_client = ((TcpDefaultIPCSession)_session).getClient();
		_messages = new ArrayList();
		_conditionsToBeAdded = new ArrayList();
		_conditionsToBeRemoved = new ArrayList();
		_isHeader = isHeader;
	}



		//********Implementation for Abstract methods starts from here!********

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_getExternalPricingConditions()
	 */
	protected HashMap _getExternalPricingConditions()
	{

	   HashMap data = new HashMap();

	  //get item conditions
	   try{
		   com.sap.spc.remote.client.tcp.ServerResponse r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_ITEM_CONDITIONS,
			   new String[] {
				   GetItemConditions.DOCUMENT_ID, getDocumentId(),
				   GetItemConditions.ITEM_ID, getItemId(),
				   GetItemConditions.FORMAT_VALUE, "N",
				   GetItemConditions.R3_VALUE, "Y",
				   GetItemConditions.IS_SWINGGUI, ServerConstants.YES //damit auch pricingUnitDimensionName geliefert wird
			   });

		   String nextParam = null;
		   String value = null;
		   String key = null;
		   for (Enumeration e = r.getParametersEnum(); e.hasMoreElements();) {
			   nextParam = (String)e.nextElement();
			   if ( nextParam.startsWith("COND-") ) {
				   String values[] = r.getParameterValues(nextParam);
				   for (int i=0; i<values.length; i++) {
					   value = values[i];
					   if (value == null) value = "";

					   key = nextParam + "[" + (i+1) +"]";
					   data.put(key, value);
				   }
			   }
		   }
	   }
	   catch(ClientException e){
		   throw new IPCException(e);
	   }

	   return data;
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_retrieveConditionsFromServer()
	 */
	protected void _retrieveConditionsFromServer() throws IPCException
	{
	   //get conditions
	   try{
			_conditions = new ArrayList();
			com.sap.spc.remote.client.tcp.ServerResponse r = ((TCPDefaultClient)_client).doCmd(
				_isHeader?com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_ALL_HEADER_CONDITIONS:com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_ITEM_CONDITIONS,
				new String[] {
					GetItemConditions.DOCUMENT_ID, getDocumentId(),
					GetItemConditions.ITEM_ID, getItemId(),
//					GetItemConditions.FORMAT_VALUE, ServerConstants.YES 
					GetItemConditions.IS_SWINGGUI, ServerConstants.YES //damit auch pricingUnitDimensionName geliefert wird
				});

			//create IPCPricingConditions and add them to this Set
			String[] stepNos = r.getParameterValues(GetConditions.PROCEDURE_STEP_NUMBERS, new String[0]);
			String[] counters = r.getParameterValues(GetConditions.CONDITION_COUNTERS);
		String[] headercounters = r.getParameterValues(GetConditions.HEADER_CONDITION_COUNTERS);
			
			String[] conditionTypeNames = r.getParameterValues(GetConditions.CONDITION_TYPES);
			String[] descriptions = r.getParameterValues(GetConditions.DESCRIPTIONS);
			String[] conditionRates = r.getParameterValues(GetConditions.CONDITION_RATES);
			String[] conditionCurrencies = r.getParameterValues(GetConditions.CONDITION_CURRENCIES);
			String[] conditionPricingUnitValues = r.getParameterValues(GetConditions.CONDITION_PRICING_UNIT_VALUES);
			String[] conditionPricingUnitUnits = r.getParameterValues(GetConditions.CONDITION_PRICING_UNIT_UNITS);
			String[] conditionPricingUnitDimensionNames = r.getParameterValues("pricingUnitDimensionNames"); 
			String[] conditionValues = r.getParameterValues(GetConditions.CONDITION_VALUES);
			String[] documentCurrencies = r.getParameterValues(GetConditions.DOCUMENT_CURRENCIES);
			String[] conversionNumerators = r.getParameterValues(GetConditions.CONVERSION_NUMERATORS);
			String[] conversionDenominators = r.getParameterValues(GetConditions.CONVERSION_DENOMINATORS);
			String[] conversionExponents = r.getParameterValues(GetConditions.CONVERSION_EXPONENTS);
			String[] conditionBaseValues = r.getParameterValues(GetConditions.CONDITION_BASE_VALUES);

			String[] exchangeRatesExtRepresentation = r.getParameterValues("exchangeRateExtRepresentations"); 
			String[] directExchangeRatesExtRepresentation = r.getParameterValues("directExchangeRateExtRepresentations"); 
			String[] isDirectExchangeRates = r.getParameterValues("isDirectExchangeRates"); 
			String[] factors = r.getParameterValues(GetConditions.CONDITION_BASE_VALUE_FACTOR_PERIODS);
			String[] variantConditionFlags = r.getParameterValues(GetConditions.INDICATORS_CONDITION_CONFIGURATION);
		    String[] variantCondKeys = r.getParameterValues(GetConditions.VARIANT_CONDITIONS);
			String[] variantFactors = r.getParameterValues(GetConditions.CONDITION_BASE_VALUE_FACTORS);
			String[] indicatorStatisticals = r.getParameterValues(GetConditions.INDICATORS_STATISTICAL);
			String[] conditionIsInactives = r.getParameterValues(GetConditions.CONDITION_IS_INACTIVES);
			String[] conditionClasses = r.getParameterValues(GetConditions.CONDITION_CLASSES);
			String[] calculationTypes = r.getParameterValues(GetConditions.CALCULATION_TYPES);
			String[] conditionCategories = r.getParameterValues(GetConditions.CONDITION_CATEGORIES);
			String[] conditionControls = r.getParameterValues(GetConditions.CONDITION_CONTROLS);
			String[] conditionOrigins = r.getParameterValues(GetConditions.CONDITION_ORIGINS);
			String[] scaleTypes = r.getParameterValues(GetConditions.SCALE_TYPES);

			String[] changeOfRatesAllowed = r.getParameterValues(GetConditions.CHANGE_OF_RATES_ALLOWED);
			String[] changeOfUnitsAllowed = r.getParameterValues(GetConditions.CHANGE_OF_UNITS_ALLOWED);
			String[] changeOfValuesAllowed = r.getParameterValues(GetConditions.CHANGE_OF_VALUES_ALLOWED);
			String[] deletionAllowed = r.getParameterValues("deletionAllowed"); 

			for (int i = 0; i < stepNos.length; i++){
				String variantConditionDescription; //the socket command packs the descriptions
				//of non-variant conditions and variant conditions into one parameter
				if (variantConditionFlags[i] != null && variantConditionFlags[i].equalsIgnoreCase(ServerConstants.YES)) {
					variantConditionDescription = descriptions[i];
				}else {
					variantConditionDescription = "";
				}
				_conditions.add(new TcpDefaultIPCPricingCondition(
					this,
					stepNos[i],
					counters[i],
					headercounters[i],
					conditionTypeNames[i],
					descriptions[i],
					conditionRates[i],
					conditionCurrencies[i],
					conditionPricingUnitValues[i],
					conditionPricingUnitUnits[i],
					conditionPricingUnitDimensionNames[i],
					conditionValues[i],
					documentCurrencies[i],
					conversionNumerators[i],
					conversionDenominators[i],
					conversionExponents[i],
					conditionBaseValues[i],

					exchangeRatesExtRepresentation[i],
					directExchangeRatesExtRepresentation[i],
					isDirectExchangeRates[i].equalsIgnoreCase(ServerConstants.YES),
					factors[i],
					variantConditionFlags[i].equalsIgnoreCase(ServerConstants.YES),
				    variantCondKeys[i],
					variantFactors[i],
                    variantConditionDescription,
					indicatorStatisticals[i].equalsIgnoreCase(ServerConstants.YES),
					(conditionIsInactives[i] == null)?' ':conditionIsInactives[i].charAt(0),
					(conditionClasses[i] == null)?' ':conditionClasses[i].charAt(0),
					(calculationTypes[i] == null)?' ':calculationTypes[i].charAt(0),
					(conditionCategories[i] == null)?' ':conditionCategories[i].charAt(0),
					(conditionControls[i] == null)?' ':conditionControls[i].charAt(0),
					(conditionOrigins[i] == null)?' ':conditionOrigins[i].charAt(0),
					(scaleTypes[i] == null)?' ':scaleTypes[i].charAt(0),

					changeOfRatesAllowed[i].equalsIgnoreCase(ServerConstants.YES),
					changeOfUnitsAllowed[i].equalsIgnoreCase(ServerConstants.YES),
					changeOfValuesAllowed[i].equalsIgnoreCase(ServerConstants.YES),
					deletionAllowed[i].equalsIgnoreCase(ServerConstants.YES)
					));
			}
			setOutOfSync();
	   }catch(ClientException e){
		throw new IPCException(e);
	   }
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#addPricingCondition(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void addPricingCondition(
		String conditionTypeName,
		String conditionRate,
		String conditionCurrency,
		String conditionPricingUnitValue,
		String conditionPricingUnitUnit,
		String conditionValue,
		String decimalSeparator,
		String groupingSeparator)
		{
			//invalidate condition list - conditions will be retrieved from server
			setOutOfSync();
			String internalConditionRate;
            TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_session).getPricingConverter();
			if (conditionRate != null) {
				internalConditionRate = pc.convertCurrencyValueExternalToInternal(conditionRate, conditionCurrency, decimalSeparator, groupingSeparator);
			}else {
				internalConditionRate = null;
			}
			String internalConditionValue;
			if (conditionValue != null) {
				internalConditionValue = pc.convertCurrencyValueExternalToInternal(conditionValue, conditionCurrency, decimalSeparator, groupingSeparator);
			}else {
				internalConditionValue = null;
			}
			_conditionsToBeAdded.addAll(Arrays.asList(new String[] {
						AddPricingConditions.DOCUMENT_ID, getDocumentId(),
						AddPricingConditions.ITEM_IDS, getItemId(),
	  //					AddPricingConditions.HEADERCONDITION_FLAG, _translateBoolean(isHeader()), //tobe implemented!
						AddPricingConditions.CONDITION_TYPES, conditionTypeName,
						AddPricingConditions.CONDITION_RATES, internalConditionRate,
						AddPricingConditions.CONDITION_CURRENCIES, conditionCurrency,
						AddPricingConditions.PRICING_UNIT_VALUES, conditionPricingUnitValue,
						AddPricingConditions.PRICING_UNIT_UNITS, conditionPricingUnitUnit,
						AddPricingConditions.CONDITION_VALUES, internalConditionValue}));
		}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_executeAdditions()
	 */
	protected StepIdSet _executeAdditions() throws IPCException
	{
		if (_conditionsToBeAdded.size() == 0){
			return null;
		}
		//Response r = null;
		ServerResponse r = null;
		String[] params = new String[_conditionsToBeAdded.size()];
		_conditionsToBeAdded.toArray(params);
		try{		
			//r = _client.command(com.sap.msasrv.usermodule.spc.ISPCCommandSet.ADD_PRICING_CONDITIONS, params);
			r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.ADD_PRICING_CONDITIONS, params);
			_conditionsToBeAdded.clear();
			String[] /*Value*/ messages = r.getParameterValues(AddPricingConditions.MESSAGES);
			String[] /*Value*/ messageTypes = r.getParameterValues(AddPricingConditions.MESSAGE_TYPES);
			String[] /*Value*/ objectIds = r.getParameterValues(AddPricingConditions.OBJECT_IDS);
			_messages.add(new DefaultIPCMessageSet(objectIds, messageTypes, messages));
			return new StepIdSet(   r.getParameterValues(AddPricingConditions.STEP_NUMBERS),
									r.getParameterValues(AddPricingConditions.CONDITION_COUNTERS));
		} catch (ClientException e) {
			throw new IPCException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#removePricingCondition(java.lang.String, java.lang.String)
	 */
	public void removePricingCondition(String stepNo, String counter)
	{
		//invalidate condition list - conditions will be retrieved from server
		setOutOfSync();
		String[] parameter;
		if (getItemId() == null){//header conditions
			parameter = new String[] {
								RemovePricingConditions.DOCUMENT_ID, getDocumentId(),
								RemovePricingConditions.STEP_NUMBERS, stepNo,
								RemovePricingConditions.CONDITION_COUNTERS, counter};
		}else {
			parameter = new String[] {
								RemovePricingConditions.DOCUMENT_ID, getDocumentId(),
								RemovePricingConditions.ITEM_IDS, getItemId(),
//								RemovePricingConditions.HEADERCONDITION_FLAG, _translateBoolean(isHeader()), //tobe implemented!!
								RemovePricingConditions.STEP_NUMBERS, stepNo,
								RemovePricingConditions.CONDITION_COUNTERS, counter};
		}
		_conditionsToBeRemoved.addAll(Arrays.asList(parameter));
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_executeRemovals()
	 */
	public void _executeRemovals() throws IPCException
	{
		if (_conditionsToBeRemoved.size() == 0)
			return;
		//Response r = null;
		ServerResponse r = null;
		String[] params = new String[_conditionsToBeRemoved.size()];
		_conditionsToBeRemoved.toArray(params);
		try{
			//r = _client.command(com.sap.msasrv.usermodule.spc.ISPCCommandSet.REMOVE_PRICING_CONDITIONS, params);
			r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.REMOVE_PRICING_CONDITIONS, params);
			_conditionsToBeRemoved.clear();
			String[] /*Value*/ messages = r.getParameterValues(RemovePricingConditions.MESSAGES);
			String[] /*Value*/ messageTypes = r.getParameterValues(RemovePricingConditions.MESSAGE_TYPES);
			String[] /*Value*/ objectIds = r.getParameterValues(RemovePricingConditions.OBJECT_IDS);
			_messages.add(new DefaultIPCMessageSet(objectIds, messageTypes, messages));
		}catch(ClientException e){
			throw new IPCException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_executeChanges()
	 */
	protected void _executeChanges() throws IPCException
	{
		if (!isOutOfSync())
			return;

		ArrayList params = new ArrayList();
		Iterator iterator = _conditions.iterator();
		while(iterator.hasNext()){
			DefaultIPCPricingCondition prCondition = (DefaultIPCPricingCondition)iterator.next();
			Collection condParams = prCondition.getChangeParameters();
			if (condParams.size() > 0){
				params.add(ChangePricingConditions.ITEM_IDS);
				params.add(getItemId());
				params.add(ChangePricingConditions.STEP_NUMBERS);
				params.add(prCondition.getStepNo());
				params.add(ChangePricingConditions.CONDITION_COUNTERS);
				if (getItemId() == null)
					{
						params.add(prCondition.getHeaderCounter());
					}
				else 
					params.add(prCondition.getCounter());
				params.addAll(condParams);
			}
		}
		if (params.size() == 0)
			return;
		params.add(ChangePricingConditions.DOCUMENT_ID);
		params.add(getDocumentId());
//		params.add(ChangePricingConditions.HEADERCONDITION_FLAG); //tobe implemented!
//		params.add(_translateBoolean(isHeader())); //tobe implemented!
		String[] paramsArray = new String[params.size()];
		params.toArray(paramsArray);
		//Response r = null;
		ServerResponse r = null;
		try {
			//r = _client.command(com.sap.msasrv.usermodule.spc.ISPCCommandSet.CHANGE_PRICING_CONDITIONS, paramsArray);
			r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.CHANGE_PRICING_CONDITIONS, paramsArray);
			String[] /*Value*/ messages = r.getParameterValues(ChangePricingConditions.MESSAGES);
			String[] /*Value*/ messageTypes = r.getParameterValues(ChangePricingConditions.MESSAGE_TYPES);
			String[] /*Value*/ objectIds = r.getParameterValues(ChangePricingConditions.OBJECT_IDS);
			addMessages(new DefaultIPCMessageSet(objectIds, messageTypes, messages));
		}catch(ClientException e){
			throw new IPCException(e);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAvailableConditionTypeNames()
	 */
	public ValueDescriptionPairs getAvailableConditionTypeNames()
		throws IPCException
		{
			if (_availableConditionTypeNames == null){
				try {
					ServerResponse r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_AVAILABLE_CONDITIONTYPES,
							new String[] {
								GetAvailableConditionTypes.DOCUMENT_ID, getDocumentId(),
								GetAvailableConditionTypes.HEADER_CONDITIONS_FLAG,_translateBoolean(_isHeader),
							});
					
					String[] names = r.getParameterValues(GetAvailableConditionTypes.NAME);
					String[] descriptions = r.getParameterValues(GetAvailableConditionTypes.DESCRIPTION);
					_availableConditionTypeNames = new DefaultValueDescriptionPairs(names, descriptions);
				} catch (ClientException e) {
					throw new IPCException(e);
				}
			}
			return _availableConditionTypeNames;
		}
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAllPhysicalUnits()
	 */
	public ValueDescriptionPairs getAllPhysicalUnits() throws IPCException
	{

		if (_allPhysicalUnits == null){
			try{
				ServerResponse r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_ALL_PHYSICALUNITS,
									new String[] {
									});
				String[] names = r.getParameterValues(GetAllPhysicalUnits.NAME);
				String[] descriptions = r.getParameterValues(GetAllPhysicalUnits.DESCRIPTION);
				_allPhysicalUnits = new DefaultValueDescriptionPairs(names, descriptions);
			}catch(ClientException e){
				throw new IPCException(e);
			}
		}
		return _allPhysicalUnits;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAvailablePhysicalUnits(java.lang.String)
	 */
	public ValueDescriptionPairs getAvailablePhysicalUnits(String dimensionName)
		throws IPCException
		{
			ValueDescriptionPairs units = (ValueDescriptionPairs)_availablePhysicalUnits.get(dimensionName);
			if (units == null){
				try{
					ServerResponse r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_ALL_PHYSICALUNITS,
							new String[] {
								GetAllPhysicalUnits.DIMENSIONNAME, dimensionName,
							});

					String[] names = r.getParameterValues(GetAllPhysicalUnits.NAME);
					String[] descriptions = r.getParameterValues(GetAllPhysicalUnits.DESCRIPTION);
					units = new DefaultValueDescriptionPairs(names, descriptions);
					_availablePhysicalUnits.put(dimensionName, units);
				}catch(ClientException e){
					throw new IPCException(e);
				}
				
			}
			return units;
		}
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAvailableQuantityUnits()
	 */
	public ValueDescriptionPairs getAvailableQuantityUnits()
		throws IPCException
		{
			if (_isHeader)
				return getAllPhysicalUnits();
			else{
				if (_availableQuantityUnits == null){
					try{
						ServerResponse r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_ITEM_INFO,
								new String[] {
									GetItemInfo.DOCUMENT_ID, getDocumentId(),
									GetItemInfo.ITEM_ID, getItemId(),
									//!!Sprache implizit aus Dokument
									GetItemInfo.FORMAT_VALUE, ServerConstants.YES
								});
						String[] /*Value*/ names = r.getParameterValues(GetItemInfo.ALT_QUANTITY_UNIT_UNITS);
						String[] /*Value*/ descriptions = r.getParameterValues(GetItemInfo.ALT_QUANTITY_UNIT_DESCRIPTIONS);
						_availableQuantityUnits = new DefaultValueDescriptionPairs(names, descriptions);
					}catch(ClientException e){
						throw new IPCException(e);
					}
				}
				return _availableQuantityUnits;
			}
		}
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAllCurrencyUnits()
	 */
	public ValueDescriptionPairs getAllCurrencyUnits() throws IPCException
	{
		if (_allCurrencyUnits == null){
			try{
				ServerResponse r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_ALL_CURRENCY_UNITS,
						new String[] {
							GetAllCurrencyUnits.CURR_LANGUAGE, getLanguage(),
							GetAllCurrencyUnits.COUNTRY, getCountry()
						});
	
				String[] names = r.getParameterValues(GetAllCurrencyUnits.NAME);
				String[] descriptions = r.getParameterValues(GetAllCurrencyUnits.DESCRIPTION);
				_allCurrencyUnits = new DefaultValueDescriptionPairs(names, descriptions);
	
			}catch(ClientException e){
				throw new IPCException(e);
			}

		}
		return _allCurrencyUnits;
	}
	//********Implementation for Abstract methods ends here!********

}
