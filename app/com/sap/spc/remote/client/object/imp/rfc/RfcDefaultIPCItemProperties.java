package com.sap.spc.remote.client.object.imp.rfc;

import java.util.Map;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;

/**
 * @author I026584
 *
 */
public class RfcDefaultIPCItemProperties
	extends DefaultIPCItemProperties
	implements IPCItemProperties {

		//Constructors
	protected RfcDefaultIPCItemProperties() {
			//initCommonProperties() will make sure of caching this set
			_productGuid 			= null;
			_productId   			= null;
			_productDescription		= null;
			_salesQuantity			= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_netValue				= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_taxValue				= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_grossValue				= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_totalGrossValue		= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_totalNetValue	    	= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_totalTaxValue  		= new com.sap.spc.remote.client.object.imp.DimensionalValue();
            _configChanged          = true; // initially set to true because the config is not known at the client
			_configurable			= null;
            
			_productVariant         = null;
			_subItemIds             = new String[0];

			_date		 			= null;
			_statistical 			= null;
			_netWeight				= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_productType            = null;
			_productLogSys          = null;
			_itemReturn				= null;
			_volume					= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_config                 = factory.newConfigValue();


			_headerAttributeNames  	= new String[0];
			_headerAttributeValues 	= new String[0];
			_itemAttributeNames    	= new String[0];
			_itemAttributeValues   	= new String[0];


			_performPricingAnalysis = false;
            _pricingRelevant        = new Boolean(true);
            _decoupleSubitems       = false;
			_baseQuantity           = new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_grossWeight			= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_externalId				= null;
			_netValueWithoutFreight	= new com.sap.spc.remote.client.object.imp.DimensionalValue();
			_contextKeys            = new String[0];
			_contextValues          = new String[0];
			_exchangeRate           = null;
			_kbLogSys               = null;
			_kbName                 = null;
			_kbVersion              = null;
			_kbProfile              = null;
			_highLevelItemId		= null;
		}


		protected RfcDefaultIPCItemProperties(IPCItemProperties initialData) throws IPCException {
			this();
            if (initialData == null){
                return;
            }
            _itemId      = initialData.getItemId();
			_productGuid = initialData.getProductGuid();
			_productId = initialData.getProductId();
            _productType = initialData.getProductType();
            _productLogSys = initialData.getProductLogSys();    
            _kbLogSys = initialData.getKbLogSys();
            _kbName = initialData.getKbName();
            _kbVersion = initialData.getKbVersion();
            _kbProfile = initialData.getKbProfile();
            _highLevelItemId = initialData.getHighLevelItemId();                    
			_date = initialData.getDate();
			_statistical = initialData.isStatistical() ? "Y" : "N";
            _pricingRelevant = new Boolean(initialData.isPricingRelevant().booleanValue()); 
            _performPricingAnalysis = initialData.getPerformPricingAnalysis();
            _decoupleSubitems = initialData.isDecoupleSubitems();
            _exchangeRateType = initialData.getExchangeRateType();
            _exchangeRateDate = initialData.getExchangeRateDate();

			_salesQuantity.setValue(initialData.getSalesQuantity());
			_baseQuantity.setValue(initialData.getBaseQuantity());
			_netWeight.setValue(initialData.getNetWeight());
			_itemReturn = initialData.isItemReturn() ? "Y" : "N";
			_volume.setValue(initialData.getVolume());

            _config = factory.newConfigValue(initialData.getConfig(null));
           
            Map initialContext = initialData.getContext();
            if (initialContext != null){
                this.setContext(initialContext);
            }
            
            Map initialHeaderAttributes = initialData.getHeaderAttributes();
            if (initialHeaderAttributes != null){
                this.setHeaderAttributes(initialHeaderAttributes);
            }

            Map initialItemAttributes = initialData.getItemAttributes();
            if (initialItemAttributes != null){
                this.setItemAttributes(initialItemAttributes);
            }
            
            String initialSubItemIds[] = initialData.getSubItemIds();
            if (initialSubItemIds != null){
                int length = initialSubItemIds.length;
                _subItemIds = new String[length];
                for (int i=0; i<length; i++){
                    _subItemIds[i] = initialSubItemIds[i];
                }
            }

            Map initialConditionTimestamps = initialData.getConditionTimestamps();
            if (initialConditionTimestamps != null){
                this.setConditionTimestamps(initialConditionTimestamps);            
            }
            
            DefaultIPCItemProperties defaultInitialData = (DefaultIPCItemProperties)initialData;
            _externalId = defaultInitialData.getExternalId();
            _productDescription = defaultInitialData.getProductDescription();
            _configurable = defaultInitialData.isConfigurable() ? "Y" : "N";
            _productVariant = defaultInitialData.isProductVariant() ? "Y" : "N";
            _grossWeight.setValue(defaultInitialData.getGrossWeight());      
            _netValue.setValue(defaultInitialData.getNetValue());
            _netValueWithoutFreight.setValue(defaultInitialData.getNetValueWithoutFreight());
    
            _taxValue.setValue(defaultInitialData.getTaxValue());
            _grossValue.setValue(defaultInitialData.getGrossValue());
            _totalGrossValue.setValue(defaultInitialData.getTotalGrossValue());
            _totalNetValue.setValue(defaultInitialData.getTotalNetValue());
            _totalTaxValue.setValue(defaultInitialData.getTotalTaxValue());
            
		}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties#initializeValues(com.sap.spc.remote.client.object.imp.DefaultIPCSession, java.lang.String, java.lang.String, boolean, boolean)
	 */
	public void initializeValues(
		DefaultIPCSession session,
		String documentId,
		String itemId,
		boolean isValueFormatting,
		boolean isExtendedData) {
		_config.initializeValues(session, documentId, itemId);
	}
}
