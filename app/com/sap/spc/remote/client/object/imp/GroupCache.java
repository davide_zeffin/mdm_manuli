package com.sap.spc.remote.client.object.imp;

/**
 * The group cache holds characteristic groups and enables an optimized
 * synchronization procedure.
 */
public class GroupCache {

    // These constants define the granularity of interest
    public static final int NO_INTEREST = 0;
    public static final int INTEREST_IN_VISIBLE_CSTICS_ONLY = 1;
    public static final int INTEREST_IN_ALL_CSTICS = 2;

    // These constant define, which part of the cache is dirty and therefore needs
    // to be updated
    public static final int NO_REFRESH_REQUIRED = 0;
    public static final int REFRESH_REQUIRED_FOR_INVISIBLE_CSTICS = 1;
    public static final int REFRESH_REQUIRED_FOR_VISIBLE_CSTICS = 2;
    public static final int REFRESH_REQUIRED_FOR_ALL_CSTICS = 3;

    protected DefaultCharacteristicGroup group;
    protected int interest = NO_INTEREST;
    protected int requiredRefresh = REFRESH_REQUIRED_FOR_ALL_CSTICS;

  
    /**
     * @return Characteristic Group of groupCache
     */
    public DefaultCharacteristicGroup getGroup() {
        return group;
    }

    /**
     * @return interest
     */
    public int getInterest() {
        return interest;
    }

    /**
     * @return required refresh flag
     */
    public int getRequiredRefresh() {
        return requiredRefresh;
    }

    /**
     * @param Characteristic Group of groupCache
     */
    public void setGroup(DefaultCharacteristicGroup group) {
        this.group = group;
    }

    /**
     * @param interest
     */
    public void setInterest(int i) {
        interest = i;
    }

    /**
     * @param required refresh state 
     */
    public void setRequiredRefresh(int i) {
        requiredRefresh = i;
    }

}
