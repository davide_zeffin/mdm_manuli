package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemProperties;

/**
 * client-side class:
 * Properties to initialize an item with. Please note that the properties object
 * you pass to an item will be stored inside the item. So you can not reuse
 * a properties object to initialize several items. Additionally, please note
 * that once you have called createItems() with a properties object, the server takes
 * over "setter control", ie. any call to set... of this object will not cause
 * any effect. You may use this object for get... access at any time, though (although
 * it is recommended to access the item via the item's methods but not via property object).
 * 
 */
public abstract class DefaultIPCItemProperties implements IPCItemProperties
{
	// values that can be set in newItem

	//Used by application level caching
	protected boolean cacheIsDirty = true;

    protected IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
        
	/*** Application Cahcing variables starts *****/
	protected String _itemId;
	protected String _productGuid;
	protected String _productId;
    protected String _productIdERP;
	protected String _productType;
	protected String _productLogSys;
	protected String _date;
	protected Boolean _pricingRelevant;

	protected String[] _headerAttributeNames;
	protected String[] _headerAttributeValues;

	protected String[] _itemAttributeNames;
	protected String[] _itemAttributeValues;
	
	protected Map _conditionTimestamps;

	protected String _statistical;
	protected boolean _performPricingAnalysis;
    protected boolean _decoupleSubitems;
	
	protected String        _externalId;
	protected String        _productDescription;
	protected String        _configurable;
    protected boolean       _configChanged;
	protected String        _productVariant;
	protected boolean       _changeableProductVariant;
	
	protected String        _itemReturn;

	protected ConfigValue      _config;
    protected ext_configuration _initialConfig;

	protected String       _kbLogSys;
	protected String       _kbName;
	protected String       _kbVersion;
	protected String       _kbProfile;
	protected String[]        _subItemIds;
	protected String		_highLevelItemId;

	/**
	 * SCE context names. Initially null, will be filled on the first update from the server
	 */
	protected String[]        _contextKeys;

	/**
	 * SCE context values. Initially null, will be filled on the first update from the server
	 */
	protected String[]        _contextValues;

	protected String        _exchangeRate;
	/*** Application Cahcing variables ends *****/
        

	protected com.sap.spc.remote.client.object.imp.DimensionalValue _salesQuantity;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _baseQuantity;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _netWeight;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _grossWeight;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _netValue;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _netValueWithoutFreight;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _taxValue;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _grossValue;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _totalNetValue;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _totalTaxValue;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _totalGrossValue;
    protected com.sap.spc.remote.client.object.imp.DimensionalValue _volume;


	// Values that are not modified by the server
	protected String           _exchangeRateType;
	protected String           _exchangeRateDate;

	/**
	 * Additional parameters for CreateItem(s). m_commandParameterNames/Values are initially null.
	 * After they are defined they always have the same number of elements; the i-th member of both
	 * corresponds.
	 */
	protected ArrayList      m_commandParameterNames;
	protected ArrayList      m_commandParameterValues;
    
    public static String PROPERTIES      = "itemProps";    

	/**
	 * Sets the product guid of this item. If a productId has been set before, it will be reset to null.
	 */
	public void setProductGuid(String guid) {
		_productId = (String)null;
		_productGuid = guid;
	}


	/**
	 * Sets the product id of this item. If a product guid has been set before, it will be reset to null.
	 */
	public void setProductId(String productId) {
		_productGuid = (String)null;
		_productId = productId;
	}


	public String getProductGuid() throws IPCException {
		return _productGuid;
	}


	public String getProductId() throws IPCException {
		return _productId;
	}


	/**
	 * Return Product Type.
	 * 
	 */
	public String getProductType() throws IPCException {
		return _productType;
	}

	/**
	 *  Sets the product type.
	 *
	 */
	public void setProductType(String productType) throws IPCException {
		_productType = productType;
	}

	/**
	 *  Return Product logical system.
	 *
	 */
	public String getProductLogSys() throws IPCException {
		return _productLogSys;
	}
	/**
	 * Return attribute: changeable product variant
	 */
	public boolean isChangeableProductVariant() throws IPCException {
		return _changeableProductVariant;
	}
	
	/**
	 * Set property: Changeable product variant. This attribute cannot be read from
	 * master data but has to be passed by IPC callers.
	 */
	public void setChangeableProductVariant(boolean changeableProductVariant) throws IPCException { 
		this._changeableProductVariant = changeableProductVariant;    	
	}
	/**
	 *  Sets the product logical system.
	 *
	 */
	public void setProductLogSys(String productLogSys) throws IPCException {
		_productLogSys = productLogSys;
	}

	/**
	 * Sets the target date for this item. This API currently uses a very simplified date model. All
	 * IPC dates (kbDate, pricingDate, billingDate, ...) are set to one single date.
	 */
	public void setDate(String date) {
		_date = date;
	}


	public String getDate() throws IPCException {
		// this code checks wheather a long date-format (14 digits) is given
		// and returns it with 8 digits since the configuration uses this format
		String date = _date;
		if (date != null)
		    return date.length() > 8 ? date.substring(0,8) : date;
		else
			return null;
	}


	public void setHeaderAttributes(Map attributes) {
		String[] keys = new String[attributes.size()];
		String[] values = new String[attributes.size()];
		int i=0;
		Iterator iter=attributes.keySet().iterator();
		while (iter.hasNext()) {
			String key = (String)iter.next();
			String value = (String)attributes.get(key);
			keys[i] = key;
			values[i] = value;
			i++;
		}
		_headerAttributeNames = keys;
		_headerAttributeValues = values;
	}


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItemProperties#getHeaderAttributes()
	 */
	public Map getHeaderAttributes() throws IPCException {
		String[] keys = _headerAttributeNames;
		String[] values = _headerAttributeValues;
		HashMap result = new HashMap();
		if (keys != null) {
		    for (int i=0; i<keys.length; i++) {
				result.put(keys[i], values[i]);
		    }
		}
		return result;
	}



	/**
	 * Returns the header attribute value for a given attribute name
	 * @param name the name of the attribute
	 * @return the value associated with the attribute name, or else null
	 * @throws IPCException
	 */
	String getHeaderBindingValue(String name) throws IPCException {
		String[] keys = _headerAttributeNames;
		String[] values = _headerAttributeValues;
		if (keys != null) {
			for (int i=0; i<keys.length; i++) {
				if (keys[i].equals(name))
					return values[i];
			}
		}
		return null;
	}


    /**
     * Gives back the header attributes as an array.
	 * @return
	 * @throws IPCException
	 */
	String[] getHeaderAttributeEnvironment() throws IPCException {
		return _headerAttributeNames;
    }


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItemProperties#setItemAttributes(java.util.Map)
	 */
	public void setItemAttributes(Map attributes) {
		String[] keys = new String[attributes.size()];
		String[] values = new String[attributes.size()];
		int i=0;
		Iterator iter=attributes.keySet().iterator();
		while (iter.hasNext()) {
			String key = (String)iter.next();
			String value = (String)attributes.get(key);
			keys[i] = key;
			values[i] = value;
			i++;
		}
		_itemAttributeNames = keys;
		_itemAttributeValues = values;
	}


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItemProperties#getItemAttributes()
	 */
	public Map getItemAttributes() throws IPCException {
		String[] keys = _itemAttributeNames;
		String[] values = _itemAttributeValues;
		HashMap result = new HashMap();
		if (keys != null) {
		    for (int i=0; i<keys.length; i++) {
				result.put(keys[i], values[i]);
		    }
		}
		return result;
	}


	/**
	 * Returns the item attribute value for a given attribute name
	 * @param name the name of the attribute
	 * @return the value associated with the attribute name, or else null
	 * @throws IPCException
	 */
	String getItemBindingValue(String name) throws IPCException {
		String[] keys = _itemAttributeNames;
		String[] values = _itemAttributeValues;
		if (keys != null) {
			for (int i=0; i<keys.length; i++) {
				if (keys[i].equals(name))
					return values[i];
			}
		}
		return null;
	}


    /**
     * Gives back the item attributes as an array.
	 * @return
	 * @throws IPCException
	 */
	String[] getItemAttributeEnvironment() throws IPCException {
		return _itemAttributeNames;
    }


	/**
	 * Changes this item's being statistical.
	 */
    public void setStatistical(boolean newStatistical) {
		_statistical = newStatistical ? "Y" : "N";
    }


    public boolean isStatistical() throws IPCException {
        return _ynToBoolean(_statistical);
	}


	protected boolean _ynToBoolean(String val) {
		if (val == null)
			return false;
		else
			return (val.equals("Y")) || (val.equals("X"));
	}

	protected String _booleanToYNValue(boolean b) {
		return b ? "Y" : "N";
	}
    
	protected String _booleanToXValue(boolean b) {
        return b ? "X" : "";
    }


	/**
	 * Sets the sales quantity of this item.
	 */
    public void setSalesQuantity(DimensionalValue newSalesQuantity) throws IPCException {
        _salesQuantity.setValue(newSalesQuantity);
    }

    public DimensionalValue getSalesQuantity() {
        return _salesQuantity;
    }


	/**
	 * Sets the base quantity of this item.
	 */
	public void setBaseQuantity(DimensionalValue newBaseQuantity) throws IPCException {
		_baseQuantity.setValue(newBaseQuantity);
    }

    public DimensionalValue getBaseQuantity() {
		return _baseQuantity;
	}


	/**
	 * Sets the net weight. Please note that it is currently not supported by
	 * the IPC command set to set net and gross weight to weights of different units.
	 * If you call setNetWeight and setGrossWeight with different units,
	 * newItem(s) will throw an IPCException.
	 */
    public void setNetWeight(DimensionalValue newNetWeight) throws IPCException {
        _netWeight.setValue(newNetWeight);
    }

	/**
	 * Returns the net weight.
	 */
    public DimensionalValue getNetWeight() {
        return _netWeight;
    }


	/**
	 * Sets the gross weight of this item.
	 */
    void setGrossWeight(DimensionalValue newGrossWeight) throws IPCException {
        _grossWeight.setValue(newGrossWeight);
    }


	/**
	 * Returns the gross weight.
	 */
    public DimensionalValue getGrossWeight() {
        return _grossWeight;
    }


	public void setVolume(DimensionalValue newVolume) throws IPCException {
        _volume.setValue(newVolume);
    }

    public DimensionalValue getVolume() {
        return _volume;
    }


	public String getExternalId() throws IPCException {
		return _externalId;
    }

    public void setExternalId(String extId){
        _externalId = extId;
    }

	public String getProductDescription() throws IPCException {
        return _productDescription;
    }


	public boolean isConfigurable() throws IPCException {
        return _ynToBoolean(_configurable);
    }

    /**
     * Temporary performance-fix: will be replaced later by hasConfigChangedSinceLastCheck.
     * Returns true/false dependent on the status of the config.
     * The status is set during syncWithServer() method of DefaultIPCDocument.
     * It is retrieved from FM GetDocumentInfo (or command GetDocumentAndItemInfo).
     * Initially this returns true (because the config is not known on the client and from the client's 
     * point-of-view it has changed.
     * If the item is not configurable this returns false.
     * @return true if the config has changed
     */
    public boolean hasConfigChanged(){
        if (isConfigurable()){
            return _configChanged;
        }
        else {
            return false;
        }
    }
    
    /**
     * Set whether the config has changed.
     * @param flag whether config has changed
     */
    public void setConfigChanged(boolean changed){
        _configChanged = changed;
    }

    public boolean isProductVariant() throws IPCException {
        return _ynToBoolean(_productVariant);
    }


    public DimensionalValue getNetValue() {
    	return _netValue;
    }

    public DimensionalValue getNetValueWithoutFreight() {
    	return _netValueWithoutFreight;
    }

	public void setNetValue(DimensionalValue v) throws IPCException {
		_netValue.setValue(v);
	}

	public void setNetValueWithoutFreight(DimensionalValue v) throws IPCException {
		_netValueWithoutFreight.setValue(v);
	}

    public DimensionalValue getTaxValue() {
        return _taxValue;
    }

	public void setTaxValue(DimensionalValue v) throws IPCException {
		_taxValue.setValue(v);
	}

    public DimensionalValue getGrossValue() {
        return _grossValue;
    }

	public void setGrossValue(DimensionalValue v) throws IPCException {
		_grossValue.setValue(v);
	}

    public DimensionalValue getTotalGrossValue() {
        return _totalGrossValue;
    }

	public void setTotalGrossValue(DimensionalValue v) throws IPCException {
		_totalGrossValue.setValue(v);
	}

    public DimensionalValue getTotalNetValue() {
        return _totalNetValue;
    }

	public void setTotalNetValue(DimensionalValue v) throws IPCException {
		_totalNetValue.setValue(v);
	}

    public DimensionalValue getTotalTaxValue() {
        return _totalTaxValue;
    }

	public void setTotalTaxValue(DimensionalValue v) throws IPCException {
		_totalTaxValue.setValue(v);
	}


    public void setItemReturn(boolean newItemReturn) {
		_itemReturn = _booleanToYNValue(newItemReturn);
    }

	public boolean isItemReturn() throws IPCException {
        return _ynToBoolean(_itemReturn);
	}


	public void setConfig(ext_configuration config) {
        _config = factory.newConfigValue(config);
	}


	public ext_configuration getConfig(String client) throws IPCException {
		if (!isConfigurable())
			return null;
		else
		    return _config.getConfig(client);
	}

	public ConfigValue getConfigValue() {
		return _config;
	}

	public void setKbLogSys(String kbLogSys){
		_kbLogSys = kbLogSys;;
	}
	public String getKbLogSys() throws IPCException {
		return _kbLogSys;
	}

	public void setKbName(String kbName) {
		_kbName = kbName;
	}
	public String getKbName() throws IPCException {
		return _kbName;
	}

	public void setKbVersion(String kbVersion) {
		_kbVersion = kbVersion;
	}
	public String getKbVersion() throws IPCException {
		return _kbVersion;
	}

	public void setKbProfile(String kbProfile) {
		_kbProfile = kbProfile;
	}
	public String getKbProfile() throws IPCException {
		return _kbProfile;
	}

	public void setContext(Map context) {
		String[] contextNames = new String[context.size()];
		String[] contextValues = new String[context.size()];
		int i = 0;
		Iterator iter=context.keySet().iterator();
		while (iter.hasNext()) {
			String key = (String)iter.next();
			String value = (String)context.get(key);
			contextNames[i] = key;
			contextValues[i] = value;
			i++;
		}
		_contextKeys = contextNames;;
		_contextValues = contextValues;
	}

	public Map getContext() throws IPCException {
		String[] contextNames = _contextKeys;
		String[] contextValues = _contextValues;
		HashMap map = new HashMap();
		if (contextNames != null) {
			for (int i=0; i<contextNames.length; i++) {
				map.put(contextNames[i], contextValues[i]);
			}
		}
		return map;
	}

	/* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItemProperties#getPerformPricingAnalysis()
     */
    public  boolean getPerformPricingAnalysis() throws IPCException {
		return _performPricingAnalysis;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItemProperties#setPerformPricingAnalysis(boolean)
     */
    public  void setPerformPricingAnalysis(boolean flag) {
		_performPricingAnalysis = flag;
    }


	public String[] getSubItemIds() throws IPCException {
		return _subItemIds;
	}


	public  void setExchangeRateType(String exchangeRateType) throws IPCException {
		_exchangeRateType = exchangeRateType;
	}


	public  String getExchangeRateType() {
		return _exchangeRateType;
	}


	public  void setExchangeRateDate(String exchangeRateDate) throws IPCException {
		_exchangeRateDate = exchangeRateDate;
	}


	public  String getExchangeRateDate() {
		return _exchangeRateDate;
	}


	public void setExchangeRate(String exchangeRate) {
		_exchangeRate = exchangeRate;
	}


	public String getExchangeRate() throws IPCException {
		return _exchangeRate;
	}

	public void setHighLevelItemId(String highLevelItemId){
		_highLevelItemId = highLevelItemId;
	}

	/**
	 * Adds a parameter=name setting to these properties that will be passed without any interpretation to
	 * the IPC command that creates the sales document. This method can be used to use sophisticated features
	 * of the pricing engine that are not included in this API because they would make it impossible to
	 * implement it differently.
	 */
	public void addCreationCommandParameter(String name, String value) {
		if (m_commandParameterNames == null) {
			m_commandParameterNames = new ArrayList();
			m_commandParameterValues = new ArrayList();
		}
		m_commandParameterNames.add(name);
		m_commandParameterValues.add(value);
	}


	public List getCreationCommandParameterNames() {
		return m_commandParameterNames;
	}

	public List getCreationCommandParameterValues() {
		return m_commandParameterValues;
	}


	/**
	 * For Client Object Layer internal use 
	 */
	void initCommonProperties(Object cacheKey,
						      String netValue, String taxValue, String grossValue,
							  String totalNetValue, String totalTaxValue, String totalGrossValue,
							  String currencyUnit,
							  String productGuid, String productId, String productDescription,
							  String baseQuantityValue, String baseQuantityUnit,
							  String salesQuantityValue, String salesQuantityUnit,
							  String configurable, String variant, List children, String configChanged) {
		_productGuid = productGuid;
		_productId = productId ;
		_productDescription = productDescription ;
		_baseQuantity.setPredeterminedValue(cacheKey, baseQuantityValue, baseQuantityUnit);
		_salesQuantity.setPredeterminedValue(cacheKey, salesQuantityValue, salesQuantityUnit);
		_netValue.setPredeterminedValue(cacheKey, netValue, currencyUnit);
		_taxValue.setPredeterminedValue(cacheKey, taxValue, currencyUnit);
		_grossValue.setPredeterminedValue(cacheKey, grossValue, currencyUnit);
		_totalNetValue.setPredeterminedValue(cacheKey, totalNetValue, currencyUnit);
		_totalTaxValue.setPredeterminedValue(cacheKey, totalTaxValue, currencyUnit);
		_totalGrossValue.setPredeterminedValue(cacheKey, totalGrossValue, currencyUnit);
		_configurable = configurable;
        boolean configChangedBoolean = _ynToBoolean(configChanged);
        // Temporary performance-fix: will be replaced later by hasConfigChangedSinceLastCheck.
        // set _configChanged only if configChangedBoolean is true (to avoid that the initialization of the config is not 
        // done if the first call returns false for configChanged -> from the server's point-of-view this might be correct
        // because the configuration did not change so far, but for the client's point-of-view this is incorrect because
        // the config has to be initialized first.
        // Later this flag can be changed directly using setConfigChanged(boolean) method (e.g. after initialization of the config)
        if (configChangedBoolean){
            _configChanged = configChangedBoolean; 
        }
		_productVariant = variant;
		if (children != null) {
			String[] childrenIds = new String[children.size()];
			children.toArray(childrenIds);
			_subItemIds = childrenIds;
		}
		else {
			_subItemIds = new String[0];
		}
	}

	/**
	 * @return
	 */
	public Boolean isPricingRelevant() {
		return _pricingRelevant;
	}

	/**
	 * @param boolean1
	 */
	public void setPricingRelevant(Boolean pricingRelevant) {
		_pricingRelevant = pricingRelevant;
	}

   public abstract void initializeValues(
	   DefaultIPCSession session,
	   String documentId,
	   String itemId,
	   boolean isValueFormatting,
	   boolean isExtendedData);
	    
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItemProperties#getHighLevelItemId()
	 */
	public String getHighLevelItemId() throws IPCException 
	{
		return _highLevelItemId/*.getSingleContent()*/;
	}

	/**
	 * For Client Object Layer internal use
	 */
	public void initOtherProperties(String documentId, String itemId, String netValueWoFreight, String currencyUnit){
        _netValueWithoutFreight.setPredeterminedValue(null, netValueWoFreight, currencyUnit);
        
        }        


	/**
	 * @return
	 */
	public Map getConditionTimestamps() {
		return _conditionTimestamps;
	}

	/**
	 * @param map
	 */
	public void setConditionTimestamps(Map map) {
		_conditionTimestamps = map;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItemProperties#isDecoupleSubitems()
     */
    public boolean isDecoupleSubitems() {
        return _decoupleSubitems;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItemProperties#setDecoupleSubitems(boolean)
     */
    public void setDecoupleSubitems(boolean decouple) {
        _decoupleSubitems = decouple;
    }

	/**
	 * @return
	 */
	public String getItemId() {
		return _itemId;
	}

	/**
	 * @param string
	 */
	public void setItemId(String string) {
		_itemId = string;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItemProperties#setInitialConfiguration(com.sap.spc.remote.client.util.ext_configuration)
     */
    public void setInitialConfiguration(ext_configuration config) {
        _initialConfig = config;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItemProperties#getInitialConfiguration()
     */
    public ext_configuration getInitialConfiguration() {
        return _initialConfig;
    }

}
