/*
 * Created on Mar 15, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import java.util.Hashtable;
import java.util.Properties;

import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.client.tcp.TcpClientSupport;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TCPDefaultClient extends TcpClientSupport implements IClient {
	//Replacement of 40 CachingClient
	
	public TCPDefaultClient(Hashtable attributes) throws ClientException {
		super(attributes);		
	}

	public TCPDefaultClient(Hashtable attributes, Properties securityProperties) throws ClientException {
		super(attributes, securityProperties);
	}

	public TCPDefaultClient(String host, int port, String encoding, Boolean viaDispatcher) throws ClientException {
		super(host,port,encoding,viaDispatcher);
	}

	public TCPDefaultClient(String host, int port, String encoding, Boolean viaDispatcher, Properties securityProperties) throws ClientException {
		super(host,port,encoding,viaDispatcher, securityProperties);
	}

	public TCPDefaultClient(String host, int port, String encoding, Boolean viaDispatcher, Hashtable attributes) throws ClientException {
		super(host,port,encoding,viaDispatcher,attributes);
	}

	public TCPDefaultClient(String host, int port, String encoding, Boolean viaDispatcher, Hashtable attributes, Properties securityProperties) throws ClientException {
		super(host,port,encoding,viaDispatcher,attributes, securityProperties); 
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IClient#getItemReference()
	 */
	public IPCItemReference getItemReference() {
		TCPIPCItemReference reference = new TCPIPCItemReference();
		reference.setHost(super.getHost());
		reference.setPort(Integer.toString(super.getPort()));
		reference.setEncoding(super.getEncoding());
		reference.setDispatcher(false);
		return reference;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IClient#getConfigReference()
	 */
	public IPCConfigReference getConfigReference() {
		IPCConfigReference reference = new TCPIPCConfigReference(super.getHost(), Integer.toString(super.getPort()), super.getEncoding(), "unknown", false );
		return reference ;
	}

	/**
	 * Executes this command immediately (unless it is cached), takes care of any waiting
	 * command invocations.
	 */
	public synchronized ServerResponse doCmd(String command, String[] parameters)throws ClientException {
		return _doCmd(null, command, parameters);
	}

	/**
	 * Executes this command immediately (unless it is cached), takes care of any waiting
	 * command invocations. Tries cache lookup by object first, by values passed second.
	 */
	public synchronized ServerResponse doCmd(Object directCacheKey, String command, String[] parameters)throws ClientException{
		return _doCmd(directCacheKey, command, parameters);
	}
	
	protected ServerResponse _doCmd(Object directCacheKey, String command, String[] parameters)throws ClientException {
		try{
			ServerResponse response = cmd(command, parameters);
				if (response == null) {
					throw new ClientException("500", "remote.cachingclient.NoRes",null,null);
				}
				return response;
		}catch(ClientException e){
			throw new IPCException(e);
		}
	}
}
