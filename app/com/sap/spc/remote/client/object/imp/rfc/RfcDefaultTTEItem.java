package com.sap.spc.remote.client.object.imp.rfc;


import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.spc.remote.client.object.TTEItem;
import com.sap.spc.remote.client.object.imp.DefaultTTEItem;


public class RfcDefaultTTEItem extends DefaultTTEItem implements TTEItem {

    public static final RfcDefaultTTEItem C_NULL = new RfcDefaultTTEItem();

    protected RfcDefaultTTEItem(){
        super();
    }

    protected RfcDefaultTTEItem(  String itemId,
                            IPCItemProperties itemProps, 
                            IPCDocumentProperties docProps, 
                            TTEDocument tteDoc, 
                            boolean createItemOnServer) {
        super(itemId, itemProps, docProps, tteDoc, createItemOnServer);
    }

    


}
