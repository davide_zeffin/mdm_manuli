package com.sap.spc.remote.client.object.imp.tcp;

import com.sap.isa.core.cache.Cache;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.KnowledgebaseCacheContainer;
import com.sap.spc.remote.client.object.imp.KnowledgebaseCacheKey;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.shared.command.GetFilteredCsticsAndValues;
import com.sap.spc.remote.shared.command.SCECommand;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class TcpKnowledgebaseCacheLoader extends Cache.Loader {
	protected static final Category category = ResourceAccessor.category;

	private static final Location location =
		ResourceAccessor.getLocation(TcpDefaultInstance.class);
	private String[] csticNames;	
	private String[] csticLNames;	
	private String[] csticDescriptions;	
	private String[] csticValueNames;	
	private String[] csticValueLNames;	
	private String[] csticValueDescriptions;	

	public Object load(Object key, Object attributes) throws Cache.Exception {
		IPCClient ipcClient = ((Instance) attributes).getIpcClient();
		TcpDefaultIPCSession session =
			(TcpDefaultIPCSession) ipcClient.getIPCSession();
		TCPDefaultClient client = (TCPDefaultClient) session.getClient();
		Instance instance = ((Instance) attributes);
		KnowledgebaseCacheKey csticKey = (KnowledgebaseCacheKey) key;
			String[] serverCommand =
				prepareJCO(csticKey, ((Instance) attributes));

			category.logT(
				Severity.PATH,
				location,
				"executing Tcp CFG_API_GET_FILTERED_CSTICS");
				ServerResponse r;
					try {
						r =
							((TCPDefaultClient) client).doCmd(
								ISPCCommandSet.GET_FILTERED_CSTICS_AND_VALUES,
								serverCommand);
						category.logT(
							Severity.PATH,
							location,
							"done with Tcp CFG_API_GET_FILTERED_CSTICS");
						return processJCOResult(r);		
					} catch (ClientException e) {
						category.logT(
							Severity.PATH,
							location,
							"error with Tcp CFG_API_GET_FILTERED_CSTICS");
						throw new Cache.Exception(e.getMessage());
					}
	}

	/**
	   * Prepares import parameter for RFC call to CFG_API_GET_FILTERED_CSTICS
	   * All descriptions of all values will be retrieved
	   * @param im Import Parameterlist
	   */
	private String[] prepareJCO(
		KnowledgebaseCacheKey csticKey,
		Instance inst) {
		Configuration cfg = inst.getConfiguration();


		// optional, TABLE, CFG: Characteristic group insterest
		// (GetFilteredCsticsAndVal
		String [] serverCommand = new String[12];

		serverCommand[0] = SCECommand.CONFIG_ID;
		serverCommand[1] = cfg.getId();
		serverCommand[2] = GetFilteredCsticsAndValues.INST_ID;
		serverCommand[3] = inst.getId();
		serverCommand[4] = GetFilteredCsticsAndValues.CSTIC_INFO_GROUPS;
		serverCommand[5] = "0";

		// Group
		// Name
		StringBuffer unknownCsticsInterest = new StringBuffer();
		unknownCsticsInterest.append(
			GetFilteredCsticsAndValues.INTEREST_CSTIC_DESCRIPTIONS);
		// unknownCsticsInterest for values
		unknownCsticsInterest.append(
			GetFilteredCsticsAndValues.INTEREST_VALUE_DESCRIPTIONS);
		unknownCsticsInterest.append(
			GetFilteredCsticsAndValues.INTEREST_CSTIC_LNAMES);
		// Values are no static knowledgebase data unknownCsticsInterest
		//	.append(GetFilteredCsticsAndValues.INTEREST_VALUE_LNAMES);
		unknownCsticsInterest.append(
			GetFilteredCsticsAndValues.INTEREST_CSTIC_GROUPS);
		unknownCsticsInterest.append(
			GetFilteredCsticsAndValues.INTEREST_CSTIC_GROUP_POSITION);

		serverCommand[6]= GetFilteredCsticsAndValues.CSTIC_INFO_GROUPS;
		serverCommand[7] = "1";
		serverCommand[8] = GetFilteredCsticsAndValues.CSTIC_INFO_GROUP_INTERESTS; 
		serverCommand[9] = unknownCsticsInterest.toString();
		serverCommand[10] = GetFilteredCsticsAndValues.CSTIC_INFO_GROUP_INTERESTS; 
		serverCommand[11] = unknownCsticsInterest.toString();

		return serverCommand;
	}

	/**
	 * Iterates throw the groups table and creates
	 * It should only be one group in the table
	 * @param ex Export parameter of CFG_API_GET_FILTERED_CSTICS
	 * @throws Exception: More than one group in the result table
	 */
	private KnowledgebaseCacheContainer processJCOResult(ServerResponse response)
		throws Cache.Exception { 
			KnowledgebaseCacheContainer container = new KnowledgebaseCacheContainer();
			csticNames = response.getParameterValues(GetFilteredCsticsAndValues.X_CSTIC_NAMES + "0");	
			csticDescriptions = response.getParameterValues(GetFilteredCsticsAndValues.X_CSTIC_DESCRIPTIONS+ "0");	
			csticLNames = response.getParameterValues(GetFilteredCsticsAndValues.X_CSTIC_LNAMES+ "0");

			csticValueNames = response.getParameterValues(GetFilteredCsticsAndValues.X_VALUE_CSTIC_NAMES+ "0");	
			csticValueLNames = response.getParameterValues(GetFilteredCsticsAndValues.X_VALUE_LNAMES+ "0");	
			csticValueDescriptions = response.getParameterValues(GetFilteredCsticsAndValues.X_VALUE_DESCRIPTIONS+ "0");
			String[]valueNames = response.getParameterValues(GetFilteredCsticsAndValues.X_VALUE_NAMES+"0");		
			for( int i = 0; i < csticNames.length; i++)
			{
				container.addCharacteristic(csticNames[i],csticLNames[i],csticDescriptions[i]);
				//Find first entry
				int j=0;
				for(; j < csticValueNames.length && !csticValueNames[j].equals(csticNames[i]);j++);
				//Iterate through rest 
				for(; j < csticValueNames.length && csticValueNames[j].equals(csticNames[i]);j++)
				{
					String vLName = null;
					String vDesc = null;
					if (csticValueLNames != null && j < csticValueLNames.length )
					 	vLName = csticValueLNames[j];
					if( csticValueDescriptions != null &&j < csticValueDescriptions.length )
						vDesc = csticValueDescriptions[j];
					container.setValue(csticNames[i],valueNames[j], vLName, csticValueDescriptions[j]);
				}				  
			}

		return container;
	}
}