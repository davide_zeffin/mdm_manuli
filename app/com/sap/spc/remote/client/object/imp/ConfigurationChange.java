package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Location;

/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

/**
 * The ConfigurationChange encapsulates a single update request for the ipc
 * server. This might be for example a new characteristic value or something
 * similar.
 */

public class ConfigurationChange {

  // Command constants
  public static final int NO_OPERATION                = 0;
  public static final int SET_CHARACTERISTIC_VALUE    = 1;
  public static final int DELETE_CHARACTERISTIC_VALUE = 2;
  
  protected int     m_Command;
  protected Object  m_Object;
  protected static final Location location = ResourceAccessor.getLocation(ConfigurationChange.class);
  
  /**
   * The constructor uses two parameters:
   * command has to be one of the command constants
   * object is the dedicated object (e.g. the CharacteristicValue instance that
   * has to be set).
   */
  public ConfigurationChange(int command,Object object) {
    m_Command = command;
    m_Object  = object;
  }

  /**
   * Returns the command of the desired change
   */
  public int getCommand() {
    location.entering("getCommand()");
    if (location.beDebug()) {
        location.debugT("return m_command " + m_Command);
    }
    location.exiting();
    return m_Command;
  }

  /**
   * Returns the dedicated object
   */
  public Object getObject() {
    location.entering("getObject()");
    if (location.beDebug()) {
        location.debugT("return m_Object " + m_Object.toString());
    }
    location.exiting();
    return m_Object;
  }

}