package com.sap.spc.remote.client.object.imp;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.util.ext_configuration;

public class DefaultConfigurationSnapshot
    extends BusinessObjectBase
    implements ConfigurationSnapshot {

    private long timestamp;
    private String name;
    private String description;
    private ext_configuration extConfig;
    private String price;
    private boolean isInitialConfiguration;
    

    /**
     * @param config Configuration object for which the snapshot should be created
     */
    public DefaultConfigurationSnapshot(Configuration config) {
        super(TechKey.generateKey());
        this.timestamp = System.currentTimeMillis();
        this.extConfig = config.toExternal();
        this.name = "";
        this.description = "";       
    }

    /**
     * @param config Configuration object for which the snapshot should be created
     * @param name name of the snapshot
     * @param price price of the configuration of the snapshot
     * @param isInitial flag whether snapshot is the initial configuration
     */
    public DefaultConfigurationSnapshot(Configuration config, String name, String price, boolean isInitial) {
        super(TechKey.generateKey());
        this.timestamp = System.currentTimeMillis();
        this.extConfig = config.toExternal();
        this.name = name;
        this.description = "";       
        this.price = price;
        this.isInitialConfiguration = isInitial;
    }

    /**
     * @param config Configuration object for which the snapshot should be created
     * @param name name of the snapshot
     * @param description description of the snapshot
     * @param price price of the configuration of the snapshot
     * @param isInitial flag whether snapshot is the initial configuration
     */
    public DefaultConfigurationSnapshot(Configuration config, String name, String description, String price, boolean isInitial) {
        super(TechKey.generateKey());
        this.timestamp = System.currentTimeMillis();
        this.extConfig = config.toExternal();
        this.name = name;
        this.description = description;       
        this.price = price;
        this.isInitialConfiguration = isInitial;
    }

    /**
     * @param extConfig external configuration for which the snapshot should be created
     * @param name name of the snapshot
     * @param description description of the snapshot
     * @param price price of the configuration of the snapshot
     * @param isInitial flag whether snapshot is the initial configuration
     */
    public DefaultConfigurationSnapshot(ext_configuration extConfig, String name, String description, String price, boolean isInitial) {
        super(TechKey.generateKey());
        this.timestamp = System.currentTimeMillis();
        this.extConfig = extConfig;
        this.name = name;
        this.description = description;       
        this.price = price;
        this.isInitialConfiguration = isInitial;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#getConfig()
     */
    public ext_configuration getConfig() {
        return extConfig;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#getPrice()
     */
    public String getPrice() {
        if ((price != null) && (price.equals(""))){
            price = null;
        }
        return price;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#getTimestamp()
     */
    public long getTimestamp() {
        return timestamp;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#isInitialConfiguration()
     */
    public boolean isInitialConfiguration() {
        return isInitialConfiguration;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#setDescription(java.lang.String)
     */
    public void setDescription(String desc) {
        this.description = desc;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#setInitialConfiguration(boolean)
     */
    public void setInitialConfiguration(boolean isInitial) {
        this.isInitialConfiguration = isInitial;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#setName(java.lang.String)
     */
    public void setName(String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConfigurationSnapshot#setPrice(java.lang.String)
     */
    public void setPrice(String price) {
        if (!price.equals("")){
            this.price = price;
        }
    }

}
