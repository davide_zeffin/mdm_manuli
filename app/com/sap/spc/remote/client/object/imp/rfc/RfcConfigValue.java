package com.sap.spc.remote.client.object.imp.rfc;

import java.util.Dictionary;
import java.util.Vector;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.imp.ConfigValue;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.TableValue;
import com.sap.spc.remote.client.rfc.ExtConfigConstants;
import com.sap.spc.remote.client.rfc.ExternalConfigConverter;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.rfc.function.SpcGetConfigItemInfo;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class RfcConfigValue extends ConfigValue implements ExtConfigConstants {

	// logging
	private static final Category category =
				ResourceAccessor.category;
	private static final Location location =
		ResourceAccessor.getLocation(RfcConfigValue.class);

	protected RfcConfigValue() {
    	//IMP: If you add/delete any element from this arry or re-organize the elements, then you have to update initProperties() also.
        _cfg = new TableValue(new String[] { EXT_CONFIG_SCE_MODE,
            EXT_CONFIG_KB_NAME, EXT_CONFIG_KB_VERSION, EXT_CONFIG_KB_BUILD, EXT_CONFIG_KB_PROFILE,
            EXT_CONFIG_KB_LANGUAGE, EXT_CONFIG_ROOT_ID, EXT_CONFIG_CONSISTENT, EXT_CONFIG_COMPLETE,
            EXT_CONFIG_INFO
            });
        _ins = new TableValue(new String[] { EXT_INST_AUTHOR, EXT_INST_CLASS_TYPE, EXT_INST_COMPLETE,
            EXT_INST_CONSISTENT, EXT_INST_ID, EXT_INST_OBJECT_KEY, EXT_INST_OBJECT_TEXT,
            EXT_INST_OBJECT_TYPE, EXT_INST_QUANTITY, EXT_INST_QUANTITY_UNIT
            });
        _prt = new TableValue(new String[] { EXT_PART_AUTHOR, EXT_PART_CLASS_TYPE, 
            EXT_PART_INST_ID, EXT_PART_OBJECT_KEY, EXT_PART_OBJECT_TYPE, EXT_PART_PARENT_ID,
            EXT_PART_POS_NR, EXT_PART_SALES_RELEVANT
            });
        _val = new TableValue(new String[] { EXT_VALUE_AUTHOR, EXT_VALUE_CSTIC_LNAME,
            EXT_VALUE_CSTIC_NAME, EXT_VALUE_INSTANCE_ID, EXT_VALUE_LNAME, EXT_VALUE_NAME
            });
        _prc = new TableValue(new String[] { EXT_PRICE_FACTOR, EXT_PRICE_INSTANCE_ID,
            EXT_PRICE_KEY
            });

        _client = null;
        _config = null;
    }

    /**
     * @param external configuration
     */
    protected RfcConfigValue(ext_configuration config) {
       this();
        setInitialValue(config);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigValue#initializeValues(com.sap.spc.remote.client.object.imp.DefaultIPCSession, java.lang.String, java.lang.String)
     */
    public void initializeValues(
        DefaultIPCSession session,
        String documentId,
        String itemId) {
        	//No need of documentId and itemId in 50 for this command.
			_client = session.getClient();
			_config = null; // free for gc
			
    }

	public void initProperties(String documentId, String itemId) {    
    	int valuesCounter;
    	int partOfCounter;
    	int variantConditionsCounter;
    	int instancesCounter;
    	int extIdsCounter;
        try {
            JCO.Function functionSpcGetConfigItemInfo = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_CONFIG_ITEM_INFO);
            JCO.ParameterList importsSpcGetConfigItemInfo = functionSpcGetConfigItemInfo.getImportParameterList();
            JCO.ParameterList exportsSpcGetConfigItemInfo = functionSpcGetConfigItemInfo.getExportParameterList();
            JCO.ParameterList tablesSpcGetConfigItemInfo = functionSpcGetConfigItemInfo.getTableParameterList();

    		importsSpcGetConfigItemInfo.setValue(documentId, SpcGetConfigItemInfo.DOCUMENT_ID);		// optional, BYTE, GUID-Schlüssel des Preisfindungsbeleges (KNUMV)
	       	importsSpcGetConfigItemInfo.setValue(itemId, SpcGetConfigItemInfo.ITEM_ID);		// optional, BYTE, Positionsnummer

    		category.logT(Severity.PATH, location, "executing RFC SPC_GET_CONFIG_ITEM_INFO");
	       	((RFCDefaultClient)_client).execute(functionSpcGetConfigItemInfo);
    		category.logT(Severity.PATH, location, "done with RFC SPC_GET_CONFIG_ITEM_INFO");

    		JCO.Structure extConfigGuidStru = exportsSpcGetConfigItemInfo.getStructure(SpcGetConfigItemInfo.EXT_CONFIG_GUID);
	       	JCO.Structure extConfigStru = extConfigGuidStru.getStructure(SpcGetConfigItemInfo.EXT_CONFIG);
            
        	Vector data = new Vector();
        	int zi = 0;
            // initialize the arrays for header-data
            String[] sce = new String[1];     
            String[] name = new String[1];
            String[] version = new String[1];
            String[] build = new String[1];
            String[] profile = new String[1];
            String[] language = new String[1];
            String[] info = new String[1];
            String[] rootId = new String[1];
            String[] complete = new String[1];
            String[] consistent = new String[1];
            // getting the header-data
    		JCO.Structure extConfigStruSpcgetconfigiteminfo_header = extConfigStru.getStructure(SpcGetConfigItemInfo.HEADER);
    		sce[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.SCE);
    		name[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.NAME);
    		version[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.VERSION);
    		build[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.BUILD);
    		profile[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.PROFILE);
    		language[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.LANGUAGE);
    		rootId[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.ROOT_ID);
    		consistent[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.CONSISTENT);
    		complete[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.COMPLETE);
    		info[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.INFO);
            //Filling the data object in this order is very importent. 
            data.add(sce);
            data.add(name);
            data.add(version);
            data.add(build);
            data.add(profile);
            data.add(language);
            data.add(rootId);
            data.add(consistent);
            data.add(complete);
            data.add(info);
            _cfg.setData(data);
		 
            data = new Vector(); //Re-set to use for other
            JCO.Table extConfigStruSpcgetconfigiteminfo_partOf = extConfigStru.getTable(SpcGetConfigItemInfo.PART_OF);
            int numRowPrt = extConfigStruSpcgetconfigiteminfo_partOf.getNumRows();
            // initialize arrays for part-of data
            String[] author = new String[numRowPrt];
            String[] classType = new String[numRowPrt];
            String[] instId = new String[numRowPrt];
            String[] objType = new String[numRowPrt];
            String[] partOfNo = new String[numRowPrt];
            String[] parentId = new String[numRowPrt];
            String[] salesRelevant = new String[numRowPrt];
            String[] objKey = new String[numRowPrt];
            // getting the part-of data        
    		for (int i=0; i<numRowPrt; i++) {
                extConfigStruSpcgetconfigiteminfo_partOf.setRow(i);
    			author[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.AUTHOR);
    			classType[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.CLASS_TYPE);
    			instId[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.INST_ID);
    			objKey[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.OBJ_KEY);
    			objType[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.OBJ_TYPE);
    			parentId[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.PARENT_ID);
    			partOfNo[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.PART_OF_NO);
    			salesRelevant[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.SALES_RELEVANT);
    		}
            //Filling the data object in this order is very importent.
            data.add(author);
            data.add(classType);
            data.add(instId);
            data.add(objKey);
            data.add(objType);
            data.add(parentId);
            data.add(partOfNo);
            data.add(salesRelevant);
            _prt.setData(data);

    		data = new Vector(); //Re-set to use for other
            JCO.Table extConfigStruSpcgetconfigiteminfo_instances = extConfigStru.getTable(SpcGetConfigItemInfo.INSTANCES);
            int numRowInst = extConfigStruSpcgetconfigiteminfo_instances.getNumRows();
            // initialize arrays for instance data
            String[] quantityUnit = new String[numRowInst];
            String[] quantity = new String[numRowInst];
            String[] objTxt = new String[numRowInst];  
            String[] insauthor =  new String[numRowInst];
            String[] insclassType =  new String[numRowInst];
            String[] inscomplete = new String[numRowInst];
            String[] insconsistent = new String[numRowInst];
            String[] insobjType = new String[numRowInst];
            String[] insobjKey = new String[numRowInst];  
            String[] insinstId = new String[numRowInst];
            // getting the instance data
    		for (int i=0; i<numRowInst; i++) {
                extConfigStruSpcgetconfigiteminfo_instances.setRow(i);
                insauthor[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.AUTHOR);
                insclassType[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.CLASS_TYPE);
    			inscomplete[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.COMPLETE);
    			insconsistent[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.CONSISTENT);
    			insinstId[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.INST_ID);
    			insobjType[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.OBJ_TYPE);
    			insobjKey[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.OBJ_KEY);
    			objTxt[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.OBJ_TXT);
    			quantity[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.QUANTITY);
    			quantityUnit[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.QUANTITY_UNIT);
    		}
            //Filling the data object in this order is very importent.
            data.add(insauthor);
            data.add(insclassType);
            data.add(inscomplete);
            data.add(insconsistent);
            data.add(insinstId);
            data.add(insobjKey);
            data.add(objTxt);
            data.add(insobjType);
            data.add(quantity);
            data.add(quantityUnit);
    		_ins.setData(data);
		  
    		data = new Vector(); //Re-set to use for other
    		JCO.Table extConfigStruSpcgetconfigiteminfo_values = extConfigStru.getTable(SpcGetConfigItemInfo.VALUES);
            int numRowVal = extConfigStruSpcgetconfigiteminfo_values.getNumRows();
            // initialize arrays for value data
            String[] valueTxt = new String[numRowVal];
            String[] value = new String[numRowVal];
            String[] valauthor =  new String[numRowVal];
            String[] charcTxt = new String[numRowVal];
            String[] charc = new String[numRowVal];  
            String[] valinstId = new String[numRowVal];      
            // getting the value data        
            for (int i=0; i<numRowVal; i++) {
    			extConfigStruSpcgetconfigiteminfo_values.setRow(i);
                valauthor[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.AUTHOR);
                charcTxt[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.CHARC_TXT);
                charc[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.CHARC);
                valinstId[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.INST_ID);
                valueTxt[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.VALUE_TXT);
                value[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.VALUE);
    		}
            //Filling the data object in this order is very importent.
            data.add(valauthor);
            data.add(charcTxt);
            data.add(charc);
            data.add(valinstId);
            data.add(valueTxt);
            data.add(value);
    		_val.setData(data);
		  
    		data = new Vector(); //Re-set to use for other
            JCO.Table extConfigStruSpcgetconfigiteminfo_variantConditions = extConfigStru.getTable(SpcGetConfigItemInfo.VARIANT_CONDITIONS);
            int numRowPrc = extConfigStruSpcgetconfigiteminfo_variantConditions.getNumRows();
            // initialize arrays for varCond data
            String[] vkey = new String[numRowPrc];
            String[] factor = new String[numRowPrc];
            String[] prcinstId = new String[numRowPrc];
            // getting the varCond data       
            for (int i=0; i<extConfigStruSpcgetconfigiteminfo_variantConditions.getNumRows(); i++) {
                extConfigStruSpcgetconfigiteminfo_variantConditions.setRow(i);
                factor[i] = extConfigStruSpcgetconfigiteminfo_variantConditions.getString(SpcGetConfigItemInfo.FACTOR);
                prcinstId[i] = extConfigStruSpcgetconfigiteminfo_variantConditions.getString(SpcGetConfigItemInfo.INST_ID);
                vkey[i] = extConfigStruSpcgetconfigiteminfo_variantConditions.getString(SpcGetConfigItemInfo.VKEY);
            }
            //Filling the data object in this order is very importent.
            data.add(factor);
            data.add(prcinstId);
            data.add(vkey);
            _prc.setData(data);
		  
            // mapping of instance ids to corresponding item ids not used
            //JCO.Table extConfigGuidStruSpcgetconfigiteminfo_extIds = extConfigGuidStru.getTable(SpcGetConfigItemInfo.EXT_IDS);
    		//for (int i=0; i<extConfigGuidStruSpcgetconfigiteminfo_extIds.getNumRows(); i++) {
            //    extConfigGuidStruSpcgetconfigiteminfo_extIds.setRow(i);
            //    instId = extConfigGuidStruSpcgetconfigiteminfo_extIds.getString(SpcGetConfigItemInfo.INST_ID);
            //    guid = extConfigGuidStruSpcgetconfigiteminfo_extIds.getString(SpcGetConfigItemInfo.GUID);
            //}
        }
        catch(ClientException e) {
            throw new IPCException(e);
        }		
	}
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigValue#getConfig(java.lang.String)
     */
    public ext_configuration getConfig(String client) throws IPCException {
        if (_client != null) {
            Integer rootId;
            try {
                rootId = new Integer(_cfg.getElement(EXT_CONFIG_ROOT_ID,0));
            }
            catch(Exception e) {
                // here, an exception may happen if the product isn't configurable at all. In this
                // case the GetItemConfig command that is responsible for reading these values has
                // been optimized away (because the command is not relevant for this item), and all
                // its values lead to ugly OpenValues
                return null;
            }
            int kbBuild = Integer.parseInt(_cfg.getElement(EXT_CONFIG_KB_BUILD,0));

            c_ext_cfg_imp writable = new c_ext_cfg_imp( "",
                                                        "",
                                                        client,
                                                        _cfg.getElement(EXT_CONFIG_KB_NAME,0),
                                                        _cfg.getElement(EXT_CONFIG_KB_VERSION,0),
                                                        kbBuild,
                                                        _cfg.getElement(EXT_CONFIG_KB_PROFILE,0),
                                                        _cfg.getElement(EXT_CONFIG_KB_LANGUAGE,0),
                                                        rootId,
                                                        _cfg.getElement(EXT_CONFIG_CONSISTENT,0).equals("T"),
                                                        _cfg.getElement(EXT_CONFIG_COMPLETE,0).equals("T")
                                                        );

            String configInfo = _cfg.getElement(EXT_CONFIG_INFO,0);
            if (configInfo != null)
                writable.set_cfg_info(configInfo);

            for (int i=0; i<_ins.rows(); i++) {
                Dictionary d = _ins.getRowDictionary(i);
                Integer instId = new Integer( ((String)d.get(EXT_INST_ID)).trim() );
                String author = (String)d.get(EXT_INST_AUTHOR);
                if (author == null || author.equals(""))
                    author = " ";

                writable.add_inst(instId,
                                  (String)d.get(EXT_INST_OBJECT_TYPE),
                                  (String)d.get(EXT_INST_CLASS_TYPE),
                                  (String)d.get(EXT_INST_OBJECT_KEY),
                                  (String)d.get(EXT_INST_OBJECT_TEXT),
                                  author,
                                  (String)d.get(EXT_INST_QUANTITY),
                                  (String)d.get(EXT_INST_QUANTITY_UNIT),
                                  ((String)d.get(EXT_INST_CONSISTENT)).equals("T"),
                                  ((String)d.get(EXT_INST_COMPLETE)).equals("T"));
            }

            for (int i=0; i<_prt.rows(); i++) {
                Dictionary d = _prt.getRowDictionary(i);

                Integer instId = new Integer( ((String)d.get(EXT_PART_INST_ID)).trim() );
                Integer parentId = new Integer( ((String)d.get(EXT_PART_PARENT_ID)).trim() );

                String author = (String)d.get(EXT_PART_AUTHOR);
                if (author == null || author.equals(""))
                    author = " ";

                writable.add_part(parentId, instId,
                                  (String)d.get(EXT_PART_POS_NR),
                                  (String)d.get(EXT_PART_OBJECT_TYPE),
                                  (String)d.get(EXT_PART_CLASS_TYPE),
                                  (String)d.get(EXT_PART_OBJECT_KEY),
                                  author,
                                  d.get(EXT_PART_SALES_RELEVANT).equals(RfcConstants.SALES_RELEVANT)); // 20020214-kha: IPC returns always "X" here
            }

            for (int i=0; i<_val.rows(); i++) {
                Dictionary d = _val.getRowDictionary(i);

                Integer instId = new Integer( ((String)d.get(EXT_VALUE_INSTANCE_ID)).trim() );
                String author = (String)d.get(EXT_VALUE_AUTHOR);
                if (author == null || author.equals("")) {
                    author = " ";
                }

                writable.add_cstic_value(instId,
                                         (String)d.get(EXT_VALUE_CSTIC_NAME),
                                         (String)d.get(EXT_VALUE_CSTIC_LNAME),
                                         (String)d.get(EXT_VALUE_NAME),
                                         (String)d.get(EXT_VALUE_LNAME),
                                         author);
            }

            for (int i=0; i<_prc.rows(); i++) {
                Dictionary d = _prc.getRowDictionary(i);
                Integer instId = new Integer( ((String)d.get(EXT_PRICE_INSTANCE_ID)).trim() );
                double  factor = Double.valueOf( ((String)d.get(EXT_PRICE_FACTOR)).trim() ).doubleValue();

                writable.add_price_key(instId,
                                       (String)d.get(EXT_PRICE_KEY),
                                       factor);
            }

            return writable;
        }
        else
            return _config;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigValue#getConfigIPCParameters(com.sap.spc.remote.client.util.ext_configuration)
     */
    public String[] getConfigIPCParameters(ext_configuration config) {
        if (config == null)
            return null;
        else {
            Vector configs = new Vector(1);
            configs.addElement(config);
            Vector resultVec = new Vector(100);
            ExternalConfigConverter.getConfigs(configs,null,0,1,new _VectorParameterSet(resultVec), false);
            String[] result = new String[resultVec.size()];
            resultVec.copyInto(result);
            return result;
        }
    }

    /**
     * Fills a JCO.Structure with the data of the own external configuration object.
     * @param extConfigStructure JCO.Structure to be filled (EXT_CONFIG)
     */
    public void getConfigIPCParameters(JCO.Structure extConfigStructure) {
        getConfigIPCParameters(extConfigStructure, _config);
    }
    
    /**
     * Fills a JCO.Structure with the data of a given external configuration object.
     * @param extCfg external configuration object
     * @param extConfigStructure JCO.Structure to be filled (EXT_CONFIG)
     */
    public void getConfigIPCParameters(JCO.Structure extConfigStructure, ext_configuration extCfg) {
        if (extCfg != null) {
            ExternalConfigConverter.getConfig(extCfg, extConfigStructure);
        }
        // else do nothing
    }


}
