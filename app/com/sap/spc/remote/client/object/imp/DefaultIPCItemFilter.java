package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemFilter;

/**
 * Title:
 * Description:  Default implementation for the item filter. Returns all the
 *      items passed by input.
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

public class DefaultIPCItemFilter implements IPCItemFilter {

    DefaultIPCItemFilter() {
    }

    public IPCItem[] filterItems(IPCItem[] input) {
		return input;
    }
}