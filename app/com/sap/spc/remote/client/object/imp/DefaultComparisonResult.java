package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.InstanceDelta;
import com.sap.spc.remote.client.util.ext_configuration;

public class DefaultComparisonResult extends BusinessObjectBase implements ComparisonResult {

    private ext_configuration configOne;
    private ext_configuration configTwo;
    private List headerDeltas;
    private List instanceDeltas;
    private HashMap instanceDeltasById2;
    private HashMap valueDeltasByConfigObjectKey;
    private List deletedInstances; 
    private boolean different;
    private boolean hasErrors;
    private List comparisonErrors;

    /**
     * @param headerDeltas list of header deltas
     * @param instanceDeltas list of instance delta objects
     */
    public DefaultComparisonResult( List headerDeltas,
                                    List instanceDeltas) {
        super();
        this.headerDeltas = headerDeltas;
        this.instanceDeltas = instanceDeltas;
        if ((headerDeltas != null && headerDeltas.size()>0) || (instanceDeltas != null && instanceDeltas.size()>0)){
            // configurations are different if header deltas or instance deltas exist
            this.different = true;    
        }
        initSpecialLists(instanceDeltas);
    }

    /**
     * @param configOne configuration one that has been compared to external configuration two
     * @param configTwo configuration two that has been compared to external configuration one
     * @param headerDeltas list of header deltas
     * @param instanceDeltas list of instance delta objects
     */
    public DefaultComparisonResult( ext_configuration configOne, 
                                    ext_configuration configTwo,
                                    List headerDeltas,
                                    List instanceDeltas) {
        super();
        this.configOne = configOne;
        this.configTwo = configTwo;
        this.headerDeltas = headerDeltas;
        this.instanceDeltas = instanceDeltas;
        initSpecialLists(instanceDeltas);
        if ((headerDeltas != null && headerDeltas.size()>0) || (instanceDeltas != null && instanceDeltas.size()>0)){
            // configurations are different if header deltas or instance deltas exist
            this.different = true;    
        }
    }

    /**
     * @param configOne configuration one that has been compared to external configuration two
     * @param configTwo configuration two that has been compared to external configuration one
     * @param headerDeltas list of header deltas
     * @param instanceDeltas list of instance delta objects
     * @param valueDeltasByConfigObjectKey a <code>HashMap</code> with the value deltas sorted by config object key
     * @param deletedInstances list of deleted instances 
     * @param different flag indicating whether configuration 1 and 2 are different
     * @param comparisonErrors list of errors that occured during comparison
     */
    public DefaultComparisonResult( ext_configuration configOne, 
                                    ext_configuration configTwo,
                                    List headerDeltas,
                                    List instanceDeltas,
                                    HashMap valueDeltasByConfigObjectKey,
                                    List deletedInstances,
                                    boolean different,
                                    List comparisonErrors) {
        super();
        this.configOne = configOne;
        this.configTwo = configTwo;
        this.headerDeltas = headerDeltas;
        this.instanceDeltas = instanceDeltas;
        this.valueDeltasByConfigObjectKey = valueDeltasByConfigObjectKey;
        this.deletedInstances = deletedInstances; 
        this.different = different;
        this.comparisonErrors = comparisonErrors;        
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#getComparisonErrors()
     */
    public List getComparisonErrors() {
        return comparisonErrors;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#getDeletedInstances()
     */
    public List getDeletedInstances() {
        return deletedInstances;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#isDifferent()
     */
    public boolean isDifferent() {
        return different;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#hasErrorsDuringComparison()
     */
    public boolean hasErrorsDuringComparison() {
        if (comparisonErrors != null && comparisonErrors.size()>0){
            return true;
        }
        else {
            return false;
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#getHeaderDeltas()
     */
    public List getHeaderDeltas() {
        return headerDeltas;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#getInstanceDeltas()
     */
    public List getInstanceDeltas() {
        return instanceDeltas;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#getValueDeltasByConfigObjectKey()
     */
    public HashMap getValueDeltasByConfigObjectKey() {
        return valueDeltasByConfigObjectKey;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#getConfigOne()
     */
    public ext_configuration getConfigOne() {
        return configOne;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#getConfigTwo()
     */
    public ext_configuration getConfigTwo() {
        return configTwo;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#setComparisonErrors(java.util.List)
     */
    public void setComparisonErrors(List errors) {
        this.comparisonErrors = errors;
    }

    /**
     * Initializes specialized lists:
     * <ul>
     * <li>deletedInstances: list with deleted instances (The instance delta type "D" marks deltas that 
     * represent deleted instances. These deltas are collected in this list)
     * <li>valueDeltasByConfigObjectKey: <code>HashMap</code> with an <code>ArrayList</code> for each 
     * configuration object that has value deltas.
     * <li><code>HashMap</code> with the instance deltas sorted by their id. This <code>HashMap</code> does not contain
     * deleted instances. They can be found in the list <code>deletedInstances</code>
     * </ul>
     * @param instanceDeltas list of instance deltas
     */
    private void initSpecialLists(List instanceDeltas) {
        deletedInstances = new ArrayList();
        instanceDeltasById2 = new HashMap();
        valueDeltasByConfigObjectKey = new HashMap();
        if (instanceDeltas != null){
            for (int i=0; i<instanceDeltas.size(); i++){
                InstanceDelta instDelta = (InstanceDelta) instanceDeltas.get(i);
                if (instDelta.getType().equals(InstanceDelta.DELETED)){
                    // it's a delete delta -> add to deleted-list
                    deletedInstances.add(instDelta);
                }
                else if(instDelta.getType().equals(InstanceDelta.UPDATED)){
                    // it's an update delta -> add instance's value delta map to the value delta map of result
                    HashMap vdMapOfInstance = instDelta.getValueDeltasByConfigObjectKey();
                    valueDeltasByConfigObjectKey.putAll(vdMapOfInstance);
                    // add the instance delta to the HashMap of instanceDeltas
                    instanceDeltasById2.put(instDelta.getInstId2(), instDelta);
                }
                else {
                    // it's an insert delta -> no value deltas will be available by definition
                    // add the instance delta to the HashMap of instanceDeltas
                    instanceDeltasById2.put(instDelta.getInstId2(), instDelta);                     
                }
            }
        }
    }    



    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ComparisonResult#getInstanceDeltasById2()
     */
    public HashMap getInstanceDeltasById2() {
        return instanceDeltasById2;
    }

    /**
     * Set manually configuration one.
     * @param ext_configuration
     */
    public void setConfigOne(ext_configuration ext_configuration) {
        configOne = ext_configuration;
    }

    /**
     * Set manually configuration two.
     * @param ext_configuration
     */
    public void setConfigTwo(ext_configuration ext_configuration) {
        configTwo = ext_configuration;
    }

}
