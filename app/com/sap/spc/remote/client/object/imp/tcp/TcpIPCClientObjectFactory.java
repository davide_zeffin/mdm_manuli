/*
 * Created on Dec 28, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.ProductVariantFeature;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.spc.remote.client.object.imp.ConfigValue;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristic;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristicValue;
import com.sap.spc.remote.client.object.imp.DefaultConfiguration;
import com.sap.spc.remote.client.object.imp.DefaultConflictParticipant;
import com.sap.spc.remote.client.object.imp.DefaultExplanationTool;
import com.sap.spc.remote.client.object.imp.DefaultIPCClient;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocumentProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultInstance;
import com.sap.spc.remote.client.object.imp.DefaultKBProfile;
import com.sap.spc.remote.client.object.imp.DefaultLoadingStatus;
import com.sap.spc.remote.client.object.imp.DefaultPricingConverter;
import com.sap.spc.remote.client.object.imp.DefaultProductVariant;
import com.sap.spc.remote.client.object.imp.DefaultTTEDocument;
import com.sap.spc.remote.client.object.imp.DefaultTTEItem;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCClient;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TcpIPCClientObjectFactory extends IPCClientObjectFactory {
	
	public static IPCClientObjectFactory getInstance() {
			return new TcpIPCClientObjectFactory();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCDocument(com.sap.spc.remote.client.object.imp.DefaultIPCSession, com.sap.spc.remote.client.object.imp.DefaultIPCDocumentProperties)
	 */
	public DefaultIPCDocument newIPCDocument(
		DefaultIPCSession session,
		DefaultIPCDocumentProperties attr)
		throws IPCException {
			return new TcpDefaultIPCDocument(session, attr);
		}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCDocument(com.sap.spc.remote.client.object.imp.DefaultIPCSession, java.lang.String)
	 */
	public DefaultIPCDocument newIPCDocument(
		DefaultIPCSession session,
		String documentId)
		throws IPCException {
			return new TcpDefaultIPCDocument(session, documentId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItem(com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.imp.DefaultIPCDocument, java.lang.String)
	 */
	public DefaultIPCItem newIPCItem(
		IClient client,
		DefaultIPCDocument document,
		String itemId)
		throws IPCException {
			return new TcpDefaultIPCItem((TCPDefaultClient)client,document, itemId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItem(com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.imp.DefaultIPCDocument, java.lang.String, java.lang.String)
	 */
	public DefaultIPCItem newIPCItem(
		IClient client,
		DefaultIPCDocument document,
		String itemId,
		String parentItemId)
		throws IPCException {
			return new TcpDefaultIPCItem((TCPDefaultClient)client,document, itemId, parentItemId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItem(com.sap.spc.remote.client.object.imp.DefaultIPCDocument, com.sap.spc.remote.client.object.IPCItemProperties)
	 */
	public IPCItem newIPCItem(
		DefaultIPCDocument document,
		IPCItemProperties properties)
		throws IPCException {
			return new TcpDefaultIPCItem(document,properties);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItemProperties()
	 */
	public DefaultIPCItemProperties newIPCItemProperties() {
		return new TcpDefaultIPCItemProperties();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItemProperties(com.sap.spc.remote.client.object.IPCItemProperties)
	 */
	public DefaultIPCItemProperties newIPCItemProperties(IPCItemProperties initialData)
		throws IPCException {
			return new TcpDefaultIPCItemProperties(initialData);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItems(com.sap.spc.remote.client.object.imp.DefaultIPCDocument, com.sap.spc.remote.client.object.IPCItemProperties[])
	 */
	public IPCItem[] newIPCItems(
		IPCDocument document,
		IPCItemProperties[] properties)
		throws IPCException {
			return TcpDefaultIPCItem.createItems(document, properties);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, com.sap.spc.remote.client.object.IPCConfigReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		IPCConfigReference reference)
		throws IPCException {
			return new TcpDefaultIPCSession(ipcClient,(IPCConfigReference) reference);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, com.sap.spc.remote.client.object.IPCConfigReference, boolean)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		IPCConfigReference reference,
		boolean isExtendedData)
		throws IPCException {
			return new TcpDefaultIPCSession(ipcClient,(IPCConfigReference) reference, new Boolean(isExtendedData));
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, com.sap.spc.remote.client.object.IPCItemReference, boolean)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		IPCItemReference reference,
		boolean isExtendedData)
		throws IPCException {
			return new TcpDefaultIPCSession(ipcClient,(TCPIPCItemReference) reference, new Boolean(isExtendedData));
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, com.sap.spc.remote.client.object.IPCItemReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		IPCItemReference reference)
		throws IPCException {
			return new TcpDefaultIPCSession(ipcClient,(TCPIPCItemReference) reference);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type)
		throws IPCException {
			        return new TcpDefaultIPCSession(
			            ipcClient,
			            mandt,
			            type,
			            null,
			            (TCPIPCItemReference) null);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.IPCConfigReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IClient client,
		IPCConfigReference ref)
		throws IPCException {
			return new TcpDefaultIPCSession((TcpDefaultIPCClient)ipcClient, mandt, type, (TCPDefaultClient)client, (TCPIPCConfigReference)ref);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.IPCConfigReference, java.util.Properties)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IClient client,
		IPCConfigReference ref,
		Properties securityProperties)
		throws IPCException {
			        return new TcpDefaultIPCSession(
			            ipcClient,
			            mandt,
			            type,
			            (TCPDefaultClient)client,
			            ref,
			            securityProperties);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.IPCItemReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IClient client,
		IPCItemReference ref)
		throws IPCException {
			return new TcpDefaultIPCSession((TcpDefaultIPCClient)ipcClient, mandt, type, (TCPDefaultClient)client, (TCPIPCItemReference)ref);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.IPCItemReference, java.util.Properties)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IClient client,
		IPCItemReference ref,
		Properties securityProperties)
		throws IPCException {
			        return new TcpDefaultIPCSession(
			            ipcClient,
			            mandt,
			            type,
			            (TCPDefaultClient)client,
			(TCPIPCItemReference)ref,
			            securityProperties);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCConfigReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IPCConfigReference ref)
		throws IPCException {
			return new TcpDefaultIPCSession((TcpDefaultIPCClient)ipcClient, mandt, type, null, (TCPIPCConfigReference)ref);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCItemReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IPCItemReference ref)
		throws IPCException {
			return new TcpDefaultIPCSession(ipcClient, mandt, type, null, (TCPIPCItemReference)ref);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String)
	 */
	public IPCClient newIPCClient(String mandt, String type)
		throws IPCException {
			return new TcpDefaultIPCClient(mandt, type);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String)
	 */
	public IPCClient newIPCClient(String sessionId) throws IPCException {
		return new TcpDefaultIPCClient(sessionId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient()
	 */
	public IPCClient newIPCClient() throws IPCException {
		return new TcpDefaultIPCClient();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient)
	 */
	public DefaultIPCClient newIPCClient(
		String mandt,
		String string,
		IClient cachingClient)
		throws IPCException {
			return new TcpDefaultIPCClient(mandt,string,cachingClient);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCItemReference, java.util.Properties)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCItemReference reference,
		Properties securityProperties) {
			return new TcpDefaultIPCClient(mandt,type,reference,securityProperties);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCConfigReference)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCConfigReference reference) {
			return new TcpDefaultIPCClient(mandt,type,reference);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCConfigReference, java.util.Properties)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCConfigReference reference,
		Properties securityProperties) {
			return new TcpDefaultIPCClient(mandt,type,reference,securityProperties);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(IMonitor monitor) {
		return new TcpDefaultIPCClient(monitor);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IMonitor monitor) {
			return new TcpDefaultIPCClient(mandt,type,monitor);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(String sessionId, IMonitor monitor) {
		return new TcpDefaultIPCClient(sessionId,monitor);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCItemReference, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCItemReference reference,
		IMonitor monitor) {
			return new TcpDefaultIPCClient(mandt,type,reference,monitor);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCConfigReference, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCConfigReference reference,
		IMonitor monitor) {
			return new TcpDefaultIPCClient(mandt,type,reference,monitor);
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCItemReference)
     */
    public IPCClient newIPCClient(
        String mandt,
        String type,
        IPCItemReference reference) {
            return new TcpDefaultIPCClient(mandt, type, reference);
    }
    
    public DefaultCharacteristic newCharacteristic(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String languageDependentName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String description) {
        return new TcpDefaultCharacteristic(
            configurationChangeStream,
            ipcClient,
            instance,
            groupName,
            name,
            unit,
            entryFieldMask,
            typeLength,
            numberScale,
            valueType,
            allowsAdditionalValues,
            visible,
            allowsMultipleValues,
            required,
            expanded,
            consistent,
            isDomainConstrained,
            hasIntervalAsDomain,
            readonly,
            pricingRelevant,
            "", null, null, null, null);
    }


    public DefaultCharacteristic newCharacteristic(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String languageDependentName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String description,
        String applicationViews,
        String[] csticMimeLNames,
        String[] csticMimeObjectTypes,
        String[] csticMimeObjects,
        int[] index) {
        return new TcpDefaultCharacteristic(
            configurationChangeStream,
            ipcClient,
            instance,
            groupName,
            name,
            unit,
            entryFieldMask,
            typeLength,
            numberScale,
            valueType,
            allowsAdditionalValues,
            visible,
            allowsMultipleValues,
            required,
            expanded,
            consistent,
            isDomainConstrained,
            hasIntervalAsDomain,
            readonly,
            pricingRelevant,
            applicationViews,
            csticMimeLNames,
            csticMimeObjectTypes,
            csticMimeObjects,
            index);
    }
    
    public DefaultInstance newInstance(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        DefaultConfiguration configuration,
        String instanceId,
        String parentId,
        boolean complete,
        boolean consistent,
        String quantity,
        String name,
        String languageDependentName,
        String description,
        String bomPosition,
        String price,
        boolean userOwned,
        boolean configurable,
        boolean specializable,
        boolean unspecializable,
        MimeObjectContainer mimeObjects) {
        return new TcpDefaultInstance(
            configurationChangeStream,
            ipcClient,
            configuration,
            instanceId,
            parentId,
            complete,
            consistent,
            quantity,
            name,
            languageDependentName,
            description,
            bomPosition,
            price,
            userOwned,
            configurable,
            specializable,
            unspecializable,
            mimeObjects);
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfigValue()
     */
    public ConfigValue newConfigValue() {
        return new TcpConfigValue();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfigValue(com.sap.sce.kbrt.ext_configuration)
     */
    public ConfigValue newConfigValue(ext_configuration config) {
        return new TcpConfigValue(config);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfiguration(com.sap.spc.remote.client.object.imp.DefaultIPCSession, java.lang.String)
     */
    public DefaultConfiguration newConfiguration(DefaultIPCSession session, String configId) throws IPCException {
        return new TcpDefaultConfiguration(session, configId);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfiguration(com.sap.spc.remote.client.object.IPCClient, java.lang.String)
     */
    public DefaultConfiguration newConfiguration(IPCClient ipcClient, String id) {
        return new TcpDefaultConfiguration(ipcClient, id);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfigurationChangeStream(com.sap.spc.remote.client.object.Configuration, com.sap.spc.remote.client.object.imp.CachingClient)
     */
    public ConfigurationChangeStream newConfigurationChangeStream(
        Configuration configuration,
        IClient cachingClient) {
        return new TcpConfigurationChangeStream(configuration, (TCPDefaultClient)cachingClient);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newExplanationTool(com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.Configuration)
     */
    public DefaultExplanationTool newExplanationTool(
        IPCClient ipcClient,
        Configuration configuration) {
        return new TcpDefaultExplanationTool(ipcClient, configuration);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConflictParticipant(com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.Conflict, java.lang.String, java.lang.String, boolean, boolean, java.util.Locale, org.apache.struts.util.MessageResources)
     */
    public DefaultConflictParticipant newConflictParticipant(IPCClient ipcClient,
        Conflict conflict,
        String id,
        String factKey,
        boolean causedByUser,
        boolean causedByDefault,
        Locale locale,
        MessageResources resources) {
            return new TcpDefaultConflictParticipant(ipcClient, conflict, id, factKey, causedByUser, causedByDefault, locale, resources);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newKBProfile(java.lang.String, com.sap.spc.remote.client.object.KnowledgeBase)
     */
    public DefaultKBProfile newKBProfile(String name, KnowledgeBase kb) {
        return new TcpDefaultKBProfile(name, kb);
    }
	public  DefaultIPCDocumentProperties newIPCDocumentProperties()
	 {
		return new TCPDefaultIPCDocumentProperties();
		//return (DefaultIPCDocumentProperties)getObject(DefaultIPCDocumentProperties.class, new Object[] {});
	}
    
	/**
	* Creates a new IPCItemProperties object as clone of initialData.
	* @param initialData
	* @return
	* @throws IPCException
	*/
	public  DefaultIPCDocumentProperties newIPCDocumentProperties(IPCDocumentProperties initialData)
	 {
		return new TCPDefaultIPCDocumentProperties(initialData);
		//return (DefaultIPCDocumentProperties)getObject(DefaultIPCDocumentProperties.class, new Object[] {initialData});
	}
    
    public DefaultCharacteristicValue newCharacteristicValue(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        int[] index,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects) {
        return new TcpDefaultCharacteristicValue(
            configurationChangeStream,
            ipcClient,
            characteristic,
            name,
            languageDependentName,
            conditionKey,
            price,
            index,
            mimeValueLNames,
            mimeObjectTypes,
            mimeObjects);
    }

    public DefaultCharacteristicValue newCharacteristicValue(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        int[] index,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects,
        boolean defaultValue) {
        return new TcpDefaultCharacteristicValue(
            configurationChangeStream,
            ipcClient,
            characteristic,
            name,
            languageDependentName,
            conditionKey,
            price,
            index,
            mimeValueLNames,
            mimeObjectTypes,
            mimeObjects,
            defaultValue);
    }

    public DefaultCharacteristicValue newCharacteristicValue( 
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        boolean isAdditionalValue) {
        return new TcpDefaultCharacteristicValue(
            configurationChangeStream,
            ipcClient,
            characteristic,
            name,
            languageDependentName,
            conditionKey,
            price,
            isAdditionalValue);
    }

    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCConfigReference()
     */
    public IPCConfigReference newIPCConfigReference() {
        return new TCPIPCConfigReference();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItemReference()
     */
    public IPCItemReference newIPCItemReference() {
        return new TCPIPCItemReference();
    }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.IPCClient, com.sap.isa.core.eai.BackendBusinessObject)
	 */
	public DefaultIPCSession newIPCSession(IPCClient ipcClient, BackendBusinessObject rfcClientSupport) {
		throw new IPCException("Unimplemented method");
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(com.sap.isa.core.eai.BackendBusinessObject, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(BackendBusinessObject rfcClientSupport, IMonitor _monitor) {
		throw new IPCException("Unimplemented method");
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.IPCClient, com.sap.isa.core.eai.BackendBusinessObject, com.sap.util.monitor.jarm.IMonitor)
	 */
	public DefaultIPCSession newIPCSession(IPCClient client, BackendBusinessObject backendObject, IMonitor monitor) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(com.sap.isa.core.eai.BackendBusinessObject)
	 */
	public IPCClient newIPCClient(BackendBusinessObject backendObject) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(com.sap.isa.core.eai.BackendBusinessObject, com.sap.util.monitor.jarm.IMonitor, java.lang.String)
	 */
	public IPCClient newIPCClient(
		BackendBusinessObject rfcClientSupport,
		IMonitor _monitor,
		String tokenId, 
		String configId) {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(com.sap.isa.core.eai.BackendBusinessObject, java.lang.String)
	 */
	public IPCClient newIPCClient(
		BackendBusinessObject rfcClientSupport,
		String tokenId,
		String configId) {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCClient, com.sap.isa.core.eai.BackendBusinessObject, java.lang.String)
	 */
	public DefaultIPCSession newIPCSession(RfcDefaultIPCClient client, BackendBusinessObject rfcClientSupport, String tokenId, String configId) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCClient, com.sap.isa.core.eai.BackendBusinessObject, com.sap.util.monitor.jarm.IMonitor, java.lang.String)
	 */
	public DefaultIPCSession newIPCSession(RfcDefaultIPCClient client, BackendBusinessObject rfcClientSupport, IMonitor monitor, String tokenId, String configId) {
		return null;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newProductVariant(com.sap.spc.remote.client.object.IPCClient, java.lang.String, double, java.lang.String, java.lang.String, java.util.List, com.sap.spc.remote.client.object.ProductVariantFeature)
     */
    public DefaultProductVariant newProductVariant(
        IPCClient ipcClient,
        String id,
        double distance,
        String matchPoints,
        String maxMatchPoints,
        List features,
        ProductVariantFeature mainDifference) {
            return new TcpDefaultProductVariant(ipcClient, id, distance, matchPoints, maxMatchPoints, features, mainDifference);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newLoadingStatus(com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.Configuration)
     */
    public DefaultLoadingStatus newLoadingStatus(IPCClient client, Configuration config) {
        return new TcpDefaultLoadingStatus(client, config);
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newTTEDocument(com.sap.spc.remote.client.object.imp.DefaultIPCSession, com.sap.spc.remote.client.object.IPCDocument, boolean)
     */
    public DefaultTTEDocument newTTEDocument(
        DefaultIPCSession session,
        IPCDocument document,
        boolean createDocumentOnServer) {
            throw new IPCException(IPCException.UNIMPLEMENTED);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newTTEItem(java.lang.String, com.sap.spc.remote.client.object.IPCItemProperties, com.sap.spc.remote.client.object.IPCDocumentProperties, com.sap.spc.remote.client.object.TTEDocument, boolean)
     */
    public DefaultTTEItem newTTEItem(
        String itemId,
        IPCItemProperties itemProps,
        IPCDocumentProperties docProps,
        TTEDocument tteDoc,
        boolean createItemOnServer) {
            throw new IPCException(IPCException.UNIMPLEMENTED);
	}
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newPricingConverter(com.sap.spc.remote.client.object.imp.DefaultIPCSession)
     */
    public DefaultPricingConverter newPricingConverter(DefaultIPCSession session) {
        return new TcpDefaultPricingConverter(session);
    }

}
