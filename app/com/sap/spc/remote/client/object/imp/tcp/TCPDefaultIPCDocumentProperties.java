package com.sap.spc.remote.client.object.imp.tcp;

import java.util.ArrayList;

import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocumentProperties;

/**
 * @author I026584
 *
 */
public class TCPDefaultIPCDocumentProperties
	extends DefaultIPCDocumentProperties {

		public TCPDefaultIPCDocumentProperties() {
			m_salesOrganisation     = null;
			m_distributionChannel   = null;
			m_division              = null;
			m_language              = null;
			m_documentCurrency      = null;
			m_localCurrency         = null;
			m_DepartureCountry      = null;
			m_country               = null;
			m_pricingProcedure      = null;
			m_DepartureRegion       = null;
			m_headerAttributeNames  = new ArrayList();
			m_headerAttributeValues = new ArrayList();
			m_externalId            = null;
			m_editMode              = ' ';

			m_groupConditionProcessing = true;
            m_stripDecimalPlaces = true;
		}
		public TCPDefaultIPCDocumentProperties(IPCDocumentProperties initialData) {
				this();

				m_salesOrganisation = initialData.getSalesOrganisation();
				m_distributionChannel = initialData.getDistributionChannel();
				m_division = initialData.getDivision();
				m_language = initialData.getLanguage();
				m_documentCurrency = initialData.getDocumentCurrency();
				m_localCurrency = initialData.getLocalCurrency();
				m_DepartureCountry = initialData.getDepartureCountry();
				m_country = initialData.getCountry();
				m_pricingProcedure = initialData.getPricingProcedure();
				m_DepartureRegion = initialData.getDepartureRegion();

                String[] headerAttributeNamesArray = initialData.getAllHeaderAttributeNames();
                for (int i=0; i<headerAttributeNamesArray.length; i++){
                    m_headerAttributeNames.add(headerAttributeNamesArray[i]);    
                }
                
                String[] headerAttributeValuesArray = initialData.getAllHeaderAttributeValues();
                for (int i=0; i<headerAttributeValuesArray.length; i++){
                    m_headerAttributeValues.add(headerAttributeValuesArray[i]);
                }
                
				m_externalId = initialData.getExternalId();
				m_editMode = initialData.getEditMode();

				m_decimalSeparator = initialData.getDecimalSeparator();
				m_groupingSeparator     = initialData.getGroupingSeparator();
				m_groupConditionProcessing = initialData.isGroupConditionProcessingEnabled();
                m_stripDecimalPlaces = initialData.isStripDecimalPlaces();
			}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setSalesOrganisation(java.lang.String)
     */
    public void setSalesOrganisation(String salesOrganisation) {
        //m_salesOrganisation.setInitialValue(salesOrganisation);
        m_salesOrganisation = salesOrganisation;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDepartureCountry(java.lang.String)
     */
    public void setDepartureCountry(String departureCountry) {
        m_DepartureCountry = departureCountry;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDepartureRegion(java.lang.String)
     */
    public void setDepartureRegion(String departureRegion) {
        m_DepartureRegion = departureRegion;

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDistributionChannel(java.lang.String)
     */
    public void setDistributionChannel(String distributionChannel) {
        m_distributionChannel = distributionChannel;

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDivision(java.lang.String)
     */
    public void setDivision(String division) {
        m_division = division;

    }


}
