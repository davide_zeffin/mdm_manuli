package com.sap.spc.remote.client.object.imp;

import java.util.Collection;
import java.util.Iterator;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.cache.Cache;

public abstract class DefaultCharacteristicValue extends BusinessObjectBase implements CharacteristicValue {

    protected IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
    protected ConfigurationChangeStream configurationChangeStream;
	protected IClient       cachingClient;
	protected IPCClient           ipcClient;
	protected Characteristic      characteristic;
	protected String              name;
	protected String              languageDependentName;
	protected String              conditionKey;
	protected String              price;
	protected MimeObjectContainer mimeObjects;
	protected boolean             assignable;
	protected boolean             assigned;
	protected boolean             assignedByUser;
	protected boolean             assignedBySystem;
	protected boolean             visible;
	protected boolean             isAdditionalValue;
	protected boolean             initialStateOfAssigned;
    protected boolean             inDomain;
    protected boolean             defaultValue;

	protected boolean             closed;
	protected int                 valueOperation;

    private static final Location location = ResourceAccessor.getLocation(DefaultCharacteristicValue.class);
    private static final Category category = ResourceAccessor.category;

    public static String convertToString(Collection characteristicValues) {
        StringBuffer result = new StringBuffer();
        for (Iterator it = characteristicValues.iterator(); it.hasNext(); ) {
            CharacteristicValue value = (CharacteristicValue)it.next();
            result.append("[");
            result.append(value.getLanguageDependentName());
            result.append("]");
        }
        return result.toString();
    }
    


	protected abstract void init(  ConfigurationChangeStream configurationChangeStream,
                        IPCClient ipcClient,
						Characteristic characteristic,
						String name,
			            String languageDependentName,
						String conditionKey,
						String price,
						boolean isAdditionalValue,
						int[] index,
						String[] mimeValueLNames,
						String[] mimeObjectTypes,
						String[] mimeObjects);

    protected abstract void init(  ConfigurationChangeStream configurationChangeStream,
                        IPCClient ipcClient,
                        Characteristic characteristic,
                        String name,
                        String languageDependentName,
                        String conditionKey,
                        String price,
                        boolean isAdditionalValue,
                        int[] index,
                        String[] mimeValueLNames,
                        String[] mimeObjectTypes,
                        String[] mimeObjects,
                        boolean defaultValue);

	/**
	 * This method updates the dynamic fields of this value
	 */
	protected void update(    boolean inDomain,
					boolean assigned,
					boolean assignedByUser,
					boolean assignedBySystem,
					boolean visible) {
        
        this.inDomain = inDomain;
        this.assigned = assigned;
        this.assignedByUser = assignedByUser;
        this.assignedBySystem = assignedBySystem;
        this.visible = visible;
        this.initialStateOfAssigned = assigned;
        
        if (characteristic.allowsAdditionalValues()) {
            this.assignable = true;
        }else if (!characteristic.isDomainConstrained()){
        	this.assignable = true;       	
        }
        else if (this.assignedByUser && this.assigned) {
            this.assignable = true;
        }
        else if (this.inDomain) {
            this.assignable = true;
        }
        else {
            this.assignable = false;
        }
        
        valueOperation = ConfigurationChange.NO_OPERATION;
    }

    /**
     * This method updates the dynamic fields of this value
     */
    protected void update(    boolean inDomain,
                    boolean assigned,
                    boolean assignedByUser,
                    boolean assignedBySystem,
                    boolean visible,
                    boolean defaultValue) {
        
        this.defaultValue = defaultValue;
        update(inDomain, assigned, assignedByUser, assignedBySystem, visible);
    }


	public Characteristic getCharacteristic() {
        location.entering("Characteristic getCharacteristic()");
        if (location.beDebug()){
            location.debugT("return this.characteristic " + this.characteristic.getName());
        }
        location.exiting();
        return this.characteristic;
        
	}

	public String getLanguageDependentName(){
        location.entering("getLanguageDependentName()");
         if (location.beDebug()){
             location.debugT("return this.languageDependentName " + this.languageDependentName + " (" + this.characteristic.getName()+")");
         }
         location.exiting();
		return this.languageDependentName;
	}

	
    private KnowledgebaseCacheContainer getKBCacheContainer()
    {
        Cache.Access access;
        try {
            access = Cache
            .getAccess(DefaultCharacteristicGroup.csticGroupCacheRegion);
        } catch (Cache.Exception e) {
            category.logT(Severity.ERROR, location, e.toString());
            return null;
        }
        KnowledgebaseCacheKey csticKey = new KnowledgebaseCacheKey(
                this.getCharacteristic().getInstance());
        return (KnowledgebaseCacheContainer)access.get(csticKey, this.getCharacteristic().getGroup().getInstance());
    }
	
	public String getDescription(){
        location.entering("getDescription()");
        String description=null;
        try {
            Cache.Access access = Cache
            .getAccess(DefaultCharacteristicGroup.csticGroupCacheRegion);
            Characteristic cstic = getCharacteristic();
            KnowledgebaseCacheKey csticKey = new KnowledgebaseCacheKey(
                    cstic.getInstance());
            KnowledgebaseCacheContainer kbContainer = getKBCacheContainer();
            if( kbContainer != null)
                description = kbContainer.getValueDescription(cstic.getName(),name);
        } 
        catch (Cache.Exception e) {
            category.logT(Severity.ERROR, location, e.toString());
        }
        if (location.beDebug()) {
            if (description != null) {
                location.debugT("return description " + description + " (" + characteristic.getName()+")");
            }
            else {
                
                location.debugT("return \"\"" + " (" + characteristic.getName()+")");
            }
        }
        location.exiting();
		return (description != null) ? description : "";
	}

	public String getPrice(){
      location.entering("getPrice()");
      if (location.beDebug()){
        location.debugT("return this.price " + this.price);
      }
      location.exiting();
	  return this.price;
	}

	public boolean isAdditionalValue() {
        location.entering("isAdditionalValue()");
         if (location.beDebug()){
             location.debugT("return this.isAdditionalValue " + this.isAdditionalValue + " (" + this.name+")");
         }
         location.exiting();
	    return this.isAdditionalValue;
	}

	public boolean isAssigned(){
        location.entering("isAssigned()");
         if (location.beDebug()){
             location.debugT("return this.assigned " + this.assigned + " (" + this.name+")");
         }
         location.exiting();
		return this.assigned;
	}

	public boolean isAssignedByUser(){
        location.entering("isAssignedByUser()");
         if (location.beDebug()){
             location.debugT("return this.isAssignedByUser " + this.assignedByUser + " (" + this.name+")");
         }
         location.exiting();
		return this.assignedByUser;
	}

	public boolean isAssignedBySystem(){
        location.entering("isAssignedBySystem()");
         if (location.beDebug()){
             location.debugT("return this.isAssignedBySystem " + this.assignedBySystem + " (" + this.name+")");
         }
         location.exiting();
		return this.assignedBySystem;
	}

	public boolean isVisible(){
        location.entering("isVisible()");
         if (location.beDebug()){
             location.debugT("return this.isVisible " + this.visible + " (" + this.name+")");
         }
         location.exiting();
		return this.visible;
	}

	public String getName(){
        location.entering("getName()");
         if (location.beDebug()){
             location.debugT("return this.name " + this.name);
         }
         location.exiting();
		return this.name;
	}

	public MimeObjectContainer getMimeObjects(){
        location.entering("getMimeObjects()");
         if (location.beDebug()){
             if (this.mimeObjects != null) {
                 location.debugT("return this.mimeObjects " + this.mimeObjects.toString());
             }
            
         }
         location.exiting();
	  return this.mimeObjects;
	}

	public boolean isAssignable(){
        location.entering("isAssignable()");
         if (location.beDebug()){
             location.debugT("return this.isAssignable " + this.assignable + " (" + this.name+")");
         }
         location.exiting();
		return this.assignable;
	}

    public boolean isInDomain(){
        location.entering("isInDomain()");
         if (location.beDebug()){
             location.debugT("return this.inDomain " + this.inDomain + " (" + this.name+")");
         }
         location.exiting();
        return this.inDomain;
    }

	public void clear(){
        location.entering("clear()");
        if(assignedByUser) {
			assigned            = false;
			assignedByUser      = true;     // for compatibility. Makes no sense to be true, but...
			assignedBySystem    = false;
			this.valueOperation = ConfigurationChange.DELETE_CHARACTERISTIC_VALUE;
        }
        location.exiting();
	}

	public void assign(){
        location.entering("assign()");
		assigned            = true;
		assignedByUser      = true;
		assignedBySystem    = false;
		this.valueOperation = ConfigurationChange.SET_CHARACTERISTIC_VALUE;
        location.exiting();
	}

	public void flush() {
        location.entering("flush()");
	    if (valueOperation != ConfigurationChange.NO_OPERATION) {
		    ConfigurationChange change = null;
		    if (valueOperation == ConfigurationChange.SET_CHARACTERISTIC_VALUE &&
				!initialStateOfAssigned) {
			    change = new ConfigurationChange(ConfigurationChange.SET_CHARACTERISTIC_VALUE,this);
			} else if (valueOperation == ConfigurationChange.DELETE_CHARACTERISTIC_VALUE &&
				initialStateOfAssigned) {
			    change = new ConfigurationChange(ConfigurationChange.DELETE_CHARACTERISTIC_VALUE,this);
			}
			if (change != null) {
			    configurationChangeStream.addConfigurationChange(change);
			}
		}
        location.exiting();
	}

	public void close(){
        location.entering("close()");
		closed = true;
//        location.exiting();
	}

	public boolean isClosed(){
        location.entering("icClosed()");
        if (location.beDebug()){
            location.debugT("return closed " + closed);
        }
        location.exiting();
		return closed;
	}

	public boolean isRelevant(String commandName) {
        location.entering("isRelevant(String commandName)");
        if (location.beDebug()){
             location.debugT("Parameters:");
             location.debugT("String commandName " + commandName);
             location.debugT("return true " + true + " (" + this.name+")");
        }
        location.exiting();
		return true;
	}

	public String toString() {
		return
		    "<VALUE>" +
            " name: " + name +
            "; languageDependentName: " + this.languageDependentName +
			"; assignable: "   + new Boolean(assignable) +
			"; assigned: " + new Boolean(assigned) +
			"; assignedBySystem: " + new Boolean(assignedBySystem) +
			"; assignedByUser: " + new Boolean(assignedByUser) +
			"; isAdditionalValue: " + new Boolean(isAdditionalValue) +
			"; price: " + price +
			"; visible: " + visible +
			"</VALUE>";
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.CharacteristicValue#getConditionKey()
	 */
	public String getConditionKey() {
		return conditionKey;
	}
    
    public boolean isDefaultValue(){
        return this.defaultValue;
    }
    

}
