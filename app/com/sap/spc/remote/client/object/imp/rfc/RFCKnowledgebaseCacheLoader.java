/*
 */
package com.sap.spc.remote.client.object.imp.rfc;

import java.util.Iterator;
import java.util.List;

import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.cache.Cache.Exception;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.ParameterList;
import com.sap.mw.jco.JCO.Table;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.KnowledgebaseCacheContainer;
import com.sap.spc.remote.client.object.imp.KnowledgebaseCacheKey;
import com.sap.spc.remote.client.object.imp.KnowledgebaseCacheContainer.CsticContainer;
import com.sap.spc.remote.client.rfc.function.CfgApiGetCstic;
import com.sap.spc.remote.client.rfc.function.CfgApiGetCsticGroups;
import com.sap.spc.remote.client.rfc.function.CfgApiGetFilteredCstics;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

/**
 * Load static data from the knowledge base and stores it in the
 * ISA cache (session spreading)
 */
public class RFCKnowledgebaseCacheLoader extends Cache.Loader {
    protected static final Category category = ResourceAccessor.category;

    private static final Location location = ResourceAccessor
            .getLocation(RfcDefaultInstance.class);
	private boolean javaImplementationUpdated = true; //assumes that the Java patch is also applied, switches if not

    /*
     * (non-Javadoc)
     * 
     * @see com.sap.isa.core.cache.Cache.Loader#load(java.lang.Object,
     *      java.lang.Object)
     */
    public Object load(Object key, Object attributes) throws Exception {
//      String configId;
//      String instanceId;
//     String groupName;
//      boolean isBaseGroup;
        
        IPCClient ipcClient = ((Instance)attributes).getIpcClient();
        RfcDefaultIPCSession session = (RfcDefaultIPCSession)ipcClient.getIPCSession();
        RFCDefaultClient client = (RFCDefaultClient)session.getClient();
        
        KnowledgebaseCacheKey csticKey = (KnowledgebaseCacheKey) key;
        JCO.Function functionCfgApiGetFilteredCstics;
        JCO.ParameterList im;
        JCO.ParameterList ex;
        JCO.ParameterList tbl;
        try {
            functionCfgApiGetFilteredCstics = client
                    .getFunction(IFunctionGroup.CFG_API_GET_FILTERED_CSTICS);
            im = functionCfgApiGetFilteredCstics.getImportParameterList();
            ex = functionCfgApiGetFilteredCstics.getExportParameterList();
            tbl = functionCfgApiGetFilteredCstics.getTableParameterList();
        } catch (ClientException ce) {
            location.traceThrowableT(Severity.DEBUG, "exception.communication",
                    ce);
            throw new Exception(ce.getMessage());
        }
        prepareJCO(im, ex, csticKey, ((Instance)attributes));

        category.logT(Severity.PATH, location,
                "executing RFC CFG_API_GET_FILTERED_CSTICS");
        try {
            client.execute(functionCfgApiGetFilteredCstics);
        } catch (ClientException ce) {
            location.traceThrowableT(Severity.DEBUG, "exception.communication",
                    ce);
            throw new Exception(ce.getMessage());
        }
        category.logT(Severity.PATH, location,
                "done with RFC CFG_API_GET_FILTERED_CSTICS");
        return processJCOResult(ex);
    }

    /**
     * Prepares import parameter for RFC call to CFG_API_GET_FILTERED_CSTICS
     * All descriptions of all values will be retrieved
     * @param im Import Parameterlist
     */
    private void prepareJCO(ParameterList im, ParameterList ex,
            KnowledgebaseCacheKey csticKey, Instance inst) {
        Configuration cfg = inst.getConfiguration();

        im.setValue(cfg.getId(), CfgApiGetFilteredCstics.CONFIG_ID);
        im.setValue(inst.getId(), CfgApiGetFilteredCstics.INST_ID);
		im.setValue("T", CfgApiGetFilteredCstics.INCLUDE_UNGROUPED_CSTICS); 
        im.setValue("F", CfgApiGetFilteredCstics.UNGROUPED_CSTICS_ONLY);

        // process table imCsticInfoGroups
        // optional, TABLE, CFG: Characteristic group insterest
        // (GetFilteredCsticsAndVal
        JCO.Table imCsticInfoGroups = im
                .getTable(CfgApiGetFilteredCstics.CSTIC_INFO_GROUPS);
        imCsticInfoGroups.appendRow();
        imCsticInfoGroups.setValue("0", CfgApiGetFilteredCstics.GROUP_ID); // CHAR,
        
        // Group
        // Name
        StringBuffer unknownCsticsInterest = new StringBuffer();
        unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_DESCRIPTIONS);
        // unknownCsticsInterest for values
        unknownCsticsInterest
                .append(CfgApiGetFilteredCstics.INTEREST_VALUE_DESCRIPTIONS);
        unknownCsticsInterest
        	.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_LNAMES);
        // Values are no static knowledgebase data unknownCsticsInterest
        //	.append(CfgApiGetFilteredCstics.INTEREST_VALUE_LNAMES);
        unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_GROUPS);
        unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_GROUP_POSITION);

        imCsticInfoGroups.setValue(unknownCsticsInterest.toString(),
                CfgApiGetFilteredCstics.INTEREST); // STRING, CFG: Data
        // element of type STRING
        imCsticInfoGroups.appendRow();
        imCsticInfoGroups.setValue("1", CfgApiGetFilteredCstics.GROUP_ID); // CHAR,
        imCsticInfoGroups.setValue(unknownCsticsInterest.toString(),
                CfgApiGetFilteredCstics.INTEREST); // STRING, CFG: Data
        // Group
        // Name

		boolean hasGroupIdLangIndep = im.hasField(CfgApiGetFilteredCstics.GROUP_ID_LANGINDEP);
		String groupKeyField = CfgApiGetCsticGroups.GROUP_NAME;
		JCO.Table exCsticGroups = ex.getTable(CfgApiGetCsticGroups.CSTIC_GROUPS);
//		reaction to note 1270205
		if (exCsticGroups.hasField(CfgApiGetCsticGroups.GROUP_ID) && javaImplementationUpdated) {
			groupKeyField = CfgApiGetCsticGroups.GROUP_ID;
		}else {
			groupKeyField = CfgApiGetCsticGroups.GROUP_NAME;
		 }
		if (hasGroupIdLangIndep && groupKeyField.equals(CfgApiGetCsticGroups.GROUP_ID) && javaImplementationUpdated) {
			//if the flag in the function definition exists
			im.setValue(RfcConstants.TRUE, CfgApiGetFilteredCstics.GROUP_ID_LANGINDEP);
		}
					
        List groups = inst.getCharacteristicGroups();

		for(Iterator iter = groups.iterator();iter.hasNext();){
			CharacteristicGroup group=(CharacteristicGroup)iter.next();
			 if(!group.isBaseGroup()){
				JCO.Table imFilterGroups = im.getTable(CfgApiGetFilteredCstics.FILTER_GROUPS);
				imFilterGroups.appendRow();
				imFilterGroups.setValue(group.getName(), CfgApiGetFilteredCstics.GROUP_ID); 
    } 
		 }
		 //ungrouped cstics always included, see top of method
    } 

    /**
     * Iterates throw the groups table and creates
     * It should only be one group in the table
     * @param ex Export parameter of CFG_API_GET_FILTERED_CSTICS
     * @throws Exception: More than one group in the result table
     */
    private KnowledgebaseCacheContainer processJCOResult(ParameterList ex) throws Exception {
        // process table exCsticsInGroups
        // TABLE, CFG: Table of characteristics sorted in groups
        JCO.Table exCsticsInGroups = ex
                .getTable(CfgApiGetFilteredCstics.CSTICS_IN_GROUPS);
        int exCsticsInGroupsCounter;
        KnowledgebaseCacheContainer container = new KnowledgebaseCacheContainer();
        for (exCsticsInGroupsCounter = 0; exCsticsInGroupsCounter < exCsticsInGroups
                .getNumRows(); exCsticsInGroupsCounter++) {
            
            exCsticsInGroups.setRow(exCsticsInGroupsCounter);
            String groupName = exCsticsInGroups.getString(CfgApiGetFilteredCstics.GROUP_ID);
            JCO.Table exCsticsInGroupsCstics = exCsticsInGroups
                    .getTable(CfgApiGetFilteredCstics.CSTICS);
            processCsticsTable(container, exCsticsInGroupsCstics,
                    ex.getTable(CfgApiGetFilteredCstics.CSTIC_GROUPS));
        }
        return container;
    }
    /**
     * Iterates through Cstics table and adds an Entry for all Characteristics into the
     * returned KnowledgebaseCacheContainer
     *
     * @param exCsticsInGroupsCstics
     * @return
     * @throws Cache.Exception
     */
    private void processCsticsTable(KnowledgebaseCacheContainer container, Table exCsticsInGroupsCstics, Table exCsticGroups) throws Cache.Exception {
        int csticsNumRows = exCsticsInGroupsCstics.getNumRows();
        
        for (int exCsticsInGroupsCsticsCounter = 0; exCsticsInGroupsCsticsCounter < csticsNumRows; exCsticsInGroupsCsticsCounter++) {
            exCsticsInGroupsCstics.setRow(exCsticsInGroupsCsticsCounter);
            
            JCO.Table csticDescription = exCsticsInGroupsCstics
                    .getTable(CfgApiGetFilteredCstics.CSTIC_DESCRIPTION);
			String description = getLongDescription(csticDescription);
            JCO.Structure exCsticsInGroupsCsticsCsticHeader = exCsticsInGroupsCstics
            .getStructure(CfgApiGetFilteredCstics.CSTIC_HEADER);
            String csticName = exCsticsInGroupsCsticsCsticHeader
            .getString(CfgApiGetFilteredCstics.CSTIC_NAME);
            String csticLName = exCsticsInGroupsCsticsCsticHeader
            	.getString(CfgApiGetFilteredCstics.CSTIC_LNAME);
            CsticContainer csticContainer = container.addCharacteristic( csticName, csticLName, description);

            JCO.Table exCsticsInGroupsCsticsCsticValues = exCsticsInGroupsCstics
            	.getTable(CfgApiGetFilteredCstics.CSTIC_VALUES);

            for (int exCsticsInGroupsCsticsCsticValuesCounter = 0; exCsticsInGroupsCsticsCsticValuesCounter < exCsticsInGroupsCsticsCsticValues
                    .getNumRows(); exCsticsInGroupsCsticsCsticValuesCounter++) {
                exCsticsInGroupsCsticsCsticValues
                .setRow(exCsticsInGroupsCsticsCsticValuesCounter);
                String valueNames = exCsticsInGroupsCsticsCsticValues
                	.getString(CfgApiGetFilteredCstics.VALUE_NAME);

                JCO.Table valueDescription = 
                    	exCsticsInGroupsCsticsCsticValues.getTable(CfgApiGetFilteredCstics.VALUE_DESCRIPTION);
                csticContainer.addValue(valueNames,getLongDescription(valueDescription));
            }
        }
    }
	/**
	 * 
	 * @param description JCO text table 
	 * @return Content of the text table
	 */
	public static String getLongDescription(JCO.Table description) {
		if (description != null) {
			Table vd = description;
			StringBuffer csticDescriptionBuffer = new StringBuffer();
			for (int k = 0; k < vd.getNumRows(); k++) {
				vd.setRow(k);
				String txt = (vd.getString(CfgApiGetCstic.TEXT_LINE));
				String formatInfo = (vd.getString(CfgApiGetCstic.TEXT_FORMAT));
				if (txt != null && txt.length() > 0) {
					
					if (addSpace(txt,72))
						txt += " ";
					
					if (formatInfo.equals(CfgApiGetCstic.SAPSCRIPT_NEWLINE)
						|| formatInfo.equals(
							CfgApiGetCstic.SAPSCRIPT_PARAGRAPH) && k != 0) { //do not add \n to before first line.
						csticDescriptionBuffer.append('\n');
					}					
					csticDescriptionBuffer.append(txt);
				}
			}
			if (vd.getNumRows() > 0) {
				return csticDescriptionBuffer.toString();
			}
		}
		return null;
	}
	
	public static boolean addSpace(String descriptionLine,
			int maxDescriptionLength) {
		boolean flag = true;
		String lastWord = new String();
		int lastWordLength;
		if (descriptionLine.length() == maxDescriptionLength) {
			for (int j = maxDescriptionLength - 1; j >= 0; j--) {
				if (descriptionLine.charAt(j) == ' ' && flag) {
					flag = false;
					lastWord = descriptionLine.subSequence(j + 1,
							maxDescriptionLength).toString();
					break;
				}
				if (j == 0)
					lastWord = descriptionLine;
			}

			lastWordLength = lastWord.length();

			switch (lastWordLength) {

			case 1:
				if ((lastWord.equalsIgnoreCase("h"))
						|| (lastWord.equalsIgnoreCase("w")))
					return false;
				else
					return true;
			case 2:
				if ((lastWord.equalsIgnoreCase("ht"))
						|| (lastWord.equalsIgnoreCase("ww")))
					return false;
				else
					return true;
			case 3:
				if ((lastWord.equalsIgnoreCase("htt"))
						|| (lastWord.equalsIgnoreCase("www")))
					return false;
				else
					return true;
			default:
				if ((lastWord.startsWith("http"))
						|| (lastWord.startsWith("www")))
					return false;
				else
					return true;
			}

		} else { // else for if (descriptionLine.length() ==
					// maxDescriptionLength)
			return true;
		}
	}	

}