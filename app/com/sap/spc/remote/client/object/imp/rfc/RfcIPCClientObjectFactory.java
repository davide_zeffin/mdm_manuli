/*
 * Created on Dec 28, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.rfc;

import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.ProductVariantFeature;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.spc.remote.client.object.imp.ConfigValue;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristic;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristicValue;
import com.sap.spc.remote.client.object.imp.DefaultConfiguration;
import com.sap.spc.remote.client.object.imp.DefaultConflictParticipant;
import com.sap.spc.remote.client.object.imp.DefaultExplanationTool;
import com.sap.spc.remote.client.object.imp.DefaultIPCClient;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocumentProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultInstance;
import com.sap.spc.remote.client.object.imp.DefaultKBProfile;
import com.sap.spc.remote.client.object.imp.DefaultLoadingStatus;
import com.sap.spc.remote.client.object.imp.DefaultPricingConverter;
import com.sap.spc.remote.client.object.imp.DefaultProductVariant;
import com.sap.spc.remote.client.object.imp.DefaultTTEDocument;
import com.sap.spc.remote.client.object.imp.DefaultTTEItem;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RfcIPCClientObjectFactory extends IPCClientObjectFactory {

	public static IPCClientObjectFactory getInstance() {
			return new RfcIPCClientObjectFactory();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCDocument(com.sap.spc.remote.client.object.imp.DefaultIPCSession, com.sap.spc.remote.client.object.imp.DefaultIPCDocumentProperties)
	 */
	public DefaultIPCDocument newIPCDocument(
		DefaultIPCSession session,
		DefaultIPCDocumentProperties attr)
		throws IPCException {
			return new RfcDefaultIPCDocument(session, attr);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCDocument(com.sap.spc.remote.client.object.imp.DefaultIPCSession, java.lang.String)
	 */
	public DefaultIPCDocument newIPCDocument(
		DefaultIPCSession session,
		String documentId)
		throws IPCException {
			return new RfcDefaultIPCDocument(session, documentId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItem(com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.imp.DefaultIPCDocument, java.lang.String)
	 */
	public DefaultIPCItem newIPCItem(
		IClient client,
		DefaultIPCDocument document,
		String itemId)
		throws IPCException {
			return new RfcDefaultIPCItem((RFCDefaultClient)client,(RfcDefaultIPCDocument)document, itemId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItem(com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.imp.DefaultIPCDocument, java.lang.String, java.lang.String)
	 */
	public DefaultIPCItem newIPCItem(
		IClient client,
		DefaultIPCDocument document,
		String itemId,
		String parentItemId)
		throws IPCException {
			return new RfcDefaultIPCItem((RFCDefaultClient)client,(RfcDefaultIPCDocument)document, itemId, parentItemId);
//            return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItem(com.sap.spc.remote.client.object.imp.DefaultIPCDocument, com.sap.spc.remote.client.object.IPCItemProperties)
	 */
	public IPCItem newIPCItem(
		DefaultIPCDocument document,
		IPCItemProperties properties)
		throws IPCException {
			return new RfcDefaultIPCItem((RfcDefaultIPCDocument)document,properties);
//            return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItemProperties()
	 */
	public DefaultIPCItemProperties newIPCItemProperties() {
		return new RfcDefaultIPCItemProperties();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItemProperties(com.sap.spc.remote.client.object.IPCItemProperties)
	 */
	public DefaultIPCItemProperties newIPCItemProperties(IPCItemProperties initialData)
		throws IPCException {
			return new RfcDefaultIPCItemProperties(initialData);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItems(com.sap.spc.remote.client.object.imp.DefaultIPCDocument, com.sap.spc.remote.client.object.IPCItemProperties[])
	 */
	public IPCItem[] newIPCItems(
		IPCDocument document,
		IPCItemProperties[] properties)
		throws IPCException {
			return RfcDefaultIPCItem.createItems(document, properties);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, com.sap.spc.remote.client.object.IPCConfigReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		IPCConfigReference reference)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, com.sap.spc.remote.client.object.IPCConfigReference, boolean)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		IPCConfigReference reference,
		boolean isExtendedData)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}



	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.IPCConfigReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IClient client,
		IPCConfigReference ref)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.IPCConfigReference, java.util.Properties)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IClient client,
		IPCConfigReference ref,
		Properties securityProperties)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.IPCItemReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IClient client,
		IPCItemReference ref)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient, com.sap.spc.remote.client.object.IPCItemReference, java.util.Properties)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IClient client,
		IPCItemReference ref,
		Properties securityProperties)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCConfigReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IPCConfigReference ref)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCItemReference)
	 */
	public DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		String mandt,
		String type,
		IPCItemReference ref)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String)
	 */
	public IPCClient newIPCClient(String mandt, String type)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	public IPCClient newIPCClient(BackendBusinessObject rfcClientSupport) throws IPCException{
		return new RfcDefaultIPCClient(rfcClientSupport);
	}

	public IPCClient newIPCClient(BackendBusinessObject rfcClientSupport, IMonitor monitor) throws IPCException{
		return new RfcDefaultIPCClient(rfcClientSupport, monitor);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String)
	 */
	public IPCClient newIPCClient(String sessionId) throws IPCException {
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient()
	 */
	public IPCClient newIPCClient() throws IPCException {
		return new RfcDefaultIPCClient();
//            return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.imp.CachingClient)
	 */
	public DefaultIPCClient newIPCClient(
		String mandt,
		String string,
		IClient cachingClient)
		throws IPCException {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCItemReference, java.util.Properties)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCItemReference reference,
		Properties securityProperties) {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCConfigReference)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCConfigReference reference) {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCConfigReference, java.util.Properties)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCConfigReference reference,
		Properties securityProperties) {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(IMonitor monitor) {
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IMonitor monitor) {
			throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(String sessionId, IMonitor monitor) {
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCItemReference, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCItemReference reference,
		IMonitor monitor) {
			return new RfcDefaultIPCClient(mandt,type,reference,monitor);
//            return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCConfigReference, com.sap.util.monitor.jarm.IMonitor)
	 */
	public IPCClient newIPCClient(
		String mandt,
		String type,
		IPCConfigReference reference,
		IMonitor monitor) {
			return new RfcDefaultIPCClient(mandt,type,reference,monitor);
//            return null;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(java.lang.String, java.lang.String, com.sap.spc.remote.client.object.IPCItemReference)
     */
    public IPCClient newIPCClient(
        String mandt,
        String type,
        IPCItemReference reference) {
			return new RfcDefaultIPCClient(mandt, type, reference);
//            return null;
    }
    
    public DefaultCharacteristic newCharacteristic(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String languageDependentName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String description) {
        return new RfcDefaultCharacteristic(
            configurationChangeStream,
            ipcClient,
            instance,
            groupName,
            languageDependentName,
            name,
            unit,
            entryFieldMask,
            typeLength,
            numberScale,
            valueType,
            allowsAdditionalValues,
            visible,
            allowsMultipleValues,
            required,
            expanded,
            consistent,
            isDomainConstrained,
            hasIntervalAsDomain,
            readonly,
            pricingRelevant,
            description,
            "", null, null, null, null);
    }

    public DefaultCharacteristic newCharacteristic(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String languageDependentName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String description,
        String applicationViews,
        String[] csticMimeLNames,
        String[] csticMimeObjectTypes,
        String[] csticMimeObjects,
        int[] index) {
        return new RfcDefaultCharacteristic(
            configurationChangeStream,
            ipcClient,
            instance,
            groupName,
            languageDependentName,
            name,
            unit,
            entryFieldMask,
            typeLength,
            numberScale,
            valueType,
            allowsAdditionalValues,
            visible,
            allowsMultipleValues,
            required,
            expanded,
            consistent,
            isDomainConstrained,
            hasIntervalAsDomain,
            readonly,
            pricingRelevant,
            description,
            applicationViews,
            csticMimeLNames,
            csticMimeObjectTypes,
            csticMimeObjects,
            index);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newInstance(com.sap.spc.remote.client.object.imp.ConfigurationChangeStream, com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.imp.DefaultConfiguration, java.lang.String, java.lang.String, boolean, boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, com.sap.spc.remote.client.object.MimeObjectContainer)
     */
    public DefaultInstance newInstance(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        DefaultConfiguration configuration,
        String instanceId,
        String parentId,
        boolean complete,
        boolean consistent,
        String quantity,
        String name,
        String languageDependentName,
        String description,
        String bomPosition,
        String price,
        boolean userOwned,
        boolean configurable,
        boolean specializable,
        boolean unspecializable,
        MimeObjectContainer mimeObjects) {
        return new RfcDefaultInstance(
            configurationChangeStream,
            ipcClient,
            configuration,
            instanceId,
            parentId,
            complete,
            consistent,
            quantity,
            name,
            languageDependentName,
            description,
            bomPosition,
            price,
            userOwned,
            configurable,
            specializable,
            unspecializable,
            mimeObjects);
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfigValue()
     */
    public ConfigValue newConfigValue() {
        return new RfcConfigValue();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfigValue(com.sap.sce.kbrt.ext_configuration)
     */
    public ConfigValue newConfigValue(ext_configuration config) {
        return new RfcConfigValue(config);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfiguration(com.sap.spc.remote.client.object.imp.DefaultIPCSession, java.lang.String)
     */
    public DefaultConfiguration newConfiguration(DefaultIPCSession session, String configId) throws IPCException {
        return new RfcDefaultConfiguration(session, configId);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfiguration(com.sap.spc.remote.client.object.IPCClient, java.lang.String)
     */
    public DefaultConfiguration newConfiguration(IPCClient ipcClient, String id) {
        return new RfcDefaultConfiguration(ipcClient, id);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConfigurationChangeStream(com.sap.spc.remote.client.object.Configuration, com.sap.spc.remote.client.object.imp.CachingClient)
     */
    public ConfigurationChangeStream newConfigurationChangeStream(
        Configuration configuration,
        IClient cachingClient) {
        return new RfcConfigurationChangeStream(configuration, cachingClient);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newExplanationTool(com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.Configuration)
     */
    public DefaultExplanationTool newExplanationTool(
        IPCClient ipcClient,
        Configuration configuration) {
        return new RfcDefaultExplanationTool(ipcClient, configuration);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newConflictParticipant(com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.Conflict, java.lang.String, java.lang.String, boolean, boolean, java.util.Locale, org.apache.struts.util.MessageResources)
     */
    public DefaultConflictParticipant newConflictParticipant(IPCClient ipcClient, Conflict conflict, String id, String factKey, boolean causedByUser, boolean causedByDefault, Locale locale, MessageResources resources) {
        return new RfcDefaultConflictParticipant(ipcClient, conflict, id, factKey, causedByUser, causedByDefault, locale, resources);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newKBProfile(java.lang.String, com.sap.spc.remote.client.object.KnowledgeBase)
     */
    public DefaultKBProfile newKBProfile(String name, KnowledgeBase kb) {
        return new RfcDefaultKBProfile(name, kb);
    }
	public  DefaultIPCDocumentProperties newIPCDocumentProperties()
	 {
		return new RFCDefaultIPCDocumentProperties();
		//return (DefaultIPCDocumentProperties)getObject(DefaultIPCDocumentProperties.class, new Object[] {});
	}
    
	/**
	* Creates a new IPCItemProperties object as clone of initialData.
	* @param initialData
	* @return
	* @throws IPCException
	*/
	public  DefaultIPCDocumentProperties newIPCDocumentProperties(IPCDocumentProperties initialData)
	 {
		return new RFCDefaultIPCDocumentProperties(initialData);
		//return (DefaultIPCDocumentProperties)getObject(DefaultIPCDocumentProperties.class, new Object[] {initialData});
	}
    
    public DefaultCharacteristicValue newCharacteristicValue(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        int[] index,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects) {
        return new RfcDefaultCharacteristicValue(
            configurationChangeStream,
            ipcClient,
            characteristic,
            name,
            languageDependentName,
            conditionKey,
            price,
            index,
            mimeValueLNames,
            mimeObjectTypes,
            mimeObjects);
    }

    public DefaultCharacteristicValue newCharacteristicValue(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        int[] index,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects,
        boolean defaultValue) {
        return new RfcDefaultCharacteristicValue(
            configurationChangeStream,
            ipcClient,
            characteristic,
            name,
            languageDependentName,
            conditionKey,
            price,
            index,
            mimeValueLNames,
            mimeObjectTypes,
            mimeObjects,
            defaultValue);
    }

    public DefaultCharacteristicValue newCharacteristicValue( 
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        boolean isAdditionalValue) {
        return new RfcDefaultCharacteristicValue(
            configurationChangeStream,
            ipcClient,
            characteristic,
            name,
            languageDependentName,
            conditionKey,
            price,
            isAdditionalValue);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCConfigReference()
     */
    public IPCConfigReference newIPCConfigReference() {
        return new RFCIPCConfigReference();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCItemReference()
     */
    public IPCItemReference newIPCItemReference() {
        return new RFCIPCItemReference();
    }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.IPCClient, com.sap.isa.core.eai.BackendBusinessObject)
	 */
	public DefaultIPCSession newIPCSession(IPCClient ipcClient, BackendBusinessObject rfcClientSupport) {
		return new RfcDefaultIPCSession(ipcClient, rfcClientSupport);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.IPCClient, com.sap.isa.core.eai.BackendBusinessObject, com.sap.util.monitor.jarm.IMonitor)
	 */
	public DefaultIPCSession newIPCSession(IPCClient client, BackendBusinessObject backendObject, IMonitor monitor) {
		return new RfcDefaultIPCSession(client, backendObject);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, com.sap.spc.remote.client.object.IPCItemReference, boolean)
	 */
	public DefaultIPCSession newIPCSession(DefaultIPCClient ipcClient, IPCItemReference reference, boolean isExtendedData) throws IPCException {
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.DefaultIPCClient, com.sap.spc.remote.client.object.IPCItemReference)
	 */
	public DefaultIPCSession newIPCSession(DefaultIPCClient ipcClient, IPCItemReference reference) throws IPCException {
		return new RfcDefaultIPCSession(ipcClient, reference);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(com.sap.isa.core.eai.BackendBusinessObject, com.sap.util.monitor.jarm.IMonitor, java.lang.String)
	 */
	public IPCClient newIPCClient(
		BackendBusinessObject rfcClientSupport,
		IMonitor _monitor,
		String tokenId,
		String configId) {
		return new RfcDefaultIPCClient(rfcClientSupport, _monitor, tokenId, configId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCClient(com.sap.isa.core.eai.BackendBusinessObject, java.lang.String)
	 */
	public IPCClient newIPCClient(
		BackendBusinessObject rfcClientSupport,
		String tokenId, 
		String configId) {
		return new RfcDefaultIPCClient(rfcClientSupport, tokenId, configId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCClient, com.sap.isa.core.eai.BackendBusinessObject, java.lang.String)
	 */
	public DefaultIPCSession newIPCSession(RfcDefaultIPCClient client, BackendBusinessObject rfcClientSupport, String tokenId, String configId) {
		return new RfcDefaultIPCSession(client, rfcClientSupport, tokenId, configId);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newIPCSession(com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCClient, com.sap.isa.core.eai.BackendBusinessObject, com.sap.util.monitor.jarm.IMonitor, java.lang.String)
	 */
	public DefaultIPCSession newIPCSession(RfcDefaultIPCClient client, BackendBusinessObject rfcClientSupport, IMonitor monitor, String tokenId, String configId) {
		return new RfcDefaultIPCSession(client, rfcClientSupport, tokenId, configId);
	}

    public DefaultProductVariant newProductVariant(IPCClient ipcClient,
        String id,
        double distance,
        String matchPoints,
        String maxMatchPoints,
        List features,
        ProductVariantFeature mainDifference) {
        return new RfcDefaultProductVariant(ipcClient, id, distance, matchPoints, maxMatchPoints, features, mainDifference);
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newLoadingStatus(com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.Configuration)
     */
    public DefaultLoadingStatus newLoadingStatus(IPCClient client, Configuration config) {
        return new RfcDefaultLoadingStatus(client, config);
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newTTEDocument(com.sap.spc.remote.client.object.imp.DefaultIPCSession, com.sap.spc.remote.client.object.IPCDocument, boolean)
     */
    public DefaultTTEDocument newTTEDocument(
        DefaultIPCSession session,
        IPCDocument document,
        boolean createDocumentOnServer) {
        return new RfcDefaultTTEDocument(session, document, createDocumentOnServer);
    }

    public DefaultTTEItem newTTEItem(  String itemId,  
                            IPCItemProperties itemProps, 
                            IPCDocumentProperties docProps, 
                            TTEDocument tteDoc, 
                            boolean createItemOnServer) {
 
        return new RfcDefaultTTEItem(itemId, itemProps, docProps, tteDoc, createItemOnServer);
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.IPCClientObjectFactory#newPricingConverter(com.sap.spc.remote.client.object.imp.DefaultIPCSession)
     */
    public DefaultPricingConverter newPricingConverter(DefaultIPCSession session) {
        return new RfcDefaultPricingConverter(session);
    }

}
