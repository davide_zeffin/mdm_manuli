package com.sap.spc.remote.client.object.imp;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCException;


/**
 * This class holds a table of values. It can be easily initialized from a number of
 * IPC parameter names and values (that should - but not need to - have the same length),
 * and provides convenient access methods.
 */
public class TableValue {

	protected Hashtable _values;
	protected String[]  _keys;

	protected IClient _client;
	protected String        _command;
	protected String[]      _params;

	/**
	 * Constructs a new, empty table for given keys. Currently, there's not much
	 * point in doing this since there is no addRow method.
	 */
    public TableValue(String[] columnIds) {
		_keys = columnIds;
		_values = new Hashtable();
		for (int i=0; i<columnIds.length; i++) {
			_values.put(columnIds[i],/*new LazyValue()*/new String[0]);
		}
    }


//VENU:Not required. Used in LazyValue concept
//	public void init(IClient client, String command, String[] params) {
//		for (Enumeration e=_values.keys(); e.hasMoreElements();) {
//			String key = (String)e.nextElement();
//			/*LazyValue*/String value = (/*LazyValue*/String)_values.get(key);
//			//value.init(client, command, params, key);
//		}
//	}


	/**
	 * Initializes a TableValue from given column ids and a vector of size columnIds that
	 * holds a Value per column. This constructor is typically called the following way:
	 * <pre>
	 * String[] keys = a number of IPC command parameters that identify a table;
	 * Vector data = new Vector(keys.length);
	 * for (int i=0; i<keys.length; i++) {
	 *   data.addElement(response.getValue(keys[i]));
	 * }
	 * new TableValue(keys,data);
	 * </pre>
	 * @param columnIds the ids of the column
	 * @param data Vector of OpenValue, each element holds a single column.
	 */
	public void setData(Vector data) throws IPCException {
		for (int i=0; i<_keys.length; i++) {
			/*Value*/ String[] columnData = (/*Value*/ String[])data.elementAt(i);
//			/*LazyValue*/ String value = (/*LazyValue*/ String)_values.get(_keys[i]);
//			value.setInitialValue(columnData.getContent());
			_values.put(_keys[i],columnData);
		}
	}


	/**
	 * Converts the values of this table to a parameter list that can be fed into IPC commands.
	 */
	public String[] toParameterList(boolean allowNullParameters) throws IPCException {
		Vector result = new Vector();
		for (Enumeration e=_values.keys(); e.hasMoreElements();) {
			String key = (String)e.nextElement();
			String[] values = ((/*Value*/ String[])_values.get(key))/*.getContent()*/;
			if (values != null || allowNullParameters) {
				if (values == null) {
				    result.addElement(key);
				    result.addElement(null);
				}
				else {
					for (int i=0; i<values.length; i++) {
						result.addElement(key);
						result.addElement(values[i]);
					}
				}
			}
		}
		String[] resultArray = new String[result.size()];
		result.copyInto(resultArray);
		return resultArray;
	}


	/**
	 * Returns the row-th row of this table in the order of the value keys.
	 */
	public String[] getRow(int row) throws IPCException {
		String[] data = new String[_keys.length];
		for (int i=0; i<_keys.length; i++) {
			String[] value = ((/*Value*/ String[])_values.get(_keys[i]))/*.getContent()*/;
			data[i] = value[row];
		}
		return data;
	}


	/**
	 * Returns a dictionary of the row-th row where the keys are the column names,
	 * and the values are Strings (the content of the given row/column)
	 */
	public Dictionary getRowDictionary(int row) throws IPCException {
		Hashtable result = new Hashtable();
		for (int i=0; i<_keys.length; i++) {
			String[] value = ((/*Value*/ String[])_values.get(_keys[i]))/*.getContent()*/;
			result.put(_keys[i],value[row]);
		}
		return result;
	}


	/**
	 * Returns the value at the specified row and column.
	 */
	public String getElement(int column, int row) throws IPCException {
		String[] value = ((/*Value*/ String[])_values.get(_keys[column]))/*.getContent()*/;
		return value[row];
	}


	/**
	 * Returns the value at the named column and at the row-th row.
	 */
	public String getElement(String key, int row) throws IPCException {
		String[] value = ((/*Value*/ String[])_values.get(key))/*.getContent()*/;
		return value[row];
	}


	/**
	 * Returns the number of rows in this table. May throw an IPC exception
	 * if the values in this table have not yet been resolved, the server is called now,
	 * and the communication with it fails.
	 */
	public int rows() throws IPCException {
		/*Value*/ String[] theValue = (/*Value*/ String[])_values.get(_keys[0]);
		if (theValue == null)
			return 0;

		String[] value = theValue/*.getContent()*/;
		if (value == null)
			return 0;

		return value.length;
	}

	/**
	 * Returns the number of columns in this table.
	 */
	public int getColumnCount(){
		if (_keys != null) return _keys.length;
		return 0;
	}

	/**
	 * Returns the number of columns in this table.
	 */
	public String[] getColumns(){
		return _keys;
	}
	
//	VENU:Not required. Used in LazyValue concept
//	public static void initTableValue(IClient client, String command, String[] params,
//									  TableValue[] values) {
//		for (int i=0; i<values.length; i++) {
//			values[i].init(client, command, params);
//		}
//	}
}
