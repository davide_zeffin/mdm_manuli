/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.IPCMessage;

/**
 * Message occured while executing commands on the IPC.
 * Messages will be collected in the IPCMessageSet
 */
public class DefaultIPCMessage implements IPCMessage{

	protected String   _message = null;
	protected String   _messageType = null;
	protected String   _objectId = null;

	DefaultIPCMessage(String objectId, String messageType, String message){
		_message = message;
		_messageType = messageType;
		_objectId = objectId;
	}

    public String getMessage(){
		return _message;
	}

    public String getMessageType(){
		return _messageType;
	}

    public String getObjectId(){
		return _objectId;
	}

}