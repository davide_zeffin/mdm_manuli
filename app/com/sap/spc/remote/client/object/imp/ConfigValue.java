package com.sap.spc.remote.client.object.imp;

import java.util.Vector;

import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.shared.ParameterSet;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;


/**
 * A Value that encapsulates an external configuration.
 */
public abstract class ConfigValue {

	protected TableValue _cfg;
	protected TableValue _ins;
	protected TableValue _prt;
	protected TableValue _val;
	protected TableValue _prc;

	protected IClient     _client;
	protected ext_configuration _config;
	// logging
	private static final Category category =
				ResourceAccessor.category;
	private static final Location location =
		ResourceAccessor.getLocation(ConfigValue.class);

	public void setInitialValue(ext_configuration config) {
        location.entering("setInitialValue(ext_configuration config)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            if (config != null ){
	            location.debugT("ext_configuration config" + config.get_name());
			}
            
        }
		_config = config;
        location.exiting();
	}

    public void setInitialValue(IClient client){
        location.entering("setInitialValue(IClient client)");
            if (location.beDebug()) {
                location.debugT("Parameters:");
                if (client!=null){
                    location.debugT("IClient client " + client.toString());
                }
                else location.debugT("IClient client null");
            }
        _client = client;
        location.exiting();
    }

	public abstract ext_configuration getConfig(String client) throws IPCException;
   
	/**
	 * Converts an external configuration to IPC parameters to be fed into ChangeItemInfo or similar
	 * commands. If the configuration this ConfigValue holds has not been set by the user but
	 * retrieved by the IPC server, the method just returns null, signalling that there is no point
	 * in getting IPC parameters.
	 */
	public String[] getConfigIPCParameters() {
        location.entering("getConfigIPCParameters()");
        String[] configIPCParameters = getConfigIPCParameters(_config);
        if (location.beDebug()) {
            if (configIPCParameters != null){
                location.debugT("return configIPCParameters: ");
                for (int i=0; i<configIPCParameters.length; i++) {
                    location.debugT(  "items["+i+"] "+configIPCParameters[i].toString());
                }
            }           
        }
        location.exiting();
		return configIPCParameters;
	}

	/**
	 * Converts an external configuration to IPC parameters to be fed into ChangeItemInfo or similar
	 * commands. If the configuration this ConfigValue holds has not been set by the user but
	 * retrieved by the IPC server, the method just returns null, signalling that there is no point
	 * in getting IPC parameters.
	 */
	public abstract String[] getConfigIPCParameters(ext_configuration config);

	public abstract void initializeValues(
		DefaultIPCSession session,
		String documentId,
		String itemId);
		
	public abstract void initProperties(String documentId, String itemId);

	protected static class _VectorParameterSet implements ParameterSet {
		protected Vector _data;

		public _VectorParameterSet(Vector data) {
            location.entering("_VectorParameterSet(Vector data)");
            if (location.beDebug()) {
                if (data != null) {
                    location.debugT("Parameters:");
                    for (int i=0; i<data.size(); i++) {
                        location.debugT("Vector data["+i+"] " + data.elementAt(i).toString());
                    }
                }
            }
			_data = data;
            location.exiting();
		}

		public void setValue(String key, String value) {
            location.entering("setValue(String key, String value)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String key " + key);
                location.debugT("String value " + value);
            }
			_data.addElement(key);
			_data.addElement(value);
            location.exiting();
		}

		public void setValue(String key, int index, String value) {
            location.entering("setValue(String key, int index, String value)");
                if (location.beDebug()){
                    location.debugT("Parameters:");
                    location.debugT("String key " + key);
                    location.debugT("int index " + index);
                    location.debugT("String value " + value);
                }
			_data.addElement(key+"["+Integer.toString(index)+"]");
			_data.addElement(value);
            location.exiting();
		}
	}
	/**
	 * @param value
	 */
	public void set_cfg(Vector value) {
		_cfg.setData(value);
}	
	/**
	 * @param value
	 */
	public void set_ins(Vector value) {
		_ins.setData(value);
	}

	/**
	 * @param value
	 */
	public void set_prc(Vector value) {
		_prc.setData(value);
	}

	/**
	 * @param value
	 */
	public void set_prt(Vector value) {
		_prt.setData(value);
	}

	/**
	 * @param value
	 */
	public void set_val(Vector value) {
		_val.setData(value);
	}

	public TableValue get_cfg() {
		return _cfg;
	}

	public TableValue get_ins() {
		return _ins;
	}

	public TableValue get_prc() {
		return _prc;
	}

	public TableValue get_prt() {
		return _prt;
	}

	public TableValue get_val() {
		return _val;
	}

}