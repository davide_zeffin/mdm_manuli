package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.shared.ConfigIdManager;
import com.sap.tc.logging.Location;

/**
 * A condition for a command. The condition describes the context which can be
 * influenced by the command. A condition C implies another condition C' if
 * C' context is affected by C.
 */
public class Condition {

	protected static final String SPECIAL_CONFIG_DOCUMENT_KEY = "CONFIG/DOC/ID";
	protected static final String SPECIAL_CONFIG_ITEM_KEY     = "CONFIG/ITEM/ID";

	protected static final Location location = ResourceAccessor.getLocation(Condition.class);

	protected String    _command;
	protected HashMap _relevantParameters;

	public Condition(String command, HashMap params) {
		_command = command;
		_relevantParameters = new HashMap();
		String docKey = _findDocKey(params);
		if (docKey != null) {
			_relevantParameters.put(docKey, _getParameter(params,docKey));
			String itemKey = _findItemKey(params);
			if (itemKey != null) {
				_relevantParameters.put(itemKey, _getParameter(params,itemKey));
			}
		}
	}


	protected Object _getParameter(HashMap params, String key) {
		if (key.equals(SPECIAL_CONFIG_DOCUMENT_KEY) || key.equals(SPECIAL_CONFIG_ITEM_KEY))
			return params.get("configId");
		else
			return params.get(key);
	}


	HashMap getRelevantParameters() {
		return _relevantParameters;
	}


	/**
	 * Returns true if this condition affects the context of another condition.
	 * This method should rather have been called affects, but when it was implemented
	 * I thought that an implies relation would be correct.
	 */
	public boolean implies(Condition other) {
		// 20020808-kha: originally I thought that a more special setter would not imply a
		// more general getter. As it turns out, e.g. RemoveItems(doc, it1) implies
		// GetDocumentAndItemInfo!
		//
		// now: a condition A implies another condition B if it affects it.
		// A key of condition A that is not there in B means that B is affected (because B might be "more global" than A)
		// A key of A that is there with B affects B if the two values of the keys have a non-empty intersection.

        HashMap mine = _decodeSpecialKeys(_relevantParameters);
		HashMap theirs = _decodeSpecialKeys(other.getRelevantParameters());
		for (Iterator iter=mine.keySet().iterator(); iter.hasNext();) {
			String key = (String)iter.next();
			ArrayList othersData = (ArrayList)theirs.get(key);
			if (othersData == null) {
				// try alternative key
				String otherKey = _getAlternativeKey(key);
				if (otherKey != null) {
					othersData = (ArrayList)theirs.get(otherKey);
					if (othersData == null) {
						return true;
					}
				}
			}
			else {
				ArrayList myData = (ArrayList)mine.get(key);
				if (!_isVectorIntersecting(myData,othersData))
					return false;
			}
		}
		return true;
	}


	protected HashMap _decodeSpecialKeys(HashMap m) {
		if (!m.containsKey(SPECIAL_CONFIG_DOCUMENT_KEY) && !m.containsKey(SPECIAL_CONFIG_ITEM_KEY)) {
			return m;
		}
		else {
			HashMap decoded = (HashMap)m.clone();

			ArrayList data = (ArrayList)decoded.get(SPECIAL_CONFIG_DOCUMENT_KEY);
			if (data != null) {
				ArrayList result = new ArrayList();
				for (Iterator iter=data.iterator(); iter.hasNext();) {
					String value = (String)iter.next();
					String documentId = ConfigIdManager.getDocumentId(value);
					if (documentId != null) {
						result.add(documentId);
					}
					else {
						result.add(value);
					}
				}
				decoded.put("documentId",result);
			}
			data = (ArrayList)decoded.get(SPECIAL_CONFIG_ITEM_KEY);
			if (data != null) {
				ArrayList result = new ArrayList();
				for (Iterator iter=data.iterator(); iter.hasNext();) {
					String value = (String)iter.next();
					String itemId = ConfigIdManager.getItemId(value);
					if (itemId != null) {
						result.add(itemId);
					}
					else {
						result.add(value);
					}
				}
				decoded.put("itemId",result);
			}
			return decoded;
		}
	}


	protected ArrayList getParameter(String key) {
		return (ArrayList)_relevantParameters.get(key);
	}


	protected String _findDocKey(HashMap params) {
		if (params.containsKey("documentId"))
			return "documentId";
		else if (params.containsKey("documentIds"))
			return "documentIds";
		else if (params.containsKey("configId"))
			return SPECIAL_CONFIG_DOCUMENT_KEY;
		else
			return null;
	}

	protected String _findItemKey(HashMap params) {
		if (params.containsKey("itemId"))
			return "itemId";
		else if (params.containsKey("itemIds"))
			return "itemIds";
		else if (params.containsKey("configId"))
			return SPECIAL_CONFIG_ITEM_KEY;
		else
			return null;
	}


	/**
	 * Returns the alternative key for a key, or null, if there is none.
	 * documentId is an alternative key for documentIds and vice versa, the same
	 * for item ids.
	 */
	protected String _getAlternativeKey(String key) {
		if (key.startsWith("documentId")) {
			if (key.length() == 10) // "documentId".length()
				return "documentIds";
			else
				// something that starts with "documentId" but isn't the same
				return "documentId";
		}
		else if (key.startsWith("itemId")) {
			if (key.length() == 6)
				return "itemIds";
			else
				return "itemId";
		}
		else
			return null;
	}


	protected boolean _isVectorIntersecting(ArrayList v1, ArrayList v2) {
		Comparator c = new Comparator() {
			public int compare(Object o1, Object o2) {
				String s1 = (String)o1;
				String s2 = (String)o2;
				return s1.compareTo(s2);
			}
		};

		TreeSet sv1 = new TreeSet(c);
		sv1.addAll(v1);

		for (Iterator iter = v2.iterator(); iter.hasNext();) {
			String s = (String)iter.next();
			if (sv1.contains(s))
				return true;
		}

		return false;
	}


	public String toString() {
		StringBuffer result = new StringBuffer("Condition[");
		result.append(_command);
		result.append(", [");
		for (Iterator iter=_relevantParameters.keySet().iterator(); iter.hasNext();) {
			String val = (String)iter.next();
		    result.append(val);
			result.append("= [");
			ArrayList data = (ArrayList)_relevantParameters.get(val);
			for (Iterator inner=data.iterator(); inner.hasNext(); ) {
				String innerString = (String)inner.next();
				result.append(innerString);
				result.append(",");
			}
			result.append("]");
		}
		result.append("]");
		return result.toString();
	}
}
