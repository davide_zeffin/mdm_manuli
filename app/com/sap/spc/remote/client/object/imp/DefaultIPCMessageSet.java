/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCMessage;

/**
 * Container for messages occured while executing commands on the IPC.
 */
public class DefaultIPCMessageSet/* implements IPCMessageSet*/ {

	protected String[] /*Value*/    _objectIds = null;
	protected String[] /*Value*/    _messageTypes = null;
	protected String[] /*Value*/    _messages = null;

	public DefaultIPCMessageSet(String[] /*Value*/ objectIds, String[] /*Value*/  messageTypes, String[] /*Value*/  messages){
		_objectIds = objectIds;
		_messageTypes = messageTypes;
		_messages = messages;
	}

	public IPCMessage[] getMessages() throws IPCException{
		String[] messages = _messages/*.getContent()*/;
		String[] objectIds = _objectIds/*.getContent()*/;
		String[] messageTypes = _messageTypes/*.getContent()*/;
		if (messages == null)
			return new IPCMessage[0];
		IPCMessage[] ipcMessages = new IPCMessage[messages.length];
		for (int i = 0; i < messages.length; i++){
			ipcMessages[i] = new DefaultIPCMessage( objectIds[i],
													messageTypes[i],
													messages[i]);
		}
		return ipcMessages;
	}
}

