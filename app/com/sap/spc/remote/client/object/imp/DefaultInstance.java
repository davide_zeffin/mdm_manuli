package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.ClientException;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.cache.Cache;

/**
 * Default implementation of the instance interface.
 */
public abstract class DefaultInstance extends BusinessObjectBase implements Instance {
    protected IPCClientObjectFactory factory = IPCClientObjectFactory.getInstance();
    protected String parentId; 
    protected ConfigurationChangeStream configurationChangeStream;

    protected HashMap cstics = new HashMap();
    protected TreeMap groups;
    protected ArrayList orderedGroups;
    protected ArrayList orderedGroups_GeneralGroupFirst;
    protected ArrayList orderedGroups_GeneralGroupLast;
    protected ArrayList csticsList; //sequential list respecting the order

    protected DefaultConfiguration configuration;
    protected IPCDocument ipcDocument;
    protected IPCItem ipcItem;
    protected String instanceId;
    protected ArrayList childInstances = new ArrayList();
    protected boolean complete;
    protected boolean consistent;
    protected String quantity;
    protected String name;
    protected String languageDependentName;
    protected String description;
    protected String position;
    protected String price;
    protected boolean userOwned;
    protected boolean configurable;
    protected boolean specializable;
    protected boolean unspecializable;
    protected MimeObjectContainer mimeObjects;

    protected boolean closed;

    protected IPCClient ipcClient;
    protected IClient cachingClient;
    protected boolean assignableValuesOnly = false;

    // Member variables for the cache
    protected boolean cacheIsDirty;
    // to use in future version to determine when to rebuild the cache
    protected boolean dataCached = false;
    public HashMap groupCaches = null; // holds the groupCache object 

    protected static final Category category = ResourceAccessor.category;
    protected static final Location location = ResourceAccessor.getLocation(DefaultInstance.class);

    /**
     * The init method allows to reuse the same object.
     */
    public abstract void init(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        DefaultConfiguration configuration,
        String instanceId,
        String parentId,
        boolean complete,
        boolean consistent,
        String quantity,
        String name,
        String languageDependentName,
        String description,
        String bomPosition,
        String price,
        boolean userOwned,
        boolean configurable,
        boolean specializable,
        boolean unspecializable,
        MimeObjectContainer mimeObjects);
        
    public IPCClient getIpcClient() {
        location.entering("getIpcClient()");
        location.exiting();
        return this.ipcClient;
    }

    public IPCDocument getIpcDocument() {
        location.entering("getIpcDocument()");
        if (ipcDocument != null) {
            if (location.beDebug()) {
                location.debugT("return ipcDocument " + ipcDocument.getId());   
            }
            location.exiting();
            return ipcDocument;
        }
        else {
            String configId = configuration.getId();
            String documentId =
                com.sap.spc.remote.shared.ConfigIdManager.getDocumentId(
                    configId);
            if (documentId != null) {
                IPCDocument ipcDocument = ipcClient.getIPCDocument(documentId);
                if (location.beDebug()) {
                    location.debugT("return ipcDocument " + ipcDocument.getId());   
                }
                location.exiting();
                this.ipcDocument = ipcDocument;
                return ipcDocument;
            } else {
                if (location.beDebug()) {
                    location.debugT("return null");   
                }
                location.exiting();
                return null; // probably a session-wide config => no document
            }
        }
    }

    public IPCItem getIpcItem() {
        location.entering("getIpcItem()");        
        if (ipcItem != null) {
            if (location.beDebug()) {
                location.debugT("return ipcItem " + ipcItem.getItemId());   
            }  
            location.exiting();      
            return ipcItem;
        }
        else {
            String configId = configuration.getId();
            IPCDocument doc = getIpcDocument();
            if (doc != null) {
                String rootItemId =
                    com.sap.spc.remote.shared.ConfigIdManager.getItemId(
                        configId);
                try {
                    IPCItem ipcRootItem = doc.getItem(rootItemId);
                    if (ipcRootItem != null) {
                    	this.ipcItem = ipcRootItem.getItemFromInstanceId(this.getId());
                    	if (this.ipcItem == null) {
                    		location.errorT("No item found for instance id " + this.getId());
                    		location.exiting();
                    		return null;
                    	}
                    }
                    if (location.beDebug()) {
                        location.debugT("return ipcItem " + ipcItem.getItemId());   
                    }
                    location.exiting();                   
                    return this.ipcItem;
                } catch (IPCException e) {
                    category.logT(Severity.ERROR, location, e.toString());
                    if (location.beDebug()) {
                        location.debugT("return null");   
                    }   
                    location.exiting();                   
                    return null;
                }
            } else {
                if (location.beDebug()) {
                    location.debugT("return null");   
                } 
                location.exiting();
                return null;
            }
        }
    }

    /**
     * Returns the identifier of this instance
     */
    public String getId() {
        location.entering("getId()");
        if (location.beDebug()) {
            location.debugT("return instanceId " + instanceId);
        }
        location.exiting();
        return instanceId;
    }

    /**
     * Call this method to send all changes of this instance and all subobjects
     * to the server.
     */
    public void flush() {
        // Flush all characteristics of this instance
        location.entering("flush()");
        Iterator csticsIt = cstics.values().iterator();
        while (csticsIt.hasNext()) {
            ((Characteristic) csticsIt.next()).flush();
        }
        location.exiting();
    }

    /**
     * Returns a list of all children (decompositions)
     */
    public synchronized List getChildren() {
        location.entering("getChildren()");
        if (location.beDebug()) {
            if (childInstances!=null){
                location.debugT("return childInstances " + childInstances.toString());
            }
            else location.debugT("return childInstances null");
        }
        location.exiting();
        return childInstances;
    }

    public synchronized void addChild(Instance child) {
        location.entering("addChild(Instance child)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("Instance child  " + child.getId());
        }
        childInstances.add(child);
        location.exiting();
    }

    /**
     * Returns true, if one or more childs are present.
     */
    public synchronized boolean hasChildren() {
        location.entering("hasChildren()");
        List children = getChildren();
        if (location.beDebug()) {
            if (children == null) location.debugT("return false");
            else location.debugT("return (children.size() > 0) " + (children.size() > 0));
        }
        location.exiting();
        return ((children == null) ? false : (children.size() > 0));
    }

    /**
     * Returns the parent instance which this instance belongs to
     */
    public Instance getParent() {
        location.entering("getParent()");
        Instance instance = configuration.getInstance(parentId);
        if (location.beDebug()) {
            location.debugT("return instance " + instance.getId());
        }
        location.exiting();
        return instance;
    }
    
    public String getParentId() {
        location.entering("getParentId()");
        if (location.beDebug()) {
            location.debugT("return this.parentId " + this.parentId);
        }
        location.exiting();
        return this.parentId;
    }

    /**
     * Returns true if the instance is the root instance;
     * returns false otherwise
     */
    public boolean isRootInstance() {
        location.entering("isRootInstance()");
        if (this.parentId.equalsIgnoreCase("0")) {
            if (location.beDebug()) {
                location.debugT("return true " + " (" + instanceId+")");        
            }
            location.exiting();
            return true;
        }
        else {
            if (location.beDebug()) {
                location.debugT("return false" + " (" + instanceId+")");        
            }
            location.exiting();   
            return false;
        }
    }

    public void setMimeObjects(MimeObjectContainer mimeObjects) {
        location.entering("setMimeObjects(MimeObjectContainer mimeObjects)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            if (mimeObjects!=null){
                location.debugT("MimeObjectContainer mimeObjects " + mimeObjects.toString());
            }
            else location.debugT("MimeObjectContainer mimeObjects null");
        }
        this.mimeObjects = mimeObjects;
        location.exiting();
    }

    /**
     * Returns a list of mime objects
     */
    public MimeObjectContainer getMimeObjects() {
        location.entering("getMimeObjects()");
        if (location.beDebug()) {
            if (mimeObjects!=null){
                location.debugT("return mimeObjects " + mimeObjects.toString());
            }
            else location.debugT("return mimeObjects null");
        }
        location.exiting();
        return mimeObjects;
    }

    public void setComplete(boolean complete) {
        location.entering("setComplete(boolean complete)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean complete " + complete);
        }
        this.complete = complete;
        location.exiting();
    }

    /**
     * Returns true if this instance is complete.
     */
    public boolean isComplete() {
        location.entering("isComplete()");
        if (location.beDebug()) {
            location.debugT("return complete " + complete + " (" + instanceId+")");
        }
        location.exiting();
        return complete;
    }

    public void setConsistent(boolean consistent) {
        location.entering("setConsistent(boolean consistent)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean consistent " + consistent);
        }
        this.consistent = consistent;
        location.exiting();
    }

    /**
     * Returns true if this instance is consistent.
     */
    public boolean isConsistent() {
        location.entering("isConsistent()");
        if (location.beDebug()) {
            location.debugT("return consistent " + consistent + " (" + instanceId+")");
        }
        location.exiting();
        return consistent;
    }

    public void setQuantity(String quantity) {
        location.entering("setQuantity(String quantity)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("String quantity " + quantity);
        }
        this.quantity = quantity;
        location.exiting();
    }

    /**
     * Returns the instance quantity of this instance (relative to
     * its parent).
     */
    public String getQuantity() {
        location.entering("getQuantity()");
        if (location.beDebug()) {
            location.debugT("return quantity " + quantity + " (" + instanceId+")");
        }
        location.exiting();
        return quantity;
    }

    public void setName(String name) {
        this.name = name;
    }
    /**
     * Returns the language independent name of the instance.
     */
    public String getName() {
        location.entering("getName()");
        if (location.beDebug()) {
            location.debugT("return name " + name + " (" + instanceId+")");
        }
        location.exiting();
        return name;
    }

    public void setLanguageDependendName(String lname) {
        this.languageDependentName = lname;
    }

    /**
     * Returns the language dependent name of the instance.
     */
    public String getLanguageDependentName() {
        location.entering("getLanguageDependentName()");
        if (location.beDebug()) {
            location.debugT("return languageDependentName " + languageDependentName + " (" + instanceId+")");
        }
        location.exiting();
        return languageDependentName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns the long description of the instance.
     */
    public String getDescription() {
        location.entering("getDescription()");
        if (location.beDebug()) {
            location.debugT("return description " + description + " (" + instanceId+")");
        }
        location.exiting();
        return description;
    }

    public void close() {
        closed = true;
    }

    public boolean isClosed() {
        location.entering("isClosed()");
        if (location.beDebug()) {
            location.debugT("return closed " + closed + " (" + instanceId+")");
        }
        location.exiting();
        return closed;
    }

    public boolean isRelevant(String commandName) {
        // we don't use the cache
        location.entering("isRelevant)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("String commandName " + commandName);
            location.debugT("return true");
        }
        location.exiting();
        return true;
    }

    public void setBOMPosition(String Position) {
        this.position = Position;
    }

    /**
     * Returns the position of this instance in the Bill of Materials of
     * its parent, or null, if this is the root instance or a non-part
     * instance.
     */
    public String getBOMPosition() {
        location.entering("getBOMPosition()");
        if (location.beDebug()) {
            location.debugT("return position " + position + " (" + instanceId+")");
        }
        location.exiting();
        return position;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Returns the price of this instance.
     */
    public String getPrice() {
        location.entering("getPrice()");
        if (location.beDebug()) {
            location.debugT("return price " + price + " (" + instanceId+")");
        }
        location.exiting();
        return price;
    }

    /**
     * Call this method if you want to update the cache
     */
    protected abstract void synchronize() throws IPCException;
    
    /**
     * Call this method if you want to update the cache for a dedicated group only.
     */
    protected synchronized void synchronize(String group, boolean includeInvisibleCstics)
        throws IPCException {
        if (groupCaches != null) {
            GroupCache gCache = (GroupCache) groupCaches.get(group);
            if (gCache != null) {
                if (includeInvisibleCstics
                    && gCache.getInterest() < GroupCache.INTEREST_IN_ALL_CSTICS) {
                    gCache.setInterest(GroupCache.INTEREST_IN_ALL_CSTICS);
                } else if (!includeInvisibleCstics
                        && gCache.getInterest() < GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY) {
                    gCache.setInterest(GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY);
                }
            }
        }
        synchronize();
    }

	/**
	 * Call this method if you want to update the cache of all groups but with the possibility
	 * to control the display of invisible cstics.
	 */
	protected synchronized void synchronize(boolean includeInvisibleCstics)
		throws IPCException {
		if (groupCaches != null) {
			Iterator cacheIt = groupCaches.values().iterator();
			while (cacheIt.hasNext()) {
				GroupCache gCache = (GroupCache) cacheIt.next();			
				if (includeInvisibleCstics
					&& gCache.getInterest() < GroupCache.INTEREST_IN_ALL_CSTICS) {
					gCache.setInterest(GroupCache.INTEREST_IN_ALL_CSTICS);
				} else if (!includeInvisibleCstics
						&& gCache.getInterest() < GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY) {
					gCache.setInterest(GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY);
				}
			}
		}
		synchronize();
	}


    /**
     * Updates the cache for a single group
     * Note: This method doesn't need to be synchronized, because it is always used
     * by one synchronized method ("synchronize").
     */
    protected abstract void updateGroupCache(GroupCache gCache) throws ClientException;

	/**
	 * Updates the cache for all groups.
	 * Note: This method doesn't need to be synchronized, because it is always used
	 * by one synchronized method ("synchronize").
	 */
	protected abstract void updateGroupCaches(HashMap groupCaches) throws ClientException;
    
    /**
     * Returns the characteristics of this instance (please cf. the
     * documentation of com.sap.sce.front.base for the difference between this
     * kind of characteristics and SCE engine characteristics).
     */
    public List getCharacteristics() {
        location.entering("getCharacteristics");
        List characteristics = getCharacteristics(true);
        if (location.beDebug()) {
            if (characteristics != null) {
                location.debugT("return characteristics: ");
                for (int i = 0; i<characteristics.size(); i++) {
                    location.debugT(characteristics.get(i).toString()); 
                }
                location.debugT(" (" + instanceId+")");
            }
        }
        location.exiting();
        return characteristics;
    }

    /**
     * Returns the characteristics of this instance.If includeInvisible is
     * true, the result set will also contain invisible characteristics. This
     * is the same result set as method getCharacteristics() returns. If set to
     * false, the visible characteristics are returned only, which might be
     * an effort according to the perfromance - depending on the amount of
     * invisible characteristics.
     */
    public List getCharacteristics(boolean includeInvisible) {
        location.entering("getCharacteristics(boolean includeInvisible)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean includeInvisible" + includeInvisible);
        }
        synchronizeAllCstics(includeInvisible);
        //first remove NULL placeholder from csticsList
        List returnCstic = new Vector();
        if(csticsList != null){
			for (int i = 0; i < csticsList.size(); i++) {
				if (csticsList.get(i) != null) {
					Characteristic cstic = (Characteristic) csticsList.get(i);
					// now check visibility
					if (cstic.isVisible() || includeInvisible) {
						returnCstic.add(csticsList.get(i));
					}
				}
			}
        }
        if (location.beDebug()) {
            if (returnCstic != null) {
                location.debugT("return returnCstics: ");
                for (int i = 0; i<returnCstic.size(); i++) {
                    location.debugT(returnCstic.get(i).toString()); 
                }
                location.debugT(" (" + instanceId+")");
            }
        }
        location.exiting();
        return returnCstic;
    }

    /**
     * Returns the characteristics of this instance and the given application view.
     * If includeInvisible is true, the result set will also contain invisible 
     * characteristics. 
     * If "*" is passed as application view all cstics are returned (depending on
     * visibility).  
     * If " " (blank) is passed as application view no cstics are returned.
     */
    public List getCharacteristics(boolean includeInvisible, char applicationView) {
        location.entering("getCharacteristics(boolean includeInvisible, char applicationView)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean includeInvisible" + includeInvisible);
            location.debugT("char applicationView" + applicationView);
        }
        List csticsOfView = new ArrayList();
        List unfilteredCstics = getCharacteristics(includeInvisible);
        if (applicationView == '*'){
            // return all cstics
            if (location.beDebug()) {
                if (unfilteredCstics != null) {
                    location.debugT("return unfilteredCstics: ");
                    for (int i = 0; i<unfilteredCstics.size(); i++) {
                        location.debugT(unfilteredCstics.get(i).toString()); 
                    }
                    location.debugT(" (" + instanceId+")");
                }
            }            
            return unfilteredCstics;
        }
        if (applicationView == ' '){
            // return no cstics
            if (location.beDebug()) {
                location.debugT("return csticsOfView: null");
            }
            location.exiting();
            return csticsOfView;
        }
        Iterator unfilteredCsticsIt = unfilteredCstics.iterator();
        while (unfilteredCsticsIt.hasNext()) {
            Characteristic cstic = (Characteristic) unfilteredCsticsIt.next();
            if (cstic.isPartOfApplicationView(applicationView)) {
                csticsOfView.add(cstic);
            }
        }
        if (location.beDebug()) {
            if (csticsOfView != null) {
                location.debugT("return csticsOfView: ");
                for (int i = 0; i<csticsOfView.size(); i++) {
                    location.debugT(csticsOfView.get(i).toString()); 
                }
                location.debugT(" (" + instanceId+")");
            }
        }
        location.exiting();
        return csticsOfView;
    } 


    /**
     * Returns the characteristics of this instance. If includeInvisible is
     * true, the result set will also contain invisible characteristics. If
     * set to false, the visible characteristics are returned only.
     * If assignableValuesOnly is true, the result set will only contain the
     * assignable values of each characteristic. If assignableValuesOnly is set
     * to false all values of each characteristic will be returned. If the method
     * getCharacteristics() is used invisible characteristics and all values of
     * each characteristic will be returned.
     * Setting includeInvisible and assignableValuesOnly to true might be an
     * effort according to the perfromance - depending on the amount of invisible
     * characteristics and not assignable values.
     */
    public List getCharacteristics(
        boolean includeInvisible,
        boolean assignableValuesOnly) {
        location.entering("getCharacteristics(boolean includeInvisible, boolean assignableValuesOnly)");    
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean includeInvisible" + includeInvisible);
            location.debugT("boolean assignableValuesOnly" + assignableValuesOnly);
        } 
        this.setIsAssignableValuesOnly(assignableValuesOnly);
        List characteristics = getCharacteristics(includeInvisible);
        if (location.beDebug()) {
            if (characteristics != null) {
                location.debugT("return characteristics: ");
                for (int i = 0; i<characteristics.size(); i++) {
                    location.debugT(characteristics.get(i).toString()); 
                }
                location.debugT(" (" + instanceId+")");
            }
        }
        location.exiting();
        return characteristics;
    }

    protected void synchronizeAllCstics(boolean includeInvisible) {
        location.entering("synchronizeAllCstics(boolean includeInvisible)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean includeInvisible" + includeInvisible);
        }
        try {
            synchronize();
            // First call makes sure that the group cache has been initialized
            Iterator gCaches = groupCaches.values().iterator();
            while (gCaches.hasNext()) {
                GroupCache gCache = (GroupCache) gCaches.next();
                if (includeInvisible) {
                    gCache.setInterest(GroupCache.INTEREST_IN_ALL_CSTICS);
                } else {
                    gCache.setInterest(GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY);
                }
            }
            synchronize(); // This call reads in all the desired groups
        } catch (IPCException e) {
            category.logT(Severity.ERROR, location, e.toString());
        }
        location.exiting();
    }

    /**
     * Returns a single characteristic - identified by the language
     * independent characteristic name, null if not found
     */
    public Characteristic getCharacteristic(
        String csticName,
        boolean includeInvisable) {
        location.entering("getCharacteristic(String csticName, boolean includeInvisable)");
        synchronizeAllCstics(includeInvisable);
        Characteristic characteristic = (Characteristic) cstics.get(csticName);
        if (location.beDebug()) {
            location.debugT("return characteristic " + characteristic.getName());
        }
        return characteristic;
    }


    /**
     * Returns all characteristic groups (design groups) of this instance.
     * If no design groups are available, an array with a single dummy group
     * with null name will be returned.
     */
    public List getCharacteristicGroups() {
        location.entering("getCharacteristicGroups()");
        List characteristicGroups = getCharacteristicGroups(true, true);
        if (location.beDebug()) {
             if (characteristicGroups != null) {
                 location.debugT("return characteristics: ");
                 for (int i = 0; i<characteristicGroups.size(); i++) {
                     location.debugT(characteristicGroups.get(i).toString()); 
                 }
                 location.debugT(" (" + instanceId+")");
             }
        }
        location.exiting();
        return characteristicGroups;
    }

    /**
     * Returns all characteristic groups (design groups) of this instance.
     * If includeInvisible is set to true, the groups will contain invisible
     * cstics as well!
     */
    public List getCharacteristicGroups(
        boolean includeInvisible,
        boolean showGeneralTabFirst) {
        location.entering("getCharacteristicGroups(boolean includeInvisible, boolean showGeneralTabFirst)");    
        try {
            synchronize();
            if (showGeneralTabFirst) {
                if (location.beDebug()) {
                    if (orderedGroups_GeneralGroupFirst != null) {
                        location.debugT("return characteristics: ");
                        for (int i = 0; i<orderedGroups_GeneralGroupFirst.size(); i++) {
                            location.debugT(orderedGroups_GeneralGroupFirst.get(i).toString()); 
                        }
                        location.debugT(" (" + instanceId+")");
                    }
                }
                location.exiting();
                return orderedGroups_GeneralGroupFirst;
            } else {
                if (location.beDebug()) {
                    if (orderedGroups_GeneralGroupLast != null) {
                        location.debugT("return characteristics: ");
                        for (int i = 0; i<orderedGroups_GeneralGroupLast.size(); i++) {
                            location.debugT(orderedGroups_GeneralGroupLast.get(i).toString()); 
                        }
                        location.debugT(" (" + instanceId+")");
                    }
                }
                location.exiting();
                return orderedGroups_GeneralGroupLast;
            }
        } catch (IPCException e) {
            category.logT(Severity.ERROR, location, e.toString());
        }
        return new ArrayList();
    }

    /**
     * Returns the characteristic group, specified by the groupName,
     * null if not found.
     */
    public CharacteristicGroup getCharacteristicGroup(String groupName) {
        try {
            location.entering("getCharacteristicGroup(String groupName)");
            if (location.beDebug()) {
                location.debugT("Parameters:");
                location.debugT("String groupName" + groupName);
            }
            synchronize(groupName, false);
            CharacteristicGroup characteristicGroup = (CharacteristicGroup) groups.get(groupName);
            if (location.beDebug()) {
                location.debugT("return characteristicGroup " + characteristicGroup.getName());
            }
            location.exiting();            
            return characteristicGroup;
        } catch (IPCException e) {
            category.logT(Severity.ERROR, location, e.toString());
        }
        return null;
    }

    public void setIsUserOwned(boolean isUserOwned) {
        location.entering("setIsUserOwned(boolean isUserOwned)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean isUserOwned" + isUserOwned);
        }
        this.userOwned = isUserOwned;
        location.exiting();
    }
    /**
     * Returns true, if the author of this instance is the user (which
     * is the case if it has been created by createInstance).
     */
    public boolean isUserOwned() {
        location.entering("isUserOwned()");
         if (location.beDebug()){
             location.debugT("return userOwned " + userOwned + " (" + instanceId+")");
         }
         location.exiting();
        return userOwned;
    }

    public boolean isConfigurable() {
        location.entering("isConfigurable()");
         if (location.beDebug()){
             location.debugT("return configurable " + configurable + " (" + instanceId+")");
         }
         location.exiting();
        return configurable;
    }

    /**
     * Specializes this instance to another type. Throws a IllegalConfigOperationException
     * if this Instance cannot be specialized to targetType.
     */
    public abstract void specialize(String specTypeId);
    
    /**
     * Unspecializes this instance. Calling this method will undo the last call of
     * specialize on this instance. If the instance has never been specialized,
     * a IllegalConfigOperationException will be thrown.
     */
    public abstract void unspecialize();
    
    /**
     * Creates a new child instance of this instance at a given BOM position,
     * or throws an IllegalConfigOperationException if the instance could not
     * be created.<p>
     */
    public abstract void createChildInstance(String decompPosition);
    
    /**
     * Returns the configuration to which this instance belongs.
     */
    public Configuration getConfiguration() {
        location.entering("getConfiguration()");
         if (location.beDebug()){
             location.debugT("return configuration " + configuration.toString() + " (" + instanceId+")");
         }
         location.exiting();
        return configuration;
    }

    public void setSpecializable(boolean specializable) {
        location.entering("setSpecializable(boolean specializable)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean specializable" + specializable);
        }
        this.specializable = specializable;
        location.exiting();
    }

    /**
     * Returns true if this instance can be specialized (further)
     */
    public boolean isSpecializable() {
        location.entering("isSpecializable()");
         if (location.beDebug()){
             location.debugT("return specializable " + specializable + " (" + instanceId+")");
         }
         location.exiting();
        return specializable;
    }

    public void setUnspecializable(boolean unspecializable) {
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean unspecializable" + unspecializable);
        }
        this.unspecializable = unspecializable;
        location.exiting();
    }

    /**
     * Returns true if this instance can be unspecialized
     */
    public boolean isUnspecializable() {
        location.entering("isUnspecializable()");
         if (location.beDebug()){
             location.debugT("return unspecializable " + unspecializable+ " (" + instanceId+")");
         }
         location.exiting();
        return unspecializable;
    }

    /**
     * Returns a list of product variants if the instance is
     * the root instances.
     * Returns an empty List otherwise.
     * Returns an empty List if no product variants were found
     */
    public abstract List getProductVariants(
        String limit,
        String quality,
        String maxMatchPoints);

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#applyProductVariant(java.lang.String)
     */
    public abstract boolean applyProductVariant(String productVariantId);
    
    protected String[] convertListToStringArray(List list) {
        String array[] = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = (String) list.get(i);
        }
        return array;
    }

    protected void setIsAssignableValuesOnly(boolean assignableValuesOnly) {
        this.assignableValuesOnly = assignableValuesOnly;
    }

    protected boolean isAssignableValuesOnly() {
        return this.assignableValuesOnly;
    }

    public void resetGroupCache() {
        this.groupCaches = null;
    }
    
    public String toString() {
    	StringBuffer result = new StringBuffer();
    	result.append("<INSTANCE");
		result.append(" instanceId=");
		result.append(instanceId);
		result.append(" name=");
		result.append(name);
		result.append("; languageDependentName:");
		result.append(languageDependentName);
		result.append("; description:");
		result.append(description);
    	result.append("; position:");
    	result.append(position);
    	result.append("; price");
    	result.append(price);
    	result.append("; quantity:");
    	result.append(quantity);
    	result.append("; specializable:");
    	result.append(specializable);
    	result.append("; unspecializable:");
    	result.append(unspecializable);
    	result.append("userOwned:");
    	result.append(userOwned);
    	result.append(">");
    	result.append("]</INSTANCE>");
    	
    	return result.toString();
    }
    
	/**
	 * @return
	 */
	public boolean isCacheIsDirty() {
		return cacheIsDirty;
	}

	/**
	 * @param b
	 */
	public void setCacheIsDirty(boolean b) {
		cacheIsDirty = b;
	}

	protected void addCharacteristic(
		int newInstPosition,
		DefaultCharacteristic cstic) {
		if (newInstPosition > csticsList.size()) {
			for (int j = csticsList.size(); j < newInstPosition; j++) {
				csticsList.add(null); //add container for predecessors
			}
		}
		((DefaultCharacteristic) cstic).setCharacteristicInstancePosition(
			newInstPosition);
		csticsList.add(newInstPosition, cstic);
	}

}
