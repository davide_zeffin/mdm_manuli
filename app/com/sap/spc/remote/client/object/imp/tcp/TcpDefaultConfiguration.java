package com.sap.spc.remote.client.object.imp.tcp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.ConflictInfo;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.InstanceDelta;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.ValueDelta;
import com.sap.spc.remote.client.object.imp.DefaultComparisonResult;
import com.sap.spc.remote.client.object.imp.DefaultConfiguration;
import com.sap.spc.remote.client.object.imp.DefaultConflictInfo;
import com.sap.spc.remote.client.object.imp.DefaultDelta;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultInstance;
import com.sap.spc.remote.client.object.imp.DefaultInstanceDelta;
import com.sap.spc.remote.client.object.imp.DefaultMimeObjectContainer;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.tcp.ExternalConfigConverter;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.shared.ParameterSet;
import com.sap.spc.remote.shared.SimpleRequestWrapper;
import com.sap.spc.remote.shared.command.DeleteInstance;
import com.sap.spc.remote.shared.command.GetConfigInfo;
import com.sap.spc.remote.shared.command.GetConflicts;
import com.sap.spc.remote.shared.command.GetInstances;
import com.sap.spc.remote.shared.command.ICompareConfigurations;
import com.sap.spc.remote.shared.command.SCECommand;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;


public class TcpDefaultConfiguration extends DefaultConfiguration implements Configuration { 
	class FieldValues {
		String[] refIdx;
		String[] fldName;
		String[] val1;
		String[] val2;
		FieldValues(ServerResponse r)
		{
			refIdx = r.getParameterValues(ICompareConfigurations.INST_FLD_REF_INDEX);
			fldName = r.getParameterValues(ICompareConfigurations.FIELD_NAME);
			val1 = r.getParameterValues(ICompareConfigurations.VALUE_1);
			val2 = r.getParameterValues(ICompareConfigurations.VALUE_2);
		}
	}
	class ValueValues {
		String[] refIdx;
		String[] charc;
		String[] value;
		String[] author;
		String[] type;
		ValueValues(ServerResponse r)
		{
			refIdx = r.getParameterValues(ICompareConfigurations.INST_VAL_REF_INDEX);
			charc = r.getParameterValues(ICompareConfigurations.CHARC);
			value = r.getParameterValues(ICompareConfigurations.VALUE);
			author = r.getParameterValues(ICompareConfigurations.VALUE_AUTHOR);
			type = r.getParameterValues(ICompareConfigurations.VALUE_CHANGE_FLAG);
		}
	}
     class ArrayListParameterSet implements ParameterSet {
    	Map container = new HashMap();
    	
		public void setValue(String key, String value)
		{
			ArrayList values = (ArrayList)container.get(key);
			if( values == null)
			{
				values = new ArrayList(10);
				container.put(key,values);
			}
			values.add(value);
		}
		public void setValue(String key, int index, String value)
		{
			String aKey = key + index;
			ArrayList values = (ArrayList)container.get(aKey);
			if( values == null)
			{
				values = new ArrayList(10);
				container.put(aKey,values);
			}
			values.add(value);			
		}
		String[] getParameters()
		{
			Collection vals = container.values();
			Iterator iter = vals.iterator();
			int size = 0;
			while(iter.hasNext())
				{
					size += ((ArrayList)iter.next()).size();
				}
			String result[] = new String[ size * 2];
			int i=0;
			Set keys = container.keySet();
			iter = keys.iterator();
			while(iter.hasNext())
				{
					String key = (String)iter.next();
					ArrayList v = (ArrayList)container.get(key);
					for(int k=0; k < v.size();k++)
					{
						result[i++]= key;
						result[i++]= (String)v.get(k);
					}
				}
			return result;
		}
    }
	// logging
	private static final Category category =
				ResourceAccessor.category;
	private static final Location location =
		ResourceAccessor.getLocation(TcpDefaultConfiguration.class);

    protected TcpDefaultConfiguration(IPCClient ipcClient, String id) {
        this.ipcClient = ipcClient;
        this.id = id;

        IPCSession session = ipcClient.getIPCSession();
        if (!(session instanceof DefaultIPCSession)) {
            throw new IllegalClassException(session, DefaultIPCSession.class);
        }
        this.cachingClient = ((DefaultIPCSession) session).getClient();

        closed = false;
        changetime = -1;
        configurationChangeStream = factory.newConfigurationChangeStream(this, cachingClient);
        initMembers();
    }

    protected TcpDefaultConfiguration(IPCSession session, String id) {
        this.id = id;
        this.ipcClient = session.getIPCClient();
        this.cachingClient = ((DefaultIPCSession) session).getClient();

        closed = false;
        changetime = -1;
        configurationChangeStream = factory.newConfigurationChangeStream(this, cachingClient);
        initMembers();
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultConfiguration#initMembers()
     */
    protected void initMembers() {
        try {
            ServerResponse r =
			((TCPDefaultClient)cachingClient).doCmd(
			ISPCCommandSet.GET_CONFIG_INFO,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        this.id,
                        GetConfigInfo.GET_CONTEXT,
                        "Y" });

            String sceModeString = r.getParameterValue(GetConfigInfo.SCE_MODE);
            if (sceModeString.equals(EMPTY)) {
                sceMode = Boolean.FALSE;
            }
            else {
                sceMode = Boolean.TRUE;
            }

            String completeString = r.getParameterValue(GetConfigInfo.IS_COMPLETE);
            if (completeString.equals(TRUE)) {
                complete = Boolean.TRUE;
            }
            else {
                complete = Boolean.FALSE;
            }

            String consistentString = r.getParameterValue(GetConfigInfo.IS_CONSISTENT);
            if (consistentString.equals(TRUE)) {
                consistent = Boolean.TRUE;
            }
            else {
                consistent = Boolean.FALSE;
            }
                
            String singleLevelString = r.getParameterValue(GetConfigInfo.IS_SINGLELEVEL);
            if (singleLevelString.equals(TRUE)) {
                singleLevel = Boolean.TRUE;
            }
            else {
                singleLevel = Boolean.FALSE;
            }

            this.knowledgeBase =
                factory.newKnowledgeBase(
                    this.ipcClient,
                    r.getParameterValue(GetConfigInfo.KB_LOGSYS),
                    r.getParameterValue(GetConfigInfo.KB_NAME),
                    r.getParameterValue(GetConfigInfo.KB_VERSION),
                    r.getParameterValue(GetConfigInfo.KB_BUILD_NUMBER));

            this.activeKBProfile = knowledgeBase.createProfile(r.getParameterValue(GetConfigInfo.KB_PROFILE));

            contextNames = r.getParameterValues(GetConfigInfo.CONTEXT_NAMES);
            contextValues = r.getParameterValues(GetConfigInfo.CONTEXT_VALUES);

            isAvailable = true;

        } catch (ClientException e) {
            isAvailable = false;
            category.logT(Severity.ERROR, location, e.toString());
        }
    }

	protected void synchronizeInstances() {
		if (instanceCacheIsDirty || !instanceDataCached) {
			location.debugT("Update required: instanceCacheIsDirty: "+ cacheIsDirty + "; instanceDataCached: "+ dataCached);
            try {
                ServerResponse r =
				((TCPDefaultClient)cachingClient).doCmd(
				ISPCCommandSet.GET_INSTANCES,
                        new String[] {
                            SCECommand.CONFIG_ID,
                            this.id,
                            GetInstances.GET_MIME_OBJECTS,
                            GetInstances.YES });
                String[] instName = r.getParameterValues(GetInstances.INST_NAME);
                String[] instLName = r.getParameterValues(GetInstances.INST_LNAME);
                String[] instId = r.getParameterValues(GetInstances.INST_ID);
                String[] instPId = r.getParameterValues(GetInstances.INST_PID);
                String[] instUnspecable = r.getParameterValues(GetInstances.INST_UNSPECALBE);
                String[] instPrice = r.getParameterValues(GetInstances.INST_PRICE);
                String[] instAuthor = r.getParameterValues(GetInstances.INST_AUTHOR);
                String[] instConflict = r.getParameterValues(GetInstances.INST_CONFLICT);
                String[] instComplete = r.getParameterValues(GetInstances.INST_COMPLETE);
                String[] instImage = r.getParameterValues(GetInstances.INST_IMAGE);
                String[] instDescription = r.getParameterValues(GetInstances.INST_DESCRIPTION);
                String[] instQuantity = r.getParameterValues(GetInstances.INST_QUANTITY);
                String[] instPosition = r.getParameterValues(GetInstances.INST_POSITION);

                String[] mimeInstIds = r.getParameterValues(GetInstances.MIME_INST_ID);
                String[] mimeObjectTypes = r.getParameterValues(GetInstances.MIME_OBJECT_TYPES);
                String[] mimeObjects = r.getParameterValues(GetInstances.MIME_OBJECTS);

                TreeMap previousInstances = m_Instances;
                m_Instances = new TreeMap();
                m_RootInstance = null; // No root instance found yet

                // Now generate the mime objects and place them into a hashtable
                String prevInstance = "";
                DefaultMimeObjectContainer mimes = null;
                HashMap instToMimeIndexHash = new HashMap();
                if (mimeInstIds != null) {
                    for (int i = 0; i < mimeInstIds.length; i++) {
                        if (!prevInstance.equals(mimeInstIds[i])) {
                            // Next Instance's mime objects?
                            mimes = factory.newMimeObjectContainer();
                            instToMimeIndexHash.put(mimeInstIds[i], mimes);
                            prevInstance = mimeInstIds[i];
                        }
                        MimeObject mimeObject =
                            factory.newMimeObject(
                                mimeObjects[i],
                                mimeObjectTypes[i]);
                        mimes.add(mimeObject);
                    }
                }
                instanceList = new Vector(instId.length);

                // Iterate over all instances and generate a business object for each of them
                for (int i = 0; i < instName.length; i++) {
                    DefaultInstance instance = null;
                    if (previousInstances != null) {
                        instance = (DefaultInstance) previousInstances.get(instId[i]);
                    }
                    if (instance == null) {
                        instance =
                            factory.newInstance(
                                configurationChangeStream,
                                ipcClient,
                                this,
                                instId[i],
                                instPId[i],
                                instComplete[i].equals(GetInstances.YES),
                                instConflict[i].equals(GetInstances.NO),
                                instQuantity[i],
                                instName[i],
                                instLName[i],
                                instDescription[i],
                                instPosition[i],
                                instPrice[i],
                                instAuthor[i].equals(GetInstances.USER),
                                true,
                                instUnspecable[i].equals(GetInstances.YES),
                                instUnspecable[i].equals(GetInstances.YES),
                                (MimeObjectContainer) instToMimeIndexHash.get(
                                    instId[i]));
                    } else {
                        instance.init(
                            configurationChangeStream,
                            ipcClient,
                            this,
                            instId[i],
                            instPId[i],
                            instComplete[i].equals(GetInstances.YES),
                            instConflict[i].equals(GetInstances.NO),
                            instQuantity[i],
                            instName[i],
                            instLName[i],
                            instDescription[i],
                            instPosition[i],
                            instPrice[i],
                            instAuthor[i].equals(GetInstances.USER),
                            true,
                            instUnspecable[i].equals(GetInstances.YES),
                            instUnspecable[i].equals(GetInstances.YES),
                            (MimeObjectContainer) instToMimeIndexHash.get(
                                instId[i]));
                    }
                    if (instPId[i].equals("0")) {
                        instanceList.add(instance);
                        m_Instances.put(instId[i], instance);
                        m_RootInstance = instance;
                    } else if (!instPId[i].equals("-1")) {
                        instanceList.add(instance);
                        m_Instances.put(instId[i], instance);
                        ((DefaultInstance) m_Instances.get(instPId[i])).addChild(instance);
                    } else {
                        //non part instances, currently ignored
                    }
                }
            } catch (ClientException e) {
                category.logT(Severity.ERROR, location, e.toString());
            }
			instanceCacheIsDirty = false;
			instanceDataCached = true;
		}    	
	}
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultConfiguration#synchronize()
     */
    protected synchronized void synchronize() {
        if (cacheIsDirty || !dataCached) {
            initMembers();
            cacheIsDirty = false;
            dataCached = true;
        }
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#toExternal()
     */
    public ext_configuration toExternal() {
        try {
            ServerResponse r =
			((TCPDefaultClient)cachingClient).doCmd(
			ISPCCommandSet.GET_CONFIG_ITEM_INFO,
                    new String[] { "configId", getId()});

            SimpleRequestWrapper s_request = new SimpleRequestWrapper(r);
            ext_configuration ext_config = ExternalConfigConverter.createConfig(
                                                s_request,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);

            if (ext_config != null) {
                return ext_config;
            } else {
                throw new java.lang.NullPointerException(
                    "Error while creating external configuration");
            }
        } catch (Exception e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new java.lang.NullPointerException(
                "Error while creating external configuration");
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getExternalConfiguration()
     */
    public HashMap getExternalConfiguration() {
        HashMap data = new HashMap();
        try {
            ServerResponse r =
			((TCPDefaultClient)cachingClient).doCmd(
			ISPCCommandSet.GET_CONFIG_ITEM_INFO,
                    new String[] { SCECommand.CONFIG_ID, this.id });

            String nextParam = null;
            String value = null;
            String key = null;
            for (Enumeration e = r.getParametersEnum(); e.hasMoreElements();) {
                nextParam = (String) e.nextElement();
                /*if (nextParam.startsWith("CFG-")
                    || nextParam.startsWith("INS-")
                    || nextParam.startsWith("PRT-")
                    || nextParam.startsWith("VAL-")
                    || nextParam.startsWith("VK-")
                    || nextParam.startsWith("CONTEXT-")) {*/
                if (nextParam.startsWith("extConfig")
                    || nextParam.startsWith("extInst")
                    || nextParam.startsWith("extPart")
                    || nextParam.startsWith("extValue")
                    || nextParam.startsWith("extPrice")
                    || nextParam.startsWith("sceContext")) {

                    String values[] = r.getParameterValues(nextParam);
                    for (int i = 0; i < values.length; i++) {
                        value = values[i];
                        if (value == null) {
                            value = "";
                        }

                        key = nextParam + "[" + (i + 1) + "]";
                        data.put(key, value);
                    }
                }
            }
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }

        return data;            
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#reset()
     */
    public void reset() throws IPCException {
        cacheIsDirty = true;
		instanceCacheIsDirty = true;
        try {
            ServerResponse r =
			((TCPDefaultClient)cachingClient).doCmd(
			ISPCCommandSet.RESET_CONFIG,
                    new String[] { SCECommand.CONFIG_ID, this.id });
			// set IPCItems dirty otherwise the reset configuration will not be loaded
			if (getInstances() != null) {
				Instance currentInstance = null; 
				for (Iterator instIter = getInstances().iterator(); instIter.hasNext();) {
					currentInstance = (Instance) instIter.next();
					DefaultIPCItem ipcItem = (DefaultIPCItem)currentInstance.getIpcItem();
					if (ipcItem != null) {
						ipcItem.setCacheDirty(); //flush() sends some SET or DELETE commands, hence make Item cache also dirty.
					}
				}
			}
            synchronize();
        } catch (ClientException e) {
            throw new IPCException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#deleteInstance(com.sap.spc.remote.client.object.Instance)
     */
    public synchronized boolean deleteInstance(Instance instance) {
        cacheIsDirty = true;
		instanceCacheIsDirty = true;
        String instId = instance.getId();
        boolean success = false;
        try {
            ServerResponse r =
			((TCPDefaultClient)cachingClient).doCmd(
			ISPCCommandSet.DELETE_INSTANCE,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        id,
                        DeleteInstance.INST_ID,
                        instId });
            // update the config's change time
            setChangeTime();

            success = r.getParameterValue(DeleteInstance.SUCCESS).equals("Y");
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            return false;
        }

        initMembers();
        return success;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getConflictInfos()
     */
    public List getConflictInfos() {
        List explanations = null;
        if (!isConsistent()) {
            try {
                ServerResponse r =
				((TCPDefaultClient)cachingClient).doCmd(
				ISPCCommandSet.GET_CONFLICTS,
                        new String[] { SCECommand.CONFIG_ID, this.id });
                String[] conflictExplanations =
                    r.getParameterValues(GetConflicts.CONFLICT_LONG_TEXT);
                String[] conflictIds =
                    r.getParameterValues(GetConflicts.CONFLICT_ID);
                String[] conflictInstanceIds =
                    r.getParameterValues(GetConflicts.CONFLICT_INST_ID);
                String[] conflictInstanceNames =
                    r.getParameterValues(GetConflicts.CONFLICT_INST_LNAME);
                String[] conflictTexts =
                    r.getParameterValues(GetConflicts.CONFLICT_TEXT);

                explanations = new Vector(conflictIds.length);

                for (int i = 0; i < conflictIds.length; ++i) {
                    String explanation;
                    String documentation;
                    if (!conflictExplanations[i].equals("")
                        && (conflictExplanations[i].indexOf(ConflictInfo.EXPL) > -1)
                        && (conflictExplanations[i].indexOf(ConflictInfo.DOCU) > -1)) {
                        explanation =
                            conflictExplanations[i].substring(
                                conflictExplanations[i].indexOf(ConflictInfo.EXPL)
                                    + ConflictInfo.EXPL.length()
                                    + 1,
                                conflictExplanations[i].indexOf(ConflictInfo.DOCU)
                                    - 1);
                        documentation =
                            conflictExplanations[i].substring(
                                conflictExplanations[i].indexOf(ConflictInfo.DOCU)
                                    + ConflictInfo.DOCU.length()
                                    + 1,
                                conflictExplanations[i].length() - 1);
                    } else {
                        explanation = conflictTexts[i];
                        documentation = conflictTexts[i];
                    }
                    String instanceName =
                        conflictInstanceNames[i].substring(
                            conflictInstanceNames[i].indexOf(ConflictInfo.INSTANCE_$)
                                + ConflictInfo.INSTANCE_$.length());

                    DefaultConflictInfo conflictInfo =
                        new DefaultConflictInfo(
                            conflictIds[i],
                            conflictInstanceIds[i],
                            instanceName,
                            conflictTexts[i],
                            explanation,
                            documentation);
                    explanations.add(conflictInfo);
                }
            } catch (ClientException ipce) {
                category.logT(Severity.ERROR, location, ipce.toString());
                throw new IPCException(ipce);
            }
        }
        return explanations;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getInstanceNames()
     */
    public String[] getInstanceNames() {
        String[] instName;
        try {        
            String configId = this.id;
            String getMimeObjects = FALSE;
            
            ServerResponse r =
            ((TCPDefaultClient)cachingClient).doCmd(
            ISPCCommandSet.GET_INSTANCES,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        configId,
                        GetInstances.GET_MIME_OBJECTS,
                        getMimeObjects });
            // we are only interested in the instance names
            instName = r.getParameterValues(GetInstances.INST_NAME);
            
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        if (instName == null){
            instName = new String[0];
        }
        return instName;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getInitialConfiguration()
     */
    public ConfigurationSnapshot getInitialConfiguration() {
        // TODO adaptation to missing IPC command
        // set import parameters -> configId
        // call FM GetInitialConfiguration
        // get export parameters
        return null;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#compareConfigurations(com.sap.spc.remote.client.util.ext_configuration, com.sap.spc.remote.client.util.ext_configuration)
     */
    public ComparisonResult compareConfigurations(
        ext_configuration extConfig1,
        ext_configuration extConfig2) {
			List errorList = new ArrayList();
			DefaultComparisonResult result = null;
			try {        


				
        
				if (extConfig1 == null || extConfig2 == null){
					errorList.add(EXT_CONFIGS_NULL);
				}
				else {
						
						
						/*
						 * @param   extCfg  Vector of ext_configuration
						 * @param   instIdToItemIdTables Vector of (Hashtable mapping String instids to String itemIds). May be null if no item mapping is needed
						 * @param   posExInt initial config position number (eg. 1)
						 * @param   cfgId initial config id (eg. 1)
						 * @param   something implementing ParameterSet that will be updated by this method.
						 * @param   useIndices attach a numeric index [<number>] to each parameter name. If you want to use the result
						 */
					    ArrayListParameterSet param= new ArrayListParameterSet();
						Vector  extCfgs = new Vector();
						extCfgs.add( extConfig1);
						extCfgs.add( extConfig2);
						ExternalConfigConverter.getConfigs(extCfgs,null,1,1,param, false );							
					ServerResponse r =
					((TCPDefaultClient)cachingClient).doCmd(
					ISPCCommandSet.COMPARE_CONFIGURATIONS,
							param.getParameters());
					FieldValues fldVals= new FieldValues(r);		
					List headerDeltas = generateHeaderDeltas(fldVals);
					List instanceDeltas = generateInstanceDeltas(r);
					if( headerDeltas != null || instanceDeltas != null)
					{	
						if( headerDeltas == null )
							headerDeltas = new ArrayList();				
						if( instanceDeltas == null )
							instanceDeltas = new ArrayList();				
						result = (DefaultComparisonResult)factory.newComparisonResult(headerDeltas, instanceDeltas);
					}
				}

				if (errorList.size()>0){
					// errors during comparison occured, create message(s)
					location.debugT("There have been errors during configuration.");
				}
				location.exiting();
            
				 // we are only interested in the instance names
            
			 } catch (ClientException e) {
					location.debugT("Error in FM call CFG_API_COMPARE_CONFIGURATIONS");
					errorList.add(ERROR_IN_FM);
		}

		if( result == null)
			result = (DefaultComparisonResult)factory.newComparisonResult(null, null);
		result.setComparisonErrors(errorList);
		result.setConfigOne(extConfig1);
		result.setConfigTwo(extConfig2);
        return result;
    }
	List generateHeaderDeltas(FieldValues fldValues)
	{
		if( fldValues.refIdx != null && fldValues.fldName != null && fldValues.val1 != null && fldValues.val2 != null )
		{
			return generateFieldDeltas( ICompareConfigurations.DELTA_HEADER_PREFIX, fldValues );
		}
		return null;
	}
	List generateFieldDeltas(String index, FieldValues fldValues) {
		int size = 0;
		if (fldValues.refIdx == null)
			return new ArrayList();
		for (int k = 0; k < fldValues.refIdx.length; k++)
			if (fldValues.refIdx[k].equals(index))
				size++;
		List result = new ArrayList(size);

		for (int k = 0; k < fldValues.refIdx.length; k++)
			if (fldValues.refIdx[k].equals(index)) {
				DefaultDelta delta =
					factory.newDelta(
						fldValues.fldName[k],
						fldValues.val1[k],
						fldValues.val2[k]);
				result.add(delta);
			}
		return result;
	}
	List generateValueDeltas(
		String index,
		String instId,
		ValueValues fldValues) {
		int size = 0;
		if (fldValues.refIdx == null)
			return new ArrayList();
		for (int k = 0; k < fldValues.refIdx.length; k++)
			if (fldValues.refIdx[k].equals(index))
				size++;
		List result = new ArrayList(size);

		for (int k = 0; k < fldValues.refIdx.length; k++)
			if (fldValues.refIdx[k].equals(index)) {
				StringBuffer configObjKey = new StringBuffer(instId);
				configObjKey.append(".");
				configObjKey.append(fldValues.charc[k]);
				ValueDelta delta =
					(ValueDelta) factory.newValueDelta(
						configObjKey.toString(),
						fldValues.charc[k],
						fldValues.value[k],
						null,
						fldValues.author[k],
						fldValues.type[k]);
				result.add(delta);
			}
		return result;
	}
    List generateInstanceDeltas(ServerResponse r)
    {
		ArrayList instDeltas = null;
		String[] deltaInstIdx= r.getParameterValues(ICompareConfigurations.DELTA_INST_IDX);
		String[] instId1     = r.getParameterValues(ICompareConfigurations.INST_ID_1);
		String[] instId2     = r.getParameterValues(ICompareConfigurations.INST_ID_2);
		String[] decompPath  = r.getParameterValues(ICompareConfigurations.DECOMP_PATH);
		String[] instAuthor  = r.getParameterValues(ICompareConfigurations.INST_AUTHOR);
		String[] changeFlag  = r.getParameterValues(ICompareConfigurations.CHANGE_FLAG);
		ValueValues valueTable = new ValueValues(r);
		FieldValues fieldTable = new FieldValues(r);
		if( instId1 == null && instId2 == null && 
			decompPath == null && instAuthor == null &&
			changeFlag == null )
			{
				// No differences returned.
			}
		else if( instId1 != null && instId2 != null && 
			decompPath != null && instAuthor != null &&
			changeFlag != null )
			{
				instDeltas = new ArrayList(instId1.length);
				for( int k = 0; k < instId1.length;k++)
				{
					List partOfDeltas = null;
					List valueDeltas = null; 
					List deltas = null;
					
					if( changeFlag[k].equals(InstanceDelta.UPDATED))
					{
//						Parts not implemented yet
						partOfDeltas = new ArrayList();
						valueDeltas = generateValueDeltas(deltaInstIdx[k],instId1[k],valueTable);
						deltas = generateFieldDeltas(deltaInstIdx[k],fieldTable);
					}
					DefaultInstanceDelta instDelta = factory.newInstanceDelta(instId1[k],instId2[k],null,decompPath[k],deltas,partOfDeltas,valueDeltas,instAuthor[k],changeFlag[k]);
					instDeltas.add(instDelta);
				}
				// No differences returned.
			}
		return instDeltas;
    }

}
