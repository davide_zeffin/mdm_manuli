package com.sap.spc.remote.client.object.imp.rfc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.TTEBusinessPartner;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.spc.remote.client.object.TTEItem;
import com.sap.spc.remote.client.object.TTEItemBusinessPartner;
import com.sap.spc.remote.client.object.TTEProduct;
import com.sap.spc.remote.client.object.TTEProductTaxClassification;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultTTEDocument;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.ParameterList;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.rfc.function.ApTteCalculateTaxes;
import com.sap.spc.remote.client.rfc.function.ApTteGetDocumentTable;
import com.sap.spc.remote.client.rfc.function.ApTteGetDocumentXml;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;


public class RfcDefaultTTEDocument extends DefaultTTEDocument implements TTEDocument {

    private static final Category category = ResourceAccessor.category;
    private static final Location location = ResourceAccessor.getLocation(RfcDefaultTTEDocument.class);
    
    public static final RfcDefaultTTEDocument C_NULL = new RfcDefaultTTEDocument();
    
    /**
     * @param document IPCDocument
     */
    protected RfcDefaultTTEDocument(DefaultIPCSession session, IPCDocument document, boolean createDocumentOnServer) {
        super(session, document);
        if (createDocumentOnServer){
            createDocument();
        }
    }

    protected RfcDefaultTTEDocument(){
        super();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultTTEDocument#createDocument()
     */
    protected void createDocument() throws IPCException {
        try {
            JCO.Function function = getTTEFunction();
            JCO.ParameterList im = function.getImportParameterList();            
    
            fillDocumentHeaderParams(im, ApTteCalculateTaxes.DOC_MODIFY);
            fillDocumentBPsParams(im);

            JCO.ParameterList ex = callTTEFunction(function);
        }
        catch(ClientException e) {
            throw new IPCException(e);
        }
    }
    
    /**
     * Consolidated location where the function is called. The import parameters have to be 
     * filled before!
     * @param function JCO.Function AP_TTE_CALCULATE_TAXES
     * @throws ClientException
     * @return Export Parameterlist
     */
    private JCO.ParameterList callTTEFunction(JCO.Function function) throws ClientException {
        category.logT(Severity.PATH, location, "executing RFC AP_TTE_CALCULATE_TAXES");
        ((RFCDefaultClient)session.getClient()).execute(function); // does not define ABAP Exceptions
        category.logT(Severity.PATH, location, "done with RFC AP_TTE_CALCULATE_TAXES");
        
        JCO.ParameterList ex = function.getExportParameterList();                   
        BigDecimal returncode = ex.getBigDecimal(ApTteCalculateTaxes.RETURNCODE);
        if (returncode.intValue() != 0){
            category.logT(Severity.ERROR, location, "Error in AP-TTE: AP_TTE_CALCULATE_TAXES returns " + returncode.toString() + " as returncode.");
        }
        return ex;
    }
    
    /**
     * Helper method.
     * Fills the import paramters for the document business partners. 
     * @param im Import Parameter list to be filled.
     */
    private void fillDocumentBPsParams(JCO.ParameterList im) {
        // process table imApttecalculatetaxes_itPartner
        // optional, TABLE, Document business partner
        JCO.Table imApttecalculatetaxes_itPartner = im.getTable(ApTteCalculateTaxes.IT_PARTNER);
        Iterator bps = businessPartners.iterator();
        while (bps.hasNext()) {
            TTEBusinessPartner bp = (TTEBusinessPartner)bps.next();
            imApttecalculatetaxes_itPartner.appendRow();
            imApttecalculatetaxes_itPartner.setValue(bp.getBusinessPartnerId(), ApTteCalculateTaxes.BUSPARTNERID);       // CHAR, Business Partner ID
            imApttecalculatetaxes_itPartner.setValue(bp.getCountry(), ApTteCalculateTaxes.COUNTRYISO);       // CHAR, Country
            imApttecalculatetaxes_itPartner.setValue(bp.getRegion(), ApTteCalculateTaxes.REGION);       // CHAR, Region
            imApttecalculatetaxes_itPartner.setValue(bp.getCounty(), ApTteCalculateTaxes.COUNTY);       // CHAR, County
            imApttecalculatetaxes_itPartner.setValue(bp.getCity(), ApTteCalculateTaxes.CITY);       // CHAR, City
            imApttecalculatetaxes_itPartner.setValue(bp.getPostalCode(), ApTteCalculateTaxes.POSTALCODE);       // CHAR, Postal Code
            imApttecalculatetaxes_itPartner.setValue(bp.getGeoCode(), ApTteCalculateTaxes.GEOCODE);     // CHAR, Geo Code
            imApttecalculatetaxes_itPartner.setValue(bp.getJurisdictionCode(), ApTteCalculateTaxes.JURISDICTIONCODE);       // CHAR, Jurisdiction Code
            imApttecalculatetaxes_itPartner.setValue(bp.getExemptedRegion(), ApTteCalculateTaxes.EXEMPTEDREGION);       // CHAR, ExemptedRegion
            imApttecalculatetaxes_itPartner.setValue(bp.getBelongsToLegEnt(), ApTteCalculateTaxes.BELONGSTOLEGENT);     // NUM, Belongs To Legal Entity
            imApttecalculatetaxes_itPartner.setValue(bp.getTaxability(), ApTteCalculateTaxes.BPTAXABILITY);       // NUM, Taxability
        
            // process table imApttecalculatetaxes_itPataxcl
            // optional, TABLE, Document business partner tax class.
            JCO.Table imApttecalculatetaxes_itPataxcl = im.getTable(ApTteCalculateTaxes.IT_PATAXCL);
            ArrayList taxGroups = bp.getTaxGroups();
            ArrayList taxTypes = bp.getTaxTypes();
            for (int i=0; i<taxGroups.size(); i++){
                String taxGroup = taxGroups.get(i).toString();
                // don't add rows for empty groups
                if (taxGroup.equals("")){
                    continue;
                }
                imApttecalculatetaxes_itPataxcl.appendRow();
                imApttecalculatetaxes_itPataxcl.setValue(taxGroup, ApTteCalculateTaxes.BPTAXGROUP);       // CHAR, Business Partner Tax Group
                imApttecalculatetaxes_itPataxcl.setValue(bp.getBusinessPartnerId(), ApTteCalculateTaxes.BUSPARTNERID);       // CHAR, Business Partner ID
                imApttecalculatetaxes_itPataxcl.setValue(bp.getCountry(), ApTteCalculateTaxes.COUNTRYISO);       // CHAR, Country
                imApttecalculatetaxes_itPataxcl.setValue(bp.getRegion(), ApTteCalculateTaxes.REGION);       // CHAR, Region
                String taxType; 
                // avoid out-of-bounds exception
                if (i >= taxTypes.size()){
                    taxType = "";
                }
                else {
                    taxType = taxTypes.get(i).toString();
                }
                imApttecalculatetaxes_itPataxcl.setValue(taxType, ApTteCalculateTaxes.TAXTYP);       // CHAR, Tax Type
                
            }
            
            // process table imApttecalculatetaxes_itPataxnum
            // optional, TABLE, Document business partner tax number
            JCO.Table imApttecalculatetaxes_itPataxnum = im.getTable(ApTteCalculateTaxes.IT_PATAXNUM);
            ArrayList taxNumbers = bp.getTaxNumbers();
            ArrayList taxNumberTypes = bp.getTaxNumberTypes();
            for (int i=0; i<taxNumbers.size(); i++){
                String taxNumber = taxNumbers.get(i).toString();
                // don't add rows for empty groups
                if (taxNumber.equals("")){
                    continue;
                }                    
                imApttecalculatetaxes_itPataxnum.appendRow();
                imApttecalculatetaxes_itPataxnum.setValue(taxNumbers.get(i).toString(), ApTteCalculateTaxes.TAXNUMBER);        // CHAR, Tax Number
                imApttecalculatetaxes_itPataxnum.setValue(bp.getBusinessPartnerId(), ApTteCalculateTaxes.BUSPARTNERID);      // CHAR, Business Partner ID
                imApttecalculatetaxes_itPataxnum.setValue(bp.getCountry(), ApTteCalculateTaxes.COUNTRYISO);      // CHAR, Country
                String taxNumberType;
                // avoid out-of-bounds exception
                if (i >= taxNumberTypes.size()){
                    taxNumberType = "";
                }
                else {
                    taxNumberType = taxNumberTypes.get(i).toString();
                }
                imApttecalculatetaxes_itPataxnum.setValue(taxNumberType, ApTteCalculateTaxes.TAXNUMBERTYPE);        // CHAR, Tax Number Type
                
            }
        }
    }
    /**
     * Helper method.
     * Fills the import paramters for the document business partners. 
     * @param im Import Parameter list to be filled.
     */
    private void fillDocumentHeaderParams(JCO.ParameterList im, String documentCommand) {
        im.setValue(documentId, ApTteCalculateTaxes.TTEDOCUMENTID);      // CHAR, TTE Command
        
        // process table imApttecalculatetaxes_itHeader
        // TABLE, Document header
        JCO.Table imApttecalculatetaxes_itHeader = im.getTable(ApTteCalculateTaxes.IT_HEADER);
        imApttecalculatetaxes_itHeader.appendRow();
        imApttecalculatetaxes_itHeader.setValue(documentId, ApTteCalculateTaxes.TTEDOCUMENTID);      // CHAR, TTE Document ID
        imApttecalculatetaxes_itHeader.setValue(documentCommand, ApTteCalculateTaxes.COMMAND);      // NUM, TTE Command
        imApttecalculatetaxes_itHeader.setValue(processingMode, ApTteCalculateTaxes.PROCESSINGMODE);        // NUM, Processing Mode
        imApttecalculatetaxes_itHeader.setValue(traceMode, ApTteCalculateTaxes.TRACEMODE);      // NUM, TTE Trace Mode
        if (textOutput) {
            imApttecalculatetaxes_itHeader.setValue(ApTteCalculateTaxes.X_FLAG, ApTteCalculateTaxes.TEXTOUTPUT);        // CHAR, Text Output
        }
        imApttecalculatetaxes_itHeader.setValue(this.language, ApTteCalculateTaxes.LANGUAGE);        // CHAR, Language
        imApttecalculatetaxes_itHeader.setValue(systemId1, ApTteCalculateTaxes.SYSTEMID1);      // CHAR, System ID 1
        imApttecalculatetaxes_itHeader.setValue(ownBusinessPartnerId, ApTteCalculateTaxes.OWNBUSPARTID);        // CHAR, Business Partner ID
        imApttecalculatetaxes_itHeader.setValue(documentCurrencyUnit, ApTteCalculateTaxes.DOCUMENTCURRENCY);        // CHAR, Document Currency Unit
        imApttecalculatetaxes_itHeader.setValue(businessTransaction, ApTteCalculateTaxes.BUSTRANSACTION);        // CHAR, Business Transaction
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#modifyDocumentOnServer()
     */
    public void modifyDocumentOnServer() throws IPCException {
        try { 
            JCO.Function function = getTTEFunction();
            JCO.ParameterList im = function.getImportParameterList();            
    
            fillDocumentHeaderParams(im, ApTteCalculateTaxes.DOC_MODIFY);
            fillDocumentBPsParams(im);
            fillProductsParams(im);
            fillItemsParams(im);

            JCO.ParameterList ex = callTTEFunction(function);
        }
        catch(ClientException e) {
            throw new IPCException(e);
        }
    }

    /**
     * Fills the import parameters for the items..
     * @param im Import parameter list to be filled
     */
    private void fillItemsParams(ParameterList im) {
        // process table imApttecalculatetaxes_itItem
        // optional, TABLE, Input item
        JCO.Table imApttecalculatetaxes_itItem = im.getTable(ApTteCalculateTaxes.IT_ITEM);
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)session).getPricingConverter();
        
        for (int i=0; i<tteItems.size(); i++){
            TTEItem tteItem = (TTEItem) tteItems.get(i);
            imApttecalculatetaxes_itItem.appendRow();
            imApttecalculatetaxes_itItem.setValue(tteItem.getItemId(), ApTteCalculateTaxes.ITEMID);      // CHAR, Item Identifier
            imApttecalculatetaxes_itItem.setValue(ApTteCalculateTaxes.ITEM_MODIFY, ApTteCalculateTaxes.ITEMCOMMAND);        // NUM, TTE Command
            //imApttecalculatetaxes_itItem.setValue(tteItem.getRetItemId(), ApTteCalculateTaxes.REFITEMID);        // CHAR, Reference Item ID            
            imApttecalculatetaxes_itItem.setValue(tteItem.getPositionNumber(), ApTteCalculateTaxes.POSITIONNUMBER);      // CHAR, Position Number
            imApttecalculatetaxes_itItem.setValue(tteItem.getBusinessProcess(), ApTteCalculateTaxes.BUSPROCESSTYPE);      // CHAR, Business Process
            imApttecalculatetaxes_itItem.setValue(tteItem.getProductId(), ApTteCalculateTaxes.PRODUCTID);        // CHAR, Product ID
            imApttecalculatetaxes_itItem.setValue(tteItem.getQuantity(), ApTteCalculateTaxes.QUANTITY);      // CHAR, Quantity
            imApttecalculatetaxes_itItem.setValue(tteItem.getDirectionIndicator(), ApTteCalculateTaxes.DIRECTIONIND);      // NUM, Direction Indicator
            String unit = pc.convertUOMExternalToInternal(tteItem.getUnit(), language);
            imApttecalculatetaxes_itItem.setValue(unit, ApTteCalculateTaxes.UNITOFMEASURE);        // CHAR, Unit of Measurement
            imApttecalculatetaxes_itItem.setValue(tteItem.getIncoterms1(), ApTteCalculateTaxes.INCOTERMS1);      // CHAR, Incoterm
            imApttecalculatetaxes_itItem.setValue(tteItem.getIncoterms2(), ApTteCalculateTaxes.INCOTERMS2);      // CHAR, Incoterm Location
            ArrayList itemBPs = tteItem.getItemBusinessPartners();
            // process table imApttecalculatetaxes_itItempart
            // optional, TABLE, Document  item  partner
            JCO.Table imApttecalculatetaxes_itItempart = im.getTable(ApTteCalculateTaxes.IT_ITEMPART);
            for (int j=0; j<itemBPs.size(); j++) {
                TTEItemBusinessPartner itemBP = (TTEItemBusinessPartner) itemBPs.get(j);
                imApttecalculatetaxes_itItempart.appendRow();
                imApttecalculatetaxes_itItempart.setValue(tteItem.getItemId(), ApTteCalculateTaxes.ITEMID);      // CHAR, Item Identifier
                imApttecalculatetaxes_itItempart.setValue(itemBP.getRole(), ApTteCalculateTaxes.BPROLE);      // CHAR, Role
                imApttecalculatetaxes_itItempart.setValue(itemBP.getBusinessPartnerId(), ApTteCalculateTaxes.BUSPARTNERID);      // CHAR, Business Partner ID
                imApttecalculatetaxes_itItempart.setValue(itemBP.getCountry(), ApTteCalculateTaxes.COUNTRYISO);      // CHAR, Country
                imApttecalculatetaxes_itItempart.setValue(itemBP.getRegion(), ApTteCalculateTaxes.REGION);      // CHAR, Region
                imApttecalculatetaxes_itItempart.setValue(itemBP.getCounty(), ApTteCalculateTaxes.COUNTY);      // CHAR, County
                imApttecalculatetaxes_itItempart.setValue(itemBP.getCity(), ApTteCalculateTaxes.CITY);      // CHAR, City
                imApttecalculatetaxes_itItempart.setValue(itemBP.getPostalCode(), ApTteCalculateTaxes.POSTALCODE);      // CHAR, Postal Code
                imApttecalculatetaxes_itItempart.setValue(itemBP.getGeoCode(), ApTteCalculateTaxes.GEOCODE);        // CHAR, Geo Code
                imApttecalculatetaxes_itItempart.setValue(itemBP.getJurisdictionCode(), ApTteCalculateTaxes.JURISDICTIONCODE);      // CHAR, Jurisdiction Code
                imApttecalculatetaxes_itItempart.setValue(itemBP.getExemptedRegion(), ApTteCalculateTaxes.EXEMPTEDREGION);      // CHAR, ExemptedRegion
            }
        }
    }

    /**
     * Fills the import parameters for the product tables.
     * @param im Import parameter list to be filled
     */
    private void fillProductsParams(ParameterList im) {
        // process table imApttecalculatetaxes_itProduct
        // optional, TABLE, Document product
        JCO.Table imApttecalculatetaxes_itProduct = im.getTable(ApTteCalculateTaxes.IT_PRODUCT);

        for (int i=0; i<products.size(); i++){
            TTEProduct tteProduct = (TTEProduct)products.get(i);
            imApttecalculatetaxes_itProduct.appendRow();
            imApttecalculatetaxes_itProduct.setValue(tteProduct.getProductId(), ApTteCalculateTaxes.PRODUCTID);     // CHAR, Product ID
            imApttecalculatetaxes_itProduct.setValue(tteProduct.getTaxability(), ApTteCalculateTaxes.PRTAXABILITY);       // NUM, Product Taxability

            // process table imApttecalculatetaxes_itPrtaxcl
            // optional, TABLE, Document product tax class.
            JCO.Table imApttecalculatetaxes_itPrtaxcl = im.getTable(ApTteCalculateTaxes.IT_PRTAXCL);
            ArrayList taxClassifs = tteProduct.getTaxClassifications();
            for (int j=0; j<taxClassifs.size(); j++){
                TTEProductTaxClassification taxClassif = (TTEProductTaxClassification) taxClassifs.get(j);
                String taxGroup = taxClassif.getTaxGroup();
                // don't add rows for empty groups
                if (taxGroup.equals("")){
                    continue;
                }
                imApttecalculatetaxes_itPrtaxcl.appendRow();
                imApttecalculatetaxes_itPrtaxcl.setValue(tteProduct.getProductId(), ApTteCalculateTaxes.PRODUCTID);     // CHAR, Product ID
                imApttecalculatetaxes_itPrtaxcl.setValue(taxClassif.getCountry(), ApTteCalculateTaxes.COUNTRYISO);       // CHAR, Country
                imApttecalculatetaxes_itPrtaxcl.setValue(taxClassif.getRegion(), ApTteCalculateTaxes.REGION);       // CHAR, Region
                imApttecalculatetaxes_itPrtaxcl.setValue(taxClassif.getTaxType(), ApTteCalculateTaxes.TAXTYP);       // CHAR, Tax Type
                imApttecalculatetaxes_itPrtaxcl.setValue(taxGroup, ApTteCalculateTaxes.PRTAXGROUP);       // CHAR, Product Tax Group
            }
        }        
    }

    /**
     * Consolidated location where the function object of AP_TTE_CALCULATE_TAXES is created. 
     * This is needed to get always the same importParameters object in different methods. 
     * @return JCO.Function object of AP_TTE_CALCULATE_TAXES
     */
    protected JCO.Function getTTEFunction(){
        JCO.Function functionApTteCalculateTaxes = null;
        try {
            functionApTteCalculateTaxes = ((RFCDefaultClient)session.getClient()).getFunction(IFunctionGroup.AP_TTE_CALCULATE_TAXES);
        }
        catch (ClientException c){
            throw new IPCException(c);
        }
        return functionApTteCalculateTaxes;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultTTEDocument#removeItemsOnServer(com.sap.spc.remote.client.object.IPCItem[])
     */
    public void removeItemsOnServer(List ipcItems) throws IPCException {
        if (ipcItems.size() > 0){
            JCO.Function function = getTTEFunction();
            JCO.ParameterList im = function.getImportParameterList();                   
            fillDocumentHeaderParams(im, ApTteCalculateTaxes.DOC_MODIFY);
    
            // process table imApttecalculatetaxes_itItem
            // optional, TABLE, Input item
            JCO.Table imApttecalculatetaxes_itItem = im.getTable(ApTteCalculateTaxes.IT_ITEM);
            Iterator ipcItemsIter = ipcItems.iterator();
            while (ipcItemsIter.hasNext()){
                IPCItem ipcItem = (IPCItem)ipcItemsIter.next();
                TTEItem tteItem = ipcItem.getTteItem();
                imApttecalculatetaxes_itItem.appendRow();
                imApttecalculatetaxes_itItem.setValue(tteItem.getItemId(), ApTteCalculateTaxes.ITEMID);      // CHAR, Item Identifier
                imApttecalculatetaxes_itItem.setValue(ApTteCalculateTaxes.ITEM_REMOVE, ApTteCalculateTaxes.ITEMCOMMAND);        // NUM, TTE Command
                
                // remove it from the document
                removeItem(tteItem);
            }
            try {    
                JCO.ParameterList ex = callTTEFunction(function);
            }
            catch(ClientException e) {
                throw new IPCException(e);
            }
        }
    }
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getTTEAnalysisDataXml()
     */
    public String[] getTTEAnalysisDataXml() throws IPCException {
        String[] xmldocuments=new String[2];
        try {
            // implementation
            // get access to function
            JCO.Function functionApTteGetDocumentXml = ((RFCDefaultClient)session.getClient()).getFunction(IFunctionGroup.AP_TTE_GET_DOCUMENT_XML);
            JCO.ParameterList im = functionApTteGetDocumentXml.getImportParameterList();
            JCO.ParameterList ex = functionApTteGetDocumentXml.getExportParameterList();
            JCO.ParameterList tbl = functionApTteGetDocumentXml.getTableParameterList();

            // Backward compatibility:
            // The correct implementation of FM AP_TTE_GET_DOCUMENT_XML is only available as of AP 7.00 SP07. 
            // To avoid an exception on the UI we check whether the right version is available.
            // The field EV_I_XML_STRING is only available in the correct version.
            boolean isNewImplementation = ex.hasField(ApTteGetDocumentXml.EV_I_XML_STRING);
            if (!isNewImplementation){
                // The correct version (AP 7.0 SP07) is not available. Don't call the FM.
                category.logT(Severity.ERROR, location, "FM AP_TTE_GET_DOCUMENT_XML in not available! This feature is only available as of AP 7.0 SP07. Please update.");
                return xmldocuments;
            }
            // fill import parameters from internal members
            im.setValue(documentId, ApTteGetDocumentXml.TTEDOCUMENTID);      // CHAR, TTE Command

            category.logT(Severity.PATH, location, "executing RFC AP_TTE_GET_DOCUMENT_XML");
            ((RFCDefaultClient)session.getClient()).execute(functionApTteGetDocumentXml); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC AP_TTE_GET_DOCUMENT_XML");
            
            // fill internal members from export parameters
            
            String evIXmlString = ex.getString(ApTteGetDocumentXml.EV_I_XML_STRING);
            String evOXmlString = ex.getString(ApTteGetDocumentXml.EV_O_XML_STRING);
            BigDecimal returncode = ex.getBigDecimal(ApTteGetDocumentXml.RETURNCODE);
            xmldocuments[0] = evIXmlString;
            xmldocuments[1] = evOXmlString;
            
        }catch (ClientException e){
            throw new IPCException(e);
        }
        return xmldocuments;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#getTTEAnalysisData()
     */
    public String[] getTTEAnalysisData() throws IPCException {
        String[] trace;
        try {
            // implementation
            // get access to function
            JCO.Function functionApTteGetDocumentTable = ((RFCDefaultClient)session.getClient()).getFunction(IFunctionGroup.AP_TTE_GET_DOCUMENT_TABLE);
            JCO.ParameterList im = functionApTteGetDocumentTable.getImportParameterList();
            JCO.ParameterList ex = functionApTteGetDocumentTable.getExportParameterList();
            JCO.ParameterList tbl = functionApTteGetDocumentTable.getTableParameterList();
            // fill import parameters from internal members
            im.setValue(documentId, ApTteGetDocumentXml.TTEDOCUMENTID);      // CHAR, TTE Command

            category.logT(Severity.PATH, location, "executing RFC AP_TTE_GET_DOCUMENT_TABLE");
            ((RFCDefaultClient)session.getClient()).execute(functionApTteGetDocumentTable); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC AP_TTE_GET_DOCUMENT_TABLE");
            
            // process table exApttegetdocumenttable_otHeader
            // TABLE, Document header
            JCO.Table exApttegetdocumenttable_otHeader = ex.getTable(ApTteGetDocumentTable.OT_HEADER);
            for (int exApttegetdocumenttable_otHeaderCounter=0; exApttegetdocumenttable_otHeaderCounter<exApttegetdocumenttable_otHeader.getNumRows(); exApttegetdocumenttable_otHeaderCounter++) {
                exApttegetdocumenttable_otHeader.setRow(exApttegetdocumenttable_otHeaderCounter);
                String ttedocumentid = exApttegetdocumenttable_otHeader.getString(ApTteGetDocumentTable.TTEDOCUMENTID);
                String tteversion = exApttegetdocumenttable_otHeader.getString(ApTteGetDocumentTable.TTEVERSION);
                BigDecimal returncode = exApttegetdocumenttable_otHeader.getBigDecimal(ApTteGetDocumentTable.RETURNCODE);
                BigDecimal taxdatetype = exApttegetdocumenttable_otHeader.getBigDecimal(ApTteGetDocumentTable.TAXDATETYPE);
                Date accordingtodate = exApttegetdocumenttable_otHeader.getDate(ApTteGetDocumentTable.ACCORDINGTODATE);
            }
            
            // process table exApttegetdocumenttable_otTrace
            // TABLE, Output Trace
            JCO.Table exApttegetdocumenttable_otTrace = ex.getTable(ApTteGetDocumentTable.OT_TRACE);
            int numRows = exApttegetdocumenttable_otTrace.getNumRows();
            trace = new String[numRows];
            for (int i=0; i<numRows; i++) {
                exApttegetdocumenttable_otTrace.setRow(i);
                StringBuffer sb = new StringBuffer();
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.TRACEENTRYKEY));
                sb.append("|");
                BigDecimal errorCode = exApttegetdocumenttable_otTrace.getBigDecimal(ApTteGetDocumentTable.ERRCODE);
                sb.append(errorCode.toString()); 
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.STATUS));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.STATUSMESSAGE));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.LOCATION));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.TECHMESSAGE1));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.TECHMESSAGE2));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.TECHMESSAGE3));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.TECHMESSAGE4));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.TECHMESSAGE5));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.MESSAGECLASS));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.PLACEHOLDER1));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.PLACEHOLDER2));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.PLACEHOLDER3));
                sb.append("|");
                sb.append(exApttegetdocumenttable_otTrace.getString(ApTteGetDocumentTable.PLACEHOLDER4));
                trace[i] = sb.toString();
            }
            
            BigDecimal returncode = ex.getBigDecimal(ApTteGetDocumentTable.RETURNCODE);
        }catch (ClientException e){
            throw new IPCException(e);
        }
        return trace;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#changeDocument(java.util.HashMap)
     */
    public void changeDocument(HashMap documentHeaderAttributes) throws IPCException {
        changeAttributes(documentHeaderAttributes);
        // change the business partners of the items if necessary
        for (int i=0; i<tteItems.size(); i++){
            TTEItem currentTteItem = (TTEItem) tteItems.get(i);
            if (currentTteItem != null){
                currentTteItem.changeBusinessPartners(documentHeaderAttributes);
            }
        }
        modifyDocumentOnServer();        
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEDocument#changeDocumentCurrencyUnit(java.lang.String)
     */
    public void changeDocumentCurrencyUnit(String currencyUnit) throws IPCException {
        this.documentCurrencyUnit = currencyUnit;
        try {
            JCO.Function function = getTTEFunction();
            JCO.ParameterList im = function.getImportParameterList();            
    
            fillDocumentHeaderParams(im, ApTteCalculateTaxes.DOC_MODIFY);

            JCO.ParameterList ex = callTTEFunction(function);
        }
        catch(ClientException e) {
            throw new IPCException(e);
        }
    }

}
