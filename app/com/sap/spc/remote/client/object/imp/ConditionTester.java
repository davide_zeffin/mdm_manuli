/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.tc.logging.Location;


public class ConditionTester {
	protected String mandt;
	protected String type;
	protected String language;
	protected IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
	protected static final Location location = ResourceAccessor.getLocation(ConditionTester.class);

	public ConditionTester(String mandt, String type, String language) {
		this.mandt = mandt;
		this.type  = type;
		this.language  = language;
	}

	public static IPCItem createDummyItem(IPCSession session, String language){
		try {

            IPCClientObjectFactory factory = IPCClientObjectFactory.getInstance();
			IPCDocumentProperties props = factory.newIPCDocumentProperties();
			props.setCountry("");
			props.setLanguage(language);
			props.setSalesOrganisation("1000");
			props.setDistributionChannel("10");
			props.setDocumentCurrency("EUR");
			props.setPricingProcedure("RVAA01");
			props.setLocalCurrency("EUR");
			IPCDocument doc = session.newDocument(props);
			IPCItemProperties itemprops = factory.newIPCItemProperties();
			itemprops.setDate("20010816");
			itemprops.setProductId("R-1001");
			itemprops.setPerformPricingAnalysis(true);
            if (location.beDebug()) {
                location.debugT("return doc.newItem(itemprops) " + doc.newItem(itemprops).getItemId());
            }
            IPCItem item = doc.newItem(itemprops);

            return item;
		}
		catch(IPCException e) {
			System.err.println("Caught an IPC exception");
			e.printStackTrace();

			return null;
		}
      
	}

	public IPCSession createDummySession(){
		try {
			IPCSession session = factory.newIPCSession((DefaultIPCClient)factory.newIPCClient(mandt, type), mandt, type, null, (IPCItemReference) null);
            return session;
		}
		catch(IPCException e) {
			System.err.println("Caught an IPC exception");
			e.printStackTrace();
			return null;
		}
	}

	public void run() {
		try {
			IPCItem item = createDummyItem(createDummySession(), language);
			IPCPricingConditionSet conditionSet = item.getPricingConditions();
			System.out.println("***** Your conditions:");
			IPCPricingCondition[] conditions = conditionSet.getPricingConditions();
			_displayConditions(conditions);
			conditionSet.addPricingCondition("PR00", "77", "DEM", "1", "PC", null, ".", ",");
			conditions = conditionSet.getPricingConditions();
			System.out.println("***** made some changes...");
			_displayConditions(conditions);
			conditions[1].setConditionRate("999");
			conditions[1].setConditionCurrency("ATS");
			conditions = conditionSet.getPricingConditions();
			System.out.println("***** again some changes...");
			_displayConditions(conditions);
			System.out.println("***** condition types:");
			String[] condTypes = conditionSet.getAvailableConditionTypeNames().getValues();
			for(int i=0; i<condTypes.length; i++){
				System.out.println(condTypes[i]);
			}

		}
		catch(IPCException e) {
			System.err.println("Caught an IPC exception");
			e.printStackTrace();
		}

	}

	protected void _displayConditions(IPCPricingCondition[] conditions) throws IPCException{
		for (int i = 0; i < conditions.length; i++){
			System.out.println( conditions[i].getStepNo()+" * "+
									conditions[i].getConditionTypeName()+" * "+
									conditions[i].getDescription()+" * "+
									conditions[i].getConditionRate()+" * "+
									conditions[i].getConditionCurrency()+" * "+
									conditions[i].getPricingUnitValue()+" * "+
									conditions[i].getPricingUnitUnit()+" * "+
									conditions[i].getConditionValue()+" * "+
									conditions[i].getConditionCurrency()
									);
		}
	}

	public static void main(String[] args) {
		String mandt = args.length > 0 ? args[0]: "100";
		String type  = "ISA";
		String lang  = "EN";
		ConditionTester condTester1 = new ConditionTester(mandt,type,lang);
		condTester1.run();
	}

}