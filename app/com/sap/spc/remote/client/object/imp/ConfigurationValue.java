package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.tc.logging.Location;

/**
 * A ConfigurationValue refers to a configuration. It will retrieve the actual
 * Configuration object as late as possible.<p>
 *
 * Not to be mixed up with ConfigValue which encapsulates external configurations
 */
public class ConfigurationValue {

	protected static final Location location = ResourceAccessor.getLocation(ConfigurationValue.class);

	protected IPCClientObjectFactory factory =
            IPCClientObjectFactory.getInstance();
	protected Configuration config;
	protected boolean       loaded;

	protected IPCClient client;
	protected String configId;

    public ConfigurationValue(IPCClient client, String configId) {
		config = null;
		loaded = false;
		this.client = client;
		this.configId = configId;
    }


	public Configuration getConfig() {
        location.entering("getConfig()");
        if (loaded) {
            if (location.beDebug()) {
                location.debugT("return config " + config.getId());
                
            }
            location.exiting();
			return config;
		}
		else {
			config = factory.newConfiguration(client, configId);
			if (!config.isAvailable()) {
				config = null;
			}
			loaded = true; // if we didn't get a config there's no point in trying to create it later
            if (location.beDebug()) {
                location.debugT("return config " + config.getId());
            }
            location.exiting();
			return config;
		}
	}
}