package com.sap.spc.remote.client.object.imp.tcp;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristic;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;


public class TcpDefaultCharacteristic extends DefaultCharacteristic implements Characteristic {

	// logging
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(TcpDefaultCharacteristic.class);

    protected TcpDefaultCharacteristic(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String applicationViews,
        String[] csticMimeLNames,
        String[] csticMimeObjectTypes,
        String[] csticMimeObjects,
        int[] index) {
        init(
            configurationChangeStream,
            ipcClient,
            instance,
            groupName,
            name,
            unit,
            entryFieldMask,
            typeLength,
            numberScale,
            valueType,
            allowsAdditionalValues,
            visible,
            allowsMultipleValues,
            required,
            expanded,
            consistent,
            isDomainConstrained,
            hasIntervalAsDomain,
            readonly,
            pricingRelevant,
            applicationViews,
            csticMimeLNames,
            csticMimeObjectTypes,
            csticMimeObjects,
            index);
    }
          
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Characteristic#getLanguageDependentUnit()
     */
    public String getLanguageDependentUnit() {
        String languageDependentUnit = "";
        TcpDefaultIPCSession session = (TcpDefaultIPCSession) ipcClient.getIPCSession();
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)session).getPricingConverter();
        // convert only if
        // - unit is not null
        // - unit is not an empty string ("")
        // - value type is not CURRENCY (for currency no conversion needed)
        if ((this.unit != null) && (!this.unit.equals("")) && (this.valueType != TYPE_CURRENCY)){
            languageDependentUnit = pc.convertUOMInternalToExternal(this.unit);
        }      
        if (languageDependentUnit.equals("")){
            languageDependentUnit = this.unit;
        }

        return languageDependentUnit;
    }
}
