/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.ValueDescriptionPairs;

/**
 * Collection of value description pairs. This pairs are generally used for
 * constructing input help.
 */
public class DefaultValueDescriptionPairs implements ValueDescriptionPairs {

	protected String[] _values = null;
	protected String[] _descriptions = null;

	public DefaultValueDescriptionPairs(String[] values, String[] descriptions){
		_values = values;
		_descriptions = descriptions;
	}

    public String[] getValues() throws IPCException{
		return _values/*.getContent()*/;
    }

    public String[] getDescriptions() throws IPCException{
		return _descriptions/*.getContent()*/;
    }
}