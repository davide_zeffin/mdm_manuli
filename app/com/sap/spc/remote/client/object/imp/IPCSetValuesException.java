package com.sap.spc.remote.client.object.imp;

import java.util.HashMap;

import com.sap.spc.remote.client.object.IPCException;

public class IPCSetValuesException extends IPCException {

    HashMap valuesByInstance;

    /**
     * @param sub
     * @param valuesByInstance
     */
    public IPCSetValuesException(Exception sub, HashMap valuesByInstance) {
        super(sub);
        this.valuesByInstance = valuesByInstance;
    }

    /**
     * @return the values for a deleted instances sorted by instance
     */
    public HashMap getValuesByInstance() {
        return valuesByInstance;
    }

}
