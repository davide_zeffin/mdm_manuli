package com.sap.spc.remote.client.object.imp;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.KBProfile;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.LoadingStatus;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.businessobject.BusinessObjectBase;


public abstract class DefaultConfiguration extends BusinessObjectBase implements Configuration {

    protected IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
    protected static final String TRUE = "T";
    protected static final String FALSE = "F";
    protected static final String EMPTY = "";
    protected static final String MIME_HASH = "mimeHash";
    protected static final String USER = "U";
    protected static final String SYSTEM = "S";
    public static final String EXT_CONFIGS_NULL = "extConfigsNull";
    public static final String ERROR_IN_FM = "errorInFunctionModuleCall";

    // logging
	private static final Category category =
				ResourceAccessor.category;
	private static final Location location =
		ResourceAccessor.getLocation(DefaultConfiguration.class);

    protected HashMap data;
    protected IClient cachingClient;
    protected ConfigurationChangeStream configurationChangeStream;
    protected IPCClient ipcClient;
    protected String id;
    protected KnowledgeBase knowledgeBase;
    protected KBProfile activeKBProfile;
    protected boolean isAvailable = false;
    protected Boolean consistent;
    protected Boolean complete;
    protected Boolean singleLevel;
    protected Boolean sceMode;
//    protected Hashtable instances = new Hashtable(); not used TM 20.07.2006
    protected String[] contextNames;
    protected String[] contextValues;
    protected long changetime;
    protected ExplanationTool explanationTool;
    protected LoadingStatus loadingStatus;

    protected boolean closed;

    // Internal cache
    protected TreeMap m_Instances;
    protected Instance m_RootInstance;
    protected Vector instanceList;
    protected boolean cacheIsDirty = true;
    protected boolean dataCached = false;
    protected boolean instanceCacheIsDirty = true;
    protected boolean instanceDataCached = false;

    public String getId() {
        location.entering("getId()");
        if (location.beDebug()){
          location.debugT("return this.id " + this.id);
        }
        location.exiting();
        return this.id;
    }

    //changed 08.07.2002 tm for performance improvement
    public boolean isAvailable() {
        location.entering("isAvailable()");

        synchronize();
        if (location.beDebug()){
            location.debugT("return this.isAvailable " + this.isAvailable);
        }
        location.exiting();
        return isAvailable;
    }

    /**
     * Call this method to send all changes of the configuration to the server
     */
    public void flush() {
        // Flush all instances of this configuration
        location.entering("flush()");
        Exception exceptionForNotSetValues = null;
        List instances = getInstances();
        for (int i = 0; i < instances.size(); i++) {
            ((Instance) instances.get(i)).flush();
        }
        try {
        configurationChangeStream.flush();
        }
        catch (IPCSetValuesException ipcSVEx){
            // catch IPCSetValuesException but continue processing of this method
            exceptionForNotSetValues = ipcSVEx;
        }
        notifyObservers();
        location.exiting();
        if (exceptionForNotSetValues != null){
            // now throw IPCSetValuesException if available
            throw (IPCSetValuesException)exceptionForNotSetValues;
    }
    }

    protected void notifyObservers() {
        if (this.getExplanationTool() != null)
            this.getExplanationTool().update();
    }

    /**
     * Returns knowledge base information about this configuration.
     */
    public KnowledgeBase getKnowledgeBase() {
        location.entering("getKnowledgeBase()");
        synchronize();
        if (location.beDebug()){
          location.debugT("return this.knowledgeBase " + this.knowledgeBase.getName());
        }
        location.exiting();
        return this.knowledgeBase;
    }

    public String[] getContextNames() {
        location.entering("getContextNames()");
        synchronize();
        if (location.beDebug()){
          if (contextNames != null) {
              location.debugT("return contextNames ");
              for (int i=0; i<contextNames.length; i++){
                  location.debugT(contextNames[i]);
              }
          }
        }
        location.exiting();
        return contextNames;
    }

    public String[] getContextValues() {
        location.entering("getContextValues()");
        synchronize();
        if (location.beDebug()){
            if (contextValues != null){
                location.debugT("return contextValues ");
                for (int i=0; i<contextValues.length; i++){
                    location.debugT(contextValues[i]);
                }
            }
        }
        location.exiting();
        return contextValues;
    }

    /**
     * Returns true, if this config is complete.
     */
    public boolean isComplete() {
        location.entering("isComplete()");
        synchronize();
        if (location.beDebug()){
          location.debugT("return complete.booleanValue() " + complete.booleanValue());
        }
        location.exiting();
        return complete.booleanValue();
    }

    /**
     * Returns true, if this config is consistent.
     */
    public boolean isConsistent() {
        location.entering("isConsistent()");
        synchronize();
        if (location.beDebug()){
          location.debugT("return consistent.booleanValue() " + consistent.booleanValue());
        }
        location.exiting();
        return consistent.booleanValue();
    }

    /**
     * Returns true, if this config is a single level configuration.
     */
    public boolean isSingleLevel() {
        location.entering("isSingleLevel()");
        synchronize();
        if (location.beDebug()){
          location.debugT("return singleLevel.booleanValue() " + singleLevel.booleanValue());
        }
        location.exiting();
        return singleLevel.booleanValue();
    }

    /**
     * Returns true if this configuration uses SCE advanced mode.
     */
    public boolean isSceMode() {
        location.entering("isSceMode()");
        synchronize();
        if (location.beDebug()){
          location.debugT("return sceMode.booleanValue() " + sceMode.booleanValue());
        }
        location.exiting();
        return sceMode.booleanValue();
    }

    /**
     * called from synchronized Method DefaultInstance updateGroupCache
     */
    protected abstract void initMembers();
    
    /**
     * Returns true, if the configuration is in initial state().
     * Returns false, if the configuration has changed during the current session.
     */
    public boolean isInitial() throws IPCException {
        location.entering("isInitial()");
        if (location.beDebug()){
            location.debugT("return true");
        }
        location.exiting();
        return true;
    }

    /**
     * Returns all part instances that are present in this configuration,
     * ie. all instances in the tree below the root instance.
     * @return an Vector of Instances, sorted in depth-first preorder (root,
     * first child of root, first child of this child, ...)
     */
    public List getInstances() {
        location.entering("getInstances()");
        synchronizeInstances();
        if (location.beDebug()){
            if (instanceList!=null){
                location.debugT("return instanceList " + instanceList.toString());
            }
            else location.debugT("return instanceList null");
        }
        location.exiting();
        return instanceList;
    }

    /**
     * Returns an instance by it's id
  	 * @param instId the id of the instance
     * @return the instance object
     */
    public Instance getInstance(String instId) {
        location.entering("getInstance(String instId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String instId " + instId);
        }
        synchronizeInstances();
        Instance inst = (Instance) m_Instances.get(instId);
        if (location.beDebug()){
          location.debugT("return (Instance) m_Instances.get(instId) " + inst.getId() + " " + inst.getName());
        }
        location.exiting();
        return inst;
    }

    /**
     * Returns the root instance of this configuration. There is always
     * a root instance - this method never returns null.
     */
    public Instance getRootInstance() {
        location.entering("getRootInstance()");
        synchronizeInstances();
        if (location.beDebug()){
          location.debugT("return _RootInstance " + m_RootInstance.getId() + " " + m_RootInstance.getName());
        }
        location.exiting();
        return m_RootInstance;
    }

    
    protected abstract void synchronizeInstances();
    
    /**
     * This method makes sure that the internal data structures are synchronized
     * with the server's state. The dirty flag tells this method that the
     * internal data structures (cache) need to be updated.
     */
    protected abstract void synchronize();
    
    /**
     * Returns all non-part instances of this configuration.
     */
    public List getNonPartInstances() {
        //to be done
        throw new java.lang.UnsupportedOperationException(
            "needs to be implemented");
    }

    /**
     * Returns an external representation of this config.
     */
    public abstract ext_configuration toExternal(); 
        
    public String toString() {
    	StringBuffer result = new StringBuffer();
    	result.append("<CONFIGURATION>");
    	result.append("activeKBProfile:");
    	result.append(activeKBProfile);
    	result.append("; changetime:");
    	result.append(changetime);
    	result.append("; complete:");
    	result.append(complete);
    	result.append("; consistent:");
    	result.append(consistent);
    	result.append("; contextNames:");
    	result.append(contextNames);
    	result.append("; contextValues:");
    	result.append(contextValues);
    	result.append("; id:");
    	result.append(id);
    	result.append("</CONFIGURATION>");
    	return result.toString();
    }
    
    /**
     * Returns an external representation of this config in a HashMap
     */
    public HashMap getExternalConfigurationStandalone() {
        location.entering("getExternalConfigurationStandalone()");
        HashMap data;
        data = getExternalConfiguration();
        if (location.beDebug()){
            if (data!=null){
                location.debugT("return data " + data.toString());
            }
            else location.debugT("return data null");
        }
        location.exiting();
        return data;
    }

    public ExplanationTool getExplanationTool() {
        location.entering("getExplanationTool()");
        if (explanationTool == null) {
            explanationTool = factory.newExplanationTool(this.ipcClient, this);
        }
        if (location.beDebug()){
            if (explanationTool!=null){
                location.debugT("return explanationTool " + explanationTool.toString());
            }
            else location.debugT("return explanationTool null");
        }
        location.exiting();
        return explanationTool;
    }

    /**
     * Returns an external representation of this config in a HashMap
     */
    public abstract HashMap getExternalConfiguration();
    
    /**
     * Performs a completeness check (incl. dependency knowledge, BOM explosion)
     * on this configuration. Forces an update of the whole configuration.
     */
    public void check() {
        throw new java.lang.UnsupportedOperationException(
            "needs to be implemented");
        //which spc command is to be used here?
        //should we return anything?
    }

    public void decompose() {
        throw new java.lang.UnsupportedOperationException(
            "needs to be implemented");
    }

    public abstract void reset() throws IPCException;

    public long getChangeTime() {
        location.entering("getChangeTime()");
        if (location.beDebug()){
            location.debugT("return changetime " + changetime);
        }
        location.exiting();
        return changetime;
    }

    public void setChangeTime() {
        location.entering("setChangeTime()");
        changetime = System.currentTimeMillis();
        location.exiting();
    }

    public abstract boolean deleteInstance(Instance instance);
   
    public boolean isClosed() {
        location.entering("isClosed()");
        if (location.beDebug()){
            location.debugT("return this.closed " + this.closed);
        }
        location.exiting();
        return this.closed;
    }

    public void close() {
        location.entering("close()");
        this.closed = true;
        location.exiting();
    }

    public boolean isRelevant(String commandName) {
        // we don't use the cache
        location.entering("isRelevant()");
        if (location.beDebug()){
            location.debugT("Parameters:"); 
            location.debugT("String commandName " + commandName);            location.debugT("return true");    
        }
        location.exiting();
        return true;
    }

    /**
     * Returns explanations for the occured conflicts as Strings.
     * If the configuration contains no conflicts null is returned.
     * Since it makes no sense to cache conflicts this methode should
     * be executed only once because a Server-Command is executed every time
     * this method is invoked. The List contains the Conflicts sorted by
     * the InstanceIds.
     */
    public abstract List getConflictInfos();
    
    /**
     * @see com.sap.spc.remote.client.object.Configuration#getActiveKBProfile()
     */
    public KBProfile getActiveKBProfile() {
        location.entering("getActiveKBProfile()");
        synchronize();
        if (location.beDebug()){
            location.debugT("return activeKBProfile " + activeKBProfile.getName());
        }
        location.exiting();
        return activeKBProfile;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getLoadingStatus()
     */
    public LoadingStatus getLoadingStatus() {
        location.entering("getLoadingStatus()");
        if (loadingStatus == null) {
            location.debugT("Loading status is null; call of AP-CFG FM necessary.");
            loadingStatus = factory.newLoadingStatus(ipcClient, this);
		}
        location.exiting();
        return loadingStatus;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#compareConfigurations(com.sap.spc.remote.client.util.ext_configuration, com.sap.spc.remote.client.util.ext_configuration)
     */    
    public abstract ComparisonResult compareConfigurations(
        ext_configuration extConfig1,
        ext_configuration extConfig2);

    public void setContextNames(String[] contextNames){
        this.contextNames = contextNames;
    }
    
    public void setContextValues(String[] contextValues){
        this.contextValues = contextValues; 
    }


	public void setCacheDirty(boolean b) {
		cacheIsDirty = b;
	}

	public void setInstanceCacheDirty(boolean b) {
		instanceCacheIsDirty = b;
	}

	public boolean isInstanceCacheDirty() {
		return instanceCacheIsDirty;
	}

	public boolean isCacheDirty() {
		return cacheIsDirty;
	}
   
}
