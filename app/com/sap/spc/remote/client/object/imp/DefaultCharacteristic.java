/************************************************************************

    Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works
    based upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any
    warranty about the software, its performance or its conformity to any
    specification.

**************************************************************************/

package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector; 

import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.shared.command.GetCsticValues;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.cache.Cache;

public abstract class DefaultCharacteristic extends BusinessObjectBase implements Characteristic {
    protected IPCClientObjectFactory factory =
            IPCClientObjectFactory.getInstance(); 
    protected ConfigurationChangeStream configurationChangeStream;
    protected Instance instance;
    private int characteristicInstancePosition;
    protected Vector groups;
    private int characteristicGroupPosition;
//    protected String groupName;
    protected Vector groupNames;
    protected String name;
    protected String unit;
    protected String entryFieldMask;
    protected int typeLength;
    protected int numberScale;
    protected int valueType;
    protected boolean allowsAdditionalValues;
    protected boolean visible;
    protected boolean allowsMultipleValues;
    protected boolean required;
    protected boolean consistent;
    protected boolean isDomainConstrained;
    protected boolean hasIntervalAsDomain;
    protected boolean readonly;
    protected boolean pricingRelevant;
    protected boolean closed;
    protected boolean selected;
    protected boolean expanded;
    
    protected String applicationViews;
    protected MimeObjectContainer mimeObjects;

    protected ArrayList valuesVec;
    protected ArrayList assignedValuesVec;
    protected ArrayList assignedValuesByUserVec;
    protected ArrayList assignableValues;
    protected HashMap valuesByName;
    protected HashMap valuesByLName;

    protected IPCClient ipcClient;

    protected boolean cacheIsDirty;
    // to use in future version to determine when to rebuild the cache
    protected boolean valueDescriptionsRead;

    protected static final List EMPTY_LIST = new ArrayList();

    // logging
    protected static final Category category = ResourceAccessor.category;
    protected static final Location location = ResourceAccessor.getLocation(DefaultCharacteristic.class);

    protected void init(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String applicationViews,
        String[] csticMimeLNames,
        String[] csticMimeObjectTypes,
        String[] csticMimeObjects,
        int[] index) {
        
        int i = index[2];
        this.configurationChangeStream = configurationChangeStream;
        this.ipcClient = ipcClient;
        this.instance = instance;
        setGroupName(groupName);
        this.name = name;
        this.unit = unit;
        this.entryFieldMask = entryFieldMask;
        this.typeLength = typeLength;
        this.numberScale = numberScale;
        this.valueType = valueType;
        this.allowsAdditionalValues = allowsAdditionalValues;
        this.visible = visible;
        this.allowsMultipleValues = allowsMultipleValues;
        this.required = required;
        this.expanded = expanded;
        this.consistent = consistent;
        this.isDomainConstrained = isDomainConstrained;
        this.hasIntervalAsDomain = hasIntervalAsDomain;
        this.readonly = readonly;
        this.pricingRelevant = pricingRelevant;
        this.selected = false;
        this.closed = false;
        this.cacheIsDirty = true;
        // in future versions you can add a setter method
        this.valuesByName = new HashMap();
        this.valuesByLName = new HashMap();
        this.valueDescriptionsRead = false;
        this.applicationViews = applicationViews;
        
        // handling of mimes: create the DefaultMimeObjectContainer only if mimes attached
        if (csticMimeLNames != null) {
            if (this.mimeObjects == null) {
                this.mimeObjects = factory.newMimeObjectContainer();
    }
            boolean mimes = false;         
            // Position to the first mime object for this characteristic
            while ( i < csticMimeLNames.length && !csticMimeLNames[i].equals(this.name)) {
                i++;
            } 
            // Now add all mime objects of this characteristic to the container
            while ( i < csticMimeLNames.length && csticMimeLNames[i].equals(this.name)) {
                mimes = true;
                MimeObject mimeObject = factory.newMimeObject(csticMimeObjects[i], csticMimeObjectTypes[i]);
                this.mimeObjects.add(mimeObject);
                i++;
            }
            if (mimes) {
                 index[2] = i; 
            } 
        }
    }

    /**
     * This method updates the dynamic information of this characteristic.
     * @deprecated Use {@link #update(boolean, boolean, boolean, boolean, boolean, boolean)} instead.
     */
    public void update(
        boolean visible,
        boolean consistent,
        boolean required,
        boolean readonly) {
        location.entering("update(boolean visible, boolean consistent, boolean required, boolean readonly)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean visible" + visible);
            location.debugT("boolean consistent" + consistent);
            location.debugT("boolean rquired" + required);
            location.debugT("boolean readonly" + readonly);
        }
        this.visible = visible;
        this.consistent = consistent;
        this.required = required;
        this.readonly = readonly;
        location.exiting();
    }
    
    /**
     * This method updates the dynamic information of this characteristic.
     */
    public void update(
        boolean visible,
        boolean consistent,
        boolean required,
        boolean readonly,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain) {
        location.entering("update(boolean visible, boolean consistent, boolean required, boolean readonly, boolean isDomainConstrained, boolean hasIntervalAsDomain)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean visible" + visible);
            location.debugT("boolean consistent" + consistent);
            location.debugT("boolean rquired" + required);
            location.debugT("boolean readonly" + readonly);
            location.debugT("boolean isDomainConstrained" + isDomainConstrained);
            location.debugT("boolean hasIntervalAsDomain" + hasIntervalAsDomain);
        }
        this.visible = visible;
        this.consistent = consistent;
        this.required = required;
        this.readonly = readonly;
        this.isDomainConstrained = isDomainConstrained;
        this.hasIntervalAsDomain = hasIntervalAsDomain;
        location.exiting();
    }    

    /**
     * Returns the instance which is the owner of this characteristic
     */
    public Instance getInstance() {
        location.entering("getInstance()");
        if (location.beDebug()){
            location.debugT("return instance " + instance.getId() + " " + instance.getName());
        }
        location.exiting();
        return instance;
    }

    //a characteristic can be associated to multiple groups
    //in order to keep compatibility with the previous interface
    //the contract is not changed
    public void setGroupName(String groupName) {
        location.entering("setGroupName(String groupName)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String groupName " + groupName);
        }
        if (groupName == null) return;
        boolean found = false;
        if (this.groupNames == null) {
            groupNames = new Vector(1);
        }
        for (Iterator i = groupNames.iterator(); i.hasNext(); ) {
            String cGroupName = (String)i.next();
            if (cGroupName.equals(groupName)) {
                found = true;
            }
        }
        if (!found) {
            groupNames.add(groupName);
        }
        location.exiting();
    }
    public String getGroupName() {
        location.entering("getGroupName()");
        if (groupNames != null && groupNames.size() > 0){
            if (location.beDebug()){
                location.debugT("return groupnames.firstelement() " + (String)groupNames.firstElement());
            }
            location.exiting();
            return (String)groupNames.firstElement();
        }else {
            if (location.beDebug()){
                location.debugT("return \"\"");
            }
            location.exiting();
            return "";
        }
    }
    
    public void setGroup(CharacteristicGroup group) {
        location.entering("setGroup(CharacteristicGroup group)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            if (group!=null){
                location.debugT("CharacteristicGroup group " + group.toString());
            }
            else location.debugT("CharacteristicGroup group null");
        }
        if (group == null) return;
        boolean found = false;
        if (this.groups == null) {
            groups = new Vector(1);
        }
        for (Iterator i = groups.iterator(); i.hasNext(); ) {
            CharacteristicGroup cGroup = (CharacteristicGroup)i.next();
            if (cGroup.equals(group)) {
                found = true;
            }
        }
        if (!found) {
            groups.add(group);
        }
        location.exiting();
    }

    public CharacteristicGroup getGroup() {
        location.entering("getGroup()");
        if (groups != null && groups.size() > 0) {
            if (location.beDebug()) {
                location.debugT("return groups.firstElement() " + groups.firstElement().toString());
            }
            location.exiting();
            return (CharacteristicGroup)groups.firstElement();
        }else {
            if (location.beDebug()) {
                location.debugT("return null");
            }
            location.exiting();
            return null;
        }
    }

    /**
     * Call this method to send all changes of this characteristic and all
     * subobjects to the server.
     */
    public void flush() {
        location.entering("flush()");
        synchronize();
       
        // flush all values
        for (int i = 0; i < valuesVec.size(); i++) {
            ((DefaultCharacteristicValue) valuesVec.get(i)).flush();
        }
        // mark cache as dirty
        cacheIsDirty = true;
        location.exiting();
    }

    /**
     * Returns the dynamic values.
     */
    public List getValues() {
        location.entering("getValues()");
        try {
            synchronize();
            if (location.beDebug()) {
                if (valuesVec!=null){
                    location.debugT("return valuesVec " + valuesVec.toString());
                }
                else location.debugT("return valuesVec null");
            }
            location.exiting();
            return valuesVec;
        } catch (IPCException e) {
            location.throwing("Cannot synchronize with server!", e);
       
        }

        return new Vector();
    }

    public CharacteristicValue getValue(String valueName) {
        location.entering("getValue(String valueName)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("String valueName " + valueName);
        }
        CharacteristicValue value =
            (CharacteristicValue) valuesByName.get(valueName);
        if (location.beDebug()) {
            location.debugT("return value " + value);
        }
        location.exiting();
        return value;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Characteristic#getValueFormLanguageDependentName(java.lang.String)
     */
    public CharacteristicValue getValueFromLanguageDependentName(String valueName) {
        location.entering("getValueFromLanguageDependentName(String valueName)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("String valueName " + valueName);
        }
        CharacteristicValue value = (CharacteristicValue) valuesByLName.get(valueName);
        if (location.beDebug()) {
            location.debugT("return value " + value);
        }
        location.exiting();
        return value;
    }    

    /**
     * Returns the values that have been assigned
     */
    public List getAssignedValues() {
        location.entering("getAssignedValues()");
        try {
            synchronize();
            if (location.beDebug()) {
                if (assignedValuesVec!=null){
                    location.debugT("return assignedValuesVec " + assignedValuesVec.toString());
                }
                else location.debugT("return assignedValuesVec null");
            }    
            location.exiting();        
            return assignedValuesVec;
        } catch (IPCException e) {
            location.throwing("Cannot synchronize with server:", e);
        }

        return EMPTY_LIST;
    }

    public void addValue(String valueName) {
        location.entering("addValue(String valueName)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("String valueName " + valueName);
        }        
        // don't set empty or null values
        if ((valueName == null)) {
            location.exiting();
            return;
        }
          // removed the check for existing values because it was not working properly:
          // It only checked for the internal names. But if the user entered e.g. "12" and
          // in the list of values there is already a value "12". It was not found because the
          // internal name of "12" is "12.0".
          // Before calling this method it has to be checked whether the value is already existing.
          // This can't be done properly in this method because the entered valueName has to be converted to the internal
          // representation (if it is a number) and this is not possible here because of the lack of 
          // information about language, etc.)
          //
          // List values = this.getValues();
          // Iterator it = values.iterator();
          // while (it.hasNext()) {
          //    CharacteristicValue currentValue = (CharacteristicValue) it.next();
          //    if (currentValue.getName().equals(valueName))
          //        return;
          // }
        DefaultCharacteristicValue value =
            factory.newCharacteristicValue(
                configurationChangeStream,
                ipcClient,
                this,
                valueName,
                valueName, //language dependent name is identic with language independent name
                null, //no description
                null, //no condition key when added from user input
                null, //no price information when added from user input
                true); // this is an additional value!!!
        value.update(true, // from now on the value is selectable
        false, // value is not assigned,yet
        false, // value is not assigned by user,yet
        false, // and not assigned by system
        true);

        // add a reference to this value in the internal data structures
        valuesVec.add(value);
//        assignValue(value);
        value.assign();
        assignedValuesVec.add(value);
        if (value.isAssignedByUser()){ // not really necessary but for documentation (value is always assigned by user if value.assign() was executed)
            assignedValuesByUserVec.add(value);
        }
        location.exiting();
        return;
    }

    /**
     * Sets the characteristic's values
     * Values of multivalue characteristics that are not contained in the array
     * will be deleted if they are currently set in the instance.
     * For single value characteristics, only the value contained in the array
     * will be assigned. The engine will remove the value that was assigned to the 
     * characteristic. This way, instances that do not need to be deleted due to the 
     * switch of one value to the other, remain intact (e.g. a characteristic
     * "number of instances" with a domain 1,2,3 that is increased from 1 to 2.
     * This method will process changes to values only
     * @param valueNames This is a list of value names
     * @return true, if a value has been changed
     */
    public boolean setValues(String[] valueNames) {
        location.entering("setValues(String[] valueNames)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            if (valueNames != null) {
                for (int i = 0; i<valueNames.length; i++) {
                    location.debugT("String valueNames["+i+"] " + valueNames[i]);
                }
            }
        }             
        boolean retVal = false;
        HashMap newValsHash = new HashMap();

        if (valueNames != null) {
			if (valueNames.length == 1 && valueNames[0].equals("")) {
				this.clearValues();
				return true;
			}else {
	            for (int i = 0; i < valueNames.length; i++) {
	                //don't set null or empty values
	                if (valueNames[i] != null && !valueNames[i].equals(""))
	                    newValsHash.put(valueNames[i], valueNames[i]);
	            }
			}
        }
        if (location.beDebug()) {
            location.debugT("Setting values for " + this.getName() + ":" + valueNames);
        }
        try {
            synchronize();
            // First step: build internal data structures for faster access
            // (bad performance in the case of few values only)

            // set the change time of the configuration. (do this now because even if
            // something fails on the server and we get an exception the config might
            // still have changed).
            this.instance.getConfiguration().setChangeTime();

            Iterator allVals = valuesVec.iterator();
            //      additional values are handled separately in the method addValue()
            while (allVals.hasNext()) {
                CharacteristicValue value =
                    (CharacteristicValue) allVals.next();
                String newValue = (String) newValsHash.get(value.getName());
                //value not yet set, a value has to be assigned
                if (newValue != null) {
					if (value.isAssignable() && !value.isAssigned()) {
                    	retVal = true;
                    	value.assign();
                    	continue;
                	}
                }else {
					//value already set, a valueAssignment has to be deleted
					//deletion of values only required for multilevel characteristics
					//the engine knows that assigning a different value than the already assigned value
					//for a single value characteristic means removing the already assigned value
					if(this.allowsMultipleValues && value.isAssignedByUser()){
                          retVal = true;
                          value.clear();
                          continue;
                	}
                }
                retVal = true;
            }

        } catch (IPCException e) {
            location.throwing("Cannot synchronize with server:", e);
        }

        if (location.beDebug()){
            location.debugT("return retVal " + retVal);
        }
        location.exiting();
        return retVal;
    }

    /**
     * This method is triggered by the instance. Since the calling method is
     * synchronized, there is no need to make this method synchronized as well.
     */
    public void updateCharacteristicValues(
        int index[],
        String[] valueCsticName,
        String[] valueNames,
        String[] valueLNames,
        String[] valueImages,
        String[] valueConditionKeys,
        String[] valuePrices,
        String[] valueIsInDomains,
        String[] valueAssigneds,
        String[] valueAuthors,
        String[] valueDescriptions,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects) {
                     
            updateCharacteristicValues(
                index,
                valueCsticName,
                valueNames,
                valueLNames,
                valueImages,
                valueConditionKeys,
                valuePrices,
                valueIsInDomains,
                valueAssigneds,
                valueAuthors,
                valueDescriptions,
                new String[valueCsticName.length],
                mimeValueLNames,
                mimeObjectTypes,
                mimeObjects);        
        
        
    }

    /**
     * This method is triggered by the instance. Since the calling method is
     * synchronized, there is no need to make this method synchronized as well.
     */
    public void updateCharacteristicValues(
        int index[],
        String[] valueCsticName,
        String[] valueNames,
        String[] valueLNames,
        String[] valueImages,
        String[] valueConditionKeys,
        String[] valuePrices,
        String[] valueIsInDomains,
        String[] valueAssigneds,
        String[] valueAuthors,
        String[] valueDescriptions,
        String[] valueDefaults,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects) {
        location.entering("updateCharacteristicValues(int index[], String[] valueCsticName, String[] valueNames, " 
                            +"String[] valueLNames, String[] valueImages, String[] valueConditionKeys, "
                            +"String[] valuePrices, String[] valueIsInDomains, String[] valueAssigneds, "
                            +"String[] valueAuthors, String[] valueDescriptions, String[] valueDefaults, String[] mimeValueLNames, "
                            +"String[] mimeObjectTypes, String[] mimeObjects");
        if (location.beDebug()) {
             location.debugT("Parameters:");
             location.debugT("Values Array:");
             for (int i = 0; i<valueCsticName.length; i++) {
                 if (valueCsticName != null)
                    location.debugT("  valueCsticName["+i+"] " + valueCsticName[i]);
                 if (valueNames != null)
                    location.debugT("  valueNames["+i+"] " + valueNames[i]);
                 if (valueLNames != null)
                    location.debugT("  valueLNames["+i+"] " + valueLNames[i]);
                 if (valueImages != null)
                    location.debugT("  valueImages["+i+"] " + valueImages[i]);
                 if (valueConditionKeys != null)
                    location.debugT("  valueConditionKeys["+i+"] "  + valueConditionKeys[i]);
                 if (valuePrices != null)
                    location.debugT("  valuePrices["+i+"] " + valuePrices[i]);
                 if (valueIsInDomains != null)
                    location.debugT("  valueIsInDomains["+i+"] " + valueIsInDomains[i]);
                 if (valueAssigneds != null)
                    location.debugT("  valueAssigneds["+i+"] "  + valueAssigneds[i]);
                 if (valueAuthors != null)
                    location.debugT("  valueAuthors["+i+"] " + valueAuthors[i]);
                 if (valueDescriptions != null)
                    location.debugT("  valueDescriptions["+i+"] " + valueDescriptions[i]);
                 if (valueDefaults != null)
                    location.debugT("  valueDefaults["+i+"] " + valueDefaults[i]);
             }  
             if (mimeValueLNames != null){
                 location.debugT("Mimes Array:");
                 for (int i = 0; i<mimeValueLNames.length; i++){
                     if (mimeValueLNames != null)
                        location.debugT("  mimeValueLNames["+i+"] "+ mimeValueLNames[i]);
                     if (mimeObjectTypes != null)
                        location.debugT("  mimeObjectTypes["+i+"] "+ mimeObjectTypes[i]);
                     if (mimeObjects != null) 
                        location.debugT("  mimeObjects["+i+"] "+ mimeObjects[i]);
                 }
             }
        }                        
        
        cacheIsDirty = false;
        int i = index[0];
        if (valueNames != null) {
            // Now generate the mime objects and place them into a hashtable
            int numberOfValues = (valueNames == null) ? 0 : valueNames.length;
            HashMap previousValuesByName = null;

            if (valuesVec != null) {
                valuesVec.clear();
            } else {
                valuesVec = new ArrayList(numberOfValues);
                assignedValuesVec = new ArrayList(numberOfValues);
            }
            if (assignedValuesVec != null && assignedValuesByUserVec != null) {
                assignedValuesVec.clear();
                assignedValuesByUserVec.clear();
            }else {
                assignedValuesVec = new ArrayList(numberOfValues);
                assignedValuesByUserVec = new ArrayList(numberOfValues);
            }
            if (assignableValues != null) {
                assignableValues.clear();
            }else {
                assignableValues = new ArrayList(numberOfValues);                
            }
            previousValuesByName = valuesByName;
            valuesByName = new HashMap();
            valuesByLName = new HashMap();
            boolean values = false;
            String valueDescr = null;
            // Search for the begining of the values definition
            while (i < valueCsticName.length
                && !valueCsticName[i].equals(this.name))
                i++;
            while (i < valueCsticName.length
                && valueCsticName[i].equals(this.name)) {
                values = true;
                if (valueDescriptions != null)
                    valueDescr = valueDescriptions[i];
                else
                    valueDescr = null;

                boolean assigned = false;
                if (valueAssigneds[i].equals(GetCsticValues.YES) || valueAssigneds[i].equals(GetCsticValues.TRUE)) { // to be compatible with VMC export parameters
                    assigned = true;
                }
                DefaultCharacteristicValue value =
                    (DefaultCharacteristicValue) previousValuesByName.get(
                        valueNames[i]);
                String valueLName = (valueLNames != null && valueLNames[i] != null)
                    ? valueLNames[i]
                    : valueNames[i];
                boolean defaultValue = false;
                if (valueDefaults != null && i < valueDefaults.length && valueDefaults[i] != null){
                    if (valueDefaults[i].equals(GetCsticValues.YES) || valueDefaults[i].equals(GetCsticValues.TRUE)) { 
                        defaultValue = true;
                    }
                }
                if (value == null) {
                    value =
                        factory.newCharacteristicValue(
                            configurationChangeStream,
                            ipcClient,
                            this,
                            valueNames[i],
                            valueLName,
                            valueDescr,
                            (valueConditionKeys != null) ? valueConditionKeys[i] : "",
                            (valuePrices != null) ? valuePrices[i] : "",
                            index,
                            mimeValueLNames,
                            mimeObjectTypes,
                            mimeObjects,
                            defaultValue);
                }
                boolean valueIsInDomain = false;
                if (valueIsInDomains[i].equals(GetCsticValues.YES) || valueIsInDomains[i].equals(GetCsticValues.TRUE)) {
                    valueIsInDomain = true;
                }
                value.update(
                    valueIsInDomain,
                    assigned,
                    valueAuthors[i].equals(GetCsticValues.USER) && assigned,
                    valueAuthors[i].equals(GetCsticValues.SYSTEM) && assigned,
                    true,
                    defaultValue);
                valuesByName.put(valueNames[i], value);
                valuesByLName.put(valueLName, value);
                valuesVec.add(value);
                valuesByName.put(valueNames[i], value);
                if (value.isAssigned()) {
                    assignedValuesVec.add(value);
                    if (value.isAssignedByUser())
                        assignedValuesByUserVec.add(value);
                }
                if (value.isAssignable()) {
                    assignableValues.add(value);
                }
                i++;
            }
            if (values)
                index[0] = i;
        } else {
            // There are no values, so create empty containers
            valuesVec = new ArrayList();
            assignedValuesVec = new ArrayList();
            assignedValuesByUserVec = new ArrayList();
        }
        location.exiting();
    }

    /**
     * Use this method to ensure that the internal cache is up-to-date
     */
    protected void synchronize() throws IPCException {
        if (cacheIsDirty) {
            ((DefaultInstance) instance).synchronize();
        }
    }

    /**
     * Clears all user set values.
     */
    public void clearValues() {
        location.entering("clearValues()");
        try {
            synchronize();
            Iterator valueEnum = valuesVec.iterator();
            while (valueEnum.hasNext()) {
                CharacteristicValue value =
                    (CharacteristicValue) valueEnum.next();
                if (value.isAssignedByUser()) {
                    value.clear();
                }
            }
        } catch (IPCException e) {
            location.throwing("Cannot synchronize with server:", e);
        }
    
        // update the config's change time
        instance.getConfiguration().setChangeTime();
        location.exiting();
    }
    /**
     * Returns a list of mime objects (images, sounds, etc.) attached to the characteristic.
     */
    public MimeObjectContainer getMimeObjects() {
        location.entering("getMimeObjects()");
        if (location.beDebug()){
            if (this.mimeObjects!=null){
                location.debugT("return this.mimeobjects " + this.mimeObjects.getMimeObjects().toString());
            }
            else location.debugT("return this.mimeobjects null");

        }
        location.exiting();
        return this.mimeObjects;
    }

    public String getLanguageDependentName() {
        location.entering("getLanguageDependentName()");
        String locLangName = null;
        /**
         * Returns a single characteristic - identified by the language
         * independent characteristic name, null if not found
         */
        KnowledgebaseCacheContainer groupContainer = getKBCacheContainer();
        if (groupContainer != null)
            locLangName = groupContainer.getCharacteristicLName(name);
        if (locLangName == null) {
            if (location.beDebug()) {
                location.debugT("return \"\"" + " (" + name + ")");
            }
            location.exiting();
            return "";
        }
        if (location.beDebug()) {
            location.debugT("return langName " + locLangName);
        }
        location.exiting();
        return locLangName;
    }

    public String getName() {
        location.entering("getName()");
        if (location.beDebug()){
            location.debugT("return Name " + name);
        }
        location.exiting();  
        return name;
    }

    public String getUnit() {
        location.entering("getUnit()");
        if (location.beDebug()){
            location.debugT("return unit " + unit);
        }
        location.exiting();  
        return unit;
    }

    public String getEntryFieldMask() {
        location.entering("getEntryFieldMask()");
        if (entryFieldMask == null) {
            if (location.beDebug()){
                location.debugT("return \"\""); 
            }
            location.exiting();
            return "";
        }
        if (location.beDebug()) {
            location.debugT("return entryFieldMask " + entryFieldMask);
        }
        return entryFieldMask;
    }

    public int getTypeLength() {
        location.entering("getTypeLength()");
        if (location.beDebug()){
            location.debugT("return typeLength " + typeLength +" ("+name+")");
        }
        location.exiting();  
        return typeLength;
    }

    public int getNumberScale() {
        location.entering("getNumberScale()");
        if (location.beDebug()){
            location.debugT("return numberScale " + numberScale +" ("+name+")");
        }
        location.exiting();
        return numberScale;
    }

    public int getValueType() {
        location.entering("getValueType()");
        if (location.beDebug()){
            location.debugT("return valueType " + valueType +" ("+name+")");
        }
        location.exiting();
        return valueType;
    }

    public boolean allowsAdditionalValues() {
        location.entering("allowsAdditionalValues()");
        if (location.beDebug()){
            location.debugT("return allowsAdditionalValues " + allowsAdditionalValues +" ("+name+")");
        }
        location.exiting();
        return allowsAdditionalValues;
    }

    public boolean isVisible() {
        location.entering("isVisible()");
        if (location.beDebug()){
            location.debugT("return visible " + visible +" ("+name+")");
        }
        location.exiting();
        return visible;
    }
    public void setVisible(boolean visible) {
        location.entering("setVisible(boolean visible)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean visible " + visible);
        }
        this.visible = visible;
        location.exiting();
    }

    public boolean allowsMultipleValues() {
        location.entering("allowsMultipleValues()");
        if (location.beDebug()){
            location.debugT("return allowsMultipleValues " + allowsMultipleValues +" ("+name+")");
        }
        location.exiting();
        return allowsMultipleValues;
    }

    public boolean isPricingRelevant() {
        location.entering("isPricingRelevant()");
        if (location.beDebug()){
            location.debugT("return pricingRelevant " + pricingRelevant +" ("+name+")");
        }
        location.exiting();
        return pricingRelevant;
    }

    public boolean isRequired() {
        location.entering("isRequired()");
        if (location.beDebug()){
            location.debugT("return required " + required +" ("+name+")");
        }
        location.exiting();
        return required;
    }

    public boolean isConsistent() {
        location.entering("isConsistent()");
        if (location.beDebug()){
            location.debugT("return consistent " + consistent +" ("+name+")");
        }
        location.exiting();
        return consistent;
    }

    /**
     * Returns true if the domain of the characteristic may contain an interval (1 - 10).
     * Since we do not have the knowledge about the single values, the method exists at the characteristic.
     * If the engine messages will deliver the more detailed information about values, we may deprecate this method.
     */
    public boolean hasIntervalAsDomain() {
        location.entering("hasIntervalAsDomain()");
        if (location.beDebug()){
            location.debugT("return hasIntervalAsDomain " + hasIntervalAsDomain +" ("+name+")");
        }
        location.exiting();
        return hasIntervalAsDomain;
    }

    public boolean isReadOnly() {
        location.entering("isReadOnly()");
        if (location.beDebug()){
            location.debugT("return readOnly " + readonly +" ("+name+")");
        }
        location.exiting();
        return readonly;
    }

    public void setReadOnly(boolean readonly) {
        location.entering("setReadOnly(boolean readonly)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean readonly " + readonly);
        }
        this.readonly = readonly;
        location.exiting();
    }

    /**
     * Returns true if the characteristic group was selected in the UI;
     * returns false otherwise
     */
    public boolean isSelected() {
        location.entering("isSelected()");
         if (location.beDebug()){
             location.debugT("return this.selected " + this.selected +" ("+name+")");
         }
         location.exiting();
        return this.selected;
    }
    
    public void setSelected(boolean selected) {
        location.entering("setSelected(boolean selected)");
        if (location.beDebug()) {
           location.debugT("Parameters:");
           location.debugT("boolean selected " + selected);
        }
        this.selected = selected;
        location.exiting();
    }

    /**
     * Returns true if the characteristic has a least one value
     * assigned; returns false otherwise
     */
    public boolean hasAssignedValues() {
        location.entering("hasAssignedValues()");
        try {
            synchronize();
            if (assignedValuesVec == null) {
                if (location.beDebug()){
                    location.debugT("return false" +" ("+name+")");     
                }
                location.exiting();
                return false;
            }
            assignedValuesVec.trimToSize();
            if (assignedValuesVec.isEmpty()){
                if (location.beDebug()){
                    location.debugT("return false" +" ("+name+")");     
                }
                location.exiting();
                return false;
            }
            if (location.beDebug()){
                location.debugT("return true" +" ("+name+")");     
            }
            location.exiting();
            return true;
        } catch (IPCException e) {
            location.throwing("Cannot synchronize with server:", e);
        }

        return false;
    }

    /**
     * Returns true if the characteristic has a least one value
     * assigned by the user; returns false otherwise
     */
    public boolean hasAssignedValuesByUser() {
        location.entering("hasAssignedValuesByUser()");
        try {
            synchronize();
            if (assignedValuesByUserVec == null){
                if (location.beDebug()){
                    location.debugT("return false" +" ("+name+")");     
                }
                location.exiting();
                return false;
            }
            assignedValuesByUserVec.trimToSize();
            if (assignedValuesByUserVec.isEmpty()){
                if (location.beDebug()){
                    location.debugT("return false" +" ("+name+")");     
                }
                location.exiting();
                return false;
            }
            if (location.beDebug()){
                location.debugT("return true" +" ("+name+")");     
            }
            location.exiting();
            return true;
        } catch (IPCException e) {
            location.throwing("Cannot synchronize with server:", e);
        }
        return false;
    }

    public void close() {
        location.entering("close()");
        closed = true;
        location.exiting();
    }

    /**
     * Returns true only if the domain is constrained.
     * A domain is constrained if it has a fixed set of possible values
     * for its Characteristic. Only constrained domains will return a non empty
     * array when calling getStaticDomain().
     *
     * @see #getStaticDomain
     */
    public boolean isDomainConstrained() {
        location.entering("isDomainConstrained()");
         if (location.beDebug()){
             location.debugT("return isDomainConstrained " + isDomainConstrained);
         }
         location.exiting();
        return isDomainConstrained;
    }

    public boolean isClosed() {
        location.entering("isClosed()");
         if (location.beDebug()){
             location.debugT("return closed " + closed);
         }
         location.exiting();
        return closed;
    }

    public boolean isRelevant(String commandName) {
        // we don't use the cache
        location.entering("isRelevant(String commandName)");
        if (location.beDebug()){
             location.debugT("Parameters:");
             location.debugT("String commandName " + commandName);
             location.debugT("return true " + true);
        }
        location.exiting();
        return true;
    }

    private KnowledgebaseCacheContainer getKBCacheContainer()
    {
        Cache.Access access;
        try {
            access = Cache
            .getAccess(DefaultCharacteristicGroup.csticGroupCacheRegion);
        } catch (Cache.Exception e) {
            category.logT(Severity.ERROR, location, e.toString());
            return null;
        }
        KnowledgebaseCacheKey csticKey = new KnowledgebaseCacheKey(
                this.getInstance());
        return (KnowledgebaseCacheContainer)access.get(csticKey, getGroup().getInstance());
    }
    public String getDescription() {
        location.entering("getDescription()");
            // description has not been retrieved yet
            String desc = null;
            /**
             * Returns a single characteristic - identified by the language
             * independent characteristic name, null if not found
             */
            KnowledgebaseCacheContainer groupContainer = getKBCacheContainer();
            if (groupContainer != null)
                desc = groupContainer.getCharacteristicDescription(name);
            if (desc == null) {
                if (location.beDebug()) {
                    location.debugT("return \"\"" + " (" + name + ")");
                }
                location.exiting();
                return "";
            } 

        if (location.beDebug()) {
            location.debugT("return this.description " + desc
                    + " (" + name + ")");
        }
        location.exiting();
        return desc;
    }
    
    /**
     * @return
     */
    public List getAssignableValues() {
        location.entering("getAssignableValues()");
        if (assignableValues != null) {
            if (location.beDebug()) {
                location.debugT(
                    "Assignable Values:"
                        + DefaultCharacteristicValue.convertToString(
                            assignableValues));
            }
            location.exiting();         
            return assignableValues;
        } else {
            if (location.beDebug()) {
                location.debugT("return EMPTY_LIST");
            }
            location.exiting();
            return EMPTY_LIST;
        }

    }
    
    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("<CHARACTERISTIC>");
        result.append("; name:");
        result.append(name); 
        result.append("; allowsAdditionalValues:");
        result.append(allowsAdditionalValues);
        result.append("; allowsMultipleValues:");
        result.append(allowsMultipleValues);
        result.append("; consistent:");
        result.append(consistent);
        result.append("; entryFieldMask:");
        result.append(entryFieldMask);
        result.append("; groupName:");
        result.append(getGroupName());
        result.append("; hasIntervalAsDomain:");
        result.append(hasIntervalAsDomain);
        result.append("; isDomainConstrained:");
        result.append(isDomainConstrained);
        result.append("; numberScale:");
        result.append(numberScale);
        result.append("; pricingRelevant:");
        result.append(pricingRelevant);
        result.append("; readonly:");
        result.append(readonly);
        result.append("; required:");
        result.append(required);
        result.append("; selected:");
        result.append(selected);
        result.append("; typeLength:");
        result.append(typeLength);
        result.append("; unit");
        result.append(unit);
        result.append("; visible:");
        result.append(visible);
        result.append("</CHARACTERISTIC>");
        
        return result.toString();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Characteristic#getApplicationViews()
     */
    public String getApplicationViews(){
        location.entering("getApplicationViews()");
         if (location.beDebug()){
             location.debugT("return applicationViews " + applicationViews +" ("+name+")");
         }
         location.exiting();
        
        return applicationViews;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Characteristic#isPartOfApplicationView(char)
     */
    public boolean isPartOfApplicationView(char applicationView) {
        location.entering("isPartOfApplicationView(char applicationView)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("char applicationView" + applicationView);
        }
        boolean partOfView = true;
        int positionOfChar = applicationViews.indexOf(applicationView);
        if (positionOfChar == -1) {
            partOfView = false;
        }
        if (location.beDebug()) {
            location.debugT("return partOfView " + partOfView);
        }
        location.exiting();
        return partOfView;
    }
    
    /**
     * Returns true if the characteristic has to be expanded 
     * and returns false otherwise.
     */
    public boolean isExpanded() {
        location.entering("isExpanded()");
         if (location.beDebug()){
             location.debugT("return this.expanded " + this.expanded +" ("+name+")");
         }
         location.exiting();
        return this.expanded;   
    }
    
    public boolean assignValue(CharacteristicValue value) {
        location.entering("assignValue(CharacteristicValue value)");
        if (location.beDebug()) {
           location.debugT("Parameters:");
           if (value!=null){
               location.debugT("CharacteristicValue value " + value.toString());
           }
           else location.debugT("CharacteristicValue value null");
        }
        // check whether the value is really existing at the cstic
        if (this.getValue(value.getName()) == null  || value.isAssigned()) {
            if (location.beDebug()) {
                location.debugT("return false");
            }
            location.exiting();
            return false;
        }
        value.assign();
        assignedValuesVec.add(value);
        if (value.isAssignedByUser()){ // not really necessary but for documentation (value is always assigned by user if value.assign() was executed)
            assignedValuesByUserVec.add(value);
        }
        if (location.beDebug()) {
            location.debugT("return true");
        }
        location.exiting();
        return true;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Characteristic#getLanguageDependentUnit()
     */
    public abstract String getLanguageDependentUnit();

	public int getCharacteristicGroupPosition() {
		return characteristicGroupPosition;
	}

	public int getCharacteristicInstancePosition() {
		return characteristicInstancePosition;
	}

	public void setCharacteristicGroupPosition(int i) {
		characteristicGroupPosition = i;
	}

	public void setCharacteristicInstancePosition(int i) {
		characteristicInstancePosition = i;
	}

}


