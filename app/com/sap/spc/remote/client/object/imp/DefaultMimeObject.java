/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object.imp;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

public class DefaultMimeObject extends BusinessObjectBase implements MimeObject {
    protected String url;
    protected String type;
    protected HashMap parameters;
    protected static final String URL_CHARACTER_ENCODING = "UTF-8";
    public static final DefaultMimeObject C_NULL = new DefaultMimeObject("");

    // logging
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(DefaultMimeObject.class);

    DefaultMimeObject() {
        init();
    }

    /**
     * Use this constructor to build a mime object from an URLEncoded String
     */
    DefaultMimeObject(int index, String queryStr) {
    	super(new TechKey(index + queryStr));
        init();
        readFromURLEncodedRepresentation(index, queryStr);
    }
    
    /**
     * Use this constructor, if you want the type to be automatically detected
     * by evaluating the url's extension.
     */
    DefaultMimeObject(String url) {
    	super (new TechKey(url));
        init();
        this.url = parseURL(url);
    
        int extensionPos = this.url.lastIndexOf('.');
        if (extensionPos > 0) {
            String extension = this.url.substring(extensionPos + 1, url.length());
            if (extension.equalsIgnoreCase("o2c")) {
                type = O2C;
            } else if (extension.equalsIgnoreCase("wav")) {
                type = SOUND;
            } else if (extension.equalsIgnoreCase("pdf") || extension.equalsIgnoreCase("doc")) {
                type = APPLICATION;
            } else // Default is always the picture...
                {
                type = IMAGE;
            }
        }
    }
    
    /**
     * Use this constructor if you already know the mime type
     */
    DefaultMimeObject(String url, String type) {
    	super(new TechKey(url + type));
        init();
        this.url = parseURL(url);
        this.type = type;
    }
    
    /**
     * Default initialization, called by all constructors
     */
    protected void init() {
        parameters = new HashMap();
        type = UNKNOWN;
    }
    
    /**
     * This method accepts an url with attached parameters in the form
     * xxx.gif?width=100&height=...
     * @param encodedURL
     * @return the url without any parameters attached to it
     */
    protected String parseURL(String encodedURL) {
        HashMap params = parseParameters(encodedURL);
        Iterator paramIterator = params.keySet().iterator();
        while (paramIterator.hasNext()) {
            String paramName = (String) paramIterator.next();
            try {
                setParameter(paramName,
                    URLDecoder.decode((String) params.get(paramName), URL_CHARACTER_ENCODING));
            }
            catch (UnsupportedEncodingException uEE) {
                category.logT(Severity.ERROR, location, uEE.toString());
            }
        }
        if (encodedURL.indexOf('?') > 0) {
            return encodedURL.substring(0, encodedURL.indexOf('?'));
        }
        return encodedURL;
    }
    
    /**
     * returns the url of the object (picture, video, o2c ...)
     */
    public String getURL() {
        return url;
    }
    
    /**
     * returns the type of this mime object (sound,picture...)
     * Check the return value against one of the predefined constants in the
     * MimeObject interface
     */
    public String getType() {
        return type;
    }
    
    /**
     * Using this method, you're able to associate any kind of string data with
     * the mime object. You can evaluate these parameters e.g. for display options.
     * However these parameters can be used at runtime only!
     */
    public void setParameter(String paramName, String paramValue) {
        parameters.put(paramName, paramValue);
    }
    
    /**
     * Returns the associated parameter. ParamName is the same key as you used
     * when calling setParameter.
     * Note: paramName is case sensitive!
     */
    public String getParameter(String paramName) {
        return (String) parameters.get(paramName);
    }
    
    /**
     * Returns the associated parameter. ParamName is the same key as you used
     * when calling setParameter. If ParamName has not been set prior calling this
     * method, the default value "defaultValue" is returned instead.
     * Note: paramName is case sensitive!
     */
    public String getParameter(String paramName, String defaultValue) {
        if (paramName == null)
            return defaultValue;
        String returnValue = getParameter(paramName);
        return (returnValue != null) ? returnValue : defaultValue;
    }
    
    /**
     * Use this method to return all members of this mime object in a way that
     * you can send the data via http request. All parameters are concatenated
     * to each others by "&". Note: You have to enter a "?" manually!
     */
    public String getURLEncodedRepresentation(int index) {
        StringBuffer urlEncodedStr = new StringBuffer();
        try {
            urlEncodedStr.append(MIME_URL);
            urlEncodedStr.append(index);
            urlEncodedStr.append("=");
            urlEncodedStr.append(URLEncoder.encode(url, URL_CHARACTER_ENCODING));
            urlEncodedStr.append("&");
            urlEncodedStr.append(MIME_TYPE);
            urlEncodedStr.append(index);
            urlEncodedStr.append("=");
            urlEncodedStr.append(URLEncoder.encode(type, URL_CHARACTER_ENCODING));
        
            Iterator keys = parameters.keySet().iterator();
            int paramNo = 0;
            while (keys.hasNext()) {
                urlEncodedStr.append("&");
                String paramName = (String) keys.next();
                urlEncodedStr.append(MIME_PARAM_NAME);
                urlEncodedStr.append(index);
                urlEncodedStr.append("_");
                urlEncodedStr.append(paramNo);
                urlEncodedStr.append("=");
                urlEncodedStr.append(URLEncoder.encode(paramName, URL_CHARACTER_ENCODING));
        
                urlEncodedStr.append("&");
                urlEncodedStr.append(MIME_PARAM_VALUE);
                urlEncodedStr.append(index);
                urlEncodedStr.append("_");
                urlEncodedStr.append(paramNo++);
                urlEncodedStr.append("=");
                urlEncodedStr.append(URLEncoder.encode((String) parameters.get(paramName), URL_CHARACTER_ENCODING));
            }
        }
        catch (UnsupportedEncodingException uEE) {
            category.logT(Severity.ERROR, location, uEE.toString());
        }
        return urlEncodedStr.toString();
    }
    
    public void readFromURLEncodedRepresentation(int index, String queryStr) {
        // The first section scans for all parameters in the query string
        HashMap params = parseParameters(queryStr);
    
        try {
            // The second section parses all parameters in the query string
            if (params.get(MIME_URL + index) != null) {
                url = URLDecoder.decode((String) params.get(MIME_URL + index), URL_CHARACTER_ENCODING);
            }
            if (params.get(MIME_TYPE + index) != null) {
                type = URLDecoder.decode((String) params.get(MIME_TYPE + index), URL_CHARACTER_ENCODING);
            }
        
            int paramNo = 0;
            String paramName;
            do {
                paramName =
                    (String) params.get(MIME_PARAM_NAME + index + "_" + paramNo);
                if (paramName != null) {
                    setParameter(
                        URLDecoder.decode(paramName, URL_CHARACTER_ENCODING),
                        URLDecoder.decode((String) params.get(MIME_PARAM_VALUE + index + "_" + paramNo++), 
                            URL_CHARACTER_ENCODING));
                }
            } while (paramName != null);
        }
        catch (UnsupportedEncodingException uEE) {
            category.logT(Severity.ERROR, location, uEE.toString());
        }
    }
    
    /**
     * Returns a HashMap containing all parameters found in the current query string
     * @param queryStr
     * @return
     */
    protected HashMap parseParameters(String queryStr) {
        HashMap params = new HashMap();
        if (queryStr.indexOf('?') >= 0)
            queryStr = queryStr.substring(queryStr.indexOf('?') + 1);
        StringTokenizer tokenizer = new StringTokenizer(queryStr, "&");
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            int equalPos = token.indexOf('=');
            if (equalPos >= 0) {
                String paramName = token.substring(0, equalPos);
                String paramValue = token.substring(equalPos + 1);
                params.put(paramName, paramValue);
            }
        }
        return params;
    }
}
