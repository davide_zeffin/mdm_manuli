package com.sap.spc.remote.client.object.imp.rfc;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.KBProfile;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.imp.DefaultKBProfile;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.rfc.function.CfgApiGetProfilesOfKb;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class RfcDefaultKBProfile extends DefaultKBProfile implements KBProfile {

	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(RfcDefaultKBProfile.class);    

    public RfcDefaultKBProfile(String name, KnowledgeBase kb) {
        super(name, kb);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultKBProfile#init()
     */
    protected void init() {
        String kbLogsys = kb.getLogsys();
        String kbName = kb.getName();
        String kbVersion = kb.getVersion();
        String kbProfile;
        String rootInstanceName;
        String uiname;

        IPCSession session = kb.getIPCClient().getIPCSession();
        if (!(session instanceof RfcDefaultIPCSession)) {
            throw new IllegalClassException(session, RfcDefaultIPCSession.class);
        }
        RFCDefaultClient cachingClient = (RFCDefaultClient)((RfcDefaultIPCSession) session).getClient();

        JCO.ParameterList ex;
        try {
            JCO.Function functionCfgApiGetProfilesOfKb = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_PROFILES_OF_KB);
            JCO.ParameterList im = functionCfgApiGetProfilesOfKb.getImportParameterList();
            ex = functionCfgApiGetProfilesOfKb.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiGetProfilesOfKb.getTableParameterList();
            
            im.setValue(kbLogsys, CfgApiGetProfilesOfKb.KB_LOGSYS);     // CHAR, Logical System
            im.setValue(kbName, CfgApiGetProfilesOfKb.KB_NAME);     // CHAR, KB Name
            im.setValue(kbVersion, CfgApiGetProfilesOfKb.KB_VERSION);       // CHAR, KB Version
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_PROFILES_OF_KB");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetProfilesOfKb);
            category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_PROFILES_OF_KB");
        } catch (ClientException e) {
            throw new IPCException(e);
        }

        // process table exKbProfiles
        // TABLE, CFG: Table of profile names with corresponding UI names
        JCO.Table exKbProfiles = ex.getTable(CfgApiGetProfilesOfKb.KB_PROFILES);
        int exKbProfilesCounter;
        int numRows = exKbProfiles.getNumRows();
        String[] kbProfiles = new String[numRows];
        String[] kbRootObjNames = new String[numRows];
        String[] uinames = new String[numRows];
        for (exKbProfilesCounter=0; exKbProfilesCounter<numRows; exKbProfilesCounter++) {
            exKbProfiles.setRow(exKbProfilesCounter);
            kbProfiles[exKbProfilesCounter] = exKbProfiles.getString(CfgApiGetProfilesOfKb.KB_PROFILE);
            kbRootObjNames[exKbProfilesCounter] = exKbProfiles.getString(CfgApiGetProfilesOfKb.ROOT_INSTANCE_NAME);
            uinames[exKbProfilesCounter] = exKbProfiles.getString(CfgApiGetProfilesOfKb.UINAME);
        }

        for (int i = 0; i < kbProfiles.length; i++) {
            if (kbProfiles[i].equals(this.name)) {
                this.rootObjName = kbRootObjNames[i];
                if (uinames != null && uinames[i] != null) {
                    this.uiName = uinames[i];
                } else {
                    this.uiName = "";
                }
            }
        }
    }

}
