package com.sap.spc.remote.client.object.imp.tcp;

import java.util.Locale;

import org.apache.struts.util.MessageResources;

import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ConflictParticipant;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.DefaultConflictParticipant;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.shared.command.DeleteConflictingAssumption;
import com.sap.spc.remote.shared.command.SCECommand;
//import com.sap.sxe.socket.client.ClientException;
//import com.sap.sxe.socket.client.ServerResponse;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.tc.logging.Severity;

public class TcpDefaultConflictParticipant
    extends DefaultConflictParticipant
    implements ConflictParticipant {

    protected TcpDefaultConflictParticipant(
        IPCClient ipcClient,
        Conflict conflict,
        String id,
        String factKey,
        boolean causedByUser,
        boolean causedByDefault,
        Locale locale,
        MessageResources resources) {
        this.ipcClient = ipcClient;
        this.conflict = conflict;
        this.id = id;
        this.causedByUser = causedByUser;
        this.causedByDefault = causedByDefault;
        this.factKey = factKey;
        this.keyAdjusted = false;
        this.text = adjustText(factKey, locale, resources);
        this.textAdjusted = true;
        this.closed = false;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConflictParticipant#remove()
     */
    public void remove() {
        try {
            // 20020315-kha: new "all or nothing" client.object implementation to clean up API (cf IPCClient internal comment)
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof DefaultIPCSession)) {
                throw new IllegalClassException(session, DefaultIPCSession.class);
            }
            this.cachingClient = ((DefaultIPCSession) session).getClient();

            ServerResponse r =
			((TCPDefaultClient)cachingClient).doCmd(
                    ISPCCommandSet.DELETE_CONFLICTING_ASSUMPTION,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        getConfigId(),
                        DeleteConflictingAssumption.ASSUMPTION_ID,
                        this.id });
            conflict.getExplanationTool().setUpdateConflicts(true);
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            // to be done
            throw new IPCException(e);
        }
    }

}
