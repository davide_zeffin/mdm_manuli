package com.sap.spc.remote.client.object.imp;
/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

import java.util.List;

import com.sap.spc.remote.client.object.Instance;

/**
 * The base group is the group, containing all the characteristics that have not
 * been assigned to other groups.
 * Note: Since BaseGroup is a virtually created class only, it has no knowledge
 * of a language dependent representation of it's name. So check out in the
 * ui if the group is the base group and use a language dependent name for it
 * there.
 */
public class BaseGroup extends DefaultCharacteristicGroup{
	public static final String BASE_GROUP_NAME = "$BASE_GROUP";

    public BaseGroup(   Instance instance,
						List characteristics,
                        int position) {
		super(instance, BASE_GROUP_NAME, BASE_GROUP_NAME, characteristics,position);
	}

	public boolean isBaseGroup() {
	    return true;
	}
}