package com.sap.spc.remote.client.object.imp.tcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCException;
//import com.sap.spc.remote.client.object.imp.CachingClient;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.IPCSetValuesException;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.shared.command.DeleteCsticValues;
import com.sap.spc.remote.shared.command.DeleteCsticsValues;
import com.sap.spc.remote.shared.command.SCECommand;
import com.sap.spc.remote.shared.command.SetCsticValues;
import com.sap.spc.remote.shared.command.SetCsticsValues;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.tcp.ServerResponse;


public class TcpConfigurationChangeStream extends ConfigurationChangeStream {

    
    protected TcpConfigurationChangeStream(Configuration configuration, IClient cachingClient) {
        m_ConfigurationChanges = new Vector();
        m_Configuration = configuration;
        m_CachingClient = cachingClient;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigurationChangeStream#executeSetCommand(java.util.HashMap)
     */
    protected void executeSetCommand(HashMap setOpsByInstance) {
        Iterator setInstIterator = setOpsByInstance.keySet().iterator();
        HashMap notSetValuesByInstance = new HashMap(); 
        Exception exceptionForNotSetValues = null;

        
        while (setInstIterator.hasNext()) {
            String instId = (String) setInstIterator.next();
            HashMap setOpsByCstic = (HashMap) setOpsByInstance.get(instId);
    
            // If there are more than just one cstics to delete, use the server command
            // DeleteCsticsValues instead of DeleteCsticValues
            boolean singleCsticSetOp = (setOpsByCstic.size() <= 1);
            ArrayList serverCommand = new ArrayList();

            ArrayList valuesOfInstanceToBeSet = new ArrayList();
    
            if (setOpsByCstic.size() > 0) {
    
                // Add the common part to the server command
                serverCommand.add(SCECommand.CONFIG_ID);
                serverCommand.add(m_Configuration.getId());
    
                // Now the dependent single part of the server command
                String valueNameConstant;
                if (singleCsticSetOp) {
                    serverCommand.add(SetCsticValues.INST_ID);
                    serverCommand.add(instId);
                    serverCommand.add(SetCsticValues.FORMAT_VALUE);
                    serverCommand.add(SetCsticValues.YES);
                    valueNameConstant = SetCsticValues.VALUE_NAME;
                } else {
                    serverCommand.add(SetCsticsValues.INST_ID);
                    serverCommand.add(instId);
                    serverCommand.add(SetCsticsValues.FORMAT_VALUE);
                    serverCommand.add(SetCsticsValues.YES);
                    valueNameConstant = SetCsticsValues.VALUE_NAMES;
                }
    
                // now generate the operative part of the server command
                Iterator setCsticIterator = setOpsByCstic.keySet().iterator();
                while (setCsticIterator.hasNext()) {
                    String csticName = (String) setCsticIterator.next();
                    if (singleCsticSetOp) {
                        serverCommand.add(SetCsticValues.CSTIC_NAME);
                        serverCommand.add(csticName);
                    }
                    ArrayList valuesToBeSet = (ArrayList) setOpsByCstic.get(csticName);
                    for (int i = 0; i < valuesToBeSet.size(); i++) {
                        CharacteristicValue value = (CharacteristicValue) valuesToBeSet.get(i);
                        if (!singleCsticSetOp) {
                            serverCommand.add(SetCsticsValues.CSTIC_NAMES);
                            serverCommand.add(csticName);
                            serverCommand.add(SetCsticsValues.FORMAT_VALUES);
                            serverCommand.add((value.isAdditionalValue()) ? SetCsticsValues.YES : SetCsticsValues.NO);
                        } else {
                            serverCommand.add(SetCsticValues.FORMAT_VALUES);
                            serverCommand.add((value.isAdditionalValue()) ? SetCsticValues.YES : SetCsticValues.NO);
                        }
                        serverCommand.add(valueNameConstant);
                        serverCommand.add(value.getName());
                        valuesOfInstanceToBeSet.add(value.getName());                        
                    }
                }
            }
    
            // Now execute the server command
            try {
                ServerResponse r =
                    ((TCPDefaultClient)m_CachingClient).doCmd(
                        (singleCsticSetOp)
                            ? ISPCCommandSet.SET_CSTIC_VALUES
                            : ISPCCommandSet.SET_CSTICS_VALUES,
                        convertListToStringArray(serverCommand));
            } catch (IPCException e) {
                notSetValuesByInstance.put(instId, valuesOfInstanceToBeSet);
                exceptionForNotSetValues = e;
            }
			catch (ClientException e) {
						   notSetValuesByInstance.put(instId, valuesOfInstanceToBeSet);
						   exceptionForNotSetValues = e;
					   }
			}
        // check whether values for a deleted instances could not be set
        if (notSetValuesByInstance.size()>0){
            throw new IPCSetValuesException(exceptionForNotSetValues, notSetValuesByInstance);
        }

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigurationChangeStream#executeDeleteCommand(java.util.HashMap)
     */
    protected void executeDeleteCommand(HashMap deleteOpsByInstance) {
        Iterator delInstIterator = deleteOpsByInstance.keySet().iterator();
        while (delInstIterator.hasNext()) {
            String instId = (String) delInstIterator.next();
            HashMap deleteOpsByCstic = (HashMap) deleteOpsByInstance.get(instId);
    
            // If there are more than just one cstics to delete, use the server command
            // DeleteCsticsValues instead of DeleteCsticValues
            boolean singleCsticDeleteOp = (deleteOpsByCstic.size() <= 1);
            ArrayList serverCommand = new ArrayList();
    
            if (deleteOpsByCstic.size() > 0) {
    
                // Add the common part to the server command
                serverCommand.add(SCECommand.CONFIG_ID);
                serverCommand.add(m_Configuration.getId());
    
                // Now the dependent single part of the server command
                String valueNameConstant;
                if (singleCsticDeleteOp) {
                    serverCommand.add(DeleteCsticValues.INST_ID);
                    serverCommand.add(instId);
                    serverCommand.add(DeleteCsticValues.FORMAT_VALUE);
                    serverCommand.add(DeleteCsticValues.YES);
                    valueNameConstant = DeleteCsticValues.VALUE_NAME;
                } else {
                    serverCommand.add(DeleteCsticsValues.INST_ID);
                    serverCommand.add(instId);
                    serverCommand.add(DeleteCsticsValues.FORMAT_VALUE);
                    serverCommand.add(DeleteCsticsValues.YES);
                    valueNameConstant = DeleteCsticsValues.VALUE_NAMES;
                }
    
                // now generate the operative part of the server command
                Iterator delCsticIterator = deleteOpsByCstic.keySet().iterator();
                while (delCsticIterator.hasNext()) {
                    String csticName = (String) delCsticIterator.next();
                    if (singleCsticDeleteOp) {
                        serverCommand.add(DeleteCsticValues.CSTIC_NAME);
                        serverCommand.add(csticName);
                    }
                    ArrayList valuesToBeDeleted = (ArrayList) deleteOpsByCstic.get(csticName);
                    for (int i = 0; i < valuesToBeDeleted.size(); i++) {
                        CharacteristicValue value = (CharacteristicValue) valuesToBeDeleted.get(i);
                        if (!singleCsticDeleteOp) {
                            serverCommand.add(DeleteCsticsValues.CSTIC_NAMES);
                            serverCommand.add(csticName);
                            serverCommand.add(DeleteCsticsValues.FORMAT_VALUES);
                            serverCommand.add(
                                (value.isAdditionalValue())
                                    ? DeleteCsticsValues.YES
                                    : DeleteCsticsValues.NO);
                        } else {
                            serverCommand.add(DeleteCsticValues.FORMAT_VALUES);
                            serverCommand.add(
                                (value.isAdditionalValue())
                                    ? DeleteCsticValues.YES
                                    : DeleteCsticValues.NO);
                        }
                        serverCommand.add(valueNameConstant);
                        serverCommand.add(value.getName());
                    }
                }
            }
    
            // Now execute the server command
            try {
                ServerResponse r =
				((TCPDefaultClient)m_CachingClient).doCmd(
                        (singleCsticDeleteOp)
                            ? ISPCCommandSet.DELETE_CSTIC_VALUES
                            : ISPCCommandSet.DELETE_CSTICS_VALUES,
                        convertListToStringArray(serverCommand));
            } catch (ClientException e) {
                e.printStackTrace(System.out);
                throw new IPCException(e);
            }
        }
    }
}
