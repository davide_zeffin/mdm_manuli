package com.sap.spc.remote.client.object.imp.rfc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.ValueDescriptionPairs;
import com.sap.spc.remote.client.object.imp.DefaultIPCPricingCondition;
import com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultValueDescriptionPairs;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.rfc.function.SpcAddPricingConditions;
import com.sap.spc.remote.client.rfc.function.SpcChangePricingConditions;
import com.sap.spc.remote.client.rfc.function.SpcGetDocumentInfo;
import com.sap.spc.remote.client.rfc.function.SpcGetPricingConditions;
import com.sap.spc.remote.client.rfc.function.SpcRemovePricingConditions;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class RfcDefaultIPCPricingConditionSet
    extends DefaultIPCPricingConditionSet
    implements IPCPricingConditionSet {
        
        private static final Category category = ResourceAccessor.category;
        private static final Location location = ResourceAccessor.getLocation(RfcDefaultIPCDocument.class);

        //Constructors
        /**
        * Creates a set of pricing conditions
        */
        protected RfcDefaultIPCPricingConditionSet(IPCDocument document){
            this(document, null, true);
        }

        protected RfcDefaultIPCPricingConditionSet(IPCItem item){
            this(item.getDocument(), item, false);
        }

        protected RfcDefaultIPCPricingConditionSet(IPCDocument document, IPCItem item, boolean isHeader){
            _document = document;
            _item = item;
            _session = document.getSession();
            // 20020315-kha: new "all or nothing" client.object implementation to clean up API (cf IPCClient internal comment)
            if (!(_session instanceof DefaultIPCSession))
                throw new IllegalClassException(_session, DefaultIPCSession.class);
            _client = ((RfcDefaultIPCSession)_session).getClient();
            _messages = new ArrayList();
            _conditionsToBeAdded = new ArrayList();
            _conditionsToBeRemoved = new ArrayList();
            _isHeader = isHeader;
        }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_getExternalPricingConditions()
     */
    protected HashMap _getExternalPricingConditions() {
        HashMap data = new HashMap();
        //get item conditions
        try{
            String numerator;
            String pricingDate;
            String roundingDiff;
            String changeOfValue;
            String condUpdate;
            String condInactive;
            String application;
            String usage;
            String stepNumber;
            String condValue;
            String itemId;
            String manualEntryFlag;
            String varcondFlag;
            String condRate;
            String changeOfUnit;
            String varcondKey;
            String condTypeDescr;
            String headerCounter;
            String periodFactor;
            String calcType;
            String condTableName;
            String interCompany;
            String condCategory;
            String accrualFlag;
            String scaleBaseValue;
            String structureCondition;
            String accountKey2;
            String condOrigin;
            String accountKey1;
            String condCounter;
            String condClass;
            String condCurrency;
            String condControl;
            String groupCondition;
            String changeOfCalcType;
            String scaleCurrency;
            String condUnitValue;
            String condRecordId;
            String scaleUnit;
            String exchRate;
            String purpose;
            String scaleType;
            String condType;
            String varcondFactor;
            String documentId;
            String condUnit;
            String invoiceList;
            String accessCounter;
            String salesTaxCode;
            String denominator;
            String statistical;
            String changeOfConvFactors;
            String datasource;
            String scaleBaseType;
            String changeOfRate;
            String exponent;
            String varcondDescr;
            String withholdingTaxCode;
            String condBase;
            String manuallyChanged;
          
            JCO.Function functionSpcGetPricingConditions = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_PRICING_CONDITIONS);
            JCO.ParameterList importsSpcGetPricingConditions = functionSpcGetPricingConditions.getImportParameterList();
            JCO.ParameterList exportsSpcGetPricingConditions = functionSpcGetPricingConditions.getExportParameterList();
          
            JCO.Table importsSpcGetPricingConditionsSpcgetpricingconditions_itItemId = importsSpcGetPricingConditions.getTable(SpcGetPricingConditions.IT_ITEM_ID);
            importsSpcGetPricingConditionsSpcgetpricingconditions_itItemId.appendRow();
            importsSpcGetPricingConditionsSpcgetpricingconditions_itItemId.setValue(getItemId(), "");     // BYTE, Item Number
          
            importsSpcGetPricingConditions.setValue(getDocumentId(), SpcGetPricingConditions.IV_DOCUMENT_ID);     // BYTE, Pricing Document GUID
            importsSpcGetPricingConditions.setValue("X", SpcGetPricingConditions.IV_INCL_SUBTOTALS);      // CHAR, Save Pricing Document With Subtotals

            category.logT(Severity.PATH, location, "executing RFC SPC_GET_PRICING_CONDITIONS");
            ((RFCDefaultClient)_client).execute(functionSpcGetPricingConditions); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_GET_PRICING_CONDITIONS");

            JCO.Table exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition = exportsSpcGetPricingConditions.getTable(SpcGetPricingConditions.ET_CONDITION);
            String key;
            for (int i=0; i<exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getNumRows(); i++) {
                exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.setRow(i);
                documentId = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.DOCUMENT_ID);
                itemId = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.ITEM_ID);
                if (getDocumentId().equals(documentId) && getItemId().equals(itemId)){ //Michael2: Is this check ok?
                    stepNumber = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.STEP_NUMBER);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.STEP_NUMBER + "[" + (i+1) +"]";
                    data.put(key, stepNumber);
                    condCounter = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_COUNTER);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_COUNTER + "[" + (i+1) +"]";
                    data.put(key, condCounter);
                    application = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.APPLICATION);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.APPLICATION + "[" + (i+1) +"]";
                    data.put(key, application);
                    usage = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.USAGE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.USAGE + "[" + (i+1) +"]";
                    data.put(key, usage);
                    condType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_TYPE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_TYPE + "[" + (i+1) +"]";
                    data.put(key, condType);
                    condTypeDescr = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_TYPE_DESCR);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_TYPE_DESCR + "[" + (i+1) +"]";
                    data.put(key, condTypeDescr);
                    purpose = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.PURPOSE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.PURPOSE + "[" + (i+1) +"]";
                    data.put(key, purpose);
                    datasource = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.DATASOURCE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.DATASOURCE + "[" + (i+1) +"]";
                    data.put(key, datasource);
                    condRate = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_RATE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_RATE + "[" + (i+1) +"]";
                    data.put(key, condRate);
                    condCurrency = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_CURRENCY);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_CURRENCY + "[" + (i+1) +"]";
                    data.put(key, condCurrency);
                    condUnitValue = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_UNIT_VALUE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_UNIT_VALUE + "[" + (i+1) +"]";
                    data.put(key, condUnitValue);
                    condUnit = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_UNIT);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_UNIT + "[" + (i+1) +"]";
                    data.put(key, condUnit);
                    condValue = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_VALUE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_VALUE + "[" + (i+1) +"]";
                    data.put(key, condValue);
                    exchRate = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.EXCH_RATE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.EXCH_RATE + "[" + (i+1) +"]";
                    data.put(key, exchRate);
                    numerator = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.NUMERATOR);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.NUMERATOR + "[" + (i+1) +"]";
                    data.put(key, numerator);
                    denominator = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.DENOMINATOR);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.DENOMINATOR + "[" + (i+1) +"]";
                    data.put(key, denominator);
                    exponent = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.EXPONENT);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.EXPONENT + "[" + (i+1) +"]";
                    data.put(key, exponent);
                    manuallyChanged = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.MANUALLY_CHANGED);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.MANUALLY_CHANGED + "[" + (i+1) +"]";
                    data.put(key, manuallyChanged);
                    statistical = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.STATISTICAL);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.STATISTICAL + "[" + (i+1) +"]";
                    data.put(key, statistical);
                    calcType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CALC_TYPE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.CALC_TYPE + "[" + (i+1) +"]";
                    data.put(key, calcType);
                    pricingDate = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.PRICING_DATE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.PRICING_DATE + "[" + (i+1) +"]";
                    data.put(key, pricingDate);
                    condBase = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_BASE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_BASE + "[" + (i+1) +"]";
                    data.put(key, condBase);
                    condCategory = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_CATEGORY);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_CATEGORY + "[" + (i+1) +"]";
                    data.put(key, condCategory);
                    scaleType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.SCALE_TYPE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.SCALE_TYPE + "[" + (i+1) +"]";
                    data.put(key, scaleType);
                    accrualFlag = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.ACCRUAL_FLAG);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.ACCRUAL_FLAG + "[" + (i+1) +"]";
                    data.put(key, accrualFlag);
                    invoiceList = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.INVOICE_LIST);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.INVOICE_LIST + "[" + (i+1) +"]";
                    data.put(key, invoiceList);
                    condOrigin = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_ORIGIN);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_ORIGIN + "[" + (i+1) +"]";
                    data.put(key, condOrigin);
                    groupCondition = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.GROUP_CONDITION);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.GROUP_CONDITION + "[" + (i+1) +"]";
                    data.put(key, groupCondition);
                    condUpdate = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_UPDATE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_UPDATE + "[" + (i+1) +"]";
                    data.put(key, condUpdate);
                    accessCounter = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.ACCESS_COUNTER);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.ACCESS_COUNTER + "[" + (i+1) +"]";
                    data.put(key, accessCounter);
                    condRecordId = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition./*getByteArray*/getString(SpcGetPricingConditions.COND_RECORD_ID);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_RECORD_ID + "[" + (i+1) +"]";
                    data.put(key, condRecordId);
                    accountKey1 = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.ACCOUNT_KEY_1);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.ACCOUNT_KEY_1 + "[" + (i+1) +"]";
                    data.put(key, accountKey1);
                    accountKey2 = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.ACCOUNT_KEY_2);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.ACCOUNT_KEY_2 + "[" + (i+1) +"]";
                    data.put(key, accountKey2);
                    roundingDiff = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.ROUNDING_DIFF);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.ROUNDING_DIFF + "[" + (i+1) +"]";
                    data.put(key, roundingDiff);
                    condControl = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_CONTROL);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_CONTROL + "[" + (i+1) +"]";
                    data.put(key, condControl);
                    condInactive = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_INACTIVE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_INACTIVE + "[" + (i+1) +"]";
                    data.put(key, condInactive);
                    condClass = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_CLASS);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_CLASS + "[" + (i+1) +"]";
                    data.put(key, condClass);
                    headerCounter = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.HEADER_COUNTER);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.HEADER_COUNTER + "[" + (i+1) +"]";
                    data.put(key, headerCounter);
                    varcondFactor = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.VARCOND_FACTOR);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.VARCOND_FACTOR + "[" + (i+1) +"]";
                    data.put(key, varcondFactor);
                    scaleBaseType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.SCALE_BASE_TYPE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.SCALE_BASE_TYPE + "[" + (i+1) +"]";
                    data.put(key, scaleBaseType);
                    scaleBaseValue = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.SCALE_BASE_VALUE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.SCALE_BASE_VALUE + "[" + (i+1) +"]";
                    data.put(key, scaleBaseValue);
                    scaleUnit = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.SCALE_UNIT);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.SCALE_UNIT + "[" + (i+1) +"]";
                    data.put(key, scaleUnit);
                    scaleCurrency = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.SCALE_CURRENCY);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.SCALE_CURRENCY + "[" + (i+1) +"]";
                    data.put(key, scaleCurrency);
                    interCompany = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.INTER_COMPANY);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.INTER_COMPANY + "[" + (i+1) +"]";
                    data.put(key, interCompany);
                    varcondFlag = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.VARCOND_FLAG);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.VARCOND_FLAG + "[" + (i+1) +"]";
                    data.put(key, varcondFlag);
                    varcondKey = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.VARCOND_KEY);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.VARCOND_KEY + "[" + (i+1) +"]";
                    data.put(key, varcondKey);
                    varcondDescr = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.VARCOND_DESCR);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.VARCOND_DESCR + "[" + (i+1) +"]";
                    data.put(key, varcondDescr);
                    salesTaxCode = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.SALES_TAX_CODE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.SALES_TAX_CODE + "[" + (i+1) +"]";
                    data.put(key, salesTaxCode);
                    withholdingTaxCode = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.WITHHOLDING_TAX_CODE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.WITHHOLDING_TAX_CODE + "[" + (i+1) +"]";
                    data.put(key, withholdingTaxCode);
                    structureCondition = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.STRUCTURE_CONDITION);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.STRUCTURE_CONDITION + "[" + (i+1) +"]";
                    data.put(key, structureCondition);
                    periodFactor = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.PERIOD_FACTOR);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.PERIOD_FACTOR + "[" + (i+1) +"]";
                    data.put(key, periodFactor);
                    condTableName = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_TABLE_NAME);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.COND_TABLE_NAME + "[" + (i+1) +"]";
                    data.put(key, condTableName);
                    manualEntryFlag = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.MANUAL_ENTRY_FLAG);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.MANUAL_ENTRY_FLAG + "[" + (i+1) +"]";
                    data.put(key, manualEntryFlag);
                    changeOfRate = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_RATE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.CHANGE_OF_RATE + "[" + (i+1) +"]";
                    data.put(key, changeOfRate);
                    changeOfUnit = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_UNIT);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.CHANGE_OF_UNIT + "[" + (i+1) +"]";
                    data.put(key, changeOfUnit);
                    changeOfValue = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_VALUE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.CHANGE_OF_VALUE + "[" + (i+1) +"]";
                    data.put(key, changeOfValue);
                    changeOfCalcType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_CALC_TYPE);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.CHANGE_OF_CALC_TYPE + "[" + (i+1) +"]";
                    data.put(key, changeOfCalcType);
                    changeOfConvFactors = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_CONV_FACTORS);
                    key = SpcGetPricingConditions.COND_PREFIX + SpcGetPricingConditions.CHANGE_OF_CONV_FACTORS + "[" + (i+1) +"]";
                    data.put(key, changeOfConvFactors);
                }else{
                    //Interested on only one item.
                    continue;
                }
            }
        }
        catch(ClientException e){
            throw new IPCException(e);
        }
        return data;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_retrieveConditionsFromServer()
     */
    protected void _retrieveConditionsFromServer() throws IPCException {
        //get conditions
        try{
            _conditions = new ArrayList();
            String numerator;
            String changeOfValue = null;
            String condInactive = null;
            String stepNumber = null;
            String condValue = null;
            String varcondFlag;
            String condRate = null;
            String changeOfUnit = null;
            String varcondKey = null;
            String condTypeDescr;
            String headerCounter = null;
            String periodFactor;
            String calcType;
            String condCategory = null;
            String condOrigin = null;
            String condCounter = null;
            String condClass = null;
            String condCurrency = null;
            String condControl = null;
            String changeOfCalcType;
            String condUnitValue = null;
            String scaleType = null;
            String condType = null;
            String varcondFactor = null;
            String condUnit = null;
            String denominator;
            String statistical;
            String changeOfRate = null;
            String ivForPrinting = "";
            String exponent;
            String varcondDescr;
            String condBase;

            JCO.Function functionSpcGetPricingConditions = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_PRICING_CONDITIONS);
            JCO.ParameterList importsSpcGetPricingConditions = functionSpcGetPricingConditions.getImportParameterList();
            JCO.ParameterList exportsSpcGetPricingConditions = functionSpcGetPricingConditions.getExportParameterList();

            JCO.Table importsSpcGetPricingConditionsSpcgetpricingconditions_itItemId = importsSpcGetPricingConditions.getTable(SpcGetPricingConditions.IT_ITEM_ID);
            importsSpcGetPricingConditionsSpcgetpricingconditions_itItemId.appendRow();
            importsSpcGetPricingConditionsSpcgetpricingconditions_itItemId.setValue(getItemId(), "");     // BYTE, Item Number
            importsSpcGetPricingConditions.setValue(getDocumentId(), SpcGetPricingConditions.IV_DOCUMENT_ID);     // BYTE, Pricing Document GUID
			importsSpcGetPricingConditions.setValue("X", SpcGetPricingConditions.IV_INCL_SUBTOTALS);      // CHAR, Save Pricing Document With Subtotals
            importsSpcGetPricingConditions.setValue(ivForPrinting, SpcGetPricingConditions.IV_FOR_PRINTING);      // CHAR, Checkbox
            
            category.logT(Severity.PATH, location, "executing RFC SPC_GET_PRICING_CONDITIONS");
            ((RFCDefaultClient)_client).execute(functionSpcGetPricingConditions); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_GET_PRICING_CONDITIONS");

            JCO.Table exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition = exportsSpcGetPricingConditions.getTable(SpcGetPricingConditions.ET_CONDITION);
            int numOfRows = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getNumRows();

            //create IPCPricingConditions and add them to this Set
            String[] stepNos = new String[numOfRows];
            String[] counters = new String[numOfRows];
            String[] headerCounters = new String[numOfRows];
            String[] conditionTypeNames = new String[numOfRows];
            String[] descriptions = new String[numOfRows];
            String[] conditionRates = new String[numOfRows];
            String[] conditionCurrencies = new String[numOfRows];
            String[] conditionPricingUnitValues = new String[numOfRows];
            String[] conditionPricingUnitUnits = new String[numOfRows];
            String[] conditionPricingUnitDimensionNames = new String[numOfRows];
            String[] conditionValues = new String[numOfRows];
            String[] documentCurrencies = new String[numOfRows];
            String[] conversionNumerators = new String[numOfRows];
            String[] conversionDenominators = new String[numOfRows];
            String[] conversionExponents = new String[numOfRows];
            String[] conditionBaseValues = new String[numOfRows];

            String[] exchangeRatesExtRepresentation = new String[numOfRows];
            String[] directExchangeRatesExtRepresentation = new String[numOfRows];
            String[] isDirectExchangeRates = new String[numOfRows];
            String[] factors = new String[numOfRows];
            String[] varcondFlags = new String[numOfRows];
            String[] variantCondKeys = new String[numOfRows];
            String[] variantFactors = new String[numOfRows];
            String[] variantCondDescriptions = new String[numOfRows];

            String[] indicatorStatisticals = new String[numOfRows];
            String[] conditionIsInactives = new String[numOfRows];
            String[] conditionClasses = new String[numOfRows];
            String[] calculationTypes = new String[numOfRows];
            String[] conditionCategories = new String[numOfRows];
            String[] conditionControls = new String[numOfRows];
            String[] conditionOrigins = new String[numOfRows];
            String[] scaleTypes = new String[numOfRows];

            String[] changeOfRatesAllowed = new String[numOfRows];
            String[] changeOfUnitsAllowed = new String[numOfRows];
            String[] changeOfValuesAllowed = new String[numOfRows];
            String[] deletionAllowed = new String[numOfRows];

            for (int i=0; i<numOfRows; i++) {
                exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.setRow(i);
                stepNumber = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.STEP_NUMBER); 
                condCounter = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_COUNTER);
                condType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_TYPE);
                condTypeDescr = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_TYPE_DESCR);
                condRate = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_RATE);
                condCurrency = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_CURRENCY);
                condUnitValue = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_UNIT_VALUE);
                condUnit = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_UNIT);
                condValue = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_VALUE);
                numerator = /*(Integer)*/exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition./*getValue*/getString(SpcGetPricingConditions.NUMERATOR);
                denominator = /*(Integer)*/exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition./*getValue*/getString(SpcGetPricingConditions.DENOMINATOR);
                exponent = /*(Integer)*/exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition./*getValue*/getString(SpcGetPricingConditions.EXPONENT);
                statistical = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.STATISTICAL);
                calcType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CALC_TYPE);
                condBase = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_BASE);
                condCategory = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_CATEGORY);
                scaleType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.SCALE_TYPE);
                condOrigin = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_ORIGIN);
                condControl = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_CONTROL);
                condInactive = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_INACTIVE);
                condClass = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.COND_CLASS);
                headerCounter = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.HEADER_COUNTER); 
                varcondFactor = /*(Double)*/exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition./*getValue*/getString(SpcGetPricingConditions.VARCOND_FACTOR);
                varcondFlag = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.VARCOND_FLAG);
                varcondKey = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.VARCOND_KEY);
                varcondDescr = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.VARCOND_DESCR);
                periodFactor = /*(Double)*/exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition./*getValue*/getString(SpcGetPricingConditions.PERIOD_FACTOR);
                changeOfRate = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_RATE);
                changeOfUnit = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_UNIT);
                changeOfValue = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_VALUE);
                changeOfCalcType = exportsSpcGetPricingConditionsSpcgetpricingconditions_etCondition.getString(SpcGetPricingConditions.CHANGE_OF_CALC_TYPE);

                stepNos[i] = stepNumber;
                counters[i] = condCounter; 
                headerCounters[i] = headerCounter;
                
                conditionTypeNames[i] = condType;
                descriptions[i] = condTypeDescr; 
                conditionRates[i] = condRate;
                conditionCurrencies[i] = condCurrency;
                conditionPricingUnitValues[i] = condUnitValue;
                conditionPricingUnitUnits[i] = condUnit;
                conditionPricingUnitDimensionNames[i] = "NOT_AVAILABLE_IN_50_REMOVE_LATER"; 
                conditionValues[i] = condValue;
                documentCurrencies[i] = _document.getDocumentCurrency();
                conversionNumerators[i] = numerator;
                conversionDenominators[i] = denominator;
                conversionExponents[i] = exponent;
                conditionBaseValues[i] = condBase;

                exchangeRatesExtRepresentation[i] = "NOT_AVAILABLE_IN_50_REMOVE_LATER";
                directExchangeRatesExtRepresentation[i] = "NOT_AVAILABLE_IN_50_REMOVE_LATER";
                isDirectExchangeRates[i] = "NOT_AVAILABLE_IN_50_REMOVE_LATER";
                factors[i] = periodFactor;
                varcondFlags[i] = varcondFlag;
                variantCondKeys[i] = varcondKey;
                variantFactors[i] = varcondFactor;
                variantCondDescriptions[i] = varcondDescr;
    
                indicatorStatisticals[i] = statistical;
                conditionIsInactives[i] = condInactive;
                conditionClasses[i] = condClass;
                calculationTypes[i] = calcType;
                conditionCategories[i] = condCategory;
                conditionControls[i] = condControl;
                conditionOrigins[i] = condOrigin;
                scaleTypes[i] = scaleType;
    
                changeOfRatesAllowed[i] = changeOfRate;
                changeOfUnitsAllowed[i] = changeOfUnit;
                changeOfValuesAllowed[i] = changeOfValue;
                deletionAllowed[i] = "NOT_AT_AVAILABLE_CONTACT_D025429";//As per Michael Gs mail on 18th May 05.
            }
            for (int i = 0; i < stepNos.length; i++){
                _conditions.add(new RfcDefaultIPCPricingCondition(
                     this,
                     formatStepNo(stepNos[i]),
                     formatCounter(counters[i]),
                     formatCounter(headerCounters[i]),
                     conditionTypeNames[i],
                     descriptions[i],
                     conditionRates[i],
                     conditionCurrencies[i],
                     conditionPricingUnitValues[i],
                     conditionPricingUnitUnits[i],
                     conditionPricingUnitDimensionNames[i],
                     conditionValues[i],
                     documentCurrencies[i],
                     conversionNumerators[i],
                     conversionDenominators[i],
                     conversionExponents[i],
                     conditionBaseValues[i],

                     exchangeRatesExtRepresentation[i],
                     directExchangeRatesExtRepresentation[i],
                     isDirectExchangeRates[i].equalsIgnoreCase("X"),
                     factors[i],
                     varcondFlags[i].equalsIgnoreCase("X"),
                     variantCondKeys[i],
                     variantFactors[i],
                     variantCondDescriptions[i],
                     indicatorStatisticals[i].equalsIgnoreCase("X"),
                     ((conditionIsInactives[i] == null) || (conditionIsInactives[i].equals("")))?' ':conditionIsInactives[i].charAt(0),
                     ((conditionClasses[i] == null) || (conditionClasses[i].equals("")))?' ':conditionClasses[i].charAt(0),
                     ((calculationTypes[i] == null) || (calculationTypes[i].equals("")))?' ':calculationTypes[i].charAt(0),
                     ((conditionCategories[i] == null) || (conditionCategories[i].equals("")))?' ':conditionCategories[i].charAt(0),
                     ((conditionControls[i] == null) || (conditionControls[i].equals("")))?' ':conditionControls[i].charAt(0),
                     ((conditionOrigins[i] == null) || (conditionOrigins[i].equals("")))?' ':conditionOrigins[i].charAt(0),
                     ((scaleTypes[i] == null) || (scaleTypes[i].equals("")))?' ':scaleTypes[i].charAt(0),

                     changeOfRatesAllowed[i].equalsIgnoreCase("X"),
                     changeOfUnitsAllowed[i].equalsIgnoreCase("X"),
                     changeOfValuesAllowed[i].equalsIgnoreCase("X"),
                     deletionAllowed[i].equalsIgnoreCase("X")
                ));
            }
            setOutOfSync();
        }catch(ClientException e){
            throw new IPCException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#addPricingCondition(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public void addPricingCondition(
        String conditionTypeName,
        String conditionRate,
        String conditionCurrency,
        String conditionPricingUnitValue,
        String conditionPricingUnitUnit,
        String conditionValue,
        String decimalSeparator,
        String groupingSeparator) {
            //invalidate condition list - conditions will be retrieved from server
            setOutOfSync();
            BigDecimal internalConditionRate;
            String internalConditionRateString;
            RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_session).getPricingConverter();
            if (conditionRate != null) {
                internalConditionRate = pc.convertCurrencyValueExternalToInternal(conditionRate, conditionCurrency, getLanguage(), decimalSeparator, groupingSeparator);
                internalConditionRateString = internalConditionRate.toString();
            }else {
                internalConditionRate = null;
                internalConditionRateString = "";
            }
            BigDecimal internalConditionValue;
            String internalConditionValueString;
            if (conditionValue != null) {
                internalConditionValue = pc.convertCurrencyValueExternalToInternal(conditionValue, conditionCurrency, getLanguage(), decimalSeparator, groupingSeparator);
                internalConditionValueString = internalConditionValue.toString();
            }else {
                internalConditionValue = null;
                internalConditionValueString = "";
            }
            _conditionsToBeAdded.addAll(Arrays.asList(new String[] {
                SpcAddPricingConditions.IV_DOCUMENT_ID, getDocumentId(),
                SpcAddPricingConditions.ITEM_ID, getItemId(),
                SpcAddPricingConditions.COND_TYPE, conditionTypeName,
                SpcAddPricingConditions.COND_RATE, internalConditionRateString,
                SpcAddPricingConditions.COND_CURRENCY, conditionCurrency,
                SpcAddPricingConditions.COND_UNIT_VALUE, conditionPricingUnitValue,
                SpcAddPricingConditions.COND_UNIT, conditionPricingUnitUnit,
                SpcAddPricingConditions.COND_VALUE, internalConditionValueString
            }));
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_executeAdditions()
     */
    protected StepIdSet _executeAdditions() throws IPCException {
        if (_conditionsToBeAdded.size() == 0){
            return null;
        }
        String[] params = new String[_conditionsToBeAdded.size()];
        _conditionsToBeAdded.toArray(params);
        try {        
            JCO.Function functionSpcAddPricingConditions = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_ADD_PRICING_CONDITIONS);
            JCO.ParameterList im = functionSpcAddPricingConditions.getImportParameterList();
            JCO.ParameterList ex = functionSpcAddPricingConditions.getExportParameterList();

            JCO.Table imSpcaddpricingconditions_itCondition = im.getTable(SpcAddPricingConditions.IT_CONDITION);
            String ivDocumentId = null;
            String ivProcessMode = null;
            for (int i=0; i<params.length; i=i+2) {
                // only add someting to the import-parameters if the value is not null
                if (params[i+1] != null){
                    if (params[i].equals(SpcAddPricingConditions.IV_DOCUMENT_ID)){
                        ivDocumentId = params[i+1];
                    }else if (params[i].equals(SpcAddPricingConditions.IV_PROCESS_MODE)){
                        ivProcessMode = params[i+1];
                    }else if (params[i].equals(SpcAddPricingConditions.ITEM_ID)){
                        imSpcaddpricingconditions_itCondition.appendRow();
                        imSpcaddpricingconditions_itCondition.setValue(params[i+1],params[i]);
                    }else if (params[i].equals(SpcAddPricingConditions.COND_RATE)){
                        imSpcaddpricingconditions_itCondition.setValue(SpcAddPricingConditions.XFLAG,SpcAddPricingConditions.XCOND_RATE);
                        imSpcaddpricingconditions_itCondition.setValue(params[i+1],params[i]);
                    }else if (params[i].equals(SpcAddPricingConditions.COND_BASE)){
                        imSpcaddpricingconditions_itCondition.setValue(SpcAddPricingConditions.XFLAG,SpcAddPricingConditions.XCOND_BASE);
                        imSpcaddpricingconditions_itCondition.setValue(params[i+1],params[i]);
                    }else if (params[i].equals(SpcAddPricingConditions.COND_VALUE)){
                        imSpcaddpricingconditions_itCondition.setValue(SpcAddPricingConditions.XFLAG,SpcAddPricingConditions.XCOND_VALUE);
                        imSpcaddpricingconditions_itCondition.setValue(params[i+1],params[i]);
                    }else if (params[i].equals(SpcAddPricingConditions.ACCRUAL_FLAG)){
                        imSpcaddpricingconditions_itCondition.setValue(SpcAddPricingConditions.XFLAG,SpcAddPricingConditions.XACCRUAL_FLAG);
                        imSpcaddpricingconditions_itCondition.setValue(params[i+1],params[i]);
                    }else{
                        imSpcaddpricingconditions_itCondition.setValue(params[i+1],params[i]);
                    }
                }
            }
            im.setValue(ivDocumentId, SpcAddPricingConditions.IV_DOCUMENT_ID);      // BYTE, Pricing Document GUID
            im.setValue(ivProcessMode, SpcAddPricingConditions.IV_PROCESS_MODE);        // CHAR, Processing Mode

            category.logT(Severity.PATH, location, "executing RFC SPC_ADD_PRICING_CONDITIONS");
            ((RFCDefaultClient)_client).execute(functionSpcAddPricingConditions); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_ADD_PRICING_CONDITIONS");

            JCO.Structure exSpcaddpricingconditions_esProfile = ex.getStructure(SpcAddPricingConditions.ES_PROFILE);
            JCO.Table exSpcaddpricingconditions_esProfileSpcaddpricingconditions_messages = exSpcaddpricingconditions_esProfile.getTable(SpcAddPricingConditions.MESSAGES);
            int rowCnt = exSpcaddpricingconditions_esProfileSpcaddpricingconditions_messages.getNumRows();
            _conditionsToBeAdded.clear();
            String[] message = new String[rowCnt];
            String[] messageType = new String[rowCnt];
            String[] objectId = new String[rowCnt];
            String[] septNumber = new String[rowCnt];
            String[] lineId = new String[rowCnt];

            for (int i=0; i<rowCnt; i++) {
                exSpcaddpricingconditions_esProfileSpcaddpricingconditions_messages.setRow(i);
                lineId[i] = ((Integer)exSpcaddpricingconditions_esProfileSpcaddpricingconditions_messages.getValue(SpcAddPricingConditions.LINE_ID)).toString(); //Michael:
                messageType[i] = exSpcaddpricingconditions_esProfileSpcaddpricingconditions_messages.getString(SpcAddPricingConditions.TYPE);
                objectId[i] = exSpcaddpricingconditions_esProfileSpcaddpricingconditions_messages.getString(SpcAddPricingConditions.ID);
                septNumber[i] = exSpcaddpricingconditions_esProfileSpcaddpricingconditions_messages.getBigDecimal(SpcAddPricingConditions.NUMBER).toString(); //Michael:
                message[i] = exSpcaddpricingconditions_esProfileSpcaddpricingconditions_messages.getString(SpcAddPricingConditions.MESSAGE);
            }
            _messages.add(new DefaultIPCMessageSet(objectId, messageType, message));
            return new StepIdSet(septNumber, lineId);
        } catch (ClientException e) {
            throw new IPCException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#removePricingCondition(java.lang.String, java.lang.String)
     */
    public void removePricingCondition(String stepNo, String counter) {
        //invalidate condition list - conditions will be retrieved from server
        setOutOfSync();
        _conditionsToBeRemoved.addAll(Arrays.asList(new String[] {
            SpcRemovePricingConditions.IV_DOCUMENT_ID, getDocumentId(),
            SpcRemovePricingConditions.ITEM_ID, getItemId(),
            SpcRemovePricingConditions.STEP_NUMBER, stepNo,
            SpcRemovePricingConditions.COND_COUNTER, counter}));
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_executeRemovals()
     */
    public void _executeRemovals() throws IPCException {
        if (_conditionsToBeRemoved.size() == 0)
            return;
        String[] params = new String[_conditionsToBeRemoved.size()];
        _conditionsToBeRemoved.toArray(params);
        try{
            JCO.Function functionSpcRemovePricingConditions = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_REMOVE_PRICING_CONDITIONS);
            JCO.ParameterList im = functionSpcRemovePricingConditions.getImportParameterList();
            JCO.ParameterList ex = functionSpcRemovePricingConditions.getExportParameterList();

            JCO.Table imSpcremovepricingconditions_itCondition = im.getTable(SpcRemovePricingConditions.IT_CONDITION);
            String ivDocumentId = null;
            String ivProcessMode = null;
            for (int i=0; i<params.length; i=i+2) {
                if (params[i+1] != null){
                    if (params[i].equals(SpcRemovePricingConditions.IV_DOCUMENT_ID)){
                        ivDocumentId = params[i+1];
                    }else if (params[i].equals(SpcRemovePricingConditions.IV_PROCESS_MODE)){
                        ivProcessMode = params[i+1];
                    }else if (params[i].equals(SpcRemovePricingConditions.ITEM_ID)){
                        imSpcremovepricingconditions_itCondition.appendRow();
                        imSpcremovepricingconditions_itCondition.setValue(params[i+1],params[i]);
                    }else{
                        imSpcremovepricingconditions_itCondition.setValue(params[i+1],params[i]);
                    }
                }
            }
            im.setValue(ivDocumentId, SpcRemovePricingConditions.IV_DOCUMENT_ID);     // BYTE, Pricing Document GUID
            im.setValue(ivProcessMode, SpcRemovePricingConditions.IV_PROCESS_MODE);       // CHAR, Processing Mode

            category.logT(Severity.PATH, location, "executing RFC SPC_REMOVE_PRICING_CONDITIONS");
            ((RFCDefaultClient)_client).execute(functionSpcRemovePricingConditions); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_REMOVE_PRICING_CONDITIONS");

            JCO.Structure exSpcremovepricingconditions_esProfile = ex.getStructure(SpcRemovePricingConditions.ES_PROFILE);
            JCO.Table exSpcremovepricingconditions_esProfileSpcremovepricingconditions_messages = exSpcremovepricingconditions_esProfile.getTable(SpcRemovePricingConditions.MESSAGES);
          
            int rowCnt = exSpcremovepricingconditions_esProfileSpcremovepricingconditions_messages.getNumRows();
            String[] message = new String[rowCnt];
            String[] messageType = new String[rowCnt];
            String[] objectId = new String[rowCnt];

            _conditionsToBeRemoved.clear();
            for (int i=0; i<rowCnt; i++) {
                exSpcremovepricingconditions_esProfileSpcremovepricingconditions_messages.setRow(i);
                messageType[i] = exSpcremovepricingconditions_esProfileSpcremovepricingconditions_messages.getString(SpcRemovePricingConditions.TYPE);
                objectId[i] = exSpcremovepricingconditions_esProfileSpcremovepricingconditions_messages.getString(SpcRemovePricingConditions.ID);
                message[i] = exSpcremovepricingconditions_esProfileSpcremovepricingconditions_messages.getString(SpcRemovePricingConditions.MESSAGE);
            }
            _messages.add(new DefaultIPCMessageSet(objectId, messageType, message));
        }catch(ClientException e){
            throw new IPCException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet#_executeChanges()
     */
    protected void _executeChanges() throws IPCException {
        if (!isOutOfSync())
            return;

        ArrayList params = new ArrayList();
        Iterator iterator = _conditions.iterator();
        while(iterator.hasNext()){
            DefaultIPCPricingCondition prCondition = (DefaultIPCPricingCondition)iterator.next();
            Collection condParams = prCondition.getChangeParameters();
            if (condParams.size() > 0){
                params.add(SpcChangePricingConditions.ITEM_ID);
                params.add(getItemId());
                params.add(SpcChangePricingConditions.STEP_NUMBER);
                params.add(prCondition.getStepNo());
                params.add(SpcChangePricingConditions.COND_COUNTER);
                params.add(prCondition.getCounter());
                params.addAll(condParams);
            }
        }
        if (params.size() == 0)
            return;
        params.add(SpcChangePricingConditions.IV_DOCUMENT_ID);
        params.add(getDocumentId());
        String[] paramsArray = new String[params.size()];
        params.toArray(paramsArray);
        try {
            
            JCO.Function functionSpcChangePricingConditions = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_CHANGE_PRICING_CONDITIONS);
            JCO.ParameterList importsSpcChangePricingConditions = functionSpcChangePricingConditions.getImportParameterList();
            JCO.ParameterList exportsSpcChangePricingConditions = functionSpcChangePricingConditions.getExportParameterList();

            JCO.Table importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition = importsSpcChangePricingConditions.getTable(SpcChangePricingConditions.IT_CONDITION);
            String ivDocumentId = null;
            String ivProcessMode = null;
            for (int i=0; i<paramsArray.length; i=i+2) {
                // only add someting to the import-parameters if the value is not null
                if (paramsArray[i+1] != null){
                    if (paramsArray[i].equals(SpcChangePricingConditions.IV_DOCUMENT_ID)){
                        ivDocumentId = paramsArray[i+1];
                    }else if (paramsArray[i].equals(SpcChangePricingConditions.IV_PROCESS_MODE)){
                        ivProcessMode = paramsArray[i+1];
                    }else if (paramsArray[i].equals(SpcChangePricingConditions.ITEM_ID)){
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.appendRow();
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.setValue(paramsArray[i+1],paramsArray[i]);
                    }else if (paramsArray[i].equals(SpcChangePricingConditions.COND_RATE)){
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.setValue(SpcChangePricingConditions.XFLAG,SpcChangePricingConditions.XCOND_RATE);
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.setValue(paramsArray[i+1],paramsArray[i]);
                    }else if (paramsArray[i].equals(SpcChangePricingConditions.COND_BASE)){
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.setValue(SpcChangePricingConditions.XFLAG,SpcChangePricingConditions.XCOND_BASE);
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.setValue(paramsArray[i+1],paramsArray[i]);
                    }else if (paramsArray[i].equals(SpcChangePricingConditions.COND_VALUE)){
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.setValue(SpcChangePricingConditions.XFLAG,SpcChangePricingConditions.XCOND_VALUE);
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.setValue(paramsArray[i+1],paramsArray[i]);
                    }else{
                        importsSpcChangePricingConditionsSpcchangepricingconditions_itCondition.setValue(paramsArray[i+1],paramsArray[i]);
                    }
                }
            }
            importsSpcChangePricingConditions.setValue(ivDocumentId, SpcChangePricingConditions.IV_DOCUMENT_ID);      // BYTE, Pricing Document GUID
            importsSpcChangePricingConditions.setValue(ivProcessMode, SpcChangePricingConditions.IV_PROCESS_MODE);        // CHAR, Processing Mode

            category.logT(Severity.PATH, location, "executing RFC SPC_CHANGE_PRICING_CONDITIONS");
            ((RFCDefaultClient)_client).execute(functionSpcChangePricingConditions); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_CHANGE_PRICING_CONDITIONS");          
            
            JCO.Structure exportsSpcChangePricingConditionsSpcchangepricingconditions_esProfile = exportsSpcChangePricingConditions.getStructure(SpcChangePricingConditions.ES_PROFILE);
            JCO.Table exportsSpcChangePricingConditionsSpcchangepricingconditions_esProfileSpcchangepricingconditions_messages = exportsSpcChangePricingConditionsSpcchangepricingconditions_esProfile.getTable(SpcChangePricingConditions.MESSAGES);
            int rowCnt = exportsSpcChangePricingConditionsSpcchangepricingconditions_esProfileSpcchangepricingconditions_messages.getNumRows();
            String[] message = new String[rowCnt];
            String[] messageType = new String[rowCnt];
            String[] objectId = new String[rowCnt];
          
            for (int i=0; i < rowCnt; i++) {
                exportsSpcChangePricingConditionsSpcchangepricingconditions_esProfileSpcchangepricingconditions_messages.setRow(i);
                messageType[i] = exportsSpcChangePricingConditionsSpcchangepricingconditions_esProfileSpcchangepricingconditions_messages.getString(SpcChangePricingConditions.TYPE);
                objectId[i] = exportsSpcChangePricingConditionsSpcchangepricingconditions_esProfileSpcchangepricingconditions_messages.getString(SpcChangePricingConditions.ID);
                message[i] = exportsSpcChangePricingConditionsSpcchangepricingconditions_esProfileSpcchangepricingconditions_messages.getString(SpcChangePricingConditions.MESSAGE);
            }
            addMessages(new DefaultIPCMessageSet(objectId, messageType, message));
        }catch(ClientException e){
            throw new IPCException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAvailableConditionTypeNames()
     */
    public ValueDescriptionPairs getAvailableConditionTypeNames()
        throws IPCException {
        // As per Thomas Machts this is only MSA specific call, no RFC impl is required.
        category.logT(Severity.DEBUG, location, "getAvailableConditionTypeNames is not supported by RFC. Contact Developer if required in RFC scenario also.");
        return null;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAllPhysicalUnits()
     */
    public ValueDescriptionPairs getAllPhysicalUnits() throws IPCException {
        if (_allPhysicalUnits == null){
            RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_document.getSession()).getPricingConverter();
            String[][] uomDesc = pc.getExternalUOMNamesAndDescriptions(getLanguage());
            int length = uomDesc.length; 
            String[] names = new String[length];
            String[] descriptions = new String[length];
            for (int i=0; i<length; i++){
                names[i] = uomDesc[i][0];
                descriptions[i] = uomDesc[i][1];
            }
            _allPhysicalUnits = new DefaultValueDescriptionPairs(names, descriptions);
        }
        return _allPhysicalUnits;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAvailablePhysicalUnits(java.lang.String)
     */
    public ValueDescriptionPairs getAvailablePhysicalUnits(String dimensionName)
        throws IPCException {
            ValueDescriptionPairs units = (ValueDescriptionPairs)_availablePhysicalUnits.get(dimensionName);
            if (units == null){
                try{
                    if (true) throw new ClientException("500","internal error");
                }catch(ClientException e){
                    throw new IPCException(e);
                }
            }
            return units;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAvailableQuantityUnits()
     */
    public ValueDescriptionPairs getAvailableQuantityUnits() throws IPCException {
        if (_isHeader)
            return getAllPhysicalUnits();
        else{
            if (_availableQuantityUnits == null){
                try{
                    JCO.Function functionSpcGetDocumentInfo = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_DOCUMENT_INFO);
                    JCO.ParameterList im2 = functionSpcGetDocumentInfo.getImportParameterList();
                    JCO.ParameterList ex2 = functionSpcGetDocumentInfo.getExportParameterList();

                    JCO.Table imSpcgetdocumentinfo_itDocumentId = im2.getTable(SpcGetDocumentInfo.IT_DOCUMENT_ID);
                    imSpcgetdocumentinfo_itDocumentId.appendRow();
                    imSpcgetdocumentinfo_itDocumentId.setValue(getDocumentId(), ""); // BYTE, Pricing Document GUID
                            
                    imSpcgetdocumentinfo_itDocumentId.appendRow();
                    imSpcgetdocumentinfo_itDocumentId.setValue(getItemId(), "");
                            
                    category.logT(Severity.PATH, location, "executing RFC SPC_GET_DOCUMENT_INFO");
                    ((RFCDefaultClient) _client).execute(functionSpcGetDocumentInfo);
                    category.logT(Severity.PATH, location, "done with RFC SPC_GET_DOCUMENT_INFO");

                    JCO.Table exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfo = ex2.getTable(SpcGetDocumentInfo.ET_DOCUMENT_INFO);
                    int rowCnt = 0;
                    String[] itemQuantityUnit = null;
                    for (int exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfoCounter=0; exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfoCounter<exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfo.getNumRows(); exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfoCounter++) {
                        exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfo.setRow(exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfoCounter);
                        String documentId = exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfo.getString(SpcGetDocumentInfo.DOCUMENT_ID);
                        if (!getDocumentId().equals(documentId)) continue;
                        JCO.Table exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfoSpcgetdocumentinfo_itemInfo = exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfo.getTable(SpcGetDocumentInfo.ITEM_INFO);
                        rowCnt = exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfoSpcgetdocumentinfo_itemInfo.getNumRows();
                        itemQuantityUnit = new String[rowCnt];
                        for (int i=0; i<rowCnt; i++) {
                            exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfoSpcgetdocumentinfo_itemInfo.setRow(i);
                            itemQuantityUnit[i] = exportsSpcGetDocumentInfoSpcgetdocumentinfo_etDocumentInfoSpcgetdocumentinfo_itemInfo.getString(SpcGetDocumentInfo.ITEM_QUANTITY_UNIT);
                        }
                    }
                    String[] descriptions = new String[rowCnt]; 
                    _availableQuantityUnits = new DefaultValueDescriptionPairs(itemQuantityUnit, descriptions);
                }catch(ClientException e){
                    throw new IPCException(e);
                }
            }
            return _availableQuantityUnits;
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCPricingConditionSet#getAllCurrencyUnits()
     */
    public ValueDescriptionPairs getAllCurrencyUnits() throws IPCException {
        if (_allCurrencyUnits == null){
            RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_document.getSession()).getPricingConverter();
            String[][] currDesc = pc.getCurrencyDescriptions(getLanguage());
            int length = currDesc.length;
            String[] names = new String[length]; 
            String[] descriptions = new String[length];
            for (int i=0; i<length; i++){
                names[i] = currDesc[i][0];
                descriptions[i] = currDesc[i][1];
            }
            _allCurrencyUnits = new DefaultValueDescriptionPairs(names, descriptions);
        }
        return _allCurrencyUnits;
    }
}
