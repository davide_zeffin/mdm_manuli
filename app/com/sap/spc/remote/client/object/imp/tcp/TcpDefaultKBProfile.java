package com.sap.spc.remote.client.object.imp.tcp;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.KBProfile;
import com.sap.spc.remote.client.object.KnowledgeBase;
//import com.sap.spc.remote.client.object.imp.CachingClient;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultKBProfile;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.shared.command.GetProfilesOfKB;
//import com.sap.sxe.socket.client.ClientException;
//import com.sap.sxe.socket.client.ServerResponse;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.tcp.ServerResponse;

public class TcpDefaultKBProfile extends DefaultKBProfile implements KBProfile {

	protected TcpDefaultKBProfile(String name, KnowledgeBase kb) {
        super(name, kb);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultKBProfile#init()
     */
    protected void init() {
        String[] kbParameter = {
                GetProfilesOfKB.KB_LOGSYS,
                kb.getLogsys(),
                GetProfilesOfKB.KB_NAME,
                kb.getName(),
                GetProfilesOfKB.KB_VERSION,
                kb.getVersion() 
        };

        IPCSession session = kb.getIPCClient().getIPCSession();
        if (!(session instanceof DefaultIPCSession)) {
            throw new IllegalClassException(session, DefaultIPCSession.class);
        }
        TCPDefaultClient cachingClient = (TCPDefaultClient)((DefaultIPCSession) session).getClient();

        try {
            ServerResponse r =
                cachingClient.doCmd(
			ISPCCommandSet.GET_PROFILES_OF_KB,
                    kbParameter);

            String[] kbProfiles = r.getParameterValues(GetProfilesOfKB.KB_PROFILE);
            String[] kbRootObjNames = r.getParameterValues(GetProfilesOfKB.ROOT_INSTANCE_NAME);
            String[] uinames = r.getParameterValues(GetProfilesOfKB.UINAME);
            for (int i = 0; i < kbProfiles.length; i++) {
                if (kbProfiles[i].equals(this.name)) {
                    this.rootObjName = kbRootObjNames[i];
                    if (uinames != null && uinames[i] != null) {
                        this.uiName = uinames[i];
                    } else {
                        this.uiName = "";
                    }
                }
            }

        } catch (ClientException e) {
            throw new IPCException(e);
        }
    }
}
