package com.sap.spc.remote.client.object.imp.tcp;

import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.imp.ConfigurationChange;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristicValue;

public class TcpDefaultCharacteristicValue extends DefaultCharacteristicValue {

   protected TcpDefaultCharacteristicValue(  ConfigurationChangeStream configurationChangeStream,
                                        IPCClient ipcClient,
                                        Characteristic characteristic,
                                        String name,
                                        String languageDependentName,
                                        String conditionKey,
                                        String price,
                                        int[] index,
                                        String[] mimeValueLNames,
                                        String[] mimeObjectTypes,
                                        String[] mimeObjects,
                                        boolean defaultValue) {
      init( configurationChangeStream,
            ipcClient,
            characteristic,
            name,
			languageDependentName,
			conditionKey,
            price,
            false,
            index,
            mimeValueLNames,
            mimeObjectTypes,
            mimeObjects,
            defaultValue);
    }

    protected TcpDefaultCharacteristicValue(  ConfigurationChangeStream configurationChangeStream,
                                        IPCClient ipcClient,
                                        Characteristic characteristic,
                                        String name,
                                        String languageDependentName,
                                        String conditionKey,
                                        String price,
                                        int[] index,
                                        String[] mimeValueLNames,
                                        String[] mimeObjectTypes,
                                        String[] mimeObjects) {
      init( configurationChangeStream,
            ipcClient,
            characteristic,
            name,
			languageDependentName,
			conditionKey,
            price,
            false,
            index,
            mimeValueLNames,
            mimeObjectTypes,
            mimeObjects);
    }

    /**
     * This package wide constructor tells the characteristic value, that it's
     * an additional value. There are differences between additional values and
     * others: they use language dependent names to identify the value.
     */
    protected TcpDefaultCharacteristicValue( ConfigurationChangeStream configurationChangeStream,
                                IPCClient ipcClient,
                                Characteristic characteristic,
                                String name,
                                String languageDependentName,
                                String conditionKey,
                                String price,
                                boolean isAdditionalValue) {

      init( configurationChangeStream,
            ipcClient,
            characteristic,
            name,
			languageDependentName,
			conditionKey,
            price,
            isAdditionalValue,
            new int[] {0,0},                      // There are no mime objects for additional values!
            null,
            null,
            null);
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultCharacteristicValue#init(com.sap.spc.remote.client.object.imp.ConfigurationChangeStream, com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.Characteristic, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, int[], java.lang.String[], java.lang.String[], java.lang.String[])
     */
    protected void init(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
		String conditionKey,
        String price,
        boolean isAdditionalValue,
        int[] index,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects) {
        int i = index[1];
        this.configurationChangeStream = configurationChangeStream;
        this.ipcClient = ipcClient;
        this.characteristic = characteristic;
        this.name = name;
        this.languageDependentName = languageDependentName;
        this.conditionKey = conditionKey;
        this.price = price;
        this.closed = false;
        this.cachingClient = ((TcpDefaultIPCSession)ipcClient.getIPCSession()).getClient();
        this.isAdditionalValue = isAdditionalValue;
        valueOperation = ConfigurationChange.NO_OPERATION;
        if (mimeValueLNames != null) {
            //create the DefaultMimeObjectContainer only if mimes attached
            if (this.mimeObjects == null) {
                this.mimeObjects = factory.newMimeObjectContainer();
            }
            boolean mimes = false;
            String valueMimeName = characteristic.getName() + "." + name;
            // Position to the first mime object for this value
            while ( i < mimeValueLNames.length &&
                    !mimeValueLNames[i].equals(valueMimeName)) i++;
            // Now add all mime objects of this value to the container
            while ( i < mimeValueLNames.length &&
                    mimeValueLNames[i].equals(valueMimeName)) {
                mimes = true;
                MimeObject mimeObject = factory.newMimeObject(mimeObjects[i],mimeObjectTypes[i]);
                this.mimeObjects.add(mimeObject);
                i++;
            }
            if (mimes) index[1] = i;
        }
/*
        String prevLName = "";
        DefaultMimeObjectContainer mimes = null;
        Hashtable valueLNameToMimeObjectHash = new Hashtable();
        if (mimeValueLNames != null) {
            for (int i=0; i < mimeValueLNames.length; i++) {
                if (!prevLName.equals(mimeValueLNames[i])) { // Next value's mime objects?
                    mimes = new DefaultMimeObjectContainer();
                    valueLNameToMimeObjectHash.put(mimeValueLNames[i],mimes);
                    prevLName = mimeValueLNames[i];
                }
                MimeObject mimeObject = new DefaultMimeObject(mimeObjects[i],mimeObjectTypes[i]);
                mimes.add(mimeObject);
            }
        }
*/

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultCharacteristicValue#init(com.sap.spc.remote.client.object.imp.ConfigurationChangeStream, com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.Characteristic, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, int[], java.lang.String[], java.lang.String[], java.lang.String[], boolean)
     */
    protected void init(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String conditionKey,
        String price,
        boolean isAdditionalValue,
        int[] index,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects,
        boolean defaultValue) {
        init(
            configurationChangeStream,
            ipcClient,
            characteristic,
            name,
            languageDependentName,
            conditionKey,
            price,
            isAdditionalValue,
            index,
            mimeValueLNames,
            mimeObjectTypes,
            mimeObjects);
        this.defaultValue = defaultValue;            
    }

}
