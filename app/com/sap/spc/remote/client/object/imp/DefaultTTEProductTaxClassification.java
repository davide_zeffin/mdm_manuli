package com.sap.spc.remote.client.object.imp;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.spc.remote.client.object.TTEProductTaxClassification;

public class DefaultTTEProductTaxClassification
    extends BusinessObjectBase
    implements TTEProductTaxClassification {

    private String country;
    private String region;
    private String taxType;
    private String taxGroup;
    private String taxTariffCode;


    DefaultTTEProductTaxClassification(
        String country,
        String region,
        String taxType,
        String taxGroup,
        String taxTariffCode)
    {
        this.country = country;
        this.region = region;
        this.taxType = taxType; 
        this.taxGroup = taxGroup;
        this.taxTariffCode = taxTariffCode;
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProductTaxClassification#getCountry()
     */
    public String getCountry() {
        return country;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProductTaxClassification#getRegions()
     */
    public String getRegion() {
        return region;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProductTaxClassification#getTaxGroups()
     */
    public String getTaxGroup() {
        return taxGroup;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProductTaxClassification#getTaxTypes()
     */
    public String getTaxType() {
        return taxType;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProductTaxClassification#getTaxTariffCode()
     */
    public String getTaxTariffCode() {
        return taxTariffCode; 
    }



}
