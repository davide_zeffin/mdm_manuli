package com.sap.spc.remote.client.object.imp.tcp;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import org.apache.struts.util.MessageResources;

import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ConflictParticipant;
import com.sap.spc.remote.client.object.DescriptiveConflict;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.DefaultExplanationTool;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.shared.command.GetConflictingAssumptions;
import com.sap.spc.remote.shared.command.GetCsticConflicts;
import com.sap.spc.remote.shared.command.SCECommand;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;


public class TcpDefaultExplanationTool extends DefaultExplanationTool implements ExplanationTool {

	// logging
	private static final Category category =
	ResourceAccessor.category;
	private static final Location location =
		ResourceAccessor.getLocation(TcpDefaultExplanationTool.class);

    protected TcpDefaultExplanationTool(
        IPCClient ipcClient,
        Configuration configuration) {
        this.ipcClient = ipcClient;
        this.configuration = configuration;
        this.closed = false;
        this.update();
        setUpdateConflicts(true);
        setUpdateDescriptiveConflicts(true);
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultExplanationTool#readInConflicts(boolean, java.util.Locale, org.apache.struts.util.MessageResources)
     */
    protected synchronized void readInConflicts(
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources) {
        conflicts = new Hashtable();
        Hashtable solutionRateOfConflictParticipants = new Hashtable();
        sortedConflicts = new Vector();
        int assumptionGroups = 0;
        try {
            // 20020315-kha: new "all or nothing" client.object implementation to clean up API (cf IPCClient internal comment)
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof DefaultIPCSession)) {
                throw new IllegalClassException(session, DefaultIPCSession.class);
            }
            this.cachingClient = ((DefaultIPCSession) session).getClient();

            ServerResponse r =
			((TCPDefaultClient)cachingClient).doCmd(
                    ISPCCommandSet.GET_CONFLICTING_ASSUMPTIONS,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        this.configuration.getId()});
            String[] assumptionGroupId =
                r.getParameterValues(GetConflictingAssumptions.ASSUMPTION_GROUP_ID);
            if (assumptionGroupId == null)
                return;
            String[] assumptionId =
                r.getParameterValues(GetConflictingAssumptions.ASSUMPTION_ID);
            String[] assumptionLName =
                r.getParameterValues(GetConflictingAssumptions.ASSUMPTION_LNAME);
            String[] assumptionAuthor =
                r.getParameterValues(GetConflictingAssumptions.ASSUMPTION_AUTHOR);

            String oldGroupId = "";
            Conflict conflict = null;
            ConflictParticipant participant;
            for (int i = 0; i < assumptionGroupId.length; i++) {
                if (!oldGroupId.equals(assumptionGroupId[i])) {
                    assumptionGroups++;
                    if (conflict != null)
                        conflicts.put(conflict.getId(), conflict);
                    conflict =
                        factory.newConflict(
                            this.ipcClient,
                            this,
                            assumptionGroupId[i]);
                    oldGroupId = assumptionGroupId[i];
                }
                participant = 
                    factory.newConflictParticipant(this.ipcClient,
                        conflict,
                        assumptionId[i],
                        assumptionLName[i],
                        assumptionAuthor[i].equals(
                            GetConflictingAssumptions.ASSUMPTION_AUTHOR_USER)
                            ? true
                            : false,
                        assumptionAuthor[i].equals(
                            GetConflictingAssumptions
                                .ASSUMPTION_AUTHOR_DEFAULT)
                            ? true
                            : false,
                        locale,
                        resources);
                conflict.addParticipant(participant);
                // Prearrangements for the calculation of the solution rate
                if (!showConflictSolutionRate)
                    continue;
                String participantKey = participant.getKey();
                if (solutionRateOfConflictParticipants.containsKey(participantKey)) {
                    Integer countInt =
                        (Integer) solutionRateOfConflictParticipants.get(participantKey);
                    int count = countInt.intValue();
                    count++;
                    solutionRateOfConflictParticipants.put(participantKey, new Integer(count));
                } else
                    solutionRateOfConflictParticipants.put(participantKey, new Integer(1));
            }
            if (conflict != null) {
                conflicts.put(conflict.getId(), conflict);
            }
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        Vector vConflicts = new Vector(conflicts.values());
        sortedConflicts = new Vector(vConflicts.size());
        Iterator iConflicts = vConflicts.iterator();
        int index = 0;
        while (iConflicts.hasNext()) {
            Conflict currentConflict = (Conflict) iConflicts.next();
            if (showConflictSolutionRate) {
                // participants solution rate
                Vector vParticipants = (Vector) currentConflict.getParticipants();
                Iterator iParticipants = vParticipants.iterator();
                while (iParticipants.hasNext()) {
                    ConflictParticipant participant = (ConflictParticipant) iParticipants.next();
                    int count =
                        ((Integer) solutionRateOfConflictParticipants
                            .get(participant.getKey()))
                            .intValue();
                    count = count * 100 / assumptionGroups;
                    participant.setSolutionRate(count);
                }
            }
            // sort of the conflicts
            int position = 0;
            int j = index - 1;
            for (int i = 0; i < index; i++) {
                if (currentConflict.countParticipants()
                    > ((Conflict) sortedConflicts.elementAt(j)).countParticipants()) {
                    position = j + 1;
                    break;
                }
                j = j / 2;
                position = j;
            }
            sortedConflicts.add(position, currentConflict);
            index++;
        }
        setUpdateConflicts(false);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultExplanationTool#readInDescriptiveConflicts(java.lang.String, java.lang.String, java.lang.String, boolean, java.util.Locale, org.apache.struts.util.MessageResources, boolean, boolean, boolean)
     */
    protected synchronized void readInDescriptiveConflicts(
        String conflictExplanationLevel,
        String conflictExplanationTextBlockTag,
        String conflictExplanationTextLineTag,
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool) {
        descriptiveConflicts = new Vector();
        descriptiveConflIndex = new Hashtable();
        try {
            // 20020315-kha: new "all or nothing" client.object implementation to clean up API (cf IPCClient internal comment)
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof DefaultIPCSession)) {
                throw new IllegalClassException(session, DefaultIPCSession.class);
            }
            this.cachingClient = ((DefaultIPCSession) session).getClient();

            ServerResponse r = null;
            String[] parameters = {
                    SCECommand.CONFIG_ID,
                    this.configuration.getId(),
                    GetCsticConflicts.LEVEL,
                    conflictExplanationLevel,
                    GetCsticConflicts.BLOCK_TAG,
                    conflictExplanationTextBlockTag,
                    GetCsticConflicts.LINE_TAG,
                    conflictExplanationTextLineTag 
                    };

            r = ((TCPDefaultClient)cachingClient).doCmd(ISPCCommandSet.GET_CSTIC_CONFLICTS, parameters);
            String[] instIds =
                r.getParameterValues(GetCsticConflicts.CONFLICT_INST_IDS);
            if (instIds == null) {
                return;
            }
            String[] csticNames = r.getParameterValues(GetCsticConflicts.CONFLICT_CSTIC_NAMES);
            String[] csticLNames = r.getParameterValues(GetCsticConflicts.CONFLICT_CSTIC_LNAMES);
            String[] lTexts = r.getParameterValues(GetCsticConflicts.CONFLICT_LONG_TEXTS);
            Vector vLTexts = new Vector();
            conflictingInstances = instIds;
            conflictingCharacteristics = csticNames;
            Hashtable csticConflictType = new Hashtable();
            for (int i = 0; i < instIds.length; i++) {
                String explanation = lTexts[i];
                DescriptiveConflict conflict = 
                    new DefaultDescriptiveConflict(
                        this,
                        instIds[i],
                        csticNames[i],
                        csticLNames[i],
                        explanation,
                        showConflictSolutionRate,
                        locale,
                        resources,
                        conflictExplanationTextLineTag,
                        useConflictHandlingShortcuts,
                        useShortcutValueConflict,
                        useExplanationTool);
                descriptiveConflicts.add(conflict);
                String key = instIds[i] + DEL + csticNames[i];
                if (descriptiveConflIndex.containsKey(key)) {
                    Vector values = (Vector) descriptiveConflIndex.get(key);
                    values.add(conflict);
                    descriptiveConflIndex.put(key, values);
                } else {
                    Vector values = new Vector(1, 1);
                    values.add(conflict);
                    descriptiveConflIndex.put(key, values);
                }
            }
            this.setUpdateDescriptiveConflicts(false);
        } catch (ClientException e) {            
            category.logT(Severity.ERROR, location, e.toString());
            Exception exc = e;
            throw new IPCException(e);
        } catch (Exception e) {
            category.logT(Severity.ERROR, location, e.toString());
            Exception exc = e;
        }
    }

}
