package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.Instance;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class DefaultCharacteristicGroup extends BusinessObjectBase implements CharacteristicGroup {
    public static final String csticGroupCacheRegion="crm.ipc.knowledgebase.region";

	protected DefaultInstance instance;
	protected String name;
	protected String languageDependentName;
    List    characteristics;
    Hashtable characteristicsByName = new Hashtable();
	protected boolean selected;

	protected boolean closed;
	protected int position;
	protected boolean consistent;
	protected boolean required;

    // logging
    private static final Category category = ResourceAccessor.category;
    private static final Location location = ResourceAccessor.getLocation(DefaultCharacteristicGroup.class);

    DefaultCharacteristicGroup(){}

    DefaultCharacteristicGroup(Instance instance,
                                      String name,
                                      String languageDependentName,
                                      List characteristics,
                                      int position) {
        super(new TechKey(instance.getId() + name));
        this.instance = (DefaultInstance) instance;
        this.name = name;
        this.languageDependentName = languageDependentName;
        this.characteristics = characteristics;
        this.selected = false;
        this.closed = false;
        this.position = position;
    }

    public Instance getInstance() {
        location.entering("getInstance()");
        if (location.beDebug()){
        	if (this.instance != null){
            	location.debugT("return this.instance " + this.instance.getId() + " " + this.instance.getName());
        	}
        }
        location.exiting();
        
        return this.instance;
    }

    public String getName(){
        location.entering("getName()");
        if (location.beDebug()){
            location.debugT("return this.name " + this.name);
        }
        location.exiting();
        return this.name;
    }
    
    public String getLanguageDependentName() {
        location.entering("getLanguageDependentName()");
        if (location.beDebug()){
            location.debugT("return this.languageDependentName " + this.languageDependentName);
        }
        location.exiting();
        return this.languageDependentName;
    }

    public List getCharacteristics(){
        location.entering("getCharacteristics()");
        List characteristics = getCharacteristics(true);
        if (location.beDebug()){
            if (characteristics!=null){
                location.debugT("return characteristics: ");
                for (int i = 0; i<characteristics.size(); i++){
                    location.debugT(characteristics.get(i).toString());
                }
            }
            else location.debugT("return characteristics null");
        }
        location.exiting();
		return characteristics;
    }

	/**
	 * Returns visible characteristics only if includeInvisible is set to false
	 * Otherwise this method returns visible and invisible characteristics (same
	 * as getCharacteristics().
	 */
	public List getCharacteristics(boolean includeInvisible) {
        location.entering("getCharacteristics(boolean includeInvisible)");
		try {
//			instance.synchronize(name,includeInvisible); // used up to CRM2007
			instance.synchronize(includeInvisible); // new with 7.0: we want to call GetFilteredCstics for all groups at once
		} catch (IPCException e) {
            category.logT(Severity.ERROR, location, e.toString());
        }
        
        if (!includeInvisible){
            ArrayList visibleCharacterstics = new ArrayList();
            for (int i=0; i<characteristics.size(); i++) {
            	Characteristic cstic = (Characteristic)characteristics.get(i);
                if (cstic.isVisible()){
                    visibleCharacterstics.add(cstic);
                }
            }
            if (location.beDebug()) {
                if (visibleCharacterstics!=null){
                    location.debugT("return visibleCharacteristics " + visibleCharacterstics.toString());
                }
                else location.debugT("return visibleCharacteristics null");
            }
            location.exiting();
            return visibleCharacterstics;
        }
        if (location.beDebug()) {
            if (characteristics != null){
                location.debugT("return Characteristics ");
                for (int i = 0; i<characteristics.size(); i++){
                    location.debugT(characteristics.get(i).toString());    
                }
            }
        }
        location.exiting();
        return characteristics;
	}
    
    /**
     * Returns the characteristics of this group and the given application view.
     * If includeInvisible is true, the result set will also contain invisible 
     * characteristics. 
     * If "*" is passed as application view all cstics are returned (depending on
     * visibility). 
     * If " " (blank) is passed as application view no cstics are returned.
     */
    public List getCharacteristics(boolean includeInvisible, char applicationView) {
        location.entering("getCharacteristics(boolean includeInvisible, char applicationView");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean includeInvisible " + includeInvisible);
            location.debugT("char applicationView "+  applicationView);
        }
        List csticsOfView = new ArrayList();
        List unfilteredCstics = getCharacteristics(includeInvisible);
        if (applicationView == '*'){
            // return all cstics
            if (location.beDebug()){
                if (unfilteredCstics != null) {
                    location.debugT("return unfilteredCstics ");
                    for (int i = 0; i<unfilteredCstics.size(); i++) {
                        location.debugT(unfilteredCstics.get(i).toString());
                    }
                }
            }
            location.exiting();
            return unfilteredCstics;
        }
        if (applicationView == ' '){
            // return no cstics
            if (location.beDebug()){
                if (csticsOfView != null) {
                    location.debugT("return csticsOfView ");               
                    for (int i = 0; i<csticsOfView.size(); i++) {
                        location.debugT(csticsOfView.get(i).toString());
                    }
                }
            }
            location.exiting();
            return csticsOfView;
        }
        Iterator unfilteredCsticsIt = unfilteredCstics.iterator();
        while (unfilteredCsticsIt.hasNext()) {
            Characteristic cstic = (Characteristic) unfilteredCsticsIt.next();
            if (cstic.isPartOfApplicationView(applicationView)) {
                csticsOfView.add(cstic);
            }
        }
        if (location.beDebug()){
            if (csticsOfView != null) {
                location.debugT("return csticsOfView ");
                for (int i = 0; i<csticsOfView.size(); i++) {
                    location.debugT(csticsOfView.get(i).toString());
                }
            }
        }
        location.exiting();
        return csticsOfView;
    } 
	
	/** 
     * @see com.sap.spc.remote.client.object.CharacteristicGroup#getCharacteristic(java.lang.String, boolean)
     */
    public Characteristic getCharacteristic(String csticName, boolean includeInvisible) {
        location.entering("getCharacteristic(String csticName, boolean includeInvisible");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("String csticName " + csticName);
            location.debugT("boolean includeInvisible "+  includeInvisible);
        }       
		getCharacteristics(includeInvisible);
        Characteristic characteristic = (Characteristic)characteristicsByName.get(csticName);
        if (location.beDebug()) {
        	if (characteristic != null) {
        		location.debugT("return characteristic: "+ characteristic.getName() );
        	}
        }
        location.exiting();
		return characteristic;
	}

	/**
	 * Returns assignable values only if assignableValuesOnly is set to true.
	 * Otherwise assignable and not assignable values will be returned. 
	 */
	public List getCharacteristics(boolean includeInvisible, boolean assignableValuesOnly){
        location.entering("getCharacteristics(boolean includeInvisible, boolean assignableValuesOnly");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("boolean includeInvisible " + includeInvisible);
            location.debugT("boolean assignableValuesOnly "+  assignableValuesOnly);
        }       
		instance.setIsAssignableValuesOnly(assignableValuesOnly);
        List characteristics = getCharacteristics(includeInvisible);
        if (location.beDebug()) {
            if (characteristics != null){
                location.debugT("return characteristics: ");
                for (int i = 0; i<characteristics.size(); i++){
                    location.debugT(characteristics.get(i).toString());
                }
            }
            
        }    
        location.exiting();    
		return characteristics;
	}

    /**
     * Returns the position of this characteristic group. 0 is the first
     * position.
     */
    public int getPosition() {
      location.entering("getPosition()");
      if (location.beDebug()){
          location.debugT("return position " + position +" ("+name+")");
      }
      location.exiting();
      return position;
    }

	/**
	 * Returns true only for the base group. The base group is a virtually
	 * defined group that holds all characteristics that have not been assigned
	 * to any group.
	 */
	public boolean isBaseGroup() {
        location.entering("isBaseGroup()");
        if (location.beDebug()){
            location.debugT("return false" +" ("+name+")");
        }
        location.exiting();
	    return false;
	}

    public void close(){
        location.entering("close()");
        closed = true;
        location.exiting();
    }

    public boolean isClosed(){
        location.entering("isClosed()");
        if (location.beDebug()){
            location.debugT("return closed " + closed +" ("+name+")");
        }
        location.exiting();
        return closed;
    }

    public boolean isRelevant(String commandName) {
	    // we don't use the cache
        location.entering("isRelevant(String commandName)");
        if (location.beDebug()){
            location.debugT("Parameters: ");
            location.debugT("String commandName " + commandName);
            location.debugT("return true");
        }
        location.exiting();
	    return true;
    }

    public void setSelected(boolean selected){
        location.entering("setSelected(boolean selected)");
        if (location.beDebug()){
            location.debugT("Parameters: ");
            location.debugT("boolean selected " + selected);
        }
        this.selected = selected;
        location.exiting();
    }

    public boolean isSelected(){
        location.entering("isSelected()");
        if (location.beDebug()){
            location.debugT("return this.selected " + this.selected +" ("+name+")");
        }
        location.exiting();
        
        return this.selected;
    }

	public boolean isConsistent() {
        location.entering("isConsistent()");
        if (location.beDebug()){
            location.debugT("return this.consistent " + this.consistent +" ("+name+")");
        }
        location.exiting();
		return this.consistent;
	}

	public boolean isRequired() {
        location.entering("isRequired()");
        if (location.beDebug()){
            location.debugT("return this.required " + this.required +" ("+name+")");
        }
        location.exiting();
		return this.required;
	}

	public void setConsistent(boolean consistent) {
        location.entering("setConsistent(boolean consistent)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean consistent " + consistent);
        }
		this.consistent = consistent;
        location.exiting();
	}

	public void setRequired(boolean requiredCstics) {
        location.entering("setRequired(boolean requiredCstics)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean requiredCstics " + requiredCstics);
        }
		this.required = requiredCstics;
        location.exiting();
	}

    public void addCharacteristic(Characteristic characteristic) {
        location.entering("addCharacteristic(Characteristic characteristic)");
        if (location.beDebug()){
        	if (characteristic != null){
	            location.debugT("Parameters:");
    	        location.debugT("Characteristic characteristic " + characteristic.getName());
        	}
        }
        int position = characteristics.size(); //last position
        ((DefaultCharacteristic)characteristic).setCharacteristicGroupPosition(position);
        characteristics.add(characteristic);
        characteristicsByName.put(characteristic.getName(), characteristic);
        location.exiting();
    }
    
    public void addCharacteristic(int position, Characteristic characteristic) {
        location.entering("addCharacteristic(int position, Characteristic characteristic");
        if (location.beDebug()){
        	if (characteristic != null){
          		 location.debugT("Parameters:");
          		 location.debugT("int Position " + position);
          		 location.debugT("Characteristic characteristic " + characteristic.getName());
       		}
        }
 		if(characteristics.size() > position) {
			((DefaultCharacteristic)characteristic).setCharacteristicGroupPosition(position);
			characteristics.add(position, characteristic);
 		}else {
			int pos = characteristics.size();
			((DefaultCharacteristic)characteristic).setCharacteristicGroupPosition(pos);
 			characteristics.add(characteristic);
 		} //add to the end
        characteristicsByName.put(characteristic.getName(), characteristic);
        characteristic.setGroup(this);
        location.exiting();
    }
    
    public boolean equals(Object o) {
        
    	if (super.equals(o)){
    		return true;
    	}else if (o instanceof DefaultCharacteristicGroup) {
    		DefaultCharacteristicGroup tmpGroup = (DefaultCharacteristicGroup)o;
    		if (tmpGroup.getName().equals(this.getName())) {
    			return true;
    		}else {
    			return false;
    		}
    	}else {
    		return false;
    	}
    }
    
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     * Overwritten because equals(Object o) is also overwritten. 
     */
    public int hashCode(){
        int result = 17;
        int nameHashCode = 0;
        if (this.getName() != null){
            nameHashCode = this.getName().hashCode();
        }
        result = 37*result + nameHashCode;
        return result;
    }
    

    /**
     * This methods returns the full list of characteristics that are currently known on
     * the client side. No synchronization with the server is done.
     * This is needed for groupCache functionality of DefaultInstance (find out which characteristics
     * are already known and which have to be retreived from the server).
     * @return list of characteristics known at the client for this group
     */
    public List getListOfCachedCharacteristics() {
        location.entering("getListOfCachedCharacteristics()");
        if (location.beDebug()){
            if (characteristics != null) {
                location.debugT("return characteristics ");
                for (int i =0; i<characteristics.size(); i++){
                    location.debugT(characteristics.get(i).toString());
                }
            }
        }
        location.exiting();
        return characteristics;
    }
    
}
