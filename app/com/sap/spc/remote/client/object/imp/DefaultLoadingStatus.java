package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.List;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.LoadingMessage;
import com.sap.spc.remote.client.object.LoadingStatus;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

public abstract class DefaultLoadingStatus extends BusinessObjectBase implements LoadingStatus {

    protected static final String TRUE = "T";
    protected static final String FALSE = "F";
    protected static final String I = "I";
    protected static final String W = "W";    
    protected static final String EMPTY = "";
    protected static final String NO_ID = "NO_ID";
    
    protected int status = UNKNOWN;    
    protected boolean newKB = false;
    protected boolean loadingMessages = false;
    protected ArrayList listOfLoadingMessages;
    protected boolean initialConfiguration = false;
    protected Configuration config;
    protected IPCClient ipcClient;
    protected IClient cachingClient; 
    
    protected IPCClientObjectFactory factory = IPCClientObjectFactory.getInstance();   
    
    // logging
    protected static final Category category = ResourceAccessor.category;
    protected static final Location location = ResourceAccessor.getLocation(DefaultLoadingStatus.class);        
    
    /**
     * @param client IPCClient 
     * @param config Configuration to which the <code>LoadingStatus</code> belongs to.
     */
    protected DefaultLoadingStatus(IPCClient client, Configuration config) {
        super(new TechKey(config.getId()));
        this.ipcClient = client;
        this.config = config;
        init();
    }

    /**
     * Initializes the <code>LoadingStatus</code>.<br>
     * Calls function module of AP-CFG to get the information on the loading status. 
     */
    protected abstract void init();

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingStatus#getStatus()
     */
    public int getStatus() {
        return status;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingStatus#hasNewKB()
     */
    public boolean hasNewKB() {
        return newKB;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingStatus#hasLoadingMessages()
     */
    public boolean hasLoadingMessages() {
        return loadingMessages;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingStatus#getLoadingMessages()
     */
    public abstract List getLoadingMessages();

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingStatus#loadedFromInitialConfiguration()
     */
    public boolean loadedFromInitialConfiguration() {
        return initialConfiguration;
    }    
    /**
     * Set loadingMessages-flag manually.<br>
     * Usually the flag should be set during initialization process of this class. 
     * @param flag whether there have been messages during the loading process
     */
    public void setLoadingMessages(boolean b) {
        loadingMessages = b;
    }

    /**
     * Set newKB-flag manually.<br>
     * Usually the flag should be set during initialization process of this class.
     * @param flag whether a new KB has been used
     */
    public void setNewKB(boolean b) {
        newKB = b;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingStatus#setLoadedFromInitialConfiguration()
     */
    public void setLoadedFromInitialConfiguration(boolean b) {
        initialConfiguration = b;
    }

    /**
     * For test purposes only.
     * Add a Loading message to the list of loading messages. The method first tries to get the
     * loading messages from the server, then adds the message.
     * @param Loading Messages that should be added to the list of loading messages.
     */
    public void addLoadingMessage(LoadingMessage lm){
        List messages = getLoadingMessages();
        messages.add(lm);
        loadingMessages = true;
    }

}
