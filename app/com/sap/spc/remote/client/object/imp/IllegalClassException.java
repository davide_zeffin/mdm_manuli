package com.sap.spc.remote.client.object.imp;

/**
 * This exception is thrown when a file that has to be its default implementation is implemented otherwise.
 * <p>
 * As of IPC 4.0, the implementation choice is "all or nothing". Either you implement all interfaces by
 * your own class, or you only use subclasses of the existing implementation. This guarantees much cleaner
 * interfaces.
 */
public class IllegalClassException extends ClassCastException {

    public IllegalClassException(Object obj, Class cl) {
		super("Object of class "+obj.getClass().getName()+" must be instance of class "+cl.getName());
    }
}