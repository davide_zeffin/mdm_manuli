package com.sap.spc.remote.client.object.imp;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.spc.remote.client.object.TTEItemBusinessPartner;

public class DefaultTTEItemBusinessPartner
    extends BusinessObjectBase
    implements TTEItemBusinessPartner {

    private String role;
    private String businessPartnerId;
    private String country;
    private String region;
    private String county; 
    private String city; 
    private String postalCode;
    private String geoCode; 
    private String exemptedRegion; 
    private String jurisdictionCode;

    DefaultTTEItemBusinessPartner(
        String role,
        String businessPartnerId,
        String country,
        String region,
        String county, 
        String city, 
        String postalCode,
        String geoCode, 
        String exemptedRegion, 
        String jurisdictionCode)
    {
        this.role = role;
        this.businessPartnerId = businessPartnerId;
        this.country = country;
        this.region = region;
        this.county = county; 
        this.city = city; 
        this.postalCode = postalCode;
        this.geoCode = geoCode; 
        this.exemptedRegion = exemptedRegion; 
        this.jurisdictionCode = jurisdictionCode;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getBusinessPartnerId()
     */
    public String getBusinessPartnerId() {
        return businessPartnerId;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getCity()
     */
    public String getCity() {
        return city;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getCountry()
     */
    public String getCountry() {
        return country;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getCounty()
     */
    public String getCounty() {
        return county;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getExemptedRegion()
     */
    public String getExemptedRegion() {
        return exemptedRegion;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getGeoCode()
     */
    public String getGeoCode() {
        return geoCode;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getJurisdictionCode()
     */
    public String getJurisdictionCode() {
        return jurisdictionCode;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getPostalCode()
     */
    public String getPostalCode() {
        return postalCode;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getRegion()
     */
    public String getRegion() {
        return region;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#getRole()
     */
    public String getRole() {
        return role;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItemBusinessPartner#setBusinessPartnerId(java.lang.String)
     */
    public void setBusinessPartnerId(String id) {
        this.businessPartnerId = id;
    }

}
