package com.sap.spc.remote.client.object.imp.rfc;

import java.math.BigDecimal;  
import java.util.ArrayList;
import java.util.List;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.LoadingStatus;
import com.sap.spc.remote.client.object.LoadingMessage;
import com.sap.spc.remote.client.object.imp.DefaultLoadingStatus;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.rfc.function.BapiMessageGetdetail;
import com.sap.spc.remote.client.rfc.function.CfgApiGetLoadMessages;
import com.sap.spc.remote.client.rfc.function.CfgApiGetLoadStatus;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.tc.logging.Severity;

public class RfcDefaultLoadingStatus extends DefaultLoadingStatus implements LoadingStatus {

    /**
     * @param client IPCClient 
     * @param config Configuration to which the <code>LoadingStatus</code> belongs to.
     */
    RfcDefaultLoadingStatus(IPCClient client, Configuration config) {
        super(client, config);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultLoadingStatus#init()
     */
    protected void init() {
        location.entering("init()");
        String configId = config.getId();
        IPCSession session = ipcClient.getIPCSession();
        if (!(session instanceof RfcDefaultIPCSession)){
            throw new IllegalClassException(session, RfcDefaultIPCSession.class);
        }
        this.cachingClient = ((RfcDefaultIPCSession) session).getClient();

        String msgLevel = "";
        String cfgLoadedFromExt = "";
        String kbVersionChanged = "";

        try {
            JCO.Function functionCfgApiGetLoadStatus = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_LOAD_STATUS);
            JCO.ParameterList im = functionCfgApiGetLoadStatus.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiGetLoadStatus.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiGetLoadStatus.getTableParameterList();
    
            im.setValue(configId, CfgApiGetLoadStatus.CONFIG_ID);       // STRING, Configuration  Identifier
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_LOAD_STATUS");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetLoadStatus);
            category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_LOAD_STATUS");

            // process structure exCfgapigetloadstatus_loadStatus
            JCO.Structure exCfgapigetloadstatus_loadStatus = ex.getStructure(CfgApiGetLoadStatus.LOAD_STATUS);
            cfgLoadedFromExt = exCfgapigetloadstatus_loadStatus.getString(CfgApiGetLoadStatus.CFG_LOADED_FROM_EXT);
            msgLevel = exCfgapigetloadstatus_loadStatus.getString(CfgApiGetLoadStatus.MSG_LEVEL);
            kbVersionChanged = exCfgapigetloadstatus_loadStatus.getString(CfgApiGetLoadStatus.KB_VERSION_CHANGED);
        }
        catch(ClientException e) {
            throw new IPCException(e);
        }

        // KB changed? 
        if (kbVersionChanged.equals(TRUE)) {
            newKB = true;
        }

        // status
        if (msgLevel.equals(EMPTY)) {
            status = OK;
        }
        else if (msgLevel.equals(I)) {
            status = INFO;
        }
        else if (msgLevel.equals(W)) {
            status = WARNING;
        }
        else {
            status = ERROR;
        }

        // has loading messages?
        if (status > OK) {
            loadingMessages= true;
        }

        // has initial configuration
        if (cfgLoadedFromExt.equals(TRUE)){
            initialConfiguration = true;
        }

        location.debugT("LoadingStatus for configuration " + config.getId() + ":");
        location.debugT("Status: " + status + ", hasNewKB: " + newKB + 
                        ", hasLoadingMessages: " + loadingMessages + 
                        ", hasInitialConfiguration: " + initialConfiguration);
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingStatus#getLoadingMessages()
     */
    public List getLoadingMessages() {
        location.entering("getLoadingMessages()");
        if (listOfLoadingMessages != null){
            location.debugT("listOfLoadingMessages is not null; no call of FM necessary.");
            location.exiting();
            return listOfLoadingMessages;
        }
        else {
            location.debugT("listOfLoadingMessages is null; creating new ArrayList and call FM");
            listOfLoadingMessages = new ArrayList();
            String configId = config.getId();
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof RfcDefaultIPCSession)){
                throw new IllegalClassException(session, RfcDefaultIPCSession.class);
            }
            this.cachingClient = ((RfcDefaultIPCSession) session).getClient();
            
            try {
                JCO.Function functionCfgApiGetLoadMessages = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_LOAD_MESSAGES);
                JCO.ParameterList im = functionCfgApiGetLoadMessages.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiGetLoadMessages.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiGetLoadMessages.getTableParameterList();
                
                im.setValue(configId, CfgApiGetLoadMessages.CONFIG_ID);     // STRING, Configuration   Identifier
                
                category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_LOAD_MESSAGES");
                ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetLoadMessages);
                category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_LOAD_MESSAGES");                

                // process table exCfgapigetloadmessages_loadMessages
                // TABLE, T100 Load Messages
                JCO.Table exCfgapigetloadmessages_loadMessages = ex.getTable(CfgApiGetLoadMessages.LOAD_MESSAGES);
                for (int exCfgapigetloadmessages_loadMessagesCounter=0; exCfgapigetloadmessages_loadMessagesCounter<exCfgapigetloadmessages_loadMessages.getNumRows(); exCfgapigetloadmessages_loadMessagesCounter++) {
                    exCfgapigetloadmessages_loadMessages.setRow(exCfgapigetloadmessages_loadMessagesCounter);
                    String type = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.TYPE);
                    int severity = ERROR;
                    if (type.equals(W)) {
                        severity = WARNING;
                    }
                    else if (type.equals(I)) {
                        severity= INFO;
                    }                    
                    String messageClass = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.ID);
                    BigDecimal number = exCfgapigetloadmessages_loadMessages.getBigDecimal(CfgApiGetLoadMessages.NUMBER);
                    int msgNumber = number.intValue();
                    String message = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.MESSAGE);
                    String messageV1 = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.MESSAGE_V1);
                    String messageV2 = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.MESSAGE_V2);
                    String messageV3 = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.MESSAGE_V3);
                    String messageV4 = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.MESSAGE_V4);
                    String instId = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.PARAMETER);
                    // if instId is not specified we create a dummy instId "NO_ID"
                    if (instId == null || instId.equals("")){
                        instId = NO_ID; 
                    }
                    String csticName = exCfgapigetloadmessages_loadMessages.getString(CfgApiGetLoadMessages.FIELD);
                    LoadingMessage lm = factory.newLoadingMessage(instId, csticName, msgNumber, 
                                                                  messageClass, severity, message, 
                                                                  new String[] {messageV1, messageV2, messageV3, messageV4});
                    location.debugT("Created new LoadingMessage(" + lm.getConfigObjKey()+ ", " + lm.getNumber() + ", " + lm.getSeverity() + ", " + lm.getText());
                    listOfLoadingMessages.add(lm);
                }
            }
            catch(ClientException e) {
                throw new IPCException(e);
            }

            // loop over the list of messages again and replace the message text with the correct one            
            for (int i=0; i<listOfLoadingMessages.size(); i++){
                LoadingMessage lm = (LoadingMessage) listOfLoadingMessages.get(i);
                try {
                    JCO.Function functionBapiMessageGetdetail = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.BAPI_MESSAGE_GETDETAIL);
                    JCO.ParameterList im = functionBapiMessageGetdetail.getImportParameterList();
                    JCO.ParameterList ex = functionBapiMessageGetdetail.getExportParameterList();
                    JCO.ParameterList tbl = functionBapiMessageGetdetail.getTableParameterList();
                    im.setValue(lm.getMessageClass(), BapiMessageGetdetail.ID);       // CHAR, Message class
                    im.setValue(lm.getNumber(), BapiMessageGetdetail.NUMBER);       // NUM, Message number
                    String[] args = lm.getArguments();
                    String messageV1 = "";
                    if (args[0] != null){
                        messageV1 = args[0];
                    }
                    String messageV2 = "";
                    if (args[1] != null){
                        messageV2 = args[1];
                    }
                    String messageV3 = "";
                    if (args[2] != null){
                        messageV3 = args[2];
                    }
                    String messageV4 = "";
                    if (args[3] != null){
                        messageV4 = args[3];
                    }
                    im.setValue(messageV1, BapiMessageGetdetail.MESSAGE_V1);        // optional, CHAR, Message variable
                    im.setValue(messageV2, BapiMessageGetdetail.MESSAGE_V2);        // optional, CHAR, Message variable
                    im.setValue(messageV3, BapiMessageGetdetail.MESSAGE_V3);        // optional, CHAR, Message variable
                    im.setValue(messageV4, BapiMessageGetdetail.MESSAGE_V4);        // optional, CHAR, Message variable
                    im.setValue(BapiMessageGetdetail.ASC, BapiMessageGetdetail.TEXTFORMAT);       // CHAR, Format of text to be displayed

                    category.logT(Severity.PATH, location, "executing RFC BAPI_MESSAGE_GETDETAIL");
                    ((RFCDefaultClient)cachingClient).execute(functionBapiMessageGetdetail); // does not define ABAP Exceptions
                    category.logT(Severity.PATH, location, "done with RFC BAPI_MESSAGE_GETDETAIL");

                    String message = ex.getString(BapiMessageGetdetail.MESSAGE);
                    if ((message != null) && (!message.equals(""))){
                        location.debugT("Replaced text \"" + lm.getText() + "\" of loading message " + lm.getConfigObjKey() + "-" + lm.getNumber() + " by text \"" + message + "\".");
                        // replace text only if it is not null and not an empty string
                        lm.setText(message);
                        
                    }
                }
                catch(ClientException e) {
                    throw new IPCException(e);
                }
            }
        }
        location.exiting();
        return listOfLoadingMessages;
    }
}
