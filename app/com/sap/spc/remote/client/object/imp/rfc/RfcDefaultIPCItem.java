package com.sap.spc.remote.client.object.imp.rfc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Table;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.LoadingStatus;
import com.sap.spc.remote.client.object.OrderConfiguration;
import com.sap.spc.remote.client.object.ProductKey;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.spc.remote.client.object.imp.ConfigurationValue;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultTTEProduct;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.rfc.ExternalConfigConverter;
import com.sap.spc.remote.client.object.imp.DefaultTTEItem;
import com.sap.spc.remote.client.rfc.PricingConverter;
import com.sap.spc.remote.client.rfc.function.SpcChangeItemProduct;
import com.sap.spc.remote.client.rfc.function.SpcGetConfigItemInfo;
import com.sap.spc.remote.client.rfc.function.SpcGetDocumentAttrInfo;
import com.sap.spc.remote.client.rfc.function.SpcGetPricingDocumentInfo;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.rfc.function.SpcChangeItemConfig;
import com.sap.spc.remote.client.rfc.function.SpcChangeItems;
import com.sap.spc.remote.client.rfc.function.SpcCopyItem;
import com.sap.spc.remote.client.rfc.function.SpcCreateItems;
import com.sap.spc.remote.client.rfc.function.ComProductReadViaRfc;
import com.sap.spc.remote.client.rfc.function.SpcGetInitialConfiguration;
import com.sap.spc.remote.client.rfc.function.SpcGetProductGuids;
import com.sap.spc.remote.client.rfc.function.SpcGetPricingTrace;
import com.sap.spc.remote.client.rfc.function.SpcSetInitialConfiguration;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;


/**
 * @author I026584
 *
 */
public class RfcDefaultIPCItem extends DefaultIPCItem implements IPCItem {

	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(RfcDefaultIPCItem.class);
	protected HashMap data = new HashMap(); //Pricing cached data.
	public static final String INITIAL_GUID = "00000000000000000000000000000000"; //32 Zeros (If guid is null FM passes as 32 Zeros)
	

	// Constructors
	protected RfcDefaultIPCItem(RfcDefaultIPCDocument document, IPCItemProperties props) throws IPCException {
        _document = document;
        _parent   = null;
        _children = null;
        _childrenCopy = null;
        _tteItem = RfcDefaultTTEItem.C_NULL; // will be set separately
        
        TTEDocument tteDoc = document.getTteDocument();
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((DefaultIPCSession)document.getSession()).getPricingConverter();


        if (props instanceof DefaultIPCItemProperties) {
            _props = (DefaultIPCItemProperties)props;
        }
        else {
            _props = new RfcDefaultIPCItemProperties(props);
        }

        _closed = false;
        _client = ((RfcDefaultIPCSession)document.getSession()).getClient();

        try{
            JCO.Function functionSpcCreateItems = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_CREATE_ITEMS);
            JCO.ParameterList im = functionSpcCreateItems.getImportParameterList();
            JCO.ParameterList ex = functionSpcCreateItems.getExportParameterList();
            JCO.ParameterList tbl = functionSpcCreateItems.getTableParameterList();

            // process table imSpccreateitems_itItem
            // TABLE, Pricing Item With Marked Fields
            JCO.Table imSpccreateitems_itItem = im.getTable(SpcCreateItems.IT_ITEM);
            imSpccreateitems_itItem.appendRow();
            String productGuid = _props.getProductGuid(); // fm accepts only productGuid as of 5.0
            String productId = _props.getProductId();
			String productType = _props.getProductType();
			String productLogSys = _props.getProductLogSys();
            String productIdERP = null;
            
            
            if (_props.getItemId() != null && !_props.getItemId().equals("")) {
				imSpccreateitems_itItem.setValue(_props.getItemId(), SpcCreateItems.ITEM_ID);     // BYTE, Item-GUID
            }
            
            // For TTE a document-wide unique external id is required (passed by SPE via formula 11000 to TTE).
            // If it is not set we set it.
            if(_props.getExternalId() == null || _props.getExternalId().equals("")){
                _props.setExternalId(String.valueOf(document.getSequenceNo()));   
            }
            
            imSpccreateitems_itItem.setValue(_props.getExternalId(), SpcCreateItems.ITEM_ID_EXT);        // NUM, External Item Number
            
            // are we running in CRM  or in ERP?
            if (document.getApplication().equals(DefaultIPCDocument.CRM)){
                if (productGuid == null) {
                    if (productId != null) {
                        // if productGuid==null and a productId has been passed we try to convert productId to productGuid
                        productGuid = getProductGuid(productId, productType, productLogSys, _client);
                    }
                    imSpccreateitems_itItem.setValue(productGuid, SpcCreateItems.PRODUCT_ID);     // BYTE, Produkt-GUID
                    imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XPRODUCT_ID); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side 
                }
                else {
                    imSpccreateitems_itItem.setValue(productGuid, SpcCreateItems.PRODUCT_ID);     // BYTE, Produkt-GUID
                    imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XPRODUCT_ID); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side                
                }
            }
            else {
                productIdERP = productId;
                if (productIdERP != null)  {
                    imSpccreateitems_itItem.setValue(productIdERP, SpcCreateItems.PRODUCT_ERP);     // BYTE, Produkt-GUID
                    imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XPRODUCT_ID); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
                }                
            }
            if ((productGuid == null) && (productId == null) && (productIdERP == null)) {
                category.logT(Severity.ERROR, location, "Neither productGuid, productId nor productIdERP available! Pass the appropriate GUID/ID for your product.");
                throw new IPCException("Neither productGuid, productId nor productIdERP available! Pass the appropriate GUID/ID for your product.");            
            }            

			Boolean pricingRelevant = _props.isPricingRelevant();                    
            if (_props.getExchangeRateType() != null) {
                imSpccreateitems_itItem.setValue(_props.getExchangeRateType(), SpcCreateItems.EXCH_RATE_TYPE);      // CHAR, Exchange Rate Type
            }
            if (_props.getExchangeRateDate() != null) {
                imSpccreateitems_itItem.setValue(_props.getExchangeRateDate(), SpcCreateItems.EXCH_RATE_DATE);      // DATE, Exchange Rate Date
            }
            DimensionalValue salesQuantity = _props.getSalesQuantity();
			DimensionalValue baseQuantity = _props.getBaseQuantity();            
            if (baseQuantity != null && baseQuantity.getValueAsString() != null) {
				String unit = baseQuantity.getUnit();
				BigDecimal value = new BigDecimal(baseQuantity.getValueAsString());
				imSpccreateitems_itItem.setValue(unit, SpcCreateItems.ITEM_QUANTITY_UNIT);      // CHAR, Quantity Unit
				imSpccreateitems_itItem.setValue(value, SpcCreateItems.ITEM_QUANTITY);       // BCD, Quantity
				imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XQUANTITY); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
            }else if (salesQuantity != null && salesQuantity.getValueAsString() != null) {
                    // conversion needed because fm doesn't convert as of 5.0
                    String convertedUnit = pc.convertUOMExternalToInternal(salesQuantity.getUnit(), document.getLanguage());
                    BigDecimal convertedValue = pc.convertValueExternalToInternal(salesQuantity.getValueAsString(), salesQuantity.getUnit(), document.getLanguage(), document._getDocumentProperties(false).getDecimalSeparator(), document._getDocumentProperties(false).getGroupingSeparator());
                    imSpccreateitems_itItem.setValue(convertedUnit, SpcCreateItems.ITEM_QUANTITY_UNIT);      // CHAR, Quantity Unit
                    imSpccreateitems_itItem.setValue(convertedValue, SpcCreateItems.ITEM_QUANTITY);       // BCD, Quantity
                    imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XQUANTITY); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
            }else {
            	log.error("No quantity information given (neither base nor sales quantity set!");
            }
            
			if (pricingRelevant != null) {
				String rfcPricingRelevant = "";
				if (pricingRelevant.equals(Boolean.TRUE)) {
					rfcPricingRelevant = RfcConstants.XFLAG;
				}else {
					rfcPricingRelevant = RfcConstants.SPACEFLAG;
				}
				imSpccreateitems_itItem.setValue(rfcPricingRelevant, SpcCreateItems.PRICING_RELEVANT);
				imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XPRICING_RELEVANT); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
			}
                    
            boolean decoupleSubitems = _props.isDecoupleSubitems();
            String rfcDecoupleSubitems = decoupleSubitems ? RfcConstants.XFLAG : RfcConstants.SPACEFLAG;
            imSpccreateitems_itItem.setValue(rfcDecoupleSubitems, SpcCreateItems.DECOUPLE_SUBITEMS);
            
                    
            // process structure imSpccreateitems_itItemSpccreateitems_configData
            JCO.Structure imSpccreateitems_itItemSpccreateitems_configData = imSpccreateitems_itItem.getStructure(SpcCreateItems.CONFIG_DATA);
            // add knowledgebase information
            String kbLogSys = _props.getKbLogSys();
            if (kbLogSys != null) {
                imSpccreateitems_itItemSpccreateitems_configData.setValue(kbLogSys, SpcCreateItems.KB_LOGSYS);      // CHAR, Logical System
            }
            String kbName = _props.getKbName();
            if (kbName != null) {
                imSpccreateitems_itItemSpccreateitems_configData.setValue(kbName, SpcCreateItems.KB_NAME);      // CHAR, KB Name
            }
            String kbVersion = _props.getKbVersion();
            if (kbVersion != null) {
                imSpccreateitems_itItemSpccreateitems_configData.setValue(kbVersion, SpcCreateItems.KB_VERSION);        // CHAR, KB Version                
            }
            String kbProfile = _props.getKbProfile();
            if (kbProfile != null) {
                imSpccreateitems_itItemSpccreateitems_configData.setValue(kbProfile, SpcCreateItems.KB_PROFILE);        // CHAR, Profile name
            }
            String targetDate = _props.getDate();
            if (targetDate != null) {
                imSpccreateitems_itItemSpccreateitems_configData.setValue(targetDate, SpcCreateItems.KB_DATE);      // CHAR, date yyyymmdd
            }            
    
            // process structure imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_extConfig
            JCO.Structure imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_extConfig = imSpccreateitems_itItemSpccreateitems_configData.getStructure(SpcCreateItems.EXT_CONFIG);
            // add configuration data
            RfcConfigValue cfgValue = (RfcConfigValue)_props.getConfigValue();
            if (cfgValue != null) {
                // fill the ext_config structure
                cfgValue.getConfigIPCParameters(imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_extConfig);
            }
            if (productId != null) {
                imSpccreateitems_itItemSpccreateitems_configData.setValue(productId, SpcCreateItems.PRODUCT_ID);        // CHAR, Product ID
            }

            Map context = _props.getContext();
            JCO.Table imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_context = imSpccreateitems_itItemSpccreateitems_configData.getTable(SpcCreateItems.CONTEXT);
            if (context != null) {
                for (Iterator iter=context.keySet().iterator(); iter.hasNext();) {
                    String key = (String)iter.next();
                    String val = (String)context.get(key);
                    imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_context.appendRow();
                    imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_context.setValue(key, SpcCreateItems.NAME);     // CHAR, Configuration Context - Name
                    imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_context.setValue(val, SpcCreateItems.VALUE);       // CHAR, Configuration Context - Value
                }
            }
            
          
            Map headerAttributes = _props.getHeaderAttributes();
            Map itemAttributes = _props.getItemAttributes();
            // process table imSpccreateitems_itItemSpccreateitems_attributes
            JCO.Table imSpccreateitems_itItemSpccreateitems_attributes = imSpccreateitems_itItem.getTable(SpcCreateItems.ATTRIBUTES);
            // processing itemAttributes            
            for (Iterator iter=itemAttributes.keySet().iterator(); iter.hasNext();) {
                imSpccreateitems_itItemSpccreateitems_attributes.appendRow();
                String key = (String)iter.next();
                String val = (String)itemAttributes.get(key);
                imSpccreateitems_itItemSpccreateitems_attributes.setValue(key, SpcCreateItems.FIELDNAME);     // CHAR, Field name
                JCO.Table imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values = imSpccreateitems_itItemSpccreateitems_attributes.getTable(SpcCreateItems.VALUES);
                imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values.appendRow();
                imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values.setValue(val, "");        // CHAR, Value of Field in VAKEY/VADAT
            } 
            // processing headerAttributes
            for (Iterator iter=headerAttributes.keySet().iterator(); iter.hasNext();) {
                imSpccreateitems_itItemSpccreateitems_attributes.appendRow();
                String key = (String)iter.next();
                String val = (String)headerAttributes.get(key);
                imSpccreateitems_itItemSpccreateitems_attributes.setValue(key, SpcCreateItems.FIELDNAME);     // CHAR, Field name
                JCO.Table imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values = imSpccreateitems_itItemSpccreateitems_attributes.getTable(SpcCreateItems.VALUES);
                imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values.appendRow();
                imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values.setValue(val, "");        // CHAR, Value of Field in VAKEY/VADAT
            } 
            
            // process table imSpccreateitems_itItemSpccreateitems_timestamps
            JCO.Table imSpccreateitems_itItemSpccreateitems_timestamps = imSpccreateitems_itItem.getTable(SpcCreateItems.TIMESTAMPS);

            setConditionTimestamps(_props, imSpccreateitems_itItemSpccreateitems_timestamps);

            im.setValue(document.getId(), SpcCreateItems.IV_DOCUMENT_ID);       // BYTE, Pricing Document GUID
            // suppress pricing if a TTE document exists.
            if(document.getTteDocument() != RfcDefaultTTEDocument.C_NULL){
                im.setValue(SpcCreateItems.XFLAG, SpcCreateItems.IV_SUPPRESS_PRICING);     // CHAR, Do not execute pricing
            }
            
            category.logT(Severity.PATH, location, "executing RFC SPC_CREATE_ITEMS");
            ((RFCDefaultClient)_client).execute(functionSpcCreateItems); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_CREATE_ITEMS");
            
            JCO.Table exSpccreateitems_etItemId = ex.getTable(SpcCreateItems.ET_ITEM_ID);
            // there should be only one itemId because only one item has been passed.
            exSpccreateitems_etItemId.setRow(0);
            _itemId = exSpccreateitems_etItemId.getString("");
            this.setTechKey(new TechKey(_itemId));
            _config = new ConfigurationValue(document.getSession().getIPCClient(),
                                         com.sap.spc.remote.shared.ConfigIdManager.getConfigId(document.getId(),_itemId));
            _props.initializeValues((DefaultIPCSession)document.getSession(), document.getId(), _itemId, true/*default formatting*/, document.getSession().isExtendedDataEnabled());
            // if the props contain an initial config, we set it at the item
            if (_props.getInitialConfiguration() != null){
                this.setInitialConfiguration(_props.getInitialConfiguration());
            }
            if(document.getTteDocument() != RfcDefaultTTEDocument.C_NULL){ 
                DefaultTTEItem tteItem = IPCClientObjectFactory.getInstance().newTTEItem(_itemId, _props, document._getDocumentProperties(false), tteDoc, false); 
                tteDoc.addItem(tteItem); 
                this.setTteItem(tteItem);             
                DefaultTTEProduct tteProduct = IPCClientObjectFactory.getInstance().newTTEProduct(_props, document._getDocumentProperties(false));
                tteDoc.addProduct(tteProduct);
                tteDoc.modifyDocumentOnServer();
            }
            
            setCacheDirty();
        }catch(ClientException e){
            throw new IPCException(e);
        }
        _pricingConditionSet = new RfcDefaultIPCPricingConditionSet(this);
	}

	/**
	 * @param _props
	 * @param imSpccreateitems_itItemSpccreateitems_timestamps
	 */
	protected static void setConditionTimestamps(IPCItemProperties _props, Table imSpccreateitems_itItemSpccreateitems_timestamps) {
		String priceDate = _props.getDate();
		if (priceDate != null) {
			imSpccreateitems_itItemSpccreateitems_timestamps.appendRow();
			imSpccreateitems_itItemSpccreateitems_timestamps.setValue(SpcCreateItems.PRICE_DATE, SpcCreateItems.FIELDNAME);     // CHAR, Field Name for Determining Access Date
			imSpccreateitems_itItemSpccreateitems_timestamps.setValue(priceDate, SpcCreateItems.TIMESTAMP);     // CHAR, Time Stamp
			imSpccreateitems_itItemSpccreateitems_timestamps.appendRow();
			imSpccreateitems_itItemSpccreateitems_timestamps.setValue(SpcCreateItems.DET_DEFAULT_TIMESTAMP, SpcCreateItems.FIELDNAME);     // CHAR, Field Name for Determining Access Date
			imSpccreateitems_itItemSpccreateitems_timestamps.setValue(priceDate, SpcCreateItems.TIMESTAMP);     // CHAR, Time Stamp
		}
            
		Map timestamps = _props.getConditionTimestamps();
		if (timestamps != null) {
			for (Iterator it = timestamps.keySet().iterator(); it.hasNext(); ) {
				String timestampName = (String)it.next();
				String timestampValue = (String)timestamps.get(timestampName);
				imSpccreateitems_itItemSpccreateitems_timestamps.appendRow();
				imSpccreateitems_itItemSpccreateitems_timestamps.setValue(timestampName, SpcCreateItems.FIELDNAME);     // CHAR, Field Name for Determining Access Date
				imSpccreateitems_itItemSpccreateitems_timestamps.setValue(timestampValue, SpcCreateItems.TIMESTAMP);     // CHAR, Time Stamp
			}
		}	
	}

	//Don't try to call sycn process, as this constrectures are not internally calling any server command.
	protected RfcDefaultIPCItem(RFCDefaultClient client, RfcDefaultIPCDocument document, String itemId) throws IPCException {
		_client = client;
		_document = document;
		_itemId = itemId;
        this.setTechKey(new TechKey(_itemId));
		_props = factory.newIPCItemProperties();
		_props.setItemId(itemId);
		_resetInternalConfiguration();
		_props.initializeValues((DefaultIPCSession)document.getSession(), document.getId(), _itemId, true/*default formatting*/, document.getSession().isExtendedDataEnabled());
		_pricingConditionSet = new RfcDefaultIPCPricingConditionSet(this);
        _tteItem = RfcDefaultTTEItem.C_NULL; // will be set separately
	}

	//	Don't try to call sycn process, as this constrectures are not internally calling any server command.
	protected RfcDefaultIPCItem(RFCDefaultClient client, RfcDefaultIPCDocument document, String itemId, String parentItemId) throws IPCException {
		this(client, document, itemId);
		if (parentItemId == null || parentItemId.equals(RfcDefaultIPCItem.INITIAL_GUID) || parentItemId.equals("")) {
			_parent = null;
			// document.addItem(this); // removed because this could lead to adding the same item twice. an item should not add itself to the document, that should be done where the item is created and then the item should be added
		}
		else {
			_parent = ((RfcDefaultIPCDocument)document)._getItemRecursive(parentItemId);
			if (_parent == null){
				throw new IPCException(
					new ClientException(ClientException.RET_INTERNAL_ERROR,"Without parent item, trying to create subitem on client for itemId " + itemId + " with parentId " +parentItemId +". Please make sure the parentItem is already avaialble on the client before creating subItems.")
					);

			}else{
				_parent.addChild(this);
			}
		}
	}

	/**
     * @param doc IPCDocument for that items should be created
     * @param props Array of IPCItemProperties for the to be created items
     * @return Array of IPCItems
     * @throws IPCException
     */
	public static IPCItem[] createItems(IPCDocument doc, IPCItemProperties[] props)
		throws IPCException {
            location.entering("createItems(IPCDocument doc, IPCItemProperties[] props)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("IPCDocument doc " + doc.getId());
                location.debugT("IPCItemProperties[] props " + props);
                if (props != null) {
                    for (int i = 0; i<props.length; i++){
                        location.debugT("  props["+i+"] "+props[i].toString());
                    }
                }
            }
			if (!(doc instanceof RfcDefaultIPCDocument))
				throw new IllegalClassException(doc, RfcDefaultIPCDocument.class);
			RfcDefaultIPCDocument document = (RfcDefaultIPCDocument)doc;
            RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((DefaultIPCSession)document.getSession()).getPricingConverter();

            TTEDocument tteDoc = document.getTteDocument();

			IClient client = ((RfcDefaultIPCSession)document.getSession()).getClient();
            try{
                JCO.Function functionSpcCreateItems = ((RFCDefaultClient)client).getFunction(IFunctionGroup.SPC_CREATE_ITEMS);
                JCO.ParameterList im = functionSpcCreateItems.getImportParameterList();
                JCO.ParameterList ex = functionSpcCreateItems.getExportParameterList();
                JCO.ParameterList tbl = functionSpcCreateItems.getTableParameterList();
    
                im.setValue(document.getId(), SpcCreateItems.IV_DOCUMENT_ID);       // BYTE, Pricing Document GUID
    
                // process table imSpccreateitems_itItem
                // TABLE, Pricing Item With Marked Fields
                JCO.Table imSpccreateitems_itItem = im.getTable(SpcCreateItems.IT_ITEM);
                
                // loop of props                
    			for (int i=0; i<props.length; i++) {

                    imSpccreateitems_itItem.appendRow();
                    
					if (props[i].getItemId() != null && !props[i].getItemId().equals("")) {
						imSpccreateitems_itItem.setValue(props[i].getItemId(), SpcCreateItems.ITEM_ID);     // BYTE, Item-GUID
					}
                    String productGuid = props[i].getProductGuid();
                    String productId = props[i].getProductId();
					String productType = props[i].getProductType();
					String productLogSys = props[i].getProductLogSys();
                    String productIdERP = null;                    
                    Boolean pricingRelevant = props[i].isPricingRelevant();                    
                    String highLevelItemId = props[i].getHighLevelItemId();
                    DimensionalValue salesQuantity = props[i].getSalesQuantity();
                    DimensionalValue baseQuantity = props[i].getBaseQuantity();
                    String targetDate = props[i].getDate();
                    Map headerAttributes = props[i].getHeaderAttributes();
                    Map itemAttributes = props[i].getItemAttributes();      
                    // For TTE a document-wide unique external id is required (passed by SPE via formula 11000 to TTE).
                    // If it is not set we set it.
                    if(props[i].getExternalId() == null || props[i].getExternalId().equals("")){
                        props[i].setExternalId(String.valueOf(document.getSequenceNo()));   
                    }

                    imSpccreateitems_itItem.setValue(props[i].getExternalId(), SpcCreateItems.ITEM_ID_EXT);        // NUM, External Item Number

                    // are we running in CRM  or in ERP?
                    if (document.getApplication().equals(DefaultIPCDocument.CRM)){
                        if (productGuid == null) {
                            if (productId != null) {
                                // if productGuid==null and a productId has been passed we try to convert productId to productGuid
                                productGuid = getProductGuid(productId, productType, productLogSys, client);
                            }
                            imSpccreateitems_itItem.setValue(productGuid, SpcCreateItems.PRODUCT_ID);     // BYTE, Produkt-GUID
                            imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XPRODUCT_ID); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side 
                        }
                        else {
                            imSpccreateitems_itItem.setValue(productGuid, SpcCreateItems.PRODUCT_ID);     // BYTE, Produkt-GUID
                            imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XPRODUCT_ID); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side                
                        }
                    }
                    else {
                        productIdERP = productId;
                        if (productIdERP != null)  {
                            imSpccreateitems_itItem.setValue(productIdERP, SpcCreateItems.PRODUCT_ERP);     // BYTE, Produkt-GUID
                            imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XPRODUCT_ID); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
                        }                
                    }
                    if ((productGuid == null) && (productId == null) && (productIdERP == null)) {
                        category.logT(Severity.ERROR, location, "Neither productGuid, productId nor productIdERP available! Pass the appropriate GUID/ID for your product.");
                        throw new IPCException("Neither productGuid, productId nor productIdERP available! Pass the appropriate GUID/ID for your product.");            
                    }  

                    if (props[i].getExchangeRateType() != null) {
                        imSpccreateitems_itItem.setValue(props[i].getExchangeRateType(), SpcCreateItems.EXCH_RATE_TYPE);      // CHAR, Exchange Rate Type
                    }
                    if (props[i].getExchangeRateDate() != null) {
                        imSpccreateitems_itItem.setValue(props[i].getExchangeRateDate(), SpcCreateItems.EXCH_RATE_DATE);      // DATE, Exchange Rate Date
                    }
                    
					String unit;
					BigDecimal value;
                    if (baseQuantity != null && baseQuantity.getValueAsString() != null) {
                    	unit = baseQuantity.getUnit();
                    	value = new BigDecimal(baseQuantity.getValueAsString());
						imSpccreateitems_itItem.setValue(unit, SpcCreateItems.ITEM_QUANTITY_UNIT);      // CHAR, Quantity Unit
						imSpccreateitems_itItem.setValue(value, SpcCreateItems.ITEM_QUANTITY);       // BCD, Quantity
						imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XQUANTITY);// XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
                    }else if (salesQuantity != null && salesQuantity.getValueAsString() != null){
                            // conversion needed because fm doesn't convert as of 5.0
                            unit = pc.convertUOMExternalToInternal(salesQuantity.getUnit(), document.getLanguage());
                            value = pc.convertValueExternalToInternal(salesQuantity.getValueAsString(), salesQuantity.getUnit(), document.getLanguage(), document._getDocumentProperties(false).getDecimalSeparator(), document._getDocumentProperties(false).getGroupingSeparator());
                            imSpccreateitems_itItem.setValue(unit, SpcCreateItems.ITEM_QUANTITY_UNIT);      // CHAR, Quantity Unit
							imSpccreateitems_itItem.setValue(value, SpcCreateItems.ITEM_QUANTITY);       // BCD, Quantity
							imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XQUANTITY);// XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
                    }else {
                    	log.error("Neiter baseQuantity nor salesQuantity specified for the creation of item for " + productId);
                    }
                    
                    if (highLevelItemId != null) {
                        imSpccreateitems_itItem.setValue(highLevelItemId, SpcCreateItems.HIGH_LEVEL_ITEM_ID);       // BYTE, Item Number
                        imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XHIGH_LEVEL_ITEM_ID); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
                    }
                    
                    if (pricingRelevant != null) {
                    	String rfcPricingRelevant = "";
                    	if (pricingRelevant.equals(Boolean.TRUE)) {
							rfcPricingRelevant = RfcConstants.XFLAG;
                    	}else {
							rfcPricingRelevant = RfcConstants.SPACEFLAG;
                    	}
						imSpccreateitems_itItem.setValue(rfcPricingRelevant, SpcCreateItems.PRICING_RELEVANT);
						imSpccreateitems_itItem.setValue(SpcCreateItems.XFLAG, SpcCreateItems.XPRICING_RELEVANT); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
                    }
                    
                    boolean decoupleSubitems = props[i].isDecoupleSubitems();
                    String rfcDecoupleSubitems = decoupleSubitems ? RfcConstants.XFLAG : RfcConstants.SPACEFLAG;
                    imSpccreateitems_itItem.setValue(rfcDecoupleSubitems, SpcCreateItems.DECOUPLE_SUBITEMS);
                    
                    // process structure imSpccreateitems_itItemSpccreateitems_configData
                    JCO.Structure imSpccreateitems_itItemSpccreateitems_configData = imSpccreateitems_itItem.getStructure(SpcCreateItems.CONFIG_DATA);
                    // add knowledgebase information
                    String kbLogSys = props[i].getKbLogSys();
                    if (kbLogSys != null) {
                        imSpccreateitems_itItemSpccreateitems_configData.setValue(kbLogSys, SpcCreateItems.KB_LOGSYS);      // CHAR, Logical System
                    }
                    String kbName = props[i].getKbName();
                    if (kbName != null) {
                        imSpccreateitems_itItemSpccreateitems_configData.setValue(kbName, SpcCreateItems.KB_NAME);      // CHAR, KB Name
                    }
                    String kbVersion = props[i].getKbVersion();
                    if (kbVersion != null) {
                        imSpccreateitems_itItemSpccreateitems_configData.setValue(kbVersion, SpcCreateItems.KB_VERSION);        // CHAR, KB Version                
                    }
                    String kbProfile = props[i].getKbProfile();
                    if (kbProfile != null) {
                        imSpccreateitems_itItemSpccreateitems_configData.setValue(kbProfile, SpcCreateItems.KB_PROFILE);        // CHAR, Profile name
                    }
                    
                    if (targetDate != null) {
                        imSpccreateitems_itItemSpccreateitems_configData.setValue(targetDate, SpcCreateItems.KB_DATE);      // CHAR, date yyyymmdd
                    }            
            
                    // process structure imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_extConfig
                    JCO.Structure imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_extConfig = imSpccreateitems_itItemSpccreateitems_configData.getStructure(SpcCreateItems.EXT_CONFIG);
                    // add configuration data
                    RfcDefaultIPCItemProperties tprop = (RfcDefaultIPCItemProperties)props[i];
                    RfcConfigValue cfgValue = (RfcConfigValue)tprop.getConfigValue();
                    if (cfgValue != null) {
                        // fill the ext_config structure
                        cfgValue.getConfigIPCParameters(imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_extConfig);
                    }
                    if (productId != null) {
                        imSpccreateitems_itItemSpccreateitems_configData.setValue(productId, SpcCreateItems.PRODUCT_ID);        // CHAR, Product ID
                    }

                    // up to 4.0 an item-reference was also passed along with the context-parameters
                    // this is not necessary anymore because the context-data is part of the item-structure (according to M. Kaiser) 
                    Map context = props[i].getContext();
                    JCO.Table imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_context = imSpccreateitems_itItemSpccreateitems_configData.getTable(SpcCreateItems.CONTEXT);
                    if (context != null) {
                        for (Iterator iter=context.keySet().iterator(); iter.hasNext();) {
                            String key = (String)iter.next();
                            String val = (String)context.get(key);
                            imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_context.appendRow();
                            imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_context.setValue(key, SpcCreateItems.NAME);     // CHAR, Configuration Context - Name
                            imSpccreateitems_itItemSpccreateitems_configDataSpccreateitems_context.setValue(val, SpcCreateItems.VALUE);       // CHAR, Configuration Context - Value
                        }
                    }                    
                    
                    // up to 4.0 an item-reference was also passed along with the attributes
                    // this is not necessary anymore because the attributes-data is part of the item-structure
                    
                    // process table imSpccreateitems_itItemSpccreateitems_attributes
                    JCO.Table imSpccreateitems_itItemSpccreateitems_attributes = imSpccreateitems_itItem.getTable(SpcCreateItems.ATTRIBUTES);
                    // processing itemAttributes            
                    for (Iterator iter=itemAttributes.keySet().iterator(); iter.hasNext();) {
                        imSpccreateitems_itItemSpccreateitems_attributes.appendRow();
                        String key = (String)iter.next();
                        String val = (String)itemAttributes.get(key);
                        imSpccreateitems_itItemSpccreateitems_attributes.setValue(key, SpcCreateItems.FIELDNAME);     // CHAR, Field name
                        JCO.Table imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values = imSpccreateitems_itItemSpccreateitems_attributes.getTable(SpcCreateItems.VALUES);
                        imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values.appendRow();
                        imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values.setValue(val, "");        // CHAR, Value of Field in VAKEY/VADAT
                    } 
                    // processing headerAttributes
                    for (Iterator iter=headerAttributes.keySet().iterator(); iter.hasNext();) {
                        imSpccreateitems_itItemSpccreateitems_attributes.appendRow();
                        String key = (String)iter.next();
                        String val = (String)headerAttributes.get(key);
                        imSpccreateitems_itItemSpccreateitems_attributes.setValue(key, SpcCreateItems.FIELDNAME);     // CHAR, Field name
                        JCO.Table imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values = imSpccreateitems_itItemSpccreateitems_attributes.getTable(SpcCreateItems.VALUES);
                        imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values.appendRow();
                        imSpccreateitems_itItemSpccreateitems_attributesSpccreateitems_values.setValue(val, "");        // CHAR, Value of Field in VAKEY/VADAT
                    } 
                    
                    // process table imSpccreateitems_itItemSpccreateitems_timestamps
                    JCO.Table imSpccreateitems_itItemSpccreateitems_timestamps = imSpccreateitems_itItem.getTable(SpcCreateItems.TIMESTAMPS);
                    
                    setConditionTimestamps(props[i], imSpccreateitems_itItemSpccreateitems_timestamps);
//                    if (targetDate != null) {
//                        imSpccreateitems_itItemSpccreateitems_timestamps.appendRow();
//                        imSpccreateitems_itItemSpccreateitems_timestamps.setValue(SpcCreateItems.PRICE_DATE, SpcCreateItems.FIELDNAME);     // CHAR, Field Name for Determining Access Date
//                        imSpccreateitems_itItemSpccreateitems_timestamps.setValue(targetDate, SpcCreateItems.TIMESTAMP);     // CHAR, Time Stamp
//                        imSpccreateitems_itItemSpccreateitems_timestamps.appendRow();
//                        imSpccreateitems_itItemSpccreateitems_timestamps.setValue(SpcCreateItems.DET_DEFAULT_TIMESTAMP, SpcCreateItems.FIELDNAME);     // CHAR, Field Name for Determining Access Date
//                        imSpccreateitems_itItemSpccreateitems_timestamps.setValue(targetDate, SpcCreateItems.TIMESTAMP);     // CHAR, Time Stamp
//                    }
        
                } // loop of props end
                
                // suppress pricing if a TTE document exists.
                if(document.getTteDocument() != RfcDefaultTTEDocument.C_NULL){
                    im.setValue(SpcCreateItems.XFLAG, SpcCreateItems.IV_SUPPRESS_PRICING);     // CHAR, Do not execute pricing
                }
                category.logT(Severity.PATH, location, "executing RFC SPC_CREATE_ITEMS");
                ((RFCDefaultClient)client).execute(functionSpcCreateItems); // does not define ABAP Exceptions
                category.logT(Severity.PATH, location, "done with RFC SPC_CREATE_ITEMS");
                
                JCO.Table exSpccreateitems_etItemId = ex.getTable(SpcCreateItems.ET_ITEM_ID);
                int numRows = exSpccreateitems_etItemId.getNumRows();
                String[] itemIds = new String[numRows];
                for (int exSpccreateitems_etItemIdCounter=0; exSpccreateitems_etItemIdCounter<numRows; exSpccreateitems_etItemIdCounter++) {
                    exSpccreateitems_etItemId.setRow(exSpccreateitems_etItemIdCounter);
                    itemIds[exSpccreateitems_etItemIdCounter] = exSpccreateitems_etItemId.getString("");
                }

                DefaultIPCItem[] items = new DefaultIPCItem[itemIds.length];
                HashMap itemsWithInitialConfig = new HashMap();
                for (int i=0; i<itemIds.length; i++) { 
                    // create the new item-object for the returned itemId
                    items[i] = IPCClientObjectFactory.getInstance().newIPCItem(client, document, itemIds[i]);
                    // the _client member of ItemProperties' configValue has to be set (done via the initializeValues method; only the session is used - the other paramters are ignored);
                    ((DefaultIPCItemProperties)props[i]).initializeValues((DefaultIPCSession)document.getSession(), null, null, false, false);
                    // use the passed itemProperties-object for the new generated item (in order to avoid the loss of data already set in the passed itemProperties)
                    items[i].setItemProperties((DefaultIPCItemProperties)props[i]);
                    // If the props contain an initial config, we put the item and the initial config into a special HashMap
                    // in order to set the initial configuration at the item after all items for this document have been created.
                    // If we would set the initial configuration directly this would lead to sync-problems because the 
                    // setInitialConfig-method internally has to execute a syncWithServer (but not all items have been created so far 
                    // on the client side).
                    if (props[i].getInitialConfiguration() != null){
                        // we memorize the item that has an initial configuration (to set it later)
                        itemsWithInitialConfig.put(items[i], props[i].getInitialConfiguration());
                    }
                    document.addItem(items[i]);
                    // create TTE items and products if a TTE document exists
                    if(document.getTteDocument() != RfcDefaultTTEDocument.C_NULL){ 
                        DefaultTTEItem tteItem = IPCClientObjectFactory.getInstance().newTTEItem(itemIds[i], props[i], document._getDocumentProperties(false), tteDoc, false); 
                        tteDoc.addItem(tteItem); 
                        items[i].setTteItem(tteItem);             
                        DefaultTTEProduct tteProduct = IPCClientObjectFactory.getInstance().newTTEProduct(props[i], document._getDocumentProperties(false));
                        tteDoc.addProduct(tteProduct);
                    }
                }
                // if there are items with initial configuration we set them
                Iterator itemsWithInitialConfigIter = itemsWithInitialConfig.keySet().iterator();
                while (itemsWithInitialConfigIter.hasNext()){
                    DefaultIPCItem currItem = (DefaultIPCItem)itemsWithInitialConfigIter.next();
                    ext_configuration initialConfig = (ext_configuration) itemsWithInitialConfig.get(currItem);
                    // set the initial config only if the item is configurable
                    if (currItem.isConfigurable()) {
                        currItem.setInitialConfiguration(initialConfig);
                    } 
                    else {
                        location.errorT("An initial configuration has been passed for item " + currItem.getItemId() 
                            + ". But this item is not configurable! We don't set the initial configuration."); 
                    }
                }
                // modify the TTE document if one exists
                if(document.getTteDocument() != RfcDefaultTTEDocument.C_NULL){
                    tteDoc.modifyDocumentOnServer();
                }
                document.setCacheDirty();
                if (location.beDebug()){
                    if (items != null) {
                        location.debugT("return items");
                        for (int i = 0; i<items.length; i++){
                            location.debugT("  items["+i+"] "+items[i].getItemId());
                        }
                    }
                }
                location.exiting();
                return items;
            }catch(ClientException e){
                throw new IPCException(e);
            }
	}

	/* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getItemReference()
     */
	public IPCItemReference getItemReference() {
        location.entering("getItemReference()");
		RFCIPCItemReference ref = new RFCIPCItemReference();
		RfcDefaultIPCSession session = (RfcDefaultIPCSession)_document.getSession();
		RFCDefaultClient rfcClient = (RFCDefaultClient)session.getClient();
		JCoConnection connection = rfcClient.getDefaultIPCJCoConnection();
		ref.setConnectionKey(connection.getConnectionKey());
		ref.setDocumentId(_document.getId());
		ref.setItemId(getItemId());
        if (location.beDebug()){
            location.debugT("return ref " + ref.getItemId());
        }
        location.exiting();
		return ref;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setHeaderAttributeBindings(java.lang.String[], java.lang.String[])
	 */
	public void setHeaderAttributeBindings(String[] names, String[] values)
		throws IPCException {
            location.entering("setHeaderAttributeBindings(String[] names, String[] values)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String[] names ");
                if (names != null) {
                    for (int i = 0; i<names.length; i++){
                        location.debugT("  names["+i+"] "+names[i]);
                    }
                }
                location.debugT("String[] values ");
                if (values != null) {
                    for (int i = 0; i<values.length; i++){
                        location.debugT("  values["+i+"] "+values[i]);
                    }
                }
            }
			Map attr = _props.getHeaderAttributes();
            JCO.Function functionSpcChangeItems = getChangeItemsFunction();
            JCO.ParameterList im = functionSpcChangeItems.getImportParameterList();
            JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
            if (imSpcchangeitems_itItem.getNumRows() == 0) {
                imSpcchangeitems_itItem.appendRow();
            }
            JCO.Table imSpcchangeitems_itItemSpcchangeitems_attributes = imSpcchangeitems_itItem.getTable(SpcChangeItems.ATTRIBUTES);
            for (int i=0; i<names.length; i++) {
                imSpcchangeitems_itItemSpcchangeitems_attributes.appendRow();
                imSpcchangeitems_itItemSpcchangeitems_attributes.setValue(names[i], SpcChangeItems.FIELDNAME);     // CHAR, Field name
                JCO.Table imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values = imSpcchangeitems_itItemSpcchangeitems_attributes.getTable(SpcChangeItems.VALUES);
                // there is only one value per name
                imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values.appendRow();
                imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values.setValue(values[i], "");        // CHAR, Value of Field in VAKEY/VADAT
				attr.put(names[i],values[i]);
            }            
            _props.setHeaderAttributes(attr);
            _changeItem(null, null, im, functionSpcChangeItems);         
            location.exiting();   
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setItemAttributeBindings(java.lang.String[], java.lang.String[])
	 */
	public void setItemAttributeBindings(String[] names, String[] values)
		throws IPCException {
            location.entering("setItemAttributeBindings(String[] names, String[] values)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String[] names ");
                if (names != null) {
                    for (int i = 0; i<names.length; i++){
                        location.debugT("  names["+i+"] "+names[i]);
                    }
                }
                location.debugT("String[] values ");
                if (values != null) {
                    for (int i = 0; i<values.length; i++){
                        location.debugT("  values["+i+"] "+values[i]);
                    }
                }
            }
			Map attr = _props.getItemAttributes();
            JCO.Function functionSpcChangeItems = getChangeItemsFunction();
            JCO.ParameterList im = functionSpcChangeItems.getImportParameterList();
            JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
            if (imSpcchangeitems_itItem.getNumRows() == 0) {
                imSpcchangeitems_itItem.appendRow();
            }
            JCO.Table imSpcchangeitems_itItemSpcchangeitems_attributes = imSpcchangeitems_itItem.getTable(SpcChangeItems.ATTRIBUTES);
            for (int i=0; i<names.length; i++) {
                imSpcchangeitems_itItemSpcchangeitems_attributes.appendRow();
                imSpcchangeitems_itItemSpcchangeitems_attributes.setValue(names[i], SpcChangeItems.FIELDNAME);     // CHAR, Field name
                JCO.Table imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values = imSpcchangeitems_itItemSpcchangeitems_attributes.getTable(SpcChangeItems.VALUES);
                // there is only one value per name
                imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values.appendRow();
                imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values.setValue(values[i], "");        // CHAR, Value of Field in VAKEY/VADAT
				attr.put(names[i],values[i]);
			}            
			_props.setItemAttributes(attr);
            
            _changeItem(null, null, im, functionSpcChangeItems);
            location.exiting();           
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setContext(java.util.Map)
	 */
	public void setContext(Map context) throws IPCException {
        location.entering("setContext(Map context)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (context!=null){
                location.debugT("Map context " + context.toString());
            }
            else location.debugT("Map context null");
        }
        JCO.Function functionSpcChangeItemConfig = getChangeItemConfigFunction();   
        JCO.ParameterList im = functionSpcChangeItemConfig.getImportParameterList();     
        JCO.Table imSpcchangeitemconfig_context = im.getTable(SpcChangeItemConfig.CONTEXT);
        if (context != null) {
            for (Iterator iter=context.keySet().iterator(); iter.hasNext();) {
                String key = (String)iter.next();
                String val = (String)context.get(key);
                imSpcchangeitemconfig_context.appendRow();
                imSpcchangeitemconfig_context.setValue(key, SpcChangeItemConfig.NAME);     // CHAR, Configuration Context - Name
                imSpcchangeitemconfig_context.setValue(val, SpcChangeItemConfig.VALUE);       // CHAR, Configuration Context - Value
            }
        }

        _changeItem(im, functionSpcChangeItemConfig, null, null);
		_resetInternalConfiguration();
        location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setAdditionalParameter(java.lang.String, java.lang.String)
	 */
	public void setAdditionalParameter(String param, String value)
		throws IPCException {
            throw new UnsupportedOperationException("Function module SPC_CHANGE_ITEM_CONFIG doesn't allow generic additional parameters.");
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setConfig(com.sap.sce.kbrt.ext_configuration)
	 */
	public void setConfig(ext_configuration config) throws IPCException {
        location.entering("setConfig(ext_configuration config)");
        if (location.beDebug()){
            if (config != null){
                String xml = ((c_ext_cfg_imp)config).cfg_ext_to_xml_string();
                location.debugT("External configuration to be set:");
                location.debugT(xml);
            }
            else {
                location.debugT("External configuration is null!");
            }
        }
        JCO.Function function = getChangeItemConfigFunction();
        JCO.ParameterList im = function.getImportParameterList();
        Map context = _props.getContext();
        JCO.Table imSpcchangeitemconfig_context = im.getTable(SpcChangeItemConfig.CONTEXT);
        if (context != null) {
            for (Iterator iter=context.keySet().iterator(); iter.hasNext();) {
                String key = (String)iter.next();
                String val = (String)context.get(key);
                imSpcchangeitemconfig_context.appendRow();
                imSpcchangeitemconfig_context.setValue(key, SpcChangeItemConfig.NAME);     // CHAR, Configuration Context - Name
                imSpcchangeitemconfig_context.setValue(val, SpcChangeItemConfig.VALUE);       // CHAR, Configuration Context - Value
            }
        }
        JCO.Structure imSpcchangeitemconfig_extConfigGuid = im.getStructure(SpcChangeItemConfig.EXT_CONFIG_GUID);
        JCO.Structure imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extConfig = imSpcchangeitemconfig_extConfigGuid.getStructure(SpcChangeItemConfig.EXT_CONFIG);
		RfcConfigValue cValue = new RfcConfigValue();
        cValue.getConfigIPCParameters(imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extConfig, config);
        _changeItem(im, function, null, null);
		_resetInternalConfiguration();
        location.exiting();
	}
	
	public void setConfig(ext_configuration config, Hashtable context) throws IPCException {
        location.entering("setConfig(ext_configuration config)");
        if (location.beDebug()){
            if (config != null){
                String xml = ((c_ext_cfg_imp)config).cfg_ext_to_xml_string();
                location.debugT("External configuration to be set:");
                location.debugT(xml);
            }
            else {
                location.debugT("External configuration is null!");
            }
        }
        JCO.Function function = getChangeItemConfigFunction();
        JCO.ParameterList im = function.getImportParameterList();
        //context = _props.getContext();
        JCO.Table imSpcchangeitemconfig_context = im.getTable(SpcChangeItemConfig.CONTEXT);
        if (context != null) {
            for (Iterator iter=context.keySet().iterator(); iter.hasNext();) {
                String key = (String)iter.next();
                String val = (String)context.get(key);
                imSpcchangeitemconfig_context.appendRow();
                imSpcchangeitemconfig_context.setValue(key, SpcChangeItemConfig.NAME);     // CHAR, Configuration Context - Name
                imSpcchangeitemconfig_context.setValue(val, SpcChangeItemConfig.VALUE);       // CHAR, Configuration Context - Value
            }
        }
        JCO.Structure imSpcchangeitemconfig_extConfigGuid = im.getStructure(SpcChangeItemConfig.EXT_CONFIG_GUID);
        JCO.Structure imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extConfig = imSpcchangeitemconfig_extConfigGuid.getStructure(SpcChangeItemConfig.EXT_CONFIG);
		RfcConfigValue cValue = new RfcConfigValue();
        cValue.getConfigIPCParameters(imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extConfig, config);
        _changeItem(im, function, null, null);
		_resetInternalConfiguration();
        location.exiting();
	}

	/* (non-Javadoc)
	 * @deprecated
	 * @see com.sap.spc.remote.client.object.IPCItem#setConfig(com.sap.spc.remote.client.object.OrderConfiguration)
	 */
	public void setConfig(OrderConfiguration config) throws IPCException {
        location.entering("setConfig(OrderConfiguration config)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("OrderConfiguration config " + config.getConfiguration().get_name());
        }
        JCO.Function function = getChangeItemConfigFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Structure imSpcchangeitemconfig_extConfigGuid = im.getStructure(SpcChangeItemConfig.EXT_CONFIG_GUID);
        JCO.Structure imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extConfig = imSpcchangeitemconfig_extConfigGuid.getStructure(SpcChangeItemConfig.EXT_CONFIG);
        RfcConfigValue cValue = new RfcConfigValue();
        cValue.getConfigIPCParameters(imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extConfig, config.getConfiguration());
        JCO.Table imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extIds = imSpcchangeitemconfig_extConfigGuid.getTable(SpcChangeItemConfig.EXT_IDS);
		Map m = config.getPosIdToInstIdMap();
        if (m != null) {
			for (Iterator iter=m.keySet().iterator(); iter.hasNext();) {
				String posId = (String)iter.next();
				String instId = (String)m.get(posId);
                imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extIds.appendRow();
                imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extIds.setValue(instId, SpcChangeItemConfig.INST_ID);        // CHAR, Internal identifier of a Instance
                imSpcchangeitemconfig_extConfigGuidSpcchangeitemconfig_extIds.setValue(posId, SpcChangeItemConfig.GUID);     // CHAR, Global Identifier
                // EXT_POS_CONFIG_ID is not available in table EXT_IDS
                // parA.add(ChangeItemConfig.EXT_POS_CONFIG_ID);
                // parA.add("1"); // will be ignored anyway
			}
		}
		String kbDate = config.getDate();
		if (kbDate != null && kbDate.length() > 0) {
            im.setValue(kbDate, SpcChangeItemConfig.KB_DATE);       // optional, CHAR, KB validity date (YYYYMMDD)
		}
        _changeItem(im, function, null, null);
		_resetInternalConfiguration();
        location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setStatistical(boolean)
	 */
	public void setStatistical(boolean flag) throws IPCException {
        location.entering("setStatistical(boolean flag)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean flag " + flag);
        }        
        JCO.Function function = getChangeItemsFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
        if (imSpcchangeitems_itItem.getNumRows() == 0) {
            imSpcchangeitems_itItem.appendRow();
        }
        imSpcchangeitems_itItem.setValue(flag ? SpcChangeItems.XFLAG : "", SpcChangeItems.STATISTICAL);      // CHAR, Position ist statistisch         
        imSpcchangeitems_itItem.setValue(SpcChangeItems.XFLAG, SpcChangeItems.XSTATISTICAL); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side

        _changeItem(null, null, im, function);
        location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setPricingDate(java.lang.String)
	 */
	public void setPricingDate(String s) throws IPCException {
        location.entering("setPricingDate(String s)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String s " + s);
        }
        JCO.Function function = getChangeItemsFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
        if (imSpcchangeitems_itItem.getNumRows() == 0) {
            imSpcchangeitems_itItem.appendRow();
        }
        JCO.Table imSpcchangeitems_itItemSpcchangeitems_timestamps = imSpcchangeitems_itItem.getTable(SpcChangeItems.TIMESTAMPS);
        imSpcchangeitems_itItemSpcchangeitems_timestamps.appendRow();
        imSpcchangeitems_itItemSpcchangeitems_timestamps.setValue(SpcCreateItems.PRICE_DATE, SpcCreateItems.FIELDNAME);     // CHAR, Field Name for Determining Access Date
        imSpcchangeitems_itItemSpcchangeitems_timestamps.setValue(s, SpcCreateItems.TIMESTAMP);     // CHAR, Time Stamp
        imSpcchangeitems_itItemSpcchangeitems_timestamps.appendRow();
        imSpcchangeitems_itItemSpcchangeitems_timestamps.setValue(SpcCreateItems.DET_DEFAULT_TIMESTAMP, SpcCreateItems.FIELDNAME);     // CHAR, Field Name for Determining Access Date
        imSpcchangeitems_itItemSpcchangeitems_timestamps.setValue(s, SpcCreateItems.TIMESTAMP);     // CHAR, Time Stamp
        
        _changeItem(null, null, im, function); 
        location.exiting();       
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setNetWeight(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setNetWeight(DimensionalValue weight) {
		try {
            location.entering("setNetWeight(DimensionalValue weight)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("DimensionalValue weight " + weight.getValueAsString() + " " + weight.getUnit());
            }
            JCO.Function function = getChangeItemsFunction();
            JCO.ParameterList im = function.getImportParameterList();
            JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
            if (imSpcchangeitems_itItem.getNumRows() == 0) {
                imSpcchangeitems_itItem.appendRow();
            }
            RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((DefaultIPCSession)_document.getSession()).getPricingConverter();
            String convertedUnit = pc.convertUOMExternalToInternal(weight.getUnit(), _document.getLanguage());
            BigDecimal convertedValue = pc.convertValueExternalToInternal(weight.getValueAsString(), weight.getUnit(), _document.getLanguage(), _document._getDocumentProperties(false).getDecimalSeparator(), _document._getDocumentProperties(false).getGroupingSeparator());
            
            imSpcchangeitems_itItem.setValue(convertedValue, SpcChangeItems.NET_WEIGHT);     // BCD, Nettogewicht           
            imSpcchangeitems_itItem.setValue(convertedUnit, SpcChangeItems.WEIGHT_UNIT);      // CHAR, Gewichtseinheit
            imSpcchangeitems_itItem.setValue(SpcChangeItems.XFLAG, SpcChangeItems.XWEIGHT);// XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
            
            _changeItem(null, null, im, function);  
            location.exiting();          
		}
		catch(IPCException e) {
			throw new RuntimeException("DefaultIPCItem.setNetWeight is supposed to be called with a manually constructed weight");
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setVolume(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setVolume(DimensionalValue value) throws IPCException {
        location.entering("setVolume(DimensionalValue value)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue value " + value.getValueAsString() + " " + value.getUnit());
        }
        JCO.Function function = getChangeItemsFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
        if (imSpcchangeitems_itItem.getNumRows() == 0) {
            imSpcchangeitems_itItem.appendRow();
        }
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((DefaultIPCSession)_document.getSession()).getPricingConverter();
        String convertedUnit = pc.convertUOMExternalToInternal(value.getUnit(), _document.getLanguage());
        BigDecimal convertedValue = pc.convertValueExternalToInternal(value.getValueAsString(), value.getUnit(), _document.getLanguage(), _document._getDocumentProperties(false).getDecimalSeparator(), _document._getDocumentProperties(false).getGroupingSeparator());
 
        imSpcchangeitems_itItem.setValue(convertedValue, SpcChangeItems.VOLUM);      // BCD, Volumen
        imSpcchangeitems_itItem.setValue(convertedUnit, SpcChangeItems.VOLUME_UNIT);       // CHAR, Volumeneinheit
        imSpcchangeitems_itItem.setValue(SpcChangeItems.XFLAG, SpcChangeItems.XVOLUME); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
        
        _changeItem(null, null, im, function);
        location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setSalesQuantity(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setBaseQuantity(DimensionalValue value) throws IPCException {
        location.entering("setBaseQuantity(DimensionalValue value)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue value " + value.getValueAsString() + " " + value.getUnit());
        }
		JCO.Function function = getChangeItemsFunction();
		JCO.ParameterList im = function.getImportParameterList();
		JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
		if (imSpcchangeitems_itItem.getNumRows() == 0) {
			imSpcchangeitems_itItem.appendRow();
		}
		imSpcchangeitems_itItem.setValue(new BigDecimal(value.getValueAsString()), SpcChangeItems.ITEM_QUANTITY);       // BCD, Quantity
		imSpcchangeitems_itItem.setValue(value.getUnit(), SpcChangeItems.ITEM_QUANTITY_UNIT);      // CHAR, Quantity Unit
		imSpcchangeitems_itItem.setValue(SpcChangeItems.XFLAG, SpcChangeItems.XQUANTITY); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
        
		_changeItem(null, null, im, function);
        location.entering();
	}
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setSalesQuantity(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setSalesQuantity(DimensionalValue value) throws IPCException {
        location.entering("setSalesQuantity(DimensionalValue value)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue value " + value.getValueAsString() + " " + value.getUnit());
        }
        JCO.Function function = getChangeItemsFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
        if (imSpcchangeitems_itItem.getNumRows() == 0) {
            imSpcchangeitems_itItem.appendRow();
        }
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((DefaultIPCSession)_document.getSession()).getPricingConverter();
		String convertedUnit = pc.convertUOMExternalToInternal(value.getUnit(), _document.getLanguage());
		BigDecimal convertedValue = pc.convertValueExternalToInternal(value.getValueAsString(), value.getUnit(), _document.getLanguage(), _document._getDocumentProperties(false).getDecimalSeparator(), _document._getDocumentProperties(false).getGroupingSeparator());
        
        imSpcchangeitems_itItem.setValue(convertedValue, SpcChangeItems.ITEM_QUANTITY);       // BCD, Quantity
        imSpcchangeitems_itItem.setValue(convertedUnit, SpcChangeItems.ITEM_QUANTITY_UNIT);      // CHAR, Quantity Unit
        imSpcchangeitems_itItem.setValue(SpcChangeItems.XFLAG, SpcChangeItems.XQUANTITY); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
        
        _changeItem(null, null, im, function);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setGrossWeight(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setGrossWeight(DimensionalValue weight) throws IPCException {
        location.entering("setGrossWeight(DimensionalValue weight)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue weight " + weight.getValueAsString() + " " + weight.getUnit());
        }
        JCO.Function function = getChangeItemsFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
        if (imSpcchangeitems_itItem.getNumRows() == 0) {
            imSpcchangeitems_itItem.appendRow();
        }
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((DefaultIPCSession)_document.getSession()).getPricingConverter();
        String convertedUnit = pc.convertUOMExternalToInternal(weight.getUnit(), _document.getLanguage());
        BigDecimal convertedValue = pc.convertValueExternalToInternal(weight.getValueAsString(), weight.getUnit(), _document.getLanguage(), _document._getDocumentProperties(false).getDecimalSeparator(), _document._getDocumentProperties(false).getGroupingSeparator());
        
        imSpcchangeitems_itItem.setValue(convertedValue, SpcChangeItems.GROSS_WEIGHT);     // BCD, Bruttogewicht            
        imSpcchangeitems_itItem.setValue(convertedUnit, SpcChangeItems.WEIGHT_UNIT);
        imSpcchangeitems_itItem.setValue(SpcChangeItems.XFLAG, SpcChangeItems.XWEIGHT); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side
        _changeItem(null, null, im, function);
        location.exiting();        
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setExchangeRateType(java.lang.String)
	 */
	public void setExchangeRateType(String exchangeRateType)
		throws IPCException {
            location.entering("setExchangeRateType(String exchangeRateType)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String exchangeRateType " + exchangeRateType);
            }
            JCO.Function function = getChangeItemsFunction();
            JCO.ParameterList im = function.getImportParameterList();
            JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
            if (imSpcchangeitems_itItem.getNumRows() == 0) {
                imSpcchangeitems_itItem.appendRow();
            }
            imSpcchangeitems_itItem.setValue(exchangeRateType, SpcChangeItems.EXCH_RATE_TYPE);      // CHAR, Exchange Rate Type            
            
            _changeItem(null, null, im, function);            
			// sync props
			_props.setExchangeRateType(exchangeRateType);
            location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setExchangeRateDate(java.lang.String)
	 */
	public void setExchangeRateDate(String exchangeRateDate)
		throws IPCException {
            location.entering("setExchangeRateDate(String exchangeRateDate)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String exchangeRateDate " + exchangeRateDate);
            }
            JCO.Function function = getChangeItemsFunction();
            JCO.ParameterList im = function.getImportParameterList();
            JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
            if (imSpcchangeitems_itItem.getNumRows() == 0) {
                imSpcchangeitems_itItem.appendRow();
            }
            imSpcchangeitems_itItem.setValue(exchangeRateDate, SpcChangeItems.EXCH_RATE_DATE);            

            _changeItem(null, null, im, function);
			// sync props
            _props.setExchangeRateDate(exchangeRateDate);
            location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#copyItem(com.sap.spc.remote.client.object.IPCDocument)
	 */
	public IPCItem copyItem(IPCDocument targetDocument) throws IPCException {
        location.entering("copyItem(IPCDocument targetDocument)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCDocument targetDocument " + targetDocument.getId());
        }
		if (!(targetDocument instanceof RfcDefaultIPCDocument))
			throw new IllegalClassException(targetDocument, RfcDefaultIPCDocument.class);

        try {
            JCO.Function functionSpcCopyItem = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_COPY_ITEM);
            JCO.ParameterList im = functionSpcCopyItem.getImportParameterList();
            JCO.ParameterList ex = functionSpcCopyItem.getExportParameterList();
            JCO.ParameterList tbl = functionSpcCopyItem.getTableParameterList();

            im.setValue(SpcCopyItem.FALSE, SpcCopyItem.REVERT_CONFIG);       // optional, CHAR, Pricing Document GUID
            im.setValue(_document.getId(), SpcCopyItem.SOURCE_DOCUMENT_ID);      // BYTE, Pricing Document GUID
            im.setValue(_itemId, SpcCopyItem.SOURCE_ITEM_ID);      // BYTE, Item Number
            im.setValue(targetDocument.getId(), SpcCopyItem.TARGET_DOCUMENT_ID);      // BYTE, Pricing Document GUID
            
            category.logT(Severity.PATH, location, "executing RFC SPC_COPY_ITEM");
            ((RFCDefaultClient)_client).execute(functionSpcCopyItem);
            category.logT(Severity.PATH, location, "done with RFC SPC_COPY_ITEM");

            String targetItemId = ex.getString(SpcCopyItem.TARGET_ITEM_ID);
            DefaultIPCItem newItem = factory.newIPCItem(_client, (RfcDefaultIPCDocument)targetDocument, targetItemId);
            
            // create a copy of the existing itemProps using the original ones
            DefaultIPCItemProperties newProps = factory.newIPCItemProperties(_props);
            
            // the _client member of ItemProperties' configValue has to be set (to avoid NullPointerExceptions during execution of initProperties()-method later)
            newProps.getConfigValue().setInitialValue(_client);
            
            // set the new props at the new item
            newItem.setItemProperties(newProps);
            
            targetDocument.addItem(newItem);
            
            // create TTE item and product if a TTE document exists
            if(targetDocument.getTteDocument() != RfcDefaultTTEDocument.C_NULL){
                // The copied item has the same external id as the source item. 
                // This leads to problems during TTE because the external id is used by TTE as
                // position number and this has to be unique.
                // Therefore we set the external id explicitly.
                int extId = ((DefaultIPCDocument)targetDocument).getSequenceNo();
                newItem.changeExternalId(String.valueOf(extId));                
                TTEDocument targetTTEDocument = targetDocument.getTteDocument();
                DefaultTTEItem tteItem = IPCClientObjectFactory.getInstance().newTTEItem(targetItemId, newProps, ((DefaultIPCDocument)targetDocument)._getDocumentProperties(false), targetTTEDocument, false); 
                targetTTEDocument.addItem(tteItem); 
                newItem.setTteItem(tteItem);             
                DefaultTTEProduct tteProduct = IPCClientObjectFactory.getInstance().newTTEProduct(newProps, ((DefaultIPCDocument)targetDocument)._getDocumentProperties(false));
                targetTTEDocument.addProduct(tteProduct);
                targetTTEDocument.modifyDocumentOnServer();
            }
            
            
			((DefaultIPCItem)newItem).setCacheDirty();
			// removed the syncWithServer because this lead to adding multiple items to the document
			//((RfcDefaultIPCDocument)targetDocument).syncWithServer();
            if (location.beDebug()){
                location.debugT("return newItem" + newItem.getItemId());
            }
            location.exiting();
            return newItem;
        }
        catch(ClientException e) {
            throw new IPCException(e);
        }
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#_getExternalPricingInfo()
	 */
	protected HashMap _getExternalPricingInfo() throws IPCException {
        
		
        data = new HashMap();
        
        try{
            JCO.Function functionSpcGetPricingDocumentInfo = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_PRICING_DOCUMENT_INFO);
            JCO.ParameterList im = functionSpcGetPricingDocumentInfo.getImportParameterList();
            JCO.ParameterList ex = functionSpcGetPricingDocumentInfo.getExportParameterList();
            JCO.ParameterList tbl = functionSpcGetPricingDocumentInfo.getTableParameterList();
    
            // process table imSpcgetpricingdocumentinfo_itItemId
            // optional, TABLE, List of Item Numbers
            JCO.Table imSpcgetpricingdocumentinfo_itItemId = im.getTable(SpcGetPricingDocumentInfo.IT_ITEM_ID);
            imSpcgetpricingdocumentinfo_itItemId.appendRow();
            imSpcgetpricingdocumentinfo_itItemId.setValue(_itemId, "");      // BYTE, Item Number
            im.setValue(_document.getId(), SpcGetPricingDocumentInfo.IV_DOCUMENT_ID);        // BYTE, Pricing Document GUID
            
            category.logT(Severity.PATH, location, "executing RFC SPC_GET_PRICING_DOCUMENT_INFO");
            ((RFCDefaultClient)_client).execute(functionSpcGetPricingDocumentInfo); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_GET_PRICING_DOCUMENT_INFO");
    
            // no conversion is done because in the TCP implementation FORMAT_VALUE="N" is sent, i.e. the returned values are not formatted
            // so we do the same in RFC: we use the unformatted values 
    
            // process table exSpcgetpricingdocumentinfo_etItemRet
            // TABLE, Return Values for Pricing at Item Level
            String key = "";
            JCO.Table exSpcgetpricingdocumentinfo_etItemRet = ex.getTable(SpcGetPricingDocumentInfo.ET_ITEM_RET);
            for (int exSpcgetpricingdocumentinfo_etItemRetCounter=0; exSpcgetpricingdocumentinfo_etItemRetCounter<exSpcgetpricingdocumentinfo_etItemRet.getNumRows(); exSpcgetpricingdocumentinfo_etItemRetCounter++) {
                exSpcgetpricingdocumentinfo_etItemRet.setRow(exSpcgetpricingdocumentinfo_etItemRetCounter);
    
                String documentCurrencyUnit = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.DOCUMENT_CURRENCY_UNIT);
                key = SpcGetPricingDocumentInfo.DOCUMENT_CURRENCY_UNIT + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, documentCurrencyUnit);
    
                String netValue = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.NET_VALUE);
                key = SpcGetPricingDocumentInfo.NET_VALUE + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, netValue);
    
                String netPrice = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.NET_PRICE);
                key = SpcGetPricingDocumentInfo.NET_PRICE + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, netPrice);
               
                String netPricePrUnitValue = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.NET_PRICE_PR_UNIT_VALUE);
                key = SpcGetPricingDocumentInfo.NET_PRICE_PR_UNIT_VALUE + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, netPricePrUnitValue);
    
                String netPricePrUnitName = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.NET_PRICE_PR_UNIT_NAME);
                key = SpcGetPricingDocumentInfo.NET_PRICE_PR_UNIT_NAME + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, netPricePrUnitName);
                
                String taxValue = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.TAX_VALUE);
                key = SpcGetPricingDocumentInfo.TAX_VALUE + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, taxValue);
                
                String grossValue = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.GROSS_VALUE);
                key = SpcGetPricingDocumentInfo.GROSS_VALUE + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, grossValue);
                
                String grossValueFromSubtotal = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.GROSS_VALUE_FROM_SUBTOTAL);
                key = SpcGetPricingDocumentInfo.GROSS_VALUE_FROM_SUBTOTAL + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, grossValueFromSubtotal);
    
                String netValueFreightless = exSpcgetpricingdocumentinfo_etItemRet.getString(SpcGetPricingDocumentInfo.NET_VALUE_FREIGHTLESS);
                key = SpcGetPricingDocumentInfo.NET_VALUE_FREIGHTLESS + "[" + (exSpcgetpricingdocumentinfo_etItemRetCounter+1) +"]";
                data.put(key, netValueFreightless);
                // process table exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotal
                JCO.Table exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotal = exSpcgetpricingdocumentinfo_etItemRet.getTable(SpcGetPricingDocumentInfo.SUBTOTAL);
                for (int exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotalCounter=0; exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotalCounter<exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotal.getNumRows(); exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotalCounter++) {
                    exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotal.setRow(exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotalCounter);
                    
                    int counter = (exSpcgetpricingdocumentinfo_etItemRetCounter+1) + exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotalCounter;
                    // flag = exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotal.getString(SpcGetPricingDocumentInfo.FLAG);
                    String value = exSpcgetpricingdocumentinfo_etItemRetSpcgetpricingdocumentinfo_subtotal.getString(SpcGetPricingDocumentInfo.VALUE);
                    key = SpcGetPricingDocumentInfo.SUBTOTAL + "[" + (counter) +"]";
                    data.put(key, value);
                }
            }
                    
        }catch(ClientException ce){
            throw new IPCException(ce);
        }
   	    return data;
	}

	/**
     * Changes an Item. 
     * This method can deal with changes in configuration as well as with changes in pricing data.
     * @param imConfig ImportParameterList of SPC_CHANGE_ITEM_CONFIG
     * @param functionSpcChangeItemConfig object of function SPC_CHANGE_ITEM_CONFIG
     * @param imPricing ImportParameterList of SPC_CHANGE_ITEMS
     * @param functionSpcChangeItems object of function SPC_CHANGE_ITEMS
     * @throws IPCException
     */
	protected void _changeItem(JCO.ParameterList imConfig, JCO.Function functionSpcChangeItemConfig, JCO.ParameterList imPricing, JCO.Function functionSpcChangeItems)
		throws IPCException {
			// config influences price => call ChangeItemConfig before ChangeItem
			if ((imConfig != null) && (functionSpcChangeItemConfig != null)) {
                try {
                    JCO.ParameterList ex = functionSpcChangeItemConfig.getExportParameterList();
                    JCO.ParameterList tbl = functionSpcChangeItemConfig.getTableParameterList();
                    imConfig.setValue(_document.getId(), SpcChangeItemConfig.DOCUMENT_ID);       // BYTE, Pricing Document GUID
                    imConfig.setValue(_itemId, SpcChangeItemConfig.ITEM_ID);       // BYTE, Item Number

                    category.logT(Severity.PATH, location, "executing RFC SPC_CHANGE_ITEM_CONFIG");
                    ((RFCDefaultClient)_client).execute(functionSpcChangeItemConfig);
                    category.logT(Severity.PATH, location, "done with RFC SPC_CHANGE_ITEM_CONFIG");

                    setCacheDirty();
                }catch(ClientException e){
                    throw new IPCException(e);
                }                
			}

			if ((imPricing != null) && (functionSpcChangeItems != null)) {
                try {
                    JCO.ParameterList ex = functionSpcChangeItems.getExportParameterList();
                    JCO.ParameterList tbl = functionSpcChangeItems.getTableParameterList();

                    JCO.Table imSpcchangeitems_itItem = imPricing.getTable(SpcChangeItems.IT_ITEM);
                    // there is only one item to be changed (this)
                    if (imSpcchangeitems_itItem.getNumRows() == 0) {
                        imSpcchangeitems_itItem.appendRow();
                    }
                    imSpcchangeitems_itItem.setValue(_itemId, SpcChangeItems.ITEM_ID);       // BYTE, Item Number
                    imPricing.setValue(_document.getId(), SpcChangeItems.IV_DOCUMENT_ID);       // BYTE, Pricing Document GUID
                    
                    // For correct tax calculation we have to pass also the external id.
                    // Otherwise SPE would overwrite the external id and then passes a default extId to 
                    // TTE, which leads to wrong taxing.
                    imSpcchangeitems_itItem.setValue(_props.getExternalId(), SpcChangeItems.ITEM_ID_EXT);        // NUM, External Item Number
                    
                    boolean decoupleSubitems = _props.isDecoupleSubitems();
                    String rfcDecoupleSubitems = decoupleSubitems ? RfcConstants.XFLAG : RfcConstants.SPACEFLAG;
                    imSpcchangeitems_itItem.setValue(rfcDecoupleSubitems, SpcChangeItems.DECOUPLE_SUBITEMS);

                    category.logT(Severity.PATH, location, "executing RFC SPC_CHANGE_ITEMS");
                    ((RFCDefaultClient)_client).execute(functionSpcChangeItems); // does not define ABAP Exceptions
                    category.logT(Severity.PATH, location, "done with RFC SPC_CHANGE_ITEMS");

                    setCacheDirty();
                }catch(ClientException e){
                    throw new IPCException(e);
                }
			}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#_resetInternalConfiguration()
	 */
	protected void _resetInternalConfiguration() {
		_config = new ConfigurationValue(this.getDocument().getSession().getIPCClient(),
										 com.sap.spc.remote.shared.ConfigIdManager.
										 getConfigId(getDocument().getId(), getItemId()));
	  //20010808-kha: please note: the context data are not changed by the server, therefore
	  //there is no need to ever retrieve/change them.
	}

	/**
	 * Sets Cache as dirty.
	 */
	public void setCacheDirty() {
        location.entering("setCacheDirty()");
		//cacheIsDirty = true;
		//Caching is done by document, pass this information.
		_document.setCacheDirty();
        location.exiting();
	}
				
//	/* (non-Javadoc)
//     * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#syncWithServer()
//     */
//    public void syncWithServer() throws IPCException {
//    	//This item cache might be dirty but doc cache may not be dirty. In this case make the doc cache as dirty so that doc will be in sycn with server in the sync call. 
//        if (isCacheDirty() && !_document.isCacheDirty())
//        	_document.setCacheDirty();
//        	
//        _document.syncWithServer();
//	}
	
	/* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getPricingAnalysisData()
     */
    public String getPricingAnalysisData() throws IPCException {
        location.entering("getPricingAnalysisData()");
		JCO.Function functionSpcGetPricingTrace;
		try {
			functionSpcGetPricingTrace =((RFCDefaultClient) _client).getFunction(IFunctionGroup.SPC_GET_PRICING_TRACE);
			JCO.ParameterList im = functionSpcGetPricingTrace.getImportParameterList();
			JCO.ParameterList ex = functionSpcGetPricingTrace.getExportParameterList();
			JCO.ParameterList tbl = functionSpcGetPricingTrace.getTableParameterList();

			im.setValue(_document.getId(), SpcGetPricingTrace.IV_DOCUMENT_ID);		// BYTE, Pricing Document GUID
		//	im.setValue(ivFillTraceTable, SpcGetPricingTrace.IV_FILL_TRACE_TABLE);		// CHAR, Checkbox
		//	im.setValue(ivFillXmlTable, SpcGetPricingTrace.IV_FILL_XML_TABLE);		// CHAR, Checkbox
			im.setValue(_itemId, SpcGetPricingTrace.IV_ITEM_ID);		// BYTE, Item Number
	
            category.logT(Severity.PATH, location, "executing RFC SPC_GET_PRICING_TRACE");
			((RFCDefaultClient)_client).execute(functionSpcGetPricingTrace); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC SPC_GET_PRICING_TRACE");
	
            String pricingAnalysisData = ex.getString(SpcGetPricingTrace.EV_XML);
            if (location.beDebug()){
                location.debugT("return pricingAnalysisData" + pricingAnalysisData);
            }
            location.exiting();
			return pricingAnalysisData;
		} catch (ClientException e) {
			throw new IPCException(e);
    	}
	}

    /**
     * Consolidated location were the function object of SPC_CHANGE_ITEM_CONFIG is created. 
     * This is needed to get always the same importParameters object in different methods. 
     * @return JCO.Function object of SPC_CHANGE_ITEM_CONFIG 
     */    
    protected JCO.Function getChangeItemConfigFunction(){
        JCO.Function functionSpcChangeItemConfig = null;
        try {
            functionSpcChangeItemConfig = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_CHANGE_ITEM_CONFIG);
        }
        catch (ClientException c){
            throw new IPCException(c);
        }
         return functionSpcChangeItemConfig;
    }

    /**
     * Consolidated location were the function object of SPC_CHANGE_ITEMS is created. 
     * This needed to get always the same importParameters object in different methods. 
     * @return JCO.Function object of SPC_CHANGE_ITEMS 
     */
    protected JCO.Function getChangeItemsFunction(){
        JCO.Function functionSpcChangeItems = null;
        try {
            functionSpcChangeItems = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_CHANGE_ITEMS);
        }
        catch (ClientException c){
            throw new IPCException(c);
        }
        return functionSpcChangeItems;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#setSubItemsAllowed(boolean)
     */
    public void setSubItemsAllowed(boolean flag) throws IPCException {
        location.entering("setSubItemsAllowed(boolean flag)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean flag " + flag);
        }
        JCO.Function function = getChangeItemConfigFunction();
        JCO.ParameterList im = function.getImportParameterList();        
        im.setValue(flag ? SpcChangeItemConfig.TRUE : SpcChangeItemConfig.FALSE, SpcChangeItemConfig.SUB_ITEMS_ALLOWED);        // optional, CHAR, Subitems allowed(T=True,F=False,Blank=True)
        _changeItem(im, function, null, null);
        _resetInternalConfiguration();
        _children = null;
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#setReturn(boolean)
     */
    public void setReturn(boolean flag) throws IPCException {
        location.entering("setReturn(boolean flag)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean flag " + flag);
        }
        JCO.Function function = getChangeItemsFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
        if (imSpcchangeitems_itItem.getNumRows() == 0) {
            imSpcchangeitems_itItem.appendRow();
        }
        imSpcchangeitems_itItem.setValue(flag ? SpcChangeItems.XFLAG : "", SpcChangeItems.RETURN_FLAG);       // CHAR, Position ist Retoure (Konditionswerte invertieren)            
        imSpcchangeitems_itItem.setValue(SpcChangeItems.XFLAG, SpcChangeItems.XRETURN_FLAG); // XFlag has to be set as soon as the appropriate value is set (needed to differ between "value has not been passed" and "value is default-value" on server-side        
        JCO.ParameterList[] imports = {im};
        _changeItem(null, null, im, function);  
        location.exiting();      
    }

	public static String getProductGuid(String productId, String productType, String productLogSys, IClient client){
		location.entering("getProductGuid(String productId, String productType, String productLogSys, IClient client)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String productId " + productId);
            location.debugT("String productType " + productType);
            location.debugT("String productLogSys " + productLogSys);
            if (client!=null){
                location.debugT("IClient client " + client.toString());
            }
            else location.debugT("IClient client null");
        }
        if (productId == null ) return null;
		if (client == null || !(client instanceof RFCDefaultClient)) return null;
		String productGuid = null;

        JCO.Function functionSpcGetProductGuids;
    	try {
            functionSpcGetProductGuids = ((RFCDefaultClient) client).getFunction(IFunctionGroup.SPC_GET_PRODUCT_GUIDS); 
		    JCO.ParameterList im = functionSpcGetProductGuids.getImportParameterList();
		    JCO.ParameterList ex = functionSpcGetProductGuids.getExportParameterList();
		    JCO.ParameterList tbl = functionSpcGetProductGuids.getTableParameterList();

	        // process table imSpcgetproductguids_itProductData
	        // TABLE, internal product data
		    JCO.Table imSpcgetproductguids_itProductData = im.getTable(SpcGetProductGuids.IT_PRODUCT_DATA);
            imSpcgetproductguids_itProductData.appendRow();
			imSpcgetproductguids_itProductData.setValue(productId, SpcGetProductGuids.PRODUCT_ID);		// CHAR, Product ID
			
			if (productType != null && !productType.equals("")){
				imSpcgetproductguids_itProductData.setValue(productType, SpcGetProductGuids.PRODUCT_TYPE);
			}else{
				category.logT(Severity.WARNING, location, SpcGetProductGuids.PRODUCT_TYPE+" is null or empty string. Ignored this while converting from " + SpcGetProductGuids.IT_PRODUCT_DATA + " to " + SpcGetProductGuids.PRODUCT_GUID);
			}
			
			if (productLogSys != null && !productLogSys.equals("")){
				imSpcgetproductguids_itProductData.setValue(productLogSys, SpcGetProductGuids.LOGICAL_SYSTEM);
			}else{
				category.logT(Severity.WARNING, location, SpcGetProductGuids.LOGICAL_SYSTEM+" is null or empty string. Ignored this while converting from " + SpcGetProductGuids.IT_PRODUCT_DATA + " to " + SpcGetProductGuids.PRODUCT_GUID);
			}
	
    		category.logT(Severity.PATH, location, "executing " + IFunctionGroup.SPC_GET_PRODUCT_GUIDS);
		    ((RFCDefaultClient)client).execute(functionSpcGetProductGuids); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with " + IFunctionGroup.SPC_GET_PRODUCT_GUIDS);

	        // process table exSpcgetproductguids_etProductData
	        // TABLE, product data
            JCO.Table exSpcgetproductguids_etProductData = ex.getTable(SpcGetProductGuids.ET_PRODUCT_DATA);
            String localProdId = null;
            for (int i=0; i<exSpcgetproductguids_etProductData.getNumRows(); i++) {
                exSpcgetproductguids_etProductData.setRow(i);
                localProdId = exSpcgetproductguids_etProductData.getString(SpcGetProductGuids.PRODUCT_ID);
                if (localProdId != null && localProdId.equals(productId)){
				    productGuid = exSpcgetproductguids_etProductData.getString(SpcGetProductGuids.PRODUCT_GUID);
				    break;
                }
		    }
        } catch (ClientException e) {
		  throw new IPCException(e);
        }
        if (location.beDebug()){
            location.debugT("return productGuid" + productGuid);
        }
        location.exiting();
		return productGuid;
	}
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#setItemAttributes(java.util.Map)
     */
    public void setItemAttributes(Map attributes) throws IPCException {
        location.entering("setItemAttributes(Map attributes)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (attributes!=null){
                location.debugT("Map attributes " + attributes.toString());
            }
            else location.debugT("Map attributes null");
        }
        _props.setItemAttributes(attributes);
        JCO.Function function = getChangeItemsFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
        if (imSpcchangeitems_itItem.getNumRows() == 0) {
            imSpcchangeitems_itItem.appendRow();
        }
        JCO.Table imSpcchangeitems_itItemSpcchangeitems_attributes = imSpcchangeitems_itItem.getTable(SpcCreateItems.ATTRIBUTES);
        // processing itemAttributes            
        for (Iterator iter=attributes.keySet().iterator(); iter.hasNext();) {
            imSpcchangeitems_itItemSpcchangeitems_attributes.appendRow();
            String key = (String)iter.next();
            String val = (String)attributes.get(key);
            imSpcchangeitems_itItemSpcchangeitems_attributes.setValue(key, SpcCreateItems.FIELDNAME);     // CHAR, Field name
            JCO.Table imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values = imSpcchangeitems_itItemSpcchangeitems_attributes.getTable(SpcCreateItems.VALUES);
            imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values.appendRow();
            imSpcchangeitems_itItemSpcchangeitems_attributesSpcchangeitems_values.setValue(val, "");        // CHAR, Value of Field in VAKEY/VADAT
        } 
        _changeItem(null, null, im, function);
        location.exiting();
    }
    
	public IPCItemProperties[] getPropertiesFromServer(String[] itemIds) {
     
	   location.entering("getPropertiesFromServer(String[] itemIds)");
       if (location.beDebug()){
           location.debugT("Parameters:");
           location.debugT("String[] itemIds");
           if (itemIds != null) {
               for (int i = 0; i<itemIds.length; i++){
                   location.debugT("  itemIds["+i+"] "+itemIds[i]);
               }
           }
        }		

//		implementation
//		get access to function
	 JCO.Function functionSpcGetDocumentAttrInfo = null;
	try {
		functionSpcGetDocumentAttrInfo =
		((RFCDefaultClient)_client).getFunction(
				IFunctionGroup.SPC_GET_DOCUMENT_ATTR_INFO);
	} catch (ClientException e) {
		throw new IPCException(e);
	}
	 JCO.ParameterList im = functionSpcGetDocumentAttrInfo.getImportParameterList();
	 JCO.ParameterList ex = functionSpcGetDocumentAttrInfo.getExportParameterList();
	 JCO.ParameterList tbl = functionSpcGetDocumentAttrInfo.getTableParameterList();
//		fill import parameters from internal members

//		process table imSpcgetdocumentattrinfo.itItemId
//		optional, TABLE, List of Item Numbers
	 JCO.Table imSpcgetdocumentattrinfo_itItemId = im.getTable(SpcGetDocumentAttrInfo.IT_ITEM_ID);
	 int i = 0;
	 while (i < itemIds.length) {
		 String ivItemId = itemIds[i];
		 imSpcgetdocumentattrinfo_itItemId.appendRow();
		 imSpcgetdocumentattrinfo_itItemId.setValue(ivItemId, "");
		 i++;
		 		// BYTE, Item Number
	 }
	 im.setValue(getDocument().getId(), SpcGetDocumentAttrInfo.IV_DOCUMENT_ID);		// BYTE, Pricing Document GUID


	 try {
		((RFCDefaultClient)_client).execute(functionSpcGetDocumentAttrInfo);
	} catch (ClientException e1) {
		throw new IPCException(e1);
	} // does not define ABAP Exceptions
//		fill internal members from export parameters

	IPCItemProperties[] itemProperties = new IPCItemProperties[itemIds.length];


//		process table exSpcgetdocumentattrinfo_etItem
//		TABLE, Pricing Item
	 JCO.Table exSpcgetdocumentattrinfo_etItem = ex.getTable(SpcGetDocumentAttrInfo.ET_ITEM);
	 for (int exSpcgetdocumentattrinfo_etItemCounter=0; exSpcgetdocumentattrinfo_etItemCounter<exSpcgetdocumentattrinfo_etItem.getNumRows(); exSpcgetdocumentattrinfo_etItemCounter++) {
		IPCItemProperties currentItemProperties = factory.newIPCItemProperties();
		itemProperties[exSpcgetdocumentattrinfo_etItemCounter] = currentItemProperties;
		 exSpcgetdocumentattrinfo_etItem.setRow(exSpcgetdocumentattrinfo_etItemCounter);
		 String itemId = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.ITEM_ID);
		 BigDecimal itemIdExt = exSpcgetdocumentattrinfo_etItem.getBigDecimal(SpcGetDocumentAttrInfo.ITEM_ID_EXT);
		 String highLevelItemId = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.HIGH_LEVEL_ITEM_ID);
		 String productId = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.PRODUCT_ID);
		currentItemProperties.setProductId(productId);
		 String productErp = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.PRODUCT_ERP);
		 String decoupleSubitems = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.DECOUPLE_SUBITEMS);
		currentItemProperties.setDecoupleSubitems(decoupleSubitems.equals("X")? true : false);
		 Integer multiplicity = (Integer)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.MULTIPLICITY);
		 String pricingRelevant = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.PRICING_RELEVANT);
		currentItemProperties.setPricingRelevant(new Boolean(pricingRelevant.equals("X")? true : false));
		 String returnFlag = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.RETURN_FLAG);
		//currentItemProperties.setReturnFlag
		 String statistical = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.STATISTICAL);
		currentItemProperties.setStatistical(statistical.equals("X")? true : false);
		 String unchangeable = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.UNCHANGEABLE);
		//currentItemProperties.setUnchangeable
		 String fixGroup = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.FIX_GROUP);
		//currentItemProperties.setFixGroup
//		 Date exchRateDate = exSpcgetdocumentattrinfo_etItem.getDate(SpcGetDocumentAttrInfo.EXCH_RATE_DATE);
		String exchRateDate = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.EXCH_RATE_DATE);
		currentItemProperties.setExchangeRateDate(exchRateDate);
//		FormattedDate formattedDate = new FormattedDate();
//		formattedDate.setYear(exchRateDate.getYear());
//		formattedDate.setMonth(exchRateDate.getMonth());
//		formattedDate.setDay(exchRateDate.getDay());
//		currentItemProperties.setExchangeRateDate(formattedDate.getFormattedDate(FormattedDate.Format.YYYYMMDD));
		 String exchRateType = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.EXCH_RATE_TYPE);
		currentItemProperties.setExchangeRateType(exchRateType);
		 String exchRate = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.EXCH_RATE);
		//currentItemProperties.setExchangeRate
		 String itemQuantity = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.ITEM_QUANTITY);
		 String itemQuantityUnit = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.ITEM_QUANTITY_UNIT);
		 DimensionalValue baseQuantity = new com.sap.spc.remote.client.object.imp.DimensionalValue();
		 baseQuantity.setUnitAndValue(itemQuantity, itemQuantityUnit);
		 currentItemProperties.setBaseQuantity(baseQuantity);
		 Integer numerator = (Integer)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.NUMERATOR);
		 Integer denominator = (Integer)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.DENOMINATOR);
		 Integer exponent = (Integer)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.EXPONENT);
		 String grossWeight = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.GROSS_WEIGHT);
		 String netWeight = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.NET_WEIGHT);
		 String weightUnit = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.WEIGHT_UNIT);
		 String volum = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.VOLUM);
		 String volumeUnit = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.VOLUME_UNIT);
		 String points = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.POINTS);
		 String pointsUnit = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.POINTS_UNIT);
		 Integer daysPerMonth = (Integer)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.DAYS_PER_MONTH);
		 Integer daysPerYear = (Integer)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.DAYS_PER_YEAR);
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidays
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidays = exSpcgetdocumentattrinfo_etItem.getTable(SpcGetDocumentAttrInfo.HOLIDAYS);
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidaysCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidaysCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidays.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidaysCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidays.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidaysCounter);
			 Date dateEmptyFieldHack = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_holidays.getDate(SpcGetDocumentAttrInfo.EMPTY_FIELD_HACK);
		 }
		 Date dateFromB = exSpcgetdocumentattrinfo_etItem.getDate(SpcGetDocumentAttrInfo.DATE_FROM_B);
		 Date dateToB = exSpcgetdocumentattrinfo_etItem.getDate(SpcGetDocumentAttrInfo.DATE_TO_B);
		 Double daysB = (Double)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.DAYS_B);
		 Double weeksB = (Double)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.WEEKS_B);
		 Double monthsB = (Double)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.MONTHS_B);
		 Double yearsB = (Double)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.YEARS_B);
		 Date dateFromS = exSpcgetdocumentattrinfo_etItem.getDate(SpcGetDocumentAttrInfo.DATE_FROM_S);
		 Date dateToS = exSpcgetdocumentattrinfo_etItem.getDate(SpcGetDocumentAttrInfo.DATE_TO_S);
		 Double daysS = (Double)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.DAYS_S);
		 Double weeksS = (Double)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.WEEKS_S);
		 Double monthsS = (Double)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.MONTHS_S);
		 Double yearsS = (Double)exSpcgetdocumentattrinfo_etItem.getValue(SpcGetDocumentAttrInfo.YEARS_S);
		 String timestampS = exSpcgetdocumentattrinfo_etItem.getString(SpcGetDocumentAttrInfo.TIMESTAMP_S);
    
//		process structure exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData
		 JCO.Structure exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData = exSpcgetdocumentattrinfo_etItem.getStructure(SpcGetDocumentAttrInfo.CONFIG_DATA);
		 String kbName = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getString(SpcGetDocumentAttrInfo.KB_NAME);
		 String kbDate = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getString(SpcGetDocumentAttrInfo.KB_DATE);
		 String kbLogsys = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getString(SpcGetDocumentAttrInfo.KB_LOGSYS);
		 String kbVersion = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getString(SpcGetDocumentAttrInfo.KB_VERSION);
		 String kbProfile = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getString(SpcGetDocumentAttrInfo.KB_PROFILE);
		 Integer kbId = (Integer)exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getValue(SpcGetDocumentAttrInfo.KB_ID);
    
//		process structure exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfig
		 JCO.Structure exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfig = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getStructure(SpcGetDocumentAttrInfo.EXT_CONFIG);
    
//		process structure exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header
		 JCO.Structure exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfig.getStructure(SpcGetDocumentAttrInfo.HEADER);
		 String rootId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.ROOT_ID);
		 String sce = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.SCE);
		 String name = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.NAME);
		 String version = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.VERSION);
		 String logsys = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.LOGSYS);
		 String profile = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.PROFILE);
		 String language = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.LANGUAGE);
		 String complete = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.COMPLETE);
		 String consistent = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.CONSISTENT);
		 String info = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getString(SpcGetDocumentAttrInfo.INFO);
		 Integer build = (Integer)exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_header.getValue(SpcGetDocumentAttrInfo.BUILD);
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfig.getTable(SpcGetDocumentAttrInfo.PART_OF);
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOfCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOfCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOfCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOfCounter);
			 String instId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getString(SpcGetDocumentAttrInfo.INST_ID);
			 String parentId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getString(SpcGetDocumentAttrInfo.PARENT_ID);
			 String partOfNo = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getString(SpcGetDocumentAttrInfo.PART_OF_NO);
			 String objType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getString(SpcGetDocumentAttrInfo.OBJ_TYPE);
			 String classType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getString(SpcGetDocumentAttrInfo.CLASS_TYPE);
			 String objKey = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getString(SpcGetDocumentAttrInfo.OBJ_KEY);
			 String author = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getString(SpcGetDocumentAttrInfo.AUTHOR);
			 String salesRelevant = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_partOf.getString(SpcGetDocumentAttrInfo.SALES_RELEVANT);
		 }
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfig.getTable(SpcGetDocumentAttrInfo.INSTANCES);
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instancesCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instancesCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instancesCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instancesCounter);
			 String instId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.INST_ID);
			 String objType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.OBJ_TYPE);
			 String classType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.CLASS_TYPE);
			 String objKey = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.OBJ_KEY);
			 String objTxt = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.OBJ_TXT);
			 String quantity = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.QUANTITY);
			 String quantityUnit = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.QUANTITY_UNIT);
			 String author = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.AUTHOR);
			 String instComplete = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.COMPLETE);
			 String instConsistent = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_instances.getString(SpcGetDocumentAttrInfo.CONSISTENT);
		 }
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfig.getTable(SpcGetDocumentAttrInfo.VALUES);
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_valuesCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_valuesCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_valuesCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_valuesCounter);
			 String valuesInstId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.getString(SpcGetDocumentAttrInfo.INST_ID);
			 String charc = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.getString(SpcGetDocumentAttrInfo.CHARC);
			 String charcTxt = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.getString(SpcGetDocumentAttrInfo.CHARC_TXT);
			 String value = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.getString(SpcGetDocumentAttrInfo.VALUE);
			 String valueUnit = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.getString(SpcGetDocumentAttrInfo.VALUE_UNIT);
			 String valueTxt = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.getString(SpcGetDocumentAttrInfo.VALUE_TXT);
			 String author = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_values.getString(SpcGetDocumentAttrInfo.AUTHOR);
		 }
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditions
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditions = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfig.getTable(SpcGetDocumentAttrInfo.VARIANT_CONDITIONS);
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditionsCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditionsCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditions.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditionsCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditions.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditionsCounter);
			 String varcondInstId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditions.getString(SpcGetDocumentAttrInfo.INST_ID);
			 String vkey = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditions.getString(SpcGetDocumentAttrInfo.VKEY);
			 String factor = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_extConfigSpcgetdocumentattrinfo_variantConditions.getString(SpcGetDocumentAttrInfo.FACTOR);
		 }
		 String configProductId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getString(SpcGetDocumentAttrInfo.PRODUCT_ID);
		 String configProductType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getString(SpcGetDocumentAttrInfo.PRODUCT_TYPE);
		 String configProductLogsys = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getString(SpcGetDocumentAttrInfo.PRODUCT_LOGSYS);
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_context
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_context = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configData.getTable(SpcGetDocumentAttrInfo.CONTEXT);
		 HashMap itemContext = new HashMap(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_context.getNumRows());
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_contextCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_contextCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_context.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_contextCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_context.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_contextCounter);
			 String contextName = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_context.getString(SpcGetDocumentAttrInfo.NAME);
			 String contextValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_configDataSpcgetdocumentattrinfo_context.getString(SpcGetDocumentAttrInfo.VALUE);
			 itemContext.put(contextName, contextValue);
		 }
		currentItemProperties.setContext(itemContext);
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributes
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributes = exSpcgetdocumentattrinfo_etItem.getTable(SpcGetDocumentAttrInfo.ATTRIBUTES);
		 HashMap itemAttributes = new HashMap(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributes.getNumRows());
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributes.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributes.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesCounter);
			 String attrName = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributes.getString(SpcGetDocumentAttrInfo.FIELDNAME);
        
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_values
			 // TABLE
			 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_values = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributes.getTable(SpcGetDocumentAttrInfo.VALUES);
			 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_valuesCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_valuesCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_values.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_valuesCounter++) {
				 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_values.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_valuesCounter);
				 String attributeValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_attributesSpcgetdocumentattrinfo_values.getString(SpcGetDocumentAttrInfo.EMPTY_FIELD_HACK);
				 itemAttributes.put(attrName, attributeValue);
			 }
		 }
		currentItemProperties.setItemAttributes(itemAttributes);
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestamps
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestamps = exSpcgetdocumentattrinfo_etItem.getTable(SpcGetDocumentAttrInfo.TIMESTAMPS);
		 HashMap timestamps = new HashMap(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestamps.getNumRows());
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestampsCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestampsCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestamps.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestampsCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestamps.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestampsCounter);
			 String timestampName = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestamps.getString(SpcGetDocumentAttrInfo.FIELDNAME);
			 String timestampValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_timestamps.getString(SpcGetDocumentAttrInfo.TIMESTAMP);
			 timestamps.put(timestampName, timestampValue);
			 if (timestampName != null && timestampName.equals(SpcCreateItems.PRICE_DATE)) {
			 	//the pricing date is now a timestamp. however the properties still have a member date
			 	currentItemProperties.setDate(timestampValue);
			 }
		 }
		currentItemProperties.setConditionTimestamps(timestamps);
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource = exSpcgetdocumentattrinfo_etItem.getTable(SpcGetDocumentAttrInfo.EXT_DATASOURCE);
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasourceCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasourceCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasourceCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasourceCounter);
			 String condType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.COND_TYPE);
			 String calcType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.CALC_TYPE);
			 String condRate = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.COND_RATE);
			 String condCurrency = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.COND_CURRENCY);
			 String condUnitValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.COND_UNIT_VALUE);
			 String condUnit = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.COND_UNIT);
			 String condBase = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.COND_BASE);
			 String condBaseUnit = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.COND_BASE_UNIT);
			 String xcondBase = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.XCOND_BASE);
			 String removeExtCond = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_extDatasource.getString(SpcGetDocumentAttrInfo.REMOVE_EXT_COND);
		 }
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantCondition
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantCondition = exSpcgetdocumentattrinfo_etItem.getTable(SpcGetDocumentAttrInfo.VARIANT_CONDITION);
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantConditionCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantConditionCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantCondition.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantConditionCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantCondition.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantConditionCounter);
			 String key = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantCondition.getString(SpcGetDocumentAttrInfo.KEY);
			 Double doubleFactor = (Double)exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantCondition.getValue(SpcGetDocumentAttrInfo.FACTOR);
			 String description = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_variantCondition.getString(SpcGetDocumentAttrInfo.DESCRIPTION);
		 }
    
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy
		 // TABLE
		 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy = exSpcgetdocumentattrinfo_etItem.getTable(SpcGetDocumentAttrInfo.ITEM_COPY);
		 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopyCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopyCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopyCounter++) {
			 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopyCounter);
			 String importConditions = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.IMPORT_CONDITIONS);
			 String sourceDocumentId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.SOURCE_DOCUMENT_ID);
			 String sourceItemId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.SOURCE_ITEM_ID);
			 String pricingType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.PRICING_TYPE);
			 String copyType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.COPY_TYPE);
        
//		process table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions
			 // TABLE
			 JCO.Table exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getTable(SpcGetDocumentAttrInfo.CONDITIONS);
			 for (int exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditionsCounter=0; exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditionsCounter<exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getNumRows(); exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditionsCounter++) {
				 exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.setRow(exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditionsCounter);
				 String copyDocumentId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.DOCUMENT_ID);
				 itemId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.ITEM_ID);
				 BigDecimal stepNumber = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getBigDecimal(SpcGetDocumentAttrInfo.STEP_NUMBER);
				 BigDecimal condCounter = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getBigDecimal(SpcGetDocumentAttrInfo.COND_COUNTER);
				 String application = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.APPLICATION);
				 String usage = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.USAGE);
				 String condType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_TYPE);
				 String condTypeDescr = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_TYPE_DESCR);
				 String purpose = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.PURPOSE);
				 String datasource = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.DATASOURCE);
				 String condRate = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_RATE);
				 String condCurrency = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_CURRENCY);
				 String condUnitValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_UNIT_VALUE);
				 String condUnit = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_UNIT);
				 String condValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_VALUE);
				 exchRate = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.EXCH_RATE);
				 numerator = (Integer)exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getValue(SpcGetDocumentAttrInfo.NUMERATOR);
				 denominator = (Integer)exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getValue(SpcGetDocumentAttrInfo.DENOMINATOR);
				 exponent = (Integer)exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getValue(SpcGetDocumentAttrInfo.EXPONENT);
				 String manuallyChanged = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.MANUALLY_CHANGED);
				 statistical = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.STATISTICAL);
				 String calcType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.CALC_TYPE);
				 String pricingDate = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.PRICING_DATE);
				 String condBase = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_BASE);
				 String condCategory = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_CATEGORY);
				 String scaleType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.SCALE_TYPE);
				 String accrualFlag = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.ACCRUAL_FLAG);
				 String invoiceList = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.INVOICE_LIST);
				 String condOrigin = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_ORIGIN);
				 String groupCondition = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.GROUP_CONDITION);
				 String condUpdate = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_UPDATE);
				 BigDecimal accessCounter = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getBigDecimal(SpcGetDocumentAttrInfo.ACCESS_COUNTER);
				 String condRecordId = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_RECORD_ID);
				 String accountKey1 = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.ACCOUNT_KEY_1);
				 String accountKey2 = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.ACCOUNT_KEY_2);
				 String roundingDiff = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.ROUNDING_DIFF);
				 String condControl = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_CONTROL);
				 String condInactive = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_INACTIVE);
				 String condClass = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_CLASS);
				 BigDecimal headerCounter = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getBigDecimal(SpcGetDocumentAttrInfo.HEADER_COUNTER);
				 Double varcondFactor = (Double)exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getValue(SpcGetDocumentAttrInfo.VARCOND_FACTOR);
				 String scaleBaseType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.SCALE_BASE_TYPE);
				 String scaleBaseValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.SCALE_BASE_VALUE);
				 String scaleUnit = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.SCALE_UNIT);
				 String scaleCurrency = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.SCALE_CURRENCY);
				 String interCompany = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.INTER_COMPANY);
				 String varcondFlag = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.VARCOND_FLAG);
				 String varcondKey = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.VARCOND_KEY);
				 String varcondDescr = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.VARCOND_DESCR);
				 String salesTaxCode = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.SALES_TAX_CODE);
				 String withholdingTaxCode = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.WITHHOLDING_TAX_CODE);
				 String structureCondition = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.STRUCTURE_CONDITION);
				 Double periodFactor = (Double)exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getValue(SpcGetDocumentAttrInfo.PERIOD_FACTOR);
				 String condTableName = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.COND_TABLE_NAME);
				 String manualEntryFlag = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.MANUAL_ENTRY_FLAG);
				 String changeOfRate = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.CHANGE_OF_RATE);
				 String changeOfUnit = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.CHANGE_OF_UNIT);
				 String changeOfValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.CHANGE_OF_VALUE);
				 String changeOfCalcType = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.CHANGE_OF_CALC_TYPE);
				 String changeOfConvFactors = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.CHANGE_OF_CONV_FACTORS);
				 String altCondCurrency = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.ALT_COND_CURRENCY);
				 String altCondBase = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.ALT_COND_BASE);
				 String altCondValue = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopySpcgetdocumentattrinfo_conditions.getString(SpcGetDocumentAttrInfo.ALT_COND_VALUE);
			 }
			 String sourceQuantity = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.SOURCE_QUANTITY);
			 String sourceQuantityUnit = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.SOURCE_QUANTITY_UNIT);
			 String netValueOrig = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.NET_VALUE_ORIG);
			 String netValueNew = exSpcgetdocumentattrinfo_etItemSpcgetdocumentattrinfo_itemCopy.getString(SpcGetDocumentAttrInfo.NET_VALUE_NEW);
		 }
	 }
     if (location.beDebug()){
         if (itemProperties != null) {
             location.debugT("return itemProperties");
             for (int j = 0; j<itemProperties.length; j++){
                 location.debugT("  itemProperties["+j+"] "+itemProperties[j].toString());
             }
         }
     }
     location.exiting();	 
	 return itemProperties;
	}


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getProductKey()
     */
    public ProductKey getProductKey() {
        location.entering("getProductKey()");
        String application = _document.getApplication();
        String prodId = "";
        String prodType = "";
        String prodLogSys = "";
        if ((application != null) && (application.equals(DefaultIPCDocument.CRM))){            
            HashMap productInfo = ((RfcDefaultIPCDocument)_document).getProductInfoForProductGuid(_props.getProductGuid());
            if (productInfo != null){
                if (productInfo.get(ComProductReadViaRfc.PRODUCT_ID) != null){
                    prodId = (String)productInfo.get(ComProductReadViaRfc.PRODUCT_ID);
                }
                if (productInfo.get(ComProductReadViaRfc.PRODUCT_TYPE) != null){
                    prodType = (String)productInfo.get(ComProductReadViaRfc.PRODUCT_TYPE);
                }
                if (productInfo.get(ComProductReadViaRfc.LOGSYS) != null){
                    prodLogSys = (String)productInfo.get(ComProductReadViaRfc.LOGSYS);
                }
            }
        }
        else { // ERP
            prodId = _props.getProductId();
        }
        ProductKey prodKey = factory.newProductKey(prodId, prodType, prodLogSys);
        if (location.beDebug()){
              location.debugT("return prodKey " + "(prodKey: "+prodKey.getProductId()+")");
        }
        location.exiting();
        return prodKey;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getProductGuid(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public String getProductGuid(String prodId, String prodType, String prodLogSys) {
        location.entering("getProductGuid(String prodId, String prodType, String prodLogSys)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String prodId " + prodId);
            location.debugT("String prodType " + prodType);
            location.debugT("String prodLogSys " + prodLogSys);
        }
        String application = _document.getApplication();
        String guid = prodId;
        if ((application != null) && (application.equals(DefaultIPCDocument.CRM))){
            guid = RfcDefaultIPCItem.getProductGuid(prodId, prodType, prodLogSys, _client);
        }
        if (location.beDebug()){
              location.debugT("return guid " + guid);
        }
        location.exiting();
        return guid;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getAttributesFromProductMaster(java.lang.String)
     */
    public Map getAttributesFromProductMaster(String guid) {
        location.entering("getAttributesFromProductMaster(String guid)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String guid " + guid);
        }
        Map attribs = new HashMap();
        // if no guid is passed we take the item's own guid
        if (guid == null){
            guid = _props.getProductGuid();
        }
        String application = _document.getApplication();
        // Dummy implementation: at the moment only the attributes containing the guid are returned.
        // We have no chance to get the attributes from the server (no function modules available).
        // Should be implemented with next release (5.1).
        if (application != null && application.equals(DefaultIPCDocument.ERP)){ // ERP
            attribs.put("PMATN", guid);  
        }
        else { // CRM
            attribs.put("PRICE_PRODUCT", guid);  
            attribs.put("PRODUCT", guid);
        }
        if (location.beDebug()){
            if (attribs!=null){
                location.debugT("return attribs " + attribs.toString());
            }
            else location.debugT("return attribs null");
        }
        location.exiting();
        return attribs;
    }
        

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#changeProduct(java.lang.String)
     */
    public boolean changeProduct(String prodId) {
        location.entering("changeProduct(String prodId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String prodId " + prodId);
        }
        boolean success = false;
        try {
            JCO.Function functionSpcChangeItemProduct = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_CHANGE_ITEM_PRODUCT);
            JCO.ParameterList im = functionSpcChangeItemProduct.getImportParameterList();
            JCO.ParameterList ex = functionSpcChangeItemProduct.getExportParameterList();
            JCO.ParameterList tbl = functionSpcChangeItemProduct.getTableParameterList();
            
            im.setValue(prodId, SpcChangeItemProduct.PRODUCT_ID);       // CHAR, Product Id
            im.setValue(this.getDocument().getId(), SpcChangeItemProduct.DOCUMENT_ID);     // BYTE, Pricing Document GUID
            im.setValue(this.getItemId(), SpcChangeItemProduct.ITEM_ID);     // BYTE, Item Number
           
            category.logT(Severity.PATH, location, "executing RFC SPC_CHANGE_ITEM_PRODUCT");
            ((RFCDefaultClient)_client).execute(functionSpcChangeItemProduct);
            category.logT(Severity.PATH, location, "done with RFC SPC_CHANGE_ITEM_PRODUCT");

            // HACK: right now, ChangeItemProduct doesn't return anything for its success, assume the best has happened :-)
            success = true;
            setCacheDirty();
        } catch (ClientException e) { 
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        if (location.beDebug()){
              location.debugT("return success " + success);
        }
        location.exiting();
        return success;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#setInitialConfiguration(com.sap.spc.remote.client.util.ext_configuration)
     */
    public void setInitialConfiguration(ext_configuration config) throws IPCException {
        location.entering("setInitialConfiguration(ext_configuration config)");
        if (location.beDebug()){
            if (config != null){
                String xml = ((c_ext_cfg_imp)config).cfg_ext_to_xml_string();
                location.debugT("Initial configuration that should be set:");
                location.debugT(xml);
            }
            else{
                location.debugT("Initial configuration is null!");
            }
        }
        try {
            JCO.Function functionSpcSetInitialConfiguration = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_SET_INITIAL_CONFIGURATION);
            JCO.ParameterList im = functionSpcSetInitialConfiguration.getImportParameterList();
            JCO.ParameterList ex = functionSpcSetInitialConfiguration.getExportParameterList();
            JCO.ParameterList tbl = functionSpcSetInitialConfiguration.getTableParameterList();
    
            im.setValue(this.getDocument().getId(), SpcSetInitialConfiguration.DOCUMENT_ID);        // optional, BYTE, Pricing Document GUID
            im.setValue(this.getItemId(), SpcSetInitialConfiguration.ITEM_ID);        // optional, BYTE, Item Number

            JCO.Structure imSpcsetinitialconfiguration_extConfig = im.getStructure(SpcSetInitialConfiguration.EXT_CONFIG);
            JCO.Structure extConfigStructure = imSpcsetinitialconfiguration_extConfig.getStructure(SpcSetInitialConfiguration.EXT_CONFIG);

            if (config != null) {
                ExternalConfigConverter.getConfig(config, extConfigStructure);
            }           
            category.logT(Severity.PATH, location, "executing RFC SPC_SET_INITIAL_CONFIGURATION");
            ((RFCDefaultClient)_client).execute(functionSpcSetInitialConfiguration);
            category.logT(Severity.PATH, location, "done with RFC SPC_SET_INITIAL_CONFIGURATION");
        }
        catch(ClientException e) {
            throw new IPCException(e);
        }
        // we have to set the hasInitialConfig flag of loading status manually
        // first get the loading status
        LoadingStatus ls = this.getConfiguration().getLoadingStatus();
        // set the flag
        ls.setLoadedFromInitialConfiguration(true);
        location.exiting();
    }
    
    
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#getDynamicReturn()
	 */
	public Map getDynamicReturn() {
		// TODO Auto-generated method stub
		return _dynamicReturn;
	}

	/**
	 * Executes RFC SPC_GET_CONFIG_ITEM_INFO and sets the result to the member _config of the item properties.
	 * @return A mapping between instance ids of the configuration and ipc items used for pricing.
	 */
	public Map readConfigItemInfoFromBackend() {    
		int valuesCounter;
		int partOfCounter;
		int variantConditionsCounter;
		int instancesCounter;
		int extIdsCounter;
		Map instanceItemMapping = new HashMap();

		try {
			JCO.Function functionSpcGetConfigItemInfo = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_CONFIG_ITEM_INFO);
			JCO.ParameterList importsSpcGetConfigItemInfo = functionSpcGetConfigItemInfo.getImportParameterList();
			JCO.ParameterList exportsSpcGetConfigItemInfo = functionSpcGetConfigItemInfo.getExportParameterList();
			JCO.ParameterList tablesSpcGetConfigItemInfo = functionSpcGetConfigItemInfo.getTableParameterList();

			importsSpcGetConfigItemInfo.setValue(_document.getId(), SpcGetConfigItemInfo.DOCUMENT_ID);		// optional, BYTE, GUID-Schlüssel des Preisfindungsbeleges (KNUMV)
			importsSpcGetConfigItemInfo.setValue(_itemId, SpcGetConfigItemInfo.ITEM_ID);		// optional, BYTE, Positionsnummer

			category.logT(Severity.PATH, location, "executing RFC SPC_GET_CONFIG_ITEM_INFO");
			((RFCDefaultClient)_client).execute(functionSpcGetConfigItemInfo);
			category.logT(Severity.PATH, location, "done with RFC SPC_GET_CONFIG_ITEM_INFO");

			JCO.Structure extConfigGuidStru = exportsSpcGetConfigItemInfo.getStructure(SpcGetConfigItemInfo.EXT_CONFIG_GUID);
			JCO.Structure extConfigStru = extConfigGuidStru.getStructure(SpcGetConfigItemInfo.EXT_CONFIG);
            
			Vector data = new Vector();
			int zi = 0;
			// initialize the arrays for header-data
			String[] sce = new String[1];     
			String[] name = new String[1];
			String[] version = new String[1];
			String[] build = new String[1];
			String[] profile = new String[1];
			String[] language = new String[1];
			String[] info = new String[1];
			String[] rootId = new String[1];
			String[] complete = new String[1];
			String[] consistent = new String[1];
			// getting the header-data
			JCO.Structure extConfigStruSpcgetconfigiteminfo_header = extConfigStru.getStructure(SpcGetConfigItemInfo.HEADER);
			sce[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.SCE);
			name[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.NAME);
			version[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.VERSION);
			build[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.BUILD);
			profile[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.PROFILE);
			language[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.LANGUAGE);
			rootId[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.ROOT_ID);
			consistent[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.CONSISTENT);
			complete[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.COMPLETE);
			info[zi] = extConfigStruSpcgetconfigiteminfo_header.getString(SpcGetConfigItemInfo.INFO);
			//Filling the data object in this order is very importent. 
			data.add(sce);
			data.add(name);
			data.add(version);
			data.add(build);
			data.add(profile);
			data.add(language);
			data.add(rootId);
			data.add(consistent);
			data.add(complete);
			data.add(info);
			_props.getConfigValue().set_cfg(data);
		 
			data = new Vector(); //Re-set to use for other
			JCO.Table extConfigStruSpcgetconfigiteminfo_partOf = extConfigStru.getTable(SpcGetConfigItemInfo.PART_OF);
			int numRowPrt = extConfigStruSpcgetconfigiteminfo_partOf.getNumRows();
			// initialize arrays for part-of data
			String[] author = new String[numRowPrt];
			String[] classType = new String[numRowPrt];
			String[] instId = new String[numRowPrt];
			String[] objType = new String[numRowPrt];
			String[] partOfNo = new String[numRowPrt];
			String[] parentId = new String[numRowPrt];
			String[] salesRelevant = new String[numRowPrt];
			String[] objKey = new String[numRowPrt];
			// getting the part-of data        
			for (int i=0; i<numRowPrt; i++) {
				extConfigStruSpcgetconfigiteminfo_partOf.setRow(i);
				author[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.AUTHOR);
				classType[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.CLASS_TYPE);
				instId[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.INST_ID);
				objKey[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.OBJ_KEY);
				objType[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.OBJ_TYPE);
				parentId[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.PARENT_ID);
				partOfNo[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.PART_OF_NO);
				salesRelevant[i] = extConfigStruSpcgetconfigiteminfo_partOf.getString(SpcGetConfigItemInfo.SALES_RELEVANT);
			}
			//Filling the data object in this order is very importent.
			data.add(author);
			data.add(classType);
			data.add(instId);
			data.add(objKey);
			data.add(objType);
			data.add(parentId);
			data.add(partOfNo);
			data.add(salesRelevant);
			_props.getConfigValue().set_prt(data);

			data = new Vector(); //Re-set to use for other
			JCO.Table extConfigStruSpcgetconfigiteminfo_instances = extConfigStru.getTable(SpcGetConfigItemInfo.INSTANCES);
			int numRowInst = extConfigStruSpcgetconfigiteminfo_instances.getNumRows();
			// initialize arrays for instance data
			String[] quantityUnit = new String[numRowInst];
			String[] quantity = new String[numRowInst];
			String[] objTxt = new String[numRowInst];  
			String[] insauthor =  new String[numRowInst];
			String[] insclassType =  new String[numRowInst];
			String[] inscomplete = new String[numRowInst];
			String[] insconsistent = new String[numRowInst];
			String[] insobjType = new String[numRowInst];
			String[] insobjKey = new String[numRowInst];  
			String[] insinstId = new String[numRowInst];
			// getting the instance data
			for (int i=0; i<numRowInst; i++) {
				extConfigStruSpcgetconfigiteminfo_instances.setRow(i);
				insauthor[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.AUTHOR);
				insclassType[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.CLASS_TYPE);
				inscomplete[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.COMPLETE);
				insconsistent[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.CONSISTENT);
				insinstId[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.INST_ID);
				insobjType[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.OBJ_TYPE);
				insobjKey[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.OBJ_KEY);
				objTxt[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.OBJ_TXT);
				quantity[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.QUANTITY);
				quantityUnit[i] = extConfigStruSpcgetconfigiteminfo_instances.getString(SpcGetConfigItemInfo.QUANTITY_UNIT);
			}
			//Filling the data object in this order is very importent.
			data.add(insauthor);
			data.add(insclassType);
			data.add(inscomplete);
			data.add(insconsistent);
			data.add(insinstId);
			data.add(insobjKey);
			data.add(objTxt);
			data.add(insobjType);
			data.add(quantity);
			data.add(quantityUnit);
			_props.getConfigValue().set_ins(data);
		  
			data = new Vector(); //Re-set to use for other
			JCO.Table extConfigStruSpcgetconfigiteminfo_values = extConfigStru.getTable(SpcGetConfigItemInfo.VALUES);
			int numRowVal = extConfigStruSpcgetconfigiteminfo_values.getNumRows();
			// initialize arrays for value data
			String[] valueTxt = new String[numRowVal];
			String[] value = new String[numRowVal];
			String[] valauthor =  new String[numRowVal];
			String[] charcTxt = new String[numRowVal];
			String[] charc = new String[numRowVal];  
			String[] valinstId = new String[numRowVal];      
			// getting the value data        
			for (int i=0; i<numRowVal; i++) {
				extConfigStruSpcgetconfigiteminfo_values.setRow(i);
				valauthor[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.AUTHOR);
				charcTxt[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.CHARC_TXT);
				charc[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.CHARC);
				valinstId[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.INST_ID);
				valueTxt[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.VALUE_TXT);
				value[i] = extConfigStruSpcgetconfigiteminfo_values.getString(SpcGetConfigItemInfo.VALUE);
			}
			//Filling the data object in this order is very importent.
			data.add(valauthor);
			data.add(charcTxt);
			data.add(charc);
			data.add(valinstId);
			data.add(valueTxt);
			data.add(value);
			_props.getConfigValue().set_val(data);
		  
			data = new Vector(); //Re-set to use for other
			JCO.Table extConfigStruSpcgetconfigiteminfo_variantConditions = extConfigStru.getTable(SpcGetConfigItemInfo.VARIANT_CONDITIONS);
			int numRowPrc = extConfigStruSpcgetconfigiteminfo_variantConditions.getNumRows();
			// initialize arrays for varCond data
			String[] vkey = new String[numRowPrc];
			String[] factor = new String[numRowPrc];
			String[] prcinstId = new String[numRowPrc];
			// getting the varCond data       
			for (int i=0; i<extConfigStruSpcgetconfigiteminfo_variantConditions.getNumRows(); i++) {
				extConfigStruSpcgetconfigiteminfo_variantConditions.setRow(i);
				factor[i] = extConfigStruSpcgetconfigiteminfo_variantConditions.getString(SpcGetConfigItemInfo.FACTOR);
				prcinstId[i] = extConfigStruSpcgetconfigiteminfo_variantConditions.getString(SpcGetConfigItemInfo.INST_ID);
				vkey[i] = extConfigStruSpcgetconfigiteminfo_variantConditions.getString(SpcGetConfigItemInfo.VKEY);
			}
			//Filling the data object in this order is very importent.
			data.add(factor);
			data.add(prcinstId);
			data.add(vkey);
			_props.getConfigValue().set_prc(data);
		  
			// mapping of instance ids to corresponding item ids not used
			JCO.Table extConfigGuidStruSpcgetconfigiteminfo_extIds = extConfigGuidStru.getTable(SpcGetConfigItemInfo.EXT_IDS);
			for (int i=0; i<extConfigGuidStruSpcgetconfigiteminfo_extIds.getNumRows(); i++) {
			    extConfigGuidStruSpcgetconfigiteminfo_extIds.setRow(i);
			    String instanceId = extConfigGuidStruSpcgetconfigiteminfo_extIds.getString(SpcGetConfigItemInfo.INST_ID);
			    String itemGuid = extConfigGuidStruSpcgetconfigiteminfo_extIds.getString(SpcGetConfigItemInfo.GUID);
			    instanceItemMapping.put(instanceId, _document.getItemFromCache(itemGuid));
			}
		}
		catch(ClientException e) {
			throw new IPCException(e);
		}
		return instanceItemMapping;		
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getInitialConfiguration()
     */    
    public ConfigurationSnapshot getInitialConfiguration() {
        ConfigurationSnapshot snap;
        location.entering("getInitialConfiguration()");

        if (initialConfiguration == null){
            try {
                JCO.Function functionSpcGetInitialConfiguration = ((RFCDefaultClient)_client).getFunction(IFunctionGroup.SPC_GET_INITIAL_CONFIGURATION);
                JCO.ParameterList im = functionSpcGetInitialConfiguration.getImportParameterList();
                JCO.ParameterList ex = functionSpcGetInitialConfiguration.getExportParameterList();
                JCO.ParameterList tbl = functionSpcGetInitialConfiguration.getTableParameterList();

                im.setValue(this.getDocument().getId(), SpcGetInitialConfiguration.DOCUMENT_ID);
                im.setValue(this.getItemId(), SpcGetInitialConfiguration.ITEM_ID);                
                
                category.logT(Severity.PATH, location, "executing RFC SPC_GET_INITIAL_CONFIGURATION");            
                ((RFCDefaultClient)_client).execute(functionSpcGetInitialConfiguration);
                category.logT(Severity.PATH, location, "done with RFC SPC_GET_INITIAL_CONFIGURATION");
            
                // process structure exSpcgetinitialconfiguration_extConfig
                JCO.Structure exSpcgetinitialconfiguration_extConfig = ex.getStructure(SpcGetInitialConfiguration.EXT_CONFIG);
                JCO.Structure exSpcgetinitialconfiguration_extConfigSpcgetinitialconfiguration_extConfig = exSpcgetinitialconfiguration_extConfig.getStructure(SpcGetInitialConfiguration.EXT_CONFIG);
                JCO.Structure header = exSpcgetinitialconfiguration_extConfigSpcgetinitialconfiguration_extConfig.getStructure(SpcGetInitialConfiguration.HEADER);
                String rootInstanceId = header.getString(SpcGetInitialConfiguration.ROOT_ID);
                if (rootInstanceId == null || rootInstanceId.equals("")){
                    // the external config is not filled
                    category.logT(Severity.DEBUG, location, "getInitialConfiguration(): The external config returned by AP is not filled");
                    snap = null;
                }
                else {
                    ext_configuration ext_config = ExternalConfigConverter.createConfig(
                                                        exSpcgetinitialconfiguration_extConfigSpcgetinitialconfiguration_extConfig,
                                                        null);                       
                    if (ext_config != null){
                        snap = factory.newConfigurationSnapshot(ext_config, "initialConfiguration", "", null, true);
                        this.initialConfiguration = ext_config;
                        if (location.beDebug()) {
                            String xml = ((c_ext_cfg_imp)ext_config).cfg_ext_to_xml_string();
                            location.debugT("Initial Configuration returned by AP:");
                            location.debugT(xml);
                        }
                    }
                    else {
                        category.logT(Severity.DEBUG, location, "getInitialConfiguration(): It was not possible to generate an ext_configuration object.");
                        snap = null;
                    }
                }
            }
            catch(ClientException e) {
                // if there is no doc/item the FM throws an exception (the initial config is 
                // linked to the item, so without one it does not work)
                category.logT(Severity.DEBUG, location, "getInitialConfiguration(): There is no document and item -> no initial configuration can be retrieved.");
                snap = null;
            }                
        }
        else {
            snap = factory.newConfigurationSnapshot(this.initialConfiguration, "initialConfiguration", "", null, true);
            category.logT(Severity.DEBUG, location, "getInitialConfiguration(): There is already an initialConfiguration -> no need to call AP-CFG.");
        }
        location.exiting();
        return snap;         
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#changeExternalId(java.lang.String)
     */
    public void changeExternalId(String value) throws IPCException {
        location.entering("changeExternalId(String value)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("External id " + value);
        }
        JCO.Function function = getChangeItemsFunction();
        JCO.ParameterList im = function.getImportParameterList();
        JCO.Table imSpcchangeitems_itItem = im.getTable(SpcChangeItems.IT_ITEM);
        if (imSpcchangeitems_itItem.getNumRows() == 0) {
            imSpcchangeitems_itItem.appendRow();
        }

        _props.setExternalId(value);
        
        _changeItem(null, null, im, function);
    }
}
