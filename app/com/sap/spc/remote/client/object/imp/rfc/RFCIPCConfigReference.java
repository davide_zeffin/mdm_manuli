package com.sap.spc.remote.client.object.imp.rfc;

import java.util.Properties;
import com.sap.spc.remote.client.object.IPCConfigReference;

/**
 * @author I026584
 *
 */
public class RFCIPCConfigReference extends IPCConfigReference {

	protected  Properties jcoConnProps = null;
	
	public RFCIPCConfigReference(){
	}

	public RFCIPCConfigReference(Properties jcoConnProps,  String sessionId, String configId) {
			this.jcoConnProps 	= jcoConnProps;
			this.configId = configId;
	}
	
	public Properties getConnectionProperties(){
		return jcoConnProps;
	}
	
	public void setConnectionProperties(Properties jcoConnProps){
		this.jcoConnProps = jcoConnProps;
	}
	
}
