package com.sap.spc.remote.client.object.imp.rfc;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.ImportConfiguration;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.imp.DefaultIPCClient;
import com.sap.spc.remote.client.rfc.function.CfgApiCreateConfig;
import com.sap.spc.remote.client.rfc.function.CfgApiFindKnowledgebases;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.rfc.function.VmcGetAllValidGridVariant;
import com.sap.spc.remote.client.rfc.function.VmcGetVarTable;
import com.sap.spc.remote.client.rfc.function.CrmPmeUimodelDetermineRfc;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I026584
 *
 */
public class RfcDefaultIPCClient
	extends DefaultIPCClient
	implements IPCClient {
        
		// get the Logging Object of the Client Layer
		private static final Category category = ResourceAccessor.category;
		private static final Location location = ResourceAccessor.getLocation(RfcDefaultIPCClient.class);
		
	/**
	 * @param rfcClientSupport
	 * @param tokenId
	 * @param configId
	 */
	public RfcDefaultIPCClient(
		BackendBusinessObject rfcClientSupport,
		String tokenId,
		String configId) {
		ipcSession = factory.newIPCSession(this, rfcClientSupport, tokenId, configId);
	}

	/**
	 * @param rfcClientSupport
	 * @param _monitor
	 * @param tokenId
	 */
	public RfcDefaultIPCClient(BackendBusinessObject rfcClientSupport, IMonitor monitor, String tokenId, String configId) {
		ipcSession = factory.newIPCSession(this, rfcClientSupport, monitor, tokenId, configId);
	}

	/**
	 * @param rfcClientSupport
	 * @param monitor
	 */
	public RfcDefaultIPCClient(BackendBusinessObject rfcClientSupport, IMonitor monitor) {
		this();
		ipcSession = factory.newIPCSession(this, rfcClientSupport, monitor);
	}

	/**
	 * @param rfcClientSupport
	 */
	public RfcDefaultIPCClient(BackendBusinessObject rfcClientSupport) {
		this();
		ipcSession = factory.newIPCSession(this, rfcClientSupport);
	}
	
	public RfcDefaultIPCClient() {
		this.closed = false;
		SAT_STRING = "SAT_NOT_SUPPORTED_BY_THIS_CLIENT"; //VersionGet.getSATRequestPrefixIPCClient() + getClass().getName();
	}

	public RfcDefaultIPCClient(String mandt, String type) throws IPCException{
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	public RfcDefaultIPCClient(String mandt, String type, IClient client)
				throws IPCException{
		this();
		ipcSession = factory.newIPCSession(this, mandt, type, client, (IPCItemReference) null);
	}


	public RfcDefaultIPCClient(String mandt, String type, IPCItemReference ref)
				throws IPCException{
		this();
		ipcSession = factory.newIPCSession(this, mandt, type, null, ref);
	}


	public RfcDefaultIPCClient(IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
	}

	public RfcDefaultIPCClient(String mandt, String type,IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
		ipcSession = factory.newIPCSession(this, mandt, type, null, (IPCItemReference) null);
	}

	public RfcDefaultIPCClient(String sessionId,IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
	}

	public RfcDefaultIPCClient(String mandt, String type, IPCItemReference reference,IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
		ipcSession = factory.newIPCSession(this, mandt, type, null, reference);
	}

	public RfcDefaultIPCClient(String mandt, String type, IPCConfigReference reference,IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
		ipcSession = factory.newIPCSession(this, mandt, type, null, reference);
	}



	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#createConfiguration(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String[], java.lang.String[])
	 */
	public Configuration createConfiguration(
		String productId,
		String productType,
		String language,
		String kbDate,
		String kbLogSys,
		String kbName,
		String kbVersion,
		String kbProfile,
		String[] contextNames,
		String[] contextValues)
		throws IPCException {
            location.entering("createConfiguration(String productId, String productType, "
                               + " String language, String kbDate, String kbLogSys, String kbName," 
                               + " String kbVersion, String kbProfile, String[] contextNames,"
                               + " String[] contextValues");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String specTypeId " + productId);
                location.debugT("String specTypeId " + productType);
                location.debugT("String specTypeId " + language);
                location.debugT("String specTypeId " + kbDate);
                location.debugT("String specTypeId " + kbLogSys);
                location.debugT("String specTypeId " + kbName);
                location.debugT("String specTypeId " + kbVersion);
                location.debugT("String specTypeId " + kbProfile);
                location.debugT("String[] contextNames: ");
                if (contextNames != null) {
                    for (int i = 0; i<contextNames.length; i++){
                        location.debugT("  contextNames["+i+"] "+contextNames[i]);
                    }
                }
                location.debugT("String[] contextValues: ");
                if (contextValues != null) {
                    for (int i = 0; i<contextValues.length; i++){
                        location.debugT("  contextValues["+i+"] "+contextValues[i]);
                    }
                }
            }

            
			Vector parameters = new Vector();
			Configuration configuration = null;
    		startComponentInternalUse("createConfiguration");

			if(productId != null){
			  parameters.addElement(CfgApiCreateConfig.PRODUCT_ID);
			  parameters.addElement(productId);
			}
			else{
			  if (location.beDebug()) {
                location.debugT("return null");  
              }
              location.exiting();
			  return null;
            }
            
			if(productType != null){
			  parameters.addElement(CfgApiCreateConfig.PRODUCT_TYPE);
			  parameters.addElement(productType);
			}
			if(kbDate != null){
			  parameters.addElement(CfgApiCreateConfig.KB_DATE);
			  parameters.addElement(kbDate);
			}
			if(kbLogSys != null){
			  parameters.addElement(CfgApiCreateConfig.KB_LOGSYS);
			  parameters.addElement(kbLogSys);
			}
			if(kbName != null){
			  parameters.addElement(CfgApiCreateConfig.KB_NAME);
			  parameters.addElement(kbName);
			}
			if(kbVersion != null){
			  parameters.addElement(CfgApiCreateConfig.KB_VERSION);
			  parameters.addElement(kbVersion);
			}
			if(kbProfile != null){
			  parameters.addElement(CfgApiCreateConfig.KB_PROFILE);
			  parameters.addElement(kbProfile);
			}

			if(parameters.size() > 0){
                String[] parametersArray = new String[parameters.size()];
			    parameters.toArray(parametersArray);
			    try {
				    startComponentInternalUse(IFunctionGroup.CFG_API_CREATE_CONFIG);
				    RFCDefaultClient rfcClient = (RFCDefaultClient)ipcSession.getClient();
      			    JCO.Function functionCfgApiCreateConfig = rfcClient.getFunction(IFunctionGroup.CFG_API_CREATE_CONFIG);
    			    JCO.ParameterList importsCfgApiCreateConfig = functionCfgApiCreateConfig.getImportParameterList();
    			    JCO.ParameterList exportsCfgApiCreateConfig = functionCfgApiCreateConfig.getExportParameterList();
    			    JCO.ParameterList tablesCfgApiCreateConfig = functionCfgApiCreateConfig.getTableParameterList();

                    // process table importsCfgApiCreateConfigCfgapicreateconfig_context
                    // optional, TABLE, Configuration Context
			        JCO.Table importsCfgApiCreateConfigCfgapicreateconfig_context = importsCfgApiCreateConfig.getTable(CfgApiCreateConfig.CONTEXT);
			        if((contextNames != null) && (contextValues != null)) {
                        if (contextNames.length == contextValues.length){
                            for(int i=0; i<contextNames.length; i++){
                                importsCfgApiCreateConfigCfgapicreateconfig_context.appendRow();
                                importsCfgApiCreateConfigCfgapicreateconfig_context.setValue(contextNames[i], CfgApiCreateConfig.NAME);		// CHAR, Configuration Context - Name
                                importsCfgApiCreateConfigCfgapicreateconfig_context.setValue(contextValues[i], CfgApiCreateConfig.VALUE);		// CHAR, Configuration Context - Value
                            }
                        }
			         }
        			 //Collect all input params in an array and set the valuse.
        			 if (parametersArray != null) {
                         for (int i=0; i<parametersArray.length; i=i+2) {
                             importsCfgApiCreateConfig.setValue(parametersArray[i+1], parametersArray[i]);
        				 }
        			 }
                     category.logT(Severity.PATH, location, "executing RFC CFG_API_CREATE_CONFIG");                     
     			     rfcClient.execute(functionCfgApiCreateConfig);
                     category.logT(Severity.PATH, location, "done with RFC CFG_API_CREATE_CONFIG");
    			     String configId = exportsCfgApiCreateConfig.getString(CfgApiCreateConfig.CONFIG_ID);
                	 endComponentInternalUse(IFunctionGroup.CFG_API_CREATE_CONFIG);
  				     configuration = factory.newConfiguration(this, configId);
  				     endComponentInternalUse("createConfiguration");
			    }catch(ClientException e){
                    category.logT(Severity.ERROR, location, e.toString());
                    endComponentInternalUse("createConfiguration");
				    throw new IPCException(e);
                }
			}
            if (location.beDebug()) {
              location.debugT("return configuration " + configuration.getId());  
            }
            location.exiting();
			return configuration;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#createConfiguration(java.lang.String, com.sap.sce.kbrt.ext_configuration, java.lang.String[], java.lang.String[], java.lang.String)
	 */
	public Configuration createConfiguration(
		String language,
		ext_configuration config,
		String[] contextNames,
		String[] contextValues,
		String date)
		throws IPCException {

            location.entering("createConfiguration(String language, ext_configuration config, " 
                              + "String[] contextNames, String[] contextValues, String date)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String language " + language);
                location.debugT("ext_configuration config " + config.get_name());
                location.debugT("String[] contextNames ");
                if (contextNames != null) {
                    for (int i = 0; i<contextNames.length; i++){
                        location.debugT("  contextNames["+i+"] "+contextNames[i]);
                    }
                }
                location.debugT("String[] contextValues: ");
                if (contextValues != null) {
                    for (int i = 0; i<contextValues.length; i++){
                        location.debugT("  contextValues["+i+"] "+contextValues[i]);
                    }
                }
                location.debugT("String date " + date);
            }
			startComponentInternalUse("createConfiguration");
            try {
                startComponentInternalUse(IFunctionGroup.CFG_API_CREATE_CONFIG);
                JCO.Function functionCfgApiCreateConfig = ((RFCDefaultClient)ipcSession.getClient()).getFunction(IFunctionGroup.CFG_API_CREATE_CONFIG);
                JCO.ParameterList im = functionCfgApiCreateConfig.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiCreateConfig.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiCreateConfig.getTableParameterList();

                // process table imCfgapicreateconfig_context
                // optional, TABLE, Configuration Context
                JCO.Table imCfgapicreateconfig_context = im.getTable(CfgApiCreateConfig.CONTEXT);
                if (contextNames != null) {
                    for (int i=0; i<contextNames.length; i++) {
                        imCfgapicreateconfig_context.appendRow();
                        imCfgapicreateconfig_context.setValue(contextNames[i], CfgApiCreateConfig.NAME);     // CHAR, Configuration Context - Name
                        imCfgapicreateconfig_context.setValue(contextValues[i], CfgApiCreateConfig.VALUE);       // CHAR, Configuration Context - Value
                    }
                }
                
                // process structure imCfgapicreateconfig_extConfig
                JCO.Structure imCfgapicreateconfig_extConfig = im.getStructure(CfgApiCreateConfig.EXT_CONFIG);
                RfcConfigValue cValue = new RfcConfigValue();
                cValue.getConfigIPCParameters(imCfgapicreateconfig_extConfig, config);

                if (date != null) {
                    im.setValue(date, CfgApiCreateConfig.KB_DATE);        // optional, CHAR, KB validity date (YYYYMMDD)
                }

                category.logT(Severity.PATH, location, "executing RFC CFG_API_CREATE_CONFIG");
                ((RFCDefaultClient)ipcSession.getClient()).execute(functionCfgApiCreateConfig);
                category.logT(Severity.PATH, location, "done with RFC CFG_API_CREATE_CONFIG");
                
                String configId = ex.getString(CfgApiCreateConfig.CONFIG_ID);
				if(_monitor != null)
				    endComponentInternalUse(IFunctionGroup.CFG_API_CREATE_CONFIG);
				endComponentInternalUse("CreateConfiguration");
                Configuration configuration = factory.newConfiguration(this, configId);
                if (location.beDebug()) {
                    location.debugT("return configuration " + configuration.getId());
                }
                location.exiting();
   			    return configuration;
            }
			catch(ClientException e){
                category.logT(Severity.ERROR, location, e.toString());
				endComponentInternalUse("createConfiguration");
                throw new IPCException(e);
			}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#createImportConfiguration(com.sap.sce.kbrt.ext_configuration, java.lang.String[], java.lang.String[])
	 */
	public ImportConfiguration createImportConfiguration(
    ext_configuration config,
    String[] contextNames,
    String[] contextValues)
		throws IPCException {
            location.entering("createImportConfiguration(ext_configuration config, String[] contextNames, String[] contextValues)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("ext_configuration config " + config.get_name());
                location.debugT("String[] contextNames ");
                if (contextNames != null) {
                    for (int i = 0; i<contextNames.length; i++){
                        location.debugT("  contextNames["+i+"] "+contextNames[i]);
                    }
                }
                location.debugT("String[] contextValues: ");
                if (contextValues != null) {
                    for (int i = 0; i<contextValues.length; i++){
                        location.debugT("  contextValues["+i+"] "+contextValues[i]);
                    }
                }
            }
			startComponentInternalUse("createImportConfiguration");

			try {
                JCO.Function functionCfgApiCreateConfig = ((RFCDefaultClient)ipcSession.getClient()).getFunction(IFunctionGroup.CFG_API_CREATE_CONFIG);
                JCO.ParameterList im = functionCfgApiCreateConfig.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiCreateConfig.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiCreateConfig.getTableParameterList();

                // process table imCfgapicreateconfig_context
                // optional, TABLE, Configuration Context
                JCO.Table imCfgapicreateconfig_context = im.getTable(CfgApiCreateConfig.CONTEXT);
                if (contextNames != null) {
                    for (int i=0; i<contextNames.length; i++) {
                        imCfgapicreateconfig_context.appendRow();
                        imCfgapicreateconfig_context.setValue(contextNames[i], CfgApiCreateConfig.NAME);     // CHAR, Configuration Context - Name
                        imCfgapicreateconfig_context.setValue(contextValues[i], CfgApiCreateConfig.VALUE);       // CHAR, Configuration Context - Value
                    }
                }
                
                // process structure imCfgapicreateconfig_extConfig
                JCO.Structure imCfgapicreateconfig_extConfig = im.getStructure(CfgApiCreateConfig.EXT_CONFIG);
                RfcConfigValue cValue = new RfcConfigValue();
                cValue.getConfigIPCParameters(imCfgapicreateconfig_extConfig, config);

                category.logT(Severity.PATH, location, "executing RFC CFG_API_CREATE_CONFIG");
                ((RFCDefaultClient)ipcSession.getClient()).execute(functionCfgApiCreateConfig);
                category.logT(Severity.PATH, location, "done with RFC CFG_API_CREATE_CONFIG");
                
                String configId = ex.getString(CfgApiCreateConfig.CONFIG_ID);
                
				if(_monitor != null)
					endComponentInternalUse(IFunctionGroup.CFG_API_CREATE_CONFIG);
				endComponentInternalUse("createImportConfiguration");
                ImportConfiguration importConfig = factory.newImportConfiguration(this, configId);
                if (location.beDebug()) {
                    location.debugT("return importConfig " + importConfig.getId());
                }
                location.exiting();
				return importConfig;
			}catch(ClientException e){
				category.logT(Severity.ERROR, location, e.toString());
				endComponentInternalUse("createImportConfiguration");
				throw new IPCException(e);
			}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getKnowledgeBases(java.lang.String)
	 */
	public List getKnowledgeBases(String kbToFind) {
        location.entering("getKnowledgeBases(String kbToFind)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String kbToFind " + kbToFind);
        }
        
		Vector kbs = new Vector();
		try {
            startComponentInternalUse("getKnowledgeBases");
    		startComponentInternalUse(IFunctionGroup.CFG_API_FIND_KNOWLEDGEBASES);
		    
            JCO.Function functionCfgApiFindKnowledgebases = ((RFCDefaultClient)ipcSession.getClient()).getFunction(IFunctionGroup.CFG_API_FIND_KNOWLEDGEBASES);
		    JCO.ParameterList im = functionCfgApiFindKnowledgebases.getImportParameterList();
		    JCO.ParameterList ex = functionCfgApiFindKnowledgebases.getExportParameterList();
		    JCO.ParameterList tbl = functionCfgApiFindKnowledgebases.getTableParameterList();
            
            // passing no OBJECT_NAME and no OBJECT_TYPE returns the list of all KBs
		    im.setValue("", CfgApiFindKnowledgebases.OBJECT_NAME);		// CHAR, Object Name
		    im.setValue("", CfgApiFindKnowledgebases.OBJECT_TYPE);		// CHAR, Object Type
		    im.setValue(CfgApiFindKnowledgebases.TRUE, CfgApiFindKnowledgebases.WITH_KB_PROFILES);		// optional, CHAR, Boolean(T=True,F=False,Blank=True)
            category.logT(Severity.PATH, location, "executing RFC CFG_API_FIND_KNOWLEDGEBASES");
            ((RFCDefaultClient)ipcSession.getClient()).execute(functionCfgApiFindKnowledgebases); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC CFG_API_FIND_KNOWLEDGEBASES");

  		    JCO.Table exCfgapifindknowledgebases_knowledgebases = ex.getTable(CfgApiFindKnowledgebases.KNOWLEDGEBASES);
		    int kbsSize = exCfgapifindknowledgebases_knowledgebases.getNumRows(); 
		    String[] kbNames            = new String[kbsSize];
		    String[] kbVersions         = new String[kbsSize];
		    String[] kbBuilds			  = new String[kbsSize];
		    String[] kbProfiles         = new String[kbsSize];
		  	String[] kbLogsys           = new String[kbsSize];
		    for (int i=0; i<kbsSize; i++) {
                exCfgapifindknowledgebases_knowledgebases.setRow(i);
    			kbBuilds[i] = ((Integer)exCfgapifindknowledgebases_knowledgebases.getValue(CfgApiFindKnowledgebases.KB_BUILD)).toString();
    		    kbLogsys[i] = exCfgapifindknowledgebases_knowledgebases.getString(CfgApiFindKnowledgebases.KB_LOGSYS);
			    kbNames[i] = exCfgapifindknowledgebases_knowledgebases.getString(CfgApiFindKnowledgebases.KB_NAME);
    			kbProfiles[i] = exCfgapifindknowledgebases_knowledgebases.getString(CfgApiFindKnowledgebases.KB_PROFILE);
    			kbVersions[i] = exCfgapifindknowledgebases_knowledgebases.getString(CfgApiFindKnowledgebases.KB_VERSION);
            }
    	    endComponentInternalUse(IFunctionGroup.CFG_API_FIND_KNOWLEDGEBASES);
            // keep a list of already known KBs to avoid redundancy
            HashMap knownKnowledgebases = new HashMap();
   		    for (int i=0; i < kbNames.length; i++) {
                KnowledgeBase kb;
			    if (kbNames[i].regionMatches(true, 0, kbToFind, 0, kbToFind.length())) {
                    // generate unique key
                    String kbTechKey = kbLogsys[i] + kbNames[i] + kbVersions[i] + kbBuilds[i];
                    // check whether this KB is already known
                    KnowledgeBase knownKB = (KnowledgeBase) knownKnowledgebases.get(kbTechKey);
                    if (knownKB == null) {
                        // KB was not found, create a new one
                        kb = factory.newKnowledgeBase(this,
    				 								kbLogsys[i],
    				 								kbNames[i],
    												kbVersions[i],
    												kbBuilds[i]);
                        knownKnowledgebases.put(kbTechKey, kb); // put it into the hashmap of known KBs
                        kbs.addElement(kb); // add it to the list of KBs    
                    }
                    else {
                        kb = knownKB; // KB already known; use this one
                    }
                    if (kbProfiles != null){
                        kb.createProfile(kbProfiles[i]);
				    }
                }
            }
        }
		catch(ClientException e) {
			 category.logT(Severity.ERROR, location, e.toString());
			 throw new IPCException(e);
		}
		endComponentInternalUse("getKnowledgeBases");
        if (location.beDebug()) {
            if (kbs != null) {
                location.debugT("return kbs");
                for (int i = 0; i<kbs.size(); i++){
                    location.debugT("  kbs["+i+"] "+kbs.elementAt(i).toString());
                }
            }
        }
        location.exiting();
		return kbs;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getUIModelGUID(com.sap.spc.remote.client.object.KnowledgeBase, java.lang.String, java.lang.String)
	 */
	public String getUIModelGUID(KnowledgeBase kb, String scenario,	String roleKey) {
		location.entering("getUIModelGUID(String kbToFind)");
		if (location.beDebug()){
			String kbName = "";
			String kbVersion = "";
			String kbLogsys = "";
			if (kb != null){
				kbName = kb.getName();
				kbVersion = kb.getVersion();
				kbLogsys = kb.getLogsys();
			}
			location.debugT("Parameters:");
			location.debugT("kbName " + kbName);
			location.debugT("kbVersion " + kbVersion);
			location.debugT("kbLogsys " + kbLogsys);
			location.debugT("scenario " + scenario);
			location.debugT("roleKey " + roleKey);
		}
		if (roleKey == null){
			roleKey = "";        
		}
        String uiModelGuid = CrmPmeUimodelDetermineRfc.INITIAL_GUID;
        String uiModelId = "";
        String uiModelDescription = "";
        if (kb == null){
			location.errorT("Error: KnowledgeBase object is null! Returning initial GUID: " + uiModelGuid);
			if (scenario.equals("")){
				location.errorT("Error: No scenario passed!");
			}
        	return uiModelGuid;
        }
		if (scenario.equals("")){
			location.errorT("Error: No scenario passed!");
		}
        
		try {
			startComponentInternalUse("getUIModelGUID");
			startComponentInternalUse(IFunctionGroup.CRM_PME_UIMODEL_DETERMINE_RFC);
		    
			JCO.Function functionCrmPmeUimodelDetermineRfc = ((RFCDefaultClient)ipcSession.getClient()).getFunction(IFunctionGroup.CRM_PME_UIMODEL_DETERMINE_RFC);
			JCO.ParameterList im = functionCrmPmeUimodelDetermineRfc.getImportParameterList();
			JCO.ParameterList ex = functionCrmPmeUimodelDetermineRfc.getExportParameterList();
			JCO.ParameterList tbl = functionCrmPmeUimodelDetermineRfc.getTableParameterList();

			im.setValue(kb.getName(), CrmPmeUimodelDetermineRfc.IV_KBNAME);
			im.setValue(kb.getVersion(), CrmPmeUimodelDetermineRfc.IV_KBVERSION);
			im.setValue(kb.getLogsys(), CrmPmeUimodelDetermineRfc.IV_KBLOGSYS);
			im.setValue(scenario, CrmPmeUimodelDetermineRfc.IV_SCENARIO);
			if (!roleKey.equals("")){
				im.setValue(roleKey, CrmPmeUimodelDetermineRfc.IV_ROLEKEY);
			}
			category.logT(Severity.PATH, location, "executing RFC CRM_PME_UIMODEL_DETERMINE_RFC");
			((RFCDefaultClient)ipcSession.getClient()).execute(functionCrmPmeUimodelDetermineRfc); // does not define ABAP Exceptions
			category.logT(Severity.PATH, location, "done with RFC CRM_PME_UIMODEL_DETERMINE_RFC");

			JCO.Structure esUimodel = ex.getStructure(CrmPmeUimodelDetermineRfc.ES_UIMODEL);
			uiModelId = esUimodel.getString(CrmPmeUimodelDetermineRfc.UIMODELID);
			uiModelGuid = esUimodel.getString(CrmPmeUimodelDetermineRfc.UIMODEL_GUID);
			uiModelDescription = esUimodel.getString(CrmPmeUimodelDetermineRfc.DESCRIPTION);

		}
		catch(ClientException e) {
			 category.logT(Severity.ERROR, location, e.toString());
			 throw new IPCException(e);
		}
		endComponentInternalUse("getUIModelGUID");
		if (location.beDebug()) {
			location.debugT("return uiModel GUID: " + uiModelGuid);
		}
		location.exiting();
		return uiModelGuid;
	}



	public HashMap getVarTable(String documentId, String itemId, String[] cstics, String filterValue){
		 location.entering("getVarTable(String documentId, String itemId, String[] cstics, String filterValue)");
         if (location.beDebug()){
            location.debugT("Parameters:");
             location.debugT("String documentId " + documentId);
             location.debugT("String itemId " + itemId);
             location.debugT("String[] cstics ");
             if (cstics != null) {
                 for (int i = 0; i<cstics.length; i++){
                     location.debugT("  cstics["+i+"] "+cstics[i]);
                 }
             }
             location.debugT("String filterValue " + filterValue);
         }
		 HashMap result = new HashMap();
		 
		 //	declarations
		 // import
		 String ivDocumentId = documentId;
		 String ivFilterDefault = "SAP_DEFAULT";
		 String ivFilterName = "SAP_RESTRICTION";
		 String ivFilterValue = filterValue;
		 String ivItemId = itemId;
		 
		 //export
		 int exportsVmcGetVarTableVmcgetvartable_etCsticsNamesCounter;
		 int exportsVmcGetVarTableVmcgetvartable_etCsticsValuesCounter;
	
		try{
		
			//	implementation
			//	get access to function
			 JCO.Function functionVmcGetVarTable = ((RFCDefaultClient)ipcSession.getClient()).getFunction(IFunctionGroup.SPC_GET_VAR_TABLE);
			 JCO.ParameterList importsVmcGetVarTable = functionVmcGetVarTable.getImportParameterList();
			 JCO.ParameterList exportsVmcGetVarTable = functionVmcGetVarTable.getExportParameterList();
			 JCO.ParameterList tablesVmcGetVarTable = functionVmcGetVarTable.getTableParameterList();
			//	fill import parameters from internal members
		
			//	process table importsVmcGetVarTableVmcgetvartable.itNameCstics
			//	optional, TABLE, table for grid characteristic names
			 importsVmcGetVarTable.setValue(ivDocumentId, VmcGetVarTable.IV_DOCUMENT_ID);		// String, Pricing Document GUID
			 importsVmcGetVarTable.setValue(ivFilterDefault, VmcGetVarTable.IV_FILTER_DEFAULT);		// optional, STRING, String
			 importsVmcGetVarTable.setValue(ivFilterName, VmcGetVarTable.IV_FILTER_NAME);		// optional, STRING, String
			 importsVmcGetVarTable.setValue(ivFilterValue, VmcGetVarTable.IV_FILTER_VALUE);		// optional, STRING, String
			 importsVmcGetVarTable.setValue(ivItemId, VmcGetVarTable.IV_ITEM_ID);		// String, ipc item id
			 
			if(cstics != null && cstics.length>0){
				JCO.Table importsVmcGetVarTableVmcgetvartable_itNameCstics = importsVmcGetVarTable.getTable(VmcGetVarTable.IT_NAME_CSTICS);
				for(int i=0; i<cstics.length; i++) {
					importsVmcGetVarTableVmcgetvartable_itNameCstics.appendRow();
					importsVmcGetVarTableVmcgetvartable_itNameCstics.setValue(cstics[i], VmcGetVarTable.CSTIC_NAMES);		// CHAR, Value
				}
			}		
		
			category.logT(Severity.PATH, location, "executing RFC SPC_GET_VAR_TABLE");
			((RFCDefaultClient)ipcSession.getClient()).execute(functionVmcGetVarTable); // does not define ABAP Exceptions
			category.logT(Severity.PATH, location, "done with RFC SPC_GET_VAR_TABLE");
			//	fill internal members from export parameters
		
			//	process table exportsVmcGetVarTableVmcgetvartable.etCsticsNames
			//	TABLE, table for grid characteristic names
			 JCO.Table exportsVmcGetVarTableVmcgetvartable_etCsticsNames = exportsVmcGetVarTable.getTable(VmcGetVarTable.ET_CSTICS_NAMES);
			 
			 String[] csticNames = new String[exportsVmcGetVarTableVmcgetvartable_etCsticsNames.getNumRows()];
			 
			 for (exportsVmcGetVarTableVmcgetvartable_etCsticsNamesCounter=0; exportsVmcGetVarTableVmcgetvartable_etCsticsNamesCounter<exportsVmcGetVarTableVmcgetvartable_etCsticsNames.getNumRows(); exportsVmcGetVarTableVmcgetvartable_etCsticsNamesCounter++) {
				 exportsVmcGetVarTableVmcgetvartable_etCsticsNames.setRow(exportsVmcGetVarTableVmcgetvartable_etCsticsNamesCounter);
				 csticNames[exportsVmcGetVarTableVmcgetvartable_etCsticsNamesCounter] = exportsVmcGetVarTableVmcgetvartable_etCsticsNames.getString(VmcGetVarTable.CSTIC_NAMES);
			 }
		
			//	process table exportsVmcGetVarTableVmcgetvartable.etCsticsValues
			//	TABLE, table for grid characteristic values
			 JCO.Table exportsVmcGetVarTableVmcgetvartable_etCsticsValues = exportsVmcGetVarTable.getTable(VmcGetVarTable.ET_CSTICS_VALUES);
			 
			 String[] csticValues = new String[exportsVmcGetVarTableVmcgetvartable_etCsticsValues.getNumRows()];
			 
			 for (exportsVmcGetVarTableVmcgetvartable_etCsticsValuesCounter=0; exportsVmcGetVarTableVmcgetvartable_etCsticsValuesCounter<exportsVmcGetVarTableVmcgetvartable_etCsticsValues.getNumRows(); exportsVmcGetVarTableVmcgetvartable_etCsticsValuesCounter++) {
				 exportsVmcGetVarTableVmcgetvartable_etCsticsValues.setRow(exportsVmcGetVarTableVmcgetvartable_etCsticsValuesCounter);
				 csticValues[exportsVmcGetVarTableVmcgetvartable_etCsticsValuesCounter] = exportsVmcGetVarTableVmcgetvartable_etCsticsValues.getString(VmcGetVarTable.CSTIC_VALUES);
			 }
			 
			result.put(VmcGetVarTable.CSTICS_NAMES, csticNames);
			result.put(VmcGetVarTable.CSTICS_VALUES, csticValues);
		}
		catch(ClientException e) {
					 category.logT(Severity.ERROR, location, e.toString());
					 throw new IPCException(e);
		}
		if (location.beDebug()) {
            if (result!=null){
                location.debugT("return result " + result.toString());
            }
            else location.debugT("return result null");
		}
        location.exiting();
		return result;
		
	}

	/* (non-Javadoc)
	* @see com.sap.spc.remote.client.object.IPCClient#getAllValidGridVariants(java.lang.String[])
	*/
	public HashMap getAllValidGridVariants(String productId, String logsys, String[] csticsNames, String[] csticsValues) {
        location.entering("getAllValidGridVariants(String productId, String logsys, String[] csticsNames, String[] csticsValues)");
        if (location.beDebug()){
           location.debugT("Parameters:");
            location.debugT("String productId " + productId);
            location.debugT("String logsys " + logsys);
            location.debugT("String[] csticsNames ");
            if (csticsNames != null) {
                for (int i = 0; i<csticsNames.length; i++){
                    location.debugT("  csticsNames["+i+"] "+csticsNames[i]);
                }
            }
            location.debugT("String[] csticsValues ");
            if (csticsValues != null) {
                for (int i = 0; i<csticsValues.length; i++){
                    location.debugT("  csticsValues["+i+"] "+csticsValues[i]);
                }
            }

        }
		HashMap result = new HashMap();
		
		// declarations
		//import
		String ivLogSys = logsys;
		String ivProductId = productId;
		
		int exportsVmcGetAllValidGridVariantVmcgetallvalidgridvariant_etVariantsHeaderCounter;
		int exportsVmcGetAllValidGridVariantVmcgetallvalidgridvariant_etVariantsValuesCounter;
	
		
		
		
		try{
			// implementation
			// get access to function
			JCO.Function functionVmcGetAllValidGridVariant = ((RFCDefaultClient)ipcSession.getClient()).getFunction(IFunctionGroup.SPC_GET_ALL_VALID_GRID_VARIANTS);
			JCO.ParameterList importsVmcGetAllValidGridVariant = functionVmcGetAllValidGridVariant.getImportParameterList();
			JCO.ParameterList exportsVmcGetAllValidGridVariant = functionVmcGetAllValidGridVariant.getExportParameterList();
			JCO.ParameterList tablesVmcGetAllValidGridVariant = functionVmcGetAllValidGridVariant.getTableParameterList();
			// fill import parameters from internal members
			
			// process table importsVmcGetAllValidGridVariantVmcgetallvalidgridvariant.itCsticsNames
			// TABLE, table for grid characteristic names
			JCO.Table importsVmcGetAllValidGridVariantVmcgetallvalidgridvariant_itCsticsNames = importsVmcGetAllValidGridVariant.getTable(VmcGetAllValidGridVariant.IT_CSTICS_NAMES);
			for(int i=0; i<csticsNames.length; i++) {
				importsVmcGetAllValidGridVariantVmcgetallvalidgridvariant_itCsticsNames.appendRow();
				importsVmcGetAllValidGridVariantVmcgetallvalidgridvariant_itCsticsNames.setValue(csticsNames[i], VmcGetAllValidGridVariant.CSTIC_NAMES);		// CHAR, Characteristic name (object characteristic)
			}
			
			// process table importsVmcGetAllValidGridVariantVmcgetallvalidgridvariant.itCsticsValues
			// TABLE, table for grid characteristic values
			JCO.Table importsVmcGetAllValidGridVariantVmcgetallvalidgridvariant_itCsticsValues = importsVmcGetAllValidGridVariant.getTable(VmcGetAllValidGridVariant.IT_CSTICS_VALUES);
			for(int i=0; i<csticsValues.length; i++) {
				importsVmcGetAllValidGridVariantVmcgetallvalidgridvariant_itCsticsValues.appendRow();
				importsVmcGetAllValidGridVariantVmcgetallvalidgridvariant_itCsticsValues.setValue(csticsValues[i], VmcGetAllValidGridVariant.CSTIC_VALUES);		// CHAR, Value
			}
			importsVmcGetAllValidGridVariant.setValue(ivLogSys, VmcGetAllValidGridVariant.IV_LOG_SYS);		// CHAR, Logical System
			importsVmcGetAllValidGridVariant.setValue(ivProductId, VmcGetAllValidGridVariant.IV_PRODUCT_ID);		// CHAR, product id
			
			category.logT(Severity.PATH, location, "executing RFC SPC_GET_GRID_VARIANTS");
			((RFCDefaultClient)ipcSession.getClient()).execute(functionVmcGetAllValidGridVariant); // does not define ABAP Exceptions
			category.logT(Severity.PATH, location, "done with RFC SPC_GET_GRID_VARIANTS");
			// fill internal members from export parameters
			
			JCO.Table variantsHeaderTable = exportsVmcGetAllValidGridVariant.getTable(VmcGetAllValidGridVariant.ET_VARIANTS_HEADER);
			JCO.Table variantsValuesTable = exportsVmcGetAllValidGridVariant.getTable(VmcGetAllValidGridVariant.ET_VARIANTS_VALUES);
			
			String variantIds[] = new String[variantsHeaderTable.getNumRows()];
			String parentIds[] = new String[variantsHeaderTable.getNumRows()];
			String spreadValues[] = new String[variantsHeaderTable.getNumRows()];
			String[][] cNames = new String[variantIds.length][3];
			String[][] cValues = new String[variantIds.length][3];
			int counter = 0; 
			
			for(int i=0; i<variantsHeaderTable.getNumRows(); i++){
				variantsHeaderTable.setRow(i);
				variantIds[i] = (String)variantsHeaderTable.getValue(VmcGetAllValidGridVariant.VARIANT_ID);
				parentIds[i] = (String)variantsHeaderTable.getValue(VmcGetAllValidGridVariant.PARENT_ID);
				spreadValues[i] = (String)variantsHeaderTable.getValue(VmcGetAllValidGridVariant.SPREAD_VALUE);
				counter = 0;
				
				for(int j=0; j<variantsValuesTable.getNumRows(); j++){
					variantsValuesTable.setRow(j);
					if(((String)variantsValuesTable.getValue(VmcGetAllValidGridVariant.VARIANT_ID)).equals(variantIds[i])){
						cNames[i][counter] = (String)variantsValuesTable.getValue(VmcGetAllValidGridVariant.CSTIC_NAMES);
						cValues[i][counter] = (String)variantsValuesTable.getValue(VmcGetAllValidGridVariant.CSTIC_VALUES);
						counter++;
					}
				}
			}
			result.put("variantId", variantIds);
			result.put("parentId", parentIds);
			result.put("spreadValue", spreadValues);
			result.put("csticsNames", cNames);
			result.put("csticsValues", cValues);
            if (location.beDebug()) {
                if (result!=null){
                    location.debugT("return result " + result.toString());
                }
                else location.debugT("return result null");
            }
            location.exiting();
			return result;
		}
		catch(ClientException e){
			category.logT(Severity.ERROR, location, e.toString());

			//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
			//new-->
			endComponentInternalUse("getAllValidGridVariants");
			//<--
            if (location.beDebug()) {
                if (result!=null){
                    location.debugT("return result " + result.toString());
                }
                else location.debugT("return result null");
            }
            location.exiting();
			return result;
		}
	}

	
	public Configuration getConfiguration(IPCConfigReference reference, boolean resetSession) throws IPCException {
		location.entering("getConfiguration(IPCConfigReference reference, boolean resetSession)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCConfigReference reference " + reference.getConfigId());
            location.debugT("boolean resetSession " + resetSession);
        }
        if (resetSession) {
            ipcSession.syncWithServer();
        }

		Configuration config = ipcSession.getConfiguration(reference.getConfigId());
        if (location.beDebug()){
            location.debugT("return config " + config.getId());
        }
        location.exiting();
		return config;
	}


	/* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCClient#getIPCItem(com.sap.spc.remote.client.object.IPCItemReference, boolean)
     */
    public IPCItem getIPCItem(IPCItemReference reference, boolean resetSession) throws IPCException {
        location.entering("getIPCItem(IPCItemReference reference, boolean resetSession)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCItemReference reference " + reference.getItemId());
            location.debugT("boolean resetSession " + resetSession);
        }
        if (resetSession)
            ipcSession.syncWithServer();

		IPCDocument document = ipcSession.getDocument(reference.getDocumentId());
		if (document == null){
			throw new IPCException(
						new ClientException(
							"500","Session do not have any document for document id: " + reference.getDocumentId()
						)
				   );
			 
		}
		IPCItem item = document.getItem(reference.getItemId());
		if (item == null){
			throw new IPCException(
						new ClientException(
							"500","Document "+ reference.getDocumentId() + " do not have any item for id: " + reference.getItemId()
						)
					);
		}
        if (location.beDebug()){
            location.debugT("return item " + item.getItemId());
        }
        location.exiting();
		return item;
	}

	public void synchronizeWithServer() throws IPCException {
		try{
			throw new ClientException("500","RfcDefaultIPCClient.synchronizeWithServer() is not supported by 5.0. Remove the usage of this method or contact Developer.");
		}catch(ClientException e){
			throw new IPCException(e);
		}
	}

    /**
     * Returns a list of kb-objects for the given product.
     * @param productId The product for which you want to have a list of kbs
     * @return List of kb-objects.
     */
    public List getKnowledgeBasesForProduct(String productId, String kbLogsysImport) {
        location.entering("getKnowledgeBasesForProduct(String productId, String kbLogsysImport)");
        if (location.beDebug()){
           location.debugT("Parameters:");
            location.debugT("String productId " + productId);
            location.debugT("String kbLogsysImport " + kbLogsysImport);
        }
        Vector kbs = new Vector();
        try {
            startComponentInternalUse("getKnowledgeBases");
            startComponentInternalUse(IFunctionGroup.CFG_API_FIND_KNOWLEDGEBASES);
            
            JCO.Function functionCfgApiFindKnowledgebases = ((RFCDefaultClient)ipcSession.getClient()).getFunction(IFunctionGroup.CFG_API_FIND_KNOWLEDGEBASES);
            JCO.ParameterList im = functionCfgApiFindKnowledgebases.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiFindKnowledgebases.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiFindKnowledgebases.getTableParameterList();
            
            // passing no OBJECT_NAME returns the list of all KBs
            im.setValue(productId, CfgApiFindKnowledgebases.OBJECT_NAME);      // CHAR, Object Name
            im.setValue(CfgApiFindKnowledgebases.MARA, CfgApiFindKnowledgebases.OBJECT_TYPE);       // CHAR, Object Type
            im.setValue(CfgApiFindKnowledgebases.TRUE, CfgApiFindKnowledgebases.WITH_KB_PROFILES);      // optional, CHAR, Boolean(T=True,F=False,Blank=True)
            im.setValue(kbLogsysImport, CfgApiFindKnowledgebases.OBJECT_LOGSYS);      // optional, CHAR, Boolean(T=True,F=False,Blank=True)
            category.logT(Severity.PATH, location, "executing RFC CFG_API_FIND_KNOWLEDGEBASES");
            ((RFCDefaultClient)ipcSession.getClient()).execute(functionCfgApiFindKnowledgebases); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC CFG_API_FIND_KNOWLEDGEBASES");

            JCO.Table exCfgapifindknowledgebases_knowledgebases = ex.getTable(CfgApiFindKnowledgebases.KNOWLEDGEBASES);
            int kbsSize = exCfgapifindknowledgebases_knowledgebases.getNumRows(); 
            String[] kbNames            = new String[kbsSize];
            String[] kbVersions         = new String[kbsSize];
            String[] kbBuilds             = new String[kbsSize];
            String[] kbProfiles         = new String[kbsSize];
            String[] kbLogsys           = new String[kbsSize];
            for (int i=0; i<kbsSize; i++) {
                exCfgapifindknowledgebases_knowledgebases.setRow(i);
                kbBuilds[i] = ((Integer)exCfgapifindknowledgebases_knowledgebases.getValue(CfgApiFindKnowledgebases.KB_BUILD)).toString();
                kbLogsys[i] = exCfgapifindknowledgebases_knowledgebases.getString(CfgApiFindKnowledgebases.KB_LOGSYS);
                kbNames[i] = exCfgapifindknowledgebases_knowledgebases.getString(CfgApiFindKnowledgebases.KB_NAME);
                kbProfiles[i] = exCfgapifindknowledgebases_knowledgebases.getString(CfgApiFindKnowledgebases.KB_PROFILE);
                kbVersions[i] = exCfgapifindknowledgebases_knowledgebases.getString(CfgApiFindKnowledgebases.KB_VERSION);
            }
            endComponentInternalUse(IFunctionGroup.CFG_API_FIND_KNOWLEDGEBASES);
            
            for (int i=0; i < kbNames.length; i++) {
                KnowledgeBase kb;
                kb = factory.newKnowledgeBase(this,
                                            kbLogsys[i],
                                            kbNames[i],
                                            kbVersions[i],
                                            kbBuilds[i]);
                if (kbProfiles != null){
                    kb.createProfile(kbProfiles[i]);
                }
                kbs.addElement(kb);
            }
        }
        catch(ClientException e) {
             category.logT(Severity.ERROR, location, e.toString());
             throw new IPCException(e);
        }
        endComponentInternalUse("getKnowledgeBases");
        if (location.beDebug()){
            if (kbs != null) {
                location.debugT("return kbs");
                    for (int i = 0; i<kbs.size(); i++){
                        location.debugT("  kbs["+i+"] "+kbs.elementAt(i).toString());
                    }
                }
        }
        location.exiting();
        return kbs;
    }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getConditionGroupAttributes(java.lang.String, java.lang.String)
	 */
	public String[] getConditionGroupAttributes(String usage, String conditionGroupName) {
		//No application is using this call in RFC scenario.
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getConditionRecordsFromDatabase(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[], java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public HashMap getConditionRecordsFromDatabase(String usage, String application, String[] selAttributesInclArray, String[] selAttributesInclValueArray, String[] selAttributesExclArray, String[] selAttributesExclValueArray, String validityDateStart_yyyyMMddmmhhss, String validityDateEnd_yyyyMMddmmhhss, String conditionGroupId, String numberOfConditionRecords) {
		//No application is using this call in RFC scenario.
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}    
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getConditionTableFieldLabel(java.lang.String, java.lang.String)
	 */
	public Hashtable getConditionTableFieldLabel(String usage, String application, String[] fieldNames, String[] dataElementNames, boolean isApplicationField) { 
		//No application is using this call in RFC scenario.
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	public HashMap convertExternalToInternal(String[] names, String[] externalValues){
		//No application is using this call in RFC scenario.
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}
	public HashMap convertInternalToExternal(String[] names, String[] internalValues){
		//No application is using this call in RFC scenario.
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}
	
	public Hashtable convertFieldnameInternalToExternal(String usage, String application, String[] intFieldNames, boolean isApplicationField ){
		//No application is using this call in RFC scenario.
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getConditionRecordsFromRefGuid(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public HashMap getConditionRecordsFromRefGuid(String usage, String application, String refGuid, String refType, String numberOfConditionRecords) throws IPCException {
		//No application is using this call in RFC scenario.
		throw new IPCException(IPCException.UNIMPLEMENTED);
	}
	
	public int getNumberOfConditionRecordSelected(String usage) throws IPCException {
		//		No application is using this call in RFC scenario.
			  throw new IPCException(IPCException.UNIMPLEMENTED);
	}

}
