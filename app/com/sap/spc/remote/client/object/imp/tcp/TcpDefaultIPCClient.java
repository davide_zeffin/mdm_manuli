
/*
 * Created on Dec 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.ImportConfiguration;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.imp.DefaultIPCClient;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.shared.command.ConditionSelectionCommand;
import com.sap.spc.remote.shared.command.ConvertExternalToInternal;
import com.sap.spc.remote.shared.command.ConvertFieldnameInternalToExternal;
import com.sap.spc.remote.shared.command.ConvertInternalToExternal;
import com.sap.spc.remote.shared.command.CreateConfig;
import com.sap.spc.remote.shared.command.FindKnowledgeBases;
import com.sap.spc.remote.shared.command.GetConditionGroupAttributes;
import com.sap.spc.remote.shared.command.GetConditionRecordsFromDatabase;
import com.sap.spc.remote.shared.command.GetConditionRecordsFromRefGuid;
import com.sap.spc.remote.shared.command.GetConditionTableFieldLabel;
import com.sap.spc.remote.shared.command.GetNumberOfConditionRecordSelected;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TcpDefaultIPCClient
	extends DefaultIPCClient implements IPCClient
	{
		
	// get the Logging Object of the Client Layer
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(TcpDefaultIPCClient.class);
	//Constructors

	public TcpDefaultIPCClient() {
		this.closed = false;
		SAT_STRING = "SAT_NOT_SUPPORTED_BY_THIS_CLIENT"; //VersionGet.getSATRequestPrefixIPCClient() + getClass().getName();
	}

	public TcpDefaultIPCClient(String mandt, String type) throws IPCException{
		this();
		ipcSession = factory.newIPCSession(this, mandt, type, null, (IPCItemReference) null);
	}


	public TcpDefaultIPCClient(String ipcSessionId) {
		//find and attach to existing session
	}


	public TcpDefaultIPCClient(String mandt, String type, IClient client)
				throws IPCException{
		this();
		ipcSession = factory.newIPCSession(this, mandt, type, client, (IPCItemReference) null);
	}


	public TcpDefaultIPCClient(String mandt, String type, IPCItemReference ref)
				throws IPCException{
		this();
		ipcSession = factory.newIPCSession(this, mandt, type, null, ref);
	}

	public TcpDefaultIPCClient(String mandt, String type, IPCItemReference ref, Properties securityProperties)
				throws IPCException{
		this();
		ipcSession = factory.newIPCSession(this, mandt, type, null, ref, securityProperties);
	}

	public TcpDefaultIPCClient(String mandt, String type, IPCConfigReference ref)
				throws IPCException{
		this();
		ipcSession = factory.newIPCSession(this, mandt, type, null, ref);
	}

	public TcpDefaultIPCClient(String mandt, String type, IPCConfigReference ref, Properties securityProperties)
				throws IPCException{
		this();
		ipcSession = factory.newIPCSession(this, mandt, type, null, ref, securityProperties);
	}


	public TcpDefaultIPCClient(IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
	}

	public TcpDefaultIPCClient(String mandt, String type,IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
		ipcSession = factory.newIPCSession(this, mandt, type, null, (IPCItemReference) null);
	}

	public TcpDefaultIPCClient(String sessionId,IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
	}

	public TcpDefaultIPCClient(String mandt, String type, IPCItemReference reference,IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
		ipcSession = factory.newIPCSession(this, mandt, type, null, reference);
	}

	public TcpDefaultIPCClient(String mandt, String type, IPCConfigReference reference,IMonitor monitor) throws IPCException
	{
		this();
		_monitor = monitor;
		ipcSession = factory.newIPCSession(this, mandt, type, null, reference);
	}




	//********Implementation for Abstract methods starts from here!********

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#createConfiguration(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String[], java.lang.String[])
	 */
	public Configuration createConfiguration(
		String productId,
		String productType,
		String language,
		String kbDate,
		String kbLogSys,
		String kbName,
		String kbVersion,
		String kbProfile,
		String[] contextNames,
		String[] contextValues)
		throws IPCException 
	{
    
    location.entering("createConfiguration(String productId, String productType, "
                       + " String language, String kbDate, String kbLogSys, String kbName," 
                       + " String kbVersion, String kbProfile, String[] contextNames,"
                       + " String[] contextValues");
    if (location.beDebug()){
        location.debugT("Parameters:");
        location.debugT("String specTypeId " + productId);
        location.debugT("String specTypeId " + productType);
        location.debugT("String specTypeId " + language);
        location.debugT("String specTypeId " + kbDate);
        location.debugT("String specTypeId " + kbLogSys);
        location.debugT("String specTypeId " + kbName);
        location.debugT("String specTypeId " + kbVersion);
        location.debugT("String specTypeId " + kbProfile);
        location.debugT("String[] contextNames: ");
        if (contextNames != null) {
            for (int i = 0; i<contextNames.length; i++){
                location.debugT("  contextNames["+i+"] "+contextNames[i]);
            }
        }
        location.debugT("String[] contextValues: ");
        if (contextValues != null) {
            for (int i = 0; i<contextValues.length; i++){
                location.debugT("  contextValues["+i+"] "+contextValues[i]);
            }
        }
    }
    
	Vector parameters = new Vector();
	Configuration configuration = null;

    //BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
    //new-->
    startComponentInternalUse("createConfiguration");
    //<--
    if (language != null) {
    	parameters.addElement(CreateConfig.KB_LANGUAGE);
    	parameters.addElement(language);
    }

	if(productId != null){
	  parameters.addElement(CreateConfig.PRODUCT_ID);
	  parameters.addElement(productId);
	}
	else
	  return null;

	if(productType != null){
	  parameters.addElement(CreateConfig.PRODUCT_TYPE);
	  parameters.addElement(productType);
	}
	if(kbDate != null){
	  parameters.addElement(CreateConfig.KB_DATE);
	  parameters.addElement(kbDate);
	}
	if(kbLogSys != null){
	  parameters.addElement(CreateConfig.KB_LOG_SYS);
	  parameters.addElement(kbLogSys);
	}
	if(kbName != null){
	  parameters.addElement(CreateConfig.KB_NAME);
	  parameters.addElement(kbName);
	}
	if(kbVersion != null){
	  parameters.addElement(CreateConfig.KB_VERSION);
	  parameters.addElement(kbVersion);
	}
	if(kbProfile != null){
	  parameters.addElement(CreateConfig.KB_PROFILE);
	  parameters.addElement(kbProfile);
	}

	if((contextNames != null) && (contextValues != null)) {
	   if (contextNames.length == contextValues.length)
	   for(int i=0; i<contextNames.length; i++){
		  parameters.addElement(CreateConfig.CONTEXT_NAMES);
		  parameters.addElement(contextNames[i]);
		  parameters.addElement(CreateConfig.CONTEXT_VALUES);
		  parameters.addElement(contextValues[i]);
	  }
	}

	if(parameters.size() > 0){
	  String[] parametersArray = new String[parameters.size()];
	  parameters.toArray(parametersArray);
	  try{
		//BD 25052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
		startComponentInternalUse("doCmd(CREATE_CONFIG)");
		//<--
		  ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(ISPCCommandSet.CREATE_CONFIG,
												  parametersArray);
		//BD 25052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
		endComponentInternalUse("doCmd(CREATE_CONFIG)");
		//<--
		  String configId = r.getParameterValue(CreateConfig.CONFIG_ID);
		  configuration = factory.newConfiguration(this, configId);
		//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
		endComponentInternalUse("createConfiguration");
		//<--
	  }catch(ClientException e){
		  category.logT(Severity.ERROR, location, e.toString());
		//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
		endComponentInternalUse("createConfiguration");
		//<--
		  throw new IPCException(e);
	  }
	}
    if (location.beDebug()) {
      location.debugT("return configuration " + configuration.getId());  
    }
    location.exiting();
	return configuration;
}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#createConfiguration(java.lang.String, com.sap.sce.kbrt.ext_configuration, java.lang.String[], java.lang.String[], java.lang.String)
	 */
	public Configuration createConfiguration(
		String language,
		ext_configuration config,
		String[] contextNames,
		String[] contextValues,
		String date)
		throws IPCException
    {

    location.entering("createConfiguration(String language, ext_configuration config, " 
                      + "String[] contextNames, String[] contextValues, String date)");
    if (location.beDebug()){
        location.debugT("Parameters:");
        location.debugT("String language " + language);
        location.debugT("ext_configuration config " + config.get_name());
        location.debugT("String[] contextNames ");
        if (contextNames != null) {
            for (int i = 0; i<contextNames.length; i++){
                location.debugT("  contextNames["+i+"] "+contextNames[i]);
            }
        }
        location.debugT("String[] contextValues: ");
        if (contextValues != null) {
            for (int i = 0; i<contextValues.length; i++){
                location.debugT("  contextValues["+i+"] "+contextValues[i]);
            }
        }
        location.debugT("String date " + date);
    }

	//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
	//new-->
	startComponentInternalUse("createConfiguration");
	//<--

    TcpConfigValue cValue = new TcpConfigValue();
    String[] configParams = cValue.getConfigIPCParameters(config);
	String[] params;
	int idx = 0;
	int clen = 0;

	if (configParams != null) clen = clen + configParams.length;
	if (contextNames != null) clen = clen + contextNames.length * 4;
	if (language != null) clen = clen + 2;
	if (date != null) clen = clen + 2;

	params = new String[clen];

	if (language != null) {
		params[idx++] = CreateConfig.KB_LANGUAGE;
		params[idx++] = language;
	}

	if (date != null) {
		params[idx++] = CreateConfig.KB_DATE;
		params[idx++] = date;
	}

	if (configParams != null) {
		for (int i=0; i<configParams.length; i++) {
			params[idx++] = configParams[i];
		}
	}

	if (contextNames != null) {
		for (int i=0; i<contextNames.length; i++) {
			params[idx++] = CreateConfig.CONTEXT_NAMES;
			params[idx++] = contextNames[i];
			params[idx++] = CreateConfig.CONTEXT_VALUES;
			params[idx++] = contextValues[i];
		}
	}

	  try{
		//BD 25052003: Changelist 42773 IPC 4.0 SP2 SAT
		//should we trace the servercall?????
		//or will it suffice to call _monitor.compAction(String compName, String action)
		//for the thread overview?????
		//new-->
		startComponentInternalUse("doCmd(CREATE_CONFIG)");
		//<--
		  ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(ISPCCommandSet.CREATE_CONFIG,
												  params);
		//BD 25052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
		if(_monitor != null)
		endComponentInternalUse("doCmd(CREATE_CONFIG)");
		//<--
		  String configId = r.getParameterValue(CreateConfig.CONFIG_ID);

		//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
		endComponentInternalUse("createConfiguration");
		//<--
          
        Configuration configuration = factory.newConfiguration(this, configId);
        if (location.beDebug()) {
            location.debugT("return configuration " + configuration.getId());
        }
        location.exiting();
		return configuration;

	  }catch(ClientException e){
		  category.logT(Severity.ERROR, location, e.toString());
		//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
		endComponentInternalUse("createConfiguration");
		//<--
		  throw new IPCException(e);
	  }
}
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#createImportConfiguration(com.sap.sce.kbrt.ext_configuration, java.lang.String[], java.lang.String[])
	 */
	public ImportConfiguration createImportConfiguration(
		ext_configuration config,
		String[] contextNames,
		String[] contextValues)
		throws IPCException 
    {
      
        location.entering("createImportConfiguration(ext_configuration config, String[] contextNames, String[] contextValues)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("ext_configuration config " + config.get_name());
            location.debugT("String[] contextNames ");
            if (contextNames != null) {
                for (int i = 0; i<contextNames.length; i++){
                    location.debugT("  contextNames["+i+"] "+contextNames[i]);
                }
            }
            location.debugT("String[] contextValues: ");
            if (contextValues != null) {
                for (int i = 0; i<contextValues.length; i++){
                    location.debugT("  contextValues["+i+"] "+contextValues[i]);
                }
            }
        }

	  startComponentInternalUse("createImportConfiguration");

      TcpConfigValue cValue = new TcpConfigValue();
	  String[] configParams = cValue.getConfigIPCParameters(config);
	  String[] params;
	  int idx = 0;
	  int clen = 0;

	  if (configParams != null) clen = clen + configParams.length;
	  if (contextNames != null) clen = clen + contextNames.length * 4;

	  params = new String[clen];
	  if (configParams != null) {
		  for (int i=0; i<configParams.length; i++) {
			  params[idx++] = configParams[i];
		  }
	  }
	  if (contextNames != null) {
		  for (int i=0; i<contextNames.length; i++) {
			  params[idx++] = CreateConfig.CONTEXT_NAMES;
			  params[idx++] = contextNames[i];
			  params[idx++] = CreateConfig.CONTEXT_VALUES;
			  params[idx++] = contextValues[i];
		  }
	  }
	  try{
		  startComponentInternalUse("doCmd(CREATE_CONFIG)");
		  ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(ISPCCommandSet.CREATE_CONFIG,
												  params);
		  if(_monitor != null)
			  endComponentInternalUse("doCmd(CREATE_CONFIG)");

		  String configId = r.getParameterValue(CreateConfig.CONFIG_ID);

		  endComponentInternalUse("createImportConfiguration");
          ImportConfiguration importConfig = factory.newImportConfiguration(this, configId);
          if (location.beDebug()) {
              location.debugT("return importConfig " + importConfig.getId());
          }
          location.exiting();
		  return importConfig;

	  }catch(ClientException e){
		  category.logT(Severity.ERROR, location, e.toString());
		  endComponentInternalUse("createImportConfiguration");
		  throw new IPCException(e);
	  }
    }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getKnowledgeBases(java.lang.String)
	 */
	public List getKnowledgeBases(String kbToFind) {
        location.entering("getKnowledgeBases(String kbToFind)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String kbToFind " + kbToFind);
        }
        Vector kbs = new Vector();
        try {
            //BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
            //new-->
            startComponentInternalUse("getKnowledgeBases");
            //<--
            String[] inputParameter = new String[] {
                FindKnowledgeBases.WITH_KB_PROFILES, FindKnowledgeBases.TRUE
            };
            //BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
            //new-->
            startComponentInternalUse("doCmd(FIND_KNOWLEDGEBASES)");
            //<--
        	ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(ISPCCommandSet.FIND_KNOWLEDGEBASES,inputParameter);
            //BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
            //new-->
            endComponentInternalUse("doCmd(FIND_KNOWLEDGEBASES)");
            //<--

        	String[] kbLogsys           = r.getParameterValues(FindKnowledgeBases.KBS_LOGSYS);
            String[] kbNames            = r.getParameterValues(FindKnowledgeBases.KBS_NAME);
        	String[] kbVersions         = r.getParameterValues(FindKnowledgeBases.KBS_VERSION);
        	String[] kbBuilds			= r.getParameterValues(FindKnowledgeBases.KBS_BUILD);
        	String[] kbProfiles         = r.getParameterValues(FindKnowledgeBases.KBS_PROFILE);
            // keep a list of already known KBs to avoid redundancy
            HashMap knownKnowledgebases = new HashMap();
        	for (int i=0; i < kbNames.length; i++) {
                KnowledgeBase kb;
                if (kbNames[i].regionMatches(true, 0, kbToFind, 0, kbToFind.length())) {
                    // generate unique key
                    String kbTechKey = kbLogsys[i] + kbNames[i] + kbVersions[i] + kbBuilds[i];
                    // check whether this KB is already known
                    KnowledgeBase knownKB = (KnowledgeBase) knownKnowledgebases.get(kbTechKey);
                    if (knownKB == null) {
                        // KB was not found, create a new one                    
                        kb = factory.newKnowledgeBase(
                                                this,
    											kbLogsys[i],
    											kbNames[i],
    											kbVersions[i],
    											kbBuilds[i]);
                        knownKnowledgebases.put(kbTechKey, kb); // put it into the hashmap of known KBs
                        kbs.addElement(kb); // add it to the list of KBs    
                    }
                    else {
                        kb = knownKB; // KB already known; use this one
                    }                    
                    if (kbProfiles != null){
                        kb.createProfile(kbProfiles[i]);
                    }
                }
            }
        }
        catch(ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        //BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
        //new-->
        endComponentInternalUse("getKnowledgeBases");
        //<--
        if (location.beDebug()) {
            if (kbs != null) {
                location.debugT("return kbs");
                for (int i = 0; i<kbs.size(); i++){
                    location.debugT("  kbs["+i+"] "+kbs.elementAt(i).toString());
                }
            }
        }
        location.exiting();
        return kbs;
    }
	
	
	public HashMap getVarTable(String documentId, String itemId, String[] cstics, String filterValue) 
	{
        location.entering("getVarTable(String documentId, String itemId, String[] cstics, String filterValue)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String documentId " + documentId);
            location.debugT("String itemId " + itemId);
            location.debugT("String[] cstics ");
            if (cstics != null) {
                for (int i = 0; i<cstics.length; i++){
                    location.debugT("  cstics["+i+"] "+cstics[i]);
                }
            }
            location.debugT("String filterValue " + filterValue);
        }
        
		final String DOCUMENT_ID = "documentId";
		final String ITEM_ID = "itemId";
		final String NAME_CSTICS = "NameCstics";
		final String RESTRICTION = "FilterName";
		final String RESTRICTION_ID = "SAP_RESTRICTION";
		final String FILTER_VALUE = "FilterValue";
		final String FILTER_DEFAULT = "FilterDefault";
		final String FILTER_DEFAULT_ID = "SAP_DEFAULT";
		final String CSTICS_NAMES = "csticsNames";
		final String CSTICS_VALUES = "csticsValues";
		
		HashMap result = new HashMap();
		ArrayList paramsList = new ArrayList();
		
		paramsList.add(DOCUMENT_ID);
		paramsList.add(documentId);
		paramsList.add(ITEM_ID);
		paramsList.add(itemId);
		
		if(cstics != null && cstics.length >0){
			paramsList.add(NAME_CSTICS);
			paramsList.add(cstics[0]);
		}
		
		paramsList.add(RESTRICTION);
		paramsList.add(RESTRICTION_ID);
		paramsList.add(FILTER_DEFAULT);
		paramsList.add(FILTER_DEFAULT_ID);
		paramsList.add(FILTER_VALUE);
		paramsList.add(filterValue);
				
		String[] params = new String[paramsList.size()];
		paramsList.toArray(params);
		
		try {
				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//new-->
				startComponentInternalUse("getVarTable");
				startComponentInternalUse("doCmd(GetVarTable)");
				//<--
				ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(ISPCCommandSet.GETVARTABLE,params);
				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//new-->
				endComponentInternalUse("doCmd(GetVarTable)");
				//<--

				String[] csticsNames = r.getParameterValues("CsticsNames");
				String[] csticsValues = r.getParameterValues("CsticsValues");

				result.put("csticsNames", csticsNames);
				result.put("csticsValues", csticsValues);

				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//new-->
				endComponentInternalUse("getVarTable");
				//<--
                if (location.beDebug()) {
                    if (result!=null){
                        location.debugT("return result " + result.toString());
                    }
                    else location.debugT("return result null");
                }
                location.exiting();
				return result;

			} catch (ClientException e) {
				category.logT(Severity.ERROR, location, e.toString());

				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//new-->
				endComponentInternalUse("getVarTable");
				//<--
                if (location.beDebug()) {
                    if (result!=null){
                       location.debugT("return result " + result.toString());
                   }
                   else location.debugT("return result null");
                }
                location.exiting();
				return result;
			}
	}
	
	
	public HashMap getAllValidGridVariants(String productId, String logsys, String[] csticsNames, String[] csticsValues){
		
        location.entering("getAllValidGridVariants(String productId, String logsys, String[] csticsNames, String[] csticsValues)");
        if (location.beDebug()){
           location.debugT("Parameters:");
            location.debugT("String productId " + productId);
            location.debugT("String logsys " + logsys);
            location.debugT("String[] csticsNames ");
            if (csticsNames != null) {
                for (int i = 0; i<csticsNames.length; i++){
                    location.debugT("  csticsNames["+i+"] "+csticsNames[i]);
                }
            }
            location.debugT("String[] csticsValues ");
            if (csticsValues != null) {
                for (int i = 0; i<csticsValues.length; i++){
                    location.debugT("  csticsValues["+i+"] "+csticsValues[i]);
                }
            }
        }
        
		HashMap result = new HashMap();
		ArrayList paramsList = new ArrayList();
		paramsList.add(CreateConfig.PRODUCT_ID);
		paramsList.add(productId);
		if(csticsNames != null && csticsNames.length > 0){
			for(int i=0; i<csticsNames.length; i++){
				paramsList.add("csticsNames");
				paramsList.add(csticsNames[i]);	
			}
			for(int i=0; i<csticsValues.length; i++){
				paramsList.add("csticsValues");
				paramsList.add(csticsValues[i]);
			}
		}
		String[] params = new String[paramsList.size()];
		paramsList.toArray(params);
			
		try {
				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//new-->
				startComponentInternalUse("getAllValidGridVariants");
				startComponentInternalUse("doCmd(GetAllValidGridVariants)");
				//<--

				// has to be set to an constant value
				ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd("GetAllValidGridVariants",params);
				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//new-->
				endComponentInternalUse("doCmd(GetAllValidGridVariants)");
				//<--

				String[] variantIds = r.getParameterValues("variantId");
				String[] parentIds = r.getParameterValues("parentId");
				String[] spreadValues = r.getParameterValues("spreadValue");
			    String[][] cNames = null;
				String[][] cValues = null;  
				if (variantIds != null){
					cNames = new String[variantIds.length][3];
					cValues = new String[variantIds.length][3];
	
					for(int i=0; i<variantIds.length; i++){
						cNames[i] = r.getParameterValues("csticName" + "[" + i + "]");
						cValues[i] = r.getParameterValues("csticValue" + "[" + i + "]");
					}
				}

				result.put("variantId", variantIds);
				result.put("parentId", parentIds);
				result.put("spreadValue", spreadValues);
				result.put("csticsNames", cNames);
				result.put("csticsValues", cValues);

				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//new-->
				endComponentInternalUse("getAllValidGridVariants");
    			//<--
                if (location.beDebug()) {
                    if (result!=null){
                        location.debugT("return result " + result.toString());
                    }
                    else location.debugT("return result null");
                }
                location.exiting();
				return result;

			} catch (ClientException e) {
				category.logT(Severity.ERROR, location, e.toString());

				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//new-->
				endComponentInternalUse("getAllValidGridVariants");
				//<--
                if (location.beDebug()) {
                    if (result!=null){
                        location.debugT("return result " + result.toString());
                    }
                    else location.debugT("return result null");
                }
                location.exiting();
				return result;
			}
	}
	
	
public Configuration getConfiguration(IPCConfigReference reference, boolean resetSession) throws IPCException {
	// 20020603-kha: performance tuning: if reference fits to the session I'm currently
	// holding, just reuse this session, instead of reinitializing a new one.
    location.entering("getConfiguration(IPCConfigReference reference, boolean resetSession)");
    if (location.beDebug()){
        location.debugT("Parameters:");
        location.debugT("IPCConfigReference reference " + reference.getConfigId());
        location.debugT("boolean resetSession " + resetSession);
    }
    boolean reuseSession = false;
	if (ipcSession != null && !resetSession) {
		TCPDefaultClient client = (TCPDefaultClient)ipcSession.getClient();
		if (client != null) {
			TCPIPCConfigReference existing = (TCPIPCConfigReference)client.getConfigReference();
			if (    existing.getHost().equals(((TCPIPCConfigReference)reference).getHost()) &&
					existing.getPort().equals(((TCPIPCConfigReference)reference).getPort()) ) {
				reuseSession = true;
			}
		}
	}

	if (!reuseSession)
		ipcSession = factory.newIPCSession(this, reference);

	Configuration config = ipcSession.getConfiguration(reference.getConfigId());
    if (location.beDebug()){
        location.debugT("return config " + config.getId());
    }
    location.exiting();
	return config;
}

public IPCItem getIPCItem(IPCItemReference reference, boolean resetSession) throws IPCException {
	// 20020603-kha: performance tuning: if reference fits to the session I'm currently
	// holding, just reuse this session, instead of reinitializing a new one.
    location.entering("getIPCItem(IPCItemReference reference, boolean resetSession)");
    if (location.beDebug()){
        location.debugT("Parameters:");
        location.debugT("IPCItemReference reference " + reference.getItemId());
        location.debugT("boolean resetSession " + resetSession);
    }
    boolean reuseSession = false;
	if (ipcSession != null && !resetSession) {
		TCPDefaultClient client = (TCPDefaultClient)ipcSession.getClient();
		if (client != null) {
			TCPIPCItemReference existing = (TCPIPCItemReference) client.getItemReference();
			if (    existing.getHost().equals(((TCPIPCItemReference)reference).getHost()) &&
					existing.getPort().equals(((TCPIPCItemReference)reference).getPort()) ) {
				reuseSession = true;
			}
		}
	}

	if (!reuseSession)
		ipcSession = factory.newIPCSession(this, reference);

	IPCDocument document = ipcSession.getDocument(reference.getDocumentId());
	if (document == null){
		throw new IPCException(
					new ClientException(
						"500","Session do not have any document for document id: " + reference.getDocumentId()
					)
				);
	}
	IPCItem item = document.getItem(reference.getItemId());
	if (item == null){
		throw new IPCException(
					new ClientException(
						"500","Document "+ reference.getDocumentId() + " do not have any item for id: " + reference.getItemId()
					)
				);
	}
    if (location.beDebug()){
        location.debugT("return item " + item.getItemId());
    }
    location.exiting();
	return item;
}
public void synchronizeWithServer() throws IPCException {
	try{
		//ipcSession.getClient().sync();
		throw new ClientException("500","TcpDefaultIPCClient.synchronizeWithServer() is not supported by 5.0. Contact Developer.");
	}catch(ClientException e){
		throw new IPCException(e);
	}
}

/* (non-Javadoc)
 * @see com.sap.spc.remote.client.object.IPCClient#getConditionGroupAttributes(java.lang.String, java.lang.String)
 */
public String[] getConditionGroupAttributes(String usage, String conditionGroupName) {
	
    location.entering("getConditionGroupAttributes(String usage, String conditionGroupName)");
    if (location.beDebug()){
        location.debugT("Parameters:");
        location.debugT("String usage " + usage);
        location.debugT("String conditionGroupName " + conditionGroupName);
    }
    String [] condGrpAttrs = null;
	try {
		ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_CONDITION_GROUP_ATTRIBUTES,new String[]{GetConditionGroupAttributes.USAGE,usage, GetConditionGroupAttributes.CONDITION_GROUP_NAME, conditionGroupName});
		condGrpAttrs = r.getParameterValues(GetConditionGroupAttributes.ATTRIBUTE_NAMES);
	} catch (ClientException e) {
		category.logT(Severity.ERROR, location, e.toString());
		throw new IPCException(e);
	}
	if (condGrpAttrs != null) {
        if (location.beDebug()) {
            location.debugT("return condGrpAttrs ");
            for (int i = 0; i<condGrpAttrs.length; i++){
                location.debugT("  condGrpAttrs["+i+"] "+condGrpAttrs[i]);
            }
        }
	}
    location.exiting();
	return condGrpAttrs;
}

private void appendParamArrayToVec(String paramName, String[] inArray,Vector outVector){
	for (int i=0; i<inArray.length; i++) {
		outVector.addElement(paramName);
		outVector.addElement(inArray[i]);
	}
}

/* (non-Javadoc)
 * @see com.sap.spc.remote.client.object.IPCClient#getConditionRecordsFromDatabase(java.lang.String, java.lang.String, java.lang.String[], java.lang.String[], java.lang.String[], java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String)
 */
public HashMap getConditionRecordsFromDatabase(String usage, String application, String[] selAttributesInclArray, String[] selAttributesInclValueArray, String[] selAttributesExclArray, String[] selAttributesExclValueArray, String validityDateStart_yyyyMMddmmhhss, String validityDateEnd_yyyyMMddmmhhss, String conditionGroupId, String numberOfConditionRecords) {
	location.entering("getConditionRecordsFromDatabase(String usage, String application,  "         +"String[] selAttributesInclArray, String[] selAttributesInclValueArray, "
        +"String[] selAttributesExclArray, String[] selAttributesExclValueArray, "
        +"String validityDateStart_yyyyMMddmmhhss, String validityDateEnd_yyyyMMddmmhhss, "
        +"String conditionGroupId, String numberOfConditionRecords");
    if (location.beDebug()){
        location.debugT("String usage " + usage);
        location.debugT("String application " + application);
        if (selAttributesInclArray != null) {
            location.debugT("String[] selAttributesInclArray");
            for (int i = 0; i<selAttributesInclArray.length; i++){
                location.debugT("  selAttributesInclArray["+i+"] "+selAttributesInclArray[i]);
            }
        }
        if (selAttributesInclValueArray != null) {
            location.debugT("String[] selAttributesInclValueArray");
            for (int i = 0; i<selAttributesInclValueArray.length; i++){
                location.debugT("  selAttributesInclValueArray["+i+"] "+selAttributesInclValueArray[i]);
            }
        }
        if (selAttributesExclArray != null) {
            location.debugT("String[] selAttributesExclArray");
            for (int i = 0; i<selAttributesExclArray.length; i++){
                location.debugT("  selAttributesExclArray["+i+"] "+selAttributesExclArray[i]);
            }
        }
        if (selAttributesExclValueArray != null) {
            location.debugT("String[] selAttributesExclValueArray");
            for (int i = 0; i<selAttributesExclValueArray.length; i++){
                location.debugT("  selAttributesExclValueArray["+i+"] "+selAttributesExclValueArray[i]);
            }
        }
        location.debugT("String validityDateStart_yyyyMMddmmhhss " + validityDateStart_yyyyMMddmmhhss);
        location.debugT("String validityDateEnd_yyyyMMddmmhhss " + validityDateEnd_yyyyMMddmmhhss);
        location.debugT("String conditionGroupId " + conditionGroupId);
        location.debugT("String numberOfConditionRecords " + numberOfConditionRecords);
    }
     
    HashMap condRecdsOutMap = new HashMap();

	Vector paraVec = new Vector();
	if (usage != null){
		paraVec.addElement(GetConditionRecordsFromDatabase.USAGE);
		paraVec.addElement(usage);
	}
	if (application != null){
		paraVec.addElement(GetConditionRecordsFromDatabase.APPLICATION);
		paraVec.addElement(application);
	}
	if (validityDateStart_yyyyMMddmmhhss != null){
		paraVec.addElement(GetConditionRecordsFromDatabase.VALIDITY_DATE_START);
		paraVec.addElement(validityDateStart_yyyyMMddmmhhss);
	}
	if (validityDateEnd_yyyyMMddmmhhss != null){
		paraVec.addElement(GetConditionRecordsFromDatabase.VALIDITY_DATE_END);
		paraVec.addElement(validityDateEnd_yyyyMMddmmhhss);
	}
	if (conditionGroupId != null){
		paraVec.addElement(GetConditionRecordsFromDatabase.CONDITION_GROUP_ID);
		paraVec.addElement(conditionGroupId);
	}
	if (numberOfConditionRecords != null){
		paraVec.addElement(GetConditionRecordsFromDatabase.NUMBER_OF_CONDITION_RECORDS);
		paraVec.addElement(numberOfConditionRecords);
	}

	if (selAttributesInclArray != null){
		appendParamArrayToVec(GetConditionRecordsFromDatabase.SEL_ATTRIBUTES_INCL_ARRAY,selAttributesInclArray,paraVec);
	}
	if (selAttributesInclValueArray != null){
		appendParamArrayToVec(GetConditionRecordsFromDatabase.SEL_ATTRIBUTES_INCL_VALUE_ARRAY,selAttributesInclValueArray,paraVec);
	}
	if (selAttributesExclArray != null){
		appendParamArrayToVec(GetConditionRecordsFromDatabase.SEL_ATTRIBUTES_EXCL_ARRAY,selAttributesExclArray,paraVec);
	}
	if (selAttributesExclValueArray != null){
		appendParamArrayToVec(GetConditionRecordsFromDatabase.SEL_ATTRIBUTES_EXCL_VALUE_ARRAY,selAttributesExclValueArray,paraVec);
	}
	
	String[] params = new String[paraVec.size()];
	paraVec.copyInto(params);
	
	try {
		ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_CONDITION_RECORDS_FROM_DATABASE, params);
		condRecdsOutMap.put(ConditionSelectionCommand.NUMBER_OF_RECORD_RETURNED, r.getParameterValue(ConditionSelectionCommand.NUMBER_OF_RECORD_RETURNED));
		condRecdsOutMap.put(ConditionSelectionCommand.CONDITION_RECORD_IDS, r.getParameterValues(ConditionSelectionCommand.CONDITION_RECORD_IDS));
		condRecdsOutMap.put(ConditionSelectionCommand.VALIDITY_ENDS, r.getParameterValues(ConditionSelectionCommand.VALIDITY_ENDS));
		condRecdsOutMap.put(ConditionSelectionCommand.VALIDITY_STARTS, r.getParameterValues(ConditionSelectionCommand.VALIDITY_STARTS));
		condRecdsOutMap.put(ConditionSelectionCommand.OBJECT_IDS, r.getParameterValues(ConditionSelectionCommand.OBJECT_IDS));
		condRecdsOutMap.put(ConditionSelectionCommand.RELEASE_STATUS, r.getParameterValues(ConditionSelectionCommand.RELEASE_STATUS));
		condRecdsOutMap.put(ConditionSelectionCommand.CONDITION_TYPES, r.getParameterValues(ConditionSelectionCommand.CONDITION_TYPES));
		condRecdsOutMap.put(ConditionSelectionCommand.SUPPLEMENTARY_CONDITION_TYPES, r.getParameterValues(ConditionSelectionCommand.SUPPLEMENTARY_CONDITION_TYPES));
		condRecdsOutMap.put(ConditionSelectionCommand.GROUP_IDS, r.getParameterValues(ConditionSelectionCommand.GROUP_IDS));
		condRecdsOutMap.put(ConditionSelectionCommand.MAINTENANCE_STATUS, r.getParameterValues(ConditionSelectionCommand.MAINTENANCE_STATUS));
		condRecdsOutMap.put(ConditionSelectionCommand.SUPPLEMENTARY_CONDITION_IDS, r.getParameterValues(ConditionSelectionCommand.SUPPLEMENTARY_CONDITION_IDS));
		condRecdsOutMap.put(ConditionSelectionCommand.DIMENSION_NUMBERS, r.getParameterValues(ConditionSelectionCommand.DIMENSION_NUMBERS));
		condRecdsOutMap.put(ConditionSelectionCommand.APPLICATION_FIELD_NUMBERS, r.getParameterValues(ConditionSelectionCommand.APPLICATION_FIELD_NUMBERS));
		condRecdsOutMap.put(ConditionSelectionCommand.APPLICATION_FIELD_OFFSETS, r.getParameterValues(ConditionSelectionCommand.APPLICATION_FIELD_OFFSETS));
		condRecdsOutMap.put(ConditionSelectionCommand.APPLICATION_FIELD_NAMES, r.getParameterValues(ConditionSelectionCommand.APPLICATION_FIELD_NAMES)); 
		condRecdsOutMap.put(ConditionSelectionCommand.APPLICATION_FIELD_VALUES, r.getParameterValues(ConditionSelectionCommand.APPLICATION_FIELD_VALUES));
		condRecdsOutMap.put(ConditionSelectionCommand.USAGE_FIELD_NUMBERS, r.getParameterValues(ConditionSelectionCommand.USAGE_FIELD_NUMBERS));
		condRecdsOutMap.put(ConditionSelectionCommand.USAGE_FIELD_OFFSETS, r.getParameterValues(ConditionSelectionCommand.USAGE_FIELD_OFFSETS));
		condRecdsOutMap.put(ConditionSelectionCommand.USAGE_FIELD_NAMES, r.getParameterValues(ConditionSelectionCommand.USAGE_FIELD_NAMES));
		condRecdsOutMap.put(ConditionSelectionCommand.USAGE_FIELD_VALUES, r.getParameterValues(ConditionSelectionCommand.USAGE_FIELD_VALUES));
		condRecdsOutMap.put(ConditionSelectionCommand.SCALE_BASE_TYPES, r.getParameterValues(ConditionSelectionCommand.SCALE_BASE_TYPES));
		condRecdsOutMap.put(ConditionSelectionCommand.SCALE_LINE_NUMBERS, r.getParameterValues(ConditionSelectionCommand.SCALE_LINE_NUMBERS));
		condRecdsOutMap.put(ConditionSelectionCommand.SCALE_LINE_OFFSETS, r.getParameterValues(ConditionSelectionCommand.SCALE_LINE_OFFSETS));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_LINE_REFS, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_LINE_REFS));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_AMOUNTS, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_AMOUNTS));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_RATES, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_RATES));	
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_RATES_CURR_UNIT, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_RATES_CURR_UNIT));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_PRICING_UNIT, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_PRICING_UNIT));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_PRODUCT_UNIT, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_PRODUCT_UNIT));				
		condRecdsOutMap.put(ConditionSelectionCommand.FG_SCALE_LINE_REFS, r.getParameterValues(ConditionSelectionCommand.FG_SCALE_LINE_REFS));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_MIN_QUANTITIES, r.getParameterValues(ConditionSelectionCommand.FG_MIN_QUANTITIES));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_QUANTITY_UNITS, r.getParameterValues(ConditionSelectionCommand.FG_QUANTITY_UNITS));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_QUANTITIES, r.getParameterValues(ConditionSelectionCommand.FG_QUANTITIES));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_ADD_QUANTITIES, r.getParameterValues(ConditionSelectionCommand.FG_ADD_QUANTITIES));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_ADD_QUANTITY_UNITS, r.getParameterValues(ConditionSelectionCommand.FG_ADD_QUANTITY_UNITS));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_ADD_PRODUCTS, r.getParameterValues(ConditionSelectionCommand.FG_ADD_PRODUCTS));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_CALC_TYPES, r.getParameterValues(ConditionSelectionCommand.FG_CALC_TYPES));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_INDICATORS, r.getParameterValues(ConditionSelectionCommand.FG_INDICATORS));
	} catch (ClientException e) {
		category.logT(Severity.ERROR, location, e.toString());
		throw new IPCException(e);
	}
	if (location.beDebug()){
        if (condRecdsOutMap!=null){
            location.debugT("return condRecdsOutMap " + condRecdsOutMap.toString());
        }
        else location.debugT("return condRecdsOutMap null");
	}
    location.exiting();
	return condRecdsOutMap;
}
	//********Implementation for Abstract methods ends here!********
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getConditionTableFieldLabel(java.lang.String, java.lang.String, java.lang.String[])
	 */
	public Hashtable getConditionTableFieldLabel(String usage, String application, String[] fieldNames, String[] dataElementNames, boolean isApplicationField ) {
		
        location.entering("getConditionRecordsFromDatabase(String usage, String application, " 
                          +"String[] fieldNames, String[] dataElementNames, boolean isApplicationField");
        if (location.beDebug()){
            location.debugT("String usage " + usage);
            location.debugT("String application " + application);
            if (fieldNames != null) {
                location.debugT("String[] fieldNames");
                for (int i = 0; i<fieldNames.length; i++){
                    location.debugT("  fieldNames["+i+"] "+fieldNames[i]);
                }
            }
            if (dataElementNames != null) {
                location.debugT("String[] dataElementNames");
                for (int i = 0; i<dataElementNames.length; i++){
                    location.debugT("  dataElementNames["+i+"] "+dataElementNames[i]);
                }
            }
            location.debugT("boolean isApplicationField " + isApplicationField);
        }
        
        String[] resFieldNames = null;
		String[] xShortTexts = null;
		String[] shortTexts = null;
		String[] mediumTexts = null;
		String[] longTexts = null;	
		String[] results = null;
		String[] textContainer = null;
		int paraLength ;  // 2*usage, 2*application, 2*isApplicationField 2*fieldnames.length()		
		Hashtable retVal = new Hashtable();
		boolean workingWithFieldnames = false;
		
		// check if we are using fieldnames or dataelement names
		if (dataElementNames == null 
		    || dataElementNames.length == 0) {
		    workingWithFieldnames = true;	
		    }
		
		// calculate the length of the needed command array
		if ( workingWithFieldnames )  {
			if ( fieldNames != null) {
				paraLength = ( 2 * fieldNames.length ) + 6;  // 2*usage, 2*application, 2*isApplicationfield, 2*fieldnames.length()
			} else {
				paraLength = 6;
			}
		}
		else {
			if (dataElementNames != null ) {
				paraLength = ( 2 * dataElementNames.length ) + 4;  // 2*usage, 2*application, 2*dataelementName.length()
			} else {
				paraLength = 4;
			}				
		}
				
		String[] cmdPara = new String[paraLength];
		
		cmdPara[0] = GetConditionTableFieldLabel.USAGE;
		cmdPara[1] = usage;
		cmdPara[2] = GetConditionTableFieldLabel.APPLICATION;
		cmdPara[3] = application;
		
		if ( workingWithFieldnames){
			// compose the field names for the command
			cmdPara[4] = GetConditionTableFieldLabel.IS_APPLICATION_FIELD;
			if (isApplicationField)
				cmdPara[5] = GetConditionTableFieldLabel.IS_APP_FIELD_TRUE;
			else
				cmdPara[5] = GetConditionTableFieldLabel.IS_APP_FIELD_FALSE;
				
			if ( fieldNames != null ) {
				    // paraLength = ( 2 * fieldNames.length ) + 6;  // 2*usage, 2*application, 2*isApplicationfield, 2*fieldnames.length()			
					for ( int loop = 0; loop < fieldNames.length; loop ++ ){
						cmdPara[6+(loop*2)] = GetConditionTableFieldLabel.FIELDNAME;
						cmdPara[7+(loop*2)] = fieldNames[loop];
				}
			}
		}	
		else {
			// compose the dataelement names for the command	
			if ( dataElementNames != null ) {
					// paraLength = ( 2 * dataElementNames.length ) + 4;  // 2*usage, 2*application, 2*dataElementNames.length()			
					for ( int loop = 0; loop < dataElementNames.length; loop ++ ){
						cmdPara[4+(loop*2)] = GetConditionTableFieldLabel.DATAELEMENTNAME;
						cmdPara[5+(loop*2)] = dataElementNames[loop];
				}
			}			
		}
							
		try {
			// trigger the command
			ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_CONDITION_TABLE_FIELD_LABEL,cmdPara);
			// get the individual result components
			if ( workingWithFieldnames){
				resFieldNames = r.getParameterValues(GetConditionTableFieldLabel.FIELDNAME);
			}
			else {
				resFieldNames = r.getParameterValues(GetConditionTableFieldLabel.DATAELEMENTNAME);							
			}
			xShortTexts = r.getParameterValues(GetConditionTableFieldLabel.EXTRA_SHORT_TEXT);
			shortTexts  = r.getParameterValues(GetConditionTableFieldLabel.SHORT_TEXT);
			mediumTexts = r.getParameterValues(GetConditionTableFieldLabel.MEDIUM_TEXT);
			longTexts   = r.getParameterValues(GetConditionTableFieldLabel.LONG_TEXT);
			
			// create the result structures and add them to the hashtable
			if (   resFieldNames != null 
			    && resFieldNames.length > 0 
			    && xShortTexts != null 
			    && xShortTexts.length > 0  ) {
			    
				for ( int loop = 0; loop < resFieldNames.length; loop ++ ){
					textContainer = new String[4];
					textContainer[0] = xShortTexts[loop];
					textContainer[1] = shortTexts[loop];
					textContainer[2] = mediumTexts[loop];
					textContainer[3] = longTexts[loop];
					// add only keys which are not null
					if (resFieldNames[loop] != null) {			
						// use the field name as key in the hash map		
						retVal.put(resFieldNames[loop], textContainer);
					}
				}
			}
			
		} catch (ClientException e) {
			category.logT(Severity.ERROR, location, e.toString());
			throw new IPCException(e);
		}
	    if (location.beDebug()) {
            if (retVal!=null){
                location.debugT("return retVal " + retVal.toString());
            }
            else location.debugT("return retVal null");
	    }
        location.exiting();
		return retVal;
	}

public HashMap convertExternalToInternal(String[] names, String[] externalValues){
    location.entering("convertExternalToInternal(String[] names, String[] externalValues");
    if (location.beDebug()){
        if (names != null) {
            location.debugT("String[] names");
            for (int i = 0; i<names.length; i++){
                location.debugT("  names["+i+"] "+names[i]);
            }
        }
        if (externalValues != null) {
            location.debugT("String[] externalValues");
            for (int i = 0; i<externalValues.length; i++){
                location.debugT("  externalValues["+i+"] "+externalValues[i]);
            }
        }
    }
	HashMap outputParams = new HashMap(3);
	String[] namesOutput = null;
	String[] internalValues = null;
	String[] errorMsgs = null;

	Vector paramVec = new Vector(0);
	if (names != null && externalValues != null){
		appendParamArrayToVec(ConvertExternalToInternal.NAMES_INPUT,names,paramVec);
		appendParamArrayToVec(ConvertExternalToInternal.EXTERNAL_VALUES,externalValues,paramVec);
	}
		
	String[] cmdPara = new String[paramVec.size()];
	paramVec.copyInto(cmdPara);
		
	try {
		// trigger the command
		ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.CONVERT_EXTERNAL_TO_INTERNAL,cmdPara);
		// get the individual result components
		namesOutput  = r.getParameterValues(ConvertExternalToInternal.NAMES_OUTPUT);
		internalValues  = r.getParameterValues(ConvertExternalToInternal.INTERNAL_VALUES);
		errorMsgs  = r.getParameterValues(ConvertExternalToInternal.ERROR_MESSAGES);
			
		outputParams.put(ConvertExternalToInternal.NAMES_OUTPUT,namesOutput);
		outputParams.put(ConvertExternalToInternal.INTERNAL_VALUES,internalValues);
		outputParams.put(ConvertExternalToInternal.ERROR_MESSAGES,errorMsgs);
			
	} catch (ClientException e) {
		category.logT(Severity.ERROR, location, e.toString());
		throw new IPCException(e);
	}
    if (location.beDebug()) {
        if (outputParams!=null){
            location.debugT("return outputParams " + outputParams.toString());
        }
        else location.debugT("return outputParams null");
    }
    location.exiting();	
	return outputParams;
		
}
public HashMap convertInternalToExternal(String[] names, String[] internalValues){
    location.entering("convertInternalToExternal(String[] names, String[] internalValues");
    if (location.beDebug()){
        if (names != null) {
            location.debugT("String[] names");
            for (int i = 0; i<names.length; i++){
                location.debugT("  names["+i+"] "+names[i]);
            }
        }
        if (internalValues != null) {
            location.debugT("String[] externalValues");
            for (int i = 0; i<internalValues.length; i++){
                location.debugT("  internalValues["+i+"] "+internalValues[i]);
            }
        }
    }
	HashMap outputParams = new HashMap(3);
	String[] externalValues = null;
	String[] errorMsgs = null;

	Vector paramVec = new Vector(0);
	if (names != null && internalValues != null){
		appendParamArrayToVec(ConvertInternalToExternal.NAMES,names,paramVec);
		appendParamArrayToVec(ConvertInternalToExternal.INTERNAL_VALUES,internalValues,paramVec);
	}
		
	String[] cmdPara = new String[paramVec.size()];
	paramVec.copyInto(cmdPara);
		
	try {
		// trigger the command
		ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.CONVERT_INTERNAL_TO_EXTERNAL,cmdPara);
		// get the individual result components
		externalValues  = r.getParameterValues(ConvertInternalToExternal.EXTERNAL_VALUES);
		errorMsgs  = r.getParameterValues(ConvertInternalToExternal.ERROR_MESSAGES);
			
		outputParams.put(ConvertInternalToExternal.EXTERNAL_VALUES,externalValues);
		outputParams.put(ConvertInternalToExternal.ERROR_MESSAGES,errorMsgs);
			
	} catch (ClientException e) {
		category.logT(Severity.ERROR, location, e.toString());
		throw new IPCException(e);
	}
    if (location.beDebug()) {
        if (outputParams!=null){
            location.debugT("return outputParams " + outputParams.toString());
        }
        else location.debugT("return outputParams null");
    }
    location.exiting();	
	return outputParams;
		
}


   public Hashtable convertFieldnameInternalToExternal(String usage, String application, String[] intFieldNames, boolean isApplicationField )throws IPCException 
   {	
    location.entering("convertFieldnameInternalToExternal(String usage, String application, String[] intFieldNames, boolean isApplicationField)");
    if (location.beDebug()){
       location.debugT("String usage " + usage);
       location.debugT("String application " + application);
       if (intFieldNames != null) {
           location.debugT("String[] intFieldNames");
           for (int i = 0; i<intFieldNames.length; i++){
               location.debugT("  intFieldNames["+i+"] "+intFieldNames[i]);
           }
       }
       location.debugT("boolean isApplicationField " + isApplicationField);
    }
	String[] resFieldNames = null;
	String[] resConcatinatedExtFieldNames = null;
    StringTokenizer sTok = null;
    int numberOfExtFieldnames = 0;
    int arrayPointer = 0;
	String[] resExtFieldNames = null;

	int paraLength = 0 ;  // 2*usage, 2*application, 2*fieldnames.length()		
	Hashtable retVal = new Hashtable();
		
    // first check if all needed parameters are here
    if ( usage == null ||
         application == null ||
         intFieldNames == null ||
         intFieldNames.length <= 0 ){
         // mandatory parameter is missing, stop processing right now
         String emsg = "Mandatory parameter is missing for command 'convertFieldnameInternalToExternal'";
         throw ( new IPCException(emsg) );  	
    }

	// calculate the length of the needed command array 
	paraLength = ( 2 * intFieldNames.length ) + 6;  // 2*usage, 2*application, 2*isApplicationField, 2*fieldnames.length()     				
	String[] cmdPara = new String[paraLength];
		
	cmdPara[0] = ConvertFieldnameInternalToExternal.USAGE;
	cmdPara[1] = usage;
	cmdPara[2] = ConvertFieldnameInternalToExternal.APPLICATION;
	cmdPara[3] = application;		
	cmdPara[4] = ConvertFieldnameInternalToExternal.IS_APPLICATION_FIELD;
	if ( isApplicationField ) 
		cmdPara[5] = ConvertFieldnameInternalToExternal.IS_APP_FIELD_TRUE;
	else
		cmdPara[5] = ConvertFieldnameInternalToExternal.IS_APP_FIELD_FALSE;
	
	for ( int loop = 0; loop < intFieldNames.length; loop ++ ){
		cmdPara[6+(loop*2)] = ConvertFieldnameInternalToExternal.INT_FIELDNAME;
		cmdPara[7+(loop*2)] = intFieldNames[loop];
	}
							
	try {
		// trigger the command
		ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.CONVERT_FIELDNAMES_INTERNAL_TO_EXTERNAL,cmdPara);
		// get the individual result components
		resFieldNames = r.getParameterValues(ConvertFieldnameInternalToExternal.INT_FIELDNAME);
		resConcatinatedExtFieldNames = r.getParameterValues(ConvertFieldnameInternalToExternal.EXT_FIELDNAME);

			
		// create the result structures and add them to the hashtable
		if (   resFieldNames != null 
			&& resFieldNames.length > 0 
			&& resConcatinatedExtFieldNames != null 
			&& resConcatinatedExtFieldNames.length > 0  ) {
			    
  
			for ( int loop = 0; loop < resFieldNames.length; loop ++ ){
				sTok = new StringTokenizer(resConcatinatedExtFieldNames[loop], ConvertFieldnameInternalToExternal.SEPARATOR_EXT_FIELD);
				numberOfExtFieldnames = sTok.countTokens();
				resExtFieldNames = new String[numberOfExtFieldnames];
				arrayPointer = 0;
				if ( numberOfExtFieldnames != 0){
					// there is at least one external field name
					while (sTok.hasMoreElements()){
					   resExtFieldNames[arrayPointer] = sTok.nextToken();	 
					   arrayPointer ++; 
					}			
				}
				else {
					// there is no external name for the field
					// that means the external and the internal name is identical
					// use the internal name to fill the value for external name
					resExtFieldNames = new String[1];					
					resExtFieldNames[0] = resFieldNames[loop];
				}

				// add only keys which are not null
				if (resFieldNames[loop] != null) {			
					// use the field name as key in the hash map		
					retVal.put(resFieldNames[loop], resExtFieldNames);
				}
			}
		}
			
	} catch (ClientException e) {
		category.logT(Severity.ERROR, location, e.toString());
		throw new IPCException(e);
	}
    if (location.beDebug()) {
        if (retVal!=null){
            location.debugT("return retVal " + retVal.toString());
        }
        else location.debugT("return retVal null");
    }
    location.exiting(); 
	return retVal;
	
   }

/* (non-Javadoc)
 * @see com.sap.spc.remote.client.object.IPCClient#getConditionRecordsFromRefGuid(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
 */
public HashMap getConditionRecordsFromRefGuid(String usage, String application, String refGuid, String refType, String numberOfConditionRecords) throws IPCException {
	
    location.entering("getConditionRecordsFromRefGuid(String usage, String application, String refGuid, String refType, String numberOfConditionRecords)");
    if (location.beDebug()){
        location.debugT("String usage " + usage);
        location.debugT("String application " + application);
        location.debugT("String refGuid " + refGuid);
        location.debugT("String refType " + refType);
        location.debugT("String numberOfConditionRecords " + numberOfConditionRecords);
    }
    HashMap condRecdsOutMap = new HashMap();

	Vector paraVec = new Vector();
	if (usage != null){
		paraVec.addElement(GetConditionRecordsFromRefGuid.USAGE);
		paraVec.addElement(usage);
	}
	if (application != null){
		paraVec.addElement(GetConditionRecordsFromRefGuid.APPLICATION);
		paraVec.addElement(application);
	}
	if (refGuid != null){
		paraVec.addElement(GetConditionRecordsFromRefGuid.REF_GUID);
		paraVec.addElement(refGuid);
	}
	if (refType != null){
		paraVec.addElement(GetConditionRecordsFromRefGuid.REF_TYPE);
		paraVec.addElement(refType);
	}
	if (numberOfConditionRecords != null){
		paraVec.addElement(GetConditionRecordsFromRefGuid.NUMBER_OF_CONDITION_RECORDS);
		paraVec.addElement(numberOfConditionRecords);
	}

	String[] params = new String[paraVec.size()];
	paraVec.copyInto(params);
	
	try {
		ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_CONDITION_RECORDS_FROM_REF_GUID, params);
		condRecdsOutMap.put(ConditionSelectionCommand.NUMBER_OF_RECORD_RETURNED, r.getParameterValue(ConditionSelectionCommand.NUMBER_OF_RECORD_RETURNED));
		condRecdsOutMap.put(ConditionSelectionCommand.CONDITION_RECORD_IDS, r.getParameterValues(ConditionSelectionCommand.CONDITION_RECORD_IDS));
		condRecdsOutMap.put(ConditionSelectionCommand.VALIDITY_ENDS, r.getParameterValues(ConditionSelectionCommand.VALIDITY_ENDS));
		condRecdsOutMap.put(ConditionSelectionCommand.VALIDITY_STARTS, r.getParameterValues(ConditionSelectionCommand.VALIDITY_STARTS));
		condRecdsOutMap.put(ConditionSelectionCommand.OBJECT_IDS, r.getParameterValues(ConditionSelectionCommand.OBJECT_IDS));
		condRecdsOutMap.put(ConditionSelectionCommand.RELEASE_STATUS, r.getParameterValues(ConditionSelectionCommand.RELEASE_STATUS));
		condRecdsOutMap.put(ConditionSelectionCommand.CONDITION_TYPES, r.getParameterValues(ConditionSelectionCommand.CONDITION_TYPES));
		condRecdsOutMap.put(ConditionSelectionCommand.SUPPLEMENTARY_CONDITION_TYPES, r.getParameterValues(ConditionSelectionCommand.SUPPLEMENTARY_CONDITION_TYPES));
		condRecdsOutMap.put(ConditionSelectionCommand.GROUP_IDS, r.getParameterValues(ConditionSelectionCommand.GROUP_IDS));
		condRecdsOutMap.put(ConditionSelectionCommand.MAINTENANCE_STATUS, r.getParameterValues(ConditionSelectionCommand.MAINTENANCE_STATUS));
		condRecdsOutMap.put(ConditionSelectionCommand.SUPPLEMENTARY_CONDITION_IDS, r.getParameterValues(ConditionSelectionCommand.SUPPLEMENTARY_CONDITION_IDS));
		condRecdsOutMap.put(ConditionSelectionCommand.DIMENSION_NUMBERS, r.getParameterValues(ConditionSelectionCommand.DIMENSION_NUMBERS));
		condRecdsOutMap.put(ConditionSelectionCommand.APPLICATION_FIELD_NUMBERS, r.getParameterValues(ConditionSelectionCommand.APPLICATION_FIELD_NUMBERS));
		condRecdsOutMap.put(ConditionSelectionCommand.APPLICATION_FIELD_OFFSETS, r.getParameterValues(ConditionSelectionCommand.APPLICATION_FIELD_OFFSETS));
		condRecdsOutMap.put(ConditionSelectionCommand.APPLICATION_FIELD_NAMES, r.getParameterValues(ConditionSelectionCommand.APPLICATION_FIELD_NAMES));
		condRecdsOutMap.put(ConditionSelectionCommand.APPLICATION_FIELD_VALUES, r.getParameterValues(ConditionSelectionCommand.APPLICATION_FIELD_VALUES));
		condRecdsOutMap.put(ConditionSelectionCommand.USAGE_FIELD_NUMBERS, r.getParameterValues(ConditionSelectionCommand.USAGE_FIELD_NUMBERS));
		condRecdsOutMap.put(ConditionSelectionCommand.USAGE_FIELD_OFFSETS, r.getParameterValues(ConditionSelectionCommand.USAGE_FIELD_OFFSETS));
		condRecdsOutMap.put(ConditionSelectionCommand.USAGE_FIELD_NAMES, r.getParameterValues(ConditionSelectionCommand.USAGE_FIELD_NAMES));
		condRecdsOutMap.put(ConditionSelectionCommand.USAGE_FIELD_VALUES, r.getParameterValues(ConditionSelectionCommand.USAGE_FIELD_VALUES));
		condRecdsOutMap.put(ConditionSelectionCommand.SCALE_BASE_TYPES, r.getParameterValues(ConditionSelectionCommand.SCALE_BASE_TYPES));
		condRecdsOutMap.put(ConditionSelectionCommand.SCALE_LINE_NUMBERS, r.getParameterValues(ConditionSelectionCommand.SCALE_LINE_NUMBERS));
		condRecdsOutMap.put(ConditionSelectionCommand.SCALE_LINE_OFFSETS, r.getParameterValues(ConditionSelectionCommand.SCALE_LINE_OFFSETS));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_LINE_REFS, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_LINE_REFS));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_AMOUNTS, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_AMOUNTS));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_RATES, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_RATES));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_RATES_CURR_UNIT, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_RATES_CURR_UNIT));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_PRICING_UNIT, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_PRICING_UNIT));
		condRecdsOutMap.put(ConditionSelectionCommand.PR_SCALE_PRODUCT_UNIT, r.getParameterValues(ConditionSelectionCommand.PR_SCALE_PRODUCT_UNIT));			
		condRecdsOutMap.put(ConditionSelectionCommand.FG_SCALE_LINE_REFS, r.getParameterValues(ConditionSelectionCommand.FG_SCALE_LINE_REFS));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_MIN_QUANTITIES, r.getParameterValues(ConditionSelectionCommand.FG_MIN_QUANTITIES));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_QUANTITY_UNITS, r.getParameterValues(ConditionSelectionCommand.FG_QUANTITY_UNITS));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_QUANTITIES, r.getParameterValues(ConditionSelectionCommand.FG_QUANTITIES));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_ADD_QUANTITIES, r.getParameterValues(ConditionSelectionCommand.FG_ADD_QUANTITIES));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_ADD_QUANTITY_UNITS, r.getParameterValues(ConditionSelectionCommand.FG_ADD_QUANTITY_UNITS));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_ADD_PRODUCTS, r.getParameterValues(ConditionSelectionCommand.FG_ADD_PRODUCTS));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_CALC_TYPES, r.getParameterValues(ConditionSelectionCommand.FG_CALC_TYPES));
		condRecdsOutMap.put(ConditionSelectionCommand.FG_INDICATORS, r.getParameterValues(ConditionSelectionCommand.FG_INDICATORS));
	} catch (ClientException e) {
		category.logT(Severity.ERROR, location, e.toString());
		throw new IPCException(e);
	}
    if (location.beDebug()) {
        if (condRecdsOutMap!=null){
            location.debugT("return condRecdsOutMap " + condRecdsOutMap.toString());
        }
        else location.debugT("return condRecdsOutMap null");

    }
    location.exiting(); 
	return condRecdsOutMap;
}

	public int getNumberOfConditionRecordSelected(String usage) throws IPCException {
		
        location.entering("getNumberOfConditionRecordSelected(String usage)");
        if (location.beDebug()){
            location.debugT("String usage " + usage);
        }
        int numOfRecds = 0;
		
		String[] cmdPara = new String[2]; 
		if (usage != null){
			cmdPara[0] = new String(GetNumberOfConditionRecordSelected.USAGE);
			cmdPara[1] = usage;
		
		}
		
		try {
			// trigger the command
			ServerResponse r = ((TCPDefaultClient)ipcSession.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_NUMBER_OF_CONDITION_RECORD_SELECTED,cmdPara);
			// get the individual result components
			 String[] numOfRecd = r.getParameterValues(GetNumberOfConditionRecordSelected.NUMBER_OF_CONDITION_RECORDS);
			if (numOfRecd != null) {
				numOfRecds = new Integer(numOfRecd[0]).intValue();
			}
			
		} catch (ClientException e) {
			category.logT(Severity.ERROR, location, e.toString());
			throw new IPCException(e);
		}	
        if (location.beDebug()) {
            location.debugT("return numOfRecds " + numOfRecds);
        }
        location.exiting(); 	
		return numOfRecds;	
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCClient#getUIModelGUID(com.sap.spc.remote.client.object.KnowledgeBase, java.lang.String, java.lang.String)
     */
    public String getUIModelGUID(
        KnowledgeBase kb,
        String scenario,
        String roleKey) {
		// Not implemented (was introduced for RFC side. UI models are not available in MSA. 
		return "";

    }

}
