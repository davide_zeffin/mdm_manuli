package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.ProductKey;

public class DefaultProductKey implements ProductKey {

    protected String productId;
    protected String productType;
    protected String productLogSys;

    DefaultProductKey() {
    }

    DefaultProductKey(String productId, String productType, String productLogSys) {
        this.productId = productId;
        this.productType = productType;
        this.productLogSys = productLogSys;
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ProductKey#getProductId()
     */
    public String getProductId() {
        
        return productId;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ProductKey#getProductType()
     */
    public String getProductType() {
        return productType;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ProductKey#getProductLogSys()
     */
    public String getProductLogSys() {
        return productLogSys;
    }

}
