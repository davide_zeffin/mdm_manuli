/*
 * Created on Apr 18, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.DynamicReturn;

/**
 * @author d028489
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DefaultDynamicReturn implements DynamicReturn {
	String _key, _sequence, _attribute;
	
	public DefaultDynamicReturn (String key, String sequence, String attribute) {
		_key = key;
		_sequence = sequence;
		_attribute = attribute;
		
	}
	public String getKey() {
		return _key;
	}
	
	public String getSequence() {
		return _sequence;
	}
	
	public String getAttribute() {
		return _attribute;
	}

}
