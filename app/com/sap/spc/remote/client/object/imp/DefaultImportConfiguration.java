package com.sap.spc.remote.client.object.imp;

import java.util.HashMap;
import java.util.List;

import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.ImportConfiguration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.KBProfile;
import com.sap.spc.remote.client.object.LoadingStatus;

public class DefaultImportConfiguration implements ImportConfiguration {
    
    String warningProtocol;
	protected Configuration config;  
    protected IPCClientObjectFactory factory = IPCClientObjectFactory.getInstance();
    
    /**
     * @param DefaultIPCSesssion
     * @param configId
     */
    public DefaultImportConfiguration(DefaultIPCSession session, String configId) {
        //config = factory.newImportConfiguration(session, configId);
		config = factory.newConfiguration(session, configId);
    }

    /**
     * @param ipcClient
     * @param id
     */
    DefaultImportConfiguration(IPCClient ipcClient, String id) {
    	//The below line has been commented because this was causing a recursive error
    	//It has to create a configuration 
        //config = factory.newImportConfiguration(ipcClient, id);
        config=factory.newConfiguration(ipcClient,id);
        warningProtocol = "";
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ImportConfiguration#getWarningProtocol()
     */
    public String getWarningProtocol() {
        return warningProtocol;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ImportConfiguration#setWarningProtocol(java.lang.String)
     */
    public void setWarningProtocol(String protocol) {
        this.warningProtocol = protocol;
    }

    public void check() {
        config.check();
    }

    public void close() {
        config.close();
    }

    public void decompose() {
        config.decompose();
    }

    /**
     * @param Instance to be deleted
     * @return true only if the instance was successfully deleted
     */
    public boolean deleteInstance(Instance instance) {
        return config.deleteInstance(instance);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return config.equals(obj);
    }

    public void flush() {
        config.flush();
    }

    /**
     * @return the knowledge base profile used for this configuration
     */
    public KBProfile getActiveKBProfile() {
        return config.getActiveKBProfile();
    }

    /**
     * @return a timestamp (System.currentTimeMillis) of when this configuration was last changed.
     */
    public long getChangeTime() {
        return config.getChangeTime();
    }

    /**
     * @return explanations for the occured conflicts 
     */
    public List getConflictInfos() {
        return config.getConflictInfos();
    }

    /**
     * @return the names of the configuration context
     */
    public String[] getContextNames() {
        return config.getContextNames();
    }

    /**
     * @return the values of the configuration context
     */
    public String[] getContextValues() {
        return config.getContextValues();
    }

    /**
     * @return the explanation tool
     */
    public ExplanationTool getExplanationTool() {
        return config.getExplanationTool();
    }

    /**
     * @return an external representation of this config in a HashMap
     * @throws IPCException
     */
    public HashMap getExternalConfiguration() throws IPCException {
        return config.getExternalConfiguration();
    }

    /**
     * @return an external representation of this config in a HashMap
     * @throws IPCException
     */
    public HashMap getExternalConfigurationStandalone() throws IPCException {
        return config.getExternalConfigurationStandalone();
    }

    /**
     * @return the config id of this configuration.
     */
    public String getId() {
        return config.getId();
    }

    /**
     * @param instId the id of the instance
     * @return the instance object
     */
    public Instance getInstance(String instId) {
        return config.getInstance(instId);
    }

    /**
     * @return an array of Instances, sorted in depth-first preorder (root,
     * first child of root, first child of this child, ...)
     */
    public List getInstances() {
        return config.getInstances();
    }

    /**
     * @return knowledge base information about this configuration
     */
    public KnowledgeBase getKnowledgeBase() {
        return config.getKnowledgeBase();
    }

    /**
     * @return all non-part instances of this configuration
     */
    public List getNonPartInstances() {
        return config.getNonPartInstances();
    }

    /**
     * @return the root instance of this configuration. There is always
     * a root instance - this method never returns null.
     */
    public Instance getRootInstance() {
        return config.getRootInstance();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return config.hashCode();
    }

    /**
     * @return true if this configuration is available on the server,
     * and false if the configuration id is unknown or the configuration
     * couldn't be loaded by the configuration engine.
     */
    public boolean isAvailable() {
        return config.isAvailable();
    }

    /**
     * @return
     */
    public boolean isClosed() {
        return config.isClosed();
    }

    /**
     * @return true, if this config is complete.
     * @throws IPCException
     */
    public boolean isComplete() throws IPCException {
        return config.isComplete();
    }

    /**
     * @return true, if this config is consistent.
     * @throws IPCException
     */
    public boolean isConsistent() throws IPCException {
        return config.isConsistent();
    }

    /**
     * @return true, if the configuration is in initial state
     * @throws IPCException
     */
    public boolean isInitial() throws IPCException {
        return config.isInitial();
    }

    /**
     * @param commandName
     * @return
     */
    public boolean isRelevant(String commandName) {
        return config.isRelevant(commandName);
    }

    /**
     * @return true if this configuration uses SCE advanced mode
     * @throws IPCException
     */
    public boolean isSceMode() throws IPCException {
        return config.isSceMode();
    }

    /**
     * @return true, if this config is a single level configuration.
     * @throws IPCException
     */
    public boolean isSingleLevel() throws IPCException {
        return config.isSingleLevel();
    }

    /**
     * @throws IPCException
     */
    public void reset() throws IPCException {
        config.reset();
    }

    /**
     * 
     */
    public void setChangeTime() {
        config.setChangeTime();
    }

    /**
     * @return
     */
    public ext_configuration toExternal() {
        return config.toExternal();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return config.toString();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getInstanceNames()
     */
    public String[] getInstanceNames() {
        return config.getInstanceNames();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getLoadingStatus()
     */
    public LoadingStatus getLoadingStatus() {
        return config.getLoadingStatus();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#compareConfigurations(com.sap.spc.remote.client.util.ext_configuration, com.sap.spc.remote.client.util.ext_configuration)
     */
    public ComparisonResult compareConfigurations(
        ext_configuration extConfig1,
        ext_configuration extConfig2) {
        return config.compareConfigurations(extConfig1, extConfig2);
    }

    public void setContextNames(String[] contextNames){
        config.setContextNames(contextNames);
    }
    
    public void setContextValues(String[] contextValues){
        config.setContextValues(contextValues); 
    }

	public boolean isCacheDirty() {
		return config.isCacheDirty();
	}

	public boolean isInstanceCacheDirty() {
		return config.isInstanceCacheDirty();
	}

	public void setCacheDirty(boolean b) {
		config.setCacheDirty(b);
	}

	public void setInstanceCacheDirty(boolean b) {
		config.setInstanceCacheDirty(b);
	}

}
