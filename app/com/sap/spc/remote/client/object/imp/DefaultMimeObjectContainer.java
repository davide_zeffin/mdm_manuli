/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object.imp;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.MimeObjectContainer;

public class DefaultMimeObjectContainer implements MimeObjectContainer {

    protected Hashtable mimeObjectsByType;
    protected Vector allMimeObjects;
    
    /**
     * default constructor
     */
    DefaultMimeObjectContainer() {
        mimeObjectsByType = new Hashtable();
        allMimeObjects = new Vector();
    }
    
    /**
     * use this constructor, if you already know the numer of mime objects
     */
    DefaultMimeObjectContainer(int initialSize) {
        mimeObjectsByType = new Hashtable(initialSize);
        allMimeObjects = new Vector(initialSize);
    }
    
    /**
     * This method enables you to add one MimeType
     */
    public void add(MimeObject mimeObject) {
        allMimeObjects.addElement(mimeObject);
        // Try to receive the vector of the given type
        Vector typeObjects = (Vector) mimeObjectsByType.get(mimeObject.getType().toLowerCase());
        if (typeObjects == null) {
            typeObjects = new Vector();
            mimeObjectsByType.put(mimeObject.getType().toLowerCase(), typeObjects);
        }
        typeObjects.addElement(mimeObject);
    }
    
    /**
     * Use this method to retreive all mime objects in this container
     */
    public List getMimeObjects() {
        return allMimeObjects;
    }
    
    /**
     * Use this method to retreive all mime objects of this type
     */
    public List getMimeObjectsByType(String type) {
        List mimeObjects = (List) mimeObjectsByType.get(type.toLowerCase());
        return (mimeObjects == null) ? new Vector() : mimeObjects;
    }
    
    /**
     * Use this method to retreive one mime objects of this type and the
     * given index (starts with 0)
     */
    public MimeObject getMimeObjectByType(String type, int index) {
        Vector mimeObjects = (Vector) mimeObjectsByType.get(type.toLowerCase());
        if (mimeObjects == null) {
            return null;
        }
        if (index > mimeObjects.size() - 1) {
            return null;
        }
        return (MimeObject) mimeObjects.elementAt(index);
    }
    
    /**
     * @return true, if this container contains no mime objects
     */
    public boolean isEmpty() {
        return allMimeObjects.size() == 0;
    }
}
