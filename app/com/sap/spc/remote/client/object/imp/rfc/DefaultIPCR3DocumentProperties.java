package com.sap.spc.remote.client.object.imp.rfc;

import java.util.ArrayList;

import com.sap.spc.remote.client.object.imp.DefaultIPCDocumentProperties;


/**
 * Title:
 * Description: R/3 and CRM use different parameter names.
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

public class DefaultIPCR3DocumentProperties extends DefaultIPCDocumentProperties {
	public static final String SALES_ORGANIZATION = "VKORG";
	public static final String DISTRIBUTION_CHANNEL = "VTWEG";
	public static final String DIVISION = "SPART";
	public static final String REFERENCE_CURRENCY = "HWAER"; //???? not found in pricingConstants.xml
	public static final String CURRENCY = "WAERK";
	public static final String TAX_DEPARTURE_COUNTRY = "ALAND";
	public static final String TAX_DEPARTURE_REGION = "";

	/**
	 * Additional parameters for CreateItem(s). m_commandParameterNames/Values are initially null.
	 * After they are defined they always have the same number of elements; the i-th member of both
	 * corresponds.
	 */
	protected ArrayList      m_commandParameterNames;
	protected ArrayList      m_commandParameterValues;

    public DefaultIPCR3DocumentProperties() {
		m_salesOrganisation     = "";
		m_distributionChannel   = "";
		m_division              = "";
		m_language              = "";
		m_documentCurrency      = "";
		m_localCurrency         = "";
		m_DepartureCountry      = "";
		m_country               = "";
		m_pricingProcedure      = "";
		m_DepartureRegion       = "";
		m_headerAttributeNames  = new ArrayList();
		m_headerAttributeValues = new ArrayList();
		m_externalId            = "";
		m_editMode              = ' ';
			
		m_decimalSeparator      = null;
		m_groupingSeparator     = null;
		m_groupConditionProcessing = true;
        m_stripDecimalPlaces = true;
    }

    public String[] getAllHeaderAttributeNames(){
        return (new String[] {
            SALES_ORGANIZATION, DISTRIBUTION_CHANNEL, DIVISION, REFERENCE_CURRENCY,
			CURRENCY, TAX_DEPARTURE_COUNTRY, TAX_DEPARTURE_REGION
        });
    }

	public void setAttribute(String attrName,String attrValue){
		if(attrName.equalsIgnoreCase(SALES_ORGANIZATION)) setSalesOrganisation(attrValue);
		else
		if(attrName.equalsIgnoreCase(DISTRIBUTION_CHANNEL))setDistributionChannel(attrValue);
		else
		if(attrName.equalsIgnoreCase(DIVISION))setDivision(attrValue);
		else
		if(attrName.equalsIgnoreCase(REFERENCE_CURRENCY))setLocalCurrency(attrValue);
		else
		if(attrName.equalsIgnoreCase(CURRENCY))setDocumentCurrency(attrValue);
		else
		if(attrName.equalsIgnoreCase(TAX_DEPARTURE_COUNTRY))setDepartureCountry(attrValue);
		else
		if(attrName.equalsIgnoreCase(TAX_DEPARTURE_REGION))setDepartureRegion(attrValue);
	}
	/**
	 * Adds a parameter=name setting to these properties that will be passed without any interpretation to
	 * the IPC command that creates the sales document. This method can be used to use sophisticated features
	 * of the pricing engine that are not included in this API because they would make it impossible to
	 * implement it differently.
	 */
	public void addCreationCommandParameter(String name, String value) {
		if (m_commandParameterNames == null) {
			m_commandParameterNames = new ArrayList();
			m_commandParameterValues = new ArrayList();
		}
		m_commandParameterNames.add(name);
		m_commandParameterValues.add(value);
	}
	

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setSalesOrganisation(java.lang.String)
     */
    public void setSalesOrganisation(String salesOrganisation) {
        //m_salesOrganisation.setInitialValue(salesOrganisation);
        m_salesOrganisation = salesOrganisation;
        m_headerAttributeNames.add(SALES_ORGANIZATION);
        m_headerAttributeValues.add(salesOrganisation);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDepartureCountry(java.lang.String)
     */
    public void setDepartureCountry(String departureCountry) {
        m_DepartureCountry = departureCountry;
        m_headerAttributeNames.add(TAX_DEPARTURE_COUNTRY);
        m_headerAttributeValues.add(departureCountry);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDepartureRegion(java.lang.String)
     */
    public void setDepartureRegion(String departureRegion) {
        m_DepartureRegion = departureRegion;
        m_headerAttributeNames.add(TAX_DEPARTURE_REGION);
        m_headerAttributeValues.add(departureRegion);

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDistributionChannel(java.lang.String)
     */
    public void setDistributionChannel(String distributionChannel) {
        m_distributionChannel = distributionChannel;
        m_headerAttributeNames.add(DISTRIBUTION_CHANNEL);
        m_headerAttributeValues.add(distributionChannel);

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDivision(java.lang.String)
     */
    public void setDivision(String division) {
        m_division = division;
        m_headerAttributeNames.add(DIVISION);
        m_headerAttributeValues.add(division);

    }


}
