package com.sap.spc.remote.client.object.imp.rfc;

import java.util.Locale;

import org.apache.struts.util.MessageResources;

import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ConflictParticipant;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.DefaultConflictParticipant;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.rfc.function.CfgApiDeleteAssumption;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Severity;


public class RfcDefaultConflictParticipant
    extends DefaultConflictParticipant
    implements ConflictParticipant {

 protected RfcDefaultConflictParticipant(
        IPCClient ipcClient,
        Conflict conflict,
        String id,
        String factKey,
        boolean causedByUser,
        boolean causedByDefault,
        Locale locale,
        MessageResources resources) {
        this.ipcClient = ipcClient;
        this.conflict = conflict;
        this.id = id;
        this.causedByUser = causedByUser;
        this.causedByDefault = causedByDefault;
        this.factKey = factKey;
        this.keyAdjusted = false;
        this.text = adjustText(factKey, locale, resources);
        this.textAdjusted = true;
        this.closed = false;
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ConflictParticipant#remove()
     */
    public void remove() {
        try {        
            String configId = getConfigId();
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof RfcDefaultIPCSession)) {
                throw new IllegalClassException(session, RfcDefaultIPCSession.class);
            }
            this.cachingClient = ((RfcDefaultIPCSession) session).getClient();

            JCO.Function functionCfgApiDeleteAssumption = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_DELETE_ASSUMPTION);
            JCO.ParameterList im = functionCfgApiDeleteAssumption.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiDeleteAssumption.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiDeleteAssumption.getTableParameterList();

            im.setValue(Integer.parseInt(this.id), CfgApiDeleteAssumption.ASSUMPTION_ID);     // INT2, Assumption ID
            im.setValue(configId, CfgApiDeleteAssumption.CONFIG_ID);     // STRING, Configuration Identifier
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_DELETE_ASSUMPTION");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiDeleteAssumption); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC CFG_API_DELETE_ASSUMPTION");
            
            String assumptionDeleted = ex.getString(CfgApiDeleteAssumption.ASSUMPTION_DELETED); // not used by COL
            conflict.getExplanationTool().setUpdateConflicts(true);
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }

    }

}
