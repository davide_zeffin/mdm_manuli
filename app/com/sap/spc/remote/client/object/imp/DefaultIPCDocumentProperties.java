/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;

/**
 * Client-side class:
 * Properties to initialize a document with. Please note that the properties object
 * you pass to a document will be stored inside the document. So you can not reuse
 * a properties object to initialize several documents. Additionally, please note
 * that once you have called newDocument() with a properties object, the server takes
 * over "setter control", ie. any call to set... of this object will not cause
 * any effect. You may use this object for get... access at any time, though (although
 * it is recommended to access the document via the document's methods,  but not via property object).
 *  
 */
public abstract class DefaultIPCDocumentProperties implements IPCDocumentProperties
{

	/*** Application Caching variables starts *****/
	protected String m_salesOrganisation;
	protected String m_distributionChannel;
	protected String m_division;
	protected String m_language;
	protected String m_documentCurrency;
	protected String m_localCurrency;
	protected String m_DepartureCountry;
	protected String m_country;
	protected String m_pricingProcedure;
	protected String m_DepartureRegion;
	protected String m_externalId;
	protected char m_editMode;
    protected boolean _performPricingAnalysis;
    protected String _usage;
    protected String _application;
    protected boolean m_stripDecimalPlaces;
	protected String _businessObjectType;

	/**
	 * Header attribute names as retrieved from the server.
	 */
    protected ArrayList      m_headerAttributeNames;

	/**
	 * Header attribute values as retrieved from the server.
	 */
    protected ArrayList     m_headerAttributeValues;    
	/*** Application Cahcing variables ends *****/


	protected String  m_decimalSeparator;
	protected String  m_groupingSeparator;

	protected boolean m_groupConditionProcessing;
    
    public static String PROPERTIES      = "docProps";

	/**
	 * Additional parameters for CreateItem(s). m_commandParameterNames/Values are initially null.
	 * After they are defined they always have the same number of elements; the i-th member of both
	 * corresponds.
	 */
	protected ArrayList      m_commandParameterNames;
	protected ArrayList      m_commandParameterValues;

    public void setLocalCurrency(String localCurrency)
    {
		m_localCurrency = localCurrency;
    }

    public String getLocalCurrency() throws IPCException
    {
        return m_localCurrency;
    }

    public String[] getAllHeaderAttributeValues() throws IPCException
    {
        return (new String[] {
            m_salesOrganisation,
			m_distributionChannel,
			m_division,
			m_localCurrency,
			m_documentCurrency,
			m_DepartureCountry,
			m_DepartureRegion
        });
    }


    public void setDocumentCurrency(String documentCurrency)
    {
	   m_documentCurrency = documentCurrency;
    }

    public String getDocumentCurrency() throws IPCException
    {
        return m_documentCurrency;
    }

    public String[] getAllHeaderAttributeNames()
    {
        return (new String[] {
            "SALES_ORG", "DIS_CHANNEL", "DIVISION", "HWAER", "WAERK", "TAX_DEPT_CTY","TAX_DEPT_REG"
        });
    }

	public void setAttribute(String attrName,String attrValue){
		if(attrName.equalsIgnoreCase("SALES_ORG")) setSalesOrganisation(attrValue);
		else
		if(attrName.equalsIgnoreCase("DIS_CHANNEL"))setDistributionChannel(attrValue);
		else
		if(attrName.equalsIgnoreCase("DIVISION"))setDivision(attrValue);
		else
		if(attrName.equalsIgnoreCase("HWAER"))setLocalCurrency(attrValue);
		else
		if(attrName.equalsIgnoreCase("WAERK"))setDocumentCurrency(attrValue);
		else
		if(attrName.equalsIgnoreCase("TAX_DEPT_CTY"))setDepartureCountry(attrValue);
		else
		if(attrName.equalsIgnoreCase("TAX_DEPT_REG"))setDepartureRegion(attrValue);

		}

    public abstract void setDivision(String division);

    public abstract void setDepartureRegion(String departureRegion);

    public String getDivision() throws IPCException {
        return m_division;
    }

    public String getDepartureRegion() throws IPCException {
        return m_DepartureRegion;
    }

    public void setLanguage(String language) {
        //m_language.setInitialValue(language);
		m_language = language;
    }

    public String getLanguage() throws IPCException {
        return m_language;
    }

	public boolean isGroupConditionProcessingEnabled() {
		return m_groupConditionProcessing;
	}


    public abstract void setDistributionChannel(String distributionChannel);

    public String getDistributionChannel() throws IPCException
    {
        return m_distributionChannel;
    }

    public void setPricingProcedure(String pricingProcedure)
    {
		m_pricingProcedure = pricingProcedure;
    }

    public String getPricingProcedure() throws IPCException
    {
        return m_pricingProcedure;
    }

    public abstract void setDepartureCountry(String departureCountry);

    public void setCountry(String country)
    {
		m_country = country;
    }

    public String getDepartureCountry() throws IPCException
    {
        return m_DepartureCountry;
    }

    public String getCountry() throws IPCException
    {
        return m_country;
    }

    public abstract void setSalesOrganisation(String salesOrganisation);

	public String getSalesOrganisation() throws IPCException
    {
        return m_salesOrganisation;
    }

	public void setGroupConditionProcessingEnabled(boolean enabled) {
		m_groupConditionProcessing = enabled;
	}


	public void setHeaderAttributes(Map headerAttributes) {
        m_headerAttributeNames = new ArrayList(headerAttributes.size());
        m_headerAttributeValues = new ArrayList(headerAttributes.size());
		Iterator iter=headerAttributes.keySet().iterator();
		while (iter.hasNext()) {
			String key = (String)iter.next();
            m_headerAttributeNames.add(key);
            m_headerAttributeValues.add((String)headerAttributes.get(key));
		}
	}


	public HashMap getHeaderAttributes() throws IPCException {
		Object[] keys = m_headerAttributeNames.toArray(); 
		Object[] values = m_headerAttributeValues.toArray();
		// kha: checking if the keys have changed since last call
		// and returning a stored Hashtable would be fine, but
		// this would probably take just as long as constructing the table
		HashMap result = new HashMap();
		if (keys != null && keys.length > 0)
		for (int i=0; i<keys.length; i++) {
			 result.put((String)keys[i], (String)values[i]);
		}
		return result;
	}


	public String[] getHeaderAttributeEnvironment() throws IPCException {
        // to be compatible we have to convert the ArrayList of attribute names to a String Array
        Object[] headerAttributeNamesObj = m_headerAttributeNames.toArray();         
        String[] headerAttributeNames = new String[headerAttributeNamesObj.length];
        for (int i=0; i<headerAttributeNamesObj.length; i++){
            headerAttributeNames[i] = (String)headerAttributeNamesObj[i];
        }
		return headerAttributeNames;
	}


	public String getHeaderAttributeBinding(String key) throws IPCException {
		Object[] keys = m_headerAttributeNames.toArray();
		Object[] vals = m_headerAttributeValues.toArray();
		if (keys == null)
			return null;

		// instead of constructing the hashmap and immediately disposing of it,
		// it's better to just walk through the values
		for (int i=0; i<keys.length; i++) {
            String currentKey = (String)keys[i];
			if (currentKey.equals(key))
				return (String)vals[i];
		}
		return null;
	}


	public void setExternalId(String id) {
		m_externalId = id;
	}


	public String getExternalId() throws IPCException {
		return m_externalId;
	}


	public void setEditMode(char mode) {
		m_editMode = mode;
	}


	public char getEditMode() throws IPCException {
		return m_editMode;
	}


	/**
	 * Sets the grouping separator to be used when displaying prices, quantities and
	 * related information of this document. Please note that the grouping separator can only be
	 * set before a Document is created and persists through the whole lifetime of the
	 * Document.
	 */
	public void setGroupingSeparator(String sep) {
		m_groupingSeparator = sep;
	}


	/**
	 * Returns the grouping separator used by this document, or null, if this separator
	 * has not been set explicitely by setGroupingSeparator.
	 */
	public String getGroupingSeparator() {
		return m_groupingSeparator;
	}

	/**
	 * Sets the decimal separator to be used when displaying prices, quantities and
	 * related information of this document. Please note that the decimal separator can only be
	 * set before a Document is created and persists through the whole lifetime of the
	 * Document.
	 */
	public void setDecimalSeparator(String sep) {
		m_decimalSeparator = sep;
	}


	/**
	 * Returns the decimal separator used by this document, or null, if this separator
	 * has not been set explicitely by setDecimalSeparator.
	 */
	public String getDecimalSeparator() {
		return m_decimalSeparator;
	}


	/**
	 * Adds a parameter=name setting to these properties that will be passed without any interpretation to
	 * the IPC command that creates the sales document. This method can be used to use sophisticated features
	 * of the pricing engine that are not included in this API because they would make it impossible to
	 * implement it differently.
	 */
	public void addCreationCommandParameter(String name, String value) {
		if (m_commandParameterNames == null) {
			m_commandParameterNames = new ArrayList();
			m_commandParameterValues = new ArrayList();
		}
		m_commandParameterNames.add(name);
		m_commandParameterValues.add(value);
	}


	public List getCreationCommandParameterNames() {
		return m_commandParameterNames;
	}

	public List getCreationCommandParameterValues() {
		return m_commandParameterValues;
	}

    public  boolean getPerformPricingAnalysis() throws IPCException {
        return _performPricingAnalysis/*.getSingleContent()*/;
    }

    public  void setPerformPricingAnalysis(boolean flag) {
        _performPricingAnalysis = flag;
    }

	protected boolean _ynToBoolean(String val) {
        if (val == null)
            return false;
        else
            return val.equals("Y");
    }

	protected String _booleanToYNValue(boolean b) {
        return b ? "Y" : "N";
    }

    /**
     * @return application 
     */
    public String getApplication() {
        return _application;
    }

    /**
     * @return Usage for Condition Technique
     */
    public String getUsage() {
        return _usage;
    }

    /**
     * @param string Application. "CRM" or "V" for ERP.
     */
    public void setApplication(String string) {
        _application = string;
    }

    /**
     * @param string Usage for Condition Technique. "PR" for CRM. "A" for ERP.
     */
    public void setUsage(String string) {
        _usage = string;
    }

	/**
	 * @return
	 */
	public String getBusinessObjectType() {
		return _businessObjectType;
	}
	
	public void setBusinessObjectType(String businessObjectType) {
		_businessObjectType = businessObjectType;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#isStripDecimalPlaces()
     */
    public boolean isStripDecimalPlaces() {
        return m_stripDecimalPlaces;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setStripDecimalPlaces(boolean)
     */
    public void setStripDecimalPlaces(boolean stripDecimalPlaces) throws IPCException {
        m_stripDecimalPlaces = stripDecimalPlaces;
    }

}
