/*
 * Created on 20.02.2006
 *
 */
package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.KnowledgeBase;

/**
 * This key identifies a <code>CharacteristicGroup</code> session spreading.
 * It's used to store <code>GroupCharacteristicsCacheContainer</code>
 * @see KnowledgebaseCacheContainer, com.sap.isa.core.cache.Cache
 */
public class KnowledgebaseCacheKey implements Comparable{
    String kblogsys;
    String kbname;
    String kbversion;
    String buildnumber;
    String language;
    private String instanceName;
    int hashCode;
    /**
     * @param group
     */
    public KnowledgebaseCacheKey(Instance inst) {
        Configuration cfg = inst.getConfiguration();
        KnowledgeBase kb = cfg.getKnowledgeBase();
        kblogsys = kb.getLogsys();
        kbname = kb.getName();
        kbversion = kb.getVersion();
        buildnumber = kb.getBuildNumber();
        instanceName = inst.getName();
        hashCode=toString().hashCode();
        language = inst.getIpcClient().getIPCSession().getLanguage();
    }

    /**
     * Constructor for automated Tests
     * @param kblogsys
     * @param kbname
     * @param kbversion
     * @param buildnumber
     * @param configId
     * @param instanceName
     * @param groupName
     * @param isBaseGroup
     */
    public KnowledgebaseCacheKey(
           String kblogsys,
           String kbname,
           String kbversion,
           String buildnumber,
           String instanceName,
           String language)
    {
        this.kblogsys =  kblogsys;
        this.kbname =   kbname;
        this.kbversion =   kbversion;
        this.buildnumber =  buildnumber;
        this.instanceName = instanceName;
        this.language = language;
        hashCode=toString().hashCode();
	}
    /**
     * @return Returns the buildnumber.
     */
    public String getBuildnumber() {
        return buildnumber;
    }
    /**
     * @return Returns the kblogsys.
     */
    public String getKblogsys() {
        return kblogsys;
    }
    /**
     * @return Returns the kbname.
     */
    public String getKbname() {
        return kbname;
    }
    /**
     * @return Returns the kbversion.
     */
    public String getKbversion() {
        return kbversion;
    }
    public boolean equals(Object other) {
        return compareTo(other) == 0;
    }

    /**
     * 
     * @param other
     * @return
     */
    public int compareTo(Object other) {
        KnowledgebaseCacheKey _other=(KnowledgebaseCacheKey)other;
        int rc = buildnumber.compareTo(_other.getBuildnumber());
        if(rc != 0)
            return rc;
        rc= kbversion.compareTo(_other.getKbversion());
        if(rc != 0)
            return rc;
        rc= kbname.compareTo(_other.getKbname());
        if(rc != 0)
            return rc;
        rc= instanceName.compareTo(_other.getInstanceName());
        if(rc != 0)
            return rc;
        rc= language.compareTo(_other.language);
        if(rc != 0)
            return rc;
        return kblogsys.compareTo(_other.getKblogsys());
    }
    /**
     * @return Returns the instanceName.
     */
    public String getInstanceName() {
        return instanceName;
    }
    public int hashCode()
    {
        return hashCode;
    }
    public String toString()
    {
        StringBuffer sb = new StringBuffer(40);
        sb.append(kblogsys);
        sb.append(kbname);
        sb.append(kbversion);
        sb.append(buildnumber);
        sb.append(instanceName);
        sb.append(language);
        return sb.toString();
    }
}
