package com.sap.spc.remote.client.object.imp.rfc;

import com.sap.spc.remote.client.object.IPCItemReference;

/**
 * @author I026584
 *
 */
public class RFCIPCItemReference extends IPCItemReference {
	protected String connectionKey = "";
	
	public RFCIPCItemReference(){
	}
	
	public RFCIPCItemReference(String documentId, String itemId) {
		this.documentId 	= documentId;
		this.itemId 		= itemId;
	}
	
	/**
	 * @return
	 */
	public String getConnectionKey() {
		return connectionKey;
	}

	/**
	 * @param string
	 */
	public void setConnectionKey(String string) {
		connectionKey = string;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItemReference#init(com.sap.spc.remote.client.object.IPCItemReference)
     */
    public void init(IPCItemReference reference) {
        this.documentId = ((RFCIPCItemReference)reference).getDocumentId();
        this.itemId = ((RFCIPCItemReference)reference).getItemId();
    }

}
