/*
 * Created on Dec 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.msasrv.usermodule.tte.ITTECommandSet;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocumentProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.shared.command.ChangeDocument;
import com.sap.spc.remote.shared.command.CreateDocument;
import com.sap.spc.remote.shared.command.GetDocumentAndItemInfo;
import com.sap.spc.remote.shared.command.GetDocumentInfo;
import com.sap.spc.remote.shared.command.RemoveItems;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.tte.remote.shared.command.TteGetXmlTaxDocs;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TcpDefaultIPCDocument
	extends DefaultIPCDocument
	implements IPCDocument {

		private static final Category category = ResourceAccessor.category;
		private static final Location location = ResourceAccessor.getLocation(TcpDefaultIPCDocument.class);
		
		//Constructors
		protected TcpDefaultIPCDocument(DefaultIPCSession session, DefaultIPCDocumentProperties attr) throws IPCException {
			_session = session;
			_properties = attr;
			_items = new Vector();
			_itemsCache = null;
			_isClosed = false;

			_netValueWithoutFreight = null;
			_taxValue = null;
			_grossValue = null;
			_currencyUnit = null;
			_freight = null;
			_netValue = null;

			createDocument();
			
		}


		protected TcpDefaultIPCDocument(DefaultIPCSession session, String documentId) throws IPCException {
			_session = session;
			_documentId = documentId;
			_items = new Vector();
			_itemsCache = null;
			_isClosed = false;
			_properties = factory.newIPCDocumentProperties();

			_netValue = null;
			_netValueWithoutFreight = null;
			_taxValue = null;
			_grossValue = null;
			_currencyUnit = null;
			_freight = null;

			_pricingConditionSet = new TcpDefaultIPCPricingConditionSet(this);

			initializeValues(_session, this.getId());
			initializeItems();
		}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCDocument#initializeItems()
	 */
	protected void initializeItems() throws IPCException 
	{
		//50 Application Caching
		if (isCacheDirty()){
			syncWithServer();
		}else{
			//Cache is in sync with server -> do nothing...
			category.logT(Severity.DEBUG, location, "Server is in sync + Saved command call: " + ISPCCommandSet.GET_ITEMS);
			return;
		} 
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCDocument#createDocument()
	 */
	protected void createDocument() throws IPCException
	{
	   try {
		   // 20020718-kha: removed the faulty "treating sales org, dist channel, ..." as normal header attributes.
		   // using an ArrayList to collect values to make the code nicer.
		   ArrayList params = new ArrayList();
		   String salesOrg = _properties.getSalesOrganisation();
		   if (salesOrg != null) {
			   params.add(CreateDocument.SALES_ORGANISATION);
			   params.add(salesOrg);
		   }
		   String distChannel = _properties.getDistributionChannel();
		   if (distChannel != null) {
			   params.add(CreateDocument.DISTRIBUTION_CHANNEL);
			   params.add(distChannel);
		   }

		   // check if the user has set decimal and/or grouping separator and reserve array space accordingly
		   String decSep = _properties.getDecimalSeparator();
		   if (decSep != null) {
			   params.add(CreateDocument.DECIMAL_SEPARATOR);
			   params.add(decSep);
		   }
		   String groupSep = _properties.getGroupingSeparator();
		   if (groupSep != null) {
			   params.add(CreateDocument.GROUPING_SEPARATOR);
			   params.add(groupSep);
		   }

		   String documentCurrency = _properties.getDocumentCurrency();
		   if (documentCurrency != null) {
			   params.add(CreateDocument.DOCUMENT_CURRENCY_UNIT);
			   params.add(documentCurrency);
		   }
		   String localCurrency = _properties.getLocalCurrency();
		   if (localCurrency != null) {
			   params.add(CreateDocument.LOCAL_CURRENCY_UNIT);
			   params.add(localCurrency);
		   }
		   String procedureName = _properties.getPricingProcedure();
		   if (procedureName != null) {
			   params.add(CreateDocument.PROCEDURE_NAME);
			   params.add(procedureName);
		   }
		   String language = _properties.getLanguage();
		   if (language != null) {
			   params.add(CreateDocument.LANGUAGE);
			   params.add(language);
		   }
		   String country = _properties.getCountry();
		   if (country != null) {
			   params.add(CreateDocument.COUNTRY);
			   params.add(country);
		   }

		   params.add(CreateDocument.GROUP_CONDITION_PROCESSING);
		   params.add(_properties.isGroupConditionProcessingEnabled() ? "Y" : "N");

		   // Header Attributes
		   HashMap props = _properties.getHeaderAttributes();
		   if (props != null) {
			   String attrName = null;
			   String attrValue = null;
			   for (Iterator iter=props.keySet().iterator(); iter.hasNext();) {
				   attrName = (String)iter.next();
				   attrValue = (String) props.get(attrName);
				   params.add(CreateDocument.HEADER_ATTRIBUTE_NAMES);
				   params.add(attrName);
				   params.add(CreateDocument.HEADER_ATTRIBUTE_VALUES);
				   params.add(attrValue);
			   }
		   }

		   List additionalNames = _properties.getCreationCommandParameterNames();
		   List additionalValues = _properties.getCreationCommandParameterValues();
		   if (additionalNames != null) {
			   int additionalParameters = additionalNames.size();
			   for (int i=0; i<additionalParameters; i++) {
				   params.add(additionalNames.get(i));
				   params.add(additionalValues.get(i));
			   }
		   }

		   // convert to string array
		   String[] inpParameters = new String[params.size()];
		   params.toArray(inpParameters);

		   // since we need the response immediately, there's no need to delay
			ServerResponse r = ((TCPDefaultClient)_session.getClient()).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.CREATE_DOCUMENT, inpParameters);
		    _documentId = r.getParameterValue(CreateDocument.DOCUMENT_ID);
		    initializeValues(_session, this.getId());
			setCacheDirty(); //50 Application caching.
	   }
	   catch(ClientException e) {
		   throw new IPCException(e);
	   }
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#setHeaderAttributeBindings(java.lang.String[], java.lang.String[])
	 */
	public void setHeaderAttributeBindings(String[] keys, String[] values)
		throws IPCException 
		{
           location.entering("setHeaderAttributeBindings(String[] keys, String[] values)");
           if (location.beDebug()){
                 location.debugT("Parameters:");
                 location.debugT("String[] keys ");
                 if (keys != null) {
                     for (int i = 0; i<keys.length; i++){
                         location.debugT("  keys["+i+"] "+keys[i]);
                     }
                 }
                 location.debugT("String[] values ");
                 if (values != null) {
                     for (int i = 0; i<values.length; i++){
                         location.debugT("  values["+i+"] "+values[i]);
                     }
                 }
           } 
		   Map attr = _properties.getHeaderAttributes();
		   Vector requestParam = new Vector(keys.length*4+2);
		   requestParam.addElement(ChangeDocument.DOCUMENT_ID);
		   requestParam.addElement(_documentId);
		   for (int i=0; i<keys.length; i++) {
			   requestParam.addElement(ChangeDocument.HEADER_ATTRIBUTE_NAMES);
			   requestParam.addElement(keys[i]);
			   requestParam.addElement(ChangeDocument.HEADER_ATTRIBUTE_VALUES);
			   requestParam.addElement(values[i]); // may be null but the request can handle this
			   attr.put(keys[i],values[i]);
		   }
		   _properties.setHeaderAttributes(attr);

		   String[] parameters = new String[requestParam.size()];
		   requestParam.copyInto(parameters);
		   _changeDocument(parameters);
           location.exiting();
		 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#changeDocumentCurrency(java.lang.String)
	 */
	public void changeDocumentCurrency(String currencyUnit)
		throws IPCException 
		{
         location.entering("changeDocumentCurrency(String currencyUnit)");
         if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String currencyUnit " + currencyUnit);
         }
		 String[] inputParameters = {ChangeDocument.DOCUMENT_CURRENCY_UNIT,currencyUnit};
		 _changeDocument(inputParameters);
         location.exiting();
	   }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCDocument#_changeDocument(java.lang.String[])
	 */
	protected void _changeDocument(String[] parameters) throws IPCException 
	{
	   String[] realParams = new String[parameters.length+2];
	   realParams[0] = ChangeDocument.DOCUMENT_ID;
	   realParams[1] = _documentId;
	   for (int i=0; i<parameters.length; i++) {
		   realParams[i+2] = parameters[i];
	   }
	   try {
		((TCPDefaultClient)_session.getClient()).doCmd(ISPCCommandSet.CHANGE_DOCUMENT, realParams);
		setCacheDirty();
	   }
	   catch(ClientException e) {
		   throw new IPCException(e);
	   }
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#removeItem(com.sap.spc.remote.client.object.IPCItem)
	 */
	public synchronized void removeItem(IPCItem item) throws IPCException 
	{
        location.entering("removeItem(IPCItem item)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCItem item " + item.getItemId());
        }
		_items.removeElement(item);
		_itemsCache = null;
		 try {
			((TCPDefaultClient)_session.getClient()).doCmd(ISPCCommandSet.REMOVE_ITEMS, new String[] {
				 RemoveItems.DOCUMENT_ID, _documentId, RemoveItems.ITEM_IDS, item.getItemId() });
			setCacheDirty();
            location.exiting();
		 }
		 catch(Exception e) {
			 throw new IPCException(e);
		 }
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCDocument#_removeItemsOnServer(com.sap.spc.remote.client.object.IPCItem[])
	 */
	protected void _removeItemsOnServer(IPCItem[] items) throws IPCException 
	{
	   String[] params = new String[items.length*2 + 2];
	   params[0] =	RemoveItems.DOCUMENT_ID;
	   params[1] = _documentId;
	   for (int i=0; i<items.length; i++) {
			params[2 + i*2] = RemoveItems.ITEM_IDS;
			params[3 + i*2] = items[i].getItemId();
	   }
	   try {
			((TCPDefaultClient)_session.getClient()).doCmd(ISPCCommandSet.REMOVE_ITEMS, params);
			setCacheDirty();
	   }
	   catch(ClientException e) {
		   throw new IPCException(e);
	   }
 	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#changeLocalCurrency(java.lang.String)
	 */
	public void changeLocalCurrency(String localCurrency) throws IPCException 
	{
        location.entering("changeLocalCurrency(String localCurrency)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String localCurrency " + localCurrency);
        }
		 String[] inputParameters = { ChangeDocument.LOCAL_CURRENCY_UNIT, localCurrency };
		 _changeDocument(inputParameters);
         location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#changePricingProcedure(java.lang.String)
	 */
	public void changePricingProcedure(String pricingProcedure)
		throws IPCException 
		{
           location.entering("changePricingProcedure(String pricingProcedure)");
           if (location.beDebug()){
               location.debugT("Parameters:");
               location.debugT("String pricingProcedure " + pricingProcedure);
           }
		   String[] inputParameters = {ChangeDocument.PROCEDURE_NAME, pricingProcedure};
		   _changeDocument(inputParameters);
           location.exiting();
		}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#changeSalesOrganisation(java.lang.String)
	 */
	public void changeSalesOrganisation(String salesOrganisation)
		throws IPCException 
		{
         location.entering("changeSalesOrganisation(String salesOrganisation)");
         if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String salesOrganisation " + salesOrganisation);
         }
		 String[] inputParameters = {ChangeDocument.SALES_ORGANISATION, salesOrganisation};
		 _changeDocument(inputParameters);
         location.exiting();
	   }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#changeDistributionChannel(java.lang.String)
	 */
	public void changeDistributionChannel(String distributionChannel)
		throws IPCException 
		{
         location.entering("changeDistributionChannel(String distributionChannel)");
         if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String distributionChannel " + distributionChannel);
         }
		 String[] inputParameters = {ChangeDocument.DISTRIBUTION_CHANNEL,distributionChannel};
		 _changeDocument(inputParameters);
         location.exiting();
	   }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#syncCommonProperties()
	 */
	public void syncCommonProperties() throws IPCException 
	{
        location.entering("syncCommonProperties()");
		if (isCacheDirty()){
			syncWithServer();
		}else{
			//Cache is in sync with server -> do nothing...
			category.logT(Severity.DEBUG, location, "Server is in sync + Saved command call: " + ISPCCommandSet.GET_DOCUMENT_AND_ITEM_INFO);
			location.exiting();
            return;
		}
        location.exiting();
	}

	 /**
	  * Sets Cache as diety.
	  */
	 public void setCacheDirty() {
        location.entering("setCacheDirty()");
		if (!isCacheDirty()){
			_setDocFlagToDirty();
			category.log(Severity.DEBUG,location,"Setting client document cache as Dirty.");
		}
		_setDocItemsFlagToDirty(); //If doc is dirty items should also be dirty.
        location.exiting();
	 }
			
	 public synchronized void syncWithServer() throws IPCException{
	 	location.entering("syncWithServer()");
		if (!isCacheDirty()) return;

		category.log(Severity.DEBUG,location,"Client document cache is dirty. Syncing client document with Server....");
		String decSep = _properties.getDecimalSeparator();
		String groupSep = _properties.getGroupingSeparator();

		TCPDefaultClient client = (TCPDefaultClient)_session.getClient();
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_session).getPricingConverter();
		try {
			ServerResponse r = client.doCmd(ISPCCommandSet.GET_DOCUMENT_AND_ITEM_INFO,
											new String[] {
												GetDocumentAndItemInfo.DOCUMENT_ID, _documentId,
												GetDocumentAndItemInfo.FORMAT_VALUE, _isValueFormatting ? "Y" : "N"
											});

			String currencyUnit = r.getParameterValue(GetDocumentAndItemInfo.CURRENCY_UNIT);
			_netValue = r.getParameterValue(GetDocumentAndItemInfo.NET_VALUE);
			_taxValue = r.getParameterValue(GetDocumentAndItemInfo.TAX_VALUE);
			_grossValue = r.getParameterValue(GetDocumentAndItemInfo.GROSS_VALUE);
			_currencyUnit = currencyUnit;
			_freight = r.getParameterValue(GetDocumentAndItemInfo.FREIGHT);

			String[] itemIds = r.getParameterValues(GetDocumentAndItemInfo.ITEM_ID);
			// error or just no item
			if (itemIds == null || itemIds.length == 0) {
				_items = new Vector();
				_itemsCache = null;
				//Now cache is not dirty.
				_setDocItemsFlagToSync();
				_setDocFlagToSync();
                location.exiting();
				return;
			}
			
			String[] parentItemIds = r.getParameterValues(GetDocumentAndItemInfo.ITEM_PARENT_ID);
			String[] netValues = r.getParameterValues(GetDocumentAndItemInfo.ITEM_NET_VALUE);
			String[] taxValues = r.getParameterValues(GetDocumentAndItemInfo.ITEM_TAX_VALUE);
			String[] grossValues = r.getParameterValues(GetDocumentAndItemInfo.ITEM_GROSS_VALUE);
			String[] totalGrossValues = r.getParameterValues(GetDocumentAndItemInfo.ITEM_TOTAL_GROSS_VALUE);
			String[] totalNetValues = r.getParameterValues(GetDocumentAndItemInfo.ITEM_TOTAL_NET_VALUE);
			String[] totalTaxValues = r.getParameterValues(GetDocumentAndItemInfo.ITEM_TOTAL_TAX_VALUE);
			String[] productGuids = r.getParameterValues(GetDocumentAndItemInfo.ITEM_PRODUCT_GUID);
			String[] productIds = r.getParameterValues(GetDocumentAndItemInfo.ITEM_PRODUCT_ID);
			String[] productDescriptions = r.getParameterValues(GetDocumentAndItemInfo.ITEM_PRODUCT_DESCRIPTION);
			String[] quantityValues = r.getParameterValues(GetDocumentAndItemInfo.ITEM_QUANTITY_VALUE);
			String[] quantityUnits = r.getParameterValues(GetDocumentAndItemInfo.ITEM_QUANTITY_UNIT);
			String[] configurables = r.getParameterValues(GetDocumentAndItemInfo.ITEM_IS_CONFIGURABLE);
			String[] productVariants = r.getParameterValues(GetDocumentAndItemInfo.ITEM_IS_PRODUCT_VARIANT);
			String[] completes = r.getParameterValues(GetDocumentAndItemInfo.ITEM_CONFIG_COMPLETE);
			String[] consistents = r.getParameterValues(GetDocumentAndItemInfo.ITEM_CONFIG_CONSISTENT);
			String[] configChangeds = r.getParameterValues(GetDocumentAndItemInfo.ITEM_CONFIG_CHANGED);
			String[] parentIds = r.getParameterValues(GetDocumentAndItemInfo.ITEM_PARENT_ID);

			// determine Map: itemId -> List of itemId, where the key is an item id, and the value the ids of all subitems
			Map subitemTable = DefaultIPCDocument.getChildrenMap(itemIds, parentIds);
			Map idToItem = new HashMap();
            // step 0: keep a list of items known on client side before the update (needed to delete items that were deleted on the server side)
            Vector oldItems = (Vector)_items.clone();            
			// step 1: create all items
			String externalQtyValue = null;
			String externalQtyUnit = null;
			String internalQtyValue = null;
			String internalQtyUnit = null;
			for (int i=0; i<itemIds.length; i++) {
				DefaultIPCItem item = _getItemRecursive(itemIds[i]);
                // if item was found already on the client side remove it from the list of oldItems
                if (item != null){
                    oldItems.removeElement(item);
                }                
				if (item == null) {
					item = factory.newIPCItem(client, this, itemIds[i], parentItemIds[i]);
                    if (item.getParent() == null) {
                        addItem(item);
                    }                    
				}
				Object cacheKey = null; 
				if (_isValueFormatting){
					// command returns external representation
					externalQtyValue 	= quantityValues[i];
					externalQtyUnit 		= quantityUnits[i];
					internalQtyValue 	= pc.convertValueExternalToInternal(quantityValues[i], quantityUnits[i], decSep, groupSep);
					internalQtyUnit 		= pc.convertUOMExternalToInternal(quantityUnits[i]);
				}else{
					// command returns internal representation
					externalQtyValue 	= pc.convertValueInternalToExternal(quantityValues[i], quantityUnits[i], decSep, groupSep);
					externalQtyUnit 		= pc.convertUOMInternalToExternal(quantityUnits[i]);
					internalQtyValue 	= quantityValues[i];
					internalQtyUnit 		= quantityUnits[i];
				}
				item.initCommonProperties(cacheKey, netValues[i], taxValues[i], grossValues[i], totalNetValues[i],
										  totalTaxValues[i], totalGrossValues[i], currencyUnit,
										  productGuids[i], productIds[i], productDescriptions[i],
										  internalQtyValue, internalQtyUnit,
										  externalQtyValue, externalQtyUnit,
										  configurables[i], productVariants[i],
										  completes[i], consistents[i], configChangeds[i], (List)subitemTable.get(itemIds[i]));
                // "net value without freight" is not returned by command, so we set it to null
                item.initOtherProperties(null, _currencyUnit);
                item.initConfiguration();
                item.initializeChildrenByInstanceId();
                //initItemDetails(_documentId, itemIds[i], item);
				((TcpDefaultIPCItem)item).initItemDetails();
				idToItem.put(itemIds[i], item);
			}
            // step 2: remove the remaining items of oldItems from _items -> these have been obviously deleted on the server
            Iterator oldItemsIterator = oldItems.iterator(); 
            while (oldItemsIterator.hasNext()){
                DefaultIPCItem oldItem = (DefaultIPCItem)oldItemsIterator.next();
                _items.removeElement(oldItem);
            }

			// step 3: connect every item to its children
			for (int i=0; i<itemIds.length; i++) {
				DefaultIPCItem item = (DefaultIPCItem)idToItem.get(itemIds[i]);
				item.connectToChildren(idToItem);
			}

		}
		catch(ClientException e) {
			throw new IPCException(e);
		}
	 	
	 	customerExitSyncWithServer();
	 	
		//Now cache is not dirty.
		_setDocItemsFlagToSync();
		_setDocFlagToSync();
        location.exiting();
	 }
			

	private void initializeValues(DefaultIPCSession session, String documentId) {

		TCPDefaultClient client = (TCPDefaultClient)session.getClient();
		ServerResponse r;
		try {
			r =
				client.doCmd(
					ISPCCommandSet.GET_DOCUMENT_INFO,
					new String[] { GetDocumentInfo.DOCUMENT_ID, documentId });
		} catch (ClientException e) {
			throw new IPCException(e);
		}
		if (r == null) {
			return;
		}
		
		String salesOrganisation = r.getParameterValue(GetDocumentInfo.SALES_ORGANISATION);
		if (_properties.getSalesOrganisation() == null && salesOrganisation != null) {
			_properties.setSalesOrganisation(salesOrganisation);
		}
		String distributionChannel = r.getParameterValue(GetDocumentInfo.DISTRIBUTION_CHANNEL);
		if (_properties.getDistributionChannel() == null && distributionChannel != null) {
			_properties.setDistributionChannel(distributionChannel);
		}
		String division = r.getParameterValue(GetDocumentInfo.DIVISION);
		if (_properties.getDivision() == null && division != null) {
			_properties.setDivision(division);
		}
		String documentCurrency = r.getParameterValue(GetDocumentInfo.DOCUMENT_CURRENCY);
		if (_properties.getDocumentCurrency() == null && documentCurrency != null) {
			_properties.setDocumentCurrency(documentCurrency);
		}
		String localCurrency = r.getParameterValue(GetDocumentInfo.LOCAL_CURRENCY);
		if (_properties.getLocalCurrency() == null && localCurrency != null) {
			_properties.setLocalCurrency(localCurrency);
		}
		String departureCountry = r.getParameterValue(GetDocumentInfo.DEPARTURE_COUNTRY);
		if (_properties.getDepartureCountry() == null && departureCountry != null) {
			_properties.setDepartureCountry(departureCountry);
		}
		String pricingProcedureName = r.getParameterValue(GetDocumentInfo.PRICING_PROCEDURE_NAME);
		if (_properties.getPricingProcedure() == null && pricingProcedureName != null) {
			_properties.setPricingProcedure(pricingProcedureName);
		}
		String departureRegion = r.getParameterValue(GetDocumentInfo.DEPARTURE_REGION);
		if (_properties.getDepartureRegion() == null && departureRegion != null) {
			_properties.setDepartureRegion(departureRegion);
		}
		String[] headerAttributeNames = r.getParameterValues(GetDocumentInfo.HEADER_ATTRIBUTES_NAMES);
		String[] headerAttributeValues = r.getParameterValues(GetDocumentInfo.HEADER_ATTRIBUTE_VALUES);
		if (_properties.getAllHeaderAttributeNames() == null && headerAttributeNames != null) {
			HashMap headerAttributes = new HashMap();
			for (int i = 0; i < headerAttributeNames.length; i++) {
				headerAttributes.put(headerAttributeNames[i], headerAttributeValues[i]);
			}
			_properties.setHeaderAttributes(headerAttributes);
		}
		String externalId = r.getParameterValue(GetDocumentInfo.EXTERNAL_ID);
		if (_properties.getExternalId() == null && externalId != null) {
			_properties.setExternalId(externalId);
		}
		String editMode = r.getParameterValue(GetDocumentInfo.EDIT_MODE);
		if (editMode != null) {
			_properties.setEditMode(editMode.charAt(0));
		}
		

	 }

	 public String[] getTTEAnalysisData() throws IPCException {
        location.entering("getTTEAnalysisData()");
		TCPDefaultClient client = (TCPDefaultClient)_session.getClient();
		String[] xmldocuments=new String[2];
		try {
			
			ServerResponse r = client.doCmd(ITTECommandSet.TTE_GET_XML_TAX_DOCS,
														new String[] {
															TteGetXmlTaxDocs.DOCUMENT_ID, _documentId,
															});
			xmldocuments[0]=r.getParameterValue(TteGetXmlTaxDocs.INPUT_DOCUMENT);
			xmldocuments[1]=r.getParameterValue(TteGetXmlTaxDocs.OUTPUT_DOCUMENT);															
															
			
		}catch (ClientException e){
			throw new IPCException(e);
		}
        if (location.beDebug()) {
            if (xmldocuments != null) {
                location.debugT("return xmldocuments");
                for (int i = 0; i<xmldocuments.length; i++){
                    location.debugT("  xmldocuments["+i+"] "+xmldocuments[i]);
                }
            }
        }
        location.exiting();
	 	return xmldocuments;
	 }
			
	//********Implementation for Abstract methods ends here!********

	public void lock() {
	   throw new IPCException(IPCException.UNIMPLEMENTED);
	}


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#getPropertiesFromServer()
	 */
	public IPCDocumentProperties getPropertiesFromServer() {
        location.entering("getPropertiesFromServer()");
        if (location.beDebug()) {
            location.debugT("return this._properties " + this._properties);
        }
		return this._properties;
	}
	

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCDocument#customerExitSyncWithServer()
	 */
	public void customerExitSyncWithServer() throws IPCException {

	}

    public HashMap getDocAndItemPropertiesFromServer() {
        location.entering("getDocAndItemPropertiesFromServer()");
        HashMap docAndItemProps = new HashMap();

        docAndItemProps.put(DefaultIPCDocumentProperties.PROPERTIES, _properties);
        
        IPCItem[] ipcItems =  _getItems(false);
        IPCItemProperties[] itemProperties = new IPCItemProperties[ipcItems.length];
        int i = 0;        
        while (i < ipcItems.length) {
            itemProperties[i] = ipcItems[i].getItemPropertiesNoSync();
            i++;
        }
        docAndItemProps.put(DefaultIPCItemProperties.PROPERTIES, itemProperties);        
        
        return docAndItemProps;

    }

}
