package com.sap.spc.remote.client.object.imp;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.ImportConfiguration;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.ClientLogListener;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.util.monitor.jarm.IMonitor;
import com.sap.isa.businessobject.BusinessObjectBase;


/**
 * Facade to the set of IPC objects (Session, Document, Item etc.)
 * in order to reduce the complexity of the accessing clients.
 */
public abstract class DefaultIPCClient extends BusinessObjectBase implements IPCClient {
	//	BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
	//	new-->
	static protected IMonitor _monitor = null;
	protected String SAT_STRING = "";
	//<--

    protected DefaultIPCSession ipcSession;
    protected String currentDocumentId; //if the client deals only with one document
    protected String currentItemId; //if the client deals only with one item
    protected Hashtable configurationHash = new Hashtable();
    protected IPCClientObjectFactory factory = IPCClientObjectFactory.getInstance();
    protected boolean closed;
    protected String serverVersion = null;

    // get the Logging Object of the Client Layer
	private static final Category category = ResourceAccessor.category;
    private static final Location location = ResourceAccessor.getLocation(DefaultIPCClient.class);

    public IPCDocument createIPCDocument(IPCDocumentProperties props)
              throws IPCException{
        //todo initialize with null object
        location.entering("createIPCDocument(IPCDocumentProperties props)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (props!=null){
                location.debugT("IPCDocumentProperties props " + props.toString());
            }
            else location.debugT("IPCDocumentProperties props null");
        }
        IPCDocument newDocument = null;
        newDocument = ipcSession.newDocument(props);
        currentDocumentId = newDocument.getId();
        if (location.beDebug()){
            location.debugT("return newDocument " + newDocument.getId());
        }
        location.exiting();
        return newDocument;
    }

    public IPCItem createIPCItem(String documentId, IPCItemProperties props)
              throws IPCException{
        location.entering("createIPCItem(String documentId, IPCItemProperties props)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String documentId " + documentId);
            if (props!=null){
                location.debugT("IPCItemProperties props " + props.toString());
            }
            else location.debugT("IPCItemProperties props null");

        }   
        IPCItem ipcItem = null;
        IPCDocument ipcDocument = ipcSession.getDocument(documentId);
        if (ipcDocument != null) {
            ipcItem = ipcDocument.newItem(props);
        }
        if (location.beDebug()){
            location.debugT("return ipcItem " + ipcItem.getItemId());
        }
        location.exiting();
        return ipcItem;
    }

    public Configuration getConfiguration(String documentId, String itemId){
        location.entering("getConfiguration(String documentId, String itemId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String documentId " + documentId);
            location.debugT("String itemId " + itemId);
        }
        IPCDocument ipcDocument = ipcSession.getDocument(documentId);
        IPCItem ipcItem = null;
        if (ipcDocument != null) {
			try {
	            ipcItem = ipcDocument.getItem(itemId);
			}
			catch(IPCException e) {
                category.logT(Severity.ERROR, location, e.toString());
                if (location.beDebug()){
                    location.debugT("return null");
                }
                location.exiting();
				return null;
			}
        }else {
            //log error messag document not found
            category.logT(Severity.ERROR, location, "Document not found");
        }
        Configuration configuration = null;
        if (ipcItem != null) {
            //todo
            //configuration = ipcItem.
        }
        if (location.beDebug()){
            location.debugT("return null");
        }
        location.exiting();
        return null;
    }

    public Configuration getConfiguration(String configId) {
        location.entering("getConfiguration(String configId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String configId " + configId);
        }
        Configuration config = ipcSession.getConfiguration(configId);
        if (location.beDebug()){
            location.debugT("return config " + config.getId());
        }
        location.exiting();
        return (Configuration)config;
    }

	public abstract Configuration getConfiguration(IPCConfigReference reference, boolean resetSession) throws IPCException;

    /**
    * Returns a reference to the configuration object for the last created
    * document and item.
    * @return Config
    **/
    public Configuration getConfiguration() {
        location.entering("getConfiguration()");
        Configuration config = getConfiguration(currentDocumentId, currentItemId);
        if (location.beDebug()){
            location.debugT("return config " + config.getId());
        }
        location.exiting();
        return config;
    }


	public Configuration getConfiguration(IPCItemReference reference)
                throws IPCException{

        location.entering("getConfiguration(IPCItemReference reference)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCItemReference reference " + reference.getItemId());
        }
		IPCItem item = getIPCItem(reference);
		if (item == null) {
            if (location.beDebug()){
                location.debugT("return null");
            }
            location.exiting();
            return null;
        }
		else{
            Configuration config = item.getConfiguration();
		    if (location.beDebug()){
                location.debugT("return config " + config.getId());
            }
            location.exiting();
		    return config;
        }
	}

	public Configuration getConfiguration(IPCConfigReference reference)
                throws IPCException{

		// stored in config object? karlheinz, any idea?

		return null;
	}

	public IPCItem getIPCItem(IPCItemReference reference) throws IPCException {
        location.entering("getIPCItem(IPCItemReference reference)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCItemReference reference " + reference.getItemId());
        }
        IPCItem ipcItem = getIPCItem(reference, false);
        if (location.beDebug()){
            location.debugT("return ipcItem " + ipcItem.getItemId());
        }
        location.exiting();
		return ipcItem;
	}


	public abstract IPCItem getIPCItem(IPCItemReference reference, boolean resetSession) throws IPCException;

    public ext_configuration getExternalConfiguration(){
        location.entering("getExternalConfiguration()");
		ext_configuration externalConfiguration = null;
        Configuration configuration = getConfiguration();
        if (configuration != null) {
            externalConfiguration = configuration.toExternal();
        }
        if (location.beDebug()){
            location.debugT("return externalConfiguration " + externalConfiguration.get_name());
        }
        location.exiting();
        return externalConfiguration;
    }

    public IPCDocument getIPCDocument(String documentId){
        location.entering("getIPCDocument(String documentId)");
        IPCDocument document = ipcSession.getDocument(documentId);
        currentDocumentId = documentId;
        if (location.beDebug()){
            location.debugT("return document " + document.getId());
        }
        location.exiting();
        return document;
    }

    public List getIPCDocuments() {
        location.entering("getIPCDocuments()");
        IPCDocument[] ipcDocumentArray = ipcSession.getDocuments();
        Vector ipcDocumentVector = new Vector(ipcDocumentArray.length);
        for (int i = 0; i < ipcDocumentArray.length; i++) {
            ipcDocumentVector.addElement(ipcDocumentArray[i]);
        }
        if (location.beDebug()){
            if (ipcDocumentVector!=null){
                location.debugT("return ipcDocumentVector " + ipcDocumentVector.toString());
            }
            else location.debugT("return ipcDocumentVector null");

        }
        location.exiting();
        return ipcDocumentVector;
    }

    public IPCItem getIPCItem(String documentId, String itemId){
        location.entering("getIPCItem(String documentId, String itemId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String documentId " + documentId);
            location.debugT("String itemId " + itemId);
        }
        IPCDocument ipcDocument = ipcSession.getDocument(documentId);
        IPCItem ipcItem = null;
        if (ipcDocument != null) {
			try {
                ipcItem = ipcDocument.getItem(itemId);
			}
			catch(IPCException e) {
				category.logT(Severity.ERROR, location, e.toString());
                if (location.beDebug()){
                    location.debugT("return null");
                }
                location.exiting();
				return null;
			}
        }
        if (location.beDebug()){
            location.debugT("return ipcItem " + ipcItem.getItemId());
        }
        location.exiting();
        return ipcItem;
    }

    public IPCSession getIPCSession(){
        location.entering("getIPCSession()");
        if (location.beDebug()){
            location.debugT("return ipcSession " + ipcSession.getSessionId());
        }
        location.exiting();
        return ipcSession;
    }

     public Configuration createConfiguration(String productId,
										      String language,
                                              String kbDate)
	                throws IPCException{
        location.entering("createConfiguration(String productId, String language, String kbDate");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String productId " + productId);
            location.debugT("String language " + language);
            location.debugT("String kbDate " + kbDate);
        }
		Configuration configuration =
		    createConfiguration(productId,
			    				null,
				    			language,
					    	    kbDate,
						        null,
						        null,
							    null,
	    					    null,
		    				    null,
			    			    null);
        if (location.beDebug()){
            location.debugT("return configuation " + configuration.getId());
        }
        location.exiting();
		return configuration;
    }
    public Configuration createConfiguration(String productId,
										     String language,
                                             String kbDate,
                                             String kbLogSys,
                                             String kbName,
                                             String kbVersion,
                                             String kbProfile,
                                             String[] contextNames,
                                             String[] contextValues)
	                throws IPCException{
        location.entering("Configuration createConfiguration(String productId, String language, "
                            + "String kbDate, String kbLogSys, String kbName, String kbVersion, "
                            + "String kbProfile, String[] contextNames, String[] contextValues)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String productId " + productId);
            location.debugT("String language " + language);
            location.debugT("String kbDate " + kbDate);
            location.debugT("String kbLogSys " + kbLogSys);
            location.debugT("String kbName " + kbName);
            location.debugT("String kbVersion " + kbVersion);
            location.debugT("String kbProfile " + kbProfile);
            location.debugT("String[] contextNames: ");
            if (contextNames != null) {
                for (int i = 0; i<contextNames.length; i++){
                    location.debugT("  contextNames["+i+"] "+contextNames[i]);
                }
            }
            location.debugT("String[] contextValues: ");
            if (contextNames != null) {
                for (int i = 0; i<contextValues.length; i++){
                    location.debugT("  contextValues["+i+"] "+contextValues[i]);
                }
            }
                
        }
	    Configuration configuration =
	    	createConfiguration(productId,
		    					null,
			    				language,
				    		    kbDate,
					    	    kbLogSys,
						        kbName,
						        kbVersion,
							    kbProfile,
	    					    contextNames,
		    				    contextValues);
        if (location.beDebug()){
            location.debugT("return configuation " + configuration.getId());
        }
        location.exiting();
		return configuration;
	}
    public abstract Configuration createConfiguration(String productId,
										     String productType,
										     String language,
                                             String kbDate,
                                             String kbLogSys,
                                             String kbName,
                                             String kbVersion,
                                             String kbProfile,
                                             String[] contextNames,
                                             String[] contextValues)
                throws IPCException;

	public Configuration createConfiguration(ext_configuration config,
										     String[] contextNames, String[] contextValues) throws IPCException {
        location.entering("createConfiguration(ext_configuration config, String[] contextNames, String[] contextValues");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("ext_configuration config " + config.get_name());
            location.debugT("String[] contextNames: ");
            if (contextNames != null) {
                for (int i = 0; i<contextNames.length; i++){
                    location.debugT("  contextNames["+i+"] " + contextNames[i]);
                }
            }
            location.debugT("String[] contextValues: ");
            if (contextNames != null) {
                for (int i = 0; i<contextValues.length; i++){
                    location.debugT("  contextValues["+i+"] " + contextValues[i]);
                }
            }
        }
        Configuration configuration = createConfiguration(config, contextNames, contextValues, null);
        if (location.beDebug()){
            location.debugT("return configuation " + configuration.getId());
        }
        location.exiting();
		return configuration;
	}

	public Configuration createConfiguration(String language, ext_configuration config,
										     String[] contextNames, String[] contextValues) throws IPCException {
         location.entering("createConfiguration(String language, ext_configuration config, String[] contextNames, String[] contextValues");
         if (location.beDebug()){
             location.debugT("Parameters:");
             location.debugT("String language " + language);
             location.debugT("ext_configuration config " + config.get_name());
             location.debugT("String[] contextNames: ");
             if (contextNames != null) {
                 for (int i = 0; i<contextNames.length; i++){
                     location.debugT("  contextNames["+i+"] " + contextNames[i]);
                 }
             }
             location.debugT("String[] contextValues: ");
             if (contextNames != null) {
                 for (int i = 0; i<contextValues.length; i++){
                     location.debugT("  contextValues["+i+"] " + contextValues[i]);
                 }
             }
         }
         Configuration configuration = createConfiguration(language, config, contextNames, contextValues, null);
         if (location.beDebug()){
             location.debugT("return configuation " + configuration.getId());
         }
         location.exiting();
         return configuration;
	}

	public Configuration createConfiguration(ext_configuration config,
										     String[] contextNames, String[] contextValues, String date) throws IPCException {
         location.entering("createConfiguration(ext_configuration config, String[] contextNames, String[] contextValues, String date");
         if (location.beDebug()){
             location.debugT("Parameters:");
             location.debugT("ext_configuration config " + config.get_name());
             location.debugT("String[] contextNames: ");
             if (contextNames != null) {
                 for (int i = 0; i<contextNames.length; i++){
                     location.debugT("  contextNames["+i+"] " + contextNames[i]);
                 }
             }
             location.debugT("String[] contextValues: ");
             if (contextNames != null) {
                 for (int i = 0; i<contextValues.length; i++){
                     location.debugT("  contextValues["+i+"] " + contextValues[i]);
                 }
             }
             location.debugT("String date " + date);
         }
         Configuration configuration = createConfiguration(null, config, contextNames, contextValues, date);
         if (location.beDebug()){
             location.debugT("return configuation " + configuration.getId());
         }
         location.exiting();
         return configuration;
	}

	public abstract Configuration createConfiguration(String language, ext_configuration config,
										     String[] contextNames, String[] contextValues, String date) throws IPCException;


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCClient#createImportConfiguration(com.sap.sce.kbrt.ext_configuration, java.lang.String[], java.lang.String[])
     */
    public abstract ImportConfiguration createImportConfiguration(ext_configuration config,
                                         String[] contextNames, String[] contextValues) throws IPCException;


    public void close(){
        location.entering("close()");
        closed = true;
        location.exiting();
    }

    public boolean isClosed(){
        location.entering("isClosed()");
        if (location.beDebug()){
            location.debugT("return closed " + closed);
        }
        location.exiting();
        return closed;
    }


    /**
     * Returns a list of all available Knowlegebases for a given knowledge base name pattern
     */
    public abstract List getKnowledgeBases(String kbToFind);



	public IPCItemReference getItemReference() {
        location.entering("getItemReference()");
        IPCItemReference ipcItemReference = ipcSession.getClient().getItemReference();
        if (location.beDebug()){
            location.debugT("return ipcItemReference " + ipcItemReference);
        }
        location.exiting();
		return ipcItemReference;
	}

	public IPCConfigReference getConfigReference() {
        location.entering("getConfigReference()");
        IPCConfigReference ipcConfigReference = ipcSession.getClient().getConfigReference();
        if (location.beDebug()){
            location.debugT("return ipcConfigReference " + ipcConfigReference);
        }
        location.exiting();
		return ipcConfigReference;
	}

	public abstract void synchronizeWithServer() throws IPCException;

	/**
	 * resets (or rather creates freshly) a new session in this client. Use this to sync
	 * the client after a server-side change of the session by another client.
	 */
	public void resetSessionToServer() throws IPCException {
        location.entering("resetSessionToServer()");
		IPCItemReference ref = getItemReference();
//		ipcSession = factory.newIPCSession(this, ref);
        ipcSession.syncWithServer();
        location.exiting();
	}

	/**
	 * Adds a new client log listener to this client. The IPCClient will register this listener
	 * at its internal client object.
	 */
	public void addLogListener(ClientLogListener l) {
        location.entering("addLogListener(ClientLogListener l)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (l!=null){
                location.debugT("ClientLogListener l " + l.toString());
            }
            else location.debugT("ClientLogListener l null");
        }
        ipcSession.getClient().addLogListener(l);
        location.exiting();
	}

	/**
	 * Removes a client log listener from this client. Doesn't perform any operation if l
	 * has not previously been added by addLogListener.
	 */
	public void removeLogListener(ClientLogListener l) {
        location.entering("removeLogListener(ClientLogListener l)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (l!=null){
                location.debugT("ClientLogListener l " + l.toString());
            }
            else location.debugT("ClientLogListener l null");
        }
        ipcSession.getClient().removeLogListener(l);
        location.exiting();
	}

	/**
	 * This method should simplify using the SAT-mechanismus startComponent(...) in
	 * nested java-command-blocks. The Method use the SAT-method startComponent(...).
	 *
	 * @param recordString : the String which you want record in the tracefile
	 */
	protected void startComponentInternalUse(String recordString)
	{
		if(_monitor != null)
			_monitor.startComponent(SAT_STRING+recordString);
	}

	/**
	 * This method should simplify using the SAT-mechanismus startComponent(...) in
	 * nested java-command-blocks. The Method use the SAT-method startComponent(...).
	 *
	 * @param recordString : the String which you want record in the tracefile
	 */
	protected void endComponentInternalUse(String recordString)
	{
		if(_monitor != null)
			_monitor.endComponent(SAT_STRING+recordString);
	}

	//BD 19062003: Changelist 42773 IPC 4.0 SP2 SAT
	//new-->
	/**
	 * Sets the IMonitor for the SAT-Tracing
	 * @param IMonitor
	 */
	public void setJARMMonitor(IMonitor monitor)
    {
	    location.entering("setJARMMonitor(IMonitor monitor)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (monitor!=null){
                location.debugT("IMonitor monitor " + monitor.toString());
            }
            else location.debugT("IMonitor monitor null");
        }
        _monitor = monitor;
        location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCClient#getJARMMonitor()
	 */
	public IMonitor getJARMMonitor()
	{
        location.entering("getJARMMonitor()");
        if (location.beDebug()){
            if (_monitor!=null){
                location.debugT("return _monitor " + _monitor.toString());
            }
            else location.debugT("return _monitor null");
        }
        location.exiting();
		return _monitor;
        
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCClient#getUIModelGUID(com.sap.spc.remote.client.object.KnowledgeBase, java.lang.String, java.lang.String)
     */
    public abstract String getUIModelGUID(
        KnowledgeBase kb,
        String scenario,
        String roleKey);

}
