package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemFilter;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.tc.logging.Location;
import com.sap.isa.businessobject.BusinessObjectBase;

public abstract class DefaultIPCDocument extends BusinessObjectBase implements IPCDocument {

	//Used by application level caching
	protected boolean cacheIsDirty = true;

    protected IPCClientObjectFactory factory = IPCClientObjectFactory.getInstance();
	protected DefaultIPCSession _session;
	
	protected String    _documentId;
    private int sequenceNumber = 0;
	
	/*** Application Cahcing variables starts *****/
	/**
	 * The root items of this document.
	 */
	protected Vector    _items;
	/**
	 * either null or a copy of _items (to speed up access)
	 */
	protected String  _netValue;
	protected String  _netValueWithoutFreight;
	protected String  _taxValue;
	protected String  _grossValue;
	protected String  _currencyUnit;
	protected String  _freight;
	
	protected DefaultIPCDocumentProperties _properties;
	
	/*** Application Cahcing variables ends *****/
	
    protected DefaultIPCPricingConditionSet _pricingConditionSet;
    
	protected IPCItem[] _itemsCache;
	
	protected boolean 	_isValueFormatting = true;
	protected boolean   _isClosed;

    public static final String CRM = "CRM";
    public static final String ERP = "V";    
    protected String    _application = CRM;

    protected TTEDocument _tteDocument;

    protected static final Location location = ResourceAccessor.getLocation(DefaultIPCDocument.class);

    protected abstract void initializeItems() throws IPCException;

	protected abstract void createDocument() throws IPCException;

    public String getId() {
        location.entering("getId()");
        if (location.beDebug()){
            location.debugT("return _documentId " + _documentId);
        }
        location.exiting();
		return _documentId;
    }

	public synchronized void pricing() throws IPCException {
       location.entering("pricing()");
       Enumeration e=_items.elements();
       while (e.hasMoreElements()) {
           IPCItem current = (IPCItem)e.nextElement();
           ((DefaultIPCPricingConditionSet)current.getPricingConditions()).setInvalid();
       }        
       location.exiting();
	}

    public String getDocumentCountry() throws IPCException {
        location.entering("getDocumentCountry()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        String country = _properties.getCountry();
        if (location.beDebug()){
            location.debugT("return country " + country);
        }
        location.exiting();
       	return country;
    }

	/* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocument#getItem(java.lang.String)
     */
    public IPCItem getItem(String itemId) throws IPCException {
        location.entering("getItem(String itemId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String itemId " + itemId);
        }
    	if (isCacheDirty()) {
    		syncWithServer();
    	}
		return getItemFromCache(itemId);
	}

	public IPCItem getItemFromCache(String itemId) {
		IPCItem[] allItems = _getAllItems(false);
		IPCItem foundItem = null;
		for (int i=0; i<allItems.length; i++){
		    IPCItem currentItem = allItems[i];
		    if (currentItem.getItemId().equals(itemId)){
		        foundItem = currentItem;
		    }
		}
		if (location.beDebug()){
		    if(foundItem != null) {
		        location.debugT("return foundItem " + foundItem.getItemId());
		    }
		    else {
		        location.debugT("return foundItem: NULL");
		    }
		    
		}
		location.exiting();
		return foundItem;
	}

//	/**
//	 * Returns a root item by itemId without contacting the server. If it's not on the client, null will
//	 * be returned (even if it might be on the server).
//	 */
//	IPCItem _getItem(String itemId) {
//		// this is no frequently used method (I hope :-) ), so we just walk through the Vector
//		Enumeration e=_items.elements();
//		IPCItem foundItem = null;
//		while (e.hasMoreElements() && foundItem == null) {
//			IPCItem current = (IPCItem)e.nextElement();
//			if (current.getItemId().equals(itemId))
//				foundItem = current;
//		}
//		return foundItem;
//	}

	/**
	 * Returns any item this document contains without contacting the server. If it's not on the client, null will
	 * be returned (even if it might be on the server).
	 */
	public DefaultIPCItem _getItemRecursive(String itemId) {
        location.entering("_getItemRecursive(String itemId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String itemId " + itemId);
        }
		Enumeration e=_items.elements();
		DefaultIPCItem foundItem = null;
		while (e.hasMoreElements() && foundItem == null) {
			DefaultIPCItem current = (DefaultIPCItem)e.nextElement();
			if (current.getItemId().equals(itemId))
				foundItem = current;
			else
				foundItem = current._getChildRecursive(itemId);
		}
        if (location.beDebug()){
            if (foundItem != null) {
                location.debugT("return foundItem " + foundItem.getItemId());
            }
            else location.debugT("return foundItem: null ");
        }
        location.exiting();
		return foundItem;
	}

	public IPCItem newItem(IPCItemProperties props) throws IPCException {
        location.entering("newItem(IPCItemProperties props)");
        if (location.beDebug()) {
            if (props!=null){
                location.debugT("IPCItemProperties props " + props.toString());
            }
            else location.debugT("IPCItemProperties props null");
        }
		IPCItem item = factory.newIPCItem(this, props);
		_items.addElement(item);
		_itemsCache = null;
        if (location.beDebug()){
            location.debugT("return item " + item.getItemId());
        }
        location.exiting();
		return item;
    }

	public synchronized IPCItem[] newItems(IPCItemProperties[] props) throws IPCException {
        location.entering("newItems(IPCItemProperties[] props)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (props != null) {
                location.debugT("IPCItemProperties[] props)");
                for (int i = 0; i<props.length; i++){
                    location.debugT("  props["+i+"] "+props[i]);
                }
            }
        }
        
		boolean multiCallPossible = true;
		int index=0;
		while (multiCallPossible && index < props.length) {
			if (props[index].getConfig(null) != null) {
				multiCallPossible = false;
			}
			else
				index++;
		}

		if (multiCallPossible) {
			IPCItem[] result = factory.newIPCItems(this, props);
			_itemsCache = null;
            if (location.beDebug()){
                if (result != null) {
                location.debugT("return result");
                for (int i = 0; i<result.length; i++){
                    location.debugT("  result["+i+"] "+result[i]);
                }
                }
            }
            location.exiting();
			return result;
		}
		else {
			IPCItem[] result = new IPCItem[props.length];
			for (int i=0; i<props.length; i++) {
				result[i] = newItem(props[i]);
			}
            if (location.beDebug()){
                if (result != null) {
                location.debugT("return result");
                for (int i = 0; i<result.length; i++){
                    location.debugT("  result["+i+"] "+result[i]);
                }
                }
            }
            location.exiting();
			return result;
		}
	}

	public void addItem(IPCItem item) { 
        location.entering("addItem(IPCItem item)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCItem item " + item.getItemId());
        }
       //Doc can have two items with same id but with diff units. So directly add to _items, don't check for existence.
		_items.addElement(item);
		_itemsCache = null;
        location.exiting();
	}

	public IPCSession getSession() {
        location.entering("getSession()");
        if (location.beDebug()){
            location.debugT("return _session " + _session.getSessionId());
        }
        location.exiting();
        return _session;
    }

    public String getGrossValue() throws IPCException {
        location.entering("getGrossValue()");
        if (isCacheDirty()){
            syncWithServer();
        }   
        if (location.beDebug()){
            location.debugT("return _grossValue " + _grossValue);
        }  
        location.exiting();   
		return _grossValue;
    }


    public String getNetValue() throws IPCException {
        location.entering("getNetValue()");
        if (isCacheDirty()){
            syncWithServer();
        }  
        if (location.beDebug()){
            location.debugT("return _netValue " + _netValue);
        }  
        location.exiting();      
		return _netValue;
    }


    public String getNetValueWithoutFreight() throws IPCException {
        location.entering("getNetValueWithoutFreight()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        if (location.beDebug()){
            location.debugT("return _netValue " + _netValue);
        }  
        location.exiting();
 		return _netValueWithoutFreight;
    }


	public String getTaxValue() throws IPCException {
        location.entering("getTaxValue()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        if (location.beDebug()){
            location.debugT("return _taxValue " + _taxValue);
        }  
        location.exiting();
		return _taxValue;
    }

    public String getFreight() throws IPCException {
        location.entering("getFreight()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        if (location.beDebug()){
            location.debugT("return _freight " + _freight);
        }  
        location.exiting();
        return _freight;
    }

	public String getDocumentLanguage() throws IPCException {
        location.entering("getDocumentLanguage()");
        if (isCacheDirty()){
            syncWithServer();
        } 
        String language =  _properties.getLanguage();
        if (location.beDebug()){
            location.debugT("return language " + language);
        }  
        location.exiting();
		return language;
    }

	// attribute environment methods

	/**
	 * Returns the header attribute environment of this document
	 */
	public String[] getHeaderAttributeEnvironment() throws IPCException {
        location.entering("getHeaderAttributeEnvironment()");
        if (isCacheDirty()){
            syncWithServer();
        }   
        String[] headerAttributeEnvironment =  _properties.getHeaderAttributeEnvironment();    
        if (location.beDebug()){
            if (headerAttributeEnvironment != null) {
            location.debugT("return headerAttributeEnvironment");
            for (int i = 0; i<headerAttributeEnvironment.length; i++){
                location.debugT("  result["+i+"] "+headerAttributeEnvironment[i]);
            }
            }
        }
        location.exiting();
		return headerAttributeEnvironment;
    }


	/**
	 * Returns the header attribute value for a given header attribute key.
	 * Please note that this method has run time O(n), where n is the number
	 * of keys. If you want to do repeated accesses on header attributes,
	 * it is better to use getHeaderAttributes and work on the resulting
	 * Map.
	 */
    public String getHeaderAttributeBinding(String s) throws IPCException {
        location.entering("getHeaderAttributeBinding(String s)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String s " + s);
        }
        if (isCacheDirty()){
            syncWithServer();
        }  
        String headerAttribute = _properties.getHeaderAttributeBinding(s);
        if (location.beDebug()){
            location.debugT("return headerAttribute " + headerAttribute);
        }  
        location.exiting();  
		return headerAttribute;
    }


	/**
	 * Returns a Map that maps header attribute keys to header
	 * attribute values. The Map will be freshly created so it will
	 * not be updated automatically if the server-side state changes.
	 */
	public Map getHeaderAttributes() throws IPCException {
        location.entering("getHeaderAttributes()");
        if (isCacheDirty()){
            syncWithServer();
        }    
        Map headerAttributes = _properties.getHeaderAttributes();    
        if (location.beDebug()){
            if (headerAttributes!=null){
                location.debugT("return headerAttributes " + headerAttributes.toString());
            }
            else location.debugT("return headerAttributes null");
        }  
        location.exiting();
		return headerAttributes;
	}


	/**
	 * Sets the header attribute binding.
	 */
	public abstract void setHeaderAttributeBindings(String[] keys, String[] values) throws IPCException;

	public String getExternalId() throws IPCException {
        location.entering("getExternalId()");
        if (isCacheDirty()){
            syncWithServer();
        }
        String externalId = _properties.getExternalId();
        if (location.beDebug()){
            location.debugT("return externalId " + externalId);
        }  
        location.exiting();        
	    return externalId;
    }

	public char getEditMode() throws IPCException {
        location.entering("getEditMode()");
        if (isCacheDirty()){
            syncWithServer();
        } 
        char editMode = _properties.getEditMode();   
        if (location.beDebug()){
            location.debugT("return editMode " + editMode);
        }  
        location.exiting();
	    return editMode;
    }

    public abstract void changeDocumentCurrency(String currencyUnit) throws IPCException;

	public Locale getLocale() throws IPCException {
        location.entering("getLocale()");
        if (isCacheDirty()){
            syncWithServer();
        }
        Locale locale =  new Locale(_properties.getLanguage().toLowerCase(),
                                    _properties.getCountry().toUpperCase());       
        if (location.beDebug()){
            if (locale!=null){
                location.debugT("return locale " + locale.toString());
            }
            else location.debugT("return locale null");
        }  
        location.exiting();
        return locale;
    }

    public String getDocumentCurrency() throws IPCException {
        location.entering("getDocumentCurrency()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        if (location.beDebug()){
            location.debugT("return _currencyUnit " + _currencyUnit);
        }  
        location.exiting();
		return _currencyUnit;
    }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#getItems()
	 */
	public IPCItem[] getItems() {
        location.entering("getItems()");
        IPCItem[] ipcItems =  _getItems(true);
        if (location.beDebug()){
            if (ipcItems != null) {
            location.debugT("return ipcItems");
            for (int i = 0; i<ipcItems.length; i++){
                location.debugT("  ipcItems["+i+"] "+ipcItems[i].getItemId());
            }
            }
        }
        location.exiting();
		return ipcItems;
	}
	
    /**
	 *  Client Object Layer internal Use. 
	 */
	protected IPCItem[] _getItems(boolean isCacheCheckRequired) {
        if(isCacheCheckRequired){		
	        if (isCacheDirty()){
	            syncWithServer();
	        }        
		}
		if (_itemsCache == null) {
			_itemsCache = new IPCItem[_items.size()];
			_items.copyInto(_itemsCache);
		}
		return _itemsCache;
    }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#getAllItems()
	 */
	public IPCItem[] getAllItems() throws IPCException {
		location.entering("getAllItems");
        IPCItem[] allIPCItems =  _getAllItems(true);
        if (location.beDebug()){
            if (allIPCItems != null) {
            location.debugT("return allIPCItems");
            for (int i = 0; i<allIPCItems.length; i++){
                location.debugT("  allIPCItems["+i+"] "+allIPCItems[i].getItemId());
            }
            }
        }
        location.exiting();
        return allIPCItems;
        
	}
	
	/**
	 *  Client Object Layer internal Use. 
	 */
	public IPCItem[] _getAllItems(boolean isCacheCheckRequired) throws IPCException {
		location.entering("_getAllItems(boolean isCacheCheckRequired)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean isCacheCheckRequired " + isCacheCheckRequired);
        }
        if(isCacheCheckRequired){		
			if (isCacheDirty()){
				syncWithServer();
			}        
		}
		ArrayList result = new ArrayList();
		IPCItem[] items = _getItems(false);
		for (int i=0; i<items.length; i++) {
			_collectAllItems(items[i], result);
		}
		IPCItem[] resultArray = new IPCItem[result.size()];
		result.toArray(resultArray);
        if (location.beDebug()){
            if (resultArray != null) {
            location.debugT("return resultArray");
            for (int i = 0; i<resultArray.length; i++){
                location.debugT("  resultArray["+i+"] "+resultArray[i].getItemId());
            }
            }
        }
        location.exiting();
		return resultArray;
	}

	public IPCItem[] getItems(IPCItemFilter ipcItemFilter) {
        location.entering("getItems(IPCItemFilter ipcItemFilter)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (ipcItemFilter!=null){
                location.debugT("IPCItemFilter ipcItemFilter " + ipcItemFilter.toString());
            }
            else location.debugT("IPCItemFilter ipcItemFilter null");
        }
        IPCItem[] items = ipcItemFilter.filterItems(getItems());
        if (location.beDebug()){
            if (items != null) {
                location.debugT("return items");
                for (int i = 0; i<items.length; i++){
                    location.debugT("  items["+i+"] "+items[i].getItemId());
                }
            }
        }
        location.exiting();
		return items;
	}


	protected void _collectAllItems(IPCItem item, ArrayList result) throws IPCException {
		result.add(item);
		IPCItem[] sub = ((DefaultIPCItem)item)._getChildren(false);
		for (int i=0; i<sub.length; i++) {
			_collectAllItems(sub[i], result);
		}
	}


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCDocument#getDocumentProperties()
	 */
	public IPCDocumentProperties getDocumentProperties() {
        location.entering("getDocumentProperties()");
        IPCDocumentProperties ipcDocumentProp = _getDocumentProperties(true);
        if (location.beDebug()){
            location.debugT("return ipcDocumentProp " + ipcDocumentProp);
        }  
        location.exiting();
		return ipcDocumentProp;
	}
	
	/**
	 *  Client Object Layer internal Use. 
	 */
   public IPCDocumentProperties _getDocumentProperties(boolean isCacheCheckRequired) {
       location.entering("_getDocumentProperties(boolean isCacheCheckRequired)");
       if (location.beDebug()){
           location.debugT("Parameters:");
           location.debugT("boolean isCacheCheckRequired " + isCacheCheckRequired);
       }
	   if(isCacheCheckRequired){
			if (isCacheDirty()){
				syncWithServer();
			}        
	   }
       if (location.beDebug()){
           location.debugT("return _properties " + _properties);
       }  
       location.exiting();
	   return _properties;
    }

    public abstract void removeItem(IPCItem item) throws IPCException;

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocument#removeItemExisting(com.sap.spc.remote.client.object.IPCItem)
     */
    public boolean removeItemExisting(IPCItem item) throws IPCException {
        location.entering("removeItemExisting(IPCItem item)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCItem item " + item.getItemId());
        }

        boolean partOfDocument = _items.removeElement(item);
        if (partOfDocument){
             location.debugT("Item was part of the document. We call AP to remove it on the server.");
             removeItem(item);
        }
        else {
            location.debugT("Item was NOT part of the document. We DON'T call AP.");
        }
        location.exiting();
        return partOfDocument;
    }

	/**
	 * Removes a number of items from the document in a single step. This method uses a single
	 * IPC call to do its job and is much faster (for more than one item) than several removeItem
	 * calls.
	 */
	public void removeItems(IPCItem[] items) throws IPCException {
        location.entering("removeItems(IPCItem[] items)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCItem[] items ");
            if (items != null) {
                for (int i = 0; i<items.length; i++){
                    location.debugT("  items["+i+"] "+items[i].getItemId());
                }
            }
        }
		// convert items to a set for log(n) access
		HashSet itemSet = new HashSet();
		for (int i=0; i<items.length; i++) {
			itemSet.add(items[i]);
		}

		// remove all items in the item set from _items. Since removing while iterating
		// is ugly we create a new vector with the remaining items and copy.
		Vector remaining = new Vector(_items.size());
		for (Enumeration e=_items.elements(); e.hasMoreElements();) {
			IPCItem item = (IPCItem)e.nextElement();
			if (!itemSet.contains(item)) {
				remaining.addElement(item);
			}
		}
		_items = remaining;
		_itemsCache = null;

		_removeItemsOnServer(items);
        location.exiting();
	}


    protected abstract void _removeItemsOnServer(IPCItem[] items) throws IPCException;

	/**
	 * Removes all items of this document. Since right now there is no command RemoveAllItems,
	 * this just calls RemoveItems.
	 */
	public void removeAllItems() throws IPCException {
        location.entering("removeAllItems()");
		// the ugly effect of this is that if a client wants to remove all items without having
		// accessed them before, the client will first do a big sequence of GetItemInfo
		IPCItem[] items = getItems();
		_items = new Vector();
		_itemsCache = null;

		_removeItemsOnServer(items);
        location.exiting();
	}


    public abstract void changeLocalCurrency(String localCurrency) throws IPCException;

	public abstract void changePricingProcedure(String pricingProcedure) throws IPCException;

    public abstract void changeSalesOrganisation(String salesOrganisation) throws IPCException;

    public abstract void changeDistributionChannel(String distributionChannel) throws IPCException;

    public String getPricingProcedure() throws IPCException {
        location.entering("getPricingProcedure()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        String pricingProcedure = _properties.getPricingProcedure();
        if (location.beDebug()){
            location.debugT("return pricingProcedure " + pricingProcedure);
        }  
        location.exiting();
        return pricingProcedure;
    }

    public String getLocalCurrency() throws IPCException {
        location.entering("getLocalCurrency()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        String localCurrency = _properties.getLocalCurrency();
        if (location.beDebug()){
            location.debugT("return localCurrency " + localCurrency);
        }  
        location.exiting();
        return localCurrency;
	}

	/**
	 * Returns the language of the document if the client that created the
	 * document has passed the language.
	 * Otherwise it returns the language of the session.
	 */
	public String getLanguage() throws IPCException {
        location.entering("getLanguage()");
		String result =  _properties.getLanguage();
		if (result == null || result.equals("")) {
			String language = _session.getLanguage();
            if (location.beDebug()){
                location.debugT("return language " + language);
            }  
            location.exiting();
            return language;
		}else {
            if (location.beDebug()){
                location.debugT("return result " + result);
            }  
            location.exiting();
			return result;
		}
    }

    public void setValueFormatting(boolean valueFormatting) throws IPCException{
        location.entering("setValueFormatting(boolean valueFormatting)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean valueFormatting " + valueFormatting);
        }
    	if (_isValueFormatting != valueFormatting) {
		    _isValueFormatting = valueFormatting;
		    //initializePriceValues();
    	}
        location.exiting();
    }

    public boolean isValueFormatting() {
        location.entering("isValueFormatting()");
        if (location.beDebug()){
            location.debugT("return _isValueFormatting " + _isValueFormatting);
        }  
        location.exiting();
    	return _isValueFormatting;
    }

    public String getCountry() throws IPCException {
        location.entering("getCountry()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        String country = _properties.getCountry();
        if (location.beDebug()){
            location.debugT("return country " + country);
        }  
        location.exiting();
		return country;
    }

	public boolean isGroupConditionProcessingEnabled() {
        location.entering("isGroupConditionProcessingEnabled()");
        if (isCacheDirty()){
            syncWithServer();
        } 
        boolean isGroupConditionProcessingEnabled = _properties.isGroupConditionProcessingEnabled();      
        if (location.beDebug()){
            location.debugT("return isGroupConditionProcessingEnabled " + isGroupConditionProcessingEnabled);
        }  
        location.exiting();
		return isGroupConditionProcessingEnabled;
	}

	public void setGroupConditionProcessingEnabled(boolean enabled) {

       throw new java.lang.UnsupportedOperationException("Method setGroupConditionProcessingEnabled not implemented.");
 	}

	public  IPCPricingConditionSet getPricingConditions() throws IPCException{
        location.entering("getPricingConditions()");
        if (isCacheDirty()){
            syncWithServer();
        }
        if (location.beDebug()){
            location.debugT("return _pricingConditionSet " + _pricingConditionSet);
        }  
        location.exiting();    
        return _pricingConditionSet;
    }

	public void close() {
        location.entering("close()");
		for (Enumeration e=_items.elements(); e.hasMoreElements();) {
			IPCItem item = (IPCItem)e.nextElement();
			item.close();
		}
		_isClosed = true;
        location.exiting();
	}

	public boolean isClosed() {
        location.entering("isClosed()");
        if (isCacheDirty()){
            syncWithServer();
        }        
        if (location.beDebug()){
            location.debugT("return _isClosed " + _isClosed);
        }  
        location.exiting();    
		return _isClosed;
	}

	public boolean isRelevant(String ipcCommand) {
        location.entering("isRelevant(String ipcCommand)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String ipcCommand " + ipcCommand);
        }
		// all my commands are always relevant
        if (location.beDebug()){
            location.debugT("return true");
        }  
        location.exiting();
		return true;
	}

	public abstract void syncCommonProperties() throws IPCException;

	/**
	 * Computes a children relationship from a parent relationship in a tree.
	 * @param   ids a number of ids
	 * @param   parents a number of ids, where parents[i] is the id of the parent of object i.
	 * @return  a Map that maps ids to Lists of ids, where the keys are ids, and the values
	 *          are List objects that contain all children of the given object. The order in this list is the same
	 *          as the order of the items.
	 */
	public static final Map getChildrenMap(String[] itemIds, String[] parentIds) {
        location.entering("getChildrenMap(String[] itemIds, String[] parentIds)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String[] itemIds ");
            if (itemIds != null) {
                for (int i = 0; i<itemIds.length; i++){
                    location.debugT("  itemIds["+i+"] "+itemIds[i]);
                }
            }
            location.debugT("String[] parentIds ");
            if (parentIds != null) {
                for (int i = 0; i<parentIds.length; i++){
                    location.debugT("  parentIds["+i+"] "+parentIds[i]);
                }
            }
            
        }
		HashMap result = new HashMap();
		for (int i=0; i<parentIds.length; i++) {
			List value = (List)result.get(parentIds[i]);
			if (value == null) {
				value = new ArrayList();
				result.put(parentIds[i], value);
			}
			value.add(itemIds[i]);
		}
        if (location.beDebug()){
            if (result != null) {
                location.debugT("return result " + result.toString());
            }
            else location.debugT("return result null");
        }
        location.exiting();
		return result;
	}
    
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String result = null;
		result = super.toString();
		result += "session:"
			+ _session
			+ "\ndocumentId:"
			+ _documentId
			+ "\nnetValue:"
			+ _netValue
			+ "\nnetValueWithoutFreight:"
			+ _netValueWithoutFreight
			+ "\ntaxValue:"
			+ _taxValue
			+ "\ngrossValue:"
			+ _grossValue
			+ "\ncurrencyUnit:"
			+ _currencyUnit
			+ "\nfreight:"
			+ this._freight
			+ "\npricingConditionSet:"
			+ this._pricingConditionSet
			+ "\nisValueFormatting:"
			+ this._isValueFormatting
			+ "\nisClosed:"
			+ this._isClosed;

		return result;
	}

	public boolean isCacheDirty(){
        location.entering("isCacheDirty()");
        if (location.beDebug()){
            location.debugT("return cacheIsDirty " + cacheIsDirty);
        }
        location.exiting();
		return cacheIsDirty;
	}
		
	/**
	 * Sets Cache as dirty.
	 */
	public abstract void setCacheDirty();

    /**
     * Syncs the client representation of the Document with the information on the server.
     * @throws IPCException
     */		
	public abstract void syncWithServer() throws IPCException;
	
	/**
	 * If customers whish to add functionality to the syncWithServerMethod, the
	 * should overwrite this method. The method is called at the end of synchWithServer
	 * method execution.
	 * @throws IPCException
	 */
	public abstract void customerExitSyncWithServer() throws IPCException;
	
	public abstract  String[] getTTEAnalysisData() throws IPCException;
    
    /**
     * @return the application we are running in ("CRM" for CRM or "V" for ERP)
     */
    public String getApplication() {
        location.entering("getApplication()");
        if (location.beDebug()){
            location.debugT("return _application " + _application);
        }
        location.exiting();
        return _application;
    }

    /**
     * @param string
     */
    public void setApplication(String string) {
        location.entering("setApplication(String string)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String string " + string);
        }
        _application = string;
        location.exiting();
    }

	/**
	 * For IPC Client Object Layer Use only
	 */
	protected void _setDocFlagToSync() throws IPCException {
	   cacheIsDirty = false;
	}
	
	/**
	 * For IPC Client Object Layer Use only
	 */
	protected void _setDocFlagToDirty() {
		cacheIsDirty = true;
	}

	protected void _setDocItemsFlagToSync() throws IPCException {
//		This code updated only root items but not sub-items..		
//	   Enumeration e=_items.elements();
//	   while (e.hasMoreElements()) {
//			DefaultIPCItem current = (DefaultIPCItem)e.nextElement();
//			current._setItemFlagToSync();
//	   }
	   IPCItem[] allItems = _getAllItems(false); //Returns root items and sub-items also without checking cache synk status.
	   int len = allItems.length;
	   for (int i=0; i <len;i++){
		((DefaultIPCItem)allItems[i])._setItemFlagToSync();
	   }
	}
	
	protected void _setDocItemsFlagToDirty() throws IPCException {
//		This code updated only root items but not sub-items..		
//	   Enumeration e=_items.elements();
//	   while (e.hasMoreElements()) {
//			DefaultIPCItem current = (DefaultIPCItem)e.nextElement();
//			current._setItemFlagToDirty();
//	   }
	   IPCItem[] allItems = _getAllItems(false); //Returns root items and sub-items also without checking cache synk status.
	   int len = allItems.length;
	   for (int i=0; i <len;i++){
		((DefaultIPCItem)allItems[i])._setItemFlagToDirty();
	   }
	}
	
    public TTEDocument getTteDocument() {
        return _tteDocument;
    }

    public void setTteDocument(TTEDocument document) {
        _tteDocument = document;
    }
    
    /**
     * Returns a sequence number starting with 0. Each call of this method increments
     * the sequence number.
     * @return sequence number
     */
    public int getSequenceNo(){
        return sequenceNumber++;
    }

}
