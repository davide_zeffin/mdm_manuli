/*
 * Created on Dec 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.rfc;

import java.math.BigDecimal;
import java.util.Hashtable;

import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.imp.DefaultIPCPricingCondition;
import com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet;
//import com.sap.spc.remote.shared.command.ChangePricingConditions;
import com.sap.spc.remote.client.rfc.function.SpcChangePricingConditions;
/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RfcDefaultIPCPricingCondition
	extends DefaultIPCPricingCondition
	implements IPCPricingCondition {

		/**
		 * Constructs the pricing condition.
		 */
		protected RfcDefaultIPCPricingCondition( DefaultIPCPricingConditionSet pricingConditionSet,
												String  stepNo,
												String  counter,
												String  headerCounter,
												String  conditionTypeName,
												String  description,
												String  conditionRate,
												String  conditionCurrency,
												String  pricingUnitValue,
												String  pricingUnitUnit,
												String  pricingUnitDimension,
												String  conditionValue,
												String  documentCurrency,
												String  conversionNumerator,
												String  conversionDenominator,
												String  conversionExponent,
												String  conditionBaseValue,

												String  exchangeRate,
												String  directExchangeRate,
												boolean isDirectExchangeRate,
												String  factor,
												boolean varcondFlag,
												String  variantConditionKey,
												String  variantFactor,
												String  variantConditionDescr,
												boolean  isStatistical,
												char     inactive,
												char     conditionClass,
												char     calculationType,
												char     conditionCategory,
												char     conditionControl,
												char     conditionOrigin,
												char     scaleType,

												boolean  changeOfRatesAllowed,
												boolean  changeOfPricingUnitsAllowed,
												boolean  changeOfValuesAllowed,
												boolean  deletionAllowed){
			this( pricingConditionSet,
				  stepNo,
				  counter,
				  conditionTypeName,
				  description,
				  conditionRate,
				  conditionCurrency,
				  pricingUnitValue,
				  pricingUnitUnit,
				  pricingUnitDimension,
				  conditionValue,
				  documentCurrency,
				  conversionNumerator,
				  conversionDenominator,
				  conversionExponent,
				  conditionBaseValue,
				  exchangeRate,
				  directExchangeRate,
				  isDirectExchangeRate,
				  factor,
				  varcondFlag,
				  variantConditionKey,
				  variantFactor,
				  variantConditionDescr,
				  isStatistical,
				  inactive,
				  conditionClass,
			      calculationType,
				  conditionCategory,
				  conditionControl,
				  conditionOrigin,
				  scaleType,
				  changeOfRatesAllowed,
				  changeOfPricingUnitsAllowed,
				  changeOfValuesAllowed,
				  deletionAllowed);
				_headerCounter = headerCounter ;
				}
	
		 protected RfcDefaultIPCPricingCondition( DefaultIPCPricingConditionSet pricingConditionSet,
										String  stepNo,
										String  counter,
										String  conditionTypeName,
										String  description,
										String  conditionRate,
										String  conditionCurrency,
										String  pricingUnitValue,
										String  pricingUnitUnit,
										String  pricingUnitDimension,
										String  conditionValue,
										String  documentCurrency,
										String  conversionNumerator,
										String  conversionDenominator,
										String  conversionExponent,
										String  conditionBaseValue,

										String  exchangeRate,
										String  directExchangeRate,
										boolean isDirectExchangeRate,
										String  factor,
										boolean varcondFlag,
										String  variantConditionKey,
										String  variantFactor,
                                        String  variantConditionDescr,
										boolean  isStatistical,
										char     inactive,
										char     conditionClass,
										char     calculationType,
										char     conditionCategory,
										char     conditionControl,
										char     conditionOrigin,
										char     scaleType,

										boolean  changeOfRatesAllowed,
										boolean  changeOfPricingUnitsAllowed,
										boolean  changeOfValuesAllowed,
										boolean  deletionAllowed){
			super(new TechKey(stepNo+counter));
			_pricingConditionSet = pricingConditionSet;
			_documentId = pricingConditionSet.getDocumentId();
			_itemId = pricingConditionSet.getItemId();
			_client = pricingConditionSet.getClient();
			_stepNo = stepNo;
			_counter = counter;
			_conditionTypeName = conditionTypeName;
			_description = description;
			_conditionRate = conditionRate;
			_conditionCurrency = conditionCurrency;
			_pricingUnitValue = pricingUnitValue;
			_pricingUnitUnit = pricingUnitUnit;
			_pricingUnitDimension = pricingUnitDimension;
			_conditionValue = conditionValue;
			_documentCurrency = documentCurrency;
			_conversionNumerator = conversionNumerator;
			_conversionDenominator = conversionDenominator;
			_conversionExponent = conversionExponent;
			_conditionBaseValue = conditionBaseValue;

			_exchangeRate = exchangeRate;
			_directExchangeRate = directExchangeRate;
			_isDirectExchangeRate = isDirectExchangeRate;
			_variantConditionKey = variantConditionKey;
			_factor = factor;
			_variantConditionFlag = varcondFlag;
			_variantFactor = variantFactor;
			_variantConditionDescr = variantConditionDescr;
			_isStatistical = isStatistical;
			_inactive = inactive;
			_conditionClass = conditionClass;
			_calculationType = calculationType;
			_conditionCategory = conditionCategory;
			_conditionControl = conditionControl;
			_conditionOrigin = conditionOrigin;
			_scaleType = scaleType;

			_changeOfRatesAllowed = changeOfRatesAllowed;
			_changeOfPricingUnitsAllowed = changeOfPricingUnitsAllowed;
			_changeOfValuesAllowed = changeOfValuesAllowed;
			_deletionAllowed = deletionAllowed;
			_initParameterQueue();
		}

	/**
	 * @param techKey
	 * techKey of a condition is step number of the pricing procedure and
	 * counter concatenated.
	 */
	protected RfcDefaultIPCPricingCondition(TechKey techKey) {
		super(techKey);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingCondition#_initParameterQueue()
	 */
	protected void _initParameterQueue() {
		if (_parameterQueue == null)
			_parameterQueue = new Hashtable(5);
		else
			_parameterQueue.clear();
		//insert default (null) values
//		_parameterQueue.put(ChangePricingConditions.CONDITION_VALUES, NULL_PARAMETER);
//		_parameterQueue.put(ChangePricingConditions.CONDITION_RATES, NULL_PARAMETER);
//		_parameterQueue.put(ChangePricingConditions.CONDITION_CURRENCIES, NULL_PARAMETER);
//		_parameterQueue.put(ChangePricingConditions.PRICING_UNIT_VALUES, NULL_PARAMETER);
//		_parameterQueue.put(ChangePricingConditions.PRICING_UNIT_UNITS, NULL_PARAMETER);
		_parameterQueue.put(SpcChangePricingConditions.COND_VALUE, NULL_PARAMETER);
		_parameterQueue.put(SpcChangePricingConditions.COND_RATE, NULL_PARAMETER);
		_parameterQueue.put(SpcChangePricingConditions.COND_CURRENCY, NULL_PARAMETER);
		_parameterQueue.put(SpcChangePricingConditions.COND_UNIT_VALUE, NULL_PARAMETER);
		_parameterQueue.put(SpcChangePricingConditions.COND_UNIT, NULL_PARAMETER);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setConditionRate(java.lang.String)
	 */
	public void setConditionRate(String conditionRate) {
		if (conditionRate.equals(_conditionRate))
			conditionRate = null;
//		_changeCondition(ChangePricingConditions.CONDITION_RATES, conditionRate);
		_changeCondition(SpcChangePricingConditions.COND_RATE, conditionRate);

	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setConditionCurrency(java.lang.String)
	 */
	public void setConditionCurrency(String conditionCurrency) {
		if (conditionCurrency.equals(_conditionCurrency))
			conditionCurrency = null;
//		_changeCondition(ChangePricingConditions.CONDITION_CURRENCIES, conditionCurrency);
		_changeCondition(SpcChangePricingConditions.COND_CURRENCY, conditionCurrency);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setPricingUnitValue(java.lang.String)
	 */
	public void setPricingUnitValue(String pricingUnitValue) {
		if (pricingUnitValue.equals(_pricingUnitValue))
			pricingUnitValue = null;
//		_changeCondition(ChangePricingConditions.PRICING_UNIT_VALUES, pricingUnitValue);
		_changeCondition(SpcChangePricingConditions.COND_UNIT_VALUE, pricingUnitValue);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setPricingUnitUnit(java.lang.String)
	 */
	public void setPricingUnitUnit(String pricingUnitUnit) {
		if (pricingUnitUnit.equals(_pricingUnitUnit))
			pricingUnitUnit = null;
//		_changeCondition(ChangePricingConditions.PRICING_UNIT_UNITS, pricingUnitUnit);
		_changeCondition(SpcChangePricingConditions.COND_UNIT, pricingUnitUnit);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setConditionValue(java.lang.String)
	 */
	public void setConditionValue(String conditionValue) {
		if (conditionValue.equals(_conditionValue))
			conditionValue = null;
//		_changeCondition(ChangePricingConditions.CONDITION_VALUES, conditionValue);
		_changeCondition(SpcChangePricingConditions.COND_VALUE, conditionValue);

	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#getExternalConditionValue(java.lang.String, java.lang.String)
	 */
	public String getExternalConditionValue(
		String decimalSeparator,
		String groupingSeparator) {
            RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
			String externalConditionValue = pc.convertCurrencyValueInternalToExternal(new BigDecimal(_conditionValue), _documentCurrency, _pricingConditionSet.getLanguage(), decimalSeparator, groupingSeparator );
			if (externalConditionValue == null || externalConditionValue.equals("")) {
				return _conditionValue;
			}else {
				return externalConditionValue;
			}
	}
	
	public String getExternalConditionRateValue(String decimalSeparator, String groupingSeparator) {
		String externalConditionRateValue;
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();        
		if (_conditionCurrency.equals("%")) {
			externalConditionRateValue = pc.convertValueInternalToExternal(new BigDecimal(_conditionRate), _conditionCurrency, _pricingConditionSet.getLanguage(), decimalSeparator, groupingSeparator );
		}else {
			externalConditionRateValue = pc.convertCurrencyValueInternalToExternal(new BigDecimal(_conditionRate), _conditionCurrency, _pricingConditionSet.getLanguage(), decimalSeparator, groupingSeparator );
		}
		if (externalConditionRateValue == null || externalConditionRateValue.equals("")) {
			return _conditionRate;
		}else {
			return externalConditionRateValue;
		}		
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setExternalPricingUnitValue(java.lang.String)
	 */
	public void setExternalPricingUnitValue(String externalPricingUnitValue, String decimalSeparator, String groupingSeparator) {
		BigDecimal internalPricingUnitValue;
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		if (externalPricingUnitValue != null) {
			internalPricingUnitValue = pc.convertValueExternalToInternal(externalPricingUnitValue, _pricingUnitUnit, _pricingConditionSet.getLanguage(), decimalSeparator, groupingSeparator);
		}else {
			internalPricingUnitValue = null;
		}
		_pricingUnitValue = internalPricingUnitValue.toString();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#getExternalPricingUnitValue()
	 */
	public String getExternalPricingUnitValue(String decimalSeparator, String groupingSeparator) {
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		String externalPricingUnitValue = pc.convertValueInternalToExternal(new BigDecimal(_pricingUnitValue), _pricingUnitUnit, _pricingConditionSet.getLanguage(), decimalSeparator, groupingSeparator );
		if (externalPricingUnitValue == null || externalPricingUnitValue.equals("")) {
			return _pricingUnitValue;
		}else {
			return externalPricingUnitValue;
		}		
	}

	/******* Method added for Manual Condition  *****/

	public void setConditionValue(String conditionValue, String decimalSeparator, String groupingSeparator) 
	{
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		String internalConditionValue = pc.convertCurrencyValueInternalToExternal(new BigDecimal(_conditionValue), _documentCurrency, _pricingConditionSet.getLanguage(), decimalSeparator, groupingSeparator );		
		BigDecimal paramInternalConditionValue = pc.convertCurrencyValueExternalToInternal(conditionValue, _documentCurrency, decimalSeparator, groupingSeparator );
		String paramExternalConditionValue = pc.convertCurrencyValueInternalToExternal(paramInternalConditionValue, _documentCurrency, decimalSeparator, groupingSeparator );
				
		if (paramExternalConditionValue.equals(internalConditionValue))					
			conditionValue = null;
		_changeCondition(SpcChangePricingConditions.COND_VALUE, conditionValue);

	}
	public void setConditionRate(String conditionRate, String decimalSeparator, String groupingSeparator) {
		
		RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		BigDecimal paramInternalConditionRate = pc.convertCurrencyValueExternalToInternal(conditionRate, _documentCurrency, decimalSeparator, groupingSeparator );

		String InternalConditionRate = paramInternalConditionRate.toString();
		
		if (InternalConditionRate.equals(_conditionRate))
			conditionRate = null;
			
		_changeCondition(SpcChangePricingConditions.COND_RATE, InternalConditionRate);
	}
	
	/******* Method added for Manual Condition  *****/
}
