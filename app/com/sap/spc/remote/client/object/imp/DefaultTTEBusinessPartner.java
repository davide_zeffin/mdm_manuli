package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.spc.remote.client.object.TTEBusinessPartner;

public class DefaultTTEBusinessPartner extends BusinessObjectBase implements TTEBusinessPartner {

    private String businessPartnerId;
    private String country;
    private String region;
    private String county;
    private String city;
    private String postalCode;
    private String geoCode;
    private String jurisdictionCode;
    private String exemptedRegion;
    private String belongsToLegEnt;
    private String taxability;
    private ArrayList taxTypes;
    private ArrayList taxGroups;
    private ArrayList taxNumbers;
    private ArrayList taxNumberTypes;

    DefaultTTEBusinessPartner(
        String businessPartnerId,
        String country,
        String region,
        String county,
        String city,
        String postalCode,
        String geoCode,
        String jurisidictionCode,
        String exemptedRegion,
        String belongsToLegEnt,
        String taxability,
        ArrayList taxTypes,
        ArrayList taxGroups,
        ArrayList taxNumbers,
        ArrayList taxNumberTypes)
    {
        this.businessPartnerId = businessPartnerId;
        this.country = country;
        this.region = region;
        this.county = county;
        this.city = city;
        this.postalCode = postalCode;
        this.geoCode = geoCode;
        this.jurisdictionCode = jurisidictionCode;
        this.exemptedRegion = exemptedRegion;
        this.belongsToLegEnt = belongsToLegEnt;
        this.taxability = taxability;
        if (taxTypes == null) {
            taxTypes = new ArrayList();
        }
        this.taxTypes = taxTypes;
        if (taxGroups == null) {
            taxGroups = new ArrayList();
        }
        this.taxGroups = taxGroups;
        if (taxNumbers == null) {
            taxNumbers = new ArrayList();
        }       
        this.taxNumbers = taxNumbers;
        if (taxNumberTypes == null) {
            taxNumberTypes = new ArrayList();
        }        
        this.taxNumberTypes = taxNumberTypes;        
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getTaxGroups()
     */
    public ArrayList getTaxGroups() {
        return taxGroups;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getTaxability()
     */
    public String getTaxability() {
        return taxability;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getTaxNumbers()
     */
    public ArrayList getTaxNumbers() {
        return taxNumbers;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getTaxNumberTypes()
     */
    public ArrayList getTaxNumberTypes() {
        return taxNumberTypes;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getTaxTypes()
     */
    public ArrayList getTaxTypes() {
        return taxTypes;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getBelongsToLegEnt()
     */
    public String getBelongsToLegEnt() {
        return belongsToLegEnt;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getBusinessPartnerId()
     */
    public String getBusinessPartnerId() {
        return businessPartnerId;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getCity()
     */
    public String getCity() {
        return city;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getCountry()
     */
    public String getCountry() {
        return country;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getCounty()
     */
    public String getCounty() {
        return county;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getExemptedRegion()
     */
    public String getExemptedRegion() {
        return exemptedRegion;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getGeoCode()
     */
    public String getGeoCode() {
        return geoCode;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getPostalCode()
     */
    public String getPostalCode() {
        return postalCode;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getRegion()
     */
    public String getRegion() {
        return region;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEBusinessPartner#getJurisdictionCode()
     */
    public String getJurisdictionCode() {
        return jurisdictionCode;
    }

}
