package com.sap.spc.remote.client.object.imp;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.KBProfile;
import com.sap.spc.remote.client.object.KnowledgeBase;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

public abstract class DefaultKBProfile extends BusinessObjectBase implements KBProfile {

    protected String name;
    protected int rootObjId;
    protected String rootObjName;
    protected int rootObjKbId;
    protected String uiName;
    protected KnowledgeBase kb;

    // logging
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(DefaultKBProfile.class);    


    /**
     * Constructor for DefaultKBProfile.
     */
    protected DefaultKBProfile(String name, KnowledgeBase kb) {
    	super(new TechKey(name + ((DefaultKnowledgeBase)kb).getTechKey()));
        this.name = name;
        this.kb = kb;
    }

    /**
    * @see com.sap.spc.remote.client.object.KBProfile#getName()
    */
    public String getName() {
        if (uiName == null) {
            init();
        }
        return name;
    }

    /**
     * @see com.sap.spc.remote.client.object.KBProfile#getUIName()
     */
    public String getUIName() {
        if (uiName == null) {
            init();
        }
        return uiName;
    }

    /**
     * @see com.sap.spc.remote.client.object.KBProfile#getRootObjName()
     */
    public String getRootObjName() {
        if (rootObjName == null) {
            init();
        }
        return rootObjName;
    }

    /**
     * Since the call to GET_PROFILES_OF_KB loads the knowledge base into memory,
     * the call of this command is deffered until read access to the profiles members.
     */
    protected abstract void init();    
}
