package com.sap.spc.remote.client.object.imp;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.OrderConfiguration;
import com.sap.spc.remote.client.object.ProductKey;
import com.sap.spc.remote.client.object.TTEItem;
import com.sap.tc.logging.Location;
import com.sap.isa.businessobject.BusinessObjectBase;

public abstract class DefaultIPCItem extends BusinessObjectBase implements IPCItem {

	//Used by application level caching
	protected boolean cacheIsDirty = true;	
	

    protected IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
	protected IClient _client;

	protected DefaultIPCDocument _document;
	protected DefaultIPCItem     _parent;

	protected Vector      _children;          // the direct children of this item (state last retrieved from the server)
	protected IPCItem[]   _childrenCopy;      // and the same as an array (lazily constructed)
	protected Map _childrenByInstanceId;
	protected String[]    _childrenIds;       // the item ids of the children as retrieved from the server.
	protected DefaultIPCItemProperties _props;
    protected ext_configuration initialConfiguration;
	protected Map _dynamicReturn;

	protected String _itemId;

	protected boolean _closed;
    protected DefaultIPCPricingConditionSet _pricingConditionSet;

	protected ConfigurationValue _config;
    
    protected TTEItem _tteItem;
    
    protected static final Location location = ResourceAccessor.getLocation(DefaultIPCItem.class);

	protected static void _addMapParamsToVector(Map t, Vector v, String nameParam, String valueParam) {
		if (t != null) {
			for (Iterator iter=t.keySet().iterator(); iter.hasNext();) {
				String key = (String)iter.next();
				String val = (String)t.get(key);
				v.addElement(nameParam);
				v.addElement(key);
				v.addElement(valueParam);
				v.addElement(val);
			}
		}
	}

	protected static void _addMapsParamsToVector(int ref, Map t, Vector v, String nameParam, String valueParam, String refParam) {
	    if (t != null) {
			for (Iterator iter=t.keySet().iterator(); iter.hasNext();) {
				String key = (String)iter.next();
				String val = (String)t.get(key);
				v.addElement(nameParam);
				v.addElement(key);
				v.addElement(valueParam);
				v.addElement(val);
				v.addElement(refParam);
				v.addElement(Integer.toString(ref));
			}
		}
	}


	protected static void _addBooleanParamToVector(boolean param, Vector v, String paramName) {
		v.addElement(paramName);
		v.addElement(param ? "Y" : "N");
	}

	public void addChild(IPCItem child) {
        location.entering("addChild(IPCItem child)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCItem child " + child.getItemId());
        }      
		if (_children == null) {
			_children = new Vector();
		}
		if (!_children.contains(child)) {
			_children.addElement(child);
			_childrenCopy = null; // force recomputation at next access
		}
        location.exiting();
	}

    public String getItemId() {
        location.entering("getItemId()");
        if (location.beDebug()) {
            location.debugT("return _itemId " + _itemId);
        }
        location.exiting();
		return _itemId;
    }

    public IPCItemProperties getItemProperties() {
        location.entering("getItemProperties()");
        if (isCacheDirty()){
            this.syncWithServer();
        }
        if (location.beDebug()) {
            location.debugT("return _props " + _props.toString());
        }
        location.exiting();       
		return _props;
    }

    public IPCItemProperties getItemPropertiesNoSync() {
        location.entering("getItemPropertiesNoSync()");
        location.exiting();       
        return _props;
    }

	public String getExternalId() throws IPCException {
        location.entering("getExternalId()");
        if (isCacheDirty()){
            this.syncWithServer();
        }      
        String externalId  =  _props.getExternalId();
        if (location.beDebug()) {
            location.debugT("return externalId " + externalId);
        }
        location.exiting();  
		return externalId;
    }

    public String getProductId() throws IPCException {
        location.entering("getProductId()");
		if (isCacheDirty()){
			this.syncWithServer();
		}        
        String productId  =  _props.getProductId();
        if (location.beDebug()) {
            location.debugT("return productId " + productId);
        }
        location.exiting(); 
		return productId;
    }

	/**
	 * This default implementation uses GetConfigItemInfo to access its data. This
	 * will fail for non-configurable products.
	 */
	public String getProductType() throws IPCException {
        location.entering("getProductType()");
		if (isCacheDirty()){
			this.syncWithServer();
		}        
        String productType  =  _props.getProductType();
        if (location.beDebug()) {
            location.debugT("return productType " + productType);
        }
        location.exiting();
		return productType;
	}

	/**
	 * This default implementation uses GetConfigItemInfo to access its data. This
	 * will fail for non-configurable products.
	 */
	public String getProductLogSys() throws IPCException {
        location.entering("getProductLogSys()");
		if (isCacheDirty()){
			this.syncWithServer();
		}        
        String productSysLog  =  _props.getProductLogSys();
        if (location.beDebug()) {
            location.debugT("return productSysLog " + productSysLog);
        }
        location.exiting();
		return productSysLog;
	}


    public String getProductGUID() throws IPCException {
        location.entering("getProductGUID()");
        if (isCacheDirty()){
			this.syncWithServer();
		}
        String productGUID  =  _props.getProductGuid();
        if (location.beDebug()) {
            location.debugT("return productGUID " + productGUID);
        }
        location.exiting();       
		return productGUID;
    }

	public abstract IPCItemReference getItemReference();

	public Configuration getConfiguration() {
        location.entering("getConfiguration()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        Configuration config = _config.getConfig();
        if (location.beDebug()) {
            location.debugT("return config " + config.getId());
        }
        location.exiting(); 
		return _config.getConfig();
	}

    public IPCItem getParent() {
        location.entering("getParent()");
        if (location.beDebug()) {
            if (_parent != null){
                location.debugT("return _parent " + _parent.getItemId());
            }
            else {
                location.debugT("return _parent null");
            }
        }
        location.exiting(); 
		return _parent;
    }

    void setParent(DefaultIPCItem parent) {
    	_parent = parent;
    }


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#getChildren()
	 */
	public IPCItem[] getChildren() throws IPCException {
        location.entering("getChildren()");
        IPCItem[] children = _getChildren(true);
        if (location.beDebug()) {
            if (children != null) {
                location.debugT("return children");
                for (int i = 0; i<children.length; i++){
                    location.debugT("  children["+i+"] "+children[i].getItemId());
                }
            }
        }
        location.exiting();
		return children;
	}
	
	/**
	 * Client Object Layer internal Use. 
	 */
	public IPCItem[] _getChildren(boolean isCacheCheckRequired) throws IPCException {
        location.entering("_getChildren(boolean isCacheCheckRequired)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean isCacheCheckRequired " + isCacheCheckRequired);
        }
		if(isCacheCheckRequired){		
			if (isCacheDirty()){
				this.syncWithServer();
			}        
		}
		/* 20020211-kha: to deal with dynamically changing subitems, keep a set of subitem-ids and
		 * compare this set every time with the latest ids you get from the server. Only if it's
		 * different, update the client-side children.
		 */
		boolean recomputationIsNecessary = false;
		if (_children != null) {
			// changed?
			String[] itemIds = _props.getSubItemIds();
		    if (itemIds == _childrenIds)
				recomputationIsNecessary = false;
			else if (itemIds == null || _childrenIds == null)
				recomputationIsNecessary = true;
			else if (itemIds.length != _childrenIds.length)
				recomputationIsNecessary = true;
			else {
				// the children have to be exactly the same, including their order
				for (int i=0; i<itemIds.length && !recomputationIsNecessary; i++) {
					if (!itemIds[i].equals(_childrenIds[i]))
						recomputationIsNecessary = true;
				}
			}
		}
		else {
			recomputationIsNecessary = true;
		}

		if (recomputationIsNecessary) {
			_determineChildren(); // will implicitely reset _childrenCopy
		}

		if (_childrenCopy == null) {
			_childrenCopy = new IPCItem[_children.size()];
			_children.copyInto(_childrenCopy);
		}
        if (location.beDebug()) {
            if (_childrenCopy != null) {
                location.debugT("return _childrenCopy");
                for (int i = 0; i<_childrenCopy.length; i++){
                    location.debugT("  _childrenCopy["+i+"] "+_childrenCopy[i].getItemId());
                }
            }
        }
        location.exiting();
		return _childrenCopy;
    }


	DefaultIPCItem _getChildRecursive(String itemId) {
		if (_itemId.equals(itemId)) {
			return this;
		}
		else if (_children != null) {
			for (Enumeration e=_children.elements(); e.hasMoreElements(); ) {
				DefaultIPCItem child = (DefaultIPCItem)e.nextElement();
				DefaultIPCItem result = child._getChildRecursive(itemId);
				if (result != null)
					return result;
			}
			return null;
		}
		else
			return null;
	}

	public IPCItem getItemFromInstanceId(String instanceId) {
		if (instanceId == null) {
			return null;
		}
		location.entering("getChild(" + instanceId + "");
		if (_childrenByInstanceId != null) {
		    IPCItem item = (IPCItem)_childrenByInstanceId.get(instanceId);
			location.exiting();
		    return item; 
		} else {
			location.exiting();
			return null;
    	}
	}

	protected void _determineChildren() throws IPCException {
		String[] itemIds = _props.getSubItemIds();
    	_children = new Vector();
		if (itemIds != null) {
			for (int i=0; i<itemIds.length; i++) {
				DefaultIPCItem child = factory.newIPCItem(_client,_document,itemIds[i]);
				child.setParent(this);
				_children.addElement(child);
			}
		}
		_childrenIds = itemIds;
		_childrenCopy = null;
    }

	/**
	 * For IPC Client Object Layer Use only
	 */
	public void connectToChildren(Map idToItem) {
        location.entering("connectToChildren(Map idToItem)");
//        if (location.beDebug()){
//            location.debugT("Parameters:");
//            if (idToItem!=null){
//                location.debugT("Map idToItem " idToItem. );
//
//            }
//            else location.debugT("Map idToItem null");
//        }
		//StackOverFlowError with syncWithServer() call (As this method is for internal use no need of sycn)
//		if (isCacheDirty()){
//			this.syncWithServer();
//		}        
		try {
			String[] itemIds = _props.getSubItemIds();
			_children = new Vector();
			_childrenCopy = null;
			for (int i=0; i<itemIds.length; i++) {
				IPCItem item = (IPCItem)idToItem.get(itemIds[i]);
                if (location.beDebug()) {
                       location.debugT("Added item "+item.getItemId() + "as child of "+this.getItemId());
                }
				_children.addElement(item);
			}
			_childrenIds = itemIds;
            location.exiting();
		}
		catch(IPCException e) {
			throw new RuntimeException("This should never happen! subitemids are not fresh in connectToChildren");
		}
	}


    public IPCItem[] getDescendants() throws IPCException {
        location.entering("getDescendants()");
		if (isCacheDirty()){
			syncWithServer();
		}
    	Vector v = new Vector();
    	_collectChildren(v);
    	IPCItem[] result = new IPCItem[v.size()];
    	v.copyInto(result);
        if (location.beDebug()){
            if (result != null) {
                location.debugT("return result");
                for (int i = 0; i<result.length; i++){
                    location.debugT("  result["+i+"] "+result[i].getItemId());
                }
            }
        }
        location.exiting();
    	return result;
    }


	protected void _collectChildren(Vector children) throws IPCException {
        if (_children == null){   
            _children = new Vector();
        }
    	for (Enumeration e=_children.elements(); e.hasMoreElements();) {
    		DefaultIPCItem child = (DefaultIPCItem)e.nextElement();
    		children.addElement(child);
    		child._collectChildren(children);
    	}
    }


	public String getProductDescription() throws IPCException {
        location.entering("getProductDescription()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        String productDescription = _props.getProductDescription();
        if (location.beDebug()) {
            location.debugT("return productDescription " + productDescription);
        }
        location.exiting();
		return productDescription;
    }

    public String[] getHeaderAttributeEnvironment() throws IPCException {
        location.entering("getHeaderAttributeEnvironment()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        String[] headerAttributeEnvironment = _props.getHeaderAttributeEnvironment(); 
        if (location.beDebug()){
            if (headerAttributeEnvironment != null) {
                location.debugT("return headerAttributeEnvironment");
                for (int i = 0; i<headerAttributeEnvironment.length; i++){
                    location.debugT("  headerAttributeEnvironment["+i+"] "+headerAttributeEnvironment[i]);
                }
            }
        }
        location.exiting();
		return headerAttributeEnvironment;
    }

    public String getHeaderBindingValue(String s) throws IPCException {
        location.entering("getHeaderBindingValue(String s)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String s " + s);
        }
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        String headerBindingValue = _props.getHeaderBindingValue(s);
        if (location.beDebug()) {
            location.debugT("return headerBindingValue " + headerBindingValue);
        }
        location.exiting();
		return headerBindingValue;
    }

    public String getItemBindingValue(String s) throws IPCException {
        location.entering("getItemBindingValue(String s)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String s " + s);
        }
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        String ItemBindingValue = _props.getItemBindingValue(s);
        if (location.beDebug()) {
            location.debugT("return ItemBindingValue " + ItemBindingValue);
        }
        location.exiting();
		return ItemBindingValue;
    }

	public abstract void setHeaderAttributeBindings(String[] names, String[] values) throws IPCException;

    public abstract void setItemAttributeBindings(String[] names, String[] values) throws IPCException;

    public String[] getItemAttributeEnvironment() throws IPCException {
        location.entering("getItemAttributeEnvironment()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        String[] itemAttributeEnvironment = _props.getItemAttributeEnvironment();
        if (location.beDebug()){
            if (itemAttributeEnvironment != null) {
                location.debugT("return itemAttributeEnvironment");
                for (int i = 0; i<itemAttributeEnvironment.length; i++){
                    location.debugT("  itemAttributeEnvironment["+i+"] "+itemAttributeEnvironment[i]);
                }
            }
        }
        location.exiting();
		return itemAttributeEnvironment;
    }

    public String[][] getVariantConditions() throws IPCException {
        /**@todo: Implement this com.sap.spc.remote.client.object.IPCItem method*/
        throw new java.lang.UnsupportedOperationException("Method getVariantConditions() not yet implemented.");
    }

	public void removeAllVariantConditions() {
        /**@todo: Implement this com.sap.spc.remote.client.object.IPCItem method*/
        throw new java.lang.UnsupportedOperationException("Method removeAllVariantConditions() not yet implemented.");
    }

	public void addVariantCondition(String name, Double factor, String description) {
        /**@todo: Implement this com.sap.spc.remote.client.object.IPCItem method*/
        throw new java.lang.UnsupportedOperationException("Method addVariantCondition() not yet implemented.");
    }


	public boolean isConfigurable() throws IPCException {
        location.entering("isConfigurable()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        boolean isConfigurable = _props.isConfigurable();
        if (location.beDebug()) {
            location.debugT("return isConfigurable " + isConfigurable);
        }
        location.exiting();
		return _props.isConfigurable();
    }


    public boolean isProductVariant() throws IPCException {
        location.entering("isProductVariant()");
        if (isCacheDirty()){
            this.syncWithServer();
        }
        boolean isProductVariant = _props.isProductVariant();
        if (location.beDebug()) {
            location.debugT("return isProductVariant " + isProductVariant);
        }
        location.exiting();       
        return _props.isProductVariant();
    }


	public  Map getContext() {
        location.entering("getContext()");
		if (isCacheDirty()){
			this.syncWithServer();
		}        
		try {
            Map context = _props.getContext();
            if (location.beDebug()) {
                location.debugT("return context " + context.toString());
            }
            location.exiting(); 
		    return context;
		}
		catch(IPCException e) {
			// may not happen since we don't ever retrieve the context from the server!
			return null;
		}
	}

	/**
	 * Changes the item's configuration context table.
	 */
	public  abstract void setContext(Map context) throws IPCException;

	/**
	 * Changes attributes on item's
	 */
	public abstract void setAdditionalParameter(String param, String value) throws IPCException;

    public ext_configuration getConfig() throws IPCException {
        location.entering("getConfig()");
        if (isCacheDirty()){
            this.syncWithServer();
        } 
        ext_configuration config = _props.getConfig(_document.getSession().getUserSAPClient()); 
        if (location.beDebug()) {
            location.debugT("return config " + config);
        }
        location.exiting();  
		return config;
	}

    public abstract void setConfig(ext_configuration config) throws IPCException;

	/* (non-Javadoc)
	 * @deprecated
	 * @see com.sap.spc.remote.client.object.IPCItem#setConfig(com.sap.spc.remote.client.object.OrderConfiguration)
	 */
	public abstract void setConfig(OrderConfiguration config) throws IPCException;
	
	public abstract void setConfig(ext_configuration config, Hashtable context) throws IPCException;

    public void pricing(char mode) throws IPCException{
        location.entering("pricing(char mode)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("char mode " + mode);
        }
        _pricingConditionSet.setInvalid();
        location.exiting();
    }
    
    public void pricing() throws IPCException {
        location.entering("pricing()");
		pricing('D');
        location.exiting();
    }

    public boolean getStatistical() throws IPCException {
        location.entering("getStatistical()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        boolean isStatistical = _props.isStatistical(); 
        if (location.beDebug()) {
            location.debugT("return isStatistical " + isStatistical);
        }
        location.exiting();  
		return isStatistical;
    }


    public abstract void setStatistical(boolean flag) throws IPCException;

    public DimensionalValue getNetValue() throws IPCException {
        location.entering("getNetValue()");
        if (isCacheDirty()){
            this.syncWithServer();
        } 
        DimensionalValue netValue =  _props.getNetValue();      
        if (location.beDebug()) {
            location.debugT("return netValue " + netValue.getValueAsString() +" "+netValue.getUnit());
        }
        location.exiting(); 
		return netValue;
    }


    public DimensionalValue getNetValueWithoutFreight() throws IPCException {
        location.entering("getNetValueWithoutFreight()");
        if (isCacheDirty()){
            this.syncWithServer();
        } 
        DimensionalValue netValueWithoutFreight =  _props.getNetValueWithoutFreight();      
        if (location.beDebug()) {
            location.debugT("return netValueWithoutFreight " + netValueWithoutFreight.getValueAsString() +" "+netValueWithoutFreight.getUnit());
        }
        location.exiting();        
    	return netValueWithoutFreight;
    }


    public DimensionalValue getTotalGrossValue() throws IPCException {
        location.entering("getTotalGrossValue()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        DimensionalValue totalGrossValue =  _props.getTotalGrossValue();      
        if (location.beDebug()) {
            location.debugT("return totalGrossValue " + totalGrossValue.getValueAsString() +" "+totalGrossValue.getUnit());
        }
        location.exiting();
		return totalGrossValue;
    }

    public DimensionalValue getTotalNetValue() throws IPCException {
        location.entering("getTotalNetValue()");
        if (isCacheDirty()){
            this.syncWithServer();
        }    
        DimensionalValue totalNetValue =  _props.getTotalNetValue();      
        if (location.beDebug()) {
            location.debugT("return totalNetValue " + totalNetValue.getValueAsString() +" "+totalNetValue.getUnit());
        }
        location.exiting();    
		return totalNetValue;
    }

    public DimensionalValue getTotalTaxValue() throws IPCException {
        location.entering("getTotalTaxValue()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        DimensionalValue totalTaxValue =  _props.getTotalTaxValue();      
        if (location.beDebug()) {
            location.debugT("return totalTaxValue " + totalTaxValue.getValueAsString() +" "+totalTaxValue.getUnit());
        }
        location.exiting();
		return totalTaxValue;
    }

    public DimensionalValue getGrossValue() throws IPCException {
        location.entering("getGrossValue()");
        if (isCacheDirty()){
            this.syncWithServer();
        }       
        DimensionalValue grossValue = _props.getGrossValue();      
        if (location.beDebug()) {
            location.debugT("return grossValue " + grossValue.getValueAsString() +" "+grossValue.getUnit());
        }
        location.exiting();
		return _props.getGrossValue();
    }

    public DimensionalValue getTaxValue() throws IPCException {
        location.entering("getTaxValue()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        DimensionalValue taxValue = _props.getTaxValue();      
        if (location.beDebug()) {
            location.debugT("return taxValue " + taxValue.getValueAsString() +" "+taxValue.getUnit());
        }
        location.exiting();
		return taxValue;
    }

    public IPCDocument getDocument() {
        location.entering("getDocument()");
        if (location.beDebug()) {
            location.debugT("return _document " + _document.getId());
        }
        location.exiting();
		return _document;
    }

    public abstract void setPricingDate(String s) throws IPCException;

    public String getPricingDate() throws IPCException {
        location.entering("getPricingDate()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        String pricingDate = _props.getDate(); 
        if (location.beDebug()) {
            location.debugT("return pricingDate " + pricingDate);
        }
        location.exiting();
		return pricingDate;
    }

    public boolean getPricingItemIsUnchangeable() throws IPCException {
        /**@todo: Implement this com.sap.spc.remote.client.object.IPCItem method*/
        throw new java.lang.UnsupportedOperationException("Method getPricingItemIsUnchangeable() not yet implemented.");
    }
    public void setPricingItemIsUnchangeable(boolean flag) {
        /**@todo: Implement this com.sap.spc.remote.client.object.IPCItem method*/
        throw new java.lang.UnsupportedOperationException("Method setPricingItemIsUnchangeable() not yet implemented.");
    }

    //tm 18.08.2006 pricing analysis can only be enabled or disabled at document level
    //only SPC_CREATE_DOCUMENT has the parameter to tell the pricing engine to trace
    public boolean getPerformPricingAnalysis() throws IPCException {
        location.entering("getPerformPricingAnalysis()");
        if (isCacheDirty()){
            this.syncWithServer();
        }  
        boolean performPricingAnalysis = this.getDocument().getDocumentProperties().getPerformPricingAnalysis();   
        if (location.beDebug()) {
            location.debugT("return performPricingAnalysis " + performPricingAnalysis);
        }
        location.exiting();
		return performPricingAnalysis;
    }
    
	/**
	 * Returns true if this item is a changeable product variant.
	 */
	public  boolean isChangeableProductVariant() throws IPCException {
		location.entering("isChangeableProductVariant()");
		
		boolean isChangeableProductVariant = _props.isChangeableProductVariant();   
		if (location.beDebug()) {
			location.debugT("return isChangeableProductVariant " + isChangeableProductVariant);
		}
		location.exiting();
		return isChangeableProductVariant;		
	}

    public void setPerformPricingAnalysis(boolean flag) {
        /**@todo: Implement this com.sap.spc.remote.client.object.IPCItem method*/
        throw new java.lang.UnsupportedOperationException("Method setPerformPricingAnalysis() not yet implemented.");
    }
    public void setServiceRendererDate(String s) throws IPCException {
        /**@todo: Implement this com.sap.spc.remote.client.object.IPCItem method*/
        throw new java.lang.UnsupportedOperationException("Method setServiceRendererDate() not yet implemented.");
    }
    public String getServiceRenderDate() throws IPCException {
        /**@todo: Implement this com.sap.spc.remote.client.object.IPCItem method*/
        throw new java.lang.UnsupportedOperationException("Method getServiceRenderDate() not yet implemented.");
    }
    public boolean isReturn() throws IPCException {
        location.entering("isReturn()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        boolean isItemReturn = _props.getPerformPricingAnalysis();   
        if (location.beDebug()) {
            location.debugT("return isItemReturn " + isItemReturn);
        }
        location.exiting();
		return isItemReturn;
    }

    public abstract void setReturn(boolean flag) throws IPCException;

	public  DimensionalValue getNetWeight() {
        location.entering("getNetWeight()");
        if (isCacheDirty()){
            this.syncWithServer();
        } 
        DimensionalValue netWeight = _props.getNetWeight();      
        if (location.beDebug()) {
            location.debugT("return netWeight " + netWeight.getValueAsString() +" "+netWeight.getUnit());
        }
        location.exiting();       
		return netWeight;
	}

	public abstract void setNetWeight(DimensionalValue weight);

	public abstract void setBaseQuantity(DimensionalValue value) throws IPCException;

    public DimensionalValue getBaseQuantity() {
        location.entering("getBaseQuantity()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        DimensionalValue baseQuantity = _props.getBaseQuantity();      
        if (location.beDebug()) {
            location.debugT("return baseQuantity " + baseQuantity.getValueAsString() +" "+baseQuantity.getUnit());
        }
        location.exiting(); 
		return _props.getBaseQuantity();
    }

    public DimensionalValue getVolume() {
        location.entering("getVolume()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        DimensionalValue volume = _props.getVolume();      
        if (location.beDebug()) {
            location.debugT("return volume " + volume.getValueAsString() +" "+volume.getUnit());
        }
        location.exiting();
		return volume;
    }

    public abstract void setVolume(DimensionalValue value) throws IPCException;

    public DimensionalValue getSalesQuantity() {
        location.entering("getSalesQuantity()");
        if (isCacheDirty()){
            this.syncWithServer();
        }
        DimensionalValue salesQuantity = _props.getSalesQuantity();      
        if (location.beDebug()) {
            location.debugT("return salesQuantity " + salesQuantity.getValueAsString() +" "+salesQuantity.getUnit());
        }
        location.exiting();
		return salesQuantity;
    }

    public abstract void setSalesQuantity(DimensionalValue value) throws IPCException;

    public DimensionalValue getGrossWeight() {
        location.entering("getGrossWeight()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        DimensionalValue grossWeight = _props.getGrossWeight();     
        if (location.beDebug()) {
            location.debugT("return grossWeight " + grossWeight.getValueAsString() +" "+grossWeight.getUnit());
        }
        location.exiting();
		return grossWeight;
    }

    public abstract void setGrossWeight(DimensionalValue weight) throws IPCException;

	public abstract void setExchangeRateType(String exchangeRateType) throws IPCException;

	public abstract void setExchangeRateDate(String exchangeRateDate) throws IPCException;

    public String getExchangeRate() throws IPCException {
        location.entering("getExchangeRate()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        String exchangeRate = _props.getExchangeRate();     
        if (location.beDebug()) {
            location.debugT("return exchangeRate " + exchangeRate);
        }
        location.exiting();
		return exchangeRate;
    }

    public abstract IPCItem copyItem(IPCDocument targetDocument) throws IPCException;

	public  IPCPricingConditionSet getPricingConditions() throws IPCException{
        location.entering("getPricingConditions()");
        if (isCacheDirty()){
            this.syncWithServer();
        }  
        if (location.beDebug()) {
            location.debugT("return _pricingConditionSet " + _pricingConditionSet.toString());
        }
        location.exiting();
        return _pricingConditionSet;
    }

    public void close() {
        location.entering("close()");
	    _closed = true;
        location.exiting();
    }

    public boolean isClosed() {
        location.entering("isClosed()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
        if (location.beDebug()) {
            location.debugT("return _closed " + _closed);
        }
        location.exiting();
		return _closed;
    }

	/**
	 * On an item, configuration commands are only relevant when the item is configurable
	 */
	public boolean isRelevant(String commandName) {
		return true; // getters are no longer scheduled so all this logic is unnecessary
		/* 40Comment block
		if (commandName.equals(CommandConstants.GET_CONFIG_HAS_CHANGED) ||
			    commandName.equals(CommandConstants.GET_CONFIG_INFO) ||
			    commandName.equals(CommandConstants.GET_CONFIG_ITEM_INFO) ||
			    commandName.equals(CommandConstants.GET_CONFIG_ITEM_INFO_XML) ||
			    commandName.equals(CommandConstants.GET_CONFIG_ITEMS_INFO)) {
			Value isConfigurable = _props.getConfigurableValue();
			if (isConfigurable instanceof OpenValue && !((OpenValue)isConfigurable).hasBeenClosed()) {
				return true; // we don't know now so we have to assume it is relevant.
				// please note that this method is called during the command execution
				// sequence of a CachingClient. Because of this we may not access a Value's
				// content if it is open -> we would get an infinite recursion this way.
			}
			else {
				try {
			        return _props.isConfigurable();
				}
				catch(IPCException e) {
					// may not happen - we have checked above that the value is there and we won't call the server
					return true;
				}
			}
		}
		else {
			return true;
		}*/
	}

	/**
	 * Returns an external (BAPI) representation of the conditions
	 */
	public HashMap getExternalPricingConditions() throws IPCException {
        location.entering("getExternalPricingConditions()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
		HashMap data = _pricingConditionSet.getExternalPricingConditions();
        if (location.beDebug()) {
            location.debugT("return data " + data.toString());
        }
        location.exiting();
		return data;
    }

	/**
	 * Returns an external (BAPI) representation of the conditions
	 */
	public HashMap getExternalPricingInfo() throws IPCException {
        location.entering("getExternalPricingInfo()");
        if (isCacheDirty()){
            this.syncWithServer();
        }        
		HashMap data;
		data = _getExternalPricingInfo();
        if (location.beDebug()) {
            location.debugT("return data " + data.toString());
        }
        location.exiting();
		return data;
    }

	protected abstract HashMap _getExternalPricingInfo() throws IPCException;

	protected abstract void _resetInternalConfiguration();

	/**
	 * Initializes common properties of this item. The cacheKey passed to this method can be
	 * used to determine if these values are still valid on the server.<p>
	 *
	 * This method is called at certain times to optimize performance. Simple item implementations
	 * can just implement it by an empty method body.
	 */
	public void initCommonProperties(Object cacheKey,
	                                 String netValue, String taxValue, String grossValue,
									 String totalNetValue, String totalTaxValue, String totalGrossValue,
									 String currencyUnit,
									 String productGuid, String productId, String productDescription,
									 String baseQuantityValue, String baseQuantityUnit,
									 String quantityValue, String quantityUnit,
									 String configurable, String variant, String complete, String consistent, String configChanged, List children) {
        location.entering("initCommonProperties(Object cacheKey, String netValue, String taxValue, " +                           "String grossValue, String totalNetValue, String totalTaxValue, " +                           "String totalGrossValue, String currencyUnit, String productGuid, " +                           "String productId, String productDescription, String baseQuantityValue, " +                           "String baseQuantityUnit, String quantityValue, String quantityUnit, " +                           "String configurable, String variant, String complete, String consistent, " +                           "String configChanged, List children)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (cacheKey!=null){
                location.debugT("Object cacheKey " + cacheKey.toString());
            }
            else location.debugT("Map cacheKey null");
            location.debugT("String netValue " + netValue);
            location.debugT("String grossValue " + grossValue);
            location.debugT("String totalNetValue " + totalNetValue);
            location.debugT("String totalTaxValue " + totalTaxValue);
            location.debugT("String totalGrossValue " + totalGrossValue);
            location.debugT("String currencyUnit " + currencyUnit);
            location.debugT("String productGuid " + productGuid);
            location.debugT("String productId " + productId);
            location.debugT("String productDescription " + productDescription);
            location.debugT("String baseQuantityValue " + baseQuantityValue);
            location.debugT("String baseQuantityUnit " + baseQuantityUnit);
            location.debugT("String quantityValue " + quantityValue);
            location.debugT("String quantityUnit " + quantityUnit);
            location.debugT("String configurable " + configurable);
            location.debugT("String variant " + variant);
            location.debugT("String complete " + complete);
            location.debugT("String consistent " + consistent);
            location.debugT("String configChanged " + configChanged);
            location.debugT("List children: ");
            if (children != null) {
                for (int i = 0; i<children.size(); i++){
                    location.debugT("  children["+i+"] "+children.get(i).toString());
                }
            }
        }
	        
        _props.initCommonProperties(cacheKey, netValue, taxValue, grossValue,
									totalNetValue, totalTaxValue, totalGrossValue,
									currencyUnit, productGuid, productId, productDescription,
									baseQuantityValue, baseQuantityUnit,
							        quantityValue, quantityUnit, configurable, variant, children, configChanged);
        location.exiting();                                    
	}
	
	public void initOtherProperties(String netValueWoFreight, String currencyUnit){
        location.entering("initOtherProperties(String netValueWoFreight, String currencyUnit)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String netValueWoFreight " + netValueWoFreight);
            location.debugT("String currencyUnit " + currencyUnit);
        }
		_props.initOtherProperties(_document.getId(),_itemId, netValueWoFreight, currencyUnit);
        location.exiting();					        
	};

    public String toString(){
		if (isCacheDirty()){
			this.syncWithServer();
		}        
    	String result = super.toString();
    	result +=
			"client:" + _client +
			"\ndocument:" + _document +
			"\nitemId:" + _itemId +
			"\nitemProperties:" + _props;

    	return result;
    }

	public boolean isCacheDirty(){
        location.entering("isCacheDirty()");
        if (location.beDebug()) {
            location.debugT("return cacheIsDirty " + cacheIsDirty);
        }
        location.exiting();
		return cacheIsDirty;
	}
		
	/**
	 * Sets Cache as dirty.
	 */
	public abstract void setCacheDirty();
		
	/**
     * Sync the client representation of the Item with the information on the server.
     * @throws IPCException
     */
	void syncWithServer() throws IPCException{
		//This item cache might be dirty but doc cache may not be dirty. In this case make the doc cache as dirty so that doc will be in sycn with server in the sync call. 
		if (isCacheDirty() && !_document.isCacheDirty())
			_document.setCacheDirty();
        	
		_document.syncWithServer();
//		_resetInternalConfiguration(); tm 20080917 document.syncWithServer performs this already
	}
	
	public abstract String getPricingAnalysisData() throws IPCException;
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#setSubItemsAllowed(boolean)
     */
    public abstract void setSubItemsAllowed(boolean flag) throws IPCException;
    
    /**
     * Set item attributes.
     * First set these attributes to the ItemProperties then send them to the server.
     * If you only want to set attributes on the client side set them directly on the ItemProperties.
     * @param attributes map of item attributes
     * @throws IPCException
     */
    public abstract void setItemAttributes(Map attributes) throws IPCException;

	/**
	 * For IPC Client Object Layer Use only
	 */
	public void _setItemFlagToSync() throws IPCException {
        location.entering("_setItemFlagToSync()");
        cacheIsDirty = false;
        location.exiting();
	}
	
	/**
	 * For IPC Client Object Layer Use only
	 */
	public void _setItemFlagToDirty() {
        location.entering("_setItemFlagToDirty()");
		cacheIsDirty = true;
        location.exiting();
	}
	
	/**
	 * Resets the configuration attached to the item, in order
	 * clear the cache of the client side object configuration.
	 * A new ConfigurationValue object will be created and with
	 * the next get to this object it will ask the server for 
	 * its state.
	 * The method is not part of the interface by intension.
	 * For IPC Client Object Layer internal use only
	 */
	public void initConfiguration() {
        location.entering("initConfiguration()");
        //only root items can have a configuration associated
        if (this.getParent() == null) {
		_resetInternalConfiguration();
        }	
        location.exiting();
	}
    
    /**
     * This method will overwrite the itemProperties of the item with a new ItemProperties-object.
     * All properties that are set in the original itemProperties and which are not set in the new one
     * will be lost.
     * @param itemProps ItemProperties that should replace the original item properties
     */
    public void setItemProperties(DefaultIPCItemProperties itemProps){
        location.entering("setItemProperties(DefaultIPCItemProperties itemProps)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            if (itemProps!=null){
                location.debugT("DefaultIPCItemProperties itemProps " + itemProps.toString());
            }
            else location.debugT("DefaultIPCItemProperties itemProps null");
        }
        _props = itemProps;
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getProductKey()
     */    
    public abstract ProductKey getProductKey();
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getProductGuid(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public abstract String getProductGuid(
        String productId,
        String productType,
        String productLogSys);


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#setInitialConfiguration(com.sap.spc.remote.client.util.ext_configuration)
     */
    public abstract void setInitialConfiguration(ext_configuration config) throws IPCException;

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getInitialConfiguration()
     */
    public abstract ConfigurationSnapshot getInitialConfiguration();

	/**
	 * @return
	 */
	public Map getDynamicReturn() {
		return _dynamicReturn;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getTteItem()
     */
    public TTEItem getTteItem() {
        return _tteItem;
	}
	
	/**
	 * @param map
	 */
	public void setDynamicReturn(Map map) {
		_dynamicReturn = map;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#setTteItem()
     */
    public void setTteItem(TTEItem tteItem) {
        this._tteItem = tteItem;
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#changeExternalId(java.lang.String)
     */
    public abstract void changeExternalId(String value) throws IPCException;

	/**
	 * Accessing pricing functions from the product configuration requires
	 * access to the pricing items.
	 * The method initializes this mapping.
	 */
	public void initializeChildrenByInstanceId() {
		location.entering("initializeChildrenByInstanceId()");
		//only root items can have a configuration associated
		if (this.getParent() == null && _props.isConfigurable()) {
			_childrenByInstanceId = readConfigItemInfoFromBackend();
	}
		location.exiting();
	}
	public abstract Map readConfigItemInfoFromBackend();

}
