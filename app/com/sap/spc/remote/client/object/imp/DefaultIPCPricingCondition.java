/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCPricingCondition;

/**
 * A single pricing condition on item level.
 * All values passed in and returned are handled in external represenation.
 */
public abstract class DefaultIPCPricingCondition extends BusinessObjectBase implements IPCPricingCondition, Comparable {

	public static final Object NULL_PARAMETER = new Object();

	protected DefaultIPCPricingConditionSet _pricingConditionSet = null;
	protected String          _documentId = null;
	protected String          _itemId = null;
	 protected IClient   _client = null;

	 protected String _stepNo = null;
	 protected String _counter = null;
	protected String _headerCounter = null;
	 protected String _conditionTypeName = null;
	 protected String _description = null;
	 protected String _conditionRate = null;
	 protected String _conditionCurrency = null;
	 protected String _pricingUnitValue = null;
	 protected String _pricingUnitUnit = null;
	 protected String _pricingUnitDimension = null;
	 protected String _conditionValue = null;
	 protected String _documentCurrency = null;
	 protected String _conversionNumerator = null;
	 protected String _conversionDenominator = null;
	 protected String _conversionExponent = null;
	 protected String _conditionBaseValue = null;

	 protected String  _exchangeRate = null;
	 protected String  _directExchangeRate = null;
	 protected boolean _isDirectExchangeRate = false;
	 protected String  _factor = null;
	            protected boolean _variantConditionFlag;

	            protected String _variantConditionKey;
	 protected String  _variantFactor = null;
	            protected String _variantConditionDescr;

	 protected boolean _isStatistical = false;
	 protected char _inactive = ' ';
	 protected char _conditionClass = ' ';
	 protected char _calculationType = ' ';
	 protected char _conditionCategory = ' ';
	 protected char _conditionControl = ' ';
	 protected char _conditionOrigin = ' ';
	 protected char _scaleType = ' ';

	 protected boolean _changeOfRatesAllowed = false;
	 protected boolean _changeOfPricingUnitsAllowed = false;
	 protected boolean _changeOfValuesAllowed = false;
	 protected boolean _deletionAllowed = false;

	 protected Hashtable _parameterQueue = null;

	 protected boolean _isInvalid = false; //out of sync


    protected DefaultIPCPricingCondition(TechKey techKey) {
    	super(techKey);
    }
//	/**
//	 * Constructs the pricing condition.
//	 */
//	DefaultIPCPricingCondition( DefaultIPCPricingConditionSet pricingConditionSet,
//								    String  stepNo,
//									String  counter,
//									String  conditionTypeName,
//									String  description,
//									String  conditionRate,
//									String  conditionCurrency,
//									String  pricingUnitValue,
//									String  pricingUnitUnit,
//									String  pricingUnitDimension,
//									String  conditionValue,
//									String  documentCurrency,
//									String  conversionNumerator,
//									String  conversionDenominator,
//									String  conversionExponent,
//									String  conditionBaseValue,
//
//									String  exchangeRate,
//									String  directExchangeRate,
//									boolean isDirectExchangeRate,
//									String  factor,
//									String  variantFactor,
//
//									boolean  isStatistical,
//									char     inactive,
//									char     conditionClass,
//									char     calculationType,
//									char     conditionCategory,
//									char     conditionControl,
//									char     conditionOrigin,
//									char     scaleType,
//
//									boolean  changeOfRatesAllowed,
//									boolean  changeOfPricingUnitsAllowed,
//									boolean  changeOfValuesAllowed,
//									boolean  deletionAllowed){
//		_pricingConditionSet = pricingConditionSet;
//		_documentId = pricingConditionSet.getDocumentId();
//		_itemId = pricingConditionSet.getItemId();
//		_client = pricingConditionSet.getClient();
//		_stepNo = stepNo;
//		_counter = counter;
//		_conditionTypeName = conditionTypeName;
//		_description = description;
//		_conditionRate = conditionRate;
//		_conditionCurrency = conditionCurrency;
//		_pricingUnitValue = pricingUnitValue;
//		_pricingUnitUnit = pricingUnitUnit;
//		_pricingUnitDimension = pricingUnitDimension;
//		_conditionValue = conditionValue;
//		_documentCurrency = documentCurrency;
//		_conversionNumerator = conversionNumerator;
//		_conversionDenominator = conversionDenominator;
//		_conversionExponent = conversionExponent;
//		_conditionBaseValue = conditionBaseValue;
//
//		_exchangeRate = exchangeRate;
//		_directExchangeRate = directExchangeRate;
//		_isDirectExchangeRate = isDirectExchangeRate;
//		_factor = factor;
//		_variantFactor = variantFactor;
//
//		_isStatistical = isStatistical;
//		_inactive = inactive;
//		_conditionClass = conditionClass;
//		_calculationType = calculationType;
//		_conditionCategory = conditionCategory;
//		_conditionControl = conditionControl;
//		_conditionOrigin = conditionOrigin;
//		_scaleType = scaleType;
//
//		_changeOfRatesAllowed = changeOfRatesAllowed;
//		_changeOfPricingUnitsAllowed = changeOfPricingUnitsAllowed;
//		_changeOfValuesAllowed = changeOfValuesAllowed;
//		_deletionAllowed = deletionAllowed;
//		_initParameterQueue();
//	}

	 protected abstract void _initParameterQueue();
//	{
//		if (_parameterQueue == null)
//			_parameterQueue = new Hashtable(5);
//		else
//			_parameterQueue.clear();
//		//insert default (null) values
//		_parameterQueue.put(ChangePricingConditions.CONDITION_VALUES, NULL_PARAMETER);
//		_parameterQueue.put(ChangePricingConditions.CONDITION_RATES, NULL_PARAMETER);
//		_parameterQueue.put(ChangePricingConditions.CONDITION_CURRENCIES, NULL_PARAMETER);
//		_parameterQueue.put(ChangePricingConditions.PRICING_UNIT_VALUES, NULL_PARAMETER);
//		_parameterQueue.put(ChangePricingConditions.PRICING_UNIT_UNITS, NULL_PARAMETER);
//	}
	/**
	 * @return the step number.
	 */
	public String getStepNo(){
		return _stepNo;
	}

	/**
	 * @return the counter.
	 */
	public String getCounter(){
		return _counter;
	}
	/**
		 * @return the headerCounter.
		 */
   public String getHeaderCounter(){
		return _headerCounter;
	}
	/**
	 * @return the name of the condition type.
	 */
	public String getConditionTypeName(){
		return _conditionTypeName;
	}

	/**
	 * @return the description.
	 */
	public String getDescription(){
		return _description;
	}

	/**
	 * Sets the condition rate.
	 */
	
	public abstract void setConditionValue(String conditionValue, String decimalSeparator, String groupingSeparator);
	
	public abstract void setConditionRate(String conditionRate, String decimalSeparator, String groupingSeparator);
	
	public abstract void setConditionRate(String conditionRate);
//	{
//		if (conditionRate.equals(_conditionRate))
//			conditionRate = null;
//		_changeCondition(ChangePricingConditions.CONDITION_RATES, conditionRate);
//	}

	/**
	 * @return the condition rate.
	 */
	public String getConditionRate(){
		if (_isInvalid)
			return null;
		return _conditionRate;
	}

	/**
	 * Sets the condition currency.
	 */
	public abstract void setConditionCurrency(String conditionCurrency);
//	{
//		if (conditionCurrency.equals(_conditionCurrency))
//			conditionCurrency = null;
//		_changeCondition(ChangePricingConditions.CONDITION_CURRENCIES, conditionCurrency);
//	}

	/**
	 * @return the name of the condition currency.
	 */
	public String getConditionCurrency(){
		if (_isInvalid)
			return null;
		return _conditionCurrency;
	}

	/**
	 * Sets the value of the pricing unit.
	 */
	public abstract void setPricingUnitValue(String pricingUnitValue);
//	{
//		if (pricingUnitValue.equals(_pricingUnitValue))
//			pricingUnitValue = null;
//		_changeCondition(ChangePricingConditions.PRICING_UNIT_VALUES, pricingUnitValue);
//	}

	/**
	 * @return the value of the pricing unit.
	 */
	public String getPricingUnitValue(){
		if (_isInvalid)
			return null;
		return _pricingUnitValue;
	}

	/**
	 * Sets the unit of the pricing unit.
	 */
	public abstract void setPricingUnitUnit(String pricingUnitUnit);
//	{
//		if (pricingUnitUnit.equals(_pricingUnitUnit))
//			pricingUnitUnit = null;
//		_changeCondition(ChangePricingConditions.PRICING_UNIT_UNITS, pricingUnitUnit);
//	}

	/**
	 * @return the name of the pricing unit.
	 */
	public String getPricingUnitUnit(){
		if (_isInvalid)
			return null;
		return _pricingUnitUnit;
	}

	/**
	 * @return the dimension of the pricing unit.
	 */
	public String getPricingUnitDimension(){
		if (_isInvalid)
			return null;
		return _pricingUnitDimension;
	}

	/**
	 * Sets the condition value.
	 */
	public abstract void setConditionValue(String conditionValue);
//	{
//		if (conditionValue.equals(_conditionValue))
//			conditionValue = null;
//		_changeCondition(ChangePricingConditions.CONDITION_VALUES, conditionValue);
//
//	}

	/**
	 * @return the condition value.
	 */
	public String getConditionValue(){
		if (_isInvalid)
			return null;
		return _conditionValue;
	}

	/**
	 * @return the name of the document currency.
	 */
	public String getDocumentCurrency(){
		return _documentCurrency;
	}

	/**
	 * @return the conversion numerator.
	 */
	public String getConversionNumerator(){
		return _conversionNumerator;
	}

	/**
	 * @return the conversion denominator.
	 */
	public String getConversionDenominator(){
		return _conversionDenominator;
	}

	/**
	 * @return the conversion exponent.
	 */
	public String getConversionExponent(){
		return _conversionExponent;
	}

	/**
	 * @return the base value.
	 */
	public String getConditionBaseValue(){
		return _conditionBaseValue;
	}

	/**
	 * @return the exchange rate.
	 */
	public String getExchangeRate(){
		return _exchangeRate;
	}

	/**
	 * @return the direct exchange rate.
	 */
	public String getDirectExchangeRate(){
		return _directExchangeRate;
	}

	/**
	 * Checks if direct exchange rate is used.
	 */
	public boolean isDirectExchangeRate(){
		return _isDirectExchangeRate;
	}

	/**
	 * @return the factor for condition basis (interval scales).
	 */
	public String getFactor(){
		return _factor;
	}

	/**
	 * @return the factor for conditions from configuration.
	 */
	public String getVariantFactor(){
		return _variantFactor;
	}

	/**
	 * @return if condition is statistical.
	 */
	public boolean isStatistical(){
		return _isStatistical;
	}

	/**
	 * @return if condition is inactive.
	 */
	public char getInactive(){
		return _inactive;
	}

	/**
	 * @return the condition class.
	 */
	public char getConditionClass(){
		return _conditionClass;
	}

	/**
	 * @return the calculation type.
	 */
	public char getCalculationType(){
		return _calculationType;
	}

	/**
	 * @return the condition category.
	 */
	public char getConditionCategory(){
		return _conditionCategory;
	}

	/**
	 * @return the condition control.
	 */
	public char getConditionControl(){
		return _conditionControl;
	}

	/**
	 * @return the condition origin.
	 */
	public char getConditionOrigin(){
		return _conditionOrigin;
	}

	/**
	 * @return the scale type.
	 */
	public char getScaleType(){
		return _scaleType;
	}

	/**
	 * @return if changing of condition rates is allowed.
	 */
	public boolean isChangeOfRatesAllowed(){
		return _changeOfRatesAllowed;
	}

	/**
	 * @return if changing of pricing units is allowed.
	 */
	public boolean isChangeOfPricingUnitsAllowed(){
		return _changeOfPricingUnitsAllowed;
	}

	/**
	 * @return if changing of condition values is allowed.
	 */
	public boolean isChangeOfValuesAllowed(){
		return _changeOfValuesAllowed;
	}

	/**
	 * @return if deletion of this pricing condition is allowed.
	 */
	public boolean isDeletionAllowed(){
		return _deletionAllowed;
	}

	public Collection getChangeParameters() throws IPCException{
		ArrayList params = new ArrayList();
		if (!_isInvalid)
			return params;
		boolean _realValueExists = false; //eigentlich �berfl�ssig, da mit _isInvalid korrespondierend
		for (Enumeration paraNames = _parameterQueue.keys(); paraNames.hasMoreElements();) {
			Object paraName = paraNames.nextElement();
			params.add(paraName);
			Object paraValue = _parameterQueue.get(paraName);
			if (paraValue == NULL_PARAMETER || paraValue.equals(""))
	    		params.add(null);
			else{
	    		params.add(paraValue);
				_realValueExists = true;
			}
		}
		_initParameterQueue();
		if (_realValueExists)
			return params;
		else
			return new ArrayList();
	}

	 protected  void _changeCondition(String parameterName, String parameterValue){
		if (parameterValue == null)
			_parameterQueue.put(parameterName, NULL_PARAMETER);
		else{
			_parameterQueue.put(parameterName, parameterValue);
			//update properties...
			_invalidateProperties(); //immer?
		}
	}

	protected void _invalidateProperties(){
		_isInvalid = true; //out of sync
		_pricingConditionSet.setInvalid();
	}

	public void close(){
	}

	public boolean isClosed(){
		return false;
	}

	public boolean isRelevant(String commandName) {
		return true;
	}

	/**
	 * @return
	 */
	public String getVariantConditionKey() {
		return _variantConditionKey;
	}
	


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * Required in order to store conditions in sorted order
	 * in TreeSet.
	 */
	public int compareTo(Object o) {
		DefaultIPCPricingCondition condition = (DefaultIPCPricingCondition)o;
		return this.techKey.getIdAsString().compareTo(condition.techKey.getIdAsString());
	}

	/**
	 * @return
	 */
	public boolean isVariantCondition() {
		return _variantConditionFlag;
	}

	/**
	 * @return
	 */
	public String getVariantConditionDescription() {
		return _variantConditionDescr;
	}

	
}