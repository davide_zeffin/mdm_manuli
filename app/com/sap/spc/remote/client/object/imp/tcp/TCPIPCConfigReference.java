/*
 * Created on Mar 15, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import com.sap.spc.remote.client.object.IPCConfigReference;

/**
 * @author I026584
 *TCPIPCConfigReference
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TCPIPCConfigReference extends IPCConfigReference {
	protected String host;
	protected String port;
	protected String encoding;
	protected boolean dispatcher;

public TCPIPCConfigReference() {
}
	
public TCPIPCConfigReference(
	  String host, String port, String encoding, String configId,
	  boolean dispatcher
) {
	  this.host = host;
	  this.port = port;
	  this.encoding = encoding;
	  this.configId = configId;
	  this.dispatcher = dispatcher;
}

	public String getHost() {
		return host;
	}
	public void setHost(String newHost) {
		host = newHost;
	}
	public void setPort(String newPort) {
		port = newPort;
	}
	public String getPort() {
		return port;
	}
	public void setEncoding(String newEncoding) {
		encoding = newEncoding;
	}
	public String getEncoding() {
		return encoding;
	}
//	public void setSessionId(String newSessionId) {
//		sessionId = newSessionId;
//	}
//	public String getSessionId() {
//		return sessionId;
//	}
//	public void setConfigId(String newConfigId) {
//		configId = newConfigId;
//	}
//	public String getConfigId() {
//		return configId;
//	}
	public void setDispatcher(boolean newDispatcher) {
		dispatcher = newDispatcher;
	}
	public boolean isDispatcher() {
		return dispatcher;
	}
}
