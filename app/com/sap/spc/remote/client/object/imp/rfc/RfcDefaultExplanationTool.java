package com.sap.spc.remote.client.object.imp.rfc;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import org.apache.struts.util.MessageResources;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ConflictParticipant;
import com.sap.spc.remote.client.object.DescriptiveConflict;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.DefaultExplanationTool;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.rfc.function.CfgApiGetConflAssumptions;
import com.sap.spc.remote.client.rfc.function.CfgApiGetCsticConflicts;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class RfcDefaultExplanationTool extends DefaultExplanationTool implements ExplanationTool {

	// logging
	private static final Category category =
	ResourceAccessor.category;
	private static final Location location =
		ResourceAccessor.getLocation(RfcDefaultExplanationTool.class);

   	protected RfcDefaultExplanationTool(
        IPCClient ipcClient,
        Configuration configuration) {
        this.ipcClient = ipcClient;
        this.configuration = configuration;
        this.closed = false;
        this.update();
        setUpdateConflicts(true);
        setUpdateDescriptiveConflicts(true);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultExplanationTool#readInConflicts(boolean, java.util.Locale, org.apache.struts.util.MessageResources)
     */
    protected void readInConflicts(
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources) {
        conflicts = new Hashtable();
        Hashtable solutionRateOfConflictParticipants = new Hashtable();
        sortedConflicts = new Vector();
        int assumptionGroups = 0;
        try {
            String configId = this.configuration.getId();
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof RfcDefaultIPCSession)) {
                throw new IllegalClassException(session, RfcDefaultIPCSession.class);
            }
            this.cachingClient = ((RfcDefaultIPCSession) session).getClient();

            JCO.Function functionCfgApiGetConflAssumptions = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_CONFL_ASSUMPTIONS);
            JCO.ParameterList im = functionCfgApiGetConflAssumptions.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiGetConflAssumptions.getExportParameterList();

            im.setValue(configId, CfgApiGetConflAssumptions.CONFIG_ID);     // STRING, Configuration Identifier

            category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_CONFL_ASSUMPTIONS");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetConflAssumptions); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_CONFL_ASSUMPTIONS");
            
            String oldGroupId = "";
            Conflict conflict = null;
            ConflictParticipant participant;
            String groupId;            

            // process table exGroupedAssumptions
            // TABLE, Configuration Conflict Handling: Table of grouped Assumption
            JCO.Table exGroupedAssumptions = ex.getTable(CfgApiGetConflAssumptions.GROUPED_ASSUMPTIONS);
            int numRows = exGroupedAssumptions.getNumRows();
            int exGroupedAssumptionsCounter;
            if (numRows < 1) {
                return;
            }
            for (exGroupedAssumptionsCounter=0; exGroupedAssumptionsCounter<numRows; exGroupedAssumptionsCounter++) {
                exGroupedAssumptions.setRow(exGroupedAssumptionsCounter);
                groupId = ((Integer)exGroupedAssumptions.getValue(CfgApiGetConflAssumptions.GROUP_ID)).toString();
                if (!oldGroupId.equals(groupId)) {
                    assumptionGroups++;
                    if (conflict != null) {
                        conflicts.put(conflict.getId(), conflict);
                    }
                    conflict =
                        factory.newConflict(
                            this.ipcClient,
                            this,
                            groupId);
                    oldGroupId = groupId;
                }
                
                // process table exGroupedAssumptionsAssumptions
                JCO.Table exGroupedAssumptionsAssumptions = exGroupedAssumptions.getTable(CfgApiGetConflAssumptions.ASSUMPTIONS);
                String assumptionId;
                String assumptionLName;
                String assumptionAuthor; 
                int exGroupedAssumptionsAssumptionsCounter;               
                for (exGroupedAssumptionsAssumptionsCounter=0; exGroupedAssumptionsAssumptionsCounter<exGroupedAssumptionsAssumptions.getNumRows(); exGroupedAssumptionsAssumptionsCounter++) {
                    exGroupedAssumptionsAssumptions.setRow(exGroupedAssumptionsAssumptionsCounter);
                    assumptionId = ((Integer)exGroupedAssumptionsAssumptions.getValue(CfgApiGetConflAssumptions.ASSUMPTION_ID)).toString();
                    assumptionLName = exGroupedAssumptionsAssumptions.getString(CfgApiGetConflAssumptions.ASSUMPTION_LNAME);
                    assumptionAuthor = exGroupedAssumptionsAssumptions.getString(CfgApiGetConflAssumptions.ASSUMPTION_AUTHOR);
                    participant = 
                        factory.newConflictParticipant(this.ipcClient,
                            conflict,
                            assumptionId,
                            assumptionLName,
                            assumptionAuthor.equals(ASSUMPTION_AUTHOR_USER)
                                ? true
                                : false,
                            assumptionAuthor.equals(ASSUMPTION_AUTHOR_DEFAULT)
                                ? true
                                : false,
                            locale,
                            resources);
                    conflict.addParticipant(participant);   
                    // Prearrangements for the calculation of the solution rate
                    if (!showConflictSolutionRate) {
                        continue;
                    }
                    String participantKey = participant.getKey();
                    if (solutionRateOfConflictParticipants.containsKey(participantKey)) {
                        Integer countInt = (Integer) solutionRateOfConflictParticipants.get(participantKey);
                        int count = countInt.intValue();
                        count++;
                        solutionRateOfConflictParticipants.put(participantKey, new Integer(count));
                    } else {
                        solutionRateOfConflictParticipants.put(participantKey, new Integer(1));
                    }
                }
            }
            if (conflict != null) {
                conflicts.put(conflict.getId(), conflict);
            }
        }catch(ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        Vector vConflicts = new Vector(conflicts.values());
        sortedConflicts = new Vector(vConflicts.size());
        Iterator iConflicts = vConflicts.iterator();
        int index = 0;
        while (iConflicts.hasNext()) {
            Conflict currentConflict = (Conflict) iConflicts.next();
            if (showConflictSolutionRate) {
                // participants solution rate
                Vector vParticipants = (Vector) currentConflict.getParticipants();
                Iterator iParticipants = vParticipants.iterator();
                while (iParticipants.hasNext()) {
                    ConflictParticipant participant = (ConflictParticipant) iParticipants.next();
                    int count =
                        ((Integer) solutionRateOfConflictParticipants
                            .get(participant.getKey()))
                            .intValue();
                    count = count * 100 / assumptionGroups;
                    participant.setSolutionRate(count);
                }
            }
            // sort of the conflicts
            int position = 0;
            int j = index - 1;
            for (int i = 0; i < index; i++) {
                if (currentConflict.countParticipants()
                    > ((Conflict) sortedConflicts.elementAt(j)).countParticipants()) {
                    position = j + 1;
                    break;
                }
                j = j / 2;
                position = j;
            }
            sortedConflicts.add(position, currentConflict);
            index++;
        }
        setUpdateConflicts(false);
        
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultExplanationTool#readInDescriptiveConflicts(java.lang.String, java.lang.String, java.lang.String, boolean, java.util.Locale, org.apache.struts.util.MessageResources, boolean, boolean, boolean)
     */
    protected void readInDescriptiveConflicts(
        String conflictExplanationLevel,        // APD doesn't support customizing of conflict tags anymore, so this is ignored
        String conflictExplanationTextBlockTag, // dito
        String conflictExplanationTextLineTag,  // dito
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool) {
        descriptiveConflicts = new Vector();
        descriptiveConflIndex = new Hashtable();
        try {
            String configId = this.configuration.getId();
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof RfcDefaultIPCSession)) {
                throw new IllegalClassException(session, RfcDefaultIPCSession.class);
            }
            this.cachingClient = ((RfcDefaultIPCSession) session).getClient();

            JCO.Function functionCfgApiGetCsticConflicts = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_CSTIC_CONFLICTS);
            JCO.ParameterList im = functionCfgApiGetCsticConflicts.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiGetCsticConflicts.getExportParameterList();

            im.setValue(configId, CfgApiGetCsticConflicts.CONFIG_ID);     // STRING, Configuration Identifier
            // im.setValue(csticName, "CSTIC_NAME");       // optional, CHAR, Characteristic name (object characteristic)
            // im.setValue(instId, "INST_ID");     // optional, CHAR, Internal identifier of a Instance
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_CSTIC_CONFLICTS");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetCsticConflicts); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_CSTIC_CONFLICTS");
            
            // process table exConflicts
            // TABLE, Confuguration Conflict Handling: Table of Conflict Explanati
            JCO.Table exConflicts = ex.getTable(CfgApiGetCsticConflicts.CONFLICTS);
            int numRows = exConflicts.getNumRows();
            int exConflictsCounter;
            if (numRows < 1) {
                return;
            }
            String[] instIds = new String[numRows];
            String[] csticNames = new String[numRows];
            String[] csticLNames = new String[numRows];
            String[] lTexts = new String[numRows];
            for (exConflictsCounter=0; exConflictsCounter<exConflicts.getNumRows(); exConflictsCounter++) {
                exConflicts.setRow(exConflictsCounter);
                instIds[exConflictsCounter] = exConflicts.getString(CfgApiGetCsticConflicts.CONFLICT_INST_ID);
                csticNames[exConflictsCounter] = exConflicts.getString(CfgApiGetCsticConflicts.CONFLICT_CSTIC_NAME);
                csticLNames[exConflictsCounter] = exConflicts.getString(CfgApiGetCsticConflicts.CONFLICT_CSTIC_LNAME);
				String conflictDescription = exConflicts.getString(CfgApiGetCsticConflicts.CONFLICT_TEXT);
				if (conflictDescription == null) lTexts[exConflictsCounter] = ""; //avoid NullPointerExceptions
				else lTexts[exConflictsCounter] = conflictDescription; 
            }

            conflictingInstances = instIds;
            conflictingCharacteristics = csticNames;
            for (int i = 0; i < instIds.length; i++) {
                String explanation = lTexts[i];
                DescriptiveConflict conflict = 
                    new DefaultDescriptiveConflict(
                        this,
                        instIds[i],
                        csticNames[i],
                        csticLNames[i],
                        explanation,
                        showConflictSolutionRate,
                        locale,
                        resources,
                        conflictExplanationTextLineTag,
                        useConflictHandlingShortcuts,
                        useShortcutValueConflict,
                        useExplanationTool);
                descriptiveConflicts.add(conflict);
                String key = instIds[i] + DEL + csticNames[i];
                if (descriptiveConflIndex.containsKey(key)) {
                    Vector values = (Vector) descriptiveConflIndex.get(key);
                    values.add(conflict);
                    descriptiveConflIndex.put(key, values);
                } else {
                    Vector values = new Vector(1, 1);
                    values.add(conflict);
                    descriptiveConflIndex.put(key, values);
                }
            }
            this.setUpdateDescriptiveConflicts(false);
        } catch (ClientException e) {            
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }        
    }

}
