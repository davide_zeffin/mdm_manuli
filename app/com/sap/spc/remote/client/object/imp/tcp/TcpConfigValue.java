package com.sap.spc.remote.client.object.imp.tcp;

import java.util.Dictionary;
import java.util.Vector;

import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.tcp.ExternalConfigConverter;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.imp.ConfigValue;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.shared.command.ExtConfigConstants;
import com.sap.spc.remote.shared.command.GetConfigItemInfo;
import com.sap.spc.remote.shared.command.SCECommand;
import com.sap.spc.remote.client.object.imp.*;

public class TcpConfigValue extends ConfigValue implements ExtConfigConstants {

    protected TcpConfigValue() {
        _cfg = new TableValue(new String[] { EXT_CONFIG_NAME, EXT_CONFIG_SCE_VERSION, EXT_CONFIG_SCE_MODE,
            EXT_CONFIG_KB_NAME, EXT_CONFIG_KB_VERSION, EXT_CONFIG_KB_BUILD, EXT_CONFIG_KB_PROFILE,
            EXT_CONFIG_KB_LANGUAGE, EXT_CONFIG_ROOT_ID, EXT_CONFIG_CONSISTENT, EXT_CONFIG_COMPLETE,
            EXT_CONFIG_INFO, EXT_CONFIG_POSITION
            });
        _ins = new TableValue(new String[] { EXT_INST_AUTHOR, EXT_INST_CLASS_TYPE, EXT_INST_COMPLETE,
            EXT_INST_CONFIG_ID, EXT_INST_CONSISTENT, EXT_INST_ID, EXT_INST_OBJECT_KEY, EXT_INST_OBJECT_TEXT,
            EXT_INST_OBJECT_TYPE, EXT_INST_QUANTITY, EXT_INST_QUANTITY_UNIT
            });
        _prt = new TableValue(new String[] { EXT_PART_AUTHOR, EXT_PART_CLASS_TYPE, EXT_PART_CONFIG_ID,
            EXT_PART_INST_ID, EXT_PART_OBJECT_KEY, EXT_PART_OBJECT_TYPE, EXT_PART_PARENT_ID,
            EXT_PART_POS_NR, EXT_PART_SALES_RELEVANT
            });
        _val = new TableValue(new String[] { EXT_VALUE_AUTHOR, EXT_VALUE_CONFIG_ID, EXT_VALUE_CSTIC_LNAME,
            EXT_VALUE_CSTIC_NAME, EXT_VALUE_CSTIC_NAME, EXT_VALUE_INSTANCE_ID, EXT_VALUE_LNAME, EXT_VALUE_NAME
            });
        _prc = new TableValue(new String[] { EXT_PRICE_CONFIG_ID, EXT_PRICE_FACTOR, EXT_PRICE_INSTANCE_ID,
            EXT_PRICE_KEY
            });

        _client = null;
        _config = null;
    }

    /**
     * @param external configuration
     */
   protected  TcpConfigValue(ext_configuration config) {
        this();
        setInitialValue(config);

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigValue#initializeValues(com.sap.spc.remote.client.object.imp.DefaultIPCSession, java.lang.String, java.lang.String)
     */
    public void initializeValues(
        DefaultIPCSession session,
        String documentId,
        String itemId) {
        _client = session.getClient();
		_config = null; // free for gc
		//No need of documentId and itemId in 50 for this command.

//        _cfg.init(_client, ISPCCommandSet.GET_CONFIG_ITEM_INFO, params);
//        _ins.init(_client, ISPCCommandSet.GET_CONFIG_ITEM_INFO, params);
//        _prt.init(_client, ISPCCommandSet.GET_CONFIG_ITEM_INFO, params);
//        _val.init(_client, ISPCCommandSet.GET_CONFIG_ITEM_INFO, params);
//        _prc.init(_client, ISPCCommandSet.GET_CONFIG_ITEM_INFO, params);
    }

	public void initProperties(String documentId, String itemId) {
		String[] params = new String[] { GetConfigItemInfo.DOCUMENT_ID, documentId,
										 GetConfigItemInfo.ITEM_ID, itemId };
                      

//			String[] cnfName = new String[_cfg.getColumnCount()];
//			String[] extPrtCnfId = new String[_cfg.getColumnCount()];
//			  String[] insauthor =  new String[_cfg.getColumnCount()];
//			  String[] insclassType =  new String[_cfg.getColumnCount()];
//			  String[] extInstCfgId =  new String[_cfg.getColumnCount()];
//		  String[] valauthor =  new String[_cfg.getColumnCount()];
//		  String[] extValCstName =  new String[_cfg.getColumnCount()];
//			String[] vkey = new String[_cfg.getColumnCount()];
//			String[] name = new String[_cfg.getColumnCount()];
//			String[] partOfNo = new String[_cfg.getColumnCount()];
//			String[] logsys = new String[_cfg.getColumnCount()];
//			String[] factor = new String[_cfg.getColumnCount()];
//			String[] sce = new String[_cfg.getColumnCount()];
//			String[] instId = new String[_cfg.getColumnCount()];
//			String[] complete = new String[_cfg.getColumnCount()];
//			String[] parentId = new String[_cfg.getColumnCount()];
//			String[] productType = new String[_cfg.getColumnCount()];
//			String[] valueUnit = new String[_cfg.getColumnCount()];
//			String[] version = new String[_cfg.getColumnCount()];
////			String[] itemId = new String[_cfg.getColumnCount()];
//			String[] valueTxt = new String[_cfg.getColumnCount()];
//			String[] quantityUnit = new String[_cfg.getColumnCount()];
//			String[] charc = new String[_cfg.getColumnCount()];
////			String[] documentId = new String[_cfg.getColumnCount()];
//			String[] objKey = new String[_cfg.getColumnCount()];
//			String[] productLogsys = new String[_cfg.getColumnCount()];
//			String[] profile = new String[_cfg.getColumnCount()];
//			String[] build = new String[_cfg.getColumnCount()];
//			String[] author = new String[_cfg.getColumnCount()];
//			String[] guid = new String[_cfg.getColumnCount()];
//			String[] charcTxt = new String[_cfg.getColumnCount()];
//			String[] language = new String[_cfg.getColumnCount()];
//			String[] info = new String[_cfg.getColumnCount()];
//			String[] rootId = new String[_cfg.getColumnCount()];
//			String[] objType = new String[_cfg.getColumnCount()];
//			String[] salesRelevant = new String[_cfg.getColumnCount()];
//			String[] value = new String[_cfg.getColumnCount()];
//			String[] consistent = new String[_cfg.getColumnCount()];
//			String[] objTxt = new String[_cfg.getColumnCount()];
//			String[] productId = new String[_cfg.getColumnCount()];
//			String[] quantity = new String[_cfg.getColumnCount()];
//			String[] configId = new String[_cfg.getColumnCount()];
//			String[] classType = new String[_cfg.getColumnCount()];
                               
			try {
				ServerResponse r = ((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.GET_CONFIG_ITEM_INFO, params);
				Vector dataVec = new Vector();
				copyTableDataFromResponse(r,_cfg,dataVec);
				_cfg.setData(dataVec);

				dataVec = new Vector();                                 
				copyTableDataFromResponse(r,_ins,dataVec);
				_ins.setData(dataVec);

				dataVec = new Vector();                                 
				copyTableDataFromResponse(r,_prt,dataVec);
				_prt.setData(dataVec);

				dataVec = new Vector();                                 
				copyTableDataFromResponse(r,_val,dataVec);
				_val.setData(dataVec);

				dataVec = new Vector();                                 
				copyTableDataFromResponse(r,_prc,dataVec);
				_prc.setData(dataVec);

			}
			catch(ClientException e) {
				throw new IPCException(e);
			}
		
	}
	protected void copyTableDataFromResponse(ServerResponse r, TableValue table, Vector data){
		String[] cols = table.getColumns();
		String[] singleData = null; 
		if (cols != null){
			for (int i = 0; i < cols.length; i++) {
				singleData = r.getParameterValues(cols[i]);
                // null is returned if the parameter does not exist
                // this should not be added to the data; add an empty array instead.
                if (singleData != null) {
    				data.add(singleData);
                }
                else {
                    data.add(new String[0]);		
                }
			}
		}
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigValue#getConfig(java.lang.String)
     */
    public ext_configuration getConfig(String client) throws IPCException {
        if (_client != null) {
            Integer rootId;
            try {
                rootId = new Integer(_cfg.getElement(EXT_CONFIG_ROOT_ID,0));
            }
            catch(Exception e) {
                // here, an exception may happen if the product isn't configurable at all. In this
                // case the GetItemConfig command that is responsible for reading these values has
                // been optimized away (because the command is not relevant for this item), and all
                // its values lead to ugly OpenValues
                return null;
            }
            int kbBuild = Integer.parseInt(_cfg.getElement(EXT_CONFIG_KB_BUILD,0));

            c_ext_cfg_imp writable = new c_ext_cfg_imp(   "",
                                                          "",
                                                          client,
                                                          _cfg.getElement(EXT_CONFIG_KB_NAME,0),
                                                          _cfg.getElement(EXT_CONFIG_KB_VERSION,0),
                                                          kbBuild,
                                                          _cfg.getElement(EXT_CONFIG_KB_PROFILE,0),
                                                          _cfg.getElement(EXT_CONFIG_KB_LANGUAGE,0),
                                                          rootId,
                                                          _cfg.getElement(EXT_CONFIG_CONSISTENT,0).equals("T"),
                                                          _cfg.getElement(EXT_CONFIG_COMPLETE,0).equals("T")
                                                          );

            String configInfo = _cfg.getElement(EXT_CONFIG_INFO,0);
            if (configInfo != null)
                writable.set_cfg_info(configInfo);

            for (int i=0; i<_ins.rows(); i++) {
                Dictionary d = _ins.getRowDictionary(i);
                Integer instId = new Integer( ((String)d.get(EXT_INST_ID)).trim() );
                String author = (String)d.get(EXT_INST_AUTHOR);
                if (author == null || author.equals(""))
                    author = " ";

                writable.add_inst(instId,
                                  (String)d.get(EXT_INST_OBJECT_TYPE),
                                  (String)d.get(EXT_INST_CLASS_TYPE),
                                  (String)d.get(EXT_INST_OBJECT_KEY),
                                  (String)d.get(EXT_INST_OBJECT_TEXT),
                                  author,
                                  (String)d.get(EXT_INST_QUANTITY),
                                  (String)d.get(EXT_INST_QUANTITY_UNIT),
                                  ((String)d.get(EXT_INST_CONSISTENT)).equals("T"),
                                  ((String)d.get(EXT_INST_COMPLETE)).equals("T"));
            }

            for (int i=0; i<_prt.rows(); i++) {
                Dictionary d = _prt.getRowDictionary(i);

                Integer instId = new Integer( ((String)d.get(EXT_PART_INST_ID)).trim() );
                Integer parentId = new Integer( ((String)d.get(EXT_PART_PARENT_ID)).trim() );

                String author = (String)d.get(EXT_PART_AUTHOR);
                if (author == null || author.equals(""))
                    author = " ";

                writable.add_part(parentId, instId,
                                  (String)d.get(EXT_PART_POS_NR),
                                  (String)d.get(EXT_PART_OBJECT_TYPE),
                                  (String)d.get(EXT_PART_CLASS_TYPE),
                                  (String)d.get(EXT_PART_OBJECT_KEY),
                                  author,
                                  d.get(EXT_PART_SALES_RELEVANT).equals(SCECommand.SALES_RELEVANT)); // 20020214-kha: IPC returns always "X" here
            }

            for (int i=0; i<_val.rows(); i++) {
                Dictionary d = _val.getRowDictionary(i);

                Integer instId = new Integer( ((String)d.get(EXT_VALUE_INSTANCE_ID)).trim() );
                String author = (String)d.get(EXT_VALUE_AUTHOR);
                if (author == null || author.equals("")) {
                    author = " ";
                }

                writable.add_cstic_value(instId,
                                         (String)d.get(EXT_VALUE_CSTIC_NAME),
                                         (String)d.get(EXT_VALUE_CSTIC_LNAME),
                                         (String)d.get(EXT_VALUE_NAME),
                                         (String)d.get(EXT_VALUE_LNAME),
                                         author);
            }

            for (int i=0; i<_prc.rows(); i++) {
                Dictionary d = _prc.getRowDictionary(i);
                Integer instId = new Integer( ((String)d.get(EXT_PRICE_INSTANCE_ID)).trim() );
                double  factor = Double.valueOf( ((String)d.get(EXT_PRICE_FACTOR)).trim() ).doubleValue();

                writable.add_price_key(instId,
                                       (String)d.get(EXT_PRICE_KEY),
                                       factor);
            }

            return writable;
        }
        else
            return _config;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigValue#getConfigIPCParameters(com.sap.spc.remote.client.util.ext_configuration)
     */
    public String[] getConfigIPCParameters(ext_configuration config) {
        if (config == null)
            return null;
        else {
            Vector configs = new Vector(1);
            configs.addElement(config);
            Vector resultVec = new Vector(100);
            ExternalConfigConverter.getConfigs(configs,null,0,1,new _VectorParameterSet(resultVec), false);
            String[] result = new String[resultVec.size()];
            resultVec.copyInto(result);
            return result;
        }
    }

}
