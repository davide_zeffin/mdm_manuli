package com.sap.spc.remote.client.object.imp.tcp;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;

public class ObjectTester {
	protected static IsaLocation log = IsaLocation.getInstance(ObjectTester.class.getName());
    private IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
	private String mandt;
	private String type;

    public ObjectTester(String mandt, String type) {
		this.mandt = mandt;
		this.type  = type;
    }

	public void run() {
		try {
			long t1 = System.currentTimeMillis();
			IPCClient client = factory.newIPCClient(mandt, type);
//			client.getUIContext().setLocale(java.util.Locale.GERMANY);
		    IPCSession session = client.getIPCSession();
			IPCDocumentProperties props = factory.newIPCDocumentProperties();
			props.setCountry("US");
			props.setLanguage("EN");
			props.setSalesOrganisation("0001");
			props.setDistributionChannel("01");
			props.setDocumentCurrency("EUR");
			props.setPricingProcedure("0CRM01");
			props.setLocalCurrency("EUR");
			IPCDocument doc = session.newDocument(props);
			DefaultIPCItemProperties[] iprops = new DefaultIPCItemProperties[5];
			for (int i=0; i<5; i++) {
				iprops[i] = factory.newIPCItemProperties();
			    iprops[i].setDate("20000110");
				if (i == 2)
					iprops[i].setProductId("LS-WPVARIANT1");
				else
			        iprops[i].setProductId("WP-940");
			}
			IPCItem[] items = doc.newItems(iprops);
			doc.pricing();
			for (int i=0; i<5; i++) {
			    String description = items[i].getProductDescription();
			    String price = items[i].getNetValue().toString();
			    System.out.println("Your "+description+" costs "+price);
				System.out.println("  Total price is "+items[i].getTotalGrossValue().toString());
				System.out.println("  Weight is "+items[i].getNetWeight().toString());
				System.out.println("  configurable is "+(items[i].isConfigurable() ? "true" : "false"));
				if (items[i].isConfigurable()) {
					System.out.println("  product logsys is "+items[i].getProductLogSys());
				}
				System.out.println("  product variant is "+(items[i].isProductVariant() ? "true" : "false"));
			}

			// test optimization
			/*items[1].setSalesQuantity(new DimensionalValue("1","PC"));
			items[1].setSalesQuantity(new DimensionalValue("2","PC"));
			items[2].setReturn(false);
			long tUncached = System.currentTimeMillis();
		    com.sap.spc.remote.client.object.DimensionalValue value = items[1].getBaseQuantity();
			value.getUnit();
			tUncached = System.currentTimeMillis()-tUncached;
			System.out.println("uncached time getbasequantity is "+tUncached);
			long tCached = System.currentTimeMillis();
			for (int i=0; i<10; i++) {
			    value = items[1].getBaseQuantity(); value.getUnit();
			}
			tCached = (System.currentTimeMillis()-tCached)/10;
			System.out.println("cached time getbasequantity is "+tCached);

		    long tSemiCached = System.currentTimeMillis();
			value = items[1].getSalesQuantity();
			value.getUnit();
			tSemiCached = System.currentTimeMillis()-tSemiCached;
			System.out.println("semi cached time is "+tSemiCached);

			value = items[1].getSalesQuantity();
			System.out.println("Old sales quantity is "+value.getValueAsString()+" "+value.getUnit());
			items[1].setSalesQuantity(new DimensionalValue("3","PC"));
			value = items[1].getSalesQuantity();
			System.out.println("New sales quantity is "+value.getValueAsString()+" "+value.getUnit());

			System.out.println("config test begins now");
			ext_configuration config = items[1].getConfig();
			System.out.println("KB is "+config.get_kb_name()+"/"+config.get_kb_version()+"/"+config.get_kb_profile_name());

			Configuration c = items[1].getConfiguration();
			Instance root = c.getRootInstance();
			Characteristic charac = root.getCharacteristic("WP_SCREEN");
			charac.setValues(new String[] { "001" });
			charac = root.getCharacteristic("WP_OPTIONS");
			charac.setValues(new String[] { "002" });
			config = items[1].getConfig();
			session.getClient().dumpStatistics();*/
			System.out.println("Multi-Level configuration/item test begins");
			DefaultIPCItemProperties sprops = factory.newIPCItemProperties();
			sprops.setProductId("LS-HOUSE");
			//sprops.setSalesQuantity(new DimensionalValue("1", "PC"));
			IPCItem item = doc.newItem(sprops);
			//item.getConfiguration().check();
			IPCItem[] all = doc.getAllItems();
			for (int i=0; i<all.length; i++) {
				System.out.println("item "+i+": "+all[i].getProductId()+", parent: "+
					( all[i].getParent() == null ? "null" : all[i].getParent().getProductId() ));
			}
			System.out.println("\nTOTAL TIME "+Long.toString(System.currentTimeMillis()-t1));
		}
		catch(IPCException e) {
			System.err.println("Caught an IPC exception");
			e.printStackTrace();
		}


	}


	private static class ObjectAttachTester {
		private TCPIPCItemReference reference;

		public ObjectAttachTester(String host, String port, String encoding, String session) {
			reference = new TCPIPCItemReference();
			reference.setHost(host);
			reference.setPort(port);
			reference.setEncoding(encoding);
//			reference.setSessionId(session);
		}

		public void run() {
			try {
				IPCClient client = IPCClientObjectFactory.getInstance().newIPCClient();
				IPCSession session = client.getIPCSession();
				IPCDocument[] documents = session.getDocuments();
				for (int i=0; i<documents.length; i++) {
					IPCItem[] items = documents[i].getItems();
					for (int j=0; j<items.length; j++) {
						System.out.println(items[i].getProductId());
					}
				}
			}
			catch(IPCException e) {log.debug(e.getMessage(),e);}
		}
	}

    public static void main(String[] args) {
		String mandt = args.length > 0 ? args[0]: "700";
		String type  = "ISA";
        ObjectTester objectTester1 = new ObjectTester(mandt,type);
		objectTester1.run();
		ObjectAttachTester objectTester2 = new ObjectAttachTester("localhost","4444","UnicodeLittle", "2");
		objectTester2.run();
    }
}