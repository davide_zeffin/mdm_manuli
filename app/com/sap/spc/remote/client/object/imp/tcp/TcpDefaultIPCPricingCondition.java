/*
 * Created on Dec 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import java.util.Hashtable;


import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.imp.DefaultIPCPricingCondition;
import com.sap.spc.remote.client.object.imp.DefaultIPCPricingConditionSet;
import com.sap.spc.remote.shared.command.ChangePricingConditions;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TcpDefaultIPCPricingCondition
	extends DefaultIPCPricingCondition
	implements IPCPricingCondition {



//		Constructor
		  /**
		   * Constructs the pricing condition.
		   */
	public TcpDefaultIPCPricingCondition( DefaultIPCPricingConditionSet pricingConditionSet,
	String  stepNo,
	String  counter,
	String  headerCounter,
	String  conditionTypeName,
	String  description,
	String  conditionRate,
	String  conditionCurrency,
	String  pricingUnitValue,
	String  pricingUnitUnit,
	String  pricingUnitDimension,
	String  conditionValue,
	String  documentCurrency,
	String  conversionNumerator,
	String  conversionDenominator,
	String  conversionExponent,
	String  conditionBaseValue,

	String  exchangeRate,
	String  directExchangeRate,
	boolean isDirectExchangeRate,
	String  factor,
	boolean  variantConditionFlag,
	String  variantConditionKey,
	String  variantFactor,
	String  variantConditionDescription,
	boolean  isStatistical,
	char     inactive,
	char     conditionClass,
	char     calculationType,
	char     conditionCategory,
	char     conditionControl,
	char     conditionOrigin,
	char     scaleType,

	boolean  changeOfRatesAllowed,
	boolean  changeOfPricingUnitsAllowed,
	boolean  changeOfValuesAllowed,
	boolean  deletionAllowed) {
		
		this(pricingConditionSet,
		 stepNo,
		counter,
		conditionTypeName,
		description,
		conditionRate,
		conditionCurrency,
		pricingUnitValue,
		pricingUnitUnit,
		pricingUnitDimension,
		conditionValue,
		documentCurrency,
		conversionNumerator,
		conversionDenominator,
		conversionExponent,
		conditionBaseValue,

		exchangeRate,
	    directExchangeRate,
		isDirectExchangeRate,
		factor,
		variantConditionFlag,
		variantConditionKey,
		variantFactor,
	    variantConditionDescription,
		isStatistical,
	    inactive,
		conditionClass,
		calculationType,
		conditionCategory,
		conditionControl,
		conditionOrigin,
		scaleType,

	   changeOfRatesAllowed,
	   changeOfPricingUnitsAllowed,
	   changeOfValuesAllowed,
	   deletionAllowed);
	   _headerCounter = headerCounter;
	}



	//Constructor
	/**
	 * Constructs the pricing condition.
	 */
	protected TcpDefaultIPCPricingCondition( DefaultIPCPricingConditionSet pricingConditionSet,
									String  stepNo,
									String  counter,
									String  conditionTypeName,
									String  description,
									String  conditionRate,
									String  conditionCurrency,
									String  pricingUnitValue,
									String  pricingUnitUnit,
									String  pricingUnitDimension,
									String  conditionValue,
									String  documentCurrency,
									String  conversionNumerator,
									String  conversionDenominator,
									String  conversionExponent,
									String  conditionBaseValue,

									String  exchangeRate,
									String  directExchangeRate,
									boolean isDirectExchangeRate,
									String  factor,
									boolean  variantConditionFlag,
		                            String  variantConditionKey,
									String  variantFactor,
                                    String  variantConditionDescription,
									boolean  isStatistical,
									char     inactive,
									char     conditionClass,
									char     calculationType,
									char     conditionCategory,
									char     conditionControl,
									char     conditionOrigin,
									char     scaleType,

									boolean  changeOfRatesAllowed,
									boolean  changeOfPricingUnitsAllowed,
									boolean  changeOfValuesAllowed,
									boolean  deletionAllowed){
		super(new TechKey(stepNo+counter));
		_pricingConditionSet = pricingConditionSet;
		_documentId = pricingConditionSet.getDocumentId();
		_itemId = pricingConditionSet.getItemId();
		_client = pricingConditionSet.getClient();
		_stepNo = stepNo;
		_counter = counter;
		_conditionTypeName = conditionTypeName;
		_description = description;
		_conditionRate = conditionRate;
		_conditionCurrency = conditionCurrency;
		_pricingUnitValue = pricingUnitValue;
		_pricingUnitUnit = pricingUnitUnit;
		_pricingUnitDimension = pricingUnitDimension;
		_conditionValue = conditionValue;
		_documentCurrency = documentCurrency;
		_conversionNumerator = conversionNumerator;
		_conversionDenominator = conversionDenominator;
		_conversionExponent = conversionExponent;
		_conditionBaseValue = conditionBaseValue;

		_exchangeRate = exchangeRate;
		_directExchangeRate = directExchangeRate;
		_isDirectExchangeRate = isDirectExchangeRate;
		_variantConditionFlag = variantConditionFlag;
		_variantConditionKey = variantConditionKey;
		_factor = factor;
		_variantFactor = variantFactor;
        _variantConditionDescr = variantConditionDescription;
		_isStatistical = isStatistical;
		_inactive = inactive;
		_conditionClass = conditionClass;
		_calculationType = calculationType;
		_conditionCategory = conditionCategory;
		_conditionControl = conditionControl;
		_conditionOrigin = conditionOrigin;
		_scaleType = scaleType;

		_changeOfRatesAllowed = changeOfRatesAllowed;
		_changeOfPricingUnitsAllowed = changeOfPricingUnitsAllowed;
		_changeOfValuesAllowed = changeOfValuesAllowed;
		_deletionAllowed = deletionAllowed;
		_initParameterQueue();
	}



		//********Implementation for Abstract methods starts from here!********

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCPricingCondition#_initParameterQueue()
	 */
	protected void _initParameterQueue() 
	{
		if (_parameterQueue == null)
			_parameterQueue = new Hashtable(5);
		else
			_parameterQueue.clear();
		//insert default (null) values
		_parameterQueue.put(ChangePricingConditions.CONDITION_VALUES, NULL_PARAMETER);
		_parameterQueue.put(ChangePricingConditions.CONDITION_RATES, NULL_PARAMETER);
		_parameterQueue.put(ChangePricingConditions.CONDITION_CURRENCIES, NULL_PARAMETER);
		_parameterQueue.put(ChangePricingConditions.PRICING_UNIT_VALUES, NULL_PARAMETER);
		_parameterQueue.put(ChangePricingConditions.PRICING_UNIT_UNITS, NULL_PARAMETER);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setConditionRate(java.lang.String)
	 */
	public void setConditionRate(String conditionRate) 
	{
		if (conditionRate.equals(_conditionRate))
			conditionRate = null;
		_changeCondition(ChangePricingConditions.CONDITION_RATES, conditionRate);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setConditionCurrency(java.lang.String)
	 */
	public void setConditionCurrency(String conditionCurrency) 
	{
		if (conditionCurrency.equals(_conditionCurrency))
			conditionCurrency = null;
		_changeCondition(ChangePricingConditions.CONDITION_CURRENCIES, conditionCurrency);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setPricingUnitValue(java.lang.String)
	 */
	public void setPricingUnitValue(String pricingUnitValue) 
	{
		if (pricingUnitValue.equals(_pricingUnitValue))
			pricingUnitValue = null;
		_changeCondition(ChangePricingConditions.PRICING_UNIT_VALUES, pricingUnitValue);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setPricingUnitUnit(java.lang.String)
	 */
	public void setPricingUnitUnit(String pricingUnitUnit) 
	{
		if (pricingUnitUnit.equals(_pricingUnitUnit))
			pricingUnitUnit = null;
		_changeCondition(ChangePricingConditions.PRICING_UNIT_UNITS, pricingUnitUnit);
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setConditionValue(java.lang.String)
	 */
	public void setConditionValue(String conditionValue) 
	{
		if(_conditionValue.equals(conditionValue))
		 conditionValue = null;
		_changeCondition(ChangePricingConditions.CONDITION_VALUES, conditionValue);
		
	}

/******* Method added for Manual Condition  *****/


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#getExternalConditionValue(java.lang.String, java.lang.String)
	 */
	public String getExternalConditionValue(String decimalSeparator, String groupingSeparator) {
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		String externalConditionValue = pc.convertCurrencyValueInternalToExternal(_conditionValue, _documentCurrency, decimalSeparator, groupingSeparator );
		if (externalConditionValue == null || externalConditionValue.equals("")) {
			return _conditionValue;
		}else {
			return externalConditionValue;
		}
	}

	public String getExternalConditionRateValue(String decimalSeparator, String groupingSeparator) {
		String externalConditionRateValue;
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		if (_conditionCurrency.equals("%")) {
			externalConditionRateValue = pc.convertValueInternalToExternal(_conditionRate, _conditionCurrency, decimalSeparator, groupingSeparator );
		}else {
			externalConditionRateValue = pc.convertCurrencyValueInternalToExternal(_conditionRate, _conditionCurrency, decimalSeparator, groupingSeparator );
		}
		if (externalConditionRateValue == null || externalConditionRateValue.equals("")) {
			return _conditionRate;
		}else {
			return externalConditionRateValue;
		}		
	}



	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#setExternalPricingUnitValue(java.lang.String)
	 */
	public void setExternalPricingUnitValue(String externalPricingUnitValue, String decimalSeparator, String groupingSeparator) {
		String internalPricingUnitValue;
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		if (externalPricingUnitValue != null) {
			internalPricingUnitValue = pc.convertValueExternalToInternal(externalPricingUnitValue, _pricingUnitUnit, decimalSeparator, groupingSeparator);
		}else {
			internalPricingUnitValue = null;
		}
		_pricingUnitValue = internalPricingUnitValue;
	}



	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCPricingCondition#getExternalPricingUnitValue()
	 */
	public String getExternalPricingUnitValue(String decimalSeparator, String groupingSeparator) {
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		String externalPricingUnitValue = pc.convertValueInternalToExternal(_pricingUnitValue, _pricingUnitUnit, decimalSeparator, groupingSeparator );
		if (externalPricingUnitValue == null || externalPricingUnitValue.equals("")) {
			return _pricingUnitValue;
		}else {
			return externalPricingUnitValue;
		}		
	}
	//********Implementation for Abstract methods ends here!********

	/******* Method added for Manual Condition  *****/

	public void setConditionValue(String conditionValue, String decimalSeparator, String groupingSeparator) 
	{
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
		String internalConditionValue = pc.convertCurrencyValueInternalToExternal(_conditionValue, _documentCurrency, decimalSeparator, groupingSeparator );
		String paramInternalConditionValue = pc.convertCurrencyValueExternalToInternal(conditionValue, _documentCurrency, decimalSeparator, groupingSeparator );
		String paramExternalConditionValue = pc.convertCurrencyValueInternalToExternal(paramInternalConditionValue, _documentCurrency, decimalSeparator, groupingSeparator );
				
		if (paramExternalConditionValue.equals(internalConditionValue))
		 conditionValue = null;
		_changeCondition(ChangePricingConditions.CONDITION_VALUES, conditionValue);

	}
	public void setConditionRate(String conditionRate, String decimalSeparator, String groupingSeparator) 
		{

			TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_pricingConditionSet.getIPCSession()).getPricingConverter();
			String paramInternalConditionRate = pc.convertCurrencyValueExternalToInternal(conditionRate, _documentCurrency, decimalSeparator, groupingSeparator );

			if (paramInternalConditionRate.equals(_conditionRate))
				conditionRate = null;
			
			_changeCondition(ChangePricingConditions.CONDITION_RATES, paramInternalConditionRate);

		
		}
	
	/******* Method added for Manual Condition  *****/
}
