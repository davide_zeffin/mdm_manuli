/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.Arrays;
//import java.util.Collection;
//import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCMessage;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.StepId;
import com.sap.spc.remote.client.object.ValueDescriptionPairs;
//import com.sap.spc.remote.shared.command.AddPricingConditions;
//import com.sap.spc.remote.shared.command.ChangePricingConditions;
//import com.sap.spc.remote.shared.command.CommandConstants;
//import com.sap.spc.remote.shared.command.GetAllCurrencyUnits;
//import com.sap.spc.remote.shared.command.GetAllPhysicalUnits;
//import com.sap.spc.remote.shared.command.GetAvailableConditionTypes;
//import com.sap.spc.remote.shared.command.GetAvailablePhysicalUnits;
//import com.sap.spc.remote.shared.command.GetConditions;
//import com.sap.spc.remote.shared.command.GetItemConditions;
//import com.sap.spc.remote.shared.command.GetItemInfo;
//import com.sap.spc.remote.shared.command.RemovePricingConditions;
//import com.sap.spc.remote.shared.command.SetClientParameters;
import com.sap.spc.remote.client.ClientException;
//import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.msasrv.constants.ServerConstants;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
* Holds a set of IPricingCondition instances
*/
public abstract class DefaultIPCPricingConditionSet extends BusinessObjectBase implements IPCPricingConditionSet{

	 protected IPCDocument     _document = null;
	 protected IPCItem         _item = null;
	 protected IClient   _client = null;
	 protected IPCSession      _session = null;

	 protected List          _conditions = null;
	 protected List            _messages = null;
	 protected IPCMessage[]    _collectedMessages = new IPCMessage[0];
	 protected List            _conditionsToBeAdded = null;
	 protected List            _conditionsToBeRemoved = null;

	 protected boolean         _isOutOfSync = true; //out of sync

	 protected static final String   STEPNO_NULL = "000";
	 protected static final String   COUNTER_NULL = "000";

	 protected boolean         _isHeader = false;


	/**
	 * Returns an external (BAPI) representation of the conditions
	 */
	public HashMap getExternalPricingConditions() throws IPCException {
		HashMap data;

        // 2005-03-09: removed according to kha: old 4.0 cmds should not be used anymore
		// setParameterNaming("BAPI-R3");
		data = _getExternalPricingConditions();
		// setParameterNaming("DEFAULT");

		return data;
    }

	protected abstract HashMap _getExternalPricingConditions();

	protected abstract void _retrieveConditionsFromServer() throws IPCException;

	public static String formatStepNo(String stepNo){
		StringBuffer formattedStepNo = new StringBuffer();
		if (stepNo.length() < 3)
			formattedStepNo.append(STEPNO_NULL.substring(0, 3 - stepNo.length()));
		formattedStepNo.append(stepNo);
		return formattedStepNo.toString();
	}

	public static String formatCounter(String counter){
		StringBuffer formattedCounter = new StringBuffer();
		if (counter.length() < 3)
			formattedCounter.append(COUNTER_NULL.substring(0, 3 - counter.length()));
		formattedCounter.append(counter);
		return formattedCounter.toString();
	}

	/**
	 * @return all pricing conditions in this set as an array of IPCPricingConditions
	 * instances.
	 */
	public IPCPricingCondition[] getPricingConditions() throws IPCException{
		if (isOutOfSync())
			//get conditions from server
			_retrieveConditionsFromServer();
		IPCPricingCondition[] conditionsArray = new IPCPricingCondition[_conditions.size()];
		_conditions.toArray(conditionsArray);
		return conditionsArray;
	}

	/**
	 * Adds a new pricing condition to the set
	 */
	public abstract void addPricingCondition(    String  conditionTypeName,
									    String  conditionRate,
									    String  conditionCurrency,
									    String  conditionPricingUnitValue,
									    String  conditionPricingUnitUnit,
									    String  conditionValue,
									    String decimalSeparator,
									    String groupingSeparator);

	 protected abstract StepIdSet _executeAdditions() throws IPCException;

	/**
	 * Removes a pricing condition from the set
	 */
	public abstract void removePricingCondition(String stepNo, String counter);

	public abstract void _executeRemovals() throws IPCException;

	public void addMessages(DefaultIPCMessageSet ipcMessageSet){
		_messages.add(ipcMessageSet);
	}

	/**
	 * Commits changes made on this set.
	 */
	public StepId[] commit() throws IPCException{
		_executeChanges();
		StepIdSet stepIdSet = _executeAdditions();
		_executeRemovals(); 
		_collectMessages(); //will force execution
        
        if (_collectedMessages.length > 0){
            StringBuffer msgs = new StringBuffer();
            msgs.append("AP-PRC engine returned message(s): ");
            for (int i=0; i<_collectedMessages.length; i++){
                msgs.append(_collectedMessages[i].getMessage());
                msgs.append("; ");
            }                        
			throw new IPCException(new ClientException("500", msgs.toString()));
		}

		if(stepIdSet != null){
			String[] stepNos = stepIdSet.getStepNos();
			String[] counters = stepIdSet.getCounters();
			StepId[] stepIds = new StepId[stepNos.length];
			for (int i=0; i<stepNos.length; i++){
				stepIds[i] = new StepId(stepNos[i], counters[i]);
			}
		    return stepIds;
		}
		else
			return null;
	}

	 protected abstract void _executeChanges() throws IPCException;

	protected void _collectMessages() throws IPCException{
		ArrayList ipcMessageList = new ArrayList();
		Iterator iterator = _messages.iterator();
		while(iterator.hasNext()){
			ipcMessageList.addAll(Arrays.asList(((DefaultIPCMessageSet)iterator.next()).getMessages())); //this will force execution
		}
		IPCMessage[] messageArray = new IPCMessage[ipcMessageList.size()];
		ipcMessageList.toArray(messageArray);
		_collectedMessages = messageArray;
	}

	/**
	 * @return messages occurred since last call
	 */
	public IPCMessage[] getMessages(){
		//return messages stored in set and in all pricing conditions and reset list
		_messages.clear();
		IPCMessage[] messages = (IPCMessage[])_collectedMessages.clone();
		_collectedMessages = new IPCMessage[0];
		return messages;
	}

	public void setOutOfSync(){
		_isOutOfSync = true;
	}

    public boolean isOutOfSync() {
        return _isOutOfSync;
    }

	public IClient getClient(){
		return _client;
	}

	public String getDocumentId(){
		return _document.getId();
	}

	public String getItemId(){
		return _item!=null?_item.getItemId():null;
	}

	public String getLanguage() throws IPCException{
		return _document.getLanguage();
	}

	protected String getCountry() throws IPCException{
		return _document.getCountry();
	}

	 protected static String _translateBoolean(boolean theBoolean){
		return theBoolean?ServerConstants.YES:ServerConstants.NO;
	}

	/****************************************************************************
	 * methods used for value selection
	 ****************************************************************************/

	//value selection
	 protected ValueDescriptionPairs _availableConditionTypeNames = null;
	 protected ValueDescriptionPairs _allPhysicalUnits = null;
	 protected ValueDescriptionPairs _availableQuantityUnits = null;
	 protected ValueDescriptionPairs _allCurrencyUnits = null;
	 protected	Hashtable _availablePhysicalUnits = new Hashtable();

	/**
	 * @return all names of condition types available in the current pricing procedure.
	 */
	public abstract ValueDescriptionPairs getAvailableConditionTypeNames() throws IPCException;

	public abstract ValueDescriptionPairs getAllPhysicalUnits() throws IPCException;

	/**
	 * @return all names of physical units available.
	 */
	public abstract ValueDescriptionPairs getAvailablePhysicalUnits(String dimensionName) throws IPCException;

	/**
	 * @return all names of quantity units available.
	 */
	public abstract ValueDescriptionPairs getAvailableQuantityUnits() throws IPCException;

	/**
	 * @return all names of currencies available.
	 */
	public abstract ValueDescriptionPairs getAllCurrencyUnits() throws IPCException;

	/****************************************************************************
	 * Step Id class
	 ****************************************************************************/
	protected class StepIdSet {

		String[] /*Value*/ _stepNos  = null;
		String[] /*Value*/ _counters  = null;

		public StepIdSet(String[] /*Value*/ stepNos, String[] /*Value*/ counters) {
			_stepNos = stepNos;
			_counters = counters;
		}

		public String[] getStepNos() throws IPCException{
			String[] stepNos = _stepNos/*().getContent()*/;
			if (stepNos == null)
				return new String[0];
			return stepNos;
		}

		public String[] getCounters() throws IPCException{
			String[] counters = _counters/*.getContent()*/;
			if (counters == null)
				return new String[0];
			for(int i=0;i<counters.length;i++){
				counters[i] = formatCounter(counters[i]);
			}
			return counters;
		}
	}

    /**
     * Container for messages occured while executing commands on the IPC.
     */
    protected class DefaultIPCMessageSet/* implements IPCMessageSet*/ {

        protected String[] /*Value*/   _objectIds = null;
        protected String[] /*Value*/   _messageTypes = null;
        protected String[] /*Value*/   _messages = null;

        public DefaultIPCMessageSet(String[] /*Value*/ objectIds, String[] /*Value*/ messageTypes, String[] /*Value*/ messages){
            _objectIds = objectIds;
            _messageTypes = messageTypes;
            _messages = messages;
        }

        public IPCMessage[] getMessages() throws IPCException{
            String[] messages = _messages/*.getContent()*/;
            String[] objectIds = _objectIds/*.getContent()*/;
            String[] messageTypes = _messageTypes/*.getContent()*/;
            if (messages == null)
                return new IPCMessage[0];
            IPCMessage[] ipcMessages = new IPCMessage[messages.length];
            for (int i = 0; i < messages.length; i++){
                ipcMessages[i] = new DefaultIPCMessage( objectIds[i],
                                                        messageTypes[i],
                                                        messages[i]);
            }
            return ipcMessages;
        }
    }



    /**
     * Message occured while executing commands on the IPC.
     * Messages will be collected in the IPCMessageSet
     */
    class DefaultIPCMessage implements IPCMessage{

        protected String   _message = null;
        protected String   _messageType = null;
        protected String   _objectId = null;

        DefaultIPCMessage(String objectId, String messageType, String message){
            _message = message;
            _messageType = messageType;
            _objectId = objectId;
        }

        public String getMessage(){
            return _message;
        }

        public String getMessageType(){
            return _messageType;
        }

        public String getObjectId(){
            return _objectId;
        }

    }

	/**
	 * @return
	 */
	public IPCSession getIPCSession() {
		return _session;
	}

    /**
     * Method returns the Calculation type of the first pricing condition fitting to requirements:</br>
     * - Condition Class = B (Price)</br>
     * - is NOT inactive</br>
     * @return Calculation type (e.g M, N, O, P) or <code>null</code> (if no active price exists)
     */
    public static String determineCalculationType(IPCPricingCondition[] IPCPricingCondition) {
    	String retVal = null;
    	for (int i = 0; i < IPCPricingCondition.length; i++) {
    		if (IPCPricingCondition[i] != null                         &&
			    IPCPricingCondition[i].getConditionClass() == 'B'      &&
			    IPCPricingCondition[i].getInactive() == ' ')    		{
                retVal = String.valueOf(IPCPricingCondition[i].getCalculationType());
            }
            if (retVal != null){
                // break the loop if the first pricing condition meets the requirements
                break;
            }
    	}
    	
    	return retVal;
    }
}