package com.sap.spc.remote.client.object.imp.rfc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.IPCSetValuesException;
import com.sap.spc.remote.client.rfc.function.CfgApiDeleteCsticsValues;
import com.sap.spc.remote.client.rfc.function.CfgApiSetCsticsValues;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class RfcConfigurationChangeStream extends ConfigurationChangeStream {
	
	//logging
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(RfcConfigurationChangeStream.class);

    protected RfcConfigurationChangeStream(Configuration configuration, IClient cachingClient) {
        m_ConfigurationChanges = new Vector();
        m_Configuration = configuration;
        m_CachingClient = cachingClient;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigurationChangeStream#executeSetCommand(java.util.HashMap)
     */
    protected void executeSetCommand(HashMap setOpsByInstance) {
        String configId = m_Configuration.getId();
        String formatValues = TRUE;
        HashMap notSetValuesByInstance = new HashMap(); 
        Exception exceptionForNotSetValues = null;
    
        
            Iterator setInstIterator = setOpsByInstance.keySet().iterator();
        String instId = "";
            while (setInstIterator.hasNext()) {
            JCO.Function functionCfgApiSetCsticsValues;
            try {
                functionCfgApiSetCsticsValues = ((RFCDefaultClient)m_CachingClient).getFunction(IFunctionGroup.CFG_API_SET_CSTICS_VALUES);
            } catch (ClientException e) {
                throw new IPCException(e);
            }            
                JCO.ParameterList im = functionCfgApiSetCsticsValues.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiSetCsticsValues.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiSetCsticsValues.getTableParameterList();
                
            ArrayList valuesOfInstanceToBeSet = new ArrayList();
            
            instId = (String) setInstIterator.next();
                HashMap setOpsByCstic = (HashMap) setOpsByInstance.get(instId);
                if (setOpsByCstic.size() > 0) {
                    im.setValue(configId, CfgApiSetCsticsValues.CONFIG_ID);     // STRING, Configuration Identifier
                    im.setValue(formatValues, CfgApiSetCsticsValues.FORMAT_VALUES);     // optional, CHAR, 'T'=True, 'F'=False, ''=False
                    im.setValue(instId, CfgApiSetCsticsValues.INST_ID);     // CHAR, Internal identifier of a Instance
            
                    Iterator setCsticIterator = setOpsByCstic.keySet().iterator();
            
                    // process table imValuesToSet
                    // optional, TABLE, Table of characteristics with values to delete
                    JCO.Table imValuesToSet = im.getTable(CfgApiSetCsticsValues.VALUES_TO_SET);                
                    while (setCsticIterator.hasNext()) {
                        imValuesToSet.appendRow();
                        String csticName = (String) setCsticIterator.next();
                        imValuesToSet.setValue(csticName, CfgApiSetCsticsValues.CSTIC_NAME);        // CHAR, Characteristic name (object characteristic)
                        ArrayList valuesToBeSet = (ArrayList) setOpsByCstic.get(csticName);
            
                        // process table imValuesToSetCsticValues
                        JCO.Table imValuesToSetCsticValues = imValuesToSet.getTable(CfgApiSetCsticsValues.CSTIC_VALUES);                   
                        for (int i = 0; i < valuesToBeSet.size(); i++) {
                            imValuesToSetCsticValues.appendRow();
                            CharacteristicValue value = (CharacteristicValue) valuesToBeSet.get(i);
                            String valueName = value.getName();
                            String formatValue  = (value.isAdditionalValue()) ? TRUE : FALSE;                        
                            imValuesToSetCsticValues.setValue(valueName, CfgApiSetCsticsValues.VALUE_NAME);     // CHAR, Value
                            imValuesToSetCsticValues.setValue(formatValue, CfgApiSetCsticsValues.FORMAT_VALUE);     // CHAR, Boolean (T = True, F = False, Blank = Unknown)
                        valuesOfInstanceToBeSet.add(valueName);
                        }
                    }
                }
            try {
                category.logT(Severity.PATH, location, "executing RFC CFG_API_SET_CSTICS_VALUES");
                ((RFCDefaultClient)m_CachingClient).execute(functionCfgApiSetCsticsValues);
                category.logT(Severity.PATH, location, "done with RFC CFG_API_SET_CSTICS_VALUES");
        } catch (ClientException e) {
                notSetValuesByInstance.put(instId, valuesOfInstanceToBeSet);
                exceptionForNotSetValues = e;
        }
    }
        // check whether values for a deleted instances could not be set
        if (notSetValuesByInstance.size()>0){
            throw new IPCSetValuesException(exceptionForNotSetValues, notSetValuesByInstance);
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.ConfigurationChangeStream#executeDeleteCommand(java.util.HashMap)
     */
    protected void executeDeleteCommand(HashMap deleteOpsByInstance) {
        String configId = m_Configuration.getId();
        String formatValues = TRUE;
        
        try {
            Iterator delInstIterator = deleteOpsByInstance.keySet().iterator();
            while (delInstIterator.hasNext()) {
                JCO.Function functionCfgApiDeleteCsticsValues = ((RFCDefaultClient)m_CachingClient).getFunction(IFunctionGroup.CFG_API_DELETE_CSTICS_VALUES);
                JCO.ParameterList im = functionCfgApiDeleteCsticsValues.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiDeleteCsticsValues.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiDeleteCsticsValues.getTableParameterList();
                String instId = (String) delInstIterator.next();
                HashMap deleteOpsByCstic = (HashMap) deleteOpsByInstance.get(instId);
        
                ArrayList serverCommand = new ArrayList();
        
                if (deleteOpsByCstic.size() > 0) {
                    im.setValue(configId, CfgApiDeleteCsticsValues.CONFIG_ID);     // STRING, Configuration Identifier
                    im.setValue(formatValues, CfgApiDeleteCsticsValues.FORMAT_VALUES);     // CHAR, T=True, F=False, ''=False
                    im.setValue(instId, CfgApiDeleteCsticsValues.INST_ID);     // CHAR, Internal identifier of a Instance
        
                    Iterator delCsticIterator = deleteOpsByCstic.keySet().iterator();
    
                    // process table imValuesToDelete
                    // TABLE, Table of characteristics with values to delete
                    JCO.Table imValuesToDelete = im.getTable(CfgApiDeleteCsticsValues.VALUES_TO_DELETE);
                    while (delCsticIterator.hasNext()) {
                        imValuesToDelete.appendRow();
                        String csticName = (String) delCsticIterator.next();
                        imValuesToDelete.setValue(csticName, CfgApiDeleteCsticsValues.CSTIC_NAME);     // CHAR, Characteristic name (object characteristic)
                        ArrayList valuesToBeDeleted = (ArrayList) deleteOpsByCstic.get(csticName);
                        
                        // process table imValuesToDeleteCsticValues
                        JCO.Table imValuesToDeleteCsticValues = imValuesToDelete.getTable(CfgApiDeleteCsticsValues.CSTIC_VALUES);                    
                        for (int i = 0; i < valuesToBeDeleted.size(); i++) {
                            imValuesToDeleteCsticValues.appendRow();
                            CharacteristicValue value = (CharacteristicValue) valuesToBeDeleted.get(i);
                            String valueName = value.getName();
                            String formatValue  = (value.isAdditionalValue()) ? TRUE : FALSE;                        
                            imValuesToDeleteCsticValues.setValue(valueName, CfgApiDeleteCsticsValues.VALUE_NAME);      // CHAR, Value
                            imValuesToDeleteCsticValues.setValue(formatValue, CfgApiDeleteCsticsValues.FORMAT_VALUE);      // CHAR, Boolean (T = True, F = False, Blank = Unknown)
                        }
                    }
                }
                category.logT(Severity.PATH, location, "executing RFC CFG_API_DELETE_CSTICS_VALUES");
                ((RFCDefaultClient)m_CachingClient).execute(functionCfgApiDeleteCsticsValues);
                category.logT(Severity.PATH, location, "done with RFC CFG_API_DELETE_CSTICS_VALUES");      
            }
        } catch (ClientException e) {
            throw new IPCException(e);
        }
    }

}
