package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.ConflictInfo;

public class DefaultConflictInfo implements ConflictInfo {

	protected String conflictId;
	protected String conflictingInstanceId;
	protected String conflictingInstanceName;
	protected String conflictText;
	protected String conflictExplanation;
	protected String conflictDocumentation;

	public DefaultConflictInfo(String conflictId,
						String conflictingInstanceId,
						String conflictingInstanceName,
						String conflictText,
						String conflictExplanation,
						String conflictDocumentation) {
		this.conflictId = conflictId;
		this.conflictingInstanceId = conflictingInstanceId;
		this.conflictingInstanceName = conflictingInstanceName;
		this.conflictText = conflictText;
		this.conflictExplanation = conflictExplanation;
		this.conflictDocumentation = conflictDocumentation;
	}

	public String getConflictId() {
		return this.conflictId;
	}

	public String getConflictingInstanceId() {
		return this.conflictingInstanceId;
	}

	public String getConflictingInstanceName() {
		return this.conflictingInstanceName;
	}

	public String getConflictText() {
		return this.conflictText;
	}

	public String getConflictExplanation() {
		return this.conflictExplanation;
	}

	public String getConflictDocumentation() {
		return this.conflictDocumentation;
	}
}