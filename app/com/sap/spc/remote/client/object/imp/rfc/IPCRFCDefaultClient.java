package com.sap.spc.remote.client.object.imp.rfc;

import java.util.Properties;

import com.sap.isa.core.Constants;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I026584
 */
public class IPCRFCDefaultClient extends RFCDefaultClient {

	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(IPCRFCDefaultClient.class);

	public IPCRFCDefaultClient()throws ClientException{
		super();
	}
	
	// in the standalone scenarios CRM online, ERP and VMS the
	// IPCClient is not using a connection of another backend.
	// The IPCClient needs to initialize the connection with the
	// correct language and connection parameter passed from outside
	boolean isInitialized(JCoConnection connection) {
		if (connection == null){ //avoid compiler warning
			return false;
		}
		// appserver, sysnumber and client are set by the logon module,
		// which also reads the language from the SSO ticket.
		// so in this case there is no need to initialize the connection 
		// start actions will not set language, if SSO logon happened
		//
		// if the SSO login failed, there is the need to initialize here.
		// But in this case the start actions will set the language.
		//
		// some scenarios do not use the logon module, but still set the language
		// or the client. the there is the need to initialize
		if (this.language != null || this.client != null){	
			if (firstCall) {
				// ipc specific stuff and first call, so there is the need to initilize
				return false;
			}
		}
		return true;
	}

	
	
	Properties getConnectionProperties(JCoConnection connection) {
		Properties conProps = new Properties();
		if (this.language != null) {
			conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, this.language);
		}
		if (connection.isPooled()) {
			//connection only used for one session, not pooled
			conProps.setProperty(JCoManagedConnectionFactory.JCO_MAXCON, "0");			
		}
		// new implementaion, properties from backendcontext
		String server = (String) getContext().getAttribute(Constants.RP_JCO_INIT_APPLICATION_SERVER);
        String sysNr = (String) getContext().getAttribute(Constants.RP_JCO_INIT_SYSTEM_NUMBER);
        String client = (String) getContext().getAttribute(Constants.RP_JCO_INIT_CLIENT);
		if (server != null && sysNr != null && client!= null) {
			conProps.setProperty(JCoManagedConnectionFactory.JCO_ASHOST, server);
			conProps.setProperty(JCoManagedConnectionFactory.JCO_SYSNR, sysNr);
			conProps.setProperty(JCoManagedConnectionFactory.JCO_CLIENT, client);
		}
		// some scenarios set only client in old style via API, so we still need this
		if (this.client != null) {
			conProps.setProperty(JCoManagedConnectionFactory.JCO_CLIENT, this.client);
		}
		return conProps;
		
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP#getDefaultJCoConnection()
	 */
	public JCoConnection getDefaultIPCJCoConnection() {

		JCoConnection connection = super.getDefaultJCoConnection();
		
		
		if (isInitialized(connection)){
			return connection;
		}
		
		Properties conProps = getConnectionProperties(connection);
      
		if (conProps.size() != 0) {
			try {
				firstCall = false;
				return getDefaultJCoConnection(conProps);
			} catch (BackendException e) {
				throw new IPCException(e);
			}
		}else {
			return connection;
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.rfc.RFCDefaultClient#getIPCStatelessConnection(java.lang.String)
	 */
	public JCoConnection getIPCStatelessConnection(String language) {
		JCoConnection ipcJcoCon;
		try {
			ipcJcoCon = (JCoConnection)getConnectionFactory().getConnection(
						   com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
						   com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS);
		} catch (BackendException e) {
			throw new IPCException(e);
		}
		
		if (ipcJcoCon.isValid()) {
			return ipcJcoCon;
		}
		
		Properties conProps = getConnectionProperties(ipcJcoCon);
		
		if (conProps.size() != 0) {
			try {
				return (JCoConnection)getConnectionFactory().getConnection(
				com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
				com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS, conProps);
			} catch (BackendException e1) {
				throw new IPCException(e1);
			}
		}else {
			return ipcJcoCon;
		}
	}
}
