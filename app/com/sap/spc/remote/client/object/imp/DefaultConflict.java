package com.sap.spc.remote.client.object.imp;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ConflictParticipant;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

public class DefaultConflict extends BusinessObjectBase implements Conflict {

	protected ExplanationTool           explanationTool;
	protected IPCClient                 ipcClient;
	protected boolean                   closed;
	protected Hashtable                 participants;
	protected Hashtable                 participantsByKey;
	protected String                    id;

	DefaultConflict(IPCClient ipcClient, ExplanationTool explanationTool, String id){
		super(new TechKey(id));
		this.ipcClient = ipcClient;
		participants = new Hashtable();
		participantsByKey = new Hashtable();
		this.explanationTool = explanationTool;
		this.id = id;
	}

	DefaultConflict(ExplanationTool explanationTool, String id) {
		super(new TechKey(id));
		participants = new Hashtable();
		participantsByKey = new Hashtable();
		this.explanationTool = explanationTool;
		this.id = id;
    }

	public void addParticipant(ConflictParticipant participant){
		participants.put(participant.getId(), participant);
		participantsByKey.put(participant.getKey(), participant);
	}
	public List getParticipants(){
		Vector vParticipants = new Vector(participants.values());
        return (List)vParticipants;

	}
	public ConflictParticipant getParticipant(String participantId){
	    return (ConflictParticipant)participants.get(participantId);
	}

    public ConflictParticipant getParticipantByKey(String key){
	    return (ConflictParticipant)participantsByKey.get(key);
	}

	public String getId(){
	    return this.id;
	}

	public int countParticipants(){
	    return this.participants.size();
	}

	public ExplanationTool getExplanationTool(){
	    return this.explanationTool;
	}

	public boolean isRelevant(String commandName){
	    // we don't use the cache
		return true;
	}
	public boolean isClosed(){
        return closed;
	}
	public void close(){
        this.closed = true;
	}
}