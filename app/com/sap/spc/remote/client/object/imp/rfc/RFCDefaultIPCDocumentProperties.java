package com.sap.spc.remote.client.object.imp.rfc;

import java.util.ArrayList;

import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocumentProperties;

/**
 * @author I026584
 *
 */
public class RFCDefaultIPCDocumentProperties
	extends DefaultIPCDocumentProperties {

		public RFCDefaultIPCDocumentProperties() {
			m_salesOrganisation     = "";
			m_distributionChannel   = "";
			m_division              = "";
			m_language              = "";
			m_documentCurrency      = "";
			m_localCurrency         = "";
			m_DepartureCountry      = "";
			m_country               = "";
			m_pricingProcedure      = "";
			m_DepartureRegion       = "";
			m_headerAttributeNames  = new ArrayList();
			m_headerAttributeValues = new ArrayList();
			m_externalId            = "";
			m_editMode              = ' ';
			
			m_decimalSeparator      = null;
			m_groupingSeparator     = null;
			m_groupConditionProcessing = true;
            m_stripDecimalPlaces    = true;
		}
		public RFCDefaultIPCDocumentProperties(IPCDocumentProperties initialData) {
				this();
	
				m_salesOrganisation = initialData.getSalesOrganisation();
				m_distributionChannel = initialData.getDistributionChannel();
				m_division = initialData.getDivision();
				m_language = initialData.getLanguage();
				m_documentCurrency = initialData.getDocumentCurrency();
				m_localCurrency = initialData.getLocalCurrency();
				m_DepartureCountry = initialData.getDepartureCountry();
				m_country = initialData.getCountry();
				m_pricingProcedure = initialData.getPricingProcedure();
				m_DepartureRegion = initialData.getDepartureRegion();

                String[] headerAttributeNamesArray = initialData.getAllHeaderAttributeNames();
                for (int i=0; i<headerAttributeNamesArray.length; i++){
                    m_headerAttributeNames.add(headerAttributeNamesArray[i]);    
                }
                
                String[] headerAttributeValuesArray = initialData.getAllHeaderAttributeValues();
                for (int i=0; i<headerAttributeValuesArray.length; i++){
                    m_headerAttributeValues.add(headerAttributeValuesArray[i]);
                }

				m_externalId = initialData.getExternalId();
				m_editMode = initialData.getEditMode();
	
				m_decimalSeparator = initialData.getDecimalSeparator();
				m_groupingSeparator     = initialData.getGroupingSeparator();
				m_groupConditionProcessing = initialData.isGroupConditionProcessingEnabled();
                m_stripDecimalPlaces = initialData.isStripDecimalPlaces();
			}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setSalesOrganisation(java.lang.String)
     */
    public void setSalesOrganisation(String salesOrganisation) {
        m_salesOrganisation = salesOrganisation;
        m_headerAttributeNames.add("SALES_ORG");
        m_headerAttributeValues.add(salesOrganisation);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDepartureCountry(java.lang.String)
     */
    public void setDepartureCountry(String departureCountry) {
        m_DepartureCountry = departureCountry;
        m_headerAttributeNames.add("TAX_DEPT_CTY");
        m_headerAttributeValues.add(departureCountry);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDepartureRegion(java.lang.String)
     */
    public void setDepartureRegion(String departureRegion) {
        m_DepartureRegion = departureRegion;
        m_headerAttributeNames.add("TAX_DEPT_REG");
        m_headerAttributeValues.add(departureRegion);

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDistributionChannel(java.lang.String)
     */
    public void setDistributionChannel(String distributionChannel) {
        m_distributionChannel = distributionChannel;
        m_headerAttributeNames.add("DIS_CHANNEL");
        m_headerAttributeValues.add(distributionChannel);

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCDocumentProperties#setDivision(java.lang.String)
     */
    public void setDivision(String division) {
        m_division = division;
        m_headerAttributeNames.add("DIVISION");
        m_headerAttributeValues.add(division);

    }

}
