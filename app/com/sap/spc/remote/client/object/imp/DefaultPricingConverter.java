package com.sap.spc.remote.client.object.imp;

import java.util.Map;
import com.sap.spc.remote.client.object.IPricingConverter;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;


public class DefaultPricingConverter implements IPricingConverter {

    // caches
    protected Map internalToExternalUOM; 
    protected Map externalToInternalUOM; 
    protected Map numberOfDecimalsForUOMs;
    protected Map externalUOMNamesAndDescriptions; 
    protected Map currencyDescriptions; 
    protected Map numberOfDecimalsForCurrencies;
    
    protected DefaultIPCSession _session; 

}
