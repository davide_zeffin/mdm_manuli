/*
 * Created on 29.04.2005
 *
 */
package com.sap.spc.remote.client.object.imp.rfc;

import com.sap.isa.core.eai.BackendBusinessObjectParams;

/**
 * @author 
 *
 */
public class IPCClientBackendBOParams implements BackendBusinessObjectParams {
	String language;
	String appServer = null;
	String sysNumber = null;
	String client = null;
	String connectionKey;
	Object iPCBOManager;

	
	/**
	 * Default constructor
	 *
	 */
	public IPCClientBackendBOParams() {
	}
	/**
	 * 
	 */
	public IPCClientBackendBOParams(String language) {
		this.language = language;
	}
	
	public IPCClientBackendBOParams(String client, String language, String appServer, String sysNumber) {
		this(language);
		this.client = client;
		this.appServer = appServer;
		this.sysNumber = sysNumber;
	}

	/**
	 * @return
	 */
	public String getAppServer() {
		return appServer;
	}

	/**
	 * @return
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @return
	 */
	public String getSysNumber() {
		return sysNumber;
	}

	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public String getConnectionKey() {
		return connectionKey;
	}

	/**
	 * @param string
	 */
	public void setConnectionKey(String string) {
		connectionKey = string;
	}

	/**
	 * @return
	 */
	public Object getIPCBOManager() {
		return iPCBOManager;
	}

	/**
	 * @param object
	 */
	public void setIPCBOManager(Object object) {
		iPCBOManager = object;
	}

}
