package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.TTEConstants;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.spc.remote.client.object.TTEItem;
import com.sap.spc.remote.client.object.TTEItemBusinessPartner;

public abstract class DefaultTTEItem extends BusinessObjectBase implements TTEItem, TTEConstants {

    protected TTEDocument tteDocument;
    protected String itemId;
    protected String refItemId;
    protected String quantity;
    protected String positionNumber;
    protected String unit;
    protected String incoterms1;
    protected String incoterms2;
    protected String businessProcess;
    protected String directionIndicator;
    protected String productId;
    protected ArrayList itemBusinessPartners;
    
    protected DefaultTTEItem(){
        initAttributes();
    }

    protected DefaultTTEItem(  String itemId,
                            IPCItemProperties itemProps, 
                            IPCDocumentProperties docProps, 
                            TTEDocument tteDoc, 
                            boolean createItemOnServer) {
        super(new TechKey(itemId));
        initAttributes(itemId, itemProps, docProps, tteDoc);
        if (createItemOnServer){
            // create the item on the server; not implemented 
            // (add the TTEItem to the TTEDocument and call modifyDocumentOnServer() instead) 
        }
    }

    private void initAttributes(String itemId, 
                                IPCItemProperties itemProps, 
                                IPCDocumentProperties docProps, 
                                TTEDocument tteDoc){    
        HashMap headerAttributes = (HashMap) itemProps.getHeaderAttributes();
        HashMap itemAttributes = (HashMap) itemProps.getItemAttributes();
        HashMap documentHeaderAttributes = (HashMap) docProps.getHeaderAttributes();
        this.tteDocument = tteDoc;
        this.itemId = itemId;
        this.refItemId = "";
        this.positionNumber = getAttributeValue(itemAttributes, TTE_POSNR);
        DimensionalValue salesQuantity = itemProps.getSalesQuantity();
        DimensionalValue baseQuantity = itemProps.getBaseQuantity();            
        if (baseQuantity != null && baseQuantity.getValueAsString() != null) {
            this.quantity = baseQuantity.getValueAsString();
            this.unit = baseQuantity.getUnit();
        } else if (salesQuantity != null && salesQuantity.getValueAsString() != null) {
            this.quantity = salesQuantity.getValueAsString();
            this.unit = salesQuantity.getUnit();
        } else {
            this.quantity = "";
            this.unit = "";
            
        }
        this.incoterms1 = getAttributeValue(documentHeaderAttributes, INCOTERMS1);
        this.incoterms2 = getAttributeValue(documentHeaderAttributes, INCOTERMS2);
        this.businessProcess = "100";
        this.directionIndicator = "01";
        // product information
        this.productId = itemProps.getProductGuid();
        // item business partners
        this.itemBusinessPartners = createItemBusinessPartners(documentHeaderAttributes);
    }

    private void initAttributes(){    
        this.tteDocument = null;
        this.itemId = "";
        this.refItemId = "";
        this.positionNumber = "";         
        this.quantity = "";
        this.unit = "";
        this.incoterms1 = "";
        this.incoterms2 = "";
        this.businessProcess = "100";
        this.directionIndicator = "01";
        // product information
        this.productId = "";
        // item business partners
        this.itemBusinessPartners = new ArrayList();
    }

    /**
     * @param docHeaderAttributes
     * @return list of item business partners (actually: ship-from and ship-to)
     */
    private ArrayList createItemBusinessPartners(HashMap docHeaderAttributes) {
        ArrayList bpList = new ArrayList();        
        // create ship-from item business partner
        TTEItemBusinessPartner businessPartnerSF = IPCClientObjectFactory.getInstance().newTTEItemBusinessPartner(
            TTEItemBusinessPartner.SHIP_FROM, // role
            getAttributeValue(docHeaderAttributes, SALES_ORG), //businessPartnerId
            "", //country
            "", //region
            "", //county 
            "", //city 
            "", //postalCode
            "", //geoCode 
            "", //exemptedRegion 
            ""); //jurisdictionCode
        bpList.add(businessPartnerSF);
        // create ship-to item business partner
        TTEItemBusinessPartner businessPartnerST = IPCClientObjectFactory.getInstance().newTTEItemBusinessPartner(
            TTEItemBusinessPartner.SHIP_TO, // role
            getAttributeValue(docHeaderAttributes, TTE_PARTNER_ID), //businessPartnerId
            "", //country
            "", //region
            "", //county 
            "", //city 
            "", //postalCode
            "", //geoCode 
            "", //exemptedRegion 
            ""); //jurisdictionCode 
        bpList.add(businessPartnerST);
        return bpList;
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#changeBusinessPartners(java.util.HashMap)
     */
    public void changeBusinessPartners(HashMap docHeaderAttributes) {
        String shipFromBPId = getAttributeValue(docHeaderAttributes, SALES_ORG);
        String shipToBPId = getAttributeValue(docHeaderAttributes, TTE_PARTNER_ID);
        
        // change shipFrom-businessPartner only if it has been passed via docHeaderAttributes
        if (!shipFromBPId.equals("")){
            TTEItemBusinessPartner shipFromBP = getBusinessPartnerByRole(TTEItemBusinessPartner.SHIP_FROM);
            if (shipFromBP != null){
                shipFromBP.setBusinessPartnerId(shipFromBPId);
            }
        }
        // change shipTo-businessPartner only if it has been passed via docHeaderAttributes
        if (!shipToBPId.equals("")){
            TTEItemBusinessPartner shipToBP = getBusinessPartnerByRole(TTEItemBusinessPartner.SHIP_TO);
            if (shipToBP != null){
                shipToBP.setBusinessPartnerId(shipToBPId);
            }
        }
    }

    /**
     * The attributes can be stored either in the item-header attributes, in the item attributes or 
     * in the document-header attributes.<br>
     * Attributes on item-level overwrite attributes on document level.<br> 
     * First we search in item attributes. If this was not successful we search
     * in the item-header attributes. If this was still not successfull we search in the document-header
     * attributes.
     * @param docHeaderAttributes document-header attributes
     * @param headerAttributes item-header attributes
     * @param itemAttributes item attrbitues
     * @param attributeName
     * @return the value if found. Otherwise an empty String ("").
     */
    private String getAttributeValue(HashMap docHeaderAttributes, HashMap headerAttributes, HashMap itemAttributes, String attributeName) {
        String attributeValue = (String) itemAttributes.get(attributeName);
        if (attributeValue == null) {
            attributeValue = (String) headerAttributes.get(attributeName);
            if (attributeValue == null){
                attributeValue = (String) docHeaderAttributes.get(attributeName);
                if (attributeValue == null){
                    attributeValue = "";
                }
            }
        }
        return attributeValue;
    }
    
    private String getAttributeValue(HashMap attributes, String attributeName) {
        String attributeValue = (String) attributes.get(attributeName);
        if (attributeValue == null) {
            attributeValue = "";
        }
        return attributeValue;
    }
    
    /**
     * Searches for a list of attribute names. 
     * See also @see DefaultTTEItem#getAttributeValue(HashMap, HashMap, HashMap, String).
     * @param docHeaderAttributes document-header attributes
     * @param headerAttributes item-header attributes
     * @param itemAttributes item attrbitues
     * @param attributeNames list of attribute names
     * @return a list of values
     */
    private ArrayList getAttributeValues(HashMap docHeaderAttributes, HashMap headerAttributes, HashMap itemAttributes, String[] attributeNames) {
        int max = attributeNames.length;
        ArrayList values = new ArrayList();
        for (int i = 0; i < max; i++){
            String value = getAttributeValue(docHeaderAttributes, headerAttributes, itemAttributes, attributeNames[i]);
            if (value != null && !value.equals("")) {
                values.add(value);
            }
        }
        return values;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getBusinessProcess()
     */
    public String getBusinessProcess() {
        return businessProcess;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getIncoterms1()
     */
    public String getIncoterms1() {
        return incoterms1;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getIncoterms2()
     */
    public String getIncoterms2() {
        return incoterms2;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getItemBusinessPartners()
     */
    public ArrayList getItemBusinessPartners() {
        return itemBusinessPartners;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getItemId()
     */
    public String getItemId() {
        return itemId;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getProductId()
     */
    public String getProductId() {
        return productId; 
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getQuantity()
     */
    public String getQuantity() {
        return quantity;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getTteDocument()
     */
    public TTEDocument getTteDocument() {
        return tteDocument;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getUnit()
     */
    public String getUnit() {
        return unit;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getDirectionIndicator()
     */
    public String getDirectionIndicator() {
        return directionIndicator;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getPositionNumber()
     */
    public String getPositionNumber() {
        return positionNumber;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEItem#getRetItemId()
     */
    public String getRetItemId() {
        return refItemId;
    }

    
    /**
     * Returns the first occurrence in the list of business partners specified by the role 
     * that is passed to this method. The method returns null if a business partner with the 
     * given role cannot be found.
     * @param role (ship-from or ship-to)
     * @return first occurrence of BP with the given role
     */
    private TTEItemBusinessPartner getBusinessPartnerByRole(String role){
        TTEItemBusinessPartner bp = null;
        for (int i=0; i<itemBusinessPartners.size(); i++){
            TTEItemBusinessPartner currentBP = (TTEItemBusinessPartner)itemBusinessPartners.get(i);
            if ((currentBP != null) && ((currentBP.getRole()).equals(role))){
                bp = currentBP;
                break;
            }
        }
        return bp;
    }

}
