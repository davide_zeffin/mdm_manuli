package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.ProductVariant;
import com.sap.spc.remote.client.object.ProductVariantFeature;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;


public abstract class DefaultProductVariant extends BusinessObjectBase implements ProductVariant {
    
    protected IPCClient ipcClient;
    protected String id;
    protected double distance;
    protected String matchPoints;
    protected String maxMatchPoints;
    protected Hashtable features;
    protected ProductVariantFeature mainDifference;
    protected boolean closed;
    protected boolean selected;
    protected Configuration configuration;
    protected IPCItem ipcItem = null;
    protected String productLogSys;
    protected String productType;
    protected String productGuid;
    
    DefaultProductVariant() {
    }

    protected DefaultProductVariant(
        IPCClient ipcClient,
        String id,
        double distance,
        String matchPoints,
        String maxMatchPoints,
        List features,
        ProductVariantFeature mainDifference) {
        	super(new TechKey(id));
        this.ipcClient = ipcClient;
        this.id = id;
        this.distance = distance;
        this.matchPoints = matchPoints;
        this.maxMatchPoints = maxMatchPoints;
        setFeatures(features);
        this.mainDifference = mainDifference;
        this.closed = false;
        this.selected = false;
    }
    
    /**
     * Set-Get-Methods for the product id of the variant
     */
    public void setId(String id) {
        this.id = id;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }
    
    public Configuration getConfiguration() {
        return configuration;
    }
    
    /**
     * Set-Get-Methods for the distance of the variant (0..infinity)
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }
    
    public double getDistance() {
        return this.distance;
    }
    
    /**
     * Set-Get-Methods for the match point:
     * how good each match is (0..maxMatchPoint, derived from distance).
     */
    public void setMatchPoints(String matchPoints) {
        this.matchPoints = matchPoints;
    }
    
    public String getMatchPoints() {
        return this.matchPoints;
    }
    
    public String getMaxMatchPoints() {
        return this.maxMatchPoints;
    }
    
    /**
     * Set-Get-Methods for the features of the product variant
     */
    public void setFeatures(List features) {
        this.features = new Hashtable(features.size());
        Iterator ifeatures = features.iterator();
        while (ifeatures.hasNext()) {
            ProductVariantFeature feature = (ProductVariantFeature) ifeatures.next();
            String key = feature.getId();
            this.features.put(key, feature);
        }
    }
    
    public List getFeatures() {
        return new ArrayList(features.values());
    }
    
    /**
     * special methods for a single feature
     */
    public void addFeature(ProductVariantFeature feature) {
        features.put(feature.getId(), feature);
    }
    
    public ProductVariantFeature getFeature(String featureId) {
        return (ProductVariantFeature) features.get(featureId);
    }
    
    /**
    * Set-Get-Methods for the main difference
    */
    public void setMainDifference(ProductVariantFeature mainDifference) {
        this.mainDifference = mainDifference;
    }
    
    public ProductVariantFeature getMainDifference() {
        return this.mainDifference;
    }
    
    public void close() {
        closed = true;
    }
    
    public boolean isClosed() {
        return closed;
    }
    
    public boolean isRelevant(String commandName) {
        // we don't use the cache
        return true;
    }
    
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    public boolean isSelected() {
        return this.selected;
    }
    
    public void setItem(IPCItem ipcItem) {
        this.ipcItem = ipcItem;
    }
    
    public IPCItem getItem() {
        return ipcItem;
    }
    
    public String toString() {
        return "[configuration:"
            + configuration
            + "; distance:"
            + distance
            + "; features:"
            + toString(features)
            + "; id:"
            + id
            + "; ipcItem:"
            + ipcItem
            + "; mainDifference:"
            + mainDifference
            + "; matchPoints:"
            + matchPoints
            + "; maxMatchPoints:"
            + maxMatchPoints
            + "selected:"
            + selected
            + "]";
    }
    
    protected String toString(Hashtable features) {
        StringBuffer result = new StringBuffer();
        for (Enumeration e = features.elements(); e.hasMoreElements();) {
            DefaultProductVariantFeature feature = (DefaultProductVariantFeature) e.nextElement();
            result.append(feature);
        }
        return result.toString();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ProductVariant#setProductLogSys(java.lang.String)
     */
    public void setProductLogSys(String logSys) {
        this.productLogSys = logSys;

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ProductVariant#setProductType(java.lang.String)
     */
    public void setProductType(String type) {
        this.productType = type;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ProductVariant#getAttributesFromProductMaster(java.lang.String)
     */
    public abstract Map getAttributesFromProductMaster(String application);

}