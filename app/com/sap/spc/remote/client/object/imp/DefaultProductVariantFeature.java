package com.sap.spc.remote.client.object.imp;

import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.ProductVariant;
import com.sap.spc.remote.client.object.ProductVariantFeature;


public class DefaultProductVariantFeature implements ProductVariantFeature{

    protected IPCClient ipcClient;
    protected ProductVariant productVariant;
    protected String type;
    protected String id;
    protected String name;
    protected String lname;
    protected String longtext;
    protected String value;
    protected String lvalue;
    protected double weight;
    protected double distance;
    
    DefaultProductVariantFeature() {
    }
    
    DefaultProductVariantFeature(
        IPCClient ipcClient,
        ProductVariant productVariant,
        String type,
        String id,
        String name,
        String lname,
        String longtext,
        String value,
        String lvalue,
        double weight,
        double distance) {
        this.ipcClient = ipcClient;
        this.productVariant = productVariant;
        this.type = type;
        this.id = id;
        this.name = name;
        this.lname = lname;
        this.longtext = longtext;
        this.value = value;
        this.lvalue = lvalue;
        this.weight = weight;
        this.distance = distance;
    }
    
    /**
     * Set-Get-Methods for the type of the feature:
     * what kind of feature, currently only: CSTIC.
     */
    public void setType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return this.type;
    }
    
    /**
     * Set-Get-Methods for the id:
     * string identifying the feature (eg. "KBCA_A0001.KBCA_TECHNOLOGY").
     */
    public void setId(String Id) {
        this.id = Id;
    }
    
    public String getId() {
        return this.id;
    }
    
    /**
     * Set-Get-Methods for the name:
     * language dependent name of the feature (may be null)
     */
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getLanguageDependentName() {
        return this.lname;
    }
    
    /**
     * Set-Get-Methods for the longtext:
     * language dependent long text of the feature (may be null)
     */
    public void setLongtext(String longtext) {
        this.longtext = longtext;
    }
    
    public String getLongtext() {
        return this.longtext;
    }
    
    /**
     * Set-Get-Methods for the feature value:
     * the value of the feature for that product variant
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public String getLanguageDependentValue() {
        return lvalue;
    }
    
    /**
     * Set-Get-Methods for the feature weight:
     * doubles holding the feature weights
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    public double getWeight() {
        return this.weight;
    }
    
    /**
     * Set-Get-Methods for the feature distance:
     * doubles holding the contribution of the feature to the distance.
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }
    
    public double getDistance() {
        return this.distance;
    }
    
    public String toString() {
        return "\n[distance:"
            + distance
            + "; id:"
            + id
            + "; lname:"
            + lname
            + "; longtext:"
            + longtext
            + "; lvalue:"
            + lvalue
            + "; name:"
            + name
            + "; type:"
            + type
            + "; value:"
            + value
            + "; weight:"
            + weight
            + "]";
    }
}