package com.sap.spc.remote.client.object.imp.rfc;

public interface RfcConstants {

    // general RFC constants
    public static final String TRUE             = "T";
    public static final String FALSE            = "F";
    public static final String YES              = "Y";
    public static final String NO               = "N";    
    public static final String USER             = "U";
    public static final String SYSTEM           = "S";
    public static final String SCE              = "1";    
    public static final String EXCEPTION_FORM   = "PARAMETER_EXCEPTION";
    public static final String SALES_RELEVANT   = "X";    
    public static final String TEXT_FORMAT      = "TEXT_FORMAT";
    public static final String TEXT_LINE        = "TEXT_LINE";
    public static final String TEXT_LINE_ID     = "TEXT_LINE_ID";
    public static final String XFLAG            = "X";
    public static final String SPACEFLAG            = " ";
    
}
