package com.sap.spc.remote.client.object.imp.rfc;

import java.util.Properties;

import com.sap.isa.core.Constants;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I026584
 * This class always
 */
public class ISARFCDefaultClient extends RFCDefaultClient implements IClient {

	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(ISARFCDefaultClient.class);

	public ISARFCDefaultClient()throws ClientException{
		super();
	}
	
	//in the ISA scenarios somebody outside the IPC clientcould have initialized the 
	//connection.
	boolean isInitialized(JCoConnection connection) {
		if (connection.isValid()) {
			return true;
		}else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP#getDefaultJCoConnection()
	 */
	public JCoConnection getDefaultIPCJCoConnection() {
		Properties conProps = new Properties();
		

		if (this.language != null) {
			conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, this.language);
		}
		//if the caller passed a connection key, the IPC is supposed to use this connection
		if (connectionKey != null && !connectionKey.equals("")) {
			try {
				return (JCoConnection)getConnectionFactory().getConnection(connectionKey);
			} catch (BackendException e) {
				location.errorT(category, "Connection " + connectionKey + " does not exist in connection factory!");
			} 
		}

		//if the user has maintained a dedicated IPCStateful connection,
		//(e.g. customers with ERP < 5.0 need a Netweaver 05 system)
		//the IPC is supposed to use this connection.
		try {
			JCoConnection ipcJcoCon = (JCoConnection)getConnectionFactory().getConnection(
										   com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
			com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_IPC_STATEFUL,
			conProps);
			if (isInitialized(ipcJcoCon)) {
				return ipcJcoCon;
			}
		} catch (BackendException e1) {
			if (location.beDebug()) {
				location.debug(category, "Connection " + com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_IPC_STATEFUL + "is not specified. Using default connection.", e1 );
			}			
		}

		//no connection key, no dedicated connection, return the default connection
		JCoConnection connection = getDefaultJCoConnection();
		if (isInitialized(connection)){
			return connection;
		}
		//connection is not initialized yet, create one
		
		if (connection.isPooled()) {
			//connection only used for one session, not pooled
			conProps.setProperty(JCoManagedConnectionFactory.JCO_MAXCON, "0");			
		}
		
		// new implementaion, properties from backendcontext
		String server = (String) getContext().getAttribute(Constants.RP_JCO_INIT_APPLICATION_SERVER);
		String sysNr = (String) getContext().getAttribute(Constants.RP_JCO_INIT_SYSTEM_NUMBER);
		String client = (String) getContext().getAttribute(Constants.RP_JCO_INIT_CLIENT);
		if (server != null && sysNr != null && client!= null) {
			conProps.setProperty(JCoManagedConnectionFactory.JCO_ASHOST, server);
			conProps.setProperty(JCoManagedConnectionFactory.JCO_SYSNR, sysNr);
			conProps.setProperty(JCoManagedConnectionFactory.JCO_CLIENT, client);
		}
		
		// some scenarios still use the old way to passjust the client, so we still need this
		if (this.client != null) {
			conProps.setProperty(JCoManagedConnectionFactory.JCO_CLIENT, this.client);
		}
      
		if (conProps.size() != 0) {
			try {
				firstCall = false;
				return getDefaultJCoConnection(conProps);
			} catch (BackendException e) {
				throw new IPCException(e);
			}
		}else {
			return connection;
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.rfc.RFCDefaultClient#getIPCStatelessConnection(java.lang.String)
	 */
	public JCoConnection getIPCStatelessConnection(String language) {
		Properties conProps = new Properties();
		if (language != null) {
			conProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, language);
		}
		JCoConnection ipcJcoCon;
		try {
			//if the user has maintained a dedicated IPCStateful connection,
			//(e.g. customers with ERP < 5.0 need a Netweaver 05 system)
			//the IPC is supposed to use this connection.
			ipcJcoCon = (JCoConnection)getConnectionFactory().getConnection(
						   com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
						   com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_IPC_STATELESS,
						   conProps);
			if (isInitialized(ipcJcoCon)) {
				return ipcJcoCon;
			}

            JCoConnection defaultConnection = getDefaultJCoConnection();
            String defaultConnectionName = defaultConnection.getConnectionConfigName();
            String connectionConfigName = com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS;
            // For some scenarios (e.g. B2B-LORD-scenario) an alternative connection is maintained. This connection
            // is used to connect to the CRM. If such an alternative connection is used as default-connection we 
            // have to use also the alternative connection parameters for the stateless connection. 
            if (useAlternativeConnection(defaultConnectionName)){
                connectionConfigName = com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS_ALT;
            }
			
			ipcJcoCon = (JCoConnection)getConnectionFactory().getConnection(
						   com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
                           connectionConfigName,
						   conProps);
		} catch (BackendException e) {
			throw new IPCException(e);
		}
		return ipcJcoCon;
	}

    /**
     * Returns true if the passed defaultConnectionName is the name of an alternative connection.
     * @param defaultConnectionName
     * @return true in case of an alternative connection
     */
    private boolean useAlternativeConnection(String defaultConnectionName) {
        boolean useAlternative = false;
        if (defaultConnectionName != null){
            if (defaultConnectionName.equals(com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS_ALT)
                || defaultConnectionName.equals(com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATEFUL_ALT)){

                useAlternative = true;
            }
        }
        return useAlternative;
    }

}
