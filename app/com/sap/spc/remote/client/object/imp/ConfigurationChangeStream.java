package com.sap.spc.remote.client.object.imp;

/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
// Note:  this class has no interface outside the imp package, because it is
//        implementation specific
public abstract class ConfigurationChangeStream {
    protected Configuration m_Configuration;
    protected IClient m_CachingClient;
    protected Vector m_ConfigurationChanges;
    protected static final String TRUE = "T";
    protected static final String FALSE = "F";
    
    // logging
    private static final Category category = ResourceAccessor.category;
	private static final Location location = com.sap.spc.remote.client.ResourceAccessor.getLocation(ConfigurationChangeStream.class);    
    
    /**
     * Call this method to add a single configuration change statement. All
     * statements will be written to the server by calling the flush method.
     */
    public void addConfigurationChange(ConfigurationChange configurationChange) {
        location.entering("addConfigurationChange(ConfigurationChange configurationChange)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            if (configurationChange!=null){
                location.debugT("ConfigurationChange configurationChange " + configurationChange.toString());
            }
            else location.debugT("ConfigurationChange configurationChange null");

        }

        if (((CharacteristicValue) configurationChange.getObject()).getName() != null) {
            m_ConfigurationChanges.addElement(configurationChange);
        }
        location.exiting();
    }
    
    /**
     * Call flush to send all changes to the server
     */
    public synchronized void flush() {
        location.entering("flush()");
        HashMap setOpsByInstance = new HashMap();
        HashMap deleteOpsByInstance = new HashMap();
        Exception exceptionForNotSetValues = null;    
    
        for (int i = 0; i < m_ConfigurationChanges.size(); i++) {
            ConfigurationChange configurationChange = (ConfigurationChange) m_ConfigurationChanges.elementAt(i);
            switch (configurationChange.getCommand()) {
                case ConfigurationChange.SET_CHARACTERISTIC_VALUE :
                    doSetCharacteristicValue(configurationChange, setOpsByInstance);
                    break;
                case ConfigurationChange.DELETE_CHARACTERISTIC_VALUE :
                    doDeleteCharacteristicValue(configurationChange, deleteOpsByInstance);
                    break;
            }
        }
        //invalidate caches if changes to the configuration
        if (m_ConfigurationChanges.size() > 0) {
			List instances = m_Configuration.getInstances();         // note 987856: getInstances() sets cacheDirty to false therefore 
            m_Configuration.setCacheDirty(true);                     // we have to call it before we set it to true
			m_Configuration.setInstanceCacheDirty(true);
			if (instances != null){
				for (Iterator instIt = instances.iterator(); instIt.hasNext(); ) {
					Instance currentInstance = (Instance)instIt.next();
					DefaultIPCItem ipcItem = (DefaultIPCItem)currentInstance.getIpcItem();
					if (ipcItem != null) {
						ipcItem.setCacheDirty(); //flush() sends some SET or DELETE commands, hence make Item cache also dirty.
					}
					HashMap groupCaches = ((DefaultInstance)currentInstance).groupCaches;
					if (groupCaches != null) {
						Iterator gCacheIt = groupCaches.values().iterator();
						while (gCacheIt.hasNext()) {
							GroupCache gCache = (GroupCache) gCacheIt.next();
							gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_ALL_CSTICS);
						}
					}
					((DefaultInstance)currentInstance).cacheIsDirty = true;
				}
			}
        

        	
        }
        try {    
            // Now the execute all delete commands
            executeDeleteCommand(deleteOpsByInstance);
            executeSetCommand(setOpsByInstance);
        }
        catch (IPCSetValuesException ipcSVEx){
            // catch IPCSetValuesException but continue processing of this method            
            exceptionForNotSetValues = ipcSVEx;
        }
    
        // Clear the whole buffer right now
        m_ConfigurationChanges.clear();
        location.exiting();
        if (exceptionForNotSetValues != null){
            // now throw IPCSetValuesException if available
            throw (IPCSetValuesException)exceptionForNotSetValues;
        }       
    }
    
    /**
     * This method collects the maximum of setable values for the current instance
     * and automatically selects the fastest possible server command for the
     * amount of values.
     */
    protected abstract void executeSetCommand(HashMap setOpsByInstance);
    
    /**
     * This method collects the maximum of deletable values for the current instance
     * and automatically selects the fastest possible server command for the
     * amount of values.
     */
    protected abstract void executeDeleteCommand(HashMap deleteOpsByInstance);
    
    protected void doSetCharacteristicValue(ConfigurationChange configurationChange, HashMap setOpsByInstance) {
        CharacteristicValue csticValue = (CharacteristicValue) configurationChange.getObject();
        Characteristic cstic = csticValue.getCharacteristic();
        Instance instance = cstic.getInstance();
    
        HashMap setOpsByCstic = (HashMap) setOpsByInstance.get(instance.getId());
        if (setOpsByCstic == null) {
            setOpsByCstic = new HashMap();
            setOpsByInstance.put(instance.getId(), setOpsByCstic);
        }
    
        ArrayList valuesToBeSet = (ArrayList) setOpsByCstic.get(cstic.getName());
        if (valuesToBeSet == null) {
            valuesToBeSet = new ArrayList();
            setOpsByCstic.put(cstic.getName(), valuesToBeSet);
        }
    
        valuesToBeSet.add(csticValue);
    }
    
    protected void doDeleteCharacteristicValue(
        ConfigurationChange configurationChange,
        HashMap deleteOpsByInstance) {
        CharacteristicValue csticValue = (CharacteristicValue) configurationChange.getObject();
        Characteristic cstic = csticValue.getCharacteristic();
        Instance instance = cstic.getInstance();
    
        HashMap deleteOpsByCstic = (HashMap) deleteOpsByInstance.get(instance.getId());
        if (deleteOpsByCstic == null) {
            deleteOpsByCstic = new HashMap();
            deleteOpsByInstance.put(instance.getId(), deleteOpsByCstic);
        }
    
        ArrayList valuesToBeDeleted = (ArrayList) deleteOpsByCstic.get(cstic.getName());
        if (valuesToBeDeleted == null) {
            valuesToBeDeleted = new ArrayList();
            deleteOpsByCstic.put(cstic.getName(), valuesToBeDeleted);
        }
    
        valuesToBeDeleted.add(csticValue);
    }
    
    protected String[] convertListToStringArray(List list) {
        String array[] = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = (String) list.get(i);
        }
        return array;
    }
}