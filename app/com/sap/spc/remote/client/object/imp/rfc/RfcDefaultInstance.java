package com.sap.spc.remote.client.object.imp.rfc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.ProductVariant;
import com.sap.spc.remote.client.object.ProductVariantFeature;
import com.sap.spc.remote.client.object.imp.BaseGroup;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristic;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristicGroup;
import com.sap.spc.remote.client.object.imp.DefaultConfiguration;
import com.sap.spc.remote.client.object.imp.DefaultInstance;
import com.sap.spc.remote.client.object.imp.GroupCache;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.rfc.function.CfgApiCreateInstance;
import com.sap.spc.remote.client.rfc.function.CfgApiGetCsticGroups;
import com.sap.spc.remote.client.rfc.function.CfgApiGetFilteredCstics;
import com.sap.spc.remote.client.rfc.function.CfgApiGetProductVariants;
import com.sap.spc.remote.client.rfc.function.CfgApiSpecialize;
import com.sap.spc.remote.client.rfc.function.CfgApiUnspecialize;
import com.sap.spc.remote.client.rfc.function.SpcChangeItemProduct;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;


public class RfcDefaultInstance extends DefaultInstance implements Instance, RfcConstants {

    protected static final Location location = ResourceAccessor.getLocation(RfcDefaultInstance.class);
    private String groupKeyField = CfgApiGetCsticGroups.GROUP_NAME;
    private String groupDescriptionField = null;
	private HashMap oldPositionsByCsticName = new HashMap();
    private boolean javaImplementationUpdated = true; //assumes that the Java patch is also applied, switches if not

    protected RfcDefaultInstance(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        DefaultConfiguration configuration,
        String instanceId,
        String parentId,
        boolean complete,
        boolean consistent,
        String quantity,
        String name,
        String languageDependentName,
        String description,
        String bomPosition,
        String price,
        boolean userOwned,
        boolean configurable,
        boolean specializable,
        boolean unspecializable,
        MimeObjectContainer mimeObjects) {
        init(
            configurationChangeStream,
            ipcClient,
            configuration,
            instanceId,
            parentId,
            complete,
            consistent,
            quantity,
            name,
            languageDependentName,
            description,
            bomPosition,
            price,
            userOwned,
            configurable,
            specializable,
            unspecializable,
            mimeObjects);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultInstance#init(com.sap.spc.remote.client.object.imp.ConfigurationChangeStream, com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.imp.DefaultConfiguration, java.lang.String, java.lang.String, boolean, boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, com.sap.spc.remote.client.object.MimeObjectContainer)
     */
    public void init(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        DefaultConfiguration configuration,
        String instanceId,
        String parentId,
        boolean complete,
        boolean consistent,
        String quantity,
        String name,
        String languageDependentName,
        String description,
        String bomPosition,
        String price,
        boolean userOwned,
        boolean configurable,
        boolean specializable,
        boolean unspecializable,
        MimeObjectContainer mimeObjects) {
            location.entering("init(ConfigurationChangeStream configurationChangeStream, "
                +"IPCClient ipcClient, DefaultConfiguration configuration, String instanceId, "
                +"String parentId, boolean complete, boolean consistent, String quantity, String name, "
                +"String languageDependentName, String description, String bomPosition, String price, "
                +"boolean userOwned, boolean configurable, boolean specializable, "
                +"boolean unspecializable, MimeObjectContainer mimeObjects)");
            if (location.beDebug()) {
                location.debugT("Parameters:");
                if (configurationChangeStream!=null){
                    location.debugT("ConfigurationChangeStream configurationChangeStream " + configurationChangeStream.toString());
                }
                else location.debugT("ConfigurationChangeStream configurationChangeStream null");
                location.debugT("DefaultConfiguration configuration " + configuration.getId());
                location.debugT("String instanceId " + instanceId);
                location.debugT("String parentId " + parentId);
                location.debugT("boolean complete " + complete);
                location.debugT("boolean consistent " + consistent);
                location.debugT("String quantity " + quantity);
                location.debugT("String name " + name);
                location.debugT("String languageDependentName " + languageDependentName);
                location.debugT("String description " + description);
                location.debugT("String bomPosition " + bomPosition);
                location.debugT("String price " + price);
                location.debugT("boolean userOwnded " + userOwned);
                location.debugT("boolean configurable " + configurable);
                location.debugT("boolean specializable " + specializable);
                location.debugT("boolean unspecializable " + unspecializable);
                if (mimeObjects != null) {
                   location.debugT("MimeObjectContainer mimeObjects " + mimeObjects.toString());
                }
            }
            this.configurationChangeStream = configurationChangeStream;
            this.ipcClient = ipcClient;
            this.configuration = configuration;
            ipcDocument = null;
            ipcItem = null;
            this.instanceId = instanceId;
            this.parentId = parentId;
            this.complete = complete;
            this.consistent = consistent;
            this.quantity = quantity;
            this.name = name;
            this.languageDependentName = languageDependentName;
            this.description = description;
            this.position = bomPosition;
            this.price = price;
            this.userOwned = userOwned;
            this.configurable = configurable;
            this.specializable = specializable;
            this.unspecializable = unspecializable;
            this.closed = false;
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof RfcDefaultIPCSession))
                throw new IllegalClassException(session, RfcDefaultIPCSession.class);
            this.cachingClient = ((RfcDefaultIPCSession) session).getClient();
            this.mimeObjects =
                (mimeObjects == null)
                    ? factory.newMimeObjectContainer()
                    : mimeObjects;
            this.cacheIsDirty = true;
            // remove all existing children
            this.childInstances = new ArrayList();
            location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultInstance#synchronize()
     */
    protected void synchronize() throws IPCException {
        location.entering("synchronize()");
        try {
            if (groupCaches == null) {
                location.debugT("groupCaches is null; retrieving info from AP-CFG");
                // generate group cache
                String configId = configuration.getId();
                String instId = getId();

                JCO.Function functionCfgApiGetCsticGroups = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_CSTIC_GROUPS);
                JCO.ParameterList im = functionCfgApiGetCsticGroups.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiGetCsticGroups.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiGetCsticGroups.getTableParameterList();

                im.setValue(configId, CfgApiGetCsticGroups.CONFIG_ID);     // STRING, Configuration Identifier
                im.setValue(instId, CfgApiGetCsticGroups.INST_ID);     // CHAR, Internal identifier of a Instance
                
                category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_CSTIC_GROUPS");
                ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetCsticGroups);
                category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_CSTIC_GROUPS");
                
                // process table exCsticGroups
                // TABLE, Table with characteristic groups
                JCO.Table exCsticGroups = ex.getTable(CfgApiGetCsticGroups.CSTIC_GROUPS);
                int numRows = exCsticGroups.getNumRows();
                int exCsticGroupsCounter;
                String groupNames[] = new String[numRows];
                String groupDescriptions[] = new String[numRows];
                String groupConsistent[] = new String[numRows];
                String groupRequired[] = new String[numRows];
                //reaction to note 1270205
                if (exCsticGroups.hasField(CfgApiGetCsticGroups.GROUP_ID) && javaImplementationUpdated) {
                	groupKeyField = CfgApiGetCsticGroups.GROUP_ID;
                	groupDescriptionField = CfgApiGetCsticGroups.GROUP_DESCRIPTION;
                }else {
                	groupKeyField = CfgApiGetCsticGroups.GROUP_NAME;
                }
                for (exCsticGroupsCounter=0; exCsticGroupsCounter<numRows; exCsticGroupsCounter++) {
                    exCsticGroups.setRow(exCsticGroupsCounter);
                    String groupKey = exCsticGroups.getString(groupKeyField);
                    
                    if (groupKey.equals("")) {
						groupNames[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetCsticGroups.GROUP_NAME);
						javaImplementationUpdated = false;
                    }else {
						groupNames[exCsticGroupsCounter] = groupKey;
                    }
                    if (groupDescriptionField != null && javaImplementationUpdated) {
                    	groupDescriptions[exCsticGroupsCounter] = exCsticGroups.getString(groupDescriptionField);
                    }else {
                    	groupDescriptions[exCsticGroupsCounter] = groupNames[exCsticGroupsCounter];
                    }
                    groupConsistent[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetCsticGroups.GROUP_CONSISTENT);
                    groupRequired[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetCsticGroups.GROUP_REQUIRED);
                }

                // generate cache
                groups = new TreeMap();
                groupCaches = new HashMap();
                orderedGroups_GeneralGroupFirst =
                    new ArrayList((groupNames != null) ? groupNames.length + 1 : 1);
                orderedGroups_GeneralGroupLast =
                    new ArrayList((groupNames != null) ? groupNames.length + 1 : 1);
                csticsList = new ArrayList();  

                // Add the base group (used for all unassigned values).
                GroupCache gCache = new GroupCache();
                // no cstics, yet
                gCache.setGroup(factory.newBaseGroup(this, new ArrayList(), 0));
                // Define the variable baseGroup to enable the setting of the
                // base group either at the beginning or at the end of the group list
                DefaultCharacteristicGroup baseGroup = gCache.getGroup();
                gCache.setInterest(GroupCache.NO_INTEREST);
                gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_ALL_CSTICS);
                groupCaches.put(baseGroup.getName(), gCache);
                orderedGroups_GeneralGroupFirst.add(baseGroup);
                groups.put(baseGroup.getName(), baseGroup);
                for (int i = 0; groupNames != null && i < groupNames.length; i++) {
                    gCache = new GroupCache();
                    gCache.setGroup(factory.newCharacteristicGroup(
                                        this,
                                        groupNames[i],
										groupDescriptions[i],  //language dep names not passed by RFC
                                        new ArrayList(), // no cstics, yet
                                        i + 1));
                    gCache.getGroup().setConsistent(groupConsistent[i].equals(TRUE)
                            ? true
                            : false);
                    gCache.getGroup().setRequired(groupRequired[i].equals(TRUE)
                            ? true
                            : false);
                    gCache.setInterest(GroupCache.NO_INTEREST);
                    gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_ALL_CSTICS);
                    groupCaches.put(gCache.getGroup().getName(), gCache);
                    orderedGroups_GeneralGroupFirst.add(gCache.getGroup());
                    orderedGroups_GeneralGroupLast.add(gCache.getGroup());
                    groups.put(gCache.getGroup().getName(), gCache.getGroup());
                }
                orderedGroups_GeneralGroupLast.add(baseGroup);
            } else {
                location.debugT("groupCaches is not null; update the groupCache");
				updateGroupCaches(groupCaches);
            }
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        location.exiting();
    }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultInstance#updateGroupCache(com.sap.spc.remote.client.object.imp.GroupCache)
	 * @deprecated
	 */
	protected void updateGroupCache(GroupCache gCache) throws ClientException {
		location.entering("updateGroupCache(GroupCache gCache)");
		if (location.beDebug()) {
			location.debugT("Parameters:");
			location.debugT("GroupCache gCache: Group Name: " 
								+ gCache.getGroup().getName() + " Interest: " 
								+ gCache.getInterest() + " Required Refresh: " 
								+ gCache.getRequiredRefresh());
		}
		String unknownCsticNames[] = null;
		String unknownCsticLNames[] = null;
		String unknownCsticDomainIsAnIntervals[] = null;
		String unknownCsticVisibles[] = null;
		String unknownCsticAddits[] = null; 
		String unknownCsticAuthors[] = null; 
		String unknownCsticConsistents[] = null; 
		String unknownCsticConstraineds[] = null;
		String unknownCsticGroups[] = null; 
		String unknownCsticMultis[] = null; 
		String unknownCsticReadOnlys[] = null; 
		String unknownCsticRequireds[] = null; 
		String unknownCsticUnits[] = null; 
		String unknownCsticEntryFieldMask[] = null; 
		Integer unknownCsticTypeLength[] = null; 
		Integer unknownCsticNumberScale[] = null; 
		Integer unknownCsticValueType[] = null; 
		Integer unknownCsticGroupPositions[] = null; 
		Integer unknownCsticInstPositions[] = null; 
		String unknownCsticApplicationViews[] = null;
		String unknownCsticExpandeds[] = null; 
		String unknownCsticMimeLNames[] = null; 
		String unknownCsticMimeType[] = null; 
		String unknownCsticMimeObjects[] = null; 
		String unknownCsticVarsearchRelevant[] = null;
		String unknownValueCsticNames[] = null; 
		String unknownValueNames[] = null; 
		String unknownValueAssigneds[] = null; 
		String unknownValueAuthors[] = null; 
		String unknownValueDefaults[] = null; 
		String unknownValueLNames[] = null; 
		String unknownValueDescriptions[] = null;
		String unknownValueConditionKeys[] = null; 
		String unknownValueLPrices[] = null; 
		String unknownValuePrices[] = null; 
		String unknownValueIsInDomains[] = null; 
		String unknownMimeValueLNames[] = null; 
		String unknownMimeType[] = null; 
		String unknownMimeObject[] = null; 
        
		String knownCsticNames[] = null;
		String knownCsticDomainIsAnIntervals[] = null;
		String knownCsticVisibles[] = null;
		String knownCsticAuthors[] = null;
		String knownCsticConsistents[] = null;
		String knownCsticConstraineds[] = null;
		Integer knownCsticGroupPositions[] = null; 
		Integer knownCsticInstPositions[] = null; 
		String knownCsticRequireds[] = null;
		String knownCsticReadonlys[] = null;
		String knownValueCsticNames[] = null;
		String knownValueNames[] = null;
		String knownValueLongNames[] = null;
		String knownValueAssigneds[] = null;
		String knownValueAuthors[] = null;
		String knownValueDefaults[] = null;
		String knownValueIsInDomains[] = null;
		String knownMimeValueLNames[] = null;
        String knownMimeType[] = null;
        String knownMimeObject[] = null;

		// verify, if an update is required!
		// first case:
		//  if interest is INTEREST_IN_ALL_CSTICS 
		//  and if requiredRefresh is REFRESH_REQUIRED_FOR_INVISIBLE_CSTICS,
		//                         or REFRESH_REQUIRED_FOR_VISIBLE_CSTICS
		//                         or REFRESH_REQUIRED_FOR_ALL_CSTICS
		// second case:
		//  if interst is INTEREST_IN_VISIBLE_CSTICS_ONLY
		//  and if requiredRefresh is REFRESH_REQUIRED_FOR_VISIBLE_CSTICS
		//                         or REFRESH_REQUIRED_FOR_ALL_CSTICS 
		if ((gCache.getInterest() == GroupCache.INTEREST_IN_ALL_CSTICS
			&& gCache.getRequiredRefresh() != GroupCache.NO_REFRESH_REQUIRED)
			|| (gCache.getInterest() == GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY
				&& (gCache.getRequiredRefresh()
					& GroupCache.REFRESH_REQUIRED_FOR_VISIBLE_CSTICS)
					!= 0)) {
			location.debugT("groupCache: update is required!");
			int i;
			HashMap previouslyKnownCstics = new HashMap();
			HashMap csticsInGroup = new HashMap();
			DefaultCharacteristic cstic;
            
			JCO.Function functionCfgApiGetFilteredCstics = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_FILTERED_CSTICS);
			JCO.ParameterList im = functionCfgApiGetFilteredCstics.getImportParameterList();
			JCO.ParameterList ex = functionCfgApiGetFilteredCstics.getExportParameterList();
			JCO.ParameterList tbl = functionCfgApiGetFilteredCstics.getTableParameterList();
            
			// unknownCsticsInterest for cstics
			StringBuffer unknownCsticsInterest = new StringBuffer();
			// Now stored in KB Cache unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_LNAMES);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_DOMAIN_IS_INTERVAL);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_VISIBLES);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_ADDITS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_CONSISTENTS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_CONSTRAINEDS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_GROUPS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_MULTIS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_READONLYS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_REQUIREDS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_UNITS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_ENTRY_FIELD_MASK);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_TYPELENGTH);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_NUMBERSCALE);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_VALUETYPE);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_GROUP_POSITION);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_INST_POSITION);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_VIEWS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_DISPLAY_MODE);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_MIMES);
            
			// unknownCsticsInterest for values
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_ASSIGNEDS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_AUTHORS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_DEFAULTS);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_LNAMES);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_LPRICES);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_PRICES);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_CONDITIONS);            
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_IS_DOMAIN_VALUE);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_MIMES);
			unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_DESCRIPTIONS);

			// knownCsticsInterest for cstics
			StringBuffer knownCsticsInterest = new StringBuffer();
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_CONSISTENTS);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_REQUIREDS);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_VISIBLES);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_GROUP_POSITION);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_INST_POSITION);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_READONLYS);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_CONSTRAINEDS); // after change on AP-side this became dynamic information (AP-side change done by TL on 2005-06-30)
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_DOMAIN_IS_INTERVAL); // after change on AP-side this became dynamic information (AP-side change done by TL on 2005-06-30)
            
			// knownCsticsInterest for values
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_ASSIGNEDS);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_AUTHORS);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_LNAMES);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_IS_DOMAIN_VALUE);
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_DEFAULTS);
			//set interest for value mimes for known cstic
			knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_MIMES);

			im.setValue(configuration.getId(), CfgApiGetFilteredCstics.CONFIG_ID);     // STRING, Configuration Identifier
			im.setValue(this.getId(), CfgApiGetFilteredCstics.INST_ID);     // CHAR, Internal identifier of a Instance
			boolean hasGroupIdLangIndep = im.hasField(CfgApiGetFilteredCstics.GROUP_ID_LANGINDEP);

			if (hasGroupIdLangIndep && groupKeyField.equals(CfgApiGetCsticGroups.GROUP_ID) && javaImplementationUpdated) {
				//if the flag in the function definition exists
				im.setValue(RfcConstants.TRUE, CfgApiGetFilteredCstics.GROUP_ID_LANGINDEP);
			}


			if (!gCache.getGroup().isBaseGroup()) {
				im.setValue(FALSE, CfgApiGetFilteredCstics.INCLUDE_UNGROUPED_CSTICS);        // optional, CHAR, include ungrouped characteristics(T=true, F=false, ""=false)
				// process table imFilterGroups
				// optional, TABLE, CFG: Table of group names
				JCO.Table imFilterGroups = im.getTable(CfgApiGetFilteredCstics.FILTER_GROUPS);
				// there is only one row
				imFilterGroups.appendRow();
				imFilterGroups.setValue(gCache.getGroup().getName(), CfgApiGetFilteredCstics.GROUP_ID);       // CHAR, Group Name
			} else {
				im.setValue(TRUE, CfgApiGetFilteredCstics.UNGROUPED_CSTICS_ONLY);      // optional, CHAR, show only ungrouped characteristics (T=true, F= false, ""=true)
			}
            
			// If you are only interested in visible cstics then only visible cstics
			// were requested. If you are interested in all cstics the next time then
			// a refresh of the invisible cstics is required.
			if (gCache.getInterest() == GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY) {
				im.setValue(TRUE, CfgApiGetFilteredCstics.VISIBLE_CSTICS_ONLY);      // optional, CHAR, show only visible characteristics (T=true, F=false, ""=false)
				gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_INVISIBLE_CSTICS);
			// If you are interested in all cstics then no refresh is required next time.
			} else {
				gCache.setRequiredRefresh(GroupCache.NO_REFRESH_REQUIRED);
			}
			gCache.setInterest(GroupCache.NO_INTEREST);

			// process table imCsticInfoGroups
			// optional, TABLE, CFG: Characteristic group insterest (GetFilteredCsticsAndVal
			JCO.Table imCsticInfoGroups = im.getTable(CfgApiGetFilteredCstics.CSTIC_INFO_GROUPS);
			// exactly two entries in the table
			imCsticInfoGroups.appendRow();
			imCsticInfoGroups.setValue("0", CfgApiGetFilteredCstics.GROUP_ID);        // CHAR, Group Name
			imCsticInfoGroups.setValue(unknownCsticsInterest.toString(), CfgApiGetFilteredCstics.INTEREST);       // STRING, CFG: Data element of type STRING
			imCsticInfoGroups.appendRow();
			imCsticInfoGroups.setValue("1", CfgApiGetFilteredCstics.GROUP_ID);        // CHAR, Group Name
			imCsticInfoGroups.setValue(knownCsticsInterest.toString(), CfgApiGetFilteredCstics.INTEREST);       // STRING, CFG: Data element of type STRING

			String assignableValuesOnlyFlag = FALSE;
			if (isAssignableValuesOnly()) {
				assignableValuesOnlyFlag = TRUE;
			}
			im.setValue(assignableValuesOnlyFlag, CfgApiGetFilteredCstics.ASSIGNABLE_VALUES_ONLY);        // optional, CHAR, show only selectable characteristic values(T=true, F=false, ""=false)

			// Now add all known characteristics
			// process table imMonitoredCstics
			// optional, TABLE, CFG: Table of monitored characteristics
			JCO.Table imMonitoredCstics = im.getTable(CfgApiGetFilteredCstics.MONITORED_CSTICS);            
			Iterator knownCsticIterator = gCache.getGroup().getListOfCachedCharacteristics().iterator();
			while (knownCsticIterator.hasNext()) {
				Characteristic characteristic = (Characteristic) knownCsticIterator.next();
				imMonitoredCstics.appendRow();
				imMonitoredCstics.setValue(characteristic.getName(), CfgApiGetFilteredCstics.CSTIC_NAME);        // CHAR, Characteristic name (object characteristic)
				imMonitoredCstics.setValue("1", CfgApiGetFilteredCstics.GROUP_ID);        // CHAR, Group Name
				previouslyKnownCstics.put(characteristic.getName(), characteristic);
			}
            
			category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_FILTERED_CSTICS");
			((RFCDefaultClient)cachingClient).execute(functionCfgApiGetFilteredCstics);
			category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_FILTERED_CSTICS");

        
			// process table exCsticsInGroups
			// TABLE, CFG: Table of characteristics sorted in groups
			JCO.Table exCsticsInGroups = ex.getTable(CfgApiGetFilteredCstics.CSTICS_IN_GROUPS);
			String groupId;
			int exCsticsInGroupsCounter;
			for (exCsticsInGroupsCounter=0; exCsticsInGroupsCounter<exCsticsInGroups.getNumRows(); exCsticsInGroupsCounter++) {
				exCsticsInGroups.setRow(exCsticsInGroupsCounter);
				groupId = exCsticsInGroups.getString(CfgApiGetFilteredCstics.GROUP_ID);

				// determine length of arrays
				JCO.Table exCsticsInGroupsCstics = exCsticsInGroups.getTable(CfgApiGetFilteredCstics.CSTICS);
				int csticsNumRows = exCsticsInGroupsCstics.getNumRows();
				int valuesNumRows = 0;
				int valueMimesNumRows = 0;
				int csticMimesNumRows = 0;
				for (int exCsticsInGroupsCsticsCounter=0; exCsticsInGroupsCsticsCounter<csticsNumRows; exCsticsInGroupsCsticsCounter++) {
					exCsticsInGroupsCstics.setRow(exCsticsInGroupsCsticsCounter);
					JCO.Table exCsticsInGroupsCsticsCsticValues = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_VALUES);
					valuesNumRows = valuesNumRows + exCsticsInGroupsCsticsCsticValues.getNumRows();
					for (int exCsticsInGroupsCsticsCsticValuesCounter=0; exCsticsInGroupsCsticsCsticValuesCounter<exCsticsInGroupsCsticsCsticValues.getNumRows(); exCsticsInGroupsCsticsCsticValuesCounter++) {
						exCsticsInGroupsCsticsCsticValues.setRow(exCsticsInGroupsCsticsCsticValuesCounter);
						JCO.Table exCsticsInGroupsCsticsCsticValuesValueMimes = exCsticsInGroupsCsticsCsticValues.getTable(CfgApiGetFilteredCstics.VALUE_MIMES);
						valueMimesNumRows = valueMimesNumRows + exCsticsInGroupsCsticsCsticValuesValueMimes.getNumRows();
					}
					JCO.Table exCsticsInGroupsCsticsCsticMimes = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_MIMES);
					csticMimesNumRows = csticMimesNumRows + exCsticsInGroupsCsticsCsticMimes.getNumRows();
				}
				// initialize the arrays with the correct size
				String[] csticNames = new String[csticsNumRows];
				String[] csticLNames = new String[csticsNumRows];
				String[] csticDescriptions = new String[csticsNumRows];
				String[] csticDomainIsIntervals = new String[csticsNumRows];
				String[] csticVisibles = new String[csticsNumRows];
				String[] csticAddits = new String[csticsNumRows];
				String[] csticAuthors = new String[csticsNumRows];
				String[] csticConsistents = new String[csticsNumRows];
				String[] csticConstraineds = new String[csticsNumRows];
				String[] csticGroups = new String[csticsNumRows];
				String[] csticMultis = new String[csticsNumRows];
				String[] csticReadOnlys = new String[csticsNumRows];
				String[] csticRequireds = new String[csticsNumRows];
				String[] csticUnits = new String[csticsNumRows];
				String[] csticEntryFieldMask = new String[csticsNumRows];
				Integer[] csticTypeLength = new Integer[csticsNumRows];
				Integer[] csticNumberScale = new Integer[csticsNumRows];
				Integer[] csticValueType = new Integer[csticsNumRows];
				Integer[] csticGroupPositions = new Integer[csticsNumRows];
				Integer[] csticInstPositions = new Integer[csticsNumRows];
				String[] csticApplicationViews = new String[csticsNumRows];
				String[] csticDisplayModes = new String[csticsNumRows];
				String[] csticMimeLNames = new String[csticMimesNumRows];
				String[] csticMimeTypes = new String[csticMimesNumRows]; 
				String[] csticMimeObjects = new String[csticMimesNumRows];
				String[] csticVarsearchRelevants = new String[csticsNumRows];                
				String[] valueCsticNames = new String[valuesNumRows];
				String[] valueNames = new String[valuesNumRows];
				String[] valueAssigneds = new String[valuesNumRows];
				String[] valueAuthors = new String[valuesNumRows];
				String[] valueDefaults = new String[valuesNumRows];
				String[] valueLNames = new String[valuesNumRows];
				String[] valueDescriptions = new String[valuesNumRows];
				String[] valueConditions = new String[valuesNumRows];
				String[] valueLPrices = new String[valuesNumRows];
				String[] valuePrices = new String[valuesNumRows];
				String[] valueIsDomainValues = new String[valuesNumRows];                   
				String[] valueMimeLNames = new String[valueMimesNumRows];
				String[] valueMimeTypes = new String[valueMimesNumRows];
				String[] valueMimeObjects = new String[valueMimesNumRows]; 

				// initialize all counters for filling the arrays (different ones than for the single table loops)
				int csticsCounter=0;
				int csticValuesCounter = 0;
				int csticValuesMimesCounter=0;
				int csticsMimesCounter=0;

				// process table exCsticsInGroupsCstics
				for (int exCsticsInGroupsCsticsCounter=0; exCsticsInGroupsCsticsCounter<csticsNumRows; exCsticsInGroupsCsticsCounter++) {
					exCsticsInGroupsCstics.setRow(exCsticsInGroupsCsticsCounter);
					// process structure exCsticsInGroupsCsticsCsticHeader
					JCO.Structure exCsticsInGroupsCsticsCsticHeader = exCsticsInGroupsCstics.getStructure(CfgApiGetFilteredCstics.CSTIC_HEADER);
					csticAddits[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_ADDIT);
					csticConstraineds[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_CONSTRAINED);
					csticEntryFieldMask[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_ENTRY_FIELD_MASK);
					csticGroups[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_GROUP);
					csticLNames[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_LNAME);
					csticMultis[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_MULTI);
					csticNames[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_NAME);
					csticNumberScale[csticsCounter] = (Integer)exCsticsInGroupsCsticsCsticHeader.getValue(CfgApiGetFilteredCstics.CSTIC_NUMBERSCALE);
					csticTypeLength[csticsCounter] = (Integer)exCsticsInGroupsCsticsCsticHeader.getValue(CfgApiGetFilteredCstics.CSTIC_TYPELENGTH);
					csticUnits[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_UNIT);
					csticValueType[csticsCounter] = (Integer)exCsticsInGroupsCsticsCsticHeader.getValue(CfgApiGetFilteredCstics.CSTIC_VALUETYPE);
					csticDisplayModes[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_DISPLAY_MODE);
					csticVarsearchRelevants[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_VARSEARCH_RELEVANT);
					csticGroupPositions[csticsCounter] = (Integer)exCsticsInGroupsCstics.getValue(CfgApiGetFilteredCstics.CSTIC_GROUP_POSITION);
					csticInstPositions[csticsCounter] = (Integer)exCsticsInGroupsCstics.getValue(CfgApiGetFilteredCstics.CSTIC_INST_POSITION);
					csticDomainIsIntervals[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_DOMAIN_IS_INTERVAL);
					csticReadOnlys[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_READONLY);
					csticVisibles[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_VISIBLE);
					csticAuthors[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_AUTHOR);
					csticRequireds[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_REQUIRED);
					csticConsistents[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_CONSISTENT);                    
                    
					// process table exCsticsInGroupsCsticsCsticValues
					JCO.Table exCsticsInGroupsCsticsCsticValues = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_VALUES);
					for (int exCsticsInGroupsCsticsCsticValuesCounter = 0;exCsticsInGroupsCsticsCsticValuesCounter<exCsticsInGroupsCsticsCsticValues.getNumRows(); exCsticsInGroupsCsticsCsticValuesCounter++) {
						exCsticsInGroupsCsticsCsticValues.setRow(exCsticsInGroupsCsticsCsticValuesCounter);
						// link to cstic-name: necessary to reuse the communication-type independent COL
						valueCsticNames[csticValuesCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_NAME);
						valueNames[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_NAME);
						valueLNames[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_LNAME);
						valueDescriptions[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_DESCRIPTION);
						valueIsDomainValues[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_IS_DOMAIN_VALUE);
						valueDefaults[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_DEFAULT);
						valueAssigneds[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_ASSIGNED);
						valueAuthors[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_AUTHOR);
						valuePrices[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_PRICE);
						valueLPrices[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_LPRICE);
						valueConditions[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_CONDITION);
                        
						// process table exCsticsInGroupsCsticsCsticValuesValueMimes
						JCO.Table exCsticsInGroupsCsticsCsticValuesValueMimes = exCsticsInGroupsCsticsCsticValues.getTable(CfgApiGetFilteredCstics.VALUE_MIMES);
						for (int exCsticsInGroupsCsticsCsticValuesValueMimesCounter=0; exCsticsInGroupsCsticsCsticValuesValueMimesCounter<exCsticsInGroupsCsticsCsticValuesValueMimes.getNumRows(); exCsticsInGroupsCsticsCsticValuesValueMimesCounter++) {
							exCsticsInGroupsCsticsCsticValuesValueMimes.setRow(exCsticsInGroupsCsticsCsticValuesValueMimesCounter);
							// link to cstic/value-name: necessary to reuse the communication-type independent COL
							valueMimeLNames[csticValuesMimesCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_NAME)
								 + "." 
								 + exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_NAME);
							valueMimeTypes[csticValuesMimesCounter] = exCsticsInGroupsCsticsCsticValuesValueMimes.getString(CfgApiGetFilteredCstics.MIME_OBJECT_TYPE);
							valueMimeObjects[csticValuesMimesCounter] = exCsticsInGroupsCsticsCsticValuesValueMimes.getString(CfgApiGetFilteredCstics.MIME_OBJECT);
							csticValuesMimesCounter++;
						}
						csticValuesCounter++;
					}
                    
					// process table exCsticsInGroupsCsticsCsticMimes
					JCO.Table exCsticsInGroupsCsticsCsticMimes = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_MIMES);
					for (int exCsticsInGroupsCsticsCsticMimesCounter=0; exCsticsInGroupsCsticsCsticMimesCounter<exCsticsInGroupsCsticsCsticMimes.getNumRows(); exCsticsInGroupsCsticsCsticMimesCounter++) {
						exCsticsInGroupsCsticsCsticMimes.setRow(exCsticsInGroupsCsticsCsticMimesCounter);
						// link to cstic-name: necessary to reuse the communication-type independent COL
						csticMimeLNames[csticsMimesCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_NAME);                       
						csticMimeTypes[csticsMimesCounter] = exCsticsInGroupsCsticsCsticMimes.getString(CfgApiGetFilteredCstics.MIME_OBJECT_TYPE);
						csticMimeObjects[csticsMimesCounter] = exCsticsInGroupsCsticsCsticMimes.getString(CfgApiGetFilteredCstics.MIME_OBJECT);
						csticsMimesCounter++;
					}
                    
					// process table exCsticsInGroupsCsticsCsticDescriptions
					JCO.Table exCsticsInGroupsCsticsCsticDescriptions = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_DESCRIPTION);
					StringBuffer csticDescriptionsLine = new StringBuffer();
					for (int exCsticsInGroupsCsticsCsticDescriptionsCounter=0; exCsticsInGroupsCsticsCsticDescriptionsCounter<exCsticsInGroupsCsticsCsticDescriptions.getNumRows(); exCsticsInGroupsCsticsCsticDescriptionsCounter++) {
						exCsticsInGroupsCsticsCsticDescriptions.setRow(exCsticsInGroupsCsticsCsticDescriptionsCounter);
						csticDescriptionsLine.append(exCsticsInGroupsCsticsCsticDescriptions.getString(CfgApiGetFilteredCstics.TEXT_LINE));
					}
					csticDescriptions[csticsCounter] = csticDescriptionsLine.toString();

					// process table exCsticsInGroupsCsticsCsticViews
					JCO.Table exCsticsInGroupsCsticsCsticViews = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_VIEWS);
					StringBuffer csticViews = new StringBuffer();
					for (int exCsticsInGroupsCsticsCsticViewsCounter=0; exCsticsInGroupsCsticsCsticViewsCounter<exCsticsInGroupsCsticsCsticViews.getNumRows(); exCsticsInGroupsCsticsCsticViewsCounter++) {
						exCsticsInGroupsCsticsCsticViews.setRow(exCsticsInGroupsCsticsCsticViewsCounter);
						csticViews.append(exCsticsInGroupsCsticsCsticViews.getString(CfgApiGetFilteredCstics.CSTIC_VIEW));
					}
					csticApplicationViews[csticsCounter] = csticViews.toString();
					csticsCounter++;
				}
                
				// assign collected data either to unknownCstic (groupId=0) or knownCstic (groupId=1) 
				if (groupId.equals("0")) {
					unknownCsticNames = csticNames;
					unknownCsticLNames = csticLNames;
					unknownCsticDomainIsAnIntervals = csticDomainIsIntervals;
					unknownCsticVisibles = csticVisibles;
					unknownCsticAddits = csticAddits;
					unknownCsticAuthors = csticAuthors;
					unknownCsticConsistents = csticConsistents;
					unknownCsticConstraineds = csticConstraineds;
					unknownCsticGroups = csticGroups;
					unknownCsticMultis = csticMultis;
					unknownCsticReadOnlys = csticReadOnlys;
					unknownCsticRequireds = csticRequireds;
					unknownCsticUnits = csticUnits;
					unknownCsticEntryFieldMask = csticEntryFieldMask;
					unknownCsticTypeLength = csticTypeLength;
					unknownCsticNumberScale = csticNumberScale; 
					unknownCsticValueType = csticValueType; 
					unknownCsticGroupPositions = csticGroupPositions; 
					unknownCsticInstPositions = csticInstPositions; 
					unknownCsticApplicationViews = csticApplicationViews;
					unknownCsticExpandeds = csticDisplayModes;
					unknownCsticMimeLNames = csticMimeLNames;
					unknownCsticMimeType = csticMimeTypes; 
					unknownCsticMimeObjects = csticMimeObjects;
					unknownCsticVarsearchRelevant = csticVarsearchRelevants;  
                    
					unknownValueCsticNames = valueCsticNames;
					unknownValueNames = valueNames;
					unknownValueAssigneds = valueAssigneds;
					unknownValueAuthors = valueAuthors;
					unknownValueDefaults = valueDefaults;
					unknownValueLNames = valueLNames;
					unknownValueDescriptions = valueDescriptions;
					unknownValueConditionKeys = valueConditions;
					unknownValueLPrices = valueLPrices;
					unknownValuePrices = valuePrices;
					unknownValueIsInDomains = valueIsDomainValues;                   
					unknownMimeValueLNames = valueMimeLNames;
					unknownMimeType = valueMimeTypes;
					unknownMimeObject = valueMimeObjects;                                        
				}
				else if (groupId.equals("1")) {
					knownCsticNames = csticNames;
					knownCsticDomainIsAnIntervals = csticDomainIsIntervals;
					knownCsticVisibles = csticVisibles;
					knownCsticAuthors = csticAuthors;
					knownCsticConsistents = csticConsistents;
					knownCsticConstraineds = csticConstraineds;
					knownCsticGroupPositions = csticGroupPositions; 
					knownCsticInstPositions = csticInstPositions; 
					knownCsticRequireds = csticRequireds;
					knownCsticReadonlys = csticReadOnlys;
                        
					knownValueCsticNames = valueCsticNames;
					knownValueNames = valueNames;
					knownValueLongNames = valueLNames;
					knownValueAssigneds = valueAssigneds;
					knownValueAuthors = valueAuthors;
					knownValueDefaults = valueDefaults;
					knownValueIsInDomains = valueIsDomainValues;  
					// Value mimes updated for known cstics
                    knownMimeValueLNames = valueMimeLNames;
                    knownMimeType = valueMimeTypes;
                    knownMimeObject = valueMimeObjects;
				}
				else {
					category
							.logT(
									Severity.ERROR,
									location,
									"Short Text: No valid InfoGroup ID: "
											+ groupId
											+ ". \nWhat happened? This is a technical error that can only occur due to a software error. The parameter groupId has two possible values: 0 and 1. The current backend call CFG_API_GET_FILTERED_CSTICS however delivered "
											+ groupId + ". \nWhat you can do? Please note down the actions that lead to this error and report to your system administrator."
											+ "\nHow to correct the error? Check the latest available patches for software component SAP-SHRJAV."
											+ "\nIf the error occurs in an non-modified program, you may find a solution in a SAP note."
											+ "\nIf you have access to SAP Notes, carry out a search with the following keywords: CFG_API_GET_FILTERED_CSTICS, InfoGroup.");                    
				}
			}
            
			// process table exCsticGroups
			// TABLE, CFG: Table of characteristic groups
			JCO.Table exCsticGroups = ex.getTable(CfgApiGetFilteredCstics.CSTIC_GROUPS);
			int exCsticGroupsCounter;
			int groupNumRows = exCsticGroups.getNumRows();
			String groupNames[] = new String[groupNumRows];
			String groupConsistents[] = new String[groupNumRows];
			String groupRequireds[] = new String[groupNumRows];
			for (exCsticGroupsCounter=0; exCsticGroupsCounter<groupNumRows; exCsticGroupsCounter++) {
				exCsticGroups.setRow(exCsticGroupsCounter);
				groupNames[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetFilteredCstics.GROUP_NAME);
				groupConsistents[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetFilteredCstics.GROUP_CONSISTENT);
				groupRequireds[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetFilteredCstics.GROUP_REQUIRED);
			}

			// is only used in case of numerical cstics; otherwise they would have a wrong format in display
			String knownValueLNames[] = null;
			if (knownValueLongNames != null) {
				knownValueLNames = new String[knownValueNames.length];
				if (knownValueLongNames.length < knownValueNames.length) {
					System.arraycopy(
						knownValueLongNames,
						0,
						knownValueLNames,
						0,
						knownValueLongNames.length);
				} else {
					knownValueLNames = knownValueLongNames;
				}
			}

			// index[0]: index for values
			// index[1]: index for value mimes
			// index[2]: index for cstic mimes
			int index[] = new int[] { 0, 0, 0 };
			// First update all known characteristics
			index[0] = 0;
            // csticsList.clear();
            gCache.getGroup().getListOfCachedCharacteristics().clear();
			for (i = 0; knownCsticNames != null && i < knownCsticNames.length; i++) {
				cstic = (DefaultCharacteristic) cstics.get(knownCsticNames[i]);
				cstic.update( 
					knownCsticVisibles[i].equals(TRUE),
					knownCsticConsistents[i].equals(TRUE),
					knownCsticRequireds[i].equals(TRUE),
					knownCsticReadonlys[i].equals(TRUE),
					knownCsticConstraineds[i].equals(TRUE),
					knownCsticDomainIsAnIntervals[i].equals(TRUE));
                    
                int newInstPosition = knownCsticInstPositions[i].intValue()  ;
				int newGroupPosition = knownCsticGroupPositions[i].intValue();
				Integer Pos = (Integer)oldPositionsByCsticName.get(knownCsticNames[i]);
				if(Pos != null)
				{
				
				int OldPosition =Pos.intValue();
				
				if( OldPosition != newInstPosition )
					{   csticsList.remove(cstic);
						addCharacteristic(newInstPosition, cstic);
					}
				}
				else
				{
					addCharacteristic(newInstPosition, cstic); //TODO could that double the cstic in the list?
				}
				gCache.getGroup().addCharacteristic(newGroupPosition, cstic);
                    
				cstic
	               .updateCharacteristicValues(
	                   index,
	                   knownValueCsticNames,
	                   knownValueNames,
	                   knownValueLNames,
	                   null, //descriptions do not change at runtime
	                   null, //conditionKeys do not change at runtime
	                   null, //valuePrices do not change at runtime
	               knownValueIsInDomains,
	               knownValueAssigneds,
	               knownValueAuthors,
	               null,
					knownValueDefaults,
					knownMimeValueLNames, 
					knownMimeType, 
					knownMimeObject);
				// Since the cstic was found in the server's response again,
				// remove it from the list of previously known cstics so that this
				// list contains only those previously known cstics that are missing
				// which means they became invisible and the server should return
				// visible cstics only
				previouslyKnownCstics.remove(cstic.getName());
				csticsInGroup.put(cstic.getName(), cstic);
			}
			// Second add the new Characteristics
			index[0] = 0;
			for (i = 0;
				unknownCsticNames != null && i < unknownCsticNames.length;
				i++) {
				int unknownCsticValueTypeInt = unknownCsticValueType[i].intValue();
				int unknownCsticTypeLengthInt;
				if (unknownCsticTypeLength[i] != null){
					unknownCsticTypeLengthInt = unknownCsticTypeLength[i].intValue();
				}
				else {
					unknownCsticTypeLengthInt = 0;
				}
				int unknownCsticNumberScaleInt;
				if (unknownCsticNumberScale[i] != null){
					unknownCsticNumberScaleInt = unknownCsticNumberScale[i].intValue();
				}
				else {
					unknownCsticNumberScaleInt = 0;
				}
                
				// for backward compatibility (in the case that the server doesn't return 
				// application views for cstics)
				String unknownCsticApplicationView = "";
				if (unknownCsticApplicationViews != null) {
					unknownCsticApplicationView = unknownCsticApplicationViews[i];
				}
                
				//for backward compatibility in case the server does not return boolean expanded value
				//If no value is returned the value is defaulted to false
				boolean csticExpanded = false;
				if (unknownCsticExpandeds!=null && unknownCsticExpandeds.length > 0 && unknownCsticExpandeds[i] != null) {
					csticExpanded = unknownCsticExpandeds[i].equals("X");
				}else {
					location.warningT("Incompatible Server version. Parameter \"X\" not returned from server. Functionality of expand/collapse of single characteristics is not available!");
				}

				cstic =
					factory
						.newCharacteristic(
							configurationChangeStream,
							ipcClient,
							this,
							unknownCsticGroups[i],
							unknownCsticLNames[i],
							unknownCsticNames[i],
							unknownCsticUnits[i],
							unknownCsticEntryFieldMask[i],
							unknownCsticTypeLengthInt,
							unknownCsticNumberScaleInt,
							unknownCsticValueTypeInt,
							unknownCsticAddits[i].equals(TRUE),
							unknownCsticVisibles[i].equals(TRUE),
							unknownCsticMultis[i].equals(TRUE),
							unknownCsticRequireds[i].equals(TRUE),
							csticExpanded,    
							unknownCsticConsistents[i].equals(TRUE),
							unknownCsticConstraineds[i].equals(TRUE),
							unknownCsticDomainIsAnIntervals[i].equals(TRUE),
							unknownCsticReadOnlys[i].equals(TRUE),
							false, 
							null,  
							unknownCsticApplicationView,
							unknownCsticMimeLNames,
							unknownCsticMimeType,
							unknownCsticMimeObjects,
							index);

				cstics.put(unknownCsticNames[i], cstic);
				csticsInGroup.put(cstic.getName(), cstic);
				//check the cstics order
				int newGroupPosition = unknownCsticGroupPositions[i].intValue();
                int newInstPosition = unknownCsticInstPositions[i].intValue() ;
				addCharacteristic(newInstPosition, cstic);
                Integer intpos = new Integer(newInstPosition);
				oldPositionsByCsticName.put(cstic.getName(),intpos);
				gCache.getGroup().addCharacteristic(newGroupPosition, cstic);

				cstic
					.updateCharacteristicValues(
						index,
						unknownValueCsticNames,
						unknownValueNames,
						unknownValueLNames,
						unknownValueDescriptions, 
					unknownValueConditionKeys,
					unknownValueLPrices,
					unknownValueIsInDomains,
					unknownValueAssigneds,
					unknownValueAuthors,
					null,
					unknownValueDefaults,
				unknownMimeValueLNames, unknownMimeType, unknownMimeObject);
			}

			// Now set all remaining cstics of the previously known cstics list
			// to the state invisible. That should keep a copy and renew the
			// information only if invisible cstics should be displayed as well.
			Iterator previouslyKnownCsticsIt = previouslyKnownCstics.values().iterator();
			while (previouslyKnownCsticsIt.hasNext()) {
				Characteristic characteristic = (Characteristic) previouslyKnownCsticsIt.next();
				characteristic.setVisible(false);
			}
            
			// set the group status
			for (int k=0; k<groupNames.length; k++) {
				// if it is a normal group the FM returns an array that contains the group name
				if (groupNames[k].equals(gCache.getGroup().getName())) {
					boolean groupConsistent = groupConsistents[k].equals(TRUE)
							? true
							: false;
					boolean groupReq = groupRequireds[k].equals(TRUE)
							? true
							: false;
					gCache.getGroup().setConsistent(groupConsistent);
					gCache.getGroup().setRequired(groupReq);
				}
			}
			// if it is the BaseGroup the array is empty
			if (groupNames.length < 1) {
				// is the current group really the BaseGroup?
				if (gCache.getGroup().isBaseGroup()){
					Iterator keys = csticsInGroup.keySet().iterator();
					boolean groupConsistent = true;
					boolean groupReq = false;
					while (keys.hasNext()){
						Characteristic currentCstic = (Characteristic)csticsInGroup.get(keys.next());
						// as soon as one cstic is not consistent the whole group is not consistent
						if (!currentCstic.isConsistent()) {
							groupConsistent = false;
						}
						// as soon as one required cstic has no assigned values the whole group is required
						if (currentCstic.isRequired() && !currentCstic.hasAssignedValues()){
							groupReq = true;
						}
						// break the loop as soon as both flags have been changed
						if ((groupConsistent == false) && (groupReq == true)){
							break;
						}
					}
					gCache.getGroup().setConsistent(groupConsistent);
					gCache.getGroup().setRequired(groupReq);                    
				}
			}
		// Reset interest, because it has been satisfied!
		gCache.setInterest(GroupCache.NO_INTEREST);
		}
		location.exiting();
	}


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultInstance#updateGroupCaches(java.util.HashMap)
     */
    protected void updateGroupCaches(HashMap gCaches) throws ClientException {
        location.entering("updateGroupCaches(HashMap gCaches)");
		ArrayList groupsToSync = new ArrayList();
		Iterator cacheIt = gCaches.values().iterator();
		// check for which groups an update is required!
		// General:
		// No refresh is required if NO_INTEREST is set. 
		// first case:
		//  if interest is INTEREST_IN_ALL_CSTICS 
		//  and if requiredRefresh is REFRESH_REQUIRED_FOR_INVISIBLE_CSTICS,
		//                         or REFRESH_REQUIRED_FOR_VISIBLE_CSTICS
		//                         or REFRESH_REQUIRED_FOR_ALL_CSTICS
		// second case:
		//  if interst is INTEREST_IN_VISIBLE_CSTICS_ONLY
		//  and if requiredRefresh is REFRESH_REQUIRED_FOR_VISIBLE_CSTICS
		//                         or REFRESH_REQUIRED_FOR_ALL_CSTICS
		while (cacheIt.hasNext()) {
			GroupCache gCache = (GroupCache) cacheIt.next();
				if ((gCache.getInterest() == GroupCache.INTEREST_IN_ALL_CSTICS
						&& gCache.getRequiredRefresh() != GroupCache.NO_REFRESH_REQUIRED)            
					|| (gCache.getInterest() == GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY
						&& (gCache.getRequiredRefresh()	& GroupCache.REFRESH_REQUIRED_FOR_VISIBLE_CSTICS) != 0)) {

							groupsToSync.add(gCache);
			}
		}                	
		// if no groups need to be synced return
		if (groupsToSync.size() < 1){
			if (location.beDebug()){
				location.debugT("No groups need to be synced.");
			}
			return;
		}

        String unknownCsticNames[] = null;
        String unknownCsticLNames[] = null;
        String unknownCsticDomainIsAnIntervals[] = null;
        String unknownCsticVisibles[] = null;
        String unknownCsticAddits[] = null; 
        String unknownCsticAuthors[] = null; 
        String unknownCsticConsistents[] = null; 
        String unknownCsticConstraineds[] = null;
        String unknownCsticGroups[] = null; 
        String unknownCsticMultis[] = null; 
        String unknownCsticReadOnlys[] = null; 
        String unknownCsticRequireds[] = null; 
        String unknownCsticUnits[] = null; 
        String unknownCsticEntryFieldMask[] = null; 
        Integer unknownCsticTypeLength[] = null; 
        Integer unknownCsticNumberScale[] = null; 
        Integer unknownCsticValueType[] = null; 
        Integer unknownCsticGroupPositions[] = null; 
        Integer unknownCsticInstPositions[] = null; 
        String unknownCsticApplicationViews[] = null;
        String unknownCsticExpandeds[] = null; 
        String unknownCsticMimeLNames[] = null; 
        String unknownCsticMimeType[] = null; 
        String unknownCsticMimeObjects[] = null; 
        String unknownCsticVarsearchRelevant[] = null;
        String unknownValueCsticNames[] = null; 
        String unknownValueNames[] = null; 
        String unknownValueAssigneds[] = null; 
        String unknownValueAuthors[] = null; 
        String unknownValueDefaults[] = null; 
        String unknownValueLNames[] = null; 
        String unknownValueDescriptions[] = null;
        String unknownValueConditionKeys[] = null; 
        String unknownValueLPrices[] = null; 
        String unknownValuePrices[] = null; 
        String unknownValueIsInDomains[] = null; 
        String unknownMimeValueLNames[] = null; 
        String unknownMimeType[] = null; 
        String unknownMimeObject[] = null; 
        
        String knownCsticNames[] = null;
        String knownCsticDomainIsAnIntervals[] = null;
        String knownCsticVisibles[] = null;
        String knownCsticAuthors[] = null;
        String knownCsticConsistents[] = null;
        String knownCsticConstraineds[] = null;
        String knownCsticRequireds[] = null;
        String knownCsticReadonlys[] = null;
        String knownValueCsticNames[] = null;
        String knownValueNames[] = null;
        String knownValueLongNames[] = null;
        String knownValueprices [] = null;        
        String knownValueLprices [] = null;        
        String knownValueAssigneds[] = null;
        String knownValueAuthors[] = null;
        String knownValueDefaults[] = null;
        String knownValueIsInDomains[] = null;

		int i;
		HashMap previouslyKnownCstics = new HashMap();
		HashMap csticsInBaseGroup = new HashMap();
		DefaultCharacteristic cstic;
		boolean hasBaseGroup = false;
		boolean hasInterestInAllCstics = false;
            
		JCO.Function functionCfgApiGetFilteredCstics = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_FILTERED_CSTICS);
		JCO.ParameterList im = functionCfgApiGetFilteredCstics.getImportParameterList();
		JCO.ParameterList ex = functionCfgApiGetFilteredCstics.getExportParameterList();
		JCO.ParameterList tbl = functionCfgApiGetFilteredCstics.getTableParameterList();
            
		// unknownCsticsInterest for cstics
		StringBuffer unknownCsticsInterest = new StringBuffer();
		// Now stored in KB Cache unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_LNAMES);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_DOMAIN_IS_INTERVAL);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_VISIBLES);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_ADDITS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_CONSISTENTS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_CONSTRAINEDS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_GROUPS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_MULTIS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_READONLYS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_REQUIREDS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_UNITS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_ENTRY_FIELD_MASK);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_TYPELENGTH);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_NUMBERSCALE);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_VALUETYPE);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_GROUP_POSITION);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_INST_POSITION);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_VIEWS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_DISPLAY_MODE);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_MIMES);
            
		// unknownCsticsInterest for values
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_ASSIGNEDS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_AUTHORS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_DEFAULTS);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_LNAMES);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_LPRICES);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_PRICES);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_CONDITIONS);            
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_IS_DOMAIN_VALUE);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_MIMES);
		unknownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_DESCRIPTIONS);

		// knownCsticsInterest for cstics
		StringBuffer knownCsticsInterest = new StringBuffer();
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_CONSISTENTS);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_REQUIREDS);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_VISIBLES);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_READONLYS);
        knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_LPRICES);
        knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_PRICES);            		
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_CONSTRAINEDS); // after change on AP-side this became dynamic information (AP-side change done by TL on 2005-06-30)
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_GROUP_POSITION);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_INST_POSITION);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_CSTIC_DOMAIN_IS_INTERVAL); // after change on AP-side this became dynamic information (AP-side change done by TL on 2005-06-30)
		
            
		// knownCsticsInterest for values
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_ASSIGNEDS);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_AUTHORS);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_LNAMES);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_IS_DOMAIN_VALUE);
		knownCsticsInterest.append(CfgApiGetFilteredCstics.INTEREST_VALUE_DEFAULTS);

		// fill the table with the information about known and unknown interests
		JCO.Table imCsticInfoGroups = im.getTable(CfgApiGetFilteredCstics.CSTIC_INFO_GROUPS);
		// exactly two entries in the table
		imCsticInfoGroups.appendRow();
		imCsticInfoGroups.setValue("0", CfgApiGetFilteredCstics.GROUP_ID);        // CHAR, Group Name
		imCsticInfoGroups.setValue(unknownCsticsInterest.toString(), CfgApiGetFilteredCstics.INTEREST);       // STRING, CFG: Data element of type STRING
		imCsticInfoGroups.appendRow();
		imCsticInfoGroups.setValue("1", CfgApiGetFilteredCstics.GROUP_ID);        // CHAR, Group Name
		imCsticInfoGroups.setValue(knownCsticsInterest.toString(), CfgApiGetFilteredCstics.INTEREST);       // STRING, CFG: Data element of type STRING

		// set general information of the instance
		im.setValue(configuration.getId(), CfgApiGetFilteredCstics.CONFIG_ID);     // STRING, Configuration Identifier
		im.setValue(this.getId(), CfgApiGetFilteredCstics.INST_ID);     // CHAR, Internal identifier of a Instance
		boolean hasGroupIdLangIndep = im.hasField(CfgApiGetFilteredCstics.GROUP_ID_LANGINDEP);
		
		if (hasGroupIdLangIndep && groupKeyField.equals(CfgApiGetCsticGroups.GROUP_ID) && javaImplementationUpdated) {
			//if the flag in the function definition exists
			im.setValue(RfcConstants.TRUE, CfgApiGetFilteredCstics.GROUP_ID_LANGINDEP);
		}

		if (location.beDebug()) {
			location.debugT("groupCache: Update is required!");
			location.debugT("The following groups will be synced:\n");
		}
		// loop over the groups that need to be synced and prepare the import parameters
		for (int n = 0; n<groupsToSync.size(); n++){
			GroupCache gCache = (GroupCache)groupsToSync.get(n);
			if (location.beDebug()) {
				location.debugT(gCache.getGroup().getName() + " Interest: " 
									+ gCache.getInterest() + " Required Refresh: " 
									+ gCache.getRequiredRefresh() + "\n");
			}
            if (!gCache.getGroup().isBaseGroup()) {
                // process table imFilterGroups
                // optional, TABLE, CFG: Table of group names
                JCO.Table imFilterGroups = im.getTable(CfgApiGetFilteredCstics.FILTER_GROUPS);
                imFilterGroups.appendRow();
                imFilterGroups.setValue(gCache.getGroup().getName(), CfgApiGetFilteredCstics.GROUP_ID);       // CHAR, Group Name
            } else {
				hasBaseGroup = true;
            }
       
            // If you are only interested in visible cstics then only visible cstics
            // were requested. If you are interested in all cstics the next time then
            // a refresh of the invisible cstics is required.
            if (gCache.getInterest() == GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY) {
                gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_INVISIBLE_CSTICS);
            // If you are interested in all cstics then no refresh is required next time.
            } else {
            	hasInterestInAllCstics = true;
                gCache.setRequiredRefresh(GroupCache.NO_REFRESH_REQUIRED);
            }
            gCache.setInterest(GroupCache.NO_INTEREST);

            // Now add all known characteristics
            // process table imMonitoredCstics
            // optional, TABLE, CFG: Table of monitored characteristics
            JCO.Table imMonitoredCstics = im.getTable(CfgApiGetFilteredCstics.MONITORED_CSTICS);            
            Iterator knownCsticIterator = gCache.getGroup().getListOfCachedCharacteristics().iterator();
            while (knownCsticIterator.hasNext()) {
                Characteristic characteristic = (Characteristic) knownCsticIterator.next();
                imMonitoredCstics.appendRow();
                imMonitoredCstics.setValue(characteristic.getName(), CfgApiGetFilteredCstics.CSTIC_NAME);        // CHAR, Characteristic name (object characteristic)
                imMonitoredCstics.setValue("1", CfgApiGetFilteredCstics.GROUP_ID);        // CHAR, Group Name
                previouslyKnownCstics.put(characteristic.getName(), characteristic);
            }
		}

		// set flag if only assignable values are necessary
		String assignableValuesOnlyFlag = FALSE;
		if (isAssignableValuesOnly()) {
			assignableValuesOnlyFlag = TRUE;
		}
		im.setValue(assignableValuesOnlyFlag, CfgApiGetFilteredCstics.ASSIGNABLE_VALUES_ONLY);        // optional, CHAR, show only selectable characteristic values(T=true, F=false, ""=false)		
		// set flag if all groups that need to be synced have only interest in visible cstics
		if (!hasInterestInAllCstics) {
			im.setValue(TRUE, CfgApiGetFilteredCstics.VISIBLE_CSTICS_ONLY);      // optional, CHAR, show only visible characteristics (T=true, F=false, ""=false)
		}
		// set flag if also the BaseGroup needs to be synced
		if (hasBaseGroup){
			im.setValue(TRUE, CfgApiGetFilteredCstics.INCLUDE_UNGROUPED_CSTICS);        // optional, CHAR, include ungrouped characteristics(T=true, F=false, ""=false)
		} 
		else {
			im.setValue(FALSE, CfgApiGetFilteredCstics.INCLUDE_UNGROUPED_CSTICS);        // optional, CHAR, include ungrouped characteristics(T=true, F=false, ""=false)
		}
		// set flag if there is only one group and if this is the BaseGroup
		if ((groupsToSync.size() == 1) && ((GroupCache)groupsToSync.get(0)).getGroup().isBaseGroup()){
			im.setValue(TRUE, CfgApiGetFilteredCstics.UNGROUPED_CSTICS_ONLY);      // optional, CHAR, show only ungrouped characteristics (T=true, F= false, ""=true)
    	}
            
        category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_FILTERED_CSTICS");
        ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetFilteredCstics);
        category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_FILTERED_CSTICS");
        
        // process table exCsticsInGroups
        // TABLE, CFG: Table of characteristics sorted in groups
        JCO.Table exCsticsInGroups = ex.getTable(CfgApiGetFilteredCstics.CSTICS_IN_GROUPS);
        String groupId;
        int exCsticsInGroupsCounter;
        for (exCsticsInGroupsCounter=0; exCsticsInGroupsCounter<exCsticsInGroups.getNumRows(); exCsticsInGroupsCounter++) {
            exCsticsInGroups.setRow(exCsticsInGroupsCounter);
            groupId = exCsticsInGroups.getString(CfgApiGetFilteredCstics.GROUP_ID);

            // determine length of arrays
            JCO.Table exCsticsInGroupsCstics = exCsticsInGroups.getTable(CfgApiGetFilteredCstics.CSTICS);
            int csticsNumRows = exCsticsInGroupsCstics.getNumRows();
            int valuesNumRows = 0;
            int valueMimesNumRows = 0;
            int csticMimesNumRows = 0;
            for (int exCsticsInGroupsCsticsCounter=0; exCsticsInGroupsCsticsCounter<csticsNumRows; exCsticsInGroupsCsticsCounter++) {
                exCsticsInGroupsCstics.setRow(exCsticsInGroupsCsticsCounter);
                JCO.Table exCsticsInGroupsCsticsCsticValues = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_VALUES);
                valuesNumRows = valuesNumRows + exCsticsInGroupsCsticsCsticValues.getNumRows();
                for (int exCsticsInGroupsCsticsCsticValuesCounter=0; exCsticsInGroupsCsticsCsticValuesCounter<exCsticsInGroupsCsticsCsticValues.getNumRows(); exCsticsInGroupsCsticsCsticValuesCounter++) {
                    exCsticsInGroupsCsticsCsticValues.setRow(exCsticsInGroupsCsticsCsticValuesCounter);
                    JCO.Table exCsticsInGroupsCsticsCsticValuesValueMimes = exCsticsInGroupsCsticsCsticValues.getTable(CfgApiGetFilteredCstics.VALUE_MIMES);
                    valueMimesNumRows = valueMimesNumRows + exCsticsInGroupsCsticsCsticValuesValueMimes.getNumRows();
                }
                JCO.Table exCsticsInGroupsCsticsCsticMimes = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_MIMES);
                csticMimesNumRows = csticMimesNumRows + exCsticsInGroupsCsticsCsticMimes.getNumRows();
            }
            // initialize the arrays with the correct size
            String[] csticNames = new String[csticsNumRows];
            String[] csticLNames = new String[csticsNumRows];
			String[] csticDescriptions = new String[csticsNumRows];
            String[] csticDomainIsIntervals = new String[csticsNumRows];
            String[] csticVisibles = new String[csticsNumRows];
            String[] csticAddits = new String[csticsNumRows];
            String[] csticAuthors = new String[csticsNumRows];
            String[] csticConsistents = new String[csticsNumRows];
            String[] csticConstraineds = new String[csticsNumRows];
            String[] csticGroups = new String[csticsNumRows];
            String[] csticMultis = new String[csticsNumRows];
            String[] csticReadOnlys = new String[csticsNumRows];
            String[] csticRequireds = new String[csticsNumRows];
            String[] csticUnits = new String[csticsNumRows];
            String[] csticEntryFieldMask = new String[csticsNumRows];
            Integer[] csticTypeLength = new Integer[csticsNumRows];
            Integer[] csticNumberScale = new Integer[csticsNumRows];
            Integer[] csticValueType = new Integer[csticsNumRows];
            Integer[] csticGroupPositions = new Integer[csticsNumRows];
            Integer[] csticInstPositions = new Integer[csticsNumRows];
            String[] csticApplicationViews = new String[csticsNumRows];
            String[] csticDisplayModes = new String[csticsNumRows];
            String[] csticMimeLNames = new String[csticMimesNumRows];
            String[] csticMimeTypes = new String[csticMimesNumRows]; 
            String[] csticMimeObjects = new String[csticMimesNumRows];
            String[] csticVarsearchRelevants = new String[csticsNumRows];                
            String[] valueCsticNames = new String[valuesNumRows];
            String[] valueNames = new String[valuesNumRows];
            String[] valueAssigneds = new String[valuesNumRows];
            String[] valueAuthors = new String[valuesNumRows];
            String[] valueDefaults = new String[valuesNumRows];
            String[] valueLNames = new String[valuesNumRows];
			String[] valueDescriptions = new String[valuesNumRows];
            String[] valueConditions = new String[valuesNumRows];
            String[] valueLPrices = new String[valuesNumRows];
            String[] valuePrices = new String[valuesNumRows];
            String[] valueIsDomainValues = new String[valuesNumRows];                   
            String[] valueMimeLNames = new String[valueMimesNumRows];
            String[] valueMimeTypes = new String[valueMimesNumRows];
            String[] valueMimeObjects = new String[valueMimesNumRows]; 

            // initialize all counters for filling the arrays (different ones than for the single table loops)
            int csticsCounter=0;
            int csticValuesCounter = 0;
            int csticValuesMimesCounter=0;
            int csticsMimesCounter=0;

            // process table exCsticsInGroupsCstics
            for (int exCsticsInGroupsCsticsCounter=0; exCsticsInGroupsCsticsCounter<csticsNumRows; exCsticsInGroupsCsticsCounter++) {
                exCsticsInGroupsCstics.setRow(exCsticsInGroupsCsticsCounter);
                // process structure exCsticsInGroupsCsticsCsticHeader
                JCO.Structure exCsticsInGroupsCsticsCsticHeader = exCsticsInGroupsCstics.getStructure(CfgApiGetFilteredCstics.CSTIC_HEADER);
                csticAddits[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_ADDIT);
                csticConstraineds[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_CONSTRAINED);
                csticEntryFieldMask[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_ENTRY_FIELD_MASK);
                csticGroups[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_GROUP);
                csticLNames[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_LNAME);
                csticMultis[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_MULTI);
                csticNames[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_NAME);
                csticNumberScale[csticsCounter] = (Integer)exCsticsInGroupsCsticsCsticHeader.getValue(CfgApiGetFilteredCstics.CSTIC_NUMBERSCALE);
                csticTypeLength[csticsCounter] = (Integer)exCsticsInGroupsCsticsCsticHeader.getValue(CfgApiGetFilteredCstics.CSTIC_TYPELENGTH);
                csticUnits[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_UNIT);
                csticValueType[csticsCounter] = (Integer)exCsticsInGroupsCsticsCsticHeader.getValue(CfgApiGetFilteredCstics.CSTIC_VALUETYPE);
                csticDisplayModes[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_DISPLAY_MODE);
                csticVarsearchRelevants[csticsCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_VARSEARCH_RELEVANT);
                csticGroupPositions[csticsCounter] = (Integer)exCsticsInGroupsCstics.getValue(CfgApiGetFilteredCstics.CSTIC_GROUP_POSITION);
                csticInstPositions[csticsCounter] = (Integer)exCsticsInGroupsCstics.getValue(CfgApiGetFilteredCstics.CSTIC_INST_POSITION);
                csticDomainIsIntervals[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_DOMAIN_IS_INTERVAL);
                csticReadOnlys[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_READONLY);
                csticVisibles[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_VISIBLE);
                csticAuthors[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_AUTHOR);
                csticRequireds[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_REQUIRED);
                csticConsistents[csticsCounter] = exCsticsInGroupsCstics.getString(CfgApiGetFilteredCstics.CSTIC_CONSISTENT);                    
                
                // process table exCsticsInGroupsCsticsCsticValues
                JCO.Table exCsticsInGroupsCsticsCsticValues = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_VALUES);
                for (int exCsticsInGroupsCsticsCsticValuesCounter = 0;exCsticsInGroupsCsticsCsticValuesCounter<exCsticsInGroupsCsticsCsticValues.getNumRows(); exCsticsInGroupsCsticsCsticValuesCounter++) {
                    exCsticsInGroupsCsticsCsticValues.setRow(exCsticsInGroupsCsticsCsticValuesCounter);
                    // link to cstic-name: necessary to reuse the communication-type independent COL
                    valueCsticNames[csticValuesCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_NAME);
                    valueNames[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_NAME);
                    valueLNames[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_LNAME);
                    valueDescriptions[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_DESCRIPTION);
                    valueIsDomainValues[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_IS_DOMAIN_VALUE);
                    valueDefaults[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_DEFAULT);
                    valueAssigneds[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_ASSIGNED);
                    valueAuthors[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_AUTHOR);
                    valuePrices[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_PRICE);
                    valueLPrices[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_LPRICE);
                    valueConditions[csticValuesCounter] = exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_CONDITION);
                    
                    // process table exCsticsInGroupsCsticsCsticValuesValueMimes
                    JCO.Table exCsticsInGroupsCsticsCsticValuesValueMimes = exCsticsInGroupsCsticsCsticValues.getTable(CfgApiGetFilteredCstics.VALUE_MIMES);
                    for (int exCsticsInGroupsCsticsCsticValuesValueMimesCounter=0; exCsticsInGroupsCsticsCsticValuesValueMimesCounter<exCsticsInGroupsCsticsCsticValuesValueMimes.getNumRows(); exCsticsInGroupsCsticsCsticValuesValueMimesCounter++) {
                        exCsticsInGroupsCsticsCsticValuesValueMimes.setRow(exCsticsInGroupsCsticsCsticValuesValueMimesCounter);
                        // link to cstic/value-name: necessary to reuse the communication-type independent COL
                        valueMimeLNames[csticValuesMimesCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_NAME)
                             + "." 
                             + exCsticsInGroupsCsticsCsticValues.getString(CfgApiGetFilteredCstics.VALUE_NAME);
                        valueMimeTypes[csticValuesMimesCounter] = exCsticsInGroupsCsticsCsticValuesValueMimes.getString(CfgApiGetFilteredCstics.MIME_OBJECT_TYPE);
                        valueMimeObjects[csticValuesMimesCounter] = exCsticsInGroupsCsticsCsticValuesValueMimes.getString(CfgApiGetFilteredCstics.MIME_OBJECT);
                        csticValuesMimesCounter++;
                    }
                    csticValuesCounter++;
                }
                
                // process table exCsticsInGroupsCsticsCsticMimes
                JCO.Table exCsticsInGroupsCsticsCsticMimes = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_MIMES);
                for (int exCsticsInGroupsCsticsCsticMimesCounter=0; exCsticsInGroupsCsticsCsticMimesCounter<exCsticsInGroupsCsticsCsticMimes.getNumRows(); exCsticsInGroupsCsticsCsticMimesCounter++) {
                    exCsticsInGroupsCsticsCsticMimes.setRow(exCsticsInGroupsCsticsCsticMimesCounter);
                    // link to cstic-name: necessary to reuse the communication-type independent COL
                    csticMimeLNames[csticsMimesCounter] = exCsticsInGroupsCsticsCsticHeader.getString(CfgApiGetFilteredCstics.CSTIC_NAME);                       
                    csticMimeTypes[csticsMimesCounter] = exCsticsInGroupsCsticsCsticMimes.getString(CfgApiGetFilteredCstics.MIME_OBJECT_TYPE);
                    csticMimeObjects[csticsMimesCounter] = exCsticsInGroupsCsticsCsticMimes.getString(CfgApiGetFilteredCstics.MIME_OBJECT);
                    csticsMimesCounter++;
                }
                
				// process table exCsticsInGroupsCsticsCsticDescriptions
				JCO.Table exCsticsInGroupsCsticsCsticDescriptions = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_DESCRIPTION);
				StringBuffer csticDescriptionsLine = new StringBuffer();
				for (int exCsticsInGroupsCsticsCsticDescriptionsCounter=0; exCsticsInGroupsCsticsCsticDescriptionsCounter<exCsticsInGroupsCsticsCsticDescriptions.getNumRows(); exCsticsInGroupsCsticsCsticDescriptionsCounter++) {
					exCsticsInGroupsCsticsCsticDescriptions.setRow(exCsticsInGroupsCsticsCsticDescriptionsCounter);
					csticDescriptionsLine.append(exCsticsInGroupsCsticsCsticDescriptions.getString(CfgApiGetFilteredCstics.TEXT_LINE));
				}
				csticDescriptions[csticsCounter] = csticDescriptionsLine.toString();

                // process table exCsticsInGroupsCsticsCsticViews
                JCO.Table exCsticsInGroupsCsticsCsticViews = exCsticsInGroupsCstics.getTable(CfgApiGetFilteredCstics.CSTIC_VIEWS);
                StringBuffer csticViews = new StringBuffer();
                for (int exCsticsInGroupsCsticsCsticViewsCounter=0; exCsticsInGroupsCsticsCsticViewsCounter<exCsticsInGroupsCsticsCsticViews.getNumRows(); exCsticsInGroupsCsticsCsticViewsCounter++) {
                    exCsticsInGroupsCsticsCsticViews.setRow(exCsticsInGroupsCsticsCsticViewsCounter);
                    csticViews.append(exCsticsInGroupsCsticsCsticViews.getString(CfgApiGetFilteredCstics.CSTIC_VIEW));
                }
                csticApplicationViews[csticsCounter] = csticViews.toString();
                csticsCounter++;
            }
            // assign collected data either to unknownCstic (groupId=0) or knownCstic (groupId=1) 
            if (groupId.equals("0")) {
                unknownCsticNames = csticNames;
                unknownCsticLNames = csticLNames;
                unknownCsticDomainIsAnIntervals = csticDomainIsIntervals;
                unknownCsticVisibles = csticVisibles;
                unknownCsticAddits = csticAddits;
                unknownCsticAuthors = csticAuthors;
                unknownCsticConsistents = csticConsistents;
                unknownCsticConstraineds = csticConstraineds;
                unknownCsticGroups = csticGroups;
                unknownCsticMultis = csticMultis;
                unknownCsticReadOnlys = csticReadOnlys;
                unknownCsticRequireds = csticRequireds;
                unknownCsticUnits = csticUnits;
                unknownCsticEntryFieldMask = csticEntryFieldMask;
                unknownCsticTypeLength = csticTypeLength;
                unknownCsticNumberScale = csticNumberScale; 
                unknownCsticValueType = csticValueType; 
                unknownCsticGroupPositions = csticGroupPositions; 
                unknownCsticInstPositions = csticInstPositions; 
                unknownCsticApplicationViews = csticApplicationViews;
                unknownCsticExpandeds = csticDisplayModes;
                unknownCsticMimeLNames = csticMimeLNames;
                unknownCsticMimeType = csticMimeTypes; 
                unknownCsticMimeObjects = csticMimeObjects;
                unknownCsticVarsearchRelevant = csticVarsearchRelevants;  
                
                unknownValueCsticNames = valueCsticNames;
                unknownValueNames = valueNames;
                unknownValueAssigneds = valueAssigneds;
                unknownValueAuthors = valueAuthors;
                unknownValueDefaults = valueDefaults;
                unknownValueLNames = valueLNames;
                unknownValueDescriptions = valueDescriptions;
                unknownValueConditionKeys = valueConditions;
                unknownValueLPrices = valueLPrices;
                unknownValuePrices = valuePrices;
                unknownValueIsInDomains = valueIsDomainValues;                   
                unknownMimeValueLNames = valueMimeLNames;
                unknownMimeType = valueMimeTypes;
                unknownMimeObject = valueMimeObjects;                                        
            }
            else if (groupId.equals("1")) {
                knownCsticNames = csticNames;
                knownCsticDomainIsAnIntervals = csticDomainIsIntervals;
                knownCsticVisibles = csticVisibles;
                knownCsticAuthors = csticAuthors;
                knownCsticConsistents = csticConsistents;
                knownCsticConstraineds = csticConstraineds;
                knownCsticRequireds = csticRequireds;
                knownCsticReadonlys = csticReadOnlys;
                knownValueprices = valuePrices;
                knownValueLprices = valueLPrices;                                        
                knownValueCsticNames = valueCsticNames;
                knownValueNames = valueNames;
                knownValueLongNames = valueLNames;
                knownValueAssigneds = valueAssigneds;
                knownValueAuthors = valueAuthors;
                knownValueDefaults = valueDefaults;
                knownValueIsInDomains = valueIsDomainValues;                 
            }
            else {
				category
						.logT(
								Severity.ERROR,
								location,
								"Short Text: No valid InfoGroup ID: "
										+ groupId
										+ ". \nWhat happened? This is a technical error that can only occur due to a software error. The parameter groupId has two possible values: 0 and 1. The current backend call CFG_API_GET_FILTERED_CSTICS however delivered "
										+ groupId + ". \nWhat you can do? Please note down the actions that lead to this error and report to your system administrator."
										+ "\nHow to correct the error? Check the latest available patches for software component SAP-SHRJAV."
										+ "\nIf the error occurs in an non-modified program, you may find a solution in a SAP note."
										+ "\nIf you have access to SAP Notes, carry out a search with the following keywords: CFG_API_GET_FILTERED_CSTICS, InfoGroup.");                    
			}
        }
         
        // process table exCsticGroups
        // TABLE, CFG: Table of characteristic groups
        JCO.Table exCsticGroups = ex.getTable(CfgApiGetFilteredCstics.CSTIC_GROUPS);
        int exCsticGroupsCounter;
        int groupNumRows = exCsticGroups.getNumRows();
        String groupNames[] = new String[groupNumRows];
        String groupConsistents[] = new String[groupNumRows];
        String groupRequireds[] = new String[groupNumRows];
        for (exCsticGroupsCounter=0; exCsticGroupsCounter<groupNumRows; exCsticGroupsCounter++) {
            exCsticGroups.setRow(exCsticGroupsCounter);
            groupNames[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetFilteredCstics.GROUP_NAME);
            groupConsistents[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetFilteredCstics.GROUP_CONSISTENT);
            groupRequireds[exCsticGroupsCounter] = exCsticGroups.getString(CfgApiGetFilteredCstics.GROUP_REQUIRED);
        }


        // is only used in case of numerical cstics; otherwise they would have a wrong format in display
        String knownValueLNames[] = null;
        if (knownValueLongNames != null) {
            knownValueLNames = new String[knownValueNames.length];
            if (knownValueLongNames.length < knownValueNames.length) {
                System.arraycopy(
                    knownValueLongNames,
                    0,
                    knownValueLNames,
                    0,
                    knownValueLongNames.length);
            } else {
                knownValueLNames = knownValueLongNames;
            }
        }

        // index[0]: index for values
        // index[1]: index for value mimes
        // index[2]: index for cstic mimes
        int index[] = new int[] { 0, 0, 0 };
        // First update all known characteristics
        index[0] = 0;
        for (i = 0; knownCsticNames != null && i < knownCsticNames.length; i++) {
            cstic = (DefaultCharacteristic) cstics.get(knownCsticNames[i]);
            cstic.update( 
                knownCsticVisibles[i].equals(TRUE),
                knownCsticConsistents[i].equals(TRUE),
                knownCsticRequireds[i].equals(TRUE),
                knownCsticReadonlys[i].equals(TRUE),
                knownCsticConstraineds[i].equals(TRUE),
                knownCsticDomainIsAnIntervals[i].equals(TRUE));
            cstic
                .updateCharacteristicValues(
                    index,
                    knownValueCsticNames,
                    knownValueNames,
                    knownValueLNames,
                    null, //descriptions do not change at runtime
                    null, //conditionKeys do not change at runtime
                    knownValueLprices,
                knownValueIsInDomains,
                knownValueAssigneds,
                knownValueAuthors,
                null,
                knownValueDefaults,
            null, 
            null, 
            null); 
            // Since the cstic was found in the server's response again,
            // remove it from the list of previously known cstics so that this
            // list contains only those previously known cstics that are missing
            // which means they became invisible and the server should return
            // visible cstics only
            previouslyKnownCstics.remove(cstic.getName());
            // if cstic belongs to base group we add it to csticsInBaseGroup
            // (used later to set the status of the baseGroup)
            DefaultCharacteristicGroup group = (DefaultCharacteristicGroup) cstic.getGroup();
            if (group.isBaseGroup()){
				csticsInBaseGroup.put(cstic.getName(), cstic);            	
            }
        }
        // Second add the new Characteristics
        index[0] = 0;
        for (i = 0;
            unknownCsticNames != null && i < unknownCsticNames.length;
            i++) {
            int unknownCsticValueTypeInt = unknownCsticValueType[i].intValue();
            int unknownCsticTypeLengthInt;
            if (unknownCsticTypeLength[i] != null){
                unknownCsticTypeLengthInt = unknownCsticTypeLength[i].intValue();
            }
            else {
                unknownCsticTypeLengthInt = 0;
            }
            int unknownCsticNumberScaleInt;
            if (unknownCsticNumberScale[i] != null){
                unknownCsticNumberScaleInt = unknownCsticNumberScale[i].intValue();
            }
            else {
                unknownCsticNumberScaleInt = 0;
            }
            
            // for backward compatibility (in the case that the server doesn't return 
            // application views for cstics)
            String unknownCsticApplicationView = "";
            if (unknownCsticApplicationViews != null) {
                unknownCsticApplicationView = unknownCsticApplicationViews[i];
            }
            
            //for backward compatibility in case the server does not return boolean expanded value
            //If no value is returned the value is defaulted to false
            boolean csticExpanded = false;
            if (unknownCsticExpandeds!=null && unknownCsticExpandeds.length > 0 && unknownCsticExpandeds[i] != null) {
                csticExpanded = unknownCsticExpandeds[i].equals("X");
            }else {
                location.warningT("Incompatible Server version. Parameter \"X\" not returned from server. Functionality of expand/collapse of single characteristics is not available!");
            }

            cstic =
                factory
                    .newCharacteristic(
                        configurationChangeStream,
                        ipcClient,
                        this,
                        unknownCsticGroups[i],
                        unknownCsticLNames[i],
                        unknownCsticNames[i],
                        unknownCsticUnits[i],
                        unknownCsticEntryFieldMask[i],
                        unknownCsticTypeLengthInt,
                        unknownCsticNumberScaleInt,
                        unknownCsticValueTypeInt,
                        unknownCsticAddits[i].equals(TRUE),
                        unknownCsticVisibles[i].equals(TRUE),
                        unknownCsticMultis[i].equals(TRUE),
                        unknownCsticRequireds[i].equals(TRUE),
                        csticExpanded,    
                        unknownCsticConsistents[i].equals(TRUE),
                        unknownCsticConstraineds[i].equals(TRUE),
                        unknownCsticDomainIsAnIntervals[i].equals(TRUE),
                        unknownCsticReadOnlys[i].equals(TRUE),
                        false, 
                        null,  
                        unknownCsticApplicationView,
                        unknownCsticMimeLNames,
                        unknownCsticMimeType,
                        unknownCsticMimeObjects,
                        index);

            cstics.put(unknownCsticNames[i], cstic);
		   // if cstic belongs to base group we add it to csticsInBaseGroup
		   // (used later to set the status of the baseGroup)
			if (unknownCsticGroups[i].equals("")){
				csticsInBaseGroup.put(cstic.getName(), cstic);	 
			}
            //check the cstics order
            int newGroupPosition = unknownCsticGroupPositions[i].intValue();
            int newInstPosition = unknownCsticInstPositions[i].intValue() - 1;
            addCharacteristic(newInstPosition, cstic);
			// add cstic to the right group
			String groupName = unknownCsticGroups[i];
			if (groupName.equals("")){
				groupName = BaseGroup.BASE_GROUP_NAME;
			}
			GroupCache gCache = (GroupCache)gCaches.get(groupName);
			if (gCache != null){
				gCache.getGroup().addCharacteristic(newGroupPosition, cstic);	
			}
            cstic
                .updateCharacteristicValues(
                    index,
                    unknownValueCsticNames,
                    unknownValueNames,
                    unknownValueLNames,
                    unknownValueDescriptions, 
                unknownValueConditionKeys,
                unknownValueLPrices,
                unknownValueIsInDomains,
                unknownValueAssigneds,
                unknownValueAuthors,
                null,
                unknownValueDefaults,
            unknownMimeValueLNames, unknownMimeType, unknownMimeObject);
        }

        // Now set all remaining cstics of the previously known cstics list
        // to the state invisible. That should keep a copy and renew the
        // information only if invisible cstics should be displayed as well.
        Iterator previouslyKnownCsticsIt = previouslyKnownCstics.values().iterator();
        while (previouslyKnownCsticsIt.hasNext()) {
            Characteristic characteristic = (Characteristic) previouslyKnownCsticsIt.next();
            characteristic.setVisible(false);
        }
            
		// set the group status
		for (int k=0; k<groupNames.length; k++) {
			GroupCache gCache = (GroupCache)gCaches.get(groupNames[k]);
			// if it is a normal group the FM returns an array that contains the group name
			if (gCache != null) {
				boolean groupConsistent = groupConsistents[k].equals(TRUE)
						? true
						: false;
				boolean groupReq = groupRequireds[k].equals(TRUE)
						? true
						: false;
				gCache.getGroup().setConsistent(groupConsistent);
				gCache.getGroup().setRequired(groupReq);
			}
		}
		// special handling for the BaseGroup
		Iterator caches = gCaches.values().iterator();
		GroupCache basegCache = null;
		while (caches.hasNext()) {
			basegCache = (GroupCache) caches.next();
			if (basegCache.getGroup().isBaseGroup()){
				break;
			}
		}			
		if (basegCache != null){
			Iterator keys = csticsInBaseGroup.keySet().iterator();
			boolean groupConsistent = true;
			boolean groupReq = false;
			while (keys.hasNext()){
				Characteristic currentCstic = (Characteristic)csticsInBaseGroup.get(keys.next());
				// as soon as one cstic is not consistent the whole group is not consistent
				if (!currentCstic.isConsistent()) {
					groupConsistent = false;
				}
				// as soon as one required cstic has no assigned values the whole group is required
				if (currentCstic.isRequired() && !currentCstic.hasAssignedValues()){
					groupReq = true;
				}
				// break the loop as soon as both flags have been changed
				if ((groupConsistent == false) && (groupReq == true)){
					break;
				}
			}
			basegCache.getGroup().setConsistent(groupConsistent);
			basegCache.getGroup().setRequired(groupReq);                    
		}
          
        for (int n=0; n<groupsToSync.size(); n++){
			GroupCache gCache = (GroupCache)groupsToSync.get(n);
			// Reset interest, because it has been satisfied!        
			gCache.setInterest(GroupCache.NO_INTEREST);
        }
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#specialize(java.lang.String)
     */
    public void specialize(String specTypeId) {
        location.entering("specialize(String specTypeId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String specTypeId " + specTypeId);
        }
        String configId = configuration.getId();
        String instId = this.getId();

        try {
            JCO.Function functionCfgApiSpecialize = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_SPECIALIZE);
            JCO.ParameterList im = functionCfgApiSpecialize.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiSpecialize.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiSpecialize.getTableParameterList();
            
            im.setValue(configId, CfgApiSpecialize.CONFIG_ID);     // STRING, Configuration Identifier
            im.setValue(instId, CfgApiSpecialize.INST_ID);     // CHAR, Internal identifier of a Instance
            im.setValue(specTypeId, CfgApiSpecialize.SPEC_TYPE_ID);        // INT2, Id used for Assumption Id or Assumption Group Id
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_SPECIALIZE");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiSpecialize);
            category.logT(Severity.PATH, location, "done with RFC CFG_API_SPECIALIZE");
        } catch (ClientException e) {
            throw new IPCException(e);
        }
        // update the config's change time
        configuration.setChangeTime();
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#unspecialize()
     */
    public void unspecialize() {
        location.entering("unspecialize()");
        String configId = configuration.getId();
        String instId = this.getId();

        try {
            JCO.Function functionCfgApiUnspecialize = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_UNSPECIALIZE);
            JCO.ParameterList im = functionCfgApiUnspecialize.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiUnspecialize.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiUnspecialize.getTableParameterList();
            
            im.setValue(configId, CfgApiUnspecialize.CONFIG_ID);     // STRING, Configuration Identifier
            im.setValue(instId, CfgApiUnspecialize.INST_ID);     // CHAR, Internal identifier of a Instance
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_UNSPECIALIZE");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiUnspecialize);
            category.logT(Severity.PATH, location, "done with RFC CFG_API_UNSPECIALIZE");
        } catch (ClientException e) {
            throw new IPCException(e);
        }

        // update the config's change time
        configuration.setChangeTime();
        location.exiting();

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#createChildInstance(java.lang.String)
     */
    public void createChildInstance(String decompPosition) {
        try {
            location.entering("createChildInstance(String decompPosition)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String decompPosition " + decompPosition);
            }        
            String configId = configuration.getId();
            String decompPosNr = decompPosition;
            String parentInstId = this.getId();

            JCO.Function functionCfgApiCreateInstance = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_CREATE_INSTANCE);
            JCO.ParameterList im = functionCfgApiCreateInstance.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiCreateInstance.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiCreateInstance.getTableParameterList();

            im.setValue(configId, CfgApiCreateInstance.CONFIG_ID);     // STRING, Configuration Identifier
            im.setValue(decompPosNr, CfgApiCreateInstance.DECOMP_POS_NR);      // CHAR, Item position
            im.setValue(parentInstId, CfgApiCreateInstance.PARENT_INST_ID);        // CHAR, Internal identifier of a Instance
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_CREATE_INSTANCE");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiCreateInstance); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC CFG_API_CREATE_INSTANCE");

            String instId = ex.getString(CfgApiCreateInstance.INST_ID); // not used by COL
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        // update the config's change time
        
        configuration.setChangeTime();
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#getProductVariants(java.lang.String, java.lang.String, java.lang.String)
     */
    public List getProductVariants(
        String limit,
        String quality,
        String maxMatchPoints) {
        location.entering("List getProductVariants(String limit, String quality, String maxMatchPoints)");
        if (location.beDebug()) {
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String limit " + limit);
                location.debugT("String quality " + quality);
                location.debugT("String maxMatchPoints " + maxMatchPoints);
            }
        }
        Vector productVariants = new Vector();
        if (!this.isRootInstance()) {
            if (location.beDebug()){
                location.debugT("return productVariants (empty list)");
            }
            location.exiting();
            return productVariants;
        }
        try {
            String configId = configuration.getId();
            Integer limitInt = new Integer(limit);
            Integer maxMatchPoint = new Integer(maxMatchPoints);
            Double qualityDbl = new Double(quality);

            JCO.Function functionCfgApiGetProductVariants = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_PRODUCT_VARIANTS);
            JCO.ParameterList im = functionCfgApiGetProductVariants.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiGetProductVariants.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiGetProductVariants.getTableParameterList();
            
            im.setValue(configId, CfgApiGetProductVariants.CONFIG_ID);     // STRING, Configuration Identifier
            im.setValue(limitInt, CfgApiGetProductVariants.LIMIT);        // optional, INT, CFG: Data element of type STRING
            im.setValue(maxMatchPoint, CfgApiGetProductVariants.MAX_MATCH_POINT);      // optional, INT, CFG: Integer
            im.setValue(qualityDbl, CfgApiGetProductVariants.QUALITY);        // optional, FLOAT, CFG: Double
            // im.setValue(searchMode, "SEARCH_MODE");     // optional, CHAR, '0'=exact / '2'=incomplete
            // im.setValue(targetDate, "TARGET_DATE");     // optional, STRING, CFG: Data element of type STRING
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_PRODUCT_VARIANTS");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetProductVariants); // does not define ABAP Exceptions
            category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_PRODUCT_VARIANTS");

            String variantCount = ((Integer)ex.getValue(CfgApiGetProductVariants.VARIANT_COUNT)).toString();
            //return an empty list if nothing found
            if (variantCount == null) {
                if (location.beDebug()){
                    location.debugT("return productVariants (empty list)");
                }
                location.exiting();
                return productVariants;
            }
            if (variantCount.equalsIgnoreCase("0")) {
                if (location.beDebug()){
                    location.debugT("return productVariants (empty list)");
                }
                location.exiting();
                return productVariants;
            }            
            
            // process table exProductVariants
            // TABLE, Configuration: Table of Product Variants
            JCO.Table exProductVariants = ex.getTable(CfgApiGetProductVariants.PRODUCT_VARIANTS);
            int exProductVariantsCounter;
            String variantProductId;
            Double distance;
            String matchPoints;            
            for (exProductVariantsCounter=0; exProductVariantsCounter<exProductVariants.getNumRows(); exProductVariantsCounter++) {
                exProductVariants.setRow(exProductVariantsCounter);
                variantProductId = exProductVariants.getString(CfgApiGetProductVariants.VARIANT_PRODUCT_ID);
                distance = (Double)exProductVariants.getValue(CfgApiGetProductVariants.DISTANCE);
                matchPoints = ((Integer)exProductVariants.getValue(CfgApiGetProductVariants.MATCH_POINTS)).toString();
                ProductVariant productVariant =
                    factory.newProductVariant(
                        ipcClient,
                        variantProductId,
                        distance.doubleValue(),
                        matchPoints,
                        maxMatchPoints,
                        new Vector(),
                        null);               
                
                // process table exProductVariantsFeatures
                JCO.Table exProductVariantsFeatures = exProductVariants.getTable(CfgApiGetProductVariants.FEATURES);
                String featureId;
                String featureType;
                String featureName;
                String featureLName;
                String featureLongtext;
                String featureValue;
                String featureLValue;
                Double featureWeight;
                Double featureDistance;  
                int exProductVariantsFeaturesCounter;              
                for (exProductVariantsFeaturesCounter=0; exProductVariantsFeaturesCounter<exProductVariantsFeatures.getNumRows(); exProductVariantsFeaturesCounter++) {
                    exProductVariantsFeatures.setRow(exProductVariantsFeaturesCounter);
                    featureId = exProductVariantsFeatures.getString(CfgApiGetProductVariants.FEATURE_ID);
                    featureType = exProductVariantsFeatures.getString(CfgApiGetProductVariants.FEATURE_TYPE);
                    featureName = exProductVariantsFeatures.getString(CfgApiGetProductVariants.FEATURE_NAME);
                    featureLName = exProductVariantsFeatures.getString(CfgApiGetProductVariants.FEATURE_LNAME);
                    featureLongtext = exProductVariantsFeatures.getString(CfgApiGetProductVariants.FEATURE_LONGTEXT);
                    featureValue = exProductVariantsFeatures.getString(CfgApiGetProductVariants.FEATURE_VALUE);
                    featureLValue = exProductVariantsFeatures.getString(CfgApiGetProductVariants.FEATURE_LVALUE);
                    featureWeight = (Double)exProductVariantsFeatures.getValue(CfgApiGetProductVariants.FEATURE_WEIGHT);
                    featureDistance = (Double)exProductVariantsFeatures.getValue(CfgApiGetProductVariants.FEATURE_DISTANCE);
                    ProductVariantFeature feature =
                        factory.newProductVariantFeature(
                            ipcClient,
                            productVariant,
                            featureType,
                            featureId,
                            featureName,
                            featureLName,
                            featureLongtext,
                            featureValue,
                            featureLValue,
                            featureWeight.doubleValue(),
                            featureDistance.doubleValue());
                    productVariant.addFeature(feature);
                    if (productVariant.getFeatures().size() == 1) {
                        productVariant.setMainDifference(feature);
                    }
                }
                productVariants.add(productVariant);
            }            
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        if (location.beDebug()) {
            if (productVariants!=null){
                location.debugT("return productVariants " + productVariants.toString());
            }
            else location.debugT("return productVariants null");
        }
        location.exiting();
        return productVariants;

    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#applyProductVariant(java.lang.String)
     */
    public boolean applyProductVariant(String productVariantId) {
        location.entering("applyProductVariant(String productVariantId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String productVariantId " + productVariantId);
        }
          boolean success = false;
        if (!this.isRootInstance()) {
            if (location.beDebug()){
                location.debugT("return false");
            }
            location.exiting();
            return false;
        }
        try {
            IPCItem item = getIpcItem();
            if (item != null) {
                success = item.changeProduct(productVariantId);
                ((RfcDefaultIPCItem)item).setCacheDirty();
            } else {
                JCO.Function functionSpcChangeItemProduct = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.SPC_CHANGE_ITEM_PRODUCT);
                JCO.ParameterList im = functionSpcChangeItemProduct.getImportParameterList();
                JCO.ParameterList ex = functionSpcChangeItemProduct.getExportParameterList();
                JCO.ParameterList tbl = functionSpcChangeItemProduct.getTableParameterList();
                
                im.setValue(productVariantId, SpcChangeItemProduct.PRODUCT_ID);       // CHAR, Product Id
                    im.setValue(configuration.getId(), SpcChangeItemProduct.CONFIG_ID);     // optional, STRING, Configuration Identifier
                category.logT(Severity.PATH, location, "executing RFC SPC_CHANGE_ITEM_PRODUCT");
                ((RFCDefaultClient)cachingClient).execute(functionSpcChangeItemProduct);
                category.logT(Severity.PATH, location, "done with RFC SPC_CHANGE_ITEM_PRODUCT");
    
                // HACK: right now, ChangeItemProduct doesn't return anything for its success, assume the best has happened :-)
                success = true;
            }
            cacheIsDirty = true;
			configuration.setCacheDirty(true);
			configuration.setInstanceCacheDirty(true);

        } catch (ClientException e) { 
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        if (location.beDebug()){
            location.debugT("return success " + success);
        }
        location.exiting();
        return success;
    }
    
    protected static String[] arrayListToStringArray(ArrayList al) {
        String[] str = null;
        if (al != null) {
            str = new String[al.size()];
            for (int i=0;i<al.size(); i++){
                str[i] = (String) al.get(i);
            }
        }
        else {
            str = new String[0];
        }
        return str;
    }    
    protected static Integer[] integerArrayListToStringArray(ArrayList al) {
        Integer[] inte = null;
        if (al != null) {
            inte = new Integer[al.size()];
            for (int i=0;i<al.size(); i++){
                inte[i] = (Integer) al.get(i);
            }
        }
        else {
            inte = new Integer[0];
        }
        return inte;
    }    
}
