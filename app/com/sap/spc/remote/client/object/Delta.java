package com.sap.spc.remote.client.object;

/**
 * A delta of a field in the configuration comparison.
 */
public interface Delta {
    
    
    /**
     * @return the name of the field, where the delta occurs.
     */
    public String getFieldName();

    /**
     * @return the old value of the field
     */
    public String getValue1();

    /**
     * @return the new value of the field
     */
    public String getValue2();

}
