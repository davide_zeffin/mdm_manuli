/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

/**
 * An IPCItemReference knows everything needed to connect to a remote IPC server session,
 * and retrieve an item from that session. This information is made up from the everything
 * needed to contact an IPC server (host, port, character encoding), and from everything
 * required to identify an item on a server (session id, document id, item id).
 */
public abstract class IPCItemReference
{
    protected String documentId;
    protected String itemId;


	
    public void setDocumentId(String newDocumentId) {
        documentId = newDocumentId;
    }
    public String getDocumentId() {
        return documentId;
    }
    public void setItemId(String newItemId) {
        itemId = newItemId;
    }
    public String getItemId() {
        return itemId;
    }
    
    /**
     * Uses the given reference to initialize itself.
     * @param reference.
     */
    public abstract void init(IPCItemReference reference);
    

}
