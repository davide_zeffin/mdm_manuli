/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object;

/**
 * A single pricing condition.
 * All values passed in and returned are handled in external represenation.
 */
public interface IPCPricingCondition extends Closeable {

	public String getStepNo()/* throws IPCException*/; //length = 3

	public String getCounter()/* throws IPCException*/; //length = 3
	
	public String getHeaderCounter()/* throws IPCException*/; //length = 3
	
	public String getConditionTypeName()/* throws IPCException*/; //length = 4

	public String getDescription()/* throws IPCException*/; //length = 40

	public void setConditionRate(String conditionRate);
	
	public void setConditionRate(String conditionRate, String decimalSeparator, String groupingSeparator);

	public String getConditionRate()/* throws IPCException*/; //length = 28,9

	public void setConditionCurrency(String conditionCurrency);

	public String getConditionCurrency()/* throws IPCException*/; //length = 5

	public void setPricingUnitValue(String pricingUnitValue); 

	/**
	 * Expects the localized form.
	 * @param externalPricingUnitValue
	 */
	public void setExternalPricingUnitValue(String externalPricingUnitValue, String decimalSeparator, String groupingSeparator);

	public String getPricingUnitValue()/* throws IPCException*/; //length = 5
	
	public String getExternalPricingUnitValue(String decimalSeparator, String groupingSeparator);

	public void setPricingUnitUnit(String pricingUnitUnit);
	
	public String getPricingUnitUnit()/* throws IPCException*/; //length = 3

	public String getPricingUnitDimension()/* throws IPCException*/;

	public void setConditionValue(String conditionValue);

	public String getConditionValue()/* throws IPCException*/; //length = 28,9

	public String getDocumentCurrency()/* throws IPCException*/;

	public String getConversionNumerator()/* throws IPCException*/;

	public String getConversionDenominator()/* throws IPCException*/;

	public String getConversionExponent()/* throws IPCException*/;

	public String getConditionBaseValue()/* throws IPCException*/;

	public String getExchangeRate()/* throws IPCException*/;

	public String getDirectExchangeRate()/* throws IPCException*/;

	public boolean isDirectExchangeRate()/* throws IPCException*/;

	public String getFactor()/* throws IPCException*/; //length = 16,16
	
	public boolean isVariantCondition();

	public String getVariantFactor()/* throws IPCException*/;
	
	public String getVariantConditionKey(); //length = 26 varCondDescription = 50
	
	public String getVariantConditionDescription();

	public boolean isStatistical()/* throws IPCException*/;

	/**
     * Returns the inactive status:
     * A Condition exclusion item
     * G Not applying scale level (in interval scales)
     * K Inactive due to calculation basis/shipping material type
     * L Condition exclusion header or inactive at header level
     * M Inactive due to manual entry
     * T Inactive at header level
     * W The document item is statistical
     * X Inactive Via Formulas or Incorrect
     * Y Inactive because of subsequent price
     * " " (space): condition is active
	 * @return the condition inactive status
	 */
	public char getInactive()/* throws IPCException*/;

	public char getConditionClass()/* throws IPCException*/;

	public char getCalculationType()/* throws IPCException*/;

	public char getConditionCategory()/* throws IPCException*/;

	public char getConditionControl()/* throws IPCException*/;

	public char getConditionOrigin()/* throws IPCException*/;

	public char getScaleType()/* throws IPCException*/;

	public boolean isChangeOfRatesAllowed();

	public boolean isChangeOfPricingUnitsAllowed();

	public boolean isChangeOfValuesAllowed();

	public boolean isDeletionAllowed();

	/**
	 * @param string
	 * @param string2
	 * @return
	 */
	public String getExternalConditionValue(String decimalSeparator, String groupingSeparator);
	
	public String getExternalConditionRateValue(String decimalSeparator, String groupingSeparator);
	
	/*Added for manual condition rate change */
	public void setConditionValue(String conditionValue, String decimalSeparator, String groupingSeparator);

}