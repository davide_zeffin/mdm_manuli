package com.sap.spc.remote.client.object;

import java.util.Hashtable;
import java.util.List;
import java.util.HashMap;

import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.ClientLogListener;
import com.sap.util.monitor.jarm.IMonitor;
//<--

/**
 * Facade interface for IPC clients (UI).
 * @see IPCDocument
 * @see IPCItem
 * @see KnowledgeBase
 */

/*
 * 20020315-kha: internal comment: for 4.0 I've changed my part of the IPC client API to "all or nothing" re-implementation:
 * If you want to subclass the default implemenations in client.object.imp, it's fine, but if you want to use your own
 * implementations instead, you have to do a complete re-implementation. Before I tried to have components that can be
 * reused by themselves. But just like in spc.base this didn't work, simply because you either do "dirty secret hack casts" or
 * you have to add more and more implementation details to the interfaces where they don't belong.
 *
 * Now with the "all or nothing" approach (that is used in most other APIs) I can clean up the APIs for 4.0.
 */
public interface IPCClient {

	//public static final String DEFAULT_CLIENT = "DefaultIPCClient";
	//No use of static IMonitor member in interface. Let the impl classes have a member variable.
	//public static IMonitor _monitor = null;

    public IPCDocument createIPCDocument(IPCDocumentProperties prop)
          throws IPCException;


    /**
     * Returns the configuration for the given documentId and itemId.
     * For IPC scenario.
     */
    public Configuration getConfiguration(String documentid, String itemId);

    /**
    * Returns a reference to the configuration object for the last created
    * document and item.
    * For convenience only.
    * @return Config
    **/
    public Configuration getConfiguration()throws IPCException;

    /**
     * For configuration only mode we address the configuration via its id.
     */
    public Configuration getConfiguration(String configurationId);

	/**
	 * Returns a configuration for a given item reference, ie. attaches to the
	 * server/session/item specified in reference.<p>
	 * <b>Please note</b>: calling getConfiguration on an item reference will
	 * re-initialize the session to use the new server connection. All
	 * documents, items and configurations that have been created up to this
	 * point are invalid after calling this method!
	 */
	public Configuration getConfiguration(IPCItemReference reference)
              throws IPCException;

	/**
	 * Returns a configuration for a given config reference, ie. attaches to the
	 * server/session/config specified in reference.<p>
	 * <b>Please note</b>: calling getConfiguration on an config reference will
	 * re-initialize the session to use the new server connection. All
	 * configurations that have been created up to this point are invalid
	 * after calling this method!
	 */
	public Configuration getConfiguration(IPCConfigReference reference)
              throws IPCException;

    /**
     * Returns the external configuration representation for the current configuration.
     */
    ext_configuration getExternalConfiguration();

    /**
    * Returns a reference to an IPCDocument.
    * @return <{IPCDocument}>
    **/
    public IPCDocument getIPCDocument(String documentId);

    /**
    * Returns a reference to an IPCDocument.
    * @return <{IPCDocument}>
    **/
    public IPCSession getIPCSession();

    /**
     * Returns the existing IPCItem for the given IPCItemReference. For performance reasons,
	 * the client will try to reuse its existing session if the IPCItemReference corresponds
	 * to its session. This can cause problems if in the meantime another application has
	 * gained control over the session and changed the server-side session state. In this case
	 * use getIPCItem(reference, true).
     * @param <{IPCItemReference}>
     * @return <{IPCItem}>
     */
    IPCItem getIPCItem(IPCItemReference reference) throws IPCException;


	/**
     * Returns the existing IPCItem for the given IPCItemReference.
     * @param <{IPCItemReference}>
	 * @param reset set this to true to reset the client session and re-sync the client state
	 * with the server state. This can take a long time, but it is necessary if you are not sure if
	 * another client has changed the server-side session in the meantime.
     * @return <{IPCItem}>
     */
    IPCItem getIPCItem(IPCItemReference reference, boolean reset) throws IPCException;

	/**
     * Returns the existing Configuration for the given IPCConfigReference.
     * @param <{IPCConfigReference}>
	 * @param reset set this to true to reset the client session and re-sync the client state
	 * with the server state. This can take a long time, but it is necessary if you are not sure if
	 * another client has changed the server-side session in the meantime.
     * @return <{Configuration}>
     */
    Configuration getConfiguration(IPCConfigReference reference, boolean reset) throws IPCException;

    /**
     * Creates a new configuration for the given productId, language and kbDate.
     */
    Configuration createConfiguration(String productId, String language, String kbDate) throws IPCException;

    /**
     * Creates a new configuration for the given productId and other
     * context data.
     */
    Configuration createConfiguration(String productId,
										     String language,
                                             String kbDate,
                                             String kbLogSys,
                                             String kbName,
                                             String kbVersion,
                                             String kbProfile,
                                             String[] contextNames,
                                             String[] contextValues)
                throws IPCException;

    /**
     * Creates a new configuration for the given productId and other
     * context data.
     */
    Configuration createConfiguration(  String productId,
										String productType,
										String language,
										String kbDate,
										String kbLogSys,
										String kbName,
										String kbVersion,
										String kbProfile,
										String[] contextNames,
										String[] contextValues)
                throws IPCException;

	/**
	 * Creates a configuration from an external configuration and optional context data.
	 * @param config the external configuration to use. Must not be null.
	 * @param contextNames array of context names, or null to use no context.
	 * @param contextValues array of context values, or null to use no context.
	 * Either contextNames and contextValues are null, or both are not null. If they are not they
	 * must have an equal length.
	 */
	public Configuration createConfiguration(ext_configuration config, String[] contextNames, String[] contextValues)
				throws IPCException;

	/**
	 * Creates a configuration from an external configuration and optional context data.
	 * @param language string, f.e. EN .
	 * @param config the external configuration to use. Must not be null.
	 * @param contextNames array of context names, or null to use no context.
	 * @param contextValues array of context values, or null to use no context.
	 * Either contextNames and contextValues are null, or both are not null. If they are not they
	 * must have an equal length.
	 */
	public Configuration createConfiguration(String language, ext_configuration config, String[] contextNames, String[] contextValues)
				throws IPCException;

	/**
	 * Creates a configuration from an external configuration and optional context data.
	 * @param config the external configuration to use. Must not be null.
	 * @param contextNames array of context names, or null to use no context.
	 * @param contextValues array of context values, or null to use no context.
	 * @param date  date string, formatted YYYYMMDD.
	 * Either contextNames and contextValues are null, or both are not null. If they are not they
	 * must have an equal length.
	 */
	public Configuration createConfiguration(ext_configuration config, String[] contextNames, String[] contextValues, String date)
				throws IPCException;

	/**
	 * Creates a configuration from an external configuration and optional context data.
	 * @param language string, f.e. EN .
	 * @param config the external configuration to use. Must not be null.
	 * @param contextNames array of context names, or null to use no context.
	 * @param contextValues array of context values, or null to use no context.
	 * @param date  date string, formatted YYYYMMDD.
	 * Either contextNames and contextValues are null, or both are not null. If they are not they
	 * must have an equal length.
	 */
	public Configuration createConfiguration(String language, ext_configuration config, String[] contextNames, String[] contextValues, String date)
				throws IPCException;


    /**
     * Creates a configuration from an external configuration and optional context data.
     * @param config the external configuration to use. Must not be null.
     * @param contextNames array of context names, or null to use no context.
     * @param contextValues array of context values, or null to use no context.
     * Either contextNames and contextValues are null, or both are not null. If they are not they
     * must have an equal length.
     */
    public ImportConfiguration createImportConfiguration(ext_configuration config, String[] contextNames, String[] contextValues)
                throws IPCException;

    IPCItem createIPCItem(String documentId, IPCItemProperties props)
              throws IPCException;

    /**
     * Returns the IPCDocuments held by this IPCClient.
     */
    List getIPCDocuments();

	 /**
	  * Returns the item reference of this client. The document and item ids of the
	  * item references are not used, if you get them from the reference, you get a null.
	  */
	 public IPCItemReference getItemReference();

	 /**
	  * Returns the config reference of this client. The config ids of the
	  * config references are not used, if you get them from the reference, you get a null.
	  */
	 public IPCConfigReference getConfigReference();

	 /**
	  * Synchronize this client's state with the server. Since IPC clients may buffer IPC commands
	  * before sending them to the server, you need to call this method whenever you're finished
	  * working with an IPCClient instance and switch to another IPCClient instance. If you don't,
	  * the server's state may be older than this client's state.
	  */
	 public void synchronizeWithServer() throws IPCException;

	/**
	 * Adds a new client log listener to this client. The IPCClient will register this listener
	 * at its internal client object.
	 */
	public void addLogListener(ClientLogListener l);

	/**
	 * Removes a client log listener from this client. Doesn't perform any operation if l
	 * has not previously been added by addLogListener.
	 */
	public void removeLogListener(ClientLogListener l);

    /**
     * Returns a list of all available Knowlegebases
     */
    public List getKnowledgeBases(String kbToFind);

	/**
	 * Returns the UI model guid
	 * @param kb Knowledgebase for which the ui model shoud be found
	 * @param scenario The scenario for which the ui model shoud be found
	 * @param roleKey The role for which the ui model shoud be found
	 * @return the guid of the ui model
	 */
	public String getUIModelGUID(KnowledgeBase kb, String scenario, String roleKey);


	/**
	 * Sets the single activity trace object
	 * @param IMonitor
	 */
	public void setJARMMonitor(IMonitor monitor);

	/**
	 * Returns the single activity trace object
	 * @return IMonitor
	 */
	public IMonitor getJARMMonitor();
	
	public HashMap getVarTable(String documentId, String itemId, String[] cstics, String filterValue);
	
	public HashMap getAllValidGridVariants(String productId, String logsys, String[] csticNames, String[] csticValues);
	
//	public void registerAtBackendContext();
	
	/**
	 * Returns the ConditionGroup attributes bases on usage and condition group name.
	 * @param usage Usage.
	 * @param conditionGroupName Condition Group Name.
	 * @return Condition Group attributes.
	 */
	public String[] getConditionGroupAttributes(String usage, String conditionGroupName)throws IPCException;
	
	/**
	 * Returns the texts for the given fields of the Condition technique field catalogue
	 * bases on usage, application and optional field names.
	 * @param usage Usage.
	 * @param application Application.
	 * @param fieldname Fieldname of a field from the Condition technique field catalogue.
	 * @param dataElementName Name od a DDIC element
	 * @param isApplicationField, flag to swithc between application and usage fields
	 *    true  : fieldnames specify application fields
	 *    flase : fieldnames specify usage fields	  
	 * @return HashTable of String arrays
	 * you can use only fieldName or DataElementName, no mix of both parameters is allowed
	 * If you use DataelementName, all other parameters ( e.g. usage, application .. ) will be ignored
	 *  Fieldname / DataElementName will be the key of the HashTable
	 *  String array will contain the texts for the field
	 *  - extra short text
	 *  - short text
	 *  - medium text
	 *  - long text 
	 */
	public Hashtable getConditionTableFieldLabel(String usage, String application, String[] fieldNames, String[] dataElementNames, boolean isApplicationField )throws IPCException;	

	/**
	 * Returns the external fieldname for a given interna fieldname of the Condition technique field catalogue
	 * based on usage, application and internal field names.
	 * @param usage Usage.
	 * @param application Application.
	 * @param fieldname internal Fieldname of a field from the Condition technique field catalogue.
	 * @param isApplicationField, flag to swithc between application and usage fields
	 *    true  : fieldnames specify application fields
	 *    flase : fieldnames specify usage fields
	 * @return HashTable of String arrays
	 *  Internal Fieldname will be the key of the HashTable
	 *  String array will contain the external field names ( if multiple available )  
	 */
	public Hashtable convertFieldnameInternalToExternal(String usage, String application, String[] intFieldNames, boolean isApplicationField)throws IPCException;	

	
	/**
	 * Returns a HashMap containing Condition Recores from database based on following input.
	 * @param usage
	 * @param application
	 * @param selAttributesInclArray
	 * @param selAttributesInclValueArray
	 * @param selAttributesExclArray
	 * @param selAttributesExclValueArray
	 * @param validityDateStart_yyyyMMddmmhhss
	 * @param validityDateEnd_yyyyMMddmmhhss
	 * @param conditionGroupId
	 * @param numberOfConditionRecords
	 * @return A HashMap containing the string/string[] for each output parameter of GetConditionRecordsFromDatabase command.
	 */
	public HashMap getConditionRecordsFromDatabase(String usage, String application, 
													String[] selAttributesInclArray, String[] selAttributesInclValueArray,
													String[] selAttributesExclArray, String[] selAttributesExclValueArray,
													String validityDateStart_yyyyMMddmmhhss, String validityDateEnd_yyyyMMddmmhhss,
													String conditionGroupId,String numberOfConditionRecords)throws IPCException;
	
	/** 
	 * Retuns no. of condition record selected and should be called after the selection cammand is called. 
	 * @param usage condition technique usage
	 * @return no. of condition records that are selected for given usage
	 * @throws IPCException
	 */
	public int getNumberOfConditionRecordSelected(String usage) throws IPCException;

	
	/**
	 * Returns a HashMap containing Condition Recores from database based on following input.
	 * @param usage
	 * @param application
	 * @param refGuid
	 * @param refType
	 * @param numberOfConditionRecords
	 * @return A HashMap containing the string/string[] for each output parameter of GetConditionRecordsFromRefGuid command.
	 */
	public HashMap getConditionRecordsFromRefGuid(String usage, String application, 
													String refGuid, String refType,
													String numberOfConditionRecords)throws IPCException;
													
	/**
	 * Returns the converted Internal Values array base on External values and names
	 * 
	 * @param names Array on Names for which the conversion is required.
	 * @param externalValues Array of externalValues for which conversion required.
	 * @return HashMap of output parameters.
	 * HashMap will have three key-value paris.
	 *      names_output   -> {N1, N2, N3}
	 *      internalValues -> {V1, V2, V3}
	 *      errorMessages  -> {E1, E2, E3}
	 */
	public HashMap convertExternalToInternal(String[] names, String[] externalValues)throws IPCException;;

	/**
	 * Returns the converted external values array base on internal values and names
	 * 
	 * @param names Array on Names for which the conversion is required.
	 * @param internalValues Array of internalValues for which conversion required.
	 * @return HashMap of output parameters.
	 * HashMap will have two key-value paris.
	 *      externalValues -> {V1, V2, V3}
	 *      errorMessages  -> {E1, E2, E3}
	 */
	public HashMap convertInternalToExternal(String[] names, String[] internalValues)throws IPCException;;
}