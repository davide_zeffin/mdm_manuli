package com.sap.spc.remote.client.object;

import com.sap.spc.remote.client.util.ext_configuration;

/**
 * Snapshot of a configuration.
 * Represents the state of a configuration at specific time.
 */
public interface ConfigurationSnapshot {


    /**
     * Returns a description of the snapshot.
     * It returns an empty string if no description has been maintained.  
     * @return description of the snapshot
     */
    public String getDescription();

    /**
     * Returns the configuration state of the snapshot as <code>ext_configuration</code>
     * object.
     * @return external configuration of the snapshot
     */
    public ext_configuration getConfig();

    /**
     * Indicates whether the snapshot is the initial configuration (without changes done by 
     * the user) or a snapshot taken during the session. If it is the initial configuration 
     * this method returns true.
     * This flag has to be set manually.
     * @return flag indicating whether the snapshot has been taken for the initial configuration.
     */
    public boolean isInitialConfiguration();

    /**
     * Returns the name of the snapshot. It can be defined during creation of the snapshot.
     * It returns an empty string if no description has been maintained.
     * @return the name of the snapshot.
     */
    public String getName();

    /**
     * It returns null if no price has been set.
     * @return the price of the snapshot.
     */
    public String getPrice();

    /**
     * Returns the timestamp of the snapshot.
     * @return timestamp of the snapshot.
     */
    public long getTimestamp();

    /**
     * Adds a description to the snapshot.
     * @param desc description 
     */
    public void setDescription(String desc);

    /**
     * Sets the flag that indicates whether this snapshot represents an initial
     * configuration.
     * @param isInitial flag whether snapshot is taken for initial configuration.
     */
    public void setInitialConfiguration(boolean isInitial);

    /**
     * Adds a name to snapshot.
     * @param name name of the snapshot
     */
    public void setName(String name);

    /**
     * Adds the price of the configuration state for this snapshot.
     * @param price price of the snapshot's configuration state
     */
    public void setPrice(String price);


}
