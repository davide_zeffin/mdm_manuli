package com.sap.spc.remote.client.object;

/**
 * A delta of a value in the configuration comparison.
 */
public interface ValueDelta {

    public static final String DELETED = "D";
    public static final String INSERTED = "I";

    /**
     * @return author of the delta
     */
    public String getAuthor();

    /**
     * Returns the type of the delta. This can be 
     * <ul>
     * <li>D - deleted
     * <li>I - inserted
     * </ul> 
     * @return type of delta
     */
    public String getType();

    /**
     * Returns the key of the configuration object, this value delta belongs to.
     * The key has the format "<instId>.<csticName>".
     * @return unique key of the configuration object to this value delta belongs to. 
     */
    public String getConfigObjectKey();

    /**
     * @return name of the characteristic the value delta belongs to
     */
    public String getCsticName();

    /**
     * @return the value of the value delta
     */
    public String getValue();

    /**
     * @return the unit of the value
     */
    public String getUnit();


}
