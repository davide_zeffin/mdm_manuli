/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

import java.util.Locale;

/**
 * Session interface. This interface can be used to access the documents
 * in an IPC session. Many methods in this interface throw IPCExceptions.
 * These occur when the session considers it necessary to contact the IPC
 * server, and this contact fails.
 */
public interface IPCSession
{
	/**
	 *  @return the id of this session.
	 */
    public  String getSessionId();

	/**
	 * Returns the IPCClient to which this session belongs
	 */
	public  IPCClient getIPCClient();

    /**
	 *  @param
	 *  @return
	 *  @exception
	 */
	public  String[] getAvailablePricingProcedureNames(String s) throws IPCException;

    /**
	 * Creates a new document using the given properties.
	 */
	public  IPCDocument newDocument(IPCDocumentProperties props)
			throws IPCException;

    /**
	 * Retrieves a document by its document ID, or null, if this session doesn't
	 * hold a document with this ID.
	 */
	public  IPCDocument getDocument(String documentId);

    /**
	 * Retrieves a configuration by its configuration ID, or null, if this session doesn't
	 * hold a configuration with this ID.
	 */
	public  Configuration getConfiguration(String configId);

    /**
	 * Returns the description string of a given pricing procedure. Use
	 * getAvailableProcedureNames for a list of procedure names.
	 */
	public  String getPricingProcedureDescription(String procedureName);

    /**
	 * Removes a given document from this session. If the document does not exist
	 * in this session, no operation will be performed. The document will always be
	 * removed from the client's view of the server, even if the actual removal
	 * on the server fails (and an exception is thrown).
	 */
	public  void removeDocument(IPCDocument document) throws IPCException;

    /**
	 * Returns all documents that are available in this session.
	 */
	public IPCDocument[] getDocuments();

    /**
	 * Retrurns all available currency units in three-digit code.
	 */
	public String[] getAvailableCurrencyUnits() throws IPCException;

	/**
	 * Returns the user name under which the IPC server knows us.<p>
	 */
	public String getUserName();

	public String getLanguage();
	/**
	 * Returns the user location at which the IPC server knows us.
	 */
	public String getUserLocation();

	/**
	 * Returns the SAP client this session is using.
	 */
	public String getUserSAPClient();

	/**
	 * Returns the parameter naming convention this session is using.
	 * Typically, this is "BAPI", "BAPI-CRM", "BAPI-R3" or "DEFAULT".
	 */
	public String getParameterNaming();

	/**
	 * updates all properties in props. Provide all information you know by using props[i].set... methods,
	 * call this method, and other properties will be retrieved from the IPC server. After this call
	 * you can access them with the respective get.. methods. For details consult ISOCodeConversionProperties.
	 *
	 * @param props an array of ISOCodeConversionProperties to be completed
	 * @param locale a Java locale, or null, if no locale is known (no localized values will be determined in this case)
	 * @param groupSeparator group separator for value formatting, or null to use the default
	 * @param decimalSeparator decimal separator for value formatting, or null to use the default
	 */
	public void determineUnitsOfMeasurement(ISOCodeConversionProperties[] props,
										    Locale locale, String groupSeparator, String decimalSeparator) throws IPCException;

	/**
	 * updates all properties in props. Provide all information you know by using props[i].set... methods,
	 * call this method, and other properties will be retrieved from the IPC server. After this call
	 * you can access them with the respective get.. methods. For details consult ISOCodeConversionProperties.
	 *
	 * @param props an array of ISOCodeConversionProperties to be completed
	 * @param locale a Java locale, or null, if no locale is known (no localized values will be determined in this case)
	 * @param groupSeparator group separator for value formatting, or null to use the default
	 * @param decimalSeparator decimal separator for value formatting, or null to use the default
	 */
	public void determineCurrencyUnits(ISOCodeConversionProperties[] props,
									   Locale locale, String groupSeparator, String decimalSeparator) throws IPCException;

	public void setExtendedDataEnabled(boolean enabled);

	public boolean isExtendedDataEnabled();

	/**
	 * Closes this session and allow all its data to be garbage collected. Every attempt to access it later will
	 * cause undefined behavior. The session will also be closed on the IPC server, freeing all its data.
	 */
	public void close();
	
	/**
	 * 	Exclusively locks the shared data associated with this session.
	 */
	public void lock();
	
	public boolean isLocked();
	
	/**
	 * Publishes contents of a shared data area for access of another session.
	 * The counterpart of this API is a bind which is executed with the construction of
	 * the session.
	 *
	 */
	public void publish();
	
	/**
	 * Sets Cache as dirty.
	 */
	public void setCacheDirty();
	
	public void syncWithServer();

	/**
	 * 
	 */
	public void unlock();


}
