package com.sap.spc.remote.client.object;


public class IllegalConfigOperationException extends Exception
{
    public IllegalConfigOperationException() {
		super("Illegal config operation");
    }
}