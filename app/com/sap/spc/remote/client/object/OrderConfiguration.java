package com.sap.spc.remote.client.object;

import java.util.Map;

import com.sap.spc.remote.client.util.ext_configuration;

/**
 * An OrderConfiguration is an exteral configuration that knows additional data from an order.
 * Most important, this is the position-GUID-to-instance-id mapping table that is used to
 * synchronize order BOM and SCE BOM.
 * @deprecated
 */
public class OrderConfiguration {

	private ext_configuration config;
	private Map               posToInstId;
	private String            date;

	/**
	 * Creates a new order configuration.
	 * @param   cfg             the external configuration to use
	 * @param   posIdToInstId   table that maps Strings to Strings (order item GUIDs to inst ids)
	 * @param   date            target date for kb finding, YYYYMMDD.
	 */
    public OrderConfiguration(ext_configuration cfg, Map posIdToInstId, String date) {
		this.config = cfg;
		this.posToInstId = posIdToInstId;
		this.date = date;
    }

	public ext_configuration getConfiguration() {
		return config;
	}

	public Map getPosIdToInstIdMap() {
		return posToInstId;
	}

	public String getDate() {
		return date;
	}
}