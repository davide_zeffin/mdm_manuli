package com.sap.spc.remote.client.object;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Client view of TTE Document.
 */
public interface TTEDocument {

    
    /**
     * Changes the properties of the TTE document using the passed header attributes.
     * Overwrites all attributes and sends the changes to AP-TTE.
     * @param HashMap attributes
     */
    public void changeDocument(HashMap documentHeaderAttributes) throws IPCException;

    /**
     * Removes the TTE items from the TTE document on engine-side (AP-TTE) and from the 
     * TTE document on client-side.<br>
     * Attention: This method expects a list of IPCItems (not TTEItems). (Each IPCItem has 
     * a reference to its TTEItem)
     * @param ipcItems list of IPCItems (!) for which the TTEItems should be deleted in AP-TTE and locally.
     */
    public void removeItemsOnServer(List ipcItems) throws IPCException;
    
    /**
     * Removes the TTE item from the document. It does not delete it from the server.
     * For item deletion from server use @see TTEDocument#removeItemsOnServer(IPCItem[])
     * @param tteItem TTEItem which should be deleted
     */
    public void removeItem(TTEItem tteItem);

    /**
     * @return the id of the TTE document.
     */
    public String getId(); 

    /**
     * @return a list of business partners of the document.
     */
    public ArrayList getBusinessPartners();

    /**
     * @return the business transaction of the document.
     */
    public String getBusinessTransaction();

    /**
     * Changes the currency unit of the document and sends the new unit to AP-TTE.
     * @param currencyUnit the currency unit of the document.
     */
    public void changeDocumentCurrencyUnit(String currencyUnit) throws IPCException; 

    /**
     * @return the currency unit of the document.
     */
    public String getDocumentCurrencyUnit();

    /**
     * @return the language.
     */
    public String getLanguage();

    /**
     * @return the business partner id of the document. 
     */
    public String getOwnBusinessPartnerId();

    /**
     * @return the processing mode.
     */
    public String getProcessingMode();

    /**
     * @return the system id 1 (TTE-Logsys).
     */
    public String getSystemId1(); 

    /**
     * @return true if text output.
     */
    public boolean isTextOutput(); 

    /**
     * @return the trace mode.
     */
    public String getTraceMode(); 

    /**
     * @param traceMode
     */
    public void setTraceMode(String traceMode); 


    /**
     * @return list of  TTE items.
     */
    public ArrayList getTteItems();
    
    /**
     * Adds a TTEItem to the list of items (does not add the TTEItem on the server!).
     * Use @see TTEDocument#modifyItemsOnServer() for this (this will send only one
     * call for all items to the server instead of one call per item):
     * @param tteItem TTEItem to be added
     */
    public void addItem(TTEItem tteItem);
    
    /**
     * Modifies the TTE document on the server (AP-TTE). It sends the document data and
     * the list of current TTEItems to the server. If an item does not exist on 
     * the server it is created if it is already existing it modifies the item data. If
     * the document at all is not existing it creates it.<br>
     * This method does not remove items on the server. Use @see TTEDocument#removeItems(TTEItem[])
     * for this.
     */
    public void modifyDocumentOnServer() throws IPCException; 
    
    /**
     * Add a TTE product to the document. Only one product per id is allowed.
     * If a product with the same id already exists, it is overwritten with 
     * the new product.<br>
     * This method does not add the product on the server.
     * To modify the document also on the server call 
     * @see TTEDocument#modifyDocumentOnServer()
     * @param tteProduct product to be added
     */
    public void addProduct(TTEProduct tteProduct);
    
    /**
     * @param productId
     * @return true if document contains TTE product with given id
     */
    public boolean containsProduct(String productId);
    
    /**
     * @return list of TTE products
     */
    public ArrayList getProducts();
    
    /**
     * @return HashMap with products by id
     */
    public HashMap getProductsById();
    
    /**
     * Removes product from document (but not on the server).
     * To modify the document also on the server call 
     * @see TTEDocument#modifyDocumentOnServer()
     * @param productId product to be removed from document
     */
    public void removeProduct(String productId);

    /**
     * @return An array with the TTE input and output document as XML.
     * @throws IPCException
     */
    public String[] getTTEAnalysisDataXml() throws IPCException;
    
    public String[] getTTEAnalysisData() throws IPCException;

}
