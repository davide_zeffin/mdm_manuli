/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;


import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Client view of an IPC document. The client view will always be in sync with
 * the server view, ie. you will never need to call pricing or whatever after
 * you change the document in any way. The client will make sure that it doesn't
 * call the server's pricing commands too often (I hope <tt>;-)</tt>).<p>
 *
 * For optimal performance you might want to be careful about when you call getter (!)
 * methods of the document. For example, if you call pricing ten times but retrieve the
 * netValue only after the tenth time, the document will "optimize away" the first
 * nine redundant pricing calls [noch nicht, Jungs, der Kommandooptimierer wird
 * erst im M�rz kommen. Sorry]. If you call pricing(); getNetValue(); every getNetValue
 * will force the client to synchronize its state with the server.<br>
 * So please, keep in mind that the Client tries to delay everything as long as possible.
 * Only once you access some data, the required state changing commands in the server
 * will actually be executed.<p>
 */
public  interface IPCDocument extends Closeable
{

    /** Returns the document id (GUID) of this document. Not actually needed anywhere
	 *  in this application, but may be useful to communicate with external applications.
	 */
    public  String getId();

	/**
	 * Performs a pricing on this document. If group condition processing is activated in this
	 * document. You should never need to call this method.
	 */
	public  void pricing() throws IPCException;

    /** Returns the document country
     */
	public  String getDocumentCountry() throws IPCException;

    /**  Returns the item with a given item id, or null, if the item can not be found in this document.
     *   @param			itemId  the item id (GUID)
     *	 @return        the corresponding item or null.
     */
	public  IPCItem getItem(String itemId) throws IPCException;

    /** Creates a new item in this document.<p>
	 *  @return the new item.
     */
	public IPCItem newItem(IPCItemProperties properties)
		throws IPCException;


	/**
	 * Creates several items.
	 */
	public IPCItem[] newItems(IPCItemProperties[] properties)
		throws IPCException;


	/**
	 * Adds an existing item to this document. This method is used internally only. To
	 * implement your own document you need to implement it, but you should not use it.<p>
	 */
	public void addItem(IPCItem item);


    /** Returns the session in which the document is connected
      *	@return		  document's UI Session
      */
	public  IPCSession getSession();

    /** Returns the gross value of the document
     *  @return		String		Gross value
     */
	public  String getGrossValue() throws IPCException;

    /** Returns the tax value of the document
     *  @return		String		taxValue
     */
	public  String getTaxValue() throws IPCException;

	/** Return the document language
	 *  @return		String		documentLanguage
	 */
    public  String getDocumentLanguage() throws IPCException;

	/** Retrieves the header attributes of the document
	 *  @return		String		Array of header attributes name value pair
	 */
	public  String[] getHeaderAttributeEnvironment() throws IPCException;

    /**
	 * Sets a header attribute of this document.
	 * @param   keys the attribute names
	 * @param   values the attribute values. null and "" are special, legal values of each array element.
     */
	public  void setHeaderAttributeBindings(String[] keys, String[] values) throws IPCException;

	/** Retrieves the external id of the document
	 *  @return		String representing the external id of the document
	 */
	public  String getExternalId() throws IPCException;

	/** Retrieves the edit mode of the document ('A' means Read/Write; 'B' means Read only)
	 *  @return		the edit mode of the document
	 */
	public  char getEditMode() throws IPCException;

     /** Changes the document currency of the document
      *  @param		ICurrencyUnit		newDocumentCurrency
      */
	public  void changeDocumentCurrency(String currencyunit) throws IPCException;

    /** Retrieves the locale(country,language) of the document
     *  @return		Locale of document
     */
	public  Locale getLocale() throws IPCException;

    /** Retrieves document currency
     *  @return		DocumentCurrency
     */
	public  String getDocumentCurrency() throws IPCException;


    /** Returns all the root items of this document
     *  @return		array of ISPCItems
     */
	public  IPCItem[] getItems();

	/**
	 * Returns all items of this document, root items as well as sub-items.
	 * The order in which the items are returned is depth-first with the parent before
	 * the children, ie. if item A has children B and C, and item B has children D and E,
	 * the items returned are A, B, D, E, C.
	 */
	public  IPCItem[] getAllItems() throws IPCException;


	/**
	 * Returns all root items that meet a filter criteria.
	 * The api is used by the catalog to limit the number
	 * of product variants according to the catalogs current
	 * view.
	 * A configurable product might have variants in two different
	 * views.
	 */
	 public IPCItem[] getItems(IPCItemFilter ipcItemFilter);

    /**
	 * Returns the headerAttributeValue attached to attribute name 's'.
	 * Please note that this method has run time O(n), where n is the number
	 * of keys. If you want to do repeated accesses on header attributes,
	 * it is better to use getHeaderAttributes and work on the resulting
	 * Map.
	 *
     *  @param		String s 	headerAttributeName
     *  @return		String		headerAttributeValue
     */
	public  String getHeaderAttributeBinding(String s) throws IPCException;

	/**
	 * Returns a Map that maps header attribute keys to header
	 * attribute values. The Map will be freshly created so it will
	 * not be updated automatically if the server-side state changes.
	 */
	public Map getHeaderAttributes() throws IPCException;

    /** Returns the DocumentAttributes of the document
     */
	public  IPCDocumentProperties getDocumentProperties();

    /** Returns the Net Value of the document
     *  @return		String	netValue
     */
	public  String getNetValue() throws IPCException;

    /** Returns the freight of the document
     *  @return		String	freight
     */
    public  String getFreight() throws IPCException;

    /** Returns the Net Value without freight of the document.<p>
     * Please note that the default implementation of this method will require an additional remote
     * call. Use with care.
     *  @return		String	netValueWithoutFreight
     */
	public  String getNetValueWithoutFreight() throws IPCException;

	/** Removes the item from the document. 
	 *  @param		item		item to be removed
	 */
	public  void removeItem(IPCItem item) throws IPCException;

    /** Removes the item from the document. If the item is not part of this document, no operation is performed.
     * @param item item to be removed
     * @return true if item was existing in the document
     */
    public boolean removeItemExisting(IPCItem item) throws IPCException;

	/**
	 * Removes a number of items from the document in a single step. This method is typically
	 * faster than repeatedly calling removeItem for the items.
	 */
	public  void removeItems(IPCItem[] items) throws IPCException;

	/**
	 * Removes all items of this document. This method is typically faster than all the others
	 * when removing all items.
	 */
	public  void removeAllItems() throws IPCException;


	/** Changes the local currency of the document
	 *  @param		ICurrencyUnit		newDocumentCurrency
	 */
	public void changeLocalCurrency(String localCurrency) throws IPCException;

	/** Changes the pricing procedure of the document.
	 *  @param			String		pricingProcedure to be used henceforth
	*/
	public void changePricingProcedure(String pricingProcedure) throws IPCException;

	/** Method to change the salesOrganisation of the document
	 *  @param		String  new SalesOrganisation
	*/
	public void changeSalesOrganisation(String salesOrganisation) throws IPCException;

	/** Method to change the distribution channel of the document.
	 *  @param			String		new distributionChannel
	*/
	public void changeDistributionChannel(String distributionChannel)throws IPCException;

	/** Returns the pricing procedure of the document
	 *  @return			String		PricingProcedure
	 *
	 */
	public  String getPricingProcedure() throws IPCException;

	/** Returns local currency of the document
	 *  @return			String		localCurrency
 	 */
	public  String  getLocalCurrency() throws IPCException;

	/** Returns the language of the document
	 *  @return			String		language
	 */
	public  String  getLanguage() throws IPCException;

	/** Returns the country of the document. Retreives from locale.
	 *  @return			String		country
 	 */
	public  String  getCountry() throws IPCException;


	/**
	 * Sets if this document uses values that are formatted in the document locale.
	 * All prices and all unit names are formatted depending on this setting.<p>
	 * Since this command may force retrieving a complete update of the whole document
	 * including all its items from the server, it should be used with care.
	 */
	public  void setValueFormatting(boolean valueFormatting) throws IPCException;

	/**
	 * Returns if this document uses values that are formatted in the document locale.
	 */
	public  boolean isValueFormatting();


	/**
	 * Changes group condition processing of this document. <p>NOT IMPLEMENTED!<p>
	 * Currently, the IPC command set does not support this method. It's there for
	 * future implementation.
	 */
	public void setGroupConditionProcessingEnabled(boolean groupConditionProcessing);

	/**
	 * Returns if this document has been created using group condition processing.
	 */
	public boolean isGroupConditionProcessingEnabled();

	/** Returns the header pricing conditions of this document.
	 * @return      set of pricing conditions.
	 */
	public  IPCPricingConditionSet getPricingConditions() throws IPCException;

	/**
	 * Retrieves the most commonly used properties of this document from the server
	 * and synchronizes the corresponding properties of this document and its items
	 * with them.<p>
	 *
	 * Calling this method whenever you need to retrieve information about many or
	 * all items in this document will improve performance.<p>
	 *
	 * The method itself doesn't need or return any parameter, it just changes the
	 * internal state of this document and all of its items.<p>
	 *
	 * <b>Technical background:</b>
	 * This method should be used if the calling application wants to know information
	 * about most of the items of a document. Normally, the information about an item
	 * is retrieved as late as possible. This minimizes the amount of data transferred
	 * between server and client. Now if all items are accessed one by one, this
	 * causes a lot of individual requests to be sent across the network, which is much
	 * less efficient than sending a request "give me all item data" and getting a single,
	 * big response about them. Since there's no way to know ahead automatically if more
	 * requests about other items will follow, this explicit call is necessary.<p>
	 *
	 * The properties that are synced by this call are all in GetDocumentAndItemInfo:
	 * grossValue/taxValue/netValue of document and all items, all sales quantities,
	 * product ids/guids/descriptions, configuration state (configurable? complete?
	 * consistent? changed?).
	 */
	public void syncCommonProperties() throws IPCException;
	
	public String[] getTTEAnalysisData() throws IPCException;
	
	/**
	 * Invalidates the cache in order to execute a synchWithServer with the next get call.
	 *
	 */
	public void setCacheDirty();

	/**
	 * @param string
	 * @return
	 */
	public IPCDocumentProperties getPropertiesFromServer();

    /**
     * Returns the document properties and all ItemProperties of the current items.
     * The object returned is a HashMap with two entries:
     * <li>DefaultIPCDocumentProperties.PROPERPTIES: DefaultIPCDocumentProperties object
     * <li>DefaultIPCItemProperties.PROPERTIES: Array of DefaultIPCItemProperties
     * @return HashMap containing document properties and item properties.
     */
    public HashMap getDocAndItemPropertiesFromServer();

	
    /**
     * @return TTE document of this IPC document (if available)
     */
    public TTEDocument getTteDocument();
    
    public void setTteDocument(TTEDocument tteDoc);	
}
