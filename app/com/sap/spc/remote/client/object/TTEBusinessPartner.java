package com.sap.spc.remote.client.object;

import java.util.ArrayList;

/**
 * Client of a TTE business partner.
 */
public interface TTEBusinessPartner {

    /**
     * @return the legal entity the business partner belongs to
     */
    public String getBelongsToLegEnt(); 

    /**
     * @return business partner id
     */
    public String getBusinessPartnerId();

    /**
     * @return city
     */
    public String getCity();

    /**
     * @return country
     */
    public String getCountry();

    /**
     * @return county
     */
    public String getCounty();

    /**
     * @return region that is exempted
     */
    public String getExemptedRegion();

    /**
     * @return geo code
     */
    public String getGeoCode();

    /**
     * @return zip code
     */
    public String getPostalCode(); 

    /**
     * @return region
     */
    public String getRegion();

    /**
     * @return list of tax groups of the business partner
     */
    public ArrayList getTaxGroups();

    /**
     * @return taxability
     */
    public String getTaxability();

    /**
     * @return jurisdiction code
     */
    public String getJurisdictionCode();


    /**
     * @return list of tax numbers of the business partner
     */
    public ArrayList getTaxNumbers();

    /**
     * @return list of tax number types of the business partner
     */
    public ArrayList getTaxNumberTypes();

    /**
     * @return  list of tax types of the business partner
     */
    public ArrayList getTaxTypes(); 


}
