/*
 * Created on 09.08.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface IPCBOManagerBase {

	/**
	 * Creates an IPCClient as member if not yet initialized and returns that IPCClient.
	 * Implementations should not add functionality, since the method will be duplicated for each BOManager in an application.
	 * This API assumes that the backend RFC (JCO) connection is already initialized with the correct language of the current
	 * user session.
	 **/
	 public IPCClient createIPCClient();

	 /**
	  * Returns the IPCClient for the given connection key if it exists or creates one,
	  * if it does not exist.
	  */
	 public IPCClient createIPCClientUsingConnection(String connectionKey);
	    
}
