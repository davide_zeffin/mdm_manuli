package com.sap.spc.remote.client.object;

import java.util.Map;

import com.sap.spc.remote.client.util.ext_configuration;

/**
 * The Properties of an IPCItem. Use the methods in this interface to set up the initial
 * properties of your items for IPCDocument.newItem(s).
 * 
 * As of IPC 5.0, the item properties have been split up into an interface and an
 * implementation. This way many methods in the imp package that needed to be public
 * only because they are used by this class can be made package private; and it
 * is consistent with the other interface/implementation classes.
 * 
 * Note that these methos are to set up the initial properties object but not to change an existing items properties.
 * For changing the existing item properties, use the IPCItem interface methods but not properties interface methods.
 * 
 * Note that the get methods of this interface are for client object layer. Instead you can use the methods on item.
 *  
 */
public interface IPCItemProperties
{
	/**
	 * Sets the product guid of this item. If a productId has been set before, it will be reset to null.
	 */
	public void setProductGuid(String guid);

	public String getProductGuid() throws IPCException;

	/**
	 * Sets the product id of this item. If a product guid has been set before, it will be reset to null.
	 */
	public void setProductId(String productId);

	public String getProductId() throws IPCException;

	/**
	 * Sets the Header attributes of this item (overwrites the header attributes that it inherits
	 * from its document).
	 * @param   attributes  a Hashtable mapping attribute names to attribute values (String -> String)
	 * (this parameter will be cloned, changing the table you pass to this method later will not
	 * affect the item.).
	 */
	public void setHeaderAttributes(Map attributes);

	public Map getHeaderAttributes() throws IPCException;
	
	/**
	 * Timestamp parameter like PRICE_DATE need to be passed explicitly.
	 * @param timestamps
	 */
	public void setConditionTimestamps(Map timestamps);
	
	public Map getConditionTimestamps();

	/**
	 * Sets the item attributes of this item.
	 * @param   attributes  a Hashtable mapping attribute names to attribute values (String -> String).
	 * (this parameter will be cloned, changing the table you pass to this method later will not
	 * affect the item.).
	 */
	public void setItemAttributes(Map attributes);

	public Map getItemAttributes() throws IPCException;

	/**
	 * Sets the target date for this item. This API currently uses a very simplified date model. All
	 * IPC dates (kbDate, pricingDate, billingDate, ...) are set to one single date.
	 */
	public void setDate(String date);

	public String getDate() throws IPCException;

	/**
	 * Changes this item's being statistical.
	 */
    public void setStatistical(boolean newStatistical);

	public boolean isStatistical() throws IPCException;


	/**
	 * Sets the base (internal representation) quantity.
	 * Do not pass user entered value into this API. They might be formatted in a wrong way.
	 * Pass only values that you are getting from getBaseQuantity();
	 * @param newBaseQuantity
	 * @throws IPCException
	 */
	public void setBaseQuantity(DimensionalValue newBaseQuantity) throws IPCException;
	
	/**
	 * 
	 * @return The base quantity (internal representation).
	 */
	public DimensionalValue getBaseQuantity();
    /**
	 * Sets the sales (external representation) quantity of the item where this property object is associated to.
	 */
    public void setSalesQuantity(DimensionalValue newSalesQuantity) throws IPCException;

    /**
     * 
     * @return The sales quantity (external representation).
     * @throws IPCException
     */
    public DimensionalValue getSalesQuantity() throws IPCException;

    /**
	 * Sets the net weight. Please note that it is currently not supported by
	 * the IPC command set to set net and gross weight to weights of different units.
	 * If you call setNetWeight and setGrossWeight with different units,
	 * newItem(s) will throw an IPCException.
	 */
    public void setNetWeight(DimensionalValue newNetWeight) throws IPCException;

	public DimensionalValue getNetWeight() throws IPCException;


	/**
	 * Specifies if the item of these properties is a return.
	 */
	public void setItemReturn(boolean newItemReturn);

	public boolean isItemReturn() throws IPCException;


	/**
	 * Sets the volume of these item properties.
	 */
    public void setVolume(DimensionalValue newVolume) throws IPCException;

    public DimensionalValue getVolume() throws IPCException;

	/**
	 * Sets the configuration of these item properties.
	 */
	public void setConfig(ext_configuration config);

	public ext_configuration getConfig(String client) throws IPCException;

    /**
     * Sets explicitely the initial configuration.
     */
    public void setInitialConfiguration(ext_configuration config);

    public ext_configuration getInitialConfiguration();

	/**
	 * Sets the knowledgebase information kbname
	 */
	public void setKbName(String kbName);
	public String getKbName() throws IPCException;

	/**
	 * Sets the knowledgebase information kbversion
	 */
	public void setKbVersion(String kbVersion);
	public String getKbVersion() throws IPCException;

	/**
	 * Sets the knowledgebase information kbprofile
	 */
	public void setKbProfile(String kbProfile);
	public String getKbProfile() throws IPCException;

	/**
	 * Sets the knowledgebase information kblogsys
	 */
	public void setKbLogSys(String kbLogSys);
	public String getKbLogSys() throws IPCException;

	/**
	 * Sets the configuration context parameters of these item properties to
	 * a copy of the Hashtable passed to this method.
	 */
	public void setContext(Map context);

	/**
	 * Returns a copy of the context hashtable.
	 */
	public Map getContext() throws IPCException;

    /**
	 * Returns true if pricing analysis should be performed on this item.
     * @deprecated flag is set on document level
	 * @return		performPricingAnalysis indicator
	 */
	public  boolean getPerformPricingAnalysis() throws IPCException;

    /**
	 * Sets if the item should perform pricing analysis.
     * @deprecated this flag has to be set on document level
	 * @param		performPricingAnalysis	indicator
	 */
	public  void setPerformPricingAnalysis(boolean flag);

	/**
	 * Returns the item ids for all (direct) subitems of this item.
	 */
	public  String[] getSubItemIds() throws IPCException;

	/**
	 * Sets the type of the exchange rate to employed.
	 */
	public  void setExchangeRateType(String exchangeRateType) throws IPCException;

	/**
	 * Returns the exchange rate type that has been last set by setExchangeRateType,
	 * or null, if the exchange rate has not been set.
	 */
	public  String getExchangeRateType();

	/**
	 * Sets the date the exchange rate employed must be valid (format: YYYYMMDD)
	 */
	public  void setExchangeRateDate(String exchangeRateDate) throws IPCException;

	/**
	 * Returns the exchange rate date that has been last set by setExchangeRateDate,
	 * or null, if the exchange rate date has not been set.
	 */
	public  String getExchangeRateDate();
	
	/**
     * @return external id of the item
     */
    public String getExternalId();
    
    public void setExternalId(String extId);
	
	/**
	 * Sets the high level item id of the parent item)
	 */
	public  void setHighLevelItemId(String highLevelItemId) throws IPCException;

	/**
	 * Returns the item id of the parent item
	 */
	public  String getHighLevelItemId();

	/**
	 * Adds a parameter=name setting to these properties that will be passed without any interpretation to
	 * the IPC command that creates the sales document.
	 */
	public void addCreationCommandParameter(String name, String value);
    
	/**
	 * 
	 * @return Is the item relevant for price determination.
	 */
	public Boolean isPricingRelevant();
	
	/**
	 * Mark the item as relevant for price determination.
	 * @param pricingRelevant
	 */
	public void setPricingRelevant(Boolean pricingRelevant);
	
	/**
	 * Sets the Product Type (Used to determined productGUID)
	 */
	public void setProductType(String productType);
	
	/**
	 * 
	 * @return Is the product Type.
	 */
	public String getProductType() throws IPCException;
    
	/**
	 * Sets the Product Type (Used to determined productGUID)
	 */
	public void setProductLogSys(String productLogSys);
	
	/**
	 * 
	 * @return Is the logical system of a product.
	 */
	public String getProductLogSys() throws IPCException;
    
    /**
     * The decoupleSubitems-flag indicates whether the subitems should be decoupled.
     * This is used in the AFS-scenario (grid-products).
     * @return true if the "decoupleSubitems"-Flag is set
     */
    public boolean isDecoupleSubitems();
    
    /**
     * The decoupleSubitems-flag indicates whether the subitems should be decoupled.
     * This is used in the AFS-scenario (grid-products).
     * If it is set to true the quantities of this item's subitems are not updated. 
     * @param decouple: true if subitems should be decoupled.
     */
    public void setDecoupleSubitems(boolean decouple);
    
	/**
	 * Product is a changeable variant.
	 */
	public boolean isChangeableProductVariant() throws IPCException;

	/**
	 * Set property: Changeable product variant. This attribute cannot be read from
	 * master data but has to be passed by IPC callers.
	 */
	public void setChangeableProductVariant(boolean changeableProductVariant) throws IPCException ;    
    
	/**
	 * @return
	 */
	public String getItemId();

	/**
	 * @param string
	 */
	public void setItemId(String string);
}