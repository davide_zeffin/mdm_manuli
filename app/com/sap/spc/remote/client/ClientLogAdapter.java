package com.sap.spc.remote.client;

/**
 * ClientLogAdapter is the usual dummy implmentation of its corresponding listener. No method
 * performs any operation.
 */
public class ClientLogAdapter implements ClientLogListener {

    public ClientLogAdapter() {
    }

    public void activated(ClientLogEvent event) {
    }
    public void requestSent(ClientLogEvent event) {
    }
    public void responseReceived(ClientLogEvent event) {
    }
    public void closed(ClientLogEvent event) {
    }
    public void exceptionThrown(ClientLogEvent event) {
    }
}
