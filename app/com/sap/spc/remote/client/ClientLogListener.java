package com.sap.spc.remote.client;

/**
 * A ClientLogListener can register at a Client object. Once it has done this it
 * will receive events whenever the client has performed some important step.
 */
public interface ClientLogListener {

	/**
	 * Called when the client was activated, immediately before it starts
	 * communication with the server over the network connection. Message
	 * contains information about the request the client was activated with,
	 * exception is null.
	 */
	public void activated(ClientLogEvent event);

	/**
	 * Called when the client has finished sending a request to the server
	 * and is waiting for a response. Message is null, exception is null.
	 */
	public void requestSent(ClientLogEvent event);

	/**
	 * Called when the client has received a complete response from the server.
	 * Message is the response string, exception is null.
	 */
	public void responseReceived(ClientLogEvent event);

	/**
	 * Called when the client has been closed. Message is null, exception is null.
	 */
	public void closed(ClientLogEvent event);

	/**
	 * Called when a communication error (or another server error) has occured.
	 * Message contains information about the error (typically the exception's
	 * message). Exception contains the exception that was thrown.
	 */
	public void exceptionThrown(ClientLogEvent event);
}