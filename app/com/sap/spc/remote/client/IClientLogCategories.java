/*
 * Created on Oct 28, 2004
 *
 */
package com.sap.spc.remote.client;

/**
 * Contains all string constants for categories in the MSA client
 */
public interface IClientLogCategories {
	public static String IPC_CLIENT_CATEGORY = "/IPCClient";
}
