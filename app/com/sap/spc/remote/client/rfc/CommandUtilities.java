/*
 * Created on 13.05.2005
 *
 */
package com.sap.spc.remote.client.rfc;

import java.io.UnsupportedEncodingException;

import com.sap.tc.logging.Location;

/**
 * @author 
 *
 */
public class CommandUtilities {
	
	protected static Location location = Location.getLocation(CommandUtilities.class); 


	public static final byte[] convertString2HEXUTF16BE(String value){
		location.entering("SCECommand.convertString2HEXUTF16BE(String)");
		location.debugT("value = " + value);
		String ENCODING_UTF16BE = "UTF-16BE";
		String val = value;
		if (val == null){
			val = "";
		}
		try{
			location.exiting("SCECommand.convertString2HEXUTF16BE(String)");
			return val.getBytes(ENCODING_UTF16BE);
		}catch(UnsupportedEncodingException e) {
			throw new RuntimeException("Converting to byte array failed " +
									"because encoding UTF-16BE is not installed."+
									" Please update your Java VM");
		}
	}

}
