package com.sap.spc.remote.client.rfc.function;

public interface CrmPmeUimodelDetermineRfc {

	public static String IV_KBNAME = "IV_KBNAME";
	public static String IV_KBVERSION = "IV_KBVERSION";
	public static String IV_KBLOGSYS = "IV_KBLOGSYS";
	public static String IV_SCENARIO = "IV_SCENARIO";
	public static String IV_ROLEKEY = "IV_ROLEKEY";
	public static String ES_UIMODEL = "ES_UIMODEL";
	public static String UIMODELID = "UIMODELID"; 
	public static String UIMODEL_GUID = "UIMODEL_GUID";
	public static String DESCRIPTION = "DESCRIPTION";
	public static String INITIAL_GUID = "00000000000000000000000000000000";

}
