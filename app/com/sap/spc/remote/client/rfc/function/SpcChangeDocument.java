package com.sap.spc.remote.client.rfc.function; 

public interface SpcChangeDocument {

    public static String TYPE     = "TYPE";
    public static String RUNTIME     = "RUNTIME";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String BUSINESS_OBJECT_TYPE     = "BUSINESS_OBJECT_TYPE";
    public static String IV_SUPPRESS_PRICING     = "IV_SUPPRESS_PRICING";
    public static String NUMBER     = "NUMBER";
    public static String PERFORM_TRACE     = "PERFORM_TRACE";
    public static String EDIT_MODE     = "EDIT_MODE";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String VALUES     = "VALUES";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String DOCUMENT_ID     = "DOCUMENT_ID";
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String IS_DOCUMENT     = "IS_DOCUMENT";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String AUTHORITY_DISPLAY     = "AUTHORITY_DISPLAY";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String LOG_NO     = "LOG_NO";
    public static String PROCEDUR     = "PROCEDUR";
    public static String IV_SUPPRESS_RATE_DET     = "IV_SUPPRESS_RATE_DET";
    public static String LOCAL_CURRENCY_UNIT     = "LOCAL_CURRENCY_UNIT";
    public static String FIELDNAME     = "FIELDNAME";
    public static String SYSTEM     = "SYSTEM";
    public static String PARENT_ID     = "PARENT_ID";
    public static String PARAMETER     = "PARAMETER";
    public static String EXPIRING_CHECK_DATE     = "EXPIRING_CHECK_DATE";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String ATTRIBUTES     = "ATTRIBUTES";
    public static String MESSAGES     = "MESSAGES";
    public static String DATE_FORMAT     = "DATE_FORMAT";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String AUTHORITY_EDIT     = "AUTHORITY_EDIT";
    public static String ROW     = "ROW";
    public static String ID     = "ID";
    public static String LINE_ID     = "LINE_ID";
    public static String DOCUMENT_CURRENCY_UNIT     = "DOCUMENT_CURRENCY_UNIT";

}
