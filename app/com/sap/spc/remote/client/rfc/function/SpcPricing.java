package com.sap.spc.remote.client.rfc.function; 

public interface SpcPricing {

    public static String TYPE     = "TYPE";
    public static String RUNTIME     = "RUNTIME";
    public static String IV_DOCUMENT_ID     = "IV_DOCUMENT_ID";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String IV_PRICING_TYPE     = "IV_PRICING_TYPE";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String NUMBER     = "NUMBER";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String LOG_NO     = "LOG_NO";
    public static String IT_ITEM_ID     = "IT_ITEM_ID";
    public static String SYSTEM     = "SYSTEM";
    public static String PARENT_ID     = "PARENT_ID";
    public static String PARAMETER     = "PARAMETER";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String MESSAGES     = "MESSAGES";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String IV_PRICING_COMPLETE     = "IV_PRICING_COMPLETE";
    public static String ROW     = "ROW";
    public static String LINE_ID     = "LINE_ID";
    public static String ID     = "ID";

}
