package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiResetConfig {

    public static String CONFIG_ID     = "CONFIG_ID";
    public static String INST_ID     = "INST_ID";

}
