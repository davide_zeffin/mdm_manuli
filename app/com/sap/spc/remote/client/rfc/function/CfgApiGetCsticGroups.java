package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetCsticGroups {

    public static String GROUP_CONSISTENT     = "GROUP_CONSISTENT";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String CSTIC_GROUPS     = "CSTIC_GROUPS";
    public static String GROUP_NAME     = "GROUP_NAME";
    public static String GROUP_DESCRIPTION = "GROUP_DESCRIPTION";
    public static String GROUP_ID       = "GROUP_ID"; //language independend id
    public static String GROUP_REQUIRED     = "GROUP_REQUIRED";
    public static String INST_ID     = "INST_ID";

}
