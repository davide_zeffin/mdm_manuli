package com.sap.spc.remote.client.rfc.function; 

public interface SpcGetDocumentAttrInfo {

    public static String IV_DOCUMENT_ID     = "IV_DOCUMENT_ID";
    public static String AUTHOR     = "AUTHOR";
    public static String DATE_TO_S     = "DATE_TO_S";
    public static String ALT_COND_CURRENCY     = "ALT_COND_CURRENCY";
    public static String NUMBER     = "NUMBER";
    public static String VOLUM     = "VOLUM";
    public static String VARCOND_KEY     = "VARCOND_KEY";
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String ITEM_ID_EXT     = "ITEM_ID_EXT";
    public static String TIMESTAMPS     = "TIMESTAMPS";
    public static String REMOVE_EXT_COND     = "REMOVE_EXT_COND";
    public static String POINTS_UNIT     = "POINTS_UNIT";
    public static String PARTIAL_PROCESSING     = "PARTIAL_PROCESSING";
    public static String SCALE_TYPE     = "SCALE_TYPE";
    public static String HEADER_COUNTER     = "HEADER_COUNTER";
    public static String MONTHS_B     = "MONTHS_B";
    public static String NET_VALUE_ORIG     = "NET_VALUE_ORIG";
    public static String LOG_NO     = "LOG_NO";
    public static String PURPOSE     = "PURPOSE";
    public static String SYSTEM     = "SYSTEM";
    public static String CONFIG_DATA     = "CONFIG_DATA";
    public static String EXCH_RATE_DATE     = "EXCH_RATE_DATE";
    public static String VERSION     = "VERSION";
    public static String KB_LOGSYS     = "KB_LOGSYS";
    public static String ATTRIBUTES     = "ATTRIBUTES";
    public static String FIELD_VARCOND     = "FIELD_VARCOND";
    public static String MESSAGES     = "MESSAGES";
    public static String DESCRIPTION     = "DESCRIPTION";
    public static String SCALE_CURRENCY     = "SCALE_CURRENCY";
    public static String ALT_COND_VALUE     = "ALT_COND_VALUE";
    public static String AUTHORITY_EDIT     = "AUTHORITY_EDIT";
    public static String VARCOND_FLAG     = "VARCOND_FLAG";
    public static String DENOMINATOR     = "DENOMINATOR";
    public static String LINE_ID     = "LINE_ID";
    public static String COND_INACTIVE     = "COND_INACTIVE";
    public static String COND_UNIT     = "COND_UNIT";
    public static String WEEKS_S     = "WEEKS_S";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String SOURCE_ITEM_ID     = "SOURCE_ITEM_ID";
    public static String MONTHS_S     = "MONTHS_S";
    public static String ES_DOCUMENT     = "ES_DOCUMENT";
    public static String KB_VERSION     = "KB_VERSION";
    public static String COND_TYPE_DESCR     = "COND_TYPE_DESCR";
    public static String DOCUMENT_ID_EXT     = "DOCUMENT_ID_EXT";
    public static String QUANTITY_UNIT     = "QUANTITY_UNIT";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String MULTIPLICITY     = "MULTIPLICITY";
    public static String ACCRUAL_FLAG     = "ACCRUAL_FLAG";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String FIELDNAME     = "FIELDNAME";
    public static String EXT_CONFIG     = "EXT_CONFIG";
    public static String OBJ_KEY     = "OBJ_KEY";
    public static String COND_VALUE     = "COND_VALUE";
    public static String PRODUCT_ERP     = "PRODUCT_ERP";
    public static String PROFILE     = "PROFILE";
    public static String PRODUCT_ID     = "PRODUCT_ID";
    public static String BUILD     = "BUILD";
    public static String TYPE     = "TYPE";
    public static String VOLUME_UNIT     = "VOLUME_UNIT";
    public static String RUNTIME     = "RUNTIME";
    public static String KB_PROFILE     = "KB_PROFILE";
    public static String NET_VALUE_NEW     = "NET_VALUE_NEW";
    public static String CHANGE_OF_RATE     = "CHANGE_OF_RATE";
    public static String SOURCE_QUANTITY_UNIT     = "SOURCE_QUANTITY_UNIT";
    public static String CHANGE_OF_UNIT     = "CHANGE_OF_UNIT";
    public static String PRICING_TYPE     = "PRICING_TYPE";
    public static String PERFORM_TRACE     = "PERFORM_TRACE";
    public static String PRODUCT_TYPE     = "PRODUCT_TYPE";
    public static String VALUES     = "VALUES";
    public static String OBJ_TXT     = "OBJ_TXT";
    public static String KB_DATE     = "KB_DATE";
    public static String INSTANCES     = "INSTANCES";
    public static String TIMESTAMP     = "TIMESTAMP";
    public static String DAYS_PER_YEAR     = "DAYS_PER_YEAR";
    public static String PROCEDUR     = "PROCEDUR";
    public static String PART_OF_NO     = "PART_OF_NO";
    public static String EXPIRING_CHECK_DATE     = "EXPIRING_CHECK_DATE";
    public static String GROSS_WEIGHT     = "GROSS_WEIGHT";
    public static String FACTOR     = "FACTOR";
    public static String MANUALLY_CHANGED     = "MANUALLY_CHANGED";
    public static String KEEP_ZERO_PRICES     = "KEEP_ZERO_PRICES";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String VARCOND_FACTOR     = "VARCOND_FACTOR";
    public static String ID     = "ID";
    public static String ACCOUNT_KEY_1     = "ACCOUNT_KEY_1";
    public static String CHANGE_OF_CALC_TYPE     = "CHANGE_OF_CALC_TYPE";
    public static String COND_TABLE_NAME     = "COND_TABLE_NAME";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String SOURCE_QUANTITY     = "SOURCE_QUANTITY";
    public static String INFO     = "INFO";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String COMPLETE     = "COMPLETE";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String DATE_FROM_S     = "DATE_FROM_S";
    public static String SALES_RELEVANT     = "SALES_RELEVANT";
    public static String SALES_TAX_CODE     = "SALES_TAX_CODE";
    public static String CLASS_TYPE     = "CLASS_TYPE";
    public static String COND_UPDATE     = "COND_UPDATE";
    public static String COND_BASE_UNIT     = "COND_BASE_UNIT";
    public static String DAYS_PER_MONTH     = "DAYS_PER_MONTH";
    public static String ALT_COND_BASE     = "ALT_COND_BASE";
    public static String WEEKS_B     = "WEEKS_B";
    public static String GROUP_CONDITION     = "GROUP_CONDITION";
    public static String ACCESS_COUNTER     = "ACCESS_COUNTER";
    public static String ONLY_SPEC_USAGE     = "ONLY_SPEC_USAGE";
    public static String CONDITIONS     = "CONDITIONS";
    public static String KB_NAME     = "KB_NAME";
    public static String WITHHOLDING_TAX_CODE     = "WITHHOLDING_TAX_CODE";
    public static String PARENT_ID     = "PARENT_ID";
    public static String KB_ID     = "KB_ID";
    public static String DECOUPLE_SUBITEMS     = "DECOUPLE_SUBITEMS";
    public static String HEADER     = "HEADER";
    public static String ACCOUNT_KEY_2     = "ACCOUNT_KEY_2";
    public static String VARCOND_DESCR     = "VARCOND_DESCR";
    public static String VALUE_UNIT     = "VALUE_UNIT";
    public static String EMPTY_FIELD_HACK     = "";
    public static String LOGSYS     = "LOGSYS";
    public static String SOURCE_DOCUMENT_ID     = "SOURCE_DOCUMENT_ID";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String BUSINESS_OBJECT_TYPE     = "BUSINESS_OBJECT_TYPE";
    public static String FIX_GROUP     = "FIX_GROUP";
    public static String STRUCTURE_CONDITION     = "STRUCTURE_CONDITION";
    public static String USAGE     = "USAGE";
    public static String NUMERATOR     = "NUMERATOR";
    public static String STATISTICAL     = "STATISTICAL";
    public static String CONSISTENT     = "CONSISTENT";
    public static String VKEY     = "VKEY";
    public static String HOLIDAYS     = "HOLIDAYS";
    public static String HIGH_LEVEL_ITEM_ID     = "HIGH_LEVEL_ITEM_ID";
    public static String COND_TYPE     = "COND_TYPE";
    public static String LOCAL_CURRENCY_UNIT     = "LOCAL_CURRENCY_UNIT";
    public static String LANGUAGE     = "LANGUAGE";
    public static String QUANTITY     = "QUANTITY";
    public static String SCALE_BASE_VALUE     = "SCALE_BASE_VALUE";
    public static String ET_ITEM     = "ET_ITEM";
    public static String DATE_FROM_B     = "DATE_FROM_B";
    public static String COND_CURRENCY     = "COND_CURRENCY";
    public static String INST_ID     = "INST_ID";
    public static String ITEM_ID     = "ITEM_ID";
    public static String AUTHORITY_DISPLAY     = "AUTHORITY_DISPLAY";
    public static String CHARC     = "CHARC";
    public static String CHARC_TXT     = "CHARC_TXT";
    public static String COND_CATEGORY     = "COND_CATEGORY";
    public static String SCALE_BASE_TYPE     = "SCALE_BASE_TYPE";
    public static String PRODUCT_LOGSYS     = "PRODUCT_LOGSYS";
    public static String COND_CLASS     = "COND_CLASS";
    public static String CHANGE_OF_CONV_FACTORS     = "CHANGE_OF_CONV_FACTORS";
    public static String YEARS_S     = "YEARS_S";
    public static String EXT_DATASOURCE     = "EXT_DATASOURCE";
    public static String DAYS_B     = "DAYS_B";
    public static String COPY_TYPE     = "COPY_TYPE";
    public static String DATASOURCE     = "DATASOURCE";
    public static String KEY     = "KEY";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String COND_UNIT_VALUE     = "COND_UNIT_VALUE";
    public static String XCOND_BASE     = "XCOND_BASE";
    public static String NAME     = "NAME";
    public static String TIMESTAMP_S     = "TIMESTAMP_S";
    public static String DOCUMENT_CURRENCY_UNIT     = "DOCUMENT_CURRENCY_UNIT";
    public static String IMPORT_CONDITIONS     = "IMPORT_CONDITIONS";
    public static String NET_WEIGHT     = "NET_WEIGHT";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String DATE_TO_B     = "DATE_TO_B";
    public static String CALC_TYPE     = "CALC_TYPE";
    public static String USAG     = "USAG";
    public static String ROUNDING_DIFF     = "ROUNDING_DIFF";
    public static String CONTEXT     = "CONTEXT";
    public static String ROOT_ID     = "ROOT_ID";
    public static String EDIT_MODE     = "EDIT_MODE";
    public static String GROUP_CONDITION_PROCESSING     = "GROUP_CONDITION_PROCESSING";
    public static String PRICING_DATE     = "PRICING_DATE";
    public static String POINTS     = "POINTS";
    public static String DOCUMENT_ID     = "DOCUMENT_ID";
    public static String WEIGHT_UNIT     = "WEIGHT_UNIT";
    public static String VARIANT_CONDITION     = "VARIANT_CONDITION";
    public static String VALUE     = "VALUE";
    public static String COND_RECORD_ID     = "COND_RECORD_ID";
    public static String ITEM_COPY     = "ITEM_COPY";
    public static String ITEM_QUANTITY_UNIT     = "ITEM_QUANTITY_UNIT";
    public static String IT_ITEM_ID     = "IT_ITEM_ID";
    public static String SCALE_UNIT     = "SCALE_UNIT";
    public static String COND_ORIGIN     = "COND_ORIGIN";
    public static String PRICING_RELEVANT     = "PRICING_RELEVANT";
    public static String EXCH_RATE     = "EXCH_RATE";
    public static String COND_CONTROL     = "COND_CONTROL";
    public static String INVOICE_LIST     = "INVOICE_LIST";
    public static String PERIOD_FACTOR     = "PERIOD_FACTOR";
    public static String STEP_NUMBER     = "STEP_NUMBER";
    public static String COND_BASE     = "COND_BASE";
    public static String COND_COUNTER     = "COND_COUNTER";
    public static String YEARS_B     = "YEARS_B";
    public static String SCE     = "SCE";
    public static String EXCH_RATE_TYPE     = "EXCH_RATE_TYPE";
    public static String APPLICATION     = "APPLICATION";
    public static String MANUAL_ENTRY_FLAG     = "MANUAL_ENTRY_FLAG";
    public static String RETURN_FLAG     = "RETURN_FLAG";
    public static String VARIANT_CONDITIONS     = "VARIANT_CONDITIONS";
    public static String DAYS_S     = "DAYS_S";
    public static String ITEM_QUANTITY     = "ITEM_QUANTITY";
    public static String UNCHANGEABLE     = "UNCHANGEABLE";
    public static String CHANGE_OF_VALUE     = "CHANGE_OF_VALUE";
    public static String OBJ_TYPE     = "OBJ_TYPE";
    public static String VALUE_TXT     = "VALUE_TXT";
    public static String EXPONENT     = "EXPONENT";
    public static String PARAMETER     = "PARAMETER";
    public static String COND_RATE     = "COND_RATE";
    public static String PART_OF     = "PART_OF";
    public static String INTER_COMPANY     = "INTER_COMPANY";
    public static String ROW     = "ROW";

}
