package com.sap.spc.remote.client.rfc.function; 

public interface SpcChangeItemConfig {

    public static String LOGSYS     = "LOGSYS";
    public static String RESET_CONFIG     = "RESET_CONFIG";
    public static String AUTHOR     = "AUTHOR";
    public static String SCE     = "SCE";
    public static String SUB_ITEMS_ALLOWED     = "SUB_ITEMS_ALLOWED";
    public static String ONLY_VAR_FIND     = "ONLY_VAR_FIND";
    public static String CONTEXT     = "CONTEXT";
    public static String INFO     = "INFO";
    public static String ROOT_ID     = "ROOT_ID";
    public static String EXT_CONFIG_GUID     = "EXT_CONFIG_GUID";
    public static String VALUES     = "VALUES";
    public static String COMPLETE     = "COMPLETE";
    public static String VARIANT_CONDITIONS     = "VARIANT_CONDITIONS";
    public static String OBJ_TXT     = "OBJ_TXT";
    public static String DOCUMENT_ID     = "DOCUMENT_ID";
    public static String GUID     = "GUID";
    public static String CONSISTENT     = "CONSISTENT";
    public static String QUANTITY_UNIT     = "QUANTITY_UNIT";
    public static String SALES_RELEVANT     = "SALES_RELEVANT";
    public static String VKEY     = "VKEY";
    public static String VALUE     = "VALUE";
    public static String CLASS_TYPE     = "CLASS_TYPE";
    public static String INST_ID     = "INST_ID";
    public static String ITEM_ID     = "ITEM_ID";
    public static String KB_DATE     = "KB_DATE";
    public static String CHARC_TXT     = "CHARC_TXT";
    public static String CHARC     = "CHARC";
    public static String INSTANCES     = "INSTANCES";
    public static String OBJ_TYPE     = "OBJ_TYPE";
    public static String VALUE_TXT     = "VALUE_TXT";
    public static String EXT_IDS     = "EXT_IDS";
    public static String LANGUAGE     = "LANGUAGE";
    public static String EXT_CONFIG     = "EXT_CONFIG";
    public static String PART_OF_NO     = "PART_OF_NO";
    public static String PARENT_ID     = "PARENT_ID";
    public static String VERSION     = "VERSION";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String OBJ_KEY     = "OBJ_KEY";
    public static String QUANTITY     = "QUANTITY";
    public static String FACTOR     = "FACTOR";
    public static String PART_OF     = "PART_OF";
    public static String NAME     = "NAME";
    public static String PROFILE     = "PROFILE";
    public static String HEADER     = "HEADER";
    public static String VALUE_UNIT     = "VALUE_UNIT";
    public static String BUILD     = "BUILD";
    public static String TRUE     = "T";
    public static String FALSE     = "F";

}
