package com.sap.spc.remote.client.rfc.function; 

public interface SpcUpdateConfiguration {

    public static String UPDATED     = "UPDATED";
    public static String CONFIGURATION_ID     = "CONFIGURATION_ID";

}
