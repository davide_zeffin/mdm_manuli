package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetKnowledgebase {

    public static String KB_PROFILE     = "KB_PROFILE";
    public static String KB_ID     = "KB_ID";
    public static String KB_LOGSYS     = "KB_LOGSYS";
    public static String KB_NAME     = "KB_NAME";
    public static String KB_DESIGNS     = "KB_DESIGNS";
    public static String KB_VERSION     = "KB_VERSION";
    public static String KB_USER_DESIGN     = "KB_USER_DESIGN";

}
