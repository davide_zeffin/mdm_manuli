package com.sap.spc.remote.client.rfc.function; 

public interface SpcGetConditionRecords {

    public static String TYPE     = "TYPE";
    public static String KNUMH     = "KNUMH";
    public static String RUNTIME     = "RUNTIME";
    public static String IV_ONLY_FOUND_RECORD     = "IV_ONLY_FOUND_RECORD";
    public static String IV_DOCUMENT_ID     = "IV_DOCUMENT_ID";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String KPEIN     = "KPEIN";
    public static String TIME_FROM     = "TIME_FROM";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String KBETR_PRT     = "KBETR_PRT";
    public static String SCALE_INFO     = "SCALE_INFO";
    public static String KMEIN     = "KMEIN";
    public static String NUMBER     = "NUMBER";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String USAGE_DATA     = "USAGE_DATA";
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String IV_ITEM_ID     = "IV_ITEM_ID";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String IV_STEP_NUMBER     = "IV_STEP_NUMBER";
    public static String KSTBS_PRT_1     = "KSTBS_PRT_1";
    public static String STFKZ     = "STFKZ";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String IV_COND_COUNTER     = "IV_COND_COUNTER";
    public static String ET_COND_RECORD     = "ET_COND_RECORD";
    public static String TIME_TO     = "TIME_TO";
    public static String STEIN     = "STEIN";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String KZBZG     = "KZBZG";
    public static String LOG_NO     = "LOG_NO";
    public static String COND_SCALE     = "COND_SCALE";
    public static String SYSTEM     = "SYSTEM";
    public static String KRECH     = "KRECH";
    public static String PARENT_ID     = "PARENT_ID";
    public static String ATTR_NAME     = "ATTR_NAME";
    public static String PARAMETER     = "PARAMETER";
    public static String WAERS     = "WAERS";
    public static String SEQ_NUMBER     = "SEQ_NUMBER";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String ATTR_VALUE     = "ATTR_VALUE";
    public static String MESSAGES     = "MESSAGES";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String ROW     = "ROW";
    public static String LINE_ID     = "LINE_ID";
    public static String ID     = "ID";

}
