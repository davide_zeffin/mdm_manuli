package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiCreateInstance {

    public static String CONFIG_ID     = "CONFIG_ID";
    public static String DECOMP_POS_NR     = "DECOMP_POS_NR";
    public static String INST_ID     = "INST_ID";
    public static String PARENT_INST_ID     = "PARENT_INST_ID";

}
