package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetConfigInfo {

    public static String SINGLE_LEVEL     = "SINGLE_LEVEL";
    public static String GET_CONTEXT     = "GET_CONTEXT";
    public static String LOGSYS     = "LOGSYS";
    public static String SCE     = "SCE";
    public static String CONTEXT     = "CONTEXT";
    public static String ROOT_ID     = "ROOT_ID";
    public static String VERSION     = "VERSION";
    public static String COMPLETE     = "COMPLETE";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String CONSISTENT     = "CONSISTENT";
    public static String CONFIG_INFO     = "CONFIG_INFO";
    public static String VALUE     = "VALUE";
    public static String NAME     = "NAME";
    public static String PROFILE     = "PROFILE";
    public static String BUILD     = "BUILD";

}
