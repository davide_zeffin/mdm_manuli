package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetProfilesOfKb {

    public static String KB_PROFILES     = "KB_PROFILES";
    public static String KB_PROFILE     = "KB_PROFILE";
    public static String KB_LOGSYS     = "KB_LOGSYS";
    public static String UINAME     = "UINAME";
    public static String KB_NAME     = "KB_NAME";
    public static String KB_VERSION     = "KB_VERSION";
    public static String ROOT_INSTANCE_NAME     = "ROOT_INSTANCE_NAME";

}
