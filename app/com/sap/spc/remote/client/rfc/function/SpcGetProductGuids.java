package com.sap.spc.remote.client.rfc.function; 

public interface SpcGetProductGuids {

    public static String TYPE     = "TYPE";
    public static String RUNTIME     = "RUNTIME";
    public static String LOGICAL_SYSTEM     = "LOGICAL_SYSTEM";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String OBJECT_FAMILY     = "OBJECT_FAMILY";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String NUMBER     = "NUMBER";
    public static String PRODUCT_TYPE     = "PRODUCT_TYPE";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String PRODUCT_GUID     = "PRODUCT_GUID";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String LOG_NO     = "LOG_NO";
    public static String IT_PRODUCT_DATA     = "IT_PRODUCT_DATA";
    public static String SYSTEM     = "SYSTEM";
    public static String PARENT_ID     = "PARENT_ID";
    public static String PARAMETER     = "PARAMETER";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String MESSAGES     = "MESSAGES";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String PRODUCT_ID     = "PRODUCT_ID";
    public static String ET_PRODUCT_DATA     = "ET_PRODUCT_DATA";
    public static String ROW     = "ROW";
    public static String LINE_ID     = "LINE_ID";
    public static String ID     = "ID";

}
