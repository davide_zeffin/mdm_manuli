package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiFindKnowledgebases {

    public static String KB_PROFILE     = "KB_PROFILE";
    public static String OBJECT_TYPE     = "OBJECT_TYPE";
    public static String KB_HIER_VERSION     = "KB_HIER_VERSION";
    public static String KB_ACTIVE     = "KB_ACTIVE";
    public static String KB_ORGTYPE     = "KB_ORGTYPE";
    public static String KB_HIERARCHY     = "KB_HIERARCHY";
    public static String KB_VERSION     = "KB_VERSION";
    public static String KB_EXT_REF     = "KB_EXT_REF";
    public static String USAGE     = "USAGE";
    public static String STATUS     = "STATUS";
    public static String VALID_DATE     = "VALID_DATE";
    public static String ORG_TYPE     = "ORG_TYPE";
    public static String OBJECT_NAME     = "OBJECT_NAME";
    public static String KB_FROMDATE     = "KB_FROMDATE";
    public static String KB_ORGID     = "KB_ORGID";
    public static String ORG_ID     = "ORG_ID";
    public static String KB_NAME     = "KB_NAME";
    public static String KNOWLEDGEBASES     = "KNOWLEDGEBASES";
    public static String KB_STATUS     = "KB_STATUS";
    public static String KB_ID     = "KB_ID";
    public static String WITH_KB_PROFILES     = "WITH_KB_PROFILES";
    public static String KB_LOGSYS     = "KB_LOGSYS";
    public static String KB_USAGE     = "KB_USAGE";
    public static String OBJECT_LOGSYS     = "OBJECT_LOGSYS";
    public static String KB_BUILD     = "KB_BUILD";
    public static String TRUE     = "T";
    public static String MARA   = "MARA";

}
