package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetLoadStatus {

    public static String CFG_LOADED_FROM_EXT     = "CFG_LOADED_FROM_EXT";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String KB_VERSION_CHANGED     = "KB_VERSION_CHANGED";
    public static String MSG_LEVEL     = "MSG_LEVEL";
    public static String LOAD_STATUS     = "LOAD_STATUS";

}
