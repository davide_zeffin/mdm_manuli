package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiDeleteCsticsValues {

    public static String CONFIG_ID     = "CONFIG_ID";
    public static String VALUES_TO_DELETE     = "VALUES_TO_DELETE";
    public static String FORMAT_VALUES     = "FORMAT_VALUES";
    public static String CSTIC_VALUES     = "CSTIC_VALUES";
    public static String FORMAT_VALUE     = "FORMAT_VALUE";
    public static String VALUE_NAME     = "VALUE_NAME";
    public static String CSTIC_NAME     = "CSTIC_NAME";
    public static String INST_ID     = "INST_ID";

}
