package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiSpecialize {

    public static String CONFIG_ID     = "CONFIG_ID";
    public static String SPEC_TYPE_ID     = "SPEC_TYPE_ID";
    public static String INST_ID     = "INST_ID";

}
