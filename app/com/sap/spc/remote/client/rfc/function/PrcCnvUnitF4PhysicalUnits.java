package com.sap.spc.remote.client.rfc.function;


public class PrcCnvUnitF4PhysicalUnits {

    public static String ET_PHYSICAL_UNIT     = "ET_PHYSICAL_UNIT";
    public static String INTERNAL_NAME     = "INTERNAL_NAME";
    public static String EXTERNAL_NAME     = "EXTERNAL_NAME";
    public static String DECIMALS     = "DECIMALS";
    public static String DESCRIPTION = "DESCRIPTION";


}
