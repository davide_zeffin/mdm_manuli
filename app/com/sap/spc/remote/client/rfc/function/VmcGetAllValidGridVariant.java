package com.sap.spc.remote.client.rfc.function; 

public interface VmcGetAllValidGridVariant {

    public static String IT_CSTICS_VALUES     = "IT_CSTICS_VALUES";
    public static String ET_VARIANTS_VALUES     = "ET_VARIANTS_VALUES";
    public static String ET_VARIANTS_HEADER     = "ET_VARIANTS_HEADER";
    public static String CSTIC_NAMES     = "CSTIC_NAMES";
    public static String IV_PRODUCT_ID     = "IV_PRODUCT_ID";
    public static String IV_LOG_SYS     = "IV_LOG_SYS";
    public static String VARIANT_ID     = "VARIANT_ID";
    public static String SPREAD_VALUE     = "SPREAD_VALUE";
    public static String CSTIC_VALUES     = "CSTIC_VALUES";
    public static String IT_CSTICS_NAMES     = "IT_CSTICS_NAMES";
    public static String PARENT_ID     = "PARENT_ID";

}
