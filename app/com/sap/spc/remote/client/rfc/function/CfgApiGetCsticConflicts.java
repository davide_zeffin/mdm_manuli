package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetCsticConflicts {

    public static String CONFLICT_INST_ID     = "CONFLICT_INST_ID";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String CONFLICT_TEXT     = "CONFLICT_TEXT";
    public static String CONFLICTS     = "CONFLICTS";
    public static String CONFLICT_CSTIC_NAME     = "CONFLICT_CSTIC_NAME";
    public static String INST_ID     = "INST_ID";
    public static String CSTIC_NAME     = "CSTIC_NAME";
    public static String CONFLICT_CSTIC_LNAME     = "CONFLICT_CSTIC_LNAME";

}
