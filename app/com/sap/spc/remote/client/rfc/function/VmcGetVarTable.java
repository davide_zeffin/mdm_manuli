package com.sap.spc.remote.client.rfc.function; 

public interface VmcGetVarTable {

    public static String IV_FILTER_NAME     = "IV_FILTER_NAME";
    public static String IV_ITEM_ID     = "IV_ITEM_ID";
    public static String CSTIC_NAMES     = "CSTIC_NAMES";
    public static String IT_NAME_CSTICS     = "IT_NAME_CSTICS";
    public static String EV_TAB_NAME     = "EV_TAB_NAME";
    public static String IV_DOCUMENT_ID     = "IV_DOCUMENT_ID";
    public static String IV_FILTER_DEFAULT     = "IV_FILTER_DEFAULT";
    public static String IV_FILTER_VALUE     = "IV_FILTER_VALUE";
    public static String CSTIC_VALUES     = "CSTIC_VALUES";
    public static String ET_CSTICS_NAMES     = "ET_CSTICS_NAMES";
    public static String ET_CSTICS_VALUES     = "ET_CSTICS_VALUES";
    public static String IV_TAB_NAME     = "IV_TAB_NAME";
    
    public static String CSTICS_NAMES	="csticsNames";
	public static String CSTICS_VALUES	="csticsValues";

}
