package com.sap.spc.remote.client.rfc.function; 

public interface SpcBindConfiguration {

    public static String TOKEN_ID     = "TOKEN_ID";
    public static String CONFIGURATION_ID     = "CONFIGURATION_ID";

}
