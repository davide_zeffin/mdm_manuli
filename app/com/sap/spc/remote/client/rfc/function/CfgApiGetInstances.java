package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetInstances {

    public static String TEXT_LINE_ID     = "TEXT_LINE_ID";
    public static String INSTANCES     = "INSTANCES";
    public static String INST_PID     = "INST_PID";
    public static String MIME_OBJECT     = "MIME_OBJECT";
    public static String INST_DESCRIPTION     = "INST_DESCRIPTION";
    public static String INST_PRICE     = "INST_PRICE";
    public static String INST_UNSPECABLE     = "INST_UNSPECABLE";
    public static String TEXT_LINE     = "TEXT_LINE";
    public static String INST_POSITION     = "INST_POSITION";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String INST_QUANTITY     = "INST_QUANTITY";
    public static String INST_CONSISTENT     = "INST_CONSISTENT";
    public static String INST_LNAME     = "INST_LNAME";
    public static String INST_AUTHOR     = "INST_AUTHOR";
    public static String INST_NAME     = "INST_NAME";
    public static String INST_COMPLETE     = "INST_COMPLETE";
    public static String MIME_OBJECT_TYPE     = "MIME_OBJECT_TYPE";
    public static String INST_MIMES     = "INST_MIMES";
    public static String GET_MIME_OBJECTS     = "GET_MIME_OBJECTS";
    public static String INST_ID     = "INST_ID";
    public static String TEXT_FORMAT     = "TEXT_FORMAT";

}
