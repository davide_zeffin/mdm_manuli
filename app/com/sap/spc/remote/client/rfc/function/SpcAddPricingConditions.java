package com.sap.spc.remote.client.rfc.function; 

public interface SpcAddPricingConditions {

    public static String XACCRUAL_FLAG     = "XACCRUAL_FLAG";
    public static String TYPE     = "TYPE";
    public static String RUNTIME     = "RUNTIME";
    public static String COND_TABLE_NAME     = "COND_TABLE_NAME";
    public static String COND_UNIT     = "COND_UNIT";
    public static String IV_DOCUMENT_ID     = "IV_DOCUMENT_ID";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String XCOND_RATE     = "XCOND_RATE";
    public static String CALC_TYPE     = "CALC_TYPE";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String NUMBER     = "NUMBER";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String IT_CONDITION     = "IT_CONDITION";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String COND_CURRENCY     = "COND_CURRENCY";
    public static String COND_RECORD_ID     = "COND_RECORD_ID";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String ITEM_ID     = "ITEM_ID";
    public static String ACCRUAL_FLAG     = "ACCRUAL_FLAG";
    public static String COND_TYPE     = "COND_TYPE";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String LOG_NO     = "LOG_NO";
    public static String XCOND_VALUE     = "XCOND_VALUE";
    public static String SYSTEM     = "SYSTEM";
    public static String IV_PROCESS_MODE     = "IV_PROCESS_MODE";
    public static String PARENT_ID     = "PARENT_ID";
    public static String PARAMETER     = "PARAMETER";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String MESSAGES     = "MESSAGES";
    public static String COND_VALUE     = "COND_VALUE";
    public static String COND_UNIT_VALUE     = "COND_UNIT_VALUE";
    public static String COND_RATE     = "COND_RATE";
    public static String XCOND_BASE     = "XCOND_BASE";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String COND_BASE     = "COND_BASE";
    public static String ROW     = "ROW";
    public static String ID     = "ID";
    public static String LINE_ID     = "LINE_ID";
    public static String XFLAG     = "X";

}
