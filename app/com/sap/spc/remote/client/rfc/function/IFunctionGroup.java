
package com.sap.spc.remote.client.rfc.function;

/**
 * @author I026584
 *
 */
public interface IFunctionGroup {
	
    // function group COM_CFG_API_UI_COMMAND
    public static String CFG_API_CREATE_INSTANCE            = "CFG_API_CREATE_INSTANCE";
    public static String CFG_API_DELETE_ASSUMPTION          = "CFG_API_DELETE_ASSUMPTION";
    public static String CFG_API_DELETE_CSTICS_VALUES       = "CFG_API_DELETE_CSTICS_VALUES";
    public static String CFG_API_DELETE_INSTANCE            = "CFG_API_DELETE_INSTANCE";
    public static String CFG_API_GET_CONFIG_INFO            = "CFG_API_GET_CONFIG_INFO";
    public static String CFG_API_GET_CONFLICTS              = "CFG_API_GET_CONFLICTS";
    public static String CFG_API_GET_CONFL_ASSUMPTIONS      = "CFG_API_GET_CONFL_ASSUMPTIONS";
    public static String CFG_API_GET_CSTIC                  = "CFG_API_GET_CSTIC";
    public static String CFG_API_GET_CSTIC_CONFLICTS        = "CFG_API_GET_CSTIC_CONFLICTS";
    public static String CFG_API_GET_CSTIC_GROUPS           = "CFG_API_GET_CSTIC_GROUPS";
    public static String CFG_API_GET_CSTIC_VALUES           = "CFG_API_GET_CSTIC_VALUES";
    public static String CFG_API_GET_FACTS                  = "CFG_API_GET_FACTS";
    public static String CFG_API_GET_FILTERED_CSTICS        = "CFG_API_GET_FILTERED_CSTICS";
    public static String CFG_API_GET_INSTANCES              = "CFG_API_GET_INSTANCES";
    public static String CFG_API_GET_LOAD_MESSAGES          = "CFG_API_GET_LOAD_MESSAGES";
    public static String CFG_API_GET_LOAD_STATUS            = "CFG_API_GET_LOAD_STATUS";
    public static String CFG_API_GET_PROFILES_OF_KB         = "CFG_API_GET_PROFILES_OF_KB";
    public static String CFG_API_RESET_CONFIG               = "CFG_API_RESET_CONFIG";
    public static String CFG_API_SET_CSTICS_VALUES          = "CFG_API_SET_CSTICS_VALUES";
    public static String CFG_API_SPECIALIZE                 = "CFG_API_SPECIALIZE";
    public static String CFG_API_UNSPECIALIZE               = "CFG_API_UNSPECIALIZE";
	
    //function group COM_CFG_API_ENGINE_COMMAND
    public static String CFG_API_COMPARE_CONFIGURATIONS     = "CFG_API_COMPARE_CONFIGURATIONS";
    public static String CFG_API_CONVERT_IDOC_TO_XML        = "CFG_API_CONVERT_IDOC_TO_XML";
    public static String CFG_API_CREATE_CONFIG              = "CFG_API_CREATE_CONFIG";
    public static String CFG_API_FIND_KNOWLEDGEBASES        = "CFG_API_FIND_KNOWLEDGEBASES";
    public static String CFG_API_GET_KNOWLEDGEBASE          = "CFG_API_GET_KNOWLEDGEBASE";
    public static String CFG_API_GET_PRODUCT_VARIANTS       = "CFG_API_GET_PRODUCT_VARIANTS";
     
    // function group SPC_DEPRECATED 
	public static String SPC_ADD_PRICING_CONDITIONS			= "SPC_ADD_PRICING_CONDITIONS";
	public static String SPC_CALCULATE_TIME_PERIODS			= "SPC_CALCULATE_TIME_PERIODS";
	public static String SPC_CHANGE_DOCUMENT				= "SPC_CHANGE_DOCUMENT";
	public static String SPC_CHANGE_ITEMS					= "SPC_CHANGE_ITEMS";
	public static String SPC_CHANGE_ITEM_CONFIG				= "SPC_CHANGE_ITEM_CONFIG";
	public static String SPC_CHANGE_ITEM_PRODUCT			= "SPC_CHANGE_ITEM_PRODUCT";
	public static String SPC_CHANGE_PRICING_CONDITIONS		= "SPC_CHANGE_PRICING_CONDITIONS";
	public static String SPC_CLEAR_PRODUCT_BUFFER			= "SPC_CLEAR_PRODUCT_BUFFER";
    public static String SPC_CONFIGURE                      = "SPC_CONFIGURE";
	public static String SPC_COPY_ITEM						= "SPC_COPY_ITEM";
    public static String SPC_CREATE_DOCUMENT                = "SPC_CREATE_DOCUMENT";
	public static String SPC_CREATE_ITEMS					= "SPC_CREATE_ITEMS";
	public static String SPC_GET_CONDITION_RECORDS			= "SPC_GET_CONDITION_RECORDS";
	public static String SPC_GET_CONFIG_ITEM_INFO			= "SPC_GET_CONFIG_ITEM_INFO";
    public static String SPC_GET_INITIAL_CONFIGURATION      = "SPC_GET_INITIAL_CONFIGURATION";
	public static String SPC_GET_DOCUMENT_INFO				= "SPC_GET_DOCUMENT_INFO";
	public static String SPC_GET_DOCUMENT_ATTR_INFO         = "SPC_GET_DOCUMENT_ATTR_INFO";
	public static String SPC_GET_PRICING_CONDITIONS			= "SPC_GET_PRICING_CONDITIONS";
	public static String SPC_GET_PRICING_DOCUMENT_INFO		= "SPC_GET_PRICING_DOCUMENT_INFO";
	public static String SPC_GET_PRICING_PROCEDURE_INFO		= "SPC_GET_PRICING_PROCEDURE_INFO";
	public static String SPC_GET_PRICING_TRACE				= "SPC_GET_PRICING_TRACE";
    public static String SPC_GET_PRODUCT_GUIDS              = "SPC_GET_PRODUCT_GUIDS";
	public static String SPC_GET_PROTOCOL					= "SPC_GET_PROTOCOL";
	public static String SPC_LOAD_PRICING_DOCUMENT			= "SPC_LOAD_PRICING_DOCUMENT";
	public static String SPC_PRICING						= "SPC_PRICING";
	public static String SPC_REMOVE_DOCUMENT				= "SPC_REMOVE_DOCUMENT";
	public static String SPC_REMOVE_ITEMS					= "SPC_REMOVE_ITEMS";
	public static String SPC_REMOVE_PRICING_CONDITIONS		= "SPC_REMOVE_PRICING_CONDITIONS";
    public static String SPC_SET_INITIAL_CONFIGURATION      = "SPC_SET_INITIAL_CONFIGURATION";    

    // function group IPC_EXCHANGE_COMMANDS
	public static String SPC_BIND_CONFIGURATION             = "SPC_BIND_CONFIGURATION";
	public static String SPC_LOCK_CONFIGURATION             = "SPC_LOCK_CONFIGURATION";
	public static String SPC_PUBLISH_CONFIGURATION          = "SPC_PUBLISH_CONFIGURATION";
	public static String SPC_REPUBLISH_CONFIGURATION          = "SPC_REPUBLISH_CONFIGURATION";
	public static String SPC_UNLOCK_CONFIGURATION           = "SPC_UNLOCK_CONFIGURATION";
	
    // function group CRM_IPC_GRID_VMC_COMMANDS
    // Changed to AP function group SPC_DEPRECATED
	public static String SPC_GET_ALL_VALID_GRID_VARIANTS 	= "SPC_GET_GRID_VARIANTS";
	public static String SPC_GET_VAR_TABLE					= "SPC_GET_VAR_TABLE";
	
    // function group CNV_UNITS
	public static String PRC_CNV_UNIT_F4_PHYSICAL_UNITS		= "PRC_CNV_UNIT_F4_PHYSICAL_UNITS";
    public static String PRC_CNV_UNIT_CONV_ISO              = "PRC_CNV_UNIT_CONV_ISO";
    
    // function group CNV_CURRENCY
    public static String PRC_CNV_CURR_F4_CURRENCY_UNITS     = "PRC_CNV_CURR_F4_CURRENCY_UNITS";
    public static String PRC_CNV_CURR_CONV_ISO              = "PRC_CNV_CURR_CONV_ISO";
	
    // function group COM_PRODUCT
    public static String COM_PRODUCT_READ_VIA_RFC           = "COM_PRODUCT_READ_VIA_RFC";
    
    // function group BATG
    public static String BAPI_MESSAGE_GETDETAIL             = "BAPI_MESSAGE_GETDETAIL";
	
    // function group CA_TTE_VMC_RFC
    public static String AP_TTE_CALCULATE_TAXES             = "AP_TTE_CALCULATE_TAXES";
    public static String AP_TTE_GET_DOCUMENT_TABLE          = "AP_TTE_GET_DOCUMENT_TABLE";
    public static String AP_TTE_GET_DOCUMENT_XML            = "AP_TTE_GET_DOCUMENT_XML";
    
    // function group CRM_PME_UIMODEL
	public static String CRM_PME_UIMODEL_DETERMINE_RFC		= "CRM_PME_UIMODEL_DETERMINE_RFC";
    
	//example for availibility check; not productively used
	public static String CRM_AV_CHECK_APO_DO_CHECK          = "CRM_AV_CHECK_APO_DO_CHECK";
	
}
