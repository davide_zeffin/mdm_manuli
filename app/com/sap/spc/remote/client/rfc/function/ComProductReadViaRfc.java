package com.sap.spc.remote.client.rfc.function;

public interface ComProductReadViaRfc {

    public static String IV_PRODUCT_TYPE = "IV_PRODUCT_TYPE";
    public static String IV_OBJECT_FAMILY = "IV_OBJECT_FAMILY";
    public static String IV_LOGSYS = "IV_LOGSYS";
    public static String SIGN = "SIGN";
    public static String OPTION = "OPTION";
    public static String LOW = "LOW";
    public static String HIGH = "HIGH";
    public static String PRODUCT_GUID = "PRODUCT_GUID";
    public static String IT_PRODUCT_ID = "IT_PRODUCT_ID";
    public static String IT_PRODUCT_GUID = "IT_PRODUCT_GUID";
    public static String ET_PRODUCT = "ET_PRODUCT";
    public static String CLIENT = "CLIENT";
    public static String PRODUCT_ID = "PRODUCT_ID";
    public static String PRODUCT_TYPE = "PRODUCT_TYPE";
    public static String CONFIG = "CONFIG";
    public static String XNOSEARCH = "XNOSEARCH";
    public static String OBJECT_FAMILY = "OBJECT_FAMILY";
    public static String BATCH_DEDICATED = "BATCH_DEDICATED";
    public static String COMPETITOR_PROD = "COMPETITOR_PROD";
    public static String VALID_FROM = "VALID_FROM";
    public static String VALID_TO = "VALID_TO";
    public static String UPNAME = "UPNAME";
    public static String HISTEX = "HISTEX";
    public static String LOGSYS = "LOGSYS";

}
