package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetConflicts {

    public static String TEXT_LINE_ID     = "TEXT_LINE_ID";
    public static String TEXT_LINE     = "TEXT_LINE";
    public static String CONFLICT_INST_ID     = "CONFLICT_INST_ID";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String CONFLICT_TEXT     = "CONFLICT_TEXT";
    public static String CONFLICT_AUTHOR     = "CONFLICT_AUTHOR";
    public static String CONFLICT_ID     = "CONFLICT_ID";
    public static String CONFLICTS     = "CONFLICTS";
    public static String CONFLICT_LONG_TEXT     = "CONFLICT_LONG_TEXT";
    public static String INST_ID     = "INST_ID";
    public static String TEXT_FORMAT     = "TEXT_FORMAT";
    public static String CONFLICT_INST_LNAME     = "CONFLICT_INST_LNAME";

}
