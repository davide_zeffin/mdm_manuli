package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiDeleteAssumption {

    public static String ASSUMPTION_ID     = "ASSUMPTION_ID";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String ASSUMPTION_DELETED     = "ASSUMPTION_DELETED";

}
