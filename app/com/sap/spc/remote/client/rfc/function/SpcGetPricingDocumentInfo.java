package com.sap.spc.remote.client.rfc.function; 

public interface SpcGetPricingDocumentInfo {

    public static String TYPE     = "TYPE";
    public static String CASH_DISCOUNT_BASIS     = "CASH_DISCOUNT_BASIS";
    public static String RUNTIME     = "RUNTIME";
    public static String IV_DOCUMENT_ID     = "IV_DOCUMENT_ID";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String SUBTOTAL     = "SUBTOTAL";
    public static String NUMBER     = "NUMBER";
    public static String ES_HEAD_RET     = "ES_HEAD_RET";
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String FREIGHT     = "FREIGHT";
    public static String VALUE     = "VALUE";
    public static String NET_PRICE     = "NET_PRICE";
    public static String GROSS_VALUE_FROM_SUBTOTAL     = "GROSS_VALUE_FROM_SUBTOTAL";
    public static String NET_VALUE_FREIGHTLESS     = "NET_VALUE_FREIGHTLESS";
    public static String NET_PRICE_PR_UNIT_VALUE     = "NET_PRICE_PR_UNIT_VALUE";
    public static String LOG_NO     = "LOG_NO";
    public static String IT_ITEM_ID     = "IT_ITEM_ID";
    public static String SYSTEM     = "SYSTEM";
    public static String GROSS_VALUE     = "GROSS_VALUE";
    public static String SEQ_NUMBER     = "SEQ_NUMBER";
    public static String MESSAGES     = "MESSAGES";
    public static String DESTINATION     = "DESTINATION";
    public static String NET_PRICE_PR_UNIT_NAME     = "NET_PRICE_PR_UNIT_NAME";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String ID     = "ID";
    public static String LINE_ID     = "LINE_ID";
    public static String DYNAMIC_RETURN     = "DYNAMIC_RETURN";
    public static String CASH_DISCOUNT_VALUE     = "CASH_DISCOUNT_VALUE";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String TAX_VALUE     = "TAX_VALUE";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String ITEM_ID     = "ITEM_ID";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String NET_VALUE     = "NET_VALUE";
    public static String PARENT_ID     = "PARENT_ID";
    public static String PARAMETER     = "PARAMETER";
    public static String ATTR_NAME     = "ATTR_NAME";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String FLAG     = "FLAG";
    public static String ATTR_VALUE     = "ATTR_VALUE";
    public static String COND_FUNCTION     = "COND_FUNCTION";
    public static String ET_ITEM_RET     = "ET_ITEM_RET";
    public static String DOCUMENT_CURRENCY_UNIT     = "DOCUMENT_CURRENCY_UNIT";
    public static String ROW     = "ROW";

}
