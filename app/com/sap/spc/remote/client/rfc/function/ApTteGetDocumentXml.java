package com.sap.spc.remote.client.rfc.function; 

public interface ApTteGetDocumentXml {

    public static String IT_DOCUMENT     = "IT_DOCUMENT";
    public static String TTEDOCUMENTID     = "TTEDOCUMENTID";
    public static String RETURNCODE     = "RETURNCODE";
    public static String OT_DOCUMENT     = "OT_DOCUMENT";
    public static String CUTLINE     = "CUTLINE";
    public static String EV_I_XML_STRING = "EV_I_XML_STRING";
    public static String EV_O_XML_STRING = "EV_O_XML_STRING";    

}
