package com.sap.spc.remote.client.rfc.function; 

public interface BapiMessageGetdetail {

    public static String TYPE     = "TYPE";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String LOG_NO     = "LOG_NO";
    public static String RETURN     = "RETURN";
    public static String LINKPATTERN     = "LINKPATTERN";
    public static String LANGUAGE     = "LANGUAGE";
    public static String NUMBER     = "NUMBER";
    public static String SYSTEM     = "SYSTEM";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String PARAMETER     = "PARAMETER";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String TEXTFORMAT     = "TEXTFORMAT";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String ROW     = "ROW";
    public static String ID     = "ID";
    public static String ASC     = "ASC";

}
