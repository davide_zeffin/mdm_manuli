package com.sap.spc.remote.client.rfc.function; 

public interface SpcLockConfiguration {

    public static String LOCKED     = "LOCKED";
    public static String CONFIGURATION_ID     = "CONFIGURATION_ID";

}
