package com.sap.spc.remote.client.rfc.function; 

public interface SpcGetDocumentInfo {

    public static String TYPE     = "TYPE";
    public static String RUNTIME     = "RUNTIME";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String NUMBER     = "NUMBER";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String DOCUMENT_ID     = "DOCUMENT_ID"; 
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String ET_DOCUMENT_INFO     = "ET_DOCUMENT_INFO";
    public static String CONFIG_COMPLETE     = "CONFIG_COMPLETE";
    public static String ITEM_QUANTITY     = "ITEM_QUANTITY";
    public static String CONFIG_CONSISTENT     = "CONFIG_CONSISTENT";
    public static String PRODUCT_VARIANT     = "PRODUCT_VARIANT";
    public static String MESSAGE     = "MESSAGE";
    public static String ITEM_INFO     = "ITEM_INFO";
    public static String FIELD     = "FIELD";
    public static String ITEM_ID     = "ITEM_ID"; 
    public static String HIGH_LEVEL_ITEM_ID     = "HIGH_LEVEL_ITEM_ID";
    public static String ITEM_QUANTITY_UNIT     = "ITEM_QUANTITY_UNIT";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String LOG_NO     = "LOG_NO";
    public static String IT_ITEM_ID     = "IT_ITEM_ID";
    public static String SYSTEM     = "SYSTEM";
    public static String PARENT_ID     = "PARENT_ID";
    public static String PARAMETER     = "PARAMETER";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String CONFIG_CHANGED     = "CONFIG_CHANGED";
    public static String MESSAGES     = "MESSAGES";
    public static String PRODUCT_ERP     = "PRODUCT_ERP";
    public static String CONFIGURABLE     = "CONFIGURABLE";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String PRODUCT_DESCRIPTION     = "PRODUCT_DESCRIPTION";
    public static String PRODUCT_ID     = "PRODUCT_ID";
    public static String LINE_ID     = "LINE_ID";
    public static String IT_DOCUMENT_ID     = "IT_DOCUMENT_ID";
    public static String ID     = "ID";
    public static String ROW     = "ROW";
    public static String GUID     = "GUID";
    
    


}
