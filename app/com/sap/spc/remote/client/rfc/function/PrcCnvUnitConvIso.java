package com.sap.spc.remote.client.rfc.function; 

public interface PrcCnvUnitConvIso {

    public static String TYPE     = "TYPE";
    public static String OBJECT     = "OBJECT";
    public static String RUNTIME     = "RUNTIME";
    public static String LOG_MSG_NO     = "LOG_MSG_NO";
    public static String MESSAGE_V4     = "MESSAGE_V4";
    public static String ES_PROFILE     = "ES_PROFILE";
    public static String FREE_MEMORY_AFTER     = "FREE_MEMORY_AFTER";
    public static String NUMBER     = "NUMBER";
    public static String MESSAGE_V3     = "MESSAGE_V3";
    public static String TOTAL_MEMORY_BEFORE     = "TOTAL_MEMORY_BEFORE";
    public static String MESSAGE_V2     = "MESSAGE_V2";
    public static String MESSAGE     = "MESSAGE";
    public static String FIELD     = "FIELD";
    public static String UNIT     = "UNIT";
    public static String TOTAL_MEMORY_AFTER     = "TOTAL_MEMORY_AFTER";
    public static String LOG_NO     = "LOG_NO";
    public static String ET_MAPPING     = "ET_MAPPING";
    public static String SYSTEM     = "SYSTEM";
    public static String PARAMETER     = "PARAMETER";
    public static String FREE_MEMORY_BEFORE     = "FREE_MEMORY_BEFORE";
    public static String IT_ISO_QUANTITY_UNIT     = "IT_ISO_QUANTITY_UNIT";
    public static String MESSAGES     = "MESSAGES";
    public static String ISO_UNIT     = "ISO_UNIT";
    public static String ET_MESSAGE     = "ET_MESSAGE";
    public static String MESSAGE_V1     = "MESSAGE_V1";
    public static String ID     = "ID";
    public static String ROW     = "ROW";

}
