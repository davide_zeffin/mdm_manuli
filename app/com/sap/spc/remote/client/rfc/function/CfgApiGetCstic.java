package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetCstic {

	public static String CSTIC_UNIT     = "CSTIC_UNIT";
    public static String VALUE_DESCRIPTION     = "VALUE_DESCRIPTION";
    public static String MIME_OBJECT     = "MIME_OBJECT";
    public static String CSTIC_GROUP     = "CSTIC_GROUP";
    public static String CSTIC_DOMAIN_IS_INTERVAL     = "CSTIC_DOMAIN_IS_INTERVAL";
    public static String CSTIC_LNAME     = "CSTIC_LNAME";
    public static String CSTIC_VIEW     = "CSTIC_VIEW";
    public static String VALUE_ASSIGNED     = "VALUE_ASSIGNED";
    public static String CSTIC_VIEWS     = "CSTIC_VIEWS";
    public static String VALUE_CONDITION     = "VALUE_CONDITION";
    public static String VALUE_PRICE     = "VALUE_PRICE";
    public static String TEXT_LINE     = "TEXT_LINE";
	/** Indicates a new paragraph in SAPScript. */
	public final static String SAPSCRIPT_PARAGRAPH ="*";

	/** Indicates a new line in SAPScript. */
	public final static String SAPSCRIPT_NEWLINE ="/";
	/** Indicates an extended line in SAPScript. This is a line that is larger than 72 character **/
	public static final String SAPSCRIPT_EXTENDED_LINE = "=";
	public static final String SAPSCRIPT_COMMENT_LINE = "/*";
	public static final String SAPSCRIPT_EXTENDED_LINE_WITH_LF = "/=";
	public static final String SAPSCRIPT_CONTINOUS_LINE = "";
	public static final String SAPSCRIPT_COMMAND_LINE = "/:";
    public static String CSTIC_VALUETYPE     = "CSTIC_VALUETYPE";
    public static String CSTIC_ADDIT     = "CSTIC_ADDIT";
    public static String CSTIC_READONLY     = "CSTIC_READONLY";
    public static String CHARACTERISTIC     = "CHARACTERISTIC";
    public static String VALUE_LPRICE     = "VALUE_LPRICE";
    public static String VALUE_NAME     = "VALUE_NAME";
    public static String VALUE_LNAME     = "VALUE_LNAME";
    public static String CSTIC_MIMES     = "CSTIC_MIMES";
    public static String CSTIC_TYPELENGTH     = "CSTIC_TYPELENGTH";
    public static String INST_ID     = "INST_ID";
    public static String CSTIC_CONSISTENT     = "CSTIC_CONSISTENT";
    public static String TEXT_FORMAT     = "TEXT_FORMAT";
    public static String CSTIC_INST_POSITION     = "CSTIC_INST_POSITION";
    public static String CSTIC_NUMBERSCALE     = "CSTIC_NUMBERSCALE";
    public static String TEXT_LINE_ID     = "TEXT_LINE_ID";
    public static String VALUE_DEFAULT     = "VALUE_DEFAULT";
    public static String CSTIC_REQUIRED     = "CSTIC_REQUIRED";
    public static String CSTIC_DISPLAY_MODE     = "CSTIC_DISPLAY_MODE";
    public static String CSTIC_VARSEARCH_RELEVANT     = "CSTIC_VARSEARCH_RELEVANT";
    public static String CSTIC_DESCRIPTION     = "CSTIC_DESCRIPTION";
    public static String CSTIC_MULTI     = "CSTIC_MULTI";
    public static String CSTIC_HEADER     = "CSTIC_HEADER";
    public static String CSTIC_VALUES     = "CSTIC_VALUES";
    public static String VALUE_IS_DOMAIN_VALUE     = "VALUE_IS_DOMAIN_VALUE";
    public static String VALUE_AUTHOR     = "VALUE_AUTHOR";
    public static String CSTIC_VISIBLE     = "CSTIC_VISIBLE";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String CSTIC_AUTHOR     = "CSTIC_AUTHOR";
    public static String VALUE_MIMES     = "VALUE_MIMES";
    public static String MIME_OBJECT_TYPE     = "MIME_OBJECT_TYPE";
    public static String CSTIC_GROUP_POSITION     = "CSTIC_GROUP_POSITION";
    public static String CSTIC_ENTRY_FIELD_MASK     = "CSTIC_ENTRY_FIELD_MASK";
    public static String CSTIC_NAME     = "CSTIC_NAME";
    public static String CSTIC_CONSTRAINED     = "CSTIC_CONSTRAINED";

}
