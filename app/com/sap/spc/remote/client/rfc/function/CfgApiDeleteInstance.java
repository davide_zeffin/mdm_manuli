package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiDeleteInstance {

    public static String CONFIG_ID     = "CONFIG_ID";
    public static String SUCCESS     = "SUCCESS";
    public static String INST_ID     = "INST_ID";

}
