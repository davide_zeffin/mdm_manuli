package com.sap.spc.remote.client.rfc.function; 

public interface SpcPublishConfiguration {

    public static String TOKEN_ID     = "TOKEN_ID";
    public static String CONFIGURATION_ID     = "CONFIGURATION_ID";
    public static String INSTANCE_NO     = "INSTANCE_NO";
    public static String HOST_NAME     = "HOST_NAME";

}
