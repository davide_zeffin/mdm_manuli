package com.sap.spc.remote.client.rfc.function; 

public interface SpcChangeItemProduct {

    public static String DATA_MODEL     = "DATA_MODEL";
    public static String DOCUMENT_ID     = "DOCUMENT_ID";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String ITEM_ID     = "ITEM_ID";
    public static String PRODUCT_ID     = "PRODUCT_ID";

}
