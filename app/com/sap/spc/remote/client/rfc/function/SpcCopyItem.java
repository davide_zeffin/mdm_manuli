package com.sap.spc.remote.client.rfc.function; 

public interface SpcCopyItem {

    public static String TARGET_ITEM_ID     = "TARGET_ITEM_ID";
    public static String REVERT_CONFIG     = "REVERT_CONFIG";
    public static String SOURCE_ITEM_ID     = "SOURCE_ITEM_ID";
    public static String SOURCE_DOCUMENT_ID     = "SOURCE_DOCUMENT_ID";
    public static String TARGET_DOCUMENT_ID     = "TARGET_DOCUMENT_ID";
    public static String FALSE     = "F";

}
