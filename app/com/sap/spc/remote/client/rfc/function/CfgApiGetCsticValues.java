package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetCsticValues {

    public static String TEXT_LINE_ID     = "TEXT_LINE_ID";
    public static String VALUE_DEFAULT     = "VALUE_DEFAULT";
    public static String VALUE_DESCRIPTION     = "VALUE_DESCRIPTION";
    public static String MIME_OBJECT     = "MIME_OBJECT";
    public static String VALUE_IS_DOMAIN_VALUE     = "VALUE_IS_DOMAIN_VALUE";
    public static String VALUE_ASSIGNED     = "VALUE_ASSIGNED";
    public static String VALUE_AUTHOR     = "VALUE_AUTHOR";
    public static String VALUE_CONDITION     = "VALUE_CONDITION";
    public static String VALUE_PRICE     = "VALUE_PRICE";
    public static String TEXT_LINE     = "TEXT_LINE";
    public static String VALUES     = "VALUES";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String VALUE_MIMES     = "VALUE_MIMES";
    public static String MIME_OBJECT_TYPE     = "MIME_OBJECT_TYPE";
    public static String VALUE_NAME     = "VALUE_NAME";
    public static String VALUE_LNAME     = "VALUE_LNAME";
    public static String VALUE_LPRICE     = "VALUE_LPRICE";
    public static String CSTIC_NAME     = "CSTIC_NAME";
    public static String INST_ID     = "INST_ID";
    public static String TEXT_FORMAT     = "TEXT_FORMAT";

}
