package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetConflAssumptions {

    public static String ASSUMPTION_ID     = "ASSUMPTION_ID";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String ASSUMPTION_LNAME     = "ASSUMPTION_LNAME";
    public static String GROUP_ID     = "GROUP_ID";
    public static String GROUPED_ASSUMPTIONS     = "GROUPED_ASSUMPTIONS";
    public static String ASSUMPTION_AUTHOR     = "ASSUMPTION_AUTHOR";
    public static String ASSUMPTIONS     = "ASSUMPTIONS";

}
