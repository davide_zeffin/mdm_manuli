package com.sap.spc.remote.client.rfc.function; 

public interface CfgApiGetProductVariants {

    public static String FEATURE_LNAME     = "FEATURE_LNAME";
    public static String FEATURE_TYPE     = "FEATURE_TYPE";
    public static String FEATURE_WEIGHT     = "FEATURE_WEIGHT";
    public static String VARIANT_COUNT     = "VARIANT_COUNT";
    public static String VARIANT_PRODUCT_ID     = "VARIANT_PRODUCT_ID";
    public static String FEATURES     = "FEATURES";
    public static String FEATURE_VALUE     = "FEATURE_VALUE";
    public static String FEATURE_NAME     = "FEATURE_NAME";
    public static String TARGET_DATE     = "TARGET_DATE";
    public static String PRODUCT_VARIANTS     = "PRODUCT_VARIANTS";
    public static String DISTANCE     = "DISTANCE";
    public static String QUALITY     = "QUALITY";
    public static String FEATURE_LVALUE     = "FEATURE_LVALUE";
    public static String CONFIG_ID     = "CONFIG_ID";
    public static String FEATURE_ID     = "FEATURE_ID";
    public static String FEATURE_LONGTEXT     = "FEATURE_LONGTEXT";
    public static String FEATURE_DISTANCE     = "FEATURE_DISTANCE";
    public static String SEARCH_MODE     = "SEARCH_MODE";
    public static String MATCH_POINTS     = "MATCH_POINTS";
    public static String MAX_MATCH_POINT     = "MAX_MATCH_POINT";
    public static String LIMIT     = "LIMIT";

}
