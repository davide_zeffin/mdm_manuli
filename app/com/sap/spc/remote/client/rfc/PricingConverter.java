package com.sap.spc.remote.client.rfc;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.imp.rfc.RFCDefaultClient;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCSession;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.rfc.function.PrcCnvUnitF4PhysicalUnits;
import com.sap.spc.remote.client.rfc.function.PrcCnvCurrF4CurrencyUnits;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

import com.sap.mw.jco.JCO;

public class PricingConverter {
    
    // caches
    protected static Map internalToExternalUOM = new HashMap();
    protected static Map externalToInternalUOM = new HashMap();
    protected static Map numberOfDecimalsForUOMs = new HashMap();
    protected static Map externalUOMNamesAndDescriptions = new HashMap();
    protected static Map currencyDescriptions = new HashMap();
    protected static Map numberOfDecimalsForCurrencies = new HashMap();
    
  
    private static final Category category = ResourceAccessor.category;
    private static final Location location = ResourceAccessor.getLocation(PricingConverter.class);
    
    
	public static BigDecimal convertCurrencyValueExternalToInternal(String extValue, String unit, RfcDefaultIPCSession session, String decimalSeparator, String groupingSeparator) {
		String language = session.getLanguage();
		BigDecimal result = convertCurrencyValueExternalToInternal(extValue, unit, session, language, decimalSeparator, groupingSeparator);
		return result;
	}

    /**
     * Converts a currency value to the internal representation. Uses the currency unit to determine the 
     * scale. The session is used for correct language-dependent parsing of the currency value.  
     * Grouping separator and decimal separator can also be passed separately. These will have higher priority than
     * the language of the session.
     * @param extValue external currency value
     * @param unit currency unit
     * @param session current sesssion (contains data like language, JCO.Connection, etc.)
     * @param decimalSeparator (pass null if you want to use the session-language for correct parsing)
     * @param groupingSeparator (pass null if you want to use the session-language for correct parsing)
     * @return currency value as BigDecimal (internal representation)
     */
    public static BigDecimal convertCurrencyValueExternalToInternal(String extValue, String unit, RfcDefaultIPCSession session, String language, String decimalSeparator, String groupingSeparator) {
        if (extValue == null){
            category.logT(Severity.ERROR, location, "Currency value conversion: external value is null. Returning null.");
            return null;
        }
        if (extValue.equals("")){
            category.logT(Severity.ERROR, location, "Currency value conversion: external value is an empty string. Returning null.");
            return null;            
        }          
        int scale = 0;
        HashMap numberOfDecimals = getNumberOfDecimalsForCurrencies(session, language);
        if ((numberOfDecimals != null) && (unit != null)){
            scale = ((Integer) numberOfDecimals.get(unit)).intValue();
        }
        BigDecimal result = convertToBigDecimal(extValue, scale, language, decimalSeparator, groupingSeparator);
		if (location.beDebug()) {
			location.debugT(
				"Converted external currency value ("
					+ extValue
					+ unit
					+ ") to internal value. "
					+ result
					+ ". decimalSeparator:"
					+ decimalSeparator
					+ ", groupingSeparator"
					+ groupingSeparator);
		}
        return result;
    }
    
	/**
	 * @param internalValueBD
	 * @param string
	 * @param session
	 * @param decimalSeparator
	 * @param groupSeparator
	 * @return
	 */
	public static String convertCurrencyValueInternalToExternal(BigDecimal decimal, String unit, RfcDefaultIPCSession session, String decimalSeparator, String groupingSeparator) {
		String language = session.getLanguage();
		String result = convertCurrencyValueInternalToExternal(decimal, unit, session, language, decimalSeparator, groupingSeparator);
		return result;
	}
    
    
    /**
     * Converts a currency value as BigDecimal to the external representation. Uses the currency unit to determine the 
     * scale and the language of the session to determine grouping separator and decimal separator.
     * Grouping separator and decimal separator can also be passed. These will have higher priority than
     * the language of the session.
     * @param decimal internal currency value (BigDecimal
     * @param unit currency unit
     * @param session current sesssion (contains data like language, JCO.Connection, etc.)
     * @param language language
     * @param decimalSeparator (pass null if you want to use the session-language)
     * @param groupingSeparator (pass null if you want to use the session-language)
     * @param stripDecimalPlaces if set to true decimal places that are zero are striped (e.g. "3.00" becomes "3")
     * @return currency value as String (external representation)
     */    
    public static String convertCurrencyValueInternalToExternal(BigDecimal decimal, String unit, RfcDefaultIPCSession session, String language, String decimalSeparator, String groupingSeparator, boolean stripDecimalPlaces) {
        if (decimal == null){
            category.logT(Severity.ERROR, location, "Currency value conversion: internal value (BigDecimal) is null. Returning null.");
            return null;
        }
        if (stripDecimalPlaces) {
            BigDecimal decimalScale0 = decimal.setScale(0, BigDecimal.ROUND_DOWN);
            if (decimal.compareTo(decimalScale0) == 0) {
                return convertToString(decimalScale0, 0, language, decimalSeparator, groupingSeparator);
            }
        }
        int scale = 0;
        category.logT(Severity.DEBUG, location, "Converting internal currency value (" + decimal.toString() + unit + ") to external.");
        HashMap numberOfDecimals = getNumberOfDecimalsForCurrencies(session, language);
        if ((numberOfDecimals != null) && (unit != null)) {
            scale = ((Integer) numberOfDecimals.get(unit)).intValue();
        }
        String result = convertToString(decimal, scale, language, decimalSeparator, groupingSeparator);         
		if (location.beDebug()) {
			location.debugT(
				"Converted internal currency value ("
					+ decimal
					+ unit
					+ ") to external value. "
					+ result
					+ ". decimalSeparator:"
					+ decimalSeparator
					+ ", groupingSeparator"
					+ groupingSeparator
					+ ", scale:"
					+ scale
					+ ", language:"
					+ language);
		}
        return result;
    }
    
    
    /**
     * Converts a currency value as BigDecimal to the external representation. Uses the currency unit to determine the 
     * scale and the language of the session to determine grouping separator and decimal separator.
     * Grouping separator and decimal separator can also be passed. These will have higher priority than
     * the language of the session.
     * @param decimal internal currency value (BigDecimal
     * @param unit currency unit
     * @param session current sesssion (contains data like language, JCO.Connection, etc.)
     * @param language language
     * @param decimalSeparator (pass null if you want to use the session-language)
     * @param groupingSeparator (pass null if you want to use the session-language)
     * @return currency value as String (external representation)
     */    
    public static String convertCurrencyValueInternalToExternal(BigDecimal decimal, String unit, RfcDefaultIPCSession session, String language, String decimalSeparator, String groupingSeparator) {
        return convertCurrencyValueInternalToExternal(decimal, unit, session, language, decimalSeparator, groupingSeparator, false);
    }
    
	public static String convertUOMExternalToInternal(String extUnit, RfcDefaultIPCSession session) {
		String language = session.getLanguage();
		String result = convertUOMExternalToInternal(extUnit, session, language);
		return result;
	}
	
    /**
     * Converts the language dependent unit of measurement to the internal represenation
     * @param extUnit external represenation (language dependent) of the unit of measurement
     * @param session current session (contains data like language, etc.)
     * @return internal representation of unit of measurement
     */
    public static String convertUOMExternalToInternal(String extUnit, RfcDefaultIPCSession session, String language) {
        if (extUnit == null) {
            category.logT(Severity.ERROR, location, "Unit conversion: external unit is null. Returning null.");
            return null;
        }
        category.logT(Severity.DEBUG, location, "Converting external unit (" + extUnit + ") to internal.");
        // mapping for this language already retrieved?
        HashMap data = (HashMap) externalToInternalUOM.get(language);
        if (data == null){
            initMappingForLanguage(session, language);
            data = (HashMap) externalToInternalUOM.get(language);
        }
        String internalUnit = "";
        if (data != null) {
            internalUnit = (String) data.get(extUnit);
            if (internalUnit == null){
                category.logT(Severity.ERROR, location, "Unit conversion: Could not find appropriate mapping for external unit "+ extUnit + " and language " + language + ". Maybe unit and language does not fit. Returning null.");
            }
        }
        return internalUnit;
    }
    
	/**
	 * @param string
	 * @param session
	 * @return
	 */
	public static String convertUOMInternalToExternal(String unit, RfcDefaultIPCSession session) {
		String language = session.getLanguage();
		String result = convertUOMInternalToExternal(unit, session, language);
		return result;
	}

    /**
     * Converts the language independent unit of measurement to the external represenation (language dependent)
     * @param intUnit internal represenation (language independent) of the unit of measurement
     * @param session current session (contains data like language, etc.)
     * @return external representation of unit of measurement
     */    
    public static String convertUOMInternalToExternal(String intUnit, RfcDefaultIPCSession session, String language) {
        if (intUnit == null){
            category.logT(Severity.ERROR, location, "Unit conversion: internal unit is null. Returning null.");
            return null;
        }
        category.logT(Severity.DEBUG, location, "Converting internal unit (" + intUnit + ") to external.");
        // mapping for this language already retrieved?
        HashMap data = (HashMap) internalToExternalUOM.get(language);
        if (data == null){
            initMappingForLanguage(session, language);
            data = (HashMap) internalToExternalUOM.get(language);
        }
        String externalUnit = "";
        if (data != null) {
            externalUnit = (String) data.get(intUnit);
        }
        if (externalUnit == null) {
			category.logT(Severity.ERROR, location, "Unit conversion: external unit for [" + intUnit + "] is null. Returning empty string. Please check your customizing!");
        	externalUnit = "";
        }else if ("".equals(externalUnit)) {
			category.logT(Severity.ERROR, location, "Unit conversion: external unit for [" + intUnit + "] is empty string. Returning empty string. Please check your customizing!");
        }
        return externalUnit;
    }
    

	public static BigDecimal convertValueExternalToInternal(String extValue, String extUnit, RfcDefaultIPCSession session, String decimalSeparator, String groupingSeparator) {
		String language = session.getLanguage();
		BigDecimal result = convertValueExternalToInternal(extValue, extUnit, session, language, decimalSeparator, groupingSeparator);
		return result;
	}

    /**
     * Converts a value to the internal representation. Uses the unit of measurement to determine the 
     * scale. The session is used for correct language-dependent parsing of the value.
     * Grouping separator and decimal separator can also be passed separately. 
     * These will have higher priority than the language of the session.
     * @param extValue external value
     * @param extUnit external unit of measurement
     * @param session current sesssion (contains data like language, JCO.Connection, etc.)
     * @param decimalSeparator (pass null if you want to use the session-language for correct parsing)
     * @param groupingSeparator (pass null if you want to use the session-language for correct parsing)
     * @return value as BigDecimal (internal representation)
     */    
    public static BigDecimal convertValueExternalToInternal(String extValue, String extUnit, RfcDefaultIPCSession session, String language, String decimalSeparator, String groupingSeparator) {
        if (extValue == null){
            category.logT(Severity.ERROR, location, "Value conversion: external value is null. Returning null.");
            return null;
        }
        if (extValue.equals("")){
            category.logT(Severity.ERROR, location, "Value conversion: external value is an empty string. Returning null.");
            return null;            
        }          
        category.logT(Severity.DEBUG, location, "Converting external value (" + extValue + extUnit + ") to internal.");
        String internalUnit = convertUOMExternalToInternal(extUnit, session, language);
        BigDecimal intDecimal = convertUOMToBigDecimal(extValue, internalUnit, session, language, decimalSeparator, groupingSeparator);
        return intDecimal;
    }


	public static String convertValueInternalToExternal(BigDecimal intDecimal, String intUnit, RfcDefaultIPCSession session, String decimalSeparator, String groupingSeparator) {
		String language = session.getLanguage();
		String result = convertValueInternalToExternal(intDecimal, intUnit, session, language, decimalSeparator, groupingSeparator);
		return result;
	}

    /**
     * Converts a value as BigDecimal to the external representation. Uses the unit of measurement to determine the 
     * scale and the language of the session to determine grouping separator and decimal separator.
     * Grouping separator and decimal separator can also be passed. These will have higher priority than
     * the language of the session.
     * @param decimal internal  value (BigDecimal)
     * @param unit internal unit of measurement
     * @param session current sesssion (contains data like language, JCO.Connection, etc.)
     * @param language language
     * @param decimalSeparator (pass null if you want to use the session-language)
     * @param groupingSeparator (pass null if you want to use the session-language)
     * @param stripDecimalPlaces if set to true decimal places that are zero are striped (e.g. "3.00" becomes "3")
     * @return value as String (external representation)
     */    
    public static String convertValueInternalToExternal(BigDecimal intDecimal, String intUnit, RfcDefaultIPCSession session, String language, String decimalSeparator, String groupingSeparator, boolean stripDecimalPlaces) {
        if (intDecimal == null){
            category.logT(Severity.ERROR, location, "Value conversion: internal value (BigDecimal) is null. Returning null.");
            return null;
        }
        category.logT(Severity.DEBUG, location, "Converting internal value (" + intDecimal.toString() + intUnit + ") to internal.");        
        String extValue = convertUOMToString(intDecimal, intUnit, session, language, decimalSeparator, groupingSeparator, stripDecimalPlaces);
        return extValue;
    }

	
    /**
     * Converts a value as BigDecimal to the external representation. Uses the unit of measurement to determine the 
     * scale and the language of the session to determine grouping separator and decimal separator.
     * Grouping separator and decimal separator can also be passed. These will have higher priority than
     * the language of the session.
     * @param decimal internal  value (BigDecimal)
     * @param unit internal unit of measurement
     * @param session current sesssion (contains data like language, JCO.Connection, etc.)
     * @param language language
     * @param decimalSeparator (pass null if you want to use the session-language)
     * @param groupingSeparator (pass null if you want to use the session-language)
     * @return value as String (external representation)
     */    
    public static String convertValueInternalToExternal(BigDecimal intDecimal, String intUnit, RfcDefaultIPCSession session, String language, String decimalSeparator, String groupingSeparator) {
        return convertValueInternalToExternal(intDecimal, intUnit, session, language, decimalSeparator, groupingSeparator, true);
    }

    /**
     * Helper method. 
     * If the scale is 0 it uses this. Otherwise it requests the number of decimals for 
     * the unit of measurements.
     * Passes this information on to the real String-conversion method.
     * @param decimal
     * @param intUnit
     * @param session
     * @param decimalSeparator
     * @param groupingSeparator
     * @param stripDecimalPlaces if set to true decimal places that are zero are striped (e.g. "3.00" becomes "3")
     * @return String converted values
     */
    protected static String convertUOMToString(BigDecimal decimal, String intUnit, RfcDefaultIPCSession session, String language, String decimalSeparator, String groupingSeparator, boolean stripDecimalPlaces) {
        if (stripDecimalPlaces){
            BigDecimal decimalScale0 = decimal.setScale(0, BigDecimal.ROUND_DOWN);
            if (decimal.compareTo(decimalScale0) == 0) {
                return convertToString(decimalScale0, 0, language, decimalSeparator, groupingSeparator);
            }
        }
        int scale = 0;
        HashMap numberOfDecimals = getNumberOfDecimalsForUOMs(session, language);
        if ((numberOfDecimals != null) && (intUnit != null)) {
        	Object o = numberOfDecimals.get(intUnit);
        	if ( o != null){
				scale = ((Integer) numberOfDecimals.get(intUnit)).intValue();
        	}
        }        
        return convertToString(decimal, scale, language, decimalSeparator, groupingSeparator);
    }

    protected static BigDecimal convertUOMToBigDecimal(String value, String internalUnit, RfcDefaultIPCSession session, String language, String decimalSeparator, String groupingSeparator) {
        int scale = 0;
        HashMap numberOfDecimals = getNumberOfDecimalsForUOMs(session, language);
        if ((numberOfDecimals != null) && (internalUnit != null)) {
            scale = ((Integer) numberOfDecimals.get(internalUnit)).intValue();
        }
        return convertToBigDecimal(value, scale, language, decimalSeparator, groupingSeparator);
    }
  
    /**
     * Returns the number of decimals for units of measurements. Tries to use the cache first.
     * If this was not successful it retrieves the data from the function module.
     * @param session current session
     * @return HashMap containting the number of decimals for units of measurements for the session-language
     */
    protected static HashMap getNumberOfDecimalsForUOMs(RfcDefaultIPCSession session, String language) {
        HashMap data = (HashMap) numberOfDecimalsForUOMs.get(language);
        if (data == null){
            initMappingForLanguage(session, language);
            data = (HashMap) numberOfDecimalsForUOMs.get(language);
        }
        return data;
    }

	public static String[][] getExternalUOMNamesAndDescriptions(RfcDefaultIPCSession session) {
		String language = session.getLanguage();
		String[][] result = getExternalUOMNamesAndDescriptions(session, language);
		return result;
	}
    /**
     * Returns a list of external names and descriptions for units of measurements. Tries to use the cache first.
     * If this was not successful it retrieves the data from the function module.
     * @param session current session
     * @return String[][] array containting the external names of UOMS and descriptions for the session-language
     */
    public static String[][] getExternalUOMNamesAndDescriptions(RfcDefaultIPCSession session, String language) {
        String[][] data = (String[][]) externalUOMNamesAndDescriptions.get(language);
        if (data == null){
            initMappingForLanguage(session, language);
            data = (String[][]) externalUOMNamesAndDescriptions.get(language);
        }
        return data;
    }

    
    /**
     * Converts the given BigDecimal using scale and language (and maybe separators) to a String. 
     * @param decimal
     * @param scale
     * @param language
     * @param decimalSeparator
     * @param groupingSeparator
     * @return Value as string
     */
    protected static String convertToString(BigDecimal decimal, int scale, String language, String decimalSeparator, String groupingSeparator) {
        String pattern = "#,##0";
        if (scale > 0) {
            pattern += '.';
        }
            
        for (int i = 0; i < scale; i++) {
            pattern += "0";
        }

        DecimalFormatSymbols formatSymbols;
        if ((decimalSeparator != null) && (groupingSeparator != null) && !decimalSeparator.equals("") && !groupingSeparator.equals("")){
            formatSymbols = new DecimalFormatSymbols();
            formatSymbols.setDecimalSeparator(decimalSeparator.charAt(0));
            formatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));
        }
        else {
            formatSymbols = new DecimalFormatSymbols(new Locale(language));
        }
        DecimalFormat df = new DecimalFormat(pattern, formatSymbols);
        String value = df.format(decimal.doubleValue());
        return value;
    }
    
    /**
     * Returns the number of decimals for currencies. Tries to use the cache first.
     * If this was not successful it retrieves the data from the function module. 
     * @param session current session
     * @return HashMap with the number of decimals for the currencies (using session language)
     */
    protected static HashMap getNumberOfDecimalsForCurrencies(RfcDefaultIPCSession session, String language) {
        HashMap data = (HashMap) numberOfDecimalsForCurrencies.get(language);
        if (data == null){
            initMappingForLanguage(session, language);
            data = (HashMap) numberOfDecimalsForCurrencies.get(language);
        }
        return data;
    }
    
	public static String[][] getCurrencyDescriptions(RfcDefaultIPCSession session) {
		String language = session.getLanguage();
		String[][] result = getCurrencyDescriptions(session, language);
		return result;
	}

    /**
     * Returns a list of descriptions for currencies. Tries to use the cache first.
     * If this was not successful it retrieves the data from the function module. 
     * @param session current session
     * @return String[][] Array with the descriptions of currencies (using session language)
     */
    public static String[][] getCurrencyDescriptions(RfcDefaultIPCSession session, String language) {
        String[][] data = (String[][])currencyDescriptions.get(language);
        if (data == null){
            initMappingForLanguage(session, language);
            data = (String[][])currencyDescriptions.get(language);
        }
        return data;
    }    
    
    /**
     * Converts the given String using scale and language (and maybe separators) to a BigDecimal.
     * @param value
     * @param scale
     * @param language
     * @param decimalSeparator
     * @param groupingSeparator
     * @return
     */
    protected static BigDecimal convertToBigDecimal(String value, int scale, String language, String decimalSeparator, String groupingSeparator) {
        DecimalFormatSymbols formatSymbols;
        if ((decimalSeparator != null) && (groupingSeparator != null) && !decimalSeparator.equals("") && !groupingSeparator.equals("")){
            formatSymbols = new DecimalFormatSymbols();
            formatSymbols.setDecimalSeparator(decimalSeparator.charAt(0));
            formatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));
        }
        else {
            formatSymbols = new DecimalFormatSymbols(new Locale(language));
        }
        DecimalFormat df = new DecimalFormat("#,##0.000", formatSymbols);
        BigDecimal decimal = null;
        try {
            decimal = new BigDecimal(df.parse(value).doubleValue());
        } catch (ParseException ex) {
        	throw new IPCException(ex);
        }
        decimal = decimal.setScale(scale, BigDecimal.ROUND_HALF_UP); // set to a
        return decimal;
    }
    
    /**
     * Initializes the mapping for the given session-language.
     * @param session current session
     */
    protected static void initMappingForLanguage(RfcDefaultIPCSession session, String language) {
        try {
            JCO.Function function = ((RFCDefaultClient)session.getClient()).getFunction(IFunctionGroup.PRC_CNV_UNIT_F4_PHYSICAL_UNITS);
            
            category.logT(Severity.PATH, location, "executing RFC PRC_CNV_UNIT_F4_PHYSICAL_UNITS");
            ((RFCDefaultClient)session.getClient()).executeStateless(function, language);
            category.logT(Severity.PATH, location, "done with RFC PRC_CNV_UNIT_F4_PHYSICAL_UNITS");
            
            JCO.ParameterList exportParameterList = function.getExportParameterList();
            JCO.Table f4Help = exportParameterList.getTable(PrcCnvUnitF4PhysicalUnits.ET_PHYSICAL_UNIT);
            
            // cache the result
            HashMap internalToExternalUOMForLanguage = new HashMap(f4Help.getNumRows());
            HashMap externalToInternalUOMForLanguage = new HashMap(f4Help.getNumRows());
            HashMap numberOfDecimalsForUOMsForLanguage = new HashMap(f4Help.getNumRows());
            String[][] uOMNamesAndDescriptionsForLanguage = new String[f4Help.getNumRows()][2]; // use array to keep order
            
            for (int i = 0; i < f4Help.getNumRows(); i++) {
                f4Help.setRow(i);
                String internalName = f4Help.getString(PrcCnvUnitF4PhysicalUnits.INTERNAL_NAME);
                String externalName = f4Help.getString(PrcCnvUnitF4PhysicalUnits.EXTERNAL_NAME);
                String description = f4Help.getString(PrcCnvUnitF4PhysicalUnits.DESCRIPTION);
                int decimals = f4Help.getInt(PrcCnvUnitF4PhysicalUnits.DECIMALS);
                internalToExternalUOMForLanguage.put(internalName, externalName);
                externalToInternalUOMForLanguage.put(externalName, internalName);
                numberOfDecimalsForUOMsForLanguage.put(internalName, new Integer(decimals));
                uOMNamesAndDescriptionsForLanguage[i][0] = externalName;
                uOMNamesAndDescriptionsForLanguage[i][1] = description;                
            }
            
            // get number of decimals for currencies
            function = ((RFCDefaultClient)session.getClient()).getFunction(IFunctionGroup.PRC_CNV_CURR_F4_CURRENCY_UNITS);
            
            category.logT(Severity.PATH, location, "executing RFC PRC_CNV_CURR_F4_CURRENCY_UNITS");            
            ((RFCDefaultClient)session.getClient()).executeStateless(function, language);
            category.logT(Severity.PATH, location, "done with RFC PRC_CNV_CURR_F4_CURRENCY_UNITS");
            
            exportParameterList = function.getExportParameterList();
            f4Help = exportParameterList.getTable(PrcCnvCurrF4CurrencyUnits.ET_CURRENCY_UNIT);
            
            // cache the result
            HashMap numberOfDecimalsForCurrenciesForLanguage = new HashMap(f4Help.getNumRows());
            String[][] listOfCurrencyDescriptionsForLanguage = new String[f4Help.getNumRows()][2]; // use array to keep order
            
            for (int i = 0; i < f4Help.getNumRows(); i++) {
                f4Help.setRow(i);
                String internalName = f4Help.getString(PrcCnvCurrF4CurrencyUnits.NAME);
                String desc = f4Help.getString(PrcCnvCurrF4CurrencyUnits.DESCRIPTION);
                int decimals = f4Help.getInt(PrcCnvCurrF4CurrencyUnits.DECIMALS);
                numberOfDecimalsForCurrenciesForLanguage.put(internalName, new Integer(decimals));
                listOfCurrencyDescriptionsForLanguage[i][0] = internalName;
                listOfCurrencyDescriptionsForLanguage[i][1] = desc;                
            }
            
            // save with language as key
            internalToExternalUOM.put(language, internalToExternalUOMForLanguage);
            externalToInternalUOM.put(language, externalToInternalUOMForLanguage);
            numberOfDecimalsForUOMs.put(language, numberOfDecimalsForUOMsForLanguage);
            externalUOMNamesAndDescriptions.put(language, uOMNamesAndDescriptionsForLanguage);
            numberOfDecimalsForCurrencies.put(language, numberOfDecimalsForCurrenciesForLanguage);
            currencyDescriptions.put(language, listOfCurrencyDescriptionsForLanguage);
        }
        catch (ClientException ce) {
            throw new IPCException(ce);
        }
    }


}
