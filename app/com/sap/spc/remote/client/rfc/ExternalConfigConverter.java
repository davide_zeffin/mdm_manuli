package com.sap.spc.remote.client.rfc;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.imp.rfc.RfcConstants;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val_seq;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_inst_seq;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.spc.remote.client.util.cfg_ext_part_seq;
import com.sap.spc.remote.client.util.cfg_ext_price_key;
import com.sap.spc.remote.client.util.cfg_ext_price_key_seq;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_inst_seq_imp;
import com.sap.spc.remote.shared.ParameterSet;
import com.sap.tc.logging.Location;


/**
 * Provides methods that convert external configurations to JCO.structure
 * and backward.
 * This ExternalConfigConverter uses the 5.0 RFC naming (like HEADER-ROOT_ID, PART_OF-PARENT_ID, INSTANCES-INST_ID, etc.)
 * There are also other ExternalConfigConverters for other naming formats:
 * @see com.sap.spc.remote.client.tcp.ExternalConfigConverter (default naming)
 * @see com.sap.isa.ipc.ui.jsp.action.ExternalConfigConverter (BAPI naming)
 * It is possible to convert from each naming to each other naming via the getConfigs() method that returns 
 * ext_configuration object that can be used with the createConfig() methods. 
 */
public class ExternalConfigConverter implements ExtConfigConstants, RfcConstants {

    protected static Location location = ResourceAccessor.getLocation(ExternalConfigConverter.class); 
    
    protected static JCO.Table _getConfigPrices(JCO.Table conditionsTable,
                                                cfg_ext_price_key_seq seq)
                                                {
        location.entering("ExternalConfigConverter._getConfigPrices(JCO.Table, cfg_ext_price_key-seq)");
        for (Iterator e = seq.iterator(); e.hasNext();) {
            cfg_ext_price_key key = (cfg_ext_price_key)e.next();
            conditionsTable.appendRow();
            //a3-349tl: priceFactor is cut to have at most 15 digits
            double factor = key.get_factor();
            try{
                String priceFactor = key.getFactorAsChar15();
                conditionsTable.setValue(key.get_inst_id().toString(), EXT_PRICE_INSTANCE_ID);
                conditionsTable.setValue(priceFactor, EXT_PRICE_FACTOR);
                conditionsTable.setValue(key.get_key(), EXT_PRICE_KEY);
            }catch (JCO.AbapException ae){
                location.errorT("could not convert priceFactor="+factor);
                throw new IPCException(ae);   
            }
        }
        location.exiting("ExternalConfigConverter._getConfigPrices(JCO.Table, cfg_ext_price_key-seq)");
        return conditionsTable;
    }


    protected static JCO.Table _getConfigCsticsVals(JCO.Table csticValsTable, cfg_ext_cstic_val_seq seq) {
        location.entering("ExternalConfigConverter._getConfigCsticsVals(...)");
        Iterator enumeration = seq.iterator();
        while(enumeration.hasNext()) {
            cfg_ext_cstic_val item = (cfg_ext_cstic_val)enumeration.next();
            csticValsTable.appendRow();
            csticValsTable.lastRow();
            csticValsTable.setValue(""+item.get_inst_id(), EXT_VALUE_INSTANCE_ID);
            csticValsTable.setValue(item.get_charc(), EXT_VALUE_CSTIC_NAME);
            csticValsTable.setValue(item.get_charc_txt(), EXT_VALUE_CSTIC_LNAME);
            csticValsTable.setValue(item.get_value(), EXT_VALUE_NAME);
            csticValsTable.setValue(item.get_value_txt(), EXT_VALUE_LNAME);
            csticValsTable.setValue(item.get_author(), EXT_VALUE_AUTHOR);
        }
        location.exiting("ExternalConfigConverter._getConfigCsticsVals(...)");
        return csticValsTable;
    }


    protected static JCO.Table _getConfigInsts(JCO.Table instsTable, cfg_ext_inst_seq insts) {
        c_ext_cfg_inst_seq_imp insts_imp = (c_ext_cfg_inst_seq_imp)insts;
        location.entering("ExternalConfigConverter._getConfigInsts(...)");
        for (Enumeration enumeration = insts_imp.elements(); enumeration.hasMoreElements();) {
            cfg_ext_inst inst = (cfg_ext_inst)enumeration.nextElement();
            instsTable.appendRow();
            instsTable.lastRow();

            instsTable.setValue(""+inst.get_inst_id(), EXT_INST_ID);
            instsTable.setValue(inst.get_obj_type(), EXT_INST_OBJECT_TYPE);
            instsTable.setValue(inst.get_class_type(), EXT_INST_CLASS_TYPE);
            instsTable.setValue(inst.get_obj_key(), EXT_INST_OBJECT_KEY);
            instsTable.setValue(inst.get_obj_txt(), EXT_INST_OBJECT_TEXT);
            instsTable.setValue(inst.get_quantity(), EXT_INST_QUANTITY);
            instsTable.setValue(inst.get_quantity_unit(), EXT_INST_QUANTITY_UNIT);
            instsTable.setValue(inst.get_author(), EXT_INST_AUTHOR);
            instsTable.setValue(inst.is_complete_p() ? TRUE : FALSE, EXT_INST_COMPLETE);
            instsTable.setValue(inst.is_consistent_p() ? TRUE : FALSE, EXT_INST_CONSISTENT);

        }
        location.exiting("ExternalConfigConverter._getConfigInsts(...)");
        return instsTable;
    }


    protected static JCO.Table _getConfigParts(JCO.Table partsTable, cfg_ext_part_seq parts) {
        location.entering("ExternalConfigConverter._getConfigParts(...)");
        for (Iterator enumeration = parts.iterator(); enumeration.hasNext();) {
            cfg_ext_part part = (cfg_ext_part)enumeration.next();
            partsTable.appendRow();
            partsTable.lastRow();

            partsTable.setValue(""+part.get_parent_id(), EXT_PART_PARENT_ID);
            partsTable.setValue(""+part.get_inst_id(), EXT_PART_INST_ID);
            partsTable.setValue(part.get_pos_nr(), EXT_PART_POS_NR);
            partsTable.setValue(part.get_obj_type(), EXT_PART_OBJECT_TYPE);
            partsTable.setValue(part.get_class_type(), EXT_PART_CLASS_TYPE);
            partsTable.setValue(part.get_obj_key(), EXT_PART_OBJECT_KEY);
            partsTable.setValue(part.get_author(), EXT_PART_AUTHOR);
            partsTable.setValue(part.is_sales_relevant_p() ? SALES_RELEVANT:"", EXT_PART_SALES_RELEVANT);
        }
        location.exiting("ExternalConfigConverter._getConfigParts(...)");
        return partsTable;
    }




    protected static JCO.Structure _getConfigHeader(ext_configuration extCfg,
                                                JCO.Structure header){
        location.entering("ExternalConfigConverter._getConfigHeader(...)");

        header.setValue(""+extCfg.get_root_id(), EXT_CONFIG_ROOT_ID);
        header.setValue(SCE, EXT_CONFIG_SCE_MODE);
        header.setValue(extCfg.get_kb_name(), EXT_CONFIG_KB_NAME);
        header.setValue(extCfg.get_kb_version(), EXT_CONFIG_KB_VERSION);
        header.setValue(extCfg.get_kb_profile_name(), EXT_CONFIG_KB_PROFILE);
        header.setValue(""+extCfg.get_kb_build(), EXT_CONFIG_KB_BUILD);
        header.setValue(extCfg.get_cfg_info(), EXT_CONFIG_INFO);
        header.setValue(extCfg.get_language(), EXT_CONFIG_KB_LANGUAGE);
        header.setValue(extCfg.is_complete_p()? TRUE: FALSE, EXT_CONFIG_COMPLETE);
        header.setValue(extCfg.is_consistent_p()? TRUE: FALSE, EXT_CONFIG_CONSISTENT);
        location.exiting("ExternalConfigConverter._getConfigHeader(...)");
        return header;
    }
    
    
    /**
     * Fills a JCO.Structure with the data of a given external configuration object.
     * @param extCfg external configuration object
     * @param extConfigStructure JCO.Structure to be filled (EXT_CONFIG)
     */
    public static void getConfig(ext_configuration extCfg, 
                                    JCO.Structure extConfigStructure) {
        location.entering("ExternalConfigConverter.getConfig(...)");
        //SET HEADER DATA
        JCO.Structure header =extConfigStructure.getStructure(EXT_CONFIG_HEADER);
        header = _getConfigHeader(extCfg, header);
        extConfigStructure.setValue(header, EXT_CONFIG_HEADER);
        // SET PARTS
        JCO.Table parts = extConfigStructure.getTable(EXT_CONFIG_PART_OF);
        parts = _getConfigParts(parts,extCfg.get_parts());
        extConfigStructure.setValue(parts, EXT_CONFIG_PART_OF);
        // SET INSTANCES
        JCO.Table insts = extConfigStructure.getTable(EXT_CONFIG_INSTANCES);
        insts = _getConfigInsts(insts,extCfg.get_insts());
        extConfigStructure.setValue(insts, EXT_CONFIG_INSTANCES);        
        // SET CSTIC_VALUES
        JCO.Table csticVals = extConfigStructure.getTable(EXT_CONFIG_VALUES);
        csticVals = _getConfigCsticsVals(csticVals, extCfg.get_cstics_values());
        extConfigStructure.setValue(csticVals, EXT_CONFIG_VALUES);
        // SET VARIANT CONDITIONS
        JCO.Table conditions = extConfigStructure.getTable(EXT_CONFIG_VARIANT_CONDITIONS);
        conditions = _getConfigPrices(conditions,extCfg.get_price_keys());
        extConfigStructure.setValue(conditions, EXT_CONFIG_VARIANT_CONDITIONS);        
        location.exiting("ExternalConfigConverter.getConfig(...)");
    }



    public static ext_configuration createConfig(JCO.Structure extConfigStructure, String client) throws JCO.AbapException {
        location.entering("ExternalConfigConverter.createConfig(JCO.Structure, String)");

        //GET HEADER DATA
        // compute kbName and kbVersion if not present
        JCO.Structure header = extConfigStructure.getStructure(EXT_CONFIG_HEADER);
        String kbLogSysExt = header.getString(EXT_CONFIG_KB_LOGSYS);
        kbLogSysExt = _checkForNull(kbLogSysExt); 
        String kbNameExt = header.getString(EXT_CONFIG_KB_NAME);
        kbNameExt = _checkForNull(kbNameExt);
        String kbVersionExt = header.getString(EXT_CONFIG_KB_VERSION);
        kbVersionExt = _checkForNull(kbVersionExt);
        String kbConfigProfileExt = extConfigStructure.
            getStructure(EXT_CONFIG_HEADER).getString(EXT_CONFIG_KB_PROFILE);
        kbConfigProfileExt = _checkForNull(kbConfigProfileExt);
        String kbLogsys = header.getString(EXT_CONFIG_KB_LOGSYS);
        kbLogsys = _checkForNull(kbLogsys);
        Integer kbBuildInt = (Integer)header.getValue(EXT_CONFIG_KB_BUILD);
        int kbBuild = 1;
        if(kbBuildInt != null)
            kbBuild = kbBuildInt.intValue();

        String rootInstanceId = header.getString(EXT_CONFIG_ROOT_ID);
        rootInstanceId = _checkForNull(rootInstanceId);
        if (rootInstanceId == null){
            location.errorT("EXT_CONFIG_ROOT_ID missing");
            throw new JCO.AbapException(EXCEPTION_FORM, "REQUIRED_PARAMETER_NOT_FOUND " + EXT_CONFIG_ROOT_ID);
        }
        // remove trailing zeros
        int rootInstanceIdNum;
        try {
            rootInstanceIdNum = Integer.parseInt(rootInstanceId);
            rootInstanceId = Integer.toString(rootInstanceIdNum);
        }
        catch (NumberFormatException nfe) {
            location.errorT("malformed object: rootInstanceId="+rootInstanceId);
            throw new JCO.AbapException(EXCEPTION_FORM, "MALFORMED_OBJECT " 
                                        + rootInstanceId + " instanceId" );
        }

        //GET INSTANCES
        JCO.Table checkInstances = extConfigStructure.getTable(EXT_CONFIG_INSTANCES);

        if(checkInstances == null);
        if(checkInstances.getNumRows() == 0);
        for(int i=0; i<checkInstances.getNumRows(); i++){
            checkInstances.setRow(i);
            String instId = checkInstances.getString(EXT_INST_ID);
            int instIdNum;
            try {
                instIdNum = Integer.parseInt(instId);
            }
            catch(NumberFormatException e) {
                location.errorT("malformed object: instId="+instId);
                throw new JCO.AbapException(EXCEPTION_FORM, 
                "MALFORMED_OBJECT: instId=" + instId);
            }
        }
        // END of lengthy code that checks if instance ids are of type Integer
        
        Integer rootId = new Integer((header.getString(EXT_CONFIG_ROOT_ID)).trim());
        String consistent = header.getString(EXT_CONFIG_CONSISTENT);
        String language = header.getString(EXT_CONFIG_KB_LANGUAGE);
        consistent = _checkForNull(consistent);
        if(consistent == null)
            consistent = TRUE;
        String complete = header.getString(EXT_CONFIG_COMPLETE);
        complete = _checkForNull(complete);
        if(complete == null)
            complete = TRUE;
        c_ext_cfg_imp writable = new c_ext_cfg_imp("", // EXT_CONFIG_NAME is not filled in ABAP structure header.getString(EXT_CONFIG_NAME),
                                                   "", // SCE_VERSION is not filled in ABAP structure sceVersion,            
                                                    client,
                                                    kbNameExt,
                                                    kbVersionExt,
                                                    kbBuild,
                                                    kbConfigProfileExt,
                                                  // 20001127-kha: the KB language is now always ignored. Instead, the language of the document
                                                  // is used. This makes sense: If I configure in English, leave the system, come back in German
                                                  // to continue my configuration I don't want the saved language for the language dependent
                                                  // texts but the current document language that is consistent with the UI texts.
                                                  //request.getParameterValue(EXT_CONFIG_KB_LANGUAGE,kbLanguage),
                                                    language,
                                                    rootId,
                                                    consistent.equals(TRUE),
                                                    complete.equals(TRUE)
                                                    );
        String configInfo = header.getString(EXT_CONFIG_INFO);
        configInfo = _checkForNull(configInfo);
        if (configInfo != null)
            writable.set_cfg_info(configInfo);

        // add instance info
        JCO.Table instances = null;
        if(extConfigStructure.getMetaData().hasField(EXT_CONFIG_INSTANCES))
            instances = extConfigStructure.getTable(EXT_CONFIG_INSTANCES);

        if((instances == null)||
           (instances.getNumRows() == 0)){
            location.errorT("missing parameter: EXT_CONFIG_INSTANCES");
            throw new JCO.AbapException(EXCEPTION_FORM,
                    "REQUIRED PARAMETER NOT FOUND: INSTANCES");
        }
        for(int i=0; i<instances.getNumRows(); i++){
            instances.setRow(i);
            String instAuthor = instances.getString(EXT_INST_AUTHOR);
            instAuthor = _checkForNull(instAuthor);
            if (instAuthor == null)
                instAuthor = " ";
            String instId = instances.getString(EXT_INST_ID);
            instId = _checkForNull(instId);
            if(instId == null){
                location.errorT("missing parameter EXT_INST_ID");
                throw new JCO.AbapException(EXCEPTION_FORM,
                    "REQUIRED PARAMETER NOT FOUND: INST_ID");
            }
            String instConsistent = instances.getString(EXT_INST_CONSISTENT);
            instConsistent = _checkForNull(instConsistent);
            if(instConsistent == null)
                instConsistent = TRUE;
            String instComplete = instances.getString(EXT_INST_COMPLETE);
            instComplete = _checkForNull(instComplete);
            if(instComplete == null)
                instComplete = TRUE;
            String quantityUnit = instances.getString(EXT_INST_QUANTITY_UNIT);
            quantityUnit = _checkForNull(quantityUnit);
            if(quantityUnit == null)
                quantityUnit = "ST";
            writable.add_inst(new Integer(instId.trim()),
                            instances.getString(EXT_INST_OBJECT_TYPE),
                            instances.getString(EXT_INST_CLASS_TYPE),
                            instances.getString(EXT_INST_OBJECT_KEY), 
                            instances.getString(EXT_INST_OBJECT_TEXT),
                            instAuthor, 
                            instances.getString(EXT_INST_QUANTITY),
                            quantityUnit,
                            instConsistent.equals(TRUE),
                            instComplete.equals(TRUE)
                            );
        }

        // add part info (if available)
        JCO.Table parts = extConfigStructure.getTable(EXT_CONFIG_PART_OF);
        if(parts != null){
            for(int i=0; i<parts.getNumRows(); i++){
                parts.setRow(i);
                String partParentId = parts.getString(EXT_PART_PARENT_ID);
                partParentId = _checkForNull(partParentId);
                String partInstId = parts.getString(EXT_PART_INST_ID);
                partInstId = _checkForNull(partInstId);
                String partPosNr = parts.getString(EXT_PART_POS_NR);
                partPosNr = _checkForNull(partPosNr);
                String partObjType = parts.getString(EXT_PART_OBJECT_TYPE);
                partObjType = _checkForNull(partObjType);
                String partClassType = parts.getString(EXT_PART_CLASS_TYPE);
                String partObjKey = parts.getString(EXT_PART_OBJECT_KEY);
                partObjKey = _checkForNull(partObjKey);
                String partAuthor = parts.getString(EXT_PART_AUTHOR);
                partAuthor = _checkForNull(partAuthor);
                if(partAuthor == null)
                    partAuthor = " ";
                // the sales relevant array is not part of the BAPI interface, so we make it
                // optional. External configs and CRM provide it, BAPI will use the default
                String partSalesRelevant = parts.getString(EXT_PART_SALES_RELEVANT);
                partSalesRelevant = _checkForNull(partSalesRelevant);
                if(partSalesRelevant == null)
                    partSalesRelevant = " ";
                writable.add_part(new Integer(partParentId.trim()),
                              new Integer(partInstId.trim()),
                              partPosNr,
                              partObjType, partClassType,
                              partObjKey, partAuthor,
                              partSalesRelevant.equals(SALES_RELEVANT));
            }
        }

        // add cstic value info (if available)
        JCO.Table values = extConfigStructure.getTable(EXT_CONFIG_VALUES);
        if(values != null){
            for(int i=0; i<values.getNumRows(); i++){
                values.setRow(i);
                String valueInstId = values.getString(EXT_VALUE_INSTANCE_ID);
                valueInstId = _checkForNull(valueInstId);
                String valueCsticName = values.getString(EXT_VALUE_CSTIC_NAME);
                valueCsticName = _checkForNull(valueCsticName);
                String valueCsticLName = values.getString(EXT_VALUE_CSTIC_LNAME);
                valueCsticLName = _checkForNull(valueCsticLName);
                String valueName = values.getString(EXT_VALUE_NAME);
// mk: note 945003 (adaptation to AP-CFG change)
//              valueName = _checkForNull(valueName);                
                String valueLName = values.getString(EXT_VALUE_LNAME);
// mk: note 945003 (adaptation to AP-CFG change)
//              valueLName = _checkForNull(valueLName);               
                String valueAuthor = values.getString(EXT_VALUE_AUTHOR);
                valueAuthor = _checkForNull(valueAuthor);
                if(valueAuthor == null)
                    valueAuthor = " ";
                writable.add_cstic_value(new Integer(valueInstId.trim()),
                                         valueCsticName, valueCsticLName,
                                         valueName, valueLName,
                                         valueAuthor);
            }
        }

        // add price keys (if available)
        JCO.Table prices = extConfigStructure.getTable(EXT_CONFIG_VARIANT_CONDITIONS);
        if(prices != null){
            for(int i=0; i<prices.getNumRows(); i++){
                prices.setRow(i);
                String priceInstId = prices.getString(EXT_PRICE_INSTANCE_ID);
                String priceKey = prices.getString(EXT_PRICE_KEY);
                String priceFactor = prices.getString(EXT_PRICE_FACTOR);
                writable.add_price_key(new Integer(priceInstId.trim()),
                                       priceKey,
                                       Double.valueOf(priceFactor.trim()).doubleValue());
            }
        }

        location.exiting("ExternalConfigConverter.createConfig(JCO.Structure, String)");
        return writable;
    }


  protected static void setParameterValue(ParameterSet set, String param, boolean useIndices, int index, String value) {
        if (useIndices) {
            set.setValue(param, index, value);
        }
        else {
            set.setValue(param, value);
        }
    }

    protected static String _checkForNull(String parameter){
        if(parameter == null)
            return parameter;
        if(parameter.equals(""))
            return null;
        return parameter;
    }

    /**
     * Fills a parameter set with the data of a given external configuration object using RFC-naming.
     * @param   extCfg  Vector of ext_configuration
     * @param   instIdToItemIdTables Vector of (Hashtable mapping String instids to String itemIds). May be null if no item mapping is needed
     * @param   posExInt initial config position number (eg. 1)
     * @param   cfgId initial config id (eg. 1)
     * @param   something implementing ParameterSet that will be updated by this method.
     * @param   useIndices attach a numeric index [<number>] to each parameter name. If you want to use the result
     * in a server response, use true here, if you want to use this in a request, use false.
     * @return  nothing - param will be updated.
     */
    public static void getConfigs(Vector extCfgs, Vector instIdToItemIdTables, int posExInt, int cfgId, ParameterSet param, boolean useIndices) {

        //BAPICUCFG
        int initialCfgId = cfgId;

        Vector parts = new Vector();
        Vector insts = new Vector();
        Vector csticVals = new Vector();
        Vector conditions = new Vector();

        for (Enumeration e = extCfgs.elements(); e.hasMoreElements();) {
            ext_configuration extCfg = (ext_configuration)e.nextElement();

            setParameterValue(param, EXT_CONFIG_POSITION,    useIndices, cfgId, Integer.toString(posExInt)); // hard-coded to default-naming because EXT_CONFIG_POSITION is not available in the RFC naming
            posExInt++;
            setParameterValue(param, EXT_CONFIG_ID,        useIndices, cfgId, Integer.toString(cfgId)); 
            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_ROOT_ID,     useIndices, cfgId, ""+extCfg.get_root_id());
            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_SCE_MODE,    useIndices, cfgId, SCE);
            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_KB_NAME,     useIndices, cfgId, extCfg.get_kb_name());
            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_KB_VERSION,  useIndices, cfgId, extCfg.get_kb_version());
            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_KB_PROFILE,  useIndices, cfgId, extCfg.get_kb_profile_name());
            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_KB_BUILD,    useIndices, cfgId, ""+extCfg.get_kb_build());
            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_INFO,        useIndices, cfgId, extCfg.get_cfg_info());

            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_KB_LANGUAGE, useIndices, cfgId, extCfg.get_language());
            setParameterValue(param, EXT_CONFIG_NAME,        useIndices, cfgId, "");
            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_COMPLETE,    useIndices, cfgId, extCfg.is_complete_p()? TRUE: FALSE);

            setParameterValue(param, EXT_CONFIG_HEADER + "-" + EXT_CONFIG_CONSISTENT,  useIndices, cfgId, extCfg.is_consistent_p()? TRUE: FALSE);

            setParameterValue(param, EXT_CONFIG_SCE_VERSION, useIndices, cfgId, extCfg.get_sce_version());

            parts.addElement(extCfg.get_parts());
            insts.addElement(extCfg.get_insts());
            csticVals.addElement(extCfg.get_cstics_values());
            conditions.addElement(extCfg.get_price_keys());
            cfgId++;
}
        _getConfigPartsForParamSet(parts, initialCfgId, param, useIndices);
        _getConfigInstsForParamSet(insts, initialCfgId, param, useIndices);
        if (instIdToItemIdTables != null) {
            _getConfigItemsForParamSet(insts, instIdToItemIdTables, initialCfgId, param, useIndices);
        }
        _getConfigCsticsValsForParamSet(csticVals, initialCfgId, param, useIndices);
        _getConfigPricesForParamSet(conditions, initialCfgId, param, useIndices);
    }
    
    protected static void _getConfigPartsForParamSet(Vector partsVec, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;

        for (Enumeration e=partsVec.elements(); e.hasMoreElements();) {
            cfg_ext_part_seq parts = (cfg_ext_part_seq)e.nextElement();
            for (Iterator enumeration = parts.iterator(); enumeration.hasNext();) {
                cfg_ext_part part = (cfg_ext_part)enumeration.next();

                index++;
                setParameterValue(param, EXT_PART_CONFIG_ID,       useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, EXT_CONFIG_PART_OF + "-" + EXT_PART_PARENT_ID,       useIndices, index, ""+part.get_parent_id());
                setParameterValue(param, EXT_CONFIG_PART_OF + "-" + EXT_PART_INST_ID,         useIndices, index, ""+part.get_inst_id());
                setParameterValue(param, EXT_CONFIG_PART_OF + "-" + EXT_PART_POS_NR,          useIndices, index, part.get_pos_nr());
                setParameterValue(param, EXT_CONFIG_PART_OF + "-" + EXT_PART_OBJECT_TYPE,     useIndices, index, part.get_obj_type());
                setParameterValue(param, EXT_CONFIG_PART_OF + "-" + EXT_PART_CLASS_TYPE,      useIndices, index, part.get_class_type());
                setParameterValue(param, EXT_CONFIG_PART_OF + "-" + EXT_PART_OBJECT_KEY,      useIndices, index, part.get_obj_key());
                setParameterValue(param, EXT_CONFIG_PART_OF + "-" + EXT_PART_AUTHOR,          useIndices, index, part.get_author());
                setParameterValue(param, EXT_CONFIG_PART_OF + "-" + EXT_PART_SALES_RELEVANT,  useIndices, index, part.is_sales_relevant_p() ? SALES_RELEVANT:"");
            }
            cfgId++;
        }
    }

    protected static void _getConfigInstsForParamSet(Vector instances, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;

        for (Enumeration e=instances.elements(); e.hasMoreElements();) {
            c_ext_cfg_inst_seq_imp insts = (c_ext_cfg_inst_seq_imp)e.nextElement();
            for (Enumeration enumeration = insts.elements(); enumeration.hasMoreElements();) {
                cfg_ext_inst inst = (cfg_ext_inst)enumeration.nextElement();
                index++;

                setParameterValue(param, EXT_INST_CONFIG_ID,        useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_ID,               useIndices, index, ""+inst.get_inst_id());
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_OBJECT_TYPE,      useIndices, index, inst.get_obj_type());
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_CLASS_TYPE,       useIndices, index, inst.get_class_type());
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_OBJECT_KEY,       useIndices, index, inst.get_obj_key());
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_OBJECT_TEXT,      useIndices, index, inst.get_obj_txt());
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_QUANTITY,         useIndices, index, inst.get_quantity());
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_QUANTITY_UNIT,    useIndices, index, inst.get_quantity_unit());
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_AUTHOR,           useIndices, index, inst.get_author());
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_COMPLETE,         useIndices, index, inst.is_complete_p() ? TRUE : FALSE);
                setParameterValue(param, EXT_CONFIG_INSTANCES + "-" + EXT_INST_CONSISTENT,       useIndices, index, inst.is_consistent_p() ? TRUE : FALSE);
            }
            cfgId++;
        }
    }
    
    protected static void _getConfigItemsForParamSet(Vector instances, Vector instIdToItemIdTables, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;
        Enumeration tables = instIdToItemIdTables.elements();
        for (Enumeration e=instances.elements(); e.hasMoreElements();) {
            c_ext_cfg_inst_seq_imp insts = (c_ext_cfg_inst_seq_imp)e.nextElement();
            Hashtable instIdToItemIdTable = (Hashtable)tables.nextElement();
            for (Enumeration enumeration = insts.elements(); enumeration.hasMoreElements();) {
                cfg_ext_inst inst = (cfg_ext_inst)enumeration.nextElement();

                String instId = Integer.toString(inst.get_inst_id().intValue());
                String itemId = (String)instIdToItemIdTable.get(instId);

                index++;
                setParameterValue(param, EXT_IDS_CONFIG_ID, useIndices, index, Integer.toString(cfgId)); 
                setParameterValue(param, EXT_IDS + "-" + EXT_IDS_INST_ID,   useIndices, index, instId); 
                setParameterValue(param, EXT_IDS + "-" + EXT_IDS_GUID,   useIndices, index, itemId);
            }
            cfgId++;
        }
    }

    protected static void _getConfigCsticsValsForParamSet(Vector csticVals, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;

        for (Enumeration e=csticVals.elements(); e.hasMoreElements();) {
            cfg_ext_cstic_val_seq seq = (cfg_ext_cstic_val_seq)e.nextElement();
            for (Iterator enumeration = seq.iterator(); enumeration.hasNext();) {
                index++;

                cfg_ext_cstic_val item = (cfg_ext_cstic_val)enumeration.next();

                setParameterValue(param, EXT_VALUE_CONFIG_ID,   useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, EXT_CONFIG_VALUES + "-" + EXT_VALUE_INSTANCE_ID, useIndices, index, ""+item.get_inst_id());
                setParameterValue(param, EXT_CONFIG_VALUES + "-" + EXT_VALUE_CSTIC_NAME,  useIndices, index, item.get_charc());
                setParameterValue(param, EXT_CONFIG_VALUES + "-" + EXT_VALUE_CSTIC_LNAME, useIndices, index, item.get_charc_txt());
                setParameterValue(param, EXT_CONFIG_VALUES + "-" + EXT_VALUE_NAME,        useIndices, index, item.get_value());
                setParameterValue(param, EXT_CONFIG_VALUES + "-" + EXT_VALUE_LNAME,       useIndices, index, item.get_value_txt());
                setParameterValue(param, EXT_CONFIG_VALUES + "-" + EXT_VALUE_AUTHOR,      useIndices, index, item.get_author());
            }
            cfgId++;
        }
    }

    protected static void _getConfigPricesForParamSet(Vector priceSeqs, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;
        for (Enumeration enum=priceSeqs.elements(); enum.hasMoreElements();) {
            cfg_ext_price_key_seq seq = (cfg_ext_price_key_seq)enum.nextElement();

            for (Iterator e = seq.iterator(); e.hasNext();) {
                cfg_ext_price_key key = (cfg_ext_price_key)e.next();
                index++;
                //a3-349tl: priceFactor is cut to have at most 15 digits
                double factor = key.get_factor();
                String priceFactor = key.getFactorAsChar15();
                setParameterValue(param, EXT_PRICE_CONFIG_ID,   useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, EXT_CONFIG_VARIANT_CONDITIONS + "-" + EXT_PRICE_INSTANCE_ID, useIndices, index, key.get_inst_id().toString());
                setParameterValue(param, EXT_CONFIG_VARIANT_CONDITIONS + "-" + EXT_PRICE_FACTOR,      useIndices, index, priceFactor);
                setParameterValue(param, EXT_CONFIG_VARIANT_CONDITIONS + "-" + EXT_PRICE_KEY,         useIndices, index, key.get_key());
            }
            cfgId++;
        }
    }
    
    /**
     * Convert Configuration coming from the Lord API. It is assumed, 
     * that only the config for one item is contained in the tables.
     * 
     * The ipcContextAttributes HasMap is filled with ipcContextAttributes
     * from the given config tables.
     * 
     * @param etVcfgInst the instance information coming from the LORD API
     * @param etVcfgChar the characteristics information coming from the LORD API
     * @param etVcfgRefChar the context attribute information coming from the LORD API
     * @param posNr  the position number of the related item
     * @param ipcContextAttributes HasMap that will be filled with ipc context attributes for the
     *        item (old values will be deleted).
     * @return ext_configuration the external configuration
     * @throws JCO.AbapException
     */
	public static ext_configuration createLrdConfig(JCO.Table etVcfgInst, 
	                                                JCO.Table etVcfgChar,
	                                                JCO.Table etVcfgRefChar,
	                                                String posNr, 
	                                                HashMap ipcContextAttributes) throws JCO.AbapException {
		location.entering("ExternalConfigConverter.createLrdConfig(JCO.Table, JCO.Table, String, HashMap)");
		
		c_ext_cfg_imp writable = new c_ext_cfg_imp("", // EXT_CONFIG_NAME is not filled in ABAP structure header.getString(EXT_CONFIG_NAME),
												   "", // SCE_VERSION is not filled in ABAP structure sceVersion,            
												   "", // Client
												   "", // kb_name
												   "", // kb_version
													0, // kb_build
												   "", // kb_profile
												   "", // language
												   new Integer(0), // rootid
												   true, // consistent
												   true  // complete
                                                   );

		String rootId = null;
		boolean consistent = true;
		boolean complete = true;
		
		//GET INSTANCES
		if (etVcfgInst == null || etVcfgInst.getNumRows() == 0) {
			location.errorT("missing parameter: etVcfgInst");
			throw new JCO.AbapException(EXCEPTION_FORM,
					"REQUIRED PARAMETER NOT FOUND: etVcfgInst");
		}

		if (ipcContextAttributes == null) {
			ipcContextAttributes = new HashMap();
		}
		else {
			ipcContextAttributes.clear();
		}
		
		for (int i=0; i < etVcfgInst.getNumRows(); i++) {
			etVcfgInst.setRow(i);
			String instId = etVcfgInst.getString("INSTANCE");
			int instIdNum;
			try {
				instIdNum = Integer.parseInt(instId);
			}
			catch(NumberFormatException e) {
				location.errorT("malformed object: instId="+instId);
				throw new JCO.AbapException(EXCEPTION_FORM, 
				"MALFORMED_OBJECT: instId=" + instId);
			}
		}
		// END of lengthy code that checks if instance ids are of type Integer
		
		String instAuthor = " ";
		String instId = null;
		
		for (int i=0; i < etVcfgInst.getNumRows(); i++) {
			etVcfgInst.setRow(i);
			
			instId = etVcfgInst.getString("INSTANCE");
			instId = _checkForNull(instId);
			if (instId == null) {
				location.errorT("missing parameter INSTANCE");
				throw new JCO.AbapException(EXCEPTION_FORM,
					"REQUIRED PARAMETER NOT FOUND: INSTANCE");
			}
			
			String instInConsistent = etVcfgInst.getString("INCONSISTENT");
			instInConsistent = _checkForNull(instInConsistent);
			if (instInConsistent == null || XFLAG.equalsIgnoreCase(instInConsistent)){
				instInConsistent = FALSE;
			}
			
			String instInComplete = etVcfgInst.getString("INCOMPLETE");
			instInComplete = _checkForNull(instInComplete);
			if (instInComplete == null || XFLAG.equalsIgnoreCase(instInComplete)) {
				instInComplete = FALSE;
			}
			
			String quantityUnit = etVcfgInst.getString("COMPONENT_UNIT");
			quantityUnit = _checkForNull(quantityUnit);
			
			String objectType = etVcfgInst.getString("OBJECT_CATEGORY");
			objectType = _checkForNull(objectType);
			if ("M".equals(objectType)) {
				objectType = "MARA";
			}
			else if ("C".equals(objectType)) {
				objectType = "KLAH";
			}
            else {
                location.errorT("Object category neither M nor C but " + objectType);
            }
			
			String classType = etVcfgInst.getString("CLASS_TYPE");
			classType = _checkForNull(classType);
			if (classType == null && "MARA".equals(objectType)) {
				classType = "300";
			}
			
			String objectKey = etVcfgInst.getString("COMPONENT");
			objectKey = _checkForNull(objectKey);
            
            if (location.beDebug()) {
                location.debugT("Create instance with: " +
                                " instId:" + instId +
                                " objectType:" + objectType +
                                " classType:" + classType +
                                " objectKey:" + objectKey +
                                " objectText:" + etVcfgInst.getString("COMPONENT_T") +
                                " instAuthor:" + instAuthor +
                                " quantity:" + etVcfgInst.getString("COMPONENT_QTY") +
                                " quantityUnit:" + quantityUnit +
                                " consistent:" + instInConsistent.equals(FALSE) +
                                " complete:" + instInComplete.equals(FALSE)
                                );
            }
			
			writable.add_inst(new Integer(instId.trim()),
			                  objectType,
			                  classType,
			                  objectKey,
			                  etVcfgInst.getString("COMPONENT_T"), //obj_txt
							  instAuthor, 
			                  etVcfgInst.getString("COMPONENT_QTY"),
							  quantityUnit,
			                  instInConsistent.equals(FALSE),
			                  instInComplete.equals(FALSE));
			                  
			complete = complete && instInComplete.equals(FALSE);
			consistent = consistent && instInConsistent.equals(FALSE);
							 
            // handle parts info for the item
            String parentId = etVcfgInst.getString("PARENT");
            
            if ("00000000".equals(parentId)) {
            	parentId = null;
            }
			parentId = _checkForNull(parentId);
			
			if (parentId != null) {
                
                if (location.beDebug()) {
                    location.debugT("Create part with: " +
                                    " parentId:" + parentId +
                                    " instId:" + instId +
                                    " posNr:" + etVcfgInst.getString("ITEM_NUMBER") +
                                    " objectType:" + objectType +
                                    " classType:" + classType +
                                    " objectKey:" + objectKey +
                                    " instAuthor:" + instAuthor
                                    );
                }
                
				writable.add_part(new Integer(parentId.trim()),
				                  new Integer(instId.trim()),
                                  etVcfgInst.getString("ITEM_NUMBER"),
				                  objectType, 
				                  classType,
				                  objectKey, 
				                  instAuthor,
							      true);
			}
			else {
				rootId = instId;
			}
		}
		
		writable.set_completeness(complete);
		writable.set_consistency(consistent);
        
        if (location.beDebug()) {
            location.debugT("Set Compeete: " + complete + " consistent " + consistent);
        }
		
		if (rootId != null && rootId.trim().length() > 0) {
			writable.set_root_id(new Integer(rootId));
            
            if (location.beDebug()) {
                location.debugT("Set RootId: " + rootId);
            }
		}

		// add cstic value info (if available)
		if (etVcfgChar != null) {
			
			HashMap contextHelpMap = null;
		
			// process context attributes
			if (etVcfgRefChar != null && etVcfgRefChar.getNumRows() > 0) {
			
				contextHelpMap = new HashMap(etVcfgRefChar.getNumRows());
			
				for (int i=0; i < etVcfgRefChar.getNumRows(); i++) {
					etVcfgRefChar.setRow(i);
				
					String attName = etVcfgRefChar.getString("ATNAM");
					String attTab= etVcfgRefChar.getString("ATTAB");
					String attFel = etVcfgRefChar.getString("ATFEL");
                    
                    if ("SDCOM".equals(attTab) && "VKOND".equals(attFel)) {
                        if (location.beDebug()) {
                            location.debugT("Skipped context attribute" + attName);
                        }
                        continue;
                    }
				
					if (attFel != null && attFel.length() > 0) {
						attTab = attTab + "-" + attFel;
					}
				     
                    if (location.beDebug()) {
                        location.debugT("Context attribute " + attTab + " added for Attribute " + attName);
                    }
				
					contextHelpMap.put(attName, attTab);
				}	
			}
			
			for (int i=0; i < etVcfgChar.getNumRows(); i++) {
				etVcfgChar.setRow(i);
				
				String valueInstId = etVcfgChar.getString("INSTANCE");
				valueInstId = _checkForNull(valueInstId);
				
				String valueCsticName = etVcfgChar.getString("ATNAM");
				valueCsticName = _checkForNull(valueCsticName);
				
				String valueCsticLName = etVcfgChar.getString("ATNAM_T");
				valueCsticLName = _checkForNull(valueCsticLName);
				
				String valueName = etVcfgChar.getString("ATVAL");
                valueName = _checkForNull(valueName); 
                
                if (valueName == null) {
                    valueName = "";
                }
                               
				String valueLName = etVcfgChar.getString("ATVAL_T");  
            
				String valueAuthor = etVcfgChar.getString("ATAUT");
				valueAuthor = _checkForNull(valueAuthor);
				
				if (valueAuthor == null) {
                    valueAuthor = " ";
				}
                
                if (location.beDebug()) {
                    location.debugT("Create instance with: " +
                                    " valueInstId:" + valueInstId +
                                    " valueCsticName:" + valueCsticName +
                                    " valueCsticLName:" + valueCsticLName +
                                    " valueName:" + valueName +
                                    " valueAuthor:" + valueAuthor
                                    );
                }
						
				writable.add_cstic_value(new Integer(valueInstId.trim()),
										 valueCsticName, 
										 valueCsticLName,
										 valueName, 
										 valueLName,
										 valueAuthor);
                // Add context values
                if (contextHelpMap != null) {
                	String contextAttr = (String) contextHelpMap.get(valueCsticName);
                	
                	if (contextAttr != null) {
                        if (location.beDebug()) {
						    location.debugT("Context attribute " + contextAttr + " set with value " + valueName);
                        }
                		ipcContextAttributes.put(contextAttr, valueName);
                	}
                }
			}
		}
		
		location.exiting("ExternalConfigConverter.createLrdConfig(JCO.Table, JCO.Table, String, HashMap");
		return writable;
	}
	
	/**
	 * Fills the LORD Configuration tables from the external configuration.
	 * 
	 * @param extCfg external configuration object
	 * @param etVcfgInstExt JCO.Table to be filled for the instances
	 * @param etVcfgCharExt JCO.Table to be filled for the caharcteristics
	 */
	public static void getLrdConfig(ext_configuration extCfg, 
	                                JCO.Table etVcfgInstExt, 
					                JCO.Table etVcfgCharExt) {
		location.entering("ExternalConfigConverter.getLrdConfig(...)");
		//SET INSTANCE DATA
		
		if (etVcfgInstExt == null ) {
			location.errorT("missing parameter: etVcfgInstExt");
			throw new JCO.AbapException(EXCEPTION_FORM,
					"REQUIRED PARAMETER NOT FOUND: etVcfgInstExt");
		}
		
		if (etVcfgCharExt == null ) {
			location.errorT("missing parameter: etVcfgCharExt");
			throw new JCO.AbapException(EXCEPTION_FORM,
					"REQUIRED PARAMETER NOT FOUND: etVcfgCharExt");
		}
		
		etVcfgInstExt.deleteAllRows();
		etVcfgCharExt.deleteAllRows();

        HashMap partsMap = null;
        cfg_ext_part part = null;
        String instId = null;
        
        // create a part Map, son Information can easier be foudn when the instances are traversed
        if (!extCfg.get_parts().isEmpty()) {
            
            location.debugT("Handling parts");
            
            partsMap = new HashMap(2);
            
            for (Iterator enumeration = extCfg.get_parts().iterator(); enumeration.hasNext();) {
                part = (cfg_ext_part) enumeration.next();
                instId = part.get_inst_id().toString();
                partsMap.put(instId, part);
                
                if (location.beDebug()) {
                    location.debugT("Adding part Mapentry for instance " + instId);
                }
            }
        }
        
        // process instances
        c_ext_cfg_inst_seq_imp insts_imp = (c_ext_cfg_inst_seq_imp) extCfg.get_insts();
		
		for (Enumeration enumeration = insts_imp.elements(); enumeration.hasMoreElements();) {
			cfg_ext_inst inst = (cfg_ext_inst)enumeration.nextElement();
			etVcfgInstExt.appendRow();
			etVcfgInstExt.lastRow();
            
            instId = inst.get_inst_id().toString();

			etVcfgInstExt.setValue(instId, "INSTANCE");
            
            if (partsMap != null && partsMap.containsKey(instId)) {
                part = (cfg_ext_part) partsMap.get(instId);
                etVcfgInstExt.setValue("" + part.get_parent_id(), "PARENT");
                etVcfgInstExt.setValue(part.get_pos_nr(), "ITEM_NUMBER"); 
                if (location.beDebug()) {
                    location.debugT("Part Mapentry found for instance " + instId);
                }
            }
            
            // ITEM_INODE should be default	
			etVcfgInstExt.setValue("X", "CRESET");
            
            if (location.beDebug()) {
                location.debugT("VcfgInstExt Entry created Inst:" + etVcfgInstExt.getString("INSTANCE") + 
                                " Parent:" + etVcfgInstExt.getString("PARENT") +
                                " ItemNo:" + etVcfgInstExt.getString("ITEM_NUMBER") +
                                " ItemInode:" + etVcfgInstExt.getString("ITEM_INODE") +
                                " Creset:" + etVcfgInstExt.getString("CRESET"));
            }
		}
        
        if (partsMap != null) {
            partsMap.clear();
        }
		
        
        // process characteristics
		for (Iterator enumeration = extCfg.get_cstics_values().iterator(); enumeration.hasNext();) {
			cfg_ext_cstic_val item = (cfg_ext_cstic_val)enumeration.next();
			etVcfgCharExt.appendRow();
			etVcfgCharExt.lastRow();
			
			etVcfgCharExt.setValue("" + item.get_inst_id(), "INSTANCE");
			etVcfgCharExt.setValue(item.get_charc(), "ATNAM");
			etVcfgCharExt.setValue(item.get_value(), "ATVAL");
			etVcfgCharExt.setValue(item.get_author(), "ATAUT");
            // UPMOD should be default	
            
            if (location.beDebug()) {
                location.debugT("VcfgCharExt Entry created Inst:" + etVcfgCharExt.getString("INSTANCE") + 
                                " Atnam:" + etVcfgCharExt.getString("ATNAM") +
                                " Atval:" + etVcfgCharExt.getString("ATVAL") +
                                " Upmod:" + etVcfgCharExt.getString("UPDMOD") +
				                " Ataut:" + etVcfgCharExt.getString("ATAUT"));
            }
		}       
		location.exiting("ExternalConfigConverter.getLrdConfig(...)");
	}

}
