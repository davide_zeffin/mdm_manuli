package com.sap.spc.remote.client.rfc;

/**
 * This interface holds constants of the names used for external
 * configuration data.  The constants are used by the commands
 */
public interface ExtConfigConstants {
    public final static String EXT_CONFIG               ="EXT_CONFIG";
    public final static String EXT_CONFIG_HEADER        ="HEADER";
    public final static String EXT_CONFIG_PART_OF       ="PART_OF";
    public final static String EXT_CONFIG_INSTANCES     ="INSTANCES";
    public final static String EXT_CONFIG_VALUES        ="VALUES";
    public final static String EXT_CONFIG_VARIANT_CONDITIONS        ="VARIANT_CONDITIONS";
    public final static String EXT_IDS                  ="EXT_IDS";
    
    public final static String EXT_CONFIG_POSITION      = "extConfigPositions"; // not available in RFC naming! Fallback to default naming.
    public final static String EXT_CONFIG_ID            = "extConfigIds"; // not available in RFC naming! Fallback to default naming.
    public final static String EXT_CONFIG_ROOT_ID       = "ROOT_ID";
    public final static String EXT_CONFIG_SCE_MODE      = "SCE";
    public final static String EXT_CONFIG_KB_LOGSYS     = "LOGSYS";
    public final static String EXT_CONFIG_KB_NAME       = "NAME";
    public final static String EXT_CONFIG_KB_VERSION    = "VERSION";
    public final static String EXT_CONFIG_KB_PROFILE    = "PROFILE";
    public final static String EXT_CONFIG_KB_BUILD      = "BUILD";
    public final static String EXT_CONFIG_KB_LANGUAGE   = "LANGUAGE";
    public final static String EXT_CONFIG_NAME          = "NAME";       
    public final static String EXT_CONFIG_INFO          = "INFO";       
    public final static String EXT_CONFIG_COMPLETE      = "COMPLETE";
    public final static String EXT_CONFIG_CONSISTENT    = "CONSISTENT";     
    public final static String EXT_CONFIG_SCE_VERSION   = "VERSION";            

    public final static String EXT_PART_CONFIG_ID       = "extPartConfigIds"; // not available in RFC naming! Fallback to default naming.  
    public final static String EXT_PART_PARENT_ID       = "PARENT_ID";
    public final static String EXT_PART_INST_ID         = "INST_ID";
    public final static String EXT_PART_POS_NR          = "PART_OF_NO";
    public final static String EXT_PART_OBJECT_TYPE     = "OBJ_TYPE";
    public final static String EXT_PART_CLASS_TYPE      = "CLASS_TYPE";
    public final static String EXT_PART_OBJECT_KEY      = "OBJ_KEY";
    public final static String EXT_PART_AUTHOR          = "AUTHOR"; 
    public final static String EXT_PART_SALES_RELEVANT  = "SALES_RELEVANT";
    
    public static final String EXT_INST_CONFIG_ID       = "extInstConfigIds"; // not available in RFC naming! Fallback to default naming.
    public static final String EXT_INST_ID              = "INST_ID";
    public static final String EXT_INST_OBJECT_TYPE     = "OBJ_TYPE";
    public static final String EXT_INST_CLASS_TYPE      = "CLASS_TYPE";
    public static final String EXT_INST_OBJECT_KEY      = "OBJ_KEY";
    public static final String EXT_INST_OBJECT_TEXT     = "OBJ_TXT";
    public static final String EXT_INST_QUANTITY        = "QUANTITY";   
    public static final String EXT_INST_QUANTITY_UNIT   = "QUANTITY_UNIT";  
    public static final String EXT_INST_AUTHOR          = "AUTHOR";
    public static final String EXT_INST_COMPLETE        = "COMPLETE";
    public static final String EXT_INST_CONSISTENT      = "CONSISTENT";
    
    public final static String EXT_VALUE_CONFIG_ID      = "extValueConfigIds"; // not available in RFC naming! Fallback to default naming.
    public final static String EXT_VALUE_INSTANCE_ID    = "INST_ID";
    public final static String EXT_VALUE_CSTIC_NAME     = "CHARC";
    public final static String EXT_VALUE_CSTIC_LNAME    = "CHARC_TXT";
    public final static String EXT_VALUE_NAME           = "VALUE";
    public final static String EXT_VALUE_LNAME          = "VALUE_TXT";
    public final static String EXT_VALUE_AUTHOR         = "AUTHOR"; 
    
    public final static String EXT_PRICE_CONFIG_ID      = "extPriceConfigIds"; // not available in RFC naming! Fallback to default naming.
    public final static String EXT_PRICE_INSTANCE_ID    = "INST_ID";
    public final static String EXT_PRICE_FACTOR         = "FACTOR";
    public final static String EXT_PRICE_KEY            = "VKEY";
    public final static String EXT_IDS_INST_ID          = "INST_ID";
    public final static String EXT_IDS_GUID             = "GUID";
    public final static String EXT_IDS_CONFIG_ID        = "extPosConfigIds"; // not available in RFC naming! Fallback to default naming.
}
