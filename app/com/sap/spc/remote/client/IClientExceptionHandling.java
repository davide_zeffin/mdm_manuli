/************************************************************************

	Copyright (c) 2002 by SAP AG, SAP Markets Europe GmbH

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * Interface to log exceptions using the SAP logging API. Until the SAP logging API provides this functionality,
 * a default implementation of this interface is found in ExceptionLog.
 */
public interface IClientExceptionHandling {

	/**
	 * Logs that an exception was thrown.
	 * @param   location    the location that should do the logging.
	 * @param   severity    the severity that should be used for the log record.
	 * @param   e           the exception to log. The method logs everything including message and stack trace.
	 */
	public void exception(Location location, int severity, Throwable e);

	/**
	 * Logs that an exception was thrown.
	 * @param   category    the category that should do the logging. A location to use will automatically
	 *                      be determined.
	 * @param   severity    the severity that should be used for the log record.
	 * @param   e           the exception to log. The method logs everything including message and stack trace.
	 */
	public void exception(Category category, int severity, Throwable e);

	/**
	 * Logs that an exception was thrown.
	 * @param   location    the location that should do the logging.
	 * @param   severity    the severity that should be used for the log record.
	 * @param   e           the exception to log. The method logs everything including message and stack trace.
	 * @param   contextMessage  this message will be shown in the log which will look like "Exception <contextMessage>: <details>"
	 *                      if null, this parameter will be ignored.
	 */
	public void exception(Location location, int severity, Throwable e, String contextMessage);

	/**
	 * Logs that an exception was thrown.
	 * @param   category    the category that should do the logging. A location to use will automatically
	 *                      be determined.
	 * @param   severity    the severity that should be used for the log record.
	 * @param   e           the exception to log. The method logs everything including message and stack trace.
	 * @param   contextMessage  this message will be shown in the log which will look like "Exception <contextMessage>: <details>"
	 *                      if null, this parameter will be ignored.
	 */
	public void exception(Category category, int severity, Throwable e, String contextMessage);
}
