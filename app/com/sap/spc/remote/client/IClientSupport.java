/*
 * Created on Dec 30, 2004
 *
 */
package com.sap.spc.remote.client;

/**
 */
public interface IClientSupport {
	public void addLogListener(ClientLogListener l);
	public void removeLogListener(ClientLogListener l);
	
}
