package com.sap.spc.remote.client.util;

import java.util.Map;

/*
   $Workfile: cfg_ext_inst_seq.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */

// import com.sap.sxe.sys.read_only_sequence;

/** Table of instances in external format */
public interface cfg_ext_inst_seq extends Map /*read_only_sequence*/
{
}

