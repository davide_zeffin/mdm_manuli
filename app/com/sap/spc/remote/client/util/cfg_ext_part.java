package com.sap.spc.remote.client.util;

/*
   $Workfile: cfg_ext_part.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */

import java.io.Serializable;

 /**
  * Part_of relation between two instances in external format.
  * Contains parent, part, position number, position type, 
  * sales relevance.
  * Corresponds to structure CUPRT in R/3 ORDERS IDOC.
  */
public interface cfg_ext_part extends Serializable
{
    /** @return the parent instance id  */
    public Integer get_parent_id();

    /** @return the part instance id  */
    public Integer get_inst_id();

    /** @return posnr of the BOM position */
    public String get_pos_nr();

    /** @return string equivalent of object type of the decomposition item type
        of this part instance (e.g. "KLAH" or "MARA") */
    public String get_obj_type();

    /** @return class type (e.g. "300") */
    public String get_class_type();

    /** @return object key (class name for a class, material number for a material) */
    public String get_obj_key();

    /** @return minimal author information for part selection */
    public String get_author();

    /** @return whether this part is sales relevant */
    public boolean is_sales_relevant_p();

}

