package com.sap.spc.remote.client.util;


import java.io.IOException;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * An object that can be saved or restored as a full XML document.
 */

public interface xml_serializable extends xml_serializable_element {

  /*
   * Initializes this object and fills its content from the XML string.
   */
   public void util_load_from_xml_string(String str, boolean validating)
                             throws IOException,
                                    SAXException,
                                    SAXParseException;

  /*
   * Creates an XML String representation for this object.
   */
   public String util_to_xml_string()
                             throws IOException;

  /*
   * Initializes this object and fills its content from an XML file.
   */
   public void util_load_from_xml_file(String path, boolean validating)
                           throws IOException,
                                  SAXException,
                                  SAXParseException;

  /*
   * Creates an XML String representation for this object.
   */
   public void util_to_xml_file(String path)
                           throws IOException;
}

