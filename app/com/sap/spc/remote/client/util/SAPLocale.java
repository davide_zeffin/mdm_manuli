package com.sap.spc.remote.client.util;

/************************************************************************

    Copyright (c) 1998 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works
    based upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any
    warranty about the software, its performance or its conformity to any
    specification.

**************************************************************************/


import java.util.Locale;
import java.io.Serializable;
import java.text.DecimalFormatSymbols;
import java.text.DateFormatSymbols;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.MessageFormat;
import java.util.Hashtable;
import java.util.ResourceBundle;

/**Workaround for the case inconsistency between the ISO Language Code
 * of java.util.Locale (lower case) and R/3 (upper case).
 * A SAPLocale object returns the language and country in the case format
 * as it was passed in at creation, whereas a Locale object converts the
 * language to lower case and the country to upper case.
 * This workaround is required if the database used is case sensitive.
 */
public class SAPLocale implements Serializable {

    public static final SAPLocale LOCALE_EN_US = new SAPLocale("en", "US");
    public static final SAPLocale LOCALE_DE_DE = new SAPLocale("de", "DE");

    protected Locale m_locale;
    protected String m_language;
    protected String m_country;
    protected DecimalFormatSymbols m_decimalFormatSymbols;
    protected String m_dateFormatPattern;
    protected DateFormatSymbols m_dateFormatSymbols;
    protected DateFormat m_dateFormat;

    public SAPLocale() {
        this(LOCALE_EN_US.getLanguage(), LOCALE_EN_US.getCountry(), null);
    }

    public SAPLocale(String language, String country) {
        this(language, country, null);
    }

    public SAPLocale(String language, String country, DecimalFormatSymbols decimalFormatSymbols) {
        m_locale = new Locale(language, country);
        m_language = language;
        m_country = country;
        if (decimalFormatSymbols == null) {
            m_decimalFormatSymbols = new DecimalFormatSymbols(m_locale);
        }
        else {
            m_decimalFormatSymbols = decimalFormatSymbols;
        }
    }

    /**
     * Sets the grouping separator.
     */
    public void setGroupingSeparator(char groupingSeparator) {
        m_decimalFormatSymbols.setGroupingSeparator(groupingSeparator);
    }

    /**
     * @return the grouping separator.
     */
    public char getGroupingSeparator() {
        return m_decimalFormatSymbols.getGroupingSeparator();
    }

    /**
     * Sets the grouping separator, accepting a string instead of char.
     */
    public void setGroupingSeparatorStr(String groupingSeparator) {
        setGroupingSeparator(groupingSeparator.charAt(0));
    }

    /**
     * @return the grouping separator, as a string instead of char.
     */
    public String getGroupingSeparatorStr() {
        return new String("" + getGroupingSeparator());
    }

    /**
     * Sets the decimal separator.
     */
    public void setDecimalSeparator(char decimalSeparator) {
        m_decimalFormatSymbols.setDecimalSeparator(decimalSeparator);
    }

    /**
     * @return the decimal separator.
     */
    public char getDecimalSeparator() {
        return m_decimalFormatSymbols.getDecimalSeparator();
    }

    /**
     * Sets the decimal separator, accepting a string instead of char.
     */
    public void setDecimalSeparatorStr(String decimalSeparator) {
        setDecimalSeparator(decimalSeparator.charAt(0));
    }

    /**
     * @return the decimal separator, as a string instead of char.
     */
    public String getDecimalSeparatorStr() {
        return new String("" + getDecimalSeparator());
    }

    public void setDateFormatPattern(String pattern) {
        setDateFormat(pattern, null);
    }

    public void setDateFormatSymbols(DateFormatSymbols formatSymbols) {
        setDateFormat(null, formatSymbols);
    }

    public void setDateFormat(String pattern, DateFormatSymbols formatSymbols) {
        if (pattern == null && formatSymbols == null)
            return;

        if (pattern == null || formatSymbols == null) {
            if (pattern == null) {
                if (m_dateFormatPattern == null)
                    m_dateFormatPattern = getPattern(DateFormat.SHORT, DateFormat.SHORT, m_locale);
            }
            else
                m_dateFormatPattern = pattern;
            if (formatSymbols == null) {
                if (m_dateFormatSymbols == null)
                    m_dateFormatSymbols = new DateFormatSymbols(m_locale);
            }
            else
                m_dateFormatSymbols = formatSymbols;
        }
        m_dateFormat = new SimpleDateFormat(m_dateFormatPattern, m_dateFormatSymbols);
    }

    public String getDateFormatPattern() {
        return m_dateFormatPattern;
    }

    public DateFormatSymbols getDateFormatSymbols() {
        return m_dateFormatSymbols;
    }

    public DateFormat getDateFormat() {
        return m_dateFormat;
    }

    public String getLanguage() {
        return m_language;
    }

    public String getCountry() {
        return m_country;
    }

    public Locale getLocale() {
        return m_locale;
    }

    public DecimalFormatSymbols getDecimalFormatSymbols() {
        return m_decimalFormatSymbols;
    }

    // protected methods

    /**
     * Cache to hold the DateTimePatterns of a Locale.
     */
    protected static Hashtable cachedLocaleData = new Hashtable(3);

    protected String getPattern(int timeStyle, int dateStyle, Locale loc) {
        String pattern = null;
        /* try the cache first */
        String[] dateTimePatterns = (String[]) cachedLocaleData.get(loc);
        if (dateTimePatterns == null) { /* cache miss */
            ResourceBundle r = ResourceBundle.getBundle
            ("java.text.resources.LocaleElements", loc);
            dateTimePatterns = r.getStringArray("DateTimePatterns");
            /* update cache */
            cachedLocaleData.put(loc, dateTimePatterns);
        }
        if ((timeStyle >= 0) && (dateStyle >= 0)) {
            Object[] dateTimeArgs = {
                dateTimePatterns[timeStyle],
                dateTimePatterns[dateStyle + 4]
            };
            pattern = MessageFormat.format(dateTimePatterns[8], dateTimeArgs);
        }
        else if (timeStyle >= 0) {
            pattern = dateTimePatterns[timeStyle];
        }
        else if (dateStyle >= 0) {
            pattern = dateTimePatterns[dateStyle + 4];
        }
        else {
            pattern = new SimpleDateFormat().toPattern();
        }
        return pattern;
    }
}
