package com.sap.spc.remote.client.util;

/*
   $Workfile: cfg_ext_cstic_val.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:03 $
 */

import java.io.*;

 /**
  * Characteristic value assignment in external format.
  * Corresponds to structure CUVAL in R/3 ORDERS IDOC.
  */
public interface cfg_ext_cstic_val extends Serializable
{
    /** @return the instance id of the instance */
    public Integer get_inst_id();

    /** @return name of the characteristic */
    public String get_charc();

    /** @return language dependent name of cstic */
    public String get_charc_txt();

    /** @return characteristic value as string */
    public String get_value();

    /** @return language dependent name of the value */
    // @@@ does this include the unit ?
    public String get_value_txt();

    /** @return the minimal author information for this cstic value */
    public String get_author();

        /** @return true if this characteristic value is hidden from display */
    public boolean is_invisible_p();

}

