package com.sap.spc.remote.client.util;

import java.util.List;

/*
   $Workfile: cfg_ext_cstic_val_seq.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */

//import com.sap.sxe.sys.read_only_sequence;

/** Table with characteristic value assignments in external format */
public interface cfg_ext_cstic_val_seq extends List /*read_only_sequence*/
{
}

