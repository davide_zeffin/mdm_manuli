package com.sap.spc.remote.client.util;

/*
   $Workfile: cfg_ext_inst.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */
// change log:
// 20010309-ak: added domain restrictions
// 19990903-ak: added quantity unit

import java.io.Serializable;

 /**
  * Instance in external format.
  * Contains current type of the instance, instance quantity
  * and configuration status of the instance.
  * Corresponds to structure CUINS in R/3 ORDERS IDOC.
  */
public interface cfg_ext_inst extends Serializable
{
    /** @return the instance id of the instance (e.g. "1", "2", ...) */
    public Integer get_inst_id();

    /** @return string equivalent of object type of the current type
        of this instance (e.g. "KLAH" or "MARA") */
    public String get_obj_type();

    /** @return class type (e.g. "300") */
    public String get_class_type();

    /** @return object key (class name for a class, material number for a material) */
    public String get_obj_key();

    /** @return language dependent text for object */
    public String get_obj_txt();

    /** @return minimal author information for current instance type */
    public String get_author();

    /** @return instance quantity  */
    public String get_quantity();

    /** @return unit of quantity  */
    public String get_quantity_unit();

    /** @return consistency of this instance */
    public boolean is_consistent_p();

    /** @return completeness of this instance */
    public boolean is_complete_p();

    /** @return sequence of external part_of relations for this ext inst */
    public cfg_ext_part_seq get_parts();

    /** @return sequence of cstic value assignments for this ext inst */
    public cfg_ext_cstic_val_seq get_cstics_values();

    /** @return sequence of price keys assigned for this ext inst */
    public cfg_ext_price_key_seq get_price_keys();

        /** @return sequence of cstic domain restriction elements for this ext inst */
    public cfg_ext_cstic_dom_element_seq get_cstics_domains();
}

