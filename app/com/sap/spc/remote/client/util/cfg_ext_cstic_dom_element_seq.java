package com.sap.spc.remote.client.util;

import java.util.Set;

/*
   $Workfile: cfg_ext_cstic_dom_element_seq.java $
   $Revision: #1 $
   $Author: d034517 $
   $Date: 2004/09/17 $
 */

// import com.sap.sxe.sys.read_only_sequence;

/** Table with characteristic value assignments in external format */
public interface cfg_ext_cstic_dom_element_seq extends Set /*read_only_sequence*/
{
}

