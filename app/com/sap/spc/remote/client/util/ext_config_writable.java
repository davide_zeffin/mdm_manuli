package com.sap.spc.remote.client.util;

/*
   $Workfile: ext_config_writable.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */
// change log:
// 20010302-ak: added invisible flag to cstic values
// 20001003-ak: use vector for cstic values (changed sort order)
// 19990903-ak: added quantity unit

//import com.sap.sxe.sys.exc.exc_not_found;


/**
 * This interface describes a writable external configuration.
 * Has methods to modify header data and to add content to the
 * configuration.
 * <tt>null</tt> is not allowed as an input value for any of
 * the fields.
 */
public interface ext_config_writable extends ext_configuration
{

    // HEADER DATA

    /** sets the name of the configuration */
    public void set_name(String name);

    /** sets the SCE version used to create the configuration (optional) */
    public void set_sce_version(String version);

    /**
     * Sets the client to sce destination client.
     */
    public void set_client( String client );

    /** sets the name of the kb with which the cfg was created */
    public void set_kb_name(String kb_name);

    /** sets the version of kb (e.g. "1" )*/
    public void set_kb_version(String kb_version);

    /** sets buildnumber of kb (e.g. 1 )*/
    public void set_kb_build(int kb_build);

    /** sets the name of the kb profile with which the cfg was done */
    public void set_kb_profile_name(String kb_profile);

    /** sets the cfg info field */
    public void set_cfg_info(String cfg_info);

    /** sets the language used to fill in lang dep text fields */
    public void set_language(String lang_id);

    /** sets the root instance identifier */
    public void set_root_id(Integer root_id);

    /** sets consistency flag for this configuration */
    public void set_consistency(boolean consistent);

    /** sets completeness flag for this configuration */
    public void set_completeness(boolean complete);


    // INSTANCES

   /**
    * Adds an instance to the external configuration.
    * Basic properties of an instance are as defined in
    * interface <tt>cfg_ext_inst</tt>.
    * If <tt>inst_id</tt> is already known, properties of
    * that instance are overwritten.
    */
    public void add_inst(Integer inst_id,
                         String obj_type,
                         String class_type,
                         String obj_key,
                         String obj_txt,
                         String author,
                         String quantity,
                         String quantity_unit,
                         boolean consistent,
                         boolean complete);
   /**
    * Variant of add_inst without quantity_unit argument.
    * @deprecated
    */
    public void add_inst(Integer inst_id,
                         String obj_type,
                         String class_type,
                         String obj_key,
                         String obj_txt,
                         String author,
                         String quantity,
                         boolean consistent,
                         boolean complete);

    // PART_OF RELATIONS

   /**
    * Adds a part_of relation to the external configuration.
    * Basic properties of the part_of relation are as defined
    * in <tt>cfg_ext_part</tt>.
    * Overwrites existing entry for the triple (parent_id, pos_nr,
    * inst_id).
    * Throws an exception if the instance corresponding to
    * <tt>parent_id</tt> is not yet known in the configuration.
    */
    public void add_part(Integer parent_id,
                         Integer inst_id,
                         String pos_nr,
                         String obj_type,
                         String class_type,
                         String obj_key,
                         String author,
                         boolean sales_relevant)
                         throws RuntimeException;

    // CHARACTERISTIC VALUES

       /**
    * Adds a characteristic value assignment to the external
    * configuration.
    * Basic properties of the characteristic value assignment are
    * as defined in <tt>cfg_ext_cstic_val</tt>.
    * Throws an exception if the instance corresponding to
    * <tt>inst_id</tt> is not yet known in the configuration.
    */
    public void add_cstic_value(Integer inst_id,
                    String charc,
                    String charc_txt,
                    String value,
                    String value_txt,
                    String author)
                    throws RuntimeException;

        public void add_cstic_value(Integer inst_id,
                    String charc,
                    String charc_txt,
                    String value,
                    String value_txt,
                    String author,
                                    boolean invisible)
                    throws RuntimeException;

    // PRICE KEYS

   /**
    * Adds a pricing key (including pricing factor) to the external
    * configuration.
    * Overwrites existing entry with ( inst_id, condition_key ).
    * Throws an exception if the instance corresponding to
    * <tt>inst_id</tt> is not yet known in the configuration.
    */
    public void add_price_key(Integer inst_id,
                              String condition_key,
                              double factor)
                              throws RuntimeException;

}

