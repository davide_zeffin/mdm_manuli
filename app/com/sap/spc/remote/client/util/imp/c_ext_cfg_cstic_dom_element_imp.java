package com.sap.spc.remote.client.util.imp;

/*
   $Workfile: c_ext_cfg_cstic_val_imp.java $
   $Revision: #2 $
   $Author: d034517 $
   $Date: 2005/03/04 $
 */

//package com.sap.sce.kbrt.imp;

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

//import com.sap.sce.kbrt.cfg_ext_cstic_dom_element;
import com.sap.spc.remote.client.util.cfg_ext_cstic_dom_element;
//import com.sap.sxe.sys.comparable;
//import com.sap.sxe.sys.exc.exc_unimplemented;
//import com.sap.sxe.util.xml.xml_serializable_element;
import com.sap.spc.remote.client.util.xml_serializable_element;

 /**
  * Characteristic domain element in external format.
  * Can be an atom or a numeric interval.
  * Contains cstic name and value, value_to in string format.
  */
public class c_ext_cfg_cstic_dom_element_imp extends c_ext_cfg_cstic_val_imp
                                          implements cfg_ext_cstic_dom_element,
                                                     Comparable,
                                                     xml_serializable_element
{
    // INSTANCE MEMBERS
    private String  m_value_to;
    private int     m_valcode;

    // CONSTRUCTORS

    protected c_ext_cfg_cstic_dom_element_imp() {
    }

    protected c_ext_cfg_cstic_dom_element_imp(Integer inst_id,
                          String  charc,
                              String  charc_txt,
                              String  value,
                                                  String  value_to,
                          int     valcode,
                          String  value_txt,
                                                  String  author) {
                super(inst_id, charc, charc_txt, value, value_txt, author, false);
        m_value_to   = value_to;
        m_valcode    = valcode;
    }

        // copy
        protected c_ext_cfg_cstic_dom_element_imp(c_ext_cfg_cstic_dom_element_imp de) {
                super(de.get_inst_id(),
                      de.get_charc(),
                      de.get_charc_txt(),
                      de.get_value(),
                      de.get_value_txt(),
                      de.get_author(),
                      de.is_invisible_p());
                m_value_to = de.get_value_to();
                m_valcode = de.get_valcode();
    }

    // FIELD ACCESSORS

    public String get_value_to() {
        return m_value_to;
    }

    public int get_valcode() {
        return m_valcode;
    }

        // use comparable implementation from superclass: supports sort by (inst_id, charc, value)

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("cstic_dom_element(");
        result.append(get_inst_id());
        result.append(", ");
        result.append(get_charc());
        result.append(", ");
        result.append(get_charc_txt());
        result.append(", ");
        result.append(get_value());
            result.append(", ");
                result.append(get_value_to());
            result.append(", ");
                result.append(String.valueOf(get_valcode()));
            result.append(", ");
                result.append(get_value_txt());
        result.append(", ");
        result.append(get_author());
        result.append(")");
        return result.toString();
    }


    public static boolean cfg_ext_equivalent(cfg_ext_cstic_dom_element cv1,
                         cfg_ext_cstic_dom_element cv2,
                         int detail) {
        return ( cv1.get_charc().equals(cv2.get_charc()) &&
                 cv1.get_value().equals(cv2.get_value()) &&
                         cv1.get_value_to().equals(cv2.get_value_to()) &&
                         cv1.get_valcode() == cv2.get_valcode());
    }

        // XML PERSISTENCE
        // @@@ todo !
        public void util_from_xml_element(Element el)
                                          throws IOException, NumberFormatException {
           throw new UnsupportedOperationException("cstic_dom_element.util_from_xml_element");
        }

        public Element util_to_xml_element(Document doc) {
            throw new UnsupportedOperationException("cstic_dom_element.util_to_xml_element");
        }

}

