package com.sap.spc.remote.client.util.imp;

/*
   $Workfile: c_ext_cfg_cstic_val_seq_imp.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:05 $
 */

// change log:
// 20020919-ak: switched to vector (like engine implementation)


import java.util.Enumeration;
import java.util.Vector;

//import com.sap.sce.kbrt.cfg_ext_cstic_val_seq;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val_seq;
//import com.sap.sxe.sys.seq.ordered_set;
//import com.sap.sxe.sys.seq.vector;

 /**
  * Container for characteristic value assignments in external format.
  */
public class c_ext_cfg_cstic_val_seq_imp extends Vector implements cfg_ext_cstic_val_seq
{
    // CONSTANTS
    final static cfg_ext_cstic_val_seq C_EMPTY = new c_ext_cfg_cstic_val_seq_imp();

    // CONSTRUCTOR

    public c_ext_cfg_cstic_val_seq_imp() {
    //  default constructor for ordered_set uses comparable interface
        super();
    }

    public String toString() {
        StringBuffer strbuf = new StringBuffer();
        for(Enumeration e = elements(); e.hasMoreElements(); ) {
            c_ext_cfg_cstic_val_imp current_value = (c_ext_cfg_cstic_val_imp)e.nextElement();
            strbuf.append(current_value.toString());
        }
        return strbuf.toString();
    }

}

