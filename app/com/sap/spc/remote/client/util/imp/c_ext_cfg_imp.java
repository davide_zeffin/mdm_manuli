package com.sap.spc.remote.client.util.imp;

import java.io.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

//import com.sap.sce.kbrt.*;
import com.sap.spc.remote.client.util.*;

//import com.sap.sxe.sys.language;
//import com.sap.sxe.sys.read_only_sequence;
//import com.sap.sxe.sys.exc.exc_not_found;
//import com.sap.sxe.sys.seq.hashtable;
//import com.sap.sxe.util.cleanable;
//import com.sap.sxe.util.xml.imp.xml_serializable_imp;
//import com.sap.vmc.logging.Location;
import com.sap.tc.logging.Location;

// change log:
// 20050309-ak: made this class final
// 20040227-ak: fixed bug in copy constructor: missing cfg info (OSS 710830)
// 20020930-ak: added method set_display_info
// 20020918-ak: complete overhaul - removed redundancy of engine implementation
// 20020716-ak: added null check for author field (OSS 537270)

/**
 * Describes a persistent external configuration.
 * Implements the readable and writable external configuration interfaces.
 * Contains central data in the configuration in a
 * transparent table format with entries that are made up of
 * basic data types only.
 * Is designed to be similar to the R/3 ORDERS0<n> IDOC format
 * for configuration data.
 * Can be saved and loaded to/from XML format.
 *
 * Note on Subclassing:
 *
 * This implementation of ext_configuration should be used as THE standard
 * implementation. All other implementations of the ext_configuration
 * interface should be derived from this implementation.
 *
 * Extensions of this class are allowed for internal/compatibility reasons only.
 * An extension of this class typically uses specialized classes for its
 * elements (instances, parts, cstic values).
 * A specialized instance of ext_configuration satisfies the 'type consistency'
 * condition, if all elements belong to the corresponding specialized
 * element types.
 *
 * In order to accommodate the creation of 'type consistent' external
 * configurations, the c_ext_cfg_imp and c_ext_cfg_inst_imp provide factory
 * methods for their elements (e.g. newInstance, newCsticValue).
 * These methods have to be consistently overwritten at subclasses of c_ext_cfg_imp.
 * (see e.g. com.sap.sce.engine.cfg.cfg_ext_imp).
 *
 * The following extensions exist:
 * - com.sap.sce.engine.cfg.cfg_ext_imp  (trivial extension, for compatibility)
 * - com.sap.spc.beans.SPCCFGExtConfigurationBean (MSA specific extension)
 *
 */
public final class c_ext_cfg_imp extends
                           xml_serializable_imp
                           implements
                           ext_config_writable,
                           /*cleanable,*/
                           Serializable {
    
    Location m_location = com.sap.spc.remote.client.ResourceAccessor.getLocation(c_ext_cfg_imp.class);
    // CONSTANTS
    static final String
    C_XML_TAG_NAME      = "CONFIGURATION",
    C_XML_SCE_VERSION   = "SCEVERSION",
    C_XML_CLIENT        = "CLIENT",
    C_XML_NAME          = "NAME",
            C_XML_KBLOGSYS      = "KBLOGSYS",
    C_XML_KBNAME        = "KBNAME",
    C_XML_KBVERSION     = "KBVERSION",
    C_XML_KBBUILD       = "KBBUILD",
    C_XML_KBPROFILE     = "KBPROFILE",
    C_XML_CFGINFO       = "CFGINFO",
    C_XML_LANGUAGE      = "LANGUAGE",
            C_XML_LANGUAGE_ISO  = "LANGUAGE_ISO",
    C_XML_ROOTID        = "ROOT_NR",
    C_XML_CONSISTENT    = "CONSISTENT",
    C_XML_COMPLETE      = "COMPLETE",
    C_XML_PARTS         = "PARTS";

    protected static final String
        C_EMPTY_STRING = "";

        static final int
        C_STRUCTURE_DETAIL            = 4,
        C_STRUCTURE_AND_STATUS_DETAIL = 8;

     // HEADER DATA
    private String    m_client;
    private String    m_name;
    private String    m_sce_version;
    private String    m_kb_name;
    private String    m_kb_version;
    private int       m_kb_build;
    private String    m_kb_profile;
    private String    m_cfg_info;
    private String    m_language;
    private Integer   m_rootid;
    private boolean   m_consistent;
    private boolean   m_complete;

     // INSTANCES
    private final c_ext_cfg_inst_seq_imp  m_insts = new c_ext_cfg_inst_seq_imp();

     // CONSTRUCTORS
    protected c_ext_cfg_imp() {
    }

    /**
     *  Creates empty configuration in external format with
     *  given header data.
     */
    public c_ext_cfg_imp( String name,
                         String sce_version,
                         String client,
                         String kb_name,
                         String kb_version,
                         int    kb_build,
                         String kb_profile,
                         String language,
                         Integer rootid,
                         boolean consistent,
                         boolean complete ) {
        init(name,
             sce_version,
             client,
             kb_name,
             kb_version,
             kb_build,
             kb_profile,
             language,
             rootid,
             consistent,
             complete);
    }

    public void init( String name,
                         String sce_version,
                         String client,
                         String kb_name,
                         String kb_version,
                         int    kb_build,
                         String kb_profile,
                         String language,
                         Integer rootid,
                         boolean consistent,
                         boolean complete ) {
        set_name(name);
        if (sce_version != null) set_sce_version(sce_version);
        set_client( client );
        if (kb_name != null) set_kb_name(kb_name);
        if (kb_version !=  null) set_kb_version(kb_version);
        set_kb_build(kb_build);
        if (kb_profile != null) set_kb_profile_name(kb_profile);
        m_cfg_info = "";
        if (language != null) set_language(language);
        set_root_id(rootid);
        m_consistent  = consistent;
        m_complete    = complete;
    }

        /**
     *  Copies a given external configuration ('deep copy').
         *
         *  This constructor satisfies the 'type consistency' condition.
         *  Copies the source configuration and its elements using the element
         *  factories of this ext configuration.
         *
         *  @internal
     */
         protected c_ext_cfg_imp( c_ext_cfg_imp cfgToCopy)
     {

        this(  cfgToCopy.get_name(),
                       cfgToCopy.get_sce_version(),
               cfgToCopy.get_client(),
               cfgToCopy.get_kb_name(),
               cfgToCopy.get_kb_version(),
               cfgToCopy.get_kb_build(),
               cfgToCopy.get_kb_profile_name() ,
               cfgToCopy.get_language(),
               cfgToCopy.get_root_id(),
               cfgToCopy.is_consistent_p(),
               cfgToCopy.is_complete_p());

               m_cfg_info = cfgToCopy.get_cfg_info();      // OSS 710830

                Enumeration keys = ((Hashtable)cfgToCopy.get_insts()).keys() ;
                Enumeration cfg =  ((Hashtable)cfgToCopy.get_insts()).elements() ;
                while(keys.hasMoreElements())  {
                    c_ext_cfg_inst_imp ext_inst = (c_ext_cfg_inst_imp)cfg.nextElement();
                    m_insts.put(keys.nextElement(), copy_inst(ext_inst));
                }
    }


     // CLEANABLE INTERFACE

        public void clean() {
           if (empty_p())
               return;
           m_client = null;
           m_name = null;
           m_sce_version = null;
           m_kb_name = null;
           m_kb_version = null;
           m_kb_build = 0;
           m_kb_profile = null;
           m_cfg_info = null;
           m_language = null;
           m_consistent = m_complete = false;
           m_rootid = null;
           m_insts.clear();
        }

        public boolean empty_p() {
           return m_insts.size() == 0;
        }

     // INSTANCE METHODS

    public String get_sce_version() {
        return m_sce_version;
    }

    public void set_sce_version(String val) {
        if ( val == null )
             throw new NullPointerException("set_sce_version: val unspecified");
        m_sce_version = val;
    }

    public void set_client( String client ) {
        m_client = client;
    }

    public String get_client() {
        return m_client;
    }

    public String get_name() {
        return m_name;
    }

    public void set_name(String val) {
        if ( val == null )
             throw new NullPointerException("set_name: val unspecified");
        m_name = val;
    }

    public String get_kb_name() {
        return m_kb_name;
    }

        // @deprecated - use set_kb_key instead
    public void set_kb_name(String val) {
        if ( val == null )
             throw new NullPointerException("set_kb_name: val unspecified");
                m_kb_name = val;
    }

    public String get_kb_version() {
        return m_kb_version;
    }

        // @deprecated - use set_kb_key instead
    public void set_kb_version(String val) {
        if ( val == null )
             throw new NullPointerException("set_kb_version: val unspecified");
        m_kb_version = val;
    }

    public int get_kb_build() {
        return m_kb_build;
    }

    public void set_kb_build(int kb_build) {
        m_kb_build = kb_build;
    }

    public String get_kb_profile_name() {
        return m_kb_profile;
    }

    public void set_kb_profile_name(String val) {
        if ( val == null )
             throw new NullPointerException("set_kb_profile_name: val unspecified");
        m_kb_profile = val;
    }

    public String get_cfg_info() {
        return m_cfg_info;
    }

    public void set_cfg_info(String val) {
        String l_val = val;
        if ( val == null )
             l_val = "";
        m_cfg_info = l_val;
    }

    public String get_language() {
        return m_language;
    }

    /**
     * 20000613-ak   normalize language identifier to char1 format
     * 20000427-ak   normalize language identifier to char2 format
     *               added basic validation of language identifier
     * 16.03.2000 tm Empty string for no language is allowed.
     */
    public void set_language(String val) {
        if ( val == null )
             throw new NullPointerException("set_language: val unspecified");

        m_language = normalized_language_id(val);
    }

        private String normalized_language_id(String val) {

           String lstr = null;

       switch(val.length()) {
            case 0:  lstr = "";
             break;
        case 1:  if (val.equals(" "))
                 lstr = "";        // replace blank by empty string
             else {
                 language.find_language_for_id(val);
                 lstr = val;
             }
             break;
        case 2:  lstr = language.find_language_for_id2(val).get_id();
             break;
        default: throw new RuntimeException("Not found: unknown language id " + val);
       }
           return lstr;
        }

    public Integer get_root_id() {
        return m_rootid;
    }

    public void set_root_id(Integer val) {
        if ( val == null )
             throw new NullPointerException("set_root_id: val unspecified");
        m_rootid = val;
    }

    public boolean is_consistent_p() {
        return m_consistent;
    }

    public void set_consistency(boolean val) {
        m_consistent = val;
    }

    public boolean is_complete_p() {
        return m_complete;
    }

    public void set_completeness(boolean val) {
        m_complete = val;
    }

    public cfg_ext_inst_seq get_insts() {
        return m_insts;
    }

    public cfg_ext_inst get_inst(Integer inst_id) {
        if ( inst_id == null )
             throw new NullPointerException("get_inst: inst_id not specified");
        return (cfg_ext_inst)m_insts.get(inst_id);
    }

    public cfg_ext_part_seq get_parts() {
        c_ext_cfg_part_seq_imp result = new c_ext_cfg_part_seq_imp();

        for (Enumeration e = m_insts.elements();
             e.hasMoreElements();
             ) {
                 cfg_ext_part_seq parts =
                     ((c_ext_cfg_inst_imp)e.nextElement()).get_parts();

                 for (Iterator ee = parts.iterator(); ee.hasNext();) {
                        result.add(ee.next());
                        }
             }
        return result;
    }

    public cfg_ext_part_seq get_parts(Integer parent_id)
                      throws RuntimeException {
        if ( parent_id == null )
             throw new NullPointerException("get_parts: parent_id not specified");
        c_ext_cfg_inst_imp ext_inst = (c_ext_cfg_inst_imp)m_insts.get(parent_id);
        if ( ext_inst == null )
             throw new RuntimeException("get_parts: parent_id not found " + parent_id);
        return ext_inst.get_parts();
    }

    public cfg_ext_cstic_val_seq get_cstics_values() {
        c_ext_cfg_cstic_val_seq_imp result = new c_ext_cfg_cstic_val_seq_imp();

        for (Enumeration e = m_insts.elements();
             e.hasMoreElements();
             ) {
                 cfg_ext_cstic_val_seq vals =
                     ((c_ext_cfg_inst_imp)e.nextElement()).get_cstics_values();

                 for (Iterator ee = vals.iterator();
                        ee.hasNext();
                        ) {
                        result.addElement(ee.next());
                        }
             }
        return result;
    }

    public cfg_ext_cstic_val_seq get_cstics_values(Integer inst_id)
                               throws RuntimeException {
        if ( inst_id == null )
             throw new NullPointerException("get_cstics_values: inst_id not specified");
        c_ext_cfg_inst_imp ext_inst = (c_ext_cfg_inst_imp)m_insts.get(inst_id);
        if ( ext_inst == null )
             throw new RuntimeException("get_cstics_values: inst_id not found " + inst_id);
        return ext_inst.get_cstics_values();
    }


    public cfg_ext_price_key_seq get_price_keys() {
        c_ext_cfg_price_key_seq_imp result = new c_ext_cfg_price_key_seq_imp();

        for (Enumeration e = m_insts.elements();
             e.hasMoreElements();
             ) {
                 cfg_ext_price_key_seq vals =
                     ((c_ext_cfg_inst_imp)e.nextElement()).get_price_keys();

                 for (Iterator ee = vals.iterator();
                        ee.hasNext();
                        ) {
                        result.add(ee.next());
                        }
             }
        return result;
    }

    public cfg_ext_price_key_seq get_price_keys(Integer inst_id)
                            throws RuntimeException {
        if ( inst_id == null )
             throw new NullPointerException("get_price_keys: inst_id not specified");
        c_ext_cfg_inst_imp ext_inst = (c_ext_cfg_inst_imp)m_insts.get(inst_id);
        if ( ext_inst == null )
             throw new RuntimeException("get_price_keys: inst_id not found " + inst_id);
        return ext_inst.get_price_keys();
    }




     // ELEMENT FACTORIES

     // @@@ try to reduce this to zero argument constructors; otherwise specializations
     //     need to extend the overwritten signatures whenever a field is added

        protected c_ext_cfg_inst_imp newInstance() {
               return new c_ext_cfg_inst_imp();
        }

    protected c_ext_cfg_inst_imp newInstance(Integer inst_id,
                         String obj_type,
                         String class_type,
                         String obj_key,
                         String obj_txt,
                         String author,
                         String quantity,
                         String quantity_unit,
                         boolean consistent,
                         boolean complete)
    {
           return new c_ext_cfg_inst_imp(inst_id,
                          obj_type,
                          class_type,
                          obj_key,
                          obj_txt,
                          author,
                          quantity,
                              quantity_unit,
                          consistent,
                          complete);
    }


        protected c_ext_cfg_part_imp newPart() {
               return new c_ext_cfg_part_imp();
        }

    protected c_ext_cfg_part_imp newPart(Integer parent_id,
                         Integer inst_id,
                         String pos_nr,
                         String obj_type,
                         String class_type,
                         String obj_key,
                         String author,
                         boolean sales_relevant)
    {
        return new c_ext_cfg_part_imp(parent_id,
                          inst_id,
                          pos_nr,
                          obj_type,
                          class_type,
                          obj_key,
                          author,
                          sales_relevant);
    }

    protected c_ext_cfg_cstic_val_imp newCsticValue(Integer inst_id,
                            String charc,
                            String charc_txt,
                            String value,
                            String value_txt,
                            String author) {
                return newCsticValue(inst_id,charc, charc_txt, value, value_txt, author, false);
        }

        protected c_ext_cfg_cstic_val_imp newCsticValue(Integer inst_id,
                            String charc,
                            String charc_txt,
                            String value,
                            String value_txt,
                            String author,
                                                        boolean invisible)
    {
        return new c_ext_cfg_cstic_val_imp(inst_id,
                               charc,
                           charc_txt,
                           value,
                           value_txt,
                           author,
                                                   invisible);
    }

    protected c_ext_cfg_price_key_imp newPriceKey(Integer inst_id,
                              String key,
                              double factor)
    {
        return new c_ext_cfg_price_key_imp(inst_id,
                           key,
                           factor);
    }

     // COPY INSTANCE (deep copy for type consistency)

        protected c_ext_cfg_inst_imp copy_inst(c_ext_cfg_inst_imp inst) {

           c_ext_cfg_inst_imp result = newInstance(inst.get_inst_id(),
                                                   inst.get_obj_type(),
                                                   inst.get_class_type(),
                                                   inst.get_obj_key(),
                                                   inst.get_obj_txt(),
                                                   inst.get_author(),
                                                   inst.get_quantity(),
                                                   inst.get_quantity_unit(),
                                                   inst.is_consistent_p(),
                                                   inst.is_complete_p());

          cfg_ext_cstic_val_seq cstic_values = inst.get_cstics_values();
          if ( cstic_values != null) {
               for(Iterator e = cstic_values.iterator();
                   e.hasNext(); ) {
                   c_ext_cfg_cstic_val_imp cv = (c_ext_cfg_cstic_val_imp)e.next();
                   result.add_cstic_value(newCsticValue(cv.get_inst_id(),
                                                        cv.get_charc(),
                                    cv.get_charc_txt(),
                                                        cv.get_value(),
                                                        cv.get_value_txt(),
                                                        cv.get_author(),
                                                        cv.is_invisible_p()));
               }
          }

          /* @@@ todo
          read_only_sequence cstic_dom_elements = inst.get_cstics_domains();
          if ( cstic_dom_elements != null) {
          }
          */

          cfg_ext_price_key_seq price_keys = inst.get_price_keys();
          if ( price_keys != null) {
               for(Iterator e = price_keys.iterator();
                   e.hasNext(); ) {
                   c_ext_cfg_price_key_imp pk = (c_ext_cfg_price_key_imp)e.next();
                   result.add_price_key(newPriceKey(pk.get_inst_id(),
                                pk.get_key(),
                                                    pk.get_factor()));
               }
          }

          cfg_ext_part_seq parts = inst.get_parts();
          if ( parts != null) {
               for(Iterator e = parts.iterator();
                   e.hasNext(); ) {
                   c_ext_cfg_part_imp part = (c_ext_cfg_part_imp)e.next();
                   result.add_part(newPart(part.get_parent_id(),
                                           part.get_inst_id(),
                                           part.get_pos_nr(),
                                           part.get_obj_type(),
                                           part.get_class_type(),
                                           part.get_obj_key(),
                                           part.get_author(),
                                           part.is_sales_relevant_p()));
               }
          }

          return result;
     }


     // MODIFIERS TO ADD CONTENT TO AN EXTERNAL CONFIGURATION (ext_config_writable interface)

    public void add_inst(Integer inst_id,
                 String obj_type,
                 String class_type,
                 String obj_key,
                 String obj_txt,
                 String author,
                 String quantity,
                 String quantity_unit,
                 boolean consistent,
                 boolean complete) {
        //2005-03-09 chs CQM
        String l_obj_txt = obj_txt;
        String l_author = author;

        if ( inst_id == null )
             throw new NullPointerException("add_inst: inst_id not specified");
        if ( obj_type   == null ||
             class_type == null ||
             obj_key    == null )
             throw new NullPointerException("add_inst: type of inst not specified");
        if ( obj_txt == null )
             l_obj_txt = C_EMPTY_STRING;
        if ( author == null )
             l_author = C_EMPTY_STRING;       // OSS 537270
        //   throw new NullPointerException("add_inst: author not specified");

        m_insts.put(inst_id, newInstance(inst_id,
                          obj_type,
                          class_type,
                          obj_key,
                          l_obj_txt,
                          l_author,
                          quantity,
                          quantity_unit,
                          consistent,
                          complete));
    }

    // @deprecated
    public void add_inst(Integer inst_id,
                         String obj_type,
                         String class_type,
                         String obj_key,
                         String obj_txt,
                         String author,
                         String quantity,
                         boolean consistent,
                         boolean complete) {
        add_inst(inst_id, obj_type, class_type, obj_key, obj_txt,
                 author,  quantity, "", consistent, complete);
    }

    public void add_part(Integer parent_id,
                 Integer inst_id,
                 String pos_nr,
                 String obj_type,
                 String class_type,
                 String obj_key,
                 String author,
                 boolean sales_relevant)
                 throws RuntimeException {

        //2005-03-09 chs CQM
        String l_author = author;
        c_ext_cfg_inst_imp ext_inst = (c_ext_cfg_inst_imp)m_insts.get(parent_id);
        if ( ext_inst == null )
             throw new RuntimeException("add_part: parent not found " + parent_id);
        if ( inst_id == null )
             throw new NullPointerException("add_part: inst_id not specified");
        if ( pos_nr == null )
             throw new NullPointerException("add_part: pos_nr not specified");
        if ( obj_type   == null ||
             class_type == null ||
             obj_key    == null )
             throw new NullPointerException("add_part: type of pos not specified");
        if ( author == null )
             l_author = C_EMPTY_STRING;       // OSS 537270
        //    throw new NullPointerException("add_part: author not specified");

        ext_inst.add_part(newPart(parent_id,
                     inst_id,
                     pos_nr,
                     obj_type,
                         class_type,
                     obj_key,
                     l_author,
                     sales_relevant));
        }

        public void add_cstic_value(Integer inst_id,
                    String charc,
                    String charc_txt,
                    String value,
                    String value_txt,
                    String author,
                                    boolean invisible)
                    throws RuntimeException {

        //2005-03-09 chs CQM
        String l_charc_txt = charc_txt;
        String l_value_txt = value_txt;
        String l_author = author;
        c_ext_cfg_inst_imp ext_inst = (c_ext_cfg_inst_imp)m_insts.get(inst_id);
        if ( ext_inst == null )
             throw new RuntimeException("add_cstic_value: inst not found " + inst_id);
        if ( charc == null )
             throw new NullPointerException("add_cstic_value: cstic not specified");
        if ( charc_txt == null )
             l_charc_txt = C_EMPTY_STRING;
        if ( value == null )
             throw new NullPointerException("add_cstic_value: value not specified");
        if ( value_txt == null )
             l_value_txt = C_EMPTY_STRING;
        if ( author == null )
             l_author = C_EMPTY_STRING;       // OSS 537270
        //   throw new NullPointerException("add_cstic_value: author not specified");

        ext_inst.add_cstic_value(newCsticValue(inst_id,
                                   charc,
                               l_charc_txt,
                               value,
                               l_value_txt,
                               l_author,
                                                       invisible));
    }

        // keep this signature as shortcut for invisible = false
        public void add_cstic_value(Integer inst_id,
                    String charc,
                    String charc_txt,
                    String value,
                    String value_txt,
                    String author)
                    throws RuntimeException {
            add_cstic_value(inst_id, charc, charc_txt, value, value_txt, author, false);
        }

    public void add_price_key(Integer inst_id,
                  String key,
                  double factor)
                  throws RuntimeException {

        c_ext_cfg_inst_imp ext_inst = (c_ext_cfg_inst_imp)m_insts.get(inst_id);
        if ( ext_inst == null )
             throw new RuntimeException("add_price_key: inst not found " + inst_id);
        if ( key == null )
             throw new NullPointerException("add_price_key: condition key not specified");

        ext_inst.add_price_key(newPriceKey(inst_id,
                           key,
                           factor));
    }


    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("#c_ext_cfg_imp: ");
        s.append("client = " + m_client);
        s.append(", name = " + m_name);
        s.append(", sce_version = " + m_sce_version);
        s.append(", kb_name = " + m_kb_name);
        s.append(", kb_version = " + m_kb_version);
        s.append(", kb_build = " + m_kb_build);
        s.append(", kb_profile = " + m_kb_profile);
        s.append(", cfg_info = " + m_cfg_info);
        s.append(", language = " + m_language);
        s.append(", rootid = " + m_rootid);
        s.append(", consistent = " + m_consistent);
        s.append(", complete = " + m_complete);
        s.append(", Instances = " + m_insts);
        return s.toString();
    }



       // XML PERSISTENCE

       // SAVE EXTERNAL CONFIGURATION TO TEXT FILE, STREAM OR STRING IN XML FORMAT

    public boolean cfg_ext_write_data(String path) {
        boolean result;
        try {
                 util_to_xml_file(path);
             result = true;
        }
        catch (Throwable t) {
        m_location.throwing("cfg_ext_write_data: " + path, t);
        result = false;
        }
        return result;
    }

    public boolean cfg_ext_write_data(OutputStream os) {
        boolean result;
        try {
                 util_to_xml_stream(os);
             result = true;
        }
        catch (Throwable t) {
            m_location.throwing("cfg_ext_write_data: " + os, t);
        result = false;
        }
        return result;
    }

    public String cfg_ext_to_xml_string() {
        try {
             return util_to_xml_string();
        }
        catch(Throwable t) {
            m_location.throwing("cfg_ext_to_xml_string: " + this, t);
          return null;
        }
    }

        /**
     * CREATE XML ELEMENT FROM THIS CONFIGURATION
     *
     * XML Element Structure:
     *
     * <CONFIGURATION
     *      SCEVERSION  = (sce_version)
     *      NAME        = (name)
     *      KBNAME      = (kbname)                                 (*)
     *      KBVERSION   = (kbversion)                              (*)
     *      KBBUILD     = (build)               (integer)
     *      KBPROFILE   = (kbprofile)                              (*)
     *      CFGINFO     = (cfginfo)
     *      LANGUAGE    = (language) (single char)
         *      LANGUAGE_ISO= (language) (ISO)
     *      ROOT_NR     = (nr of root instance) (integer)          (*)
     *      CONSISTENT  = (consistent) in {"T", "F"}
     *      COMPLETE    = (complete) in {"T", "F"}>
     *
     *  <INST             // root instance element
     *      NR          = (nr of instance)      (integer)          (*)
     *      OBJ_TYPE    = (object type)                            (*)
     *      CLASS_TYPE  = (class type)                             (*)
     *      OBJ_KEY     = (object key) (e.g. matnr)                (*)
         *      OBJ_TXT     = (language dep object text)
     *      AUTHOR      = (author of current type)                 (*)                                                    defaults to USER)
     *      QTY         = (instance quantity)                      (*)
     *      UNIT        = (unit of instance quantity)              (*)
     *      CONSISTENT  = (consistent) in {"T", "F"}
     *      COMPLETE    = (complete) in {"T", "F"}>
     *
     *   <CSTICS>         // characteristic values
     *    <CSTIC>
     *       CHARC      = (neutral key)                            (*)
     *       CHARC_TXT  = (language dep cstic text)
     *       VALUE      = (cstic value)                            (*)
     *       VALUE_TXT  = (language dep value text)
     *       AUTHOR     = (author) />                              (*)
     *    ...
     *   </CSTICS>
     *
     *   <PKEYS>          // pricing condition keys
     *    <PKEY>
     *       KEY        = (condition key)                          (*)
     *       FACTOR     = (multiplier) />                     (default: 1)
     *    ...
     *   </PKEYS>
     *
     *  </INST>
     *
     *  <PARTS>           // part instances in a flat list
     *   <PART
     *      PARENT_NR   = (nr of parent instance)   (integer)      (*)
     *      POS_NR      = (position number in BOM)                 (*)
     *      OBJ_TYPE    = (object type of BOM item)                (*)
     *      CLASS_TYPE  = (class type of BOM item)                 (*)
     *      OBJ_KEY     = (object key of BOM item)                 (*)
     *      AUTHOR      = (author)  (SPACE (" ") for user input)   (*)
     *      S           = (sales relevant) in {"T", "F"} >
     *    <INST ..> .. </INST>
     *   </PART>
     *   ...
     *  </PARTS>
     *
     * </CONFIGURATION>
     *
     *
     * NOTE: attributes marked with an asterisk (*) are mandatory for
     *       loading a configuration from XML format.
     *       A formal DTD specification is given in file SceConfig.dtd.
     */
        public Element util_to_xml_element(Document doc) {

        if ( doc == null )
             throw new NullPointerException(
                   "util_to_xml_element: XML document not specified");

        Element config =  doc.createElement(C_XML_TAG_NAME);

        config.setAttribute(C_XML_SCE_VERSION, m_sce_version);
        config.setAttribute(C_XML_CLIENT,      m_client);
        config.setAttribute(C_XML_NAME,        m_name);
        config.setAttribute(C_XML_KBNAME,      m_kb_name);
        config.setAttribute(C_XML_KBVERSION,   m_kb_version);
        config.setAttribute(C_XML_KBBUILD,     String.valueOf(m_kb_build));
        config.setAttribute(C_XML_KBPROFILE,   m_kb_profile);
        config.setAttribute(C_XML_CFGINFO,     m_cfg_info);
        config.setAttribute(C_XML_LANGUAGE,    m_language);
                // 20010503
                if (m_language.length() == 1) {
                    String language_iso = language.find_language_for_id(m_language).get_id2();
                    config.setAttribute(C_XML_LANGUAGE_ISO, language_iso);
                }
                config.setAttribute(C_XML_ROOTID,      m_rootid.toString());
        config.setAttribute(C_XML_CONSISTENT,  m_consistent ? "T" : "F");
        config.setAttribute(C_XML_COMPLETE,    m_complete   ? "T" : "F");

        // root instance element
        c_ext_cfg_inst_imp root_inst = (c_ext_cfg_inst_imp)m_insts.get(m_rootid);
        Element root_inst_element = root_inst.util_to_xml_element(doc);
        config.appendChild(root_inst_element);

        // parts in a depth first order
        Element parts_element = doc.createElement(C_XML_PARTS);
        config.appendChild(parts_element);

        cfgx_ext_parts_to_xml_document(doc, parts_element, root_inst);
        return config;
    }

    void cfgx_ext_parts_to_xml_document(Document doc,
                        Element parts_element,
                        c_ext_cfg_inst_imp parent) {

        cfg_ext_part_seq parts = parent.get_parts();
        for(Iterator e = parts.iterator();
            e.hasNext();
            ) {
               c_ext_cfg_part_imp ext_part = (c_ext_cfg_part_imp)e.next();
               c_ext_cfg_inst_imp ext_inst =
                           (c_ext_cfg_inst_imp)m_insts.get(ext_part.get_inst_id());
               Element part_element  = ext_part.util_to_xml_element(doc);
               Element inst_element  = ext_inst.util_to_xml_element(doc);
               part_element.appendChild(inst_element);
               parts_element.appendChild(part_element);
               cfgx_ext_parts_to_xml_document(doc, parts_element, ext_inst);
            }
    }

        // @deprecated
        public Element cfg_ext_to_xml_element(Document doc)
                          throws IOException {
                return util_to_xml_element(doc);
        }

       // LOAD EXTERNAL CONFIGURATION FROM TEXT FILE IN XML FORMAT

    public static ext_configuration cfg_ext_read_data(String path)
                        throws  FileNotFoundException
        {
               c_ext_cfg_imp result = new c_ext_cfg_imp();
               result.cfg_ext_load_data_from_file(path);
               // return null if an error occurred
               return result.empty_p() ? null : result;
        }

        /**
    * New instance method: fills the content of this ext_configuration from XML file.
        * The fact that this is an instance method allows to create special implementations
        * of ext_configuration with consistent specializations of config elements
        * by using 'this' as an element factory.
        * If the input file is not found, this object remains unchanged.
        * If other (internal) errors occur, this object is cleaned.
        */
    protected void cfg_ext_load_data_from_file(String path)
                                              throws  FileNotFoundException {
        try {
             util_load_from_xml_file(path, false);    // validating = 'false'
        }
        catch (FileNotFoundException fe) {
            throw fe;
        }
        catch (SAXParseException se) {
            String line   = String.valueOf(se.getLineNumber());
            m_location.throwing("XML Parser error loading configuration from " + path +
                        " at line " + line, se);
            clean();
        }
        catch (SAXException se) {
            m_location.throwing("XML Parser error loading configuration from " + path, se);
            clean();
        }
        catch (Throwable t) {
            m_location.throwing("Error loading configuration from  " + path, t);
            clean();
        }
    }



    // read ext configuration from XML String
    public static ext_configuration cfg_ext_read_data_from_string(String str)
    {
                c_ext_cfg_imp result = new c_ext_cfg_imp();
                result.cfg_ext_load_data_from_string(str);
                return result.empty_p() ? null : result;
        }


       /**
    * Instance method: fills the content of this ext_configuration from XML String.
        * The fact that this is an instance method allows to create special implementations
        * of ext_configuration with consistent specializations of config elements
        * by using 'this' as an element factory.
        */
    protected void cfg_ext_load_data_from_string(String str)
    {
        try {
             util_load_from_xml_string(str, false);   // validating = 'false'
            }
        catch (SAXParseException se) {
               String line   = String.valueOf(se.getLineNumber());
               m_location.throwing( "XML Parser error loading configuration from string" +
                        " at line " + line, se);
               clean();
        }
        catch (SAXException se) {
            m_location.throwing("XML Parser error loading configuration from string", se);
               clean();
        }
        catch (Throwable t) {
            m_location.throwing("Error loading configuration from string", t);
               clean();
        }
    }


        // @deprecated
    public static ext_configuration cfg_ext_from_xml_element(Element config_element)
                                 throws IOException {
           c_ext_cfg_imp result = new c_ext_cfg_imp();
           result.util_load_from_xml_element(config_element);
           return result;
        }


        // Core XML load method: cleans this ext_cfg_imp and reloads its content
        // from an XML element
        public void util_load_from_xml_element(Element config_element)
                                               throws IOException,
                                                      NumberFormatException {
                clean();

        if ( !config_element.getTagName().equals(C_XML_TAG_NAME) )
            throw new IOException("Invalid configuration tag: " +
                           config_element.getTagName());
        // getAttribute always returns a String ...
        String sce_version = config_element.getAttribute(C_XML_SCE_VERSION);
        String client      = config_element.getAttribute(C_XML_CLIENT);
        String name        = config_element.getAttribute(C_XML_NAME);

        String kb_name     = config_element.getAttribute(C_XML_KBNAME);
        String kb_version  = config_element.getAttribute(C_XML_KBVERSION);

        String kb_build_s  = config_element.getAttribute(C_XML_KBBUILD);
        int kb_build       = 0;
        try{
            kb_build       = Integer.parseInt(kb_build_s);
        }
        catch(NumberFormatException e) {
            m_location.throwing("kb build unspecified, set to 0", e);
        }
        String kb_profile  = config_element.getAttribute(C_XML_KBPROFILE);
        String cfg_info    = config_element.getAttribute(C_XML_CFGINFO);
        String language    = config_element.getAttribute(C_XML_LANGUAGE);
        String consist_s   = config_element.getAttribute(C_XML_CONSISTENT);
        boolean consistent = consist_s.equals("T");
        String complete_s  = config_element.getAttribute(C_XML_COMPLETE);
        boolean complete   = complete_s.equals("T");

        set_sce_version(sce_version);
        set_name(name);
        set_client(client);
        set_kb_name(kb_name);
        set_kb_version(kb_version);
        set_kb_build(kb_build);
        set_kb_profile_name(kb_profile);
        set_cfg_info(cfg_info);
        set_language(language);
        set_consistency(consistent);
        set_completeness(complete);

       //  sce version can be used to support 'old' formats below
        cfgx_ext_from_xml_element(config_element, sce_version);
    }

    void cfgx_ext_from_xml_element(Element config,
                       String sce_version)
                       throws IOException,
                                              NumberFormatException {
    //  read root instance
        Element root_inst_element = cfgx_get_root_inst_element(config);
        if ( root_inst_element == null )
            throw new IOException("cfg_ext_from_xml_element: no root instance found");

        c_ext_cfg_inst_imp root_inst = cfgx_ext_add_inst_from_xml(root_inst_element);
        set_root_id(root_inst.get_inst_id());

    //  read parts
        NodeList l = config.getElementsByTagName(C_XML_PARTS);
        if ( l.getLength() == 0 )
             return;
        NodeList p = ((Element)l.item(0)).
                      getElementsByTagName(c_ext_cfg_part_imp.C_XML_TAG_NAME);
        for( int i = 0; i < p.getLength(); ++i ) {
            cfgx_ext_add_part_from_xml((Element)p.item(i));
        }
    }

    // find first direct child element of type instance
    private static Element cfgx_get_root_inst_element(Element config) {
        NodeList l = config.getChildNodes();
        for (int i = 0; i < l.getLength(); ++i) {
             Node n = l.item(i);
             if ( n.getNodeName().equals(c_ext_cfg_inst_imp.C_XML_TAG_NAME) &&
                  n instanceof Element )
                  return (Element)n;
        }
        return null;
    }

    c_ext_cfg_inst_imp cfgx_ext_add_inst_from_xml(Element inst_element)
                          throws IOException, NumberFormatException {
        c_ext_cfg_inst_imp result = newInstance();
                result.util_load_from_xml_element(inst_element);
        m_insts.put(result.get_inst_id(), result);
        return result;
    }

    void cfgx_ext_add_part_from_xml(Element part_element)
                    throws IOException, NumberFormatException {
        c_ext_cfg_part_imp ext_part = newPart();
                ext_part.util_load_from_xml_element(part_element);
        NodeList l = part_element.getElementsByTagName(c_ext_cfg_inst_imp.C_XML_TAG_NAME);
        if ( l.getLength() == 0 )
             throw new IOException("Part element for parent nr " +
                        ext_part.get_parent_id() + " has no instance data");
        c_ext_cfg_inst_imp part = cfgx_ext_add_inst_from_xml((Element)l.item(0));
        ext_part.set_inst_id(part.get_inst_id());
        c_ext_cfg_inst_imp ext_inst =
                 (c_ext_cfg_inst_imp)m_insts.get(ext_part.get_parent_id());
        ext_inst.add_part(ext_part);
    }

        // DUMP EXTERNAL CONFIGURATION IN ASCII FORMAT

    public void cfg_ext_dump(PrintWriter ps) {
        ps.println();
        ps.println("#EXT CFG DUMP# (SCE version " + get_sce_version() + ")");
        ps.println();
        ps.println("Client       " + get_client());
        ps.println("Name         " + get_name());
        ps.println("KB           " + get_kb_name() + " "
                       + get_kb_version() + " (build "
                       + get_kb_build() + ")");
        ps.println("KB profile   " + get_kb_profile_name());
        ps.println("CFG info     " + get_cfg_info());
        ps.println("Language     " + get_language());
        ps.println("Root ID      " + get_root_id());
        ps.println("Consistent   " + is_consistent_p());
        ps.println("Complete     " + is_complete_p());
        ps.println("No of Insts  " + get_insts().size());
        ps.println();
        ps.println("Part Instances: ");
        cfgx_ext_dump(ps, get_root_id());
        ps.println();
        ps.println("#END OF EXT CFG DUMP#");
    }

    void cfgx_ext_dump(PrintWriter ps,
               Integer inst_id) {
        c_ext_cfg_inst_imp ext_inst = (c_ext_cfg_inst_imp)m_insts.get(inst_id);
                ext_inst.cfgx_ext_dump(ps);
        ps.println();
        for(Iterator e = ext_inst.get_parts().iterator();
            e.hasNext();
           ) {
              cfg_ext_part ext_part = (cfg_ext_part)e.next();
              ps.println(ext_part.toString());
              cfgx_ext_dump(ps, ext_part.get_inst_id());
        }
    }

        // COMPARISON OF TWO EXTERNAL CONFIGURATIONS

    public static boolean cfg_ext_equivalent(ext_configuration c1,
                         ext_configuration c2,
                         int detail) {

        if ( detail >= C_STRUCTURE_AND_STATUS_DETAIL ) {
             if ( c1.is_consistent_p() != c2.is_consistent_p() )
               return false;
             if ( c1.is_complete_p() != c2.is_complete_p() )
              return false;
        }

        if ( c1.get_insts().size() != c2.get_insts().size() )
             return false;

        return cfgx_ext_insts_equivalent(c1, c1.get_root_id(),
                         c2, c2.get_root_id(),
                         detail);
    }

    static boolean cfgx_ext_insts_equivalent(ext_configuration c1,
                         Integer inst_id1,
                         ext_configuration c2,
                             Integer inst_id2,
                         int detail) {

        cfg_ext_inst i1 = c1.get_inst(inst_id1);
        cfg_ext_inst i2 = c2.get_inst(inst_id2);

        if ( ! c_ext_cfg_inst_imp.cfg_ext_equivalent(i1, i2, detail) )
             return false;

        // compare parts (depth first recursive)
        cfg_ext_part_seq ps_1 = i1.get_parts();
        cfg_ext_part_seq ps_2 = i2.get_parts();
        if ( ps_1.size() != ps_2.size() )
             return false;

        Iterator eps_1 = ps_1.iterator();
        Iterator eps_2 = ps_2.iterator();

        while ( eps_1.hasNext() ) {
            cfg_ext_part p1 = (cfg_ext_part)eps_1.next();
            cfg_ext_part p2 = (cfg_ext_part)eps_2.next();

            if ( ! c_ext_cfg_part_imp.cfg_ext_equivalent(p1, p2, detail) )
                 return false;
            if ( ! cfgx_ext_insts_equivalent(c1, p1.get_inst_id(),
                             c2, p2.get_inst_id(),
                             detail) )
                 return false;
        }
        return true;
    }

}
