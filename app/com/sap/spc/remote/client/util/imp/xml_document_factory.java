package com.sap.spc.remote.client.util.imp;

// change log:
// 20040903-tl: use standard APIs again to avoid compile-time dependencies to inqmy
// 20031215-ak: use inqmy document builder factory (OSS 691330)

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

//import com.sap.sxe.sys.exc.exc_internal_error;

/**
 * A factory for XML documents.
 */

public abstract class xml_document_factory {

    private static DocumentBuilder utilx_create_builder(boolean validating) {
        try {
            DocumentBuilderFactory inst = DocumentBuilderFactory.newInstance();
            inst.setValidating(validating);
            return inst.newDocumentBuilder();
        } catch (Exception e) {
            throw new RuntimeException("Could not create XML DocumentBuilderFactory");
        }
    }

    /*
     * Creates an empty XML Document.
     */
    public static Document util_create_document() {
        return utilx_create_builder(false).newDocument();
    }

    /*
     * Creates an XML Document from a given string.
     */
    public static Document util_create_document_from_string(String str,
            boolean validating) throws IOException, SAXParseException,
            SAXException {
        InputSource is = new InputSource(new StringReader(str));
        return utilx_create_builder(validating).parse(is);
    }

    /*
     * Creates an XML Document from a given file.
     */
    public static Document util_create_document_from_file(String path,
            boolean validating) throws IOException, SAXParseException,
            SAXException {
        return utilx_create_builder(validating).parse(new File(path));
    }
}