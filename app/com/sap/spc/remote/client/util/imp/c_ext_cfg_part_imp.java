package com.sap.spc.remote.client.util.imp;

/*
   $Workfile: c_ext_cfg_part_imp.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:05 $
 */

//package com.sap.sce.kbrt.imp;

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

//import com.sap.sce.kbrt.cfg_ext_part;
import com.sap.spc.remote.client.util.cfg_ext_part;
//import com.sap.sxe.sys.comparable;
//import com.sap.sxe.sys.exc.exc_illegal_type_arg;
//import com.sap.sxe.util.xml.xml_serializable_element;
import com.sap.spc.remote.client.util.xml_serializable_element;


 /**
  * PartOf relation in external format.
  * Contains parent, child instance id, position number and
  * decomposition item type.
  */
public class c_ext_cfg_part_imp implements cfg_ext_part,
                                           Comparable,
                                           xml_serializable_element
{
        // CONSTANTS
    static final String
            C_XML_TAG_NAME      = "PART",
            C_XML_PARENT_ID     = "PARENT_NR",
            C_XML_INST_ID       = "INST_NR",
            C_XML_POS_NR        = "POS_NR",
            C_XML_OBJ_TYPE      = "OBJ_TYPE",
            C_XML_CLASS_TYPE    = "CLASS_TYPE",
            C_XML_OBJ_KEY       = "OBJ_KEY",
            C_XML_AUTHOR        = "AUTHOR",
            C_XML_SALES_RELEVANT= "S";

    // INSTANCE MEMBERS
    private Integer m_parent_id;
    private Integer m_inst_id;
    private String  m_pos_nr;
    private String  m_obj_type;
    private String  m_class_type;
    private String  m_obj_key;
    private String  m_author;
    private boolean m_sales_relevant;

    // CONSTRUCTORS

    protected c_ext_cfg_part_imp() {
    }

    protected c_ext_cfg_part_imp(Integer parent_id,
                     Integer inst_id,
                     String pos_nr,
                     String obj_type,
                     String class_type,
                     String obj_key,
                     String author,
                     boolean sales_relevant) {
        m_parent_id  = parent_id;
        m_inst_id    = inst_id;
        m_pos_nr     = pos_nr;
        m_obj_type   = obj_type;
        m_class_type = class_type;
        m_obj_key    = obj_key;
        m_author     = author;
        m_sales_relevant = sales_relevant;
    }

        //copy
        protected c_ext_cfg_part_imp(c_ext_cfg_part_imp extPart) {
        m_parent_id  = extPart.get_parent_id();
        m_inst_id    = extPart.get_inst_id();
        m_pos_nr     = extPart.get_pos_nr();
        m_obj_type   = extPart.get_obj_type();
        m_class_type = extPart.get_class_type();
        m_obj_key    = extPart.get_obj_key();
        m_author     = extPart.get_author();
        m_sales_relevant = extPart.is_sales_relevant_p();
    }

    // FIELD ACCESSORS

    public Integer get_parent_id() {
        return m_parent_id;
    }

    public Integer get_inst_id() {
        return m_inst_id;
    }

    public String get_pos_nr() {
        return m_pos_nr;
    }

    public String get_obj_type() {
        return m_obj_type;
    }

    public String get_class_type(){
        return m_class_type;
    }

    public String get_obj_key() {
        return m_obj_key;
    }

    public String get_author() {
        return m_author;
    }

    public boolean is_sales_relevant_p() {
        return m_sales_relevant;
    }

    /**
     * comparable implementation: supports sort by
     * (parent_id, pos_nr, obj_type, class_type, obj_key, inst_id)
     * @param o the Object to be compared
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
     */
    public int compareTo(Object o) {
        if (o == null || !(o instanceof c_ext_cfg_part_imp))
            throw new IllegalArgumentException("c_ext_cfg_part_imp: lt " + o);

        c_ext_cfg_part_imp rhs = (c_ext_cfg_part_imp)o;

        int k = m_parent_id.intValue() - rhs.get_parent_id().intValue();
        if ( k < 0 )
             return -1;
        else if ( k > 0 )
             return 1;

        k = m_pos_nr.compareTo(rhs.get_pos_nr());
        if ( k < 0 )
             return -1;
        else if ( k > 0 )
             return 1;

        k = m_obj_type.compareTo(rhs.get_obj_type());
        if ( k < 0 )
             return -1;
        else if ( k > 0 )
             return 1;

        k = m_class_type.compareTo(rhs.get_class_type());
        if ( k < 0 )
             return -1;
        else if ( k > 0 )
             return 1;

        k = m_obj_key.compareTo(rhs.get_obj_key());
        if ( k < 0 )
             return -1;
        else if ( k > 0 )
             return 1;

        k = m_inst_id.intValue() - rhs.get_inst_id().intValue();
        if ( k < 0 )
            return -1;
        else
            return 1;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("part(");
        result.append(m_parent_id);
        result.append(", ");
        result.append(m_pos_nr);
        result.append(", ");
        result.append(m_obj_type);
        result.append(", ");
        result.append(m_class_type);
        result.append(", ");
        result.append(m_obj_key);
        result.append(", ");
        result.append(m_inst_id);
        result.append(", ");
        result.append(m_author);
        result.append(", ");
        result.append(m_sales_relevant);
        result.append(")");
        return result.toString();
    }


    void set_inst_id(Integer inst_id) {
        m_inst_id = inst_id;
    }

     // COMPARISON
    public static boolean cfg_ext_equivalent(cfg_ext_part p1,
                             cfg_ext_part p2,
                         int detail) {
        return ( p1.get_obj_type().equals(p2.get_obj_type()) &&
             p1.get_class_type().equals(p2.get_class_type()) &&
             p1.get_obj_key().equals(p2.get_obj_key()) );
    }

        // XML PERSISTENCE

        public void util_load_from_xml_element(Element el)
                                               throws IOException, NumberFormatException {
            m_parent_id  = new Integer(el.getAttribute(C_XML_PARENT_ID));
        m_pos_nr     = el.getAttribute(C_XML_POS_NR);
        m_obj_type   = el.getAttribute(C_XML_OBJ_TYPE);
        m_class_type = el.getAttribute(C_XML_CLASS_TYPE);
        m_obj_key    = el.getAttribute(C_XML_OBJ_KEY);
        // m_inst_id  -> is set by caller
        m_author     = el.getAttribute(C_XML_AUTHOR);
        m_sales_relevant = el.getAttribute(C_XML_SALES_RELEVANT).equals("T");
        }

        public Element util_to_xml_element(Document doc) {
            Element result = doc.createElement(C_XML_TAG_NAME);
        result.setAttribute(C_XML_PARENT_ID,  m_parent_id.toString());
        result.setAttribute(C_XML_POS_NR,     m_pos_nr);
        result.setAttribute(C_XML_OBJ_TYPE,   m_obj_type);
        result.setAttribute(C_XML_CLASS_TYPE, m_class_type);
        result.setAttribute(C_XML_OBJ_KEY,    m_obj_key);
        // inst_id is filled in from child element with instance data
        // result.setAttribute(C_XML_INST_ID,    m_inst_id.toString());
        result.setAttribute(C_XML_AUTHOR,     m_author);
        result.setAttribute(C_XML_SALES_RELEVANT, m_sales_relevant ? "T" : "F");
        return result;
        }

}

