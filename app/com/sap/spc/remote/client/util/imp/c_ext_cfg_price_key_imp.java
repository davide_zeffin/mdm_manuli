package com.sap.spc.remote.client.util.imp;

/*
   $Workfile: c_ext_cfg_price_key_imp.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:05 $
 */

//package com.sap.sce.kbrt.imp;

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;


//import com.sap.sce.kbrt.cfg_ext_price_key;
import com.sap.spc.remote.client.util.cfg_ext_price_key;
//import com.sap.sxe.sys.comparable;
//import com.sap.sxe.sys.exc.exc_illegal_type_arg;
//import com.sap.sxe.util.xml.xml_serializable_element;
import com.sap.spc.remote.client.util.xml_serializable_element;

 /**
  * Characteristic value assignment in external format.
  * Contains cstic name and value in string format.
  */
public class c_ext_cfg_price_key_imp implements cfg_ext_price_key,
                                                Comparable,
                                                xml_serializable_element
{
     // CONSTANTS
    static final String
             C_XML_TAG_NAME      = "PKEY",
             C_XML_KEY           = "KEY",
                     C_XML_FACTOR        = "FACTOR";


     // INSTANCE MEMBERS
    private Integer m_inst_id;
    private String  m_key;
    private double  m_factor;


  
  /* a3-349tl:
      * Converts a double to a String that has at most
      * 15 characters as specified in the RFCCommands.xml. 
      * The double is rounded to fit into the 15 digits. 
      */
     private static DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.US);
     //used positive numbers with a 2 digit exponent (e.g. E1)
     private static DecimalFormat C_DF_1 = new DecimalFormat("0.0##########E0", dfs);
     //used for negative numbers with a 2 digit-exponent (e.g. E1) and positive numbers
     //with a 3 digit exponent (E-1, E11)
     private static DecimalFormat C_DF_2 = new DecimalFormat("0.0#########E0", dfs);
     //used for negative numbers with a 3 digit-exponent (e.g. E-1) and positive numbers
     //with a 4 digit exponent (e.g. E-11, E111)
     private static DecimalFormat C_DF_3 = new DecimalFormat("0.0########E0", dfs);
     //used for negative numbers with a 4 digit-exponent (e.g. E-11, E111) and positive numbers
     //with a 5 digit exponent (e.g. E-111)
     private static DecimalFormat C_DF_4 = new DecimalFormat("0.0#######E0", dfs);
     //used for negative numbers with a 5 digit-exponent (e.g. E-111)
     private static DecimalFormat C_DF_5 = new DecimalFormat("0.0######E0", dfs);
     private static double        C_10_POW_PLUS_7 = Math.pow(10.0, 7.0); 
     private static double        C_10_POW_PLUS_10 = Math.pow(10.0, 10.0); 
     private static double        C_10_POW_PLUS_100 = Math.pow(10.0, 100.0); 
     private static double        C_10_POW_MINUS_3 = Math.pow(10.0, -3.0); 
     private static double        C_10_POW_MINUS_9 = Math.pow(10.0, -9.0); 
     private static double        C_10_POW_MINUS_99= Math.pow(10.0, -99.0); 

     // CONSTRUCTORS

    protected c_ext_cfg_price_key_imp () {
    }

    protected c_ext_cfg_price_key_imp(Integer inst_id,
                      String  key,
                      double  factor) {
        m_inst_id    = inst_id;
        m_key        = key;
        m_factor     = factor;
    }

        // copy
        protected c_ext_cfg_price_key_imp (c_ext_cfg_price_key_imp extPriceKey) {
           m_inst_id = extPriceKey.get_inst_id();
           m_key =  extPriceKey.get_key();
           m_factor = extPriceKey.get_factor();
    }

    // FIELD ACCESSORS

    public Integer get_inst_id() {
        return m_inst_id;
    }

    public String get_key() {
        return m_key;
    }

    public double get_factor(){
        return m_factor;
    }

        public void set_inst_id(Integer inst_id) {
           m_inst_id = inst_id;
        }

    /**
     * comparable implementation: supports sort by (inst_id, key)
     * @param o the Object to be compared
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
     */
    public int compareTo(Object o) {
        if (o == null || !(o instanceof c_ext_cfg_price_key_imp))
            throw new IllegalArgumentException("c_ext_cfg_price_key_imp: lt " + o);

        c_ext_cfg_price_key_imp rhs = (c_ext_cfg_price_key_imp)o;

        int k = m_inst_id.intValue() - rhs.get_inst_id().intValue();
        if ( k < 0 )
             return -1;
        else if ( k > 0 )
             return 1;

        k = m_key.compareTo(rhs.get_key());
        if ( k < 0 )
            return -1;
        else 
            return 1;
       
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("price_key(");
        result.append(m_inst_id);
        result.append(", ");
        result.append(m_key);
        result.append(", ");
        result.append(String.valueOf(m_factor));
        result.append(")");
        return result.toString();
    }

        // XML PERSISTENCE
        public void util_load_from_xml_element(Element el)
                                               throws IOException, NumberFormatException {
           // m_inst_id is set by caller
           m_key = el.getAttribute(C_XML_KEY);
       String factor = el.getAttribute(C_XML_FACTOR);
       if ( factor.length() == 0 ) {
            m_factor = (double)1.0;
       }
       else {
            m_factor = Double.valueOf(factor).doubleValue();
       }
        }

        public Element util_to_xml_element(Document doc) {
           Element result = doc.createElement(C_XML_TAG_NAME);
       result.setAttribute(C_XML_KEY,    m_key);
       result.setAttribute(C_XML_FACTOR, getFactorAsChar15());       
       return result;
        }

        // COMPARISON
    public static boolean cfg_ext_equivalent(cfg_ext_price_key pk1,
                         cfg_ext_price_key pk2,
                         int detail) {
        return ( pk1.get_key().equals(pk2.get_key()) &&
             pk1.get_factor() == pk2.get_factor());
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.util.cfg_ext_price_key#getFactorAsChar15()
     */
    public String getFactorAsChar15() {
         double magnitude = Math.abs(m_factor);
         String factor = null;
         //magnitude >= 1.0E100
         if(magnitude >= C_10_POW_PLUS_100){
             if(m_factor >= 0)
                 factor = C_DF_3.format(m_factor);
             else
                 factor = C_DF_4.format(m_factor);
         }
         //1.0E100 > magnitude >= 1.0E10
         else if(magnitude >= C_10_POW_PLUS_10){
             if(m_factor >= 0)
                 factor = C_DF_2.format(m_factor);
             else
                 factor = C_DF_3.format(m_factor);
         }
         //1.0E10 > magnitude >= 1.0E7
         else if(magnitude >= C_10_POW_PLUS_7){
             if(m_factor >= 0)
                 factor = C_DF_1.format(m_factor);
             else
                 factor = C_DF_2.format(m_factor);
         }
         //if factor is just a number between 1.0E-3 and 1.0E7
         //the digits exceeding the index 15 are just cut of without rounding
         else if(C_10_POW_PLUS_7 > magnitude &&  magnitude >= C_10_POW_MINUS_3 ){
             factor = Double.toString(m_factor);
             factor = factor.substring(0, Math.min(factor.length(), 15));
         }
         //1.0E-3 >= magnitude > 1.0E-10
         else if(C_10_POW_MINUS_3 > magnitude && magnitude > C_10_POW_MINUS_9){
             if(m_factor >= 0)
                 factor = C_DF_2.format(m_factor);
             else
                 factor = C_DF_3.format(m_factor);
         }
         //1.0E-10 >= magnitude > 1.0E-100
         else if(C_10_POW_MINUS_9 >= magnitude && magnitude > C_10_POW_MINUS_99){
             if(m_factor >= 0)
                 factor = C_DF_3.format(m_factor);
             else
                 factor = C_DF_4.format(m_factor);
         }
         //1.0E-100 >= magnitude > 0.0
         else if(C_10_POW_MINUS_99 >= magnitude ){
             if(m_factor >= 0)
                 factor = C_DF_4.format(m_factor);
             else
                 factor = C_DF_5.format(m_factor);
         }
         return factor;
    }

}

