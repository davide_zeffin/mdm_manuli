package com.sap.spc.remote.client.util.imp;

/*
   $Workfile: c_ext_cfg_price_key_seq_imp.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:05 $
 */

//import java.util.Enumeration;
import java.util.Iterator;

//import com.sap.sce.kbrt.cfg_ext_price_key_seq;
import com.sap.spc.remote.client.util.cfg_ext_price_key_seq;
//import com.sap.sxe.sys.seq.ordered_set;
import java.util.TreeSet;

 /**
  * Container for characteristic value assignments in external format.
  */
public class c_ext_cfg_price_key_seq_imp extends TreeSet /*ordered_set*/ implements cfg_ext_price_key_seq
{
    // CONSTANTS

    final static c_ext_cfg_price_key_seq_imp C_EMPTY = new c_ext_cfg_price_key_seq_imp();

    // CONSTRUCTOR

    c_ext_cfg_price_key_seq_imp() {
    //  default constructor for ordered_set uses comparable interface
        super();
    }

    public String toString() {
        StringBuffer strbuf = new StringBuffer();
        for(Iterator e = iterator(); e.hasNext(); ) {
            c_ext_cfg_price_key_imp current_price_key = (c_ext_cfg_price_key_imp)e.next();
            strbuf.append(current_price_key.toString());
        }
        return strbuf.toString();
    }

}

