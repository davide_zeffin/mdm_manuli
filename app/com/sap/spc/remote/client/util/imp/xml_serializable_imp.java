package com.sap.spc.remote.client.util.imp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

//import com.sap.sxe.util.xml.xml_serializable;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.util.xml_serializable;
import com.sap.isa.core.util.JspUtil;

/**
 * An abstract class for xml_serializable: implements standard document and file
 * handling. Core methods util_load_from_xml_element and util_to_xml_element
 * must be implemented for the non-abstract subclass.
 */

public abstract class xml_serializable_imp implements xml_serializable {

    public abstract void util_load_from_xml_element(Element el)
            throws IOException, NumberFormatException;

    public abstract Element util_to_xml_element(Document doc);

    /*
     * Initializes this object and fills its content from the XML string.
     */
    public void util_load_from_xml_string(String str, boolean validating)
            throws IOException, SAXException, SAXParseException {
        if (str == null)
            throw new NullPointerException(
                    "util_load_from_xml_string: string not specified");

        Document doc = xml_document_factory.util_create_document_from_string(
                str, validating);
        Element root_element = doc.getDocumentElement();
        util_load_from_xml_element(root_element);
    }

    /*
     * Creates an XML String representation for this object.
     */
    public String util_to_xml_string() throws IOException {
        Document doc = xml_document_factory.util_create_document();
        Element root_element = util_to_xml_element(doc);
        doc.appendChild(root_element);
        StringWriter stringWriter = new StringWriter();
        try {
            TransformerFactory transformerFactory = TransformerFactory
                    .newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
            transformer.transform(new DOMSource(doc), new StreamResult(
                    stringWriter));
        } catch (TransformerConfigurationException tce) {
        	throw new IPCException(tce);
        } catch (TransformerException te) {
        	throw new IPCException(te);
        }

        String xmlfile = stringWriter.getBuffer().toString();
        stringWriter.close();
        return xmlfile;
    }

    /*
     * Initializes this object and fills its content from an XML file.
     */
    public void util_load_from_xml_file(String path, boolean validating)
            throws IOException, SAXException, SAXParseException {
        if (path == null)
            throw new NullPointerException(
                    "util_load_from_xml_file: path not specified");
        Document doc = xml_document_factory.util_create_document_from_file(
                path, validating);
        Element root_element = doc.getDocumentElement();
        util_load_from_xml_element(root_element);
    }

    /*
     * Saves this object to an XML file.
     */
    public void util_to_xml_file(String path) throws IOException {
        if (path == null)
            throw new NullPointerException(
                    "util_to_xml_file: path not specified");
        FileOutputStream fout = new FileOutputStream(path);
        util_to_xml_stream(fout);
        fout.close();
        fout = null;
    }

    /*
     * Saves this object to an XML output stream.
     */
    public void util_to_xml_stream(OutputStream os) throws IOException {
        if (os == null)
            throw new NullPointerException(
                    "util_to_xml_stream: stream not specified");
        Document doc = xml_document_factory.util_create_document();
        Element root_element = util_to_xml_element(doc);
        doc.appendChild(root_element);
        try {
            TransformerFactory transformerFactory = TransformerFactory
                    .newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(
                    os));
        } catch (TransformerConfigurationException tce) {
        	throw new IPCException(tce);
        } catch (TransformerException te) {
        	throw new IPCException(te);
        }
        doc = null;
    }
}