package com.sap.spc.remote.client.util.imp;

/*
   $Workfile: c_ext_cfg_inst_imp.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:05 $
 */

// change log:
// 20031204-ak: made factory methods protected (OSS 687219)
// 20020930-ak: added set_display_info (translation, display infos)
// 20020918-ak: rework: established this as THE default implementation of cfg_ext_inst
// 20020416-ak: load OBJ_TXT from XML
// 20010906-kha: fixed NullPointerException in copy constructor for sub instances without cstics
// 20010227-ak: added OBJ_TXT attribute for XML
// 20010309-ak: added domain restrictions
// 19990903-ak: added quantity unit

import java.io.IOException;
import com.sap.isa.core.util.JspUtil;
import java.io.PrintWriter;
//import java.util.Enumeration;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

//import com.sap.sce.kbrt.*;
import com.sap.spc.remote.client.util.*;
//import com.sap.sxe.sys.language;
//import com.sap.sxe.sys.read_only_sequence;
//import com.sap.sxe.util.xml.xml_serializable_element;

 /**
  * Instance in external format.
  * Contains current type of the instance and instance quantity.
  * Has local containers with parts and cstic values/domains in ext format.
  */
public class c_ext_cfg_inst_imp // extends hashtable  20020919-ak
                                implements cfg_ext_inst,
                                           xml_serializable_element
{
        // CONSTANTS
    static final String
                C_XML_TAG_NAME      = "INST",
            C_XML_INST_ID       = "NR",
            C_XML_OBJ_TYPE      = "OBJ_TYPE",
            C_XML_CLASS_TYPE    = "CLASS_TYPE",
            C_XML_OBJ_KEY       = "OBJ_KEY",
                        C_XML_OBJ_TXT       = "OBJ_TXT",
            C_XML_AUTHOR        = "AUTHOR",
            C_XML_QUANTITY      = "QTY",
            C_XML_QUANTITY_UNIT = "UNIT",
            C_XML_CONSISTENT    = "CONSISTENT",
            C_XML_COMPLETE      = "COMPLETE",
            C_XML_CSTICS_VALUES = "CSTICS",
            C_XML_PRICE_KEYS    = "PKEYS";


        // INSTANCE MEMBERS
    private Integer m_inst_id;
    private String  m_obj_type;
    private String  m_class_type;
    private String  m_obj_key;
    private String  m_obj_txt;
    private String  m_author;
    private String  m_quantity;
    private String  m_quantity_unit;
    private boolean m_consistent;
    private boolean m_complete;

    private c_ext_cfg_part_seq_imp      m_parts;
    private c_ext_cfg_cstic_val_seq_imp m_cstic_vals;
    private c_ext_cfg_price_key_seq_imp m_price_keys;
        private c_ext_cfg_cstic_dom_element_seq_imp m_cstic_domains;


    // CONSTRUCTOR

    protected c_ext_cfg_inst_imp() {
    }

    protected c_ext_cfg_inst_imp(Integer inst_id,
                     String obj_type,
                     String class_type,
                     String obj_key,
                     String obj_txt,
                     String author,
                     String quantity,
                     String quantity_unit,
                     boolean consistent,
                     boolean complete) {
        m_inst_id    = inst_id;
        m_obj_type   = obj_type;
        m_class_type = class_type;
        m_obj_key    = obj_key;
        m_obj_txt    = obj_txt;
        m_author     = author;
        m_quantity   = quantity;
        m_quantity_unit = quantity_unit;
        m_consistent = consistent;
        m_complete   = complete;
        m_parts      = null;
        m_cstic_vals = null;
                m_cstic_domains = null;
        m_price_keys = null;
    }

       /**
    *  Copies a given external instance ('shallow copy').
        *  Note that elements of the copied instance (cstic values, price keys)
        *  need not match the local factory type of this external instance !!
        *  @internal
    */
        /* 20021022-ak: removed this constructor because it's not type consistent
        protected c_ext_cfg_inst_imp(c_ext_cfg_inst_imp extInst)
    {
        m_inst_id    = extInst.get_inst_id();
        m_obj_type   = extInst.get_obj_type();
        m_class_type = extInst.get_class_type();
        m_obj_key    = extInst.get_obj_key();
        m_obj_txt    = extInst.get_obj_txt();
        m_author     = extInst.get_author();
        m_quantity   = extInst.get_quantity();
        m_quantity_unit = extInst.get_quantity_unit();
        m_consistent = extInst.is_consistent_p();
        m_complete   =  extInst.is_complete_p();
                m_parts = extInst.m_parts;
                cfgx_ensure_cstic_vals();
                // 20010906-kha: fixed NullPointerException for sub instances without cstics
                if (extInst.m_cstic_vals != null)
                    m_cstic_vals.push_seq((extInst.m_cstic_vals.elements()));
            m_price_keys = extInst.m_price_keys;
    }
        */

    // FIELD ACCESSORS

    public Integer get_inst_id() {
        return m_inst_id;
    }

    public String get_obj_type() {
        return m_obj_type;
    }

    public String get_class_type(){
        return m_class_type;
    }

    public String get_obj_key() {
        return m_obj_key;
    }

    public String get_obj_txt() {
        return m_obj_txt;
    }

    public String get_author() {
        return m_author;
    }

    public String get_quantity() {
        return m_quantity;
    }

    public String get_quantity_unit() {
        return m_quantity_unit;
    }

        public void set_quantity_unit(String quantity_unit) {
               m_quantity_unit = quantity_unit;
        }

    public boolean is_complete_p() {
        return m_complete;
    }

    public boolean is_consistent_p() {
        return m_consistent;
    }

        void set_obj_txt(String obj_txt) {
       m_obj_txt = obj_txt;
    }

    // ACCESSORS FOR PARTS, CSTICS VALUES AND PRICE KEYS

    public cfg_ext_part_seq get_parts() {
        if ( m_parts == null ) 
             return c_ext_cfg_part_seq_imp.C_EMPTY;
        return m_parts;
    }

    public cfg_ext_cstic_val_seq get_cstics_values() {
        if ( m_cstic_vals == null )
             return c_ext_cfg_cstic_val_seq_imp.C_EMPTY;
        return m_cstic_vals;
    }

    public cfg_ext_price_key_seq get_price_keys() {
        if ( m_price_keys == null )
             return c_ext_cfg_price_key_seq_imp.C_EMPTY;
        return m_price_keys;
    }

        public cfg_ext_cstic_dom_element_seq get_cstics_domains() {
        if ( m_cstic_domains == null )
             return c_ext_cfg_cstic_dom_element_seq_imp.C_EMPTY;
        return m_cstic_domains;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("inst(");
        result.append(m_inst_id);
        result.append(", ");
        result.append(m_obj_type);
        result.append(", ");
        result.append(m_class_type);
        result.append(", ");
        result.append(m_obj_key);
        result.append(", ");
        result.append(m_author);
        result.append(", ");
        result.append(m_quantity);
        result.append(", ");
        result.append(m_quantity_unit);
        result.append(", ");
        result.append(String.valueOf(m_consistent));
        result.append(", ");
        result.append(String.valueOf(m_complete));
        result.append(" ,");
        result.append("parts(");
        result.append(m_parts);
        result.append("), ");
        result.append("values(");
        result.append(m_cstic_vals);
        result.append("), ");
        result.append("price_keys(");
        result.append(m_price_keys);
        result.append("), ");
        result.append("cstic domains(");
        result.append(m_cstic_domains);
        result.append("))");
        return result.toString();
    }


    // COMPARISON
    public static boolean cfg_ext_equivalent(cfg_ext_inst i1,
                         cfg_ext_inst i2,
                         int detail) {
        if ( detail >= c_ext_cfg_imp.C_STRUCTURE_AND_STATUS_DETAIL ) {
             if ( i1.is_consistent_p() != i2.is_consistent_p() )
                  return false;
             if ( i1.is_complete_p() != i2.is_complete_p() )
                  return false;
        }

        if ( ! i1.get_obj_type().equals(i2.get_obj_type()) ||
             ! i1.get_class_type().equals(i2.get_class_type()) ||
             ! i1.get_obj_key().equals(i2.get_obj_key()) )
             return false;

        if ( ! i1.get_quantity().equals(i2.get_quantity()) )  // @@@ unit
             return false;

        cfg_ext_cstic_val_seq cvs_1 = i1.get_cstics_values();
        cfg_ext_cstic_val_seq cvs_2 = i2.get_cstics_values();
        if ( cvs_1.size() != cvs_2.size() )
             return false;

        Iterator ecvs_1 = cvs_1.iterator();
        Iterator ecvs_2 = cvs_2.iterator();

        while ( ecvs_1.hasNext() ) {
            cfg_ext_cstic_val c1 = (cfg_ext_cstic_val)ecvs_1.next();
            cfg_ext_cstic_val c2 = (cfg_ext_cstic_val)ecvs_2.next();
            if (! c_ext_cfg_cstic_val_imp.cfg_ext_equivalent(c1, c2, detail))
                 return false;
        }

        cfg_ext_price_key_seq pks_1 = i1.get_price_keys();
        cfg_ext_price_key_seq pks_2 = i2.get_price_keys();
        if ( pks_1.size() != pks_2.size() )
             return false;

        Iterator epk_1 = pks_1.iterator();
        Iterator epk_2 = pks_2.iterator();

        while ( epk_1.hasNext() ) {
            cfg_ext_price_key pk1 = (cfg_ext_price_key)epk_1.next();
            cfg_ext_price_key pk2 = (cfg_ext_price_key)epk_2.next();
            if ( ! c_ext_cfg_price_key_imp.cfg_ext_equivalent(pk1, pk2,
                                                            detail) )
                 return false;
        }

                cfg_ext_cstic_dom_element_seq cds_1 = i1.get_cstics_domains();
        cfg_ext_cstic_dom_element_seq cds_2 = i2.get_cstics_domains();
        if ( cds_1.size() != cds_2.size() )
             return false;

        Iterator ecd_1 = cds_1.iterator();
        Iterator ecd_2 = cds_2.iterator();

        while ( ecd_1.hasNext() ) {
            cfg_ext_cstic_dom_element cd1 = (cfg_ext_cstic_dom_element)ecd_1.next();
            cfg_ext_cstic_dom_element cd2 = (cfg_ext_cstic_dom_element)ecd_2.next();
            if ( ! c_ext_cfg_cstic_dom_element_imp.cfg_ext_equivalent(cd1, cd2,
                                                                      detail) )
                 return false;
        }
        return true;
    }


    // PROTECTED INSTANCE METHODS

    void add_part(c_ext_cfg_part_imp part) {
        cfgx_ensure_parts();
        m_parts.add(part);
    }

    void add_cstic_value(c_ext_cfg_cstic_val_imp value) {
        cfgx_ensure_cstic_vals();
        m_cstic_vals.add(value);
    }

    void add_price_key(c_ext_cfg_price_key_imp price_key) {
        cfgx_ensure_price_keys();
        m_price_keys.add(price_key);
    }

        void add_cstic_dom_element(c_ext_cfg_cstic_dom_element_imp dom_element) {
        cfgx_ensure_cstic_domains();
        m_cstic_domains.add(dom_element);
    }

    void cfgx_ensure_parts() {
        if ( m_parts == null )
             m_parts = new c_ext_cfg_part_seq_imp();
    }

    void cfgx_ensure_cstic_vals() {
        if ( m_cstic_vals == null )
             m_cstic_vals = new c_ext_cfg_cstic_val_seq_imp();
    }

    void cfgx_ensure_price_keys() {
        if ( m_price_keys == null )
             m_price_keys = new c_ext_cfg_price_key_seq_imp();
    }

        void cfgx_ensure_cstic_domains() {
        if ( m_cstic_domains == null )
             m_cstic_domains = new c_ext_cfg_cstic_dom_element_seq_imp();
    }

        // ELEMENT FACTORIES

        protected c_ext_cfg_cstic_val_imp newCsticValue() {
            return new c_ext_cfg_cstic_val_imp();
        }

        protected c_ext_cfg_price_key_imp newPriceKey() {
            return new c_ext_cfg_price_key_imp();
        }

        // PRESENTATION AND DISPLAY INFOS

        // XML PERSISTENCE

        public void util_load_from_xml_element(Element el)
                                               throws IOException, NumberFormatException {

        m_inst_id    = new Integer(el.getAttribute(C_XML_INST_ID));
        m_obj_type   = el.getAttribute(C_XML_OBJ_TYPE);
        m_class_type = el.getAttribute(C_XML_CLASS_TYPE);
        m_obj_key    = el.getAttribute(C_XML_OBJ_KEY);
                m_obj_txt    = el.getAttribute(C_XML_OBJ_TXT);       // 20020416-ak
        m_author     = el.getAttribute(C_XML_AUTHOR);
        m_quantity   = el.getAttribute(C_XML_QUANTITY);
        m_quantity_unit = el.getAttribute(C_XML_QUANTITY_UNIT);
        m_consistent = ( el.getAttribute(C_XML_CONSISTENT).equals("T") );
        m_complete   = ( el.getAttribute(C_XML_COMPLETE).equals("T") );

                m_parts      = null;
        m_cstic_vals = null;
                m_cstic_domains = null;
        m_price_keys = null;

        NodeList l = el.getElementsByTagName(C_XML_CSTICS_VALUES);
        if ( l.getLength() == 1 ) {
             NodeList c = ((Element)l.item(0)).
                  getElementsByTagName(c_ext_cfg_cstic_val_imp.C_XML_TAG_NAME);
             for (int i = 0; i < c.getLength(); ++i) {
                          c_ext_cfg_cstic_val_imp cv = newCsticValue();
                          cv.util_load_from_xml_element((Element)c.item(i));
                          cv.set_inst_id(m_inst_id);
              add_cstic_value(cv);
            }
        }

        l = el.getElementsByTagName(C_XML_PRICE_KEYS);
        if ( l.getLength() == 1 ) {
             NodeList pk = ((Element)l.item(0)).
                         getElementsByTagName(c_ext_cfg_price_key_imp.C_XML_TAG_NAME);
             for (int i = 0; i < pk.getLength(); ++i) {
                          c_ext_cfg_price_key_imp pkey = newPriceKey();
                          pkey.util_load_from_xml_element((Element)pk.item(i));
                          pkey.set_inst_id(m_inst_id);
              add_price_key(pkey);
             }
        }
        }

        public Element util_to_xml_element(Document doc) {
        Element result = doc.createElement(C_XML_TAG_NAME);
        result.setAttribute(C_XML_INST_ID,    m_inst_id.toString());
        result.setAttribute(C_XML_OBJ_TYPE,   m_obj_type);
        result.setAttribute(C_XML_CLASS_TYPE, m_class_type);
        result.setAttribute(C_XML_OBJ_KEY,    m_obj_key);
                if ( m_obj_txt.length() > 0 ) {
            result.setAttribute(C_XML_OBJ_TXT,  m_obj_txt);
        }
        result.setAttribute(C_XML_AUTHOR,     m_author);
        result.setAttribute(C_XML_QUANTITY,   m_quantity);
        result.setAttribute(C_XML_QUANTITY_UNIT, m_quantity_unit);
        result.setAttribute(C_XML_CONSISTENT, m_consistent ? "T" : "F");
        result.setAttribute(C_XML_COMPLETE,   m_complete ? "T" : "F");

        cfg_ext_cstic_val_seq cvs = get_cstics_values();
        if ( cvs.size() > 0 ) {
             Element cstics = doc.createElement(C_XML_CSTICS_VALUES);
             result.appendChild(cstics);

             for(Iterator e = cvs.iterator();e.hasNext();) {
              	
            	 c_ext_cfg_cstic_val_imp cv = (c_ext_cfg_cstic_val_imp)e.next();

            	 c_ext_cfg_cstic_val_imp temp = 
            		 					new c_ext_cfg_cstic_val_imp(cv.get_inst_id(),
            		 							
        		 							JspUtil.replaceSpecialCharacters(cv.get_charc()),
        		 							JspUtil.replaceSpecialCharacters(cv.get_charc_txt()),
        		 							JspUtil.replaceSpecialCharacters(cv.get_value()),
        		 							JspUtil.replaceSpecialCharacters(cv.get_value_txt()),            		 							
     										cv.get_author(),
     										cv.is_invisible_p());             

            	 cstics.appendChild(((xml_serializable_element)temp).
                         util_to_xml_element(doc));             
             }
        }

        cfg_ext_price_key_seq pks = get_price_keys();
        if ( pks.size() > 0 ) {
             Element pkeys = doc.createElement(C_XML_PRICE_KEYS);
             result.appendChild(pkeys);
             for(Iterator e = pks.iterator();
             e.hasNext();
            ) {
               pkeys.appendChild(((xml_serializable_element)e.next()).
                            util_to_xml_element(doc));
             }
        }
        return result;
    }

        // DUMP

        protected void cfgx_ext_dump(PrintWriter ps) {
       ps.println(cfgxx_inst_string_for_dump());
       for(Iterator e = get_cstics_values().iterator();
           e.hasNext();
           ) {
          ps.println("  " + e.next().toString());
       }
       for(Iterator e = get_price_keys().iterator();
           e.hasNext();
           ) {
          ps.println("  " + e.next().toString());
       }
           for(Iterator e = get_cstics_domains().iterator();
           e.hasNext();
           ) {
          ps.println("  " + e.next().toString());
       }
    }

        // returns a string for the proper instance data (does not include inner elements)
        private String cfgxx_inst_string_for_dump() {
        StringBuffer result = new StringBuffer();
        result.append("inst(");
        result.append(get_inst_id());
        result.append(", ");
        result.append(get_obj_type());
        result.append(", ");
        result.append(get_class_type());
        result.append(", ");
        result.append(get_obj_key());
        result.append(", ");
        result.append(get_author());
        result.append(", ");
        result.append(get_quantity());
        result.append(", ");
        result.append(get_quantity_unit());
        result.append(", ");
        result.append(String.valueOf(is_consistent_p()));
        result.append(", ");
        result.append(String.valueOf(is_complete_p()));
        result.append(")");
        return result.toString();
    }

}

