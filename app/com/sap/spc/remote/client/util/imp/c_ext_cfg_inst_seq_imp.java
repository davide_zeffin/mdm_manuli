package com.sap.spc.remote.client.util.imp;

/*
   $Workfile: c_ext_cfg_inst_seq_imp.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:05 $
 */

//package com.sap.sce.kbrt.imp;

import java.util.Enumeration;
import java.util.Hashtable;

//import com.sap.sce.kbrt.*;
import com.sap.spc.remote.client.util.cfg_ext_inst_seq;
//import com.sap.sxe.sys.seq.hashtable;

 /**
  * Container for instances in external format.
  * External instances are hashed by inst id.
  */
public class c_ext_cfg_inst_seq_imp extends Hashtable/*hashtable*/ implements cfg_ext_inst_seq
{
    public static final c_ext_cfg_inst_seq_imp C_EMPTY = new c_ext_cfg_inst_seq_imp();

    //CONSTRUCTOR
    public c_ext_cfg_inst_seq_imp() {
        super();
    }

    public String toString() {
        StringBuffer strbuf = new StringBuffer();
        for (Enumeration e = elements(); e.hasMoreElements();) {
            c_ext_cfg_inst_imp current_instance = (c_ext_cfg_inst_imp)e.nextElement();
            strbuf.append(current_instance.toString() + "\n");
        }
        return strbuf.toString();
    }

}

