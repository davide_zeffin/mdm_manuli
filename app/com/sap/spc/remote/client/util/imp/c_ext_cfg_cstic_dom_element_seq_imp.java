package com.sap.spc.remote.client.util.imp;
/*
   $Workfile: c_ext_cfg_cstic_val_seq_imp.java $
   $Revision: #2 $
   $Author: d034517 $
   $Date: 2005/03/04 $
 */

//import com.sap.sce.kbrt.cfg_ext_cstic_dom_element_seq;
import java.util.TreeSet;

import com.sap.spc.remote.client.util.cfg_ext_cstic_dom_element_seq;
// import com.sap.sxe.sys.seq.ordered_set;

 /**
  * Container for characteristic domain elements in external format.
  */
public class c_ext_cfg_cstic_dom_element_seq_imp extends TreeSet /*ordered_set*/
                                   implements cfg_ext_cstic_dom_element_seq
{
    // CONSTANTS

    final static cfg_ext_cstic_dom_element_seq C_EMPTY =
                               new c_ext_cfg_cstic_dom_element_seq_imp();

}

