package com.sap.spc.remote.client.util.imp;

/*
   $Workfile: c_ext_cfg_cstic_val_imp.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:05 $
 */

// change log:
// 20020930-ak: added set_display_info (translation)
// 20020919-ak: made this the standard implementation of cfg_ext_cstic_val
// 20010302-ak: added invisible flag for cstic values

import java.io.IOException;
import java.io.Serializable;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

//import com.sap.sce.kbrt.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
//import com.sap.sxe.sys.comparable;
//import com.sap.sxe.sys.exc.exc_illegal_type_arg;
//import com.sap.sxe.util.xml.xml_serializable_element;
import com.sap.spc.remote.client.util.xml_serializable_element;


 /**
  * Characteristic value assignment in external format.
  * Contains cstic name and value in string format.
  */
public class c_ext_cfg_cstic_val_imp implements cfg_ext_cstic_val,
                            Comparable,
                            xml_serializable_element,
                            Serializable
{

        // CONSTANTS
    static final String
        C_XML_TAG_NAME      = "CSTIC",
        C_XML_CHARC         = "CHARC",
        C_XML_CHARC_TXT     = "CHARC_TXT",
        C_XML_VALUE         = "VALUE",
        C_XML_VALUE_TXT     = "VALUE_TXT",
        C_XML_AUTHOR        = "AUTHOR",
                C_XML_INVISIBLE     = "INVISIBLE";

        static final String
                C_EMPTY_STR         = "";

    // INSTANCE MEMBERS
    private Integer m_inst_id;
    private String  m_charc;
    private String  m_charc_txt;
    private String  m_value;
    private String  m_value_txt;
    private String  m_author;
        private boolean m_invisible;

    // CONSTRUCTORS

    protected c_ext_cfg_cstic_val_imp () {
    }

        protected c_ext_cfg_cstic_val_imp(Integer inst_id,
                      String  charc,
                      String  charc_txt,
                      String  value,
                      String  value_txt,
                      String  author) {
        m_inst_id    = inst_id;
        m_charc      = charc;
        m_charc_txt  = charc_txt;
        m_value      = value;
        m_value_txt  = value_txt;
        m_author     = author;
                m_invisible  = false;
    }

    protected c_ext_cfg_cstic_val_imp(Integer inst_id,
                      String  charc,
                      String  charc_txt,
                      String  value,
                      String  value_txt,
                      String  author,
                                          boolean invisible) {
        m_inst_id    = inst_id;
        m_charc      = charc;
        m_charc_txt  = charc_txt;
        m_value      = value;
        m_value_txt  = value_txt;
        m_author     = author;
                m_invisible  = invisible;
    }

        // copy
        // @internal
        protected c_ext_cfg_cstic_val_imp (c_ext_cfg_cstic_val_imp extCsticVal) {
               m_inst_id    = extCsticVal.get_inst_id();
               m_charc      = extCsticVal.get_charc();
               m_charc_txt  = extCsticVal.get_charc_txt();
               m_value      = extCsticVal.get_value();
               m_value_txt  = extCsticVal.get_value_txt();
               m_author     = extCsticVal.get_author();
               m_invisible  = extCsticVal.is_invisible_p();
    }

    // FIELD ACCESSORS

    public Integer get_inst_id() {
        return m_inst_id;
    }

    public String get_charc() {
        return m_charc;
    }

    public String get_charc_txt(){
        return m_charc_txt;
    }

    public String get_value() {
        return m_value;
    }

    public String get_value_txt() {
        return m_value_txt;
    }

    public String get_author() {
        return m_author;
    }

        public boolean is_invisible_p() {
        return m_invisible;
        }

        public void set_inst_id(Integer inst_id) {
           m_inst_id = inst_id;
        }

        void set_charc_txt(String charc_txt) {
           m_charc_txt = charc_txt;
        }

        void set_value_txt(String value_txt) {
           m_value_txt = value_txt;
        }


    /**
     * comparable implementation: supports sort by (inst_id, charc, value)
     * @param o the Object to be compared
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
     */
    public int compareTo(Object o) {
        if (o == null || !(o instanceof c_ext_cfg_cstic_val_imp))
            throw new IllegalArgumentException("c_ext_cfg_cstic_val_imp: lt " + o);

        c_ext_cfg_cstic_val_imp rhs = (c_ext_cfg_cstic_val_imp)o;

        int k = m_inst_id.intValue() - rhs.get_inst_id().intValue();
        if ( k < 0 )
             return -1;
        else if ( k > 0 )
             return 1;

        k = m_charc.compareTo(rhs.get_charc());
        if ( k < 0 )
             return -1;
        else if ( k > 0 )
             return 1;

        k = m_value.compareTo(rhs.get_value());
        if ( k < 0 )
            return -1;
        else 
            return 1;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("cstic_val(");
        result.append(m_inst_id);
        result.append(", ");
        result.append(m_charc);
        result.append(", ");
        result.append(m_charc_txt);
        result.append(", ");
        result.append(m_value);
            result.append(", ");
        result.append(m_value_txt);
        result.append(", ");
        result.append(m_author);
                result.append(", ");
        result.append(String.valueOf(m_invisible));
        result.append(")");
        return result.toString();
    }

       // PRESENTATION AND DISPLAY INFOS

     // COMPARISON

        public static boolean cfg_ext_equivalent(cfg_ext_cstic_val cv1,
                         cfg_ext_cstic_val cv2,
                         int detail) {
        return ( cv1.get_charc().equals(cv2.get_charc()) &&
             cv1.get_value().equals(cv2.get_value()) );
    }


        // XML PERSISTENCE

        public void util_load_from_xml_element(Element el)
                                               throws IOException, NumberFormatException {
            // m_inst_id -> is filled by caller
        m_charc     = el.getAttribute(C_XML_CHARC);
        m_charc_txt = el.getAttribute(C_XML_CHARC_TXT);
        m_value     = el.getAttribute(C_XML_VALUE);
        m_value_txt = el.getAttribute(C_XML_VALUE_TXT);
        m_author    = el.getAttribute(C_XML_AUTHOR);
            m_invisible  = ( el.getAttribute(C_XML_INVISIBLE).equals("T") );
        }

        public Element util_to_xml_element(Document doc) {
            Element result = doc.createElement(C_XML_TAG_NAME);
        result.setAttribute(C_XML_CHARC, m_charc);
        if ( m_charc_txt.length() > 0 ) {
         result.setAttribute(C_XML_CHARC_TXT,  m_charc_txt);
        }
        result.setAttribute(C_XML_VALUE, m_value);
        if ( m_value_txt.length() > 0 ) {
         result.setAttribute(C_XML_VALUE_TXT,  m_value_txt);
        }
        result.setAttribute(C_XML_AUTHOR, m_author);
            if ( m_invisible )
                 result.setAttribute(C_XML_INVISIBLE, "T" );
        return result;
        }
}

