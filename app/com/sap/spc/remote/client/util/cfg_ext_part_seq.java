package com.sap.spc.remote.client.util;

import java.util.Set;

/*
   $Workfile: cfg_ext_part_seq.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */

//import com.sap.sxe.sys.read_only_sequence;

/** Table with part_of hierarchy in external format */
public interface cfg_ext_part_seq extends Set/*read_only_sequence*/
{
}

