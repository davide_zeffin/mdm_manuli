package com.sap.spc.remote.client.util;


import java.io.IOException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * An object that can be saved or restored as part of an
 * XML document.
 */

public interface xml_serializable_element {

  /*
   * Initializes this object and fills its content from the XML element.
   */
   public void util_load_from_xml_element(Element el)
                                          throws IOException,
                                                 NumberFormatException;

  /*
   * Creates an XML Element representation for this object.
   * The element is created in the scope of a given XML document.
   */
   public Element util_to_xml_element(Document doc);
}
