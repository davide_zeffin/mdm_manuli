package com.sap.spc.remote.client.util;

/*
   $Workfile: cfg_ext_price_key.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */

import java.io.*;

 /**
  * Pricing condition key in external format.
  * Includes a pricing factor for each key.
  */
public interface cfg_ext_price_key extends Serializable
{
    /** @return the instance id of the instance */
    public Integer get_inst_id();

    /** @return name of the pricing condition key */
    public String get_key();

    /** @return pricing factor */ 
    public double get_factor();
    
    /** @return pricing factor as Char 15*/ 
    public String getFactorAsChar15();    

}

