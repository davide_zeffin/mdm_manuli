package com.sap.spc.remote.client.util;

/*
   $Workfile: ext_configuration.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */

// change log:
// 20010227-ak: added OBJ_TXT attribute for XML
// 20001003-ak: changed sort order of cstic values

//import com.sap.sxe.sys.exc.exc_not_found;
import java.io.Serializable;

import java.io.IOException;
import org.w3c.dom.*;


/**
 * An <b>ext_configuration</b> represents the core result of a configuration
 * session in the SCE.
 * <br>
 * Unlike the <b>configuration</b>, it does <b>not</b> support
 * consistency or completeness checks, validation of changes or
 * search and inference functions.
 * <br>
 * The ext_configuration can be used to copy configuration data or to pass
 * configuration data between the SCE and other applications.
 * It has a simple structure and can be saved to or loaded from
 * a readable file format, transparent database tables or an XML string.
 * <br>
 * In order to change configuration data with full support of SCE
 * functionality, the ext_configuration must be translated back into a
 * <b>configuration</b>. This is possible if the required knowledgebase
 * is accessible. <br>
 *
 * <br>
 *
 * <b><font size=+1>Content</font></b><br>
 *
 * An external configuration contains the name, the overall completeness
 * and consistency status, the names of knowledgebase, version and
 * profile and the number of the root instance.
 * It also holds basic information about instances and their properties:
 *
 * <br>
 *  <ul>
 *    <li> instance number </li>
 *    <li> current type of instance identified by object type,
 *         class type, object key </li>
 *    <li> language dependent name of current type </li>
 *    <li> instance quantity </li>
 *    <li> completeness and consistency of the instance</li>
 *    <li> instance numbers of parts and their BOM position</li>
 *    <li> characteristic value assignments</li>
 *    <li> pricing keys and pricing factors</li>
 *    <li> authoring information</li>
 *  </ul><br>
 *
 * The external configuration does not contain the full state
 * information contained in an active SCE configuration.
 * <br>
 * Examples of omitted data are:
 *
 * <br>
 *  <ul>
 *    <li> full dependencies between facts (justifications)</li>
 *    <li> runtime data for dependency evaluation (pattern instantiations)</li>
 *    <li> intermediate characteristic domain restrictions that were used
 *         to decide the current state of the configuration</li>
 *    <li> full detail of incompletenesses and conflicts</li>
 *  </ul><br>
 *
 * The main reason for omitting these data is to limit the size of
 * configuration data that need to be transferred in the network and
 * that need to be saved in a persistent storage (IBASE).
 * These data can be restored from the knowledgebase if an
 * SCE configuration is loaded from an ext_configuration. This process
 * and its limitations are described in a special section below.
 * <br>
 * An ext_configuration contains no reference to kb, configuration or
 * ddb_inst. <br>
 *
 * <br>
 *
 *
 * <b><font size=+1>Creation and Deletion</font></b><br>
 * An external configuration can be created from a <b>configuration</b>
 * using  <b>configuration.cfg_to_ext_format</b>.
 * The result is not affected by subsequent changes to the source configuration.
 * It can also be built from scratch using <b>api.cfg_ext_new</b>
 * or by loading it from an XML file using <b>api.cfg_ext_read_data</b>.
 * <br>
 * There is no special API for deletion of an ext_configuration. <br>
 *
 * <br>
 *
 * <b><font size=+1>Loading a Configuration from External Format</font></b><br>
 * A <b>configuration</b> can be loaded from an <b>ext_configuration</b> using
 * <b>api.cfg_from_ext_format</b>. This function requires a running kb server
 * to load the specified knowledgebase and to restore data that are not
 * saved in the external format.
 * <br>
 * However, since the knowledgebase can change over time it may be
 * impossible to recreate the original configuration from its external format.
 * <br>
 * As an example, consider a constraint X that calculates a characteristic
 * value V1 in build 10 of the given knowledgebase.
 * At a later point in time, an ext_configuration created with build 10 needs
 * to be changed using a later build or even a higher version of the
 * knowledgebase. Constraint X was changed and no longer calculates V1.
 * Pattern instances and justifications of the original configuration
 * cannot be restored.
 * <br>
 * Another example is that value V1 or even the characteristic no
 * longer exist in the later version of the knowledgebase.
 * <br>
 * The API mentioned above needs to support a <b>tolerant</b> load mechanism
 * to cope with this situation. The SCE uses a customizable load error handler
 * to define the proper system reaction in case of version mismatches.
 * The default error handler has the following features:
 *
 * <br>
 *  <ul>
 *    <li> if kb version is not available, pick same kb with latest version</li>
 *    <li> ignore unknown characteristic values</li>
 *    <li> ignore values for unknown cstics</li>
 *    <li> ignore parts on unknown decomposition items</li>
 *    <li> ignore specializations with unknown types</li>
 *    <li> discard values in the external configuration that
 *         are marked as non-user input but cannot be recalculated</li>
 *    <li> abort after maximum number of N errors</li>
 *  </ul><br>
 * <br>
 *
 * More details about the content of the external configuration can be
 * found in the cfg_ext* classes.
 *
 *
 * @see com.sap.spc.remote.client.util.ext_config_writable
 * @see com.sap.spc.remote.client.util.cfg_ext_inst
 * @see com.sap.spc.remote.client.util.cfg_ext_part
 * @see com.sap.spc.remote.client.util.cfg_ext_cstic_val
 * @see com.sap.spc.remote.client.util.cfg_ext_price_key
 *
 * @author Andreas Kraemer
 */

public interface ext_configuration extends Serializable
{
    /**
     * Returns the client of the external configuration and the knowledge base of the ext config.
     */
    public String get_client();

    /** Returns the name of the configuration. */
    public String get_name();

    /** Returns the SCE version used to create the configuration. */
    public String get_sce_version();

    /** Returns the name of the kb (knowledgebase) with which the configuration was created. */
    public String get_kb_name();

    /** Returns the version of the kb. */
    public String get_kb_version();

    /** Returns build number of the kb. */
    public int get_kb_build();

    /** Returns the name of the kb profile used for this configuration. */
    public String get_kb_profile_name();

    /** @internal  Returns additional info used for this configuration. */
    public String get_cfg_info();

    /**
     * Returns the language used in language dependent names for
     * classes, materials, characteristics and values.
     * @return the single character language identifier or the empty string
     * ("") if no language dependent names are stored in this ext_configuration.
     * @see com.sap.sxe.sys.language#get_id
     * @see cfg_ext_inst#get_obj_txt
     * @see cfg_ext_cstic_val#get_charc_txt
     * @see cfg_ext_cstic_val#get_value_txt
     * */
    public String get_language();

    /** Returns the root instance identifier. */
    public Integer get_root_id();

    /** Returns whether this configuration is complete. */
    public boolean is_complete_p();

    /** Returns whether this configuration is consistent. */
    public boolean is_consistent_p();

    /** Returns all instances that are contained in this configuration.
     *  @see cfg_ext_inst
     */
    public cfg_ext_inst_seq get_insts();

    /** Returns instance info for given inst id.
     *  Result is <tt>null</tt> if inst id does not exist.
     *  @see cfg_ext_inst
     */
    public cfg_ext_inst get_inst(Integer inst_id);

    /** Returns all part-of relationships in this configuration.
     *  @see cfg_ext_part
     */
    public cfg_ext_part_seq get_parts();

    /** Returns part-of relationships for given parent id. <br
     *  Result is ordered by <br>
     *  <par>
     *     pos_nr
     *     obj_type
     *     class_type
     *     obj_key
     *     inst_id
     *  </par>
     *  @exception com.sap.sxe.sys.exc.exc_not_found thrown if
     *   this parent id is not found.
     *  @see cfg_ext_part
     */
    public cfg_ext_part_seq get_parts(Integer parent_id)
                                      throws RuntimeException;

    /** Returns all characteristic value assignments in this configuration.
     *  @see cfg_ext_cstic_val
     */
    public cfg_ext_cstic_val_seq get_cstics_values();

    /** Returns characteristic value assignments for given inst id.
     *  Result is ordered by display position of the characteristic
     *  relative to the current type (oo_class) of the instance.
     *  @see cfg_ext_cstic_val
     */
    public cfg_ext_cstic_val_seq get_cstics_values(Integer inst_id)
                                                   throws RuntimeException;

    /** Returns all price keys in this configuration.
     *  @see cfg_ext_price_key
     */
    public cfg_ext_price_key_seq get_price_keys();

    /** Returns price keys for given inst id.
     *  @exception com.sap.sxe.sys.exc.exc_not_found thrown if
     *  this inst id is not found.
     *  @see cfg_ext_price_key
     */
    public cfg_ext_price_key_seq get_price_keys(Integer inst_id)
                                                throws RuntimeException;

    /**
     *  Returns the ext_configuration as an XML element.
     *  The element has a tree structure with
     *  a configuration top node, a special root instance node
     *  and a flat list of parts.
     *
     * <pre>
     * (CONFIGURATION
     *      SCEVERSION  = (sce_version)
     *      NAME        = (name)
     *      KBNAME      = (kbname)                                 (*)
     *      KBVERSION   = (kbversion)                              (*)
     *      KBBUILD     = (build)               (integer)
     *      KBPROFILE   = (kbprofile)                              (*)
     *      CFGINFO     = (cfginfo)
     *      LANGUAGE    = (language)
     *      ROOT_NR     = (nr of root instance) (integer)          (*)
     *      CONSISTENT  = (consistent) in {"T", "F"}
     *      COMPLETE    = (complete) in {"T", "F"})
     *
     *   (INST             // root instance element
     *      NR          = (nr of instance)      (integer)          (*)
     *      OBJ_TYPE    = (object type)                            (*)
     *      CLASS_TYPE  = (class type)                             (*)
     *      OBJ_KEY     = (object key) (e.g. matnr)                (*)
         *      OBJ_TXT     = (language dep object text)
     *      AUTHOR      = (author of current type)                 (*)                                                    defaults to USER)
     *      QTY         = (instance quantity)                      (*)
     *      UNIT        = (unit of instance quantity)              (*)
     *      CONSISTENT  = (consistent) in {"T", "F"}
     *      COMPLETE    = (complete) in {"T", "F"})
     *
     *   (CSTICS)         // characteristic values
     *    (CSTIC)
     *       CHARC      = (neutral key)                            (*)
     *       CHARC_TXT  = (language dep cstic text)
     *       VALUE      = (cstic value)                            (*)
     *       VALUE_TXT  = (language dep value text)
     *       AUTHOR     = (author)                                 (*)
         *       INVISIBLE  = (invisible flag)
     *    ...
     *   (/CSTICS)
     *
     *   (PKEYS)          // pricing condition keys
     *    (PKEY)
     *       KEY        = (condition key)                          (*)
     *       FACTOR     = (multiplier) /)                     (default: 1)
     *    ...
     *   (/PKEYS)
     *
     *  (/INST)
     *
     *  (PARTS)           // part instances in a flat list
     *   (PART
     *      PARENT_NR   = (nr of parent instance)   (integer)      (*)
     *      POS_NR      = (position number in BOM)                 (*)
     *      OBJ_TYPE    = (object type of BOM item)                (*)
     *      CLASS_TYPE  = (class type of BOM item)                 (*)
     *      OBJ_KEY     = (object key of BOM item)                 (*)
     *      AUTHOR      = (author)  (SPACE (" ") for user input)   (*)
     *      S           = (sales relevant) in {"T", "F"} )
     *    (INST ..) .. (/INST)
     *   (/PART)
     *   ...
     *  (/PARTS)
     *
     * (/CONFIGURATION)
     * </pre>
     *
     * Attributes marked with an asterisk (*) are mandatory for
     * loading a configuration from XML format.
         * A formal DTD specification is given in file SceConfig.dtd.
     *
     * @param  XML document container for result element.
     * @return an XML element
     * @exception IOException thrown if element could not be
     *  created.
     * @see org.w3c.dom.Document
     * @see org.w3c.dom.Element
     */
    public Element cfg_ext_to_xml_element(Document doc)
                          throws IOException;

}

