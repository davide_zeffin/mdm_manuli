package com.sap.spc.remote.client.util;

import java.util.Set;

/*
   $Workfile: cfg_ext_price_key_seq.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 18.08.98 14:04 $
 */

//import com.sap.sxe.sys.read_only_sequence;

/** Table with price keys in external format */
public interface cfg_ext_price_key_seq extends Set /*read_only_sequence*/
{
}

