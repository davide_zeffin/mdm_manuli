package com.sap.spc.remote.client.util;

/*
   $Workfile: cfg_ext_cstic_dom_element.java $
   $Revision: #1 $
   $Author: d034517 $
   $Date: 2004/09/17 $
 */


 /**
  * Characteristic domain restriction element in external format.
  */
public interface cfg_ext_cstic_dom_element extends cfg_ext_cstic_val
{
    /** @return upper bound (if this element is an interval) */
    public String get_value_to();

        // note: get_value_txt returns the full interval string if
        // this element is an interval

    /** @return the type of this domain element
         *  This information is relevant for numeric elements.
         *
         *  1 = single value (EQ)
         *  2 = half open interval GE_LT [)
         *  3 = closed interval GE_LE []
         *  4 = open interval GT_LT ()
         *  5 = half open interval GT_LE (]
         *  6 = infinite interval LT (-inf, )
         *  7 = infinite interval LT (-inf, ]
         *  8 = infinite interval GT ( , +inf)
         *  9 = infinite interval GE [ , +inf)
         *
         * @see com.sap.sxe.util.xnumeric_interval#util_get_interval_type
         */
    public int get_valcode();
}

