package com.sap.spc.remote.client.util;

/*
   $Workfile: language.java $
   $Revision: 1 $
   $Author: Fm $
   $Date: 17.08.98 10:45 $
 */

// change log:
// 20021008-ak: added accessors for Locale and SAPLocale (country = "")

//import com.sap.sxe.sys.seq.hashtable;
//import com.sap.sxe.sys.exc.exc_access_error;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;

//
//
// language
//
//
public class language implements Serializable{

    private static Hashtable c_langs_by_id  = new Hashtable();
    private static Hashtable c_langs_by_id2 = new Hashtable();


    public final static language
        C_CHINESE   = new language("1", "ZH"),
        C_THAI      = new language("2", "TH"),
        C_KOREAN    = new language("3", "KO"),
        C_ROMANIAN  = new language("4", "RO"),
        C_SLOVENE   = new language("5", "SL"),
        C_CROATIAN  = new language("6", "HR"),
        C_MALAY     = new language("7", "MS"),
        C_UKRAINIAN = new language("8", "UK"),
        C_ESTNIAN   = new language("9", "ET"),
        C_ARABIC    = new language("A", "AR"),
        C_HEBREW    = new language("B", "HE"),
        C_CZECH     = new language("C", "CS"),
        C_GERMAN    = new language("D", "DE"),
        C_ENGLISH   = new language("E", "EN"),
        C_FRENCH    = new language("F", "FR"),
        C_GREEK     = new language("G", "EL"),
        C_HUNGARIAN = new language("H", "HU"),
        C_ITALIAN   = new language("I", "IT"),
        C_JAPANESE  = new language("J", "JA"),
        C_DANISH    = new language("K", "DA"),
        C_POLISH    = new language("L", "PL"),
        C_CHINESE_TRAD = new language("M", "ZF"),
        C_DUTCH     = new language("N", "NL"),
        C_NORWEGIAN = new language("O", "NO"),
        C_PORTUGUESE= new language("P", "PT"),
        C_SLOVAKIAN = new language("Q", "SK"),
        C_RUSSIAN   = new language("R", "RU"),
        C_SPANISH   = new language("S", "ES"),
        C_TURKISH   = new language("T", "TR"),
        C_FINNISH   = new language("U", "FI"),
        C_SWEDISH   = new language("V", "SV"),
        C_BULGARIAN = new language("W", "BG"),
        C_LITHUANIAN= new language("X", "LI"),
        C_LETTIAN   = new language("Y", "LE"),
    C_SERBIAN   = new language("0", "SR"),
    C_AFRIKAANS = new language("a", "AF"),
    C_ISLANDIC  = new language("b", "IS"),
    C_CATALAN   = new language("c", "CA"),
    C_SERBO_CROATIAN = new language("d", "SH"),
    C_INDONESIAN = new language("i", "ID"),

        C_CUSTOM    = new language("Z", "Z1");


    public static language find_language_for_id(String id)
    {
        language result = (language)c_langs_by_id.get(id);
        if ( result == null )
            throw new RuntimeException("unknown language id " + id);
        return result;
    }


    public static language find_language_for_id2(String id)
    {
        language result = (language)c_langs_by_id2.get(id);
        if ( result == null )
            throw new RuntimeException("unknown language id2 " + id);
        return result;
    }

    private String m_lang_id;
    private String m_lang_id2;
    private SAPLocale m_sap_locale;


    public language(String lang_id, String lang_id2) {
        m_lang_id  = lang_id;
        m_lang_id2 = lang_id2;
        m_sap_locale = new SAPLocale(m_lang_id2, "");
        c_langs_by_id.put(lang_id, this);
        c_langs_by_id2.put(lang_id2, this);
    }

    public String get_id() {
        return m_lang_id;
    }

    public String get_id2() {
        return m_lang_id2;
    }

    public SAPLocale get_sap_locale() {
            return m_sap_locale;
    }

    public  Locale get_locale() {
            return m_sap_locale.getLocale();
    }
    
    public String toString() {
            return "#[language: " + m_lang_id2 + " (" + m_lang_id + ")]";
    }

    public boolean equals(Object rhs) {
        if(rhs != null && rhs instanceof language) {
            return
                m_lang_id2.equals(((language)rhs).m_lang_id2);
        }
        else
            return false;
    }
    
    public int hashCode() {
       return m_lang_id2.hashCode();
    }

    public static Enumeration all_languages() {
        return c_langs_by_id2.elements();
    }
}
