/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client;

import java.util.ArrayList;

import com.sap.msasrv.socket.shared.ErrorCodes;
import com.sap.msasrv.socket.shared.ErrorConstants;

/**
 * An exception thrown by a server return code other than RET_OK.  <p>
 * This exception is thrown by the servlet when a server command returns
 * a return code signaling that an error has occured. The exception
 * knows the server's code and its message string.
 */
public class ClientException extends Exception implements ErrorCodes
{
	protected String _code;
	protected String _message;
	protected int	   _number;
	protected String key;
	protected ArrayList placeHolders;
	protected Exception orgEx;

	public ClientException(String code, String message) {
		this(code,message,ErrorConstants.UNKNOWN_ERROR);
	}

	public ClientException(String code, String message, int number) {
		super("Server error: "+code+" ("+message+")");
		_code = code;
		_message = message;
		_number = number;
	}

	/*
	* This constructor takes key and original exception as parameter.
	*/
	public ClientException(String code, String key, ArrayList placeHolders, Exception ex){
		this._code = code;
		this.key = key;
		this.orgEx = ex;
		this.placeHolders = placeHolders;
	}

	/**
	 * Returns the server's error code.<p>
	 *
	 * @see com.sap.msasrv.socket.shared.ErrorCodes
	 */
	public String getCode() {
		return _code;
	}

	public String getKey(){
		return key;
	}


	public ArrayList getPlaceHolders(){
		return placeHolders;
	}

	/**
	 * Returns the Server's error number.
	 * @see	com.sap.msasrv.socket.ErrorConstants
	 */
	public int getErrorNumber() {
		return _number;
	}


	/**
	 * Returns the server's message and code.<p>
	 *
	 * In error situations the server will return a response with a parameter
	 * SPC_ERROR_MESSAGE. The value of this parameter is retrieved with this method.
	 *
	 * @see com.sap.msasrv.socket.shared.ServerConstants
	 */
	public String getMessage() {
        String message = "";
        if (_message==null) {
            if (orgEx == null){
                if (_code!=null) {
                    message = "Exception: " + _code;
                }
            }
            else {
                message = "Original Exception Message: " + orgEx.getMessage();
            }
        }
        else {
            message = _message;
        }
		return message;
        
	}

	/**
	* Returns the original exception
	*/
	public Exception getOriginalException(){
		return this.orgEx;
	}

	/**
	 * Returns true if this Exception is fatal, ie if the client should
	 * terminate its session.<p>
	 *
	 * Fatal errors are those explicitely marked
	 * fatal by the server as well as references to unknown or "timeouted"
	 * session ids.<p>
	 *
	 * Recoverable errors are those marked "error" by the server and references
	 * to unknown server commands.<p>
	 */
	public boolean isRecoverable() {
		if (_code.equals(RET_INTERNAL_ERROR))
			return true;
		else if (_code.equals(RET_INTERNAL_FATAL))
			return false;
		else if (_code.equals(RET_OK))
			return true;
		else if (_code.equals(RET_TIMEDOUT_ID))
			return false;
		else if (_code.equals(RET_UNKNOWN_CMD))
			return true;
		else if (_code.equals(RET_UNKNOWN_ID))
			return false;
		else
			// default: not recoverable
			return false;
	}
}
