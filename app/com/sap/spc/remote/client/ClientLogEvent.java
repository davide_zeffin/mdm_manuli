package com.sap.spc.remote.client;

import java.util.EventObject;


/**
 * A client log event holds information about a single "logworthy" entity.
 * Please note that there are no log events for initialization progress
 * and errors, simply because the client is already initialized when you
 * are able to register a listener to it.
 */
public class ClientLogEvent extends EventObject {

	public static final int TYPE_ACTIVATED = 1;
	public static final int TYPE_REQUEST_SENT = 2;
	public static final int TYPE_RESPONSE_RECEIVED = 3;
	public static final int TYPE_CLOSED = 4;
	public static final int TYPE_EXCEPTION = 5;

	protected long      timeStamp;
	protected int       type;
	protected String    message;
	protected Throwable exception;

    public ClientLogEvent(Object obj, int type, String message, Exception exception) {
		super(obj);
		this.type = type;
		this.message = message;
		this.exception = exception;
		timeStamp = System.currentTimeMillis();
    }

	/**
	 * Returns the type of this event, one of the constants in this class starting with TYPE_.
	 */
	public int getType() {
		return type;
	}

	/**
	 * Returns the message of this event. May be null.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Returns the timestamp at which this event was created.
	 */
	public long getTime() {
		return timeStamp;
	}

	public Throwable getException() {
		return exception;
	}
}