/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAPMarkets does not make
	any warranty about the software, its performance or its conformity to
	any specification.

**************************************************************************/

package com.sap.spc.remote.client;

/**
 * An object that knows how to handle exceptions. Typically it logs them or something
 * like this.
 */
public interface ExceptionHandler {

	/**
	 * Deal with a given exception.
	 */
	public void handleException(Object source, String message, Exception exception);
}