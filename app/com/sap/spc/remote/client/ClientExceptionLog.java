/************************************************************************

	Copyright (c) 2002 by SAP AG, SAP Markets Europe GmbH

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

public class ClientExceptionLog implements IClientExceptionHandling {
	protected static IsaLocation log = IsaLocation.getInstance(ClientExceptionLog.class.getName());
	protected static Throwable lastException = null;

	protected static ClientExceptionLog instance = null;

	public static IClientExceptionHandling getInstance() {
		// trivial constructor => no need to synchronize - in the worst case we get more than one singleton,
		// but that doesn't matter.
		if (instance == null)
		    instance = new ClientExceptionLog();
		return instance;
	}

	protected ClientExceptionLog() {}

	public void exception(Location location, int severity, Throwable e) {
		exception(location, null, severity, e, null);
	}
	public void exception(Category category, int severity, Throwable e) {
		exception(null, category, severity, e, null);
	}
	public void exception(Location location, int severity, Throwable e, String contextMessage) {
		exception(location, null, severity, e, contextMessage);
	}
	public void exception(Category category, int severity, Throwable e, String contextMessage) {
		exception(null, category, severity, e, contextMessage);
	}

	protected void exception(Location location, Category category, int severity, Throwable e, String contextMessage) {
		try {
			StringWriter wr = new StringWriter(); // StringWriter(size) can't be used for mini-spe (protected access in jdk118)
			PrintWriter pwr = new PrintWriter(wr);
			e.printStackTrace(pwr);
			String context = contextMessage == null ? "" : "  "+contextMessage;
			if (location != null) {
				location.logT(severity, category, "Exception "+context+": "+e.getMessage()+"\n"+wr.getBuffer().toString());
			}
			else {
				category.logT(severity, com.sap.spc.remote.client.ResourceAccessor.getLocation(ClientExceptionLog.class), "Exception "+context+": "+e.getMessage()+"\n"+wr.getBuffer().toString());
			}
		}
		catch(Exception f) {log.debug(f.getMessage(),f);}
	}


	/**
	 * Remembers this exception if it is the first one. Later exceptions are not remembered because
	 * they are likely to be consequences of the first one. Only getFirstExceptionAndReset controls
	 * when a new "remembering interval" starts.
	 */
	public static void rememberException(Throwable t) {
		synchronized(ClientExceptionLog.class) {
			if (lastException == null) {
				lastException = t;
			}
		}
	}


	/**
	 * Returns the first exception that was remembered since the last call of getFirstExceptionAndReset,
	 * or null, if none was remembered.
	 */
	public static Throwable getFirstExceptionAndReset() {
    	synchronized(ClientExceptionLog.class) {
    		Throwable t = lastException;
    		lastException = null;
    		return t;
    	}
    }
}
