/*****************************************************************************
	Class         PrepareDBMigrationAction
	Copyright (c) 2004, SAP AG, All rights reserved.
	Description:  Check all Prerequesites and prepare Database Migration
	              to migrate from a J2EE 6.20 database to the WEB AS 6.30
	              database
	Author:       SAP AG
	Created:      28. Jan 2004
	Version:      1.0
*****************************************************************************/
package com.sap.isacore.action.dbmig;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.db.dbmig.DBMigration;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Check all Prerequesites and prepare Database Migration
 * to migrate from a J2EE 6.20 database to the WEB AS 6.30
 * database
 * @author SAP AG
 * @version 1
 */

public class PrepareDBMigrationAction extends Action {
	
	/**
	 * Request context parameter that is set to true, if the connection
	 * to the target database is not available
	 */
	public static final String RC_TARGETDB_CONN_ERR = "targetdbconnerr";
	
	/**
	 * Request context parameter that is set to true, if the connection
	 * to the source database is not available
	 */
	public static final String RC_SOURCEDB_CONN_ERR = "sourcedbconnerr";
	
	/**
	 * Request context parameter that is set to true, if the objectid
	 * table in the target database is not empty
	 */
	public static final String RC_OBJECTID_NOT_EMPTY = "objectidnotempty";
	
	/**
	 * Request context parameter that holds the HashMap with the clients
	 * and their related systemIds
	 */
	public static final String RC_CLIENT_SYSTEMID = "clientsystemid";
	
	/**
	 * Request context parameter that is set, in case the HashMap with the 
	 * clients and their related systemIds contains no entries
	 */
	public static final String RC_CLIENT_SYSTEMID_EMPTY = "clientsystemidempty";
	
	/**
	 * Request context parameter tto store filename for errorneous logfile
	 */
	public static final String RC_ERRORNEOUS_LOG = "errorneouslog";
	
	/**
	 * Request context parameter tto store filename for errorneous logfile
	 */
	public static final String RC_DUPLIC_LOG = "dupliclog";
	
	protected static IsaLocation log =
							   IsaLocation.getInstance(PrepareDBMigrationAction.class.getName());

	/**
	 *  Process the specified HTTP request, and create the corresponding HTTP
	 *  response (or forward to another web component that will create it).
	 *  Return an <code>ActionForward</code> instance describing where and how
	 *  control should be forwarded, or <code>null</code> if the response has
	 *  already been completed.
	 *
	 *@param  mapping               The ActionMapping used to select this instance
	 *@param  request               The HTTP request we are processing
	 *@param  response              The HTTP response we are creating
	 *@param  form                  Description of Parameter
	 *@return                       Description of the Returned Value
	 *@exception  IOException       if an input/output error occurs
	 *@exception  ServletException  if a servlet exception occurs
	 */         	

		// Page that should be displayed next.
	public ActionForward  perform(
								ActionMapping mapping,
								ActionForm form,
								HttpServletRequest request,
								HttpServletResponse response)
								throws IOException, ServletException {
						
		String forwardTo = "error";
		
//		 check if the user has the rights to do this action.
		if(!AdminConfig.isXCMAdmin(request)){
		    if (log.isDebugEnabled()) {
		    	log.debug("The user has read only rights.");
		    }
		    return mapping.findForward(forwardTo);
		}
        DBMigration dbmig = new DBMigration();
        HashMap clientSystemIds = null;
        
        int retValCon = dbmig.checkDBConnections();
        
        if (retValCon == DBMigration.BOTH_DB_CONN_UNAV || retValCon == DBMigration.TARGET_DB_CONN_UNAV) {
		    request.setAttribute(RC_TARGETDB_CONN_ERR, "true");
		    if (log.isDebugEnabled()) {
		    	log.debug("Target DB Connection error");
		    }
        }
		if (retValCon == DBMigration.BOTH_DB_CONN_UNAV || retValCon == DBMigration.SOUCRE_DB_CONN_UNAV) {
			request.setAttribute(RC_SOURCEDB_CONN_ERR, "true");
			if (log.isDebugEnabled()) {
				log.debug("Source DB Connection error");
			}
		}
        
        boolean isObjectIdEmpty = false;
        
        if (retValCon != DBMigration.TARGET_DB_CONN_UNAV && retValCon != DBMigration.BOTH_DB_CONN_UNAV) {
			isObjectIdEmpty = dbmig.checkIfObjectIdIsEmpty();
        	if (!isObjectIdEmpty) {
        		request.setAttribute(RC_OBJECTID_NOT_EMPTY, "true");
				if (log.isDebugEnabled()) {
					log.debug("ObjectId table not empty");
				}
        	}
        }
		if (retValCon == DBMigration.DB_CONN_OK && isObjectIdEmpty) {
			dbmig.determineClientIds();
			clientSystemIds = dbmig.getSystemIdsForClients();
			if (clientSystemIds != null && !clientSystemIds.isEmpty()) {
				request.setAttribute(RC_CLIENT_SYSTEMID, clientSystemIds.entrySet());
				forwardTo = "showrelation";
			}
			else {
				request.setAttribute(RC_CLIENT_SYSTEMID_EMPTY, "true");
				if (log.isDebugEnabled()) {
					log.debug("ClientId list is empty");
				}
			}
            
            HttpSession session = request.getSession();
            UserSessionData userSession = UserSessionData.getUserSessionData(session); 
            if (userSession == null) {
                userSession = UserSessionData.createUserSessionData(session);
            }
            
            if (userSession == null) {
                log.error("Could not create UserSessionData. Stop Migration");
                forwardTo = "error";
            }
		}
		
		request.setAttribute(RC_ERRORNEOUS_LOG, "C:\\errorneousrows.log");
		request.setAttribute(RC_DUPLIC_LOG, "C:\\duplicatekeyrows.log");

		return mapping.findForward(forwardTo);
	}

}
