/*****************************************************************************
	Class         StartDBMigrationAction
	Copyright (c) 2004, SAP AG, All rights reserved.
	Description:  Parse all the user entries and start the Migration
	              if everything is o.k.
	Author:       SAP AG
	Created:      28. Jan 2004
	Version:      1.0
*****************************************************************************/
package com.sap.isacore.action.dbmig;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.db.dbmig.DBMigration;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Check all Prerequesites and prepare Database Migration
 * to migrate from a J2EE 6.20 database to the WEB AS 6.30
 * database
 * @author SAP AG
 * @version 1
 */

public class StartDBMigrationAction extends BaseAction {
	
	/**
	 * Request context parameter that is set to true, if not for all clients 
	 * a system was specified;
	 */
	public static final String RC_SYSTEM_MISSING = "systemmissing";
	
	/**
	 * Request context parameter that is set to true, if something is wrong
	 * with the log file for the errorneous rows
	 */
	public static final String RC_ERROR_LOG_ERR = "errorlogerr";
	
	/**
	 * Request context parameter that is set to true, if if something is wrong
	 * with the log file for the duplicate key rows
	 */
	public static final String RC_DUPLIC_LOG_ERR = "dupliclogerr";
	
	/**
	 * Session context parameter that holds the migration object
	 */
	public static final String SC_DBMIG = "dbmig";

	protected static IsaLocation log =
							   IsaLocation.getInstance(StartDBMigrationAction.class.getName());


	/**
	 *  Process the specified HTTP request, and create the corresponding HTTP
	 *  response (or forward to another web component that will create it).
	 *  Return an <code>ActionForward</code> instance describing where and how
	 *  control should be forwarded, or <code>null</code> if the response has
	 *  already been completed.
	 *
	 *@param  mapping               The ActionMapping used to select this instance
	 *@param  request               The HTTP request we are processing
	 *@param  response              The HTTP response we are creating
	 *@param  form                  Description of Parameter
	 *@return                       Description of the Returned Value
	 *@exception  IOException       if an input/output error occurs
	 *@exception  ServletException  if a servlet exception occurs
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {

		RequestParser parser = new RequestParser(request);
		
        //Page that should be displayed next.
		String forwardTo = "error";
		
		RequestParser.Parameter clients =
			            parser.getParameter("client[]");
		RequestParser.Parameter systemIds =
						parser.getParameter("systemid[]");
		RequestParser.Parameter errlog =
						parser.getParameter("errorneouslog");
		RequestParser.Parameter duplog =
						parser.getParameter("duplicatekeyrow");
						

		String client;
		String systemId;
		boolean allSystemIdsSpecified = true;
		boolean errLogOk = false;
		boolean duplicLogOk = false;
		DBMigration dbmig = new DBMigration();
		HashMap clientSystemIds = dbmig.getSystemIdsForClients();
		
		clientSystemIds.clear();
						
		for (int i = 1; i <= clients.getNumValues(); i++) {
			
			client = clients.getValue(i).getString();
            if (client.length() > 0) {
				systemId = systemIds.getValue(i).getString();
				if (systemId.trim().length() == 0) {
					allSystemIdsSpecified = false;
					request.setAttribute(RC_SYSTEM_MISSING, "true");
					systemId = "";
				}
				clientSystemIds.put(client, systemId);
            }
		}
		
		if(log.isDebugEnabled()) {
			log.debug("Were all systemIds specified: " + allSystemIdsSpecified);
		}
		
	    errLogOk = (errlog.isSet() && errlog.getValue().getString().length() > 0 && 
	                dbmig.openErrorneousLog(errlog.getValue().getString()));

		if (!errLogOk) {			
			request.setAttribute(RC_ERROR_LOG_ERR, "true");
	    }
	    
		if(log.isDebugEnabled()) {
			log.debug("Errorenous row log ok: " + errLogOk);
		}
		
		duplicLogOk = (duplog.isSet() && duplog.getValue().getString().length() > 0 &&
		               dbmig.openDuplicateLog(duplog.getValue().getString()));

		if (!duplicLogOk) {			
			request.setAttribute(RC_DUPLIC_LOG_ERR, "true");
		}
		
		if(log.isDebugEnabled()) {
			log.debug("Duplicat row log ok: " + duplicLogOk);
		}
		
		if (allSystemIdsSpecified && duplicLogOk && errLogOk) {

			HttpSession session = request.getSession();
			UserSessionData userSession = UserSessionData.getUserSessionData(session); 
			
			if (userSession == null) {
				log.error("Could not create UserSessionData. Stop Migration");
			}
			else {
				if(log.isDebugEnabled()) {
				   log.debug("Everything ok. Start migration");
				}
				forwardTo = "startmig";
				dbmig.setBasketClients(clientSystemIds);
				userSession.setAttribute(SC_DBMIG, dbmig);
			}
		}
		else {
			request.setAttribute(PrepareDBMigrationAction.RC_CLIENT_SYSTEMID, dbmig.getSystemIdsForClients().entrySet());
			request.setAttribute(PrepareDBMigrationAction.RC_ERRORNEOUS_LOG, errlog.getValue().getString());
			request.setAttribute(PrepareDBMigrationAction.RC_DUPLIC_LOG, duplog.getValue().getString());
			dbmig.closeErrorneousLog();
			dbmig.closeRejectedLog();
		}
		 	
		return mapping.findForward(forwardTo);
	}

}
