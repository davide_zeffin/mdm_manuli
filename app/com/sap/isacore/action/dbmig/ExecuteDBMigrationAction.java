/*****************************************************************************
	Class         ExecuteDBMigrationResultsAction
	Copyright (c) 2004, SAP AG, All rights reserved.
	Description:  Show the current status of the migration process
	              
	Author:       SAP AG
	Created:      06. Feb 2004
	Version:      1.0
*****************************************************************************/
package com.sap.isacore.action.dbmig;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.db.dbmig.DBMigration;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;


public class ExecuteDBMigrationAction extends BaseAction {
	
	protected static IsaLocation log =
								   IsaLocation.getInstance(ShowDBMigrationResultsAction.class.getName());

	/**
	 *  Process the specified HTTP request, and create the corresponding HTTP
	 *  response (or forward to another web component that will create it).
	 *  Return an <code>ActionForward</code> instance describing where and how
	 *  control should be forwarded, or <code>null</code> if the response has
	 *  already been completed.
	 *
	 *@param  mapping               The ActionMapping used to select this instance
	 *@param  request               The HTTP request we are processing
	 *@param  response              The HTTP response we are creating
	 *@param  form                  Description of Parameter
	 *@return                       Description of the Returned Value
	 *@exception  IOException       if an input/output error occurs
	 *@exception  ServletException  if a servlet exception occurs
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
			
		RequestParser parser = new RequestParser(request);
		
		//Page that should be displayed next.
		String forwardTo = "finished";

		HttpSession session = request.getSession();
		UserSessionData userSession = UserSessionData.getUserSessionData(session); 
		
		if (userSession == null) {
			log.error("Could not retrieve UserSessionData.  No information about the Migration process");
			forwardTo = "error";
		}
		else {
			DBMigration dbmig = (DBMigration) userSession.getAttribute(StartDBMigrationAction.SC_DBMIG);
			if (dbmig == null) {
				log.error("Could not retrieve DBMigration object. No information about the Migration process");
				forwardTo = "error";
			}
			else {
				boolean migOk = dbmig.migrate();
				if (!migOk) {
					forwardTo = "error";
				}
			}
		}
		
		return mapping.findForward(forwardTo);
	}

}
