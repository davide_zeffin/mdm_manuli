/*****************************************************************************
	Class         ShowDBMigrationResultsAction
	Copyright (c) 2004, SAP AG, All rights reserved.
	Description:  Show the current status of the migration process
	              
	Author:       SAP AG
	Created:      02. Feb 2004
	Version:      1.0
*****************************************************************************/
package com.sap.isacore.action.dbmig;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.db.dbmig.DBMigration;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;

/**
 * Shows the current status of the migration process
 * 
 * @author SAP AG
 * @version 1.0
 */

public class ShowDBMigrationResultsAction extends BaseAction {
	
	/**
	 * Request context parameter containing the number of migrated
	 * Basket rows
	 */
	public static final String RC_MIG_BSKT_ROWS = "migbsktrows";
	
	/**
	 * Request context parameter containing the number of migrated
	 * ObjectId rows
	 */
	public static final String RC_MIG_OBJID_ROWS = "migobjidrows";
	
	/**
	 * Request context parameter containing the number of migrated
	 * item rows
	 */
	public static final String RC_MIG_ITEM_ROWS = "migitemdrows";

	/**
	 * Request context parameter containing the number of migrated
	 * Business Partner rows
	 */
	public static final String RC_MIG_BUPA_ROWS = "migbuparows";

	/**
	 * Request context parameter containing the number of migrated
	 * ExtConfig rows
	 */
	public static final String RC_MIG_EXTCONF_ROWS = "migextconfrows";
	
	/**
	 * Request context parameter containing the number of migrated
	 * Address rows
	 */
	public static final String RC_MIG_ADDRESS_ROWS = "migaddrrows";	

	/**
	 * Request context parameter containing the number of migrated
	 * ShipTo rows
	 */
	public static final String RC_MIG_SHIPTO_ROWS = "migshiptorows";	
	
	/**
	 * Request context parameter containing the number of migrated
	 * ExtDataHeader rows
	 */
	public static final String RC_MIG_EXTDATHEAD_ROWS = "migextdatheadrows";	
	
	/**
	 * Request context parameter containing the number of migrated
	 * ExtDataHeader rows
	 */
	public static final String RC_MIG_EXTDATITEM_ROWS = "migextdatitemrows";	
	
	/**
	 * Request context parameter containing the number of migrated
	 * Text rows
	 */
	public static final String RC_MIG_TEXT_ROWS = "migtextrows";
	
	/**
	 * Request context parameter containing the number of rejected
	 * Basket rows
	 */
	public static final String RC_REJ_BSKT_ROWS = "rejbsktrows";
	
	/**
	 * Request context parameter containing the number of rejected
	 * ObjectId rows
	 */
	public static final String RC_REJ_OBJID_ROWS = "rejobjidrows";
	
	/**
	 * Request context parameter containing the number of rejected
	 * item rows
	 */
	public static final String RC_REJ_ITEM_ROWS = "rejitemdrows";

	/**
	 * Request context parameter containing the number of rejected
	 * Business Partner rows
	 */
	public static final String RC_REJ_BUPA_ROWS = "rejbuparows";

	/**
	 * Request context parameter containing the number of rejected
	 * ExtConfig rows
	 */
	public static final String RC_REJ_EXTCONF_ROWS = "rejextconfrows";
	
	/**
	 * Request context parameter containing the number of rejected
	 * Address rows
	 */
	public static final String RC_REJ_ADDRESS_ROWS = "rejaddrrows";	

	/**
	 * Request context parameter containing the number of rejected
	 * ShipTo rows
	 */
	public static final String RC_REJ_SHIPTO_ROWS = "rejshiptorows";	
	
	/**
	 * Request context parameter containing the number of rejected
	 * ExtDataHeader rows
	 */
	public static final String RC_REJ_EXTDATHEAD_ROWS = "rejextdatheadrows";	
	
	/**
	 * Request context parameter containing the number of rejected
	 * ExtDataHeader rows
	 */
	public static final String RC_REJ_EXTDATITEM_ROWS = "rejextdatitemrows";	
	
	/**
	 * Request context parameter containing the number of rejected
	 * Text rows
	 */
	public static final String RC_REJ_TEXT_ROWS = "rejtextrows";
	
	/**
	 * Request context parameter containing the number of errorneous
	 * Basket rows
	 */
	public static final String RC_ERR_BSKT_ROWS = "errbsktrows";
	
	/**
	 * Request context parameter containing the number of errorneous
	 * ObjectId rows
	 */
	public static final String RC_ERR_OBJID_ROWS = "errobjidrows";
	
	/**
	 * Request context parameter containing the number of errorneous
	 * item rows
	 */
	public static final String RC_ERR_ITEM_ROWS = "erritemdrows";

	/**
	 * Request context parameter containing the number of errorneous
	 * Business Partner rows
	 */
	public static final String RC_ERR_BUPA_ROWS = "errbuparows";

	/**
	 * Request context parameter containing the number of errorneous
	 * ExtConfig rows
	 */
	public static final String RC_ERR_EXTCONF_ROWS = "errextconfrows";
	
	/**
	 * Request context parameter containing the number of errorneous
	 * Address rows
	 */
	public static final String RC_ERR_ADDRESS_ROWS = "erraddrrows";	

	/**
	 * Request context parameter containing the number of errorneous
	 * ShipTo rows
	 */
	public static final String RC_ERR_SHIPTO_ROWS = "errshiptorows";	
	
	/**
	 * Request context parameter containing the number of errorneous
	 * ExtDataHeader rows
	 */
	public static final String RC_ERR_EXTDATHEAD_ROWS = "errextdatheadrows";	
	
	/**
	 * Request context parameter containing the number of errorneous
	 * ExtDataHeader rows
	 */
	public static final String RC_ERR_EXTDATITEM_ROWS = "errextdatitemrows";	
	
	/**
	 * Request context parameter containing the number of errorneous
	 * Text rows
	 */
	public static final String RC_ERR_TEXT_ROWS = "errtextrows";
	
	/**
	 * Request context that is set to true, if the Migration process
	 * is finished
	 */
	public static final String RC_MIRATION_FINISHED = "migfinished";
	
	/**
	 * Request context that holds the current duration of the Migration process
	 */
	public static final String RC_MIRATION_DURATION = "migduration";
	
	protected static IsaLocation log =
								   IsaLocation.getInstance(ShowDBMigrationResultsAction.class.getName());

	/**
	 *  Process the specified HTTP request, and create the corresponding HTTP
	 *  response (or forward to another web component that will create it).
	 *  Return an <code>ActionForward</code> instance describing where and how
	 *  control should be forwarded, or <code>null</code> if the response has
	 *  already been completed.
	 *
	 *@param  mapping               The ActionMapping used to select this instance
	 *@param  request               The HTTP request we are processing
	 *@param  response              The HTTP response we are creating
	 *@param  form                  Description of Parameter
	 *@return                       Description of the Returned Value
	 *@exception  IOException       if an input/output error occurs
	 *@exception  ServletException  if a servlet exception occurs
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
			
		RequestParser parser = new RequestParser(request);
		
        //Page that should be displayed next.
		String forwardTo = "showresults";

		HttpSession session = request.getSession();
		UserSessionData userSession = UserSessionData.getUserSessionData(session); 
		
		if (userSession == null) {
			log.error("Could not retrieve UserSessionData.  No information about the Migration process");
			forwardTo = "error";
		}
		else {
			DBMigration dbmig = (DBMigration) userSession.getAttribute(StartDBMigrationAction.SC_DBMIG);
			if (dbmig == null) {
				log.error("Could not retrieve DBMigration object. No information about the Migration process");
				forwardTo = "error";
			}
			
			// retrieve info
			request.setAttribute(RC_MIG_ADDRESS_ROWS, String.valueOf(dbmig.getProcessedAddressRows() - dbmig.getRejectedAddressRows() - dbmig.getErrorneousAddressRows()));
			request.setAttribute(RC_ERR_ADDRESS_ROWS, String.valueOf(dbmig.getErrorneousAddressRows()));
			request.setAttribute(RC_REJ_ADDRESS_ROWS, String.valueOf(dbmig.getRejectedAddressRows()));
			
			request.setAttribute(RC_MIG_BSKT_ROWS, String.valueOf(dbmig.getProcessedBasketRows() - dbmig.getRejectedBasketRows() - dbmig.getErrorneousBasketRows()));
			request.setAttribute(RC_ERR_BSKT_ROWS, String.valueOf(dbmig.getErrorneousBasketRows()));
			request.setAttribute(RC_REJ_BSKT_ROWS, String.valueOf(dbmig.getRejectedBasketRows()));
			
			request.setAttribute(RC_MIG_OBJID_ROWS, String.valueOf(dbmig.getProcessedObjectIdRows() - dbmig.getRejectedObjectIdRows() - dbmig.getErrorneousObjectIdRows()));
			request.setAttribute(RC_ERR_OBJID_ROWS, String.valueOf(dbmig.getErrorneousObjectIdRows()));
			request.setAttribute(RC_REJ_OBJID_ROWS, String.valueOf(dbmig.getRejectedObjectIdRows()));
			
			request.setAttribute(RC_MIG_ITEM_ROWS, String.valueOf(dbmig.getProcessedItemsRows() - dbmig.getRejectedItemsRows() - dbmig.getErrorneousItemsRows()));
			request.setAttribute(RC_ERR_ITEM_ROWS, String.valueOf(dbmig.getErrorneousItemsRows()));
			request.setAttribute(RC_REJ_ITEM_ROWS, String.valueOf(dbmig.getRejectedItemsRows()));
			
			request.setAttribute(RC_MIG_BUPA_ROWS, String.valueOf(dbmig.getProcessedBPRows() - dbmig.getRejectedBPRows() - dbmig.getErrorneousBPRows()));
			request.setAttribute(RC_ERR_BUPA_ROWS, String.valueOf(dbmig.getErrorneousBPRows()));
			request.setAttribute(RC_REJ_BUPA_ROWS, String.valueOf(dbmig.getRejectedBPRows()));
			
			request.setAttribute(RC_MIG_EXTCONF_ROWS, String.valueOf(dbmig.getProcessedExtConfigRows() - dbmig.getRejectedExtConfigRows() - dbmig.getErrorneousExtConfigRows()));
			request.setAttribute(RC_ERR_EXTCONF_ROWS, String.valueOf(dbmig.getErrorneousExtConfigRows()));
			request.setAttribute(RC_REJ_EXTCONF_ROWS, String.valueOf(dbmig.getRejectedExtConfigRows()));
			
			request.setAttribute(RC_MIG_SHIPTO_ROWS, String.valueOf(dbmig.getProcessedShipToRows() - dbmig.getRejectedShipToRows() - dbmig.getErrorneousShipToRows()));
			request.setAttribute(RC_ERR_SHIPTO_ROWS, String.valueOf(dbmig.getErrorneousShipToRows()));
			request.setAttribute(RC_REJ_SHIPTO_ROWS, String.valueOf(dbmig.getRejectedShipToRows()));
			
			request.setAttribute(RC_MIG_EXTDATHEAD_ROWS, String.valueOf(dbmig.getProcessedExtDataHeaderRows() - dbmig.getRejectedExtDataHeaderRows() - dbmig.getErrorneousExtDataHeaderRows()));
			request.setAttribute(RC_ERR_EXTDATHEAD_ROWS, String.valueOf(dbmig.getErrorneousExtDataHeaderRows()));
			request.setAttribute(RC_REJ_EXTDATHEAD_ROWS, String.valueOf(dbmig.getRejectedExtDataHeaderRows()));
			
			request.setAttribute(RC_MIG_EXTDATITEM_ROWS, String.valueOf(dbmig.getProcessedExtDataItemRows() - dbmig.getRejectedExtDataItemRows() - dbmig.getErrorneousExtDataItemRows()));
			request.setAttribute(RC_ERR_EXTDATITEM_ROWS, String.valueOf(dbmig.getErrorneousExtDataItemRows()));
			request.setAttribute(RC_REJ_EXTDATITEM_ROWS, String.valueOf(dbmig.getRejectedExtDataItemRows()));
			
			request.setAttribute(RC_MIG_TEXT_ROWS, String.valueOf(dbmig.getProcessedTextRows() - dbmig.getRejectedTextRows() - dbmig.getErrorneousTextRows()));
			request.setAttribute(RC_ERR_TEXT_ROWS, String.valueOf(dbmig.getErrorneousTextRows()));
			request.setAttribute(RC_REJ_TEXT_ROWS, String.valueOf(dbmig.getRejectedTextRows()));
			
			request.setAttribute(RC_MIRATION_FINISHED, String.valueOf(dbmig.isMigrationFinished()));
			
			request.setAttribute(RC_MIRATION_DURATION, dbmig.getMigrationDuration());
		}
		 	
		return mapping.findForward(forwardTo);
	}

}

