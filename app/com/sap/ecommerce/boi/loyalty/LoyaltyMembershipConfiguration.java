/*
 * Created on Apr 9, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.ecommerce.boi.loyalty;

/**
 * @author d038645
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface LoyaltyMembershipConfiguration {
	/**
	 * get the loyalty membership identification
	 * 
	 * @return id of the loyalty membership
	 * 
	 */   
	public String getMembershipId();

	/**
	 * set the loyalty membership identification
	 * 
	 * @param id of the loyalty membership
	 * 
	 */   
	 public void setMembershipId(String newMembShipId);

}
