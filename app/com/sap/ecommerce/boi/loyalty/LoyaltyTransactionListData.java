package com.sap.ecommerce.boi.loyalty;

import java.text.DateFormat;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.Iterable;

public interface LoyaltyTransactionListData	extends BusinessObjectBaseData, Iterable {

	/**
	 * sets a loyalty transaction list
	 * @param loyaltyTransactionList, the loyalty transaction list to be setted
	 */
	public void addLoyaltyTransactionList(LoyaltyTransactionListData loyaltyTransactionList);

	/**
	 * adds a loyalty transaction to the loyalty transaction list
	 * @param loyaltyTransaction, the loyalty transaction to be added
	 */
	public void addLoyaltyTransaction(LoyaltyTransactionData loyaltyTransaction);

	/**
	 * iterator for the loyalty transaction list
	 * @return iterator that contains loyalty transaction objects
	 */
	public Iterator iterator();

	/**
	 * Returns the loyaltyTransaction line of the list
	 * 
	 * @param i of type int, index of loyaltyTransaction line 
	 * @return loyaltyTransaction line of type LoyaltyTransaction 
	 */
	public LoyaltyTransactionData getData(int i);

	/**
	 * Returns the number of transactions in the list
	 * @return count of transactions lines
	 */
	public int size();

	/**
	 * sorts the loyalty transaction list by "attribute"
	 * @param the attribute which occurs the sort
	 */
	public void order(DateFormat dateFormat, String attribute);

	/**
	 * sets the transaction From Date
	 * @param the transaction From Date to be set
	 */
	public void setFromTransactionDate(String fromTransaction);

	/**
	 * sets the transaction To Date
	 * @param the transaction To Date to be set
	 */
	public void setToTransactionDate(String toTransaction);

	/**
	 * returns the transaction From Date
	 * @return the transaction From Date
	 */
	public String getFromTransactionDate();

	/**
	 * returns the transaction To Date
	 * @return the transaction To Date
	 */
	public String getToTransactionDate();

}
