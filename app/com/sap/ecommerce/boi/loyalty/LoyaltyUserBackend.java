/*****************************************************************************
	Class:        LoyaltyUserBackend
	Copyright (c) 2008, SAP AG, All rights reserved.
	Created:      14.3.2008
	Version:      1.0
*****************************************************************************/

package com.sap.ecommerce.boi.loyalty;

 
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipData;




public interface LoyaltyUserBackend {


   public static final String LOYALTY_ERROR = "3";


   /**
    * Register new consumer and create 
    * a new loyalty program membership
    * for this consumer
    * 
    */
   public RegisterStatus registerAndJoinLoyaltyProgram(IsaUserBaseData user,
					 	  							   String shopId,
                                                       String ptCode,
													   LoyaltyMembershipData loyMemberShip,
													   AddressData address,
													   String password,
													   String password_verify)
	   throws BackendException;			
	       
	       
   /**
    * login of the internet user into
    * the b2b or b2c szenario and
    * check if the user is member of
    * a loyalty program
    *
    * @param id of the internet user
    * @param password of the internet user
    * @param loyalty membership 
    *
    * @return integer value that indicates if
    *         the login was sucessful
    */
    public LoginStatus loginAndCheckLoyaltyMembership(IsaUserBaseData user,
												      LoyaltyMembershipData loyMembership)
	   throws BackendException; 	       




	/**
	 * login of the internet user into
	 * the b2b or b2c szenario and
	 * join the given loyalty program
	 *
	 * @param id of the internet user
	 * @param password of the internet user
	 * @param loyalty membership 
	 *
	 * @return integer value that indicates if
	 *         the login was sucessful
	 */
	 public LoginStatus loginAndJoinLoyaltyProgram(IsaUserBaseData user,
												   LoyaltyMembershipData loyMembership)
		throws BackendException; 	
	 
	 
	 public boolean isLoyaltyCampaign(IsaUserBaseData user,
			                          String loyaltyCampaignKey)
           throws BackendException;   
}	       							        