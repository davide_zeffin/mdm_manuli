package com.sap.ecommerce.boi.loyalty;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

public interface LoyaltyTransactionData extends BusinessObjectBaseData {

	/**
	 * return the loyalty transaction expiration date
	 * 
	 * @return a string that represents the loyalty transaction expiration date
	 */
	public String getExpirationDate();

	/**
	 * return the loyalty transaction miles
	 * 
	 * @return a string that represents the loyalty transaction miles
	 */
	public String getMiles();

	/**
	 * return the loyalty transaction posting date
	 * 
	 * @return a string that represents the loyalty transaction posting date
	 */
	public String getPostingDate();

	/**
	 * return the loyalty transaction qualification type
	 * 
	 * @return a string that represents the loyalty transaction qualification type
	 */
	public String getQualificationType();

	/**
	 * return the loyalty transaction qualifying miles
	 * 
	 * @return a string that represents the loyalty transaction qualifying miles
	 */
	public String getQualifyingMiles();

	/**
	 * return the loyalty transaction reason
	 * 
	 * @return a string that represents the loyalty transaction reason
	 */
	public String getReason();

	/**
	 * return the loyalty transaction id
	 * 
	 * @return a string that represents the loyalty transaction id
	 */
	public String getTransactionId();

	/**
	 * return the loyalty transaction transaction type
	 * 
	 * @return a string that represents the loyalty transaction type
	 */
	public String getTransactionType();

	/**
	 * sets the loyalty transaction expiration date
	 * 
	 * @param a string that represents the loyalty transaction expiration date
	 */
	public void setExpirationDate(String string);

	/**
	 * sets the loyalty transaction miles
	 * 
	 * @param a string that represents the loyalty transaction miles
	 */
	public void setMiles(String string);

	/**
	 * sets the loyalty transaction posting date
	 * 
	 * @param a string that represents the loyalty transaction posting date
	 */
	public void setPostingDate(String string);

	/**
	 * sets the loyalty transaction qualification type
	 * 
	 * @param a string that represents the loyalty transaction qualification type
	 */
	public void setQualificationType(String string);

	/**
	 * sets the loyalty transaction qualifiying Miles
	 * 
	 * @param a string that represents the loyalty transaction qualifiying miles
	 */
	public void setQualifyingMiles(String string);

	/**
	 * sets the loyalty transaction qualification reason
	 * 
	 * @param a string that represents the loyalty transaction reason
	 */
	public void setReason(String string);

	/**
	 * sets the loyalty transaction qualification ID
	 * 
	 * @param a string that represents the loyalty transaction qualification ID
	 */
	public void setTransactionId(String string);

	/**
	 * sets the loyalty transaction qualification transaction type
	 * 
	 * @param a string that represents the loyalty transaction type
	 */
	public void setTransactionType(String string);

}