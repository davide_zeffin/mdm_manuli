package com.sap.ecommerce.boi.loyalty;

import java.text.DateFormat;
import java.util.Date;

import com.sap.ecommerce.businessobject.campaign.MarketingCampaignList;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyProgram;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyCampaign;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

public interface LoyaltyMembershipData extends BusinessObjectBaseData, LoyaltyMembershipConfiguration {

	/**
	 * Retrieves the loyalty transaction for the membership from the backend.
	 *
	 * @return list of loyalty transactions
	 */
	public LoyaltyTransactionListData getLoyaltyTransactionsFromBackend(String membershipGuid, String pointAccountGuid, DateFormat dateFormat, Date fromDate, Date toDate);

	/**
	 * Retrieves the loyalty campaigns for the membership from the backend.
	 *
	 * @return list of loyalty transactions
	 */
	public MarketingCampaignList getLoyaltyCampaignsFromBackend(String membershipGuid, String memberGuid, String campaignTextId, DateFormat dateFormat);


	/**
	 * Retrieves the loyalty transaction for the membership from the backend.
	 *
	 * @return list of loyalty transactions
	 */
	public LoyaltyTransactionListData getLoyaltyTransactions();

	/**
	 * Retrieves the loyalty campaigns for the membership from the backend.
	 *
	 * @return list of loyalty transactions
	 */
	public MarketingCampaignList getLoyaltyCampaigns();

	/**
	 * sets the loyalty transaction list to this loyalty membership
	 * 
	 * @param list, the loyalty transaction list that should be set
	 */
	public void setLoyaltyTransactionList(LoyaltyTransactionListData list);

	/**
	 * sets the loyalty campaign list to this loyalty membership
	 * 
	 * @param list, the loyalty campaign list that should be set
	 */
	public void setLoyaltyCampaignList(MarketingCampaignList list);

    /**
     * sets the loyalty program to this loyalty membership
     * 
     * @param program, the loyalty program that should be set
     */
    public void setLoyaltyProgram(LoyaltyProgram program);
    
    
	/**
	 * get the loyalty program of this loyalty membership
	 * 
	 * @param program, the loyalty program that should be set
	 */
	public LoyaltyProgram getLoyaltyProgram(); 
	
	     
 	/**
 	 * set the loyalty promotion
 	 * 
 	 * @param promotion
 	 */
     public void setLoyaltyPromotion(LoyaltyCampaign promotion);
     
 	/** 
 	 * get the loyalty campaign that promotes the loyalty program
 	 * 
 	 * @return loyalty campaign that promotes the loyalty program
 	 */
     public LoyaltyCampaign getLoyaltyPromotion();
     
    /** 
     * get the loyalty point account of the loyalty membership
     * 
     * @return loyalty point account 
     */
     public LoyaltyPointAccountData getPointAccountData();

}
