package com.sap.ecommerce.boi.loyalty;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

public interface LoyaltyPointAccountData extends BusinessObjectBaseData {

    /**
     * Returns the point code id of the point account
     * 
     * @return pointCodeId as String, point code id of the point account
     */
    public String getPointCodeId();

    /**
     * Sets the point code id of the point account
     * 
     * @param pointCodeId as String, point code id of the point account
     */
    public void setPointCodeId(String pointCodeId);

	/**
	 * Sets the description of point account
	 * 
	 * @param descr as String, description of the point account
	 */
	public void setDescr(String descr);

}
