/*****************************************************************************
	class		  ProductData
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.boi.campaign;

import com.sap.isa.backend.boi.isacore.ProductData;

/**
 * 
 * Campaign product item data
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface CampaignProductItemData extends ProductData {

	public String getAmount();

	public void setAmount(String amount);

	public String getAmountUom();

	public void setAmountUom(String amountUom);

	public boolean getIsChecked();

	public void setIsChecked(boolean isChecked);

}
