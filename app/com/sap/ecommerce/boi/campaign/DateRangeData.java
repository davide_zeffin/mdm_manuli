/*****************************************************************************
	class		  DateRangeData
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.boi.campaign;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/** 
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface DateRangeData extends BusinessObjectBaseData {

	public static final String DATE_RANGE_TYPE_PLAN = "1";
	public static final String DATE_RANGE_TYPE_ACTUAL = "2";
	public static final String DATE_RANGE_TYPE_EXT = " ";

	public static final String PERIOD_TYPE_CREATE_FOLLOW_UP = "90";
	public static final String PERIOD_TYPE_CREATE_GOODS_RECEIPT = "95";


	public String getDateID();

	public String getEndDate();

	public String getPeriodType();

	public String getStartDate();

	public void setDateID(String dateID);

	public void setEndDate(String endDate);

	public void setPeriodType(String periodType);

	public void setStartDate(String startDate);

	public String getDateRangeType();

	public void setDateRangeType(String string);
}
