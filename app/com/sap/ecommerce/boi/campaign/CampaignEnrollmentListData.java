/*****************************************************************************
    class         CampaignEnrollmentListData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      28.11.2006
    Version:      1.0

    $Revision: #01 $
    $Date: 2006/11/28 $
*****************************************************************************/
package com.sap.ecommerce.boi.campaign;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.Iterable;

/**
 * 
 * Campaign enrollment list data
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface CampaignEnrollmentListData extends BusinessObjectBaseData, Iterable {

    public CampaignEnrollmentData createEnrollment();

    public void addEnrollment(CampaignEnrollmentData enrollment);

    public Iterator iterator();
    
    public CampaignEnrollmentData getData(int i);

    public int size();
}
