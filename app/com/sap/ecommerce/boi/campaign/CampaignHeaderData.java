/*****************************************************************************
	class		  CampaignHeaderData
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.boi.campaign;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/**
 * 
 * Campaign header data
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface CampaignHeaderData extends BusinessObjectBaseData {
	
	public static final String SURPLUS_INVITATION = "01";
	
	public static final String SPL_REQUESTED_RETURN = "02";

	public String getCampaignID();

	public void setCampaignID(String campaignID);

	public String getDescription();

	public void setDescription(String description);

	public String getType();

	public void setType(String type);

	public String getTypeDescription();

	public void setTypeDescription(String campaignTypeDescription);

	public void setPublic(boolean isPublic);

	public void setValid(boolean isValid);

	public void setEnteredManually(boolean isEnteredManually);

	public boolean isPublic();

	public boolean isValid();

	public boolean isEnteredManually();

	public String getMessage();

	public void setMessage(String message);
	
	public String getRefType();

	public void setRefType(String type);	
	
	public String getScenario();

	public void setScenario(String type);	

    /**
     * Method sets the long text of the Campaign
     *
     * @param text as String
     */
    public void setCampaignText(String text);
     
    /**
     * Method returns the long text of the Campaign
     *
     * @return text as String
     */
    public String getCampaignText();
    
    /**
     * Method sets the enrollment start date
     *
     * @param date as String
     */
    public void setEnrollmStartDate(String date);
     
    /**
     * Method returns the enrollment start date
     *
     * @return date as String
     */
    public String getEnrollmStartDate();
    
    /**
     * Method sets the enrollment end date
     *
     * @param date as String
     */
    public void setEnrollmEndDate(String date);
     
    /**
     * Method returns the enrollment end date
     *
     * @return date as String
     */
    public String getEnrollmEndDate();
    
    /**
     * Method sets the planned start date of the campaign
     *
     * @param date as String
     */
    public void setPlannedStartDate(String date);
     
    /**
     * Method returns the planned start date of the campaign 
     *
     * @return date as String
     */
    public String getPlannedStartDate();
    
    /**
     * Method sets the planned end date of the campaign
     *
     * @param date as String
     */
    public void setPlannedEndDate(String date);
     
    /**
     * Method returns the planned end date of the campaign
     *
     * @return date as String
     */
    public String getPlannedEndDate();

    /**
     * Method sets the flag if enrollment is allowed for this campaign
     *
     * @param enrollmentAllowed as boolean
     */
    public void setIsEnrollmAllowed(boolean enrollmentAllowed);
     
    /**
     * Method returns the flag if enrollment is allowed for this campaign
     *
     * @return enrollmentAllowed as boolean
     */
    public boolean setIsEnrollmAllowed();
    
    /**
     * Method sets the enroller Guid 
     *
     * @param enrollerGuid as String
     */
    public void setEnrollerGuid(String enrollerGuid);
     
    /**
     * Method returns the Guid of the enroller
     *
     * @return enrollerGuid as String
     */
    public String getEnrollerGuid();
    
    /**
     * @param enrollmPeriodeStatus as String, the status indicates if enrollment period is in the past, 
     *        in the future or in the present.
     */
    public void setEnrollmPeriodStatus(String enrollmPeriodeStatus);

}
