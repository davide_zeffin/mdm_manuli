/*****************************************************************************
	class		  CampaignEnrollmentData
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      28.11.2006
	Version:      1.0

	$Revision: #01 $
	$Date: 2006/11/28 $
*****************************************************************************/
package com.sap.ecommerce.boi.campaign;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;

/** 
 * Campaign Enrollment Data
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface CampaignEnrollmentData extends BusinessObjectBaseData {

    /**
     * Method sets the GUID of the enrollee. This corresponds to the GUID of the business partner.
     *
     * @param enrolleeGuid as GUID
     */
    public void setEnrolleeGuid(String enrolleeGuid);
     
    /**
     * Method returns the GUID of the enrollee. 
     *
     * @return enrolleeGuid as GUID
     */
    public String getEnrolleeGuid();
     
    /**
     * Method sets the description of the enrollee
     *
     * @param description as String
     */
    public void setEnrolleeDescription(String description);
     
    /**
     * Method returns the description of the enrollee
     *
     * @return description as String
     */
    public String getEnrolleeDescription();
    
    /**
     * Method sets the id of the enrollee. This corresponds to the business partner Id.
     *
     * @param id as String, business partner id of the enrollee
     */
    public void setEnrolleeID(String id);
     
    /**
     * Method returns the ID of the enrollee
     *
     * @return id as String, business partner id of the enrollee
     */
    public String getEnrolleeID(); 
 
    /**
     * Method sets the description of the SoldTo of the enroller who did the first enrollment. 
     *
     * @param soldToDescr as String
     */
    public void setFirstEnrolledBySoldTo(String soldToDescr);
     
    /**
     * Method returns the description of the SoldTo of the enroller who did the first enrollment. 
     *
     * @return soldToDescr as String
     */
    public String getFirstEnrolledBySoldTo(); 
 
    /**
     * Method sets the description of the contact person of the enroller who did the first enrollment.  
     * The contact person is derived from the user.
     *
     * @param contactDescr as String
     */
    public void setFirstEnrolledByContact(String contactDescr);
     
    /**
     * Method returns the description of the contact person of the enroller who did the first enrollment. 
     *
     * @return contactDescr as String
     */
    public String getFirstEnrolledByContact(); 
 
    /**
     * Method sets the date of the first enrollment.  
     *
     * @param date as String, first enrollment date
     */
    public void setFirstEnrolledDate(String date);
     
    /**
     * Method returns the date of the first enrollment. 
     *
     * @return date as String, first enrollment date
     */
    public String getFirstEnrolledDate(); 
 
    /**
     * Method sets the timestamp of the first enrollment.  
     *
     * @param timestamp as String, first enrollment date as timestamp
     */
    public void setFirstEnrolledTimestamp(String timestamp);
     
    /**
     * Method returns the timestamp of the first enrollment. 
     *
     * @return timestamp as String, first enrollment date as timestamp
     */
    public String getFirstEnrolledTimestamp(); 
 
    /**
     * Method sets the description of the SoldTo of the enroller who did the last change. 
     *
     * @param soldToDescr as String
     */
    public void setLastChangedBySoldTo(String soldToDescr);
     
    /**
     * Method returns the description of the SoldTo of the enroller who did the last change. 
     *
     * @return soldToDescr as String
     */
    public String getLastChangedBySoldTo(); 
 
    /**
     * Method sets the description of the contact person of the enroller who did the last change.  
     * The contact person is derived from the user.
     *
     * @param contactDescr as String
     */
    public void setLastChangedByContact(String contactDescr);
     
    /**
     * Method returns the description of the contact person of the enroller who did the last change. 
     *
     * @return contactDescr as String
     */
    public String getLastChangedByContact(); 
 
    /**
     * Method sets the date of the last change.  
     *
     * @param date as String, last change date
     */
    public void setLastChangedDate(String date);
     
    /**
     * Method returns the date of the last change.  
     *
     * @return date as String, last change date
     */
    public String getLastChangedDate(); 
 
    /**
     * Method sets the timestamp of the last change.  
     *
     * @param timestamp as String, last change date as timestamp
     */
    public void setLastChangedTimestamp(String timestamp);
     
    /**
     * Method returns the timestamp of the last change.  
     *
     * @return timestamp as String, last change date as timestamp
     */
    public String getLastChangedTimestamp(); 
 
    /**
     * Method sets the enrollment flag.  
     *
     * @param enrolledFlag as boolean
     */
    public void setIsEnrolled(boolean enrolledFlag);
     
    /**
     * Method returns the enrollment flag. 
     *
     * @return enrolledFlag as boolean
     */
    public boolean isEnrolled(); 
 
    /**
     * Method sets the flag indicating if the first enrollment was done internally.  
     *
     * @param enrolledIntFlag as boolean
     */
    public void setIsFirstEnrolledInternally(boolean enrolledIntFlag);
     
    /**
     * Method returns the flag indicating if the first enrollment was done internallyflag. 
     *
     * @return enrolledIntFlag as boolean
     */
    public boolean isFirstEnrolledInternally(); 
 
    /**
     * Method sets the flag indicating if the last change was done internally.  
     *
     * @param enrolledIntFlag as boolean
     */
    public void setIsLastChangedInternally(boolean enrolledIntFlag);
     
    /**
     * Method returns the flag indicating if the last change was done internallyflag. 
     *
     * @return enrolledIntFlag as boolean
     */
    public boolean isLastChangedInternally(); 
 
    /**
     * Method sets the flag indicating if the enrollment was changed.  
     */
    public void setIsChanged();
     
    /**
     * Method returns the flag indicating if the enrollment was changed. 
     *
     * @return isChanged as boolean
     */
    public boolean isChanged(); 
 
}
