package com.sap.ecommerce.boi.campaign;

import java.text.DateFormat;
import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.Iterable;

public interface MarketingCampaignListData extends BusinessObjectBaseData, Iterable {

	/**
	 * adds a Campaign to the Marketing Campaign List
	 * @param the campaign that will be added to the list
	 */
	public void addCampaign(CampaignData campaign);

	/**
	 * iterator
	 * @return an iterator that contains the campaign objects from this marketing campaign list
	 */
	public Iterator iterator();

	/**
	 * Returns the CampaignData line of the list
	 * 
	 * @param i of type int, index of CampaignData line 
	 * @return campaign line of type CampaignData 
	 */
	public CampaignData getData(int i);

	/**
	 * @return the size of the marketing campaign list
	 */
	public int size();

	/**
	 * sorts the marketing campaign list by "attribute"
	 * @param the attribute which occurs the sort
	 */
	public void order(DateFormat dateFormat, String attribute);

	/**
	 * returns the campaign object identified by the campaignID
	 * @return the campaign identified by campaignID
	 * @param the campaignID for the returned campaign
	 */
	public CampaignData getCampaign(String campaignID);
}
