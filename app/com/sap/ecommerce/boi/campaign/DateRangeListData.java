/*****************************************************************************
	class		  DateRangeListData
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.boi.campaign;

import java.util.Iterator;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.Iterable;

/**
 * 
 * Date range list data
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface DateRangeListData extends BusinessObjectBaseData, Iterable {

	public DateRangeData createDateRange();

	public void addDateRange(DateRangeData dateRange);

	public Iterator iterator();
}
