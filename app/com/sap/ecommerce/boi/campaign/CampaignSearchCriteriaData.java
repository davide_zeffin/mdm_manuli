/*****************************************************************************
	class		  CampaignSearchCriteriaData
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      28.11.2006
	Version:      1.0

	$Revision: #01 $
	$Date: 2006/11/28 $
*****************************************************************************/
package com.sap.ecommerce.boi.campaign;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.core.TechKey;

/**
 * 
 * Campaign search criteria data
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface CampaignSearchCriteriaData extends BusinessObjectBaseData {
	
    /**
     * Method sets the campaign ID
     *
     * @param campID as String
     */
    public void setCampaignID(String campID);
     
    /**
     * Method returns the campaign ID
     *
     * @return campID as String
     */
    public String getCampaignID();
    
    /**
     * Method sets the flag controlling if the complete business partner hierarchy 
     * of the enroller is searched.
     *
     * @param searchBpHier as boolean
     */
    public void setSearchForBpHier(boolean searchBpHier);
     
    /**
     * Method returns the search control flag.
     *
     * @return searchBpHier as boolean
     */
    public boolean getSearchForBpHier();
    
    /**
     * Method sets the Guid of the enroller. This corresponds to the GUID 
     * of the business partner. 
     *
     * @param enrollerGuid as String
     */
    public void setEnrollerGuid(String enrollerGuid);
     
    /**
     * Method returns the Guid of the enroller. 
     *
     * @return enrollerGuid as String
     */
    public String getEnrollerGuid();
    
    /**
     * Method sets the id of the enroller. This corresponds to the id 
     * of the business partner. 
     *
     * @param enrollerId as String
     */
    public void setEnrollerId(String enrollerId);
     
    /**
     * Method returns the id of the enroller. 
     *
     * @return enrollerId as String
     */
    public String getEnrollerId();
    
    /**
     * Method sets the guid of the enrollee. This corresponds to the GUID 
     * of the business partner. 
     *
     * @param enrolleeGuid as String
     */
    public void setEnrolleeGuid(String enrolleeString);
     
    /**
     * Method returns the guid of the enrollee. 
     *
     * @return enrolleeGuid as String
     */
    public String getEnrolleeGuid();
    
    /**
     * Method sets the id of the enrollee. This corresponds to the id 
     * of the business partner. 
     *
     * @param enrolleeId as String
     */
    public void setEnrolleeId(String enrolleeId);
     
    /**
     * Method returns the id of the enrollee. 
     *
     * @return enrolleeId as String
     */
    public String getEnrolleeId();
    
    /**
     * Method sets the text type for the long text of the Campaign.
     *
     * @param textType as String
     */
    public void setTextTypeCampDescr(String textType);
     
    /**
     * Method returns the text type for the long text of the Campaign. 
     *
     * @return textType as String
     */
    public String getTextTypeCampDescr();

    /**
     * Method sets the country
     *
     * @param country as String
     */
    public void setCountry(String country);
     
    /**
     * Method returns the country
     *
     * @return country as String
     */
    public String getCountry();

}
