/*****************************************************************************
	Class         CampaignData
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.boi.campaign;

import java.util.Date;
import com.sap.isa.core.TechKey;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.businessobject.CommunicationException;


/**
 * 
 * Campaign data
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface CampaignData extends BusinessObjectBaseData{

	/**
	 * Returns a new recall header instance
	 *
	 * @return Header data
	 */
	public CampaignHeaderData createHeader();

	/**
	 * Sets the header information. 
	 *
	 * @param header Header data to set
	 */
	public void setHeader(CampaignHeaderData header);

	/**
	 * Retrieves the header information of the document.
	 *
	 * @return Header data
	 */
	public CampaignHeaderData getHeaderData();
	
	/**
	 * Checks if the BP entered on UI is in one of the target group of the campaign
	 */
	public void checkBPisInTargetGroup(String refDocId, String soldTo) throws CommunicationException;
	
	/**
	 * Returns the product list
	 *
	 * @return Product data list
	 */
	public ProductListData getProductListData();
	
	/**
	 * Returns the date range list
	 *
	 * @return Date range data list
	 */
	public DateRangeListData getDateRangeListData();
	
	/**
	 * Retrieves campaign header data from the backend and stores the data
	 * in this object
	 */
	public void readCampaignHeader() throws CommunicationException;
	
	/**
	 * Checks eligibility of a sold-to for a private campaign
	 * and stores in this object
	 */
	public void checkPrivateCampaignEligibility (TechKey soldToGuid, String salesOrg, String distChannel, String division, Date campaignDate) throws CommunicationException;  

    /**
     * Retrieves the enrollment list of the campaign.
     *
     * @return enrollmList Campaign enrollment list
     */
    public CampaignEnrollmentListData getCampaignEnrollmentListData();

    /**
     * Retrieves the search criteria information of the campaign.
     *
     * @return searchCriteria Search criteria data
     */
    public CampaignSearchCriteriaData getSearchCriteriaData();

}
