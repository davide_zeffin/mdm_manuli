package com.sap.ecommerce.action.loyalty;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;


/*****************************************************************************
			Class         ShowLoyaltyRedeemPointsAction
			Copyright (c) 2008, SAP AG, Germany, All rights reserved.
			Description:  Action to check prerequisites for entering in redemption process 
			Created:      29.02.08
			Version:      1.0
*****************************************************************************/

public class ShowLoyaltyRedeemPointsAction extends EComBaseAction {

	public void initialize() {
	}
	
	public final ActionForward ecomPerform(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
			throws IOException, ServletException, CommunicationException {	   
	
	   final String METHOD_NAME = "isaPerform()";
	   log.entering(METHOD_NAME);
	   String forward = "success";
	   
	   log.exiting();
	   return mapping.findForward(forward);
	}
		   
}