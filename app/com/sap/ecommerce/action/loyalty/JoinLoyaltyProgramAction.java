/*****************************************************************************
		Class         JoinLoyaltyProgramAction
		Copyright (c) 2008, SAP AG, Germany, All rights reserved.
		Description:  Action to join a loyalty program
		Created:      18.03.08
		Version:      1.0
*****************************************************************************/

package com.sap.ecommerce.action.loyalty;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;


public class JoinLoyaltyProgramAction extends EComBaseAction {

	  /**
	   * Parameter name for password change.
	   */
	   public static final String JOIN_LOYALTY_PROGRAM = "joinLoyaltyProgram";

				
	public final ActionForward ecomPerform(ActionMapping mapping,
				                           ActionForm form,
				                           HttpServletRequest request,
				                           HttpServletResponse response,
				                           UserSessionData userSessionData,
				                           RequestParser requestParser,
				                           MetaBusinessObjectManager mbom,
			                     	       boolean multipleInvocation,
				                           boolean browserBack)
				throws IOException, ServletException, CommunicationException {

		   final String METHOD_NAME = "ecomPerform()";
		   log.entering(METHOD_NAME);
		   
		   String forward = "success";
	   
		   // the user has select the "Join Now" button
		   userSessionData.setAttribute(JOIN_LOYALTY_PROGRAM, "true");
		   
		   // preset the "join loyalty program flag" in this case
		   request.setAttribute("joinloyaltyprogramflag", "true"); 
   		   userSessionData.setAttribute("joinloyaltyprogramflag", "true");
   		 	   
		   log.exiting();
		   return mapping.findForward(forward);
		}
	}