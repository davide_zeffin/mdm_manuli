/*****************************************************************************
	Class         CampaignBaseAction
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      05.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/05 $
*****************************************************************************/

package com.sap.ecommerce.action.campaign;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;

/**
 * <p>
 * Base action class for the campaign component. The user has to log in 
 * before calling this class. 
 * </p>
 *
 * @author SAP
 * @version 1.0
 * 
 * @see com.sap.isa.isacore.action.EComBaseAction
  */
public abstract class CampaignBaseAction extends EComBaseAction {

	/**
	 * String constant specifying if actions are succesful
	 */
	public static final String FORWARD_SUCCESS = "success";
	
	/**
	 * String constant specifying if actions are succesful
	 */
	public static final String FORWARD_ERROR = "error";

	/**
	 * Implements the method ecomPerform used by EComBaseAction.
	 */
	public final ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {

		CampaignBusinessObjectManager campaignBom =
			(CampaignBusinessObjectManager) mbom.getBOMbyName(
				CampaignBusinessObjectManager.CAMPAIGN_BOM);

		return eserviceCampaignPerform(
			mapping,
			form,
			request,
			response,
			userSessionData,
			requestParser,
			mbom,
			campaignBom,
			multipleInvocation,
			browserBack);

	}

	/**
	 * You have to have implement here your action coding. <br>
	 * As an additional parameter you will get a flag indicating, whether or not
	 * a multiple invocation occured.
	 *
	 * @param mapping            The ActionMapping used to select this instance
	 * @param form               The <code>FormBean</code> specified in the
	 *                              config.xml file for this action
	 * @param request            The request object
	 * @param response           The response object
	 * @param userSessionData    Object wrapping the session
	 * @param requestParser      Parser to simple retrieve data from the request
	 * @param mbom               Reference to the MetaBusinessObjectManager
	 * @param campaignBom 		 Reference to the CampaignBusinessObjectManager
	 * @param multipleInvocation Flag indicating, that a multiple invocation occured
	 * @param browserBack        Flag indicating a browser back
	 * @return Forward to another action or page
	 */
	public abstract ActionForward eserviceCampaignPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		CampaignBusinessObjectManager campaignBom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException;

}
