/*****************************************************************************
	Class         CampaignCRM.java
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26-Jan-2004
	Version:      1.0

	Revision: #01
	Date: 2004/01/05
*****************************************************************************/
package com.sap.ecommerce.backend.crm.campaign;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sap.ecommerce.backend.intf.campaign.CampaignBackend;
import com.sap.ecommerce.boi.campaign.CampaignData;
import com.sap.ecommerce.boi.campaign.CampaignEnrollmentData;
import com.sap.ecommerce.boi.campaign.CampaignEnrollmentListData;
import com.sap.ecommerce.boi.campaign.CampaignHeaderData;
import com.sap.ecommerce.boi.campaign.CampaignProductItemData;
import com.sap.ecommerce.boi.campaign.CampaignSearchCriteriaData;
import com.sap.ecommerce.boi.campaign.DateRangeData;
import com.sap.ecommerce.boi.campaign.DateRangeListData;
import com.sap.ecommerce.boi.campaign.ProductListData;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.mw.jco.JCO;

/**
 *
 * CampaignCRM reads campaign data from the CRM backend
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class CampaignCRM extends IsaBackendBusinessObjectBaseSAP implements CampaignBackend {

	private final static IsaLocation log = IsaLocation.getInstance(CampaignCRM.class.getName());

	protected static final String GET_DETAIL_FUNCTION = "CRM_ESRV_CAMPAIGN_GET_PRODUCTS";

	/**
	  * Read campaign data from the backend. 
	  *
	  * @param campaign The document to read the data in
	  */
	public void readFromBackend(CampaignData campaign) throws BackendException {

		final String METHOD_NAME = "readFromBackend(campaign)";

		log.entering(METHOD_NAME);

		JCoConnection aJCoCon = getAvailableStatelessConnection();

		// JCO init
		JCO.Function function = aJCoCon.getJCoFunction(CampaignCRM.GET_DETAIL_FUNCTION);
		
		try {
			JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());

			// add import parameter 
			importParam.setValue(campaign.getTechKey().toString(), "IV_CAMPAIGN_GUID");

			// log the import parameter
			if (log.isDebugEnabled()) {
				JCoHelper.logCall(CampaignCRM.GET_DETAIL_FUNCTION, importParam, null, log);
			}

			// call the back end function
			aJCoCon.execute(function);

			// getting export parameter
			JCO.ParameterList exportParam = function.getExportParameterList();

			// log the export parameter
			if (log.isDebugEnabled()) {
				JCoHelper.logCall(CampaignCRM.GET_DETAIL_FUNCTION, null, exportParam, log);
			}

			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

			if (messages != null) {
				MessageCRM.splitMessagesToTarget(campaign, messages);
			}

			// retrieve date ranges
			retrieveDateRanges(campaign, function);

			// retrieve products
			retrieveProducts(campaign, function);			
		}
		catch (JCO.Exception ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_BUSINESS_LOGIC, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);
		}
		finally {
			// close JCO connection 			
			aJCoCon.close();

			log.exiting();
		}
	}


	/**
	 * This method verifies if the business partner entered on the UI
	 * is part of one of the target group of this campaign.
	 * It returns void but fills the message list with the appropriate
	 * message to display on campaign_status.jsp in case of error.
	 * @param refDocId - Campaign id 
	 * @param soldTo - Business partner number entered on UI
 	 * @param campaign - BO Campaign
	 */
	public void checkBPInTargetGroup(CampaignData campaign, String refDocId, String soldTo) throws BackendException {
	
		final String METHOD_NAME = "checkBpInTargetGroup(refDocId, soldTo)";

		log.entering(METHOD_NAME);

		try {		
			//Get the connection
			JCoConnection cn = getDefaultJCoConnection();
						
			//Get function module			
			JCO.Function function = cn.getJCoFunction("CRM_ESRV_CAMPAIGN_CHECK_BP_TG");

			//Importing parameters
			JCO.ParameterList importParams = function.getImportParameterList();
			JCoHelper.setValue(importParams, refDocId, "IV_CAMPAIGN_EXT_ID");
			JCoHelper.setValue(importParams, soldTo, "IV_BUPA_ID");
			
			// Executes the function module
			cn.execute(function);

			// Get the possible error messages of soldTo is not in any target group of the campaign
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
			if (!messages.isEmpty()){
				Message msg = new Message(Message.ERROR);
				msg.setDescription(messages.getString("MESSAGE"));
				campaign.addMessage(msg);
			}												
		}
		catch (JCO.Exception ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_BUSINESS_LOGIC, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);					
		}
		finally {
			log.exiting();
		}	
	
	
	}


	/**
	  * Read campaign header data from the backend. 
	  *
	  * @param campaign The document to read the data in
	  */
	public void getCampaignHeader(CampaignData campaign) throws BackendException {

		JCoConnection aJCoCon = getAvailableStatelessConnection();

		final String METHOD_NAME = "getCampaignHeader(campaign)";
		boolean headerCreated = false;

		log.entering(METHOD_NAME);

		try {
			// call the function module "CRMT_ISA_CAMPAIGN_INFO_GET"
			JCO.Function function = aJCoCon.getJCoFunction("CRM_ISA_CAMPAIGN_INFO_GET");

			// setting the import table 
			JCO.Table campaignTab = function.getTableParameterList().getTable("ET_CAMPAIGN_INFO");

			CampaignHeaderData headerData = campaign.getHeaderData();

			campaignTab.appendRow();

			if (headerData != null){
				if (headerData.getCampaignID() != null && headerData.getCampaignID().length() > 0)
					JCoHelper.setValue(campaignTab, (String)headerData.getCampaignID().toUpperCase(), "CAMPAIGN_ID");	
			}
			else{
				headerData = campaign.createHeader();
				headerCreated = true;				
			}
			
			boolean campaignGuidIsSet = false;
			headerData.clearMessages();
			if (headerData.getTechKey().getIdAsString() != null && headerData.getTechKey().getIdAsString().length() > 0){
				JCoHelper.setValue(campaignTab, headerData.getTechKey().getIdAsString(), "CAMPAIGN_GUID");
				campaignGuidIsSet = true;
			}
			
			if (!campaignGuidIsSet){
				if (campaign.getTechKey().getIdAsString() != null && campaign.getTechKey().getIdAsString().length() > 0)
					JCoHelper.setValue(campaignTab, campaign.getTechKey().getIdAsString(), "CAMPAIGN_GUID");						
			}
			
			// call the function
			aJCoCon.execute(function);
			
			// couldn't we read the campaign infos due to missing ACE rights?
			boolean aceRightMissing = false;
			
			//  get the output message table
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

			if (messages.getNumRows() > 0) {
				// only one message should be there, in case the necessary rights to 
				// read campaign information are missing
				MessageCRM messageCRM = MessageCRM.create(messages);

				log.warn(LogUtil.APPS_COMMON_CONFIGURATION, messageCRM.getDescription());
				
				aceRightMissing = true;
			}

			// read results
			campaignTab.setRow(0);

			headerData.setCampaignID(campaignTab.getString("CAMPAIGN_ID"));
			headerData.setTechKey(new TechKey(campaignTab.getString("CAMPAIGN_GUID")));
			headerData.setDescription(campaignTab.getString("CAMPAIGN_DESC"));
			headerData.setType(campaignTab.getString("CAMPAIGN_TYPE"));
			headerData.setTypeDescription(campaignTab.getString("CAMPAIGN_TYPE_DESC"));
			headerData.setRefType("REF_TYPE");

			String privateStr = campaignTab.getString("CAMPAIGN_PRIVATE");
			if (privateStr == null || privateStr.length() == 0)
				headerData.setPublic(true);
			else
				headerData.setPublic(false);

			String isEnteredManually = campaignTab.getString("CAMPAIGN_ENTERED_MANUALLY");
			if (isEnteredManually == null || isEnteredManually.length() == 0)
				headerData.setEnteredManually(false);
			else
				headerData.setEnteredManually(true);

			String invalidStr = campaignTab.getString("CAMPAIGN_INVALID");
			if ((invalidStr == null || invalidStr.length() == 0) && !aceRightMissing)
				headerData.setValid(true);
			else
			{
				headerData.setValid(false);
				addCampaignMessage(campaign, false);
			}
			if (headerCreated){
				campaign.setHeader(headerData);
			}

			if (log.isDebugEnabled()) {
				log.debug(
					"CampaignHeaderInfo: Id="
						+ headerData.getCampaignID()
						+ " Guid= "
						+ headerData.getTechKey().getIdAsString()
						+ "              Desc="
						+ headerData.getDescription()
						+ " Type="
						+ headerData.getType()
						+ " Entered Manually="
						+ headerData.isEnteredManually()
						+ "              Public="
						+ headerData.isPublic()
						+ " Valid="
						+ headerData.isValid());
						
			}
		}
		catch (JCO.Exception ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);
		}
		finally {
			// Close the JCo connection
			aJCoCon.close();

			log.exiting();
		}
	}

	/**
	 * Check eligibility of a sold-to for private campaigns.
	 * @param campaign 		The document to read the data in
	 * @param soldToGuid 	Sold-to Guid
	 * @param salesOrg		Sales Organisation
	 * @param distChannel   Distribution Channel 
	 * @param division      Division
	 * @throws BackendException
	 */
	public void checkPrivateCampaignEligibility(CampaignData campaign, TechKey soldToGuid, String salesOrg, String distChannel, String division, Date campaignDate)
		throws BackendException {

		final String METHOD_NAME = "checkPrivateCampaignEligibility()";

		log.entering(METHOD_NAME);

		if (salesOrg == null || "".equals(salesOrg) || distChannel == null || "".equals(distChannel) || soldToGuid.isInitial()) {
			if (log.isDebugEnabled()) {
				log.debug("SalesOrg or DistributionChannel or Division or Sold-to not set. Cannot execute eligibilty check.");
				log.debug("SalesOrg = " + salesOrg + " DistributionChannel = " + distChannel);
			}
			return;
		}

		JCoConnection aJCoCon = getDefaultJCoConnection();

		try {
			// call the function module "CRM_ISA_CAMP_ELIGIBILITY_CHECK"
			JCO.Function function = aJCoCon.getJCoFunction("CRM_ISA_CAMP_ELIGIBILITY_CHECK");

			// setting the import table basket_item
			JCO.Table campaignElTab = function.getTableParameterList().getTable("ET_CAMPAIGN_ELIGIBILITY_CHECK");
			CampaignHeaderData headerData = campaign.getHeaderData();

			campaignElTab.appendRow();
			JCoHelper.setValue(campaignElTab, headerData.getTechKey().getIdAsString(), "CAMPAIGN_GUID");

			DateFormat targetDateFormat = new SimpleDateFormat("yyyyMMdd");
			String targetDate = targetDateFormat.format(campaignDate);
			JCoHelper.setValue(campaignElTab, targetDate, "CAMPAIGN_DATE");

			JCoHelper.setValue(campaignElTab, salesOrg, "SALES_ORG");
			JCoHelper.setValue(campaignElTab, distChannel, "DIST_CHANNEL");
			JCoHelper.setValue(campaignElTab, division, "DIVISION");

			if (!soldToGuid.isInitial()) {
				JCoHelper.setValue(campaignElTab, soldToGuid.getIdAsString(), "SOLD_TO");
			}

			// call the function
			aJCoCon.execute(function);

			//	read results
			campaignElTab.setRow(0);
			headerData.setMessage(campaignElTab.getString("MESSAGE"));
			String invalidStr = campaignElTab.getString("CAMPAIGN_INVALID");
			if (invalidStr != null && invalidStr.length() > 0) {
				headerData.setValid(false);
				addCampaignMessage(campaign, true);
			}
			else {
				headerData.setValid(true);
			}

			//headerData.setMessage(campaignElTab.getString("MESSAGE"));

			if (log.isDebugEnabled()) {
				log.debug("EligibilityCheck for refGuid: " + campaign.getTechKey().getIdAsString() + " = " + headerData.isValid());
			}

			//  get the output message table
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");

			if (messages.getNumRows() > 0) {
				// only one message should be there, in case the necessary rights to 
				// read campaign information are missing
				MessageCRM messageCRM = MessageCRM.create(messages);

				log.warn(LogUtil.APPS_COMMON_CONFIGURATION, messageCRM.getDescription());
			}

		}
		catch (JCO.Exception ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_BUSINESS_LOGIC, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);
		}
		finally {
			// Close the JCo connection
			aJCoCon.close();
			
			log.exiting();
		}
	}

	/**
	 * Add Campaign messages
	 * @param campaign  Campaign
	 * @param isSoldMsg Indicates if the message is for Sold-To eligibility
	 */
	protected void addCampaignMessage(CampaignData campaign, boolean isSoldMsg){
		
		Message msg = null;
		if (campaign != null)
		{
			CampaignHeaderData headerData = campaign.getHeaderData();
			if (!headerData.isValid()){
				if (isSoldMsg){
					//Sold-to not eligible for the campaign
					msg = new Message(Message.WARNING, "catalog.isa.camp.ineligible", new String[] {headerData.getCampaignID().toUpperCase()}, "");
				}
				else
				{
					//Campaign is invalid
					msg = new Message(Message.WARNING, "catalog.isa.camp.invalid", new String[] {headerData.getCampaignID().toUpperCase()}, ""); 
				}
			}
			MessageList messages = headerData.getMessageList();
			if (msg != null) {
			messages.add(msg); 
			log.debug("Camp message Type: " + msg.getType() + 
					  " Desc: " + msg.getDescription() + 
					  " Key: " + msg.getResourceKey()); 
			}
		}
	}

	/**
	  * retrieve date ranges
	  *
	  * @param campaign campaign object
	  * @param function JCO objects
	  */
	protected void retrieveDateRanges(CampaignData campaign, JCO.Function function) throws BackendException {
		final String METHOD_NAME = "retrieveDateRanges(campaign, function)";

		log.entering(METHOD_NAME);

		try {
			// get the date range list reference 
			DateRangeListData dateRangeList = campaign.getDateRangeListData();

			// get date ranges
			JCO.Table dateRangesBE = function.getTableParameterList().getTable("ET_DATE_RANGES");

			int numDateRanges = dateRangesBE.getNumRows();

			for (int i = 0; i < numDateRanges; i++) {
				dateRangesBE.setRow(i);

				DateRangeData dateRange = dateRangeList.createDateRange();
				dateRange.setDateID(JCoHelper.getString(dateRangesBE, "DATE_ID"));
				dateRange.setPeriodType(JCoHelper.getString(dateRangesBE, "PERIOD_TYPE"));
				dateRange.setStartDate(JCoHelper.getString(dateRangesBE, "DATE_FROM"));
				dateRange.setEndDate(JCoHelper.getString(dateRangesBE, "DATE_TO"));
				dateRange.setDateRangeType(JCoHelper.getString(dateRangesBE, "DATE_TYPE"));
				dateRangeList.addDateRange(dateRange);
			}
		}
		finally {
			log.exiting();
		}
	}

	/**
	  * retrieve products
	  *
	  * @param campaign campaign object
	  * @param function JCO objects
	  */
	protected void retrieveProducts(CampaignData campaign, JCO.Function function) throws BackendException {
		final String METHOD_NAME = "retrieveProducts(campaign, function)";

		log.entering(METHOD_NAME);

		try {
			// product list will be filled with returned values
			JCO.Table productsBE = function.getTableParameterList().getTable("ET_PRODUCTS");
			int numProducts = productsBE.getNumRows();

			ProductListData productList = campaign.getProductListData();

			for (int i = 0; i < numProducts; i++) {
				productsBE.setRow(i);
				CampaignProductItemData product =
					productList.createProduct(new TechKey(JCoHelper.getString(productsBE, "PRODUCT_GUID")), JCoHelper.getString(productsBE, "PRODUCT_ID"));
				product.setDescription(JCoHelper.getString(productsBE, "PROD_SHORT_TEXT"));
				product.setAmount(JCoHelper.getString(productsBE, "QUANTITY"));
				product.setAmountUom(JCoHelper.getString(productsBE, "SALES_UOM"));
				productList.addProduct(product);
			}
		}
		finally {
			log.exiting();
		}
	}
    
    /**
      * Read the campaign with the enrollment data from the backend. 
      *
      * @param campaign     The document to read the data in
      */
    public void readCampaignWithEnrollment(CampaignData campaign) 
                throws BackendException {

        final String METHOD = "readCampaignWithEnrollment()";

        log.entering(METHOD);

        JCoConnection aJCoCon = getDefaultJCoConnection();

        // call the function module "CRM_ECO_ENROLLMENT_DATA_GET"
        JCO.Function function = aJCoCon.getJCoFunction("CRM_ECO_ENROLLMENT_DATA_GET");
        
        try {
            JCoHelper.RecordWrapper importParam = new JCoHelper.RecordWrapper(function.getImportParameterList());

            CampaignSearchCriteriaData searchData = campaign.getSearchCriteriaData();

            String enrollerGuid = searchData.getEnrollerGuid();
            String enrolleeGuid = searchData.getEnrolleeGuid();
               
            if (log.isDebugEnabled()) {
                String campId = "";
                if (searchData != null) {
                    campId = searchData.getCampaignID();
                }
                log.debug(METHOD + ": campaignId=" + campId + " / searchBpHier=" + searchData.getSearchForBpHier() 
                          + " / enrollerGuid=" + enrollerGuid + " / enrolleeGuid=" + enrolleeGuid
                          + " / textType=" + searchData.getTextTypeCampDescr());
            }

            // add import parameter 
            importParam.setValue(searchData.getCampaignID(), "IV_PROJECT_ID");
            importParam.setValue(enrollerGuid, "IV_ENROLLER_BP_GUID");
            importParam.setValue(searchData.getTextTypeCampDescr(), "IV_TEXT_ID");
            importParam.setValue(searchData.getCountry(), "IV_COUNTRY");
            if (searchData.getSearchForBpHier()) { 
                importParam.setValue("X", "IV_SEARCH_FOR_BP_HIERARCHY");
            }
            else {
                importParam.setValue(" ", "IV_SEARCH_FOR_BP_HIERARCHY");
                importParam.setValue(enrolleeGuid, "IV_ENROLLEE_BP_GUID");
            }

            // log the import parameter
            if (log.isDebugEnabled()) {
                JCoHelper.logCall(function.getName(), importParam, null, log);
            }

            // call the back end function
            aJCoCon.execute(function);

            // getting export parameter
            JCO.ParameterList exportParam = function.getExportParameterList();

            // log the export parameter
            if (log.isDebugEnabled()) {
                JCoHelper.logCall(function.getName(), null, exportParam, log);
            }
            
            // messages information
            JCO.Table messages = exportParam.getTable("ET_MESSAGES");

            // system error in backend
            String systemErrorInBackend = exportParam.getString("EV_APPLICATION_ERROR");

            boolean campaignExists = true;
            
            // check messages
            int numEntries = messages.getNumRows();
            String number = null;
            for (int i = 0; i < numEntries; i++) {
                number = messages.getString("NUMBER");
                if (number != null && number.equals("103")) {
                    campaignExists = false;
                }
            }
            
            if (systemErrorInBackend.equals("X")) {
                campaignExists = false;
            }

            if (campaignExists) {
                // campaign header information
                CampaignHeaderData campHeaderData = campaign.getHeaderData();
                if (campHeaderData == null) {
                    campHeaderData = campaign.createHeader();
                    if (log.isDebugEnabled()) {
                        log.debug(METHOD + ": campaign header was created");
                    }
                }
                // campaign header information
                JCO.Structure structCampDetail = exportParam.getStructure("ES_PROJECT_DETAILS");
                campHeaderData.setCampaignID(structCampDetail.getString("PROJECT_ID"));
                campHeaderData.setTechKey(new TechKey(structCampDetail.getString("PROJECT_GUID")));
                campHeaderData.setDescription(structCampDetail.getString("PROJECT_TEXT_SHORT"));
                campHeaderData.setCampaignText(structCampDetail.getString("PROJECT_TEXT_LONG"));
                campHeaderData.setType(structCampDetail.getString("PROJECT_TYPE"));
                campHeaderData.setTypeDescription(structCampDetail.getString("PROJECT_TYPE_TEXT"));
                campHeaderData.setEnrollmStartDate(structCampDetail.getString("PROJECT_ENROLLMENT_START"));
                campHeaderData.setEnrollmEndDate(structCampDetail.getString("PROJECT_ENROLLMENT_END"));
                campHeaderData.setPlannedStartDate(structCampDetail.getString("PROJECT_PLANNED_START"));
                campHeaderData.setPlannedEndDate(structCampDetail.getString("PROJECT_PLANNED_END"));
                campHeaderData.setEnrollmPeriodStatus(structCampDetail.getString("ENROLLMENT_PERIOD_STATUS"));
                if (structCampDetail.getString("PROJECT_ENROLLMENT_REQ").equals("X")) {
                    campHeaderData.setIsEnrollmAllowed(true);
                }
                else {
                    campHeaderData.setIsEnrollmAllowed(false);
                }
                if (!campHeaderData.getCampaignID().equals("")) {
                    campHeaderData.setValid(true);
                }
                // store campaign header in campaign
                campaign.setHeader(campHeaderData);
     
                // campaign enrollment information
                JCO.Table enrollmJCOTable = exportParam.getTable("ET_ENROLLMENT_DATA");
                numEntries = enrollmJCOTable.getNumRows();
                
                if (numEntries > 0) {
                    CampaignEnrollmentListData enrollmListData = campaign.getCampaignEnrollmentListData();
                    
                    for (int i = 0; i < numEntries; i++) {
                        CampaignEnrollmentData enrollmData = enrollmListData.createEnrollment();
                        enrollmData.setEnrolleeGuid(enrollmJCOTable.getString("ENROLLEE_BP_GUID"));
                        enrollmData.setEnrolleeID(enrollmJCOTable.getString("ENROLLEE_BP_ID"));
                        enrollmData.setEnrolleeDescription(enrollmJCOTable.getString("ENROLLEE_BP_DESCR"));
                        enrollmData.setIsEnrolled(enrollmJCOTable.getString("ENROLLMENT_STATUS").equals("X"));
                        enrollmData.setFirstEnrolledDate(enrollmJCOTable.getString("FIRST_ENROLLED_ON_EXT"));
                        enrollmData.setFirstEnrolledTimestamp(enrollmJCOTable.getString("FIRST_ENROLLED_ON_INT"));
                        enrollmData.setFirstEnrolledByContact(enrollmJCOTable.getString("FIRST_ENROLLED_BY_CTCT_DESCR"));
                        enrollmData.setFirstEnrolledBySoldTo(enrollmJCOTable.getString("FIRST_ENROLLED_BY_BP_DESCR"));
                        enrollmData.setIsFirstEnrolledInternally(enrollmJCOTable.getString("FIRST_ENROLLED_INTERNALLY").equals("X"));
                        enrollmData.setLastChangedDate(enrollmJCOTable.getString("LAST_CHANGED_ON_EXT"));
                        enrollmData.setLastChangedTimestamp(enrollmJCOTable.getString("LAST_CHANGED_ON_INT"));
                        enrollmData.setLastChangedByContact(enrollmJCOTable.getString("LAST_CHANGED_BY_CTCT_DESCR"));
                        enrollmData.setLastChangedBySoldTo(enrollmJCOTable.getString("LAST_CHANGED_BY_BP_DESCR"));
                        enrollmData.setIsLastChangedInternally(enrollmJCOTable.getString("LAST_CHANGED_INTERNALLY").equals("X"));
     
                        enrollmListData.addEnrollment(enrollmData);
                        enrollmJCOTable.nextRow();
                    }
                }
            }
            
        }
        catch (JCO.Exception ex) {
            log.error("The following error occured: ", ex);
        }
        finally {
            // close JCO connection             
            if (aJCoCon != null) {
                aJCoCon.close();
            } 
        }
        
        log.exiting();
    }

    /**
      * Store the enrollment data for the campaign on the backend. 
      *
      * @param campaign The document with the enrollment data to be stored.
      * @return errorFlag as boolean, if error appears by backend processing
      */
    public boolean enrollBpForCampaign(CampaignData campaign) 
                throws BackendException {
        final String METHOD = "enrollBpForCampaign()";
        log.entering(METHOD);

        boolean errorFlag = false; 
        JCoConnection aJCoCon = getDefaultJCoConnection();

        // call the function module "CRM_ECO_ENROLLMENT_DATA_SET"
        JCO.Function function = aJCoCon.getJCoFunction("CRM_ECO_ENROLLMENT_DATA_SET");

        try {
            CampaignHeaderData campHeaderData = campaign.getHeaderData();
            CampaignEnrollmentListData enrollmListData = campaign.getCampaignEnrollmentListData();

            // getting export parameter
            JCO.ParameterList importParam = function.getImportParameterList();
            // log the import parameter
            if (log.isDebugEnabled()) {
                JCoHelper.logCall(function.getName(), importParam, null, log);
            }

            // add import parameter 
            if (campHeaderData != null) {
                importParam.setValue(campHeaderData.getTechKey().getIdAsString(), "IV_PROJECT_GUID");
                importParam.setValue(campHeaderData.getEnrollerGuid(), "IV_ENROLLER_BP_GUID");
                // log the import parameter values
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": IV_PROJECT_GUID=" + campHeaderData.getTechKey().getIdAsString()  
                              + " / IV_ENROLLER_BP_GUID=" + campHeaderData.getEnrollerGuid());
                }
            }

            // campaign enrollment information
            JCO.Table enrollmJCOTable = importParam.getTable("IT_ENROLLMENT_DATA");
            
            int numEntries = enrollmListData.size();
            for (int i = 0; i < numEntries; i++) {
                if (enrollmListData.getData(i).isChanged()) {
                    // add the enrollment line
                    enrollmJCOTable.appendRow();
                    
                    JCoHelper.setValue(enrollmJCOTable, enrollmListData.getData(i).getEnrolleeGuid(), "ENROLLEE_BP_GUID");
                    
                    String enrollStatus = ""; 
                    if (enrollmListData.getData(i).isEnrolled()) {
                        enrollStatus = "X";
                    }
                    JCoHelper.setValue(enrollmJCOTable, enrollStatus, "ENROLLMENT_STATUS");
                    
                    if (log.isDebugEnabled()) {
                        log.debug(METHOD + ": Row[" + i + "]: ENROLLEE_BP_GUID=" + enrollmListData.getData(i).getEnrolleeGuid()  
                                  + " / ENROLLMENT_STATUS=" + enrollStatus);
                    }
                }
            }
            importParam.setValue(enrollmJCOTable, "IT_ENROLLMENT_DATA");

            // call the back end function
            aJCoCon.execute(function);

            // getting export parameter
            JCO.ParameterList exportParam = function.getExportParameterList();

            // log the export parameter
            if (log.isDebugEnabled()) {
                JCoHelper.logCall(function.getName(), null, exportParam, log);
            }

            // campaign header information
            String applError = exportParam.getString("EV_APPLICATION_ERROR");
            
            // messages information
            JCO.Table messages = exportParam.getTable("ET_MESSAGES");
            if (applError.equals("X") || !messages.isEmpty()) {
                errorFlag = true;
            }
        }
        catch (JCO.Exception ex1) {
            log.error("The following error occured: ", ex1);
            errorFlag = true;
        }
        catch (BackendException ex2) {
            log.error("The following error occured: ", ex2);
            errorFlag = true;
        }
        finally {
            // close JCO connection             
            if (aJCoCon != null) {
                aJCoCon.close();
            } 
        }
        log.exiting();
        return (errorFlag);
    }

}
