/*****************************************************************************
	Class         LoyaltyCampaignCRM.java
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP
*****************************************************************************/
package com.sap.ecommerce.backend.crm.loyalty;

 

import com.sap.ecommerce.backend.crm.campaign.CampaignCRM;
import com.sap.ecommerce.backend.intf.campaign.CampaignBackend;
import com.sap.ecommerce.boi.campaign.CampaignData;
import com.sap.ecommerce.boi.campaign.CampaignHeaderData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.mw.jco.JCO;

/**
 *
 * CampaignCRM reads campaign data from the CRM backend
 *
 * @author  SAP AG
 * @version 1.0
 *
 */
public class LoyaltyCampaignCRM extends CampaignCRM implements CampaignBackend {

	private final static IsaLocation log = IsaLocation.getInstance(LoyaltyCampaignCRM.class.getName());


	/**
	  * Read campaign header data from the backend. 
	  *
	  * @param campaign The document to read the data in
	  */
	public void getCampaignHeader(CampaignData campaign) throws BackendException {

		JCoConnection aJCoCon = getDefaultJCoConnection();

		final String METHOD_NAME = "getCampaignHeader(campaign)";
		boolean headerCreated = false;

		log.entering(METHOD_NAME);

		try {
			// call the function module "CRM_ISA_LOYALTY_CAMPAIGN_GET"
			JCO.Function function = aJCoCon.getJCoFunction("CRM_ISA_LOYALTY_CAMPAIGN_GET");

//			set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			 
			CampaignHeaderData headerData = campaign.getHeaderData();

			if (headerData != null){
				if (headerData.getCampaignID() != null && headerData.getCampaignID().length() > 0)
					importParams.setValue(campaign.getHeaderData().getCampaignID(), "LOYALTY_CAMPAIGN_ID");
			}
			else{
				headerData = campaign.createHeader();
				headerCreated = true;				
			}
			
			boolean campaignGuidIsSet = false;
			headerData.clearMessages();
			if (headerData.getTechKey().getIdAsString() != null && headerData.getTechKey().getIdAsString().length() > 0){
				importParams.setValue(headerData.getTechKey().getIdAsString(), "LOYALTY_CAMPAIGN_GUID");
				campaignGuidIsSet = true;
			}
			
			if (!campaignGuidIsSet){
				if (campaign.getTechKey().getIdAsString() != null && campaign.getTechKey().getIdAsString().length() > 0)
					importParams.setValue(campaign.getTechKey().getIdAsString(), "LOYALTY_CAMPAIGN_GUID");						
			}
			
			// call the function
			aJCoCon.execute(function);
						
			//  get the output message table
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");

			if (messages.getNumRows() > 0) {
				// only one message should be there, in case the necessary rights to 
				// read campaign information are missing
				MessageCRM messageCRM = MessageCRM.create(messages);

				log.warn(LogUtil.APPS_COMMON_CONFIGURATION, messageCRM.getDescription());
								 
			}

			// read results
			JCO.ParameterList exportParams = function.getExportParameterList();
			JCO.Structure campaignData = exportParams.getStructure("LOYALTY_CAMPAIGN_INFO");
			headerData.setCampaignID(campaignData.getString("CAMPAIGN_ID"));
			headerData.setTechKey(new TechKey(campaignData.getString("CAMPAIGN_GUID")));
			headerData.setDescription(campaignData.getString("CAMPAIGN_DESC"));
			headerData.setType(campaignData.getString("CAMPAIGN_TYPE"));
			headerData.setTypeDescription(campaignData.getString("CAMPAIGN_TYPE_DESC"));
			headerData.setRefType("REF_TYPE");

			String privateStr = campaignData.getString("CAMPAIGN_PRIVATE");
			if (privateStr == null || privateStr.length() == 0)
				headerData.setPublic(true);
			else
				headerData.setPublic(false);

			String isEnteredManually = campaignData.getString("CAMPAIGN_ENTERED_MANUALLY");
			if (isEnteredManually == null || isEnteredManually.length() == 0)
				headerData.setEnteredManually(false);
			else
				headerData.setEnteredManually(true);

			String invalidStr = campaignData.getString("CAMPAIGN_INVALID");
			if ((invalidStr == null || invalidStr.length() == 0))
				headerData.setValid(true);
			else
			{
				headerData.setValid(false);
				addCampaignMessage(campaign, false);
			}
			if (headerCreated){
				campaign.setHeader(headerData);
			}

			if (log.isDebugEnabled()) {
				log.debug(
					"CampaignHeaderInfo: Id="
						+ headerData.getCampaignID()
						+ " Guid= "
						+ headerData.getTechKey().getIdAsString()
						+ "              Desc="
						+ headerData.getDescription()
						+ " Type="
						+ headerData.getType()
						+ " Entered Manually="
						+ headerData.isEnteredManually()
						+ "              Public="
						+ headerData.isPublic()
						+ " Valid="
						+ headerData.isValid());
						
			}
		}
		catch (JCO.Exception ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);
		}
		finally {
			// Close the JCo connection
			aJCoCon.close();

			log.exiting();
		}
	}

 

}
