package com.sap.ecommerce.backend.crm.loyalty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sap.ecommerce.backend.crm.campaign.CampaignCRM;
import com.sap.ecommerce.backend.intf.loyalty.LoyaltyMembershipBackend;
import com.sap.ecommerce.boi.campaign.MarketingCampaignListData;
import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipData;
import com.sap.ecommerce.boi.loyalty.LoyaltyPointAccountData;
import com.sap.ecommerce.boi.loyalty.LoyaltyTransactionListData;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignHeader;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyPointAccount;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyTransaction;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

public class LoyaltyMembershipCRM
	extends IsaBackendBusinessObjectBaseSAP
	implements LoyaltyMembershipBackend {

	private final static IsaLocation log =
		IsaLocation.getInstance(CampaignCRM.class.getName());

	protected static final String READ_TRANSACTION_FUNCTION =
		"CRM_LOY_PTAC_TRANSACTION_READ";

	protected static final String READ_LOY_POINT_ACCOUNT_FUNCTION =
		"CRM_MKTPL_LOY_POINT_IN_PRG_RFC";

	protected static final String READ_LOY_POINT_ACCOUNT_BALANCE_FUNCTION =
		"CRM_LOY_PTAC_BALANCE_READ";

	protected static final String READ_LOY_CAMPAIGN_FUNCTION =
		"CRM_MKTPL_LOY_CPG_GET";

	protected static final String ENROLL_LOY_CAMPAIGN_FUNCTION =
		"CRM_MKTPL_ENROLMEMBER_API";

	/**
	 * read the loyalty transactions data from the back end
	 *
	 * @param loyaltyTransactionlist document to fill the data in
	 * @param fromDate the transaction From Date
	 * @param toDate the transaction To Date
	 *
	 */
	public void readLoyaltyTransactionsFromBackend(
		String membershipGuid,
		String pointAccountGuid,
		DateFormat dateFormat,
		Date fromDate,
		Date toDate,
		LoyaltyTransactionListData loyaltyTransactionList)
		throws BackendException {
		final String METHOD_NAME = "readLoyaltyTransactionsFromBackend(...)";

		log.entering(METHOD_NAME);

		// JCO init
		JCoConnection aJCoCon = getDefaultJCoConnection();

		JCO.Function function =
			aJCoCon.getJCoFunction(
				LoyaltyMembershipCRM.READ_TRANSACTION_FUNCTION);

		try {
			JCoHelper.RecordWrapper importParam =
				new JCoHelper.RecordWrapper(function.getImportParameterList());

			importParam.setValue(membershipGuid, "IV_MEMBERSHIP_ID");
			importParam.setValue(pointAccountGuid, "IV_POINT_ACCOUNT_GUID");

			//we should call the backend function module with an internal date format 20080404
			DateFormat internalFormat = new SimpleDateFormat("yyyyMMdd");
			String internalFromDate = internalFormat.format(fromDate);
			String internalToDate = internalFormat.format(toDate);

			importParam.setValue(internalFromDate, "IV_FROM_DATE");
			importParam.setValue(internalToDate, "IV_TO_DATE");

			// log the import parameter
			if (log.isDebugEnabled()) {
				JCoHelper.logCall(
					LoyaltyMembershipCRM.READ_TRANSACTION_FUNCTION,
					importParam,
					null,
					log);
			}

			// call the back end function
			aJCoCon.execute(function);

			// getting export parameter
			JCO.ParameterList exportParam = function.getExportParameterList();

			JCO.Table et_transaction = exportParam.getTable("ET_TRANSACTIONS");

			LoyaltyTransaction loyaltyTransaction = null;

			int numTransaction = et_transaction.getNumRows();

            //output format: YYYYMMDDhhmmss
            DateFormat outputFormat = new SimpleDateFormat("yyyyMMddHHmmss");

			String txn_type_desc;
			String qual_type_desc;
			String txn_reason_desc;
			
			for (int i = 0; i < numTransaction; i++) {
				et_transaction.setRow(i);

				txn_type_desc = null;
				qual_type_desc = null;
				txn_reason_desc = null;

				loyaltyTransaction = new LoyaltyTransaction();

				loyaltyTransaction.setTransactionId(
					JCoHelper.getString(et_transaction, "GUID"));

			    txn_type_desc = JCoHelper.getString(et_transaction, "TXN_TYPE_DESC");
			    if (txn_type_desc != null && txn_type_desc.length() > 0) {
					loyaltyTransaction.setTransactionType(txn_type_desc);
			    }
			    else {
				loyaltyTransaction.setTransactionType(
					JCoHelper.getString(et_transaction, "TXN_TYPE"));
			    }

				//we need to convert the expiry date in the web shop format.				
				loyaltyTransaction.setExpirationDate(
					convertInternalDateFormatToShopFormat(
						dateFormat,
						JCoHelper.getDate(et_transaction, "EXPIRY_DATE")));
				loyaltyTransaction.setMiles(
					JCoHelper.getString(et_transaction, "POINTS"));
				loyaltyTransaction.setPostingDate(
					convertInternalDateFormatToShopFormat(
                      dateFormat,
						JCoHelper.getDate(et_transaction, "POSTING_DATE_ALT")));
				loyaltyTransaction.setQualifyingMiles(
					JCoHelper.getString(et_transaction, "QUAL_POINTS"));

				qual_type_desc = JCoHelper.getString(et_transaction, "QUAL_TYPE_DESC");
				if (qual_type_desc != null && qual_type_desc.length() > 0) {
					loyaltyTransaction.setQualificationType(qual_type_desc);
				}
				else {
				loyaltyTransaction.setQualificationType(
					JCoHelper.getString(et_transaction, "QUAL_TYPE"));
				}

				txn_reason_desc = JCoHelper.getString(et_transaction, "TXN_REASON_DESC");
				if (txn_reason_desc != null && txn_reason_desc.length() > 0) {
					loyaltyTransaction.setReason(txn_reason_desc);
				}
				else {
				loyaltyTransaction.setReason(
					JCoHelper.getString(et_transaction, "TXN_REASON"));
				}

				loyaltyTransactionList.addLoyaltyTransaction(
					loyaltyTransaction);
			}

			// log the export parameter
			if (log.isDebugEnabled()) {
				JCoHelper.logCall(
					LoyaltyMembershipCRM.READ_TRANSACTION_FUNCTION,
					null,
					exportParam,
					log);
			}

			JCO.Table messages = exportParam.getTable("ET_MESSAGE");

			if (messages != null) {
				MessageCRM.splitMessagesToTarget(
					loyaltyTransactionList,
					messages);
			}

		} catch (JCO.Exception ex) {
			log.error("The following error occured: ", ex);

		} finally {
			// close JCO connection 			
			aJCoCon.close();

			log.exiting();
		}

		log.exiting();
	}

	private String convertInternalDateFormatToShopFormat(
		DateFormat dateFormat,
		Date dateToConvert) {
		log.entering("convertInternalDateFormatToShopFormat");
		String returnValue = new String();

		try {
			if (dateToConvert != null) {
				returnValue = dateFormat.format(dateToConvert);
			}
		} catch (Exception e) {
			log.debug(
				"Exception occurs in convertInternalDateFormatToShopFormat:"
					+ e.getMessage());
		}

		log.exiting();
		return returnValue;
	}

	/**
	 * read the loyalty campaigns data from the back end
	 *
	 * @param campaignList document to fill the data in
	 *
	 */
	public void readLoyaltyCampaignsFromBackend(
		String membershipId,
		String memberGuid,
		String campaignTextId,
		DateFormat dateFormat,
		MarketingCampaignListData campaignList)
		throws BackendException {
		final String METHOD_NAME =
			"readLoyaltyCampaignsFromBackend(CampaignListData campaignList)";

		//actually the backend function doesn't exist, that's why we fill it with dummy data.
		Campaign campaign = new Campaign();
		CampaignHeader campaignHeader = new CampaignHeader();

		log.entering(METHOD_NAME);

		// JCO init
		JCoConnection aJCoCon = getDefaultJCoConnection();

		JCO.Function function =
			aJCoCon.getJCoFunction(
				LoyaltyMembershipCRM.READ_LOY_CAMPAIGN_FUNCTION);

		try {
			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();
			importParams.setValue(membershipId, "IV_MEMBERSHIP_ID");
			importParams.setValue(memberGuid, "IV_MEMBER_GUID");
			importParams.setValue(campaignTextId, "IV_TEXT_ID");

			// call the function
			aJCoCon.execute(function);

			//get the returned values
			JCO.ParameterList exportParams = function.getExportParameterList();

			JCO.Table et_campaigns = exportParams.getTable("ET_CAMPAIGN");

			//output format: YYYYMMDDhhmmss
			DateFormat outputFormat = new SimpleDateFormat("yyyyMMddHHmmss");

			for (int i = 0; i < et_campaigns.getNumRows(); i++) {
				et_campaigns.setRow(i);
				campaign = new Campaign();
				campaignHeader = new CampaignHeader();
				campaignHeader.setCampaignID(
					JCoHelper.getString(et_campaigns, "EXTERNAL_ID"));
				campaignHeader.setTechKey(
					new TechKey(JCoHelper.getString(et_campaigns, "GUID")));
				campaignHeader.setDescription(
					JCoHelper.getString(et_campaigns, "TEXT1"));
				String longDescription =
					JCoHelper.getString(et_campaigns, "PROJECT_TEXT_LONG");
				if (longDescription == null) {
					longDescription = new String();
				}
				campaignHeader.setCampaignText(longDescription);
				String s = JCoHelper.getString(et_campaigns, "PLANSTART");
				try {
					Date d = outputFormat.parse(s);
					//we need to convert the planstart Date 						
					campaignHeader.setPlannedStartDate(
						convertInternalDateFormatToShopFormat(dateFormat, d));
				} catch (Exception e) {
					log.debug("Error by converting Plannstart:" + e);
				}
				s = JCoHelper.getString(et_campaigns, "PLANFINISH");
				try {
					Date d = outputFormat.parse(s);
					campaignHeader.setPlannedEndDate(
						convertInternalDateFormatToShopFormat(dateFormat, d));
				} catch (Exception e) {
					log.debug("Error by converting Plannfinish:" + e);
				}
				s = JCoHelper.getString(et_campaigns, "CREATED_ON");
				if (!s.equals("")) {
					try {
						Date d = outputFormat.parse(s);
						campaignHeader.setEnrollmStartDate(
							convertInternalDateFormatToShopFormat(
								dateFormat,
								d));
					} catch (Exception e) {
						log.debug("Error by converting Created_on:" + e);
						campaignHeader.setEnrollmStartDate("");
					}
				} else {
					campaignHeader.setEnrollmStartDate("");
				}
				String enrollmentRequired =
					JCoHelper.getString(et_campaigns, "ENROLLMENT_REQ");
				String membershipEnrolled =
					JCoHelper.getString(et_campaigns, "ENROLLED");
				int status = CampaignHeader.REGISTRATION_NOT_REQUIRED;
				if (membershipEnrolled.equals("X")) {
					status = CampaignHeader.REGISTRATION_ALR_REGISTERED;
				} else {
					if (enrollmentRequired.equals("X")) {
						status = CampaignHeader.REGISTRATION_REQUIRED;
					}
				}
				campaignHeader.setRegistrationStatus(status);
				campaign.setHeader(campaignHeader);
				campaignList.addCampaign(campaign);
			}

			// get messages
			JCO.Table messages = exportParams.getTable("ET_ERRORS");
			if (messages != null) {
				MessageCRM.splitMessagesToTarget(campaignList, messages);
			}
		} catch (JCO.Exception ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error calling CRM function:" + ex);
			}
			JCoHelper.splitException(ex);
		} finally {
			log.exiting();
		}

		log.exiting();
	}

	/**
	 * Get the data of the loyalty membership
	 * 
	 * @param jco connection to use
	 * @param loyalty membership
	 * @param the user 
	 */
	public static void getMembership(
		JCoConnection jcoCon,
		LoyaltyMembershipData loyMembership,
		TechKey loyMember)
		throws BackendException {

		final String METHOD_NAME = "getLoyaltyMembership()";
		log.entering(METHOD_NAME);

		try {
			// get the repository infos of the function that we want to call
			JCO.Function function =
				jcoCon.getJCoFunction("CRM_ISA_MSH_READ_SINGLE");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(
				loyMembership.getLoyaltyProgram().getProgramId(),
				"IV_PROGRAM_ID");
			importParams.setValue(loyMember.getIdAsString(), "IV_MEMBER_GUID");

			// call the function
			jcoCon.execute(function);

			//get the returned values
			JCO.ParameterList exportParams = function.getExportParameterList();
			JCO.Table membershipData = exportParams.getTable("ET_MEMBERSHIP");

			if (!membershipData.isEmpty()) {
				membershipData.firstRow();
				if (membershipData.getString("GUID") != null
					&& membershipData.getString("GUID").length() > 0
					&& membershipData.getString("OBJECT_ID") != null
					&& membershipData.getString("OBJECT_ID").length() > 0) {
                    // fill membership data    
					loyMembership.setTechKey(
						new TechKey(membershipData.getString("GUID")));
					loyMembership.setMembershipId(
						membershipData.getString("OBJECT_ID"));
                    // fill loyalty program data    
                    loyMembership.getLoyaltyProgram().setTechKey(    
                        new TechKey(membershipData.getString("LOY_PROG_GUID")));
                        
                    // fill point account    
                    JCO.Table et_pointAccount = exportParams.getTable("ET_POINTTYPES");
                    
                    if (loyMembership.getPointAccountData() != null &&
                        loyMembership.getPointAccountData().getPointCodeId() != null) {
                        LoyaltyPointAccountData ptAccount = loyMembership.getPointAccountData();
                        String pointType = ptAccount.getPointCodeId();
                        boolean found = false;
                        for (int i = 0; !found && i < et_pointAccount.getNumRows(); i++) {
                            String ptCode = JCoHelper.getString(et_pointAccount, "POINT_CODE");
                            if (ptCode != null && ptCode.equals(pointType)) {
                                ptAccount.setPointCodeId(ptCode);
                                ptAccount.setDescr(JCoHelper.getString(et_pointAccount, "POINT_NAME"));
                                found = true;
                            }
                            et_pointAccount.nextRow();
                        }
                    }
				}
                
			}
		} catch (JCO.Exception ex) {
			if (log.isDebugEnabled())
				log.debug("Error calling CRM function:" + ex);
			JCoHelper.splitException(ex);
		} finally {
			log.exiting();
		}

	}

	/**
	 * create new loyalty membership
	 * 
	 * @param jco connection to use
	 * @param loyalty membership
	 * @param the user 
	 */
	public static void createMembership(
		JCoConnection jcoCon,
		LoyaltyMembershipData loyMembership,
		String loyMemberGUID,
		String loyMemberID)
		throws BackendException {

		final String METHOD_NAME = "createMembership()";
		log.entering(METHOD_NAME);

		loyMembership.clearMessages();

		try {
			// get the repository infos of the function that we want to call
			JCO.Function function =
				jcoCon.getJCoFunction("CRM_ISA_MSH_CREATE_SINGLE");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(
				loyMembership.getLoyaltyProgram().getProgramId(),
				"IV_PROGRAM_ID");
			importParams.setValue(
				loyMembership.getLoyaltyProgram().getTechKey().getIdAsString(),
				"IV_PROGRAM_GUID");
			if (loyMemberGUID != null)
				importParams.setValue(loyMemberGUID, 
				"IV_MEMBER_GUID");
			if (loyMemberID != null)
				importParams.setValue(loyMemberID, 
				"IV_MEMBER_ID");

			if (loyMembership.getLoyaltyPromotion() != null &&
				loyMembership.getLoyaltyPromotion().getHeader() != null &&	
				loyMembership.getLoyaltyPromotion().getHeader().getTechKey().toString() != null) {
				importParams.setValue(loyMembership.getLoyaltyPromotion().getHeader().getTechKey().toString(),
					                  "IV_CAMPAIGN_CODE");
			}

			// call the function
			jcoCon.execute(function);

			//get the returned values
			JCO.ParameterList exportParams = function.getExportParameterList();
			String loyMemberShipId = exportParams.getString("EV_MEMBERSHIP_ID");
			String loyMemberShipGuid =
				exportParams.getString("EV_MEMBERSHIP_GUID");

			// get messages
			JCO.Table messages = exportParams.getTable("ET_ERROR_MESSAGES");
			if (!messages.isEmpty()) {
				MessageCRM.logBapiMessagesToBusinessObject(
					loyMembership,
					messages);
			}

			// if available fill the loyaltiy membership accordingly
			if (loyMemberShipId != null
				&& loyMemberShipId.length() > 0
				&& loyMemberShipGuid != null
				&& loyMemberShipGuid.length() > 0) {
				loyMembership.setMembershipId(loyMemberShipId);
				loyMembership.setTechKey(new TechKey(loyMemberShipGuid));
                
                // fill point account    
                JCO.Table et_pointAccount = exportParams.getTable("ET_POINTTYPES");
                if (loyMembership.getPointAccountData() != null &&
                    loyMembership.getPointAccountData().getPointCodeId() != null) {
                    LoyaltyPointAccountData ptAccount = loyMembership.getPointAccountData();
                    String pointType = ptAccount.getPointCodeId();
                    boolean found = false;
                    for (int i = 0; !found && i < et_pointAccount.getNumRows(); i++) {
                        String ptCode = JCoHelper.getString(et_pointAccount, "POINT_CODE");
                        if (ptCode != null && ptCode.equals(pointType)) {
                            ptAccount.setPointCodeId(ptCode);
                            ptAccount.setDescr(JCoHelper.getString(et_pointAccount, "POINT_NAME"));
                            found = true;
                        }
                        et_pointAccount.nextRow();
                    }
                }
			}

		} catch (JCO.Exception ex) {
			if (log.isDebugEnabled())
				log.debug("Error calling CRM function:" + ex);
			JCoHelper.splitException(ex);
		} finally {
			log.exiting();
		}

	}

	/**
	 * read loyalty point account balance from backend
	 * @param loyalty membership guid
	 * @param point type
	 * @param loyalty point account
	 */
	public void readLoyaltyPointAccountBalanceFromBackend(
		String membershipGuid,
		String pointType,
		LoyaltyPointAccount loyaltyPointAccount)
		throws BackendException {
		final String METHOD_NAME =
			"readLoyaltyPointAccountBalanceFromBackend()";
		log.entering(METHOD_NAME);

		// JCO init
		JCoConnection aJCoCon = getDefaultJCoConnection();

		JCO.Function function =
			aJCoCon.getJCoFunction(
				LoyaltyMembershipCRM.READ_LOY_POINT_ACCOUNT_BALANCE_FUNCTION);

		try {
			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(membershipGuid, "IV_MEMBERSHIP_GUID");
			importParams.setValue(pointType, "IV_POINT_TYPE");

			// call the function
			aJCoCon.execute(function);

			//get the returned values
			JCO.ParameterList exportParams = function.getExportParameterList();

			JCO.Table et_pointAccount =
				exportParams.getTable("ET_POINT_ACCOUNT_BALANCES");

			int numPointAccount = et_pointAccount.getNumRows();

			if (numPointAccount >= 1) {
				//we only read the first one
				et_pointAccount.setRow(0);
				loyaltyPointAccount.setTechKey(
					new TechKey(
						JCoHelper.getString(
							et_pointAccount,
							"POINT_ACCOUNT_GUID")));
				loyaltyPointAccount.setAccountId(
					JCoHelper.getString(et_pointAccount, "POINT_ACCOUNT_ID"));
                loyaltyPointAccount.setEarned(
                    JCoHelper.getString(et_pointAccount, "EARNED"));
                loyaltyPointAccount.setConsumed(
                    JCoHelper.getString(et_pointAccount, "CONSUMED"));
                loyaltyPointAccount.setExpired(
                    JCoHelper.getString(et_pointAccount, "EXPIRED"));
                loyaltyPointAccount.setBalance(
                    JCoHelper.getString(et_pointAccount, "BALANCE"));
			}

			// get messages
			JCO.Table messages = exportParams.getTable("ET_MESSAGE");
			if (messages != null) {
				MessageCRM.splitMessagesToTarget(loyaltyPointAccount, messages);
			}
		} catch (JCO.Exception ex) {
			if (log.isDebugEnabled())
				log.debug("Error calling CRM function:" + ex);
			JCoHelper.splitException(ex);
		} finally {
			log.exiting();
		}
		log.exiting();
	}

	/**
	 * read loyalty point account from backend
	 * @param loyalty program guid
	 * @param loyalty program id
	 * @param point type
	 * @param loyalty point account
	 */
	public void readLoyaltyPointAccountFromBackend(
		String programGuid,
		String programId,
		String pointType,
		LoyaltyPointAccountData loyaltyPointAccount)
		throws BackendException {
		final String METHOD_NAME = "readLoyaltyPointAccountFromBackend()";
		log.entering(METHOD_NAME);

		// JCO init
		JCoConnection aJCoCon = getDefaultJCoConnection();

		JCO.Function function =
			aJCoCon.getJCoFunction(
				LoyaltyMembershipCRM.READ_LOY_POINT_ACCOUNT_FUNCTION);

		try {
			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(programGuid, "IV_PROGRAM_GUID");
			importParams.setValue(programId, "IV_PROGRAM_ID");

			// call the function
			aJCoCon.execute(function);

			//get the returned values
			JCO.ParameterList exportParams = function.getExportParameterList();

			JCO.Table et_pointAccount = exportParams.getTable("ET_POINTTYPES");
			boolean found = false;

			for (int i = 0; !found && i < et_pointAccount.getNumRows(); i++) {
				String ptCode =
					JCoHelper.getString(et_pointAccount, "POINT_CODE");
				if (ptCode != null && ptCode.equals(pointType)) {
					loyaltyPointAccount.setPointCodeId(ptCode);
					loyaltyPointAccount.setDescr(
						JCoHelper.getString(et_pointAccount, "POINT_NAME"));
					found = true;
				}
				et_pointAccount.nextRow();
			}

			// get message
			String msg = exportParams.getString("EV_MESSAGE_TEXT");
			log.error(msg);
		} catch (JCO.Exception ex) {
			if (log.isDebugEnabled())
				log.debug("Error calling CRM function:" + ex);
			JCoHelper.splitException(ex);
		} finally {
			log.exiting();
		}
		log.exiting();
	}

	/**
	 * create a new loyalty membership in the backend
	 * 
	 *@param loyalty membership data
	 *@param the business partner to create the membership for
	 * 
	 */
	public void createInBackend(
		LoyaltyMembershipData loyMembership,
		BusinessPartnerData member)
		throws BackendException {

		final String METHOD_NAME = "createInBackend()";
		log.entering(METHOD_NAME);

		JCoConnection jcoCon =
			(JCoConnection) getConnectionFactory().getDefaultConnection();
		LoyaltyMembershipCRM.createMembership(
			jcoCon,
			loyMembership,
			member.getTechKey().getIdAsString(),
			member.getId());

		log.exiting();
	}

	/**
	 * function that permits to enroll a given business partner for a given campaign
	 * 
	 * @param campaignGuid, the campaignGuid that is used for the enrollment process
	 * @param memberGuid, the memberGuid that is used for the enrollment process
	 * @return boolean, true if the enrollment process was successfull, false otherwise
	 */
	public boolean enrollLoyaltyCampaignInBackend(
		String campaignGuid,
		String memberGuid)
		throws BackendException {
		final String METHOD_NAME = "enrollLoyaltyCampaignInBackend()";
		log.entering(METHOD_NAME);

		boolean returnValue = false;

		// JCO init
		JCoConnection aJCoCon = getDefaultJCoConnection();

		JCO.Function function =
			aJCoCon.getJCoFunction(
				LoyaltyMembershipCRM.ENROLL_LOY_CAMPAIGN_FUNCTION);

		try {
			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			importParams.setValue(campaignGuid, "IV_MKTELEMENT_GUID");
			importParams.setValue(memberGuid, "IV_BP_GUID");
//			importParams.setValue("X","IV_COMMIT");

            // log the import parameter values
            if (log.isDebugEnabled()) {
                log.debug(METHOD_NAME + ": IV_MKTELEMENT_GUID=" + campaignGuid  
                          + " / IV_BP_GUID=" + memberGuid);
            }

			// call the function
			aJCoCon.execute(function);

			//get the returned value : enrollment was successfull
			JCO.ParameterList exportParams = function.getExportParameterList();
			String enroll_success = exportParams.getString("EV_ENROLL_SUCCESS");
			if (enroll_success != null && enroll_success.equals("X")) {
				returnValue = true;
			}
            
            // log the export parameter values
            if (log.isDebugEnabled()) {
                log.debug(METHOD_NAME + ": EV_ENROLL_SUCCESS=" + enroll_success);
            }

		} catch (JCO.Exception ex) {
			if (log.isDebugEnabled()) {
				log.debug("Error calling CRM function:" + ex);
				JCoHelper.splitException(ex);
			}
			returnValue = false;
		} finally {
			log.exiting();
		}

		log.exiting();
		return returnValue;
	}
	
	
	/**
	 * Get the data of the loyalty membership
	 * 
	 * @param loyalty membership
	 * @param the business partner linked to the membership 
	 */
	public void readFromBackend(
		LoyaltyMembershipData loyMembership,
		TechKey loyMember)
		throws BackendException {

		final String METHOD_NAME = "readFromBackend()";
		log.entering(METHOD_NAME);
		
		JCoConnection jcoCon = getDefaultJCoConnection();
		
		getMembership( jcoCon, loyMembership, loyMember);
		
	}		

}
