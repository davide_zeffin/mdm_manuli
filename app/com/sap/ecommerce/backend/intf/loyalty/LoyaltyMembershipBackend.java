package com.sap.ecommerce.backend.intf.loyalty;

import java.text.DateFormat;
import java.util.Date;

import com.sap.ecommerce.boi.campaign.MarketingCampaignListData;
import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipData;
import com.sap.ecommerce.boi.loyalty.LoyaltyPointAccountData;
import com.sap.ecommerce.boi.loyalty.LoyaltyTransactionListData;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyPointAccount;
import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;

public interface LoyaltyMembershipBackend extends BackendBusinessObjectSAP {

	/**
	 * read the loyalty transactions data from the back end
	 *
	 * @param loyaltyTransactionlist document to fill the data in
	 * @param fromDate the transaction From Date
	 * @param toDate the transaction To Date
	 *
	 */
	public void readLoyaltyTransactionsFromBackend(
		String membershipGuid,
		String pointAccountGuid,
		DateFormat dateFormat,
		Date fromDate,
		Date toDate,
		LoyaltyTransactionListData loyaltyTransactionList) throws BackendException;

	/**
	 * read the loyalty campaigns data from the back end
	 *
	 * @param campaignList document to fill the data in
	 *
	 */
	public void readLoyaltyCampaignsFromBackend(
		String membershipId,
		String memberGuid,
		String campaignTextId,
		DateFormat dateFormat,
		MarketingCampaignListData campaignList) throws BackendException;

	/**
	 * read the loyalty point account information from the back end
	 * @param, loyaltyPointAccount object to fill the data in
	 * @param memberShipGuid, the membership guid
	 * @param pointType, the point type for reading the point Account
	 */
	public void readLoyaltyPointAccountBalanceFromBackend(
		String membershipGuid,
		String pointType,
		LoyaltyPointAccount loyaltyPointAccount)
		throws BackendException;

    /**
     * read loyalty point account from backend
     * @param loyalty program guid
     * @param loyalty program id
     * @param point type
     * @param loyalty point account
     */
    public void readLoyaltyPointAccountFromBackend(
        String programGuid,
        String programId,
        String pointType,
        LoyaltyPointAccountData loyaltyPointAccount)
    throws BackendException;

	/**
	 * create the loyalty membership in the backend
	 * 
	 */
	public void createInBackend(
		LoyaltyMembershipData loyMembership,
		BusinessPartnerData member)
		throws BackendException;
		
	/**
	 * function that permits to enroll a given business partner for a given campaign
	 * 
	 * @param campaignGuid, the campaignGuid that is used for the enrollment process
	 * @param memberGuid, the memberGuid that is used for the enrollment process
	 * @return boolean, true if the enrollment process was successfull, false otherwise
	 */		
	public boolean enrollLoyaltyCampaignInBackend(String campaignGuid, String memberGuid) throws BackendException;
	
	
	/**
	 * Get the data of the loyalty membership
	 * 
	 * @param loyalty membership
	 * @param the business partner linked to the membership 
	 */
	public void readFromBackend(LoyaltyMembershipData loyMembership,
		                        TechKey loyMember)
		throws BackendException; 

}
