/*****************************************************************************
	Class         CampaignBackend
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.backend.intf.campaign;

import java.util.Date;
import com.sap.ecommerce.boi.campaign.CampaignData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;

/**
 * 
 * Backend interface for campaign based objects 
 * 
 * @author  SAP AG
 * @version 1.0
*/
public interface CampaignBackend extends BackendBusinessObjectSAP {

	/**
	 * read the data from the back end
	 *
     * @param campaign docuement to fill the data in
	 *
	 */
	public void readFromBackend(CampaignData campaign)
		throws BackendException;
	
	/**
	 * This method verifies if the business partner entered on the UI
	 * is part of one of the target group of this campaign.
	 * It returns void but fills the message list with the appropriate
	 * message to display on campaign_status.jsp in case of error.
	 * @param refDocId - Campaign id 
	 * @param soldTo - Business partner number entered on UI
	 * @param campaign - BO Campaign
	 */
	public void checkBPInTargetGroup(CampaignData campaign, String refDocId, String soldTo)
		 throws BackendException; 	
		
	/**
	  * Read campaign header data from the backend. 
	  *
	  * @param campaign The document to read the data in
	  */
	public void getCampaignHeader(CampaignData campaign)
		throws BackendException;
			
	/**
	 * Check eligibility of a sold-to for private campaigns.
	 * @param campaign 		The document to read the data in
	 * @param soldToGuid 	Sold-to Guid
	 * @param salesOrg		Sales Organisation
	 * @param distChannel   Distribution Channel 
	 * @param division      Division
	 * @throws BackendException
	 */
	public void checkPrivateCampaignEligibility(CampaignData campaign, TechKey soldToGuid, String salesOrg, String distChannel, String division, Date campaignDate)
		throws BackendException;	

    /**
      * Read the campaign with the enrollment data from the backend. 
      *
      * @param campaign     The document to read the data in
      */
    public void readCampaignWithEnrollment(CampaignData campaign) 
                throws BackendException;


    /**
      * Store the enrollment data for the campaign on the backend. 
      *
      * @param campaign The document with the enrollment data to be stored.
      * @return errorFlag as boolean, if error appears by backend processing
      */
    public boolean enrollBpForCampaign(CampaignData campaign) 
                throws BackendException;

}
