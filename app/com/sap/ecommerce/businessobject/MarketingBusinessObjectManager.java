package com.sap.ecommerce.businessobject;

import com.sap.ecommerce.businessobject.loyalty.LoyaltyCampaign;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyProgram;
import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;

/*****************************************************************************
	Class         LoyaltyMembershipObjectManager
	Copyright (c) 2008, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      04.03.2008
	Version:      1.0

	$Revision: #01 $
	$Date: 2008/03/04 $
*****************************************************************************/

/**
 * Central point to create and access Objects of the business logic. After
 * a request for a specific object, BusinessObjectManager checks if there is already
 * an object created. If so, a reference to this object is returned, otherwise
 * a new object is created and a reference to the new object is returned.
 * The reason for this class is, that the construction process of the businnes
 * logic objects may be quite complicated (because of some contextual
 * information) and most objects should only exist once per session.
 * To achieve this the <code>BusinessObjectManager</code>
 * is used. If you store a reference to this
 * object (for examle in a session context) you can get access to all other
 * business obejects using it.
 * 
 * @author  SAP AG
 * @version 1.0
 */
public class MarketingBusinessObjectManager extends GenericBusinessObjectManager implements BackendAware {
	public final static String MARKETING_BOM = "MARKETING-BOM";


	private LoyaltyProgram loyaltyProgram;
	
	/**
	 * Create a new instance of the object.
	 */
	public MarketingBusinessObjectManager() {
	}

	/**
	 * Creates a new loyalty membership object. If such an object was already 
	 * created, a reference to the old object is returned and no 
	 * new object is created.
	 *
	 * @return reference to a newly created or already existend loyalty membership 
	 * object
	 */
	public synchronized LoyaltyMembership createLoyaltyMembership() {
		return (LoyaltyMembership) createBusinessObject(LoyaltyMembership.class);
	}

	/**
	 * Returns a reference to an existing loyalty membership object.
	 *
	 * @return reference to loyalty membership object or null if no object is present
	 */
	public LoyaltyMembership getLoyaltyMembership() {
	  return (LoyaltyMembership) getBusinessObject(LoyaltyMembership.class);
	}

	/**
	 * Release the references to created campaign object.
	 */
	public synchronized void releaseLoyaltyMembership() {
		releaseBusinessObject(LoyaltyMembership.class);
	}
	
	
	public synchronized LoyaltyProgram createLoyaltyProgram(TechKey loyaltyProgramGuid, String loyaltyProgramId) {
		if (this.loyaltyProgram == null) {
		  this.loyaltyProgram = new LoyaltyProgram(loyaltyProgramGuid, loyaltyProgramId);
		}
		return this.loyaltyProgram;
	}
	
	
	public LoyaltyProgram getLoyaltyProgram() {
		return this.loyaltyProgram;
	}
	
	
	public synchronized void releaseLoyaltyProgram() {
		this.loyaltyProgram = null;
	}

	/**
	 * Creates a new loyalty campaign object. If such an object was already 
	 * created, a reference to the old object is returned and no 
	 * new object is created.
	 *
	 * @return reference to a newly created or already existend loyalty membership 
	 * object
	 */
	public synchronized LoyaltyCampaign createLoyaltyCampaign() {
		return (LoyaltyCampaign) createBusinessObject(LoyaltyCampaign.class);
	}

	/**
	 * Returns a reference to an existing loyalty campaign object.
	 *
	 * @return reference to loyalty membership object or null if no object is present
	 */
	public LoyaltyCampaign getLoyaltyCampaign() {
	  return (LoyaltyCampaign) getBusinessObject(LoyaltyCampaign.class);
	}

	/**
	 * Release the references to created campaign object.
	 */
	public synchronized void releaseLoyaltyCampaign() {
		releaseBusinessObject(LoyaltyCampaign.class);
	}	
	
	
}

