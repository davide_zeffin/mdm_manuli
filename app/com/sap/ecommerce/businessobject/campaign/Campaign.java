/*****************************************************************************
	Class         Campaign
	Copyright (c) 2004, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import java.util.Date;

import com.sap.ecommerce.backend.intf.campaign.CampaignBackend;
import com.sap.ecommerce.boi.campaign.CampaignData;
import com.sap.ecommerce.boi.campaign.CampaignEnrollmentListData;
import com.sap.ecommerce.boi.campaign.CampaignHeaderData;
import com.sap.ecommerce.boi.campaign.CampaignSearchCriteriaData;
import com.sap.ecommerce.boi.campaign.DateRangeListData;
import com.sap.ecommerce.boi.campaign.ProductListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.LogUtil;

/**
 * 
 * Campaign object
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class Campaign extends BusinessObjectBase implements BackendAware, CampaignData, DocumentState {

	protected static final String BEO_NAME = "Campaign";

	private int state = 0;

	protected BackendObjectManager bem;

	/**
	 * Backend to the campaign
	 */
	private CampaignBackend backendService;

    private CampaignHeader header = null;
	private ProductList productList = new ProductList();
	private DateRangeList dateRangeList = new DateRangeList();
    private CampaignEnrollmentList enrollmList = new CampaignEnrollmentList();
    private CampaignSearchCriteria searchCriteria = null;

	/**
	* Sets the BackendObjectManager for the object. This method is used
	* by the object manager to allow the user object interaction with
	* the backend logic. This method is normally not called
	* by classes other than BusinessObjectManager.
	*
	* @param bem Backend object manager to be used
	*/
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}

	/**
	 * Retrieves the backend service
	 *
	 * @return backendService Campaign backend service
	 */
	protected CampaignBackend getBackendService() throws BackendException {
		synchronized (this) {
			if (backendService == null) {
				backendService = (CampaignBackend)bem.createBackendBusinessObject(Campaign.BEO_NAME, null);
			}
		}
		return backendService;
	}

	/**
	 * Retrieves the data from the backend and stores the data in this object
	 */
	public void read() throws CommunicationException {
		final String METHOD_NAME = "read()";

		log.entering(METHOD_NAME);

		try {
			// get data from back end
			getBackendService().readFromBackend(this);
		}
		catch (BackendException ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);
		}
		finally {
			log.exiting();
		}
	}
	
	/**
	 * Checks if the BP entered on UI is in one of the target group of the campaign
	 */
	public void checkBPisInTargetGroup(String refDocId, String soldTo) throws CommunicationException {
		final String METHOD_NAME = "checkBPisInTargetGroup()";

		log.entering(METHOD_NAME);

		try {			
			getBackendService().checkBPInTargetGroup(this, refDocId, soldTo);
		}
		catch (BackendException ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);
		}
		finally {
			log.exiting();
		}
	}	
	

	/**
	 * Retrieves campaign header data from the backend and stores the data
	 * in this object
	 */
	public void readCampaignHeader() throws CommunicationException {
		final String METHOD_NAME = "readCampaignHeader()";

		log.entering(METHOD_NAME);

		try {
			// get data from back end
			getBackendService().getCampaignHeader(this);
		}
		catch (BackendException ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);
		}
		finally {
			log.exiting();
		}
	}

	/**
	 * Checks eligibility of a sold-to for a private campaign
	 * and stores in this object
	 */
	public void checkPrivateCampaignEligibility(TechKey soldToGuid, String salesOrg, String distChannel, String division, Date campaignDate) throws CommunicationException {
		final String METHOD_NAME = "checkPrivateCampaignEligibility(...)";

		log.entering(METHOD_NAME);

		try {
			// get data from back end
			getBackendService().checkPrivateCampaignEligibility(this, soldToGuid, salesOrg, distChannel, division, campaignDate);
		}
		catch (BackendException ex) {
			log.error("The following error occured: ", ex);
			log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "esrv.com.log.error.generic", new Object[] { METHOD_NAME }, ex);
		}
		finally {
			log.exiting();
		}
	}

	/**
	 * Creates a <code>CampaignHeader</code> for the campaign.
	 *
	 * @return created header
	 */
	public CampaignHeaderData createHeader() {
		return (CampaignHeaderData)new CampaignHeader();
	}

	/**
	 * Sets the header. The header contains general campaign
	 * information
	 *
	 * @param header Header data to set
	 */
	public void setHeader(CampaignHeaderData header) {
		if (header instanceof CampaignHeader) {
			this.header = (CampaignHeader)header;
		}
	}

	/**
	 * Retrieves the header information of the campaign.
	 *
	 * @return header Header data
	 */
	public CampaignHeaderData getHeaderData() {
		return (CampaignHeaderData)header;
	}

    /**
     * Retrieves the header information of the campaign.
     *
     * @return header Header data as CampaignHeader
     */
    public CampaignHeader getHeader() {
        return header;
    }

    /**
     * Sets the search criteria. The search criteria contain the
     * criteria used to search or restrict the campaign data 
     *
     * @param searchCriteria Search criteria data to set
     */
    public void setSearchCriteria(CampaignSearchCriteria searchCriteria) {
        this.searchCriteria = (CampaignSearchCriteria)searchCriteria;
    }

    /**
     * Retrieves the search criteria information of the campaign.
     *
     * @return searchCriteria Search criteria data
     */
    public CampaignSearchCriteriaData getSearchCriteriaData() {
        return (CampaignSearchCriteriaData)searchCriteria;
    }

    /**
     * Retrieves the search criteria information of the campaign.
     *
     * @return searchCriteria Search criteria data as CampaignSearchCriteria 
     */
    public CampaignSearchCriteria getSearchCriteria() {
        return this.searchCriteria;
    }

	/**
	 * Retrieves the product list of the campaign.
	 *
	 * @return productList Product list
	 */
	public ProductListData getProductListData() {
		return (ProductListData)productList;
	}

	/**
	 * Retrieves the date range list of the campaign.
	 *
	 * @return dateRangeList Date tange list
	 */
	public DateRangeListData getDateRangeListData() {
		return (DateRangeListData)dateRangeList;
	}

    /**
     * Retrieves the enrollment list of the campaign.
     *
     * @return enrollmList as CampaignEnrollmentListData Campaign enrollment list
     */
    public CampaignEnrollmentListData getCampaignEnrollmentListData() {
        return (CampaignEnrollmentListData)enrollmList;
    }

    /**
     * Retrieves the enrollment list of the campaign.
     *
     * @return enrollmList as CampaignEnrollmentList Campaign enrollment list
     */
    public CampaignEnrollmentList getCampaignEnrollmentList() {
        return enrollmList;
    }


	/**
	 * Callback method for the <code>BusinessObjectManager</code> to tell
	 * the object that life is over and that it has to release
	 * all ressources.
	 */
	public void destroy() {
		this.clear();
		bem = null;
		super.destroy();
	}

	/**
	 * releases / initializes the resources of the object
	 */
	public void clear() {
		techKey = TechKey.EMPTY_KEY;
		productList = new ProductList();
		dateRangeList = new DateRangeList();
        enrollmList = new CampaignEnrollmentList();
		header = null;
        searchCriteria = null;
	}

	/**
	 * Returns the string representation of the object.
	 *
	 * @retun String representation
	 */
	public String toString() {
		String retVal;

		if (this.header == null) {
			return super.toString();
		}
		else {
			retVal = this.header.toString();
			return retVal;
		}
	}

	/**
	 * Gets the state of the document
	 *
	 * @return the state as described by the constants of this interface
	 */
	public int getState() {
		return state;
	}

	/**
	 * Sets the state of the document
	 *
	 * @param state the state as described by the constants of this interface
	 */
	public void setState(int state) {
		this.state = state;
	}
    
    /**
     * Retrieves the data from the backend and stores the data in this object
     */
    public void readCampaignWithEnrollment() throws CommunicationException {
        final String METHOD = "readCampaignWithEnrollment()";

        log.entering(METHOD);

        try {
            // get data from back end
            getBackendService().readCampaignWithEnrollment(this);
        }
        catch (BackendException ex) {
            log.error("The following error occured: ", ex);
        }
        finally {
            log.exiting();
        }
    }
    
    /**
     * Strores the enrollment data on the backend 
     * @return errorFlag as boolean, if error appears by backend processing
     */
    public boolean enrollBpForCampaign() throws CommunicationException {
        final String METHOD = "enrollBpForCampaign()";
        boolean errorFlag = false;

        log.entering(METHOD);

        try {
            if (this.enrollmList != null) {
                boolean enrollmChanged = false;
                for (int i = 0; i < enrollmList.size() && !enrollmChanged; i++) {
                    enrollmChanged = enrollmList.get(i).isChanged();   
                }
                if (enrollmChanged) {
                    // set data on backend
                    errorFlag = getBackendService().enrollBpForCampaign(this);
                }
                else {
                    errorFlag = true;
                }
            }
            else {
                errorFlag = true;
            }
        }
        catch (BackendException ex) {
            log.error("The following error occured: ", ex);
        }
        finally {
            log.exiting();
        }
        return(errorFlag);
    }

}
