/*****************************************************************************
	Class         ProductList
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.ecommerce.boi.campaign.CampaignProductItemData;
import com.sap.ecommerce.boi.campaign.ProductListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

/**
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class ProductList extends BusinessObjectBase implements ProductListData {

	protected List productList = new ArrayList(20);

	/**
	 * Returns an iterator for the products. Method necessary because of
	 * the <code>Iterable</code> interface.
	 *
	 * @return iterator to iterate over products
	 *
	 */
	public Iterator iterator() {
		if (productList != null)
			return productList.iterator();
		else
			return null;
	}

	/**
	 * create a product
	 * @param techKey  techKey of the product
	 */
	public CampaignProductItemData createProduct(TechKey techKey) {
		return (CampaignProductItemData)new CampaignProductItem(techKey);
	}

	/**
	 * create a product
	 * @param techKey	techKey of the product
	 * @param ID		ID of the product
	 */
	public CampaignProductItemData createProduct(TechKey techKey, String ID) {
		return (CampaignProductItemData)new CampaignProductItem(techKey, ID);
	}

	/**
	 * Add a new product to the product list, must overwrite from 
	 * objects which didn't use the <code>Product</code> directly.
	 * @param Product which should add to the list
	 * @see Product
	 */
	public void addProduct(CampaignProductItemData product) {
		if (product != null && product instanceof CampaignProductItem) {
			productList.add(product);
		}
	}

	/**
	 * returns the object as string
	 * @return String which contains all products
	 */
	public String toString() {
		String string = "";

		for (int i = 0; i < productList.size(); i++) {
			string = string + productList.get(i).toString() + "\n";
		}

		return string;
	}

	/**
	 * Returns the number of products in the list
	 * @return count of products
	 */
	public int size() {
		return productList.size();
	}
}
