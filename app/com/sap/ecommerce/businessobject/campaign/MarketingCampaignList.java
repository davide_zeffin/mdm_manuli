package com.sap.ecommerce.businessobject.campaign;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import com.sap.ecommerce.boi.campaign.CampaignData;
import com.sap.ecommerce.boi.campaign.MarketingCampaignListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

public class MarketingCampaignList extends BusinessObjectBase implements Iterable, MarketingCampaignListData {

	protected ArrayList campaignList = null;

	/**
	 * adds a Campaign to the Marketing Campaign List
	 * @param the campaign that will be added to the list
	 */
	public void addCampaign(CampaignData campaign) {
		log.entering("addCampaign(CampaignData campaign)");

		if (campaignList == null) {
			campaignList = new ArrayList();
			if (log.isDebugEnabled()) {
				log.debug("addCampaign : new loyaltyCampaign list created");
			}
		}

		campaignList.add(campaign);

		if (log.isDebugEnabled()) {
			log.debug("addCampaign : loyaltyCampaign added to list; size = "	+ size());
		}
		log.exiting();
	}

	/**
	 * iterator
	 * @return an iterator that contains the campaign objects from this marketing campaign list
	 */
	public Iterator iterator() {
		log.entering("iterator()");
		log.exiting();
		if (campaignList != null)
			return campaignList.iterator();
		else
			return null;
	}

	/**
     * Returns the CampaignData line of the list
	 * 
	 * @param i of type int, index of CampaignData line 
	 * @return campaign line of type CampaignData 
	 */
	public CampaignData getData(int i) {
		log.entering("getData(int i");

		CampaignData campaign = null;
		if (campaignList != null) {
			campaign = (CampaignData) campaignList.get(i);
		}
		log.exiting();
		return campaign;
	}

	/**
	 * @return the size of the marketing campaign list
	 */
	public int size() {
		log.entering("size");
		int size = 0;
		if (campaignList != null) {
			size = campaignList.size();
		}
		log.exiting();
		return size;
	}

	/**
	 * sorts the marketing campaign list by "attribute"
	 * @param the attribute which occurs the sort
	 */
	public void order(DateFormat dateFormat, String attribute) {
		log.entering("order(String attribute)");
		if (campaignList != null) {
			Collections.sort(campaignList, new CampaignComparator(dateFormat, attribute));
		}
		log.exiting();
	}
	
	/**
	 * returns the campaign object identified by the campaignID
	 * @return the campaign identified by campaignID
	 * @param the campaignID for the returned campaign
	 */
	public CampaignData getCampaign(String campaignID){
		log.entering("getCampaign(String campaignID)");
		
		Campaign returnCampaign = null;
		Iterator it = iterator();
		while(it.hasNext()){
			Campaign campaignTemp = (Campaign)it.next();
			if( campaignTemp.getHeader().getCampaignID().equals(campaignID)){
				returnCampaign = campaignTemp;
				break; 
			}
		}
		log.exiting();
		return returnCampaign;
	}
}
