package com.sap.ecommerce.businessobject.campaign;

import java.text.DateFormat;
import java.util.Comparator;
import java.util.Date;

import com.sap.isa.core.logging.IsaLocation;

public class CampaignComparator implements Comparator {

	/**
	 * Name of the attribute to be used for compare
	 */
	private String attribute;

	private DateFormat dateFormat = null;

	protected static IsaLocation log =
		IsaLocation.getInstance(CampaignComparator.class.getName());

	/**
	 * Create new comparator
	 * @param attribute attribute which is key for compare
	 */
	public CampaignComparator(DateFormat dateFormat, String attribute) {
		log.entering("CampaignComparator()");
		this.attribute = attribute;
		this.dateFormat = dateFormat;
		log.exiting();
	}

	/**
	 * compare two Campaign regarding <code>attribute</code>
	 * @param object1 first object to compare, needs to be of class Campaign
	 * @param object2 second object to compare, needs to be of class Campaign
	 * @return result int which is < 0 if object1 < object2, = 0 if they are equal
	 *         and >0 if object1 > object2
	 */
	public int compare(Object obj1, Object obj2) {
		log.entering("compare(Object obj1, Object obj2)");

		int returnValue = 0;
		Campaign item1 = (Campaign) obj1;
		Campaign item2 = (Campaign) obj2;

		if (attribute.equals(CampaignHeader.CAMPAIGN_ID)) {
			String s1 = item1.getHeader().getCampaignID();
			String s2 = item2.getHeader().getCampaignID();
			returnValue = s1.compareTo(s2);
		} else {
			if (attribute.equals(CampaignHeader.CAMPAIGN_DESC)) {
				String s1 = item1.getHeader().getDescription();
				String s2 = item2.getHeader().getDescription();
				returnValue = s1.compareTo(s2);
			} else {
				if (attribute.equals(CampaignHeader.CAMPAIGN_REG_STATUS)) {
					int i1 = item1.getHeader().getRegistrationStatus();
					int i2 = item2.getHeader().getRegistrationStatus();
					returnValue = (new Integer(i1)).compareTo((new Integer(i2)));
				} else {
					if (attribute.equals(CampaignHeader.CAMPAIGN_START_DATE)) {
						return compareDate(
							item1.getHeader().getPlannedStartDate(),
							item2.getHeader().getPlannedStartDate());
					} else {
						if (attribute.equals(CampaignHeader.CAMPAIGN_END_DATE)) {
							return compareDate(
								item1.getHeader().getPlannedEndDate(),
								item2.getHeader().getPlannedEndDate());
						} else {
							if (attribute.equals(CampaignHeader.CAMPAIGN_REG_ON)) {
								return compareDate(
									item1.getHeader().getEnrollmEndDate(),
									item2.getHeader().getEnrollmEndDate());
							}
						}
					}
				}
			}
		}
		return returnValue;
	}

    /**
     * compare two dates
     * @param s1 first date to compare of type String
     * @param s2 second date to compare of type String
     * @return result int which is < 0 if s1 < s2, = 0 if they are equal
     *         and >0 if s1 > s2
     */
	private int compareDate(String s1, String s2) {
		int returnValue = 0;
        if (s1 == null || s1.length() == 0) {
            if (s2 == null || s2.length() == 0) {
                returnValue = 0;
            }
            else { 
                returnValue = -1;
            }
        }
        else if (s2 == null || s2.length() == 0) {
            returnValue = 1;
        }
        else {
            try {
                Date d1 = dateFormat.parse(s1);
                Date d2 = dateFormat.parse(s2);
                returnValue = d1.compareTo(d2);
            } 
            catch (Exception e) {
                returnValue = 0;
            }
        }
        return returnValue;
    }
}
