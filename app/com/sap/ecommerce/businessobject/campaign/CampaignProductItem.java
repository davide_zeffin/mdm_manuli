/*****************************************************************************
	Class         Product
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import com.sap.ecommerce.boi.campaign.CampaignProductItemData;
import com.sap.isa.businessobject.Product;
import com.sap.isa.core.TechKey;

/**
 * 
 * CampaignProductItem is a data container for campaign product items
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class CampaignProductItem extends Product implements CampaignProductItemData {

	private String amount;
	private String amountUom;
	private boolean isChecked;

	/**
	 * Constructor
	 * @param techKey	product techkey
	 */
	public CampaignProductItem(TechKey techKey) {
		super(techKey);
	}

	/**
	 * Constructor
	 * @param techKey	product techkey
	 * @param ID		product ID
	 */
	public CampaignProductItem(TechKey techKey, String ID) {
		super(techKey, ID);
	}

	public boolean getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmountUom() {
		return amountUom;
	}

	public void setAmountUom(String amountUom) {
		this.amountUom = amountUom;
	}
}
