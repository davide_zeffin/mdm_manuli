/*****************************************************************************
	Class         DateRange
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import com.sap.ecommerce.boi.campaign.DateRangeData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * 
 * DateRange is a data container for a campaign date range
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class DateRange extends BusinessObjectBase implements DateRangeData {

	private String startDate;
	private String endDate;
	private String dateID;
	private String periodType;
	private String dateRangeType;

	public String getDateID() {
		return dateID;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getPeriodType() {
		return periodType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setDateID(String dateID) {
		this.dateID = dateID;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getDateRangeType() {
		return dateRangeType;
	}

	public void setDateRangeType(String string) {
		dateRangeType = string;
	}

	/**
	 * Returns the string representation of the object.
	 * @retun String representation
	 */
	public String toString() {
		return this.startDate + "-" + this.endDate;
	}
}
