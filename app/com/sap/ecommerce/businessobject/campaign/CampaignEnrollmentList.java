/*****************************************************************************
	Class         CampaignEnrollmentList
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      28.11.2006
	Version:      1.0

	$Revision: #01 $
	$Date: 2006/11/28 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.ecommerce.boi.campaign.CampaignEnrollmentData;
import com.sap.ecommerce.boi.campaign.CampaignEnrollmentListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

/**
 * 
 * CampaignEnrollmentList represents a list of enrollment lines of a campaign 
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class CampaignEnrollmentList extends BusinessObjectBase implements Iterable, CampaignEnrollmentListData {

	protected ArrayList enrollmList = null;

	/**
	 * Returns an iterator for the enrollment lines. Method necessary because 
	 * of the <code>Iterable</code> interface.
	 * @return iterator to iterate over enrollment lines
	 */
	public Iterator iterator() {
		if (enrollmList != null)
			return enrollmList.iterator();
		else
			return null;
	}

	/**
	 * create a enrollment object
	 */
	public CampaignEnrollmentData createEnrollment() {
		return (CampaignEnrollmentData)new CampaignEnrollment();
	}

	/**
	 * Add a new enrollment to the list, must overwrite from 
	 * objects which didn't use the <code>CampaignEnrollment</code> directly.
	 * @param enrollment which should add to the list
	 * @see CampaignEnrollment
	 */
	public void addEnrollment(CampaignEnrollmentData enrollment) {
        final String METHOD = "addEnrollment(CampaignEnrollmentData enrollment)";
        log.entering(METHOD);
        
        if (enrollmList == null) {
            enrollmList = new ArrayList();
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": new enrollment list created");
            }
        }

        enrollmList.add(enrollment);

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": enrollment added to list; size = " + size());
        }
        log.exiting();
	}

	/**
	 * Returns the number of enrollments in the list
	 * @return count of enrollment lines
	 */
	public int size() {
        int size = 0;
        if (enrollmList != null) { 
            size = enrollmList.size();
        }
		return size;
	}

    /**
     * Returns the enrollment line of the list
     * 
     * @param idx of type int, index of enrollment line 
     * @return enrollment line of type CampaignEnrollment 
     */
    public CampaignEnrollment get(int idx) {
        CampaignEnrollment enrollm = null;
        if (enrollmList != null) { 
            enrollm = (CampaignEnrollment) enrollmList.get(idx);
        }
        return enrollm;
    }

    /**
     * Returns the enrollment line with the specified GUID from the list
     * 
     * @param guid of type String, unique index of enrollment line 
     * @return enrollment line of type CampaignEnrollment 
     */
    public CampaignEnrollment get(String guid) {
        CampaignEnrollment enrollm = null;
        boolean found = false;
        
        if (enrollmList != null) { 
            for (int i = 0; i < enrollmList.size() && !found; i++) {
                enrollm = (CampaignEnrollment) enrollmList.get(i);
                found = enrollm.getEnrolleeGuid().equals(guid);
            }
        }
        return enrollm;
    }

    /**
     * Returns the an enrollment line of the list
     * 
     * @param idx of type int, index of enrollment line 
     * @return enrollment line of type CampaignEnrollmentData
     */
    public CampaignEnrollmentData getData(int idx) {
        CampaignEnrollment enrollm = null;
        if (enrollmList != null) { 
            enrollm = (CampaignEnrollment) enrollmList.get(idx);
        }
        return enrollm;
    }
    
    /**
     * Returns the information if all business partners are enrolled
     * 
     * @return flag as boolean, true if all business partners are enrolled
     */
    public boolean areAllBpEnrolled() {
        final String METHOD = "areAllBpEnrolled()";
        log.entering(METHOD);
        
        boolean retval = true;
        
        Iterator iter = enrollmList.iterator();
        
        while (iter.hasNext() && retval) { 
            retval = ((CampaignEnrollment) iter.next()).isEnrolled();
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": all business partner are enrolled = " + retval);
        }
        
        log.exiting();
        return (retval);
    }
    
    /**
     * Changes all enrollments according to the parameter value
     * 
     * @param isNewEnrollFlag as boolean, value to be set for all business partners
     */
    public void changeAllEnrollments(boolean isNewEnrollFlag) {
        final String METHOD = "changeAllEnrollments()";
        log.entering(METHOD);
        
        Iterator iter = enrollmList.iterator();
        int counter = 0;
        
        while (iter.hasNext()) { 
            CampaignEnrollment enrollm = (CampaignEnrollment) iter.next();
            if (enrollm.isEnrolled() != isNewEnrollFlag) {
                // enrollment was changed
                enrollm.setIsEnrolled(isNewEnrollFlag);
                enrollm.setIsChanged();
                counter++;
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": number of changed enrollment flags = " + counter);
        }
        
        log.exiting();
    }

    /**
     * Returns the information if only the enroller is contained as business partners in the list
     * 
     * @return flag as boolean, true if only the enroller is in the list
     */
    public boolean isEnrollerOnly(String enrollerGuid) {
        final String METHOD = "isEnrollerOnly()";
        log.entering(METHOD);
        
        boolean retval = true;
        String enrolleeGuid;
        int i = 0;
        Iterator iter = enrollmList.iterator();
        
        while (iter.hasNext() && retval) {
            i++; 
            enrolleeGuid = ((CampaignEnrollment) iter.next()).getEnrolleeGuid();
            if (i>1 || !enrolleeGuid.equals(enrollerGuid)) {
                retval = false;
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": only the enroller is in the list = " + retval);
        }
        
        log.exiting();
        return (retval);
    }
    
    /**
     * Returns the information if at least one enrollment was changed
     * 
     * @return flag as boolean, true if at least one enrollment was changed
     */
    public boolean isEnrollmListChanged() {
        final String METHOD = "isEnrollmListChanged()";
        log.entering(METHOD);
        
        boolean retval = false;
        Iterator iter = enrollmList.iterator();
        
        while (iter.hasNext() && !retval) {
            retval = ((CampaignEnrollment) iter.next()).isChanged();
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": at least one enrollment is changed = " + retval);
        }
        
        log.exiting();
        return (retval);
    }

}
