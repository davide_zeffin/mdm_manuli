/*****************************************************************************
	Class         CampaignBusinessObjectManager
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.businessobject.BackendAware;

/**
 * Central point to create and access Objects of the business logic. After
 * a request for a specific object, BusinessObjectManager checks if there is already
 * an object created. If so, a reference to this object is returned, otherwise
 * a new object is created and a reference to the new object is returned.
 * The reason for this class is, that the construction process of the businnes
 * logic objects may be quite complicated (because of some contextual
 * information) and most objects should only exist once per session.
 * To achieve this the <code>BusinessObjectManager</code>
 * is used. If you store a reference to this
 * object (for examle in a session context) you can get access to all other
 * business obejects using it.
 * 
 * @author  SAP AG
 * @version 1.0
 */
public class CampaignBusinessObjectManager extends GenericBusinessObjectManager 
										 implements BackendAware {
										 	
    public final static String CAMPAIGN_BOM = "CAMPAIGN-BOM";

	/**
	 * Create a new instance of the object.
	 */
	public CampaignBusinessObjectManager() {
	}

	/**
	 * Creates a new campaign object. If such an object was already 
	 * created, a reference to the old object is returned and no 
	 * new object is created.
	 *
	 * @return referenc to a newly created or already existend campaign 
	 * object
	 */
	public synchronized Campaign createCampaign() {
		return (Campaign) createBusinessObject(Campaign.class);
	}

	/**
	 * Returns a reference to an existing campaign object.
	 *
	 * @return reference to campaign object or null if no object is present
	 */
	public Campaign getCampaign() {
	  return (Campaign) getBusinessObject(Campaign.class);
	}

	/**
	 * Release the references to created campaign object.
	 */
	public synchronized void releaseCampaign() {
		releaseBusinessObject(Campaign.class);
	}
}

