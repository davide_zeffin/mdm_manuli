/*****************************************************************************
	Class         DateRangeList
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.ecommerce.boi.campaign.DateRangeData;
import com.sap.ecommerce.boi.campaign.DateRangeListData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * 
 * DateRangeList represents a list of campaign date ranges
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class DateRangeList extends BusinessObjectBase implements DateRangeListData {

	protected List dateRangeList = new ArrayList(5);

	/**
	 * Returns an iterator for the date ranges. Method necessary because 
	 * of the <code>Iterable</code> interface.
	 * @return iterator to iterate over date ranges
	 */
	public Iterator iterator() {
		if (dateRangeList != null)
			return dateRangeList.iterator();
		else
			return null;
	}

	/**
	 * create a date range object
	 */
	public DateRangeData createDateRange() {
		return (DateRangeData)new DateRange();
	}

	/**
	 * Add a new date range to the list, must overwrite from 
	 * objects which didn't use the <code>DateRange</code> directly.
	 * @param DateRange which should add to the list
	 * @see DateRange
	 */
	public void addDateRange(DateRangeData dateRange) {
		if (dateRange != null && dateRange instanceof DateRange) {
			dateRangeList.add(dateRange);
		}
	}

	/**
	 * Returns the number of date ranges in the list
	 * @return count of date ranges
	 */
	public int size() {
		return dateRangeList.size();
	}
}
