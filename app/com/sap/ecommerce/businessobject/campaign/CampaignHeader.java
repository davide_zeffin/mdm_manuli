/*****************************************************************************
	Class         CampaignHeader
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      26.01.2004
	Version:      1.0

	$Revision: #01 $
	$Date: 2004/01/26 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import com.sap.ecommerce.boi.campaign.CampaignHeaderData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * 
 * CampaignHeader is a data container for general campaign data
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class CampaignHeader extends BusinessObjectBase implements CampaignHeaderData {

    protected final String PERIOD_STATUS_PRESENT = "A";
    protected final String PERIOD_STATUS_FUTURE = "B";
    protected final String PERIOD_STATUS_PAST = "C";
    
    public static final int REGISTRATION_NOT_REQUIRED = 0; 
	public static final int REGISTRATION_REQUIRED = 1;
	public static final int REGISTRATION_ALR_REGISTERED =2;
	
	public static final String CAMPAIGN_ID = "campaignID";
	public static final String CAMPAIGN_DESC = "campaignDesc";
	public static final String CAMPAIGN_REG_STATUS = "campaignRegStatus";
	public static final String CAMPAIGN_START_DATE = "campaignStartDate";
	public static final String CAMPAIGN_END_DATE = "campaignEndDate";
	public static final String CAMPAIGN_REG_ON = "campaignRegOn";
	

	protected String campaignID;
	protected String description;
	protected String type;
	protected String typeDescription;
	protected String message;
	protected boolean isPublic = true;
	protected boolean isValid = false;
	protected boolean isEnteredManually = false;
	protected String campaignReferenceType;
	protected String scenario;
    protected String campaignText;               // long text of the Campaign
    protected boolean isEnrollmAllowed = false;  // is enrollment allowed
    protected String enrollmStartDate;           // enrollment start date
    protected String enrollmEndDate;             // enrollment end date
    protected String plannedStartDate;           // planned start date of the campaign
    protected String plannedEndDate;             // planned end date of the campaign
    protected String enrollerGuid = null;        // Guid of the enroller
    protected String enrollmPeriodStatus;        // enrollment period status: past, future, present
    protected int registrationStatus;            // registration status (used for loyalty campaigns)
    
	public String getCampaignID() {
		return this.campaignID;
	}

	public void setCampaignID(String campaignID) {
		this.campaignID = campaignID;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeDescription() {
		return this.typeDescription;
	}

	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}

	/**
	 * Callback method for the <code>BusinessObjectManager</code> to tell
	 * the object that life is over and that it has to release
	 * all ressources.
	 */
	public void destroy() {
		super.destroy();
	}

	/**
	 * Returns the string representation of the object.
	 * @retun String representation
	 */
	public String toString() {
		return this.typeDescription + this.campaignID + this.description;
	}


	public boolean isPublic() {
		return isPublic;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isEnteredManually() {
		return isEnteredManually;
	}

	public void setEnteredManually(boolean isEnteredManually) {
		this.isEnteredManually = isEnteredManually;
	}

	public String getRefType() {
		return campaignReferenceType;
	}

	public void setRefType(String campaignReferenceType) {
		this.campaignReferenceType = campaignReferenceType;
	}
	
	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}	
    
    /**
     * Method sets the long text of the Campaign
     *
     * @param text as String
     */
    public void setCampaignText(String text) {
       this.campaignText = text;
    }
     
    /**
     * Method returns the long text of the Campaign
     *
     * @return text as String
     */
    public String getCampaignText() {
        return this.campaignText;
    }
    
    /**
     * Method sets the enrollment start date
     *
     * @param date as String
     */
    public void setEnrollmStartDate(String date) {
        this.enrollmStartDate = date;
    }
     
    /**
     * Method returns the enrollment start date
     *
     * @return date as String
     */
    public String getEnrollmStartDate() {
        return this.enrollmStartDate;
    }
    
    /**
     * Method sets the enrollment end date
     *
     * @param date as String
     */
    public void setEnrollmEndDate(String date) {
        this.enrollmEndDate = date;
    }
     
    /**
     * Method returns the enrollment end date
     *
     * @return date as String
     */
    public String getEnrollmEndDate() {
        return this.enrollmEndDate;
    }
    
    /**
     * Method sets the planned start date of the campaign
     *
     * @param date as String
     */
    public void setPlannedStartDate(String date) {
        this.plannedStartDate = date;
    }
     
    /**
     * Method returns the planned start date of the campaign
     *
     * @return date as String
     */
    public String getPlannedStartDate() {
        return this.plannedStartDate;
    }
    
    /**
     * Method sets the planned end date of the campaign
     *
     * @param date as String
     */
    public void setPlannedEndDate(String date) {
        this.plannedEndDate = date;
    }
     
    /**
     * Method returns the planned end date of the campaign
     *
     * @return date as String
     */
    public String getPlannedEndDate() {
        return this.plannedEndDate;
    }

    /**
     * Method sets the flag if enrollment is allowed for this campaign
     *
     * @param enrollmentAllowed as boolean
     */
    public void setIsEnrollmAllowed(boolean enrollmentAllowed) {
        this.isEnrollmAllowed = enrollmentAllowed;
    }
     
    /**
     * Method returns the flag if enrollment is allowed for this campaign
     *
     * @return enrollmentAllowed as boolean
     */
    public boolean setIsEnrollmAllowed() {
        return this.isEnrollmAllowed;
    }

    /**
     * Method sets the enroller Guid 
     *
     * @param enrollerGuid as String
     */
    public void setEnrollerGuid(String enrollerGuid) {
        this.enrollerGuid = enrollerGuid;
    }
     
    /**
     * Method returns the Guid of the enroller
     *
     * @return enrollerGuid as String
     */
    public String getEnrollerGuid() {
        return this.enrollerGuid;
    }

    /**
     * Method returns the information if the enrollment period is in the past
     *
     * @param the date format taken from the shop
     * @return isInPast as boolean, true if enrollment period is closed
     */
    public boolean isEnrollmPeriodInPast(String dateFormat) {
        return this.enrollmPeriodStatus.equals(this.PERIOD_STATUS_PAST);         
    }

    /**
     * Method returns the information if the enrollment period is in the future
     *
     * @param the date format taken from the shop
     * @return isInFuture as boolean, true if enrollment period is closed
     */
    public boolean isEnrollmPeriodInFuture(String dateFormat) {
        return this.enrollmPeriodStatus.equals(this.PERIOD_STATUS_FUTURE);         
    }

    /**
     * @return isEnrollmPeriodOpen as boolean, flag if enrollment period is currently open
     */
    public boolean isEnrollmPeriodOpen() {
        return this.enrollmPeriodStatus.equals(this.PERIOD_STATUS_PRESENT);
    }

    /**
     * @param enrollmPeriodeStatus as String, the Status indicates if enrollment period is in the past, 
     *        in the future or in the present.
     */
    public void setEnrollmPeriodStatus(String enrollmPeriodeStatus) {
        this.enrollmPeriodStatus = enrollmPeriodeStatus;
    }

	/**
	 * method returns the registration status: 
	 *    0 -> REGISTRATION_NOT_REQUIRED 
	 *    1 -> REGISTRATION_REQUIRED
	 *    2 -> REGISTRATION_ALR_REGISTERED  
	 * @return registrationStatus as int
	 */
	public int getRegistrationStatus() {
		return registrationStatus;
	}

	/**
	 * method sets the registration status: 
	 *    0 -> REGISTRATION_NOT_REQUIRED 
	 *    1 -> REGISTRATION_REQUIRED
	 *    2 -> REGISTRATION_ALR_REGISTERED  
	 * @param registrationStatus as int
	 */
	public void setRegistrationStatus(int i) {
		registrationStatus = i;
	}

}
