/*****************************************************************************
	Class         CampaignEnrollment
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      28.11.2006
	Version:      1.0

	$Revision: #01 $
	$Date: 2006/11/28 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import com.sap.ecommerce.boi.campaign.CampaignEnrollmentData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * 
 * CampaignEnrollment is a data container for an enrollment line of campaign
 * In an enrollment line the information is stored if a business partner (enrollee) 
 * is enrolled for a campaign. 
 * The enrollment line contains also the information who did the enrollment and
 * when the enrollment was changed.    
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class CampaignEnrollment extends BusinessObjectBase implements CampaignEnrollmentData {

	private String enrolleeGuid;
	private String enrolleeDescr = "";
	private String enrolleeId = "";
    private String firstEnrolledBySoldToDescr = "";
    private String firstEnrolledByContactDescr = "";
    private String firstEnrolledDate = "";
    private String firstEnrolledTimestamp = "";
    private boolean isFirstEnrolledInternally = false;
    private String lastChangedBySoldToDescr = "";
    private String lastChangedByContactDescr = "";
    private String lastChangedDate = "";
    private String lastChangedTimestamp = "";
    private boolean isLastChangedInternally = false;
    private boolean isEnrolled = false;
    private boolean isChanged = false;
    
	/**
	 * Constructor
	 */
	public CampaignEnrollment() {
	}

    /**
     * Method sets the GUID of the enrollee. This corresponds to the GUID of the business partner.
     *
     * @param enrolleeGuid as GUID
     */
    public void setEnrolleeGuid(String enrolleeGuid) {
        this.enrolleeGuid = enrolleeGuid;
    }
     
    /**
     * Method returns the GUID of the enrollee. 
     *
     * @return enrolleeGuid as GUID
     */
    public String getEnrolleeGuid() {
        return this.enrolleeGuid;
    }
     
    /**
     * Method sets the description of the enrollee
     *
     * @param description as String
     */
    public void setEnrolleeDescription(String description) {
        if (description != null) {
            this.enrolleeDescr = description;
        }
    }
     
    /**
     * Method returns the description of the enrollee
     *
     * @return description as String
     */
    public String getEnrolleeDescription() {
        return this.enrolleeDescr;
    }
    
    /**
     * Method sets the id of the enrollee. This corresponds to the business partner Id.
     *
     * @param id as String, business partner id of the enrollee
     */
    public void setEnrolleeID(String id) {
        if (id != null) {
            this.enrolleeId = id;
        }
    }
     
    /**
     * Method returns the ID of the enrollee
     *
     * @return id as String, business partner id of the enrollee
     */
    public String getEnrolleeID() {
        return this.enrolleeId; 
    }
 
    /**
     * Method sets the description of the SoldTo of the enroller who did the first enrollment. 
     *
     * @param soldToDescr as String
     */
    public void setFirstEnrolledBySoldTo(String soldToDescr) {
        if (soldToDescr != null) {
            this.firstEnrolledBySoldToDescr = soldToDescr;
        }
    }
     
    /**
     * Method returns the description of the SoldTo of the enroller who did the first enrollment. 
     *
     * @return soldToDescr as String
     */
    public String getFirstEnrolledBySoldTo() {
        return this.firstEnrolledBySoldToDescr; 
    }
 
    /**
     * Method sets the description of the contact person of the enroller who did the first enrollment.  
     * The contact person is derived from the user.
     *
     * @param contactDescr as String
     */
    public void setFirstEnrolledByContact(String contactDescr) {
        if (contactDescr != null) {
            this.firstEnrolledByContactDescr = contactDescr;
        }
    }
     
    /**
     * Method returns the description of the contact person of the enroller who did the first enrollment. 
     *
     * @return contactDescr as String
     */
    public String getFirstEnrolledByContact() {
        return this.firstEnrolledByContactDescr;
    }
 
    /**
     * Method sets the date of the first enrollment.  
     *
     * @param date as String, first enrollment date
     */
    public void setFirstEnrolledDate(String date) {
        if (date != null) {
            this.firstEnrolledDate = date;
        }
    }
     
    /**
     * Method returns the date of the first enrollment. 
     *
     * @return date as String, first enrollment date
     */
    public String getFirstEnrolledDate() {
        return this.firstEnrolledDate; 
    }
 
    /**
     * Method sets the timestamp of the first enrollment.  
     *
     * @param timestamp as String, first enrollment date as timestamp
     */
    public void setFirstEnrolledTimestamp(String timestamp) {
        if (timestamp != null) {
            this.firstEnrolledTimestamp = timestamp;
        }
    }
     
    /**
     * Method returns the timestamp of the first enrollment. 
     *
     * @return timestamp as String, first enrollment date as timestamp
     */
    public String getFirstEnrolledTimestamp() { 
        return this.firstEnrolledTimestamp; 
    }
 
    /**
     * Method sets the description of the SoldTo of the enroller who did the last change. 
     *
     * @param soldToDescr as String
     */
    public void setLastChangedBySoldTo(String soldToDescr) {
        if (soldToDescr != null) {
            this.lastChangedBySoldToDescr = soldToDescr;
        }
    }
    
     
    /**
     * Method returns the description of the SoldTo of the enroller who did the last change. 
     *
     * @return soldToDescr as String
     */
    public String getLastChangedBySoldTo() {
        return this.lastChangedBySoldToDescr; 
    }
 
    /**
     * Method sets the description of the contact person of the enroller who did the last change.  
     * The contact person is derived from the user.
     *
     * @param contactDescr as String
     */
    public void setLastChangedByContact(String contactDescr) {
        if (contactDescr != null) {
            this.lastChangedByContactDescr = contactDescr;
        }
    }
     
    /**
     * Method returns the description of the contact person of the enroller who did the last change. 
     *
     * @return contactDescr as String
     */
    public String getLastChangedByContact() {
        return this.lastChangedByContactDescr;
    }
 
    /**
     * Method sets the date of the last change.  
     *
     * @param date as String, last change date
     */
    public void setLastChangedDate(String date) {
        if (date != null) {
            this.lastChangedDate = date;
        }
    }
     
    /**
     * Method returns the date of the last change.  
     *
     * @return date as String, last change date
     */
    public String getLastChangedDate() {
        return this.lastChangedDate; 
    }
 
    /**
     * Method sets the timestamp of the last change.  
     *
     * @param timestamp as String, last change date as timestamp
     */
    public void setLastChangedTimestamp(String timestamp) {
        if (timestamp != null) {
            this.lastChangedTimestamp = timestamp;
        }
    }
     
    /**
     * Method returns the timestamp of the last change.  
     *
     * @return timestamp as String, last change date as timestamp
     */
    public String getLastChangedTimestamp() { 
        return this.lastChangedTimestamp; 
    }
 
    /**
     * Method sets the enrollment flag.  
     *
     * @param enrolledFlag as boolean
     */
    public void setIsEnrolled(boolean enrolledFlag) {
        this.isEnrolled = enrolledFlag;
    }
     
    /**
     * Method returns the enrollment flag. 
     *
     * @return enrolledFlag as boolean
     */
    public boolean isEnrolled() {
        return this.isEnrolled; 
    }
 
    /**
     * Method sets the flag indicating if the first enrollment was done internally.  
     *
     * @param enrolledIntFlag as boolean
     */
    public void setIsFirstEnrolledInternally(boolean enrolledIntFlag) {
        this.isFirstEnrolledInternally = enrolledIntFlag;
    }
     
    /**
     * Method returns the flag indicating if the first enrollment was done internallyflag. 
     *
     * @return enrolledIntFlag as boolean
     */
    public boolean isFirstEnrolledInternally() {
        return this.isFirstEnrolledInternally; 
    }
 
    /**
     * Method sets the flag indicating if the last change was done internally.  
     *
     * @param enrolledIntFlag as boolean
     */
    public void setIsLastChangedInternally(boolean enrolledIntFlag) {
        this.isLastChangedInternally = enrolledIntFlag;
    }
     
    /**
     * Method returns the flag indicating if the last change was done internallyflag. 
     *
     * @return enrolledIntFlag as boolean
     */
    public boolean isLastChangedInternally() {
        return this.isLastChangedInternally; 
    }
 
    /**
     * Method sets the flag indicating if the enrollment was changed.  
     */
    public void setIsChanged() {
        this.isChanged = true;
    }
     
    /**
     * Method returns the flag indicating if the enrollment was changed. 
     *
     * @return isChanged as boolean
     */
    public boolean isChanged() {
        return this.isChanged; 
    }

    /**
     * Method returns the flag indicating if an enrollment was deleted internally. 
     *
     * @return isDeletedInternally as boolean
     */
    public boolean isDeletedInternally() {
        return (isLastChangedInternally() && !isEnrolled()); 
    }

    /**
     * Method returns the flag indicating if an enrollment was changed after the first enrollment.
     * The timestamp of the first enrollment is compared with the timestamp of the last change.
     *
     * @return isChangedAfterFirstEnrollment as boolean; true if enrollment was changed after the first enrollment
     */
    public boolean isChangedAfterFirstEnrollment() {
        boolean retval = !(this.lastChangedTimestamp.equals(this.firstEnrolledTimestamp));
        return retval;
    }

}
