/*****************************************************************************
	Class         CampaignSearchCriteria
	Copyright (c) 2006, SAP AG, Germany, All rights reserved.
	Author:       SAP
	Created:      28.11.2006
	Version:      1.0

	$Revision: #01 $
	$Date: 2006/11/28 $
*****************************************************************************/
package com.sap.ecommerce.businessobject.campaign;

import com.sap.ecommerce.boi.campaign.CampaignSearchCriteriaData;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * 
 * CampaignSearchCriteria is a data container used for the search criteria
 * 
 * @author  SAP AG
 * @version 1.0
*/
public class CampaignSearchCriteria extends BusinessObjectBase implements CampaignSearchCriteriaData {

	protected String campaignID;            // campaign id
	protected boolean searchBpHier = false; // flag controlling the intersection of the 
                                            // business partner hierarchy with the campaign 
                                            // TRUE, the whole business partner hierarchy is searched
    protected String textType;              // text type for long text of the Campaign
    protected String enrollerGuid;          // Guid of the enroller
    protected String enrollerId;            // Id of the enroller
    protected String enrolleeGuid;          // Guid of the enrollee
    protected String enrolleeId;            // Id of the enrollee
    protected String country;               // country
    
    /**
     * Method sets the campaign ID
     *
     * @param campaignID as String
     */
	public void setCampaignID(String campaignID) {
		this.campaignID = campaignID;
	}

    /**
     * Method returns the campaign ID
     *
     * @return campaignID as String
     */
    public String getCampaignID() {
        return this.campaignID;
    }

    /**
     * Method sets the flag controlling if the complete business partner hierarchy 
     * of the enroller is searched.
     *
     * @param searchBpHier as boolean
     */
    public void setSearchForBpHier(boolean searchBpHier) {
        this.searchBpHier = searchBpHier;
    }
     
    /**
     * Method returns the search control flag.
     *
     * @return searchBpHier as boolean
     */
    public boolean getSearchForBpHier() {
        return this.searchBpHier;
    }
    
    /**
     * Method sets the Guid of the enroller. This corresponds to the GUID 
     * of the business partner. 
     *
     * @param enrollerGuid as String
     */
    public void setEnrollerGuid(String enrollerGuid) {
        this.enrollerGuid = enrollerGuid;
    }
     
    /**
     * Method returns the Guid of the enroller. 
     *
     * @return enrollerGuid as String
     */
    public String getEnrollerGuid() {
        return this.enrollerGuid;
    }
    
    /**
     * Method sets the id of the enroller. This corresponds to the id 
     * of the business partner. 
     *
     * @param enrollerId as String
     */
    public void setEnrollerId(String enrollerId) {
        this.enrollerId = enrollerId;
    }
     
    /**
     * Method returns the id of the enroller. 
     *
     * @return enrollerId as String
     */
    public String getEnrollerId() {
        return this.enrollerId;
    }
    
    /**
     * Method sets the Guid of the enrollee. This corresponds to the GUID 
     * of the business partner. 
     *
     * @param enrolleeTechKey as String
     */
    public void setEnrolleeGuid(String enrolleeGuid) {
        this.enrolleeGuid = enrolleeGuid;
    }
     
    /**
     * Method returns the Guid of the enrollee. 
     *
     * @return enrolleeGuid as String
     */
    public String getEnrolleeGuid() {
        return this.enrolleeGuid;
    }
    
    /**
     * Method sets the id of the enrollee. This corresponds to the id 
     * of the business partner. 
     *
     * @param enrolleeId as String
     */
    public void setEnrolleeId(String enrolleeId) {
        this.enrolleeId = enrolleeId;
    }
     
    /**
     * Method returns the id of the enrollee. 
     *
     * @return enrolleeId as String
     */
    public String getEnrolleeId() {
        return this.enrolleeId;
    }
    
    /**
     * Method sets the text type for the long text of the Campaign.
     *
     * @param textType as String
     */
    public void setTextTypeCampDescr(String textType) {
        this.textType = textType;
    }
     
    /**
     * Method returns the text type for the long text of the Campaign. 
     *
     * @return textType as String
     */
    public String getTextTypeCampDescr() {
        return this.textType;
    }

    /**
     * Method sets the country
     *
     * @param country as String
     */
    public void setCountry(String country) {
        this.country = country;
    }
     
    /**
     * Method returns the country
     *
     * @return country as String
     */
    public String getCountry() {
        return this.country;
    }

}
