package com.sap.ecommerce.businessobject.loyalty;

import java.util.HashMap;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

public final class LoyaltyProgram extends BusinessObjectBase {

    private String programId = null; /* draft */
	private String progDescr = null; /* draft */
    
    // point codes
    private HashMap pointCodes = null;

    /**
     * Constructor
     */
    LoyaltyProgram () {
        super();
    }
    
    /**
     * Constructor
     */
    public LoyaltyProgram (String newProgramId, String newProgDescr) {
        this.programId = newProgramId;
        this.progDescr = newProgDescr;
    }

    public LoyaltyProgram (TechKey programGuid, String programId) {
    	this.techKey = programGuid;
    	this.programId = programId;   	
    }
    
    public String getProgDescr() {
        return progDescr;
    }

    public String getProgramId() {
        return programId;
    }

    public void setProgDescr(String string) {
        progDescr = string;
    }

    public void setProgramId(String string) {
        programId = string;
    }

    public HashMap getPointCodes() {
        return pointCodes;
    }

    public void setPointCodes(HashMap map) {
        pointCodes = map;
    }

}
