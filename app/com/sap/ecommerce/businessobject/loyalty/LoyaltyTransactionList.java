package com.sap.ecommerce.businessobject.loyalty;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import com.sap.ecommerce.boi.loyalty.LoyaltyTransactionData;
import com.sap.ecommerce.boi.loyalty.LoyaltyTransactionListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.Iterable;

public class LoyaltyTransactionList
	extends BusinessObjectBase
	implements Iterable, LoyaltyTransactionListData {

	protected ArrayList transactionList = null;
	protected String fromTransaction = null;
	protected String toTransaction = null;

	/**
		 * constructor for the loyalty transaction list
		 */
	public LoyaltyTransactionList() {
		transactionList = new ArrayList();
	}

	/**
	 * sets a loyalty transaction list
	 * @param loyaltyTransactionList, the loyalty transaction list to be setted
	 */
	public void addLoyaltyTransactionList(LoyaltyTransactionListData loyaltyTransactionList) {
		final String METHOD = "addLoyaltyTransactionList(LoyaltyTransactionListData loyaltyTransactionList)";
		log.entering(METHOD);

		transactionList = new ArrayList();
		if (loyaltyTransactionList != null) {
			Iterator it = loyaltyTransactionList.iterator();
			while (it.hasNext()) {
				transactionList.add((LoyaltyTransaction) it.next());
			}
		}

		log.exiting();
	}

	/**
	 * adds a loyalty transaction to the loyalty transaction list
	 * @param loyaltyTransaction, the loyalty transaction to be added
	 */
	public void addLoyaltyTransaction(LoyaltyTransactionData loyaltyTransaction) {

		final String METHOD = "addLoyaltyTransaction(addLoyaltyTransaction addLoyaltyTransaction)";
		log.entering(METHOD);

		if (transactionList == null) {
			transactionList = new ArrayList();
			if (log.isDebugEnabled()) {
				log.debug(METHOD + ": new loyaltyTransaction list created");
			}
		}

		transactionList.add(loyaltyTransaction);

		if (log.isDebugEnabled()) {
			log.debug(METHOD + ": loyaltyTransaction added to list; size = " + size());
		}
		log.exiting();
	}

	/**
	 * iterator for the loyalty transaction list
	 * @return iterator that contains loyalty transaction objects
	 */
	public Iterator iterator() {
		log.entering("iterator()");
		log.exiting();
		if (transactionList != null)
			return transactionList.iterator();
		else
			return null;
	}

	/**
	 * Returns the loyaltyTransaction line of the list
	 * 
	 * @param i of type int, index of loyaltyTransaction line 
	 * @return loyaltyTransaction line of type LoyaltyTransaction 
	 */
	public LoyaltyTransactionData getData(int i) {
		log.entering("getData(int i");

		LoyaltyTransactionData loyaltyTransaction = null;
		if (transactionList != null) {
			loyaltyTransaction =
				(LoyaltyTransactionData) transactionList.get(i);
		}
		log.exiting();
		return loyaltyTransaction;
	}

	/**
	 * Returns the number of transactions in the list
	 * @return count of transactions lines
	 */
	public int size() {
		log.entering("size");
		int size = 0;
		if (transactionList != null) {
			size = transactionList.size();
		}
		log.exiting();
		return size;
	}

	/**
	 * sorts the loyalty transaction list by "attribute"
	 * @param the attribute which occurs the sort
	 */
	public void order(DateFormat dateFormat, String attribute) {
		log.entering("order(String sort)");
		if (transactionList != null) {
			Collections.sort(
				transactionList,
				new LoyaltyTransactionComparator(dateFormat, attribute));
		}
		log.exiting();
	}

	/**
	 * sets the transaction From Date
	 * @param the transaction From Date to be set
	 */
	public void setFromTransactionDate(String fromTransaction) {
		this.fromTransaction = fromTransaction;
	}

	/**
	 * sets the transaction To Date
	 * @param the transaction To Date to be set
	 */
	public void setToTransactionDate(String toTransaction) {
		this.toTransaction = toTransaction;
	}

	/**
	 * returns the transaction From Date
	 * @return the transaction From Date
	 */
	public String getFromTransactionDate() {
		log.entering("getFromTransactionDate()");
		log.exiting();
		return this.fromTransaction;
	}

	/**
	 * returns the transaction To Date
	 * @return the transaction To Date
	 */
	public String getToTransactionDate() {
		log.entering("getToTransactionDate()");
		log.exiting();
		return this.toTransaction;
	}
}
