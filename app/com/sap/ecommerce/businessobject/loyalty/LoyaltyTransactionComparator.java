package com.sap.ecommerce.businessobject.loyalty;

import java.text.DateFormat;
import java.util.Comparator;
import java.util.Date;

import com.sap.isa.core.logging.IsaLocation;

public class LoyaltyTransactionComparator implements Comparator {

  /**
   * Name of the attribute to be used for compare
   */
  private String attribute;
  protected static IsaLocation log =
	  IsaLocation.getInstance(LoyaltyTransactionComparator.class.getName());
private DateFormat dateFormat = null;	  
	  

  /**
   * Create new comparator
   * @param attribute attribute which is key for compare
   */
  public LoyaltyTransactionComparator(DateFormat dateFormat, String attribute) {
	log.entering("LoyaltyTransactionComparator()");
	this.dateFormat = dateFormat;
	this.attribute = attribute;
	log.exiting();
  }

  /**
   * compare two LoyaltyTransaction regarding <code>attribute</code>
   * @param object1 first object to compare, needs to be of class LoyaltyTransaction
   * @param object2 second object to compare, needs to be of class LoyaltyTransaction
   * @return result int which is < 0 if object1 < object2, = 0 if they are equal
   *         and >0 if object1 > object2
   */
  public int compare(Object obj1, Object obj2) {
  	log.entering("compare(Object obj1, Object obj2)");
  	
  	int returnValue = 0;
	LoyaltyTransaction item1 = (LoyaltyTransaction) obj1;
	LoyaltyTransaction item2 = (LoyaltyTransaction) obj2;
	
	if (attribute.equals(LoyaltyTransaction.TRANSACTION_ID)) {
		String s1 = item1.getTransactionId();
		String s2 = item2.getTransactionId();
		returnValue = s1.compareTo(s2);
	}
	else if ( attribute.equals(LoyaltyTransaction.POSTING_DATE) ) {
        returnValue = compareDate(item1.getPostingDate(), item2.getPostingDate());
    }
    else if ( attribute.equals(LoyaltyTransaction.EXPIRATION_DATE) ) {
        returnValue = compareDate(item1.getExpirationDate(), item2.getExpirationDate());
	}
	return returnValue;
  }
  
  /**
   * compare two dates
   * @param s1 first date to compare of type String
   * @param s2 second date to compare of type String
   * @return result int which is < 0 if s1 < s2, = 0 if they are equal
   *         and >0 if s1 > s2
   */
  private int compareDate(String s1, String s2) {
      int returnValue = 0;
      if (s1 == null || s1.length() == 0) {
          if (s2 == null || s2.length() == 0) {
              returnValue = 0;
          }
          else { 
              returnValue = -1;
          }
      }
      else if (s2 == null || s2.length() == 0) {
          returnValue = 1;
      }
      else {
          try {
              Date d1 = dateFormat.parse(s1);
              Date d2 = dateFormat.parse(s2);
              returnValue = d1.compareTo(d2);
          } 
          catch (Exception e) {
              returnValue = 0;
          }
      }
      return returnValue;
  }

}

