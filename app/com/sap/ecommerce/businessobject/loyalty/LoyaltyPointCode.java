package com.sap.ecommerce.businessobject.loyalty;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;

/**
 * The class LoyaltyPointCode is the representation of a point code assigned to a loyalty Program
  *
 * @author SAP AG
 * @version 1.0
*
 */
public class LoyaltyPointCode extends BusinessObjectBase {

    // point code
    private String pointCodeId = null;  
    // language dependent description of point code
	private String descr = null; 

    /**
     * Constructor without attributes 
     */
    public LoyaltyPointCode() {
        super();
    }

    /**
     * Constructor with attributes 
     * @param newPointCode as String
     * @param newDescr as String
     */
    public LoyaltyPointCode(String newPointCode, String newDescr) {
        super(new TechKey(newPointCode));
        this.pointCodeId = newPointCode;
        this.descr = newDescr;
    }

    /**
     * Gets the language dependent description of a point code 
     * @return description as String
     */
    public String getDescr() {
        String descr = this.descr;
        if (descr == null) {
            descr = this.pointCodeId;
        }
        return descr;
    }

    /**
     * Gets the Id of the point code 
     * @return Id as String, Id of the point code 
     */
    public String getPointCodeId() {
        return this.pointCodeId;
    }

    /**
     * Sets the language dependent description of a point code 
     * @param newDescr as String, description of the point code
     */
    public void setDescr(String newDescr) {
        this.descr = newDescr;
    }

    /**
     * Sets the Id of the point code 
     * @param Id as String, Id of the point code 
     */
    public void setPointCodeId(String newPointCodeId) {
        this.pointCodeId = newPointCodeId;
    }

}
