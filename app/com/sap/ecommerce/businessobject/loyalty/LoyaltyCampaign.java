package com.sap.ecommerce.businessobject.loyalty;

import java.text.DateFormat;
import java.util.Date;

 
import com.sap.ecommerce.backend.intf.campaign.CampaignBackend;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
 

public class LoyaltyCampaign extends Campaign {
	
	protected static final String BEO_NAME = "LoyaltyCampaign";

	/**
	 * Backend to the campaign
	 */
	private CampaignBackend backendService;
	
	/**
	 * Retrieves the backend service
	 *
	 * @return backendService Campaign backend service
	 */
	protected CampaignBackend getBackendService() throws BackendException {
		synchronized (this) {
			if (backendService == null) {
				backendService = (CampaignBackend)bem.createBackendBusinessObject(LoyaltyCampaign.BEO_NAME, null);
			}
		}
		return backendService;
	}
}