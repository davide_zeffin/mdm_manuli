package com.sap.ecommerce.businessobject.loyalty;

import com.sap.ecommerce.boi.loyalty.LoyaltyTransactionData;
import com.sap.isa.businessobject.BusinessObjectBase;

public class LoyaltyTransaction	extends BusinessObjectBase implements LoyaltyTransactionData {

	public static final String TRANSACTION_ID  = "TRANSACTION_ID";
	public static final String POSTING_DATE    = "POSTING_DATE";
	public static final String EXPIRATION_DATE = "EXPIRATION_DATE";
	
	
	private String transactionId;
	private String postingDate;
	private String transactionType;
	private String reason;
	private String qualificationType;
	private String miles;
	private String qualifyingMiles;
	private String expirationDate;


	/**
	 * return the loyalty transaction expiration date
	 * 
	 * @return a string that represents the loyalty transaction expiration date
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * return the loyalty transaction miles
	 * 
	 * @return a string that represents the loyalty transaction miles
	 */
	public String getMiles() {
		return miles;
	}

	/**
	 * return the loyalty transaction posting date
	 * 
	 * @return a string that represents the loyalty transaction posting date
	 */
	public String getPostingDate() {
		return postingDate;
	}

	/**
	 * return the loyalty transaction qualification type
	 * 
	 * @return a string that represents the loyalty transaction qualification type
	 */
	public String getQualificationType() {
		return qualificationType;
	}

	/**
	 * return the loyalty transaction qualifying miles
	 * 
	 * @return a string that represents the loyalty transaction qualifying miles
	 */
	public String getQualifyingMiles() {
		return qualifyingMiles;
	}

	/**
	 * return the loyalty transaction reason
	 * 
	 * @return a string that represents the loyalty transaction reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * return the loyalty transaction id
	 * 
	 * @return a string that represents the loyalty transaction id
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * return the loyalty transaction transaction type
	 * 
	 * @return a string that represents the loyalty transaction type
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * sets the loyalty transaction expiration date
	 * 
	 * @param a string that represents the loyalty transaction expiration date
	 */
	public void setExpirationDate(String string) {
		expirationDate = string;
	}

	/**
	 * sets the loyalty transaction miles
	 * 
	 * @param a string that represents the loyalty transaction miles
	 */
	public void setMiles(String string) {
		miles = string;
	}

	/**
	 * sets the loyalty transaction posting date
	 * 
	 * @param a string that represents the loyalty transaction posting date
	 */
	public void setPostingDate(String string) {
		postingDate = string;
	}

	/**
	 * sets the loyalty transaction qualification type
	 * 
	 * @param a string that represents the loyalty transaction qualification type
	 */
	public void setQualificationType(String string) {
		qualificationType = string;
	}

	/**
	 * sets the loyalty transaction qualifiying Miles
	 * 
	 * @param a string that represents the loyalty transaction qualifiying miles
	 */
	public void setQualifyingMiles(String string) {
		qualifyingMiles = string;
	}

	/**
	 * sets the loyalty transaction qualification reason
	 * 
	 * @param a string that represents the loyalty transaction reason
	 */
	public void setReason(String string) {
		reason = string;
	}

	/**
	 * sets the loyalty transaction qualification ID
	 * 
	 * @param a string that represents the loyalty transaction qualification ID
	 */
	public void setTransactionId(String string) {
		transactionId = string;
	}

	/**
	 * sets the loyalty transaction qualification transaction type
	 * 
	 * @param a string that represents the loyalty transaction type
	 */
	public void setTransactionType(String string) {
		transactionType = string;
	}

}
