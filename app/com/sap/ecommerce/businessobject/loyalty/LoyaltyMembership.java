package com.sap.ecommerce.businessobject.loyalty;

import java.text.DateFormat;
import java.util.Date;

import com.sap.ecommerce.backend.intf.loyalty.LoyaltyMembershipBackend;
import com.sap.ecommerce.boi.loyalty.LoyaltyMembershipData;
import com.sap.ecommerce.boi.loyalty.LoyaltyPointAccountData;
import com.sap.ecommerce.boi.loyalty.LoyaltyTransactionListData;
import com.sap.ecommerce.businessobject.campaign.MarketingCampaignList;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;



public final class LoyaltyMembership extends BusinessObjectBase	implements BackendAware, LoyaltyMembershipData, DocumentState {

	static final private IsaLocation loc = IsaLocation.getInstance(LoyaltyMembership.class.getName());

	protected static final String BEO_NAME = "LoyaltyMembership";

	private int state = 0;

    // Membership Id
    private String membershipId;

    // Loyalty Program    
    private LoyaltyProgram loyaltyProgram;

    // Point Code
    private LoyaltyPointAccount pointAccount;

    // loyalty campaigns
    private MarketingCampaignList campaignList = null;

    //loyalty transactions
	private LoyaltyTransactionList loyaltyTransactionList = null;
	
	// loyalty promotion
	private LoyaltyCampaign loyaltyPromotion;


	protected BackendObjectManager bem;

	/**
	 * Backend to the loyalty membership
	 */
	private LoyaltyMembershipBackend backendService;


	/**
	* Sets the BackendObjectManager for the object. This method is used
	* by the object manager to allow the user object interaction with
	* the backend logic. This method is normally not called
	* by classes other than BusinessObjectManager.
	*
	* @param bem Backend object manager to be used
	*/
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}

	/**
	 * Retrieves the backend service
	 *
	 * @return backendService Campaign backend service
	 */
	protected LoyaltyMembershipBackend getBackendService()
		throws BackendException {
		synchronized (this) {
			if (backendService == null) {
				backendService = (LoyaltyMembershipBackend) bem.createBackendBusinessObject(LoyaltyMembership.BEO_NAME,	null);
			}
		}
		return backendService;
	}

	/**
	 * Retrieves the data from the backend and stores the data in this object
	 */
	public LoyaltyTransactionListData getLoyaltyTransactionsFromBackend(String membershipGuid, String pointAccountGuid, DateFormat dateFormat, Date fromDate, Date toDate) {
		final String METHOD_NAME = "getLoyaltyTransactions()";

		log.entering(METHOD_NAME);

		try {
			// get data from back end
			LoyaltyTransactionList list = new LoyaltyTransactionList();
			getBackendService().readLoyaltyTransactionsFromBackend(membershipGuid, pointAccountGuid, dateFormat, fromDate, toDate, list);
			return list;
		} catch (BackendException ex) {
			log.error("getLoyaltyTransactions - The following error occured: ", ex);
		} finally {
			log.exiting();
		}
		return null;
	}
	

	/**
	 * Retrieves the data from the backend and stores the data in this object
	 */
	public MarketingCampaignList getLoyaltyCampaignsFromBackend(String membershipId, String memberGuid, String campaignTextId, DateFormat dateFormat) {
		final String METHOD_NAME = "getLoyaltyCampaigns()";
		log.entering(METHOD_NAME);

		try {
			// get data from back end
			MarketingCampaignList list = new MarketingCampaignList();
			getBackendService().readLoyaltyCampaignsFromBackend(membershipId, memberGuid, campaignTextId, dateFormat, list);
			return list;
		} catch (BackendException ex) {
			log.error("getLoyaltyCampaigns - The following error occured: ", ex);
		} finally {
			log.exiting();
		}
		return null;
	}
	
	public boolean enrollLoyaltyCampaignInBackend(String campaignGuid, String memberGuid){
		final String METHOD_NAME = "enrollCampaignInBackend()";
		log.entering(METHOD_NAME);
		boolean returnValue = true;
		try {
			getBackendService().enrollLoyaltyCampaignInBackend(campaignGuid, memberGuid);
		} catch (BackendException ex) {
			log.error("enrollCampaignInBackend - The following error occured: ", ex);
		} finally {
			log.exiting();
		}
		return returnValue;
	}
	
	/**
	 * Returns the loyalty transaction list
	 */
	public LoyaltyTransactionListData getLoyaltyTransactions() {
		log.entering("getLoyaltyTransactions()");
		log.exiting();
	
		return loyaltyTransactionList;
	}
		
	/**
	 * Returns the loyalty transaction list
	 */
	public MarketingCampaignList getLoyaltyCampaigns() {
		log.entering("getLoyaltyCampaigns()");
		log.exiting();
	
		return campaignList;
	}
		

	/**
	 * Gets the state of the document
	 *
	 * @return the state as described by the constants of this interface
	 */
	public int getState() {
		return state;
	}

	/**
	 * Sets the state of the document
	 *
	 * @param state the state as described by the constants of this interface
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * setts the loyalty transaction list to this loyalty membership
	 * 
	 * @param list, the loyalty transaction list that should be setted
	 */
	public void setLoyaltyTransactionList(LoyaltyTransactionListData list) {
		log.entering("setLoyaltyTransactionList()");
		loyaltyTransactionList = (LoyaltyTransactionList)list;
        log.exiting();
	}
	
	/**
	 * setts the loyalty campaign list to this loyalty membership
	 * 
	 * @param list, the loyalty campaign list that should be setted
	 */
	public void setLoyaltyCampaignList(MarketingCampaignList list) {
		log.entering("setLoyaltyCampaignList()");
		campaignList = list;
        log.exiting();
	}

    /**
     * gets the loyalty membership Id of this loyalty membership
     * 
     * @return membership Id , the loyalty membership Id that should be set
     */
    public String getMembershipId() {
        return this.membershipId;
    }

    /**
     * gets the point account of this loyalty membership
     * 
     * @return point account of this loyalty membership
     */
    public LoyaltyPointAccount getPointAccount() {
        return this.pointAccount;
    }

    /**
     * gets the point account of this loyalty membership
     * 
     * @return point account of this loyalty membership
     */
    public LoyaltyPointAccountData getPointAccountData() {
        if (getPointAccount() == null) {
            setPointAccount(new LoyaltyPointAccount());
        }    
        return (LoyaltyPointAccountData) getPointAccount();
    }

    /**
     * gets the loyalty program of this loyalty membership
     * 
     * @return program, the loyalty program  
     */
    public LoyaltyProgram getLoyaltyProgram() {
        return this.loyaltyProgram;
    }

    /**
     * sets the loyalty membership Id
     * 
     * @param newMembShipId, the loyalty membership Id that should be set
     */
    public void setMembershipId(String newMembShipId) {
        this.membershipId = newMembShipId;
    }

    /**
     * sets the point account of this loyalty membership
     * 
     * @param newPointAccount, the point account of the loyalty membership that should be set
     */
    public void setPointAccount(LoyaltyPointAccount newPointAccount) {
        this.pointAccount = newPointAccount;
    }

    /**
     * sets the loyalty program to this loyalty membership
     * 
     * @param program, the loyalty program that should be set
     */
    public void setLoyaltyProgram(LoyaltyProgram newProgram) {
        this.loyaltyProgram = newProgram;
    }
    
    /**
     * returns true if a membership to the loyalty program exists
     * and fals if not
     * 
     * @return boolean value indication if a membership exists or not
     */
     public boolean exists() {
     	if (this.membershipId != null && this.membershipId.length() > 0) {
     	  return true;
     	} else {
     	  return false; 
     	}
     }
     
     
     
     /**
      * create a new membership for the given member techkey
      * in the backen
      * 
      * @param the technical key of the member  
      */
     public void createNewMembership(BusinessPartner member) 
            throws CommunicationException {

	   final String METHOD = "createNewMembership()";
	   loc.entering(METHOD);
	   
	   if (log.isDebugEnabled())
	   log.debug("member="+member.getTechKey().getIdAsString());
		
		try {
		  getBackendService().createInBackend(this, member);
		}
		catch (BackendException ex) {
		   BusinessObjectHelper.splitException(ex);
		}
		finally {
		  loc.exiting();
		}
       	
     }
     
	/**
	 * Retrieves the point account balance data from the backend and stores the data in this object
	 */
	public LoyaltyPointAccount getLoyaltyPointAccountBalanceFromBackend(String pointType, String pointDescr) {
		final String METHOD_NAME = "getLoyaltyPointAccountBalanceFromBackend()";
		log.entering(METHOD_NAME);

		try {
            if (this.pointAccount == null) {
                this.pointAccount = new LoyaltyPointAccount(pointType, pointDescr);
            }
            // get data from back end
			getBackendService().readLoyaltyPointAccountBalanceFromBackend(this.techKey.getIdAsString(), pointType, this.pointAccount);
			return this.pointAccount;
		} catch (BackendException ex) {
			log.error("getLoyaltyPointAccountBalanceFromBackend - The following error occured: ", ex);
		} finally {
			log.exiting();
		}
		return null;
	}

    /**
     * Retrieves the point account data from the backend and stores the data in this object
     */
    public LoyaltyPointAccount getLoyaltyPointAccountFromBackend(String programGuid, String programId, String pointType) {
        final String METHOD_NAME = "getLoyaltyPointAccountFromBackend()";
        log.entering(METHOD_NAME);

        try {
            if (this.pointAccount == null) {
                this.pointAccount = new LoyaltyPointAccount(pointType, "");
            }
            // get data from back end
            getBackendService().readLoyaltyPointAccountFromBackend(programGuid, programId, pointType, this.pointAccount);
            return this.pointAccount;
        } catch (BackendException ex) {
            log.error("getLoyaltyPointAccountFromBackend - The following error occured: ", ex);
        } finally {
            log.exiting();
        }
        return null;
    }
    
	/**
	 * set the loyalty promotion
	 * 
	 * @param promotion
	 */
	public void setLoyaltyPromotion(LoyaltyCampaign promotion) {
		this.loyaltyPromotion = promotion;
	}
	
	/** 
	 * get the loyalty campaign that promotes the loyalty program
	 * 
	 * @return loyalty campaign that promotes the loyalty program
	 */
	public LoyaltyCampaign getLoyaltyPromotion() {
		return this.loyaltyPromotion;
	}
	
	
	/**
	 * Read the membership data from the backend
	 * 
	 * @param member the business partner that has a membership to the actual loyalty program
	 * @throws CommunicationException
	 */
	public void getMembershipData(BusinessPartner member)
	   throws CommunicationException {
		
		   final String METHOD = "getMembershipData()";
		   loc.entering(METHOD);
			
			try {
			  getBackendService().readFromBackend(this, member.getTechKey());
			}
			catch (BackendException ex) {
			   BusinessObjectHelper.splitException(ex);
			}
			finally {
			  loc.exiting();
			}	
	}
}

