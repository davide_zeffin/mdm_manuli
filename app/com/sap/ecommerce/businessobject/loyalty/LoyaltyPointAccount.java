package com.sap.ecommerce.businessobject.loyalty;

import com.sap.ecommerce.boi.loyalty.LoyaltyPointAccountData;



/**
 * The class LoyaltyPointAccount is the representation of a point account which is assigned to a membership.
 * A point account is an instance of a LoyaltyPointCode. 
  *
 * @author SAP AG
 * @version 1.0
*
 */
public final class LoyaltyPointAccount extends LoyaltyPointCode implements LoyaltyPointAccountData {

    private String accountId = null;
    private String balance = "0.00";
    private String earned = "0.00";
    private String consumed = "0.00";
    private String expired = "0.00";
    
    /**
     * Constructor with attributes 
     * @param newPointCode as String
     * @param newDescr as String
     */
    LoyaltyPointAccount() {
        super();
    }

   /**
    * Constructor with attributes 
    * @param newPointCode as String
    * @param newDescr as String
    */
   public LoyaltyPointAccount(String newPointCode, String newDescr) {
       super(newPointCode, newDescr);
   }

    /**
     * Gets the balance of the point account
     * 
     * @return balance as String
     */

    public String getBalance() {
        return this.balance;
    }

    /**
     * Sets the balance of the point account
     * 
     * @param newBalance as String
     */
    public void setBalance(String newBalance) {
        this.balance = newBalance;
    }

	/**
	 * Gets the consumed points as String
	 * @return consumed points as String
	 */
	public String getConsumed() {
		return this.consumed;
	}

	/**
	 * Gets the earned points as String
	 * @return earned points as String
	 */
	public String getEarned() {
		return this.earned;
	}

	/**
	 * Gets the expired points as String
	 * @return expired points as String
	 */
	public String getExpired() {
		return this.expired;
	}

	/**
 	 * Sets the consumed points as String
 	 * @param consumed points as String 
 	 */
	public void setConsumed(String d) {
		this.consumed = d;
	}

	/**
	 * Sets the earned points as String
	 * @param earned points as String 
	 */
	public void setEarned(String d) {
		this.earned = d;
	}

	/**
	 * Sets the expired points as String
	 * @param expired points as String 
	 */
	public void setExpired(String d) {
		this.expired = d;
	}

    /**
     * Gets the point account id as String
     * @return point account id  as String
     */
    public String getAccountId() {
        return this.accountId;
    }

    /**
     * Sets the point account Id as String
     * @param point account Id as String
     */
    public void setAccountId(String newAccId) {
        this.accountId = newAccId;
    }

}
