package com.customer.client.object;

import java.math.BigDecimal;
import java.util.Iterator;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.rfc.RFCDefaultClient;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultConfiguration;
import com.sap.spc.remote.client.rfc.function.CrmAvCheckApoDoCheck;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.spc.remote.client.util.cfg_ext_price_key;
import com.sap.spc.remote.client.util.ext_configuration;

/**
 *
 */
public class CustomerDefaultConfiguration extends RfcDefaultConfiguration {
	public static int NOT_AVAILABLE = 0;
	public static int AVAILABLE_IN_TWO_WEEKS = 1;
	public static int AVAILABLE = 2;

	CustomerDefaultConfiguration(IPCClient ipcClient, String id) {
		super(ipcClient, id);
	}
	
	public int checkAvailability() {
		Instance tmpInstance;
		Characteristic tmpCharacteristic;
		CharacteristicValue tmpValue;
		tmpInstance = this.getRootInstance();
		tmpCharacteristic = tmpInstance.getCharacteristic("IP_PRODUCT_SELECTION", true);
		if (tmpCharacteristic == null) {
			return NOT_AVAILABLE;
		}else {
			tmpValue = tmpCharacteristic.getValue("BBAND");
			if (tmpValue != null && tmpValue.isAssigned()) {
				return NOT_AVAILABLE;
			}else {
				tmpValue = tmpCharacteristic.getValue("GAS");
				if (tmpValue!= null && tmpValue.isAssigned()) {
					return NOT_AVAILABLE; 
				}else {
					tmpValue = tmpCharacteristic.getValue("TEL");
					if (tmpValue != null && tmpValue.isAssigned()) {
						return AVAILABLE_IN_TWO_WEEKS;
					} else {
						return AVAILABLE;
					}
				}
			}		 
		}
	}
		
/**
 * This method will show you how you could call an RFC function from a customer client object.
 * The coding here will not work right away, since a number of parameter for function module
 * CRM_AV_CHECK_APO_DO_CHECK are missing here, you may call a prepare function module to get
 * these parameter. Also calling the ATP check after every characteristic value change will
 * result in bad performance. ATP data should be cached in shared cache on client side.
 **/
//		private void callATPCheck() {
//	//			declarations
//				 byte[] msgGuid;
//				 Integer msgIndex;
//				 String line;
//				 String apoSystemId;
//				 byte[] guid;
//				 String msgType;
//				 String msgId;
//				 BigDecimal msgNo;
//				 String msgText;
//				 byte[] itemGuid;
//				 String correlationMinCom;
//				 String standardCorrelation;
//				 String beginOfKinds;
//				 String showAtpOverview;
//				 String overviewQuota;
//				 String overviewPreplanning;
//				 String dialogAtpSuggestion;
//				 String dialogDeliverySuggestion;
//				 String dialogEnviromAtp;
//				 String dialogEnviromQuota;
//				 String dialogEnviromPreplanning;
//				 String messagesOutput;
//				 String groupAtpList;
//				 String showHierarAtp;
//				 String endOfKinds;
//				 String readCumulatedRec;
//				 String readSingleRec;
//				 String activateDeliveryScheduling;
//				 String activateTranspScheduling;
//				 String activateIntDelete;
//				 String grpChkSimulationOnly;
//				 String grpChkCompleteDelivery;
//				 String grpChkScheduling;
//				 String supressReadLocations;
//				 String orderItemGuid;
//				 String extOrderNumber;
//				 BigDecimal itemNumber;
//				 String extLocationNumber;
//				 String extLocationType;
//				 String extProductNumber;
//				 String configId;
//				 String context;
//				 String instId;
//				 String objType;
//				 String classType;
//				 String objKey;
//				 String objTxt;
//				 String quantity;
//				 String author;
//				 String quantityUnit;
//				 String complete;
//				 String consistent;
//				 String objectGuid;
//				 String persistId;
//				 String persistIdType;
//				 String parentId;
//				 String partOfNo;
//				 String salesRelevant;
//				 String partOfGuid;
//				 String posex;
//				 String rootId;
//				 String sce;
//				 String kbname;
//				 String kbversion;
//				 String cfginfo;
//				 String kbprofile;
//				 String kblanguage;
//				 String cbaseId;
//				 String cbaseIdType;
//				 String charc;
//				 String charcTxt;
//				 String value;
//				 String valueTxt;
//				 String valueTo;
//				 String valcode;
//				 String vkey;
//				 String factor;
//				 String characteristicFieldName;
//				 String characteristic;
//				 String orderNumber;
//				 byte[] headerGuid;
//				 BigDecimal numberInt;
//				 byte[] product;
//				 String orderedProd;
//				 String atpProfile;
//				 String activityType;
//				 String partDlv;
//				 String confMode;
//				 String structInd;
//				 String locNumber;
//				 String locType;
//				 String actType;
//				 byte[] atpGuid;
//				 String modifyTmpObj;
//				 String batchId;
//				 String dateQtyFixed;
//				 String productKind;
//				 String subLocation;
//				 BigDecimal dlvGroup;
//				 String safetyStockIndicator;
//				 String roundingIndicator;
//				 String overrideLocation;
//				 String overrideLocationType;
//				 String quantityFactor;
//				 String quantityDivisor;
//				 String consolidationLocation;
//				 String consolidationLocationType;
//				 String directShip;
//				 String fromTime;
//				 String toTime;
//				 BigDecimal schedlinNo;
//				 String mode;
//				 String selection;
//				 String charcName;
//				 String valueMax;
//				 String valueCode;
//				 String position;
//				 String ivKindOfMsg;
//				 String ivLinkmask;
//				 String ivLog;
//				 String ivTextFormat;
//				 int exCrmavcheckapodocheck_etMsgLongtypeCounter;
//				 int exCrmavcheckapodocheck_etMsgShorttypeCounter;
//				 int exCrmavcheckapodocheck_etHeaderCounter;
//				 String apoTrGuid;
//				 String tempObjIndicator;
//				 String bopTrguid;
//				 int exCrmavcheckapodocheck_etItemCounter;
//				 byte[] parent;
//				 byte[] anchor;
//				 byte[] previousGuid;
//				 String taxDepartJuris;
//				 String taxDepartReg;
//				 String taxCountry;
//				 String substReason;
//				 String mainitem;
//				 String itmUsage;
//				 String itmTypeUsage;
//				 String rrflg;
//				 String errorFlag;
//				 int exCrmavcheckapodocheck_etItemBtExtensionCounter;
//				 int exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondataCounter;
//				 int exCrmavcheckapodocheck_etItemExtensionCounter;
//				 String productNumber;
//				 String scenario;
//				 String atpRelevant;
//				 String tmpObjMode;
//				 BigDecimal checkProfile;
//				 String crmMsgNo;
//				 String apoRelease;
//				 String apoUpd;
//				 String status1;
//				 String status2;
//				 String atpCategory;
//				 String purchOrg;
//				 String tpdsPlant;
//				 String tpdsLocType;
//				 String tpdsFlag;
//				 String carrierSrc;
//				 String georouteSrc;
//				 String checkedAt;
//				 String firstsLocation;
//				 String firstsLocType;
//				 String facingLocation;
//				 String facingLoctype;
//				 String initiatorProc;
//				 String cdLocation;
//				 String cdLocationType;
//				 String checkedInLuw;
//				 String consLocation;
//				 String consLoctype;
//				 String carrierLocation;
//				 String carrierLoctype;
//				 String georoute;
//				 String noReq;
//				 String processUnit;
//				 String agreement;
//				 BigDecimal agmtItem;
//				 String sourceType;
//				 int exCrmavcheckapodocheck_etReqSchedlinDateCounter;
//				 Integer requirementItemIndex;
//				 String cumulatedResultState;
//				 int exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_schedulingCounter;
//				 String kindOfDate;
//				 String dateFrom;
//				 String dateTo;
//				 String roundedQty;
//				 int exCrmavcheckapodocheck_etSchedlinCounter;
//				 byte[] schedlinGuid;
//				 BigDecimal schedlinHandle;
//				 byte[] parentSdlnGuid;
//				 int exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_schedulingCounter;
//				 String source;
//				 int exCrmavcheckapodocheck_etSubitemCounter;
//				 int exCrmavcheckapodocheck_etSubitemSchedlinCounter;
//				 int exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_schedulingCounter;
//	
//	
//			ext_configuration ext_config = this.toExternal();
//	
//	
//	//			implementation
//	//			get access to function
//			 JCO.Function functionCrmAvCheckApoDoCheck = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CRM_AV_CHECK_APO_DO_CHECK);
//			 JCO.ParameterList im = functionCrmAvCheckApoDoCheck.getImportParameterList();
//			 JCO.ParameterList ex = functionCrmAvCheckApoDoCheck.getExportParameterList();
//			 JCO.ParameterList tbl = functionCrmAvCheckApoDoCheck.getTableParameterList();
//	//			fill import parameters from internal members
//	
//	//			process table imCrmavcheckapodocheck.etMsgLongtype
//	//			TABLE
//			 JCO.Table imCrmavcheckapodocheck_etMsgLongtype = im.getTable(CrmAvCheckApoDoCheck.ET_MSG_LONGTYPE);
//			 for (int i=0; i<1; i++) {
//				 imCrmavcheckapodocheck_etMsgLongtype.appendRow();
//				 imCrmavcheckapodocheck_etMsgLongtype.setValue(msgGuid, CrmAvCheckApoDoCheck.MSG_GUID);		// BYTE, GUID of a CRM Order Object
//				 imCrmavcheckapodocheck_etMsgLongtype.setValue(msgIndex, CrmAvCheckApoDoCheck.MSG_INDEX);		// INT, Natural number
//	    
//	//			process structure imCrmavcheckapodocheck.etMsgLongtypeCrmavcheckapodocheck.text
//				 JCO.Structure imCrmavcheckapodocheck_etMsgLongtypeCrmavcheckapodocheck_text = imCrmavcheckapodocheck_etMsgLongtype.getStructure(CrmAvCheckApoDoCheck.TEXT);
//				 imCrmavcheckapodocheck_etMsgLongtypeCrmavcheckapodocheck_text.setValue(line, CrmAvCheckApoDoCheck.LINE);		// CHAR, Line in documentation text
//			 }
//	
//	//			process table imCrmavcheckapodocheck.etMsgShorttype
//	//			TABLE
//			 JCO.Table imCrmavcheckapodocheck_etMsgShorttype = im.getTable(CrmAvCheckApoDoCheck.ET_MSG_SHORTTYPE);
//			 for (int i=0; i<1; i++) {
//				 imCrmavcheckapodocheck_etMsgShorttype.appendRow();
//				 imCrmavcheckapodocheck_etMsgShorttype.setValue(apoSystemId, CrmAvCheckApoDoCheck.APO_SYSTEM_ID);		// CHAR, Logical Destination (Specified in Function Call)
//				 imCrmavcheckapodocheck_etMsgShorttype.setValue(guid, CrmAvCheckApoDoCheck.GUID);		// BYTE, GUID of a CRM Order Object
//				 imCrmavcheckapodocheck_etMsgShorttype.setValue(msgType, CrmAvCheckApoDoCheck.MSG_TYPE);		// CHAR, Message type: S Success, E Error, W Warning, I Info, A Abort
//				 imCrmavcheckapodocheck_etMsgShorttype.setValue(msgId, CrmAvCheckApoDoCheck.MSG_ID);		// CHAR, Message Class
//				 imCrmavcheckapodocheck_etMsgShorttype.setValue(msgNo, CrmAvCheckApoDoCheck.MSG_NO);		// NUM, Message Number
//				 imCrmavcheckapodocheck_etMsgShorttype.setValue(msgText, CrmAvCheckApoDoCheck.MSG_TEXT);		// CHAR, Message Text
//				 imCrmavcheckapodocheck_etMsgShorttype.setValue(itemGuid, CrmAvCheckApoDoCheck.ITEM_GUID);		// BYTE, GUID of a CRM Order Object
//			 }
//	
//	//			process structure imCrmavcheckapodocheck.isLogicalSwitch
//			 JCO.Structure imCrmavcheckapodocheck_isLogicalSwitch = im.getStructure(CrmAvCheckApoDoCheck.IS_LOGICAL_SWITCH);
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(correlationMinCom, CrmAvCheckApoDoCheck.CORRELATION_MIN_COM);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(standardCorrelation, CrmAvCheckApoDoCheck.STANDARD_CORRELATION);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(beginOfKinds, CrmAvCheckApoDoCheck.BEGIN_OF_KINDS);		// CHAR, Character Field Length 1
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(showAtpOverview, CrmAvCheckApoDoCheck.SHOW_ATP_OVERVIEW);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(overviewQuota, CrmAvCheckApoDoCheck.OVERVIEW_QUOTA);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(overviewPreplanning, CrmAvCheckApoDoCheck.OVERVIEW_PREPLANNING);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(dialogAtpSuggestion, CrmAvCheckApoDoCheck.DIALOG_ATP_SUGGESTION);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(dialogDeliverySuggestion, CrmAvCheckApoDoCheck.DIALOG_DELIVERY_SUGGESTION);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(dialogEnviromAtp, CrmAvCheckApoDoCheck.DIALOG_ENVIROM_ATP);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(dialogEnviromQuota, CrmAvCheckApoDoCheck.DIALOG_ENVIROM_QUOTA);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(dialogEnviromPreplanning, CrmAvCheckApoDoCheck.DIALOG_ENVIROM_PREPLANNING);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(messagesOutput, CrmAvCheckApoDoCheck.MESSAGES_OUTPUT);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(groupAtpList, CrmAvCheckApoDoCheck.GROUP_ATP_LIST);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(showHierarAtp, CrmAvCheckApoDoCheck.SHOW_HIERAR_ATP);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(endOfKinds, CrmAvCheckApoDoCheck.END_OF_KINDS);		// CHAR, Character Field Length 1
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(readCumulatedRec, CrmAvCheckApoDoCheck.READ_CUMULATED_REC);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(readSingleRec, CrmAvCheckApoDoCheck.READ_SINGLE_REC);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(activateDeliveryScheduling, CrmAvCheckApoDoCheck.ACTIVATE_DELIVERY_SCHEDULING);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(activateTranspScheduling, CrmAvCheckApoDoCheck.ACTIVATE_TRANSP_SCHEDULING);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(activateIntDelete, CrmAvCheckApoDoCheck.ACTIVATE_INT_DELETE);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(grpChkSimulationOnly, CrmAvCheckApoDoCheck.GRP_CHK_SIMULATION_ONLY);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(grpChkCompleteDelivery, CrmAvCheckApoDoCheck.GRP_CHK_COMPLETE_DELIVERY);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(grpChkScheduling, CrmAvCheckApoDoCheck.GRP_CHK_SCHEDULING);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//			 imCrmavcheckapodocheck_isLogicalSwitch.setValue(supressReadLocations, CrmAvCheckApoDoCheck.SUPRESS_READ_LOCATIONS);		// CHAR, Data element for domain BOOLE: TRUE (='X') and FALSE (=' ')
//	
//	//			process table imCrmavcheckapodocheck.itApolocprodExcl
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itApolocprodExcl = im.getTable(CrmAvCheckApoDoCheck.IT_APOLOCPROD_EXCL);
//			 for (int i=0; i<1; i++) {
//				 imCrmavcheckapodocheck_itApolocprodExcl.appendRow();
//				 imCrmavcheckapodocheck_itApolocprodExcl.setValue(orderItemGuid, CrmAvCheckApoDoCheck.ORDER_ITEM_GUID);		// CHAR, ATP: Encryption of DELNR and DELPS
//				 imCrmavcheckapodocheck_itApolocprodExcl.setValue(extOrderNumber, CrmAvCheckApoDoCheck.EXT_ORDER_NUMBER);		// CHAR, MRP element number
//				 imCrmavcheckapodocheck_itApolocprodExcl.setValue(itemNumber, CrmAvCheckApoDoCheck.ITEM_NUMBER);		// NUM, MRP element item
//				 imCrmavcheckapodocheck_itApolocprodExcl.setValue(extLocationNumber, CrmAvCheckApoDoCheck.EXT_LOCATION_NUMBER);		// CHAR, Location number in APO
//				 imCrmavcheckapodocheck_itApolocprodExcl.setValue(extLocationType, CrmAvCheckApoDoCheck.EXT_LOCATION_TYPE);		// CHAR, APO location type
//				 imCrmavcheckapodocheck_itApolocprodExcl.setValue(extProductNumber, CrmAvCheckApoDoCheck.EXT_PRODUCT_NUMBER);		// CHAR, Material number in APO
//			 }
//	
//	//			process table imCrmavcheckapodocheck.itCfgsBlob
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itCfgsBlob = im.getTable(CrmAvCheckApoDoCheck.IT_CFGS_BLOB);
//			 for (int i=0; i<1; i++) {
//				 imCrmavcheckapodocheck_itCfgsBlob.appendRow();
//				 imCrmavcheckapodocheck_itCfgsBlob.setValue(configId, CrmAvCheckApoDoCheck.CONFIG_ID);		// CHAR, External Configuration ID (Temporary)
//				 imCrmavcheckapodocheck_itCfgsBlob.setValue(context, CrmAvCheckApoDoCheck.CONTEXT);		// CHAR, CU: Configuration BLOB (SCE)
//			 }
//	
//	//			process table imCrmavcheckapodocheck.itCfgsInst
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itCfgsInst = im.getTable(CrmAvCheckApoDoCheck.IT_CFGS_INST);
//			 for (Iterator instIt = ext_config.get_insts().values().iterator(); instIt.hasNext(); ) {
//				 cfg_ext_inst currentInstance = (cfg_ext_inst)instIt.next();
//				 imCrmavcheckapodocheck_itCfgsInst.appendRow();
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(configId, CrmAvCheckApoDoCheck.CONFIG_ID);		// CHAR, External Configuration ID (Temporary)
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.get_inst_id(), CrmAvCheckApoDoCheck.INST_ID);		// CHAR, Instance Number in Configuration
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.get_obj_type(), CrmAvCheckApoDoCheck.OBJ_TYPE);		// CHAR, CUIB: External Type of (Referencing) Object
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.get_class_type(), CrmAvCheckApoDoCheck.CLASS_TYPE);		// CHAR, Class Type
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.get_obj_key(), CrmAvCheckApoDoCheck.OBJ_KEY);		// CHAR, CUIB: Key of Referencing Object
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.get_obj_txt(), CrmAvCheckApoDoCheck.OBJ_TXT);		// CHAR, Language-Dependent Object Description
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.get_quantity(), CrmAvCheckApoDoCheck.QUANTITY);		// CHAR, Instance Quantity
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.get_author(), CrmAvCheckApoDoCheck.AUTHOR);		// CHAR, Statement was Inferred
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.get_quantity_unit(), CrmAvCheckApoDoCheck.QUANTITY_UNIT);		// CHAR, Unit of Measure
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.is_complete_p() ? TRUE : FALSE, CrmAvCheckApoDoCheck.COMPLETE);		// CHAR, General Indicator
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(currentInstance.is_consistent_p() ? TRUE : FALSE, CrmAvCheckApoDoCheck.CONSISTENT);		// CHAR, General Indicator
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(objectGuid, CrmAvCheckApoDoCheck.OBJECT_GUID);		// CHAR, GUID for TYPE_OF Statement of Instance
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(persistId, CrmAvCheckApoDoCheck.PERSIST_ID);		// CHAR, Instance Number (Persistent)
//				 imCrmavcheckapodocheck_itCfgsInst.setValue(persistIdType, CrmAvCheckApoDoCheck.PERSIST_ID_TYPE);		// CHAR, Type of Instance Number (Persistent)
//	
//	
//			//		process table imCrmavcheckapodocheck_itCfgsPartOf
//			//		optional, TABLE
//				 JCO.Table imCrmavcheckapodocheck_itCfgsPartOf = im.getTable(CrmAvCheckApoDoCheck.IT_CFGS_PART_OF);
//				 for (Iterator prtIt = currentInstance.get_parts().iterator(); prtIt.hasNext(); ) {
//					 cfg_ext_part part = (cfg_ext_part)prtIt.next();
//					 imCrmavcheckapodocheck_itCfgsPartOf.appendRow();
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(configId, CrmAvCheckApoDoCheck.CONFIG_ID);		// CHAR, External Configuration ID (Temporary)
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(part.get_parent_id(), CrmAvCheckApoDoCheck.PARENT_ID);		// CHAR, Instance Number in Configuration
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(part.get_inst_id(), CrmAvCheckApoDoCheck.INST_ID);		// CHAR, Instance Number in Configuration
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(part.get_pos_nr(), CrmAvCheckApoDoCheck.PART_OF_NO);		// CHAR, Part_of Item Number
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(part.get_obj_type(), CrmAvCheckApoDoCheck.OBJ_TYPE);		// CHAR, CUIB: External Type of (Referencing) Object
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(part.get_class_type(), CrmAvCheckApoDoCheck.CLASS_TYPE);		// CHAR, Class Type
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(part.get_obj_key(), CrmAvCheckApoDoCheck.OBJ_KEY);		// CHAR, CUIB: Key of Referencing Object
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(part.get_author(), CrmAvCheckApoDoCheck.AUTHOR);		// CHAR, Statement was Inferred
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(part.is_sales_relevant_p() ? TRUE : FALSE, CrmAvCheckApoDoCheck.SALES_RELEVANT);		// CHAR, Part is Sales-Relevant
//					 imCrmavcheckapodocheck_itCfgsPartOf.setValue(partOfGuid, CrmAvCheckApoDoCheck.PART_OF_GUID);		// CHAR, GUID for PART_OF Statement for Instance
//				 }
//	
//	//			process table imCrmavcheckapodocheck_itCfgsValue
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itCfgsValue = im.getTable(CrmAvCheckApoDoCheck.IT_CFGS_VALUE);
//			 for (Iterator valIt = currentInstance.get_cstics_values().iterator(); valIt.hasNext();) {
//				cfg_ext_cstic_val val = (cfg_ext_cstic_val)valIt.next();
//				 imCrmavcheckapodocheck_itCfgsValue.appendRow();
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(configId, CrmAvCheckApoDoCheck.CONFIG_ID);		// CHAR, External Configuration ID (Temporary)
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(val.get_inst_id(), CrmAvCheckApoDoCheck.INST_ID);		// CHAR, Instance Number in Configuration
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(val.get_charc(), CrmAvCheckApoDoCheck.CHARC);		// CHAR, Characteristic Name
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(val.get_charc_txt(), CrmAvCheckApoDoCheck.CHARC_TXT);		// CHAR, Language-Dependent Characteristic Description
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(val.get_value(), CrmAvCheckApoDoCheck.VALUE);		// CHAR, Characteristic Value
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(val.get_value_txt(), CrmAvCheckApoDoCheck.VALUE_TXT);		// CHAR, Language-Dependent Description of Characteristic Value
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(val.get_author(), CrmAvCheckApoDoCheck.AUTHOR);		// CHAR, Statement was Inferred
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(valueTo, CrmAvCheckApoDoCheck.VALUE_TO);		// CHAR, Characteristic Value
//				 imCrmavcheckapodocheck_itCfgsValue.setValue(valcode, CrmAvCheckApoDoCheck.VALCODE);		// CHAR, Value Type: Interval Limits - Single Values
//			 }
//	
//	//			process table imCrmavcheckapodocheck.itCfgsVk
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itCfgsVk = im.getTable(CrmAvCheckApoDoCheck.IT_CFGS_VK);
//			 for (Iterator vkIt = currentInstance.get_price_keys().iterator(); vkIt.hasNext(); ) {
//				cfg_ext_price_key vkKey = (cfg_ext_price_key)vkIt.next();
//				 imCrmavcheckapodocheck_itCfgsVk.appendRow();
//				 imCrmavcheckapodocheck_itCfgsVk.setValue(configId, CrmAvCheckApoDoCheck.CONFIG_ID);		// CHAR, External Configuration ID (Temporary)
//				 imCrmavcheckapodocheck_itCfgsVk.setValue(vkKey.get_inst_id(), CrmAvCheckApoDoCheck.INST_ID);		// CHAR, Instance Number in Configuration
//				 imCrmavcheckapodocheck_itCfgsVk.setValue(vkKey.get_key(), CrmAvCheckApoDoCheck.VKEY);		// CHAR, Variant Condition Name
//				 imCrmavcheckapodocheck_itCfgsVk.setValue(vkKey.get_factor(), CrmAvCheckApoDoCheck.FACTOR);		// CHAR, Weighting Factor for Variant Conditions
//			 }
//	
//			 }
//	
//	
//	//			process table imCrmavcheckapodocheck.itCfgsRef
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itCfgsRef = im.getTable(CrmAvCheckApoDoCheck.IT_CFGS_REF);
//			 for (int i=0; i<1; i++) {
//				 imCrmavcheckapodocheck_itCfgsRef.appendRow();
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(posex, CrmAvCheckApoDoCheck.POSEX);		// CHAR, External Item Number
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(getId(), CrmAvCheckApoDoCheck.CONFIG_ID);		// CHAR, External Configuration ID (Temporary)
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(getRootInstance().getId(), CrmAvCheckApoDoCheck.ROOT_ID);		// CHAR, Instance Number in Configuration
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(isSceMode() ? TRUE : FALSE, CrmAvCheckApoDoCheck.SCE);		// CHAR, CUX: Configuration Type
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(getKnowledgeBase().getName(), CrmAvCheckApoDoCheck.KBNAME);		// CHAR, Knowledge-Base Object
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(getKnowledgeBase().getVersion(), CrmAvCheckApoDoCheck.KBVERSION);		// CHAR, Runtime Version of SCE Knowledge Base
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(isComplete() ? TRUE : FALSE, CrmAvCheckApoDoCheck.COMPLETE);		// CHAR, General Indicator
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(isConsistent() ? TRUE : FALSE, CrmAvCheckApoDoCheck.CONSISTENT);		// CHAR, General Indicator
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(cfginfo, CrmAvCheckApoDoCheck.CFGINFO);		// CHAR, CU: Configuration BLOB (SCE)
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(getActiveKBProfile(), CrmAvCheckApoDoCheck.KBPROFILE);		// CHAR, Knowledge-Base Profile
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(kblanguage, CrmAvCheckApoDoCheck.KBLANGUAGE);		// CHAR, Configuration Language
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(cbaseId, CrmAvCheckApoDoCheck.CBASE_ID);		// CHAR, Instance Number (Persistent)
//				 imCrmavcheckapodocheck_itCfgsRef.setValue(cbaseIdType, CrmAvCheckApoDoCheck.CBASE_ID_TYPE);		// CHAR, Type of Instance Number (Persistent)
//			 }
//	
//	
//	//			process table imCrmavcheckapodocheck.itFieldcat
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itFieldcat = im.getTable(CrmAvCheckApoDoCheck.IT_FIELDCAT);
//			 while (aCondition) {
//				 imCrmavcheckapodocheck_itFieldcat.appendRow();
//				 imCrmavcheckapodocheck_itFieldcat.setValue(extOrderNumber, CrmAvCheckApoDoCheck.EXT_ORDER_NUMBER);		// CHAR, MRP element number
//				 imCrmavcheckapodocheck_itFieldcat.setValue(itemNumber, CrmAvCheckApoDoCheck.ITEM_NUMBER);		// NUM, MRP element item
//				 imCrmavcheckapodocheck_itFieldcat.setValue(characteristicFieldName, CrmAvCheckApoDoCheck.CHARACTERISTIC_FIELD_NAME);		// CHAR, 30 Characters
//				 imCrmavcheckapodocheck_itFieldcat.setValue(characteristic, CrmAvCheckApoDoCheck.CHARACTERISTIC);		// CHAR, Character field of length 40
//			 }
//	
//	//			process table imCrmavcheckapodocheck.itHeader
//	//			TABLE
//			 JCO.Table imCrmavcheckapodocheck_itHeader = im.getTable(CrmAvCheckApoDoCheck.IT_HEADER);
//			 while (aCondition) {
//				 imCrmavcheckapodocheck_itHeader.appendRow();
//				 imCrmavcheckapodocheck_itHeader.setValue(guid, CrmAvCheckApoDoCheck.GUID);		// BYTE, GUID of a CRM Order Object
//				 imCrmavcheckapodocheck_itHeader.setValue(orderNumber, CrmAvCheckApoDoCheck.ORDER_NUMBER);		// CHAR, Transaction ID
//			 }
//	
//	//			process table imCrmavcheckapodocheck_itItem
//	//			TABLE
//			 JCO.Table imCrmavcheckapodocheck_itItem = im.getTable(CrmAvCheckApoDoCheck.IT_ITEM);
//			 while (aCondition) {
//				 imCrmavcheckapodocheck_itItem.appendRow();
//				 imCrmavcheckapodocheck_itItem.setValue(guid, CrmAvCheckApoDoCheck.GUID);		// BYTE, GUID of a CRM Order Object
//				 imCrmavcheckapodocheck_itItem.setValue(headerGuid, CrmAvCheckApoDoCheck.HEADER_GUID);		// BYTE, GUID of a CRM Order Object
//				 imCrmavcheckapodocheck_itItem.setValue(numberInt, CrmAvCheckApoDoCheck.NUMBER_INT);		// NUM, Item Number in Document
//				 imCrmavcheckapodocheck_itItem.setValue(product, CrmAvCheckApoDoCheck.PRODUCT);		// BYTE, Internal Unique ID of Product
//				 imCrmavcheckapodocheck_itItem.setValue(orderedProd, CrmAvCheckApoDoCheck.ORDERED_PROD);		// CHAR, Product Name Entered
//				 imCrmavcheckapodocheck_itItem.setValue(atpProfile, CrmAvCheckApoDoCheck.ATP_PROFILE);		// CHAR, ATP Profile (Reference Customizing for ATP Control in APO)
//				 imCrmavcheckapodocheck_itItem.setValue(activityType, CrmAvCheckApoDoCheck.ACTIVITY_TYPE);		// CHAR, Action Type in Change and Transport System
//				 imCrmavcheckapodocheck_itItem.setValue(partDlv, CrmAvCheckApoDoCheck.PART_DLV);		// CHAR, Delivery Control
//				 imCrmavcheckapodocheck_itItem.setValue(confMode, CrmAvCheckApoDoCheck.CONF_MODE);		// CHAR, Confirmation Mode
//				 imCrmavcheckapodocheck_itItem.setValue(structInd, CrmAvCheckApoDoCheck.STRUCT_IND);		// CHAR, ATP: structure indicator
//				 imCrmavcheckapodocheck_itItem.setValue(locNumber, CrmAvCheckApoDoCheck.LOC_NUMBER);		// CHAR, Char 20
//				 imCrmavcheckapodocheck_itItem.setValue(locType, CrmAvCheckApoDoCheck.LOC_TYPE);		// CHAR, Not More Closely Defined Area, Possibly Used for Patchlevels
//				 imCrmavcheckapodocheck_itItem.setValue(actType, CrmAvCheckApoDoCheck.ACT_TYPE);		// CHAR, ATP: Transaction type for RBA
//				 imCrmavcheckapodocheck_itItem.setValue(atpGuid, CrmAvCheckApoDoCheck.ATP_GUID);		// BYTE, GUID of a CRM Order Object
//				 imCrmavcheckapodocheck_itItem.setValue(modifyTmpObj, CrmAvCheckApoDoCheck.MODIFY_TMP_OBJ);		// CHAR, ATP: Indicator for writing delta records
//				 imCrmavcheckapodocheck_itItem.setValue(batchId, CrmAvCheckApoDoCheck.BATCH_ID);		// CHAR, Batch Number
//				 imCrmavcheckapodocheck_itItem.setValue(dateQtyFixed, CrmAvCheckApoDoCheck.DATE_QTY_FIXED);		// CHAR, Fixed Date and Quantity
//				 imCrmavcheckapodocheck_itItem.setValue(productKind, CrmAvCheckApoDoCheck.PRODUCT_KIND);		// CHAR, Technnical Type of Product
//				 imCrmavcheckapodocheck_itItem.setValue(subLocation, CrmAvCheckApoDoCheck.SUB_LOCATION);		// CHAR, Sublocation
//				 imCrmavcheckapodocheck_itItem.setValue(dlvGroup, CrmAvCheckApoDoCheck.DLV_GROUP);		// NUM, Delivery Group (Items are Delivered Together)
//				 imCrmavcheckapodocheck_itItem.setValue(safetyStockIndicator, CrmAvCheckApoDoCheck.SAFETY_STOCK_INDICATOR);		// CHAR, Parameter-Dependent ATP Safety Stock
//				 imCrmavcheckapodocheck_itItem.setValue(roundingIndicator, CrmAvCheckApoDoCheck.ROUNDING_INDICATOR);		// CHAR, Round Requirement Quantity
//				 imCrmavcheckapodocheck_itItem.setValue(overrideLocation, CrmAvCheckApoDoCheck.OVERRIDE_LOCATION);		// CHAR, Location number in APO
//				 imCrmavcheckapodocheck_itItem.setValue(overrideLocationType, CrmAvCheckApoDoCheck.OVERRIDE_LOCATION_TYPE);		// CHAR, APO location type
//				 imCrmavcheckapodocheck_itItem.setValue(quantityFactor, CrmAvCheckApoDoCheck.QUANTITY_FACTOR);		// BCD, ATP: Quotient Counter for Unit of Measure Conversion
//				 imCrmavcheckapodocheck_itItem.setValue(quantityDivisor, CrmAvCheckApoDoCheck.QUANTITY_DIVISOR);		// BCD, ATP: Quotient Divisor for Unit of Measure Conversion
//				 imCrmavcheckapodocheck_itItem.setValue(consolidationLocation, CrmAvCheckApoDoCheck.CONSOLIDATION_LOCATION);		// CHAR, Location number in APO
//				 imCrmavcheckapodocheck_itItem.setValue(consolidationLocationType, CrmAvCheckApoDoCheck.CONSOLIDATION_LOCATION_TYPE);		// CHAR, APO location type
//				 imCrmavcheckapodocheck_itItem.setValue(directShip, CrmAvCheckApoDoCheck.DIRECT_SHIP);		// CHAR, Direct Delivery Without Consolidation
//			 }
//	
//	//			process table imCrmavcheckapodocheck_itSchedlin
//	//			TABLE
//			 JCO.Table imCrmavcheckapodocheck_itSchedlin = im.getTable(CrmAvCheckApoDoCheck.IT_SCHEDLIN);
//			 while (aCondition) {
//				 imCrmavcheckapodocheck_itSchedlin.appendRow();
//				 imCrmavcheckapodocheck_itSchedlin.setValue(guid, CrmAvCheckApoDoCheck.GUID);		// BYTE, GUID of a CRM Order Object
//				 imCrmavcheckapodocheck_itSchedlin.setValue(itemGuid, CrmAvCheckApoDoCheck.ITEM_GUID);		// BYTE, GUID of a CRM Order Object
//				 imCrmavcheckapodocheck_itSchedlin.setValue(fromTime, CrmAvCheckApoDoCheck.FROM_TIME);		// BCD, Start Time Stamp: Date
//				 imCrmavcheckapodocheck_itSchedlin.setValue(toTime, CrmAvCheckApoDoCheck.TO_TIME);		// BCD, End Time Stamp: Date
//				 imCrmavcheckapodocheck_itSchedlin.setValue(quantity, CrmAvCheckApoDoCheck.QUANTITY);		// BCD, CRM Schedule Line Quantity
//				 imCrmavcheckapodocheck_itSchedlin.setValue(schedlinNo, CrmAvCheckApoDoCheck.SCHEDLIN_NO);		// NUM, Schedule Line Number
//				 imCrmavcheckapodocheck_itSchedlin.setValue(mode, CrmAvCheckApoDoCheck.MODE);		// CHAR, Processing Mode of Transaction
//			 }
//	
//	//			process table imCrmavcheckapodocheck_itSelectiondata
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itSelectiondata = im.getTable(CrmAvCheckApoDoCheck.IT_SELECTIONDATA);
//			 while (aCondition) {
//				 imCrmavcheckapodocheck_itSelectiondata.appendRow();
//				 imCrmavcheckapodocheck_itSelectiondata.setValue(selection, CrmAvCheckApoDoCheck.SELECTION);		// CHAR, Selection ID (Not Persistent)
//				 imCrmavcheckapodocheck_itSelectiondata.setValue(charcName, CrmAvCheckApoDoCheck.CHARC_NAME);		// CHAR, Attribute Name
//				 imCrmavcheckapodocheck_itSelectiondata.setValue(charcTxt, CrmAvCheckApoDoCheck.CHARC_TXT);		// CHAR, Language-Dependent Attribute Description
//				 imCrmavcheckapodocheck_itSelectiondata.setValue(value, CrmAvCheckApoDoCheck.VALUE);		// CHAR, Characteristic Value
//				 imCrmavcheckapodocheck_itSelectiondata.setValue(valueTxt, CrmAvCheckApoDoCheck.VALUE_TXT);		// CHAR, Language-Dependent Description of Characteristic Value
//				 imCrmavcheckapodocheck_itSelectiondata.setValue(valueMax, CrmAvCheckApoDoCheck.VALUE_MAX);		// CHAR, Upper Limit Value of Attribute
//				 imCrmavcheckapodocheck_itSelectiondata.setValue(valueCode, CrmAvCheckApoDoCheck.VALUE_CODE);		// CHAR, Value Type: Interval Limits - Single Value
//			 }
//	
//	//			process table imCrmavcheckapodocheck_itSelectionhead
//	//			optional, TABLE
//			 JCO.Table imCrmavcheckapodocheck_itSelectionhead = im.getTable(CrmAvCheckApoDoCheck.IT_SELECTIONHEAD);
//			 while (aCondition) {
//				 imCrmavcheckapodocheck_itSelectionhead.appendRow();
//				 imCrmavcheckapodocheck_itSelectionhead.setValue(selection, CrmAvCheckApoDoCheck.SELECTION);		// CHAR, Selection ID (Not Persistent)
//				 imCrmavcheckapodocheck_itSelectionhead.setValue(position, CrmAvCheckApoDoCheck.POSITION);		// CHAR, Order Item ID
//			 }
//			 im.setValue(ivKindOfMsg, CrmAvCheckApoDoCheck.IV_KIND_OF_MSG);		// optional, CHAR
//			 im.setValue(ivLinkmask, CrmAvCheckApoDoCheck.IV_LINKMASK);		// optional, CHAR
//			 im.setValue(ivLog, CrmAvCheckApoDoCheck.IV_LOG);		// optional, CHAR
//			 im.setValue(ivTextFormat, CrmAvCheckApoDoCheck.IV_TEXT_FORMAT);		// optional, CHAR
//	
//	
//			 try {
//				((RFCDefaultClient)cachingClient).execute(functionCrmAvCheckApoDoCheck);
//			 }
//			 catch(ClientException e) {
//				 throw new IPCException(e);
//			 }
//	
//	//			fill internal members from export parameters
//	
//	//			process table exCrmavcheckapodocheck_etMsgLongtype
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etMsgLongtype = ex.getTable(CrmAvCheckApoDoCheck.ET_MSG_LONGTYPE);
//			 for (exCrmavcheckapodocheck_etMsgLongtypeCounter=0; exCrmavcheckapodocheck_etMsgLongtypeCounter<exCrmavcheckapodocheck_etMsgLongtype.getNumRows(); exCrmavcheckapodocheck_etMsgLongtypeCounter++) {
//				 exCrmavcheckapodocheck_etMsgLongtype.setRow(exCrmavcheckapodocheck_etMsgLongtypeCounter);
//				 msgGuid = exCrmavcheckapodocheck_etMsgLongtype.getByteArray(CrmAvCheckApoDoCheck.MSG_GUID);
//				 msgIndex = (Integer)exCrmavcheckapodocheck_etMsgLongtype.getValue(CrmAvCheckApoDoCheck.MSG_INDEX);
//	    
//	//			process structure exCrmavcheckapodocheck_etMsgLongtypeCrmavcheckapodocheck_text
//				 JCO.Structure exCrmavcheckapodocheck_etMsgLongtypeCrmavcheckapodocheck_text = exCrmavcheckapodocheck_etMsgLongtype.getStructure(CrmAvCheckApoDoCheck.TEXT);
//				 line = exCrmavcheckapodocheck_etMsgLongtypeCrmavcheckapodocheck_text.getString(CrmAvCheckApoDoCheck.LINE);
//			 }
//	
//	//			process table exCrmavcheckapodocheck.etMsgShorttype
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etMsgShorttype = ex.getTable(CrmAvCheckApoDoCheck.ET_MSG_SHORTTYPE);
//			 for (exCrmavcheckapodocheck_etMsgShorttypeCounter=0; exCrmavcheckapodocheck_etMsgShorttypeCounter<exCrmavcheckapodocheck_etMsgShorttype.getNumRows(); exCrmavcheckapodocheck_etMsgShorttypeCounter++) {
//				 exCrmavcheckapodocheck_etMsgShorttype.setRow(exCrmavcheckapodocheck_etMsgShorttypeCounter);
//				 apoSystemId = exCrmavcheckapodocheck_etMsgShorttype.getString(CrmAvCheckApoDoCheck.APO_SYSTEM_ID);
//				 guid = exCrmavcheckapodocheck_etMsgShorttype.getByteArray(CrmAvCheckApoDoCheck.GUID);
//				 msgType = exCrmavcheckapodocheck_etMsgShorttype.getString(CrmAvCheckApoDoCheck.MSG_TYPE);
//				 msgId = exCrmavcheckapodocheck_etMsgShorttype.getString(CrmAvCheckApoDoCheck.MSG_ID);
//				 msgNo = exCrmavcheckapodocheck_etMsgShorttype.getBigDecimal(CrmAvCheckApoDoCheck.MSG_NO);
//				 msgText = exCrmavcheckapodocheck_etMsgShorttype.getString(CrmAvCheckApoDoCheck.MSG_TEXT);
//				 itemGuid = exCrmavcheckapodocheck_etMsgShorttype.getByteArray(CrmAvCheckApoDoCheck.ITEM_GUID);
//			 }
//	
//	//			process table exCrmavcheckapodocheck_etHeader
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etHeader = ex.getTable(CrmAvCheckApoDoCheck.ET_HEADER);
//			 for (exCrmavcheckapodocheck_etHeaderCounter=0; exCrmavcheckapodocheck_etHeaderCounter<exCrmavcheckapodocheck_etHeader.getNumRows(); exCrmavcheckapodocheck_etHeaderCounter++) {
//				 exCrmavcheckapodocheck_etHeader.setRow(exCrmavcheckapodocheck_etHeaderCounter);
//				 headerGuid = exCrmavcheckapodocheck_etHeader.getByteArray(CrmAvCheckApoDoCheck.HEADER_GUID);
//				 apoTrGuid = exCrmavcheckapodocheck_etHeader.getString(CrmAvCheckApoDoCheck.APO_TR_GUID);
//				 tempObjIndicator = exCrmavcheckapodocheck_etHeader.getString(CrmAvCheckApoDoCheck.TEMP_OBJ_INDICATOR);
//				 bopTrguid = exCrmavcheckapodocheck_etHeader.getString(CrmAvCheckApoDoCheck.BOP_TRGUID);
//			 }
//	
//	//			process table exCrmavcheckapodocheck.etItem
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etItem = ex.getTable(CrmAvCheckApoDoCheck.ET_ITEM);
//			 for (exCrmavcheckapodocheck_etItemCounter=0; exCrmavcheckapodocheck_etItemCounter<exCrmavcheckapodocheck_etItem.getNumRows(); exCrmavcheckapodocheck_etItemCounter++) {
//				 exCrmavcheckapodocheck_etItem.setRow(exCrmavcheckapodocheck_etItemCounter);
//				 headerGuid = exCrmavcheckapodocheck_etItem.getByteArray(CrmAvCheckApoDoCheck.HEADER_GUID);
//				 itemGuid = exCrmavcheckapodocheck_etItem.getByteArray(CrmAvCheckApoDoCheck.ITEM_GUID);
//				 parent = exCrmavcheckapodocheck_etItem.getByteArray(CrmAvCheckApoDoCheck.PARENT);
//				 anchor = exCrmavcheckapodocheck_etItem.getByteArray(CrmAvCheckApoDoCheck.ANCHOR);
//				 previousGuid = exCrmavcheckapodocheck_etItem.getByteArray(CrmAvCheckApoDoCheck.PREVIOUS_GUID);
//				 taxDepartJuris = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.TAX_DEPART_JURIS);
//				 taxDepartReg = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.TAX_DEPART_REG);
//				 taxCountry = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.TAX_COUNTRY);
//				 substReason = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.SUBST_REASON);
//				 mainitem = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.MAINITEM);
//				 confMode = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.CONF_MODE);
//				 itmUsage = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.ITM_USAGE);
//				 itmTypeUsage = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.ITM_TYPE_USAGE);
//				 rrflg = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.RRFLG);
//				 errorFlag = exCrmavcheckapodocheck_etItem.getString(CrmAvCheckApoDoCheck.ERROR_FLAG);
//			 }
//	
//	//			process table exCrmavcheckapodocheck_etItemBtExtension
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etItemBtExtension = ex.getTable(CrmAvCheckApoDoCheck.ET_ITEM_BT_EXTENSION);
//			 for (exCrmavcheckapodocheck_etItemBtExtensionCounter=0; exCrmavcheckapodocheck_etItemBtExtensionCounter<exCrmavcheckapodocheck_etItemBtExtension.getNumRows(); exCrmavcheckapodocheck_etItemBtExtensionCounter++) {
//				 exCrmavcheckapodocheck_etItemBtExtension.setRow(exCrmavcheckapodocheck_etItemBtExtensionCounter);
//				 itemGuid = exCrmavcheckapodocheck_etItemBtExtension.getByteArray(CrmAvCheckApoDoCheck.ITEM_GUID);
//	    
//	//			process table exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata
//				 // TABLE
//				 JCO.Table exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata = exCrmavcheckapodocheck_etItemBtExtension.getTable(CrmAvCheckApoDoCheck.SELECTIONDATA);
//				 for (exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondataCounter=0; exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondataCounter<exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.getNumRows(); exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondataCounter++) {
//					 exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.setRow(exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondataCounter);
//					 selection = exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.getString(CrmAvCheckApoDoCheck.SELECTION);
//					 charcName = exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.getString(CrmAvCheckApoDoCheck.CHARC_NAME);
//					 charcTxt = exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.getString(CrmAvCheckApoDoCheck.CHARC_TXT);
//					 value = exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.getString(CrmAvCheckApoDoCheck.VALUE);
//					 valueTxt = exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.getString(CrmAvCheckApoDoCheck.VALUE_TXT);
//					 valueMax = exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.getString(CrmAvCheckApoDoCheck.VALUE_MAX);
//					 valueCode = exCrmavcheckapodocheck_etItemBtExtensionCrmavcheckapodocheck_selectiondata.getString(CrmAvCheckApoDoCheck.VALUE_CODE);
//				 }
//			 }
//	
//	//			process table exCrmavcheckapodocheck_etItemExtension
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etItemExtension = ex.getTable(CrmAvCheckApoDoCheck.ET_ITEM_EXTENSION);
//			 for (exCrmavcheckapodocheck_etItemExtensionCounter=0; exCrmavcheckapodocheck_etItemExtensionCounter<exCrmavcheckapodocheck_etItemExtension.getNumRows(); exCrmavcheckapodocheck_etItemExtensionCounter++) {
//				 exCrmavcheckapodocheck_etItemExtension.setRow(exCrmavcheckapodocheck_etItemExtensionCounter);
//				 itemGuid = exCrmavcheckapodocheck_etItemExtension.getByteArray(CrmAvCheckApoDoCheck.ITEM_GUID);
//				 productNumber = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.PRODUCT_NUMBER);
//				 locNumber = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.LOC_NUMBER);
//				 locType = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.LOC_TYPE);
//				 itemNumber = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.ITEM_NUMBER);
//				 scenario = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.SCENARIO);
//				 atpRelevant = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.ATP_RELEVANT);
//				 tmpObjMode = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.TMP_OBJ_MODE);
//				 checkProfile = exCrmavcheckapodocheck_etItemExtension.getBigDecimal(CrmAvCheckApoDoCheck.CHECK_PROFILE);
//				 crmMsgNo = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CRM_MSG_NO);
//				 apoRelease = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.APO_RELEASE);
//				 apoUpd = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.APO_UPD);
//				 persistId = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.PERSIST_ID);
//				 status1 = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.STATUS_1);
//				 status2 = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.STATUS_2);
//				 atpCategory = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.ATP_CATEGORY);
//				 subLocation = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.SUB_LOCATION);
//				 purchOrg = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.PURCH_ORG);
//				 tpdsPlant = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.TPDS_PLANT);
//				 tpdsLocType = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.TPDS_LOC_TYPE);
//				 tpdsFlag = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.TPDS_FLAG);
//				 carrierSrc = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CARRIER_SRC);
//				 georouteSrc = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.GEOROUTE_SRC);
//				 checkedAt = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CHECKED_AT);
//				 firstsLocation = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.FIRSTS_LOCATION);
//				 firstsLocType = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.FIRSTS_LOC_TYPE);
//				 facingLocation = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.FACING_LOCATION);
//				 facingLoctype = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.FACING_LOCTYPE);
//				 quantityFactor = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.QUANTITY_FACTOR);
//				 quantityDivisor = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.QUANTITY_DIVISOR);
//				 initiatorProc = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.INITIATOR_PROC);
//				 cdLocation = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CD_LOCATION);
//				 cdLocationType = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CD_LOCATION_TYPE);
//				 atpGuid = exCrmavcheckapodocheck_etItemExtension.getByteArray(CrmAvCheckApoDoCheck.ATP_GUID);
//				 checkedInLuw = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CHECKED_IN_LUW);
//				 consLocation = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CONS_LOCATION);
//				 consLoctype = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CONS_LOCTYPE);
//				 carrierLocation = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CARRIER_LOCATION);
//				 carrierLoctype = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.CARRIER_LOCTYPE);
//				 georoute = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.GEOROUTE);
//				 noReq = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.NO_REQ);
//				 processUnit = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.PROCESS_UNIT);
//				 dateQtyFixed = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.DATE_QTY_FIXED);
//				 agreement = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.AGREEMENT);
//				 agmtItem = exCrmavcheckapodocheck_etItemExtension.getBigDecimal(CrmAvCheckApoDoCheck.AGMT_ITEM);
//				 sourceType = exCrmavcheckapodocheck_etItemExtension.getString(CrmAvCheckApoDoCheck.SOURCE_TYPE);
//			 }
//	
//	//			process table exCrmavcheckapodocheck_etReqSchedlinDate
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etReqSchedlinDate = ex.getTable(CrmAvCheckApoDoCheck.ET_REQ_SCHEDLIN_DATE);
//			 for (exCrmavcheckapodocheck_etReqSchedlinDateCounter=0; exCrmavcheckapodocheck_etReqSchedlinDateCounter<exCrmavcheckapodocheck_etReqSchedlinDate.getNumRows(); exCrmavcheckapodocheck_etReqSchedlinDateCounter++) {
//				 exCrmavcheckapodocheck_etReqSchedlinDate.setRow(exCrmavcheckapodocheck_etReqSchedlinDateCounter);
//				 guid = exCrmavcheckapodocheck_etReqSchedlinDate.getByteArray(CrmAvCheckApoDoCheck.GUID);
//				 requirementItemIndex = (Integer)exCrmavcheckapodocheck_etReqSchedlinDate.getValue(CrmAvCheckApoDoCheck.REQUIREMENT_ITEM_INDEX);
//				 cumulatedResultState = exCrmavcheckapodocheck_etReqSchedlinDate.getString(CrmAvCheckApoDoCheck.CUMULATED_RESULT_STATE);
//	    
//	//			process table exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_scheduling
//				 // TABLE
//				 JCO.Table exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_scheduling = exCrmavcheckapodocheck_etReqSchedlinDate.getTable(CrmAvCheckApoDoCheck.SCHEDULING);
//				 for (exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_schedulingCounter=0; exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_schedulingCounter<exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_scheduling.getNumRows(); exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_schedulingCounter++) {
//					 exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_scheduling.setRow(exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_schedulingCounter);
//					 kindOfDate = exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.KIND_OF_DATE);
//					 dateFrom = exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.DATE_FROM);
//					 dateTo = exCrmavcheckapodocheck_etReqSchedlinDateCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.DATE_TO);
//				 }
//				 roundedQty = exCrmavcheckapodocheck_etReqSchedlinDate.getString(CrmAvCheckApoDoCheck.ROUNDED_QTY);
//			 }
//	
//	//			process table exCrmavcheckapodocheck.etSchedlin
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etSchedlin = ex.getTable(CrmAvCheckApoDoCheck.ET_SCHEDLIN);
//			 for (exCrmavcheckapodocheck_etSchedlinCounter=0; exCrmavcheckapodocheck_etSchedlinCounter<exCrmavcheckapodocheck_etSchedlin.getNumRows(); exCrmavcheckapodocheck_etSchedlinCounter++) {
//				 exCrmavcheckapodocheck_etSchedlin.setRow(exCrmavcheckapodocheck_etSchedlinCounter);
//				 itemGuid = exCrmavcheckapodocheck_etSchedlin.getByteArray(CrmAvCheckApoDoCheck.ITEM_GUID);
//				 schedlinGuid = exCrmavcheckapodocheck_etSchedlin.getByteArray(CrmAvCheckApoDoCheck.SCHEDLIN_GUID);
//				 schedlinHandle = exCrmavcheckapodocheck_etSchedlin.getBigDecimal(CrmAvCheckApoDoCheck.SCHEDLIN_HANDLE);
//				 fromTime = exCrmavcheckapodocheck_etSchedlin.getString(CrmAvCheckApoDoCheck.FROM_TIME);
//				 toTime = exCrmavcheckapodocheck_etSchedlin.getString(CrmAvCheckApoDoCheck.TO_TIME);
//				 quantity = exCrmavcheckapodocheck_etSchedlin.getString(CrmAvCheckApoDoCheck.QUANTITY);
//				 parentSdlnGuid = exCrmavcheckapodocheck_etSchedlin.getByteArray(CrmAvCheckApoDoCheck.PARENT_SDLN_GUID);
//	    
//	//			process table exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck.scheduling
//				 // TABLE
//				 JCO.Table exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_scheduling = exCrmavcheckapodocheck_etSchedlin.getTable(CrmAvCheckApoDoCheck.SCHEDULING);
//				 for (exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_schedulingCounter=0; exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_schedulingCounter<exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_scheduling.getNumRows(); exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_schedulingCounter++) {
//					 exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_scheduling.setRow(exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_schedulingCounter);
//					 kindOfDate = exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.KIND_OF_DATE);
//					 dateFrom = exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.DATE_FROM);
//					 dateTo = exCrmavcheckapodocheck_etSchedlinCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.DATE_TO);
//				 }
//				 source = exCrmavcheckapodocheck_etSchedlin.getString(CrmAvCheckApoDoCheck.SOURCE);
//			 }
//	
//	//			process table exCrmavcheckapodocheck.etSubitem
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etSubitem = ex.getTable(CrmAvCheckApoDoCheck.ET_SUBITEM);
//			 for (exCrmavcheckapodocheck_etSubitemCounter=0; exCrmavcheckapodocheck_etSubitemCounter<exCrmavcheckapodocheck_etSubitem.getNumRows(); exCrmavcheckapodocheck_etSubitemCounter++) {
//				 exCrmavcheckapodocheck_etSubitem.setRow(exCrmavcheckapodocheck_etSubitemCounter);
//				 headerGuid = exCrmavcheckapodocheck_etSubitem.getByteArray(CrmAvCheckApoDoCheck.HEADER_GUID);
//				 itemGuid = exCrmavcheckapodocheck_etSubitem.getByteArray(CrmAvCheckApoDoCheck.ITEM_GUID);
//				 parent = exCrmavcheckapodocheck_etSubitem.getByteArray(CrmAvCheckApoDoCheck.PARENT);
//				 anchor = exCrmavcheckapodocheck_etSubitem.getByteArray(CrmAvCheckApoDoCheck.ANCHOR);
//				 previousGuid = exCrmavcheckapodocheck_etSubitem.getByteArray(CrmAvCheckApoDoCheck.PREVIOUS_GUID);
//				 taxDepartJuris = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.TAX_DEPART_JURIS);
//				 taxDepartReg = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.TAX_DEPART_REG);
//				 taxCountry = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.TAX_COUNTRY);
//				 substReason = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.SUBST_REASON);
//				 mainitem = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.MAINITEM);
//				 confMode = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.CONF_MODE);
//				 itmUsage = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.ITM_USAGE);
//				 itmTypeUsage = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.ITM_TYPE_USAGE);
//				 rrflg = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.RRFLG);
//				 errorFlag = exCrmavcheckapodocheck_etSubitem.getString(CrmAvCheckApoDoCheck.ERROR_FLAG);
//			 }
//	
//	//			process table exCrmavcheckapodocheck_etSubitemSchedlin
//	//			TABLE
//			 JCO.Table exCrmavcheckapodocheck_etSubitemSchedlin = ex.getTable(CrmAvCheckApoDoCheck.ET_SUBITEM_SCHEDLIN);
//			 for (exCrmavcheckapodocheck_etSubitemSchedlinCounter=0; exCrmavcheckapodocheck_etSubitemSchedlinCounter<exCrmavcheckapodocheck_etSubitemSchedlin.getNumRows(); exCrmavcheckapodocheck_etSubitemSchedlinCounter++) {
//				 exCrmavcheckapodocheck_etSubitemSchedlin.setRow(exCrmavcheckapodocheck_etSubitemSchedlinCounter);
//				 itemGuid = exCrmavcheckapodocheck_etSubitemSchedlin.getByteArray(CrmAvCheckApoDoCheck.ITEM_GUID);
//				 schedlinGuid = exCrmavcheckapodocheck_etSubitemSchedlin.getByteArray(CrmAvCheckApoDoCheck.SCHEDLIN_GUID);
//				 schedlinHandle = exCrmavcheckapodocheck_etSubitemSchedlin.getBigDecimal(CrmAvCheckApoDoCheck.SCHEDLIN_HANDLE);
//				 fromTime = exCrmavcheckapodocheck_etSubitemSchedlin.getString(CrmAvCheckApoDoCheck.FROM_TIME);
//				 toTime = exCrmavcheckapodocheck_etSubitemSchedlin.getString(CrmAvCheckApoDoCheck.TO_TIME);
//				 quantity = exCrmavcheckapodocheck_etSubitemSchedlin.getString(CrmAvCheckApoDoCheck.QUANTITY);
//				 parentSdlnGuid = exCrmavcheckapodocheck_etSubitemSchedlin.getByteArray(CrmAvCheckApoDoCheck.PARENT_SDLN_GUID);
//	    
//	//			process table exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_scheduling
//				 // TABLE
//				 JCO.Table exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_scheduling = exCrmavcheckapodocheck_etSubitemSchedlin.getTable(CrmAvCheckApoDoCheck.SCHEDULING);
//				 for (exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_schedulingCounter=0; exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_schedulingCounter<exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_scheduling.getNumRows(); exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_schedulingCounter++) {
//					 exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_scheduling.setRow(exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_schedulingCounter);
//					 kindOfDate = exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.KIND_OF_DATE);
//					 dateFrom = exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.DATE_FROM);
//					 dateTo = exCrmavcheckapodocheck_etSubitemSchedlinCrmavcheckapodocheck_scheduling.getString(CrmAvCheckApoDoCheck.DATE_TO);
//				 }
//				 source = exCrmavcheckapodocheck_etSubitemSchedlin.getString(CrmAvCheckApoDoCheck.SOURCE);
//			 }
//		}

}
