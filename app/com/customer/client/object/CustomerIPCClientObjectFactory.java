package com.customer.client.object;

import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.imp.DefaultConfiguration;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.rfc.RfcIPCClientObjectFactory;

public class CustomerIPCClientObjectFactory extends RfcIPCClientObjectFactory {

	public static IPCClientObjectFactory getInstance() {
			return new CustomerIPCClientObjectFactory();
	}



	/**
	 *
	 */

	public DefaultConfiguration newConfiguration(
		IPCClient ipcClient,
		String id) {
		return new CustomerDefaultConfiguration(ipcClient, id);
	}

}
