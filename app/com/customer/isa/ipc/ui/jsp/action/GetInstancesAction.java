/*
 * Created on 28.10.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.customer.isa.ipc.ui.jsp.action;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.action.IPCBaseAction;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GetInstancesAction
	extends com.sap.isa.ipc.ui.jsp.action.GetInstancesAction {
		
		private static final String INITIAL_REQUEST = "INITIAL_REQUEST"; 

	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.action.IPCBaseAction#customerExit(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	protected void customerExit(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack) {
			
			UIContext uiContext = IPCBaseAction.getUIContext(request);
			String initialRequest = uiContext.getPropertyAsString(INITIAL_REQUEST);
			if (initialRequest == null) { //no one else sets this flag
				Hashtable instanceTreeStatus = (Hashtable) uiContext.getProperty(Constants.INSTANCE_TREE_STATUS_CHANGE);
				Configuration config = uiContext.getCurrentConfiguration();
				List instances = config.getInstances();
				//mark all instances as collapsed
				for (Iterator it = instances.iterator(); it.hasNext(); ) {
					Instance inst = (Instance)it.next();
					instanceTreeStatus.put(inst.getId(),inst.getId());
				}
				uiContext.setProperty(INITIAL_REQUEST, "F");
			}
	}

}
