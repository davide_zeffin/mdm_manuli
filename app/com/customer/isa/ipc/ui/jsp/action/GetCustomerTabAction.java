package com.customer.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants;
import com.sap.isa.ipc.ui.jsp.action.IPCBaseAction;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.constants.CharacteristicDetailsRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;

public class GetCustomerTabAction extends IPCBaseAction {

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {

		UIContext uiContext = getUIContext(request);

		ConfigUIBean currentConfigUIBean =
			this.getCurrentConfiguration(uiContext);

		// Do this to transfer the current instance and characteristic into the ui context
		//Instance currentInstance = ObjectFinder.getCurrentInstance(request,uiContext);
		String currentInstanceId =
			(String) request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (!isSet(currentInstanceId)) {
			currentInstanceId =
				getContextValue(request, Constants.CURRENT_INSTANCE_ID);
		}
		InstanceUIBean currentInstanceUIBean;
		if (currentInstanceId == null) {
			currentInstanceUIBean =
				this.getCurrentInstance(
					uiContext,
					currentConfigUIBean,
					currentConfigUIBean
						.getBusinessObject()
						.getRootInstance()
						.getId(),
					request);
		} else {
			currentInstanceUIBean =
				this.getCurrentInstance(
					uiContext,
					currentConfigUIBean,
					currentInstanceId,
					request);
		}

		String currentCharacteristicGroupName =
			(String) request.getParameter(
				Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
		if (!isSet(currentCharacteristicGroupName)) {
			currentCharacteristicGroupName =
				getContextValue(
					request,
					Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
		}
		CharacteristicGroup currentCharacteristicGroup;
		if (currentCharacteristicGroupName == null) {
			// get last group
			List groups = currentInstanceUIBean.getBusinessObject().getCharacteristicGroups();
			currentCharacteristicGroup = (CharacteristicGroup) groups.get(0);
		    if(currentCharacteristicGroup.getCharacteristics(uiContext.getShowInvisibleCharacteristics(), uiContext.getAssignableValuesOnly()).size() == 0){
				currentCharacteristicGroup = (CharacteristicGroup) groups.get(1);
		    }
			//return parameterError(
			//	mapping,
			//	request,
			//	Constants.CURRENT_CHARACTERISTIC_GROUP_NAME,
			//	this.getClass().getName());
		}else{
		 currentCharacteristicGroup =
			currentInstanceUIBean.getBusinessObject().getCharacteristicGroup(
				currentCharacteristicGroupName);
		}
		GroupUIBean currentGroupBean =
			UIBeanFactory.getUIBeanFactory().newGroupUIBean(
				currentCharacteristicGroup,
				uiContext);
		currentInstanceUIBean.addGroup(currentGroupBean);

		String currentCharacteristicName =
			(String) request.getParameter(
				Constants.CURRENT_CHARACTERISTIC_NAME);
		if (!isSet(currentCharacteristicName)) {
			currentCharacteristicName =
				getContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME);
		}
		if (currentCharacteristicName == null) {
			List characteristics = currentCharacteristicGroup.getCharacteristics(uiContext.getShowInvisibleCharacteristics(), uiContext.getAssignableValuesOnly());
			Iterator characteristicsIt;
			if (characteristics != null) {
				characteristicsIt = characteristics.iterator();
				if (characteristicsIt.hasNext()) {
					Characteristic firstCharacteristic = (Characteristic)characteristicsIt.next();
					currentCharacteristicName = firstCharacteristic.getName();
				}else {
					log.debug("Cannot set initial context value for characteristic. Group " + currentCharacteristicGroup.getName() + " has no visible characteristics in the current uiContext.");
					return parameterError(
						mapping,
						request,
						Constants.CURRENT_CHARACTERISTIC_NAME,
						this.getClass().getName());
				}
			}else {
				log.debug("Cannot set initial context value for characteristic. Group " + currentCharacteristicGroup.getName() + " has null characteristics in the current uiContext.");
			}		
		}
		boolean showInvisibleCstics =
			uiContext.getShowInvisibleCharacteristics();
		Characteristic currentCharacteristic =
			currentInstanceUIBean.getBusinessObject().getCharacteristic(
				currentCharacteristicName,
				showInvisibleCstics);
		CharacteristicUIBean characteristicUIBean =
			CharacteristicUIBean.getInstance(
				request,
				currentCharacteristic,
				uiContext);
		currentInstanceUIBean.addCharacteristic(characteristicUIBean);
		currentGroupBean.addCharacteristic(characteristicUIBean);

		customerExit(
			mapping,
			form,
			request,
			response,
			userSessionData,
			requestParser,
			mbom,
			multipleInvocation,
			browserBack);

		request.setAttribute(
			CharacteristicDetailsRequestParameterConstants.CURRENT_INSTANCE,
			currentInstanceUIBean);
		request.setAttribute(
			CharacteristicDetailsRequestParameterConstants
				.CURRENT_CHARACTERISTIC_GROUP,
			currentGroupBean);
		request.setAttribute(
			CharacteristicDetailsRequestParameterConstants
				.CURRENT_CHARACTERISTIC,
			characteristicUIBean);
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
}
