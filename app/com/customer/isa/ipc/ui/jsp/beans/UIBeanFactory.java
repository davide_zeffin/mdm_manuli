/*
 * Created on 27.10.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.customer.isa.ipc.ui.jsp.beans;

import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.Configuration;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UIBeanFactory extends com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory {

	// used instance (Singeleton Pattern)
	public static UIBeanFactory instance = new UIBeanFactory();

	/**
	 * Return the only instance of the class (Singleton). <br>
	 * 
	 * @return Factory object. Depending on the scenario either RfcIPCClientObjectFactory or TcpIPCClientObjectFactory.
	 */
	public static com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory getInstance() {
		return instance;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory#newValueUIBean(com.sap.spc.remote.client.object.CharacteristicValue, com.sap.isa.ipc.ui.jsp.uiclass.UIContext)
	 */
	public ValueUIBean newValueUIBean(
		CharacteristicValue valueBO,
		UIContext uiContext) {
		return new com.customer.isa.ipc.ui.jsp.beans.ValueUIBean(valueBO, uiContext);
	}

	/**
	 *
	 */

	public ConfigUIBean newConfigUIBean(
		Configuration config,
		UIContext uiContext) {
		return new com.customer.isa.ipc.ui.jsp.beans.CustomerConfigUIBean(config, uiContext);
	}

}
