package com.customer.isa.ipc.ui.jsp.beans;

import com.customer.client.object.CustomerDefaultConfiguration;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.StatusImage;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;

public class CustomerConfigUIBean extends ConfigUIBean {

	/**
	 *
	 */

	public CustomerConfigUIBean(Configuration config, UIContext uiContext) {
		super(config, uiContext);
	}
	
	public StatusImage getAvailibilityImage() {
		if (((CustomerDefaultConfiguration)getBusinessObject()).checkAvailability() == CustomerDefaultConfiguration.NOT_AVAILABLE) {
			return UIBeanFactory.getUIBeanFactory().newStatusImage(
				"ipc.config.inconsistent",
				"ipc/mimes/images/sys/ipc_inconsist.gif"); //just reuse the consistency image
		} else {
			if (((CustomerDefaultConfiguration)getBusinessObject()).checkAvailability() == CustomerDefaultConfiguration.AVAILABLE) {
				return UIBeanFactory.getUIBeanFactory().newStatusImage(
					"ipc.config.complete",
					"ipc/mimes/images/sys/ipc_completeness.gif");
			} else {
				return UIBeanFactory.getUIBeanFactory().newStatusImage(
					"ipc.config.incomplete",
					"ipc/mimes/images/sys/ipc_incompleteness.gif");
			}
		}
	}


}
