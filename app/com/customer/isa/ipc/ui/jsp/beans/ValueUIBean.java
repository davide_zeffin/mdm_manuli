/*
 * Created on 27.10.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.customer.isa.ipc.ui.jsp.beans;

import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ValueUIBean extends com.sap.isa.ipc.ui.jsp.beans.ValueUIBean {
	
	public ValueUIBean(CharacteristicValue valueBO,	UIContext uiContext) {
		super(valueBO, uiContext);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#getLanguageDependentName()
	 */
	public String getLanguageDependentName() {

		if (isFreeTextValue()) {
			return super.getLanguageDependentName();
		}else {
			return super.getName() + ":" + super.getLanguageDependentName();
		}		
	}
	
	private boolean isFreeTextValue() {
		Characteristic characteristicBO = this.getBusinessObject().getCharacteristic();
		UIContext uiContext = getUiContext();
		if (!uiContext.getDisplayMode()
			&& !characteristicBO.isReadOnly()
			&& characteristicBO.isVisible()
			&& (characteristicBO.allowsAdditionalValues()
				|| !characteristicBO.isDomainConstrained()
				|| characteristicBO.hasIntervalAsDomain())) {
			return true;
		} else {
			return false;
		}
	}

}
