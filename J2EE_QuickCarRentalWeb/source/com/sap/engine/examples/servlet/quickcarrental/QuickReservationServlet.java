package com.sap.engine.examples.servlet.quickcarrental;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;

import javax.ejb.CreateException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.engine.examples.ejb.quickcarrental.QuickOrderProcessorLocal;
import com.sap.engine.examples.ejb.quickcarrental.QuickOrderProcessorLocalHome;
import com.sap.engine.examples.util.Constants;
import com.sap.engine.examples.util.QuickBookingModel;
import com.sap.engine.examples.util.QuickCarRentalException;

public class QuickReservationServlet extends HttpServlet {
	protected void doGet(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		doWork(request, response); 
	}

	protected void doPost(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		doWork(request, response); 
	}
	
	public void doWork(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException {
		QuickOrderProcessorLocal order = initializeController();
		handleRequest(request, response, order);
		viewAllBookings(request, order);
		HttpSession session = request.getSession(true);
		RequestDispatcher dispatcher =
			request.getRequestDispatcher("/view");
		try {
			dispatcher.forward(request, response);
		} catch (IOException e) {
			e.printStackTrace();
			throw new ServletException(e.getMessage());
		}
	}
	
	private QuickOrderProcessorLocal initializeController()
		throws ServletException {
		try {
			Context ctx = new InitialContext();
			QuickOrderProcessorLocalHome orderHome =
				(QuickOrderProcessorLocalHome) ctx.lookup(
					"java:comp/env/QuickOrderProcessorBean");
			return orderHome.create();
		} catch (CreateException e) {
			e.printStackTrace();
			throw new ServletException(e.toString());
		} catch (NamingException e) {
			e.printStackTrace();
			throw new ServletException(e.toString());
		}
	}

	public void handleRequest(
		HttpServletRequest request,
		HttpServletResponse response,
		QuickOrderProcessorLocal order)
		throws ServletException {
		HttpSession session = request.getSession(true);
		session.setAttribute(Constants.CLIENT_MESSAGE,null);
		String action = request.getParameter("appAction");
		if (action != null && action.length() > 0) {
			switch (Integer.parseInt(action)) {
				case Constants.ACTION_SAVE :
					{
						saveAction(request, order);
					};
					break;
				case Constants.ACTION_CANCEL :
					{
						cancelAction(request, order);
					}
					break;
			}
		}

		
	}
	
	private void viewAllBookings(
		HttpServletRequest request,
		QuickOrderProcessorLocal order) {
		HttpSession session = request.getSession(true);
		QuickBookingModel[] bookings;
		try {
			bookings = order.viewActiveBookings();
			session.setAttribute(
			Constants.RESERVATIONS,
			formatBookings(bookings));
		} catch (QuickCarRentalException e) {
			session.setAttribute(Constants.CLIENT_MESSAGE,e.getMessage());
		}
		

	}

	
	private void saveAction(
		HttpServletRequest request,
		QuickOrderProcessorLocal order) {
		HttpSession session = request.getSession(true);
		try {
		java.lang.String dateFrom = request.getParameter("pickupDate");
		java.lang.String dateTo = request.getParameter("dropoffDate");
		String vehicleTypeId = request.getParameter("vehicleTypeId");
		
			String pickupLocation = request.getParameter("pickupLocation");
			String dropoffLocation = request.getParameter("dropoffLocation");
			order.saveBooking(vehicleTypeId,dateFrom,
				dateTo,
				pickupLocation,
				dropoffLocation);
		} catch (QuickCarRentalException e) {
			session.setAttribute(Constants.CLIENT_MESSAGE,e.getMessage());
		}
	}
	
	private void cancelAction(
		HttpServletRequest request,
		QuickOrderProcessorLocal order) {
		HttpSession session = request.getSession(true);

		String[] selectedBookings = request.getParameterValues("check");
		for (int i = 0; i < selectedBookings.length; i++) {
			try {
				order.cancelBooking((String) selectedBookings[i]);
			} catch (QuickCarRentalException e) {
				
				session.setAttribute(
			Constants.CLIENT_MESSAGE,e.getMessage());
			}

		}

	}
	
	private ArrayList formatBookings(QuickBookingModel[] bookings){
		ArrayList bookings_list = new ArrayList();
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
		for (int i=0;i<bookings.length;i++){
			String[] booking = new String[7];
			booking[0] = bookings[i].getBookingId();
			booking[1] = bookings[i].getVehicleType();
			booking[2] = bookings[i].getPickupLocation();
			booking[3] = bookings[i].getDropoffLocation();
			booking[4] = bookings[i].getDateFrom();
			booking[5] = bookings[i].getDateTo();
			booking[6] = bookings[i].getPrice();
			bookings_list.add(booking);
		}
		
		return bookings_list;
	}

}

