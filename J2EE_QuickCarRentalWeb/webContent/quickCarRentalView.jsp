<%@ page import="com.sap.engine.examples.util.Constants" language="java" %>
<html>
	<head><title>QuickCarRental</title></head>
	<body>
		<table border="1">
			<b><font color="red"><%if(session.getAttribute(Constants.CLIENT_MESSAGE)!=null)%><%=session.getAttribute(Constants.CLIENT_MESSAGE) %></font></b>
			<form name="wizard" method="POST" action="/QuickCarRental">
				<input type="hidden" name="appAction" value="<%=Constants.ACTION_SAVE%>"/>
				
							<tr	align="center">
								<td align="center">Pick-up Location</td>
								<td align="center">Vehicle Type</td>
								<td	align="center">Drop_off Location</td>
								<td align="center">Time Frame</td>
								<td align="center">&nbsp;</td>
							</tr>
							<tr	align="center">
								<td	align="center">
									<select name="pickupLocation">
										<% for (int j = 0;j < Constants.LOCATION.length;j++) {
												String value = Constants.LOCATION[j];
										%>
										<option value="<%=value%>"><%=value%></option>
										<%	} %>
									</select>
								</td>
								<td	align="center">
									<select name="vehicleTypeId">
										<% for (int j = 0;j < Constants.VEHICLE_TYPE.length;j++) {
												String value = Constants.VEHICLE_TYPE[j];
										%>
										<option value="<%=value%>"><%=value%></option>
										<% } %>
									</select>
								</td>
								<td	align="center">
									<select name="dropoffLocation" >
										<% for (int j = 0;j < Constants.LOCATION.length;j++) {
												String value = Constants.LOCATION[j];
										%>
										<option value="<%=value%>"><%=value%></option>
										<% } %>
									</select>
								</td>
								<td align="center">Pick-up Date:
									<br/>
									<input name="pickupDate" type="text" size="10" maxlength="10"/>
									
									<br/>
									Return Date:
									<br/>
									<input name="dropoffDate" type="text" size="10" maxlength="10"/>
									
								</td>
								<td align="center">
									<input type="submit" name="reservationAdd" value="Add Reservation">
								</td>
							</tr>
						
			</form>
		</table>
		<%
			ArrayList reservations =(ArrayList) session.getAttribute(Constants.RESERVATIONS);
			if (reservations != null && reservations.size() > 0) {
		%>
		</br>
		<table border="1">
			<form name="reservations" method="POST" action="/QuickCarRental">
				<input type="hidden" name="appAction" value="<%=Constants.ACTION_CANCEL%>"/>
				<tr>
					<th>&nbsp</th>
					<th>ID</th>
					<th>Vehicle type</th>
					<th>Pick-up location</th>
					<th>Drop-off location</th>
					<th>Begin date</th>
					<th>End date</th>
					<th>Price</th>
				</tr>
				<%
					for (int i = 0; i < reservations.size(); i++) {
						String[] reservation = (String[]) reservations.get(i);
				%>
				<tr>
					<td>
						<input type="checkbox" name="check" value="<%=reservation[0]%>"/>
					</td>
					<%
						for (int j = 0; j < reservation.length; j++) {
							String value = reservation[j];
					%>
					<td><%=value%></td>
					<% } %>
				</tr>
				<% } %>
				<tr>
				<td align="center" colspan="8">
				<table>
				<tr>
					<td align="left">
						<input type="submit" name="reservationCancel" value="Cancel Reservation">
					</td>
					<td/>
				</tr>
			</form>
		</table>
		<% } else { %> No Reservations available !
		<% } %>
	</body>
</html>

