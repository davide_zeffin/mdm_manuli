/*
 * ============================================================================
 * GNU Lesser General Public License
 * ============================================================================
 *
 * JasperReports - Free Java report-generating library.
 * Copyright (C) 2001-2006 JasperSoft Corporation http://www.jaspersoft.com
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 * 
 * JasperSoft Corporation
 * 303 Second Street, Suite 450 North
 * San Francisco, CA 94107
 * http://www.jaspersoft.com
 */
package net.sf.jasperreports.engine.util;

/**
 * An XML namespace.
 * 
 * @author Lucian Chirita (lucianc@users.sourceforge.net)
 * @version $Id: XmlNamespace.java 2291 2008-08-19 13:34:14Z lucianc $
 * @see JRXmlWriteHelper#startElement(String, XmlNamespace)
 */
public class XmlNamespace
{

	private final String nsURI;
	private final String prefix;
	private final String schemaURI;
	
	/**
	 * Creates an XML namespace.
	 * 
	 * @param uri the namespace URI
	 * @param prefix the namespace prefix
	 * @param schemaURI the URI of the XML schema associated with the namespace
	 */
	public XmlNamespace(String uri, String prefix, String schemaURI)
	{
		this.prefix = prefix;
		this.schemaURI = schemaURI;
		this.nsURI = uri;
	}
	
	/**
	 * Returns the namespace URI.
	 * 
	 * @return the namespace URI
	 */
	public String getNamespaceURI()
	{
		return nsURI;
	}

	/**
	 * Returns the namespace prefix.
	 * 
	 * @return the namespace prefix
	 */
	public String getPrefix()
	{
		return prefix;
	}

	/**
	 * Returns the URI of the XML schema associated with the namespace.
	 * 
	 * @return the namespace XML schema URI
	 */
	public String getSchemaURI()
	{
		return schemaURI;
	}
	
}
