/*
 * Created on 22-mag-2013
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.manulirubber.mdm.fittings;

import com.sap.tc.webdynpro.basesrvc.util.IOUtil;
import com.sap.tc.webdynpro.clientserver.adobe.pdfdocument.api.*;
import java.io.*;

public class PDFGenerator
{

	public PDFGenerator(String source, String templateName)
	{
		this.source = null;
		this.templateName = null;
		this.source = source;
		this.templateName = templateName;
	}

	public InputStream generate()
	{
		IWDPDFDocument pdfDoc = null;
		try
		{
			InputStream template = new FileInputStream(templateName);
			ByteArrayInputStream dataSourceIS = new ByteArrayInputStream(source.getBytes("UTF-8"));
			ByteArrayOutputStream dataSourceOS = new ByteArrayOutputStream();
			ByteArrayOutputStream templateSourceOS = new ByteArrayOutputStream();
			IOUtil.write(template, templateSourceOS);
			IOUtil.write(dataSourceIS, dataSourceOS);
			IWDPDFDocumentCreationContext creationContext = WDPDFDocumentFactory.getDocumentHandler().getDocumentCreationContext();
			creationContext.setTemplate(templateSourceOS);
			creationContext.setData(dataSourceOS);
			creationContext.setDynamic(false);
			creationContext.setInteractive(false);
			pdfDoc = creationContext.execute();
		}
		catch(IOException e)
		{
			return null;
		}
		catch(Exception e)
		{
			return null;
		}
		return pdfDoc.getPDFAsStream();
	}

	private String source;
	private String templateName;
}
