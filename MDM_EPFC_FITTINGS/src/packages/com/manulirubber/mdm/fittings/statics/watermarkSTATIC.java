/*
 * Created on 22-mar-2016
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.manulirubber.mdm.fittings.statics;

import a2i.core.Value;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class watermarkSTATIC {

	public watermarkSTATIC(String cacheServer, int cachePort, String cacheUser, String cachePassword, String cacheDir)
		{
			this.cacheServer = cacheServer;
			this.cachePort = cachePort;
			this.cacheUser = cacheUser;
			this.cachePassword = cachePassword;
			this.cacheDir = cacheDir;
		}

		public String getWatermark_text()
		{
			return Watermark_text;
		}

		public void setWatermark_text(Value value)
		{
			if(value.IsNull())
				Watermark_text = null;
			else
			for (int n = 0; n < 200; n++) {				
				Watermark_text = Watermark_text + " " + value.GetStringValue();
			}		
		}


		private String cacheServer;
		private int cachePort;
		private String cacheUser;
		private String cachePassword;
		private String cacheDir;
		private String Watermark_text;
}
