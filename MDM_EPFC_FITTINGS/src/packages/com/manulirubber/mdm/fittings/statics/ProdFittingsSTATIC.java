/*
 * Created on 22-mag-2013
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.manulirubber.mdm.fittings.statics;

import a2i.cache.CatalogCache;
import a2i.core.Value;
import java.util.ArrayList;
import java.util.List;

public class ProdFittingsSTATIC
{

	public ProdFittingsSTATIC(String cacheServer, int cachePort, String cacheUser, String cachePassword, String cacheDir)
	{
		this.cacheServer = cacheServer;
		this.cachePort = cachePort;
		this.cacheUser = cacheUser;
		this.cachePassword = cachePassword;
		this.cacheDir = cacheDir;
		dimLabels = new ArrayList();
		colLabels1 = new ArrayList();
		colLabels2 = new ArrayList();
		labelsAdded = false;
	}

	public String getCatalogueImage()
	{
		return catalogueImage;
	}

	public String getCatalogueSubtitle()
	{
		return catalogueSubtitle;
	}

	public String getCatalogueTitle()
	{
		return catalogueTitle;
	}

	public String getFamilyDescription()
	{
		return familyDescription;
	}

	public String getName()
	{
		return name;
	}

	public String getNote1()
	{
		return note1;
	}

	public String getPrintableName()
	{
		return printableName;
	}

	public String getTypeImage()
	{
		return typeImage;
	}

	public String getDimHeadLabel()
	{
		return dimHeadLabel;
	}

	public List getDimLabels()
	{
		return dimLabels;
	}

	public List getColLabels1()
	{
		return colLabels1;
	}

	public List getColLabels2()
	{
		return colLabels2;
	}

	public void setCatalogueImage(Value value)
	{
		if(value.IsNull())
		{
			catalogueImage = null;
			return;
		}
		try
		{
			CatalogCache catalogCache = new CatalogCache();
			catalogCache.Init(cacheServer, cachePort, cacheUser, cachePassword, cacheDir, "English [US]");
			Object id = value.GetData();
			int idx = id.hashCode();
			catalogueImage = cacheDir + "\\" + catalogCache.GetImagePath("Images", "Original", idx);
			catalogCache.Shutdown();
		}
		catch(Exception e)
		{
			catalogueImage = null;
		}
	}

	public void setCatalogueSubtitle(Value value)
	{
		if(value.IsNull())
			catalogueSubtitle = null;
		else
			catalogueSubtitle = value.GetStringValue();
	}

	public void setCatalogueTitle(Value value)
	{
		if(value.IsNull())
			catalogueTitle = null;
		else
			catalogueTitle = value.GetStringValue();
	}

	public void setFamilyDescription(Value value)
	{
		if(value.IsNull())
			familyDescription = null;
		else
			familyDescription = value.GetStringValue();
	}

	public void setName(Value value)
	{
		if(value.IsNull())
			name = null;
		else
			name = value.GetStringValue();
	}

	public void setNote1(Value value)
	{
		if(value.IsNull())
			note1 = null;
		else
			note1 = value.GetStringValue();
	}

	public void setPrintableName(Value value)
	{
		if(value.IsNull())
			printableName = null;
		else
			printableName = value.GetStringValue();
	}

	public void setTypeImage(Value value)
	{
		if(value.IsNull())
		{
			typeImage = null;
			return;
		}
		try
		{
			CatalogCache catalogCache = new CatalogCache();
			catalogCache.Init(cacheServer, cachePort, cacheUser, cachePassword, cacheDir, "English [US]");
			Object id = value.GetData();
			int idx = id.hashCode();
			typeImage = cacheDir + "\\" + catalogCache.GetImagePath("Images", "Original", idx);
			catalogCache.Shutdown();
		}
		catch(Exception e)
		{
			typeImage = null;
		}
	}

	public void setDimLabel(String label, boolean mm)
	{
		if(labelsAdded)
		{
			return;
		} else
		{
			dimLabels.add(label);
			dimHeadLabel = mm ? "DIMENSIONS mm" : "DIMENSIONS Inch";
			return;
		}
	}

	public void setColLabel(String label1, String label2)
	{
		if(labelsAdded)
		{
			return;
		} else
		{
			colLabels1.add(label1);
			colLabels2.add(label2);
			return;
		}
	}

	public void setLabelAdded(boolean labelsAdded)
	{
		if(labelsAdded && dimLabels.isEmpty())
		{
			return;
		} else
		{
			this.labelsAdded = labelsAdded;
			return;
		}
	}

	private String cacheServer;
	private int cachePort;
	private String cacheUser;
	private String cachePassword;
	private String cacheDir;
	private String name;
	private String typeImage;
	private String catalogueTitle;
	private String catalogueSubtitle;
	private String catalogueImage;
	private String printableName;
	private String familyDescription;
	private String note1;
	private String tree;
	private ArrayList dimLabels;
	private String dimHeadLabel;
	private ArrayList colLabels1;
	private ArrayList colLabels2;
	private boolean labelsAdded;
}
