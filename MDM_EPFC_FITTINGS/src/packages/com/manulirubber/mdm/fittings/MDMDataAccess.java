/*
 * Created on 22-mag-2013
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.manulirubber.mdm.fittings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import a2i.common.A2iResultSet;
import a2i.common.CatalogData;
import a2i.common.ResultSetDefinition;
import a2i.core.A2iIntArray;
import a2i.core.StringException;
import a2i.generated.MeasurementManager;
import a2i.search.FreeFormParameterField;
import a2i.search.FreeFormTableParameter;
import a2i.search.LookupParameter;
import a2i.search.Search;




// Referenced classes of package com.manuli:
//			  ProdFittingsSTATIC, ProdFittingsRecSTATIC

public class MDMDataAccess
{

	public MDMDataAccess(boolean allProducts, String recId)
	{
		this.allProducts = allProducts;
		this.recId = recId;
		headerFields = new ArrayList();
		headerFields.add("Name");
		headerFields.add("type_image");
		headerFields.add("cat_title");
		headerFields.add("catalogue_subtitle");
		headerFields.add("catalogue_image");
		headerFields.add("printable_name");
		headerFields.add("family_description");
		headerFields.add("note1");
		headerFields.add("tree");
		recFields = new ArrayList();
		recFields.add("Name");
		recFields.add("category");
		recFields.add("nominal_size");
		recFields.add("thread");
		recFields.add("pipe_mm");
		recFields.add("flange_inch");
		recFields.add("catalogue");
		recFields.add("phase");
		recFields.add("printable");
		recFields.add("public");
		recFields.add("b_mm");
		recFields.add("b_inch");
		recFields.add("a_mm");
		recFields.add("a_inch");
		recFields.add("c_mm");
		recFields.add("c_inch");
		recFields.add("d_mm");
		recFields.add("d_inch");
		recFields.add("d_OR_mm");
		recFields.add("d_OR_inch");
		recFields.add("e_mm");
		recFields.add("e_inch");
		recFields.add("f_mm");
		recFields.add("f_inch");
		recFields.add("f_diam_mm");
		recFields.add("f_diam_inch");
		recFields.add("k_mm");
		recFields.add("k_inch");
		recFields.add("l_mm");
		recFields.add("l_inch");
		recFields.add("s_mm");
		recFields.add("s_inch");
		recFields.add("s_OR_mm");
		recFields.add("s_OR_inch");
		recFields.add("z_mm");
		recFields.add("z_inch");
		recFields.add("h_mm");
		recFields.add("h_inch");
		recFields.add("h1_mm");
		recFields.add("h1_inch");
		recFields.add("h2_mm");
		recFields.add("h2_inch");
		recFields.add("v_mm");
		recFields.add("v_inches");
		recFields.add("bolt_mm");
		recFields.add("working_press_bar");
		recFields.add("working_press_psi");
	}

	public boolean login()
	{
		try
		{
			catalogData = new CatalogData();
			catalogData.Login(cacheServer, cachePort, cacheUser, cachePassword, "English [US]");
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

	public boolean logout()
	{
		if(catalogData == null)
			return false;
		try
		{
			catalogData.Logout();
		}
		catch(Exception e)
		{
			return false;
		}
		catalogData = null;
		return true;
	}

	public ProdFittings getProdFittings()
	{
		ProdFittings fit = null;
		try
		{
			ResultSetDefinition recRsd = new ResultSetDefinition("prod_fittings_rec");
			recRsd.AddField("category");
			A2iIntArray intArray = new A2iIntArray();
			intArray.Add(Integer.parseInt(recId));
			A2iResultSet recRes = catalogData.GetRecordsById(recRsd, intArray);
			intArray.Clear();
			intArray = recRes.GetLookupIDAt(0, "category");
			headRecordID = intArray.GetAt(0);
			ResultSetDefinition headRsd = new ResultSetDefinition("prod_fittings");
			for(Iterator itr = headerFields.iterator(); itr.hasNext(); headRsd.AddField((String)itr.next()));
			A2iResultSet headRes = catalogData.GetRecordsById(headRsd, intArray);
			for(int i = 0; i < headRes.GetRecordCount(); i++)
			{
				fit = new ProdFittings(cacheServer, cachePort, cacheUser, cachePassword, cacheDir);
				fit.setName(headRes.GetValueAt(i, "Name"));
				fit.setTypeImage(headRes.GetValueAt(i, "type_image"));
				fit.setCatalogueTitle(headRes.GetValueAt(i, "cat_title"));
				fit.setCatalogueSubtitle(headRes.GetValueAt(i, "catalogue_subtitle"));
				fit.setCatalogueImage(headRes.GetValueAt(i, "catalogue_image"));
				fit.setPrintableName(headRes.GetValueAt(i, "printable_name"));
				fit.setFamilyDescription(headRes.GetValueAt(i, "family_description"));
				fit.setNote1(headRes.GetValueAt(i, "note1"));
			}

		}
		catch(Exception e)
		{
			return null;
		}
		return fit;
	}
	public watermark getWatermark() {
			watermark wmark = null;
			try {

				/*** create a ResultSetDefinition on the prod_machine_rec table ***/
				ResultSetDefinition rsd_watermark;
				rsd_watermark = new ResultSetDefinition("Watermark");
				rsd_watermark.AddField("Watermark_text");
				Search search_options = new Search(rsd_watermark.GetTable());

				FreeFormTableParameter fftp_W =
					search_options.GetParameters().NewFreeFormTableParameter(
						rsd_watermark.GetTable());
				FreeFormParameterField ffpf_W_Field00 =
					fftp_W.GetFields().New("Watermark_text");

				A2iResultSet rs_W =
					catalogData.GetResultSet(
						search_options,
						rsd_watermark,
						null,
						true,
						0);

				for (int i = 0; i < rs_W.GetRecordCount(); i++) {

					wmark =
						new watermark(
							cacheServer,
							cachePort,
							cacheUser,
							cachePassword,
							cacheDir);
					wmark.setWatermark_text(rs_W.GetValueAt(i, "Watermark_text"));
				}

			} catch (Exception e) {
				return null;
			}
			return wmark;
		}
	private void columnAddRowsCount(HashMap hm, A2iResultSet rs, int row, String col) throws StringException{

		if (!hm.containsKey(col)){
			hm.put(col,new Integer(0));
		}

		// lettura nr di righe con campo xxx valorizzato
		int count = ((Integer)hm.get(col)).intValue();
				
		// aggiorna nr di righe con campo xxx se valore valido
		if(ProdFittingsRec.isDimValueValid(rs.GetValueAt(row, col))){
			count++;
			hm.put(col,new Integer(count));
		}
	}
	
	public List getProdFittingsRec(ProdFittings header, boolean isISO)
	{
		
		HashMap dimHm = new HashMap();
		HashMap hm = new HashMap();
		List listRec = null;
		ResultSetDefinition rsd = new ResultSetDefinition("prod_fittings_rec");
		for(Iterator itr = recFields.iterator(); itr.hasNext(); rsd.AddField((String)itr.next()));
		Search search = new Search(rsd.GetTable());
		LookupParameter lookup = search.GetParameters().NewLookupParameter(rsd.GetTable(), "category");
		lookup.Add(headRecordID);
		try
		{
			A2iResultSet rs = catalogData.GetResultSet(search, rsd, "category", true, 0L);
			header.setLabelAdded(false);

			// Interpreta quali colonne vanno stampate (max 7)
			if (isISO){
				for(int i = 0; i < rs.GetRecordCount(); i++)
					if((allProducts || rs.GetValueAt(i, "catalogue").GetStringValue().equals("YES")) && !rs.GetValueAt(i, "phase").GetStringValue().equals("BLOCKED") && rs.GetValueAt(i, "printable").GetStringValue().equals("YES") && rs.GetValueAt(i, "public").GetStringValue().equals("YES")){
						columnAddRowsCount(dimHm, rs, i, "b_mm");
						columnAddRowsCount(dimHm, rs, i, "a_mm");
						columnAddRowsCount(dimHm, rs, i, "c_mm");
						columnAddRowsCount(dimHm, rs, i, "d_mm");
						columnAddRowsCount(dimHm, rs, i, "d_OR_mm");
						columnAddRowsCount(dimHm, rs, i, "e_mm");
						columnAddRowsCount(dimHm, rs, i, "f_mm");
						columnAddRowsCount(dimHm, rs, i, "f_diam_mm");
						columnAddRowsCount(dimHm, rs, i, "k_mm");
						columnAddRowsCount(dimHm, rs, i, "l_mm");
						columnAddRowsCount(dimHm, rs, i, "s_mm");
						columnAddRowsCount(dimHm, rs, i, "s_OR_mm");
						columnAddRowsCount(dimHm, rs, i, "z_mm");
						columnAddRowsCount(dimHm, rs, i, "h_mm");
						columnAddRowsCount(dimHm, rs, i, "h1_mm");
						columnAddRowsCount(dimHm, rs, i, "h2_mm");
						columnAddRowsCount(dimHm, rs, i, "v_mm");
						
					}
			}else{
				for(int i = 0; i < rs.GetRecordCount(); i++)
					if((allProducts || rs.GetValueAt(i, "catalogue").GetStringValue().equals("YES")) && !rs.GetValueAt(i, "phase").GetStringValue().equals("BLOCKED") && rs.GetValueAt(i, "printable").GetStringValue().equals("YES") && rs.GetValueAt(i, "public").GetStringValue().equals("YES")){
						columnAddRowsCount(dimHm, rs, i, "b_inch");
						columnAddRowsCount(dimHm, rs, i, "a_inch");
						columnAddRowsCount(dimHm, rs, i, "c_inch");
						columnAddRowsCount(dimHm, rs, i, "d_inch");
						columnAddRowsCount(dimHm, rs, i, "d_OR_inch");
						columnAddRowsCount(dimHm, rs, i, "e_inch");
						columnAddRowsCount(dimHm, rs, i, "f_inch");
						columnAddRowsCount(dimHm, rs, i, "f_diam_inch");
						columnAddRowsCount(dimHm, rs, i, "k_inch");
						columnAddRowsCount(dimHm, rs, i, "l_inch");
						columnAddRowsCount(dimHm, rs, i, "s_inch");
						columnAddRowsCount(dimHm, rs, i, "s_OR_inch");
						columnAddRowsCount(dimHm, rs, i, "z_inch");
						columnAddRowsCount(dimHm, rs, i, "h_inch");
						columnAddRowsCount(dimHm, rs, i, "h1_inch");
						columnAddRowsCount(dimHm, rs, i, "h2_inch");
						columnAddRowsCount(dimHm, rs, i, "v_inches");
					}
			}

			for(int i = 0; i < rs.GetRecordCount(); i++)
				if((allProducts || rs.GetValueAt(i, "catalogue").GetStringValue().equals("YES")) && !rs.GetValueAt(i, "phase").GetStringValue().equals("BLOCKED") && rs.GetValueAt(i, "printable").GetStringValue().equals("YES") && rs.GetValueAt(i, "public").GetStringValue().equals("YES"))
				{
					ProdFittingsRec rec = new ProdFittingsRec(isISO);
					rec.setName(rs.GetValueAt(i, "Name"));
					rec.setNominalSize(rs.GetValueAt(i, "nominal_size"));
					if(rec.setThread(rs.GetValueAt(i, "thread")))
						header.setColLabel("Thread", "T");
					if(rec.setPipeMm(rs.GetValueAt(i, "pipe_mm")))
						header.setColLabel("Pipe", "mm");
					if(rec.setFlangeInch(rs.GetValueAt(i, "flange_inch")))
						header.setColLabel("Flange", "Inch");
					if(rec.setBolt(rs.GetValueAt(i, "bolt_mm")))
						header.setColLabel("Bolt", "Length (mm)");
					if(isISO)
					{
						if(rec.setWorkPressBar(rs.GetValueAt(i, "working_press_bar")))
							header.setColLabel("W.P.", "Bar");
						if(rec.setDimValue(dimHm, rs, i, "b_mm")) header.setDimLabel("B", true);
						if(rec.setDimValue(dimHm, rs, i, "a_mm")) header.setDimLabel("A", true);
						if(rec.setDimValue(dimHm, rs, i, "c_mm")) header.setDimLabel("C", true);
						if(rec.setDimValue(dimHm, rs, i, "d_mm")) header.setDimLabel("D", true);
						if(rec.setDimValue(dimHm, rs, i, "d_OR_mm")) header.setDimLabel("d-OR", true);
						if(rec.setDimValue(dimHm, rs, i, "e_mm")) header.setDimLabel("E", true);
						if(rec.setDimValue(dimHm, rs, i, "f_mm")) header.setDimLabel("F", true);
						if(rec.setDimValue(dimHm, rs, i, "f_diam_mm")) header.setDimLabel("F\330", true);
						if(rec.setDimValue(dimHm, rs, i, "k_mm")) header.setDimLabel("K", true);
						if(rec.setDimValue(dimHm, rs, i, "l_mm")) header.setDimLabel("L", true);
						if(rec.setDimValue(dimHm, rs, i, "s_mm")) header.setDimLabel("S", true);
						if(rec.setDimValue(dimHm, rs, i, "s_OR_mm")) header.setDimLabel("s-OR", true);
						if(rec.setDimValue(dimHm, rs, i, "z_mm")) header.setDimLabel("Z", true);
						if(rec.setDimValue(dimHm, rs, i, "h_mm")) header.setDimLabel("H", true);
						if(rec.setDimValue(dimHm, rs, i, "h1_mm")) header.setDimLabel("H1", true);
						if(rec.setDimValue(dimHm, rs, i, "h2_mm")) header.setDimLabel("H2", true);
						if(rec.setDimValue(dimHm, rs, i, "v_mm")) header.setDimLabel("V", true);
					} else
					{
						if(rec.setWorkPressPSI(rs.GetValueAt(i, "working_press_psi")))
							header.setColLabel("W.P.", "Psi");
						if(rec.setDimValue(dimHm, rs, i, "b_inch")) header.setDimLabel("B", false);
						if(rec.setDimValue(dimHm, rs, i, "a_inch")) header.setDimLabel("A", false);
						if(rec.setDimValue(dimHm, rs, i, "c_inch")) header.setDimLabel("C", false);
						if(rec.setDimValue(dimHm, rs, i, "d_inch")) header.setDimLabel("D", false);
						if(rec.setDimValue(dimHm, rs, i, "d_OR_inch")) header.setDimLabel("d-OR", false);
						if(rec.setDimValue(dimHm, rs, i, "e_inch")) header.setDimLabel("E", false);
						if(rec.setDimValue(dimHm, rs, i, "f_inch")) header.setDimLabel("F", false);
						if(rec.setDimValue(dimHm, rs, i, "f_diam_inch")) header.setDimLabel("F\330", false);
						if(rec.setDimValue(dimHm, rs, i, "k_inch")) header.setDimLabel("K", false);
						if(rec.setDimValue(dimHm, rs, i, "l_inch")) header.setDimLabel("L", false);
						if(rec.setDimValue(dimHm, rs, i, "s_inch")) header.setDimLabel("S", false);
						if(rec.setDimValue(dimHm, rs, i, "s_OR_inch")) header.setDimLabel("s-OR", false);
						if(rec.setDimValue(dimHm, rs, i, "z_inch")) header.setDimLabel("Z", false);
						if(rec.setDimValue(dimHm, rs, i, "h_inch")) header.setDimLabel("H", false);
						if(rec.setDimValue(dimHm, rs, i, "h1_inch")) header.setDimLabel("H1", false);
						if(rec.setDimValue(dimHm, rs, i, "h2_inch")) header.setDimLabel("H2", false);
						if(rec.setDimValue(dimHm, rs, i, "v_inches")) header.setDimLabel("V", false);
					}
					header.setLabelAdded(true);
					String hmKey = rec.getName();
					if(!hm.containsKey(hmKey))
						hm.put(hmKey, rec);
				}

			listRec = new LinkedList(hm.values());
		}
		catch(StringException e)
		{
			System.out.println("Error logging out of MDM server: " + e.getMessage());
			return null;
		}
		return listRec;
	}

	private CatalogData catalogData;
	private MeasurementManager measurementManager;
	private static int cachePort = 2345;
//	*** PRODUZIONE ***
//	private static String cacheServer = "manepprd.sapman.dc";
//	private static String cacheUser = "Admin";
//	private static String cachePassword = "mdm prod";
//	private static String cacheDir = "E:\\MDMPortal\\reports\\output\\fittings\\";
//	*** PRODUZIONE ***

//	***  SVILUPPO  ***
	private static String cacheServer = "manepsvi.sapman.dc";
	private static String cacheUser = "Admin";
	private static String cachePassword = "admin";
	private static String cacheDir = "F:\\MDMPortal\\reports\\output\\fittings\\";	
//	***  SVILUPPO  ***
	private boolean allProducts;
	private String recId;
	private String strWatermark;	
	private List headerFields;
	private List recFields;
	private int headRecordID;

}
