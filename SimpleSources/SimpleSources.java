/*
 * Created on 19-dic-2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

public class SimpleSources {

JRDataSource simpleDS;
JRMapCollectionDataSource subDS1;
JRMapCollectionDataSource subDS2;

public SimpleSources() {

try {

List simpleSubMasterList = new ArrayList();
Map simpleSubMasterMap = new HashMap();
simpleSubMasterMap.put("subTitle", "This is the Subreport1 Title");
simpleSubMasterList.add(simpleSubMasterMap);

simpleSubMasterMap = new HashMap();
simpleSubMasterMap.put("subTitle", "This is the Subreport1 Title2");
simpleSubMasterList.add(simpleSubMasterMap);

simpleSubMasterMap = new HashMap();
simpleSubMasterMap.put("subTitle", "This is the Subreport1 Title3");
simpleSubMasterList.add(simpleSubMasterMap);

subDS1 = new JRMapCollectionDataSource(simpleSubMasterList);

simpleSubMasterList = new ArrayList();
simpleSubMasterMap = new HashMap();
simpleSubMasterMap.put("subTitle", "This is the Subreport2 Title");
simpleSubMasterList.add(simpleSubMasterMap);

simpleSubMasterMap = new HashMap();
simpleSubMasterMap.put("subTitle", "This is the Subreport2 Title2");
simpleSubMasterList.add(simpleSubMasterMap);

simpleSubMasterMap = new HashMap();
simpleSubMasterMap.put("subTitle", "This is the Subreport2 Title3");
simpleSubMasterList.add(simpleSubMasterMap);

simpleSubMasterMap = new HashMap();
simpleSubMasterMap.put("subTitle", "This is the Subreport2 Title4");
simpleSubMasterList.add(simpleSubMasterMap);

subDS2 = new JRMapCollectionDataSource(simpleSubMasterList);

Map simpleMasterMap = new HashMap();
simpleMasterMap.put("master","This is the Master JRMapCollectionDataSource");
simpleMasterMap.put("title", "SIMPLE_DATA TITLE");
simpleMasterMap.put("subReport1DS", subDS1);
simpleMasterMap.put("subReport2DS", subDS2);
List simpleMasterList = new ArrayList();
simpleMasterList.add(simpleMasterMap);
simpleDS = new JRMapCollectionDataSource(simpleMasterList);


String pdfFile = JasperRunManager.runReportToPdfFile("C:/Job/ManuliJasper/jasper/masterReport.jasper", null, simpleDS);
System.out.println("pdfFile=" + pdfFile);

} catch (Exception e) {
e.printStackTrace();
System.exit(1);
}
}

public static void main(String[] args) {
new SimpleSources();
System.out.println("done");
}
}
