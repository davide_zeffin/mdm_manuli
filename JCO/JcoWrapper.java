import com.sap.mw.jco.IRepository;
import com.sap.mw.jco.JCO;

import java.io.IOException;
import java.util.*;
/*
 * Created on 18-lug-2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JcoWrapper
{
		  private static JcoServer jcs = null;

		  public static void startServer() throws IOException
		 {
		  java.util.Properties props = new java.util.Properties();
		  java.net.URL url = ClassLoader.getSystemResource("logon.properties");
		  props.load(url.openStream());

		  JCO.Client client = JCO.createClient(props);

		  IRepository repository = JCO.createRepository("REP", client);

		  JcoServer jcs = new JcoServer("gwhost",  //gateway host, often the same as host
						  			    "sapgw00", //gateway service, generally sapgw+<SYSNR>
			  							"JCOSERVER01", // corresponds to program ID defined in SM59
			 							repository);


		  jcs.start();
		 }

		 public static void stopServer()
		 {
				  jcs.stop();
		 }

		 public void main(String[] args)
		 {
				  jcs.startServer() ;
		 }

} 
