/*
 * Created on Sep 15, 2011
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package it.sdb;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.sap.aii.mapping.api.AbstractTransformation;
import com.sap.aii.mapping.api.StreamTransformationException;
import com.sap.aii.mapping.api.TransformationInput;
import com.sap.aii.mapping.api.TransformationOutput;

/*
 * @author sdb.it
 */
public class getBarcode extends AbstractTransformation {
	String result = "";

	/* The transform method which must be implemented */
	public void transform(TransformationInput in, TransformationOutput out)
			throws StreamTransformationException {
		
		InputStream instream = in.getInputPayload().getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i;
	
		try {
			while (( i = instream.read()) != -1){
				baos.write(i);
				
			}
			result = baos.toString();
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//          	
		
		// String for constructing target message structure
		String fresult = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		fresult = fresult
				.concat("<ns0:eDocsResponse xmlns:ns0=\"urn:sdb.it:EDOCS:Invoice\">");
		fresult = fresult.concat("<result>");
		fresult = fresult.concat(result);
		fresult = fresult.concat("</result>");

		fresult = fresult.concat("</ns0:eDocsResponse>");

		try {
			out.getOutputPayload().getOutputStream().write(fresult.getBytes());
			/* assigning the created target message to "TransformationOutput" */
		} catch (IOException e1) {
		}
	}

}