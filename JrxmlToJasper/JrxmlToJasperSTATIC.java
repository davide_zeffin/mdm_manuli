import net.sf.jasperreports.engine.JasperCompileManager;

class JrxmlToJasperSTATIC {
	public static String JASPER_REPORT_FOLDER = "C:/Job/JAVA/MDMPortalSTATIC/reports/MDMJasper/";
	public static String JASPER_FILENAME_EXT = "ManuliExtCrimping";
	public static String JASPER_FILENAME = "ManuliIntCrimping";
	public static String JASPER_FILENAME_SUB1 = "ManuliIntCrimpingSub1";
	public static String JASPER_FILENAME_SUB2 = "ManuliIntCrimpingSub2";
	public static String JASPER_FILENAME_CAT = "ManuliCatalogue";
	public static String JASPER_FILENAME_CAT_EXT = "ManuliCatalogueExtended";
	public static String JASPER_FILENAME_MARCATURE = "ManuliCatalogueMarcature";
	public static String JASPER_FILENAME_MACHINE_CATALOGUE = "ManuliMachineCatalogue";
	public static String JASPER_FILENAME_MACHINE_CATALOGUE_SUB1 = "ManuliMachineCatalogueSub1";
	public static String JASPER_FILENAME_MACHINE_CATALOGUE_SUB2 = "ManuliMachineCatalogueSub2";
	public static String JASPER_FILENAME_MACHINE_CATALOGUE_SUB3 = "ManuliMachineCatalogueSub3";
	public static String JASPER_FILENAME_MACHINE_CATALOGUE_SUB4 = "ManuliMachineCatalogueSub4";
	public static String JASPER_FILENAME_MACHINE_REFERENCE = "ManuliMachineReference";
	public static String JASPER_FILENAME_MACHINE_REFERENCE_SUB1 = "ManuliMachineReferenceSub1";
	public static String JASPER_FILENAME_MACHINE_REFERENCE_SUB2 = "ManuliMachineReferenceSub2";
	public static String JASPER_FILENAME_MACHINE_REFERENCE_SUB3 = "ManuliMachineReferenceSub3";
	public static String JASPER_FILENAME_MACHINE_FAMILIES = "ManuliMachineFamilies";
	
	public static void main(String[] args) throws Exception {
		System.out.println("Started.");
		//		caricamento file JRXML
//		JasperDesign jasperDesign =
//			JRXmlLoader.load(
//				JASPER_REPORT_FOLDER + JASPER_FILENAME + ".jrxml");
		System.out.println("Started.");
		

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_SUB1 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_SUB1 + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_SUB2 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_SUB2 + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_EXT + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_EXT + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_CAT + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_CAT + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_CAT_EXT + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_CAT_EXT + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MARCATURE + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MARCATURE + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE_SUB1 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE_SUB1 + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE_SUB2 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE_SUB2 + ".jasper");  //the path and name we want to
	
		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE_SUB3 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE_SUB3 + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE_SUB4 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_CATALOGUE_SUB4 + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_REFERENCE + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_REFERENCE + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_REFERENCE_SUB1 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_REFERENCE_SUB1 + ".jasper");  //the path and name we want to
	
		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_REFERENCE_SUB2 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_REFERENCE_SUB2 + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_REFERENCE_SUB3 + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_REFERENCE_SUB3 + ".jasper");  //the path and name we want to

		JasperCompileManager.compileReportToFile(
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_FAMILIES + ".jrxml",    //the path to the jrxml file to compile
		JASPER_REPORT_FOLDER + JASPER_FILENAME_MACHINE_FAMILIES + ".jasper");  //the path and name we want to

		//compilazione del file e generazione del file JASPER
//		JasperCompileManager.compileReportToFile(
//			jasperDesign,
//			JASPER_REPORT_FOLDER + JASPER_FILENAME + ".jasper");
	}
}
