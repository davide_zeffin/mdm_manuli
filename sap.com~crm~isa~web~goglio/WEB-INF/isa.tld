<?xml version="1.0" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
    "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<!-- a tab library descriptor -->

<taglib>
  <tlibversion>1.0</tlibversion>
  <jspversion>1.1</jspversion>
  <shortname>isa</shortname>
  <info>
    Tag library for ISA Applications
  </info>

  <!-- tags -->
      <tag>
          <name>mimeURL</name>
          <tagclass>com.sap.isa.core.taglib.MimeURLTag</tagclass>
          <bodycontent>JSP</bodycontent>


          <attribute>
             <name>name</name>
             <required>true</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>theme</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>language</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

      </tag>


      <tag>
          <name>webappsURL</name>
          <tagclass>com.sap.isa.core.taglib.WebappsURLTag</tagclass>
          <bodycontent>JSP</bodycontent>

          <attribute>
             <name>name</name>
             <required>true</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>secure</name>
             <required>false</required>
          </attribute>

          <attribute>
             <name>anchor</name>
             <required>false</required>
          </attribute>

          <attribute>
             <name>completeUrl</name>
             <required>false</required>
          </attribute>

      </tag>

      <tag>
          <name>reentryURL</name>
          <tagclass>com.sap.isa.core.taglib.ReEntryURLTag</tagclass>
          <bodycontent>JSP</bodycontent>

          <attribute>
             <name>name</name>
             <required>true</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>secure</name>
             <required>false</required>
          </attribute>

          <attribute>
             <name>anchor</name>
             <required>false</required>
          </attribute>

          <attribute>
             <name>completeUrl </name>
             <required>false</required>
          </attribute>

      </tag>

      <tag>
          <name>param</name>
          <tagclass>com.sap.isa.core.taglib.ParameterTag</tagclass>
          <bodycontent>empty</bodycontent>

          <attribute>
             <name>name</name>
             <required>true</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>value</name>
             <required>true</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>contextValue</name>
             <required>false</required>
          </attribute>
      </tag>


<!-- *************************************
     This tag is DEPRECATED and will be removed in one of the succeeding releases to CRM 5.2.  
     Please use instead IsaLogTag 
     ************************************* -->
      <tag>
          <name>logcategory</name>
          <tagclass>com.sap.isa.core.taglib.LogCategoryTag</tagclass>
          <attribute>
            <name>priority</name>
            <required>false</required>
          </attribute>
          <attribute>
            <name>name</name>
            <required>false</required>
          </attribute>
     </tag>

<!-- *************************************
     This tag is DEPRECATED and will be removed in one of the succeeding releases to CRM 5.2.
     Please use instead IsaLogTag 
     ************************************* -->
     <tag>
         <name>log</name>
         <tagclass>com.sap.isa.core.taglib.LogTag</tagclass>
         <attribute>
            <name>priority</name>
            <required>true</required>
         </attribute>
         <attribute>
            <name>key</name>
            <required>true</required>
         </attribute>
          <attribute>
             <name>arg0</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
             <name>arg1</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
             <name>arg2</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
             <name>arg3</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
             <name>arg4</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>
     </tag>

     <tag>
          <name>debugmsg</name>
          <tagclass>com.sap.isa.core.taglib.DebugMsgTag</tagclass>
          <attribute>
            <name>logtxt</name>
            <required>false</required>
          </attribute>
          <attribute>
            <name>headtxt</name>
            <required>false</required>
          </attribute>
          <attribute>
            <name>displaymode</name>
            <required>false</required>
          </attribute>
     </tag>

     <tag>
         <name>iterate</name>
         <tagclass>com.sap.isa.core.taglib.IterateTag</tagclass>
         <teiclass>com.sap.isa.core.taglib.IterateTagTEI</teiclass>
         <bodycontent>JSP</bodycontent>

         <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
         </attribute>

         <attribute>
            <name>id</name>
            <required>true</required>
            <rtexprvalue>false</rtexprvalue>
         </attribute>

         <attribute>
            <name>type</name>
            <required>true</required>
            <rtexprvalue>false</rtexprvalue>
         </attribute>

         <attribute>
            <name>ignoreNull</name>
            <required>false</required>
            <rtexprvalue>false</rtexprvalue>
         </attribute>

         <attribute>
            <name>resetCursor</name>
            <required>false</required>
            <rtexprvalue>false</rtexprvalue>
         </attribute>

     </tag>

     <tag>
         <name>message</name>
         <tagclass>com.sap.isa.core.taglib.MessageTag</tagclass>
         <teiclass>com.sap.isa.core.taglib.MessageTagTEI</teiclass>
         <bodycontent>JSP</bodycontent>

         <attribute>
            <name>name</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
         </attribute>

         <attribute>
            <name>id</name>
            <required>true</required>
            <rtexprvalue>false</rtexprvalue>
         </attribute>

         <attribute>
            <name>type</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
         </attribute>

         <attribute>
            <name>property</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
         </attribute>

         <attribute>
            <name>ignoreNull</name>
            <required>false</required>
            <rtexprvalue>false</rtexprvalue>
         </attribute>
     </tag>


      <tag>
          <name>translate</name>
          <tagclass>com.sap.isa.core.taglib.TranslateTag</tagclass>
          <bodycontent>empty</bodycontent>

          <attribute>
             <name>key</name>
             <required>true</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

<!--
          <attribute>
             <name>lang</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>
-->
          <attribute>
             <name>arg0</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg1</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg2</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg3</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg4</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>html0</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>html1</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>html2</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>html3</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>html4</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
             <name>html5</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>
      </tag>

	  <tag>
          <name>UCtranslate</name>
          <tagclass>com.sap.isa.core.taglib.TranslateTagUnicode</tagclass>
          <bodycontent>empty</bodycontent>

          <attribute>
             <name>key</name>
             <required>true</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg0</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg1</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg2</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg3</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

          <attribute>
             <name>arg4</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

      </tag>
      
      <tag>
          <name>encodeURL</name>
          <tagclass>com.sap.isa.core.taglib.EncodeURLTag</tagclass>
          <bodycontent>empty</bodycontent>

          <attribute>
             <name>text</name>
             <required>yes</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

      </tag>


      <tag>
          <name>contentType</name>
          <tagclass>com.sap.isa.core.taglib.ContentTypeTag</tagclass>
          <teiclass>com.sap.isa.core.taglib.ContentTypeTagTEI</teiclass>
          <bodycontent>empty</bodycontent>

          <attribute>
             <name>type</name>
             <required>false</required>
             <rtexprvalue>true</rtexprvalue>
          </attribute>

      </tag>

      <tag>
          <name>moduleName</name>
          <tagclass>com.sap.isa.core.taglib.ModuleNameTag</tagclass>
          <bodycontent>empty</bodycontent>
          <attribute>
            <name>name</name>
            <required>true</required>
          </attribute>
      </tag>

      <tag>
          <name>imageAttribute</name>
          <tagclass>com.sap.isa.catalog.taglib.ImageAttributeTag</tagclass>
          <bodycontent>empty</bodycontent>
          <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
            <name>guids</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
            <name>defaultImg</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
            <name>width</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
            <name>height</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
          </attribute>
          <attribute>
            <name>language</name>
            <required>false</required>
          </attribute>

      </tag>
      
      <tag>
            <name>ifImageAvailable</name>
            <tagclass>com.sap.isa.catalog.taglib.IfImageAvailableTag</tagclass>
            <bodycontent>JSP</bodycontent>

            <attribute>
                  <name>name</name>
                  <required>true</required>
                  <rtexprvalue>true</rtexprvalue>
            </attribute>
            <attribute>
                  <name>guids</name>
                  <required>false</required>
                  <rtexprvalue>true</rtexprvalue>
            </attribute>
      </tag>
      <tag>
            <name>ifImageNotAvailable</name>
            <tagclass>com.sap.isa.catalog.taglib.IfImageNotAvailableTag</tagclass>
            <bodycontent>JSP</bodycontent>

            <attribute>
                  <name>name</name>
                  <required>true</required>
                  <rtexprvalue>true</rtexprvalue>
            </attribute>
            <attribute>
                  <name>guids</name>
                  <required>false</required>
                  <rtexprvalue>true</rtexprvalue>
            </attribute>
      </tag>
      <tag>
            <name>ifThumbAvailable</name>
            <tagclass>com.sap.isa.catalog.taglib.IfThumbAvailableTag</tagclass>
            <bodycontent>JSP</bodycontent>

            <attribute>
                  <name>name</name>
                  <required>true</required>
                  <rtexprvalue>true</rtexprvalue>
            </attribute>
            <attribute>
                  <name>guids</name>
                  <required>false</required>
                  <rtexprvalue>true</rtexprvalue>
            </attribute>
      </tag>
      <tag>
            <name>ifThumbNotAvailable</name>
            <tagclass>com.sap.isa.catalog.taglib.IfThumbNotAvailableTag</tagclass>
            <bodycontent>JSP</bodycontent>

            <attribute>
                  <name>name</name>
                  <required>true</required>
                  <rtexprvalue>true</rtexprvalue>
            </attribute>
            <attribute>
                  <name>guids</name>
                  <required>false</required>
                  <rtexprvalue>true</rtexprvalue>
            </attribute>
      </tag>
                        
      <!-- This tag includes the stylesheets in the JSP pages -->
      <tag>
          <name>includes</name>
          <tagclass>com.sap.isa.core.taglib.IncludeTag</tagclass>
          <bodycontent>empty</bodycontent>
      </tag>

      <!-- This tag includes the stylesheets in the JSP pages -->
      <tag>
          <name>stylesheets</name>
          <tagclass>com.sap.isa.core.taglib.StyleSheetsTag</tagclass>
          <bodycontent>empty</bodycontent>
          <attribute>
          	<name>theme</name>
			<required>false</required>
		  </attribute>	
          <attribute>
             <name>group</name>
             <required>false</required>
          </attribute>
      </tag>

      <tag>
        <name>suppressNullStr</name>
        <tagclass>com.sap.isa.catalog.taglib.SuppressNullStringTag</tagclass>
        <bodycontent>empty</bodycontent>
        <attribute>
                <name>string</name>
                <required>true</required>
                <rtexprvalue>true</rtexprvalue>
        </attribute>
      </tag>

       <tag>
            <name>ifPortal</name>
            <tagclass>com.sap.isa.core.taglib.IfPortalTag</tagclass>
            <bodycontent>JSP</bodycontent>

             <attribute>
                <name>value</name>
                <required>false</required>
                <rtexprvalue>false</rtexprvalue>
             </attribute>

             <attribute>
                <name>ignoreNull</name>
                <required>false</required>
                <rtexprvalue>false</rtexprvalue>
             </attribute>
        </tag>

       <tag>
            <name>PricingToolUrl</name>
            <tagclass>com.sap.isa.core.taglib.PricingToolUrlTag</tagclass>
            <bodycontent>JSP</bodycontent>
        </tag>

       <tag>
            <name>reloginURL</name>
            <tagclass>com.sap.isa.core.taglib.ReLoginURLTag</tagclass>
            <bodycontent>JSP</bodycontent>
        </tag>

     <tag>
         <name>helpValues</name>
         <tagclass>com.sap.isa.isacore.taglib.HelpValuesTag</tagclass>
         <teiclass>com.sap.isa.isacore.taglib.HelpValuesTagTEI</teiclass>
         <bodycontent>JSP</bodycontent>

         <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
         </attribute>

         <attribute>
            <name>id</name>
            <required>true</required>
            <rtexprvalue>false</rtexprvalue>
         </attribute>

         <attribute>
            <name>ignoreNull</name>
            <required>false</required>
            <rtexprvalue>false</rtexprvalue>
         </attribute>
     </tag>

</taglib>

