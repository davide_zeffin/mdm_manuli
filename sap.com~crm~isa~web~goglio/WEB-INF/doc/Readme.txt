This product includes software developed by the Apache Software Foundation 
(http://www.apache.org/).

This product includes the library Pushlets, a software developed under 
the GNU Lesser General Public License.
The source code of the library Pushlets can be obtained, 
free of charge besides the costs of shipping and handling, from us. 

We gurantee the support of the included open source libraries, with the
exception of the struts-tags,  which are not used by our application.
