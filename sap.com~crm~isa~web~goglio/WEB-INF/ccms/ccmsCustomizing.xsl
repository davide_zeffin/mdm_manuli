<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes" method="xml" />
	<xsl:param name="scendesc">dummy</xsl:param>
	<xsl:param name="URL">http://egal:50000/egal21</xsl:param>
	<xsl:template match="text()"/>
	<xsl:template match="scenarios">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="/">
		<customizing>
			<control>
				<grmgruns>X</grmgruns>
				<runlog/>
				<errorlog/>
			</control>
			<scenarios>
			<xsl:apply-templates/>
			</scenarios>
		</customizing>
	</xsl:template>
	<xsl:template name="getHostFromUrl">
		<xsl:param name="URL"/>
		<xsl:variable name="Url1" select="substring-after($URL, 'http://')"/>
		<xsl:choose>
			<xsl:when test="contains($Url1,'/')"><xsl:value-of select="substring-before($Url1,'/')"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$Url1"/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="scenariodescription">
		<xsl:param name="scenario-name"/>
		<xsl:variable name="maxlength" select="(39 - string-length($scendesc)) div 2"/>
		<xsl:choose>
			<xsl:when test="string-length(concat($scendesc,$scenario-name))>39">
				<xsl:value-of select="concat($scendesc, substring($scenario-name,1,$maxlength - 1),substring($scenario-name,string-length($scenario-name) + 1 - $maxlength))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($scendesc,$scenario-name)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="tests">
		<scenario>
			<scenname>
				<xsl:value-of select="@ccmsscenname"/>
			</scenname>
			<scenversion/>
			<sceninst>
				<xsl:value-of select="position()"/>
			</sceninst>
			<scentype>URL</scentype>
			<scenstarturl>
				<xsl:value-of select="concat($URL, '/ccms/init.do?scenario.xcm=',@scenario-name)"/>
			</scenstarturl>
			<scenstartmod>unknown</scenstartmod>
			<scentexts>
				<scentext>
					<scenlangu>E</scenlangu>
					<scendesc>
						<xsl:call-template name="scenariodescription">
							<xsl:with-param name="scenario-name" select="@scenario-name"/>
						</xsl:call-template>
					</scendesc>
				</scentext>
			</scentexts>
			<components>
		<component>
			<compname>webapp</compname>
			<compversion/>
			<comptype>Not Used</comptype>
			<comptexts>
				<comptext>
					<complangu>E</complangu>
					<compdesc>Web Application <xsl:value-of select="@ccmsscenname"/></compdesc>
				</comptext>
			</comptexts>
			<properties>
				<property>
					<!-- an application needs to have at least one property to be visible on the ccms system -->
					<propname>dummy</propname>
					<propvalue>dummy</propvalue>
				</property>
			</properties>
		</component>
		<xsl:apply-templates/>
			</components>
		</scenario>
	</xsl:template>
	<xsl:template match="component">
		<xsl:choose>
			<xsl:when test="(@index and @selected) or not(@index)">
		<component>
			<compname>
				<xsl:value-of select="@ccmsname"/>
			</compname>
			<compversion/>
			<comptype>Not Used</comptype>
			<comptexts>
				<comptext>
					<complangu>E</complangu>
					<compdesc>
						<xsl:value-of select="concat(@configuration,' (',@id,')')"/>
					</compdesc>
				</comptext>
			</comptexts>
			<properties>
				<property>
					<!-- the comptype will not be passed to the ccms application, but it's required to identify a component -->
					<propname>componentId</propname>
					<propvalue>
						<xsl:value-of select="@configuration"/>
					</propvalue>
				</property>
				<property>
					<!-- the comptype will not be passed to the ccms application, but it's required to identify a component -->
					<propname>componentName</propname>
					<propvalue>
						<xsl:value-of select="@id"/>
					</propvalue>
				</property>
			</properties>
		</component>
			</xsl:when>
			<xsl:otherwise>
				<xsl:comment>component <xsl:value-of select="concat(@configuration, '(',@id)"/>) was not selected</xsl:comment>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
