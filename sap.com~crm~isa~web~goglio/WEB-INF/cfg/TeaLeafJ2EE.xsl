<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:param name="tealeaf.logdir">c:\temp</xsl:param>
<xsl:param name="tealeaf.server">localhost</xsl:param>
<xsl:param name="tealeaf.port">1966</xsl:param>
<xsl:param name="tealeaf.user">user</xsl:param>
<xsl:param name="tealeaf.password">password</xsl:param>

<xsl:template match="/">
<tealeaf>
	<jmxMgmt on="false" htmlAdaptor="false" htmlAdaptorPort="8082" rmiAdaptor="false" rmiAdaptorPort="1099"/>
	<!-- Available capture modes:
        business - capture text data only
        it       - capture all request data and responses for text data only
        it+      - capture all request/response data

	 Available sessionization modes:
	 	session  - using application server session model
	 	cookie   - using Tealeaf's session model (inserting TLTSID session cookie)
-->
	<captureSource mode="it" excludelist=".gif;.jpg;.jpeg;.bmp;.pdf;.zip;.gzip;.png;.exe;.ico;.swf;.mov;.avi;.tar" sessionizationMode="cookie"/>
	<!-- Available log levels:
    	WARNING = 1;
    	INFO = 2;
    	ERROR = 4;
    	EXCEPTION = 8;
    	ALL_LEVELS = -1;

	You can use multiple levels, e.g. WARNING + INFO = 1 + 2 = 3
-->
	<!-- here you can change the dircetory, which is used for log files -->
	<logger on="true" dir="{$tealeaf.logdir}" level="-1"/>
	<!-- These attributes can also be used with general or other pipelines
	    failoverAgent="tlaWriter"
  	    decoupler="true"
	    maxQueueSize="10000"
-->
	<!-- to enable capturing: set enable on true -->
	<general enabled="true" jmxMgmt="false" sessionIdRequired="true" startAgent="socketWriter" failoverAgent="tlaWriter" decoupler="true" maintenancePeriod="600">
		<!-- These attributes can also be used with tlaWriter
	 	fileId="aaa"
-->
		<tlaWriter class="com.tealeaf.pipeline.sessionagents.tar.TARFile" logDirName="{$tealeaf.logdir}" tlaExtension="tla" rollSize="2048" rollTime="16:30"/>
		<!-- please enter the used tealeaf server -->
		<socketWriter class="com.tealeaf.pipeline.sessionagents.socket.SocketWriter" server="{$tealeaf.server}" port="{$tealeaf.port}"/>
		<canisterWriter class="com.tealeaf.pipeline.sessionagents.canister.CanisterWriter" user="{$tealeaf.user}" password="{$tealeaf.password}" server="{$tealeaf.server}"/>
	</general>
</tealeaf>
</xsl:template>
</xsl:stylesheet>
