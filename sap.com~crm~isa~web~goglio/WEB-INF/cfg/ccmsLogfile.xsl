<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="application-context-root"/>
	<xsl:param name="application-name">application</xsl:param>
	<xsl:output  method="text" />
<xsl:template match="/">
LOGFILE_TEMPLATE
DIRECTORY="<xsl:value-of select="$application-context-root"/>/WEB-INF/logs"
FILENAME="isa.log*"
MTE_CLASS="CRM.ISA.<xsl:value-of select="$application-name"/>_log"
PREFIX="<xsl:value-of select="$application-name"/>"
PATTERN_0="Fatal"
VALUE_0=RED
MESSAGECLASS_0="SAPT001"
MESSAGEID_0="rt 584"
.
</xsl:template>
</xsl:stylesheet>
