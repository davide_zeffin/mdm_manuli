<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.iview.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.portalurlgenerator.businessobject.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ page import="com.sap.isa.isacore.actionform.order.DocumentListSelectorForm" %>
<%@ page import="com.sap.isa.isacore.action.CrossEntryAction" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ include file="/b2b/usersessiondata.inc" %>
<%@ include file="portalnavigation.inc" %>

<% BaseUI ui = new BaseUI(pageContext);
   SoldFrom soldFrom = (SoldFrom)request.getAttribute(CCHSetupStartAction.RK_CCHSG_SALESPARTNER);
   String cchScenario = "";
   if (soldFrom.getShopCall().equals("1")  ||  soldFrom.getShopCall().equals("2") ) {
       // Unsupported scenario combination would have ended on errorpage!!
       cchScenario = "sc";
   } else {
       cchScenario = "oh";
   }
   String resKey00, resKey01, resKey02, resKey03, resKey04;

   String notCompletedOrders   = (String)request.getAttribute(CCHSetupStartAction.RK_CCHSG_OPENORDERS);
   String allOrdersOfLast7Days = (String)request.getAttribute(CCHSetupStartAction.RK_CCHSG_ALLORDERSOFLAST7DAYS);  
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title>SAP Internet Sales CCH Setup Guide</title>
        <%-- isa:includes/ --%>
        <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css")%>"
                type="text/css" rel="stylesheet" />

        <script src="<%=WebUtil.getMimeURL(pageContext, "/iviews/jscript/crm_isa_epcm.js")%>"
                type="text/javascript">
        </script>
        <script src="<%=WebUtil.getMimeURL(pageContext, "/appbase/jscript/portalnavigation.js.jsp")%>"
                type="text/javascript">
        </script>
    </head>
    <body style="width: 97%; height: 95%;">

        <div id="newdoc" style="HEIGHT: 100%; WIDTH: 100;">
            <div class="module-name"><isa:moduleName name="iviews/setupguide/oh_activated.jsp" /></div>
            <br/>
            <table border="0"  cellpadding="2" width="100%" cellspacing="0">
            <tr><td valign="top" width="50%">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr>
                    <td valign="top" align="right" width="15%">
                     <img src="<%=WebUtil.getMimeURL(pageContext, "iviews/setupguide/cch/mimes/images/fa1.gif") %>" alt="<isa:translate key="cch.setup.ac.part010.link"/>" border="0" />
                     &nbsp;
                    </td>
                    <td><% //targetUrl = urlGen.readPageURL(crmObjectTypeSalesDocsCRM, urlSalesDocPars, portalURL, navModelVersion);
                            AnchorAttributes anchorAttrSalesItem = urlGen.readPageURL(crmObjectTypeSalesDocsCRM,"","",crmMethodSalesDocsCRM,"",urlSalesDocPars,request);%>
                      <% if (!anchorAttrSalesItem.getURI().equals("")) {  %>
                        <a href="<%=anchorAttrSalesItem.getURI()%>" onclick="doNavigate('<%=anchorAttrSalesItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.ac.part010.link"/>">
                          <b><isa:translate key="cch.setup.ac.part010.link"/></b>
                        </a>
                        <% }
                            else { %>
                            <b><span><isa:translate key="cch.setup.ac.part010.link"/></span></b>
                            <%}
                        %>

                        &nbsp;
                        <br/>
                        <span><isa:translate key="cch.setup.ac.part011"/>&nbsp;</span>
                        <br/><br/>
                        <%  urlSalesDocPars.add(CrossEntryAction.RP_NEXT_ACTION,            "cedocumentlist");
                            urlSalesDocPars.add(DocumentListSelectorForm.DOCUMENT_TYPE,     DocumentListSelectorForm.ORDER);
                            urlSalesDocPars.add(DocumentListSelectorForm.DOCUMENT_STATUS,   DocumentListSelectorForm.ALL);
                            urlSalesDocPars.add(DocumentListSelectorForm.VALIDITYSELECTION, DocumentListSelectorForm.PERIOD);                                                        
                            urlSalesDocPars.add(DocumentListSelectorForm.PERIOD,            DocumentListSelectorForm.LAST_WEEK);                                                        
                            anchorAttrSalesItem = urlGen.readPageURL(crmObjectTypeSalesDocsCRM,"","",crmMethodSalesDocsCRM,"",urlSalesDocPars,request);
                        %>
                        <% if (!anchorAttrSalesItem.getURI().equals("")) {  %>
                          <a href="<%=anchorAttrSalesItem.getURI()%>" onclick="doNavigate('<%=anchorAttrSalesItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.ac.part012.link" arg0="<%= allOrdersOfLast7Days %>"/>">
                            <isa:translate key="cch.setup.ac.part012.link" arg0="<%= allOrdersOfLast7Days %>"/>
                          </a>
                        <% }
                            else { %>
                            <span><isa:translate key="cch.setup.ac.part012.link" arg0="<%= allOrdersOfLast7Days %>"/></span>
                            <%}
                        %>
                        <br/>
                        <%  urlSalesDocPars = new UrlParameters();
                            urlSalesDocPars.add(CrossEntryAction.RP_NEXT_ACTION,            "cedocumentlist");
                            urlSalesDocPars.add(DocumentListSelectorForm.DOCUMENT_TYPE,     DocumentListSelectorForm.ORDER);
                            urlSalesDocPars.add(DocumentListSelectorForm.DOCUMENT_STATUS,   DocumentListSelectorForm.OPEN);
                            urlSalesDocPars.add(DocumentListSelectorForm.VALIDITYSELECTION, DocumentListSelectorForm.PERIOD);                                                        
                            urlSalesDocPars.add(DocumentListSelectorForm.PERIOD,            DocumentListSelectorForm.NOT_SPECIFIED);                                                        
                            anchorAttrSalesItem = urlGen.readPageURL(crmObjectTypeSalesDocsCRM,"","",crmMethodSalesDocsCRM,"",urlSalesDocPars,request);
                        %>
                        <% if (!anchorAttrSalesItem.getURI().equals("")) {  %>
                          <a href="<%=anchorAttrSalesItem.getURI()%>" onclick="doNavigate('<%=anchorAttrSalesItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.ac.part014.link"/>">
                            <isa:translate key="cch.setup.ac.part014.link" arg0="<%= notCompletedOrders %>"/>
                          </a>
                        <% }
                            else { %>
                            <span><isa:translate key="cch.setup.ac.part014.link" arg0="<%= notCompletedOrders %>"/></span>
                            <%}
                        %>
                        &nbsp;
                        <%--br>
                        <a href="#"><isa:translate key="cch.setup.ac.part013.link" arg0="23"/></a>&nbsp;--%>
                    </td>
                </tr>
              </tbody>
              </table>

              </td>


              <td valign="top">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr><td valign="top" align="right" width="15%">
                      <img src="<%=WebUtil.getMimeURL(pageContext, "iviews/setupguide/cch/mimes/images/icon_edit_productline.gif") %>" alt="<isa:translate key="cch.setup.ac.part02.link"/>" border="0" />
                      &nbsp;
                    </td>
                    <td><% //targetUrl = urlGen.readPageURL(crmObjectTypeProduct, urlSalesDocPars, portalURL, navModelVersion);
                        AnchorAttributes anchorAttrProdItem = urlGen.readPageURL(crmObjectTypeProduct,"","","","",urlSalesDocPars,request);%>
                        <% if (!anchorAttrSalesItem.getURI().equals("")) {  %>
                          <a href="<%=anchorAttrProdItem.getURI()%>" onclick="doNavigate('<%=anchorAttrProdItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.ac.part02.link"/>">
                          <b><isa:translate key="cch.setup.ac.part02.link"/></b>
                          </a>
                        <% }
                            else { %>
                          <b><span><isa:translate key="cch.setup.ac.part02.link"/></span></b>
                            <%}
                        %>

                          &nbsp;
                          <br/>
                          <span><isa:translate key="cch.setup.ac.part020"/></span>
                    </td>
                </tr>

                <tr><td><br/></td><td></td>
                </tr>

                <tr><td valign="top" align="right" width="15%">
                      <img src="<%=WebUtil.getMimeURL(pageContext, "iviews/setupguide/cch/mimes/images/icon_edit_prices.gif") %>" alt="<isa:translate key="cch.setup.ac.part03.link"/>" border="0" />
                      &nbsp;
                     </td>
                    <td><% //targetUrl = urlGen.readPageURL(crmObjectTypePrice, urlSalesDocPars, portalURL, navModelVersion);
                           AnchorAttributes anchorAttrPriceItem = urlGen.readPageURL(crmObjectTypePrice,"","",crmMethodPrice,"",urlSalesDocPars,request);%>
                        <% if (!anchorAttrPriceItem.getURI().equals("")) {  %>
                         <a href="<%=anchorAttrPriceItem.getURI()%>" onclick="doNavigate('<%=anchorAttrPriceItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.ac.part03.link"/>">
                          <b><isa:translate key="cch.setup.ac.part03.link"/></b>                          
                        </a>&nbsp;
                        <% }
                            else { %>
                            	<b><span><isa:translate key="cch.setup.ac.part03.link"/><span></b>
                           <%}
                         %>   
                         &nbsp;
                         <br/>
                         <span><isa:translate key="cch.setup.da.part030"/><span>
                    </td>
                </tr>

                <tr><td><br/></td><td></td>
                </tr>

                <%-- Deactivation-request-link removed on request of PM
                tr><td></td>
                    <td><isa:translate key="cch.setup.ac.part04.oh.pre"/>&nbsp;
                        <a href="mailto:webadmin@company.com"><isa:translate key="cch.setup.ac.part04.oh.link"/></a>&nbsp;
                        <isa:translate key="cch.setup.ac.part04.oh.post"/>&nbsp;
                    </td>
                </tr--%>


              </tbody>
              </table>
            </td></tr>
            </table>
        </div>
    </body>
</html>


