<%--
  Name:        portalnavigation.inc
  Description: Includes all portal navigation related objects
--%>

<%@page import="com.sap.isa.portalurlgenerator.util.*" %>

<% // get the portalUrlGenerator object
   BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
   PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();

//   String portalURL = (String)userSessionData.getAttribute(CCHSetupStartAction.SC_PORTALURL);

//   // get the navModelVersion parameter for ISA
//   String navModelVersion = (String) userSessionData.getAttribute(CCHSetupStartAction.SC_NAVMODELVERSION);
//   if ((navModelVersion == null) || (navModelVersion.equals(""))) {
//      navModelVersion = "portal";
//   }

  // Businessobjecttypes point to the portal page they are on
  String crmObjectTypeServiceCenter = "SERVICEPROCESSCRM";
  String crmMethodServiceCenter = "TOICSS";
  
  String crmObjectTypeUser = "EMPLOYEECRM";
  String crmMethodUser = "DEFAULT";

  String crmObjectTypeProduct = "SHAREDCATALOGCRM";
  String crmMethodProduct = "DEFAULT";

  String crmObjectTypePrice = "SHAREDCATALOGCRM";
  String crmMethodPrice = "TOPRICE";

  String crmObjectTypeSalesDocsCRM = "SALESORDERCRM";
  String crmMethodSalesDocsCRM = "TOHOM";

  String crmObjectTypeShop = "PARTNERPROFILECRM";
  String crmMethodShop = "DEFAULT";

  String targetUrl = null;

  UrlParameters urlSalesDocPars = new UrlParameters();
 %>
