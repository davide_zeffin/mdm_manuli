<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.iview.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.portalurlgenerator.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<%@ include file="portalnavigation.inc" %>

<% BaseUI ui = new BaseUI(pageContext);
   SoldFrom soldFrom = (SoldFrom)request.getAttribute(CCHSetupStartAction.RK_CCHSG_SALESPARTNER);
   String cchScenario = "";
   if (soldFrom.getShopCall().equals("1")  ||  soldFrom.getShopCall().equals("2") ) {
       // Unsupported scenario combination would have ended on errorpage!!
       cchScenario = "sc";
   } else {
       cchScenario = "oh";
   }
   String resKey00, resKey01, resKey02, resKey03, resKey04;
 %>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title>SAP Internet Sales CCH Setup Guide</title>
        <%-- isa:includes/ --%>
        <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css")%>"
                type="text/css" rel="stylesheet" />

        <script src="<%=WebUtil.getMimeURL(pageContext, "/iviews/jscript/crm_isa_epcm.js")%>"
                type="text/javascript">
        </script>
        <script src="<%=WebUtil.getMimeURL(pageContext, "/appbase/jscript/portalnavigation.js.jsp")%>"
                type="text/javascript">
        </script>
    </head>
    <body style="width: 97%; height: 95%;">

        <div id="newdoc" style="HEIGHT: 100%; WIDTH: 100;">
            <div class="module-name"><isa:moduleName name="iviews/setupguide/designetedactivated.jsp" /></div>
            <br/>
            <table border="0" cellpadding="2" width="100%" cellspacing="0">
            <tr><td valign="top" width="50%">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr><td valign="top" align="right" width="15%">
                        <img src="<%=WebUtil.getMimeURL(pageContext, "iviews/setupguide/cch/mimes/images/icon_new_user.gif") %>" alt="<isa:translate key="cch.setup.da.part010.link"/>" border="0" />
                        &nbsp;
                    </td>
                    <td><span><isa:translate key="cch.setup.da.part010.prelink"/></span>
                        <% //targetUrl = urlGen.readPageURL(crmObjectTypeUser, urlSalesDocPars, portalURL, navModelVersion);
                           AnchorAttributes anchorAttrUserItem = urlGen.readPageURL(crmObjectTypeUser,"","",crmMethodUser,"",urlSalesDocPars,request);%>
                        <% if (!anchorAttrUserItem.getURI().equals("")) {  %>
                          <a href="<%=anchorAttrUserItem.getURI()%>" onclick="doNavigate('<%=anchorAttrUserItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.da.part010.link"/>">
                            <b><isa:translate key="cch.setup.da.part010.link"/></b>
                          </a>&nbsp;
                        <% }
                            else { %>
                            <b><span><isa:translate key="cch.setup.da.part010.link"/></span></b>
                            <%}
                        %>

                        <span><isa:translate key="cch.setup.da.part010.postlink"/></span>
                        <%--
                        <isa:translate key="cch.setup.da.part011"/>&nbsp;
                        <isa:translate key="cch.setup.da.part012.prelink"/>
                        <% if (!anchorAttrUserItem.getURI().equals("")) {  %>
                          <a href="<%=anchorAttrUserItem.getURI()%>" onClick="doNavigate('<%=anchorAttrUserItem.getJavaScript()%>'); return false;">
                            <b><isa:translate key="cch.setup.da.part012.link"/></b>
                          </a>&nbsp;
                        <% }
                            else { %>
                            <b><isa:translate key="cch.setup.da.part012.link"/></b>
                            <%}
                        %>

                        <isa:translate key="cch.setup.da.part012.postlink"/> 
                        --%>
                        <br/>
                        <span><isa:translate key="cch.setup.da.part02"/></span>
                    </td>
                </tr>

                <tr>
                  <td><br/></td><td></td>
                </tr>

                <tr><td valign="top" align="right" width="15%">
                        <img src="<%=WebUtil.getMimeURL(pageContext, "iviews/setupguide/cch/mimes/images/icon_edit_productline.gif") %>" alt="<isa:translate key="cch.setup.da.part03.link"/>" border="0" />
                        &nbsp;
                    </td>
                    <td><% //targetUrl = urlGen.readPageURL(crmObjectTypeProduct, urlSalesDocPars, portalURL, navModelVersion);
                          AnchorAttributes anchorAttrProductItem = urlGen.readPageURL(crmObjectTypeProduct,"","",crmMethodProduct,"",urlSalesDocPars,request);%>
                        <% if (!anchorAttrProductItem.getURI().equals("")) {  %>
                          <a href="<%=anchorAttrProductItem.getURI()%>" onclick="doNavigate('<%=anchorAttrProductItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.da.part03.link"/>">
                            <b><isa:translate key="cch.setup.da.part03.link"/></b>
                          </a>&nbsp;
                        <% }
                            else { %>
                            <b><span><isa:translate key="cch.setup.da.part03.link"/></span></b>
                            <%}
                        %>

                        <br/>
                        <span>
                        <% resKey00 = "cch.setup.da.part030.".concat(cchScenario);
                           resKey01 = "cch.setup.da.part031.".concat(cchScenario);
                           resKey02 = "cch.setup.da.part032.".concat(cchScenario);
                           resKey03 = "cch.setup.da.part033.".concat(cchScenario);
                         %>
                        <isa:translate key="<%= resKey00 %>"/>&nbsp;
                        <isa:translate key="<%= resKey01 %>"/>&nbsp;
                        <isa:translate key="<%= resKey02 %>"/>&nbsp;
                        <isa:translate key="<%= resKey03 %>"/>&nbsp;
                        </span>
                    </td>
                </tr>

                <tr>
                  <td><br/></td><td></td>
                </tr>
              </tbody>
              </table>

              </td>

              
              <td valign="top">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr><td valign="top" align="right" width="15%">
                        <img src="<%=WebUtil.getMimeURL(pageContext, "iviews/setupguide/cch/mimes/images/icon_edit_prices.gif") %>" alt="<isa:translate key="cch.setup.da.part04.link"/>" border="0" />
                        &nbsp;
                    </td>
                    <td><% //targetUrl = urlGen.readPageURL(crmObjectTypePrice, urlSalesDocPars, portalURL, navModelVersion);
                           AnchorAttributes anchorAttrPriceItem = urlGen.readPageURL(crmObjectTypePrice,"","",crmMethodPrice,"",urlSalesDocPars,request);%>
                        <% if (!anchorAttrPriceItem.getURI().equals("")) {  %>
                         <a href="<%=anchorAttrPriceItem.getURI()%>" onclick="doNavigate('<%=anchorAttrPriceItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.da.part04.link"/>">
                          <b><isa:translate key="cch.setup.da.part04.link"/></b>
                        </a>&nbsp;
                        <% }
                            else { %>
                            <b><span><isa:translate key="cch.setup.da.part04.link"/></span></b>
                            <%}
                         %>   
                        <br/>
                        <span>                        
                        <% resKey00 = "cch.setup.da.part040.".concat(cchScenario);
                           resKey01 = "cch.setup.da.part041.".concat(cchScenario);
                         %>
                        <isa:translate key="<%= resKey00 %>"/>&nbsp;
                        <isa:translate key="<%= resKey01 %>"/>&nbsp;
                        </span>
                    </td>
                </tr>

                <tr><td><br/></td><td></td>
                </tr>

                <tr><td valign="top" align="right" width="15%">
                        <img src="<%=WebUtil.getMimeURL(pageContext, "iviews/setupguide/cch/mimes/images/icon_webshop_custom.gif") %>" alt="<isa:translate key="cch.setup.da.part05.link"/>" border="0" />
                        &nbsp;
                    </td>
                    <td><% //targetUrl = urlGen.readPageURL(crmObjectTypeShop, urlSalesDocPars, portalURL, navModelVersion);
                           AnchorAttributes anchorAttrShopItem = urlGen.readPageURL(crmObjectTypeShop,"","",crmMethodShop,"",urlSalesDocPars,request);%>
                        <% if (!anchorAttrShopItem.getURI().equals("")) {  %>
                          <a href="<%=anchorAttrShopItem.getURI()%>" onclick="doNavigate('<%=anchorAttrShopItem.getJavaScript()%>'); return false;" title="<isa:translate key="cch.setup.da.part05.link"/>">
                            <b><isa:translate key="cch.setup.da.part05.link"/></b>
                          </a>&nbsp;
                        <% }
                            else { %>
                            <b><span><isa:translate key="cch.setup.da.part05.link"/></span></b>
                            <%}
                        %>

                        <br/>
                        <span>
                        <% resKey00 = "cch.setup.da.part050.".concat(cchScenario);
                           resKey01 = "cch.setup.da.part051.".concat(cchScenario);
                         %>
                        <isa:translate key="<%= resKey00 %>"/>&nbsp;
                        <isa:translate key="<%= resKey01 %>"/>&nbsp;
                        </span>                        
                    </td>
                </tr>

                <tr><td><br/><br/></td><td></td>
                </tr>

                <% if (soldFrom.getShopCall().equals("1")   ||
                       soldFrom.getOrderHosting().equals("1") ) { %>
                <tr><td></td>
                    <td><% resKey00 = "cch.setup.da.part060.".concat(cchScenario);
                           resKey01 = "cch.setup.da.part061.".concat(cchScenario);
                           resKey02 = "cch.setup.da.part062.".concat(cchScenario);
                           resKey03 = "cch.setup.da.part063.".concat(cchScenario);
                           resKey04 = "cch.setup.da.part064.".concat(cchScenario);
                         %>
                        <span>
                        <isa:translate key="<%= resKey00 %>"/>&nbsp;
                        <isa:translate key="<%= resKey01 %>"/>&nbsp;
                        <isa:translate key="<%= resKey02 %>"/>&nbsp;
                        <isa:translate key="<%= resKey03 %>"/>&nbsp;
                        <isa:translate key="<%= resKey04 %>"/>&nbsp;
                        <span>
                    </td>
                </tr>
                <% } else {
                     // Sales partner is active
                 %>
                <!-- Deactivation-request-link removed on request of PM
                  tr><td></td>
                    <td><isa:translate key="cch.setup.da.part07.sc.pre"/>&nbsp;
                        <a href="mailto:webadmin@company.com"><isa:translate key="cch.setup.da.part07.sc.link"/></a>&nbsp;
                        <isa:translate key="cch.setup.da.part07.sc.post"/>&nbsp;
                    </td>
                </tr-->
                <% } %>
    
                <% if (soldFrom.getShopCall().equals("1") ||
                       soldFrom.getOrderHosting().equals("1") ) {%>
                <tr><td><br/><br/></td><td></td>
                </tr>
<%
    String relBtnTxt = WebUtil.translate(pageContext, "cch.setup.da.but.rel", null);
%>    
                <tr><td></td>
                    <td><form name="salespartnerstatuschange" method="get" action='<isa:webappsURL name ="iview/setupguide/cch/salespartnerstatuschange.do"/>'>
                    <input  class="green" type="submit" name="release_partner" value="<isa:translate key="cch.setup.da.but.rel"/>"
		                    title="<isa:translate key="access.button" arg0="<%=relBtnTxt%>" arg1=""/>">
                    </form>
                    </td>
                <% } %>

                <% if (request.getAttribute(CCHSalesPartnerStatusChangeAction.RK_CCHSC_ERROROCCURED) != null) {
                       String msgFinal = "cch.setup.da.part90.".concat((String)request.getAttribute(CCHSalesPartnerStatusChangeAction.RK_CCHSC_ERROROCCURED));
                %>
                <tr><td><br/><br/></td><td></td>
                </tr>

                <tr><td></td>
                    <td><span style="color:#f00;"><isa:translate key="<%=msgFinal%>"/></span>
                    </td>
                </tr>
                <% } %>
    

              </tbody>
              </table>
            </td></tr>
            </table>
        </div>
    </body>
</html>


