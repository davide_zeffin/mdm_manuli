<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.iview.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.portalurlgenerator.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<%@ include file="portalnavigation.inc" %>

<isa:contentType />

<% BaseUI ui = new BaseUI(pageContext);
   String errorcode = (String)request.getAttribute(CCHSetupStartAction.RK_CCHSG_ERRORCODE);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title>SAP Internet Sales CCH Setup Guide</title>
        <%-- isa:includes/ --%>
        <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css")%>"
                type="text/css" rel="stylesheet" />

        <script src="<%=WebUtil.getMimeURL(pageContext, "/iviews/jscript/crm_isa_epcm.js")%>"
                type="text/javascript">
        </script>
        <script src="<%=WebUtil.getMimeURL(pageContext, "/appbase/jscript/portalnavigation.js.jsp")%>"
                type="text/javascript">
        </script>
    </head>
    <body class="hints">

        <div id="newdoc" class="module">
            <div class="module-name"><isa:moduleName name="iviews/setupguide/errorpage.jsp" /></div>
            <br/>
            <table><!-- Outer table used for left spacing -->
            <tr><td width="20"></td><td>
              <table border="0" cellpadding="0" cellspacing="0" width="600">
              <tbody>
                <% if ("2000".equals(errorcode)) {%>                
                <tr><span><isa:translate key="cch.setup.errorp.part01"/></span>
                </tr>

                <tr><br/>
                </tr>
                <% } %>
                <tr><% //targetUrl = urlGen.readPageURL(crmObjectTypeServiceCenter, urlSalesDocPars, portalURL, navModelVersion);
                       AnchorAttributes anchorAttrServiceItem = urlGen.readPageURL(crmObjectTypeServiceCenter,"","","","",urlSalesDocPars,request);%>
                      <% if (!"".equals(anchorAttrServiceItem.getURI())) {  %>
                        <a href="<%=anchorAttrServiceItem.getURI()%>" onclick="doNavigate('<%=anchorAttrServiceItem.getJavaScript()%>'); return false;" title="<%=WebUtil.translate(pageContext, "cch.setup.errorp.part02.link", null)%>">
                          <b><%=WebUtil.translate(pageContext, "cch.setup.errorp.part02.link", null)%></b>
                        </a>
                      <% } else { %>
                            <b><span><%=WebUtil.translate(pageContext, "cch.setup.errorp.part02.link", null)%></span></b>
                      <% } %>
                </tr>

              </tbody>
              </table>
            </td></tr>
            </table>
        </div>
    </body>
</html>