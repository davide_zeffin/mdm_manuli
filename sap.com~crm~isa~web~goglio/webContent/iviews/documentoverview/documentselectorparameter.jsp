<%--
********************************************************************************
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
********************************************************************************
--%>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.iview.*" %>
<%@ page import="com.sap.isa.isacore.actionform.iview.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="java.sql.ResultSetMetaData" %>

<%@ page import="java.util.*" %>

<!-- imports will be used for portal page navigation -->
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>
<%@ page import="com.sap.isa.portalurlgenerator.businessobject.PortalUrlGenerator"%>
<%@ page import="com.sap.isa.portalurlgenerator.util.*"%>

<%@ include file="/b2b/messages-declare.inc.jsp" %>
<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>
<% BaseUI ui = new BaseUI(pageContext);%>
<% String accessText = ""; %>
<%
   String serviceDocType="";
   String lastDocType = (String)userSessionData.getAttribute(ReadiViewParameterAction.SC_DOCTYPE);
   if (lastDocType == null) lastDocType = "";
   String currentDocType = lastDocType;
   String docTypeArea;
   if (lastDocType.equals(DocumentListiViewParameterForm.QUOTATION)      ||
       lastDocType.equals(DocumentListiViewParameterForm.ORDER)          ||
       lastDocType.equals(DocumentListiViewParameterForm.ORDERTEMPLATE)) {
         docTypeArea = "sales";
   } else if (lastDocType.equals(DocumentListiViewParameterForm.INVOICE) ||
          lastDocType.equals(DocumentListiViewParameterForm.CREDITMEMO)  ||
         lastDocType.equals(DocumentListiViewParameterForm.DOWNPAYMENT)) {
         docTypeArea = "bill";
   } else {
         docTypeArea = "service";
   }

   Shop shop = (Shop)request.getAttribute(DocumentListAction.RK_DLA_SHOP);

   ResultData List = (ResultData)request.getAttribute(DocumentOverviewListParameterAction.RK_DOCLIST);
   ResultSetMetaData ListLayout = null;
   if (List != null) {
       ListLayout = List.getMetaData();
   }

   //String selectionPeriod = (String)userSessionData.getAttribute(DocumentOverviewListAction.SC_SELECTPERIOD);
   String documentsOrigin = (String)request.getAttribute(DocumentOverviewListAction.RC_DOCSORIGIN);

   String orderID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_ORDER_ID);
   String quotationID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_QUOTATION_ID);
   String orderTemplateID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_ORDERTEMPLATE_ID);
   String invoiceID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_INVOICE_ID);
   String creditMemoID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CREDITMEMO_ID);
   String downPaymentID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_DOWNPAYMENT_ID);
   String serviceRequestID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_SERVICEREQUEST_ID);
   String infoRequestID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_INFOREQUEST_ID);
   String complaintsID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_COMPLAINTS_ID);
   String serviceContractID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_SERVICECONTRACT_ID);

   String crmObjectTypeOrder = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_ORDER);
   String crmObjectTypeQuotation = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_QUOTATION);
   String crmObjectTypeOrderTemplate = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_ORDERTEMPLATE);
   String crmObjectTypeInvoice = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_INVOICE);
   String crmObjectTypeCreditMemo = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_CREDITMEMO);
   String crmObjectTypeDownPayment = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_DOWNPAYMENT);
   String crmObjectTypeServiceRequest = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_SERVICEREQUEST);
   String crmObjectTypeInfoRequest = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_INFOREQUEST);
   String crmObjectTypeComplaints = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_COMPLAINTS);
   String crmObjectTypeServiceContract = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CRMOBJECTTYPE_SERVICECONTRACT);


   int docCount = 0;
   try {
       docCount = Integer.parseInt((String)request.getAttribute(DocumentListAction.RK_DOCUMENT_COUNT));
   } catch (NumberFormatException NFEx) {
   }

    // get the portalUrlGenerator object
    BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();

    UrlParameters urlSalesDocPars = new UrlParameters();
    UrlParameters urlBillingDocPars = new UrlParameters();
    UrlParameters urlServiceDocPars = new UrlParameters();

   //String[][] serviceA = (String[][])request.getAttribute(ServiceTypesReadAction.RK_SERVICETYPES);
   //if (serviceA == null) {
       // Set SAP standard values as default
//       String[][] serviceA = new String[][] {{"CRMC", "BUS2000120"},
//                                  {"S1",   "BUS2000116"},
//                                  {"S3",   "BUS2000116"},
//                                  {"SC",   "BUS2000112"} } ;
       String[][] serviceA = new String[][] {{"CRMC", "complaint"},
                                  {"S1",   "service"},
                                  {"S3",   "service"},
                                  {"SC",   "contract"} } ;

   //}

   boolean backendR3ButNotPI = shop.getBackend().equals(Shop.BACKEND_R3) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_40) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_45) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_46);

   boolean backendR3 = backendR3ButNotPI ||
                       shop.getBackend().equals(Shop.BACKEND_R3_PI);
%>
<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css" ) %>" type="text/css" rel="stylesheet">

  <script src="../iviews/jscript/crm_isa_epcm.js" type="text/javascript"></script>
  <script src="<%=WebUtil.getMimeURL(pageContext, "/appbase/jscript/portalnavigation.js.jsp")%>"
                type="text/javascript">
  </script>

 <script type="text/javascript">
    // If a service document has been selected, it is necessary
    // to set the process type too
    function SetServiceBusinessObjectType(serviceProcessType) {
         <%=currentDocType%>=serviceProcessType;
         var jserviceAsp = new Array(<%=serviceA.length%>);
         var jserviceAbp = new Array(<%=serviceA.length%>);
        <% for (int i = 0; i < serviceA.length; i++) { %>
        jserviceAsp[<%=i%>]="<%=serviceA[i][0]%>";
        jserviceAbp[<%=i%>]="<%=serviceA[i][1]%>";
        <% } %>
        //var hiddenBusinessType = document.forms['<%=DocumentListiViewParameterForm.IVIEW_DOCUMENT_TYPE%>'].elements['<%=DocumentOverviewListParameterAction.RC_SERVICEBUSINESSTYPE%>'];
        var serviceDocType = "";
        for (var i = 0; i < jserviceAsp.length; i++) {
            if (jserviceAsp[i] == serviceProcessType) {
                //hiddenBusinessType.value = jserviceAbp[i];
                serviceDocType= jserviceAbp[i];
            }
        }
        document.all["MyServiceId"].value=serviceDocType;
    }
  </script>
</head>

<body>

    <isa:moduleName name="b2b/billing/organizer-content-doc-search.jsp" />

      <%-- Selection Screen Area --%>
	  <a href="#" title="<isa:translate key="iview.docslctpar.jsp.pagebgn"/>" accesskey="<isa:translate key="iview.docslctpar.jsp.pagekey"/>"></a>
      <form name="<%=DocumentListiViewParameterForm.IVIEW_DOCUMENT_TYPE%>" method="get" action='<isa:webappsURL name ="iview/documentlistparameter.do"/>' >
        <!--fieldset>
          <legend>Suchkriterien</legend-->
          <table border="0" cellspacing="0" cellpadding="1" width="10%">
            <tbody>
              <tr>
                <td><label id="<%=DocumentListiViewParameterForm.DOCUMENT_TYPE%>Lbl" for="<%=DocumentListiViewParameterForm.DOCUMENT_TYPE%>" title="<isa:translate key="b2b.status.iview.showmeall"/>"><isa:translate key="b2b.status.iview.showmeall"/></label></td>
              </tr>
              <tr>
                <td>
                     <select class="selectDocument" name="<%=DocumentListiViewParameterForm.IVIEW_DOCUMENT_TYPE%>" id="<%=DocumentListiViewParameterForm.IVIEW_DOCUMENT_TYPE%>"
                       onChange="SetServiceBusinessObjectType(this.form.<%= DocumentListiViewParameterForm.IVIEW_DOCUMENT_TYPE %>.options[this.form.<%= DocumentListiViewParameterForm.IVIEW_DOCUMENT_TYPE %>.options.selectedIndex].value);">
                      <option value="<%=DocumentListiViewParameterForm.QUOTATION%>" <%=(lastDocType.equals(DocumentListiViewParameterForm.QUOTATION)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=quotationID%>"/></option>
                      <option value="<%=DocumentListiViewParameterForm.ORDER%>" <%=(lastDocType.equals(DocumentListiViewParameterForm.ORDER)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=orderID%>" /></option>
                      <% if (!backendR3) { %>
                      <option value="<%=DocumentListiViewParameterForm.ORDERTEMPLATE%>" <%=(lastDocType.equals(DocumentListiViewParameterForm.ORDERTEMPLATE)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=orderTemplateID%>" /></option>
                      <% } %>
                      <% if (shop.isContractAllowed()) {%>
                      <%       //docTypeClear = WebUtil.translate(pageContext, "b2b.status.shuffler.key1val4", null); %>
                      <%-- option value="<%=DocumentListSelectorForm.CONTRACT%>"<%=(lastDocType.equals(DocumentListSelectorForm.CONTRACT)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=docTypeClear%>" /></option --%>
                      <% } %>
                      <% if (shop.isBillingDocsEnabled()) {%>
                      <option value="<%=DocumentListiViewParameterForm.INVOICE%>"<%=(lastDocType.equals(DocumentListiViewParameterForm.INVOICE)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=invoiceID%>" /></option>
                      <!--
                      <option value="<%=DocumentListiViewParameterForm.CREDITMEMO%>"<%=(lastDocType.equals(DocumentListiViewParameterForm.CREDITMEMO)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=creditMemoID%>" /></option>
                      <option value="<%=DocumentListiViewParameterForm.DOWNPAYMENT%>"<%=(lastDocType.equals(DocumentListiViewParameterForm.DOWNPAYMENT)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=downPaymentID%>" /></option>
                      -->
                      <% } %>
                      <option value="<%=DocumentListiViewParameterForm.SERVICEREQUEST%>"<%=(lastDocType.equals(DocumentListiViewParameterForm.SERVICEREQUEST)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=serviceRequestID%>" /></option>
                      <option value="<%=DocumentListiViewParameterForm.INFOREQUEST%>"<%=(lastDocType.equals(DocumentListiViewParameterForm.INFOREQUEST)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=infoRequestID%>" /></option>
                      <option value="<%=DocumentListiViewParameterForm.COMPLAINTS%>"<%=(lastDocType.equals(DocumentListiViewParameterForm.COMPLAINTS)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=complaintsID%>" /></option>
                      <option value="<%=DocumentListiViewParameterForm.SERVICECONTRACT%>"<%=(lastDocType.equals(DocumentListiViewParameterForm.SERVICECONTRACT)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=serviceContractID%>" /></option>
                    </select>
		     <a 
		        href="#" title= "<isa:translate key="iview.docslctpar.btnarea"/>" accesskey="<isa:translate key="iview.docslctpar.btnkey"/>"
		     ></a>            
                    
                </td>                
              </tr>
<%
    String searchBtnTxt = WebUtil.translate(pageContext, "status.select.searching", null);
%>              
              <tr>
                <td align="left">
                	<input class="button" type="submit" value="<isa:translate key="status.select.searching"/>"
						   title="<isa:translate key="access.button" arg0="<%=searchBtnTxt%>" arg1=""/>"                	
					>
                </td>
              </tr>
            </tbody>
          </table>

          <input type="hidden" name="<%=DocumentOverviewListParameterAction.RC_SERVICEBUSINESSTYPE%>" value=""/>
          <input type="hidden" id="MyServiceId" name="MyServiceId" value=""/>

      </form>

      <%-- List Screen Area --%>

      <% String documentTypeKey = "status.".concat(docTypeArea).concat(".dt.").concat(lastDocType);
         if ( docCount == 0  ||  docCount > 1) {
            documentTypeKey = documentTypeKey.concat("s");          // plural
         };
      %>

      <hr size="1" width="100%">

      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr><td colspan="3"><div class="searchresult"><span>
          <%=String.valueOf(docCount)%>&nbsp;<isa:translate key="<%= documentTypeKey %>"/> <isa:translate key="status.select.searchresult02"/>
          </span></div></td>
        </tr>
      </table>

      <br>

      <% if (docCount > 0) { %>
	<%
	// Accessibility requirement
	int table1_col_nro = 0;
	// SALES
	if (docTypeArea.equals("sales")) 
	{
		table1_col_nro = 2;
		if ( ! lastDocType.equals(DocumentListiViewParameterForm.ORDERTEMPLATE) ) 
		{
		   table1_col_nro++;
	  	}
	}
	// BILLING
	if (docTypeArea.equals("bill")) 
	{
		table1_col_nro = 2;
	}

	// SERVICE
	if (docTypeArea.equals("service")) 
	{
		table1_col_nro = 3;
	}	
	String table1_col_cnt = Integer.toString(table1_col_nro);
	String table1_row_cnt = Integer.toString(docCount);
	String table1_title = WebUtil.translate(pageContext, "iview.docslctpar.table1", null);
	%>
	<%
		if (ui.isAccessible())
		{
	%>
		<a 
			id="beginTable1"
			href="#endTable1"
			title="<isa:translate key="access.table.size" arg0="<%=table1_col_cnt%>" arg1="<%=table1_row_cnt%>" arg2=""/>"
		></a>	
	<%
		}
	%>
      <table width="100%" class="list" align="center" cellpadding="1" cellspacing="1" border="0" summary="<%=table1_title%>">
        <thead>
          <tr>
          <% String colNameFinal = ""; %>
          <%--      SALES     --%>
          <% if (docTypeArea.equals("sales")) {
                 if ( ! lastDocType.equals(DocumentListiViewParameterForm.ORDERTEMPLATE) ) {
           %>
            <th scope="col" class="head"><div class="bold"><isa:translate key="b2b.iview.resultlist.STATUS"/></div></th>
            <%   }
                 colNameFinal = "b2b.status.dn.".concat(lastDocType);%>
            <th scope="col" class="head"><div class="bold"><isa:translate key="<%=colNameFinal%>"/></div>
                <div class="bold"><isa:translate key="b2b.status.iview.result.DATE"/></div>
            </th>
            <th scope="col" class="head"><div class="bold"><isa:translate key="b2b.iview.DESCRIPTION"/></div></th>
          <%} %>

          <%--      BILLING     --%>
          <% if (docTypeArea.equals("bill")) {
                 colNameFinal = "b2b.status.dn.".concat(lastDocType);%>
            <th scope="col" class="head"><div class="bold"><isa:translate key="<%=colNameFinal%>"/></div>&nbsp;</th>
            <%--td bgcolor="#ffffff"><div class="bold"><isa:translate key="status.billing.reference.2lines"/></div></td--%>
            <th scope="col" class="head"><div class="bold"><isa:translate key="b2b.status.iview.GROSSVALUE"/></div></th>
          <%} %>

          <%--      SERVICE     --%>
          <% if (docTypeArea.equals("service")) {
          %>
            <th scope="col"class="head"><div class="bold"><isa:translate key="b2b.iview.resultlist.STATUS"/></div></th>
               <%colNameFinal = "status.service.dt.".concat(lastDocType);%>
            <th scope="col" class="head"><div class="bold"><isa:translate key="<%=colNameFinal%>"/></div>
                <div class="bold"><isa:translate key="b2b.status.iview.result.DATE"/></div>
            </th>
            <th scope="col" class="head"><div class="bold"><isa:translate key="b2b.iview.DESCRIPTION"/></div></th>
          <%} %>

          </tr>

        </thead>
      <% String even_odd = "odd"; %>

      <isa:iterate id="list" name="<%= DocumentOverviewListParameterAction.RK_DOCLIST %>"
                     type="com.sap.isa.core.util.table.ResultData">
          <% if (even_odd.equals("odd")) {
                  even_odd = "even";
                } else {
                  even_odd = "odd";
                }
          %>
          <% String finalColValue = ""; %>
        <tbody>
          <tr>

          <%--      SALES     --%>
          <% if (docTypeArea.equals("sales")) {
                 if ( ! lastDocType.equals(DocumentListiViewParameterForm.ORDERTEMPLATE) ) {%>
            <% finalColValue = "status.sales.status.".concat(list.getString("STATUS")); %>
            <td class="<%= even_odd %>"><isa:translate key="<%=finalColValue%>"/></td>
            <%   }
             urlSalesDocPars.add("doc_key",list.getRowKey().toString());
             urlSalesDocPars.add("doc_type",currentDocType);
             urlSalesDocPars.add("doc_id",list.getString("DOCID"));
             urlSalesDocPars.add("objects_origin",documentsOrigin);
             urlSalesDocPars.add("nextaction","cedocumentdetails");
             AnchorAttributes anchorAttrSales;

             if (currentDocType.equals(DocumentListiViewParameterForm.QUOTATION)) {
              anchorAttrSales = urlGen.readPageURL(crmObjectTypeQuotation,"",list.getRowKey().toString(),"TOB2B","",urlSalesDocPars,request);
             } else if (currentDocType.equals(DocumentListiViewParameterForm.ORDER)) {
                anchorAttrSales = urlGen.readPageURL(crmObjectTypeOrder,"",list.getRowKey().toString(),"TOB2B","",urlSalesDocPars,request);
               } else {
                  anchorAttrSales = urlGen.readPageURL(crmObjectTypeOrderTemplate,"",list.getRowKey().toString(),"TOB2B","",urlSalesDocPars,request);
                 }
            %>

            <td class="<%= even_odd %>">
              <% if (!anchorAttrSales.getURI().equals("")) {  %>
                <a href="<%=anchorAttrSales.getURI()%>" onClick="doNavigate('<%=anchorAttrSales.getJavaScript()%>'); return false;"><%=list.getString("DOCID")%></a>
              <% }
                  else { %>
                  <%=list.getString("DOCID")%>
                  <%}
              %>
              <br>
              <%=list.getString("DATE")%>
            </td>
            <td class="<%= even_odd %>"><%=list.getString("DESCRIPTION")%></td>
          <%} %>

          <%--      BILLING     --%>
          <% if (docTypeArea.equals("bill")) {
             urlBillingDocPars.add("invKey",list.getRowKey().toString());
             urlBillingDocPars.add("invType","VB");        // Allways VB if document is from R/3 SD (BE = CRM Billing Engine) (See int.msg 450499 2004)
             urlBillingDocPars.add("company.code", list.getString("COMPANYCODE"));
             //urlBillingDocPars.add("doc_id",list.getString("DOCID"));
             //urlBillingDocPars.add("doc_origin",documentsOrigin);
             //urlGen.setTargetPageUrl(portalURLBilling);
             //urlGen.setUrlParameters(urlBillingDocPars);

             AnchorAttributes anchorAttrBilling;

             if (currentDocType.equals(DocumentListiViewParameterForm.INVOICE)) {
                anchorAttrBilling = urlGen.readPageURL(crmObjectTypeInvoice,"",list.getRowKey().toString(),"TOFSCM","",urlBillingDocPars,request);
             } else if (currentDocType.equals(DocumentListiViewParameterForm.CREDITMEMO)) {
                anchorAttrBilling = urlGen.readPageURL(crmObjectTypeCreditMemo,"",list.getRowKey().toString(),"TOB2B","",urlBillingDocPars,request);
              } else {
              	anchorAttrBilling = urlGen.readPageURL(crmObjectTypeDownPayment,"",list.getRowKey().toString(),"TOB2B","",urlBillingDocPars,request);
              }

            %>

            <td class="<%= even_odd %>">
                <% if (!anchorAttrBilling.getURI().equals("")) {  %>
                  <a href="<%=anchorAttrBilling.getURI()%>" onClick="doNavigate('<%=anchorAttrBilling.getJavaScript()%>'); return false;"><%=list.getString("DOCID")%>
                  </a>
                <% }
                    else { %>
                    <%=list.getString("DOCID")%>
                    <%}
                %>

                <br>
                <div class="bold"><isa:translate key="status.billing.list.from"/>:&nbsp;</div><%=list.getString("DATE")%> </br>
                <div class="bold"><isa:translate key="b2b.status.iview.result.DUEDATE"/>:&nbsp;</div><%=list.getString("DUEDATE")%>
            </td>
            <%--td><div class="bold"><%=list.getString("DESCRIPTION")%></div></font></td--%>
            <td class="<%= even_odd %>" align="right"><div class="bold"><%=list.getString("GROSSVALUE")%>&nbsp;<%=list.getString("CURRENCY")%></div></td>
          <%} %>

          <%--      SERVICE     --%>
          <% if (docTypeArea.equals("service")) {
             urlServiceDocPars.add("guid",list.getRowKey().toString());
             urlServiceDocPars.add("requesttype", request.getParameter("MyServiceId") );
             urlServiceDocPars.add("detailsonly","no");
             urlServiceDocPars.add("action","DispatchDetailFromOverview");
             AnchorAttributes anchorAttrService;

             if (currentDocType.equals(DocumentListiViewParameterForm.SERVICECONTRACT)) {
              anchorAttrService = urlGen.readPageURL(crmObjectTypeServiceContract,"",list.getRowKey().toString(),"TOICSS","",urlServiceDocPars,request);
             } else if (currentDocType.equals(DocumentListiViewParameterForm.COMPLAINTS)) {
                anchorAttrService = urlGen.readPageURL(crmObjectTypeComplaints,"",list.getRowKey().toString(),"TOICSS","",urlServiceDocPars,request);
               } else if (currentDocType.equals(DocumentListiViewParameterForm.INFOREQUEST)) {
                  anchorAttrService = urlGen.readPageURL(crmObjectTypeInfoRequest,"",list.getRowKey().toString(),"TOICSS","",urlServiceDocPars,request);
                 } else {
                  anchorAttrService = urlGen.readPageURL(crmObjectTypeServiceRequest,"",list.getRowKey().toString(),"TOICSS","",urlServiceDocPars,request);
                 }
          %>

            <td class="<%= even_odd %>"><%=list.getString("STATUS").toLowerCase()%></td>
            <td class="<%= even_odd %>">
            <% if (!anchorAttrService.getURI().equals("")) {  %>
              <a href="<%=anchorAttrService.getURI()%>" onClick="doNavigate('<%=anchorAttrService.getJavaScript()%>'); return false;"><%=list.getString("DOCID")%>
              </a>
            <% }
                else { %>
                <%=list.getString("DOCID")%>
                <%}
            %>

            <br>
                <%=list.getString("DATE")%>
            </td>
            <td class="<%= even_odd %>"><div class="bold"><%=list.getString("DESCRIPTION")%></div></font></td>
          <%} %>

          </tr>
        </tbody>
      </isa:iterate>
      </table>
	<%
		if (ui.isAccessible())
		{
	%>
		<a 
			id="endTable1"
			href="#beginTable1"
			title="<isa:translate key="access.table.exit"/>"
		></a>	
	<%
		}
	%>
      <% } // end of docCount > 0
      %>
</body>
</html>
