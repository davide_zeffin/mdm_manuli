<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.isacore.action.marketing.ShowRecommendationAction" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.RecommendationUI" %>

<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<!-- imports will be used for portal page navigation -->
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>
<%@ page import="com.sap.isa.portalurlgenerator.businessobject.PortalUrlGenerator"%>
<%@ page import="com.sap.isa.portalurlgenerator.util.*"%>

<!-- <isa:contentType/> page is included -->
<%
    // get the number of recommendations, which will be displayed in the appropriate iView
    String strRecCount = "";
    strRecCount = (String) request.getParameter("recommendationscount");
    int intRecCount; //
    if ((strRecCount == null) || (strRecCount.equals("")) || (strRecCount.equals("-1")) ) {
      intRecCount = Integer.MAX_VALUE;
    }
    else {
      Integer objRecCount = new Integer(strRecCount);
      intRecCount = objRecCount.intValue();
    }

    String linkMethod = "default";
    linkMethod = (String) request.getParameter("linkMethod");

    // get the portalUrlGenerator object
    UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
    BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();
    //urlGen.setNavISAModelVersion(navModelVersion);

    UrlParameters urlCatItemPars = new UrlParameters();
    urlCatItemPars.add("nextaction","ceshowcatalogitem");

    UrlParameters urlCatPars = new UrlParameters();
    urlCatPars.add("nextaction","ceshowcatalog");
    ProductListUI ui = new RecommendationUI(pageContext);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%= ui.getLanguage() %>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="recommendation.jsp.title"/></title>
<%--    <isa:stylesheets/>  --%>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css" ) %>" type="text/css" rel="stylesheet">

  <script src="../iviews/jscript/crm_isa_epcm.js" type="text/javascript"></script>

  <script type="text/javascript">
      function doNavigate(urlFinal) {
        return EPCMPROXY_EX.doNavigate(urlFinal);
      }

  </script>
</head>
<body>

  <script type="text/javascript">
      liBehindFirstDot = location.hostname.indexOf( "." ) + 1;
      if (liBehindFirstDot > 0) {
        document.domain = location.hostname.substr( liBehindFirstDot );
      }
  </script>

<isa:moduleName name="iview/specialoffers/iviewRecommendation.jsp" /></div>

  <%
  if (ui.getProductList().size() > 0) {
  %>
    <div class="bold"> <isa:translate key="specialoffers.jsp.header"/> </div>

    <br>

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tbody>

          <% int intProductCount = 0; %>
          <isa:iterate id="product" name="<%= ProductListUI.PC_PRODUCT_LIST %>"
                       type="com.sap.isa.businessobject.Product">

          <% if (intProductCount < intRecCount) {
               if ((intProductCount%3)==0) {%>
                  <tr>
            <% }
            intProductCount++;%>
          <td>
          <table border="0" cellpadding="4" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td>
                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif" ) %>" height="5" width="1" border="0" alt="" class="display-image"><br>
                <% if (product.getThumb().length() > 0) { %>
                <img src="<%=product.getThumb() %>" border="0" alt="<%=product.getPicture() %>">
                <%} else { %>
                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/small.gif" ) %>" border="0" alt="Thumbnail">
                <%}%>
              </td>
              <td width="100%">
                <%
                  urlCatItemPars.add("productId", product.getItemId() );
                  urlCatItemPars.add("productArea", product.getArea() );
                  //urlGen.setUrlParameters(urlCatItemPars);
                  AnchorAttributes anchorAttrItem = urlGen.readPageURL("PRODUCT","",product.getItemId(),linkMethod,"",urlCatItemPars,request);
                %>
                <% if (!anchorAttrItem.getURI().equals("")) {  %>
                  <a href="<%=anchorAttrItem.getURI()%>" onClick="doNavigate('<%=anchorAttrItem.getJavaScript()%>'); return false;"> <%= JspUtil.encodeHtml(product.getId()) %> </a>
                <% }
                    else { %>
                    <span><%= JspUtil.encodeHtml(product.getId()) %></span>
                    <%}
                %>
                <br>
                <% if (!anchorAttrItem.getURI().equals("")) {  %>
                  <a href="<%=anchorAttrItem.getURI()%>" onClick="doNavigate('<%=anchorAttrItem.getJavaScript()%>'); return false;"> <%= JspUtil.encodeHtml(product.getDescription()) %> </a>
                <% }
                    else {%>
                       <span><%= JspUtil.encodeHtml(product.getDescription()) %></span>
                    <%}
                %>

               <%
               if (ui.isListPriceAvailable()) { %>
                 <br>
                 <span><%=product.getListPrice() %></span>
                 <strong>
               <%
               }
               if (ui.isCustomerSpecificPriceAvailable()) { %>
                 <br>
                 <span><%=product.getCustomerSpecificPrice() %></span>
                 <br>
               <%
               }
               if (ui.isListPriceAvailable()) { %>
                 </strong>
               <%
               } %>
              </td>
            </tr>
          </tbody>
        </table>
        </td>
        <% } %>
        </isa:iterate>
        </tr>
       </tbody>
      </table>

      <br>
      <br>

      <%
        //urlGen.setUrlParameters(urlCatPars);
        //AnchorAttributes anchorAttr = urlGen.readPageURL(objType, urlCatPars, pageUrl, navModelVersion);
        AnchorAttributes anchorAttr = urlGen.readPageURL("PRODUCT","","",linkMethod,"",urlCatPars,request);
      %>

      <div class="bold"> <span><isa:translate key="specialoffers.jsp.catalog1"/></span>
      <span><isa:translate key="specialoffers.jsp.catalog2"/></span>
      <% if (!anchorAttr.getURI().equals("")) {  %>
        <a href="<%=anchorAttr.getURI()%>" onClick="doNavigate('<%=anchorAttr.getJavaScript()%>'); return false;" title="<isa:translate key="specialoffers.jsp.catalog3"/>">
        <isa:translate key="specialoffers.jsp.catalog3"/></a>
      <% }
          else {
      %>
          <span><isa:translate key="specialoffers.jsp.catalog3"/><span>
          <%}
      %>
      </div>

  <%}
  else {%>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                        <span>
                    <isa:translate key="recommendation.jsp.notFound"/> <br>
                    <isa:translate key="recommendation.jsp.notFound2"/>
                    </span>
                  </td>
                </tr>
        </tbody>
    </table>

  <%}%>



</body>
</html>