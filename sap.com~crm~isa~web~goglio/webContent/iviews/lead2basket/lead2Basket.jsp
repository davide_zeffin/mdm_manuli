<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.isacore.*" %>

<%-- import the taglibs used on this page --%>
<%@ taglib prefix="isa" uri="/isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<% //get messageDoc Object 

 UserSessionData userSessionData =
            UserSessionData.getUserSessionData(pageContext.getSession());

 DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

 MessageDocument msgDoc = documentHandler.getMessageDocument();

%>




<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
     <title><isa:translate key="bupa.search.details.title"/></title>
     <link rel="stylesheet" type="text/css" href="<isa:webappsURL name="mimes/shared/style/stylesheet.css" />">
     <script type="text/javascript">
		        
	  function send_close() {                                
	       
	                                                           
	       document.forms[0].submit();                       
	           
	                                                          
	   }		
	   
	   function send_createDoc(BupaKey) {                                
	       
	       
	            
	       document.forms[0].createDoc.value="true";                                                                          	       	       
	       document.forms[0].bupaKey.value=BupaKey;                                                                          
	       document.forms[0].submit();                       
	       
	                                                          
	   }		
	
     </script>
  </head>
  
  <body class="ship">
        <% String nextAction = msgDoc.getValue("nextAction"); %>
  	
  	
  	<form action="<isa:webappsURL name="<%= nextAction %>"/>" name="michasaveregistration" method="post" >
  	
 	
 	<table border="0" cellpadding="0" cellspacing="0" width="380">
          <tbody>
            <tr>
              <td class="opener">&nbsp;</td>
              <td class="opener"><span><isa:translate key="b2b.messageDocument.title"/></span>&nbsp;&nbsp;&nbsp;</td>
              <td style="vertical-align:middle;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2blue.gif") %>" width="7" height="17" alt="" border="0"></td>
            </tr>
          </tbody>
        </table>

  	
  	
        
         	<table border=0 cellspacing="0" cellpadding="0" width="100%">    
         		
         		<tr>             		
            			<td class="emphasize">  	      				            				            				
            			        <span><isa:translate key="b2b.messageDocument.text1"/></span>
            			</td>
         		</tr>         		
         		
         		<tr><td>&nbsp;</td></tr>
         		
         		<tr>
         			<td class="emphasize">  	      				
  	      				<span><isa:translate key="b2b.messageDocument.text2"/></span>
            			</td>         		
         		</tr>
         		
         		<tr>
         			<td class="emphasize">
         				<span><isa:translate key="b2b.messageDocument.text3"/></span>
            			</td>         		
         		</tr>
         		
         		
  		</table>
        <!-- Buttons -->
<%
    String yesBtnTxt = WebUtil.translate(pageContext, "b2b.messageDocument.button.yes", null);
	String noBtnTxt = WebUtil.translate(pageContext, "b2b.messageDocument.button.no", null);
%>         	
		<table border=0 cellspacing="0" cellpadding="0" width="100%">    
         		<tr>          
		            	<td colspan="2" align="right">
                                	<input type="Button" value="&nbsp;&nbsp;<isa:translate key="b2b.messageDocument.button.yes"/>&nbsp;&nbsp;"
											title="<isa:translate key="access.button" arg0="<%=yesBtnTxt%>" arg1=""/>"
                         		onclick="send_createDoc('<%=msgDoc.getValue("BupaKey")%>')" class="green">
          			
		            	&nbsp;
		            	
                                	<input type="Button" value="&nbsp;&nbsp;<isa:translate key="b2b.messageDocument.button.no"/>&nbsp;&nbsp;"
											title="<isa:translate key="access.button" arg0="<%=noBtnTxt%>" arg1=""/>"
                         		onclick="send_close()" class="green">                                                                                                     
          			</td>          
          			
          			
	 		</tr> 
        	</table>  
              
              <!-- hidden fields -->
   	      <input type="hidden" name="createDoc"   value="">
   	      <input type="hidden" name="bupaKey"   value="">
   	      
  	</form>
  </body>   
</html>  