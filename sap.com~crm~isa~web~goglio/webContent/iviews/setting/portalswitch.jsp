<%--
    Display the setting for WebShop, Sold-To Party and Web Catalog.

    This JSP should be used in customer portal enviroment, when the setting
    is changed, the complete user session will be reset and the application will 
    be restarted.
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<!-- imports will be used for portal page navigation -->
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.Constants"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.isacore.action.b2b.ReadCatalogAction" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.core.util.table.RowKey" %>
<%@ include file="/b2b/messages-declare.inc.jsp" %>
<%
	BaseUI ui = new BaseUI(pageContext);

%>

<%
	String accessText = "";

%>

<%
	int attributeCount = 0;

%>

<%
	String[] evenOdd = new String[] { "even", "odd" };

%>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>
			<isa:translate 
				key="portal.jsp.SettingTitle"/>
		</title>
		
		<isa:includes/>
		<!-- <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet"> -->
	</head>
	<script type="text/javascript">
        function refreshForm() {
            document.forms["settingform"].submit();
            return true;
	    }
	  </script> 

   <script type="text/javascript">
       function domainRelax() {
        try {
            parent.document.domain;
        } 
        catch(e) {
            var hostname = document.domain; 
            var posBehindFirstDot = hostname.indexOf(".") + 1;
            if (posBehindFirstDot>0) {
                document.domain=hostname.substr(posBehindFirstDot);
            }
        }
       }

	   function doNavigate() {
	     domainRelax();
	     try {
	      this.top.EPCM.doObjBasedNavigate('SAP_CRM_UI', 'CRMLinkNav', '', 'CST-STG-RFR');
	     }
	     catch (ex) {
	       alert (ex.getMessage());
	     }
	   }
   </script>
	<body 
		class="hints">
		<br>
		<br>
		<div 
			id="new-doc" 
			class="module">
		<div 
			class="module-name">
			<isa:moduleName 
				name="iviews/setting/portalswitch.jsp" 
				/>
		</div>
		<div 
			class="opener">
			<isa:translate 
				key="setting.jsp.PortalSwitch"/>
		</div>
			<form 
				method="post" 
				action='<isa:webappsURL name="iview/checkOpenDoc.do"/>' name="settingform">
				<%-- show errors concern the questionary --%>
				<%
					UserSessionData userSessionData =
						UserSessionData.getUserSessionData(
							pageContext.getSession());
					BusinessObjectManager bom =
						(BusinessObjectManager) userSessionData.getBOM(
							BusinessObjectManager.ISACORE_BOM);
					Shop shop = bom.getShop();
					// get all the request parameter
					String shopId1 = (String) request.getAttribute(Constants.PARAM_SHOP);
					String soldTo = (String) request.getAttribute(Constants.PARAM_SOLDTO);
					String tmpCatalogKey = (String) request.getAttribute("catalogKey");
					String tmpCatalog = (String) request.getAttribute("catalog");
					String views[] = (String[]) request.getAttribute("catalogViews[]"); 
					
					tmpCatalog = (tmpCatalogKey == null || tmpCatalogKey.trim().length() == 0) ? tmpCatalog : tmpCatalogKey;
					if (tmpCatalog == null){
					    tmpCatalog = new String();
					}
					
					String catalog = tmpCatalog;
					String display = (String) request.getAttribute("display");
					String refresh = (String) request.getAttribute(Constants.PARAM_PORTAL_REFRESH);
										
					boolean displayOnly = false;
					if (display != null) {
					    if (display.equalsIgnoreCase("yes")) {
					        displayOnly = true;
					    }
					}
					if (refresh != null && refresh.equalsIgnoreCase("yes")) {
					    request.setAttribute(Constants.PARAM_PORTAL_REFRESH, "");
					    displayOnly = true;
                        %>
                        <script  type="text/javascript">
                            doNavigate();
                        </script>
                        <%
					}
					
					if (displayOnly == true) {
				%>

				<BASEFONT 
					FACE="Arial" 
					SIZE="3">
				<table 
					border="0" 
					cellspacing="0" 
					cellpadding="0">
					<tr>
						<td>
							<label 
								for="webshop">
								<isa:translate 
									key="b2b.portal.webshop"/>
							</label>
						</td>
						<td 
							valign="top">
							<input 
								type="text" 
								class="textInput" 
								id="webshop" 
								name="webshop" 
								value="<%= shopId1 %>" 
								disabled=true
								size="30" 
								maxlength="60">
						</td>
					</tr>
					<tr>
						<td>
							<label 
								for="soldto">
								<isa:translate 
									key="b2b.portal.soldto"/>
							</label>
						</td>
						<td 
							valign="top">
							<input 
								type="text" 
								class="textInput" 
								id="soldto" 
								name="soldto" 
								value="<%= soldTo %>" 
								disabled=true
								size="30" 
								maxlength="60">
							<br>
						</td>
					</tr>
					<tr>
						<td>
							<label 
								for="catalog">
								<isa:translate 
									key="b2b.portal.webcatalog"/>
							</label>
						</td>
						<td 
							valign="top">
							<input 
								type="text" 
								class="textInput" 
								id="pcatalog" 
								name="pcatalog" 
								value="<%= (catalog.length() > 30) ? catalog.substring(0, 30).trim() : catalog.trim() %>"
								disabled=true
								size="30" 
								maxlength="60">
							<br>
						</td>
					</tr>
					<tr>
						<td>
							<label 
								for="catalog">
								<isa:translate 
									key="b2b.portal.webcatalogvar"/>
							</label>
						</td>
						<td 
							valign="top">
							<input 
								type="text" 
								class="textInput" 
								id="pcatalogvar" 
								name="pcatalogvar" 
								value="<%= (catalog.length() > 30) ? catalog.substring(30).trim() : ""  %>"
								disabled=true
								size="30" 
								maxlength="60">
							<br>
						</td>
					</tr>
				</table>
				<%
					} else {
				%>

				<%
					ResultData shopList =
						(ResultData) request.getAttribute("shoplist");
					ResultData soldToList =
						(ResultData) request.getAttribute("soldtolist");
					ResultData catalogList =
						(ResultData) request.getAttribute("cataloglist");

					int numOfRows = 0;
					if (shopList != null) {
						numOfRows = shopList.getNumRows();
					}
					int numOfSoldTo = 0;
					if (soldToList != null) {
						numOfSoldTo = soldToList.getNumRows();
					}
					int numOfCatalogs = 0;
					if (catalogList != null) {
						numOfCatalogs = catalogList.getNumRows();
					}
				%>

				<table 
					border="0" 
					cellspacing="3" 
					cellpadding="0">
					<tr>
						<td>
							<label 
								for="webshop">
								<isa:translate 
									key="b2b.portal.webshop"/>
							</label>
						</td>
						<td>
						&nbsp;
						</td>
						<td 
							valign="top">
			<%
			String exitField = "onChange=\"" + "settingform.WebCatalog.value='';" + "refreshForm();\"";
			%>							
							<select 
								style="width:160" 
								class="textInput" 
								size="1" 
								id="sub" 
								name="WebShop" 
								width="40" 
								maxlength="75" <%=exitField%> >
								<%
									if (numOfRows != 0) {
										shopList.first();

										for (int i = 1; i <= numOfRows; i++) {
											String shopName =
												shopList.getRowKey().toString();
											if (shopName
												.equalsIgnoreCase(shopId1)) {
								%>

								<option 
									selected=true 
									value="<%= shopList.getRowKey().toString() %>"><%= shopList.getRowKey().toString() %>
								</option>
								<%
									} else {
								%>

								<option 
									value="<%= shopList.getRowKey().toString() %>"><%= shopList.getRowKey().toString() %>
								</option>
								<%
									}
									shopList.next();
								}
								}
								%>

							</select>
						</td>
					</tr>
					<tr>
						<td>
							<label 
								for="soldto">
								<isa:translate 
									key="b2b.portal.soldto"/>
							</label>
						</td>
						<td>
						&nbsp;
						</td>
						<td 
							valign="top">
							<select 
								style="width:160" 
								class="textInput" 
								size="1" 
								id="sub" 
								name="SoldTo" 
								width="40" 
								maxlength="75">
								<%
									if (numOfSoldTo != 0) {
										soldToList.first();

										for (int i = 1;
											i <= numOfSoldTo;
											i++) {
											String  soldToP = soldToList.getString("SOLDTO");
											if (soldToP.equalsIgnoreCase(soldTo)) {
		
								%>
								<option 
									selected=true 
									value="<%= soldTo %>"><%= soldTo %>
								</option>
								<%
									} else {
								%>
								<option 
									value="<%= soldToList.getString("SOLDTO") %>"><%= soldToList.getString("SOLDTO") %>
								</option>
								<%
									}
									soldToList.next();
								}
								}
								%>

							</select>
						</td>
					</tr>
					<tr>
						<td>
							<label 
								for="catalog">
								<isa:translate 
									key="b2b.portal.webcatalog"/>
							</label>
						</td>
						<td>
						&nbsp;
						</td>
						<td 
							valign="top">
							<select 
								style="width:160" 
								class="textInput" 
								size="1" 
								id="sub" 
								name="WebCatalog" 
								width="40" 
								maxlength="75">
								<%
									if (!shop.isCatalogDeterminationActived() && numOfCatalogs < 2) {								
								%>

								<option 
									value="<%= request.getAttribute( ReadCatalogAction.PARAM_CATALOG) %>"><%= request.getAttribute(
								ReadCatalogAction.PARAM_CATALOG) %>
								</option>
								<%
									} else {
								%>

								<%
									if (numOfCatalogs != 0) {
										catalogList.first();

                                        for (int i = 1;	i <= numOfCatalogs; i++) {
                                            //RowKey catalogName = catalogList.getRowKey();
                                            if (catalog.equals(catalogList.getString("CATALOG_KEY"))) {
                                %>

                                <option 
                                    selected=true
                                    value="<%= catalogList.getString("CATALOG_KEY") %>"><%= catalogList.getString("CATALOG_KEY") %>
                                </option>
                                <%
                                            }
                                            else {
											
                                %>

                                <option 
                                    value="<%= catalogList.getString("CATALOG_KEY") %>"><%= catalogList.getString("CATALOG_KEY") %>
                                </option>
                                <%
                                            }
                                            catalogList.next();
                                        }
                                    }
                                }
                                %>

							</select>
						</td>
					</tr>
				</table>
				<table 
					border="0" 
					cellspacing="2" 
					cellpadding="10">
					<tr>
					</tr>
					<br>
					<tr>
						<td 
							align="left">
						&nbsp;&nbsp;	
						</td>
						<td 
							align="left">
							<input 
								style="width:60" 
								class="green" 
								type=submit 
								name="SaveSetting" 
								value="<isa:translate key="setting.button.set"/>"
							title="
							<isa:translate 
								key="access.button" 
								arg0="" 
								arg1=""/>"
							>
						</td>
						<td 
							align="right">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					
						<td 
							align="right">
							<input 
								style="width:60" 
								class="green" 
								type=submit 
								name="CancelSetting" 
								value="<isa:translate key="setting.button.cancel"/>"
							title="
							<isa:translate 
								key="access.button" 
								arg0="" 
								arg1=""/>"
							>
						</td>
					</tr>
				</table>
				<%
					}
					if (views != null) {
					    for (int viewIt = 0; viewIt < views.length; viewIt++) { %>
							<input type="hidden" name="ViewIds[<%= viewIt %>]"  value="<%= views[viewIt] %>" />
						    <%					    
						}
					}
				%>

			</form>
		</div>
	</body>
</html>
