<% /** ToDo:
   **/
%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%  MessageDisplayer messageDisplayer = (MessageDisplayer)request.getAttribute(MessageDisplayer.CONTEXT_NAME);

	boolean isApplicationError = true;

	if (messageDisplayer != null) {
	  isApplicationError = messageDisplayer.isApplicationError();
	} %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<isa:contentType/>
<html>
<head>
 <title><isa:translate key="error.jsp.title"/></title>
  <isa:includes/>
  <!--
  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet"> 
  -->
  <script type="text/javascript">
	function goBack() {
	  history.back();
	}
  </script>
</head>
<body class="workarea">
  <div class="errorMsg">
	<div class="module-name"><isa:moduleName name="/iviews/marketingprofile/error.jsp" /></div>
	<span>
		<isa:translate key="b2b.prt.marketingprofile.mess1"/> <br>
	<isa:translate key="b2b.prt.marketingprofile.mess2"/> <br>
	<isa:translate key="b2b.prt.marketingprofile.mess3"/> <br>
	</span>
  </div>

</body>
</html>