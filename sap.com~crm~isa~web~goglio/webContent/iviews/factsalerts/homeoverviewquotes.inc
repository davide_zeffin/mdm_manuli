       <!-- table for quotes, table 4 -->       
       <table border="0" width=100% height=100%>
        <tr>
         <td valign="top" align="right" width=15%>
                
          	<%
       		UrlParameters urlOrdersPars4 = new UrlParameters();       		
			//urlOrdersPars4.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
			//urlOrdersPars4.add("nextaction","setgensearchparam");    
			//urlOrdersPars4.add("rc_documenttypes", "QUOTATION" );
			//urlOrdersPars4.add("rc_datetoken", "last_year" );            
            AnchorAttributes anchorAttrOrder4 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars4,request);
     		%>
                <% if (!anchorAttrOrder4.getURI().equals("")) {  %>
	            <%
	            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.quotes", null);
	            %>
                  <a href="<%=anchorAttrOrder4.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder4.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                      <img src="<isa:webappsURL name ="/iviews/factsalerts/mimes/images/fa7.gif"/>" alt="<isa:translate key="b2b.prt.factsalerts.quotes"/>" border="0">
                  </a>
                <% }
                    else { %>
                      <!-- <img src="<isa:webappsURL name ="/iviews/factsalerts/mimes/images/fa7.gif"/>" alt="<isa:translate key="b2b.prt.factsalerts.quotes"/>" border="0"> -->
                <% } %>                                     
         </td>
         
         <td width=85%>
         
         <div class="bold">
                <% if (!anchorAttrOrder4.getURI().equals("")) {  %>
                
                
	            <%
	            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.quotes", null);
	            %>
          	<a href="<%=anchorAttrOrder4.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder4.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
          		<isa:translate key="b2b.prt.factsalerts.quotes"/>
          	</a>
          	          	
                <% }
                    else { %>
                      <!-- <isa:translate key="b2b.prt.factsalerts.quotes"/> -->
                <% } %>

          </div>
          <span><isa:translate key="b2b.prt.factsalerts.viewquotes"/></span><br><br>

                <%

       		UrlParameters urlOrdersPars5 = new UrlParameters();
			urlOrdersPars5.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
			urlOrdersPars5.add("nextaction","setgensearchparam");    
			urlOrdersPars5.add("rc_documenttypes", "QUOTATION" );
			urlOrdersPars5.add("rc_datetoken1", "last_week" );
			urlOrdersPars5.add("rc_dateattributes_select", "last_week" );					
			
                AnchorAttributes anchorAttrOrder5 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars5,request);
     		%>
                <% if (!anchorAttrOrder5.getURI().equals("")) {  %>
                
                <% if (shop.isQuotationExtended()) { %>

	            <%
	            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.quotenew", null);
				//accessText = accessText + " (" + newextendedquota +")";
	            %>
                  	<a href="<%=anchorAttrOrder5.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder5.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          <isa:translate key="b2b.prt.factsalerts.new"/> (<%= newextendedquota %>)
                  	</a>                
                
                
                <% } else { %> 	                  	        
                                                      
	            <%
	            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.quotenew", null);
				accessText = accessText + " (" + sumquota +")";
	            %>
                  	<a href="<%=anchorAttrOrder5.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder5.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          <isa:translate key="b2b.prt.factsalerts.new"/> (<%= sumquota %>)
                  	</a>
                
                <% } %>
                                                      
                <% }
                    else { %>
                      <!-- <span><isa:translate key="b2b.prt.factsalerts.new"/></span>  -->
                <% } %>

                <br>
                

<!-- changed -->
			<%

       			//completed (lean)
       			UrlParameters urlOrdersPars7 = new UrlParameters();
				urlOrdersPars7.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
				urlOrdersPars7.add("nextaction","setgensearchparam");    
				urlOrdersPars7.add("rc_documenttypes", "QUOTATION" );
				urlOrdersPars7.add("rc_datetoken1", "last_week" );
				urlOrdersPars7.add("rc_dateattributes_select", "last_week" );					            		
            	urlOrdersPars7.add("rc_status_head2", "I1005" );


       			//urlGen.setUrlParameters(urlOrdersPars7);
       			//String urlFinal7 = urlGen.readPageURL("SALESORDERCRM",urlOrdersPars7,portalURL,navModelVersion);
                AnchorAttributes anchorAttrOrder7 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars7,request);

				//expired (extended)
       			UrlParameters urlOrdersPars10 = new UrlParameters();
				urlOrdersPars10.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
				urlOrdersPars10.add("nextaction","setgensearchparam");    
				urlOrdersPars10.add("rc_documenttypes", "QUOTATION" );
				urlOrdersPars10.add("rc_datetoken1", "last_week" );
				urlOrdersPars10.add("rc_dateattributes_select", "last_week" );					            		
            	urlOrdersPars10.add("rc_status_head2", "COMPLETED" );
				AnchorAttributes anchorAttrOrder10 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars10,request);
				

     			%>
                	<% if (!anchorAttrOrder7.getURI().equals("")) {  %>
                  	
                  	  <% if (shop.isQuotationExtended()) { %> 	                  	        
			            <%
			            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.qcot", null);
						//accessText = accessText + " (" + donequota+")";
			            %>
                  	        <a href="<%=anchorAttrOrder10.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder10.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          		<isa:translate key="b2b.prt.factsalerts.cotq"/> (<%= expiredquota %>)                          		
                  		</a>
                  	
                  	  <% } else { %>
                  	  
			            <%
			            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.qcot", null);
						//accessText = accessText + " (" + donequota+")";
			            %>
                  		<a href="<%=anchorAttrOrder7.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder7.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          		<isa:translate key="b2b.prt.factsalerts.complofthose"/> (<%= donequota %>)
                  		</a>
                  		
                  	  <% } %>
                  		
                	<% }
                    	else { %>
                      		<!-- <span><isa:translate key="b2b.prt.factsalerts.complofthose"/></span> -->
                	<% } %>

                <br>
                <br>


          	<%
            
            //open (lean)
       		UrlParameters urlOrdersPars6 = new UrlParameters();
			urlOrdersPars6.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
			urlOrdersPars6.add("nextaction","setgensearchparam");    
			urlOrdersPars6.add("rc_documenttypes", "QUOTATION" );
			urlOrdersPars6.add("rc_daterange_low", "01.01.1900" );
			urlOrdersPars6.add("rc_daterange_high", "30.12.9999" );
			urlOrdersPars6.add("GSdateformat", "dd.mm.yyyy" );		
			urlOrdersPars6.add("rc_dateattributes_select", "att_in_period" );														
			urlOrdersPars6.add("rc_status_head2", "I1002" );
            AnchorAttributes anchorAttrOrder6 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars6,request);

       		UrlParameters urlOrdersPars11 = new UrlParameters();
			urlOrdersPars11.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
			urlOrdersPars11.add("nextaction","setgensearchparam");    
			urlOrdersPars11.add("rc_documenttypes", "QUOTATION" );
		    urlOrdersPars11.add("rc_daterange_low", "01.01.1900" );
			urlOrdersPars11.add("rc_daterange_high", "30.12.9999" );
			urlOrdersPars11.add("GSdateformat", "dd.mm.yyyy" );		
			urlOrdersPars11.add("rc_dateattributes_select", "att_in_period" );											
			urlOrdersPars11.add("rc_status_head2", "OPEN" );
            AnchorAttributes anchorAttrOrder11 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars11,request);

			//inrequest= open, (extended)

     		%>
                <% if (!anchorAttrOrder6.getURI().equals("")) {  %>
                
                  <% if (shop.isQuotationExtended()) { %> 	
                  
		            <%
		            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.qurqu", null);
					//accessText = accessText + " (" + openquota+")";
		            %>
                  	<a href="<%=anchorAttrOrder11.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder11.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          <isa:translate key="b2b.prt.factsalerts.reququs"/> (<%= inrequestquota %>)
                  	</a>                  	
                  	
                  <% } else { %>
                  
		            <%
		            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.quopen", null);
					//accessText = accessText + " (" + openquota+")";
		            %>
                  	<a href="<%=anchorAttrOrder6.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder6.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          <isa:translate key="b2b.prt.factsalerts.open"/> (<%= openquota %>)
                  	</a>                  	
                  
                  <% } %>
                   	
                <% }
                    else { %>
                      <!-- <span><isa:translate key="b2b.prt.factsalerts.open"/> (<%= openquota %>)</span> -->
                <% } %>

                <br>

		<% if (shop.isQuotationExtended()) { %> 	
		
 			
 			<%
		
			// Confirmed (Released) Quotations
		        		       		        
			UrlParameters urlOrdersPars8 = new UrlParameters();
			urlOrdersPars8.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
			urlOrdersPars8.add("nextaction","setgensearchparam");    
			urlOrdersPars8.add("rc_documenttypes", "QUOTATION" );
		    urlOrdersPars8.add("rc_daterange_low", "01.01.1900" );
			urlOrdersPars8.add("rc_daterange_high", "30.12.9999" );
			urlOrdersPars8.add("GSdateformat", "dd.mm.yyyy" );		
			urlOrdersPars8.add("rc_dateattributes_select", "att_in_period" );								
			urlOrdersPars8.add("rc_status_head2", "RELEASED" );
//            AnchorAttributes anchorAttrOrder11 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars11,request);

       			//urlGen.setUrlParameters(urlOrdersPars7);
       			//String urlFinal7 = urlGen.readPageURL("SALESORDERCRM",urlOrdersPars7,portalURL,navModelVersion);
                	AnchorAttributes anchorAttrOrder8 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars8,request);

     			%>
                	<% if (!anchorAttrOrder8.getURI().equals("")) {  %>
                	
                	<% if (shop.isQuotationExtended()) { %> 	                  	        
					
					<%
		            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.quacpt", null);
					//accessText = accessText + " (" + releasedquota+")";
		            %>
                  		<a href="<%=anchorAttrOrder8.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder8.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          		<isa:translate key="b2b.prt.factsalerts.accqus"/> (<%= confirmedquota %>)
                  		</a>                	
                  		
                	<% } else { %>
                	
		            <%
		            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.quacpt", null);
					//accessText = accessText + " (" + releasedquota+")";
		            %>
                  		<a href="<%=anchorAttrOrder8.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder8.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          		<isa:translate key="b2b.prt.factsalerts.accqus"/> (<%= acceptedquota %>)
                  		</a>
                  		
                  	<% } %>
                  	
                	<% }
                    	else { %>
                      		 <!-- <span><isa:translate key="b2b.prt.factsalerts.acceptedquotes"/></span> -->
                	<% } %>
		        
		        <br>		       
		
		<% } //endif shop.isQuotationExtended %>
		

                  <!--<br><br>--></b>
         
                  
         </td>         
        </tr>
       <!-- end table 4 -->     
       </table>       
