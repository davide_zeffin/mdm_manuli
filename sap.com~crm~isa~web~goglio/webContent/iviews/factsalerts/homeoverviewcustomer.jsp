<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.isacore.action.iview.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.util.Collection" %>


<%@ page import="com.sap.isa.businessobject.contract.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.actionform.b2b.contract.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<!-- imports will be used for portal page navigation -->
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>
<%@ page import="com.sap.isa.portalurlgenerator.businessobject.PortalUrlGenerator"%>
<%@ page import="com.sap.isa.portalurlgenerator.util.*"%>

<%@ include file="/b2b/messages-declare.inc.jsp" %>
<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<% BaseUI ui = new BaseUI(pageContext);%>
<% String accessText = ""; %>
<% DocumentListSelectorForm form =
    (DocumentListSelectorForm)pageContext.findAttribute("DocumentListSelectorFormFA");
%>
<%
   Shop shop = (Shop)request.getAttribute(DocumentListAction.RK_DLA_SHOP);

   String openorders = (String)request.getAttribute(HomeStartAction.RK_OPEN_ORDERS);
   String doneorders = (String)request.getAttribute(HomeStartAction.RK_DONE_ORDERS);
   String sumorders  = (String)request.getAttribute(HomeStartAction.RK_SUM_ORDERS);
   String openquota  = (String)request.getAttribute(HomeStartAction.RK_OPEN_QUOTA);
   String donequota  = (String)request.getAttribute(HomeStartAction.RK_DONE_QUOTA);
   String sumquota   = (String)request.getAttribute(HomeStartAction.RK_SUM_QUOTA);

    // get and init the portalUrlGenerator object
    //UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
    BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();

	boolean errorOccured = false;

%>

<isa:stylesheets/>
<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head><title>Homeoverview Customer</title>

  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css")%>" type="text/css" rel="stylesheet">

  <script src="../iviews/jscript/crm_isa_epcm.js" type="text/javascript"></script>

  <script type="text/javascript">
      function doNavigate(urlFinal) {
        return EPCMPROXY_EX.doNavigate(urlFinal);
      }
  </script>

</head>

<body class="organizer">


   <script type="text/javascript">
      liBehindFirstDot = location.hostname.indexOf( "." ) + 1;
      if (liBehindFirstDot > 0) {
        document.domain = location.hostname.substr( liBehindFirstDot );
      }
  </script>

  <div id="organizer-search" class="module">
  <div class="module-name"><isa:moduleName name="iviews/factsalerts/homeoverviewcustomer.jsp" /></div>
    
<form>
<!-- BEGIN TABLE STRUCTURE -->
<!--outer table, table 1-->
<table border="0" cellpadding="3" width="100%" cellspacing="0">
 <tr>
  <td>   
   
   <!-- table for orders and quotes, table 2 -->   
   <table border="0" cellpadding="3" width="100%" cellspacing="0">
    
    <tr>
     <!-- My orders -->
     <td width=50%>
       <!-- table for orders, table 3 -->   
       <table border="0" width=100% height=100%>
        <tr>
         <td valign="top" align="right" width=15%>       
       		<%
       		UrlParameters urlOrdersPars = new UrlParameters();
			//urlOrdersPars.add("genericsearch.name", "SearchCriteria_BOB_Sales" );             
			//urlOrdersPars.add("nextaction","setgensearchparam");    
			//urlOrdersPars.add("rc_documenttypes", "ORDER" );
			//urlOrdersPars.add("rc_datetoken", "last_year" );
       		AnchorAttributes anchorAttrOrder = urlGen.readPageURL("SALESORDERCRM","","","TOB2R","",urlOrdersPars,request);
       		%>
       		<% if (!anchorAttrOrder.getURI().equals("")) {  %>
                <%
                accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.custord", null);
                %>
        		<a href="<%=anchorAttrOrder.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
        			<img src="<isa:webappsURL name ="/iviews/factsalerts/mimes/images/fa4.gif"/>" alt="<isa:translate key="b2b.prt.factsalerts.custord"/>" border="0">
        		</a>
      		<% }
          	else { %>
            		<img src="<isa:webappsURL name ="/iviews/factsalerts/mimes/images/fa4.gif"/>" alt="<isa:translate key="b2b.prt.factsalerts.custord"/>" border="0">
      		<% } %>
     
      		<br><br>
      		
         </td>
         
         <td width=85%><div class="bold">
        	
        	<% if (!anchorAttrOrder.getURI().equals("")) {  %>
                <%
                accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.custord", null);
                %>
        		<a href="<%=anchorAttrOrder.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
          		<isa:translate key="b2b.prt.factsalerts.custord"/>
        		</a>
        	<% }
           	 else { %>
              		<% errorOccured = true; %>
        	<% } %>

	      </div>
	  		<span><isa:translate key="b2b.prt.factsalerts.viewcusto"/></span><br><br>


      		<% UrlParameters urlOrdersPars1 = new UrlParameters();
			urlOrdersPars1.add("genericsearch.name", "SearchCriteria_BOB_Sales" );             
			urlOrdersPars1.add("nextaction","setgensearchparam");    
			urlOrdersPars1.add("rc_documenttypes", "ORDER" );
			urlOrdersPars1.add("rc_datetoken1", "last_week" );
			urlOrdersPars1.add("rc_dateattributes_select", "last_week" );	
      		AnchorAttributes anchorAttrOrder1 = urlGen.readPageURL("SALESORDERCRM","","","TOB2R","",urlOrdersPars1,request);
      		%>
      		<% if (!anchorAttrOrder1.getURI().equals("")) {  %>
            <%
            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.ordernew", null);
			accessText = accessText + " (" + sumorders +")";
            %>
        	<a href="<%=anchorAttrOrder1.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder1.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
          		<isa:translate key="b2b.prt.factsalerts.new"/> (<%= sumorders %>)
        	</a>
        	<% }
            	else { %>
					<% errorOccured = true; %>
        	<% } %>

        	<br>

      		<% UrlParameters urlOrdersPars3 = new UrlParameters();
			urlOrdersPars3.add("genericsearch.name", "SearchCriteria_BOB_Sales" );             
			urlOrdersPars3.add("nextaction","setgensearchparam");    
			urlOrdersPars3.add("rc_documenttypes", "ORDER" );
			urlOrdersPars3.add("rc_datetoken1", "last_week" );
			urlOrdersPars3.add("rc_dateattributes_select", "last_week" );								
			urlOrdersPars3.add("rc_status_head1", "I1005" );
      		AnchorAttributes anchorAttrOrder3 = urlGen.readPageURL("SALESORDERCRM","","","TOB2R","",urlOrdersPars3,request);
      		%>
      		<% if (!anchorAttrOrder3.getURI().equals("")) {  %>
            <%
            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.ocom", null);
			accessText = accessText + " (" + doneorders +")";
            %>
        	<a href="<%=anchorAttrOrder3.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder3.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
          		<isa:translate key="b2b.prt.factsalerts.complofthose"/> (<%= doneorders %>)
        	</a>
      		<% }
          	else { %>
				<% errorOccured = true; %>
      		<% } %>

      		<br>
      		<br>
      		      		      		      		            		
      		
      		<% UrlParameters urlOrdersPars2 = new UrlParameters();
			urlOrdersPars2.add("genericsearch.name", "SearchCriteria_BOB_Sales" );             
			urlOrdersPars2.add("nextaction","setgensearchparam");    
			urlOrdersPars2.add("rc_documenttypes", "ORDER" );
			urlOrdersPars2.add("rc_daterange_low", "01.01.1900" );
			urlOrdersPars2.add("rc_daterange_high", "30.12.9999" );
			urlOrdersPars2.add("GSdateformat", "dd.mm.yyyy" );		
			urlOrdersPars2.add("rc_dateattributes_select", "att_in_period" );											
			urlOrdersPars2.add("rc_status_head1", "I1002" );			
      		AnchorAttributes anchorAttrOrder2 = urlGen.readPageURL("SALESORDERCRM","","","TOB2R","",urlOrdersPars2,request);
      		%>
      		<% if (!anchorAttrOrder2.getURI().equals("")) {  %>
            <%
            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.orderopen", null);
			accessText = accessText + " (" + openorders +")";
            %>
        	<a href="<%=anchorAttrOrder2.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder2.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
          		<isa:translate key="b2b.prt.factsalerts.open"/> (<%= openorders %>)
        	</a>
      		<% }
          	else { %>
				<% errorOccured = true; %>
      		<% } %>

        	<br>

                                                           
         </td>         
        </tr>
       <!-- end table 3 -->    
       </table>
     </td>
     
     <td width=50%>
       
       <!---- My Quotes ---->
       <% if (shop.isQuotationAllowed()) {%>
        
       <!-- table for quotes, table 4 -->       
       <table border="0" width=100% height=100%>
        <tr>
         <td valign="top" align="right" width=15%>
          
              	<% UrlParameters urlOrdersPars4 = new UrlParameters();			
			urlOrdersPars4.add("genericsearch.name", "SearchCriteria_BOB_Sales" );             
			urlOrdersPars4.add("nextaction","setgensearchparam");    
			urlOrdersPars4.add("rc_documenttypes", "QUOTATION" );
			urlOrdersPars4.add("rc_datetoken1", "last_year" );
		    urlOrdersPars4.add("rc_dateattributes_select", "last_year" );								
      		AnchorAttributes anchorAttrOrder4 = urlGen.readPageURL("SALESORDERCRM","","","TOB2R","",urlOrdersPars4,request);
      		%>
      		<% if (!anchorAttrOrder4.getURI().equals("")) {  %>
            <%
            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.custquotes", null);
            %>
        	<a href="<%=anchorAttrOrder4.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder4.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
             		<img src="<isa:webappsURL name ="/iviews/factsalerts/mimes/images/fa5.gif"/>" alt="<isa:translate key="b2b.prt.factsalerts.custquotes"/>" border="0">
        	</a>
      		<% }
          	else { %>
            		<% errorOccured = true; %>
      		<% } %>

      		<br>
      		<br>
                                        
         </td>
         
         
         <td width=85%><div class="bold">
          	
          	<% if (!anchorAttrOrder4.getURI().equals("")) {  %>
            <%
            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.custquotes", null);
            %>
            	<a href="<%=anchorAttrOrder4.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder4.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
              		<isa:translate key="b2b.prt.factsalerts.custquotes"/>
            	</a>
          	<% }
              	else { %>
                	<span><isa:translate key="b2b.prt.factsalerts.custquotes"/></span>
          	<% } %>
             </div>
        		<span><isa:translate key="b2b.prt.factsalerts.viewcustio"/></span>
           	<br>
           	<br>
          	</a>
    		<% UrlParameters urlOrdersPars5 = new UrlParameters();
			urlOrdersPars5.add("genericsearch.name", "SearchCriteria_BOB_Sales" );             
			urlOrdersPars5.add("nextaction","setgensearchparam");    
			urlOrdersPars5.add("rc_documenttypes", "QUOTATION" );
			urlOrdersPars5.add("rc_datetoken1", "last_week" );
		    urlOrdersPars5.add("rc_dateattributes_select", "last_week" );					
      		AnchorAttributes anchorAttrOrder5 = urlGen.readPageURL("SALESORDERCRM","","","TOB2R","",urlOrdersPars5,request);
      		%>
      		<% if (!anchorAttrOrder5.getURI().equals("")) {  %>
            <%
            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.quotenew", null);
			accessText = accessText + " (" + sumquota +")";
            %>
        	<a href="<%=anchorAttrOrder5.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder5.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
          		<isa:translate key="b2b.prt.factsalerts.new"/> (<%= sumquota %>)
        	</a>
      		<% }
          	else { %>
            		<% errorOccured = true; %>
      		<% } %>

      		<br>
      		
      		<% UrlParameters urlOrdersPars7 = new UrlParameters();
			urlOrdersPars7.add("genericsearch.name", "SearchCriteria_BOB_Sales" );             
			urlOrdersPars7.add("nextaction","setgensearchparam");    
			urlOrdersPars7.add("rc_documenttypes", "QUOTATION" );
			urlOrdersPars7.add("rc_datetoken1", "last_week" );
		    urlOrdersPars7.add("rc_dateattributes_select", "last_week" );					
			urlOrdersPars7.add("rc_status_head2", "I1005" );
      		AnchorAttributes anchorAttrOrder7 = urlGen.readPageURL("SALESORDERCRM","","","TOB2R","",urlOrdersPars7,request);
      		%>

      		<% if (!anchorAttrOrder7.getURI().equals("")) {  %>
            <%
            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.qcot", null);
			accessText = accessText + " (" + donequota +")";
            %>
        	<a href="<%=anchorAttrOrder7.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder7.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
          		<isa:translate key="b2b.prt.factsalerts.complofthose"/> (<%= donequota %>)
        	</a>
      		<% }
          	else { %>
          		<% errorOccured = true; %>
      		<% } %>
      		
      		<br>
      		<br>
      		

     		<% UrlParameters urlOrdersPars6 = new UrlParameters();
			urlOrdersPars6.add("genericsearch.name", "SearchCriteria_BOB_Sales" );             
			urlOrdersPars6.add("nextaction","setgensearchparam");    
			urlOrdersPars6.add("rc_documenttypes", "QUOTATION" );
    		urlOrdersPars6.add("rc_daterange_low", "01.01.1900" );
	  	    urlOrdersPars6.add("rc_daterange_high", "30.12.9999" );
		    urlOrdersPars6.add("GSdateformat", "dd.mm.yyyy" );		
		    urlOrdersPars6.add("rc_dateattributes_select", "att_in_period" );								
			
			urlOrdersPars6.add("rc_status_head2", "I1002" );
	      	AnchorAttributes anchorAttrOrder6 = urlGen.readPageURL("SALESORDERCRM","","","TOB2R","",urlOrdersPars6,request);
      		%>
      		<% if (!anchorAttrOrder6.getURI().equals("")) {  %>
            <%
            accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.quopen", null);
			accessText = accessText + " (" + openquota +")";
            %>
        	<a href="<%=anchorAttrOrder6.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder6.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
          		<isa:translate key="b2b.prt.factsalerts.open"/> (<%= openquota %>)
        	</a>
      		<% }
          	else { %>
            		<% errorOccured = true; %>
      		<% } %>

      		<br>

      		

         </td>         
        </tr>
       <!-- end table 4 -->     
       </table>       
        
     
       <% } //end if: shop.isQuotationAllowed %>
       
     </td>      
    </tr>
   <!-- end table 2 -->      
   </table>
   
  </td>  
 </tr> 
 
    		    <!-- display error message if no links can be determined -->
   				<% if ( errorOccured ) { %>
   				<div class="error-items">
                    <span>
   						 
   						 <% WebUtil.translate(pageContext, "b2b.iview.nolink", null); %>

   				
   					</span>
   				</div>
   				
   				<% } %>
 
 
<!-- end table 1 -->       
</table>

</form>

</body>
</html>