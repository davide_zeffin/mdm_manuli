<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.isacore.action.iview.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.util.Collection" %>

<%@ page import="com.sap.isa.isacore.action.iview.HomeStartAction" %>


<%@ page import="com.sap.isa.businessobject.contract.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.actionform.b2b.contract.*" %>

<!-- imports will be used for portal page navigation -->
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>
<%@ page import="com.sap.isa.portalurlgenerator.businessobject.PortalUrlGenerator"%>
<%@ page import="com.sap.isa.portalurlgenerator.util.*"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ include file="/b2b/messages-declare.inc.jsp" %>
<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<% BaseUI ui = new BaseUI(pageContext);%>
<% String accessText = ""; %>
<% DocumentListSelectorForm form =
    (DocumentListSelectorForm)pageContext.findAttribute("DocumentListSelectorFormFA");
%>
<%
   Shop shop = (Shop)request.getAttribute(DocumentListAction.RK_DLA_SHOP);
   
   String openorders = (String)request.getAttribute(HomeStartAction.RK_OPEN_ORDERS);
   String doneorders = (String)request.getAttribute(HomeStartAction.RK_DONE_ORDERS);
   String sumorders  = (String)request.getAttribute(HomeStartAction.RK_SUM_ORDERS);
   String openquota  = (String)request.getAttribute(HomeStartAction.RK_OPEN_QUOTA);
   String donequota  = (String)request.getAttribute(HomeStartAction.RK_DONE_QUOTA);
   String sumquota   = (String)request.getAttribute(HomeStartAction.RK_SUM_QUOTA);

   String newextendedquota  = (String)request.getAttribute(HomeStartAction.RK_NEW_EXTENDED_QUOTA);
   String expiredquota  = (String)request.getAttribute(HomeStartAction.RK_EXPIRED_QUOTA);
   String inrequestquota = (String)request.getAttribute(HomeStartAction.RK_INREQUEST_QUOTA);
   String confirmedquota = (String)request.getAttribute(HomeStartAction.RK_CONFIRMED_QUOTA);

   String releasedquota  = (String)request.getAttribute(HomeStartAction.RK_RELEASED_QUOTA);
   String acceptedquota  = (String)request.getAttribute(HomeStartAction.RK_ACCEPTED_QUOTA);
   String completedquota = (String)request.getAttribute(HomeStartAction.RK_COMPLETED_QUOTA);

   // get and init the portalUrlGenerator object
   //UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
   BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
   PortalUrlGenerator urlGen = bom.createPortalUrlGenerator();
   
   boolean errorOccured = false;
   
%>

<isa:stylesheets/>
<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
  <title>Homeoverview</title>
<link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css")%>" type="text/css" rel="stylesheet">

   <script src="../iviews/jscript/crm_isa_epcm.js" type="text/javascript"></script>

   <script type="text/javascript">
   function doNavigate(urlFinal) {
      return EPCMPROXY_EX.doNavigate(urlFinal);
   }
   </script>

</head>

<body>

<script type="text/javascript">
       liBehindFirstDot = location.hostname.indexOf( "." ) + 1;
       if (liBehindFirstDot > 0) {
         document.domain = location.hostname.substr( liBehindFirstDot );
       }
</script>
    
    <div id="organizer-search" class="module">
    <div class="module-name"><isa:moduleName name="iviews/factsalerts/homeoverview.jsp" /></div>
      
      <form> 
	
	<!--outer table, table 1-->
	<table border="0" cellpadding="3" width="100%" cellspacing="0">
 		<tr>
  		<td>   
			   <!-- table for orders and quotes, table 2 -->   
			   <table border="0" cellpadding="3" width="100%" cellspacing="0">
    				<tr>     
     				<td width=50%>
       					<!-- table for orders, table 3 -->   
       					<table border="0" width=100% height=100%>
        				<tr>
         				<td valign="top" align="right" width=15%>
          	
          				<% UrlParameters urlOrdersPars = new UrlParameters();       		
            				//urlOrdersPars.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
            				//urlOrdersPars.add("rc_datetoken", "last_year" );
		                	AnchorAttributes anchorAttrOrder = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars,request);
     					%>
                			<% if (!anchorAttrOrder.getURI().equals("")) {  %>
                			<%
                			accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.orders", null);
							
                			%>
      		  			<a href="<%=anchorAttrOrder.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>" >
                  			<img src="<isa:webappsURL name ="/iviews/factsalerts/mimes/images/fa1.gif"/>" alt="<isa:translate key="b2b.prt.factsalerts.orders"/>" border="0">
      		  			</a>
                			<% }
                    			else { %>
                      				<!-- <img src="<isa:webappsURL name ="/iviews/factsalerts/mimes/images/fa1.gif"/>" alt="<isa:translate key="b2b.prt.factsalerts.orders"/>" border="0"> -->
                      				<% errorOccured = true; %>
                			<% } %>
					<br>
					<br>
			        	</td>
         				<td width=85% colspan=2><div class="bold">
                                          
             		   		<% if (!anchorAttrOrder.getURI().equals("")) {  %>
                			<%
                			//accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.orders", null);
							
                			%>
                  			<a href="<%=anchorAttrOrder.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          		<isa:translate key="b2b.prt.factsalerts.orders"/>
                  			</a>
                			<% }
                    			else { %>
                      			<!-- <isa:translate key="b2b.prt.factsalerts.orders"/> -->
								<% errorOccured = true; %>
                			<% } %>
					</div>
					<span><isa:translate key="b2b.prt.factsalerts.entries"/></span><br><br>                        
         				</td>
        				</tr>
        				
					<% UrlParameters urlOrdersPars1 = new UrlParameters();      		
					urlOrdersPars1.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
					urlOrdersPars1.add("nextaction","setgensearchparam");    
					urlOrdersPars1.add("rc_documenttypes", "ORDER" );
					urlOrdersPars1.add("rc_datetoken1", "last_week" );
					urlOrdersPars1.add("rc_dateattributes_select", "last_week" );					

                			AnchorAttributes anchorAttrOrder1 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars1,request);
                			%>
                			<tr>
         				<td>
         				</td>
        				<td>
                			<% if (!anchorAttrOrder1.getURI().equals("")) {  %>
                			<%
                			//accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.ordernew", null);
					        //accessText = accessText + " (" + sumorders +")";
							
                			%>
                  			<a href="<%=anchorAttrOrder1.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder1.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          			<isa:translate key="b2b.prt.factsalerts.new"/> (<%= sumorders %>) 
                  			</a>
                			<% }
                    			else { %>
                      				<!-- <isa:translate key="b2b.prt.factsalerts.new"/>  -->
								<% errorOccured = true; %>
                			<% } %>                
                			<br>
        				<% UrlParameters urlOrdersPars3 = new UrlParameters();      		
					urlOrdersPars3.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
					urlOrdersPars3.add("nextaction","setgensearchparam");    
					urlOrdersPars3.add("rc_documenttypes", "ORDER" );
					urlOrdersPars3.add("rc_datetoken1", "last_week" );
					urlOrdersPars3.add("rc_dateattributes_select", "last_week" );					
            		urlOrdersPars3.add("rc_status_head1", "I1005" );
                	AnchorAttributes anchorAttrOrder3 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars3,request);
                			%>
                			<% if (!anchorAttrOrder3.getURI().equals("")) {  %>
                			<%
                				accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.ocom", null);
						        //accessText = accessText + " (" + doneorders +")";
                			%>               
      					<a href="<%=anchorAttrOrder3.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder3.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
      						<isa:translate key="b2b.prt.factsalerts.complofthose"/> (<%= doneorders %>)
      					</a>
                			<% }
                    			else { %>
                      				 <!-- <isa:translate key="b2b.prt.factsalerts.complofthose"/>  -->
                      				 <% errorOccured = true; %>
                			<% } %>               
               				<br>
               				<br>
					<% UrlParameters urlOrdersPars2 = new UrlParameters();
					urlOrdersPars2.add("genericsearch.name", "SearchCriteria_B2B_Sales" );             
					urlOrdersPars2.add("nextaction","setgensearchparam");    
					urlOrdersPars2.add("rc_documenttypes", "ORDER" );
					urlOrdersPars2.add("rc_daterange_low", "01.01.1900" );
					urlOrdersPars2.add("rc_daterange_high", "30.12.9999" );
					urlOrdersPars2.add("GSdateformat", "dd.mm.yyyy" );		
					urlOrdersPars2.add("rc_dateattributes_select", "att_in_period" );								
					urlOrdersPars2.add("rc_status_head1", "I1002" );
                	AnchorAttributes anchorAttrOrder2 = urlGen.readPageURL("SALESORDERCRM","","","TOB2B","",urlOrdersPars2,request);
                			%>
                			<% if (!anchorAttrOrder2.getURI().equals("")) {  %>
                			<%
                				accessText = WebUtil.translate(pageContext, "b2b.prt.factsalerts.mo.orderopen", null);
						        //accessText = accessText + " (" + openorders +")";
                			%>                
                  			<a href="<%=anchorAttrOrder2.getURI()%>" onClick="doNavigate('<%=anchorAttrOrder2.getJavaScript()%>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                          			<isa:translate key="b2b.prt.factsalerts.open"/> (<%= openorders %>)
                  			</a>
                			<% }
                    			else { %>
                      				 <!-- <isa:translate key="b2b.prt.factsalerts.open"/>  -->
                      				 <% errorOccured = true; %>
                			<% } %>        				
        				

			        	</td>
       					</tr> 
       					</table> <!-- end table 3-->
       				</td>     
     				<td width=50%>
     					       <% if (shop.isQuotationAllowed()) {%>       
							<%@ include file="/iviews/factsalerts/homeoverviewquotes.inc" %>       
       						<% } else {  // if available: contracts, else bills    %>         
         					<% if (shop.isContractAllowed()) {%>         
         						<%@ include file="/iviews/factsalerts/homeoverviewcontracts.inc" %>		         
       						<% } else { %>       		
       						<% if (shop.isBillingDocsEnabled()) { %>           
           						<%@ include file="/iviews/factsalerts/homeoverviewbills.inc" %>	           
           					<% } %>           		
                				<% } %>                
       					     <% } //end if quotationAllowed %>       				     
     				</td>      
			   </td>
			   </tr>
			   </table> <!-- end table 2-->
		</td>
       		</tr>
       		
       		<tr>
       		<td>
       		   	   <!-- table for contracts and bills, table 5 -->   
   			   <table border="0" cellpadding="3" width="100%" cellspacing="0">    
    				<tr>
     				<!-- My Contracts -->
     				<td width=50%>     
     				<% if (shop.isContractAllowed() && shop.isQuotationAllowed()) {%>     
					<%@ include file="/iviews/factsalerts/homeoverviewcontracts.inc" %>       
     				<% } // endif isContractsAllowed%>
            
     				<% if (!shop.isContractAllowed() && shop.isQuotationAllowed()) {%>       
       					<% if (shop.isBillingDocsEnabled()) { %>       
						<%@ include file="/iviews/factsalerts/homeoverviewbills.inc" %>	       
       					<% } %>       
     				<% } // endif isContractsAllowed%>  
     
     				<% if (shop.isContractAllowed() && !shop.isQuotationAllowed()) {%>       
       					<% if (shop.isBillingDocsEnabled()) { %>       
						<%@ include file="/iviews/factsalerts/homeoverviewbills.inc" %>	       
       					<% } %>       
     				<% } // endif isContractsAllowed%>  	       
     				</td>
     
     				<!-- My Bills -->
     				<td width=50%>       
      				<!-- link to list of open bills in direct biller system
	     				precautions: 	existing portalURL (from property-File)
     					call from Portal
     					billingdocs allowed for shop   -->

      					<% //if (portalBillingURL != null || portalBillingURL.equals("")) {        
        					if (shop.isBillingDocsEnabled() && shop.isContractAllowed() && shop.isQuotationAllowed()) {%>
     							<%@ include file="/iviews/factsalerts/homeoverviewbills.inc" %>	              	            	     	
       					<% } //endif portalbillingenabled %>              
     				</td>      
    				</tr>
   				<!-- end table 5 -->      
   				
   				<!-- display error message if no links can be determined -->
   				<% if ( errorOccured ) { %>
   				<div class="error-items">
                    <span>
   						 
   						 <% WebUtil.translate(pageContext, "b2b.iview.nolink", null); %>

   				
   					</span>
   				</div>
   				
   				<% } %>
   			   </table>     		
       		</td>
       		</tr>       		
	</table> <!-- end table 1 -->      
      </form>  
</body>
</html>