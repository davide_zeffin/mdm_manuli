<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title></title>
    <script type="text/javascript">
      function loadStartPage(){

        document.location="<isa:webappsURL name="/b2b/home-start.do"/>";
      }
    </script>
  </head>
  <body onload="loadStartPage();return true;">
  </body>
</html>