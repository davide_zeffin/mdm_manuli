<%--
********************************************************************************
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author        SAP AG
********************************************************************************
--%>

<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.AutoentryErrorConstants" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ page import="com.sap.isa.core.util.*" %>

<%@ include file="/b2b/messages-declare.inc.jsp" %>
<%@ include file="/b2b/usersessiondata.inc" %>
<isa:contentType />
<% BaseUI ui = new BaseUI(pageContext);%>
<% String accessText = ""; %>
<%  String errorContext = (String) request.getAttribute(SelectSoldToAction.RK_AUTOENTRYERROR);
    String additionInfo = (String) request.getAttribute(SelectSoldToAction.RK_AUTOENTRYADDINFO);
    String errorMsg = "";
    String errorCode = "";
    if (errorContext.equals("shoperror")) {
        errorMsg = WebUtil.translate(pageContext, "autoentry.error.shop01", null);
        errorCode=AutoentryErrorConstants.ERROR_FINDING_SHOP;
    }
    if (errorContext.equals("catdetermineerror")) {
        errorMsg = WebUtil.translate(pageContext, "autoentry.error.shop02", null);
        errorCode=AutoentryErrorConstants.ERROR_CAT_DETERMINATION_ACTIVE;
    }
    if (errorContext.equals("soldtoerror")) {
        errorMsg = WebUtil.translate(pageContext, "autoentry.error.soldto01", null);
        errorCode=AutoentryErrorConstants.ERROR_MULTIPLE_SOLDTOS;
    }
    errorMsg = errorMsg.concat(" ").concat(WebUtil.translate(pageContext, "autoentry.error.part2", null));
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <!--
  <isales:autoentry:errorcode><%=errorCode%></isales:autoentry:errorcode>
  <isales:autoentry:message><%=errorMsg%></isales:autoentry:message>
  <isales:autoentry:addinfo><%=additionInfo%></isales:autoentry:addinfo>
  -->
  <title>Auto entry Error page</title>
  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css" ) %>" type="text/css" rel="stylesheet" />
</head>

<body class="hints">
    <div id="newdoc" class="module">
    <div class="module-name"><isa:moduleName name="iviews/autoentryloginerror.jsp" /></div>
    
    <% if (errorContext.equals("shoperror")) { %>
    <span><isa:translate key="autoentry.error.shop01"/></span>
    <% } %>
    <% if (errorContext.equals("catdetermineerror")) { %>
    <span><isa:translate key="autoentry.error.shop02"/></span>
    <% } %>
    <% if (errorContext.equals("soldtoerror")) { %>
    <span><isa:translate key="autoentry.error.soldto01"/></span>
    <% } %>
    <br/><br/>
    <span><isa:translate key="autoentry.error.part2"/></span>
    </div>
</body>
</html>