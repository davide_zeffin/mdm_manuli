/*----------------------------------------------------*/
/* Javascript for the "SAP CCH SetupGuide Project"    */
/*                                                    */
/* Document:       portalnavigation.js                */
/* Description:    functions for portal nav.          */
/* Version:        1.0                                */
/* Author:         SAP AG                             */
/* Creation-Date:  16.01.2003                         */
/* Last-Update:    16.01.2002                         */
/*                                                    */
/*                               (c) SAP AG, Walldorf */
/*----------------------------------------------------*/
function doNavigate(urlFinal, navModelVersion, CRMIsAvailable) {
    if (navModelVersion == "page") {
      top.frames['Desktop'].location.href=urlFinal;
    }
    else {
      if (CRMIsAvailable == true) {
        EPCMPROXY_EX.doNavigate(urlFinal);
      }
      else {
        top.frames['Desktop'].location.href=urlFinal;
      }
    }
}
