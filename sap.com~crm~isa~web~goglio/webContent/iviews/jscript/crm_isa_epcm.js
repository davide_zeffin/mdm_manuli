//------------------------------------------------------------------------------
// 
// CRM_BSP_EPCM.js
//
//------------------------------------------------------------------------------

// ============================================================================
// * Title:        Client Framework Proxy Extended Include
// * --------------------------------------------------------------------------
// * Description: EPCMPROXY Extended object (wrapper for heavyweight EPCM Proxy)
// *              and heavyweight EPCM Proxy
// * Version 1.0
// * Copyright (c) SAP AG 2002
// * --------------------------------------------------------------------------
// * Notes; 
// *  1) Works only with browsers with JavaScipt 1.2+ (IE 5.0+, NN6.0+)
// *  2) Subscribe Event must be called with the Object signature with the
// *     current window object
// *  3) All system methods returns null,when the parent.EPCM object could
// *     not be accessed
// *  4) The proxy looks for the EPCM in an object's parents, up the hierarchy
// *     until there are no more parents
// *  5) Eventing identical to original EPCM object
// ============================================================================


// ==========================================================
//             EPCMPROXY Extended object
// ==========================================================

var EPCMPROXY_EX = {
 OTHER   : 0, 
 MSIE    : 1,
 NETSCAPE: 2,
 MOZILLA : 2,
 OPERA   : 3,
 NOKIA   : 4,
 UP      : 5,
 ERICSSON: 6,
 MSPIE   : 7,
 PALM    : 8,   
    
 OTHER_PLATFORM : 0,
 NT_PLATFORM    : 1,
 WIN_PLATFORM   : 2,
 MAC_PLATFORM   : 3,
 LINUX_PLATFORM : 4, 
 WAP_PLATFORM   : 5,
 PDA_PLATFORM   : 6

}

// --------- Getting the EPCM object ------------------------
EPCMPROXY_EX.getParentEPCM = function() {
  return EPCM;
}

// --------- Proxy for System methods  ------------------------
EPCMPROXY_EX.getUAType = function() {
  try{
    return this.getParentEPCM().getUAType(); 
  }catch(ex){
    return null;
  } 
}
EPCMPROXY_EX.getUAVersion = function() {
  try{
    return this.getParentEPCM().getUAVersion();
  }catch(ex){
    return null;
  }
}
EPCMPROXY_EX.getUAPlatform = function() {
  try{  
    return this.getParentEPCM().getUAPlatform();
  }catch(ex){
    return null;
  }
}
EPCMPROXY_EX.getVersion = function() {
  try{
    return this.getParentEPCM().getVersion();
  }catch(ex){
    return null; 
  }	
}
EPCMPROXY_EX.getLevel = function() {
  try{
    return this.getParentEPCM().getLevel();
  }catch(ex){
    return null;
  }  
}
EPCMPROXY_EX.getInstanceId = function() {
  try{
    return this.getParentEPCM().getInstanceId();
  }catch(ex){
    return null; 
  }
}
EPCMPROXY_EX.getUniqueWindowId = function() {
  try{
    return this.getParentEPCM().getUniqueWindowId();
  }catch(ex){
    return null;
  }	  
}

// --------- Proxy for DataBag methods ------------------------
EPCMPROXY_EX.storeClientData = function(pUrn, pName, pValue){
 try{
   this.getParentEPCM().storeClientData(pUrn,pName,pValue)
 }catch(ex){}

}
EPCMPROXY_EX.loadClientData = function(pUrn, pName){
  try{
    return this.getParentEPCM().loadClientData(pUrn, pName);
  }catch(ex){
    return null; 
  }  
}
EPCMPROXY_EX.deleteClientData = function(pUrn, pName){
  try{
    this.getParentEPCM().deleteClientData(pUrn, pName);
  }catch(ex){}  
}

// --------- Proxy for Eventing methods ------------------------
EPCMPROXY_EX.subscribeEvent = function( nameSpace, eventName, handlerRef, methodName ){
  try{
  	// the length of the argument list is queried by EPCM
  	// --> it matters how many arguments we pass to it
  	if (arguments.length == 3) {
  		// this works for event handlers specified by a function pointer
  		this.getParentEPCM().subscribeEvent(nameSpace, eventName, handlerRef);
		}
		else {
			// this works for object methods as event handler
			// and for a method inside a specified frame as event handler
    	this.getParentEPCM().subscribeEvent(nameSpace, eventName, handlerRef, methodName); 
    }
  }catch(ex){}  
}
EPCMPROXY_EX.raiseEvent = function( nameSpace, eventName, dataObject, sourceId ) {
  try{
    this.getParentEPCM().raiseEvent(nameSpace, eventName, dataObject, sourceId);
  }catch(ex){}  
}

// --------- Proxy for Dirty & Navigation  ---------------------
EPCMPROXY_EX.setDirty = function ( val ){
  try{ 
    this.getParentEPCM().setDirty(val);
  } catch(ex){} 
}
EPCMPROXY_EX.getDirty = function( ){
  try{
    return this.getParentEPCM().getDirty();	
  } catch(ex){
    return false;
  } 
}
EPCMPROXY_EX.doNavigate = function (pcdurl){
  try{
    return this.getParentEPCM().doNavigate(pcdurl);	
  } catch(ex){
    return true;
  } 
}

function raiseAfterSelectionChange(param){
	try{
		EPCMPROXY_EX.raiseEvent("com.sap.pct.crm","iviewCommunicationEvent", param);
	}
	catch(ex){
		return false;
	}
}


// ================================
// ***  heavyweight EPCM Proxy  ***
// ================================

var EPCM = {
 
 OTHER   : 0, 
 MSIE    : 1,
 NETSCAPE: 2,
 MOZILLA : 2,
 OPERA   : 3,
 NOKIA   : 4,
 UP      : 5,
 ERICSSON: 6,
 MSPIE   : 7,
 PALM    : 8,   
    
 OTHER_PLATFORM : 0,
 NT_PLATFORM    : 1,
 WIN_PLATFORM   : 2,
 MAC_PLATFORM   : 3,
 LINUX_PLATFORM : 4, 
 WAP_PLATFORM   : 5,
 PDA_PLATFORM   : 6,

  _private: {
    id: "EPCM_Light_for_CRM",
    dbgException: false
  }   



}

// --------- Proxy for System methods, delegate to parent  ------------------------
EPCM.getUAType = function() {
  try{
    return this._private.getParentEPCM().getUAType(); 
  }catch(ex){
    return null;
  } 
}
EPCM.getUAVersion = function() {
  try{
    return this._private.getParentEPCM().getUAVersion();
  }catch(ex){
    return null;
  }
}
EPCM.getUAPlatform = function() {
  try{  
    return this._private.getParentEPCM().getUAPlatform();
  }catch(ex){
    return null;
  }
}
EPCM.getVersion = function() {
  try{
    return this._private.getParentEPCM().getVersion();
  }catch(ex){
    return null; 
  }	
}
EPCM.getLevel = function() {
  try{
    return this._private.getParentEPCM().getLevel();
  }catch(ex){
    return null;
  }  
}
EPCM.getInstanceId = function() {
  try{
    return this._private.getParentEPCM().getInstanceId();
  }catch(ex){
    return null; 
  }
}
EPCM.getUniqueWindowId = function() {
  try{
    return this._private.getParentEPCM().getUniqueWindowId();
  }catch(ex){
    return null;
  }	  
}

// --------- Proxy for DataBag methods delegate to parent ------------------------
EPCM.storeClientData = function(pUrn, pName, pValue){
 try{
   this._private.getParentEPCM().storeClientData(pUrn,pName,pValue)
 }catch(ex){}

}
EPCM.loadClientData = function(pUrn, pName){
  try{
    return this._private.getParentEPCM().loadClientData(pUrn, pName);
  }catch(ex){
    return null; 
  }  
}
EPCM.deleteClientData = function(pUrn, pName){
  try{
    this._private.getParentEPCM().deleteClientData(pUrn, pName);
  }catch(ex){}  
}

// --------- Proxy for Dirty & Navigation  delegate to parent ---------------------
EPCM.setDirty = function ( val ){
  try{ 
    this._private.getParentEPCM().setDirty(val);
  } catch(ex){} 
}
EPCM.getDirty = function( ){
  try{
    return this._private.getParentEPCM().getDirty();	
  } catch(ex){
    return false;
  } 
}
EPCM.doNavigate = function (pcdurl){
  try{
    return this._private.getParentEPCM().doNavigate(pcdurl);	
  } catch(ex){
    return true;
  } 
}



// --- Eventing distribution logic - full code taken from original EPCM ------

EPCM.EventObject = function( eventName, dataObject, sourceId ){
  this.eventName  = eventName;
  this.dataObject = dataObject;
  this.sourceId   = sourceId;
}




EPCM.subscribeEvent = function( nameSpace, eventName, handlerRef, methodName ){
  // a) define FunctionListener constructor
  function FunctionListener( fref ) {
    this.fref = fref;
    this.call = function ( data ) { return this.fref( data ); }
  }
  // b1) define MethodListener constructor
  function MethodListener( oref, mname ) {
    this.oref   = oref;
    this.method = mname;
    this.call   = function ( data ) { return this.oref[this.method]( data ); }
  }
  // b2) define FrameMethodListener constructor
  function FrameMethodListener ( fname, mname) {
    this.frame  = fname
    this.method = mname;
    this.call   = function ( data ) { return window.frames[this.frame][this.method]( data ); } 	
  } 

  // concatenate name space and event name to single event id
  // ('&' is forbidden inside URNs)
  var lsEvtId = nameSpace + "&" + eventName;

  var loSys   = this._private;

  // create subscription list at the first time
  if (loSys.eList == null) loSys.eList = new Array();
  if (loSys.eList[lsEvtId] == null) loSys.eList[lsEvtId] = new Array();
  var laListener = loSys.eList[lsEvtId];

  var eListener = null;
  // check, if variant a) ,b1) or b2)

  if ( ("function" == typeof handlerRef) && (arguments.length == 3) ) {
    // a) create new function event listener
    eListener = new FunctionListener( handlerRef );

  } else if ( ("object" == typeof handlerRef) && (arguments.length == 4) ) {
    // create new frame-method or method listener
    for(var i=0; i<frames.length;i++){
     if (handlerRef == frames[i]){
       // b1) create frame-method listener (if valid)  
       eListener = new FrameMethodListener(frames[i].name, methodName);	          	
       break; 
     }
    } 
    if (eListener == null){
      // b2) create new method event listener
      eListener = new MethodListener( handlerRef, methodName );
    } 
  } else {
    var polluter = caller;
    alert( "EPCM ERROR\n" +
           "\tIllegal argument type in method EPCM.subscribeEvent()" +
           "\tThe function was called from " + polluter.toString() );
  }
  // add new event listener to subscription list
  laListener[laListener.length] = eListener;

}


EPCM.raiseEvent = function( nameSpace, eventName, dataObject, sourceId ) {

  function distribute( consumers, eventObject ) {
    for (var idx in consumers) {
      // get event handler (should be function or object)
      var eListener = consumers[idx];

//    encapsulate event handler call into try-catch block for JS13
      try {
        // try to call registerd event handler and pass dataObject
        eListener.call( eventObject );
      } catch( e ) {
          /* @todo@ error in event handler */ 
          if (EPCM._private.dbgException){
            if(!confirm("Exception in EPCM.raiseEvent occured.\nContinue ?")) throw(e);
          }
      } 
//    no try-catch for older browsers
    } // for
  } // distribute

  // concatenate name space and event name to single event id
  // ('&' is forbidden inside URNs)
  var lsEvtId   = nameSpace + "&" + eventName;
  var lsEvtAll  = nameSpace + "&*";
  var loEvtObj  = new EPCM.EventObject( eventName, dataObject, sourceId );

  // get all EPCMs object which are reachable
  var laEPCM = this._private.getAdjacentEPCM();

  // iterate over all EPCM objects
  for (var idx = 0; idx < laEPCM.length; idx++) {

    if (laEPCM[idx]._private.eList == null) continue;

      // get array of event handlers for the given event id
      var laListener = laEPCM[idx]._private.eList[lsEvtId];
      if ( laListener != null) {
        distribute( laListener, loEvtObj );
      } // if

      // get array of event handlers which are registered to all events
      laListener = laEPCM[idx]._private.eList[lsEvtAll];
      if ( laListener != null) {
        distribute( laListener, loEvtObj );
      } // if
   } // for
}


EPCM._private.getAdjacentEPCM = function() {
  // find root EPCM and get all child EPCM objects of it.
  var ldTopFrame = this.getTopmostFrame();
  return this.getChildrenEPCM( ldTopFrame );
}


EPCM._private.getTopmostFrame = function(){
  var ldFrame   = window;
  try {
    /* try to walk the frame hierarchy upwards as long as possible */
    while (ldFrame.parent != ldFrame) {
      ldFrame   = ldFrame.parent;
    }
  } catch (ex) { /* ignore, take current frame */ }
  return ldFrame;
}


EPCM._private.getChildrenEPCM = function( froot, paList ) {
  // create EPCM-array on demand
  if (paList == null) paList = new Array();

  // add current frame to result list, if it contains a EPCM object
  if (froot.EPCM != null) paList[paList.length] = froot.EPCM;

  // look for EPCM objects in child frames recursively
  for (var idx = 0; idx < froot.frames.length; idx++) {
    try {
      this.getChildrenEPCM( froot.frames[idx], paList );
    } catch (ex) {
      /* access to frame object denied, stop further search at this sub-frame */
    }
  }
  return paList;
}

EPCM._private.getParentEPCM = function() {
  var currentParent = parent;
  var epcmObject = null;

  try {
    do {
      epcmObject = currentParent.EPCM;
      if ((epcmObject != null) && (epcmObject != undefined)) {
        return epcmObject;
      }
      if (currentParent == top)
        break;
      currentParent = currentParent.parent;
    } while (true);
  }
  catch (ex) {
  }

  return null;
}
//------------------------------------------------------------------------------
// 
// crm_bsp_eventing_proxy.js
//
//------------------------------------------------------------------------------

// ============================================================================
// * Title:        CRM BSP Eventing Proxy
// * --------------------------------------------------------------------------
// * Description: CRM BSP Eventing Proxy object.
// *              Allows CRM BSPs to raise predefined standard events.
// * Version 1.0
// * Copyright (c) SAP AG 2002
// * --------------------------------------------------------------------------
// * Notes:
// *  1) 
// * 
// * History:
// *  1)
// * 
// ============================================================================


var CBEP = {
	nameSpace : "iviewCommunicationEvent"
}

CBEP.getNameSpace = function() {
	return this.nameSpace;
}
CBEP.setNameSpace = function(newNameSpace) {
	this.nameSpace = newNameSpace;
	return true;
}

CBEP.afterSelectionChange = function(objectID) {
	var dataObject = null;
	var eventName = "afterSelectionChange";
  try {
  	if (EPCMPROXY_EX != null) {
  		dataObject =  objectID;
  		EPCMPROXY_EX.raiseEvent(this.nameSpace, eventName, dataObject);
  		
  	}
 	}
 	catch (e) {
 		return null;
 	}
}