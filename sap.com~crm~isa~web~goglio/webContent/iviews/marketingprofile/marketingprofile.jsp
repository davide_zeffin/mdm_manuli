<%--
    Display an marketing profile as an questionary.

    The questionary consists of questions and every question consists of answers.
    The iterate tag is used to loop over questions and answers.

    To every question you found an info message, which gives a short instruction
    how to anwser the questions. The info text will be created dynamic in the
    ShowProfileAction and used only resources with key messages.info.* .
    If there some errors occurs while the user maintains the profile the
    errors will display with the message tag. (Here only resources with key
    message.usererror.* will be used).
--%>
<%--
    @deprecated please use "b2b/marketing/maintainprofile.jsp" instead.
--%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.marketing.*" %>
<%@ page import="com.sap.isa.isacore.action.marketing.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%@ include file="/b2b/messages-declare.inc.jsp" %>


<% BaseUI ui = new BaseUI(pageContext);%>
<% String accessText = ""; %>

<% int attributeCount = 0; %>
<% String[] evenOdd = new String[] { "even", "odd" }; %>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%= ui.getLanguage() %>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="marketing.jsp.ProfileTitle"/></title>
    <isa:stylesheets/> 
<%--    <isa:includes/> --%>
  </head>
<body class="marketingprofile">

  <div class="module-name"><isa:moduleName name="iviews/marketingprofile/marketingprofile.jsp" /></div>
  <h1><isa:translate key="marketing.jsp.ChangeProfile"/> </h1>
  <div id="questions">
  <form method="post" action="<isa:webappsURL name="b2b/changemarketingprofile.do"/>" >

    <jsp:useBean id="questionary"
                 class="com.sap.isa.businessobject.marketing.MktQuestionary"
                 scope="request" />
    <div>
        <input type="hidden" name="AttributeSetKey" value="<%= JspUtil.encodeHtml(questionary.getAttributeSetKey().toString()) %> " />
        <input type="hidden" name="ProfileKey" value="<%= JspUtil.encodeHtml(questionary.getTechKey().toString()) %> " />
    </div>
    <%-- show errors concern the questionary --%>
    <div class="top-message">
    <%@ include file="/b2b/messages-begin.inc.jsp" %>
    <isa:message id="errortext" name="questionary" type="<%=Message.ERROR%>">
	<%
	addMessage(Message.ERROR, errortext);
	%>
	</isa:message>
	<%
	if (!messages.isEmpty())
	{
	%>
      <div class="error">
		<%-- Error Display --%>
		
		<%@ include file="/b2b/messages-end.inc.jsp" %>
		      
      </div>
    <%
	}
	%>
	
    <div class="info">
        <% if (ui.isAccessible) { %>
            <span><span style="color:red">(*)</span> <isa:translate key="marketing.jsp.required.access"/></span><br/>
        <% } else { %>
            <span><span style="color:red">(*)</span> <isa:translate key="marketing.jsp.required"/></span><br/>
        <% } %>
    </div>
      <% if (questionary.isSaved()) {%>
          <div class="info">
              <span><b><isa:translate key="marketing.jsp.saveConfirm"/></b></span>
          </div>
      <% } %>          
	</div>

    <table class="filter-result" border="1" cellspacing="0" cellpadding="3">

      <% int line = 0; %>
      <isa:iterate id="question" name="<%= ShowProfileAction.RK_MKTATTRIBUTE_LIST %>"
                   type="com.sap.isa.businessobject.marketing.MktQuestion">

          <tr class="<%= evenOdd[++line % 2]%>">
            <td>
                <% if (question.getAttribute().isRequired()) {%>
                  <span style="color:red">*</span>
                <% } %>
                
                <%= JspUtil.encodeHtml(question.getAttribute().getDescription()) %>
            <%
              /* Set the current Attribute to pageContext for the iterator tag */
              request.setAttribute("mktanswerlist", question);
              /* Index to Attribute */
              attributeCount++;
            %>
              <i>
                <%-- show infos about the unit or currency of an attribute --%>
                <isa:message id="infotext" name="mktanswerlist" type="<%=Message.INFO%>" property="Unit">
					<span><%=infotext%> <br/></span>
                </isa:message>
              </i>
            </td>
            <td>

            <%-- show errors concern the question --%>
             <isa:message id="errortext" name="mktanswerlist" type="<%=Message.ERROR%>" property="">
             <% if(!errortext.trim().equals(""))
             {
             %>
             	<div class="error">              
				<span><%=errortext%> <br/></span>
                </div>
             <%
             }
             %>
             </isa:message>

            <isa:message id="infotext" name="mktanswerlist" type="<%=Message.INFO%>" property="">
            	<span><%=infotext%> <br/></span>
            </isa:message>
                    

            <%--  save the id and the datatype of the attribute in hidden fields  --%>
            <input type="hidden" name="Attribute[<%= attributeCount %>]" value="<%= question.getAttributeKey().toString() %>" />
            <input type="hidden" name="ChosenValues[<%= attributeCount %>]" value="<%= question.getTechKey().toString() %>" />
            <input type="hidden" name="DataType[<%= attributeCount %>]"  value="<%= question.getAttribute().getDataType() %>" />

			<% int j = 0; %>
            <isa:iterate id="answer" name="mktanswerlist" type="com.sap.isa.businessobject.marketing.MktAnswer">
            <% j++; %>
            <%
              switch(question.getRenderType())
              {
                case MktQuestion.PREDEFINED_VALUES:
                  /*  *** CASE 1:  CHARACTERISTICS WITH LIST OF PREDEFINED VALUES *** */
                  String selectionType;
                  String checked = "";

                  if (question.getAttribute().isMultiple()) {
                    selectionType = "checkbox";
                  }
                  else {
                    selectionType = "radio";
                  }

                  if ( answer.isChosen()) {
                    checked = "checked";
                  }

                  %>
                  
                  <input type="<%= selectionType %>"
                         name="<%= question.getAttributeKey().toString() %>"
                         value="<%= answer.getTechKey().toString() %>"
                         id="<%= question.getAttributeKey().toString() %>_<%= j %>"
                         <% if (checked.length() > 0) { %> 
                             checked="<%= checked %>"
                         <% } %>
                         />
                         <label for="<%= question.getAttributeKey().toString() %>_<%= j %>"> <%= JspUtil.encodeHtml(answer.getDescription()) %> </label> <br/>
                  <%
                  break;
                case MktQuestion.NO_PREDEFINED_VALUES_SINGLE:
                  /* *** CASE 2:  CHARACTERISTICS WITHOUT LIST OF PREDEFINED VALUES, SINGLE-VALUED *** */
                  %>
                  
                  <input type="text" class="textInput"
                         name="<%= question.getAttributeKey().toString() %>"
                         value="<%= JspUtil.encodeHtml(answer.getTechKey().toString()) %>"
                         size="<%= question.getLength() %>"
                         maxlength="<%= question.getLength() %>" />
                  <br/>

                  <%
                  break;
                case MktQuestion.NO_PREDEFINED_VALUES_MULTIPLE:
                  /*** CASE 2b:  CHARACTERISTICS WITHOUT LIST OF PREDEFINED VALUES, MULTIPLE-VALUED ***/

                %>
                  
                  <input type="text" class="textInput"
                         name="<%= question.getAttributeKey().toString() %>"
                         value="<%= JspUtil.encodeHtml(answer.getTechKey().toString()) %>"
                         size="<%= question.getLength() %>"
                         maxlength="<%= question.getLength() %>" />
                  <br/>


                <%
                  break;
              }
              request.setAttribute("answer", answer);
              %>

              <%-- show errors concern the answer --%>
              <% if (answer.getTechKey().toString().length() > 0) { %>
                 <isa:message id="errortext" name="mktanswerlist" type="<%=Message.ERROR%>" property="<%= JspUtil.encodeHtml(answer.getTechKey().toString()) %>" >	
				 <% if(!errortext.trim().equals(""))
	             {
	             %>
	             	<div class="error">              
					<span><%=errortext%> <br/></span>
	                </div>
	             <%
	             }
	             %>
	             </isa:message>
                 
               <%
               }%>

            </isa:iterate>

            <% if (question.getRenderType() == MktQuestion.NO_PREDEFINED_VALUES_MULTIPLE) {
            %>
              <!-- add a button to add more lines for free entries -->
              <label for="CountofFields<%= question.getAttributeKey().toString() %>" >
              <isa:translate key="marketing.jsp.MoreEntries"/>
              </label>
              <select name="CountofFields<%= question.getAttributeKey().toString() %>" id="CountofFields<%= question.getAttributeKey().toString() %>">
                <option selected="selected" value="1"> 1</option>
                <option value="2"> 2</option>
                <option value="3"> 3</option>
                <option value="4"> 4</option>
              </select>

<%
    String changeBtnTxt = WebUtil.translate(pageContext, "marketing.jsp.ChangeFields", null);
%>         	
              <input type="submit" name="More<%= question.getAttributeKey().toString() %>" value="<isa:translate key="marketing.jsp.ChangeFields"/>"
                     title="<isa:translate key="access.button" arg0="<%=changeBtnTxt%>" arg1=""/>"
              />
            <%
              }
            %>
            </td>
          </tr>
      </isa:iterate>
    </table>
<%
    String saveBtnTxt = WebUtil.translate(pageContext, "marketing.jsp.SaveProfile", null);
%>         	
    <table border="0" cellspacing="0" cellpadding="10" width="95%">
      <tr>
      <td align="left">&nbsp;</td>
        <td align="right">
        <input  class="green" type="submit" name="SaveMktProfile" value="<isa:translate key="marketing.jsp.SaveProfile"/>"
        		title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>"
        />

        </td>
      </tr>
    </table>
  </form>
</div>
</body>
</html>