<% /** ToDo:
   **/
%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%  MessageDisplayer messageDisplayer = (MessageDisplayer)request.getAttribute(MessageDisplayer.CONTEXT_NAME);

	boolean isApplicationError = true;

	if (messageDisplayer != null) {
	  isApplicationError = messageDisplayer.isApplicationError();
	} %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
    <head>
    	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="error.jsp.title"/></title>
		<isa:stylesheets/>
    
        <script type="text/javascript">
	        function goBack() {
	            history.back();
	        }
        </script>
    </head>
<body class="workarea">
  
  <div class="module-name"><isa:moduleName name="/iviews/marketingprofile/error.jsp" /></div>
  <div class="error">
	<span>
		<isa:translate key="b2b.prt.marketingprofile.mess1"/> <br/>
		<isa:translate key="b2b.prt.marketingprofile.mess2"/> <br/>
		<isa:translate key="b2b.prt.marketingprofile.mess3"/> <br/>
	</span>
  </div>

</body>
</html>