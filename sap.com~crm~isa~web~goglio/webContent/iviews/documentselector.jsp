<%--
********************************************************************************
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author        Ralf Witt

    $Revision: #1 $
    $DateTime: 2003/09/25 18:19:45 $ (Last changed)
    $Change: 150323 $ (changelist)

********************************************************************************
--%>

<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="java.sql.ResultSetMetaData" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.iview.*" %>
<%@ page import="com.sap.isa.isacore.actionform.iview.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="java.util.*" %>

<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>
<% BaseUI ui = new BaseUI(pageContext);%>
<% String accessText = ""; %>
<%@ include file="/b2b/messages-declare.inc.jsp" %>
<%
   String lastDocType = (String)userSessionData.getAttribute(ReadiViewParameterAction.SC_DOCTYPE);
   if (lastDocType == null) lastDocType = "";
   String docTypeArea = "iview";
   System.out.println("lastDocType: " + lastDocType);
   if (lastDocType.equals(DocumentListiViewForm.QUOTATION)     ||
       lastDocType.equals(DocumentListiViewForm.ORDER)         ||
       lastDocType.equals(DocumentListiViewForm.ORDERTEMPLATE)) {
         docTypeArea = "sales";
   } else if (lastDocType.equals(DocumentListiViewForm.INVOICE)   ||
            lastDocType.equals(DocumentListiViewForm.CREDITMEMO)  ||
            lastDocType.equals(DocumentListiViewForm.DOWNPAYMENT)) {
         docTypeArea = "billing";
   } else {
         docTypeArea = "service";
   }

   Shop shop = (Shop)request.getAttribute(DocumentListAction.RK_DLA_SHOP);

   ResultData List = (ResultData)request.getAttribute(TimoDocumentOverviewListAction.RK_DOCLIST);
   ResultSetMetaData ListLayout = null;
   if (List != null) {
       ListLayout = List.getMetaData();
   }

   String selectionPeriod = (String)userSessionData.getAttribute(DocumentOverviewListAction.SC_SELECTPERIOD);
   String documentsOrigin = (String)request.getAttribute(TimoDocumentOverviewListAction.RC_DOCSORIGIN);

   String orderID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_ORDER_ID);
   String quotationID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_QUOTATION_ID);
   String orderTemplateID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_ORDERTEMPLATE_ID);
   String invoiceID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_INVOICE_ID);
   String creditMemoID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_CREDITMEMO_ID);
   String downPaymentID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_DOWNPAYMENT_ID);
   String serviceRequestID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_SERVICEREQUEST_ID);
   String infoRequestID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_INFOREQUEST_ID);
   String complaintsID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_COMPLAINTS_ID);
   String serviceContractID = (String) userSessionData.getAttribute(ReadiViewParameterAction.SC_SERVICECONTRACT_ID);

   int docCount = 0;
   try {
       docCount = Integer.parseInt((String)request.getAttribute(DocumentListAction.RK_DOCUMENT_COUNT));
   } catch (NumberFormatException NFEx) {
   }

   String portalURL = (String)userSessionData.getAttribute(ReadiViewParameterAction.SC_PORTALURL);
   String portalURLService = (String)userSessionData.getAttribute(ReadiViewParameterAction.SC_PORTALURLSERVICE);


   //String[][] serviceA = (String[][])request.getAttribute(ServiceTypesReadAction.RK_SERVICETYPES);
   //if (serviceA == null) {
       // Set SAP standard values as default
       String[][] serviceA = new String[][] {{"CRMC", "BUS2000120"},
                                  {"S1",   "BUS2000116"},
                                  {"S3",   "BUS2000116"},
                                  {"SC",   "BUS2000112"} } ;
   //}

   boolean backendR3ButNotPI = shop.getBackend().equals(Shop.BACKEND_R3) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_40) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_45) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_46);

   boolean backendR3 = backendR3ButNotPI ||
                       shop.getBackend().equals(Shop.BACKEND_R3_PI);
%>
<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/iview_stylesheet.css" ) %>" type="text/css" rel="stylesheet">

  <script type="text/javascript">
  function gotoDetail(docKey, docType, docId, docOrigin) {
      <% if (docTypeArea.equals("service")) { %>
      var portalurl = "<%=portalURLService%>";
      <% } else {%>
      var portalurl = "<%=portalURL%>";
      <% } %>
      var urlFinal = portalurl;

      if (portalurl.indexOf("?") > 0)
        urlFinal = urlFinal + "&";
      else
        urlFinal = urlFinal + "?";

      var urlFinal = urlFinal  + "prt_iviewscenario=prt_iviewscenario_docoverview"
                               + "&doc_key=" + docKey
                               + "&doc_type=" + docType
                               + "&doc_origin=" + docOrigin
                               + "&doc_id=" + docId;
      // alert("Final url: " + urlFinal);
      // 'parent.' as target overlays the entire workset with the result instead of the iView
      parent.location.href=urlFinal;
  }

  // If a service document has been selected, it is necessary
  // to set the process type too
  function SetServiceBusinessObjectType(serviceProcessType) {
       alert("SetServiceBusinessObjectType");
       var jserviceAsp = new Array(<%=serviceA.length%>);
       var jserviceAbp = new Array(<%=serviceA.length%>);
      <% for (int i = 0; i < serviceA.length; i++) { %>
      jserviceAsp[<%=i%>]="<%=serviceA[i][0]%>";
      jserviceAbp[<%=i%>]="<%=serviceA[i][1]%>";
      <% } %>
      var hiddenBusinessType = document.forms['<%=DocumentListiViewForm.DOCUMENT_TYPE%>'].elements['<%=TimoDocumentOverviewListAction.RC_SERVICEBUSINESSTYPE%>'];
      for (var i = 0; i < jserviceAsp.length; i++) {
          if (jserviceAsp[i] == serviceProcessType) {
              hiddenBusinessType.value = jserviceAbp[i];
          }
      }
  }
  </script>

</head>
  <body onLoad="">
    <isa:moduleName name="b2b/billing/organizer-content-doc-search.jsp" />
      <%-- Selection Screen Area --%>
      <form name="<%=DocumentListiViewForm.DOCUMENT_TYPE%>" method="get" action='<isa:webappsURL name ="iview/documentlistprepare.do"/>' >
        <!--fieldset>
          <legend>Suchkriterien</legend-->
          <table border="0" cellspacing="0" cellpadding="1" width="10%">
            <tbody>
              <tr>
                <td><label id="<%=DocumentListiViewForm.DOCUMENT_TYPE%>Lbl" for="<%=DocumentListiViewForm.DOCUMENT_TYPE%>" title="<isa:translate key="b2b.status.iview.showmeall"/>"><isa:translate key="b2b.status.iview.showmeall"/></label></td>
              </tr>
              <tr>
                <td>
                     <select class="selectDocument" name="<%=DocumentListiViewForm.DOCUMENT_TYPE%>" id="<%=DocumentListiViewForm.DOCUMENT_TYPE%>"
                       onChange="SetServiceBusinessObjectType(this.form.<%= DocumentListiViewForm.DOCUMENT_TYPE %>.options[this.form.<%= DocumentListiViewForm.DOCUMENT_TYPE %>.options.selectedIndex].value);">
                      <option value="<%=DocumentListiViewForm.QUOTATION%>" <%=(lastDocType.equals(DocumentListiViewForm.QUOTATION)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=quotationID%>"/></option>
                      <option value="<%=DocumentListiViewForm.ORDER%>" <%=(lastDocType.equals(DocumentListiViewForm.ORDER)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=orderID%>" /></option>
                      <% if (!backendR3) { %>
                      <option value="<%=DocumentListiViewForm.ORDERTEMPLATE%>" <%=(lastDocType.equals(DocumentListiViewForm.ORDERTEMPLATE)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=orderTemplateID%>" /></option>
                      <% } %>
                      <% if (shop.isContractAllowed()) {%>
                      <%       //docTypeClear = WebUtil.translate(pageContext, "b2b.status.shuffler.key1val4", null); %>
                      <%-- option value="<%=DocumentListSelectorForm.CONTRACT%>"<%=(lastDocType.equals(DocumentListSelectorForm.CONTRACT)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=docTypeClear%>" /></option --%>
                      <% } %>
                      <% if (shop.isBillingDocsEnabled()) {%>
                      <option value="<%=DocumentListiViewForm.INVOICE%>"<%=(lastDocType.equals(DocumentListiViewForm.INVOICE)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=invoiceID%>" /></option>
                      <option value="<%=DocumentListiViewForm.CREDITMEMO%>"<%=(lastDocType.equals(DocumentListiViewForm.CREDITMEMO)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=creditMemoID%>" /></option>
                      <option value="<%=DocumentListiViewForm.DOWNPAYMENT%>"<%=(lastDocType.equals(DocumentListiViewForm.DOWNPAYMENT)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=downPaymentID%>" /></option>
                      <% } %>
                      <option value="<%=DocumentListiViewForm.SERVICEREQUEST%>"<%=(lastDocType.equals(DocumentListiViewForm.SERVICEREQUEST)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=serviceRequestID%>" /></option>
                      <option value="<%=DocumentListiViewForm.INFOREQUEST%>"<%=(lastDocType.equals(DocumentListiViewForm.SERVICEREQUEST)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=infoRequestID%>" /></option>
                      <option value="<%=DocumentListiViewForm.COMPLAINTS%>"<%=(lastDocType.equals(DocumentListiViewForm.COMPLAINTS)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=complaintsID%>" /></option>
                      <option value="<%=DocumentListiViewForm.SERVICECONTRACT%>"<%=(lastDocType.equals(DocumentListiViewForm.SERVICECONTRACT)?"selected":"")%>>&nbsp;<isa:translate key="b2b.status.shuffler.key8val1"  arg0="<%=serviceContractID%>" /></option>
                    </select>
                </td>
              </tr>
              <tr>
                <td align="left"><span><isa:translate key="b2b.status.shuffler.key9val1" arg0="<%=selectionPeriod%>"/></span></td>
              </tr>
<%
    String searchBtnTxt = WebUtil.translate(pageContext, "status.select.searching", null);
%>              
              <tr>
                <td align="left">
                	<input class="button" type="submit" value="<isa:translate key="status.select.searching"/>"
						   title="<isa:translate key="access.button" arg0="<%=searchBtnTxt%>" arg1=""/>"                	
					>
                </td>
              </tr>
            </tbody>
          </table>
          <input type="hidden" name="<%=TimoDocumentOverviewListAction.RC_SERVICEBUSINESSTYPE%>" value=""/>
      </form>

      <%-- List Screen Area --%>

      <% String documentTypeKey = "status.".concat(docTypeArea).concat(".documenttype.").concat(lastDocType);
         if ( docCount == 0  ||  docCount > 1) {
            documentTypeKey = documentTypeKey.concat("s");          // plural
         };
      %>

      <hr size="1" width="100%">

      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr><td colspan="3"><div class="searchresult"><span>
          <%=String.valueOf(docCount)%>&nbsp;<isa:translate key="<%= documentTypeKey %>"/><isa:translate key="status.select.searchresult02"/>
          </span></div></td>
        </tr>
      </table>

      <br>

      <% if (docCount > 0) { %>

      <table width="100%" class="list" align="center" cellpadding="1" cellspacing="1" border="0">
        <thead>
          <tr>
          <% String colNameFinal = ""; %>
          <%--      SALES     --%>
          <% if (docTypeArea.equals("sales")) {
                 if ( ! lastDocType.equals(DocumentListiViewForm.ORDERTEMPLATE) ) {
           %>
            <th scope="col" class="head"><div class="bold"><isa:translate key="b2b.iview.resultlist.STATUS"/></div></th>
            <%   }
                 colNameFinal = "b2b.status.dn.".concat(lastDocType);%>
            <th scope="col" class="head"><div class="bold"><isa:translate key="<%=colNameFinal%>"/></div>
                <div class="bold"><isa:translate key="b2b.status.iview.result.DATE"/></div>
            </th>
            <th scope="col" class="head"><div class="bold"><isa:translate key="b2b.iview.DESCRIPTION"/></div></th>
          <%} %>

          <%--      BILLING     --%>
          <% if (docTypeArea.equals("billing")) {
                 colNameFinal = "b2b.status.dn.".concat(lastDocType);%>
            <th class="head"><div class="bold"><isa:translate key="<%=colNameFinal%>"/></div>&nbsp;</th>
            <%--td bgcolor="#ffffff"><div class="bold"><isa:translate key="status.billing.reference.2lines"/></div></td--%>
            <th scope="col" class="head"><div class="bold"><isa:translate key="b2b.status.iview.GROSSVALUE"/></div></th>
          <%} %>

          <%--      SERVICE     --%>
          <% if (docTypeArea.equals("service")) {
           %>
            <th scope="col"  class="head"><div class="bold"><isa:translate key="b2b.iview.resultlist.STATUS"/></div></th>
               <%colNameFinal = "status.service.documenttype.".concat(lastDocType);%>
            <th scope="col"  class="head"><div class="bold"><isa:translate key="<%=colNameFinal%>"/></div>
                <div class="bold"><isa:translate key="b2b.status.iview.result.DATE"/></div>
            </th>
            <th scope="col" class="head"><div class="bold"><isa:translate key="b2b.iview.DESCRIPTION"/></div></th>
          <%} %>
          </tr>
        </thead>
      <% String even_odd = "odd"; %>

      <isa:iterate id="list" name="<%= TimoDocumentOverviewListAction.RK_DOCLIST %>"
                     type="com.sap.isa.core.util.table.ResultData">
          <% if (even_odd.equals("odd")) {
                  even_odd = "even";
                } else {
                  even_odd = "odd";
                }
          %>
          <% String finalColValue = ""; %>
        <tbody>
          <tr>

          <%--      SALES     --%>
          <% if (docTypeArea.equals("sales")) {
                 if ( ! lastDocType.equals(DocumentListiViewForm.ORDERTEMPLATE) ) {%>
            <% finalColValue = "status.sales.status.".concat(list.getString("STATUS")); %>
            <td class="<%= even_odd %>"><isa:translate key="<%=finalColValue%>"/></td>
            <%   }  %>
            <td class="<%= even_odd %>"><a href="#" onClick="gotoDetail('<%=list.getRowKey().toString()%>','<%=lastDocType%>','<%=list.getString("DOCID")%>','<%=documentsOrigin%>');" ><%=list.getString("DOCID")%></a><br>
                <%=list.getString("DATE")%>
            </td>
            <td class="<%= even_odd %>"><%=list.getString("DESCRIPTION")%></td>
          <%} %>

          <%--      BILLING     --%>
          <% if (docTypeArea.equals("billing")) {
           %>
            <td class="<%= even_odd %>"><a href="#" onClick="gotoDetail('<%=list.getRowKey().toString()%>','<%=lastDocType%>','<%=list.getString("DOCID")%>','<%=documentsOrigin%>');" ><%=list.getString("DOCID")%></a><br>
                <div class="bold"><isa:translate key="status.billing.list.from"/>:&nbsp;</div><%=list.getString("DATE")%> </br>
                <div class="bold"><isa:translate key="b2b.status.iview.result.DUEDATE"/>:&nbsp;</div><%=list.getString("DUEDATE")%>
            </td>
            <%--td><div class="bold"><%=list.getString("DESCRIPTION")%></div></font></td--%>
            <td class="<%= even_odd %>" align="right"><div class="bold"><%=list.getString("GROSSVALUE")%>&nbsp;<%=list.getString("CURRENCY")%></div></td>
          <%} %>

          <%--      SERVICE     --%>
          <% if (docTypeArea.equals("service")) { %>
            <td class="<%= even_odd %>"><%=list.getString("STATUS").toLowerCase()%></td>
            <td class="<%= even_odd %>"><a href="#" onClick="gotoDetail('<%=list.getRowKey().toString()%>','<%=lastDocType%>','<%=list.getString("DOCID")%>','<%=documentsOrigin%>');" ><%=list.getString("DOCID")%></a><br>
                <%=list.getString("DATE")%>
            </td>
            <td class="<%= even_odd %>"><div class="bold"><%=list.getString("DESCRIPTION")%></div></font></td>
          <%} %>

          </tr>
        </tbody>
      </isa:iterate>
      </table>

      <% } // end of docCount > 0 %>
</body>
</html> 
