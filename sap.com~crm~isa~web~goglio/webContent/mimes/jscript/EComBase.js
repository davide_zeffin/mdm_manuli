/*----------------------------------------------------*/
/* Javascript for the "SAP ECommerce Applications"    */
/*                                                    */
/* Document:       EComBase.js                        */
/* Description:    Base Functionality                 */
/* Version:        1.0                                */
/* Author:         SAP                                */
/* Creation-Date:  23.01.2004                         */
/* Last-Update:    23.01.2004                         */
/*                                                    */
/*                           (c) SAP AG,              */
/*----------------------------------------------------*/


var isOldOpera = getIsOldOpera();
var isToggleSupported = !isOldOpera;


/*  Open popup window for the given url  */
function openWinExtended(url, width, height) {
    var sat = window.open( url,'sat_details',
                           'width='+ width + ',height='+ height +
                           ', menubar=no, locationbar=no'+
                           ',scrolling=auto,resizable=yes');
    sat.focus();
}


/*  Open popup window for the given url  */
function openWin(url) {
    openWinExtended( url,'400','230');
}


/* Return if the browser is Opera version < 7 */
/* Because Opera V6 cannot change style.display */
function getIsOldOpera() {

    var old_opera = false;

    var agt = navigator.userAgent;

    if (agt.indexOf("Opera") != -1) {

        if (agt.indexOf("Opera 6") != -1 || agt.indexOf("Opera/6") != -1) {
            old_opera = true;
        }    
        if (agt.indexOf("Opera 5") != -1 || agt.indexOf("Opera/5") != -1) {
            old_opera = true;
        }
        if (agt.indexOf("Opera 4") != -1 || agt.indexOf("Opera/4") != -1) {
            old_opera = true;
        }    
        if (agt.indexOf("Opera 3") != -1 || agt.indexOf("Opera/3") != -1) {
            old_opera = true;
        }    
        if (agt.indexOf("Opera 2") != -1 || agt.indexOf("Opera/2") != -1) {
            old_opera = true;
        }    

    }

    return old_opera;
}



/* Return a pointer of an element independ from browser version */
function getElement(idName) {

    var element;

    if (document.all) {
        element = document.all(idName);
    }
    else if(document.getElementById) {
        element = document.getElementById(idName);
    }

    return element;
}

/* Return true, if the area is opened, else false */
function isaEasyToggle(id) {
    return isaToggle(id,id+'open', id+'close');
}


/* return true, if the area is opened, else false */
function isaToggle(idName, openIconId, closerIconId) {

    var opera = isOldOpera;
    var open  = false;

    if (document.all && !opera) {
        // the world of the IEs
        openIcon = document.all(openIconId);
        closeIcon = document.all( closerIconId);
        target = document.all(idName);

        if(target.style.display == "none") {
            open = true;
            target.style.display = "";
            openIcon.style.display = "none";
            closeIcon.style.display = "";
        }
        else {
            target.style.display = "none";
            openIcon.style.display = "";
            closeIcon.style.display = "none";
        }
    }

    else if(document.getElementById && !opera) {
        // the world of W3C without Opera
        openIcon = document.getElementById(openIconId);
        closeIcon = document.getElementById(closerIconId);
        target = document.getElementById(idName);

        if(target.style.display == "none") {
            open = true;
            target.style.display = "";
            openIcon.style.display = "none";
            closeIcon.style.display = "";
        }
        else {
            target.style.display = "none";
            openIcon.style.display = "";
            closeIcon.style.display = "none";
        }
    }
    else {
        // handle NS4.7 and old opera
        alert("ServerRequest");
    }

    return open;
}



function hideElement(idName) {

    var element = getElement(idName);

    if (element != null) {
        element.style.display = "none";
    }
}


function showElement(idName) {

    var element = getElement(idName);

    if (element != null) {
        element.style.display = "";
    }
}


function deleteText(idName) {

    var element = getElement(idName);

    if (element !=null ) {
        var child = element.firstChild;
        child.data= "";
    }
}


function addText(idName, text) {

    var element = getElement(idName);

    if ( element!=null ) {
        var child = element.firstChild;
        var textData = document.createTextNode(text);
        child.appendData(textData.nodeValue);
    }
}


function setText(idName, text) {

    var element = getElement(idName);

    if (element!= null) {
        var child = element.firstChild;
        child.data = text;
    }

}


function closeToggleArea(idName) {
    var opera = isOldOpera;

    if(document.all && !opera) {
        openIcon = document.all(idName + "open");
        closeIcon = document.all(idName + "close");
        target = document.all(idName);

        if(target.style.display == "inline") {
            target.style.display = "none";
            openIcon.style.display = "inline";
            closeIcon.style.display = "none";
        }
    }
}


/** Call the help value search with name "name"   */
function getHelpValues(name, formName, index) {

  var formObject = getElement(formName);

  if (formObject != null) {
      if (formObject.helpValuesSearch != null) {
          formObject.helpValuesSearch.value = name;
      }
      if (formObject.parameterIndex != null) {
          formObject.parameterIndex.value = index;
      }

    formObject.submit();
  }


}


function isNav() {

    var agt=navigator.userAgent.toLowerCase();
    
    // Note: Opera and WebTV spoof Navigator.  We do strict client detection.
    // If you want to allow spoofing, take out the tests for opera and webtv.
    return ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)
                && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)
                && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1));
}


function prepareKeyHandler() {

    var is_major = parseInt(navigator.appVersion);

    if (isNav() && (is_major < 5)) {
        //Only necessary for Netscape versions < 5
       document.captureEvents(Event.KEYPRESS);
       document.onkeypress = checkReturnKeyPressed;
    }

}


// check if the given key was pressed
function checkKeyPressed(evt, keyCode) {

    var pressedKeyCode;

    if (isNav()) {
        //Netscape
        pressedKeyCode = evt.which;
    }
    else {
        //Microsoft
        pressedKeyCode = window.event.keyCode;
    };

    if (pressedKeyCode == keyCode) {
        return true;
    };

    return false
}


// check if the return key was pressed
function checkReturnKeyPressed(evt){

    return checkKeyPressed(evt, '13');
}

// Display the waiting screen
// The parameter 'hideDiv' is optional.
function displayBusy(hideCont) {
    if (!hideCont) {
        hideCont = "page";
    }
    var hideScreen = getElement(hideCont);
    var dispScreen = getElement("busy");   
    if (hideScreen) {
	    hideScreen.style.display = "none";
    	dispScreen.style.display = "";
    }	
} 
    
	function displayBusyOverlay(styleClass) {
		 		
 		var body = document.getElementsByTagName("body")[0];
   		var node = document.createElement('div');
        	
       	node.id        = 'IsaOverlayShadowObject';                   
		node.classname = styleClass;

		body.appendChild(node);        		                     
    }    

