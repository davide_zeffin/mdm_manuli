/*----------------------------------------------------*/
/* Javascript for the "SAP ECommerce Applications"    */
/*                                                    */
/* Document:       EComLogin.js                       */
/* Description:    Offers a login functionality       */
/* Version:        1.0                                */
/* Author:         SAP                                */
/*                           (c) SAP AG,              */
/*----------------------------------------------------*/

  function getTopWindow(frameName) {

      var tmpWnd = window;

	  try {	
	      while (tmpWnd.parent != tmpWnd && tmpWnd.name != frameName) {
    	       tmpWnd = tmpWnd.parent;
	      }
    	  tmpWnd=tmpWnd.parent;
      }	  
	  catch (e) {
	  	 // this can happen because of domain relaxation!	  
	  }	      
	  
      return tmpWnd;
  }
 
  function loginInTopFrame(frameName, loginUrl) {

      var tmpWnd = getTopWindow(frameName);

      tmpWnd.location.href = loginUrl;
  }
