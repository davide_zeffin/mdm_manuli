<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.helpvalues.action.*" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.*" %>
<%@ page import="com.sap.isa.helpvalues.*" %>
<%@ page import="java.util.*" %>

<% String[] evenOdd = new String[] { "even", "odd" }; %>

<%
HelpValuesUI ui = new HelpValuesUI(pageContext);
%>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  xml:lang="en" lang="<%=ui.getLanguage()%>">

<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

  <isa:stylesheets/>

  <title><isa:translate key="helpValues.jsp.title"/></title>

  <script type="text/javascript"
          src="<%=WebUtil.getMimeURL(pageContext, "/mimes/jscript/EComBase.js")%>">
  </script>


  <script type="text/javascript">

    <%
    if (ui.isJavascriptTransfer()) { %>
      function selectValue (<%= ui.getJSParameterNameList() %>) {

          var formObject = opener.document.forms["<%=ui.getFormName()%>"];
          var fieldObject;

          <%
            for (int i = 0; i < ui.getJSParameterCount(); i++) { %>
              fieldObject = formObject.elements["<%=ui.getJSParameter(i)%><%=ui.getParameterSuffix()%>"];
              if (fieldObject != null) {
                fieldObject.value = <%=ui.getJSParameterName(i)%>;
              }
          <%
          }%>
          window.close();
      }

      function cancelSearch() {
        window.close();
      }

    <%
    }
    else { %>
      function selectValue (href) {

        window.location.href = href;
      }

      function cancelSearch() {

        var href = "<isa:webappsURL name="/base/helpvalues/getForward.do">
                      <isa:param  name="forward"  value="<%=ui.getActionForReturn()%>" />
                   </isa:webappsURL>";

        window.location.href = href;
      }
      <%
    }%>

    function searchIfReturnKeyIsPressed(evt) {

      if (checkReturnKeyPressed(evt)) {
        startSearch();
      }
    }

    function startSearch() {
      displayBusy("help-values-search-content");
      document.forms["search"].submit();
    }
    
   <%
   if (ui.isDisplayInputValues() ) { 
    	Iterator iter = ui.getHelpValuesSearch().getParameterIterator(); %>
  	function clearFields() {
        var formObject = document.forms["search"];
        var fieldObject;
        <%
        while (iter.hasNext()) {
      	HelpValuesSearch.Parameter parameterIn = (HelpValuesSearch.Parameter)iter.next();  
          if (parameterIn.isInputParameter() && !parameterIn.isHidden()) { %>
            fieldObject = formObject.elements["<%=parameterIn.getName() + ui.getParameterSuffix() %>"];
            if (fieldObject != null) {
              fieldObject.value = "";
            }
        <%
          }
        } %>
      }   
    <%    
    } %>

    
  </script>
  
  

</head>

<%
if (ui.isJavascriptTransfer()) { %>
  <body class="help-values-search-popup" onkeypress="searchIfReturnKeyIsPressed(event)">
<%
}
else { %>
  <body class="help-values-search" onkeypress="searchIfReturnKeyIsPressed(event)">
<%
} %>

  <div class="module-name"><isa:moduleName name="helpvalues/displayhelpValues.jsp"/></div>

  <% ui.includeHeader(); %>
  
  <%! MessageList messages = new MessageList();
      Message message = null;
      private void addMessage(int level, String text) {
          if (null != text && text.trim().length() > 0) {
              message = new Message(level, text);
              messages.add(message);
          }
      }
  %>

  <%@ include file="/appbase/busy.inc.jsp" %>

  <div id="help-values-search-content">
    <h1><isa:translate key="helpValues.jsp.title"/></h1>
    
    <form id="search" method="get" action="<isa:webappsURL name="/base/helpvalues.do"/>" >
      <div>
      <input type="hidden" name="helpValuesSearch" value="<%=ui.getHelpValuesSearch().getName()%>" />
      <input type="hidden" name="<%=HelpValuesConstants.PN_FORM_NAME%>" value="<%=ui.getFormName()%>" />
      <input type="hidden" name="<%=HelpValuesConstants.PN_PARAMETER_INDEX%>" value="<%=ui.getAdditionalIndex()%>" />
      <input type="hidden" name="<%=HelpValuesConstants.PN_RETURN_ACTION%>" value="<%=ui.getActionForReturn()%>" />
      <%= ui.addHiddenFields() %>
    </div>

  	<%-- Message Handling --%>
  	<% if (ui.hasMessages()) { %>

        <% messages = new MessageList(); message = null; %>
    
    	<isa:message id="errortext" name="helpValuesObject" type="<%=Message.ERROR%>"  ignoreNull="true">
        	<% addMessage(Message.ERROR, errortext); %>
    	</isa:message>

    	<isa:message id="errortext" name="helpValuesObject" type="<%=Message.WARNING%>"  ignoreNull="true">
        	<% addMessage(Message.ERROR, errortext); %>
    	</isa:message>
    
    	<% if(!messages.isEmpty()) { %>
        	<%@ include file="messages-end.inc.jsp" %>
    	<% } %>
    
 	 <% } %>
  
      <%
      if (ui.isDisplayInputValues() ) { %>
        <div class="input-values"> <!-- level: sub2 -->
          <input type="hidden" name="<%=HelpValuesConstants.PN_SEARCH %>" value="true" />

          <%
          if (ui.isShowResult()) { %>
            <a id="access-input" href="#access-search" title="<isa:translate key="helpvalues.inputFields"/>" accesskey="<isa:translate key="helpvalues.inputFields.key"/>"></a>
          <%
          }%>

          <a href="#access-buttons" title="<isa:translate key="helpvalues.inputFields.skip"/>"></a>


          <table class="data" summary="">
            <isa:iterate id="parameter" name="parameterIterator" type="com.sap.isa.helpvalues.HelpValuesSearch.Parameter">
              <%
              if (parameter.isInputParameter() && !parameter.isHidden()) { %>
                <tr>
                  <% if (ui.useResourceKeys()) { %>
                    <td class="identifier"><isa:translate key="<%=parameter.getDescription()%>"/>:</td>
                  <% } else { %>
                    <td class="identifier"><%=parameter.getDescription()%>:</td>
                  <% } %>
                  <td class="value"><input type="text" name="<%=parameter.getName() + ui.getParameterSuffix() %>" value="<%=JspUtil.encodeHtml(parameter.getValue())%>" /></td>
                </tr>
              <%
              } %>
            </isa:iterate>
            <tr>
              <td class="identifier"><isa:translate key="helpValues.jsp.maxRows"/>:</td>
              <td class="value"><input type="text" name="<%=HelpValuesConstants.PN_MAX_ROWS %>" value="<%=JspUtil.encodeHtml(ui.getMaxRow())%>" /></td>
            </tr>
          </table>
        </div>

        <div class="search-buttons">
           <a id="access-buttons" href="#access-results" title="<isa:translate key="helpvalues.buttons"/>" accesskey="<isa:translate key="helpvalues.buttons.key"/>"></a>
           <ul class="buttons-1">
              <li><a href="javascript:startSearch();"  <% if (ui.isAccessible) { %> title="<isa:translate key="helpValues.jsp.search"/>" <% } %> ><isa:translate key="helpValues.jsp.search"/></a></li>
              <li><a href="javascript:clearFields();" <% if (ui.isAccessible) { %> title="<isa:translate key="helpValues.jsp.clear"/>" <% } %> ><isa:translate key="helpValues.jsp.clear"/></a></li>
              <li><a href="javascript:cancelSearch();" <% if (ui.isAccessible) { %> title="<isa:translate key="helpValues.jsp.cancel"/>" <% } %> ><isa:translate key="helpValues.jsp.cancel"/></a></li>
          </ul>
        </div>
      <%
      }%>

      <%
      if (ui.getHelpValues() != null) { %>

        <div class="search-result"> <!-- level: sub2 -->
          <div>
          <%if (ui.isOverFlow()) { %>
             <div class="error2">
              <span><isa:translate key="helpValues.jsp.moreThenMax" arg0="<%=ui.getMaxRow()%>"/></span>
          <%
          }
          else { %>
             <div class="info2">
             <% if (ui.useResourceKeys()) { %>
                  <span><isa:translate key="<%=ui.getHelpValues().getDescription()%>"
                                         arg0="<%= ui.getNumRows()%>"/></span>
             <% } else { %>
             <% if (ui.getHelpValues().getDescription().length() > 0) { %>
                  <span><%=ui.getHelpValues().getDescription()%> (<%= ui.getNumRows()%>)</span>
             <% } %>
             <% } %>
          <%
          } %>
            </div>
          </div>


          <%
          if (ui.isShowResult()) {
            String tableTitle = ui.getAccessTableSummary("helpvalues.searchResults.title",
                                                       ui.getHelpValues().getValues().getNumRows(),
                                                       ui.getColumnNames().size());

            if (ui.isDisplayInputValues()) { %>
              <a id="access-results" href="#access-input" title="<isa:translate key="helpvalues.searchResults"/>" accesskey="<isa:translate key="helpvalues.searchResults.key"/>"></a>
              <a href="#access-results-end" title="<isa:translate key="helpvalues.searchResults.skip"/>" ></a>
            <%
            }
            else { %>
              <a id="access-results" href="#access-buttons" title="<isa:translate key="helpvalues.searchResults.only"/>" accesskey="<isa:translate key="helpvalues.searchResults.key"/>"></a>
            <%
            } %>


            <table class="itemlist" summary="<%=tableTitle%>">
              <%
              int line = 0; %> 
              <tr>
                <% for (int j = 0; j < ui.getLabelNames().size(); j++) {
                  if (ui.isColumnVisible(j)) { %>
                  <% if (ui.useResourceKeys()) { %>
                    <th class="hlpval-title"><isa:translate key="<%=(String)ui.getLabelNames().get(j)%>"/></th>
                    <% } else { %>
                    <th class="hlpval-title"><%=(String)ui.getLabelNames().get(j)%></th>
                    <% } %>
                  <% 
                    }
                  } %>
              </tr>

              <%-- Set the resultData object to pageContext for the iterator tag --%>
              <%
              request.setAttribute("resultData", ui.getHelpValues().getValues());
              %>
              <isa:iterate id="resultData" name="resultData"
                           type="com.sap.isa.core.util.table.ResultData">

                <tr class="<%= evenOdd[++line % 2]%>">
                 <%
                 for (int j = 0; j < ui.getColumnNames().size(); j++) {
                  if (ui.isColumnVisible(j)) { %>
                    <td>
                      <% if (!ui.getColumnValue(resultData,j).equals("")) { %>
                          <%
                          if (ui.isJavascriptTransfer()) { %>
                            <a href="javascript:selectValue(<%=ui.createJSParameter(resultData)%>
                          <%
                          }
                          else { %>
                           <a href="javascript:selectValue('<isa:webappsURL name="/base/selecthelpvalue.do">
                                  <%
                                  List requestParameter = ui.createParameter(resultData);
                                  for (int i = 0; i < requestParameter.size(); i++) {
                                    String[] parameter = (String[]) requestParameter.get(i);
                                    %>
                                    <isa:param  name="<%=parameter[0]%>"  value="<%=parameter[1]%>" />
                                  <%
                                  }
                                  %>
                                 </isa:webappsURL>'
                          <%
                          }%> )">
                           <%=ui.getColumnValue(resultData,j) %></a>
                      <% } else { %>
                        &nbsp;
                      <% } %>
                    </td>
                    <%
                    }
                  } %>
                </tr>
              </isa:iterate>

            </table>

          <%
          }%>

        </div>
        <%
        }%>

        <%
        if (ui.isDisplayInputValues()) {
          if (ui.isShowResult()) {%>
          <div>
            <a id="access-results-end" href="#access-input" title="<isa:translate key="helpvalues.searchResults.end"/>" ></a>
          </div>
          <%
          }
        }
        else { %>
          <div id="buttons">
            <a id="access-buttons" href="#access-results" title="<isa:translate key="helpvalues.buttons"/>" accesskey="<isa:translate key="helpvalues.buttons.key"/>"></a>
            <ul class="buttons-1">
              <li><a href="javascript:cancelSearch();" <% if (ui.isAccessible) { %> title="<isa:translate key="helpValues.jsp.cancel"/>" <% } %> ><isa:translate key="helpValues.jsp.cancel"/></a></li>
            </ul>
          </div>
        <%
        }%>

    </form>
  </div>
</body>
</html>