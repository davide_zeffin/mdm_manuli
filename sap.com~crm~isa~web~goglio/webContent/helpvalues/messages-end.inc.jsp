<%
if (!messages.isEmpty()) 
{ 
java.util.Iterator messagesIterator = messages.iterator();
int messagesCounter = 0;
String messageSize = ""+messages.size();
%>
	<%
	// switch
	if (ui.isAccessible())
	{
	%>
	    <!-- Accesskey for the error section. It must be an resourcekey, so that it can be translated. -->
		<a id="messages" href="#messages-list-start" accesskey="<isa:translate key="access.messages.key"/>" title="<isa:translate key="access.messages.header" arg0="<%=messageSize%>" arg1=""/>">
		<isa:translate key="access.messages.header" arg0="<%=messageSize%>" arg1=""/>
		</a> 
		<br />
		<a class= "messageText" name="messages-list-start" href="#messages-list-end" title="<isa:translate key="access.messages.start" arg0="<%=messageSize%>" arg1=""/>"
		onkeypress="if(event.keyCode=='115') {getElement('messages-list-end').focus();}"
		>
		<isa:translate key="access.messages.start" arg0="<%=messageSize%>" arg1=""/>
		</a> 
		<br />
	<%
	} 
	// switch
	%>
        <%
        String messageTitle = "access.messages.info";
        String messageColor = "black";
		String messageClass = "info";
        String messageLink = "";
        String nextLink = "";
        while (messagesIterator.hasNext())
        {
            message = (Message)messagesIterator.next();
            messageLink = "m"+messagesCounter++;
            if (Message.ERROR == message.getType())
            {
                messageColor = "red";
				messageClass = "error";
                messageTitle = "access.messages.error";
            }
            if (Message.WARNING == message.getType())
            {
                messageColor = "green";
				messageClass = "warn";
                messageTitle = "access.messages.warning";
            }
            if (Message.INFO == message.getType())
            {
                messageColor = "green";
				messageClass = "info";
                messageTitle = "access.messages.info";
            }
            if (Message.SUCCESS == message.getType())
            {
                messageColor = "black";
				messageClass = "info";
                messageTitle = "access.messages.info";
            }
            if (messagesIterator.hasNext())
            {
                nextLink = "m"+messagesCounter;
            }
            else if (ui.isAccessible())
            {
                nextLink = "messages-list-end";
            }
            else
            {
                nextLink = "";
            }
            // display message depend on ui switch
            // switch
			if (ui.isAccessible())
			{
			%>
				<div nowrap>
				- <a class= "<%=messageClass%>" id="<%=messageLink%>" name="<%=messageLink%>" href="#<%=nextLink%>" title="<isa:translate key="<%=messageTitle%>" arg0="<%=message.getResourceKey()%>" arg1=""/>">
				<font color="<%=messageColor%>">
				<span><%=message.getResourceKey()%></span>
				</font>
				</a>
				</div>
			<%
			}
			// switch
			else
			{
			%>
				<div class= "<%=messageClass%>">
				<span><%=message.getResourceKey()%></span>
				</div>
			<%
			}
        }
        %>
<%
// switch
if (ui.isAccessible())
{
%>
        <a class= "messageText" id="messages-list-end" name="messages-list-end" href="#messages-list-start" title="<isa:translate key="access.messages.end"/>">
        <img src="<%=WebUtil.getMimeURL(pageContext, "/b2c/mimes/images/spacer.gif") %>" alt="<isa:translate key="access.messages.end"/>" border="0" width="1" height="1">
        </a>
<%
}
// switch
%>
<%
// focus the error
// switch
if (ui.isAccessible())
{
%>
    <script 
    	type="text/javascript"
 		language="JAVAScript" 
    >
    getElement('messages').focus();
    </script>
<%
}
// switch
// end focus
%>
<%
}
messages.clear();
%>
<!-- end messages -->    
<!-- From here the normal document continues -->
<!-- ERRRORS -->
