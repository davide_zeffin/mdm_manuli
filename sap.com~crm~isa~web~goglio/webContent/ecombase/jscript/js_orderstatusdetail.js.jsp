/*----------------------------------------------------*/
/* Javascript for JSP ecombase/orderstatusdetail.jsp  */
/*                                                    */
/* Version:        1.0                                */
/* Author:         SAP AG                             */
/*                                                    */
/*                           (c) SAP AG, Walldorf     */
/*----------------------------------------------------*/
<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase" %>
<%@ page import="com.sap.isa.isacore.ManagedDocumentLargeDoc" %>
<%@ taglib uri="/isa" prefix="isa" %>

      var NS4 = (document.layers) ? true : false;

      function requestConfirmation(processType) {
            var confirmMsg = "<isa:UCtranslate key="b2b.ord.conf.basket.plain"/>";

            return confirm(confirmMsg);
      }

      function orderQuotation(processType) {
          var choice;
          var missingChoiceMsg = "<isa:UCtranslate key="status.sales.detail.selmis"/>";
        
          var request = "<isa:webappsURL name ="/b2b/documentstatusorderquot.do"/>";
          var confirmed = false;
        
          if (processType) {         // Has process type being passed through?
              // YES
              confirmed = requestConfirmation(choice);
          } else {
              //  NO -> pick it from select box!
              for (i=0; i < document.getElementById('processtypesSID').length; i++) 
                  if(document.getElementById('processtypesSID').options[i].selected == true) 
                    choice = document.getElementById('processtypesSID').options[i].value;
            
              switch (choice) {
                case 'no_choice': alert(missingChoiceMsg);
                  break;
                default: confirmed = requestConfirmation(choice);
                  processType = choice;
                  break;
              }
          }
          if (confirmed) {
              request = request + "?processtype=" + processType;
              myTop = form_input();
              myTop.location.href = request;
          }
          
      }
      
      function transferToOrderChange() {
            if (checkMarked()) {
                document.order_positions.target="form_input";
                document.order_positions.action="<isa:webappsURL name ="/b2b/changeorder.do"/>";
                document.order_positions.submit();
            }
            else {
                alert("<isa:UCtranslate key="b2b.status.noitemstochange"/>");
            }
      }

      function changeDocument(what) {
          var changeAllowed = parent.documents.editableDocumentAlert('<isa:UCtranslate key="b2b.onedocnav.onlyonedoc"/>\n\n<isa:UCtranslate key="b2b.onedocnav.onlyonedoc2"/>\n\n<isa:UCtranslate key="b2b.onedocnav.onlyonedoc3"/>')
          if (! changeAllowed) {
              return;
          }
          var request = "<isa:webappsURL name="/b2b/changeorder.do"/>";
          if (what == 'HEADONLY') {
            request = request + "?changeHeadOnly=true";
          }
         var myTop = form_input();
         myTop.location.href = request;
      }
      
      function transferToBasket() {
            warnTextCopy();
            if (checkMarked()) {
                document.order_positions.target="form_input";
                document.order_positions.submit();
            }
      }
      // Function sends an alert box in case copy includes any texts
      function warnTextCopy() {
            if (!NS4) {
                var inputs =  document.getElementsByTagName('input');
                var reqConfirmation = false
                for (var i=1; i<=inputs.length;i++) {
                  if ((inputs[i] != null) && (inputs[i].type == 'checkbox') && (inputs[i].checked) && (inputs[i].name != 'cball')) {
                      // Line marked, get index to find comment
                      var iname = inputs[i].name;
                      var part = iname.substr(iname.indexOf('['), iname.length);
                      // Build comment name 'comment[1]'
                      var commentname = 'comment' + part;
                      
                      if (document.forms['order_positions'].elements[commentname]) {
                          var comment = document.forms['order_positions'].elements[commentname];
                          if (comment != null && typeof comment != 'undefined' &&  comment.value != "") {
                              reqConfirmation = true;
                          }
                      }
                  }
                }
                if (reqConfirmation == true) {
                    // Inform user about text copy
                    var warnMsg = "<isa:UCtranslate key="b2b.status.detail.copytext.msg1"/>\n<isa:UCtranslate key="b2b.status.detail.copytext.msg2"/>";
                    alert(warnMsg);
                }
            }
      }

      function print() {
         var myTop = form_input();
         myTop.location.href = '<isa:webappsURL name="b2b/statusdocumentprint.do"/>';
      }

      function closeDocument() {
                var myTop = form_input();
                myTop.location.href = '<isa:webappsURL name="b2b/statusdocumentclose.do"/>';
      }

      function cbtoggle() {
          if (document.order_positions.cball.checked) {
                if (document.getElementById("LItransferbutton")) {
                    document.getElementById("LItransferbutton").className="";
                }
                markAll();
          }
          else {
                if (document.getElementById("LItransferbutton")) {
                    document.getElementById("LItransferbutton").className="disabled";
                }
                unmarkAll();
          }
      }

      function markAll() {
          var inputs = getElementsByType('order_positions','checkbox');
          for (var i=1; i<inputs.length;i++) {
            if (inputs[i].type == 'checkbox')
              inputs[i].checked = true;
         }
      }

      function unmarkAll() {
          var inputs = getElementsByType('order_positions','checkbox');
          for (var i=1; i<inputs.length;i++) {
            if (inputs[i].type == 'checkbox')
              inputs[i].checked = false;
         }
      }

      function checkMarked() {
          // NO NS4 support : var inputs = document.getElementsByTagName('input');
          var inputs = getElementsByType('order_positions','checkbox');
          checkedItem = false;
          for (var i=1; i<=inputs.length;i++) {
            if ((inputs[i] != null) && (inputs[i].type == 'checkbox') && (inputs[i].checked))
              checkedItem = true;
          }
          return checkedItem;
      }

      function trackwin(sURL) {
          tw = window.open(sURL,"Tracking","width=670,height=600,left=400,top=100,resizable=yes,scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no");
          tw.focus();
      }

      function itemconfig(itemId) {
          form_input().location.href = "<isa:webappsURL name='/b2b/inititemconfiguration.do'/>?<%=ActionConstantsBase.PARAM_ITEMID%>=" + itemId;
          return true;
      }
      
      //Grid link in Order Status
      function griditemconfig(itemId) {
          form_input().location.href = "<isa:webappsURL name='/b2b/initgriditemconfig.do'/>?<%=ActionConstantsBase.PARAM_ITEMID%>=" +itemId +"&configtype=G";
          return true;
	  }


      function showShipTo(linekey) {
          var url = "<isa:webappsURL name="/b2b/showshipto.do"/>?<%=ActionConstantsBase.PARAM_SHIPTO_TECHKEY%>=" + linekey;
          var sat = window.open(url, 'sat_details', 'width=450,height=450,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
          sat.focus();
          return true;
      }

      // Text handling remark box
      function toggleremark() {
          openIcon = document.getElementById("remark_open");
          closeIcon = document.getElementById("remark_close");
          remarkarea = document.getElementById("remark");

              if (openIcon.style.display == "inline") {
              openIcon.style.display = "none";
              closeIcon.style.display = "inline";
              remarkarea.rows=10;
          }else {
              openIcon.style.display = "inline";
              closeIcon.style.display = "none";
              remarkarea.rows=1;
          }
      }

      function openremark() {
          openIcon = document.getElementById("remark_open");
          closeIcon = document.getElementById("remark_close");
          remarkarea = document.getElementById("remark");

          if (openIcon.style.display == "inline") {
              openIcon.style.display = "none";
              closeIcon.style.display = "inline";
              remarkarea.rows=10;
          }
      }
      
      function toggle(idName) {
        opera = false;
        // Opera V6 cannot change style.display, but it can disguise itself
        // as nearly every browser. With the userAgent property we find out
        // its true nature.
        if (navigator.userAgent.indexOf("Opera") != -1){
          opera = true;
        }
        if (document.all && !opera) {
          // the world of the IEs
          openIcon = document.all(idName + "open");
          closeIcon = document.all(idName + "close");
          target = document.all(idName);

          if (target.style.display == "none") {
                      target.style.display = "inline";
                      openIcon.style.display = "none";
                      closeIcon.style.display = "inline";
          }else {
                      target.style.display = "none";
                      openIcon.style.display = "inline";
                      closeIcon.style.display = "none";
          }
        }
        else if (document.getElementById) {
          // the world of W3C without Opera
          openIcon = document.getElementById(idName + "open");
          closeIcon = document.getElementById(idName + "close");
          target = document.getElementById(idName);

          if (target.style.display == "none") {
                      target.style.display = "table-row";
                      openIcon.style.display = "none";
                      closeIcon.style.display = "inline";
          }else {
                      target.style.display = "none";
                      openIcon.style.display = "inline";
                      closeIcon.style.display = "none";
          }
        }
        else {
           // handle NS4.7 and Opera
          alert("ServerRequest");
        }
      }
 
	function toggleAllItemDetails(id, idRow, number, startindex) {
		opera = false;
		if (navigator.userAgent.indexOf("Opera") != -1){
			opera = true;
		}
		if (document.all && !opera) {
			var targetStyleDisplay = "none";
			var openIconStyleDisplay = "inline";
			var closerIconStyleDisplay = "none";
			/* toggle icon in header line first */
			openIconId = id + 'open';
			openIcon = document.all(openIconId);
			closerIconId = id + 'close';
			closerIcon = document.all( closerIconId);
			if(closerIcon.style.display == "none") {
				targetStyleDisplay = "inline";
				openIconStyleDisplay = "none";
				openIcon.style.display = openIconStyleDisplay;
				closerIconStyleDisplay = "inline";
				closerIcon.style.display = closerIconStyleDisplay;
			}
			else {
				targetStyleDisplay = "none";
				openIcon.style.display = openIconStyleDisplay;
				closerIcon.style.display = closerIconStyleDisplay;
			}
			/* adjust all rows */            
			i = 0;
			while(i < number) {
			    rowNo = startindex + i;
				openIconIdRow= idRow + i + 'open'
				openIconRow = document.all(openIconIdRow);
				closerIconIdRow = idRow + i + 'close';
       			closerIconRow = document.all( closerIconIdRow);
       			targetId = idRow + i;
       			targetRow = document.all(targetId);
				if (targetRow != null) {        			       
       			  targetRow.style.display = targetStyleDisplay;
       			}
       			if (openIconRow != null) { 
       			  openIconRow.style.display = openIconStyleDisplay;
       			}
       			if (closerIconRow != null) { 
       			  closerIconRow.style.display = closerIconStyleDisplay;
       			}
       			i++;
   			}   
		}
        else if (document.getElementById) {
        // the world of W3C without Opera
			var targetStyleDisplay = "none";
			var openIconStyleDisplay = "inline";
			var closerIconStyleDisplay = "none";
			/* toggle icon in header line first */
			openIcon = document.getElementById(id + "open");
			closerIcon = document.getElementById(id + "close");
			target = document.getElementById(id);  			
			if(closerIcon.style.display == "none") {
				targetStyleDisplay = "table-row";
				openIconStyleDisplay = "none";
				openIcon.style.display = openIconStyleDisplay;
				closerIconStyleDisplay = "inline";
				closerIcon.style.display = closerIconStyleDisplay;
			}
			else {
				openIcon.style.display = openIconStyleDisplay;
				closerIcon.style.display = closerIconStyleDisplay;
			}
			/* adjust all rows */            
			i = 0;
			while(i < number) {
			    rowNo = startindex + i;
				openIconRow = document.getElementById(idRow + rowNo + 'open');;
       			closerIconRow = document.getElementById(idRow + rowNo + 'close');
       			targetRow = document.getElementById(idRow + rowNo);
       			if (targetRow != null) {
       			    targetRow.style.display = targetStyleDisplay;
       			}       
       			if (openIconRow != null) {       			
       			    openIconRow.style.display = openIconStyleDisplay;
       			}
       			if (closerIconRow != null) {   
       			    closerIconRow.style.display = closerIconStyleDisplay;
       			}       			    
       			i++;
   			}
        }
   		else {
           // handle NS4.7 and Opera
           alert("ServerRequest");
        }                  
	}
 
      
      //
      function showProductReplacementHelp() {
          window.open('<isa:webappsURL name="b2b/productreplacementhelp.jsp"/>',
                      '<isa:UCtranslate key="b2b.prodrepl.helpwindow.titel"/>',
                      'scrollbars=yes,height=360,width=370');
      }

  
      searchRunning = false;
          
      function showproductincatalog(productId) {
          form_input().location.href="<isa:webappsURL name="/b2b/showproductdetail.do"/>?productId=" + productId;
          return true;
      }

      function setItemPropertyIndex() {
          if (document.getElementById("itemProperty") != null) {
             itemproperty = document.getElementById("itemProperty").value;
             disableStatus = itemproperty == "<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_BACKORDER %>" ;
                              
             disablePropVal = itemproperty == "<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE %>"
                              || itemproperty == "<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_BACKORDER %>";
             
             showLowPropVal = itemproperty != "<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE %>";
             showHighPropVal = itemproperty == "<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT %>";

             if (disableStatus == true) {
                 document.getElementById("itemStatus").value="<%=ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED%>";
             }

             document.getElementById("itemStatus").disabled = disableStatus;
             document.getElementById("itemPropertyLowValue").disabled = disablePropVal;
             document.getElementById("itemPropertyHighValue").disabled = disablePropVal;
             
             if (showLowPropVal) {
                 document.getElementById("lowValue").style.display = "inline";
             }
             else {
                 document.getElementById("lowValue").style.display = "none";
             }
             
             if (showHighPropVal) {    
                 document.getElementById("highValue").style.display = "inline";
             }
             else {
                 document.getElementById("highValue").style.display = "none";
             }
          }
      }
          
       function searchItems() {
          if (searchRunning == false) {
              searchRunning = true;
              document.search_items.submit();
          }
      }
      
function documentDisplay(navtype, guid, id, type, isBilling, origin) {
    if ("PORTAL" == navtype) {
      var finalportalJScript = portalJScriptForBillDocDisplay + escape("&doc_key=") + guid + escape("&doc_id=") + id + escape("&doc_type=") + type + escape("&objects_origin=") + origin;
      var finalportalURI = portalUriForBillDocDisplay + escape("&doc_key=") + guid + escape("&doc_id=") + id + escape("&doc_type=") + type + escape("&objects_origin=") + origin;
      doNavigate(finalportalJScript);
      document.location.href=finalportalURI;
    }
    if ("SHOP" == navtype) {
      var request = "<isa:webappsURL name ="b2b/documentstatusdetailprepare.do"/>?techkey=" + guid + "&object_id=" + id + "&objecttype=" + type + "&objects_origin=" + origin;
      if (isBilling == 'X') {
          request = request + "&billing=X";
      }
      document.location.href = request;
    }    
} 

  	function toggleHeader() {
  	    if (navigator.userAgent.indexOf("MSIE") != -1) {
  	    // the world of the IEs
            //alert("IE - Modus");
            icon = document.all("addheadericon");
    		target = document.all("addheader");
    		if (target.style.display == "none") {
    			target.style.display = "block";
                iconpath = icon.src.substring(0, icon.src.lastIndexOf("open.gif"));
    			icon.src=iconpath+"close.gif";
    		}else {
    			target.style.display = "none";
                iconpath = icon.src.substring(0, icon.src.lastIndexOf("close.gif"));
    			icon.src=iconpath+"open.gif";
    		}
    	} 
    	else {
    	  // the world of W3C
    	    //alert("W3C - Modus");
            icon = document.getElementById("addheadericon");
    		target = document.getElementById("addheader");
    		if (target.style.display == "none") {
    			target.style.display = "block";
                iconpath = icon.src.substring(0, icon.src.lastIndexOf("open.gif"));
    			icon.src=iconpath+"close.gif";
    		}else {
    			target.style.display = "none";
                iconpath = icon.src.substring(0, icon.src.lastIndexOf("close.gif"));
    			icon.src=iconpath+"open.gif";
    		}
    	} 
  	}     
  	
	function itemCheckBoxChanged(obj) {
	    var transBut = document.getElementById("LItransferbutton");
	    if (transBut == null) return;
	    if (obj.checked) {
	        transBut.className="";
	    } else {
	        if ( ! checkMarked()) {
	            transBut.className="disabled";
	        }
	    }
	}
	
	// send document to OCI-Client
	// the string "salesDocumentKey" must match the constant OciLinesSendAction.SALES_DOCUMENT_KEY
	// I do NOT this constant here because we must not use classes from crm/isa/isacore DC here in ecommercebase
	// this is only a temp. fix. I will have to clarify with further actions.
    function sendOci(guid) {
            document.order_positions.action = '<isa:webappsURL name ="/b2b/ocilinessend.do"/>?salesDocumentKey=' + guid
            document.order_positions.target = "isaTop";
            document.order_positions.submit();
    }
    
    <%-- Use this function to set all items visible --%>
    function showAllItems() {
        if (document.all) {
            var targetStyleDisplay = "inline";
        } else { 
            var targetStyleDisplay = "table-row";    
        }
        
        var items = document.getElementsByTagName("TR");
        for(var i = 0; i<items.length; i++) {
            if (items[i].id.substring(0,3) == 'row') {     <%-- relevant item ids start with 'row' (e.g. row_0, rowdetail_0) --%>
                items[i].style.display=targetStyleDisplay;
            }
        }
        var msgdiv = document.getElementById("itemnotoshowmsg");
        if (msgdiv != null) {
            msgdiv.style.display="none";
        }
}    