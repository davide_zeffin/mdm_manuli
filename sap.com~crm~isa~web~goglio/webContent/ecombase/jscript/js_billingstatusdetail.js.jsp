/*------------------------------------------------------*/
/* Javascript for JSP ecombase/billingstatusdetail.jsp  */
/*                                                      */
/* Version:        1.0                                  */
/* Author:         SAP AG                               */
/*                                                      */
/*                           (c) SAP AG, Walldorf       */
/*------------------------------------------------------*/
<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase" %>
<%@ page import="com.sap.isa.isacore.ManagedDocumentLargeDoc" %>
<%@ taglib uri="/isa" prefix="isa" %>

// function parameter: 'sType' can be: checkbox, hidden, password ...
//                     'sForm' is the form name in which will be searched
//                     'sId'   can be the basic name for an Array sId[1], sId[2], ...
function getElementsByType(sForm, sType, sId) {
    var objList = new Array(0);                        
    var oIdx = 0;                                      
                                                       
    var ende = false                                   
    var idx = 0;                                       
                                                       
    var vForm = document.forms[sForm];      // get Form object
    if (vForm == null) {
      vForm = document.getElementById(sForm);
    }
    while (! ende  &&  vForm != null) {                
        var obj = vForm.elements[idx];                 
        if (obj == null) {                             
            ende = true;                               
        } else {
            if (sId) {                                   
              if (obj.type == sType) {
                  if (obj.id != ""  &&  obj.id.substring(0,sId.length) == sId) {
                    objList[oIdx] = obj;                   
                    oIdx++;
                  }
              }
            } else {
              if (obj.type == sType) {
                  objList[oIdx] = obj;                   
                  oIdx++;
              }
            }                                          
        }                                              
        idx++;                                         
    }
    // Return collected objects (inputs with type 'sType')
    return objList;
}            
function cbtoggle() {
     if (document.billing_items.cball.checked) {
           markAll();
     }
     else {
           unmarkAll();
     }
 }

function markAll() {
     var inputs = getElementsByType('billing_items','checkbox');
     for (var i=1; i<inputs.length;i++) {
       if (inputs[i].type == 'checkbox')
         inputs[i].checked = true;
    }
 }

function unmarkAll() {
     var inputs = getElementsByType('billing_items','checkbox');
     for (var i=1; i<inputs.length;i++) {
       if (inputs[i].type == 'checkbox')
         inputs[i].checked = false;
    }
 }
function createClaim() {
    if ( portalUriForClaimCreation   &&    portalJScriptForClaimCreation) {
      // Collect items
      var cbs = getElementsByType('billing_items','checkbox', 'itemcheckbox');
      var items = getElementsByType('billing_items', 'hidden', 'docitemnumber');
      var aItem = new Array(cbs.length);
      var aItemCnt = 0;
      for (var i=0; i<cbs.length;i++) {
          if (cbs[i].checked == true) {
            aItem[aItemCnt] = items[i].value;
            aItemCnt++;
          }
      }
      if (aItemCnt == 0) {
        alert("<isa:UCtranslate key="ecm.stat.bill.ccr.noitem"/>");
        return true;
      }
      // Add items to request
      var addParams = escape("&docnumber=") + documentNumber;
      for (var i=0; (i<aItem.length && aItem[i] != null); i++) {
        addParams = addParams + escape("&docitemnumber[" + (i+1) + "]=" + aItem[i]); 
      }
      var finalportalJScript = portalJScriptForClaimCreation + addParams;
      var finalportalURI = portalUriForClaimCreation + addParams;
      // Do navigate
      //alert("doNavigate: " + finalportalJScript);
      doNavigate(finalportalJScript);
      // Send request
      //alert("Request: " + finalportalURI);
      document.location.href=finalportalURI;
    }
}

function docDisplay(navtype, guid, id, type, objects_origin) {
    var billing = "";
    if (type == 'billing') {
      billing = 'X';
    }
    if ("PORTAL" == navtype) {
      var finalportalJScript = portalJScriptForSalesDocDisplay + encodeURIComponent("&doc_key=") + guid + encodeURIComponent("&doc_id=") + id + encodeURIComponent("&doc_type=") + type;
      var finalportalURI = portalUriForSalesDocDisplay + encodeURIComponent("&doc_key=") + guid + encodeURIComponent("&doc_id=") + id + encodeURIComponent("&doc_type=") + type;
      doNavigate(finalportalJScript);
      document.location.href=finalportalURI;
    }
    if ("SHOP" == navtype) {
      var request = "<isa:webappsURL name ="b2b/documentstatusdetailprepare.do"/>?techkey=" + guid + "&object_id=" + id + "&objecttype=" + type + "&billing=" + billing + "&objects_origin=" + objects_origin;
      document.location.href = request;
    }
}

function setItemPropertyIndex() {
          if (document.getElementById("itemProperty") != null) {
             itemproperty = document.getElementById("itemProperty").value;
             
             showHighPropVal = itemproperty == "<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT %>";

             if (showHighPropVal) {    
                 document.getElementById("highValue").style.display = "inline";
             }
             else {
                 document.getElementById("highValue").style.display = "none";
             }
          }
}
<%-- Use this function to set all items visible --%>
function showAllItems() {
    var items = document.getElementsByTagName("TR");
    for(var i = 0; i<items.length; i++) {
        if (items[i].id.substring(0,4) == 'row_') {     <%-- relevant item ids start with 'row_' (e.g. row_man_1) --%>
            items[i].style.display="inline";
        }
    }
    var msgdiv = document.getElementById("itemnotoshowmsg");
    if (msgdiv != null) {
        msgdiv.style.display="none";
    }
}