<%--
********************************************************************************
    File:         prodsubstmsg.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      01.07.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/07/01 $

    Needed variables:
    - A instance of the SalesDocumentDataUI with name ui must be avaiable.
    - a variable called substMsgColspan

********************************************************************************
--%>

           <% if ( ui.isProdSubstitution() || 
                   ( ui.isProdKitBOM() && ui.isElementVisible("order.bomExplosion") ) || 
				   ( ui.isProdKitBOM() && ui.isBOMSubItemErroneous() ) ||
                   ui.isFreeGoodExclusive() || ui.isFreeGoodInclusive() ) { %>
               <%-- explanation, that productsubstitution has occured --%>
               <% String applic = pageContext.getServletContext().getInitParameter(ContextConst.IC_SCENARIO); %>                                             
               <tr class="<%= ui.even_odd %>">
                <% if (("B2B".equals(applic)) || ("OOB".equals(applic)) ||("POM".equals(applic)) ) { %>
				  <td>&nbsp;</td>
                <% } %>
				  <td colspan="<%= substMsgColspan %>">
					   <%-- Info --%>
					   <div class="info">
						  <span>
                               <% if (ui.isProdSubstitution()) { 
                                      if (ui.hasDifferingProductATPSubpos()) { %>
                                          <isa:translate key="b2b.productreplacement.message1" 
                                          arg0="<%= JspUtil.encodeHtml(ui.getMainQuantity()) %>" arg1="<%= JspUtil.encodeHtml(ui.getMainUnit()) %>"
                                          arg2="<%= JspUtil.encodeHtml(ui.getMainProduct()) %>"  arg3="<%= JspUtil.encodeHtml(ui.getMainReqDeliveryDate()) %>"/>
                                          <isa:translate key="<%= ui.getSubstMessage() %>" arg0="<%= ui.getSubPositionsPosNo() %>"/>
                                          <a href="#" onclick="showProductReplacementHelp(); return false;" >
                                          <isa:translate key="b2b.productreplacement.helplink"/>
                                          </a>
                               <%     }
                                      else { %>
                                          <isa:translate key="b2b.prodreplacement.msg4" arg0="<%= JspUtil.encodeHtml(ui.getMainProduct()) %>" arg1="<%= ui.getSubPositionsPosNo() %>"/>
                               <%     }
                                  }
                                  else if (ui.isProdKitBOM() && ui.isElementVisible("order.bomExplosion")){ %>
                                  <% if ((applic.equals("B2B")) || (applic.equals("OOB")) ||(applic.equals("POM")) ) { %>
                                    <isa:translate key="<%= ui.getKitProdMessage() %>" arg0="<%= JspUtil.encodeHtml(ui.getMainProduct()) %>" arg1="<%= ui.getSubPositionsPosNo() %>"/>
                                  <% }
                                     else { %>
                                    <isa:translate key="<%= ui.getKitProdMessage() %>" arg0="<%= JspUtil.encodeHtml(ui.getMainProduct()) %>" arg1="<%= ui.getSubPositionsProductId() %>"/>                                     
                                  <% } %>   
                               <% } 
								  else if (ui.isProdKitBOM() && ui.isBOMSubItemErroneous()){ %>
								  <isa:translate key="ecm.BOM.erroneous.subitem" />
				 			   <% } 
                               	  if (ui.isFreeGoodExclusive()){
                               %>
                                  <isa:translate key="ecm.freegood.exclusive" />
                               <%
                               	  }
                               	  if (ui.isFreeGoodInclusive()){
                               %>		
                                  <isa:translate key="ecm.freegood.inclusive" arg0="<%= ui.getFreeQuantity() %>" arg1="<%= JspUtil.encodeHtml(ui.getUnit()) %>"/>
                               <%
                               	  }
                               %>		
						  </span>
					   </div> <%-- End Info --%>
				  </td>
               </tr>
           <% } %>