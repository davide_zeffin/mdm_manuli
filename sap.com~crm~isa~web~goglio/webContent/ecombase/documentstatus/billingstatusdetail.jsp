<%--
********************************************************************************
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      08.5.2001

    !! This JSP is part of the eCommerceBase branch, and with that is has to have
       references only to object available in that branch. !!
********************************************************************************
--%>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.core.TechKey" %>

<%@ page import="com.sap.isa.businessobject.Address" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase" %>
<%@ page import="com.sap.isa.isacore.ManagedDocumentLargeDoc" %>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ page import="com.sap.isa.businessobject.header.HeaderBillingDocument"%>
<%@ page import="com.sap.isa.businesspartner.backend.boi.PartnerFunctionData" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.BusinessPartner" %>
<%@ page import="com.sap.isa.isacore.uiclass.BillingDocumentStatusBaseUI" %>
<%@ page import="com.sap.isa.businessobject.ConnectedDocument" %>
<%@ page import="com.sap.isa.businessobject.ConnectedDocumentItem" %>


<%@ taglib prefix="isa" uri="/isa" %>
<%@ include file="/appbase/checksession.inc" %> 

<%
   int line = 0;
   String[] evenOdd = new String[] { "even", "odd" };

   BillingDocumentStatusBaseUI ui = (BillingDocumentStatusBaseUI)GenericFactory.getInstance("billingStatusUI");
   ui.initContext(pageContext);
   
   BusinessPartner busPart = ui.getHeaderBusinessPartner(PartnerFunctionData.PAYER);
   Address address = (busPart != null ? busPart.getAddress() : new Address());

%>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="pragma" content="no-cache" />
        <title>Billing Document Status</title>
        
        <isa:stylesheets />

        <script type="text/javascript">
        <!--
                <%@ include file="/appbase/jscript/urlrewrite.inc"  %>
        //-->
        </script>                   
        <script src="<%=WebUtil.getMimeURL(pageContext, "/ecombase/jscript/js_billingstatusdetail.js.jsp")%>"
              type="text/javascript">
        </script> 
                
        <%-- Get JScript Files --%>
        <% ResultData jScriptNamesTab = new ResultData(ui.jScriptFileNames);
           jScriptNamesTab.beforeFirst();
           while(! jScriptNamesTab.isLast()) {
               jScriptNamesTab.next();
         %>
               <script src="<%=WebUtil.getMimeURL(pageContext, jScriptNamesTab.getString("FILENAME"))%>"
                       type="text/javascript">
               </script>
         <% } /* of while */ %>
                
        <script type="text/javascript">
        <!--
            <%-- Global Variables --%>
            searchRunning = false;
            <% String portalURI = "";
               String portalJScript = "";
               if (ui.getPortalAnchorAttributesForClaimCreation() != null) {
                  portalURI = ui.getPortalAnchorAttributesForClaimCreation().getURI();
                  portalJScript = ui.getPortalAnchorAttributesForClaimCreation().getJavaScript();
               }
            %>
            var portalUriForClaimCreation = "<%=portalURI%>";
            var portalJScriptForClaimCreation = "<%=portalJScript%>";
            <% portalURI = "";
               portalJScript = "";
               if (ui.getPortalAnchorAttributesForSalesDocDisplay() != null) {
                  portalURI = ui.getPortalAnchorAttributesForSalesDocDisplay().getURI();
                  portalJScript = ui.getPortalAnchorAttributesForSalesDocDisplay().getJavaScript();
               }
             %>
            var portalUriForSalesDocDisplay = "<%=portalURI%>";
            var portalJScriptForSalesDocDisplay = "<%=portalJScript%>";
            var documentNumber = "<%=ui.getDocNumber()%>"

            function searchItems() {
                if (searchRunning == false) {
                    searchRunning = true;
                    search_items.submit();
                }
            }
        //-->
        </script>
        </head>
        <body class="invoice" onload="setItemPropertyIndex();">
                <div class="module-name"><isa:moduleName name="ecombase/documentstatus/billingstatusdetail.jsp" /></div>
                <% String key = "b2b.docnav." + ui.getDocType(); %>
                <h1><span><isa:translate key="<%= key %>"/>:&nbsp;<%= ui.getDocNumber() %>&nbsp;<isa:translate key="b2b.docnav.from"/>&nbsp;<%= ui.getDocDate() %></span></h1>
                <div id="document">
                        <div class="document-header">
                                <%-- Document Header Area --%>
                        <a id="access-header" href="#access-items" title= "<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>">
                                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/1X1.gif")%>" alt="" style="display:none" width="1" height="1" />
                        </a>
                          <% HeaderBillingDocument billingheader = (HeaderBillingDocument)ui.header; %>
                          <div class="header-basic">
                            <table class="layout">
                              <tr>
                               <td class="col1">        
                                <%-- table data --%>
                                <table class="header-general" summary="<isa:translate key="ecm.acc.header.dat.sum"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="5"/>">
                                        <% String billDocCancelStatusTransKey = "";
                                                if (billingheader.isStatusCancelled()) billDocCancelStatusTransKey = "status.bill.cancel.doc";
                                                if (billingheader.isReversedDocument()) billDocCancelStatusTransKey = "esrv.crb2b.gs.txt.reverdoc";
                                                if (billingheader.isStatusCancelled() || billingheader.isReversedDocument()) {%>
                                                        <tr class="title">
                                                                <td class="identifier" scope="row">&nbsp;</td>
                                                                <td class="value"><span style="color:#f00;"><isa:translate key="<%=billDocCancelStatusTransKey%>"/></span></td>
                                                        </tr>
                                             <% } %>
                                        <tr>
                                                <td class="identifier-multiline" scope="row"><isa:translate key="status.bill.detail.payer"/>:</td>
                                                <td class="value"><%= ui.getPrintableAddress(busPart, address) %></td>
                                        </tr>
                                        <tr>
                                                <td class="identifier" scope="row"><isa:translate key="status.billing.detail.dueat"/>:</td>
                                                <td class="value"><%= billingheader.getDueAt() %></td>
                                        </tr>                                                                                                                                                 
                                </table><%-- End table header-general --%>
                               </td>
                               <td class="col2">
                                <%-- table price --%>
                                <table class="price-info" summary="<isa:translate key="ecm.acc.header.prieces"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="4"/>">
                                        <tr>
                                                <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalnet"/>:</td>
                                                <td class="value"><%= billingheader.getNetValue() %>&nbsp;<%= billingheader.getCurrency() %></td>
                                        </tr>
                                        <%-- tr>
                                                <td class="identifier" scope="row">Freight:</td>
                                                <td class="value">0,00&nbsp;<%= billingheader.getCurrency() %></td>
                                        </tr --%>
                                        <tr>
                                                <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totaltax"/>:</td>
                                                <td class="value"><%= billingheader.getTaxValue() %>&nbsp;<%= billingheader.getCurrency() %></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" class="separator"></td>
                                        </tr>
                                        <tr>
                                                <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalgross"/>:</td>
                                                <td class="value"><%= billingheader.getGrossValue() %>&nbsp;<%= billingheader.getCurrency() %></td>
                                        </tr>
                                        <tr>
                                                <td class="identifier-1" scope="row"><isa:translate key="status.billing.detail.payTerms"/>:</td>
                                                <td class="terms"><%= billingheader.getPaymentTerms() %></td>
                                        </tr>   
                                </table><%-- End table price --%>
                               </td>
                              </tr>
                              <tr> 
                               <td colspan="2">
                                <table class="header-general" summary="<isa:translate key="ecm.acc.docflow.info"/>">
                                 <% List predecessorList = billingheader.getPredecessorList(); 
	                                       ConnectedDocument predecessor;
	                                       String predecessorTypeKey = null;
	                                       String predecessorHref = null;
	                                       String predecessorText = "status.sales.predecessor";
	                                       List predecessorHeader = new ArrayList();                                                                          
	                                       int l= 0; 
	                                       if (predecessorList.size() > 0) {
		                                       predecessorText = "status.sales.predecessors";
                                          
	                                           for (int i = 0; i < predecessorList.size(); i++) {
		                                           predecessor = (ConnectedDocument)predecessorList.get(i); 
		                                           if ( predecessorHeader.contains(predecessor.getDocNumber()) == false ) {		                                      		                                          
			                                           predecessorHeader.add(predecessor.getDocNumber());	  		                                                                                                                                                          		            
                                                   %> 
                                                  <% l++; %>                              
                                                    <% if (l==1) { %>			                                              
			                                           <tr>                                                                                          
		                                                  <td class="doc_flow_bold"><isa:translate key="status.billing.detail.docf"/>:</td>
		                                                  <td class="doc_flow_value">
                                                    <% } %>         
			                                                 <% String transkey = "status.billdet.dtyp." + predecessor.getDocType(); %>
			                                                    <isa:translate key="<%=transkey%>"/>
			                                                    &nbsp;
			                                                 <% if (!"".equals(ui.writeHeaderRefOnClickEvent(predecessor))) { %>
				                                            <a href="#" <%=ui.writeHeaderRefOnClickEvent(predecessor)%> ><% } %>
				                                            <%=predecessor.getDocNumber()%><% if (!"".equals(ui.writeHeaderRefOnClickEvent(predecessor))) { %></a><% } %><% if (i != (predecessorList.size() - 1)){ %>,&nbsp;<% } %>				                                    			                                        	                                        
		                                                         <% } %> 
	                                            <% } %>  
	                                        </td></tr>
	                                       <% } %>                                                                               
                                </table>
                               </td>
                              </tr>
                            </table> <%-- class="layout"> --%>
                          </div> <%-- class="header-basic"> --%>
                       </div><%-- End document-header --%>

                        <%-- Document Item Filter --%>
                        <% // must item search criteria for large documents be displayed ?
                        if (ui.isLargeDocument()) {
                            String noSearchCriteria = (request.getAttribute(ActionConstantsBase.RC_NO_SEARCH_CRITERIA) != null ? (String)request.getAttribute(ActionConstantsBase.RC_NO_SEARCH_CRITERIA) : "");
                            String noItemsFound = (request.getAttribute(ActionConstantsBase.RC_NO_ITEMS_FOUND) != null ? (String)request.getAttribute(ActionConstantsBase.RC_NO_ITEMS_FOUND) : "");
                            String lowValInvalid = (request.getAttribute(ActionConstantsBase.RC_LOW_VAL_INVALID) != null ? (String)request.getAttribute(ActionConstantsBase.RC_LOW_VAL_INVALID) : "");
                            String highValInvalid = (request.getAttribute(ActionConstantsBase.RC_HIGH_VAL_INVALID) != null ? (String)request.getAttribute(ActionConstantsBase.RC_HIGH_VAL_INVALID) : "");
                        %>
                                <div class="document-item-filter">
                                        <a id="access-items" href="#access-buttons" title= "<isa:translate key="b2b.acc.items.title"/>" accesskey="<isa:translate key="b2b.acc.items.key"/>">
                                                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/1X1.gif")%>" alt="" style="display:none" width="1" height="1" />
                                        </a>
      
                                        <%-- Begin of zb change --%>
                                        <% if (ui.existingProcess ) { %>
                                        <div class="title">
                                                        <isa:translate key="b2b.status.itemsfound" arg0="<%=String.valueOf(ui.noOfOriginalItems)%>" arg1="<%= ui.getLargeDocThreshold() %>"/>
                                                        <br/><br/>
                                                </div>
                                        <% } else { %>  
                                        <%-- End of zb change --%>
    
                                                <div class="title">
                                                        <isa:translate key="b2b.orderstatus.largedoc" arg0="<%= String.valueOf(ui.noOfOriginalItems) %>" arg1="<%=String.valueOf(ui.getShopLargeDocNoOfItemsThreshold())%>" />
                                                </div>
                                                <div class="separator-top"><span></span></div>
                                                <form id="search_items" method="post" action='<isa:webappsURL name ="/b2b/documentstatussearchitems.do"/>?billingmode=<%=JspUtil.encodeURL(request.getParameter("billingmode"))%>'>                                      
                                                        <div><input name="<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>" type="hidden" value="SearchCriteria_B2B_Billing_BillingStatus_Items" /></div>
                                                        <% if ("billingdirect".equals((String)request.getParameter("externalforward"))) { %>
                                                                <input name="billingmode" type="hidden" value="standalone" />
                                                        <% } %>
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                <% if (noSearchCriteria.equals("true") ||
                                                                       noItemsFound.equals("true") ||
                                                                       lowValInvalid.equals("true") ||
                                                                       highValInvalid.equals("true")) { %>
                                                                        <tr>
                                                                           <td colspan="2">
                                                                               <div class="info">
                                                                                   <span>
                                                                                      <% if (noSearchCriteria.equals("true")) { %>
                                                                                            <isa:translate key="b2b.status.nocriteria"/><br/>
                                                                                      <% } %>
                                                                                      <% if (noItemsFound.equals("true")) { %>
                                                                                            <isa:translate key="b2b.status.noitemsfound"/><br/>
                                                                                      <% } %>
                                                                                      <% if (lowValInvalid.equals("true")) { %>
                                                                                            <isa:translate key="b2b.status.lowvalinvalid"/><br/>
                                                                                      <% } %>
                                                                                      <% if (highValInvalid.equals("true")) { %>
                                                                                            <isa:translate key="b2b.status.highvalinvalid"/><br/>
                                                                                      <% } %>
                                                                                    </span>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                <% } %>
                                                                <tr style="width:100%">
                                                                        <td>
                                                                                <label for="itemProperty"><isa:translate key="b2b.orderstatus.showpos"/>&nbsp;</label>
                                                                        </td>
                                                                        <td style="width:100%">
                                                                                <span title="label shuffler">
                                                                                        <select style="max-width:45%" size="1" name="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY%>" id="itemProperty" onchange="setItemPropertyIndex();">
                                                                                                <option value="<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_PRODUCT%>" <%=ui.getSearchItemProperty().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_PRODUCT) ? "selected=\"selected\"" : ""%> ><isa:translate key="b2b.status.prop.prod"/></option>
                                                                                                <option value="<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT%>" <%=ui.getSearchItemProperty().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT) ? "selected=\"selected\"" : ""%> ><isa:translate key="b2b.status.prop.itmposno"/></option>
                                                                                        </select>
                                                                                </span>
                                                                                <input name="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE%>" class="textinput-middle" value="<%=ui.getSearchItemPropertyLowValue()%>" size="15" maxlength="40"/>
                                                                                <div id="highValue">
                                                                                <isa:translate key="b2b.status.prop.nointhigh"/>
                                                                                <input name="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_HIGH_VALUE%>" class="textinput-middle" value="<%=ui.getSearchItemPropertyHighValue()%>" size="15" maxlength="40"/>
                                                                                </div>
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td colspan="2">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td> <a class="button" onclick="searchItems();" href="#"><isa:translate key="b2b.orderstatus.searchpos"/></a></td>
                                                                </tr>
                                                                <% if (ui.getNumberOfItemsFound() != ManagedDocumentLargeDoc.NO_OF_FOUND_ITEMS_UNDEFINED) { %>
                                                                        <tr>
                                                                                <td colspan="2">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td colspan="2">
                                                                                  <% if (ui.getNumberOfItemsFound() <= ui.getShopLargeDocNoOfItemsThreshold()) { %>
                                                                                     <isa:translate key="b2b.status.itemsfound" arg0="<%=String.valueOf(ui.getNumberOfItemsFound())%>"/>
                                                                                  <% }
                                                                                     else { %>
                                                                                     <isa:translate key="b2b.status.itemsfound.excd" arg0="<%=String.valueOf(ui.getNumberOfItemsFound())%>" arg1="<%= String.valueOf(ui.getShopLargeDocNoOfItemsThreshold()) %>"/>
                                                                                  <% } %>
                                                                                </td>
                                                                        </tr>
                                                                <%  } %>
                                                        </table>
                                        </form>
                                                <div class="separator-bottom"><span></span></div>
                                        <%-- Begin of zb change --%>
                                        <% } /* ui.existingProcess*/ %>
                                <%-- End of zb change --%>
                                </div><%-- document-item-filter --%>
                        <% } else if (ui.getItemNoToShow() != null) { /* ui.isLargeDocument() */ %>
                          <div id="itemnotoshowmsg" class="document-item-filter">
                          <isa:translate key="b2b.status.show.item.only.bl" arg0="<%=ui.getItemNoToShow()%>"/>
                          <a href="#" onclick="showAllItems();"><isa:translate key="b2b.status.show.item.only.lk" arg0="<%=ui.getItemNoToShow()%>"/></a>
                          </div>
                        <% } %>
                        
                        <form name="billing_items" method="post" action='<isa:webappsURL name ="/b2b/documentstatussearchitems.do"/>?billingmode=<%=JspUtil.encodeURL(request.getParameter("billingmode"))%>'>                     
                        <%-- Document Item Rows --%>
                        <div class="document-items">
                                <table class="itemlist" summary="<isa:translate key="ecm.acc.items.sum"/> <isa:translate key="ecm.acc.tab.size.info" arg0="7" arg1="<%=(String.valueOf(ui.accNumOfItems)) %>"/>">
                                        <%-- Table-Header --%>
                                        <tr>
                                                <%-- Begin of zb change --%>
                                                <%--<th><input id="cball" name="cball" onclick="cbtoggle()" type="checkbox"/></th>--%>
                                                <% if (!ui.existingProcess ) { %>
                                                  <% if (ui.isCheckBoxRequested)  { %>
                                                        <th class="select"><input id="cball" name="cball" type="checkbox" onclick="cbtoggle();" value="ON" title="<isa:translate key="status.sales.cb.checkall"/>"/></th>
                                                <% } else { %>
                                                        <th class="select"></th>
                                                <% } %>
                                                <% } else { %>
                                                        <th class="select"></th>
                                                <% } %>
                                                <%-- End of zb change --%>      
                                                <th class="item" scope="col"><isa:translate key="status.sales.detail.itemposno"/></th>
                                                <th class="product" scope="col"><isa:translate key="status.sales.detail.product"/></th>
                                                <th class="desc" scope="col"><isa:translate key="status.sales.detail.description"/></th>
                                                <th class="qty" scope="col"><isa:translate key="status.sales.detail.itemquantity"/></th>
                    
                                                <%--<th class="ref-doc" scope="col"><isa:translate key="status.sales.detail.refdoc"/></th>  --%>    
                                                <th class="price" scope="col"><isa:translate key="status.billing.detail.netvalue"/></th>
                                                <th class="price-unit" scope="col"><isa:translate key="status.billing.detail.grossvalue"/></th>
                                        </tr>
          
                                        <%-- Main Position --%>  
                                        <% 
                                                String rowname = "";
                                                int itemrowno = 0;

                                                ui.setItemsToIterate();
                                        %>
                                        <%-- <form name="claim_items" method="post" >--%>
                                        <isa:iterate id="billingitem" name="<%= ActionConstantsBase.RK_BILLING_DOC_STATUS_DETAIL_ITEMITERATOR %>"
                                               type="com.sap.isa.businessobject.item.ItemBillingDoc">
                  
                                                <% if (!ui.isBOMSubItemToBeSuppressed() ) {   
                                                  itemrowno++; 
                                                  line++;%>
                                                <% if (!billingitem.isClaimable()) { %>
                                                <tr id="row_msg_<%=itemrowno%>" <%=(ui.isItemNoToShow(billingitem.getNumberInt()) ? "" : "style=\"display:none\"")%> >
                                                        <td></td>
                                                        <td colspan="6"><div class="info"><span><%=ui.getUnclaimableMessage(billingitem)%></span></div></td>
                                                </tr>
                                                <% } %>
                                                <tr id="row_man_<%=itemrowno%>" class="<%= evenOdd[line % 2]%>" <%=(ui.isItemNoToShow(billingitem.getNumberInt()) ? "" : "style=\"display:none\"")%> >
                                                        <% int rowspan = 2;
                                                           if (!billingitem.isClaimable()) {
                                                               rowspan++;
                                                           }
                                                         %>
                                                         
                                                      <%-- <td rowspan="<%=rowspan%>">--%>  
                                                       <td>                                                     
                                                                <%-- Begin of zb change --%>
                                                                <%--<input name="docitemnumber[<%= billingitem.getNumberInt() %>]" type="checkbox"/>--%>
                                                                <% if (!ui.existingProcess) { %>
                                                                  <% if (ui.isCheckBoxRequested)  { %>
                                                                     <% String itmCB = "";
                                                                         if (!billingitem.isClaimable()) {
                                                                             itmCB = "disabled";
                                                                         }
                                                                      %>
                                                                        <input name="<%=ActionConstantsBase.RK_CHECKBOXNAME%>[<%=itemrowno%>]"  id="<%=ActionConstantsBase.RK_CHECKBOXNAME%>_<%=itemrowno%>" type="checkbox" <%=itmCB%>/>
                                                                  <% } %>
                                                                <% } else { %>
                                                                        <input name="docitemnumberchecked"  type="radio" value="<%= JspUtil.removeNull(billingitem.getNumberInt()) %>" onclick="oneSelected=true"/>
                                                                <% } %>
                                                                <input name="docitemnumber[<%=itemrowno%>]" id="docitemnumber[<%=itemrowno%>]" type="hidden" value="<%= JspUtil.removeNull(billingitem.getNumberInt()) %>"/>
                                                                <%-- End of zb change --%>
                                                        </td>
                                                        <td class="item"><%= billingitem.getNumberInt() %></td>
                                                        <td class="product"><%= billingitem.getProduct() %></td>
                                                        <td class="desc"><%= billingitem.getDescription() %></td>
                                                        <td class="qty"><%= billingitem.getQuantity() %> <abbr title="piece"><%= billingitem.getUnit() %></abbr></td>
                                                        <%--
                                                        <% if (ui.getRefDocumentLinkAttributes(billingitem) != null) { %>
                                                                <td class="ref-doc">
                                                                        <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>?<%= ui.getRefDocumentLinkAttributes(billingitem)%>" target="form_input">
                                                                                <%= billingitem.getSalesRefDocNo() %>
                                                                        </a>
                                                                        &nbsp;/&nbsp;<%= billingitem.getSalesRefDocPosNo() %>
                                                                </td>
                                                        <% } else {%>
                                                                <td class="ref-doc"><%= billingitem.getSalesRefDocNo() %>
                                                                        &nbsp;/&nbsp;<%= billingitem.getSalesRefDocPosNo() %>
                                                                </td>
                                                        <% } %>--%>
                                                        <td class="price"><%= billingitem.getNetValue() %>&nbsp;<%= billingitem.getCurrency() %></td>       
                                                        <td class="price-unit"><%= billingitem.getGrossValue() %>&nbsp;<%= billingitem.getCurrency() %></td>
                                                </tr>
                         
                                                <tr id="row_dtl_<%=itemrowno%>" class="<%= evenOdd[line % 2]%>" <%=(ui.isItemNoToShow(billingitem.getNumberInt()) ? "" : "style=\"display:none\"")%> >
                                                        <td></td>
                                                        <td colspan="6" align="left">
                                                                <%-- Begin of ir change --%>
                                                                <table class="item-detail" >
                                                                <%-- end of ir change --%>
                                 
                                                                <%-- ----------------connected documents--------------- --%>
                                                                <% 
													                List itemPredecessorList = billingitem.getPredecessorList(); 
																	ConnectedDocumentItem itemPredecessor;
																	String itemPredecessorTypeKey = null;
																	String itemPredecessorHref = null;
																	String itemPredecessorText = "status.sales.predecessor";														             
														            
                                                                        int k= 0;
                                                                        if (itemPredecessorList.size() > 0) {
                                                                        itemPredecessorText = "status.sales.predecessors";
                                                                        int listSize= itemPredecessorList.size();
                                                                        for (int i = 0; i < itemPredecessorList.size(); i++) {
                                                                                itemPredecessor = (ConnectedDocumentItem) itemPredecessorList.get(i);  %>           
                                                                                        <% k++; %>
                                                                                        <% if (k==1) { %><tr>                                                                                                 
                                                                                                <td class="doc_flow_ident"><isa:translate key="status.billing.detail.docf"/>:</td>                                                                                               
                                                                                                <td class="doc_flow_value">
                                                                                                    <table class="doc_flow_value_inner" summary="<isa:translate key="ecm.acc.item.docflow.info"/>">
                                                                                        <% } %>         
                                                                                                        <tr class="doc_flow_value_inner">
                                                                                                            <td class="doc_flow_value_inner">                                                                                                
                                                                                                                <% String transkey = "status.billdet.dtyp." + itemPredecessor.getDocType(); %>
                                                                                                                <isa:translate key="<%=transkey%>"/>
                                                                                                                &nbsp;
                                                                                                            </td> 
                                                                                                            <td class="doc_flow_value_inner">   
                                                                                                                <% if (!"".equals(ui.writeItemOnClickEvent(billingitem, itemPredecessor))) { %>
                                                                                                                <a href="#" class="icon" <%=ui.writeItemOnClickEvent(billingitem, itemPredecessor)%> >
                                                                                                                <% } %>
                                                                                                                <%=itemPredecessor.getDocNumber()%>&nbsp;/&nbsp;<%=itemPredecessor.getDocItemNumber()%>
                                                                                                                <% if (!"".equals(ui.writeItemOnClickEvent(billingitem, itemPredecessor))) { %>
                                                                                                                </a>
                                                                                                                <% } %>
                                                                                                            </td> 
                                                                                                        </tr>       
                                                                                           <% if (k==listSize) { %>                       
                                                                                                    </table>
                                                                                                 </td>
                                                                                             </tr>
                                                                                           <% } %>                                                                                                                                                                                                                                                                                                                                                                                                                        
                                                                        <% } /* for... */ %>                                                                         
                                                                        <% } /* if predecessorList... */ %>
                                                                </table>
                                                        </td>
                                                </tr>
                                               <%-- <tr><td colspan="8"></td></tr>--%>
                        
                                                <%-- ********** Seperator between two items ********** --%>      
                                                <tr>
                                                        <td colspan="7" class="separator"></td>
                                                </tr>
                                            <% } %>   <%--  ui.isBOMSubItemToBeSuppressed()   --%> 
                                        </isa:iterate>
                                        <%--</form>--%>
                                </table><%-- End table itemlist --%>
                        </div><%-- End document-items --%>
                        <div><input type="hidden" name="docnumber" value="<%= ui.getDocNumber() %>" /></div>
                </form>
        </div> 

                <%-- Buttons --%>
                <div id="buttons">

                        <a id="access-buttons" href="#access-header" title= "<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>">
                                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/1X1.gif")%>" alt="" style="display:none" width="1" height="1" />
                        </a>
    
                        <% ResultData navBut = new ResultData( ui.navButtons );%>
                        <ul class="buttons-1">
                               <li/>
                               <% navBut.beforeFirst();
                                  while( ! navBut.isLast()) {
                                        navBut.next();
                                        if ("LEFT".equals(navBut.getString("ALIGN"))) {
                                                String resKey = navBut.getString("NAME");
                                                String butTitle = navBut.getString("TITLE");
                                        %>
                                                <li><a href="<%= navBut.getString("HREF")%>" onclick="<%= navBut.getString("ONCLICK")%>" title="<isa:translate key="<%= butTitle %>"/>"><isa:translate key="<%= resKey %>"/></a></li>
                               <% } %>
                        <% } /* while */ %>
                        </ul>
                        <ul class="buttons-3">
                                <li/>
                                <% navBut.beforeFirst();
                                while( ! navBut.isLast()) {
                                        navBut.next();
                                        if ("RIGHT".equals(navBut.getString("ALIGN"))) {
                                                String resKey = navBut.getString("NAME");
                                                String butTitle = navBut.getString("TITLE");
                                        %>
                                                <li>
                                                        <%-- Begin of zb change --%>
                                                        <% if ("PROCESSTYPESELECTION".equals(navBut.getString("LINKED_ELEMENT"))) { %>
                                                                <% ResultData orderProcessTypes = new ResultData(ui.getSucProcTypeAllowedList().getTable());
                                                                orderProcessTypes.beforeFirst();
                                                                %>
                                                                <select id="processtypesSID">
                                                                        <option value="no_choice"> <isa:translate key="status.sales.detail.selcho"/> </option>
                                                                        <% while ( ! orderProcessTypes.isLast()) {
                                                                                orderProcessTypes.next();
                                                                        %>
                                                                                <%-- <option value="<%= orderProcessTypes.getString(1) %>"><isa:translate key="status.sales.detail.selquo"/>&nbsp;<%= orderProcessTypes.getString(2) %></option>--%>
                                                                                <option value="<%= orderProcessTypes.getString(1) %>"><%= orderProcessTypes.getString(2) %></option>
                                                                        <% } /* while */ %> 
                                                                </select>
                                                        <% } /* if ("PROCESSTYPESELECTION" ... */  %>
                                                        <%-- end of zb change --%>
                                                        <a href="<%= navBut.getString("HREF")%>" onclick="<%= navBut.getString("ONCLICK")%>" title="<isa:translate key="<%= butTitle %>"/>"><isa:translate key="<%= resKey %>"/></a>
                                                </li>
                                        <% } /* ("RIGHT".equals ... */  %>
                                <% } /* while( ! navBut ... */ %>
                        </ul>
                </div><%-- buttons --%>
        </body>
</html>