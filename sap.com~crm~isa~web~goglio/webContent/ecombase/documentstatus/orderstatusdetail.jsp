<%--
********************************************************************************
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      08.5.2001

    !! This JSP is part of the eCommerceBase DC, and with that is has to have
       references only to object available in that Development Component. !!                      

********************************************************************************
--%>

<%@ page import="com.sap.isa.core.ContextConst" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase" %>
<%@ page import="com.sap.isa.isacore.ManagedDocumentLargeDoc" %>
<%@ page import="com.sap.isa.isacore.uiclass.SalesDocumentStatusBaseUI" %>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.isa.businessobject.order.ExtRefObjectList" %>
<%@ page import="com.sap.isa.businessobject.order.ExternalReference" %>
<%@ page import="com.sap.isa.businessobject.ConnectedDocument" %>
<%@ page import="com.sap.isa.businessobject.ConnectedDocumentItem" %>
<%@ page import="com.sap.isa.businessobject.Schedline" %>
<%@ page import="com.sap.isa.businessobject.item.ItemSalesDoc" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.sap.isa.businessobject.item.ItemList" %>
<%@ include file="/appbase/checksession.inc" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%
  SalesDocumentStatusBaseUI ui = (SalesDocumentStatusBaseUI)GenericFactory.getInstance("orderStatusUI");
  ItemSalesDoc orderitem = null;
  ui.initContext(pageContext);
  String inputValue = null;
  //inline display of product configuration will be restricted on values of identifying characteristics
  boolean showDetailView = false;
  List itemPredecessorList = new ArrayList();
  List itemSuccessorList = new ArrayList();  
  boolean defaultOpenItem = false;  
%>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <title>Sales Document Status</title>

    <isa:stylesheets/>

    <script type="text/javascript">
    <!--
        <%@ include file="/appbase/jscript/urlrewrite.inc"  %>
    //-->
    </script>

    <%-- Get JScript Files --%>
    <%  ResultData jScriptNamesTab = new ResultData(ui.jScriptFileNames);
        jScriptNamesTab.beforeFirst();
        while(! jScriptNamesTab.isLast()) {
            jScriptNamesTab.next();
    %>
        <script src="<%=WebUtil.getMimeURL(pageContext, jScriptNamesTab.getString("FILENAME"))%>"
              type="text/javascript">
        </script>
    <% } %>

    <script type="text/javascript">
    <!--
     <%-- Global Variables --%>
     <% String portalURI = "";
        String portalJScript = "";
        if (ui.getPortalAnchorAttributesForBillingDocDisplay() != null) {
            portalURI = ui.getPortalAnchorAttributesForBillingDocDisplay().getURI();
            portalJScript = ui.getPortalAnchorAttributesForBillingDocDisplay().getJavaScript();
        }
      %>
        var portalUriForBillDocDisplay = "<%=portalURI%>";
        var portalJScriptForBillDocDisplay = "<%=portalJScript%>";

    //-->
    </script>
  </head>
  <body class="orderstatus" onLoad="setItemPropertyIndex();">
  <div class="module-name"><isa:moduleName name="ecombase/documentstatus/orderstatusdetail.jsp" /></div>
  <h1><span><%= JspUtil.encodeHtml(ui.getHeaderDescription()) %>:&nbsp;<%= JspUtil.encodeHtml(ui.getDocNumber()) %>&nbsp;<isa:translate key="b2b.docnav.from"/>&nbsp;<%= ui.getDocDate() %></span></h1>
  <div id="document">

  <div class="document-header">

    <%-- Header table--%>
    <a id="access-header" href="#access-items" title= "<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>
    <div class="header-basic">
    <table class="layout">
        <tr>
            <td class="col1">
                <% if (ui.isAccessible) { %>
                    <a href="#end-table1" title="<isa:translate key="ecm.acc.header.dat.sum"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="11"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
                <% } %>
                <table class="header-general" summary="<isa:translate key="ecm.acc.header.dat.sum"/>">
                <% if (ui.isServiceRecall()) { %>
                        <tr>
                            <% if (ui.isElementVisible("order.recallId")){ %>
                              <td class="identifier"><isa:translate key="b2b.order.disp.recall"/></td>
                              <td class="value"><%= JspUtil.encodeHtml(ui.header.getRecallId()) %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getRecallDesc()) %></td>
                            <% } %>
                        </tr>
                        <% if (ui.isElementVisible("order.extRefObjects") && ui.header.getExtRefObjectType() != null &&  ui.header.getExtRefObjectType().length() > 0) { %>
                            <tr>
                                 <td class="identifier"><%= JspUtil.encodeHtml(ui.header.getExtRefObjectTypeDesc()) %>:</td>
                                 <td class="value">
                                    <%  ExtRefObjectList extRefObjects = ui.header.getAssignedExtRefObjects();
                                    if ( extRefObjects != null && extRefObjects.size() > 0) {
                                       for (int j=0; j < extRefObjects.size(); j++) { %>
                                          <%= JspUtil.encodeHtml(extRefObjects.getExtRefObject(j).getData()) %>
                                          <% if (j < extRefObjects.size()-1) { %>;<% } %>
                                       <% }
                                    } %>
                                 </td>
                            </tr>
                        <% } %>
                <% } %>
                <%-- render the consumer, only in B2R scenario --%>
                <% if (ui.isSoldToVisible() && ui.isElementVisible("order.soldTo")) { %>
                <tr>
                    <td class="identifier"><isa:translate key="status.sales.consumer.name"/> (<isa:translate key="status.sales.consumer.no"/>):</td>
                    <td class="value"><%= JspUtil.encodeHtml(ui.getSoldToName()) %> (<%= JspUtil.encodeHtml(ui.getSoldToId()) %>)</td>
                </tr>
                <% } %>
                <% if (ui.isElementVisible("order.numberExt")) { %>
                <tr>
                    <td class="identifier"><isa:translate key="status.sales.purchaseorderext"/>:</td>
                    <td class="value"><%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %></td>
                </tr>
                <% } %>
                <% if (ui.isElementVisible("order.description")) { %>
                <tr>
                    <td class="identifier"> <% if (!ui.backendR3) { %> <isa:translate key="status.sales.description"/>: <% } %></td>
                    <td class="value"><% if (!ui.backendR3) { %> <%= JspUtil.encodeHtml(ui.header.getDescription()) %> <% } %></td>
                </tr>
                <% } %>
                </table>
                <% if (ui.isAccessible) { %>
                    <a name="#end-table1" title="<isa:translate key="ecm.acc.head.dat.end"/>"></a>
                <% } %>
            </td>
            <td class="col2">
            <%-- Status table--%>
            <%   // determine status information
                 // some temporary variables for storing status information, which can be displayed
                 boolean overallStatus=false;
                 boolean deliveryStatus=false;
                 boolean quotationValidDate=false;
                 if ( ! ui.header.isDocumentTypeOrderTemplate()) {
                     overallStatus=true;
                     if (! ui.header.isDocumentTypeQuotation()) {
                         deliveryStatus=true;
                     }
                 }
                 if ( ui.header.isDocumentTypeQuotation() &&
                    ( ui.header.isStatusOpen() && !ui.header.isQuotationExtended()
                   || ui.header.isStatusReleased() && ui.header.isQuotationExtended() ) ) {
                      quotationValidDate=true;
                 } %>
                <% if (ui.isAccessible) { %>
                    <a href="#end-table2" title="<isa:translate key="ecm.acc.head.status"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="2"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
                <% } %>
                <table class="status" summary="<isa:translate key="ecm.acc.head.status"/>">
                    <% if (overallStatus && ui.isElementVisible("order.status.overall")) { %>
                    <tr>
                        <td class="identifier"><isa:translate key="status.sales.overallstatus"/>:</td>
                        <td class="value"><isa:translate key="<%= JspUtil.encodeHtml(ui.getDocumentStatusKey()) %>"/></td>
                    </tr>
                    <% } %>
                    <% if (deliveryStatus && !ui.isLargeDocument()&& ui.isElementVisible("order.status.delivery")) { %>
                    <tr>
                        <td class="identifier"><isa:translate key="status.sales.deliverystatus"/>:</td>
                        <td class="value"><isa:translate key="<%= JspUtil.encodeHtml(ui.getDocumentDeliveryStatusKey()) %>"/></td>
                    </tr>
                    <% } %>
                    <% if (quotationValidDate && ui.isElementVisible("order.validTo")) { %>
                    <tr>
                        <td class="identifier"><isa:translate key="status.sales.detail.validTo"/>:</td>
                        <td class="value">
                        <% if ( ! ui.header.getValidTo().equals("00000000")) { %>
                            <%= JspUtil.encodeHtml(ui.header.getValidTo()) %>
                        <% } else { %>
                            <isa:translate key="status.sales.detail.noValidTo"/>
                        <% } %>
                        </td>
                    </tr>
                    <% } %>
                </table>
                <% if (ui.isAccessible) { %>
                    <a name="#end-table2" title="<isa:translate key="ecm.acc.head.stat.end"/>"></a>
                <% } %>
                <%-- table price --%>
                <% if (ui.isAccessible) { %>
                    <a href="#end-table3" title="<isa:translate key="ecm.acc.header.prieces"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="4"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
                <% } %>
                <table class="price-info" summary="<isa:translate key="ecm.acc.header.prieces"/>">
                     <% if (ui.isElementVisible("order.priceNet") && ui.isNetValueAvailable() ) { %>
                    <tr>
                        <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalnet"/>:</td>
                        <td class="value"><%= ui.getHeaderValue() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                    </tr>
                    <% } %>
                    <% if (ui.isFreightValueAvailable()&& ui.isElementVisible("order.freight")) {   // For R/3 created documents, freight value is not available! %>
                    <tr>
                        <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalfreight"/>:</td>
                        <td class="value"><%= ui.header.getFreightValue() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                    </tr>
                    <% } %>
                    <% if (ui.isElementVisible("order.tax") && ui.isTaxValueAvailable()) { %>
                    <tr>
                        <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totaltax"/>:</td>
                        <td class="value"><%= ui.header.getTaxValue() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                    </tr>
                    <% } %>
                    <% if (ui.isElementVisible("order.priceGross") && ui.isGrossValueAvailable()) { %>
                    <tr>
                        <td colspan="2" class="separator"></td>
                    </tr>
                    <tr>
                        <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalgross"/>:</td>
                        <td class="value"><%= ui.header.getGrossValue() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                    </tr>
                    <% } %>
                    <% if (ui.showHeaderPaymentTerms() && ui.isElementVisible("order.paymentterms")) { %>
                    <tr>
                        <td class="identifier-1" scope="row"><isa:translate key="b2b.order.disp.paymentterms"/></td>
                        <td class="terms"><%= JspUtil.encodeHtml(ui.header.getPaymentTermsDesc()) %></td>
                    </tr>
                    <% } %>
                </table>
                <% if (ui.isAccessible) { %>
                    <a name="end-table3" title="<isa:translate key="ecm.acc.head.prc.end"/>"></a>
                <% } %>
            </td>
        </tr>
    </table> <%-- class="layout"> --%>
    </div> <%-- class="header-basic"> --%>
    <div class="header-itemdefault">  <%-- level: sub3 --%>
        <h1 class="group"><span><isa:translate key="status.header.itemdefault"/></span></h1>
        <div class="group">
        <%-- Item defaults Data--%>
        <% if (ui.isAccessible) { %>
             <a name="#end-table4" title="<isa:translate key="b2b.acc.header.defdata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
        <% } %>
            <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>">
                <% if (ui.isElementVisible("order.deliverTo")) { %>                 
                <tr>
                    <td class="identifier"><isa:translate key="status.sales.detail.deliveryaddr"/>:</td>
                    <td class="value"><%= JspUtil.encodeHtml(ui.header.getShipTo().getShortAddress()) %>&nbsp;
                        <a href="#" class="icon" onclick="showShipTo('<%=ui.header.getShipTo().getTechKey().getIdAsString()%>');">
                            <img class="img-2" src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/shipad_detail.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/>"  />
                        </a>
                    </td>
                </tr>
                <% } %>
                <% if (ui.isElementVisible("order.shippingCondition")){ %>
                <tr>
                    <td class="identifier"><% if (ui.header.getShipCond() != null) { %><isa:translate key="status.sales.detail.shippingcond"/>:<%}%></td>
                    <td class="value"><% if (ui.header.getShipCond() != null) { %><%= JspUtil.encodeHtml(ui.header.getShipCond()) %> <%}%></td>
                </tr>
                <% }
                   if ((!ui.backendR3 && ui.isElementVisible("order.deliveryPriority")) &&
                       (ui.header.getDeliveryPriority() != null && ui.header.getDeliveryPriority().length() > 0)) { %>
                <tr>
                    <td class="identifier"><isa:translate key="status.sales.detail.dlv.prio"/>:</td>
                    <td class="value"><%= JspUtil.encodeHtml(ui.getDeliveryPriorityDescription(ui.header.getDeliveryPriority())) %></td>
                </tr>
                <% } %>
                <% if ( ! ui.header.isDocumentTypeOrderTemplate() && ui.isElementVisible("order.reqDeliveryDate")) { %>
                <tr>
                    <td class="identifier"><isa:translate key="b2b.order.disp.reqdeliverydate"/></td>
                    <td class="value"><%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %></td>
                </tr>
                <% } %>
                <%--Header Cancel date--%>
                <% if ( ! ui.header.isDocumentTypeOrderTemplate() &&
                          ui.isElementVisible("order.latestDeliveryDate")) { %>
                <tr>
                    <td class="identifier"><isa:translate key="b2b.order.grid.canceldate"/></td>
                    <td class="value"><%= JspUtil.encodeHtml(ui.header.getLatestDlvDate()) %></td>
                </tr>
                <% } %>
            </table> <%-- end class="data" --%>
            <% if (ui.isAccessible) { %>
                <a name="end-table4" title="<isa:translate key="b2b.acc.header.end.defdata"/>"></a>
            <% } %>
            <%-- End Item defaults Data--%>
        </div> <%-- end class="group" --%>
    </div> <%-- class="header-itemdefault"> --%>
    <%-- Additional Order Data --%>
    <% if (ui.isAccessible) { %>
        <a name="#end-table5" title="<isa:translate key="b2b.acc.header.adddata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
    <% } %>
    <div class="header-additional">  <%-- level: sub3 --%>
        <h1 class="area">
            <a class="icon" href="javascript:toggleHeader()" ><img id="addheadericon" src=<%=WebUtil.getMimeURL(pageContext,"mimes/images/open.gif")%> alt="<isa:translate key="status.adddata.icon.detail"/>"  /></a>
            <span><isa:translate key="status.header.adddata"/></span>
        </h1>
        <div class="area" id="addheader" style="display: none">
            <div class="header-misc">
                <h1 class="group"><span><isa:translate key="status.header.group.misc"/></span></h1>
                <div class="group">
                    <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>">
                        <% if (!ui.backendR3 && ui.header.getIncoTerms1()!= null && ui.isElementVisible("order.incoTerms1")) { %>
                        <tr>
                            <td class="identifier"><isa:translate key="ecm.order.disp.incoterms1"/></td>
                            <td class="value"><%= JspUtil.encodeHtml(ui.header.getIncoTerms1()) %>
                                <% if (ui.header.getIncoTerms1().length() > 0 && ui.header.getIncoTerms1Desc().length() > 0) { %>
                                    <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/hint.gif") %>" alt="<%= JspUtil.encodeHtml(ui.header.getIncoTerms1Desc()) %>"/>
                                <% } %>
                                    <% if (ui.isElementVisible("order.incoTerms2")) { %>
                                <%= JspUtil.encodeHtml(ui.header.getIncoTerms2()) %>
                                    <% } %>                                 
                            </td>
                        </tr>
                        <% } %>
                        <% if (ui.isHeaderCampaignListSet() && ui.isElementVisible("order.campaign")) { 
                              for (int headCamp  = 0; headCamp < ui.getNoHeaderCampaignEntries(); headCamp++) { %>
                        <tr>
                            <td class="identifier"><% if (headCamp == 0) { %><isa:translate key="b2b.order.disp.camp"/><% } %></td>
                            <td class="value"><%= JspUtil.encodeHtml(ui.getHeaderCampaignId(headCamp)) %></td>
                            <td class="value">
                            <% if (!ui.isHeaderCampaignValid( headCamp))  { %>
                                <img width="16" height="16" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/error.gif") %>" alt="<isa:translate key="b2b.disp.camp.inv"/>"/>
                            <% } %>
                            </td>
                            <td class="value">
                                <%= JspUtil.encodeHtml(ui.getHeaderCampaignDescs(headCamp)) %>
                           </td>
                        </tr>
                           <% }
                           } %>
                        <%-- display of external reference objects (e.g. vehicle identification numbers) --%>
                        <% if (!ui.isServiceRecall() && ui.header.getExtRefObjectType() != null &&  ui.header.getExtRefObjectType().length() > 0 && ui.isElementVisible("order.extRefObjects")) { %>
                          <tr>
                            <td class="identifier"><%= JspUtil.encodeHtml(ui.header.getExtRefObjectTypeDesc()) %>:</td>
                            <td class="value">
                            <%  ExtRefObjectList extRefObjects = ui.header.getAssignedExtRefObjects();
                                if ( extRefObjects != null && extRefObjects.size() > 0) {
                                  for (int j=0; j < extRefObjects.size(); j++) { %>
                                     <%= JspUtil.encodeHtml(extRefObjects.getExtRefObject(j).getData()) %>
                                     <% if (j < extRefObjects.size()-1) { %>;<% } %>
                                  <% }
                             } %>
                            </td>
                          </tr>
                        <% } %>
                        <%-- external reference numbers --%>  
                        <% if (ui.header.getAssignedExternalReferences() != null && ui.header.getAssignedExternalReferences().size() > 0 && ui.isElementVisible("order.extRefNumbers")) {
                             ExternalReference externalReference;
                             for (int i = 0; i < ui.header.getAssignedExternalReferences().size(); i++) {
                               externalReference = (ExternalReference)ui.header.getAssignedExternalReferences().getExtRef(i);
                        %>      
                               <tr>
                                 <td class="identifier">
                                   <label for="extRefNumberType"><%= externalReference.getDescription() %>:</label>
                                 </td>
                                 <td class="value"> 
                                   <%= JspUtil.encodeHtml(externalReference.getData()) %>
                                 </td>   
                               </tr>
                        <%   }
                           } %>                                                
                        
                    </table> <%-- end class="data" --%>
                </div> <%-- end class="group" --%>
            </div> <%-- end class="header-misc" --%>
            <%-- Payment data --%>
                <%@ include file="/ecombase/paymentinfo.inc.jsp" %>
            <% if (ui.isElementVisible("order.comment")) { %>
            <div class="header-message">
                <h1 class="group"><span><isa:translate key="status.header.group.message"/></span></h1>
                <%-- Data Message --%>
                <div class="group">
                    <table class="message" summary="<isa:translate key="ecm.acc.head.msg.d"/>">
                        <tr>
                            <td class="identifier"><label for="remark"><isa:translate key="b2b.order.details.note"/></label></td>
                            <td class="value"><textarea class="textarea-disabled" id="remark" rows="5" cols="80" readonly="readonly">
                            <%= (ui.hasHeaderText()) ? JspUtil.encodeHtml(ui.header.getText().getText(), new char[] {'\n'}):"" %></textarea></td>
                        </tr>
                    </table>
                </div> <%-- end class="group" --%>
                <% } %>
                <%-- End Data Message --%>
            </div> <%-- end class="header-message" --%>
        </div> <%-- End class="area" --%>
    </div> <%-- class="header-additional"> --%>
    <% if (ui.isAccessible) { %>
        <a name="end-table5" title="<isa:translate key="b2b.acc.header.end.adddata"/>"></a>
    <% } %>
    <%-- Document flow --%>
    <% if (!ui.isHomActivated() && ui.isHeaderDocFlowAvailable()) { %>
        <div class="header-docflow">  <%-- level: sub3 --%>
            <h1 class="group"><span><isa:translate key="status.header.group.docflow"/></span></h1>
            <div class="group">
                <table class="data">
                  <%@ include file="/ecombase/connecteddocuments.inc.jsp" %>
                </table> <%-- end class="data" --%>
            </div> <%-- end class="group" --%>
        </div> <%-- class="header-docflow"> --%>
    <% } %>
    <%-- End Document flow --%>
  </div> <%-- End class="document-header" --%>
  
    <%-- item serach criteria --%>
    <% // must item search criteria for large documents be displayed ?
       if(ui.isLargeDocument()) {
           String noSearchCriteria = (request.getAttribute(ActionConstantsBase.RC_NO_SEARCH_CRITERIA) != null ? (String)request.getAttribute(ActionConstantsBase.RC_NO_SEARCH_CRITERIA) : "");
           String noItemsFound = (request.getAttribute(ActionConstantsBase.RC_NO_ITEMS_FOUND) != null ? (String)request.getAttribute(ActionConstantsBase.RC_NO_ITEMS_FOUND) : "");
           String lowValInvalid = (request.getAttribute(ActionConstantsBase.RC_LOW_VAL_INVALID) != null ? (String)request.getAttribute(ActionConstantsBase.RC_LOW_VAL_INVALID) : "");
           String highValInvalid = (request.getAttribute(ActionConstantsBase.RC_HIGH_VAL_INVALID) != null ? (String)request.getAttribute(ActionConstantsBase.RC_HIGH_VAL_INVALID) : "");
    %>
      <div class="document-item-filter">
      <div class="title">
         <isa:translate key="b2b.orderstatus.largedoc" arg0="<%= String.valueOf(ui.noOfOriginalItems) %>" arg1="<%=String.valueOf(ui.getShopLargeDocNoOfItemsThreshold())%>" />
      </div>
      <div class="separator-top"><span></span></div>

        <form id="search_items" name="search_items" method="post" action='<isa:webappsURL name ="/b2b/documentstatussearchitems.do"/>'>
          <input name="<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>" type="hidden" value="SearchCriteria_B2B_Sales_OrderStatus_Items"/>
          <table border="0" cellspacing="0" cellpadding="0">
                <%-- tbody --%>
                <% if (noSearchCriteria.equals("true") ||
                       noItemsFound.equals("true") ||
                       lowValInvalid.equals("true") ||
                       highValInvalid.equals("true")) { %>
                  <tr>
                    <td colspan="2">
                        <div class="info">
                            <span>
                                <% if (noSearchCriteria.equals("true")) { %>
                                    <isa:translate key="b2b.status.nocriteria"/><br/>
                                <% } %>
                                <% if (noItemsFound.equals("true")) { %>
                                    <isa:translate key="b2b.status.noitemsfound"/><br/>
                                <% } %>
                                <% if (lowValInvalid.equals("true")) { %>
                                    <isa:translate key="b2b.status.lowvalinvalid"/><br/>
                                <% } %>
                                <% if (highValInvalid.equals("true")) { %>
                                    <isa:translate key="b2b.status.highvalinvalid"/><br/>
                                <% } %>

                            </span>
                        </div>
                        <br/>
                    </td>
                  </tr>
                <% } %>
                  <tr style="width:100%">
                    <td>
                      <isa:translate key="b2b.orderstatus.showpos"/>&nbsp;
                    </td>
                    <td>
                        <span title="label shuffler">
                            <select size="1" id ="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY%>" name="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY%>" onChange="setItemPropertyIndex();">
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE%>"<%=ui.getSearchItemProperty().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE) ? "selected" : ""%> ><isa:translate key="b2b.status.prop.none"/></option>
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_PRODUCT%>" <%=ui.getSearchItemProperty().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_PRODUCT) ? "selected" : ""%> ><isa:translate key="b2b.status.prop.prod"/></option>
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT%>" <%=ui.getSearchItemProperty().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT) ? "selected" : ""%> ><isa:translate key="b2b.status.prop.itmposno"/></option>
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_BACKORDER%>" <%=ui.getSearchItemProperty().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_BACKORDER) ? "selected" : ""%> ><isa:translate key="b2b.status.prop.backorder"/></option>
                                <%-- option value="<%=ManagedDocumentLargeDoc.ITEM_PROPERTY_DESC%>" <%=ui.getSearchItemProperty().equals(ManagedDocumentLargeDoc.ITEM_PROPERTY_DESC) ? "selected" : ""%> ><isa:translate key="b2b.status.prop.desc"/></option --%>
                            </select>
                        </span>
                       <span id="lowValue">
                       <input id="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE%>" name="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE%>" class="textinput-middle" value="<%= JspUtil.encodeHtml(ui.getSearchItemPropertyLowValue()) %>" size="15"/>
                       </span>
                       <span id="highValue">
                       <isa:translate key="b2b.status.prop.nointhigh"/>
                       <input id="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_HIGH_VALUE%>" name="<%=ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_HIGH_VALUE%>" class="textinput-middle" value="<%= JspUtil.encodeHtml(ui.getSearchItemPropertyHighValue()) %>" size="15"/>
                       </span>
                       &nbsp;
                        <% if (ui.header.isDocumentTypeOrder()) { %>
                            <% if (ui.isAccessible) { %>
                                <label for="itemStatus"><isa:translate key="b2b.orderstatus.itemstatus"/></label>
                            <% } %>
                            <select size="1" id="<%=ActionConstantsBase.RK_SEARCH_ITEM_STATUS%>" name="<%=ActionConstantsBase.RK_SEARCH_ITEM_STATUS%>" >
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED%>"<%=ui.getSearchItemStatus().equals(ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED)? "selected" : ""%> ><isa:translate key="b2b.status.item.none"/></option>
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_STATUS_OPEN%>" <%=ui.getSearchItemStatus().equals(ManagedDocumentLargeDoc.ITEM_STATUS_OPEN) ? "selected" : ""%> ><isa:translate key="b2b.status.item.open"/></option>
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_STATUS_INPROCESS%>" <%=ui.getSearchItemStatus().equals(ManagedDocumentLargeDoc.ITEM_STATUS_INPROCESS) ? "selected" : ""%> ><isa:translate key="b2b.status.item.inprocess"/></option>
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_STATUS_COMPLETED%>" <%=ui.getSearchItemStatus().equals(ManagedDocumentLargeDoc.ITEM_STATUS_COMPLETED) ? "selected" : ""%> ><isa:translate key="b2b.status.item.completed"/></option>
                                <option value="<%=ManagedDocumentLargeDoc.ITEM_STATUS_DELIVERED%>" <%=ui.getSearchItemStatus().equals(ManagedDocumentLargeDoc.ITEM_STATUS_DELIVERED) ? "selected" : ""%> ><isa:translate key="b2b.status.item.delivered"/></option>
                            </select>
                            <% }
                          else { %>
                          <input type="hidden" id="<%=ActionConstantsBase.RK_SEARCH_ITEM_STATUS%>" name="<%=ActionConstantsBase.RK_SEARCH_ITEM_STATUS%>" value="<%=ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED%>"/>
                       <% } %>
                    </td>
                  </tr>
                  <tr>
                      <td colspan="2">&nbsp;</td>
                  </tr>
                  <tr>
                      <td>&nbsp;</td>
                      <td><a class="button" onclick="searchItems();" href="#"><isa:translate key="b2b.orderstatus.searchpos"/></a></td>
                  </tr>
                  <% if (ui.getNumberOfItemsFound() != ManagedDocumentLargeDoc.NO_OF_FOUND_ITEMS_UNDEFINED) { %>
                  <tr>
                      <td colspan="2">&nbsp;</td>
                  </tr>
                  <tr>
                      <td colspan="2">
                        <% if (ui.getNumberOfItemsFound() <= ui.getShopLargeDocNoOfItemsThreshold()) { %>
                           <isa:translate key="b2b.status.itemsfound" arg0="<%=String.valueOf(ui.getNumberOfItemsFound())%>"/>
                        <% }
                           else { %>
                           <isa:translate key="b2b.status.itemsfound.excd" arg0="<%=String.valueOf(ui.getNumberOfItemsFound())%>" arg1="<%= String.valueOf(ui.getShopLargeDocNoOfItemsThreshold()) %>"/>
                        <% } %>
                      </td>
                  </tr>
                  <%  } %>
                 <!-- /tbody -->
          </table>
        </form>
        <div class="separator-bottom"><span></span></div>
    </div>
    <br />

    <% } else if (ui.getItemNoToShow() != null) { /* END OF   ui.isLargeDocument()*/ %>
        <div id="itemnotoshowmsg" class="document-item-filter">
          <isa:translate key="b2b.status.show.item.only.bl" arg0="<%=ui.getItemNoToShow()%>"/>
          <a href="#" onclick="showAllItems();toggleAllItemDetails('rowdetail_all','rowdetail_', <%= ui.items.size()%>, 0)"><isa:translate key="b2b.status.show.item.only.lk" arg0="<%=ui.getItemNoToShow()%>"/></a>
        </div>
   <% } %>

    <div class="document-items">

    <%-- Document Item Rows --%>
    <% String documentItemStatusKey = ""; %>
    <%-- do not show header for large documents, if now rows are selected --%>
    <% if(!(ui.isLargeDocument() && ui.isSelectedItemGuidTableEmpty())) { %>

    <form name="order_positions" method="post" action='<isa:webappsURL name ="/b2b/documentstatusaddtobasket.do"/>'>
     <input type="hidden" name="configtype" value=""/>
     <% if (ui.isAccessible) { %>
        <a href="#end-table5" title="<isa:translate key="ecm.acc.items.sum"/> <isa:translate key="ecm.acc.tab.size.info" arg0="7" arg1="<%=(String.valueOf(ui.accNumOfItems)) %>"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
     <% } %>
     <input type="hidden" name="docnumber" value="<%= JspUtil.encodeHtml(ui.header.getSalesDocNumber()) %>" />
     <table class="itemlist" summary="<isa:translate key="ecm.acc.items.sum"/>">
        <%-- Items Table Header --%>
        <tr>
            <% if (!ui.existingProcess ) { %>
                <th class="select">
                    <input id="cball" name="cball" type="checkbox" onclick="cbtoggle();" value="ON" title="<isa:translate key="status.sales.cb.checkall"/>"/>
                </th>
            <% } else { %>
                <th class="select"></th>
            <% } %>
            <th class="opener">
               <% int numberOfRows = ui.items.size();
                  int startIndex   = 0; 
                  if (ui.isElementVisible("orderitem.initdetailview")) {
                    defaultOpenItem = true;
                  } else {
                    defaultOpenItem = false;    
                  }%>
               <a class="icon" href='javascript: showAllItems();toggleAllItemDetails("rowdetail_all","rowdetail_", <%= numberOfRows%>, <%= startIndex %>)' >
                 <% if (defaultOpenItem == false) { %>               
                   <img class="img-1" id="rowdetail_allclose" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.close"/> <% } %>"><img class="img-1" id="rowdetail_allopen" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.open"/> <% } %>">
                 <%} else {%>
                   <img class="img-1" id="rowdetail_allclose" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.close"/> <% } %>"><img class="img-1" id="rowdetail_allopen" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.open"/> <% } %>">
                 <%}%>
               </a>
            </th>
            <th class="item" scope="col"><isa:translate key="status.sales.detail.itemposno"/></th>
            <th class="product" scope="col"><isa:translate key="status.sales.detail.product"/></th>
            <th class="qty" scope="col"><isa:translate key="status.sales.detail.itemquantity"/></th>
            <th class="desc" scope="col"><isa:translate key="status.sales.detail.description"/></th>
            <% if (ui.showConfirmedDeliveryDate && ! ui.header.isDocumentTypeOrderTemplate() ) {%>
             <th class="qty-avail" scope="col"><isa:translate key="status.sales.dt.confDelQty"/></th>
             <th class="date-on" scope="col"><isa:translate key="status.sales.detail.confDelDate"/></th>
            <% } %>
            <% if (ui.isContractInfoAvailable()) { %>
             <th class="item" scope="col"><isa:translate key="status.sales.detail.itemcontract"/></th>
            <% } %>
            <th class="price" scope="col"><isa:translate key="status.sales.detail.totalvalue"/><br /><em><isa:translate key="b2b.order.details.price"/></em></th>
            <% if ( ui.showItemStatus() ) { %>
             <th class="status" scope="col"><isa:translate key="status.sales.status"/></th>
            <% } %>
            <% if ( ui.showQtyToDeliver() ) { %>
             <th class="qty-rest" scope="col"><isa:translate key="status.sales.detail.openquantity"/></th>
            <% } %>
        </tr>

        <%-- Items Table Positions --%>
              <% int itemrowno = 0;
                 int line = 0;
                       
                 boolean isGridProduct = false; // required to know whether the subitem is a grid product
              %>
                <% String subinfos_colspan = "11";
                   if (ui.isContractInfoAvailable()) {
                     subinfos_colspan="12";
                   }
                   if ( ui.header.isDocumentTypeOrderTemplate()
                     || ui.header.isDocumentTypeQuotation()) {
                     subinfos_colspan="9";
                   }
                 %>
              <% ui.setItemsToIterate(); 
              Iterator it = ((ItemList)request.getAttribute(ActionConstantsBase.RK_SALES_DOC_STATUS_DETAIL_ITEMITERATOR)).iterator();
              while( it.hasNext() == true) {
                 orderitem = (ItemSalesDoc)it.next();
                 ui.setItem(orderitem);
                 String itemKey = orderitem.getTechKey().getIdAsString();
                 if ( !ui.isBOMSubItemToBeSuppressed() ) { %>
              <% documentItemStatusKey = "status.sales.status.".concat(orderitem.getStatus());

                 itemrowno++;
                 int margin = 0;
                 String substMsgColspan = subinfos_colspan;                 
               %>               
               <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>
               <%//Set it to false, for non-grid product
                  if (!ui.itemHierarchy.isSubItem(orderitem) ||
                      !orderitem.isConfigurable()) {
                    isGridProduct = false;
                  } %>
                  
                <tr class="<%= ui.even_odd%>" id="row_<%=itemrowno%>" <%=(ui.isItemNoToShow(orderitem.getNumberInt()) ? "" : "style=\"display:none\"")%>>
                
                  <%  //Set it true, for main Grid product
                      if ( ui.itemHierarchy.hasSubItems(orderitem) &&
                         ui.itemHierarchy.isRootConfigurable(orderitem) &&
                         (orderitem.getConfigType() != null &&
                         orderitem.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)) ){
                        isGridProduct = true;
                      } %>
                      
                  <% if (orderitem.getProductId() != null ) {%>
                    <td class="select">
                      <% if ( ui.displayCheckboxes(orderitem) && 
                              (!ui.itemHierarchy.isSubItem(orderitem) || !(ui.itemHierarchy.isRootConfigurable(orderitem) || orderitem.isItemUsageBOM())) ) { %>
                        <% if (!ui.existingProcess ) { %>
                            <input name="<%=ActionConstantsBase.RK_CHECKBOXNAME%>[<%=itemrowno%>]" onClick="itemCheckBoxChanged(this);" type="checkbox"/>
                        <% } else { %>
                            <input name="docitemnumberchecked"  type="radio" value="<%= JspUtil.encodeHtml(orderitem.getNumberInt()) %>" onClick="oneSelected=true"/>
                        <% } %>
                      <% } %>
                      <input name="<%=ActionConstantsBase.RK_CHECKBOXHIDDEN%>[<%=itemrowno%>]" type="hidden" value="<%=orderitem.getTechKey()%>"/>
                      <input name="docitemnumber[<%=itemrowno%>]" type="hidden" value="<%= JspUtil.encodeHtml(orderitem.getNumberInt()) %>"/>
                    </td>
                  <% }
                     else { %>
                        <td> </td>
                  <% } %>
                  <td class="opener">
                     <% if (!ui.hideItemDetailButton(orderitem)) { %>
                      <% if (ui.showItemDetailButton() ||  orderitem.deliveryExists()) { %> <%-- do not insert "details symbol" for configurable items on sub-levels --%>
                              <a href='javascript: toggle("rowdetail_<%= line %>")' class="iconlink">
                               <% if (defaultOpenItem == false) { %>                             
                                <img id="rowdetail_<%= line %>close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.order.details.item.close" /><% if (ui.isAccessible) { %><isa:translate key="ecm.acc.button"/> <% } %>" width="16" height="16"/><img id="rowdetail_<%= line %>open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.order.details.item.open" /><% if (ui.isAccessible) { %><isa:translate key="ecm.acc.button"/> <% } %>" width="16" height="16"/>
                              <%} else {%>
                                <img id="rowdetail_<%= line %>close" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.order.details.item.close" /><% if (ui.isAccessible) { %><isa:translate key="ecm.acc.button"/> <% } %>" width="16" height="16"/><img id="rowdetail_<%= line %>open" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.order.details.item.open" /><% if (ui.isAccessible) { %><isa:translate key="ecm.acc.button"/> <% } %>" width="16" height="16"/> 
                              <% } %>                                        
                              </a>
                       <% } %>
                      <% } %> 
                  </td>
                  <td <% if (ui.itemHierarchy.isSubItem(orderitem)) { %> class="item-sub-<%= ui.getItemLevel() -1 %>" <% } else { %> class="item" <% } %>>
                        <% if (ui.isElementVisible("order.item.posNo", itemKey)) { %>
                        <%= JspUtil.encodeHtml(orderitem.getNumberInt()) %>
                        <% } %>
                  </td>
                  <td class="product"><% if (ui.isElementVisible("order.item.product", itemKey)) { %><%= JspUtil.encodeHtml(ui.getProduct()) %><% } %></td>
                  <td class="qty"><% if (ui.isElementVisible("order.item.qty", itemKey)) { %><%= JspUtil.encodeHtml(orderitem.getQuantity()) %>&nbsp;<abbr title="<isa:translate key="status.sales.detail.itemunit"/>"><%= JspUtil.encodeHtml(orderitem.getUnit()) %></abbr><% } %></td>             
                  <td class="desc">
                    <% if (ui.showInternalCatalog) { %>
                              <% String prodId = orderitem.getProductId().getIdAsString(); %>
                              <a href="#" class="iconlink" onclick="showproductincatalog('<%= JspUtil.encodeHtml(prodId) %>');">
                    <% } %>
                    <% if (ui.isElementVisible("order.item.description", itemKey)) { %><%= JspUtil.encodeHtml(orderitem.getDescription().trim()) %><% } %>
                    <% if (ui.showInternalCatalog) { %>
                              </a>
                    <% } %>
                    <%= ui.getSubstitutionReason() %><br /> <var class="icon"></var>
                    <% if (orderitem.isConfigurable()&& ui.isElementVisible("order.item.configuration", itemKey) ) {
                          if (!isGridProduct) { 
                             if (ui.header.getIpcDocumentId() != null) {%>
                               <a href="#" class="icon" onclick="itemconfig('<%=orderitem.getTechKey()%>');">
                                 <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>"
                                 alt="<isa:translate key="status.sales.detail.config.item"/>
                                 <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.config.item"/> <% } %>" />
                               </a>
                            <% } else { %>
                                <a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.config.imp"/>');">
                                          <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
                                           alt="<isa:translate key="status.sales.detail.config.item"/>" />
                                </a>                        
                            <% } %>
                    <%    } else {%>
                            <%if (!ui.itemHierarchy.isSubItem(orderitem)){
                                if (orderitem.getExternalItem() != null) {%>
                                    <a href="#" onclick="griditemconfig('<%= orderitem.getTechKey().getIdAsString() %>');">
                                        <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_grid.gif") %>"
                                            alt="<isa:translate key="b2b.order.display.grid"/>
                                            <% if (ui.isAccessible) { %><isa:translate key="b2b.order.display.grid"/> <% } %>" />
                                    </a>
                              <%}else{ %>
                                    <a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.grid.item.imp"/>');">
                                          <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_grid.gif") %>"
                                            alt="<isa:translate key="b2b.order.display.grid"/>" />
                                    </a>
                            <%  }
                               }  %>
                    <%    }
                       } %>
                    <% if (ui.isItemCampaignAssignedAuto()) { %>
                                              <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/camp_assigned.gif") %>" alt="<isa:translate key="b2b.disp.camp.ass"/>"/>
                                        <% } %>
                  </td>
                  <% if (ui.showConfirmedDeliveryDate && ! ui.header.isDocumentTypeOrderTemplate() ) { %>
                  <td class="qty-avail">
                      <% ArrayList aList = orderitem.getScheduleLines();
                         if (ui.showItemATPInfo() && ui.isElementVisible("order.item.schedulineQty", itemKey)) {
                            for (int i = 0; i < aList.size(); i++) {
                                Schedline scheduleLine = (Schedline) aList.get(i); 
                                if (!scheduleLine.getCommittedQuantity().equals("0")){ /*Note 1045524*/%>
                                <%= JspUtil.encodeHtml(scheduleLine.getCommittedQuantity()) %><br />
                      <%    }
                      	  }
                         } %>
                  </td>
                  <td class="date-on">
                      <% if (ui.showItemATPInfo() && ui.isElementVisible("order.item.schedulineDate", itemKey)) {
                            for (int i = 0; i < aList.size(); i++) {
                               Schedline scheduleLine = (Schedline) aList.get(i); 
                               if (!scheduleLine.getCommittedQuantity().equals("0")){ /*Note 1045524*/%>
                               <%= JspUtil.encodeHtml(scheduleLine.getCommittedDate()) %><br />
                      <%    }
                          }
                         } %>
                  </td>
                  <% } %>
                  <% if (ui.isContractInfoAvailable() && ui.isElementVisible("order.item.contract", itemKey)) { %>
                  <td class="item>"><% if ( (orderitem.getContractId() != null) && !(orderitem.getContractId().equals(""))) { %>
                  <a href="<isa:webappsURL name="b2b/contractselect.do"/>?contractSelected=<%= orderitem.getContractKey() %>"><%= JspUtil.encodeHtml(orderitem.getContractId())%></a>&nbsp;/&nbsp;<%= JspUtil.encodeHtml(orderitem.getContractItemId())%>
                      <% } %>
                  </td>
                  <% } %>
                  <td class="price">
                  <% if (!ui.itemHierarchy.hasATPSubItem(orderitem) && orderitem.isPriceRelevant()) { %>
                    <% if (ui.showIPCpricing == true  && ui.header.getIpcDocumentId() != null && ui.backendR3 == false) { %>
                        <a href="#" onclick="displayIpcPricingConds(  '<%= JspUtil.removeNull(ui.header.getIpcConnectionKey()) %>',
                                                                      '<%= JspUtil.removeNull(ui.header.getIpcDocumentId().getIdAsString()) %>',
                                                                      '<%= JspUtil.removeNull(orderitem.getTechKey().getIdAsString()) %>'
                                                                   )">
                    <% } %>
                    <% if ( ui.isElementVisible("order.item.grossValue", itemKey)) { %><%= ui.getItemValue() %>&nbsp;<%= JspUtil.encodeHtml(orderitem.getCurrency()) %><% } %>
                    <% if (ui.showIPCpricing == true && ui.header.getIpcDocumentId() != null && ui.backendR3 == false) { %>
                        </a>
                    <% } %>
                    <% if (ui.isElementVisible("order.item.netPrice", itemKey) && (orderitem.getNetPriceUnit()!=null) && (!orderitem.getNetPriceUnit().equals(""))){%>
                        <br /><em>
                            <%= JspUtil.removeNull(orderitem.getNetPrice()) + "&nbsp;" + JspUtil.encodeHtml(orderitem.getCurrency()) + "&nbsp;" + '/' + "&nbsp;" + JspUtil.encodeHtml(orderitem.getNetQuantPriceUnit()) + ' ' + JspUtil.encodeHtml(orderitem.getNetPriceUnit()) %>
                            </em>
                    <% } %>
                    </td>
                  <% } else { %>&nbsp;
                    </td>
                  <% } %>
                  <% if ( ui.showItemStatus() ) { %>
                      <td class="status"><% if (ui.isElementVisible("order.item.status", itemKey)) { %><isa:translate key="<%= documentItemStatusKey %>"/><% } %></td>
                  <% } %>
                  <% if (ui.showQtyToDeliver()) { %>
                        <td class="qty-rest"><% if (ui.isElementVisible("order.item.qtyToDeliver", itemKey)) { %><%= ui.getItemQuantityToDeliver()%><% } %></td>
                  <% } %>

                </tr>


                <%-- START of expandable area --%>

                  <%-- Item Details --%>
                  <% if (! ui.hideItemDetailButton(orderitem)) { %>
                  <tr class="<%=ui.even_odd + "-detail"%>" id="rowdetail_<%= line %>"
                  <% if (!ui.getBrowserType().isUnknown() && !ui.getBrowserType().isNav4()) {
                      if (defaultOpenItem == false || !ui.isItemNoToShow(orderitem.getNumberInt())) { %>
                         style="display:none;">
                      <% } else { %> 
                         style="">
                      <% } %>   
                  <% } else { %>
                   >
                  <% } %>
                  <% if (ui.showItemDetailButton() && ! ui.hideItemDetailButton(orderitem)) { %>
                     <td class="select"></td>
                     <td class="detail" colspan="<%=subinfos_colspan%>">
                       <table class="item-detail" summary="<isa:translate key="ecm.acc.item.detail.info"/>">
                         <% if (ui.isProductDeterminationInfoAvailable() && ui.isElementVisible("order.item.systemProductId", itemKey) && ui.isElementVisible("order.itemdetailgroup1")){ %>
                         <tr>
                           <td class="identifier"><isa:translate key="b2b.proddet.sysprodid"/></td>
                           <td class="identifier" colspan="<%= ui. getNonCampItemDetailColspan() %>"><%= JspUtil.encodeHtml(orderitem.getSystemProductId()) %></td>
                         </tr>
                         <% } %>
                         <%-- product configuration --%>
                         <% if (ui.isElementVisible("order.item.configuration", itemKey) && ui.isElementVisible("order.itemdetailgroup1")) { %>
                            <% ItemSalesDoc item = orderitem; %>
                            <%@ include file="/ecombase/orderitemconfiginfo.inc.jsp" %>
                         <% } %>
                         <%-- Shipto --%>
                         <% if (  ui.isElementVisible("order.item.deliverTo", itemKey) && ui.isElementVisible("order.itemdetailgroup1") && orderitem.getShipTo() != null  &&
                                ! orderitem.getShipTo().getShortAddress().equals("") ) { %>
                            <tr>
                              <td class="identifier"><isa:translate key="status.sales.detail.deliveryaddr"/>:</td>
                              <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>"><%= JspUtil.encodeHtml(orderitem.getShipTo().getShortAddress()) %>
                                  <a class="icon" href="#" onclick="showShipTo('<%=orderitem.getShipTo().getTechKey().getIdAsString()%>');" >
                                     <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/>"/></a>
                              </td>
                            </tr>
                         <% } %>
                         <%-- delivery priority --%>
                         <% if (ui.isElementVisible("order.item.deliveryPriority", itemKey) &&
                                orderitem.getDeliveryPriority() != null &&
                                orderitem.getDeliveryPriority().length() > 0 && ui.isElementVisible("order.itemdetailgroup1")) { %>
                            <tr>
                              <td class="identifier"><isa:translate key="status.sales.detail.dlv.prio"/>:</td>
                              <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>"><%= JspUtil.encodeHtml((String) ui.getDeliveryPriorityDescription(orderitem.getDeliveryPriority())) %>
                              </td>
                            </tr>
                         <% } %>                      
                         <%--  Request Delivery Date (for orders only)--%>
                         <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey) && ! ui.header.isDocumentTypeOrderTemplate() && ui.isElementVisible("order.itemdetailgroup1")) { %>
                            <tr>
                              <td class="identifier"><isa:translate key="b2b.order.reqdeliverydate"/></td>
                              <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>"><%= JspUtil.encodeHtml(orderitem.getReqDeliveryDate()) %></td>
                            </tr>
                         <% } %>
                         <%--Item cancel date--%>
                         <% if ( ui.isElementVisible("order.latestDeliveryDate") && ! ui.header.isDocumentTypeOrderTemplate() &&
                                   ui.isElementVisible("order.latestDeliveryDate") && ui.isElementVisible("order.itemdetailgroup1")) { %>
                            <tr>
                                <td class="identifier"><isa:translate key="b2b.order.grid.canceldate"/></td>
                                <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>"><%= JspUtil.encodeHtml(orderitem.getLatestDlvDate()) %></td>
                            </tr>
                         <% } %>                         
                      <%-- campaigns --%>
                      <% if (ui.isElementVisible("order.item.campaign", itemKey) && ui.isItemCampaignListSet() && ui.isElementVisible("order.itemdetailgroup1")) { 
                             for (int itemCamp = 0; itemCamp < ui.getNoItemCampaignEntries(); itemCamp++) { %>
                          <tr>
                            <td class="identifier"><% if (itemCamp == 0) { %><isa:translate key="b2b.order.disp.camp"/><% } %></td>
                            <td class="value"><%= JspUtil.encodeHtml(ui.getItemCampaignId(itemCamp)) %></td>
                            <td class="value">
                             <% if (!ui.isItemCampaignValid(itemCamp))  { %>
                                 <img  width="16" height="16" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/error.gif") %>" alt="<isa:translate key="b2b.disp.camp.inv" arg0="<%= JspUtil.encodeHtml(ui.getItemCampaignId(itemCamp))%>"/>"/>
                             <% } %>
                             <% if (ui.isItemCampaignManuEntered(itemCamp)) { %>
                                 <img  width="16" height="16" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_manCampaignEntry.gif") %>" alt="<isa:translate key="b2b.disp.camp.man"/>"/>
                             <% } %>
                             </td>
                             <td class="value">
                                 <%= JspUtil.encodeHtml(ui.getItemCampaignDescs(itemCamp)) %>
                             </td>
                          </tr>
                        <% } %>
		      <% } %>  
                      <%-- external reference objects --%>
                      <% if (ui.isElementVisible("order.item.extRefObjects", itemKey) && orderitem.getExtRefObjectType() != null &&  orderitem.getExtRefObjectType().length() > 0 && ui.isElementVisible("order.itemdetailgroup1")) { %>
                        <tr>
                            <td class="identifier"><%= JspUtil.encodeHtml(ui.header.getExtRefObjectTypeDesc()) %>:</td>
                            <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>">
                            <%  ExtRefObjectList itemExtRefObjects = orderitem.getAssignedExtRefObjects();
                                if ( itemExtRefObjects != null && itemExtRefObjects.size() > 0) {
                                  for (int j=0; j < itemExtRefObjects.size(); j++) { %>
                                     <%= JspUtil.encodeHtml(itemExtRefObjects.getExtRefObject(j).getData()) %>
                                     <% if (j < itemExtRefObjects.size()-1) { %>;<% } %>
                                  <% }
                             } %>
                            </td>
                        </tr>
                      <% } %>
                      <%-- external reference numbers --%>
                      <% if (ui.isElementVisible("order.item.extRefNumbers", itemKey) && orderitem.getAssignedExternalReferences() != null && orderitem.getAssignedExternalReferences().size() > 0 && ui.isElementVisible("order.itemdetailgroup1")) {
                            ExternalReference externalReference;
                            for (int i = 0; i < orderitem.getAssignedExternalReferences().size(); i++) {
                               externalReference = (ExternalReference)orderitem.getAssignedExternalReferences().getExtRef(i);
                      %>        
                             <tr>
                               <td class="identifier">
                                  <label for="itemExtRefNumberType"><%= externalReference.getDescription() %>:</label>
                               </td>
                               <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>"> 
                                  <%= JspUtil.encodeHtml(externalReference.getData()) %>
                               </td>   
                             </tr>
                      <%    }
                         } %>
                      <%-- payment terms --%>
                      <% if (ui.showItemPaymentTerms() && ui.isElementVisible("order.item.paymentterms", itemKey) && ui.isElementVisible("order.itemdetailgroup1")) { %>
                            <tr>
                               <td class="identifier" scope="row"><isa:translate key="ecm.order.disp.paymentterms"/></td>
                               <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>"><%=JspUtil.encodeHtml(orderitem.getPaymentTermsDesc()) %></td>
                           </tr>
                      <% } %>                             
                      <%-- notes --%>
                      <% if (ui.isElementVisible("order.item.comment", itemKey) && ui.isElementVisible("order.itemdetailgroup1") && orderitem.hasText()) { %>
                      <tr class="message-data">
                        <td class="identifier"><isa:translate key="b2b.order.details.note"/></td>
                        <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>">
                            <%= JspUtil.encodeHtml(orderitem.getText().getText(), new char[] {'\n'}) %>
                            <%-- Hidden Field for check on text copy function --%>
                            <input type="hidden" name="comment[<%= itemrowno %>]" value="<%= orderitem.getText() != null ? JspUtil.encodeHtml(orderitem.getText().getText(), new char[] {'\n'}) : ""%>"/>
                        </td>
                      </tr>
                      <% } %>
                      <% if(ui.isElementVisible("order.item.batchId", itemKey) && orderitem.getBatchID() != null && !orderitem.getBatchID().equals("") && ui.isElementVisible("order.itemdetailgroup1")){ %>
                        <tr>
                          <td class="identifier"><isa:translate key="b2b.order.details.batch.number"/></td>
                          <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>"><%= JspUtil.encodeHtml(orderitem.getBatchID()) %></td>
                        </tr>
                      <% } %>
                       <%-- Batch element info start --%>
                       <% if (ui.isElementVisible("order.item.batchChar", itemKey) && orderitem.getBatchCharListJSP() != null && orderitem.getBatchCharListJSP().size() != 0 && ui.isElementVisible("order.itemdetailgroup1")) {
                            for(int i=0; i<orderitem.getBatchCharListJSP().size(); i++) {
                                inputValue = "";
                                for(int j=0; j<orderitem.getBatchCharListJSP().get(i).getCharValTxtNum(); j++) {
                                    inputValue += orderitem.getBatchCharListJSP().get(i).getCharValTxt(j) + ";";
                                } %>
                               <tr>
                                <% if (orderitem.getBatchCharListJSP().get(i).getCharTxt().equals("")) { %>
                                   <td class="identifier"><%= JspUtil.encodeHtml(orderitem.getBatchCharListJSP().get(i).getCharName()) %>:&nbsp;</td>
                                <% } else { %>
                                   <td class="identifier"><%= JspUtil.encodeHtml(orderitem.getBatchCharListJSP().get(i).getCharTxt()) %>:&nbsp;</td>
                                <% } %>
                                   <td class="value" colspan="<%= ui. getNonCampItemDetailColspan() %>">&nbsp;<%= inputValue %></td>
                               </tr>
                          <% }
                       } %>
                      <%-- Batch element info end --%>
                      <%-- Predecessor info start --%>  
                      <%if (ui.isElementVisible("order.itemdetailgroup2") && (orderitem.getPredecessorList().size() > 0 || orderitem.getSuccessorList().size() > 0)) {%>                                            
                       <tr>                                                 
                          <% itemPredecessorList = orderitem.getPredecessorList();
                             itemSuccessorList = orderitem.getSuccessorList(); %>
                          <%@ include file="/ecombase/itemconnecteddocuments.inc.jsp" %>
                       </tr>  
                      <% } %>                   
                     <%-- Predecessor info end --%>
                   </table>
                   <% } // END of ui.showItemDetailButton() && ! ui.hideItemDetailButton(orderitem) %> 
                  <% } else {   // END of ui.showItemDetailButton() %>
                    <td class="select"></td>
                    <td class="detail" colspan="<%=subinfos_colspan%>">
                  <% } %>    
                  <%-- information, that item is backordered --%>
                  <% if (orderitem.isBackordered()) { %>
                      <tr class="<%= ui.even_odd %>" id="rowinfo_<%=itemrowno%>" <%=(ui.isItemNoToShow(orderitem.getNumberInt()) ? "" : "style=\"display:none\"")%>>
                       <td>&nbsp;</td>
                           <td colspan="<%=subinfos_colspan%>" >
                          <div class="info">
                              <span>
                                  <isa:translate key="b2b.ordr.item.backorder"/>
                              </span>
                          </div>
                       </td>
                      </tr>
                   <% } %>
                   <%-- ********** Seperator between two items ********** --%>
                   <tr>
                    <td colspan="<%= subinfos_colspan %>" class="separator"></td>
                  </tr>
                   <% line++; %>
              <% } 
                 }%>     <%-- ui.isBOMSubItemToBeSuppressed() --%>

     </table>
     <% if (ui.isAccessible) { %>
       <a name="#end-table5" title="<isa:translate key="ecm.acc.item.dat.end"/>"></a>
     <% } %>
    </form>
    <% } // END OF !(ui.isLargeDocument() && ui.isSelectedItemGuidTableEmpty()) %>

    <%-- Document Download handling --%>

    <% if (ui.isOrderDownloadRequired()) { %>
        <form name="orderdownload" method="post" action='<isa:webappsURL name ="/b2b/orderDownload.do"/>'>
            <input type="hidden" name="orderIDS" value="<%=ui.header.getSalesDocNumber()%>" />
            <input type="hidden" name="orderGUIDS" value="<%= ui.header.getTechKey().getIdAsString()%>"/>
            <%-- Field which distinguishes between the single order/mass download of orders --%>
            <input type="hidden" name="orderDetail" value="X"/>
            <div id="nodoc-default">
                <ul>
                    <li>
                        <div>
                            <b><label for="format"><isa:translate key="odrdwnld.odrdetail.title"/></label></b>
                            &nbsp;&nbsp;
                            <select name="format" id="format">
                                <option  value="<%= ActionConstantsBase.ORDDOWN_PDF_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.pdf"/></option>
                                <option  value="<%= ActionConstantsBase.ORDDOWN_CSV_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.csv"/></option>
                                <option  value="<%= ActionConstantsBase.ORDDOWN_XML_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.xml"/></option>
                            </select>
                            &nbsp;&nbsp; <%-- <input type="submit" name="orderdownloadbtn" value="<isa:translate key="odrdwnld.odrdetail.btn"/>" class="green"/> --%>
                            <a href="#" onclick="document.orderdownload.submit();" class="button" name="orderdownloadbtn" id="orderdownloadbtn">
                                <isa:translate key="odrdwnld.odrdetail.btn"/>
                            </a>
                            <%
                                String url ="b2b/order/addOrderToMassDownloadList.do";
                                url+="?orderguid="+ui.header.getTechKey().getIdAsString()+"&operation=add&orderid="+ui.header.getSalesDocNumber()
                                +"&desc="+(ui.header.getDescription()==null?"":ui.header.getDescription())+"&status="+ui.header.getStatus();
                            %>
                            &nbsp;&nbsp;
                            <a href="<isa:webappsURL name ="<%=url%>"/>" target="form_input">
                                <img src="<isa:webappsURL name ="/b2b/mimes/images/download.gif"/>" alt="" title="<isa:translate key="massdwnld.addorderimg.ttip"/>" border="0"/>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </form>
    <% } %>
     </div>

    </div>

    <div id="buttons">
        <%-- Accesskey for the button section. It must be an resourcekey, so that it can be translated. --%>
        <a href="#" title= "Main functions of the document." accesskey="b">
            <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/1X1.gif")%>" alt="" style="display:none" width="1" height="1" />
        </a>
        <ul class="buttons-1">
            <% ResultData navBut = new ResultData( ui.navButtons );
            navBut.beforeFirst();
            while( ! navBut.isLast()) {
                navBut.next();
                if ("LEFT".equals(navBut.getString("ALIGN"))) {
                    String resKey = navBut.getString("NAME");
                    String resKeyTitle = navBut.getString("TITLE");
                    String elemID = ( (navBut.getString("ID") != null && navBut.getString("ID").length() > 0) ? ("id=\"" + navBut.getString("ID") + "\"") : "");
                    String LIelemID = ( (navBut.getString("ID") != null && navBut.getString("ID").length() > 0) ? ("id=\"LI" + navBut.getString("ID") + "\"") : "");
                    String elemDIS = ("true".equals(navBut.getString("DISABLED")) ? "class=\"disabled\"" : "");
                 %>
                    <li <%=LIelemID%> <%=elemDIS%>><a href="<%= navBut.getString("HREF")%>" <%=elemID%>  onclick="<%= navBut.getString("ONCLICK")%>" title="<isa:translate key="<%= resKeyTitle %>"/><% if (ui.isAccessible) { %><isa:translate key="ecm.acc.button"/> <% } %>"><isa:translate key="<%= resKey %>"/></a></li>
            <%  } %>
        <% } %>
        </ul>
        <ul class="buttons-3">
            <% navBut.beforeFirst();
            while( ! navBut.isLast()) {
                navBut.next();
                if ("RIGHT".equals(navBut.getString("ALIGN"))) {
                    String resKey = navBut.getString("NAME");
                    String resKeyTitle = navBut.getString("TITLE");
                    String elemID = ( (navBut.getString("ID") != null && navBut.getString("ID").length() > 0) ? ("id=\"" + navBut.getString("ID") + "\"") : "");
                    String LIelemID = ( (navBut.getString("ID") != null && navBut.getString("ID").length() > 0) ? ("id=\"LI" + navBut.getString("ID") + "\"") : "");
                    String elemDIS = ("true".equals(navBut.getString("DISABLED")) ? "class=\"disabled\"" : "");
                %>
                    <li <%=LIelemID%> <%=elemDIS%>>
                    <% if ("PROCESSTYPESELECTION".equals(navBut.getString("LINKED_ELEMENT"))) { %>
                        <% ResultData orderProcessTypes = new ResultData(ui.getSucProcTypeAllowedList().getTable());
                        orderProcessTypes.beforeFirst();
                     %>
                        <% if (ui.isAccessible) { %>                                           
                            <label for="processtypesSID"><isa:translate key="status.sales.detail.select"/></label>
                        <% } %>
                        <select id="processtypesSID">
                            <option value="no_choice"> <isa:translate key="status.sales.detail.selcho"/> </option>
                            <% while ( ! orderProcessTypes.isLast()) {
                                orderProcessTypes.next();
                            %>
                                <%-- <option value="<%= orderProcessTypes.getString(1) %>"><isa:translate key="status.sales.detail.selquo"/>&nbsp;<%= orderProcessTypes.getString(2) %></option>--%>
                                <option value="<%= orderProcessTypes.getString(1) %>"><%= orderProcessTypes.getString(2) %></option>
                            <% } %>
                        </select>
                    <% } %>
                        <a href="<%= navBut.getString("HREF")%>" <%=elemID%> <%=elemDIS%> onclick="<%= navBut.getString("ONCLICK")%>" title="<isa:translate key="<%= resKeyTitle %>"/><% if (ui.isAccessible) { %><isa:translate key="ecm.acc.button"/> <% } %>"><isa:translate key="<%= resKey %>"/></a></li>
            <% } %>
      <% } %>
    </ul>
   </div>


</body>
</html>
