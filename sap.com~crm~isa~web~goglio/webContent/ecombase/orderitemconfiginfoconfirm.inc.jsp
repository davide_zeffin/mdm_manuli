<%--
********************************************************************************
    File:         orderitemconfiginfoconfirm.inc.jsp
    Copyright (c) 2005, SAP AG
    Author:       SAP AG
    Created:      18.08.2005
    Version:      1.0

    Display of item configuration information in the item details in the confirm.jsp

    Prerequisites: request must contain  RC_CONFIG_INFO attribute
                   request must contain  RC_CONFIG_INFO_DETAIL attribute
                   showDetailView - true(boolean), if detailled information should be displayed

    $Revision: #1 $
    $Date: 2005/19/08$
********************************************************************************
--%>

<%@ page import="com.sap.isa.isacore.uiclass.ItemConfigurationInfoHelper" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sap.spc.remote.client.object.Characteristic" %>
<%@ page import="com.sap.spc.remote.client.object.CharacteristicGroup" %>

<% ItemConfigurationInfoHelper itemConfigInfo = ui.getItemConfigInfoHelper(); 
   char configInfoDetailView = ui.getOrderDetailView();

   if (item.isConfigurable()) { 
       if (showDetailView == false) { 
           if (ui.getItemConfigInfo() != null) { %>
			   <%-- in the short view only the configuration of the rootitem should be displayed --%>
                <tr>
                   <td class="identifier" ><isa:translate key="b2b.prodconfiginfo"/></td>
                   <td class="value" ><%= ui.getItemConfigInfo() %></td>                              
                </tr>
        <% } %>
    <% } 
       else { 
           if (configInfoDetailView != ' ') { %>
                <%-- the detailled view displays the characteristics sorted by groups --%>
                <% ArrayList charGroups = ui.getItemConfigInfoDetail();
                   if (charGroups != null ) {
                   		if (charGroups.size() == 1) {  %>
                   			<%-- group name will not be displayed if there is not more than one group--%>                 	
                   			<% ArrayList characteristics = itemConfigInfo.getCharacteristics((CharacteristicGroup)charGroups.get(0), configInfoDetailView);
                      		Characteristic characteristic;                      
                     		for (int i = 0; i < characteristics.size(); i++) {
   						 		characteristic = (Characteristic) characteristics.get(i); 
   						 		if ( itemConfigInfo.getCharacteristicValues(characteristic).length() > 0 ) {%>
                           			<tr>
                             			<td>
                               				<% if (i == 0) { %>
                               					<%-- first line  --%>
                                   				<isa:translate key="b2b.prodconfiginfo"/></td>
                               				<%  } else { %>
                                   				&nbsp; 
                               				<% } %>
                             			</td> 
                             			<td class="config-cstic"> <span class="name"><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicName(characteristic)) %>:</span>&nbsp;<span class="value"><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicValues(characteristic)) %></span</td>              
                           			</tr>
                         		<% } %>
                      		<% } %>
                   		<% } else { %>
                       		<%  ArrayList characteristics = null;
					        Characteristic characteristic; 
                           	for (int j = 0; j < charGroups.size(); j++) { 
                            	characteristics = itemConfigInfo.getCharacteristics((CharacteristicGroup)charGroups.get(j), configInfoDetailView); %>                
   						    	<tr>
   						        	<%-- group name  --%>
						        	<td>
							        	<% if (j == 0) { %>
                                     		<%-- first line  --%>							        
								       		<isa:translate key="b2b.prodconfiginfo"/></td>
							        	<%  } else { %>
								       		&nbsp; 
							        	<% } %>
						         	</td> 
						         	<td class="config-group">
						            	<span><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicGroupName((CharacteristicGroup)charGroups.get(j))) %></span>
						         	</td>
						      	</tr>
						      	<%-- characteristics  --%>   
                              	<% for (int i = 0; i < characteristics.size(); i++) {
   						        	characteristic = (Characteristic)characteristics.get(i); 
								 	if ( itemConfigInfo.getCharacteristicValues(characteristic).length() > 0 ) { %>
                                   		<tr>
                                     		<td>
                                        		&nbsp; 
                                     		</td> 
                                     		<td class="config-cstic"> <span class="name"><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicName(characteristic)) %>:</span>&nbsp;<span class="value"><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicValues(characteristic)) %></span></td>              
                                   		</tr>
                                 	<% } %>
                              	<% } %>   <%-- end characteristics --%>   
                           	<% } %>   <%-- end group names --%>
                    	<% } %>    <%-- if (charGroups.size()   --%>  
                    <% } %>
                 <% } %>   <%-- if (configInfoDetailView)   --%>            
          <% } %> <%-- if (showDetailView == false) --%>   						                         
<% } %>   <%-- if (item.isconfigurable..   --%>