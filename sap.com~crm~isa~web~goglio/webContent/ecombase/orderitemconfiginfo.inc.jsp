<%--
********************************************************************************
    File:         orderitemconfiginfo.inc.jsp
    Copyright (c) 2004, SAP AG
    Author:       SAP AG
    Created:      13.12.2004
    Version:      1.0

    Display of item configuration information in the item details

    Prerequisites: ui - must point to the OrderUI class
                   item - must point to the current item (ItemSalesDoc)
                   showDetailView - true(boolean), if detailled information should be displayed

    $Revision: #1 $
    $Date: 2004/12/06$
********************************************************************************
--%>
<%@ page import="com.sap.isa.isacore.uiclass.ItemConfigurationInfoHelper" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sap.spc.remote.client.object.Characteristic" %>
<%@ page import="com.sap.spc.remote.client.object.CharacteristicGroup" %>

<%  ItemConfigurationInfoHelper itemConfigInfo = ui.getItemConfigInfoHelper();
    char configInfoView = ui.getOrderView(); 
    char configInfoDetailView = ui.getOrderDetailView();
    char emptyChar = ' ';
%>

<% if (item.isConfigurable() && configInfoView != emptyChar && configInfoDetailView != emptyChar) { 
      itemConfigInfo.setItem(item); 
      if (itemConfigInfo.isConfigurationAvailable()) { 
          if (showDetailView == false) { 
			 if ( configInfoView != ' ' && !(ui.itemHierarchy.isSubItem(item) && item.isItemUsageConfiguration())) { %>
			   <%-- in the short view only the configuration of the rootitem should be displayed --%>
                <tr>
                   <td class="identifier" ><isa:translate key="b2b.prodconfiginfo"/></td>
                   <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
                   <td class="campimg-1"></td>
                   <% } %> 
                   <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>"><%= itemConfigInfo.getItemConfigValues( configInfoView ) %></td>                              
                </tr>
             <% } %>
          <% } else { 
               if (configInfoDetailView != ' ') { %>
                <%-- the detailled view displays the characteristics sorted by groups --%>
                <% ArrayList charGroups = itemConfigInfo.getItemConfigGroups(configInfoDetailView);
                   if (charGroups.size() == 1) {  %>
                   <%-- group name will not be displayed if there is not more than one group--%>                 	
                   <% ArrayList characteristics = itemConfigInfo.getCharacteristics((CharacteristicGroup)charGroups.get(0), configInfoDetailView);
                      Characteristic characteristic;                      
                      for (int i = 0; i < characteristics.size(); i++) {
   						 characteristic = (Characteristic) characteristics.get(i); 
   						 if ( itemConfigInfo.getCharacteristicValues(characteristic).length() > 0 ) {%>
                           <tr>
                             <td>
                               <% if (i == 0) { %>
                               <%-- first line  --%>
                                   <isa:translate key="b2b.prodconfiginfo"/></td>
                               <%  } else { %>
                                   &nbsp; 
                               <% } %>
                             </td> 
                             <td class="config-cstic" colspan="<%= ui.getNonCampItemDetailColspan() %>"> <span class="name"><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicName(characteristic)) %>:</span>&nbsp;<span class="value"><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicValues(characteristic)) %></span</td>              
                           </tr>
                         <% } %>
                      <% } %>
                   <% } else { %>
                       <%  ArrayList characteristics = null;
					       Characteristic characteristic; 
                           for (int j = 0; j < charGroups.size(); j++) { 
                              characteristics = itemConfigInfo.getCharacteristics((CharacteristicGroup)charGroups.get(j), configInfoDetailView); %>                
   						      <tr>
   						         <%-- group name  --%>
						         <td>
							        <% if (j == 0) { %>
                                      <%-- first line  --%>							        
								       <isa:translate key="b2b.prodconfiginfo"/></td>
							        <%  } else { %>
								       &nbsp; 
							        <% } %>
						         </td> 
						         <td class="config-group" colspan="<%= ui.getNonCampItemDetailColspan() %>">
						            <span><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicGroupName((CharacteristicGroup)charGroups.get(j))) %></span>
						         </td>
						      </tr>
						      <%-- characteristics  --%>   
                              <% for (int i = 0; i < characteristics.size(); i++) {
   						         characteristic = (Characteristic)characteristics.get(i); 
								 if ( itemConfigInfo.getCharacteristicValues(characteristic).length() > 0 ) { %>
                                   <tr>
                                     <td>
                                        &nbsp; 
                                     </td> 
                                     <td class="config-cstic" colspan="<%= ui.getNonCampItemDetailColspan() %>"> <span class="name"><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicName(characteristic)) %>:</span>&nbsp;<span class="value"><%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicValues(characteristic)) %></span></td>              
                                   </tr>
                                 <% } %>
                              <% } %>   <%-- end characteristics --%>   
                           <% } %>   <%-- end group names --%>
                    <% } %>    <%-- if (charGroups.size()   --%>  
                 <% } %>   <%-- if (configInfoDetailView)   --%>            
          <% } %> <%-- if (showDetailView == false) --%>   
      <% } %>  <%-- if (!itemConfigInfo.isConfigurationAvailable())   --%>  							                         
<% } %>   <%-- if (item.isconfigurable..   --%>