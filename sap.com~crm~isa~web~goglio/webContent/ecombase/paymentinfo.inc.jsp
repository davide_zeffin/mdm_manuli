<%--
********************************************************************************
    File:         paymentinfo.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      20.12.2004

    !! This JSP is part of the eCommerceBase branch, and with that is has to have
       references only to object available in that branch. !!
    

     Display available payment information of the current order.
     
********************************************************************************
--%>


<%@ page import="com.sap.isa.payment.businessobject.*"%>
<%@ page import="com.sap.isa.payment.backend.boi.*"%>
<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase"%>



<%-- Credit card details Start --%> 
                     <% if (ui.header.getPaymentData() != null &&
                            ui.header.getPaymentData().getPaymentMethods() != null &&
                            ui.header.getPaymentData().getPaymentMethods().size() > 0 &&
                            (ui.isElementVisible("payment.type") ||
                             ui.isElementVisible("payment.card.number") ||
                             ui.isElementVisible("payment.one.card.number"))) { %>
                       <div class="header-payment">
                        <% if (ui.isAccessible) { %>
                          <a name="#end-paymentmaintain" 
	                         title="<isa:translate key="payment.acc.maintenance"/>">
                         </a>
                       <% } %>    
                       <h1 class="group"><span><isa:translate key="payment.type.txt"/></span></h1>    
                       <% if (ui.header.getPaymentData().getPaymentMethods() != null &&
					          ui.header.getPaymentData().getPaymentMethods().get(0) != null &&
                              ui.isElementVisible("payment.type")) { %>   
                       <table class="data">    
                       <tr>   
                         <td class="identifier"><isa:translate key="payment.type.selected"/>:</td>  
                         <td class="value">                                                  
                          <% if (ui.header.getPaymentData().getPaymentMethods().get(0) instanceof Invoice) { %>
                            <isa:translate key="payment.type.invoice"/>
                          <% } else if (ui.header.getPaymentData().getPaymentMethods().get(0) instanceof COD) { %>
                            <isa:translate key="payment.type.cod"/>
                          <% } else if (ui.header.getPaymentData().getPaymentMethods().get(0) instanceof PaymentCCard) { %>
                            <isa:translate key="payment.type.paycard"/>                                     
                           <% } %>
                         </td>   
                       </tr>   
                      </table>                        
                      <% } %>   
                     <div class="group">
                     <%  
					 	if (ui.getPaymentCardList() != null && ui.getPaymentCardList().size() > 0) {
					   		int numberOfCards = ui.getPaymentCardList().size(); 
                      %>                       
                          <% int line = 0;
                             if (numberOfCards == 1) {
                           %>
                           <table class="data" summary="<isa:translate key="payment.acc.maintenance"/>"> 
                            <isa:iterate id="card" name="<%=ActionConstantsBase.RC_PAYMENT_CARDS%>"
                                             type="com.sap.isa.payment.businessobject.PaymentCCard"> 
                              <% if (ui.isElementVisible("payment.card.type") ||
                                     ui.isElementVisible("payment.one.card.type")) {%>
                              <tr> 
                                <td class="identifier"><isa:translate key="payment.card.type"/>:</td>
                                <td class="value"><%=JspUtil.encodeHtml(card.getTypeDescription())%></td>     
                              </tr>
                              <% } %>
                              <% if (ui.isElementVisible("payment.card.holder") ||
                                     ui.isElementVisible("payment.one.card.holder")) {%>
                              <tr>
                                <td class="identifier"><isa:translate key="payment.card.holder"/>:</td>
                                <td class="value"><%=JspUtil.encodeHtml(card.getHolder())%></td>
                              </tr>
                              <% } %>
                              <% if (ui.isElementVisible("payment.card.number") ||
                                     ui.isElementVisible("payment.one.card.number")) {%>
                              <tr>
                                <td class="identifier"><isa:translate key="payment.card.number"/>:</td>
                                <td class="value"><%=card.getNumber()%></td>
                              </tr>
                              <% } %>         
                              <!-- Serialnumber for switch cards --> 
                              <% if (ui.isElementVisible("payment.card.number.suffix") ||
                                     ui.isElementVisible("payment.one.card.number.suffix")) { %>
                              <tr>
                                <td class="identifier"><isa:translate key="payment.card.number.suffix"/>:</td>
                                <td class="value"><%=card.getNumberSuffix()%></td>
                              </tr>    
                              <% } %>                           
                               <% if (ui.isElementVisible("payment.card.validity") ||
                                      ui.isElementVisible("payment.one.card.validity")) { %>         
                              <tr>
                                <td class="identifier"><isa:translate key="payment.card.expd.txt"/></td> 
                                <td class="value">                      
                                <%-- <isa:translate key="payment.card.expd.month"/>:  --%>     
                                <%=card.getExpDateMonth()%>
                                &nbsp;/&nbsp;
                                <%-- <isa:translate key="payment.card.expd.year"/>: --%>
                                <%=card.getExpDateYear()%>
                                </td>   
                             </tr> 
                             <% } %>                          
                             </isa:iterate>
                             </table>
                            <% } else { %>
                            <table class="list-simple" summary="<isa:translate key="payment.acc.card.multiple.maintenance"/>"> 
                              <tr>
                                <% if (ui.isElementVisible("payment.card.type")) { %>
                                <th class="type"><isa:translate key="payment.card.type"/></th>
                                <% } %>
                                <% if (ui.isElementVisible("payment.card.holder")) { %>
                                <th class="holder"><isa:translate key="payment.card.holder"/></th>
                                <% } %>
                                <% if (ui.isElementVisible("payment.card.number")) { %>
                                <th class="number">
                                <isa:translate key="payment.card.number"/>             
                                <% if (ui.isElementVisible("payment.card.number.suffix")) { %>
                                -<isa:translate key="payment.card.number.suffix"/>
                                <% } %>    
                                </th>
                                <% } %>                            
                                <% if (ui.isElementVisible("payment.card.validity")) { %>
                                <th class="validity"><isa:translate key="payment.card.expd.txt"/></th>
                                <% } %>
                                <% if (ui.isElementVisible("payment.card.limit.flag")) { %>
                                <th><isa:translate key="payment.card.limit.flag"/></th>
                                <% } %>
                                <% if (ui.isElementVisible("payment.card.limit.amount")) { %>
                                <th class="amount"><isa:translate key="payment.card.limit.maxamount"/></th>
                                <% } %>                                                              
                               </tr>
                               <isa:iterate id="card" name="<%=ActionConstantsBase.RC_PAYMENT_CARDS%>"
                                             type="com.sap.isa.payment.businessobject.PaymentCCard">
                                <tr> 
                                  <%-- credit card institutes--%>
                                  <% if (ui.isElementVisible("payment.card.type")) { %>
                                  <td class="type"><%=JspUtil.encodeHtml(card.getTypeDescription())%></td>     
                                  <% } %>
                                  <%-- credit card owner --%>  
                                  <% if (ui.isElementVisible("payment.card.holder")) { %>
                                  <td class="holder"><%=JspUtil.encodeHtml(card.getHolder())%></td>
                                  <% } %>
                                  <%-- credit card number --%>
                                  <% if (ui.isElementVisible("payment.card.number")) { %>
                                  <td class="number">
                                  <%=card.getNumber()%>
                                  <%-- credit card serial number --%>
                                  <% if (ui.isElementVisible("payment.card.number.suffix")) { %>
                                  <% if (card.getNumberSuffix() != null &&
                                         card.getNumberSuffix().length() > 0) { %>
                                  -<%=card.getNumberSuffix()%>
                                  <% } %>
                                  <% } %>
                                  </td>
                                  <% } %>                                  
                                  <%-- credit card month --%>
                                  <% if (ui.isElementVisible("payment.card.validity")) { %>       
                                  <td class="validity">        
                                  <%=card.getExpDateMonth()%>
                                  &nbsp;/&nbsp;
                                  <%-- credit card year --%>                                
                                  <%=card.getExpDateYear()%>   
                                  </td>
                                  <% } %>
                                  <%-- credit card auth. limited --%>
                                  <% if (ui.isElementVisible("payment.card.limit.flag")) { %>
                                  <td class="value"><input type="Checkbox" <% if (card.isAuthLimited()) {%> checked="checked" <%}%>  disabled /></td>
                                  <% } %>
                                  <%-- credit card auth. limit amount --%>
                                  <% if (ui.isElementVisible("payment.card.limit.amount")) { %>
                                  <td class="value"><%=card.getAuthLimit()%></td>
                                  <% } %>                                                     
                              </tr> 
                               <% line++; %>
                              </isa:iterate>
                            </table>  
                          <% } %> 
                          <% } %> 
                          <% if (ui.isAccessible) { %>
                            <a name="end-paymentmaintain" 
	                           title="<isa:translate key="payment.acc.maintenance.end"/>">
                            </a>
                          <% } %>	     
                         </div>
                        </div>
                        <% } %>                                                                  
<%-- Credit card details End --%>