<%--
********************************************************************************
    File:         connecteddocuments_acc.inc.jsp
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      08.03.2004
    Version:      1.0

    $Revision: #6 $
    $Date: 2004/07/21 $
********************************************************************************
--%>
<% java.util.List predecessorList = ui.header.getPredecessorList();
   ConnectedDocument predecessor = null;
   String predecessorTypeKey = " ";
   String predecessorHref = " ";
   String predecessorText = "status.sales.predecessor";
   String docType = new String();
 
   for ( int i = 0; i < predecessorList.size(); i++) {
      predecessor = (ConnectedDocument)predecessorList.get(i);
      docType = predecessor.getDocType();
      if(docType == null) {
      	docType = new String();
      }
      if (!ui.isElementVisible("order.pred.".concat(docType))) {
         predecessorList.remove(i);
      }
   }
   if (predecessorList.size() > 1) {
      predecessorText = predecessorText.concat("s");
   }      
   for ( int i = 0; i < predecessorList.size(); i++) {
	  predecessor = (ConnectedDocument)predecessorList.get(i);
      docType = predecessor.getDocType();
	  if(docType == null) {
		docType = new String();
	  }
      predecessorTypeKey = "status.sales.dt.".concat(docType);
      if (predecessor.isDisplayable()) {
          predecessorHref = ("b2b/documentstatusdetailprepare.do?".concat("techkey=".concat(predecessor.getTechKey().getIdAsString()))).concat("&");
          predecessorHref = ((predecessorHref.concat("object_id=")).concat(predecessor.getDocNumber())).concat("&");
          predecessorHref = predecessorHref.concat("objects_origin=").concat("&").concat("objecttype=").concat(docType);
    %>
       <% if (i == 0) { %><tr><td class="doc_flow_ident"><isa:translate key="<%= predecessorText %>"/>:</td><% } %>
       <% if (i == 0) { %><td class="doc_flow_value"><% } %>
          <isa:translate key="<%= predecessorTypeKey %>"/> 
          <a class="icon" href='<isa:webappsURL name="<%= predecessorHref %>"/>'><%= predecessor.getDocNumber()%></a><% if (i != (predecessorList.size() - 1)){ %>,&nbsp;<% } else { %></td></tr><% } %>              
      <%
      } else {
          %>
       <% if (i == 0) { %><td class="doc_flow_ident"><isa:translate key="<%= predecessorText %>"/>:</td><% } %>
       <% if (i == 0) { %><td class="doc_flow_value"><% } %>
          <isa:translate key="<%= predecessorTypeKey %>"/> <%= predecessor.getDocNumber() %>
       <% if (i != (predecessorList.size() - 1)) { %>,&nbsp;<% } else { %></td></tr><% } %>
          <%
	  }
   }
   
   java.util.List successorList = ui.header.getSuccessorList();
   ConnectedDocument successor = null;
   String successorTypeKey = " ";
   String successorHref = " ";
   String successorText = "status.sales.successor";
   for (int i = 0; i < successorList.size(); i++) {
 	  successor = (ConnectedDocument)successorList.get(i);
	  docType = successor.getDocType();
	  if(docType == null) {
		docType = new String();
	  }
      if (!ui.isElementVisible("order.succ.".concat(docType))) {
	     successorList.remove(i);
	  }
   }   
   if (successorList.size() > 1) {
      successorText = successorText.concat("s");
   }
   for (int i = 0; i < successorList.size(); i++) {
       successor = (ConnectedDocument)successorList.get(i);
       docType = successor.getDocType();
	   if(docType == null) {
          docType = new String();
       }
       successorTypeKey = "status.sales.dt.".concat(docType); %>
       <% if (i == 0) { %><tr><td class="doc_flow_ident"><isa:translate key="<%= successorText %>"/>:</td><% } %>
       <% if (i == 0) { %><td class="doc_flow_value"><% } %>
       <isa:translate key="<%= successorTypeKey %>"/>
       <% if (!"".equals(ui.writeHeaderRefOnClickEvent(successor))) { %>
              <a class="icon" href='#' <%=ui.writeHeaderRefOnClickEvent(successor)%> >
       <% } %>
       <%= successor.getDocNumber() %>
       <% if (!"".equals(ui.writeHeaderRefOnClickEvent(successor))) { %></a><% } %>
       <% if (i != (successorList.size() - 1)) { %>,&nbsp;<% } else { %></td></tr><% } %>
<%
   }
%>
   