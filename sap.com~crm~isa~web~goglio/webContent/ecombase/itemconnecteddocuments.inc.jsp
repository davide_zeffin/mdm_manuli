<%--
********************************************************************************
    File:         itemconnecteddocuments.inc.jsp
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      18.09.2006
    Version:      1.0

********************************************************************************
--%>
<%  itemPredecessorList = orderitem.getPredecessorList();   
    itemSuccessorList = orderitem.getSuccessorList();
    ConnectedDocumentItem itemPredecessor = new ConnectedDocumentItem();
    String itemPredecessorTypeKey = " ";
    String itemPredecessorTextKey = "status.sales.predecessor";
    String itemPredecessorsTextKey = "status.sales.predecessors";
    ConnectedDocumentItem itemSuccessor = new ConnectedDocumentItem();
    String itemSuccessorTypeKey = " ";
    String itemSuccessorTextKey = "status.sales.successor";
    String itemSuccessorsTextKey = "status.sales.successors";
   
    if (!ui.isHomActivated()) {
		for ( int i = 0; i < itemPredecessorList.size(); i++) {
		   itemPredecessor = (ConnectedDocumentItem)itemPredecessorList.get(i);
		   if ( itemPredecessor.getDocType() == null ) {
  		   	  continue;
		   }
		   if (!ui.isElementVisible("order.item.pred.".concat(itemPredecessor.getDocType()), itemKey)) {
			  itemPredecessorList.remove(i);
		   }
		}
		if (itemPredecessorList.size() > 0) {
%>
           <tr>
              <td class="doc_flow_ident">
<%               if (itemPredecessorList.size() > 1) {
%>	   	  
                    <isa:translate key="<%= itemPredecessorsTextKey %>"/>:
<%               } else { %>
                    <isa:translate key="<%= itemPredecessorTextKey %>"/>:
<%               } %>	   	  
              </td>
              <td class="doc_flow_value">
                 <table class="doc_flow_value_inner"> 
<%                  for (int i = 0; i < itemPredecessorList.size(); i++) {
                       itemPredecessor = (ConnectedDocumentItem)itemPredecessorList.get(i);
	                   if ( itemPredecessor.getDocType() == null ) {
  	                      continue;
	                   }

                       itemPredecessorTypeKey = "status.sales.dt.".concat(itemPredecessor.getDocType());
%>             
                       <tr class="doc_flow_value_inner">
                          <td class="doc_flow_value_inner"><isa:translate key="<%= itemPredecessorTypeKey %>"/></td>
                          <td class="doc_flow_value_inner">
                
<%                           if (itemPredecessor.isDisplayable() && !"".equals(ui.writeItemRefOnClickEvent(itemPredecessor))) { %>
                                <a class="icon" href='#' <%=ui.writeItemRefOnClickEvent(itemPredecessor)%> >
<%                           } %>
                             <%= itemPredecessor.getDocNumber() %>&nbsp;/&nbsp;<%= itemPredecessor.getPosNumber() %>
<%                           if (!"".equals(ui.writeItemRefOnClickEvent(itemPredecessor))) { %>
                                </a>
<%                           } %>              									 
<%                           if ( ! (itemPredecessor.getDate().equals("0")))   { %>               									 
                                <%= itemPredecessor.getDate() %>
<%                           } %>  
<%                           if ( ! (itemPredecessor.getQuantity().equals("0")))   { %>             									 
                                <%= itemPredecessor.getQuantity() %>
<%                           } %>              									 
<%                           if ( ! (itemPredecessor.getUnit().equals("0")))   { %>
                                <%= itemPredecessor.getUnit() %>
<%                           } %>
                          </td>
                       </tr> 
<%                  } %>
                 </table>
              </td>
           </tr>      
<%      } // end: if (itemPredecessorList.size() > 0) %>
<%
		for (int i = 0; i < itemSuccessorList.size(); i++) {
		   itemSuccessor = (ConnectedDocumentItem)itemSuccessorList.get(i);
		   if ( itemSuccessor.getDocType() == null ) {
			  continue;
		   }

		   if (!ui.isElementVisible("order.item.succ.".concat(itemSuccessor.getDocType()), itemKey)) {
			  itemSuccessorList.remove(i);
		   }
		}	
        if (itemSuccessorList.size() > 0) {
%>
          <tr>
            <td class="doc_flow_ident">
<%             if (itemSuccessorList.size() > 1) { %>	          
                  <isa:translate key="<%= itemSuccessorsTextKey %>"/>:
<%             } else { %>
                  <isa:translate key="<%= itemSuccessorTextKey %>"/>:
<%             } %>	   	            
            </td>	
            <td class="doc_flow_value">
               <table class="doc_flow_value_inner" summary="<isa:translate key="ecm.acc.item.docflow.info"/>"> 	
<%             for ( int i = 0; i < itemSuccessorList.size(); i++) {
                  itemSuccessor = (ConnectedDocumentItem)itemSuccessorList.get(i);
                  if(itemSuccessor.getDocType() == null) {
                  	continue;
                  }
                  itemSuccessorTypeKey = "status.sales.dt.".concat(itemSuccessor.getDocType());
%>          
                  <tr class="doc_flow_value_inner">
	                  <td class="doc_flow_value_inner"><isa:translate key="<%= itemSuccessorTypeKey %>"/></td>
	                  <td class="doc_flow_value_inner"> 
	                
	<%                   if (!"".equals(ui.writeItemRefOnClickEvent(itemSuccessor))) { %>
	                       <a class="icon" href='#' <%=ui.writeItemRefOnClickEvent(itemSuccessor)%> >
	<%                   } %>                                                   
	                     <%= itemSuccessor.getDocNumber() %>&nbsp;/&nbsp;<%= itemSuccessor.getPosNumber() %>
	<%                   if (!"".equals(ui.writeItemRefOnClickEvent(itemSuccessor))) { %>
	                        </a>
	<%                   } %>                                                    
	<%                   if ( itemSuccessor.getDocType().equals("DLVY") || itemSuccessor.getDocType().equals("delivery") ) {%>                                                   
	<%                      if ( ! ((itemSuccessor.getDate().equals("0")) || (itemSuccessor.getDate().equals(" "))))   { %>       
    <%                          if (!ui.backendR3)  { %>	                            																     
	                               ( <isa:translate key="status.delivery.date"/>:&nbsp;<%= itemSuccessor.getDate() %>,&nbsp;
    <%                          } else { %>
                                    <% /** In ERP scenarios only the creation date is available, not the delivery date! **/ %>
                                   ( <isa:translate key="connected.item.dlvr.creat.date"/>:&nbsp;<%= itemSuccessor.getDate() %>,&nbsp;    
    <%                          } %>
	<%                       } %> 
	<%                      if ( ! (itemSuccessor.getQuantity().equals("0")))   { %>             																     
	                           <isa:translate key="status.delivery.quantity"/>:&nbsp;<%= itemSuccessor.getQuantity() %>
	<%                      } %>     
	<%                      if ( ! (itemSuccessor.getUnit().equals("0")))   { %>                                                                          
	                           <%= itemSuccessor.getUnit() %>                                                   
	<%                      } %>
	<%                      if ( ! (itemSuccessor.getTrackingURL().equals("")))   { %>  
	                           <a href="#" onclick="trackwin('<%= itemSuccessor.getTrackingURL() %>'); return false;">
	                              <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/truck.gif")%>" alt="<isa:translate key="status.sales.detail.del.tracking"/>" width="17" height="17"/>
	                           </a>                                                                                                                                               
	<%                      } %>) 
	<%                   } %> 
	                  </td>
                  </tr>                                                                             
<%             } %>
               </table>
           </td>
          </tr>      
<%      } // end: if (itemSuccessorList.size() > 0) %>     
<%  } // emd: if (!ui.isHomActivated()) %>
   