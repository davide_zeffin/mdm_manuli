<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionsForm" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>
<jsp:useBean id="auctionsForm" scope="session"
class="com.sap.isa.auction.actionforms.buyer.AuctionsForm" />
<isa:contentType />

<%
	String auctionsTitle = auctionsForm.getPageTitle();
	UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());
%>

	<div class="module-name"><isa:moduleName name="/auction/buyer/auctionsforproduct.inc.jsp" /></div>
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="iViewHeader" width="10"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headline_header_small.gif") %>" width="5" height="17" alt="" border="0"></td>
			<td class="iViewHeader">
				<%=auctionsTitle%>&nbsp;<%=auctionsForm.getProduct().getDescription()%>
			</td>
		</tr>
		<tr>
			<td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="5" alt="" border="0"></td>
		</tr>
	</table>

	 <table border="0">
		<tbody>
		  <tr>
			 <% userSessionData.setAttribute("item" , auctionsForm.getProduct());%>
				<td align="left">
					<img src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="item" defaultImg="mimes/shared/no_thumb.gif"/>" width=60 height=60>
				</td>
				<td>
			  	   <%=auctionsForm.getProduct().getDescription()%>
				</td>
		 </tr>
		</tbody>
	  </table>
	  <jsp:include page="auctionlist.jsp" flush="true" />