<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.auction.bean.b2x.PrivateAuction" %>
<%@ page import="com.sap.isa.auction.bean.AuctionStatusEnum" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionsForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionBaseForm" %>
<%@ page import="com.sap.isa.auction.bean.search.QueryResult" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/ehtmlb" prefix="ehbj" %>

<jsp:useBean id="auctionsForm" scope="session"
class="com.sap.isa.auction.actionforms.buyer.AuctionsForm" />

<div class="module-name"><isa:moduleName name="/auction/buyer/auctionlist.jsp" /></div>
	
<%
	auctionsForm.preprocessing();
    String url = WebUtil.getAppsURL(pageContext , Boolean.FALSE ,
        "/buyer/auctiondetails.do" +  AuctionsForm.AUCTIONID + "=", null , null , false);
	String startPriceTxt = auctionsForm.translate(AuctionBaseForm.STARTPRICETEXT);
	String myLastBidTxt = auctionsForm.translate(AuctionBaseForm.MYLASTBIDTEXT);
	String currentBidTxt = auctionsForm.translate(AuctionBaseForm.CURRENTBIDTEXT);
	String winningBidTxt = auctionsForm.translate(AuctionBaseForm.WINNINGBIDTEXT);
	String finalBidTxt = auctionsForm.translate(AuctionBaseForm.FINALBIDTEXT);
    String buyNowPriceTxt = auctionsForm.translate(AuctionBaseForm.BUYNOWPRICETEXT);
	String timeLeftTxt = auctionsForm.translate(AuctionBaseForm.TIMELEFTTEXT);
	String nextPageTxt = auctionsForm.translate(AuctionBaseForm.NEXTTEXT);
	String prevPageTxt = auctionsForm.translate(AuctionBaseForm.PREVIOUSTEXT);
	String noImageTxt = auctionsForm.translate(AuctionBaseForm.NOIMAGETEXT);
	String unitsTxt = auctionsForm.translate(AuctionBaseForm.UNITSTEXT);
	String quantityTxt = auctionsForm.translate(AuctionBaseForm.QUANTITYTEXT);
	String showPageTxt = auctionsForm.translate(AuctionBaseForm.SHOWPAGETEXT);
	String showItemsTxt = auctionsForm.translate(AuctionBaseForm.SHOWTEXT);
	String fiveItemsTxt = auctionsForm.translate(AuctionBaseForm.FIVEITEMSTEXT);
	String tenItemsTxt = auctionsForm.translate(AuctionBaseForm.TENITEMSTEXT);
	String fifteenItemsTxt = auctionsForm.translate(AuctionBaseForm.FIFTEENITEMSTEXT);
%>

    <form id="auctionsForProducts" action="<isa:webappsURL name="/buyer/showauctions.do"/>" method="post">
      <input type="hidden" name="<%=AuctionsForm.NEXTPAGE%>" id="<%=AuctionsForm.NEXTPAGE%>" value="<%=(auctionsForm.getEndIndex()+1)%>">
      <input type="hidden" name="<%=AuctionsForm.PREVIOUSPAGE%>" id="<%=AuctionsForm.PREVIOUSPAGE%>" value="<%=(auctionsForm.getStartIndex()-auctionsForm.getPageSize())%>">
	  <input type="hidden" name="<%=AuctionsForm.PAGESIZE%>" id="<%=AuctionsForm.PAGESIZE%>" value="<%=auctionsForm.getPageSize()%>">
	  <input type="hidden" name="<%=AuctionsForm.STATUSOPTIONTEXT%>" id="<%=AuctionsForm.STATUSOPTIONTEXT%>" value="<%=auctionsForm.getRcStatus()%>">
	  <input type="hidden" name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" id="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=auctionsForm.getContextInfo()%>" >
	  <input type="hidden" name="<%=AuctionsForm.AUCTIONID%>" id="<%=auctionsForm.AUCTIONID%>" >
	  <input type="hidden" name="<%=AuctionsForm.PRODUCTID%>" id="<%=auctionsForm.PRODUCTID%>" value="<%=auctionsForm.getProductId()%>">
	  <input type="hidden" name="<%=AuctionsForm.STARTINDEX%>" id="<%=auctionsForm.STARTINDEX%>" value="">
	  <input type="hidden" name="<%=AuctionsForm.SHOWPAGEPARAMETER%>" id="<%=auctionsForm.SHOWPAGEPARAMETER%>" value="<%=auctionsForm.getCurrentPage()%>">
	  <input type="hidden" name="<%=AuctionsForm.NAVIGATION%>" id="<%=auctionsForm.NAVIGATION%>" value="">
      <table border="0" width="100%">
	  <tr>
		<td>&nbsp;&nbsp;
		</td>
		<td align="right" nowrap>
			<%  if(auctionsForm.getResultSize() > 5)  {  %>
			<span><%=showItemsTxt%></span>
			<select onChange="_auction_setFetchSize()" id="_fetchSize" name="_fetchSize">
				<option <%=(auctionsForm.getPageSize() == 5 ? "selected" : "")%>><%=fiveItemsTxt%></option>
				<option <%=(auctionsForm.getPageSize() == 10 ? "selected" : "")%>><%=tenItemsTxt%></option>
				<option <%=(auctionsForm.getPageSize() == 15 ? "selected" : "")%>><%=fifteenItemsTxt%></option>
			</select>
			<%  }  %>
			&nbsp;
			<%  if(auctionsForm.getNumberOfPages() > 1)  {  %>
			<span><%=showPageTxt%></span>
			<select onChange="_auction_fetchPage()" id="_fetchPage" name="_fetchPage">
				<% for(int pageIndex = 1 ; pageIndex <= auctionsForm.getNumberOfPages(); pageIndex++)  {  %>
				<option <%=(auctionsForm.getCurrentPage() == pageIndex ? "selected" : "")%>><%=Integer.toString(pageIndex)%></option>
				<%  }  %>
			</select>
			<%  }  %>
			&nbsp;
		</td>
	  </tr>

	  </tr>
      <tr>
      <td>&nbsp;&nbsp;
      </td>
      <td>
      <table class="poslist" border="1" cellspacing="0" cellpadding="3" width="95%">

<%  
	String[] trEvenOdd = new String[] { "", "" };
	String tdEvenOdd[] = new String[] { "poslist-pos-odd", "poslist-pos-even" };
	
	int index = auctionsForm.getStartIndex();
	int pageSize = auctionsForm.getPageSize();
	
	PrivateAuction auction = null;
	int i = 0;
	int endIndex = auctionsForm.getEndIndex();
	BigDecimal bidValue = null;
	String reserveTxt = null;
	java.util.Date date = java.util.Calendar.getInstance().getTime();
	java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
	int countShownAuctions = 0;
	for(i = index ; i <= endIndex; i++)  {
		auction = auctionsForm.getAuctionByIndex(i);
		int status = auction.getStatus();
		if((status == AuctionStatusEnum.toInt(AuctionStatusEnum.PUBLISHED)) && auction.getEndDate().before(timestamp))
			continue;
		countShownAuctions++;
%>
		<tr class="<%= trEvenOdd[i % 2] %>">
			<td align="left" valign="top" class="<%= tdEvenOdd[i % 2]%>">
			
			</td>
			<td align="left" valign="top" class="<%= tdEvenOdd[i % 2]%>">

			<table width="100%">
			<thead>
				<th class="poslist" colSpan="2">
					<%=(i)%>&nbsp;
					<a href="#" onclick=_fetchAuction('<%=Integer.toString(i)%>') alt="<%=auction.getAuctionTitle()%>">
					<%=auction.getAuctionTitle()%>
					</a>
				</th>
			</thead>
			<tr>
			<td width="70">
				<% if(auction.getImageURL() != null && !auction.getImageURL().equals(""))  {  
						String imageURL = auction.getImageURL();
						if(!imageURL.startsWith("http"))  {
							imageURL = WebUtil.getMimeURL(pageContext, imageURL);
						}
				%>
					<a href="#" onclick=_fetchAuction('<%=Integer.toString(i)%>')>
					<img src="<%=imageURL%>" width=60 height=60 alt="<%=auction.getAuctionTitle()%>">
					</img></a>
				<% }  else  { %>
					<img src="<%=WebUtil.getMimeURL(pageContext,"/auction/images/noimage.jpg")%>" width=60 height=60 alt="<%=noImageTxt%>">
					</img>
				<% }  %>
			</td>

			<td valign="top">
				<table width="350">
					<tr>
						<td>
							<strong><%=startPriceTxt%></strong>						
						</td>
						<td align="right">
							<%=auctionsForm.getDisplayCurrencyValue(auction.getStartPrice())%>
						</td>
						<td>&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td>
							<%if(status == AuctionStatusEnum.toInt(AuctionStatusEnum.PUBLISHED)){%>
								<strong><%=currentBidTxt%></strong>
							<%} else if (status == AuctionStatusEnum.toInt(AuctionStatusEnum.CLOSED)
								|| status == AuctionStatusEnum.toInt(AuctionStatusEnum.NOT_FINALIZED)) {%>
								<strong><%=finalBidTxt%></strong>
							<%} else if (status == AuctionStatusEnum.toInt(AuctionStatusEnum.FINALIZED)
								|| status == AuctionStatusEnum.toInt(AuctionStatusEnum.FINISHED)) {%>
								<strong><%=winningBidTxt%></strong>
							<%}%>
						</td>
						<%
							bidValue = auctionsForm.getHighestBid(auction);
							String currentBid = "-";
							reserveTxt = "( " +auctionsForm.getReservePriceInfoText(auction , bidValue)+ " )";
							if (!bidValue.equals(new BigDecimal(0.0))) {
								currentBid = auctionsForm.getDisplayCurrencyValue(bidValue);
							}
						%>
						<td align="right" nowrap>
							<strong><%=currentBid%></strong>
						</td>
						<td nowrap>&nbsp;<%=reserveTxt%>&nbsp;
						</td>
					</tr>
					<% if(auction.getBuyItNowPrice() != null && auction.getBuyItNowPrice().doubleValue() > 0.0)  {  %>
					<tr>
						<td>
							<strong><%=buyNowPriceTxt%></strong>
						</td>
						<td align="right">
							<%=auctionsForm.getDisplayCurrencyValue(auction.getBuyItNowPrice())%>
						</td>
						<td>&nbsp;&nbsp;</td>
					</tr>
					<%  }  %>
					<tr>
						<td>
							<strong><%=quantityTxt%></strong>
						</td>
						<td align="right" nowrap>
							<%=auction.getQuantity()%>&nbsp;(<%=unitsTxt%>)
						</td>
						<td>&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td colSpan="3">
							
						</td>
					</tr>
					<tr>
						<td>
							<strong><%=timeLeftTxt%></strong>
						</td>
						<td align="right">
							<%=auctionsForm.getTimeLeft(auction)%>
						</td>
						<td>&nbsp;&nbsp;</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
			</td>
		</tr>

<%
	}
%>

        </table>
        </td>
        </tr>
        <tr>
            <td align="left" valign="middle" class="<%= tdEvenOdd[i % 2]%>">
            &nbsp;
            </td>
            <td align="right">
<%  if(index > 1)  {  %>
                <input class="head" type="button" onclick="fetchPage('<%=(auctionsForm.getCurrentPage() - 1)%>')" value="<%=prevPageTxt%>">
<%  }  
	if(endIndex < auctionsForm.getResultSize())  {  %>
                <input class="head" type="button" onclick="fetchPage('<%=(auctionsForm.getCurrentPage() + 1)%>')" value="<%=nextPageTxt%>">
<%  }  %>
            </td>
        </tr>
      </table>

<%--  if(countShownAuctions == 0)  {  %>
	<span><strong>No Auctions available</strong></span>
<%  }  --%>
<script language="Javascript">
	function fetchPage(index)  {
		_auction_fetchPage(index);
	}


	function _fetchAuction(id)  {
		var idInp = document.getElementById("<%=AuctionsForm.AUCTIONID%>");
	    if(idInp != null)
			idInp.value = id;
		if(document.getElementById("auctionsForProducts") != null)  {
			document.getElementById("auctionsForProducts").submit();
		}
	}

	function _auction_setFetchSize()  {
		var fetchSize = document.getElementById("_fetchSize");
		var fetchSizeParameter = document.getElementById("<%=AuctionsForm.PAGESIZE%>");
		var fetchPageParameter = document.getElementById("<%=AuctionsForm.SHOWPAGEPARAMETER%>");
		if(fetchPageParameter != null)  {
			fetchPageParameter.value = null;
		}
		if(fetchSize != null  && fetchSizeParameter != null)  {
			fetchSizeParameter.value = fetchSize.selectedIndex;
		}
		var naviInp = document.getElementById("<%=AuctionsForm.NAVIGATION%>");
		if(naviInp != null)
			naviInp.value = "<%=AuctionsForm.NAVIGATION%>";
		if(document.getElementById("auctionsForProducts") != null)  {
			document.getElementById("auctionsForProducts").submit();
		}
	}

	function _auction_fetchPage(pageNumber)  {
		var fetchPageParameter = document.getElementById("<%=AuctionsForm.SHOWPAGEPARAMETER%>");
		if(pageNumber == null)  {
			var fetchPage = document.getElementById("_fetchPage");
			if(fetchPage != null  && fetchPageParameter != null)  {
				fetchPageParameter.value = fetchPage.selectedIndex + 1;
			}
		}  else  {
			fetchPageParameter.value = pageNumber;
		}
		
		var fetchSizeParameter = document.getElementById("<%=AuctionsForm.PAGESIZE%>");
		fetchSizeParameter.value = null;
		var naviInp = document.getElementById("<%=AuctionsForm.NAVIGATION%>");
		if(naviInp != null)
			naviInp.value = "<%=AuctionsForm.NAVIGATION%>";
		if(document.getElementById("auctionsForProducts") != null)  {
			document.getElementById("auctionsForProducts").submit();
		}
	}

<%
	auctionsForm.postProcessing();
%>
</script>
      </form>