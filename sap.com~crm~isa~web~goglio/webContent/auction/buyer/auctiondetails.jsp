<%@ page import="com.sap.isa.auction.bean.b2x.PrivateAuction" %>
<%@ page import="com.sap.isa.auction.bean.AuctionStatusEnum" %>
<%@ page import="com.sap.isa.auction.bean.Bid" %>
<%@ page import="com.sap.isa.auction.bean.search.QueryResult" %>
<%@ page import="com.sap.isa.auction.bean.AuctionItem" %>
<%@ page import="com.sap.isa.auction.bean.Winner" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionDetailsForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionBaseForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionsForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.CheckoutAuctionsForm" %>
<%@ page import="com.sap.isa.auction.util.Utilidad" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.math.BigDecimal" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/ehtmlb" prefix="ehbj" %>
<html>

<head>
     <isa:includes/>
	<isa:contentType/>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/docnav.js") %>"
          type="text/javascript">
    </script> 
	<script type="text/javascript" language="JavaScript1.2">
   	function changeDocument() {
     	var changeAllowed = parent.documents.editableDocumentAlert('<isa:translate key="b2b.onedocnav.onlyonedoc"/>\n\n<isa:translate key="b2b.onedocnav.onlyonedoc2"/>\n\n<isa:translate key="b2b.onedocnav.onlyonedoc3"/>')
     	if (changeAllowed) {
            document.checkoutForm.submit();
            return true;
     	}
     	else return false;
     }	
 	</script>       
</head>
<body class1="organizercatalog">
    <ehbj:htmlPageLoading/>
	<div class="module-name"><isa:moduleName name="/auction/buyer/auctiondetails.jsp" /></div>
<jsp:useBean id="auctionDetailsForm" scope="session"
class="com.sap.isa.auction.actionforms.buyer.AuctionDetailsForm" />

<%
	auctionDetailsForm.preprocessing();
	String auctionTxt = auctionDetailsForm.getPageTitle();
	String detailsTxt = auctionDetailsForm.translate(AuctionDetailsForm.DETAILSTITLETEXT);;
	String startPriceTxt = auctionDetailsForm.translate(AuctionBaseForm.STARTPRICETEXT);
	String myLastBidTxt = auctionDetailsForm.translate(AuctionBaseForm.MYLASTBIDTEXT);
	String currentBidTxt = auctionDetailsForm.translate(AuctionBaseForm.CURRENTBIDTEXT);
	String winningBidTxt = auctionDetailsForm.translate(AuctionBaseForm.WINNINGBIDTEXT);
	String finalBidTxt = auctionDetailsForm.translate(AuctionBaseForm.FINALBIDTEXT);
    String buyNowPriceTxt = auctionDetailsForm.translate(AuctionBaseForm.BUYNOWPRICETEXT);
	String timeLeftTxt = auctionDetailsForm.translate(AuctionBaseForm.TIMELEFTTEXT);
	String endTimeTxt = auctionDetailsForm.translate(AuctionBaseForm.ENDTIMETEXT);
	String nextPageTxt = auctionDetailsForm.translate(AuctionBaseForm.NEXTTEXT);
	String prevPageTxt = auctionDetailsForm.translate(AuctionBaseForm.PREVIOUSTEXT);
	String noImageTxt = auctionDetailsForm.translate(AuctionBaseForm.NOIMAGETEXT);
	String unitsTxt = auctionDetailsForm.translate(AuctionBaseForm.UNITSTEXT);
	String quantityTxt = auctionDetailsForm.translate(AuctionBaseForm.QUANTITYTEXT);
	String totalBidsTxt = auctionDetailsForm.translate(AuctionDetailsForm.NUMOFBIDS);
	String refreshBtnTxt = auctionDetailsForm.translate(AuctionDetailsForm.REFRESHBUTTONTEXT);
	
	String nextMinimumBidTxt = auctionDetailsForm.translate(AuctionDetailsForm.NEXTMINBIDTEXT);
	String minBidMsgTxt = auctionDetailsForm.translate(AuctionDetailsForm.MINBIDMESSAGETEXT);
	String bidTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDTEXT);
	String bidBtnTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDBUTTONTEXT);
	String bidAmountTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDAMOUNTTEXT);
	String buynowTxt = auctionDetailsForm.translate(AuctionDetailsForm.BUYNOWTEXT);
	String bidQtyTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDQUANTITYTEXT);

	String noBidsYetTxt = auctionDetailsForm.translate(AuctionDetailsForm.NOBIDSYETTEXT);
	String bidderTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDDERTEXT);
	String bidsAmountTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDDERAMOUNTTEXT);
	String bidQuantityTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDDERQUANTITYTEXT);
	String bidDateTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDONTEXT);

	String productsTxt = auctionDetailsForm.translate(AuctionDetailsForm.PRODUCTSTEXT);
	String bidHistoryTxt = auctionDetailsForm.translate(AuctionDetailsForm.BIDHISTORY);
	String termsAndCondTxt = auctionDetailsForm.translate(AuctionDetailsForm.TERMSANDCONDTEXT);
	String bidAlertMessageTxt = auctionDetailsForm.translate(AuctionDetailsForm.DOYOUWANTBIDTEXT);
	String buyNowAlertMessageTxt = auctionDetailsForm.translate(AuctionDetailsForm.DOYOUWANTTOBUYNOWTEXT);
	String checkoutMessageTxt = auctionDetailsForm.translate(AuctionDetailsForm.CHECKOUTREMINDER);
	String checkedoutTxt = auctionDetailsForm.translate(AuctionDetailsForm.CHECKEDOUTMSG);
	String unitTxt = auctionDetailsForm.translate(AuctionDetailsForm.UNITTEXT);
	String forTxt = auctionDetailsForm.translate(AuctionDetailsForm.FORTEXT);
	String perUnitTxt = auctionDetailsForm.translate(AuctionDetailsForm.PERUNITTEXT);
	String checkoutTxt = auctionDetailsForm.translate(AuctionDetailsForm.CHECKOUTTEXT);
	String orderTxt = auctionDetailsForm.translate(AuctionDetailsForm.ORDERTEXT);
%>

            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="iViewHeader" width="10"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headline_header_small.gif") %>" width="5" height="17" alt="" border="0"></td>
                    <td class="iViewHeader">
					<%  if(!auctionDetailsForm.isInvitation())  {  %>
					<a href="<isa:webappsURL name="/buyer/showauctions.do"><isa:param name="<%=AuctionsForm.PRODUCTID%>" value="<%=auctionDetailsForm.getProductId()%>"/></isa:webappsURL>" alt="<%=auctionTxt%>"><span nowrap>&nbsp;<%=auctionTxt%>&nbsp;</a>
					>&nbsp;
					<%  }  %><%=detailsTxt%>&nbsp;</var></span>
                    </td>
                </tr>
                <tr>
                    <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="5" alt="" border="0"></td>
                </tr>
            </table>

    <table class1="headerWhite" border="0" width="100%" cellpadding="0" cellspacing="0">
	<%  if(auctionDetailsForm.getErrors() != null)  {  %>
	<tr>
		<td class="bodylight" align="left" colSpan="2" width="100%">
		<br>
		<UI>
		<%
			
			Iterator iterate = auctionDetailsForm.getErrors().iterator();
			while(iterate.hasNext())  {
		%>
			<li><%=(String)iterate.next()%>
			</li>
		<%

			}
		%>
		</UI>
		</td>
	</tr>
	<%  }  %>
<%
	PrivateAuction auction = auctionDetailsForm.getAuction();
	boolean openForBidding = auctionDetailsForm.isOpenForBidding(auction);
	boolean showCheckoutBtn = false;
	Winner winner = null;
	if (!openForBidding) {
		winner = auctionDetailsForm.getWinner();
		if(winner != null && !auctionDetailsForm.isCheckedOut(winner))
			showCheckoutBtn = true;
	}
%>
	<% if (showCheckoutBtn) {%>
		<tr>
    		<td class="bodylight" colspan="2">
    			<br>
				<%=checkoutMessageTxt%>  
    		</td>
    	</tr>
	<%}%>
	<%if(!openForBidding && winner != null && auctionDetailsForm.isCheckedOut(winner)) {%>
		<tr>
    		<td class="bodylight" colspan="2">
    			<br>
				<%=checkedoutTxt%>  
    		</td>
    	</tr>
	<%}%>
    <tr>
    	<td class="bodylight" align="left" >
			<br>
    		<%=auction.getAuctionTitle()%>   
    		<br>
    	</td>
    	<td class="bodylight" width="30%" align="right">
    		<br>
    		<% if(showCheckoutBtn)  {%>
				<form id="checkoutForm" name="checkoutForm" action="<isa:webappsURL name="/buyer/checkoutauction.do"/>" method="post">
	  				<input type="hidden" name="<%=CheckoutAuctionsForm.CHECKOUT%>" id="<%=CheckoutAuctionsForm.CHECKOUT%>" value="<%=CheckoutAuctionsForm.CHECKOUT%>" >
	  				<input type="hidden" name="<%=CheckoutAuctionsForm.AUCTIONID%>" id="<%=CheckoutAuctionsForm.AUCTIONID%>" value="<%=auction.getAuctionId()%>">
					<input id="checkout"
							type="button" class="green" size="80" onClick="changeDocument()" value="&nbsp&nbsp;&nbsp&nbsp;<%=checkoutTxt%>&nbsp;&nbsp&nbsp;">

				</form>
			<%}%> 
    	</td>
  	</tr>       
    </table>

	<table width="100%">
		<tr>
			<td width="70">
				<% if(auction.getImageURL() != null && !auction.getImageURL().equals(""))  {  
						String imageURL = auction.getImageURL();
						if(!imageURL.startsWith("http"))  {
							imageURL = WebUtil.getMimeURL(pageContext, imageURL);
						}
				%>
					<img src="<%=imageURL%>" width=60 height=60>
					</img>
				<% }  else  { %>
					<img src="<%=WebUtil.getMimeURL(pageContext,"/auction/images/noimage.jpg")%>" width=60 height=60>
					</img>
				<% }  %>
			</td>

			<td valign="top">
				<table width="300">
					<tr>
						<td>
							<strong><%=startPriceTxt%></strong>
						</td>
						<td>
							<%=auctionDetailsForm.getDisplayCurrencyValue(auction.getStartPrice())%>
						</td>
					</tr>
					<tr>
						<td>
							<%	int status = auction.getStatus();
								if(status == AuctionStatusEnum.toInt(AuctionStatusEnum.PUBLISHED)){%>
								<strong><%=currentBidTxt%></strong>
							<%} else if (status == AuctionStatusEnum.toInt(AuctionStatusEnum.CLOSED)
								|| status == AuctionStatusEnum.toInt(AuctionStatusEnum.NOT_FINALIZED)) {%>
								<strong><%=finalBidTxt%></strong>
							<%} else if (status == AuctionStatusEnum.toInt(AuctionStatusEnum.FINALIZED)
								|| status == AuctionStatusEnum.toInt(AuctionStatusEnum.FINISHED)) {%>
								<strong><%=winningBidTxt%></strong>
							<%}%>
						</td>
						<td>
						<%
							BigDecimal bidValue = auctionDetailsForm.getHighestBid(auction);
	 						String currentBid = "-";
							String reserveTxt = "";
							if (!bidValue.equals(new BigDecimal(0.0))) {
								currentBid = auctionDetailsForm.getDisplayCurrencyValue(bidValue);
								reserveTxt = "( " +auctionDetailsForm.getReservePriceInfoText(auction , bidValue) +" )";
							}
						%>
							<%=currentBid%>&nbsp;&nbsp;<%=reserveTxt%>&nbsp;
						</td>
					</tr>
					<% if(auction.getBuyItNowPrice() != null && auction.getBuyItNowPrice().doubleValue() > 0.0)  {  %>
					<tr>
						<td>
							<strong><%=buyNowPriceTxt%></strong>
						</td>
						<td >
							<%=auctionDetailsForm.getDisplayCurrencyValue(auction.getBuyItNowPrice())%>
						</td>
					</tr>
					<%  }  %>
					<tr>
						<td>
							<strong><%=quantityTxt%></strong>
						</td>
						<td nowrap>
							<%=auction.getQuantity()%>&nbsp;(<%=unitsTxt%>)
						</td>
					</tr>
					<tr>
						<td>
							<strong><%=timeLeftTxt%></strong>
						</td>
						<td>
							<%=auctionDetailsForm.getTimeLeft(auction)%>
						</td>
					</tr>
					<tr>
						<td>
							<strong><%=endTimeTxt%></strong>
						</td>
						<td>
							<%=auctionDetailsForm.getDisplayDateValue(auction.getEndDate())%>
						</td>
					</tr>
					<tr>
						<td>
							<strong><%=totalBidsTxt%></strong>
						</td>
						<td>
							<%=auctionDetailsForm.getTotalBids()%>
						</td>
					</tr>
					<%if(!openForBidding && winner != null && auctionDetailsForm.isCheckedOut(winner)) 
						{  
							String orderLink = auctionDetailsForm.buildOrderLink(winner);
							String orderId = winner.getOrderId();
					%>
					<tr>
						<td>
							<strong><%=orderTxt%></strong>
						</td>
						<td align = "left">
							<a href="<isa:webappsURL name="<%=orderLink%>"/>"><%=orderId%></a>
						</td>
					<tr>
					<%} %>
					<% if(openForBidding) {%>
					<tr>
					<td colspan="2">
						<form id="refreshForm" name="refreshForm" action="<isa:webappsURL name="/buyer/refreshauction.do"/>" method="post">
	  						<input id="refresh"
									type="submit" class="green" size="80" value="&nbsp&nbsp;<%=refreshBtnTxt%>&nbsp&nbsp;">
						</form>
					</td></tr>
					<% } %>
				</table>
			</td>
			<%  if(auctionDetailsForm.isOpenForBidding(auction))  {%>
			<td align="right">
				<form id="bidding" name="bidding" method="post" action="<isa:webappsURL name="/buyer/bid.do"/>">
				<input type="hidden" name="<%=AuctionDetailsForm.BIDTYPE%>" id="<%=AuctionDetailsForm.BIDTYPE%>">
				<input id="<%=AuctionDetailsForm.BIDQUANTITYPARAMETER%>" 
										type="hidden" name="<%=AuctionDetailsForm.BIDQUANTITYPARAMETER%>" value="1">

            	<table width="100%">
              		<thead>
                	<tr>
                  	<td class="headerTab">
                      <%=bidTxt%>
                  	</td>
                	</tr>
              		</thead>
              		<tr>
              		<td class="poslist-pos-even">
                  		<table cellpadding="1" cellSpacing="4" width="90%">
							<tr>
								<td align="left" width="50%">
									<strong><%=nextMinimumBidTxt%></strong>
									<br><span>*<%=minBidMsgTxt%></span>
								</td>
								<td>
									<%=auctionDetailsForm.getNextMinimumBid(auction)%>
								</td>
							</tr>
							<tr>
								<td align="left">
									<strong><%=bidAmountTxt%></strong>
								</td>
								<td>
									<input id="<%=AuctionDetailsForm.BIDAMOUNTPARAMETER%>" 
										class="textInput" class="submitDoc" size="10" name="<%=AuctionDetailsForm.BIDAMOUNTPARAMETER%>">
								</td>
							</tr>
							<% if(auctionDetailsForm.isBrokenLot())  {  %>
							<tr>
								<td align="left">
									<strong><%=bidQtyTxt%></strong>
								</td>
								<td nowrap>
									<select class="input_type" id="<%=AuctionDetailsForm.BIDQUANTITYPARAMETER%>Sel" 
										size="1" name="<%=AuctionDetailsForm.BIDQUANTITYPARAMETER%>Sel">
										<%
											String selected = (String)request.getAttribute(AuctionDetailsForm.BIDQUANTITYPARAMETER);
											for(int i = 1; i <= auction.getQuantity(); i++)  {  %>
												<option <%=(selected != null && selected.equals(Integer.toString(i)) ? "selected" : "")%>><%=Integer.toString(i)%></option>
										<%  }
										%>
									</select>
								</td>
							</tr>
							<%  }  %>
							<tr>
								<td>
								&nbsp;</td>
							</tr>
							<tr>
								<td align="left" colSpan="2">
									<input id="bid"
										type="submit" class="green" size="80" onClick="bidAuction()" value="&nbsp&nbsp;&nbsp&nbsp;<%=bidBtnTxt%>&nbsp;&nbsp&nbsp;">
								<% if(auctionDetailsForm.getBuyNowPrice() != null && auction.getBuyItNowPrice().doubleValue() > 0.0)  {  %>
									<input id="buyit" 
											type="submit" class="green" onClick="buyNow()" value="<%=buynowTxt%>">
								</td>
								<%  }  %>
							</tr>
						</table>
						
					</td>
					</tr>
				</table>
			</form>
			</td>
			<% }  %>
		</tr>

		<tr>
			<td colSpan="4" align="left">
				<br>
				<table class="poslist" border="1" cellspacing="0" cellpadding="3" width="100%">
				<thead><th class="poslist"><%=productsTxt%></th></thead>
				</table>
				<table class1="poslist" border="0" cellspacing="0" cellpadding="3" width="95%">
				<%
				java.util.Collection items = auction.getItems();
				java.util.Iterator itemsIterator = items.iterator();
				UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
				AuctionItem item = null;
				while(itemsIterator.hasNext())  {
					item = (AuctionItem)itemsIterator.next();
				%>
					<tr>
						<td width="70">
						<%userSessionData.setAttribute("item" , auctionDetailsForm.getProduct(item));%>
						<img src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="item" defaultImg="mimes/shared/no_thumb.gif"/>" width=60 height=60>
						</td>
						<td valign="top">
							<table cellPadding="0" CellSpacing="2" width="300">
								<tr>
									<td colspan="2">
										<%=item.getProductCode()%>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<%=item.getDescription()%>
									</td>
								</tr>
								<tr>
									<td>
										<strong>
											<%=quantityTxt%>
										</strong> &nbsp;
										<%=auctionDetailsForm.getDisplayQuantityValue(item.getQuantity())%>(<%=item.getUOM()%>)
									</td>
								</tr>
							</table>
						</td>
					</tr>
				<%
				}
				%>
				</table>
			</td>
		</tr>

		<tr>
			<td colSpan="4" align="left">
				<br>
				<Strong><%=bidHistoryTxt%></Strong>
				<%
				java.util.Collection bids = auctionDetailsForm.getBids();
				String tdEvenOdd[] = new String[] { "poslist-pos-odd", "poslist-pos-even" };
				if(bids != null && bids.size() > 0)  {
				%>
				<table class="poslist" border="1" cellspacing="0" cellpadding="3" width="95%">
					<thead>
					<th class="poslist"></th>
					<th class="poslist"><%=bidderTxt%></th>
					<th class="poslist"><%=bidsAmountTxt%></th>
					<th class="poslist"><%=bidQuantityTxt%></th>
					<th class="poslist"><%=bidDateTxt%></th>
					</thead>
				<%
					java.util.Iterator iterator = bids.iterator();
					int i = 0;
					Bid bid = null;
					while(iterator.hasNext())  {
						bid = (Bid)iterator.next();
				%>
					<tr>
						<td>&nbsp;</td>
						<td class="<%=tdEvenOdd[i%2]%>"><% if (auctionDetailsForm.getBusinessPartnerName(bid.getBuyerId()) != null ) { %> <%=auctionDetailsForm.getBusinessPartnerName(bid.getBuyerId())%> <% } else { %> <isa:translate key="b2b.auction.other.bidder"/> <% } %></td>
						<td class="<%=tdEvenOdd[i%2]%>"><%=auctionDetailsForm.getDisplayCurrencyValue(bid.getBidAmount())%></td>
						<td class="<%=tdEvenOdd[i%2]%>"><%=auctionDetailsForm.getDisplayQuantityValue(bid.getQuantity())%></td>
						<td class="<%=tdEvenOdd[i%2]%>"><%=auctionDetailsForm.getDisplayDateValue(bid.getBidDate())%></td>
					</tr>
				<% 		i++;
					}
				%>
	
				</table>
				<%
				}  else  {
				%>
				<table class="poslist" border="1" cellspacing="0" cellpadding="3" width="100%">
				<tr><td><%=noBidsYetTxt%></td></tr>
				</table>
				<%
				}
				%>
			</td>
		</tr>
		<tr>
			<td>
				<br>
			</td>
		</tr>
		<tr>
			<td colSpan="4">
				<table class="poslist" border="1" cellspacing="0" cellpadding="3" width="100%">
				<thead><th class="poslist"><%=termsAndCondTxt%></th></thead>
				<tr>
					<td>
					<%--td class1="poslist-pos-even"--%>
						<%=auctionDetailsForm.getTermsAndConditions()%>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<ehbj:htmlPageLoaded/>
</body>
<script language="Javascript">
	function bidAuction()  {
		var typeInfo = document.getElementById("<%=AuctionDetailsForm.BIDTYPE%>");
		var valueInp = document.getElementById("<%=AuctionDetailsForm.BIDAMOUNTPARAMETER%>");
		var qtyInp = document.getElementById("<%=AuctionDetailsForm.BIDQUANTITYPARAMETER%>");
		var qtySelInp = document.getElementById("<%=AuctionDetailsForm.BIDQUANTITYPARAMETER%>Sel");
		var msg = "<%=bidAlertMessageTxt%>" + " " + valueInp.value + " " + "<%=auctionDetailsForm.getCurrencyText()%>";
		<%  if(auctionDetailsForm.isBrokenLot())  {  %>
		msg = msg + " " + "<%=perUnitTxt%>" + " " + "<%=forTxt%>" + " " + qtySelInp.options[qtySelInp.selectedIndex].text + "(" + "<%=unitTxt%>" + ")";
		<%  }  %>
		if(confirm(msg))  {
			if(qtySelInp != null)
				qtyInp.value = qtySelInp.options[qtySelInp.selectedIndex].text;
			if(typeInfo != null)  {
				typeInfo.value = "<%=AuctionDetailsForm.BIDDINGNOW%>";
			}
		}  else  {
			event.returnValue = false;
			return false;
		}
	}

	function buyNow()  {
		var typeInfo = document.getElementById("<%=AuctionDetailsForm.BIDTYPE%>");
		var qtyInp = document.getElementById("<%=AuctionDetailsForm.BIDQUANTITYPARAMETER%>");
		var msg = "<%=buyNowAlertMessageTxt%>" + " " + "<%=auctionDetailsForm.getBuyNowPrice()%>";
		<%  if(auctionDetailsForm.isBrokenLot())  {  %>
		qtyInp.value = null;
		qtyInp.value = showModalDialog("<isa:webappsURL name="/auction/buyer/buynow.jsp"> <isa:param name="<%=AuctionDetailsForm.BIDQUANTITYPARAMETER%>" value="<%=Integer.toString(auction.getQuantity())%>"/><isa:param name="BUYNOW" value="<%=auctionDetailsForm.getDisplayCurrencyValue(auction.getBuyItNowPrice())%>"/></isa:webappsURL>" , "", "dialogHeight:140px;dialogWidth:320px;status:no;scroll:no;")
		if(qtyInp.value == null || qtyInp.value == "null")  {
			event.returnValue = false;
			return false;
		}
		msg = msg + " " + "<%=perUnitTxt%>" + " " + "<%=forTxt%>" + " " + qtyInp.value + "(" + "<%=unitTxt%>" + ")";
		<%  }  %>
		if(confirm(msg))  {
			if(typeInfo != null)  {
				typeInfo.value = "<%=AuctionDetailsForm.BUYINGNOW%>";
			}
		}  else  {
			event.returnValue = false;
			return false;
		}
	}

<%
	auctionDetailsForm.postProcessing();
%>
</script>

</html>
