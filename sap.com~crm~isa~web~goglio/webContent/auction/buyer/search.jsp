<%@ taglib uri="/isa"  prefix="isa" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.actionform.b2b.contract.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionMonitorForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionsForm" %>
<%@ page import="com.sap.isa.auction.util.FormUtility" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>
<%@ page import="com.sap.isa.maintenanceobject.businessobject.GSProperty" %>
<%@ page import="com.sap.isa.maintenanceobject.businessobject.GSAllowedValue" %>
<%@ page import="com.sap.isa.isacore.DocumentHandler" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>

<isa:contentType />

<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<%
    String showTxt = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.SHOW , null);
    String wonTxt = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.WON , null);
    String participationTxt = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.PARTICIPATED , null);

    String participatedHistoryFinalizedTxt = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.PARTICIPATEDFINALIZED , null);
    String participatedHistoryClosedTxt = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.PARTICIPATIONCLOSE , null);
    String participationActive = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.PARTICIPATEDACTIVE , null);
    String toBeCheckout = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.PENDINGCHECKOUT , null);
    String checkOut = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.CHECKOUT , null);
    String upComingAuctionsTxt = WebUtil.translate(pageContext,
                                            AuctionMonitorForm.UPCOMING, null);
    String thatI = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.THATI , null);
    String whichAre = WebUtil.translate(pageContext ,
                                            AuctionMonitorForm.WHICHARE , null);
	boolean calledFromPortal = (((IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).getPortal().equals("YES") ? true : false);
	String auctionsURL = "/buyer/showauctions.do";
  DocumentHandler documentHandler = (DocumentHandler)
    userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

%>

<html>
<head>
    <title></title>
<isa:includes/>
<script src='<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js" ) %>' type="text/javascript">
</script>
<script type="text/javascript">

  var IE4 = IE5 = NS4 = NS6 = false;
  var IE4 = (document.all && !document.getElementById) ? true : false;
  var IE5 = (document.all && document.getElementById) ? true : false;
  var NS4 = (document.layers) ? true : false;
  var NS6 = (document.getElementById && !document.all) ? true : false;

  function setPeriod() {
    document.selectform.period.disabled = false;
    if(!NS4){document.selectform.period.style.backgroundColor = "white";}
    document.selectform.month.disabled = true;
    if(!NS4){document.selectform.month.style.backgroundColor = "silver";}
    document.selectform.year.disabled = true;
    if(!NS4){document.selectform.year.style.backgroundColor = "silver";}
  }
  function setDate() {
    document.selectform.period.disabled = true;
    if(!NS4){document.selectform.period.style.backgroundColor = "silver";}
    document.selectform.month.disabled = false;
    if(!NS4){document.selectform.month.style.backgroundColor = "white";}
    document.selectform.year.disabled = false;
    if(!NS4){document.selectform.year.style.backgroundColor = "white";}
  }
  function submitForOtherDocument(documentType) {
    if (document.selectform.<%=ContractSearchAction.DOCUMENT_TYPE%>[
        document.selectform.<%=ContractSearchAction.DOCUMENT_TYPE%>.selectedIndex].value
        != "<%=ContractSearchAction.AUCTION%>") {
            document.selectform.submit();
    }
  }

  function changeOption()  {
    if (document.selectform != null)  {
        if(document.selectform.auction_options != null)  {
            if(document.selectform.auction_options.selectedIndex == 0)  {
                if(document.getElementById("wonAuctionDiv") != null)  {
                    document.getElementById("wonAuctionDiv").style.display = "none";
                }
                if(document.getElementById("participatedDiv") != null)  {
                    document.getElementById("participatedDiv").style.display = "none";
                }
            }  else if(document.selectform.auction_options.selectedIndex == 1)  {
                if(document.getElementById("wonAuctionDiv") != null)  {
                    document.getElementById("wonAuctionDiv").style.display = "block";
                }
                if(document.getElementById("participatedDiv") != null)  {
                    document.getElementById("participatedDiv").style.display = "none";
                }
            } else if(document.selectform.auction_options.selectedIndex == 2)  {
                if(document.getElementById("wonAuctionDiv") != null)  {
                    document.getElementById("wonAuctionDiv").style.display = "none";
                }
                if(document.getElementById("participatedDiv") != null)  {
                    document.getElementById("participatedDiv").style.display = "block";
                }
            }
        }
    }
  }

      function load_genericdocSearch() {
        var screenName = "<%= documentHandler.getOrganizerParameter("genericsearch.name") %>"
        parent.organizer_content.location.href=("<isa:webappsURL name="/appbase/genericsearch.jsp?genericsearch.name="/>" + screenName);
        // isaTop().header.busy(form_input());
        // form_input().location.href="<isa:webappsURL name="/b2b/catalogend.do"/>";
      }

</script>
</head>
<body class="organizer">
<div class="module-name"><isa:moduleName name="auction/buyer/auctionmonitor.jsp"/></div>
<div id="organizer-search" class="module">

    <form name="selectform"  method="post" action='<isa:webappsURL name="/b2b/contractsearch.do"></isa:webappsURL>'>
    <input type="hidden" value="" name="<isa:translate key="eAuctions.search.status"/>">


        <br>
        <table cellspacing="0" cellpadding="1">
        <tbody>
            <tr>
                <td nowrap><isa:translate key="b2b.contract.shuffler.key1"/>&nbsp;</td>
                <td>
				<select id="optionsNav" name="optionsNav" onChange="load_genericdocSearch()">
          <%  GSProperty pty = (GSProperty)userSessionData.getAttribute("genericsearch.doclist");
            java.util.Iterator itAV = pty.iterator();
            while (itAV.hasNext()) {
            	GSAllowedValue av = (GSAllowedValue)itAV.next();
            	if ( ! av.isHidden()) {
          		// Write allowed values as select box option
          			if(av.getValue().equals("AUCTION"))  {
          %>
					<option value="<%=av.getValue()%>" selected><%=WebUtil.translate(pageContext , av.getDescription() , null)%> </option>
		  <%		}  else  {  %>
              <option value="<%=av.getValue()%>"><%=WebUtil.translate(pageContext , av.getDescription() , null)%> </option>

          <%  		}
				}
            }
          %>
				</select>
                </td>
            </tr>
            <tr>
                <td nowrap><%=thatI%>&nbsp;</td>
                <td><select class="bigCatalogInput" name="auction_options" id="auction_options"
                      onChange="changeOption()">
                      <option value=" " key="empty"></option>
                      <option value="<%=AuctionMonitorForm.WON%>"><%=wonTxt%></option>
                      <option value="<%=AuctionMonitorForm.PARTICIPATED%>"><%=participationTxt%></option>
                </select></td>
            </tr>
        </table>
        <p>

      <div id="wonAuctionDiv" style="display:none;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td><%=whichAre%></td>
          <td colspan=1>
                    <table border="0">
                        <tr>
                          <td><a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.WON"/></isa:webappsURL>" target="form_input"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_m_rand_gruen.gif") %>" width="16" height="16" alt="<%=toBeCheckout%>" border="0"></a></td>
                          <td>&nbsp;<a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.WON%>"/></isa:webappsURL>" target="form_input"><%=toBeCheckout%></a></td>
                        </tr>
                    </table>
          </td></tr>
          <tr><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td>
          <td colspan=1>
                    <table border="0">
                        <tr>
                          <td><a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.CHECKEDOUT%>"/></isa:webappsURL>" target="form_input"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_m_rand_gruen.gif") %>" width="16" height="16" alt="<%=checkOut%>" border="0"></a></td>
                          <td>&nbsp;<a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.CHECKEDOUT%>"/></isa:webappsURL>" target="form_input"><%=checkOut%></a></td>
                        </tr>
                    </table>
          </td></tr>
          </table>
          </div>
          <div id="participatedDiv" style="display:none;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
          <tr><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp</td><td><%=whichAre%></td>
          <td colspan=1>
                    <table border="0">
                        <tr>
                          <td><a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.PARTICIPATIONACTIVE%>"/></isa:webappsURL>" target="form_input"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_m_rand_gruen.gif") %>" width="16" height="16" alt="<%=participationActive%>" border="0"></a></td>
                          <td>&nbsp;<a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.PARTICIPATIONACTIVE%>"/></isa:webappsURL>" target="form_input"><%=participationActive%></a></td>
                        </tr>
                    </table>
          </td></tr>
          <tr><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp</td><td>&nbsp;&nbsp;&nbsp;</td>
          <td colspan=1>
                    <table border="0">
                        <tr>
                          <td><a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.PARTICIPATIONHISTORYFINALIZED%>"/></isa:webappsURL>" target="form_input"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_m_rand_gruen.gif") %>" width="16" height="16" alt="<%=participatedHistoryFinalizedTxt%>" border="0"></a></td>
                          <td>&nbsp;<a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.PARTICIPATIONHISTORYFINALIZED%>"/></isa:webappsURL>" target="form_input"><%=participatedHistoryFinalizedTxt%></a></td>
                        </tr>
                    </table>
          </td></tr>
          <tr><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp</td><td>&nbsp;&nbsp;&nbsp;</td>
          <td colspan=1>
                    <table border="0">
                        <tr>
                          <td><a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.PARTICIPATIONHISTORYCLOSED%>"/></isa:webappsURL>" target="form_input"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_m_rand_gruen.gif") %>" width="16" height="16" alt="<%=participatedHistoryClosedTxt%>" border="0"></a></td>
                          <td>&nbsp;<a href="<isa:webappsURL name="<%=auctionsURL%>"><isa:param name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=AuctionsForm.PARTICIPATIONHISTORYCLOSED%>"/></isa:webappsURL>" target="form_input"><%=participatedHistoryClosedTxt%></a></td>
                        </tr>
                    </table>
          </td></tr>
        </tbody>
        </table>
        </div>
<%--for crm40, show the upcoming auctions (published), not for 31
        <hr>
        <div id="upcomingDiv" style="display:block;">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tbody>
                 <tr><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp</td><td>&nbsp;&nbsp;&nbsp;</td>
                  <td colspan=1>
                        <table border="0">
                            <tr>
                              <td><a href="<isa:webappsURL name="/auction/bidder/showUpcomingAuctions.do"/>" target="form_input"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_m_rand_gruen.gif") %>" width="16" height="16" alt="<%=upComingAuctionsTxt%>" border="0"></a></td>
                              <td>&nbsp;<a href="<isa:webappsURL name="/auction/bidder/showUpcomingAuctions.do"/>" target="form_input"><%=upComingAuctionsTxt%></a></td>
                            </tr>
                        </table>
                  </td></tr>
              </tbody>
             </table>
        </div>
--%>
</form>
</div>
</body>
</html>
