<%@ page import="com.sap.isa.auction.bean.b2x.PrivateAuction" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionDetailsForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionBaseForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionsForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.CheckoutAuctionsForm" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.auction.bean.search.QueryResult" %>
<%@ page import="com.sap.isa.auction.bean.AuctionItem" %>
<%@ page import="com.sap.isa.auction.bean.Winner" %>
<%@ page import="com.sap.isa.auction.util.Utilidad" %>
<%@ page import="com.sap.isa.auction.bean.Bid" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.math.BigDecimal" %>
<%@ taglib uri="/isa" prefix="isa" %>

<body class1="organizercatalog">
	<div class="module-name"><isa:moduleName name="/auction/buyer/buynowconf.inc.jsp" /></div>
<jsp:useBean id="auctionDetailsForm" scope="session"
class="com.sap.isa.auction.actionforms.buyer.AuctionDetailsForm" />

<%
	auctionDetailsForm.preprocessing();
	String auctionsTitle = auctionDetailsForm.translate(AuctionDetailsForm.BUYNOWAUCTIONCONFTITLE);
	String detailsTxt = auctionDetailsForm.translate(AuctionDetailsForm.DETAILSTITLETEXT);;
	String startPriceTxt = auctionDetailsForm.translate(AuctionBaseForm.STARTPRICETEXT);
	String myLastBidTxt = auctionDetailsForm.translate(AuctionBaseForm.MYLASTBIDTEXT);
	String currentBidTxt = auctionDetailsForm.translate(AuctionBaseForm.CURRENTBIDTEXT);
    String buyNowPriceTxt = auctionDetailsForm.translate(AuctionBaseForm.BUYNOWPRICETEXT);
	String timeLeftTxt = auctionDetailsForm.translate(AuctionBaseForm.TIMELEFTTEXT);
	String nextPageTxt = auctionDetailsForm.translate(AuctionBaseForm.NEXTTEXT);
	String prevPageTxt = auctionDetailsForm.translate(AuctionBaseForm.PREVIOUSTEXT);
	String noImageTxt = auctionDetailsForm.translate(AuctionBaseForm.NOIMAGETEXT);
	String unitsTxt = auctionDetailsForm.translate(AuctionBaseForm.UNITSTEXT);
	String quantityTxt = auctionDetailsForm.translate(AuctionBaseForm.QUANTITYTEXT);
	String productsTxt = auctionDetailsForm.translate(AuctionDetailsForm.PRODUCTSTEXT);
	String laterCheckoutTxt = auctionDetailsForm.translate(AuctionDetailsForm.LATERCHECKOUTTEXT);
	String checkoutTxt = auctionDetailsForm.translate(AuctionDetailsForm.CHECKOUTTEXT);
	String orderTxt = auctionDetailsForm.translate(AuctionDetailsForm.ORDERTEXT);
	String checkoutURL = "";
	String termsAndCondTxt = auctionDetailsForm.translate(AuctionDetailsForm.TERMSANDCONDTEXT);
%>

            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="iViewHeader" width="10"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headline_header_small.gif") %>" width="5" height="17" alt="" border="0"></td>
                    <td class="iViewHeader">
					<%=auctionsTitle%>
                    </td>
                </tr>
                <tr>
                    <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="5" alt="" border="0"></td>
                </tr>
            </table>

    <table class1="headerWhite" border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
    	<td class="bodylight" align="left" colSpan="2" width="100%">
		<br><br>
<%
	PrivateAuction auction = auctionDetailsForm.getAuction();
%>
    
    		<%=auction.getAuctionTitle()%> 	
     
    		<br><br></td>
  	</tr>       
    </table>

	<table width="100%">
		<tr>
			<td width="70">
				<% if(auction.getImageURL() != null && !auction.getImageURL().equals(""))  {  
						String imageURL = auction.getImageURL();
						if(!imageURL.startsWith("http"))  {
							imageURL = WebUtil.getMimeURL(pageContext, imageURL);
						}
				%>
					<img src="<%=imageURL%>" width=60 height=60>
					</img>
				<% }  else  { %>
					<img src="<%=WebUtil.getMimeURL(pageContext,"/auction/images/noimage.jpg")%>" width=60 height=60>
					</img>
				<% }  %>
			</td>

			<td valign="top">
				<table width="300">
					<tr>
						<td>
							<strong><%=startPriceTxt%></strong>
						</td>
						<td>
							<%=auctionDetailsForm.getDisplayCurrencyValue(auction.getStartPrice())%>
						</td>
					</tr>
					<tr>
						<td>
							<strong><%=currentBidTxt%></strong>
						</td>
						<td>
						<%
							BigDecimal bidValue = auctionDetailsForm.getHighestBid(auction);
							String reserveTxt = auctionDetailsForm.getReservePriceInfoText(auction , bidValue);
						%>
							<%=auctionDetailsForm.getDisplayCurrencyValue(bidValue)%>&nbsp;&nbsp;(&nbsp;<%=reserveTxt%>&nbsp;)&nbsp;
						</td>
					</tr>
					<% if(auction.getBuyItNowPrice() != null)  {  %>
					<tr>
						<td>
							<strong><%=buyNowPriceTxt%></strong>
						</td>
						<td >
							<%=auctionDetailsForm.getDisplayCurrencyValue(auction.getBuyItNowPrice())%>
						</td>
					</tr>
					<%  }  %>
					<tr>
						<td>
							<strong><%=quantityTxt%></strong>
						</td>
						<td nowrap>
							<%=auction.getQuantity()%>&nbsp;(<%=unitsTxt%>)
						</td>
					</tr>
					<%  if(!auctionDetailsForm.isOpenForBidding(auction))  {
							Winner winner = auctionDetailsForm.getWinner();
							if(winner != null && !auctionDetailsForm.isCheckedOut(winner))  {
					%>
						<td colspan="2">
							<form id="checkoutForm" name="checkoutForm" action="<%=checkoutURL%>" method="post">
	  						<input type="hidden" name="<%=CheckoutAuctionsForm.CHECKOUT%>" id="<%=CheckoutAuctionsForm.CHECKOUT%>" value="<%=CheckoutAuctionsForm.CHECKOUT%>" >
	  						<input type="hidden" name="<%=CheckoutAuctionsForm.AUCTIONGUID%>" id="<%=CheckoutAuctionsForm.AUCTIONID%>" value="<%=auction.getAuctionId()%>">
								<input id="checkout"
										type="submit" class="green" size="80" value="&nbsp&nbsp;&nbsp&nbsp;<%=checkoutTxt%>&nbsp;&nbsp&nbsp;">

							</form>
						</td>
					<%		}  
							if(winner != null && auctionDetailsForm.isCheckedOut(winner))  {  %>
						<td colspan="1">
							<strong><%=orderTxt%></strong>
						</td>
						<td colspan="1">
							<a href="http://pwdf2024.wdf.sap.corp:50000/b2b/b2b/documentstatusdetailprepare.do?techkey=0CF7BE40FEEC7216E10000000A15533D&object_id=6400000626&objecttype=order&processtype=AATA"/>
						</td>
					<%      }
					
					    }  %>
				</table>
			</td>
		</tr>

		<tr>
			<td colSpan="4" align="left">
				<br>
					<strong>
						<%=laterCheckoutTxt%>
					</strong>
			</td>
		</tr>
		<tr>
			<td colSpan="4" align="left">
				<br>
				<table class="poslist" border="1" cellspacing="0" cellpadding="3" width="100%">
				<thead><th class="poslist"><%=productsTxt%></th></thead>
				</table>
				<table class1="poslist" border="0" cellspacing="0" cellpadding="3" width="95%">
				<%
				java.util.Collection items = auction.getItems();
				java.util.Iterator itemsIterator = items.iterator();
				UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
				AuctionItem item = null;
				while(itemsIterator.hasNext())  {
					item = (AuctionItem)itemsIterator.next();
				%>
					<tr>
						<td width="70">
						<%userSessionData.setAttribute("item" , auctionDetailsForm.getProduct(item));%>
						<img src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="item" defaultImg="mimes/shared/no_thumb.gif"/>" width=60 height=60>
						</td>
						<td valign="top">
							<table cellPadding="0" CellSpacing="2" width="300">
								<tr>
									<td colspan="2">
										<%=item.getProductCode()%>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<%=item.getDescription()%>
									</td>
								</tr>
								<tr>
									<td>
										<strong>
											<%=quantityTxt%>
										</strong> &nbsp;
										<%=auctionDetailsForm.getDisplayQuantityValue(item.getQuantity())%>(<%=item.getUOM()%>)
									</td>
								</tr>
							</table>
						</td>
					</tr>
				<%
				}
				%>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<br>
			</td>
		</tr>
		<tr>
			<td colSpan="4">
				<table class="poslist" border="1" cellspacing="0" cellpadding="3" width="100%">
				<thead><th class="poslist"><%=termsAndCondTxt%></th></thead>
				<tr>
					<td class1="poslist-pos-even">
						<%=auctionDetailsForm.getTermsAndConditions()%>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>

