
<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.auction.bean.b2x.PrivateAuction" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionsForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionBaseForm" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.CheckoutAuctionsForm" %>
<%@ page import="com.sap.isa.auction.bean.search.QueryResult" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/ehtmlb" prefix="ehbj" %>

<jsp:useBean id="auctionsForm" scope="session"
class="com.sap.isa.auction.actionforms.buyer.AuctionsForm" />

<%
	auctionsForm.preprocessing();
	String url = WebUtil.getAppsURL(pageContext , Boolean.FALSE ,
		"/buyer/showauctions.do", null , null , false);
	String checkoutURL = WebUtil.getAppsURL(pageContext , Boolean.FALSE,
		"/buyer/checkoutauction.do" , null , null , false);

	String startPriceTxt = auctionsForm.translate(AuctionBaseForm.STARTPRICETEXT);
	String myLastBidTxt = auctionsForm.translate(AuctionBaseForm.MYLASTBIDTEXT);
	String winningBidTxt = auctionsForm.translate(AuctionBaseForm.WINNINGBIDTEXT);
	String buyNowPriceTxt = auctionsForm.translate(AuctionBaseForm.BUYNOWPRICETEXT);
	String timeLeftTxt = auctionsForm.translate(AuctionBaseForm.TIMELEFTTEXT);
	String nextPageTxt = auctionsForm.translate(AuctionBaseForm.NEXTTEXT);
	String prevPageTxt = auctionsForm.translate(AuctionBaseForm.PREVIOUSTEXT);
	String noImageTxt = auctionsForm.translate(AuctionBaseForm.NOIMAGETEXT);
	String unitsTxt = auctionsForm.translate(AuctionBaseForm.UNITSTEXT);
	String quantityTxt = auctionsForm.translate(AuctionBaseForm.QUANTITYTEXT);
	String checkoutTxt = auctionsForm.translate(CheckoutAuctionsForm.CHECKOUTTEXT);
	String noAuctionsWonTxt = auctionsForm.translate(CheckoutAuctionsForm.NOAUCTIONSFORCHTEXT);
	String auctionsTxt = auctionsForm.getPageTitle();
	String showPageTxt = auctionsForm.translate(AuctionBaseForm.SHOWPAGETEXT);
	String showItemsTxt = auctionsForm.translate(AuctionBaseForm.SHOWTEXT);
	String fiveItemsTxt = auctionsForm.translate(AuctionBaseForm.FIVEITEMSTEXT);
	String tenItemsTxt = auctionsForm.translate(AuctionBaseForm.TENITEMSTEXT);
	String fifteenItemsTxt = auctionsForm.translate(AuctionBaseForm.FIFTEENITEMSTEXT);

%>

<isa:contentType />

<html>

<head>
	<isa:stylesheets/>
	<isa:includes/>
	<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
		  type="text/javascript">
	</script>
</head>
<body class="organizerCatalog">
	<div class="module-name"><isa:moduleName name="/auction/buyer/auctionsforproduct.jsp" /></div>
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="iViewHeader" width="10"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headline_header_small.gif") %>" width="5" height="17" alt="" border="0"></td>
					<td class="iViewHeader">
					<%=auctionsTxt%>
					</td>
				</tr>
				<tr>
					<td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="5" alt="" border="0"></td>
				</tr>
			</table>

	<br>
	<br>
	
	<ehbj:htmlPageLoading/>
	<form id="checkoutForm" name="checkoutForm" action="<isa:webappsURL name="/buyer/checkoutauction.do"/>" method="post">
	  <input type="hidden" name="<%=CheckoutAuctionsForm.CHECKOUT%>" id="<%=CheckoutAuctionsForm.CHECKOUT%>" value="<%=CheckoutAuctionsForm.CHECKOUT%>" >
	  <input type="hidden" name="<%=CheckoutAuctionsForm.AUCTIONID%>" id="<%=CheckoutAuctionsForm.AUCTIONID%>" >
	</form>

	<% if(auctionsForm.getAuctionsCount() < 1)  {  %>
	<br>
	<table><tr><td>
		<Strong><%=noAuctionsWonTxt%></strong>
	</td></tr>
	</table>
	</br>
	<%  } %>

	<form id="auctionsForProducts" name="auctionsForProducts" action="<isa:webappsURL name="/buyer/showauctions.do"/>" method="post">
	  <input type="hidden" name="<%=AuctionsForm.NEXTPAGE%>" id="<%=AuctionsForm.NEXTPAGE%>" value="<%=(auctionsForm.getEndIndex()+1)%>">
	  <input type="hidden" name="<%=AuctionsForm.PREVIOUSPAGE%>" id="<%=AuctionsForm.PREVIOUSPAGE%>" value="<%=(auctionsForm.getStartIndex()-auctionsForm.getPageSize())%>">
	  <input type="hidden" name="<%=AuctionsForm.PAGESIZE%>" id="<%=AuctionsForm.PAGESIZE%>" value="<%=auctionsForm.getPageSize()%>">
	  <input type="hidden" name="<%=AuctionsForm.STATUSOPTIONTEXT%>" id="<%=AuctionsForm.STATUSOPTIONTEXT%>" value="<%=auctionsForm.getRcStatus()%>">
	  <input type="hidden" name="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" id="<%=AuctionsForm.PARTICIPATIONCONTEXT%>" value="<%=auctionsForm.getContextInfo()%>" >
	  <input type="hidden" name="<%=AuctionsForm.AUCTIONID%>" id="<%=auctionsForm.AUCTIONID%>" >
	  <input type="hidden" name="<%=AuctionsForm.STARTINDEX%>" id="<%=auctionsForm.STARTINDEX%>" value="">
	  <input type="hidden" name="<%=AuctionsForm.SHOWPAGEPARAMETER%>" id="<%=auctionsForm.SHOWPAGEPARAMETER%>" value="<%=auctionsForm.getCurrentPage()%>">
	  <input type="hidden" name="<%=AuctionsForm.NAVIGATION%>" id="<%=auctionsForm.NAVIGATION%>" value="">

	  <table border="0" width="100%">
	  <tr>
		<td>&nbsp;&nbsp;
		</td>
		<td align="right" nowrap>
			<%  if(auctionsForm.getResultSize() > 5)  {  %>
			<span><%=showItemsTxt%></span>
			<select onChange="_auction_setFetchSize()" id="_fetchSize" name="_fetchSize">
				<option <%=(auctionsForm.getPageSize() == 5 ? "selected" : "")%>><%=fiveItemsTxt%></option>
				<option <%=(auctionsForm.getPageSize() == 10 ? "selected" : "")%>><%=tenItemsTxt%></option>
				<option <%=(auctionsForm.getPageSize() == 15 ? "selected" : "")%>><%=fifteenItemsTxt%></option>
			</select>
			<%  }  %>
			&nbsp;
			<%  if(auctionsForm.getNumberOfPages() > 1)  {  %>
			<span><%=showPageTxt%></span>
			<select onChange="_auction_fetchPage()" id="_fetchPage" name="_fetchPage">
				<% for(int pageIndex = 1 ; pageIndex <= auctionsForm.getNumberOfPages(); pageIndex++)  {  %>
				<option <%=(auctionsForm.getCurrentPage() == pageIndex ? "selected" : "")%>><%=Integer.toString(pageIndex)%></option>
				<%  }  %>
			</select>
			<%  }  %>
			&nbsp;
		</td>
	  </tr>

	  <tr>
	  <td>&nbsp;&nbsp;
	  </td>
	  <td>
	  <table class="poslist" border="1" cellspacing="0" cellpadding="3" width="95%">

<%  
	String[] trEvenOdd = new String[] { "", "" };
	String tdEvenOdd[] = new String[] { "poslist-pos-odd", "poslist-pos-even" };
	
	int index = auctionsForm.getStartIndex();
	int pageSize = auctionsForm.getPageSize();
	
	PrivateAuction auction = null;
	int i = 0;
	int endIndex = auctionsForm.getEndIndex();
	java.util.Date date = java.util.Calendar.getInstance().getTime();
	java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
	for(i = index ; i <= endIndex; i++)  {
		auction = auctionsForm.getAuctionByIndex(i);
%>
		<tr class="<%= trEvenOdd[i % 2] %>">
			<td align="left" valign="top" class="<%= tdEvenOdd[i % 2]%>">
			
			</td>
			<td align="left" valign="top" class="<%= tdEvenOdd[i % 2]%>">

			<table width="100%">
			<thead>
				<th class="poslist" colSpan="2">
					<%=(i)%>&nbsp;
					<a href="#" onclick="_fetchAuction('<%=Integer.toString(i)%>')" alt="<%=auction.getAuctionTitle()%>">
					<%=auction.getAuctionTitle()%>
					</a>
				</th>
			</thead>
			<tr>
			<td width="70">
				<% if(auction.getImageURL() != null && !auction.getImageURL().equals(""))  {  
						String imageURL = auction.getImageURL();
						if(!imageURL.startsWith("http"))  {
							imageURL = WebUtil.getMimeURL(pageContext, imageURL);
						}
				%>
					<a href="#" onclick=_fetchAuction('<%=Integer.toString(i)%>')>
					<img src="<%=imageURL%>" width=60 height=60 alt="<%=auction.getAuctionTitle()%>">
					</img></a>
				<% }  else  { %>
					<img src="<%=WebUtil.getMimeURL(pageContext,"/auction/images/noimage.jpg")%>" width=60 height=60 alt="<%=noImageTxt%>">
					</img>
				<% }  %>
			</td>

			<td>
				<table width="350">
					<tr>
						<td colSpan="2"><br></td>
					</tr>
					<tr>
						<td>
							<strong><%=startPriceTxt%></strong>
						</td>
						<td align="right">
							<%=auctionsForm.getDisplayCurrencyValue(auction.getStartPrice())%>
						</td>
					</tr>
					<tr>
						<td>
							<strong><%=winningBidTxt%></strong>
						</td>
						<td align="right">
							<strong><%=auctionsForm.getCurrentBid(auction)%></strong>
						</td>
					</tr>
					<tr>
						<td colSpan="2">
							
						</td>
					</tr>
					<tr>
						<td colSpan="2">
							<input id="checkout" type="button" class="green" size="80" onClick="_checkout('<%=Integer.toString(i)%>')" value="&nbsp;&nbsp;<%=checkoutTxt%>&nbsp;&nbsp;&nbsp;">

						</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
			</td>
		</tr>

<%
	}
%>

		</table>
		</td>
		</tr>
		<tr>
			<td align="left" valign="middle" class="<%= tdEvenOdd[i % 2]%>">
			&nbsp;
			</td>
			<td align="right">
<%  if(index > 1)  {  %>
				<input class="head" type="button" onclick="fetchPage('<%=(auctionsForm.getCurrentPage() - 1)%>')" value="<%=prevPageTxt%>">
<%  }  
	if(endIndex < auctionsForm.getResultSize())  {  %>
				<input class="head" type="button" onclick="fetchPage('<%=(auctionsForm.getCurrentPage() + 1)%>')" value="<%=nextPageTxt%>">
<%  }  %>
			</td>
		</tr>
	  </table>
<script language="Javascript">
	function fetchPage(index)  {
		_auction_fetchPage(index);
	}

	function _fetchAuction(id)  {
		var idInp = document.getElementById("<%=AuctionsForm.AUCTIONID%>");
		if(idInp != null)
			idInp.value = id;
		if(document.getElementById("auctionsForProducts") != null)  {
			document.getElementById("auctionsForProducts").submit();
		}
	}

	function _checkout(auctionId)  {
		var idInp = document.getElementById("<%=CheckoutAuctionsForm.AUCTIONID%>");
     	var changeAllowed = parent.documents.editableDocumentAlert('<isa:translate key="b2b.onedocnav.onlyonedoc"/>\n\n<isa:translate key="b2b.onedocnav.onlyonedoc2"/>\n\n<isa:translate key="b2b.onedocnav.onlyonedoc3"/>')
		if(idInp != null)
			idInp.value = auctionId;
		if(document.getElementById("checkoutForm") != null)  {
			if (changeAllowed) { 
				document.getElementById("checkoutForm").submit();
			}
		}
		return false;
	}

	function _auction_setFetchSize()  {
		var fetchSize = document.getElementById("_fetchSize");
		var fetchSizeParameter = document.getElementById("<%=AuctionsForm.PAGESIZE%>");
		var fetchPageParameter = document.getElementById("<%=AuctionsForm.SHOWPAGEPARAMETER%>");
		if(fetchPageParameter != null)  {
			fetchPageParameter.value = null;
		}
		if(fetchSize != null  && fetchSizeParameter != null)  {
			fetchSizeParameter.value = fetchSize.selectedIndex;
		}
		var naviInp = document.getElementById("<%=AuctionsForm.NAVIGATION%>");
		if(naviInp != null)
			naviInp.value = "<%=AuctionsForm.NAVIGATION%>";
		if(document.getElementById("auctionsForProducts") != null)  {
			document.getElementById("auctionsForProducts").submit();
		}
	}

	function _auction_fetchPage(pageNumber)  {
		var fetchPageParameter = document.getElementById("<%=AuctionsForm.SHOWPAGEPARAMETER%>");
		if(pageNumber == null)  {
			var fetchPage = document.getElementById("_fetchPage");
			if(fetchPage != null  && fetchPageParameter != null)  {
				fetchPageParameter.value = fetchPage.selectedIndex + 1;
			}
		}  else  {
			fetchPageParameter.value = pageNumber;
		}
		
		var fetchSizeParameter = document.getElementById("<%=AuctionsForm.PAGESIZE%>");
		fetchSizeParameter.value = null;
		var naviInp = document.getElementById("<%=AuctionsForm.NAVIGATION%>");
		if(naviInp != null)
			naviInp.value = "<%=AuctionsForm.NAVIGATION%>";
		if(document.getElementById("auctionsForProducts") != null)  {
			document.getElementById("auctionsForProducts").submit();
		}
	}


<%
	auctionsForm.postProcessing();
%>
</script>
	  </form>

	<ehbj:htmlPageLoaded/>
</body>
</html>
