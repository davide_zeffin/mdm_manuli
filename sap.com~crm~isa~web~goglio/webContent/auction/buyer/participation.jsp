<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionsForm" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/ehtmlb" prefix="ehbj" %>
<jsp:useBean id="auctionsForm" scope="session"
class="com.sap.isa.auction.actionforms.buyer.AuctionsForm" />
<isa:contentType />

<%
	String auctionsTitle = auctionsForm.getPageTitle();
	UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());
	String noAuctionsWonTxt = auctionsForm.translate(AuctionsForm.NOAUCTIONSTEXT);
%>

<html>

<head>
    <isa:stylesheets/>
	<isa:includes/>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
          type="text/javascript">
    </script>
</head>
<body class="organizerCatalog">
	<div class="module-name"><isa:moduleName name="/auction/buyer/participation.jsp" /></div>
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="iViewHeader" width="10"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headline_header_small.gif") %>" width="5" height="17" alt="" border="0"></td>
                    <td class="iViewHeader">
                        <%=auctionsTitle%>
                    </td>
                </tr>
                <tr>
                    <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="5" alt="" border="0"></td>
                </tr>
            </table>
	<% if(auctionsForm.getAuctionsCount() < 1)  {  %>
	<br>
	<table><tr><td>
		<Strong><%=noAuctionsWonTxt%></strong>
	</td></tr>
	</table>
	</br>
	<%  } %>

    <ehbj:htmlPageLoading/>

              <table border="0">
                <tbody>
                  <tr>
                    <td align="left">
						<br>
					</td>
                  </tr>
                </tbody>
              </table>
			  <jsp:include page="auctionlist.jsp" flush="true" />
	<ehbj:htmlPageLoaded/>
</body>
</html>
