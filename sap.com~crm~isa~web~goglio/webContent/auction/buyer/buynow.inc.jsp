<%@ page import="com.sap.isa.auction.bean.b2x.PrivateAuction" %>
<%@ page import="com.sap.isa.auction.actionforms.buyer.AuctionDetailsForm" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%
	UserSessionData userSession = UserSessionData.getUserSessionData(session);
	String title = WebUtil.translate(userSessionData.getLocale() , AuctionDetailsForm.BUYNOWTITLE , null);
	String buyNowTxt = WebUtil.translate(userSessionData.getLocale() , AuctionDetailsForm.BUYNOWPRICETEXT , null);
	String quantityText = WebUtil.translate(userSessionData.getLocale() , AuctionDetailsForm.PLEASESELUNITSTEXT , null);
	String buyNowBtnTxt = WebUtil.translate(userSessionData.getLocale() , AuctionDetailsForm.BUYNOWTEXT , null);
	String cancelTxt = WebUtil.translate(userSessionData.getLocale() , AuctionDetailsForm.CANCELTEXT , null);
%>

<div class="module-name"><isa:moduleName name="/auction/buyer/buynow.inc.jsp" /></div>
<form>
       <table cellpadding="4" class="poslist-pos-even" cellSpacing="0" width="100%">
			<tr>
				<td class="poslist-pos-even">
					<br>
				</td>
				<td class="poslist-pos-even">
				</td>
			</tr>
			<tr>
				<td class="poslist-pos-even" align="left" width="50%" nowrap>
					<strong><span nowrap><%=buyNowTxt%></span></strong>
				</td>
				<td class="poslist-pos-even">
					<%=request.getParameter("BUYNOW")%>
				</td>
			</tr>
			<tr>
				<td class="poslist-pos-even" align="left" width="50%" nowrap>
					<strong><span nowrap><%=quantityText%></span></strong>
				</td>
				<td class="poslist-pos-even">
					<select class="input_type" id="buyNowQuantity" 
							size="1" name="buyNowQuantity">
					<%
						String quantityStr = (String)request.getParameter(AuctionDetailsForm.BIDQUANTITYPARAMETER);
						int qty = 1;
						if(quantityStr != null)
							qty = Integer.parseInt(quantityStr);
						for(int i = 1; i <= qty; i++)  {  %>
							<option><%=Integer.toString(i)%></option>
					<%  }
					%>
					</select>
				</td>
			</tr>
			<tr>
				<td align="left" class="poslist-pos-even" colSpan="2">
					<br>
					<input id="buyNow"
										type="button" class="green" size="80" onClick="buyNowAuction()" value="&nbsp&nbsp;&nbsp&nbsp;<%=buyNowBtnTxt%>&nbsp;&nbsp&nbsp;">
					<input id="cancel"
										type="button" class="green" size="80" onClick="cancelBuyNowAuction()" value="&nbsp&nbsp;&nbsp&nbsp;<%=cancelTxt%>&nbsp;&nbsp&nbsp;">
				</td>
			</tr>
		</table>
	
</form>

<script language="Javascript">

function buyNowAuction()  {
	var value = "";
	var qtyInp = document.getElementById("buyNowQuantity");
	if( qtyInp != null)  {
		value = qtyInp.options[qtyInp.selectedIndex].text;
	}
	window.returnValue = value;
	window.close();
}

function cancelBuyNowAuction()  {
		window.returnValue = null;
		window.close();
}
</script>