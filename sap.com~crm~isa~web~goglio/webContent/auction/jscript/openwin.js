/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       openwin.js                         */
/* Description:    opens details window               */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  04.04.2001                         */
/* Last-Update:    14.05.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/

// opens calendar window in the help section
function openWinCalendar(fileName) {
  var satCalendar = window.open(fileName, 'sat_calendar', 'width=220,height=250,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
  satCalendar.focus();
}

// opens details window
function openWin(fileName) {
  var sat = window.open(fileName, 'sat_details', 'width=400,height=250,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
  sat.focus();
}

// opens product info window
function openWinProduct(fileName) {
  var satProduct = window.open(fileName, 'product_infos', 'width=450,height=400,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
  satProduct.focus();
}

// opens product configuration window
function openWinConfig(fileName) {
  var satConfig = window.open(fileName, 'product_config', 'width=700,height=600,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
  satConfig.focus();
}

// opens help, user settings window
function openWinMeta(fileName) {
  var satMeta = window.open(fileName, 'meta', 'width=500,height=400,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
  satMeta.focus();
}
