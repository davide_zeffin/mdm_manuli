/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       closer.js                          */
/* Description:    resizes closer frame               */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  02.05.2001                         */
/* Last-Update:    07.05.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/

// resizes closer frame
function resize(direction) {
  if (document.all) {
    if (direction == "min") top.form_input.workareaFS.rows = '*,56,35';
    else top.form_input.workareaFS.rows = '*,56,100';
  }
}

function writeArrows() {
  if (document.all) {
    document.writeln("<table class=\"closer\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">");
    document.writeln("<tbody>");
    document.writeln("<tr>");
    document.writeln("<td>");
    document.writeln("<div class=\"closerArrow\" align=\"center\"><a href=\"#\" onclick=\"resize('min'); return false;\"> &darr; </a>&nbsp;&nbsp;<a href=\"#\" onclick=\"resize('max'); return false;\"> &uarr; </a></div>");
    document.writeln("</td>");
    document.writeln("</tr>");
    document.writeln("</tbody>");
    document.writeln("</table>");
  }
}
