/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       fancy_closer.js                    */
/* Description:    changes arrow images in closerFrame*/
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  26.04.2001                         */
/* Last-Update:    26.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/

// changes arrow-image
function chgArrow(dir) {
  if (dir == 'left') {
    document.all.rightArrow.style.visibility = 'hidden';
    document.all.leftArrow.style.visibility = 'visible';
  }
  else {
    document.all.leftArrow.style.visibility = 'hidden';
    document.all.rightArrow.style.visibility = 'visible';
  }
}


// highlights arrow-image
function overArrow(dir) {
  if (dir == 'left')
    document.images["arrow_left"].src = "images/but_arrow_left_h.gif";
  else
    document.images["arrow_right"].src = "images/but_arrow_right_h.gif";
}

// resets arrow-image
function outArrow(dir) {
  if (dir == 'left')
    document.images["arrow_left"].src = "images/but_arrow_left.gif";
  else
    document.images["arrow_right"].src = "images/but_arrow_right.gif";
}

