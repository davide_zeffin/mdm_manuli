//
// JavaScript Calendar Component
// 
// Usage:  Add the following lines of code to your page to enable the Calendar
//         component.
//
//         // THIS LINE LOADS THE JS LIBRARY FOR THE CALENDAR COMPONENT 
//
//         <SCRIPT LANGUAGE="JavaScript" SRC="/js/calendar.js"></SCRIPT>
//
//
//
//         // THIS LINE IS USED IN CONJUNCTION WITH A FORM FIELD (myDateField) IN A FORM (myForm).
//         // Replace "myForm" and "myDateField" WITH THE NAME OF YOUR FORM AND INPUT FIELD RESPECTIVELY
//         // WINDOW OPTIONS SET THE WIDTH, HEIGHT, AND X/Y POSITION OF THE CALENDAR WINDOW 
//         // WITH TITLEBAR ON, ALL OTHER OPTIONS (TOOLBARS, ETC) ARE DISABLED BY DEFAULT
//
//         <A HREF="javascript:doNothing()" onClick="setDateField(document.myForm.myDateField);top.newWin = window.open('calendar.html','cal','dependent=yes,width=210,height=280,screenX=200,screenY=300,titlebar=yes')">
//         <IMG SRC="calendar.gif" BORDER=0></A><font size=1>Popup Calendar</font>
//
//
// 
// Required Files:
//
//         calendar.js   - contains all JavaScript functions to make the calendar work
//
//         calendar.htm - frameset document (not required if you call the showCalendar()
//                         function.  However, calling showCalendar() directly causes
//                         the Java Virtual Machine (JVM) to start which slows down the
//                         loading of the calendar.)
//
// 
// Files Generally Included:
//
//         yourPage.html - page that contains a form and a date field which implements 
//                         the calendar component
// 



// BEGIN USER-EDITABLE SECTION -----------------------------------------------------



// SPECIFY DATE FORMAT RETURNED BY THIS CALENDAR
// (THIS IS ALSO THE DATE FORMAT RECOGNIZED BY THIS CALENDAR)

// DATE FORMAT OPTIONS:
//
// dd   = 1 or 2-digit Day
// DD   = 2-digit Day
// mm   = 1 or 2-digit Month
// MM   = 2-digit Month
// yy   = 2-digit Year
// YY   = 4-digit Year
// yyyy = 4-digit Year
// month   = Month name in lowercase letters
// Month   = Month name in initial caps
// MONTH   = Month name in captital letters
// mon     = 3-letter month abbreviation in lowercase letters
// Mon     = 3-letter month abbreviation in initial caps
// MON     = 3-letter month abbreviation in uppercase letters
// weekday = name of week in lowercase letters
// Weekday = name of week in initial caps
// WEEKDAY = name of week in uppercase letters
// wkdy    = 3-letter weekday abbreviation in lowercase letters
// Wkdy    = 3-letter weekday abbreviation in initial caps
// WKDY    = 3-letter weekday abbreviation in uppercase letters
//
// Examples:
//
// calDateFormat = "mm/dd/yy";
// calDateFormat = "Weekday, Month dd, yyyy";
// calDateFormat = "wkdy, mon dd, yyyy";
// calDateFormat = "DD.MM.YY";     // FORMAT UNSUPPORTED BY JAVASCRIPT -- REQUIRES CUSTOM PARSING
//
function setDateFormat(outDateFormat) {

calDateFormat    = outDateFormat;
//calDateFormat    = "mm/dd/yyyy hh:nn xx";

}
calDateFormat    = "dd/mm/yyyy";

// CALENDAR COLORS
topBackground    = "95b1c1";//   "#336699";         // BG COLOR OF THE TOP FRAME
bottomBackground = "e8e3d7"; //"#E7EEF0";         // BG COLOR OF THE BOTTOM FRAME
tableBGColor     = "95b1c1";//"e8e3d7";"#336699";         // BG COLOR OF THE BOTTOM FRAME'S TABLE
cellColor        = "e8e3d7"; //"#E7EEF0";     // TABLE CELL BG COLOR OF THE DATE CELLS IN THE BOTTOM FRAME
headingCellColor = "95b1c1";"#336699";         // TABLE CELL BG COLOR OF THE WEEKDAY ABBREVIATIONS
headingTextColor = "#FFFFFF";         // TEXT COLOR OF THE WEEKDAY ABBREVIATIONS
dateColor        = "blue";          // TEXT COLOR OF THE LISTED DATES (1-28+)
focusColor       = "#ff0000";       // TEXT COLOR OF THE SELECTED DATE (OR CURRENT DATE)
hoverColor       = "darkred";       // TEXT COLOR OF A LINK WHEN YOU HOVER OVER IT
fontStyle        = "12pt arial, helvetica";           // TEXT STYLE FOR DATES
headingFontStyle = "10pt arial, helvetica";      // TEXT STYLE FOR WEEKDAY ABBREVIATIONS

// FORMATTING PREFERENCES
bottomBorder  = false;        // TRUE/FALSE (WHETHER TO DISPLAY BOTTOM CALENDAR BORDER)
tableBorder   = 0;            // SIZE OF CALENDAR TABLE BORDER (BOTTOM FRAME) 0=none



// END USER-EDITABLE SECTION -------------------------------------------------------



// DETERMINE BROWSER BRAND
var isNav = false;
var isIE  = false;

// ASSUME IT'S EITHER NETSCAPE OR MSIE
if (navigator.appName == "Netscape") {
    isNav = true;
}
else {
    isIE = true;
}

// GET CURRENTLY SELECTED LANGUAGE
selectedLanguage = navigator.language;

//Time standard
var timestandard;


// PRE-BUILD PORTIONS OF THE CALENDAR WHEN THIS JS LIBRARY LOADS INTO THE BROWSER
buildCalParts();



// CALENDAR FUNCTIONS BEGIN HERE ---------------------------------------------------



// SET THE INITIAL VALUE OF THE GLOBAL DATE FIELD
function setDateField(dateField) {

    // ASSIGN THE INCOMING FIELD OBJECT TO A GLOBAL VARIABLE
    calDateField = dateField;

    // GET THE VALUE OF THE INCOMING FIELD
    inDate = dateField.value;

    // SET calDate TO THE DATE IN THE INCOMING FIELD OR DEFAULT TO TODAY'S DATE
    setInitialDate();

    // THE CALENDAR FRAMESET DOCUMENTS ARE CREATED BY JAVASCRIPT FUNCTIONS
    calDocTop    = buildTopCalFrame();
    calDocBottom = buildBottomCalFrame(showOldDateLinks);
}


// SET THE INITIAL VALUE OF THE GLOBAL DATE FIELD
function setDateFieldFormatA(dateField,showOldDatelinks) {

     //Assign the date Format A
     calDateFormat    = "mm/dd/yyyy hh:nn xx";

    // ASSIGN THE INCOMING FIELD OBJECT TO A GLOBAL VARIABLE
    calDateField = dateField;

    // GET THE VALUE OF THE INCOMING FIELD
    inDate = dateField.value;

    // SET calDate TO THE DATE IN THE INCOMING FIELD OR DEFAULT TO TODAY'S DATE
    setInitialDate();
    //set to show old date links 
    showOldDateLinks =showOldDatelinks;

    // THE CALENDAR FRAMESET DOCUMENTS ARE CREATED BY JAVASCRIPT FUNCTIONS
    calDocTop    = buildTopCalFrame();
    calDocBottom = buildBottomCalFrame(showOldDateLinks);
}





// SET THE INITIAL CALENDAR DATE TO TODAY OR TO THE EXISTING VALUE IN dateField
function setInitialDate() {
   
    // CREATE A NEW DATE OBJECT (WILL GENERALLY PARSE CORRECT DATE EXCEPT WHEN "." IS USED AS A DELIMITER)
    // (THIS ROUTINE DOES *NOT* CATCH ALL DATE FORMATS, IF YOU NEED TO PARSE A CUSTOM DATE FORMAT, DO IT HERE)
    calDate = new Date(inDate);

    // IF THE INCOMING DATE IS INVALID, USE THE CURRENT DATE
    if (isNaN(calDate)) {

        // ADD CUSTOM DATE PARSING HERE
        // IF IT FAILS, SIMPLY CREATE A NEW DATE OBJECT WHICH DEFAULTS TO THE CURRENT DATE
        calDate = new Date();
    }

    // KEEP TRACK OF THE CURRENT DAY VALUE
    calDay  = calDate.getDate();
    
    // SET DAY VALUE TO 1... TO AVOID JAVASCRIPT DATE CALCULATION ANOMALIES
    // (IF THE MONTH CHANGES TO FEB AND THE DAY IS 30, THE MONTH WOULD CHANGE TO MARCH
    //  AND THE DAY WOULD CHANGE TO 2.  SETTING THE DAY TO 1 WILL PREVENT THAT)
    calDate.setDate(1);
}


// POPUP A WINDOW WITH THE CALENDAR IN IT
function showCalendar(dateField) {

    // SET INITIAL VALUE OF THE DATE FIELD AND CREATE TOP AND BOTTOM FRAMES
    setDateField(dateField);

    // USE THE JAVASCRIPT-GENERATED DOCUMENTS (calDocTop, calDocBottom) IN THE FRAMESET
    calDocFrameset = 
        "<HTML><HEAD><TITLE>Calendar</TITLE></HEAD>\n" +
        "<FRAMESET ROWS='70,*' FRAMEBORDER='0'>\n" +
        "  <FRAME NAME='topCalFrame' SRC='javascript:calDocTop' SCROLLING='no'>\n" +
        "  <FRAME NAME='bottomCalFrame' SRC='javascript:calDocBottom' SCROLLING='no'>\n" +
        "</FRAMESET>\n";

    // DISPLAY THE CALENDAR IN A NEW POPUP WINDOW
    top.newWin = window.open("javascript:calDocFrameset", "calWin", winPrefs);
    top.newWin.focus();
}


// CREATE THE TOP CALENDAR FRAME
function buildTopCalFrame() {
	
    // CREATE THE TOP FRAME OF THE CALENDAR
    var calDoc =
        "<input type=hidden id=curday value=''>" +
        "<CENTER>" +
        "<TABLE CELLPADDING=0 CELLSPACING=1 BORDER=0 ALIGN=CENTER>" +
        "<TR><TD COLSPAN=7 NOWRAP>" +
        "<CENTER>" +
        getMonthSelect() +
        "<INPUT id='year' VALUE='" + calDate.getFullYear() + "'TYPE=TEXT class='submitDoc' SIZE=4 MAXLENGTH=4 onChange='javascript:setYear()'>" +
        "</CENTER>" +
        "</TD>" +
        "</TR>" +
        "<TR>" +
        "<TD COLSPAN=7 ALIGN=CENTER>" +
        "<INPUT " +
        "TYPE=BUTTON class='submitDoc' NAME='previousYear' VALUE='<<'    onClick='setPreviousYear()'><INPUT " +
        "TYPE=BUTTON class='submitDoc' NAME='previousMonth' VALUE=' < '   onClick='setPreviousMonth()'><INPUT " +
        "TYPE=BUTTON class='submitDoc' NAME='today' VALUE='Today' onClick='setToday()'><INPUT " +
        "TYPE=BUTTON class='submitDoc' NAME='nextMonth' VALUE=' > '   onClick='setNextMonth()'><INPUT " +
        "TYPE=BUTTON class='submitDoc' NAME='nextYear' VALUE='>>'    onClick='setNextYear()'>" +
        "</TD>" +
        "</TR>" +
        "</TABLE>" +
        "</CENTER>";

    return calDoc;
}


// CREATE THE BOTTOM CALENDAR FRAME 
// (THE MONTHLY CALENDAR)
function buildBottomCalFrame(showOldDateLinks) {       
	
    // START CALENDAR DOCUMENT
    //var calDoc = calendarBegin;
    
    var calDoc = "<CENTER>";//"<BODY BGCOLOR='" + bottomBackground + "' topmargin=10 onload='makeDateString(" + calDay + ")'>" +
        	  

	hour = calDate.getHours();
	minute = calDate.getMinutes();
	
	if(minute < 10) {
		minute = "0" + minute;
	}
	
	if (hour == 0) {
		hour = 12;
		ampmField = "AM";
	}
	else if (hour < 12) {
		ampmField = "AM";
	}
	else if (hour > 12) {
		hour -= 12;
		ampmField = "PM";
	}
	else {
		ampmField = "PM";
	}


	calTimeRow = "<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0" + " ALIGN=CENTER BGCOLOR='" + tableBGColor + "'><TR><TD ALIGN=CENTER BGCOLOR='" + cellColor + "'><form name=timefrm><b><font face=arial size=-1>Time:</font></b> <SELECT onChange='setTime()' id='hourlist'>";
	
	for(j=1;j<=12;j++) {
		if (j == hour) {
			calTimeRow += "<OPTION SELECTED VALUE=" + j + ">" + j;
		}
		else {
			calTimeRow += "<OPTION VALUE=" + j + ">" + j;
		}
	}
	
	calTimeRow += "</SELECT>";
	calTimeRow += "&nbsp;:&nbsp;";
	calTimeRow += "<SELECT onChange='setTime()' id='minlist'>";

	for(j=0;j<60;j++) {
		
		if (j < 10) {
			j = "0" + j;
		}
		
		if (j == minute) {
			calTimeRow += "<OPTION SELECTED VALUE=" + j +">" + j;
		}
		else {
			calTimeRow += "<OPTION VALUE=" + j + ">" + j;
		}
	}

	calTimeRow += "</SELECT>";
	calTimeRow += "&nbsp;";
	
	if (ampmField == "AM")
		calTimeRow += "<SELECT onChange='setTime()' id='ampmlist'><OPTION SELECTED VALUE=AM>AM<OPTION VALUE=PM>PM</SELECT>";
	else
		calTimeRow += "<SELECT onChange='setTime()' id='ampmlist'><OPTION VALUE=AM>AM<OPTION SELECTED VALUE=PM>PM</SELECT>";
	
	calTimeRow += "</form></TD></TR></TABLE>";
	
	


    // NAVIGATOR NEEDS A TABLE CONTAINER TO DISPLAY THE TABLE OUTLINES PROPERLY
    if (isNav) {
        calDoc += 
            "<TABLE CELLPADDING=0 CELLSPACING=1 BORDER=" + tableBorder + " ALIGN=CENTER BGCOLOR='" + tableBGColor + "'><TR><TD>";
    }

    // BUILD WEEKDAY HEADINGS
    calDoc +=
        "<TABLE CELLPADDING=0 CELLSPACING=1 BORDER=" + tableBorder + " ALIGN=CENTER BGCOLOR='" + tableBGColor + "'>" +
        weekdays +
        "<TR>";


    // GET MONTH, AND YEAR FROM GLOBAL CALENDAR DATE
    month   = calDate.getMonth();
    year    = calDate.getFullYear();
    var show="false";
    currDate= new Date();
    month1 = currDate.getMonth();
    year1= currDate.getYear();
    var show="false";
    var show1="false";
    
    if(year>year1){
	      show ="true";
      }
    
      if(year==year1 && month>month1){
      show ="true";
      }
      if(year==year1 && month==month1){
            show1 ="true";
            show="false";
      }
      
    
    
    
    
    
    

    // GET GLOBALLY-TRACKED DAY VALUE (PREVENTS JAVASCRIPT DATE ANOMALIES)
    day     = calDay;

    var i   = 0;

    // DETERMINE THE NUMBER OF DAYS IN THE CURRENT MONTH
    var days = getDaysInMonth();
    
    // IF GLOBAL DAY VALUE IS > THAN DAYS IN MONTH, HIGHLIGHT LAST DAY IN MONTH
    if (day > days) {
        day = days;
    }

    // DETERMINE WHAT DAY OF THE WEEK THE CALENDAR STARTS ON
    var firstOfMonth = new Date (year, month, 1);

    // GET THE DAY OF THE WEEK THE FIRST DAY OF THE MONTH FALLS ON
    var startingPos  = firstOfMonth.getDay();
    days += startingPos;

    // KEEP TRACK OF THE COLUMNS, START A NEW ROW AFTER EVERY 7 COLUMNS
    var columnCount = 0;

    // MAKE BEGINNING NON-DATE CELLS BLANK
    for (i = 0; i < startingPos; i++) {

        calDoc += blankCell;
	columnCount++;
    }

    // SET VALUES FOR DAYS OF THE MONTH
    var currentDay = 0;
    var dayType    = "weekday";

    // DATE CELLS CONTAIN A NUMBER
    for (i = startingPos; i < days; i++) {

	var paddingChar = "&nbsp;";

        // ADJUST SPACING SO THAT ALL LINKS HAVE RELATIVELY EQUAL WIDTHS
        if (i-startingPos+1 < 10) {
            padding = "&nbsp;&nbsp;";
        }
        else {
            padding = "&nbsp;";
        }

        // GET THE DAY CURRENTLY BEING WRITTEN
        currentDay = i-startingPos+1;

        // SET THE TYPE OF DAY, THE focusDay GENERALLY APPEARS AS A DIFFERENT COLOR
        if (currentDay == day) {
            dayType = "focusDay";
        }
        else {
            dayType = "weekDay";
        }

        // ADD THE DAY TO THE CALENDAR STRING
        //changes for disabling old dates..
        
        
        
       if (showOldDateLinks=="false" ) {
        
        if (show=='true') {
         
        calDoc += "<TD align=center bgcolor='" + cellColor + "'>" +
                  "<a class='" + dayType + "' href='javascript:writeCalendar2(" + 
                  currentDay + ")'>" + padding + currentDay + paddingChar + "</a></TD>";
                  
         }else if(show1=='true' && currentDay>=currDate.getDate()){
         	calDoc += "<TD align=center bgcolor='" + cellColor + "'>" +
	                   "<a class='" + dayType + "' href='javascript:writeCalendar2(" + 
                  currentDay + ")'>" + padding + currentDay + paddingChar + "</a></TD>";
         
         		}
         else
         
         	{      
         	calDoc += "<TD align=center style='font-size:x-small;' bgcolor='" + cellColor + "'>"+currentDay+"</TD>" ;
	                   
                }
       
	}
	
	else {
	calDoc += "<TD align=center bgcolor='" + cellColor + "'>" +
	                  "<a class='" + dayType + "' href='javascript:writeCalendar2(" + 
                  currentDay + ")'>" + padding + currentDay + paddingChar + "</a></TD>";
	
	}
        columnCount++;

        // START A NEW ROW WHEN NECESSARY
        if (columnCount % 7 == 0) {
            calDoc += "</TR><TR>";
        }
    }

    // MAKE REMAINING NON-DATE CELLS BLANK
    for (i=days; i<42; i++)  {

        calDoc += blankCell;
	columnCount++;

        // START A NEW ROW WHEN NECESSARY
        if (columnCount % 7 == 0) {
            calDoc += "</TR>";
            if (i<41) {
                calDoc += "<TR>";
            }
        }
    }
    

    // WHETHER OR NOT TO DISPLAY A THICK LINE BELOW THE CALENDAR
    if (bottomBorder) {
        calDoc += "";
    }

	// END THE CALENDAR TABLE
	calDoc += "</TABLE><br>";
	
    calDoc += calTimeRow;

    // NAVIGATOR NEEDS A TABLE CONTAINER TO DISPLAY THE BORDERS PROPERLY
    if (isNav) {
        calDoc += "</TD></TR></TABLE>";
    }

	if (isNav) {
		calDoc += "<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=1 BORDER=0 BGCOLOR='" + cellColor + "' ALIGN=CENTER><TR></TR><TR><TD>";
	}

	// ADD A TEXTBOX TO THE BOTTOM OF THE CALENDAR TO DISPLAY THE SELECTED DATE
    calDoc += "<TABLE CELLPADDING=0 CELLSPACING=1 ALIGN=CENTER BORDER=0 BGCOLOR='" + cellColor + "'><TR></TR><TR><TD COLSPAN=2 ALIGN=CENTER><input type=text style='font:x-small;' class='submitDoc' id='dispDateField' value='' readonly onfocus='this.blur()'></TD><td><span style='font-size:x-small;'>(GMT)</td></TR>";

	// ADD THE OK AND CANCEL BUTTONS TO THE BOTTOM OF THE CALENDAR
    calDoc += "<TR></TR><TR></TR><TR><TD ALIGN=CENTER VALIGN=TOP><INPUT TYPE=BUTTON CLASS='submitDoc' VALUE='   OK   ' onClick='returnDate()'></TD><TD ALIGN=CENTER VALIGN=TOP><INPUT TYPE=BUTTON CLASS='submitDoc' VALUE=Cancel ONCLICK='cancel();'></TD></TR></TABLE>";
    
	if (isNav) {
		calDoc += "</TD></TR></TABLE>";
	}

    // FINISH THE NEW CALENDAR PAGE
    //calDoc += calendarEnd;
    
    // RETURN THE COMPLETED CALENDAR PAGE
    return calDoc;
}

function cancel()  {
    document.all("calendarArea").style.display = "none";
}


// WRITE THE MONTHLY CALENDAR TO THE BOTTOM CALENDAR FRAME
function writeCalendar() {

    // CREATE THE NEW CALENDAR FOR THE SELECTED MONTH & YEAR
    calDocBottom = buildBottomCalFrame(showOldDateLinks);


    //document.all("").innerHTML = calDocBottom;
    document.all("middleSection").innerHTML = calDocBottom;
    //top.newWin.frames['bottomCalFrame'].document.close();
    
    makeDateString(calDay);
}


// WRITE THE MONTHLY CALENDAR TO THE BOTTOM CALENDAR FRAME
// THIS IS USED TO REWRITE THE CALENDAR EVERY TIME A DATE CLICKED
// THIS DOES THE SAME JOB AS THE WRITECALENDAR FUNCTION BUT ACCEPTS A
// DATE ARGUMENT.  THIS WAS CREATED TO MINIMIZE THE IMPACT ON THE CODE.
function writeCalendar2(clickedDate) {
	
	calDay = clickedDate;

    // CREATE THE NEW CALENDAR FOR THE SELECTED MONTH & YEAR
    calDocBottom = buildBottomCalFrame(showOldDateLinks);

    // WRITE THE NEW CALENDAR TO THE BOTTOM FRAME
    document.all("middleSection").innerHTML = calDocBottom;
    
    makeDateString(calDay);
}

// SET THE CALENDAR TO TODAY'S DATE AND DISPLAY THE NEW CALENDAR
function setToday() {

    // SET GLOBAL DATE TO TODAY'S DATE
    calDate = new Date();
    
    calDay = calDate.getDate();

    // SET DAY MONTH AND YEAR TO TODAY'S DATE
    var month = calDate.getMonth();
    var year  = calDate.getFullYear();

    // SET MONTH IN DROP-DOWN LIST
    document.all("month").selectedIndex = month;

    // SET YEAR VALUE
    document.all("year").value = year;

    // DISPLAY THE NEW CALENDAR
    writeCalendar();
    
}


// SET THE GLOBAL DATE TO THE NEWLY ENTERED YEAR AND REDRAW THE CALENDAR
function setYear() {

    // GET THE NEW YEAR VALUE
    var year  = document.all("year").value;

    // IF IT'S A FOUR-DIGIT YEAR THEN CHANGE THE CALENDAR
    if (isFourDigitYear(year)) {

		var month = calDate.getMonth();

	    // DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
	    var days = getDaysInMonth2(month, year);
	    
	    if (days < calDay) {
	    	calDate.setDate(days);
	    	calDay = days;
	    }
			
        calDate.setFullYear(year);
        writeCalendar();
    }
    else {
        // HIGHLIGHT THE YEAR IF THE YEAR IS NOT FOUR DIGITS IN LENGTH
        year.focus();
        year.select();
    }
    
}


// SET THE GLOBAL DATE TO THE SELECTED MONTH AND REDRAW THE CALENDAR
function setCurrentMonth() {

    // GET THE NEWLY SELECTED MONTH AND CHANGE THE CALENDAR ACCORDINGLY
    var month = document.all("month").selectedIndex;
		
    // DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
    var days = getDaysInMonth2(month, calDate.getFullYear());
    
    if (days < calDay) {
    	calDate.setDate(days);
    	calDay = days;
    }
		
    calDate.setMonth(month);
    writeCalendar();
}


// SET THE GLOBAL DATE TO THE PREVIOUS YEAR AND REDRAW THE CALENDAR
function setPreviousYear() {

    var year  = document.all("year").value;

    if (isFourDigitYear(year) && year > 1000) {
        year--;

		var month = calDate.getMonth();

	    // DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
	    var days = getDaysInMonth2(month, year);
	    
	    if (days < calDay) {
	    	calDate.setDate(days);
	    	calDay = days;
	    }
			
        calDate.setFullYear(year);
        document.all("year").value = year;
        writeCalendar();
    }
}


// SET THE GLOBAL DATE TO THE PREVIOUS MONTH AND REDRAW THE CALENDAR
function setPreviousMonth() {

    var year  = document.all("year").value;
    if (isFourDigitYear(year)) {
        var month = document.all("month").selectedIndex;

        // IF MONTH IS JANUARY, SET MONTH TO DECEMBER AND DECREMENT THE YEAR
        if (month == 0) {
            month = 11;
            if (year > 1000) {
                year--;
                calDate.setFullYear(year);
                document.all("year").value = year;
            }
        }
        else {
            month--;
        }

		
	    // DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
	    var days = getDaysInMonth2(month, calDate.getFullYear());
	    
	    if (days < calDay) {
	    	calDate.setDate(days);
	    	calDay = days;
	    }
		
        calDate.setMonth(month);
        
        document.all("month").selectedIndex = month;
        writeCalendar();
    }
}


// SET THE GLOBAL DATE TO THE NEXT MONTH AND REDRAW THE CALENDAR
function setNextMonth() {

    var year = parseInt(document.all("year").value);

    if (isFourDigitYear(year)) {
        var month = document.all("month").selectedIndex;

        // IF MONTH IS DECEMBER, SET MONTH TO JANUARY AND INCREMENT THE YEAR
        if (month == 11) {
            month = 0;
            year++;
            calDate.setFullYear(year);
            document.all("year").value = year;
        }
        else {
            month++;
        }
			
	    // DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
	    var days = getDaysInMonth2(month, calDate.getFullYear());
	    
	    if (days < calDay) {
	    	calDate.setDate(days);
	    	calDay = days;
	    }
			
        calDate.setMonth(month);
        document.all("month").selectedIndex = month;
        writeCalendar();
    }
}


// SET THE GLOBAL DATE TO THE NEXT YEAR AND REDRAW THE CALENDAR
function setNextYear() {

    var year  = document.all("year").value;
    if (isFourDigitYear(year)) {
        year++;

		var month = calDate.getMonth();

	    // DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
	    var days = getDaysInMonth2(month, year);
	    
	    if (days < calDay) {
	    	calDate.setDate(days);
	    	calDay = days;
	    }
			
        calDate.setFullYear(year);
        document.all("year").value = year;
        writeCalendar();
    }
}


// GET NUMBER OF DAYS IN MONTH
function getDaysInMonth()  {

    var days;
    var month = calDate.getMonth()+1;
    var year  = calDate.getFullYear();
    

    // RETURN 31 DAYS
    if (month==1 || month==3 || month==5 || month==7 || month==8 ||
        month==10 || month==12)  {
        days=31;
    }
    // RETURN 30 DAYS
    else if (month==4 || month==6 || month==9 || month==11) {
        days=30;
    }
    // RETURN 29 DAYS
    else if (month==2)  {
        if (isLeapYear(year)) {
            days=29;
        }
        // RETURN 28 DAYS
        else {
            days=28;
        }
    }
    
    return (days);
}


// GET NUMBER OF DAYS IN MONTH (SECOND FUNCTION WHICH ACCEPTS AN ARGUMENT)
function getDaysInMonth2(monthNum, yearArg)  {

    var days;
    var month = monthNum+1;
    var year  = yearArg;
    

    // RETURN 31 DAYS
    if (month==1 || month==3 || month==5 || month==7 || month==8 ||
        month==10 || month==12)  {
        days=31;
    }
    // RETURN 30 DAYS
    else if (month==4 || month==6 || month==9 || month==11) {
        days=30;
    }
    // RETURN 29 DAYS
    else if (month==2)  {
        if (isLeapYear(year)) {
            days=29;
        }
        // RETURN 28 DAYS
        else {
            days=28;
        }
    }
    
    return (days);
}
// CHECK TO SEE IF YEAR IS A LEAP YEAR
function isLeapYear (Year) {

    if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0)) {
        return (true);
    }
    else {
        return (false);
    }
}


// ENSURE THAT THE YEAR IS FOUR DIGITS IN LENGTH
function isFourDigitYear(year) {

    if (year.length != 4) {
        document.all("year").value = calDate.getFullYear();
        //year.select();
        //year.focus();
        return true;
    }
    else {
        return true;
    }
}


// BUILD THE MONTH SELECT LIST
function getMonthSelect() {

    // BROWSER LANGUAGE CHECK DONE PREVIOUSLY (navigator.language())
    // FIRST TWO CHARACTERS OF LANGUAGE STRING SPECIFIES THE LANGUAGE
    // (THE LAST THREE OPTIONAL CHARACTERS SPECIFY THE LANGUAGE SUBTYPE)
    // SET THE NAMES OF THE MONTH TO THE PROPER LANGUAGE (DEFAULT TO ENGLISH)

    // IF FRENCH
    if (selectedLanguage == "fr") {
        monthArray = new Array('Janvier', 'F�vrier', 'Mars', 'Avril', 'Mai', 'Juin',
                               'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'D�cembre');
    }
    // IF GERMAN
    else if (selectedLanguage == "de") {
        monthArray = new Array('Januar', 'Februar', 'M�rz', 'April', 'Mai', 'Juni',
                               'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');
    }
    // IF SPANISH
    else if (selectedLanguage == "es") {
        monthArray = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                               'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    }
    // DEFAULT TO ENGLISH
    else {
        monthArray = new Array('January', 'February', 'March', 'April', 'May', 'June',
                               'July', 'August', 'September', 'October', 'November', 'December');
    }

    // DETERMINE MONTH TO SET AS DEFAULT
    var activeMonth = calDate.getMonth();

    // START HTML SELECT LIST ELEMENT
    monthSelect = "<SELECT id='month' onChange='setCurrentMonth()'>";

    // LOOP THROUGH MONTH ARRAY
    for (i in monthArray) {
        
        // SHOW THE CORRECT MONTH IN THE SELECT LIST
        if (i == activeMonth) {
            monthSelect += "<OPTION SELECTED>" + monthArray[i] + "\n";
        }
        else {
            monthSelect += "<OPTION>" + monthArray[i] + "\n";
        }
    }
    monthSelect += "</SELECT>";

    // RETURN A STRING VALUE WHICH CONTAINS A SELECT LIST OF ALL 12 MONTHS
    return monthSelect;
}


// SET DAYS OF THE WEEK DEPENDING ON LANGUAGE
function createWeekdayList() {

    weekdayList  = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    weekdayArray = new Array('Su','Mo','Tu','We','Th','Fr','Sa');

    // START HTML TO HOLD WEEKDAY NAMES IN TABLE FORMAT
    var weekdays = "<TR style='font-size:x-small;' BGCOLOR='" + headingCellColor + "'>";

    // LOOP THROUGH WEEKDAY ARRAY
    for (i in weekdayArray) {

        weekdays += "<TD  align=center>" + weekdayArray[i] + "</TD>";
    }
    weekdays += "</TR>";

    // RETURN TABLE ROW OF WEEKDAY ABBREVIATIONS TO DISPLAY ABOVE THE CALENDAR
    return weekdays;
}


// PRE-BUILD PORTIONS OF THE CALENDAR (FOR PERFORMANCE REASONS)
function buildCalParts() {

    // GENERATE WEEKDAY HEADERS FOR THE CALENDAR
    weekdays = createWeekdayList();

    // BUILD THE BLANK CELL ROWS
    blankCell = "<TD align=center bgcolor='" + cellColor + "'>&nbsp;&nbsp;&nbsp;</TD>";

    // BUILD THE TOP PORTION OF THE CALENDAR PAGE USING CSS TO CONTROL SOME DISPLAY ELEMENTS
    calendarBegin =
        "<HTML>" +
        "<HEAD>" +
        // STYLESHEET DEFINES APPEARANCE OF CALENDAR
       /* "<STYLE type='text/css'>" +
        "<!--" +
        "TD.heading { text-decoration: none; color:" + headingTextColor + "; font: " + headingFontStyle + "; }" +
        "A.focusDay { color: " + focusColor + "; text-decoration: none; font: " + fontStyle + "; }" +
        "A.focusDay:hover { color: " + focusColor + "; text-decoration: none; font: " + fontStyle + "; }" +
        "A.weekDay { color: " + dateColor + "; text-decoration: none; font: " + fontStyle + "; }" +
        "A.weekDay:hover { color: " + hoverColor + "; font: " + fontStyle + "; }" +
//        "A:visited { color: " + focusColor + "; text-decoration: none; font: " + fontStyle + "; }" +
//        "INPUT.BUTTONS { font-family:  Arial, Verdana, Geneva, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #ffffff; background-color: #8c95cb; cursor: hand;}" +
        "-->" +
        "</STYLE>" + */
        "<link href='/b2b/auction/css/main.css' type='text/css' rel='stylesheet'>" +
        "</HEAD>";


    // BUILD THE BOTTOM PORTION OF THE CALENDAR PAGE
    calendarEnd = "";


    calendarEnd +=
        "</CENTER>" +
        "</BODY>" +
        "</HTML>";

}


// REPLACE ALL INSTANCES OF find WITH replace
// inString: the string you want to convert
// find:     the value to search for
// replace:  the value to substitute
//
// usage:    jsReplace(inString, find, replace);
// example:  jsReplace("To be or not to be", "be", "ski");
//           result: "To ski or not to ski"
//
function jsReplace(inString, find, replace) {

    var outString = "";

    if (!inString) {
        return "";
    }

    // REPLACE ALL INSTANCES OF find WITH replace
    if (inString.indexOf(find) != -1) {
        // SEPARATE THE STRING INTO AN ARRAY OF STRINGS USING THE VALUE IN find
        t = inString.split(find);

        // JOIN ALL ELEMENTS OF THE ARRAY, SEPARATED BY THE VALUE IN replace
        return (t.join(replace));
    }
    else {
        return inString;
    }
}


// JAVASCRIPT FUNCTION -- DOES NOTHING (USED FOR THE HREF IN THE CALENDAR CALL)
function doNothing() {
}


// ENSURE THAT VALUE IS TWO DIGITS IN LENGTH
function makeTwoDigit(inValue) {

    var numVal = parseInt(inValue, 10);

    // VALUE IS LESS THAN TWO DIGITS IN LENGTH
    if (numVal < 10) {

        // ADD A LEADING ZERO TO THE VALUE AND RETURN IT
        return("0" + numVal);
    }
    else {
        return numVal;
    }
}


// SET FIELD VALUE TO THE DATE SELECTED AND CLOSE THE CALENDAR WINDOW
function makeDateString(inDay)
{

	calDay = inDay;

	calDate.setDate(inDay);

    // inDay = THE DAY THE USER CLICKED ON
    calDate.setDate(inDay);
    
    // SET THE DATE RETURNED TO THE USER
    var day           = calDate.getDate();
    var month         = calDate.getMonth()+1;
    var year          = calDate.getFullYear();
    var monthString   = monthArray[calDate.getMonth()];
    var monthAbbrev   = monthString.substring(0,3);
    var weekday       = weekdayList[calDate.getDay()];
    var weekdayAbbrev = weekday.substring(0,3);

    outDate = calDateFormat;

    // RETURN TWO DIGIT DAY
    if (calDateFormat.indexOf("DD") != -1) {
        day = makeTwoDigit(day);
        outDate = jsReplace(outDate, "DD", day);
    }
    // RETURN ONE OR TWO DIGIT DAY
    else if (calDateFormat.indexOf("dd") != -1) {
        outDate = jsReplace(outDate, "dd", day);
    }

    // RETURN TWO DIGIT MONTH
    if (calDateFormat.indexOf("MM") != -1) {
        month = makeTwoDigit(month);
        outDate = jsReplace(outDate, "MM", month);
    }
    // RETURN ONE OR TWO DIGIT MONTH
    else if (calDateFormat.indexOf("mm") != -1) {
        outDate = jsReplace(outDate, "mm", month);
    }

    // RETURN FOUR-DIGIT YEAR
    if (calDateFormat.indexOf("yyyy") != -1) {
        outDate = jsReplace(outDate, "yyyy", year);
    }
    // RETURN TWO-DIGIT YEAR
    else if (calDateFormat.indexOf("yy") != -1) {
        var yearString = "" + year;
        var yearString = yearString.substring(2,4);
        outDate = jsReplace(outDate, "yy", yearString);
    }
    // RETURN FOUR-DIGIT YEAR
    else if (calDateFormat.indexOf("YY") != -1) {
        outDate = jsReplace(outDate, "YY", year);
    }

    // RETURN DAY OF MONTH (Initial Caps)
    if (calDateFormat.indexOf("Month") != -1) {
        outDate = jsReplace(outDate, "Month", monthString);
    }
    // RETURN DAY OF MONTH (lowercase letters)
    else if (calDateFormat.indexOf("month") != -1) {
        outDate = jsReplace(outDate, "month", monthString.toLowerCase());
    }
    // RETURN DAY OF MONTH (UPPERCASE LETTERS)
    else if (calDateFormat.indexOf("MONTH") != -1) {
        outDate = jsReplace(outDate, "MONTH", monthString.toUpperCase());
    }

    // RETURN DAY OF MONTH 3-DAY ABBREVIATION (Initial Caps)
    if (calDateFormat.indexOf("Mon") != -1) {
        outDate = jsReplace(outDate, "Mon", monthAbbrev);
    }
    // RETURN DAY OF MONTH 3-DAY ABBREVIATION (lowercase letters)
    else if (calDateFormat.indexOf("mon") != -1) {
        outDate = jsReplace(outDate, "mon", monthAbbrev.toLowerCase());
    }
    // RETURN DAY OF MONTH 3-DAY ABBREVIATION (UPPERCASE LETTERS)
    else if (calDateFormat.indexOf("MON") != -1) {
        outDate = jsReplace(outDate, "MON", monthAbbrev.toUpperCase());
    }

    // RETURN WEEKDAY (Initial Caps)
    if (calDateFormat.indexOf("Weekday") != -1) {
        outDate = jsReplace(outDate, "Weekday", weekday);
    }
    // RETURN WEEKDAY (lowercase letters)
    else if (calDateFormat.indexOf("weekday") != -1) {
        outDate = jsReplace(outDate, "weekday", weekday.toLowerCase());
    }
    // RETURN WEEKDAY (UPPERCASE LETTERS)
    else if (calDateFormat.indexOf("WEEKDAY") != -1) {
        outDate = jsReplace(outDate, "WEEKDAY", weekday.toUpperCase());
    }

    // RETURN WEEKDAY 3-DAY ABBREVIATION (Initial Caps)
    if (calDateFormat.indexOf("Wkdy") != -1) {
        outDate = jsReplace(outDate, "Wkdy", weekdayAbbrev);
    }
    // RETURN WEEKDAY 3-DAY ABBREVIATION (lowercase letters)
    else if (calDateFormat.indexOf("wkdy") != -1) {
        outDate = jsReplace(outDate, "wkdy", weekdayAbbrev.toLowerCase());
    }
    // RETURN WEEKDAY 3-DAY ABBREVIATION (UPPERCASE LETTERS)
    else if (calDateFormat.indexOf("WKDY") != -1) {
        outDate = jsReplace(outDate, "WKDY", weekdayAbbrev.toUpperCase());
    }
    

    outDate = jsReplace(outDate, "hh", hour);
    outDate = jsReplace(outDate, "nn", minute);
    outDate = jsReplace(outDate, "xx", ampmField);

	// STORE THE VALUE OF THE SELECTED DATE IN THE HIDDEN FIELD IN THE BOTTOM FRAME
    document.all("dispDateField").value = outDate;

}


function returnDate() {
	
	makeDateString(calDay);

	// SET THE VALUE OF THE FIELD THAT WAS PASSED TO THE CALENDAR
	calDateField.value = document.all("dispDateField").value;
	
    // GIVE FOCUS BACK TO THE DATE FIELD
    calDateField.focus();

    // CLOSE THE CALENDAR WINDOW
    document.all("calendarArea").style.display = "none";
}

function setTime() {
	
    // GET THE NEW TIME VALUE
    //var docroot = top.newWin.frames['bottomCalFrame'].document.timefrm;
    var hr = document.all("hourlist").options[document.all("hourlist").selectedIndex].value;
    var mn = document.all("minlist").options[document.all("minlist").selectedIndex].value;
    var ampm = document.all("ampmlist").options[document.all("ampmlist").selectedIndex].value;

	if (ampm == "AM") {
		if(hr == 12) {
			hr = 0;
		}		
	}
	else {
		if (hr < 12) {
			hr = parseInt(hr) + 12;
		}
	}

	calDate.setHours(hr);
	calDate.setMinutes(mn);
	
    writeCalendar();

}

