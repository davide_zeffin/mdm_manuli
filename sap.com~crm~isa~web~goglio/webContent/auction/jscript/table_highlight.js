/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       table_highlight.js                 */
/* Description:    highlights a single table row      */
/* Version:        1.1                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    17.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/


// global variables
var oldRow;
var oldRowBgColor;

// change actual and old table_row
function changeRowStyle(id) {
  if (!document.layers) {
    if (oldRow != null)
      for (i=0; i<oldRow.length; i++) {
        oldRow[i].style.backgroundColor = oldRowBgColor;
        oldRow[i].style.borderTop = "1px solid #eee";
        oldRow[i].style.borderBottom = "1px solid #eee";
      }

    var actualRow = document.getElementById(id).getElementsByTagName("TD");

    for (i=0; i<actualRow.length; i++) {
      actualRow[i].style.backgroundColor = "#ffffff";
      actualRow[i].style.borderTop = "2px solid #000";
      actualRow[i].style.borderBottom = "2px solid #000";
    }

    if (document.getElementById(id).className == "odd")
      oldRowBgColor = "#e1e1e1";
    else
      oldRowBgColor = "#ccc";

    oldRow = actualRow;
  }
}
