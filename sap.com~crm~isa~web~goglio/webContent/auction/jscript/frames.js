/*****************************************************************************
    File:         frames.js
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler, Franz-Dieter Berger
    Created:      May 2001
    Version:      1.0

    $Revision: #14 $
    $Date: 2001/05/30 $
*****************************************************************************/

/**
 *
 * returns an reference to the top frame
 *
 */
function isaTop() {

    var tmpWnd = window;

    while (tmpWnd.parent != tmpWnd && tmpWnd.name != "isaTop") {
         tmpWnd = tmpWnd.parent;
    }
    return tmpWnd;
}


/**
 *
 * returns an reference to the work_history frame
 *
 */
function work_history() {

    return isaTop().work_history;

}


/**
 *
 * returns an reference to the organizer_nav frame
 *
 */
function organizer_nav() {

    return isaTop().organizer_nav;

}


/**
 *
 * returns an reference to the organizer_content frame
 *
 */
function organizer_content() {

    return isaTop().organizer_content;

}


