
/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       send_confirm.js                    */
/* Description:    confirm.window with reload         */
/* Version:        1.0                                */
/* Author:         m_vogel@pixelpark.com              */
/* Creation-Date:  11.04.2001                         */
/* Last-Update:    03.05.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/


// confirm.window
  function send_confirm() {
    var check = confirm("Wollen Sie den Auftrag entg�ltig absenden?");
    if(check == true) { 
      reloadFrame('frameset_order_confirm.html', 'open_order_with_open_order_grey.html');
    }
  }
