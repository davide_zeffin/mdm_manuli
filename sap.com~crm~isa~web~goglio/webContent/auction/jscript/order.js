/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       order.js                           */
/* Description:    presents order details             */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    29.03.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/


function soft_submit(input) {
      var details_form = parent.details.document.forms[0];
      var wld = "wunschlieferdatum" + details_form.position_number.value;
      var cus = "customer" + details_form.position_number.value;
      if (details_form.wunschlieferdatum != null) {
        document.forms[1].elements[wld].value = details_form.wunschlieferdatum.value;
        wld = "wunschlieferdatum" + input;
        details_form.wunschlieferdatum.value = document.forms[1].elements[wld].value;
      }
      if (details_form.customer != null) {
        document.forms[1].elements[cus].value = details_form.customer.selectedIndex;
        cus = "customer" + input;
        details_form.customer.selectedIndex = document.forms[1].elements[cus].value;
      }

      details_form.position_number.value = input;
}

function changeDetailPosNumber(input){
  var details_form = parent.details.document.forms[0];
  details_form.position_number.value = input.value;
}