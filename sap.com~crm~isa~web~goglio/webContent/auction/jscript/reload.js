/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       reload.js                          */
/* Description:    reloads multiple frames            */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    03.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/


// reloads multiple frames
function reloadFrame(formInputLoc, docLoc, historyLoc, orgLoc) {
  if ((formInputLoc != null) && (formInputLoc != ''))
    top.form_input.location.href = formInputLoc;
  if ((docLoc != null) && (docLoc != ''))
    top.documents.location.href = docLoc;
  if ((historyLoc != null) && (historyLoc != ''))
    top._history.location.href = historyLoc;
  if ((orgLoc != null) && (orgLoc != ''))
    top.organizer_content.location.href = orgLoc;
}

// reloads multiple frames from a satellite
function openerReloadFrame(formInputLoc, docLoc, historyLoc, orgLoc) {
  if ((formInputLoc != null) && (formInputLoc != ''))
    opener.top.form_input.location.href = formInputLoc;
  if ((docLoc != null) && (docLoc != ''))
    opener.top.documents.location.href = docLoc;
  if ((historyLoc != null) && (historyLoc != ''))
    opener.top._history.location.href = historyLoc;
  if ((orgLoc != null) && (orgLoc != ''))
    opener.top.organizer_content.location.href = orgLoc;
}
