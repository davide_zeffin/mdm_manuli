<%@ page import= "java.util.*,com.sap.isa.auction.util.*,com.sap.isa.auction.bean.*,javax.servlet.http.*" %>
<%@ page import="com.sap.isa.businessobject.*,com.sap.isa.core.*,com.sap.isa.auction.*" %>

<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>

<jsp:useBean id="editopportunityForm" scope="session" type="com.sap.isa.auction.actionforms.EditOpportunityForm"/>
<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">
function details(externalId,screen) {
	document.openForm.Id.value = externalId;
	document.openForm.show.value = screen;
        openForm.submit();
}

 function getList(action) {
          document.monitorForm.status.value = action;
          monitorForm.submit();
 }

</script>
</head>

<body bgcolor="95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/confirmedit.jsp"/></div>

<%@ include file="webAuctionHeader.html" %> 



<br>

<table cellspacing="0" cellpadding="0" border="0">
      <tr>
      <td width="820" colspan="3" valign="top">
       <table cellspacing="0" cellpadding="0" width="100%" border="0">
         <tr>
			<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
			<td width="28"><img src="<isa:mimeURL name="auction/images/layout/froff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
			<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>" class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/catsearch.jsp"/>">
			<isa:translate key="eAuctions.seller.create.title"/></a>&nbsp;</td>
			<td width="24"><img src="<isa:mimeURL name="auction/images/layout/offon.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
			<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;
			 <isa:translate key="eAuctions.seller.monitor.title"/>&nbsp;&#160;</td>
			<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkon.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
			<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
	</tr>
       </table>
      </td>
      </tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="e8e3d7" height="500">
	<TR>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
	</TR>
	<tr>
		<td bgcolor="95b1c1">&nbsp;</td>
		<td class="CONTENT"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="30" height="11" border="0" alt="">
		 &nbsp;&nbsp;<isa:translate key="eAuctions.seller.confirm.title"/>&nbsp;&nbsp;
		<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="600" height="11" border="0" alt=""></td>
	</tr>
	<tr>
		<Td bgcolor="95b1c1">&nbsp;</TD>
		<td valign="top">
		<form  action='<isa:webappsURL name="auction/seller/editopportunity.do"/>' name="editopportunityForm">

		<!-- hidden variable to determine if the edit if from an open opportunity
		  or a closed opportunity that is returned to open state -->
		<input type="hidden" name="editaction" value="open"/>
		   <table cellpadding="0" cellspacing="0" class="content" border="0">
		   <%
	   	        String currency = (String) session.getAttribute("Currency"); 

			// get the target groups.
			OpportunityBean opbean = (OpportunityBean) session.getAttribute("oppbeanforbid");
			Object[] targetgroups = null;
			if(opbean!=null) {
				String tgs = (String) opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.TARGET_GROUPS);
				targetgroups = EAUtilities.getInstance().getTargetGroups(tgs,",");
			}
			//TargetGroupListBean  tlistBean = application.get
			// get the target group description and display them,


		   %>


			<Tr>
				<td colspan="3" class="CONTENT">&nbsp;&nbsp;&nbsp;&nbsp;
				<isa:translate key="eAuctions.seller.changeinfoto"/></td>
			</TR>

			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.auctionid"/>:</td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="opportunityId"/></td>
			</tr>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auction.name"/></td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td><jsp:getProperty name="editopportunityForm" property="opportunityName"/></td>
			</tr>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.product.quantity"/></td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="quantity"/></td>
			</tr>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auction.desc"/></td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="description"/></td>
			</tr><br>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.startprice"/>:</td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="startPrice"/> <%=currency%> </td>
			</tr><br>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.reserveprice"/>:</td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="reservePrice"/> <%=currency%></td>
			</tr>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.startdate"/>:</td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="startDate"/></td>
			</tr>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.enddate"/>:</td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="endDate"/></td>
			</tr>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auction.resvexprdate"/></td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="resExpDate"/></td>
			</tr>
			<tr>
				<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auction.termsandconds"/></td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT"><jsp:getProperty name="editopportunityForm" property="terms"/></td>
			</tr>
			<tr>
				<td align="right" class="CONTENT" valign="top"><isa:translate key="eAuctions.seller.auction.targetGroup"/></td>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
				<td class="CONTENT">
				<%if(targetgroups!=null)
					  for(int i=0; i<targetgroups.length;i++) {
					    String temp = (String)targetgroups[i];%>
					       <%=temp%><br>
				<%}%>
				</td>
			</tr>
			<tr>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;<input type="submit" name="yes" value='<isa:translate key="eAuctions.seller.msg.yes"/>' class="submitDoc">&nbsp;&nbsp;&nbsp;
				<input type="button" name="no"class="submitDoc" value='<isa:translate key="eAuctions.seller.msg.no"/>'  
				<%if((opbean.getStatus())==(IStatus.Open)){%>
				  onClick="javascript:location.replace('open.jsp')" class="submitDoc"></td>
				<% } else {%>
				  onClick="javascript:location.replace('published.jsp')" class="submitDoc"></td>
				<% } %>
		        </tr>
			<tr>
				<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
			</tr>
		    </table>
		    </form>
		</td>
	</tr>

	<tr>
		<td bgcolor="95b1c1">&nbsp;</td>
		<td height="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</tr>
</table>

</body>
</html>
