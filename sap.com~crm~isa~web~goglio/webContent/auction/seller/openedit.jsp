<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*"%>
<%@ page import= "com.sap.isa.auction.*,com.sap.isa.auction.util.*"%>
<%@ page import="com.sap.isa.businessobject.*,com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.auction.table.ObjectPage" %>
<%@ page import="com.sap.isa.auction.table.actionforms.TableForm" %>
<%@ page import="com.sap.isa.auction.table.ObjectItemList" %>

<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<jsp:useBean id="listHandlerForm"
            scope="session" class="com.sap.isa.auction.actionforms.OpportunityListForm" />

<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">

function gotoPage()  {
    document.openForm.<%=TableForm.TABLEACTION%>.value = "<%=TableForm.TABLEACTION%>";
    document.openForm.<%=TableForm.GETPAGE%>.value = document.all("myPageNumber").selectedIndex + 1;
    document.openForm.submit();
}

function resetPageSize()  {
    document.openForm.<%=TableForm.TABLEACTION%>.value = "<%=TableForm.SETPAGESIZ%>";
    document.openForm.<%=TableForm.SETPAGESIZ%>.value = document.all("myPageSize").selectedIndex;
    document.openForm.submit();
}

function getPreviousPage()  {
    document.openForm.<%=TableForm.TABLEACTION%>.value = "<%=TableForm.TABLEACTION%>";
    document.openForm.<%=TableForm.GETPAGE%>.value = "<%=listHandlerForm.getList().getCurrentPageNumber() - 1%>";
    document.openForm.submit();
}

function getNextPage()  {
    document.openForm.<%=TableForm.TABLEACTION%>.value = "<%=TableForm.TABLEACTION%>";
    document.openForm.<%=TableForm.GETPAGE%>.value = "<%=listHandlerForm.getList().getCurrentPageNumber() + 1%>";
    document.openForm.submit();
}

function details(externalId,screen) {
	document.openForm.Id.value = externalId;
	document.openForm.show.value = screen;
        openForm.submit();
}

 function getList(action) {
          document.monitorForm.status.value = action;
          monitorForm.submit();
 }


</script>
</head>

<body bgcolor="95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">

<div class="module-name"><isa:moduleName name="auction/seller/open.jsp" /></div>
<%@ include file="webAuctionHeader.html" %>

<%@ include file="shopChangeLink.jsp" %>
<br>
<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/froff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>"  class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/catsearch.jsp"/>"><isa:translate key="eAuctions.seller.create.title"/></a>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/offon.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>"class="TABB" width="5%" nowrap>&nbsp;<isa:translate key="eAuctions.seller.monitor.title"/>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkon.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="e8e3d7" height="500">
	<TR valign="top">
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
	</TR>
	<tr valign="top">

		<td bgcolor="95b1c1">&nbsp;</td>
		<td>&nbsp;&nbsp;</td>
		<td>
			<form  name="monitorForm" action='<isa:webappsURL name="auction/seller/getopportunitylist.do"/>'>
			<input type="hidden" name="status" />

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('open')"><isa:translate key="eAuctions.seller.open.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('published')"><isa:translate key="eAuctions.seller.published.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('activeopportunity')"><isa:translate key="eAuctions.seller.active.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('closed')"><isa:translate key="eAuctions.seller.closed.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('finalized')"><isa:translate key="eAuctions.seller.finalized.title"/></a>
			<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="200" height="1" border="0" alt=""><a href="oppsearch.jsp">
			<img src="<isa:mimeURL name="auction/images/layout/button_search.gif"  language=""/>" width="15" height="15" border="0" alt="">&nbsp;&nbsp;<isa:translate key="eAuctions.seller.search.title"/></a>
			</form>
		</td>


        </tr>

        <tr>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
			<td colspan="2"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="5" border="0" alt=""></td>
	</tr>
	<tr valign="top">
		<td bgcolor="95b1c1">&nbsp;</td>

		<td>&nbsp;</td>
		<td class="CONTENT">
                <table cellpadding="2" cellspacing="1" border="0" width="98%">
                    <tr>
                            <td class="SEC_TB_TD" width="100%">&nbsp;
<%              if(listHandlerForm.getStatus() == IStatus.Open)  {  %>
                        <isa:translate key="eAuctions.seller.openauctions.title"/>
<%              }  else if(listHandlerForm.getStatus() == IStatus.Closed)  {  %>
                        <isa:translate key="eAuctions.seller.closedauctions.title"/>
<%              }  else if(listHandlerForm.getStatus() == IStatus.Active)  {  %>
                        <isa:translate key="eAuctions.seller.activeauctions.title"/>
<%              }  else if(listHandlerForm.getStatus() == IStatus.Published)  {  %>
                        <isa:translate key="eAuctions.seller.publishedauctions.title"/>
<%              }  else if(listHandlerForm.getStatus() == IStatus.Finalized)  {  %>
                        <isa:translate key="eAuctions.seller.finalizedauctions.title"/>
<%              } %>
                          </td>
                    </tr>
                </table>
          </td>
	</tr>


<%
                        ObjectPage myPage = listHandlerForm.getCurrentPage();
                        ObjectItemList itemList = listHandlerForm.getList();
                        if(!myPage.hasNext()) { %>

                <tr valign="top">
                    <td bgcolor="95b1c1">&nbsp;</td>

                    <td>&nbsp;</td>
                    <TD class="CONTENT"><isa:translate key="eAuctions.seller.monitor.message.noauction"/></TD>
                </tr>
<%
                        }  else  {
%>

		<tr valign="top">
			<Td bgcolor="95b1c1"></TD>
			<td>&nbsp;</td>
			<Td align="center">

	        	<form  name="openForm" action='<isa:webappsURL name="auction/seller/opplist.do"/>'>

			   <input type=hidden name="Id" />
			   <input type=hidden name="show" />
                           <input type=hidden name="<%=TableForm.GETPAGE%>" />
                           <input type=hidden name="<%=TableForm.TABLEACTION%>" />
                           <input type=hidden name="<%=TableForm.SETPAGESIZ%>" />

			<table cellpadding="5" cellspacing="0" border="1">
                           <tr>
<!-- Start of Na -->
                            <td class="CONTENTHEAD" colspan="6">
                            <table width="100%">
                                <tr >
                                        <td  class="CONTENTHEAD" width="100%" align="right">
                                        <!-- Start Page Navigation -->
                                        <IMG height=1 src="<isa:mimeURL name="auction/images/layout/sp.gif"/>" width="20">
<%
                                        if(itemList.getCurrentPageNumber() > 1)  {
%>
                                        <span style="cursor:hand;" onclick="getPreviousPage();">
<%
                                        }  else  {
%>
                                        <span>
<%
                                        }
%>
                                        <img src="<isa:mimeURL name="auction/images/layout/left.gif"/>" width="14" border="0" alt="Previous Page">&nbsp;
                                        <span class="TBLO_XXS">Total:</span> <%=itemList.getSize()%> Items <span class="TBLO_XXS">.&nbsp;Display&nbsp;
                                        <select id="myPageSize" size="1" class="DROPDOWN_XXS" onChange="resetPageSize();">
                                                <option value="5" <%=itemList.getPageSize() == 5 ? "SELECTED" : ""%>>5</option>
                                                <option value="10" <%=itemList.getPageSize() == 10 ? "SELECTED" : ""%>>10</option>
                                                <option value="15" <%=itemList.getPageSize() == 15 ? "SELECTED" : ""%>>15</option>
                                        </select>&nbsp;per page.&nbsp;This is &nbsp;
                                        <select id="myPageNumber" size="1" class="DROPDOWN_XXS" onChange="gotoPage();">
<%
                                              for(int i = 1; i <= itemList.getNumberOfPages(); i++)  {
                                                  if(itemList.getCurrentPageNumber() == i)  {
%>
                                                  <option value="1" SELECTED><%=i%></option>
<%
                                                  }  else  {
%>
                                                  <option value="2"><%=i%></option>
<%
                                                  }
                                              }
%>
                                        </select>&nbsp;of&nbsp;</span><%=itemList.getNumberOfPages()%><span class="TBLO_XXS">Page(s)</span>&nbsp;
<%
                                        if(itemList.getCurrentPageNumber() == itemList.getNumberOfPages())  {
%>
                                            <span>
<%
                                        }  else  {
%>
                                        <span style="cursor:hand;" onclick="getNextPage();">
<%
                                        }
%>
                                        <img src="<isa:mimeURL name="auction/images/layout/right.gif"/>" width="14" border="0" alt="Next Page"></span>&nbsp;&nbsp;&nbsp;
                                        <!-- End Page Navigation -->
                                        </td>
                                    </tr>
                                </table>
                            </td>

<!-- End of Na-->
                           </tr>

			   <Tr>
			   				<td align="center" class="CONTENTHEAD"><isa:translate key="eAuctions.seller.auctions.auctionname"/></td>
			   				<td align="center" class="CONTENTHEAD"><isa:translate key="eAuctions.seller.auctions.startdate"/></td>
			   				<td align="center" class="CONTENTHEAD"><isa:translate key="eAuctions.seller.auctions.enddate"/></td>
			   				<td align="center" class="CONTENTHEAD"><isa:translate key="eAuctions.seller.auctions.startprice"/></td>
			   				<td align="center" class="CONTENTHEAD"><isa:translate key="eAuctions.seller.auctions.reserveprice"/></td>
			   				<td align="center" class="CONTENTHEAD"><isa:translate key="eAuctions.seller.auctions.action"/></td>
			</TR>

			   <%

                           OpportunityBean opbean = null;
                           while(myPage.hasNext())  {

				    opbean=(OpportunityBean) myPage.next();
				    String starttemp = opbean.getStartDate().toString();
				    String startdate = starttemp.substring(0,4) + " / " + starttemp.substring(5,7) + " / " + starttemp.substring(8,10);

				    String endtemp = opbean.getEndDate().toString();
				    String enddate = endtemp.substring(0,4) + " / " + endtemp.substring(5,7) + " / " + endtemp.substring(8,10);
				    String startprice = opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.START_PRICE)==null ?
					   "" : ((opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.START_PRICE))).toString();
				    String reserveprice = opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.RESERVE_PRICE)==null ?
					   "" : ((opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.RESERVE_PRICE))).toString();


				%>
					<tr>
					<!--	<td><A href="<isa:webappsURL name="auction/seller/audetails.jsp"/>">
						   <%=opbean.getOpportunityName()%></a></td> -->

						   <td class="CONTENTR"><A href="javascript:onClick=details(<%=opbean.getInternalOpportunityId()%>,'auctiondetails')">
						   <%=opbean.getOpportunityName()%></a></td>

						<!--<td><%=opbean.getOpportunityName()%> </td> -->
						<Td class="CONTENTR"><%=startdate%></TD>
						<Td class="CONTENTR"><%=enddate%></TD>
						<td class="CONTENTR"><%=currency%> <%=startprice%></td>
						<Td class="CONTENTR"><%=currency%> <%=reserveprice%></TD>
					<% if(opbean.getStatus() == IStatus.Open || opbean.getStatus() == IStatus.Published)  { %>
						<td class="CONTENTR" align="center"><A href="javascript:onClick=details(<%=opbean.getInternalOpportunityId()%>,'openedit')"><img src="<isa:mimeURL name="auction/images/layout/edit.gif"  language=""/>" width="18" height="19" border="0" alt="" title="<isa:translate key="eAuctions.seller.tooltip.edit"/>"</A>&nbsp;&nbsp;
								<A href="javascript:onClick=details(<%=opbean.getInternalOpportunityId()%>,'openclose')"><img src="<isa:mimeURL name="auction/images/layout/icon_terminateAuc.gif"  language=""/>" width="19" height="18" border="0" alt="" title="<isa:translate key="eAuctions.seller.tooltip.close"/>"></A>&nbsp;&nbsp;&nbsp;
								<a href="javascript:onClick=details(<%=opbean.getInternalOpportunityId()%>,'openpublish')"><img src="<isa:mimeURL name="auction/images/layout/icon_publishedAuc.gif"  language=""/>" width="16" height="16" border="0" alt="" title="<isa:translate key="eAuctions.seller.tooltip.publish"/>"></a>&nbsp;&nbsp;</td>
					<%  }  else if (opbean.getStatus() == IStatus.Active || opbean.getStatus() == IStatus.Finalized)  { %>
						<td align="center"><A href="javascript:onClick=details(<%=opbean.getInternalOpportunityId()%>,'opportunitybidhistory')">
							<isa:translate key="eAuctions.seller.view.title"/> &nbsp;<isa:translate key="eAuctions.seller.auctions.bidhistory"/></A></td>
					<%  }  else if(opbean.getStatus() == IStatus.Closed)  { %>
						<td>
						</td>
					<%  }  %>

					</tr>
			<%  } %>


		</table>
		</form>

	</td>
	</tr>
<%
                        }
%>
	<tr>
		<td bgcolor="95b1c1">&nbsp;</td>
		<td>&nbsp;</td>
		<td height="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</tr>
 </table>


</body>
</html>

