<%@ page language="java" %>
<%@ page import= "java.util.*,com.sap.isa.auction.bean.TargetBean,com.sap.isa.core.TechKey,com.sap.isa.auction.util.TreeMapModel"%>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">
function setHiddenFields(hiddenValue)
{
    //	alert(hiddenValue);
	targetGroup.ClickAction.value=hiddenValue;
}	
	

</script>

</head>

<body bgcolor="95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/targetGroup.jsp"/></div>
<form action='<isa:webappsURL name="auction/seller/createAuction.do"/>' name="targetGroup"  method="POST">

 <input type="hidden" name="ClickAction" />
<%@ include file="webAuctionHeader.html" %> 
<br>

<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/fron.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;
		<isa:translate key="eAuctions.seller.create.title"/>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/onoff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>"  class="TAB" width="5%" nowrap>&nbsp;
		<a href="<isa:webappsURL name="auction/seller/monitorOpportunity.do"/>"><isa:translate key="eAuctions.seller.monitor.title"/></a>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkoff.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="e8e3d7" height="500">
	<TR>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
	</TR>
	<tr>
		<td bgcolor="95b1c1">&nbsp;</td>
		<td class="CONTENT">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="30" height="11" border="0" alt="">&nbsp;&nbsp;TARGET GROUP&nbsp;&nbsp;<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="580" height="11" border="0" alt=""></td>
	</tr>
	<tr>
		<td bgcolor="95b1c1"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">


<%		// HttpSession s = request.getSession();
		TreeSet selIds= (TreeSet)session.getAttribute("selected");
		TreeMapModel mdl = (TreeMapModel) session.getAttribute("TGmodel");
		boolean checked= false;
		Iterator iter2=null;

		//Hashtable tgroups= (Hashtable)session.getAttribute("TGroup");
		TreeMap tgroups=(TreeMap)session.getAttribute("view");

		if (tgroups != null && tgroups.size()>0) {
            	    Set set= tgroups.keySet();
            	    Iterator iter= set.iterator();
                    while(iter.hasNext()){
                            String key= (String) iter.next();

                        String value= (String)tgroups.get(key);
                        String chboxname="chbox";
                        chboxname="chbox"+value;
                        checked= false;
                        if(selIds !=null){
		               Object[] ids=selIds.toArray();
		               
		               //out.println(ids.length);
		               for(int i=0;  i<ids.length;i++){
		                  String tk =(String) ids[i];
		                   //out.println(tk);
                       		  if(tk.equals(value)){
                        	  checked=true;
                        	 }
                                }
                         }





                        if (checked) { %>
                            <tr>
                            <Td class="CONTENT" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="<%=chboxname%>" class="submitDoc" value="<%=value %>" CHECKED>&nbsp;<%=key%></TD>
                            </TR>

                            <% } if (!checked){ %>
                            <Tr>
                            <Td class="CONTENT" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="<%=chboxname%>" class="submitDoc" value="<%=value%>" >&nbsp;<%=key %></TD>
                            </TR>

                    <%} %>

	<% } } %>
	</table>
	        </td>
	</tr>
	<tr> <td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="20" border="0" alt=""></td><td></td> </tr>
	
	<tr> <td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="20" border="0" alt=""></td><td>
	
	<table>
	<TR>
			<td bgcolor="e8e3d7"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
			<td> 
			<% if(mdl.isFirstPage()) { %>
			<input type="submit" name="submit" class="submitDoc" value="|<" onclick='setHiddenFields("FIRST_PAGE")'>
			<% }  
			if(mdl.isPrevPageThere()) { %>
			<input type="submit" name="submit" class="submitDoc" value="<" onclick='setHiddenFields("PREVIOUS_PAGE")'>
			<% } %>
			<% if(mdl.isNextPageThere()) { %>
			<input type="submit" name="submit" class="submitDoc" value=">" onclick='setHiddenFields("NEXT_PAGE")'>
			<% } 
			 if(mdl.isLastPage()) { %>
			<input type="submit" name="submit" class="submitDoc" value=">|"onclick='setHiddenFields("LAST_PAGE")'>
			<% } %>
			<input type="submit" name="submit" class="submitDoc" value="<isa:translate key="eAuctions.seller.button.continue"/>" onclick='setHiddenFields("SUBMIT")'></td>
	</TR>
	</table>

	
        


		<tr>
		<td bgcolor="95b1c1">&nbsp;</td>
		<td height="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</tr>
</table>


</form>
</body>

</html>
