<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.*,com.sap.isa.auction.bean.*, com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page language="java" %>
<%@ page import="com.sap.isa.businessobject.*,com.sap.isa.core.*" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />


 <html>
 <%
         Calendar cal = Calendar.getInstance();
         cal.setTime(new Date());
         Date gmtTime = new Date(cal.getTime().getTimezoneOffset()*60*1000+cal.getTime().getTime());
 %>

<head>
<TITLE><isa:translate key="eAuctions.title"/></TITLE>
<style>
	.calendarArea {
		border: 2px outset white;
	}
</style>
<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script src="<isa:mimeURL name="auction/jscript/ourCalendarSeller.js"  language=""/>"
          type="text/javascript"></script>

<script language="JavaScript">
<!--

var currentdate = new  Date("<%=gmtTime.toString()%>");

function showOurCalendar(element)  {
	if(element.value == '')
		element.value = window.currentdate;
	setDateFieldFormatA(element,'false');
	element.value = '';
	var ourCal = document.all("calendarArea");
	//ourCal.innerHTML = calDocTop + calDocBottom;
	document.all("topSection").innerHTML = calDocTop;
	document.all("middleSection").innerHTML = calDocBottom;
	ourCal.style.pixelLeft = event.clientX;
	ourCal.style.pixelTop = event.clientY - 30;
	ourCal.style.display = "block";
	makeDateString(calDay);
}

function MM_goToURL() { //v3.0
  		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">

function clearFields()  {
	searchForm.startDate.value = "";
	searchForm.oppName.value = "";
	searchForm.oppId.value = "";
	searchForm.prodId.value = "";
	return false;
}

function details(externalId,screen) {
	document.openForm.Id.value = externalId;
	document.openForm.show.value = screen;
        openForm.submit();
}

 function getList(action) {
          document.monitorForm.status.value = action;
          monitorForm.submit();
 }


</script>

</head>

<body class="backgroundColor95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/oppsearch.jsp" /></div>

<%@ include file="webAuctionHeader.html" %> 

<%@ include file="shopChangeLink.jsp" %> 




<br>

<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/froff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>" class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/catsearch.jsp"/>">
		 <isa:translate  key="eAuctions.seller.create.title"/></a>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/offon.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;<isa:translate key="eAuctions.seller.monitor.title"/>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkon.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="780" class="backgroundColore8e3d7" height="500">
	<TR>
		<td class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
	</TR>
	<tr>
		<td class="backgroundColor95b1c1">&nbsp;</td>
		<td>&nbsp;&nbsp;</td>
		<td>
			<form  name="monitorForm" action='<isa:webappsURL name="auction/seller/getopportunitylist.do"/>'>
			<input type="hidden" name="status" />

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('open')"><isa:translate key="eAuctions.seller.open.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('published')"><isa:translate key="eAuctions.seller.published.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('activeopportunity')"><isa:translate key="eAuctions.seller.active.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('closed')"><isa:translate key="eAuctions.seller.closed.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('finalized')"><isa:translate key="eAuctions.seller.finalized.title"/></a>
			<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="200" height="1" border="0" alt=""><a href="oppsearch.jsp">
			<img src="<isa:mimeURL name="auction/images/layout/button_search.gif"  language=""/>" width="15" height="15" border="0" alt="">&nbsp;&nbsp;<isa:translate key="eAuctions.seller.search.title"/></a>

			</form>
		</td>	</tr>
	<tr>
	<td class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<td colspan="2"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="5" border="0" alt=""></td>
	</tr>
	<tr>
		<td class="backgroundColor95b1c1">&nbsp;</td>

		<td>&nbsp;</td>
		<td class="CONTENT">
                <table cellpadding="2" cellspacing="1" border="0" width="98%">
                    <tr>
                            <td class="SEC_TB_TD" width="100%">&nbsp;
                            <isa:translate key="eAuctions.seller.search.title"/>
                            </td>
                    </tr>
                </table>
		</td>
	</tr>

	<tr align="left">
		<Td class="backgroundColor95b1c1"></TD>

		<td>&nbsp;</td>
		<Td>
		<table cellpadding="1" cellspacing="0" border="0">
		<tr>
			<Td class="3">&nbsp;</TD>
		</tr>

             <form action='<isa:webappsURL name="auction/seller/search.do"/>' method="GET" name="searchForm" >

		<Tr>
			<Td align="right" class="CONTENT">Status</TD>
			<td>&nbsp;&nbsp;</td>
			<Td><select name="status" property="oppStatus">
				<option value=""></option>
				<option value="OPEN"><isa:translate key="eAuctions.seller.open.title"/></option>
				<option value="PUBLISHED"><isa:translate key="eAuctions.seller.published.title"/></option>
				<option value="ACTIVE"><isa:translate key="eAuctions.seller.active.title"/></option>
				<option value="CLOSED"><isa:translate key="eAuctions.seller.closed.title"/></option>
				<option value="FINALIZED"><isa:translate key="eAuctions.seller.finalized.title"/></option>
			  </select></TD>
		</TR>
		<Tr>
			<Td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.auctionname"/></TD>
			<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></td>
			<td><input type="text" class="submitDoc" name="oppName" size="15"></td>
		</TR>
		<Tr>
			<Td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.auctionid"/></TD>
			<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></td>
			<td><input type="text" class="submitDoc" name="oppId" size="15"></td>
		</TR>
		<Tr>
			<Td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.product.id"/></TD>
			<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></td>
			<td><input type="text" class="submitDoc" name="prodId" size="15"></td>
		</TR>
		<Tr>
			<Td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.startdate"/></TD>
			<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></td>
			<td><input type="text" name="startDate" class="submitDoc" size="10" onFocus="javascript:blur()">
			<A HREF="javascript:doNothing()"
		                onClick="javascript:showOurCalendar(searchForm.startDate);">
		                <img src="<isa:mimeURL name="auction/images/layout/calendar.gif"  language=""/>" alt="Calendar" title="Calendar" border="0"'>
                 </A><font size=1><isa:translate key="eAuctions.seller.auctions.calendar"/></font>
			</td>
		</TR>
		<Tr>
			<Td>&nbsp;</TD>
		</TR>
		<tr>
			<td align="right"><input type="submit" name="search" class="submitDoc" value='<isa:translate key="eAuctions.seller.search.title"/>' </td>
			<td>&nbsp;</td>
			<td><input type="submit" name="REFRESH" class="submitDoc" value='<isa:translate key="eAuctions.seller.button.refresh"/>' onClick="MM_goToURL('parent','oppsearch.jsp');return document.MM_returnValue" >&nbsp;
			<input type="button" name="clear" class="submitDoc" onclick="clearFields();" value='<isa:translate key="eAuctions.seller.search.clear"/>'</input></td>
		</tr>
            </form>
		</table>
		</TD>
	</tr>
	<tr>
		<td class="backgroundColor95b1c1">&nbsp;</td>
		<td>&nbsp;</td>
		<td height="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</tr>
</table>


</form>
<div id="calendarArea" class="calendarArea" style="background-color:#e8e3d7;display:none;position:absolute;width:200;height:60;left:200;top;300;">
	<br>
	<table width="100%">
	<tr>
	  <td>
		<div id="topSection">
		</div>
	  </td>
	</tr>
	<tr>
	  <td>
		<div id="middleSection">
		</div>
	  </td>
	</tr>
	</table>
</div>
</body>

</html>
