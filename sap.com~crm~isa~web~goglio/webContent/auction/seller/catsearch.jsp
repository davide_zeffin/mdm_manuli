<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.*,com.sap.isa.auction.bean.*, com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page language="java" %>
<%@ page import="com.sap.isa.auction.actionforms.OpportunityForm" %>
<%@ page import ="com.sap.isa.core.UserSessionData"%>
<%@ taglib uri="/isa"  prefix="isa" %>

<%
    OpportunityForm createOpptyForm = new com.sap.isa.auction.actionforms.CreateOpportunityForm();
    pageContext.getSession().setAttribute(com.sap.isa.auction.actions.ActionConstants.OPPORTUNITYFORM , createOpptyForm);
%>
<isa:contentType />
<html>

<HEAD><title><isa:translate key="eAuctions.title"/></title>


<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script src="<isa:mimeURL name="auction/jscript/calendar.js"  language=""/>"
          type="text/javascript"></script>

<SCRIPT>
function MM_goToURL() { //v3.0
  		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}


function openProduct()
	{

		classPrd = window.open("<isa:webappsURL name="auction/seller/catalog/getCatalog.do"/>", "Start", "status,width=700,height=600,scrollbars");
		which = "start";
}

function fillProduct(a,b,c,d,e) {
     		classPrd.close();
		document.createOpptyForm.productName.value=c;
		document.createOpptyForm.productCatalog.value=d;
		document.createOpptyForm.productId.value=b;
		document.createOpptyForm.productDesc.value=a;
		document.createOpptyForm.productUOM.value=e;

}

function addToOpportunity(a) {
     		classPrd.close();
		document.location.href='<isa:webappsURL name="auction/seller/addtooppty.do?itemkey="/>'+a;

}

function openTarget()
	{
	     classTG = window.open("<isa:webappsURL name="auction/seller/targetgroup.do?nextscreen=define"/>", "targetGroup", "status,scrollbars,width=700,height=400");
		which = "targetGroup";
	}
function fillTargetInfo(targetgroups)
	{
		classTG.close();
		document.createOpptyForm.targetGroup.value=targetgroups;

	}
</SCRIPT>
</head>

<body class="backgroundColor95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/catsearch.jsp"/></div>
<form action='<isa:webappsURL name="/auction/seller/startCreateAuction.do"/>' name="createOpptyForm" method="POST">

<%@ include file="webAuctionHeader.html" %>

<%@ include file="shopChangeLink.jsp" %>
<br>

<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/fron.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;<isa:translate  key="eAuctions.seller.create.title"/>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/onoff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>"  class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/monitorOpportunity.do"/>">
			<isa:translate key="eAuctions.seller.monitor.title"/></a>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkoff.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="780" class="backgroundColore8e3d7" height="500">
	<tr>
          <tD class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
          <Td colspan="7" valign="top">
		<table cellpadding="0" width="100%" cellspacing="0" border="0">
		<tr>
		<td colspan="3" class="CONTENT" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <table cellpadding="2" cellspacing="1" border="0" width="98%">
                    <tr>
                            <td class="SEC_TB_TD" width="100%">&nbsp;
		                <isa:translate key="eAuctions.seller.createauction.title"/>
                            </td>
                    </tr>
                </table>
                </td>
		</tr>
		<Tr>
		<Td>


		<table cellpadding="0" cellspacing="0" border="0" class="CONTENT">
                    <tr>
                            <Td rowspan="21"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></TD>
                    </tr>
                    <tr>
                            <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                            <td><isa:translate key="eAuctions.seller.auction.name" /><td>
                            <td><IMG alt="" border=0 height=1 src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>"  width=10></TD>
                            <td><input type="text" name="opptyName" class="submitDoc" value="<jsp:getProperty name="createOpptyForm"  property="opportunityName"/>" size="15"/></td>
                            <td><IMG alt="" border=0 height=1 src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>"  width=10></TD>
                            <td><input type="submit" name="go" value="<isa:translate key="eAuctions.seller.create.title"/>"  class="submitDoc"></TD>
                    </tr>
                    <Tr>
                    <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
                    </TR>
		</table>
		</td>
		</tr>
		</table>
		</td>
	</tr>
</table>

</form>
</body>

</html>
