<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<html>

	<head>
		<title><isa:translate key="eAuctions.seller.productcatalog.title"/></title>
	</head>
    <!-- border="0" frameBorder="0" frameSpacing="0" is non standard and should not be used. -->
    <frameset rows="72,*,10" border="0">
	<frame id="header" name="headerframe" src="<isa:webappsURL name="/auction/seller/createAuctionHeader.jsp"/>" frameborder="0" NOSCROLL></frame>
		<frameset cols="20%,*" >
			<frame id="categoriesFrame" name="categoriesFrame" src="<isa:webappsURL name="/auction/seller/catalog/categories.do"/>" frameborder="0" scrolling="no"></frame>
			<frame id="productsFrame" name="productsFrame" src="<isa:webappsURL name="/auction/seller/catalog/blank_window.jsp"/>" frameborder="0"></frame>
		</frameset>
	<frame id="footer" name="footerframe" src="<isa:webappsURL name="/auction/seller/catalog/footer.jsp"/>" frameborder="0" scrolling="no"  ></frame>	
	
    </frameset>
</html>


