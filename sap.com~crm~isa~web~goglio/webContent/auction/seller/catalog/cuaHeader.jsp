
<% com.sap.isa.catalog.webcatalog.WebCatItem currentItem = (com.sap.isa.catalog.webcatalog.WebCatItem) request.getAttribute("currentItem");
   boolean isAlone;
   if (currentItem != null) {
   isAlone = (currentItem.getContractItems() == null || currentItem.getContractItems().size() == 0);
   } else {
   isAlone = true;
   }
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
        <td colspan="3" align="left">
           <table border="0">
             <tbody>


<% if ( request.getParameter("cuaproducttype") != null && request.getParameter("cuaproducttype").equals("A") ) {%>
                  <tr>
                  <%if (!isAlone) {%>
                    <td class="DetailsTab"><a href="<isa:webappsURL name="/catalog/contractDetails.do"/>"><isa:translate key="catalog.isa.contractDetails"/></a></td>
                    <% } %>
                    <td class="actualDetailsTab"><isa:translate key="catalog.isa.accesories"/></td>
                    <td class="DetailsTab"><a href="<isa:webappsURL name="/catalog/crossSelling.do?inFrame=yes&cuaproducttype=C"/>"><isa:translate key="catalog.isa.crossSelling"/></a></td>
                  </tr>
<% } %>
<% if ( request.getParameter("cuaproducttype") != null && request.getParameter("cuaproducttype").equals("C") ) {%>
                  <tr>
                  <%if (!isAlone) {%>
                    <td class="DetailsTab"><a href="<isa:webappsURL name="/catalog/contractDetails.do"/>"><isa:translate key="catalog.isa.contractDetails"/></a></td>
                    <% } %>
                    <td class="DetailsTab"><a href="<isa:webappsURL name="/catalog/accesories.do?inFrame=yes&cuaproducttype=A"/>"><isa:translate key="catalog.isa.accesories"/></a></td>
                    <td class="actualDetailsTab"><isa:translate key="catalog.isa.crossSelling"/></td>
                  </tr>
<% } %>

             </tbody>
            </table>
           </td>
           <td colspan="3" align="left"><div class="opener"><isa:translate key="catalog.isa.detailsOn"/> <var><%= currentItem.getAttribute("OBJECT_DESCRIPTION") %></var></div></td>
        </tr>
        <tr class="actualDetails">
        <td colspan="6">


<!--        &nbsp;</td>
          </tr>
        </tbody>
      </table> -->
