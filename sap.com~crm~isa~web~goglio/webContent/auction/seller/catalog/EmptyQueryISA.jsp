<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<head>
     <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">
</head>

<body>
<div class="module-name"><isa:moduleName name="auction/seller/catalog/EmptyQueryISA.jsp"/></div>

<% com.sap.isa.catalog.webcatalog.WebCatArea currentArea = (com.sap.isa.catalog.webcatalog.WebCatArea) request.getAttribute("currentArea");
   if (currentArea == null || currentArea.getAreaID().equals("$ROOT")) {
%>
  <a href="<isa:webappsURL name="/auction/seller/catalog/categorieInPath.do?key=0&type=Full"/>" ><isa:translate key="catalog.isa.backTo"/> <isa:translate key="catalog.isa.products"/></a>
<% } else {%>
  <% String tmpPath="/auction/seller/catalog/categorieInPath.do?key="+currentArea.getPathAsString()+"&type=Full";%>
  <a href="<isa:webappsURL name="<%= tmpPath %>"/>" ><isa:translate key="catalog.isa.backTo"/> <%= currentArea.getAreaName() %></a>
<% } %>
<br>
This query returned no data !!
</body>
