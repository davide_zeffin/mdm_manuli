<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<html>
  <head>
    <title>SAP Internet Sales - Closer Details</title>
     <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">
  </head>
  <script type="text/javascript">
    var closer_enable = 0;
    <!--
    if(navigator.appName == 'Microsoft Internet Explorer')  {
      closer_enable = 1;
    }
    //-->
  </script>
  <body class="closer">
  <div class="module-name"><isa:moduleName name="auction/seller/catalog/DetailsMiddle.jsp" /></div>
  
  <script type="text/javascript">
    <!--
    if (closer_enable == 1)  {
      document.writeln("<div class=\"vertCloser\" align=\"center\"><a href=\"#\" onclick=\"parent.resize('min'); return false;\"> &darr; </a>&nbsp;&nbsp;<a href=\"#\" onclick=\"parent.resize('max'); return false;\"> &uarr; </a></div>");
    }
    //-->
  </script>
  </body>
</html>