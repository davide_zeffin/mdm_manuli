<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatWeightedArea" %>
<isa:contentType />
<jsp:useBean    id="categoriesList"
                type="java.util.ArrayList"
                scope="request" >
</jsp:useBean>
<jsp:useBean    id="currentArea"
                type="com.sap.isa.catalog.webcatalog.WebCatArea"
                scope="request" >
</jsp:useBean>
<head>
     <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">

<script language="JavaScript1.2">
var shown=new Array();
var path=new Array();
var shownTmp=new Array();

function shows(myPath) {
showActivePath(myPath);
}


function pathLength(thePath) {
  leng=1;
  for (j=0;j<thePath.length;j++)
    if (thePath.charAt(j) == "/".charAt(0)) leng++;
  return leng;
}

function seeProducts(pos,thePath) {
  for (i=0;i<path.length;i++) {
      st="d"+i;
      document.all(st).innerHTML=document.all(st).innerText;
  }
st="d"+pos;
document.all(st).innerHTML='<B><I>'+document.all(st).innerText+'</I></B>';
}


function showActivePath(currentPath) {
  for (i=0;i<path.length;i++) shownTmp[i]=shown[i];
    for (i=0;i<path.length;i++) {

      if (pathLength(path[i]) < pathLength(currentPath)) //actual path may be a sub-path of the currently selected path
        if (currentPath.indexOf(path[i]) == 0) shownTmp[i]=1;

      if (pathLength(path[i]) == pathLength(currentPath)) { //actual path may be a brother of the currently selected path
        st=path[i];
        st=st.substring(0,st.lastIndexOf("/"));
        if (currentPath.indexOf(st) == 0) shownTmp[i]=1;
      } // if the lengths are equal


      if (pathLength(path[i]) == pathLength(currentPath)+1) { //actual path may be a child of the currently selected path
        st=path[i];
        st=st.substring(0,st.lastIndexOf("/"));
        if (currentPath == st) shownTmp[i]=1-shownTmp[i];
      } // end for length is one greater

      if (pathLength(path[i]) > pathLength(currentPath)+1) { //actual path may be a nephew, aso ... of the currently selected path
        st=path[i];
//        st=st.substring(0,st.lastIndexOf("/"));
        if (st.indexOf(currentPath) == 0 && shownTmp[i] == 1) shownTmp[i]=1-shownTmp[i];
      } // end for length more than one greater

    } // end for
  st=" ";
  for (i=0;i<path.length;i++)
    st+=(shownTmp[i]+" ");
//  alert(st);

    for (i=0;i<path.length;i++) {
      st="p"+i.toString();
      if (shownTmp[i] - shown[i] != 0) {
        if (shownTmp[i] == 0) TblMain.rows(st).style.display="none";
            else TblMain.rows(st).style.display="block";
        shown[i]=shownTmp[i];
      }
    }
}

function activatePaths() {
myPath="<%= currentArea.getPathAsString() %>";
//alert(myPath);
//TblMain.rows("p0").style.display="block";
//shown[0]=1;
showActivePath(myPath);
}
</script>
</head>

<body class="organizer" onLoad="activatePaths();">

<div id="new-doc" class="module">
<div class="module-name"><isa:moduleName name="auction/seller/CategoriesFullISA.jsp" /></div>
<font size="-2">
<table border="0" cellspacing="0" cellpadding="0" id="TblMain">
<tr valign="middle">
<td valign="middle">
<br><br>
</td>
</tr>
<tr valign="middle">
<td valign="middle">
&nbsp;&nbsp;<a href="<isa:webappsURL name="b2b/showInCatalogFrame.do"/>?type=recommendation " target="form_input">
<isa:translate key="catalog.isa.personalPreferences"/></a>
</td>
</tr>

<tr valign="middle">
<td valign="middle">
&nbsp;&nbsp;<a href="<isa:webappsURL name="b2b/showInCatalogFrame.do"/>?type=bestseller" target="form_input">
<isa:translate key="catalog.isa.bestseller"/></a>
</td>
</tr>
<tr valign="middle">
<td valign="middle">
<br><br>
      <form action="<isa:webappsURL name="/catalog/query.do"/>" target="form_input">
        <table>
          <tr>
            <td><isa:translate key="catalog.isa.quickSearch"/></td>
            <td><input type="text" name="query" size="22"></td>
          <tr>
            <td></td>
            <td nowrap><input type="Submit" value="<isa:translate key="catalog.isa.search"/>"> <a href="<isa:webappsURL name="/catalog/prepareBigSearch.do"/>"><isa:translate key="catalog.isa.toProfile"/></a></td>
          </tr>
          </tr>
        </table>
      </form>
<br><br>
</td>
</tr>
<% int signNo=-1;
   int i,indentSize;
   i=0;
   for (int pos=0;pos<categoriesList.size();pos++) {
%>
<tr valign="bottom" style="display:none" id="p<%= pos %>">
<td valign="bottom">
<%
   WebCatWeightedArea area = (WebCatWeightedArea)categoriesList.get(pos);
   i++;
   indentSize=14*area.getPosition();
   String tmpPath;
   %>
  <img src="mimes/isa/graphics/blank.gif" border=0 height=14 width=<%= indentSize %>>
<% if (area.getSign() == area.PLUS){
    signNo++;
%>
  <a href="javascript:shows('<%= area.getPath() %>')"><img src="mimes/isa/graphics/plus.gif" border=0 height=14 width=14></a>
<% } else {%>
  <img src="mimes/isa/graphics/blank.gif" border=0 height=14 width=14>
<% } %>
  <% tmpPath="/catalog/categorieInPath.do?key="+area.getPath()+"&type=Full";%>

 <a href="javascript:seeProducts(<%=pos %>,'<%= area.getPath() %>')">
<SPAN id="d<%= pos %>">
<%= area.getArea().getAreaName()%>
</SPAN>
  </a><br>

</td>
</tr>
<% } %>
</table>
<script language="JavaScript1.2">
<%
    java.util.Iterator categoriesTree = categoriesList.iterator();
    i=-1;
    while (categoriesTree.hasNext()) {
      com.sap.isa.catalog.webcatalog.WebCatWeightedArea theArea = (com.sap.isa.catalog.webcatalog.WebCatWeightedArea) categoriesTree.next();
      i++;
%>path[<%= i %>]="<%= theArea.getPath() %>";
<% } %>
for (i=0;i<path.length;i++)
  shown[i]=0;
</script>
</body>
