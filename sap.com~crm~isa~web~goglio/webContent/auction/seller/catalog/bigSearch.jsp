<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />

<jsp:useBean    id="catAttrIterator"
                type="java.util.Iterator"
                scope="request" >
</jsp:useBean>

<%@ include file="attributeNameParser.jsp" %>

<html>
  <head>
     <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">

  </head>
  <body class="organizer">
  <div class="module-name"><isa:moduleName name="auction/seller/catalog/bigSearch.jsp" /></div>
  
  <br><br>
  <table align=center width="70%">
    <form action="<isa:webappsURL name="/catalog/bigSearch.do"/>" method="POST" target="form_input">
    <tr>
      <td><isa:translate key="catalog.isa.productID"/>: </td>
      <td><input type="text" name="OBJECT_ID" size="20"></td>
    </tr>
    <tr>
      <td><isa:translate key="catalog.isa.description"/>: </td>
      <td><input type="text" name="OBJECT_DESCRIPTION" size="20"></td>
    </tr>
    <% while (catAttrIterator.hasNext()) {
        com.sap.isa.core.eai.boi.catalog.IAttribute attrib = (com.sap.isa.core.eai.boi.catalog.IAttribute) catAttrIterator.next();
        String st = parseAttributeName(pageContext, "catalog.isa",attrib.getGuid());
        if (st != null) {
    %>
    <tr>
      <td><%= st %></td>
      <td><input type="text" name="<%= attrib.getGuid() %>" size="20"></td>
    </tr>
    <% } %>
    <% } %>
    <tr>
      <td align="right"><isa:translate key="catalog.isa.search"/>:</td><td><input type="radio" name="algorithm" value="and">And</td>
    </tr>
    <tr>
      <td></td><td><input type="radio" name="algorithm" value="or" checked>Or</td>
    </tr>
    <tr>
      <td colspan=2 align=right><br><input type="submit" value="<isa:translate key="catalog.isa.search"/>"></td>
    </tr>
  </form>
  <tr>
  <td colspan=2 align="right"><br><isa:translate key="catalog.isa.backTo"/>&nbsp;
  <a href="<isa:webappsURL name="/catalog/categories.do"/>"><isa:translate key="catalog.isa.categoriespage"/></a></td>
  </tr>
  </table>

  </body>
</html>
