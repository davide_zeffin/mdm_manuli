<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatAreaAttribute" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatArea" %>

<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.auction.actions.ActionConstants" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ page import="com.sap.isa.core.SessionConst"%>
<%@ page import="com.sap.isa.catalog.webcatalog.*" %>
<%@ page import="java.util.HashMap" %>
<isa:contentType />
<jsp:useBean    id="itemPage"
                scope="request"
                type="com.sap.isa.catalog.webcatalog.WebCatItemPage">
</jsp:useBean>
<jsp:useBean    id="refNames"
                type="com.sap.isa.businessobject.contract.ContractAttributeList"
                scope="request" >
</jsp:useBean>
<jsp:useBean    id="contractDisplayMode"
                type="com.sap.isa.catalog.webcatalog.ContractDisplayMode"
                scope="request" >
</jsp:useBean>
<jsp:useBean id="createOpptyForm"
                scope="session"
                type="com.sap.isa.auction.actionforms.OpportunityForm"/>
<HTML>
<head>

<!-- Stylesheet -->
     <link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
     <link href="<isa:mimeURL name="auction/css/sellerPC.css"  language=""/>"
          type="text/css" rel="stylesheet">

<%
int spanSize;
java.util.Iterator refNamesIterator;
int i;
if (!contractDisplayMode.showContracts()) spanSize=0;
    else spanSize = refNames.size();
%>

<script language="JavaScript1.2">

<%
    HashMap itemslist = createOpptyForm.getLineItems();
    if(itemslist != null)  { %>
       var noOfSelectedProducts = <%=itemslist.size()%>;
<%
    }  else  {
%>
       var noOfSelectedProducts = 0;
<%  }  %>

  function checkEmptySelection()  {
      var itemsChecked = false;
      var selections = document.all("<%=com.sap.isa.catalog.actions.ActionConstants.RA_ITEMKEY + "checkbox"%>");
      if(selections != null)  {
          if(selections.length == null)  {
              if(selections.checked)
                  itemsChecked = true;
          }  else {
              var i = 0;
              for(i = 0 ; i < selections.length ; i++)  {
                  if(selections[i].checked)  {
                      itemsChecked = true;
                      break;
                  }
              }
          }
      }
      if(itemsChecked || noOfSelectedProducts > 0)
          return false;
      return true;
  }

   function setPage(Zahl)
    {
      document.productform.page.value = Zahl;
<%if (request.getAttribute("isQuery") == null || !request.getAttribute("isQuery").equals("yes")) { %>
      document.productform.next.value = "products";
<% } else { %>
      document.productform.next.value = "setPageSize";
<% } %>
      document.productform.submit();
    }
   function setPageSize(Zahl)
    {
      document.productform.itemPageSize.value = Zahl;
      document.productform.next.value = "setPageSize";
      document.productform.submit();
    }
   function setNext(Next)
    {
      document.productform.next.value = Next;
      document.productform.submit();
    }
   function addSingleItem(Itemkey,contractkey,contractitemkey)
    {
      document.productform.next.value = 'addToBasket';
      document.productform.itemkey.value = Itemkey;
      document.productform.contractkey.value=contractkey;
      document.productform.contractitemkey.value=contractitemkey;
      document.productform.submit();
    }

   function goToAuctionDetails()
    {
      if(checkEmptySelection() == false)  {
          document.productform.<%=com.sap.isa.auction.actions.ActionConstants.GOTOAUCTION%>.value = "<%=com.sap.isa.auction.actions.ActionConstants.GOTOAUCTIONVALUE%>";;
          document.productform.target = "_top";
          document.productform.submit();
      }  else  {
          document.productform.<%=com.sap.isa.auction.actions.ActionConstants.GOTOAUCTION%>.value = "NoItems";
          alert("<isa:translate key="eAuction.seller.noselection"/>");
          event.cancelBubble = true;
          return false;
      }
    }

    function clearSelectionItems()  {
      document.productform.<%=com.sap.isa.auction.actions.ActionConstants.GOTOAUCTION%>.value = "<%=com.sap.isa.auction.actions.ActionConstants.CLEARSELECTIONVALUE%>";
      document.productform.target = "_top";
      document.productform.submit();
    }

    function getMoreFunctions(Itemkey,contractkey,contractitemkey,Next) {
      document.productform.itemkey.value = Itemkey;
      document.productform.contractkey.value=contractkey;
      document.productform.contractitemkey.value=contractitemkey;
      document.productform.next.value = Next;
      document.productform.submit();
    }

    function seeSingleItem(Itemkey)
    {
      document.productform.next.value = 'seeItemAndContract';
      document.productform.itemkey.value = Itemkey;
      document.productform.target='form_input';
//      document.productform.target='djdshjhd';
      document.productform.submit();
    }

    function popUpCompare() {
      window.open("CompareContainer.jsp","compareWindow","menubar=no, directories=no, height=400 width=500, scrollbars=no, status=no, toolbar=no");
    }


        function addToOpportunity(a)
        {
		parent.document.location.href='<isa:webappsURL name="auction/seller/addtooppty.do?"/><%=ActionConstants.GOTOAUCTION%>';
        }

</script>

</head>

<BODY class="organizer"
<%
if ( (request.getParameter("popUpCompare") != null && request.getParameter("popUpCompare").equals("yes")) ||
    (request.getAttribute("popUpCompare") != null && request.getAttribute("popUpCompare").equals("yes")) )
{
%>
 onLoad="javascript:popUpCompare();"
<%} %>
>

<table cellpadding="0" cellspacing="0" border="0" class="backgroundColor95b1c1" height="100%" width="100%">
<tr>
    <td class="backgroundColore8e3d7">
                <br>
                <table cellpadding="2" cellspacing="1" border="0" width="98%">
                    <tr>
                            <td class="SEC_TB_TD" width="100%">&nbsp;
                            <span style="font-size:x-small;"><isa:translate key="eAuction.seller.selectItemsBeforeGoingToAuctionDetails"/></span>
                            &nbsp;
                            <span style="font-size:x-small;"><jsp:getProperty name="createOpptyForm" property="opportunityName" /></span>
                            </td>
                    </tr>
                </table>

    </td>
</tr>
<tr>
	<td class="backgroundColore8e3d7" width="100%" height="100%">


<div class="module-name"><isa:moduleName name="auction/seller/catalog/ProductsISA.jsp"/></div>

<%if ((request.getAttribute("isQuery") == null || !request.getAttribute("isQuery").equals("yes"))
      && request.getAttribute("isProductList") == null) {
 %>
<%@ include file="ProductsISAHeader.jsp"%>
<% } %>
<%if (request.getAttribute("isProductList") != null) {
    if (request.getAttribute("isProductList").equals("bestseller")){
    %>
      <div class="opener"><isa:translate key="bestseller.jsp.header"/></div>
    <%}
    else if (request.getAttribute("isProductList").equals("recommendation")){
    %>
      <div class="opener"><isa:translate key="recommendation.jsp.header"/></div>
    <%}
    }
%>

<!-- END OF CODE for the upper part of page (ie: Categorie Picture, Categorie Name, Categorie Description, And Filter -->

<FORM name="productform" action="<isa:webappsURL name="/auction/seller/auction/updateItems.do"/>" method="POST" target="">
<INPUT type="hidden" name="page" value="">
<INPUT type="hidden" name="itemPageSize" value="<%= itemPage.pageSize() %>">
<INPUT type="hidden" name="next" value="">
<INPUT type="hidden" name="itemkey" value="">
<%if (request.getAttribute("isQuery") == null || !request.getAttribute("isQuery").equals("yes")) { %>
<INPUT type="hidden" name="display_scenario" value="products">
<% } else { %>
<INPUT type="hidden" name="display_scenario" value="query">
<% } %>
<INPUT type="hidden" name="contractkey" value="">
<INPUT type="hidden" name="contractitemkey" value="">
<br>

<br>

<!-- Start: Table -->
<table class="list" border="0" cellspacing="0" cellpadding="3" width="100%">
<thead>
<tr>
    <td align="right" colspan="8">
	<span style="cursor:hand;" onclick="setPage(<%= itemPage.getPage()-1 %>);"><IMG height=1 src="layout/sp.gif" width="20"><img src="<isa:mimeURL name="auction/images/layout/left.gif"/>" width="14" height="13" border="0" alt="Previous Page"></span>&nbsp;
	<span class="TBLO_XXS">.&nbsp;Display&nbsp;
            <select name=pageselect size=1 onChange="setPageSize(document.productform.pageselect[document.productform.pageselect.selectedIndex].value);" valign=middle>
            <option value="5" <% if (itemPage.pageSize() == 5) { %>SELECTED<% } %>>5
            <option value="10" <% if (itemPage.pageSize() == 10) { %>SELECTED<% } %>>10
            <option value="25" <% if (itemPage.pageSize() == 25) { %>SELECTED<% } %>>25
            <option value="50" <% if (itemPage.pageSize() == 50) { %>SELECTED<% } %>>50
            <option value="0" <% if (itemPage.pageSize() == 0) { %>SELECTED<% } %>><isa:translate key="catalog.isa.all"/>
            </select>
	<span style="cursor:hand;" onclick="setPage(<%= itemPage.getPage()+1 %>);"><img src="<isa:mimeURL name="auction/images/layout/right.gif"/>" width="14" height="13" border="0" alt="Next Page"></span>&nbsp;&nbsp;&nbsp;
    </td>
</tr>
<tr>
<th colspan=1 align="center"><isa:translate key="catalog.isa.overview"/></th>
<th rowspan=2 align="center"><isa:translate key="catalog.isa.Picture"/></th>
<th colspan=2 align="center"><isa:translate key="catalog.isa.product"/></th>
<!-- At this point it might be that you should put here colspan=contractDetails.size()+3, that is for right now only 3, because we're not showing any contract information -->
<th rowspan=2 align=center><isa:translate key="catalog.isa.price"/></th>
<th rowspan=2 align=center><isa:translate key="eAuctions.seller.addtooppty"/></th>
</tr>
 <tr>
  <!-- <th>&nbsp;</th> -->
  <th align=center><isa:translate key="catalog.isa.quantity"/></th>
  <!-- <th>&nbsp</th> -->
  <th><isa:translate key="catalog.isa.productID"/></th>
  <th><isa:translate key="catalog.isa.description"/></th>
 </tr>
</thead>
<%
   i=-1;
   HashMap auctionItemsTable = createOpptyForm.getLineItems();
%>


  <isa:iterate id="item"
               name="itemPage"
               type="com.sap.isa.catalog.webcatalog.WebCatItem">

<%
  i++;
  java.util.Collection contractRefs = item.getContractItems();
  int noOfContractItems;
  boolean isAlone=(contractRefs == null || contractRefs.size() == 0);

  if (isAlone) noOfContractItems=1;
    else noOfContractItems=contractRefs.size()+1;
%>
 <input type="hidden" name="item[<%= i %>].itemID" value="<%= item.getItemID() %>">
 <input type="hidden" name="item[<%= i %>].contractKey" value="">
 <input type="hidden" name="item[<%= i %>].contractItemKey" value="">

<!-- Here starts the code for the main item data -->
<tr>
  <td align="center">
   <input type="text" class="submitDoc" name="item[<%= i %>].quantity" value="<%= item.getQuantity() %>" size="4" maxlength="4">
  </td>
  <td rowspan="<%= noOfContractItems %>" align="center">
    <%if (item.getAttribute("DOC_PC_CRM_THUMB") == null || item.getAttribute("DOC_PC_CRM_THUMB") == "") { %>
    <font color="ff0000">No Picture</font>
    <% } else { %>
    <img src="<%=item.getItemKey().getParentCatalog().getImageServer()+item.getAttribute("DOC_PC_CRM_THUMB") %>" alt="Thumbnail for product" width=60 height=60>
    <% } %>
  </td>
  <td rowspan="<%= noOfContractItems %>" align="left">
    <%= item.getAttribute("OBJECT_ID") %>&nbsp;
  </td>
  <td rowspan="<%= noOfContractItems %>" align="center">
     <%String prodDesc = null; %>
    <% if (item.getAttribute("TEXT_0001") != null && !item.getAttribute("TEXT_0001").equals("")) {
 		prodDesc=item.getAttribute("TEXT_0001");
 	} else {
 		prodDesc=item.getAttribute("OBJECT_DESCRIPTION");
 	}
     %>

     <%= prodDesc %> &nbsp;

  </td>

  <td align="right">
  <%if (item.getItemPrice().isScalePrice()) { %>
    <select name="price_combo_box" >
    <% java.util.Iterator pricesIterator = item.getItemPrice().getPrices();
       while (pricesIterator.hasNext()) {
          String st = (String) pricesIterator.next();
    %>
          <option value="lll"><%= st %></option>
    <% } %>
    </select>
  <% } else { %>
  <%= item.getItemPrice().getPrice() %>
  <% } %>
  </td>
  <td>
<%    if(auctionItemsTable != null && auctionItemsTable.get(item.getItemID()) != null)  { %>
    <input name='<%=com.sap.isa.catalog.actions.ActionConstants.RA_ITEMKEY%>checkbox' value='<%=item.getItemID()%>' checked type="checkbox"></input>
<%  } else {  %>
    <input name='<%=com.sap.isa.catalog.actions.ActionConstants.RA_ITEMKEY%>checkbox' value='<%=item.getItemID()%>' type="checkbox"></input>
<%  }  %>
  </td>

</tr>
<!-- End of code for the main item data -->

<!-- Start of code for Contract Subitems -->
<% if (contractDisplayMode.showContracts()) {%>
<%
  if (contractRefs != null) {
    java.util.Iterator contractItems=contractRefs.iterator();
    while (contractItems.hasNext()) {
      i++;
      WebCatItem subItem=(WebCatItem)contractItems.next();
%>
 <input type="hidden" name="item[<%= i %>].itemID" value="<%= subItem.getItemID() %>">
 <input type="hidden" name="item[<%= i %>].contractKey" value="<%= subItem.getContractRef().getContractKey().toString()%>">
 <input type="hidden" name="item[<%= i %>].contractItemKey" value="<%= subItem.getContractRef().getItemKey().toString()%>">
<tr>
  <td align="center">
   <input type="Checkbox" class="submitDoc" name="item[<%= i %>].selected" value="true" <%= subItem.isSelectedStr() %>>
  </td>
  <td align="center">
   <input type="text" class="submitDoc" name="item[<%= i %>].quantity" value="<%= subItem.getQuantity() %>" size="4" maxlength="4">
  </td>
  <td align="center" valign"=middle">
  <a href="javascript:addSingleItem('<%= item.getItemID() %>','<%= subItem.getContractRef().getContractKey().toString()%>','<%= subItem.getContractRef().getItemKey().toString()%>')">[WK]</a>
  </td>
  <td align="center"><%= subItem.getContractRef().getContractId()%></td>
  <td align="center"><%= subItem.getContractRef().getItemID()%></td>

  <!-- At this point it might be that you should iterate over all the detail names for a contract that should be displayed -->
  <%
  refNamesIterator = refNames.iterator();
  int q=-1;
  while (refNamesIterator.hasNext()) {
    refNamesIterator.next();
    q++; %>
  <td><%= subItem.getContractRef().getAttributeValues().get(q).getValue()+" "+subItem.getContractRef().getAttributeValues().get(q).getUnitValue() %></td>
  <% } %>
  <td align="center">
     <% if (item.getAttribute("CONFIG_USE_IPC") != null && item.getAttribute("CONFIG_USE_IPC ").equals("X")) { //it is an "old fashion" config%>
          <% if (item.getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null && item.getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("A")) { //a "old fashion" config%>
              <a href="javascript:getMoreFunctions('<%= item.getItemID() %>','','','config')"><isa:translate key="catalog.isa.config"/></a><br>
          <% } %>
          <% if (item.getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null && item.getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("X")) { //a "old fashion" config%>
              <a href="javascript:getMoreFunctions('<%= item.getItemID() %>','','','config')"><isa:translate key="catalog.isa.config"/></a><br>
          <% } %>
          <% if (item.getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null && item.getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("C")) { //a "old fashion" config, with special conditions%>
              <a href="javascript:getMoreFunctions('<%= item.getItemID() %>','','','config_special')"><isa:translate key="catalog.isa.config"/></a><br>
          <% } %>
     <% } %>
     <% if (item.getAttribute("CONFIG_USE_IPC") == null || item.getAttribute("CONFIG_USE_IPC ").equals("")) { //it might be a "new fashion" config%>
          <% if (item.getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null && item.getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("B")) { //a "new fashion" config%>
              &nbsp;
          <% } else {%>
              &nbsp;
          <% } %>
     <% } %>
     <% if (item.getAttribute("PRODUCT_VARIANT_FLAG") != null && !item.getAttribute("PRODUCT_VARIANT_FLAG").equals("")) { %>
            <a href="javascript:getMoreFunctions('<%= item.getItemID() %>','','','config_view')"><isa:translate key="catalog.isa.configView"/></a><br>
      <% } else {%>
            &nbsp;
      <% } %>
  </td>
  <td align="right">
  <%if (subItem.getItemPrice().isScalePrice()) { %>
    <select name="price_combo_box" >
    <% java.util.Iterator pricesIterator = subItem.getItemPrice().getPrices();
       while (pricesIterator.hasNext()) {
          String st = (String) pricesIterator.next();
    %>
          <option value="lll"><%= st %></option>
    <% } %>
    </select>
  <% } else { %>
  <%= subItem.getItemPrice().getPrice() %>
  <% } %>
  </td>
</tr>
<%
    } //end of while loop
  } //end of if the current Item has a valid list of subitems
%>
<% } %>
<!-- End of code for Contract Subitems -->
  </isa:iterate>

</table>
</table>
</div>
<input type="text" style="display:none;" name="<%=com.sap.isa.auction.actions.ActionConstants.GOTOAUCTION%>" text=""></input>
<input type="button" value="<isa:translate key="eAuction.product.goauction"/>" class="submitDoc" onclick="goToAuctionDetails();"></input>&nbsp;
<input type="submit" value="<isa:translate key="eAuction.product.clearselection"/>" class="submitDoc" onclick="clearSelectionItems();"></input>
</td></tr></table>
</td></tr></table>
</FORM>


</td>
	<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="100%" border="0" alt=""></td>
</tr>
<tr>
	<Td colspan="2"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="15" border="0" alt=""></TD>
</tr>
</table>

</body>
</html>
