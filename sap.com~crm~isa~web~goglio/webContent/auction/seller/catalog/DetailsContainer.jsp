<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.isacore.action.marketing.ShowCUAAction" %>
<isa:contentType />
<%@page import="com.sap.isa.core.util.JspUtil"%>
<%@page import="com.sap.isa.catalog.uiclass.ProductsUI"%>
<html>
  <head>
    <title>SAP Internet Sales B2B</title>
    <script type="text/javascript">
    <!--
      function resize(direction) {
        if (document.all) {
          if (direction == "min")
            //window.workareaFS.rows = '70%,15,0';
            window.workareaFS.rows = '*,15,36';
          else
            window.workareaFS.rows = '70%,15,*';
        }
      }
    //-->
    </script>
  </head>
  <frameset rows="*,15,36" id="workareaFS" border="1" frameborder="1" framespacing="1">
  <%String scenarioParams =  ProductsUI.determineScenario(request, false);
	String st="/catalog/getAtpInfo.do?" + scenarioParams;%>
    <frame name="positions" src="<isa:webappsURL name="<%= st %>"/>" frameborder="0">
    <frame name="closer_details" src="<isa:webappsURL name="/catalog/DetailsMiddle.jsp"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
    <frame name="details" src="<isa:webappsURL name="/catalog/crossSelling.do?inFrame=yes&cuaproducttype=C&"/><%=scenarioParams%>" frameborder="0">

    <!--  <isa:webappsURL name="b2b/cua.do"/><%= "ShowCUAAction.createRequest((String)request.getAttribute('productID'), ShowCUAAction.CROSSUPSELLING)"%>    -->
    <noframes>
      <body>&nbsp;</body>
    </noframes>
  </frameset>
</html>