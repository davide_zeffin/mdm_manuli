
<jsp:useBean    id="currentArea"
                type="com.sap.isa.catalog.webcatalog.WebCatArea"
                scope="request" >
</jsp:useBean>
<jsp:useBean    id="attributesList"
                type="java.util.ArrayList"
                scope="request" >
</jsp:useBean>


<div class="module-name"><isa:moduleName name="auction/seller/catalog/ProductsISAHeader.jsp"/></div>
<script language="JavaScript1.2">
var allowedValues=new Array();
var allowedValuesLength=new Array();
var noOfElements;
<%
  java.util.Iterator attributes = attributesList.iterator();
  java.util.Iterator values;
  i=-1;
  while (attributes.hasNext()) {
      WebCatAreaAttribute attribute = (WebCatAreaAttribute) attributes.next();
      if (attribute.getStyle() == WebCatAreaAttribute.INPUT_FIELD) {
      i++;
  %>
      allowedValues[<%= i %>]=new Array();
  <%
          values = attribute.getValues().iterator();
          int q=0;
          while (values.hasNext()) {
            q++;
            String value = (String) values.next();
%>
          allowedValues[<%= i %>][<%= q-1 %>]="<%= value %>";
<%
     } //end while values.hasNext()
%>
        allowedValuesLength[<%= i %>]=<%= q %>;
<%
  } // end if the attribute is an input field
  } // end loop on attributes
%>
     noOfElements=<%= i+1 %>;



</script>

<script language="JavaScript1.2">

    function verifier() {
      i=-1;
      for (l=0;l<document.filterForm.length;l++)
        if (document.filterForm.elements[l].type == "text")
        {
        i++;
//        if (document.filterForm.elements[l].value == '' ) document.filterForm.elements[l].value='<isa:translate key="catalog.isa.all"/>';
//        if (document.filterForm.elements[l].value.toUpperCase() == '<isa:translate key="catalog.isa.allUpper"/>' ) t=1;
        if (document.filterForm.elements[l].value == '' ) t=1;
            else t=0;
        for (j=0;j<allowedValuesLength[i];j++)
            if (document.filterForm.elements[l].value.toUpperCase() == allowedValues[i][j].toUpperCase()) t=1;
        if (allowedValuesLength[i] == 0) t=1;
        if (t == 0) {
//            st="<isa:translate key="catalog.isa.all"/>";
            st="";
            for (j=0;j<allowedValuesLength[i];j++)
                st+=(" "+allowedValues[i][j]);
            alert('Not a valid value !\nAllowed values are: '+st);
            document.filterForm.elements[l].focus();
            document.filterForm.elements[l].select();
            return false;
        }
      }
//    alert('everything is ok');
    return true;
  }

</script>


<!-- BEGIN OF CODE for the upper part of page (ie: Categorie Picture, Categorie Name, Categorie Description, And Filter -->
      <table width="100%" border="0">
        <tbody>
          <tr>
            <td>&nbsp;</td>
<!-- Here is the form for FILTERS. -->
            <td rowspan="2" align="right">
            <% if (attributesList.size() != 0) { %>
            <form action="<isa:webappsURL name="/auction/seller/catalog/filter.do"/>" method="POST" name="filterForm" onSubmit="return verifier();">
                <table border="0">
                  <thead>
                    <tr>
                      <td colspan="2"><div class="opener"><label><isa:translate key="catalog.isa.areaFilters"/></label></div></td>
                    </tr>
                  </thead>
                  <tbody>
<%

  attributes = attributesList.iterator();
  while (attributes.hasNext()) {
      WebCatAreaAttribute attribute = (WebCatAreaAttribute) attributes.next();
      if (attribute.getDescription() != null) {
%>
                     <tr>
                      <td><label><var><%= attribute.getDescription() %></var></label></td>
<%  if (attribute.getStyle() == WebCatAreaAttribute.DROP_DOWN && attribute.getValues() != null) { %>
                      <td><select style="width: 100px;" name="<%= attribute.getGuid() %>" >
                          <option selected="selected" value=""><isa:translate key="catalog.isa.all"/></option>
<%
          values = attribute.getValues().iterator();
          while (values.hasNext()) {
            String value = (String) values.next();
%>
                          <option value="<%= value %>"><%= value %></option>
<%
     } //end while values.hasNext()
%>
                        </select></td>
<%
  } // end if the Style is DropDown
%>

<%  if (attribute.getStyle() == WebCatAreaAttribute.RADIO_BUTTON && attribute.getValues() != null) { %>
                      <td> <input type="radio" name="<%= attribute.getGuid() %>" value="" checked> <isa:translate key="catalog.isa.all"/><br>
<%
          values = attribute.getValues().iterator();
          while (values.hasNext()) {
            String value = (String) values.next();
%>
                          <input type="radio" name="<%= attribute.getGuid() %>" value="<%= value %>"> <%= value %><br>
<%
     } //end while values.hasNext()
%>
                        </td>
<%
  } // end if the Style is RadioButton
%>

<%  if (attribute.getStyle() == WebCatAreaAttribute.INPUT_FIELD && attribute.getValues() != null) { %>
                      <td><input type="text" name="<%= attribute.getGuid() %>" size="7" maxlength="8">
                        </td>
<%
  } // end if the Style is input field
%>
                    </tr>
<% } //  end if the description is != null
  } //end looping through attributes %>

                    <tr>
                      <td>&nbsp;</td>
                      <td><input type="Submit" value="<isa:translate key="catalog.isa.Filter"/>"></td>
                    </tr>
                  </tbody>
                </table>
              </form>
              <% } else {%>
               &nbsp;
              <% } %>
              </td>
          </tr>
          <tr>
            <td>
              <table border="0">
                <tbody>
                  <tr>
<!-- Here is categorie's picture -->
                    <%if (currentArea.getCategory().getDetail("DOC_PC_CRM_IMAGE") == null) { %>
                    <td><font color="ff0000">No Picture for this Category</font></td>
                    <% } else { %>
                    <td><img src="<%=currentArea.getCatalog().getImageServer()+currentArea.getCategory().getDetail("DOC_PC_CRM_IMAGE").getAsString() %>" alt="Picture for Categorie" width=150 height=150></td>
                    <% } %>
                    <td><var><h2 style="margin-top: 0px;"><%= currentArea.getAreaName() %></h2></var> <%= currentArea.getCategory().getName() %> <br> <%= currentArea.getCategory().getDescription() %></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
        </tbody>
      </table>

<!-- END OF CODE for the upper part of page (ie: Categorie Picture, Categorie Name, Categorie Description, And Filter -->
