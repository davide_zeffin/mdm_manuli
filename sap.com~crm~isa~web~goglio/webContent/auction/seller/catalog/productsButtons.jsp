<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">
  </head>
  <body class="organizer">
    <div id="new-doc" class="module">
      <div class="module-name"><isa:moduleName name="auction/seller/catalog/productsButtons.jsp"/></div>
      <form action="">
      <table class="cat" width="100%" border="0" cellpadding="2" cellspacing="0">
        <tbody>
            <tr>
              <td><input type="button" value="<isa:translate key="catalog.isa.selectAll"/>" onclick="javascript:parent.productsFrame.setNext('selectAll')">&nbsp;
                  <input onclick="javascript:parent.productsFrame.setNext('unselectAll')" type="button" value="<isa:translate key="catalog.isa.unselectAll"/>"></td>
              <td class="scrollbarSpace" align="right">
                 <input type="button" value="<isa:translate key="catalog.isa.addItems"/>" onclick="javascript:parent.productsFrame.setNext('addToBasket')" >&nbsp;
                 <input type="button" value="<isa:translate key="catalog.isa.compareItems"/>" onclick="javascript:parent.productsFrame.setNext('compareItems')"></td>
            </tr>
        </tbody>
      </table>
      </form>
    </div>
  </body>
</html>