<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<html>
  <head>
     <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">
  </head>
  <body>
    <div id="new-doc" class="module">
      <div class="module-name"><isa:moduleName name="auction/seller/catalog/CompareUpperFrame.jsp"/></div>
      <form action="">
        <table width=100% border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><input type="button" value="<isa:translate key="catalog.isa.newCompare"/>" onClick="parent.compareFrame.setNext('compareSameWnd')">
              </td>
              <td align="right"><input type="button" value="<isa:translate key="catalog.isa.addItem"/>" onClick="parent.compareFrame.setNext('addToBasket')"> &nbsp;
              <input type="button" value="<isa:translate key="catalog.isa.close"/>" onclick="parent.window.close();"></td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </body>
</html>