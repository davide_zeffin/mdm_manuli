<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<isa:contentType />
<jsp:useBean    id="currentItem"
                type="com.sap.isa.catalog.webcatalog.WebCatItem"
                scope="request" >
</jsp:useBean>

<jsp:useBean    id="refNames"
                type="com.sap.isa.businessobject.contract.ContractAttributeList"
                scope="request" >
</jsp:useBean>

<%
    boolean isAlone=(currentItem.getContractItems() == null || currentItem.getContractItems().size() == 0);
    java.util.Iterator refNamesIterator;
%>

<html>
  <head>
     <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">

<script language="JavaScript1.2">
   function addSingleItem(Itemkey,contractkey,contractitemkey)
    {
      document.productform.next.value = 'addToBasket';
      document.productform.itemkey.value = Itemkey;
      document.productform.contractkey.value=contractkey;
      document.productform.contractitemkey.value=contractitemkey;
      document.productform.submit();
    }
</script>

  </head>
  <body class="organizer">
<div class="module-name"><isa:moduleName name="auction/seller/catalog/ContractDetails.jsp"/></div>
<FORM name="productform" action="<isa:webappsURL name="/catalog/updateItems.do"/>" method="POST">
<INPUT type="hidden" name="next" value="">
<INPUT type="hidden" name="itemkey" value="">
<INPUT type="hidden" name="contractkey" value="">
<INPUT type="hidden" name="contractitemkey" value="">
<input type="hidden" name="item[1].itemID" value="<%= currentItem.getItemID() %>">

<br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td>&nbsp;</td>
            <td align="left">
              <table border="0">
                <tbody>
                  <tr>
                    <td class="actualDetailsTab"><isa:translate key="catalog.isa.contractDetails"/></td>
                    <td class="DetailsTab"><a href="<isa:webappsURL name="/catalog/accesories.do?inFrame=yes&cuaproducttype=A"/>"><isa:translate key="catalog.isa.accesories"/></a></td>
                    <td class="DetailsTab"><a href="<isa:webappsURL name="/catalog/crossSelling.do?inFrame=yes&cuaproducttype=C"/>"><isa:translate key="catalog.isa.crossSelling"/></a></td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td align="left"><div class="opener"><isa:translate key="catalog.isa.detailsOn"/> <var><%= currentItem.getAttribute("OBJECT_DESCRIPTION") %></var></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr class="actualDetails">
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr class="actualDetails">
            <td>&nbsp;</td>
            <td width="100%" align="center" colspan=2>

              <table class="list" border="0" cellpadding="3" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th colspan=<%= refNames.size()+2+2 %>>
                    <isa:translate key="catalog.isa.contractDataTo"/> <%= currentItem.getAttribute("OBJECT_DESCRIPTION") %>
                  </th>
                </tr>
                <tr>
                  <th><isa:translate key="catalog.isa.quantity"/></th>
                  <th><isa:translate key="catalog.isa.buy"/></th>
                  <th><isa:translate key="catalog.isa.contractID"/></th>
                  <th><isa:translate key="catalog.isa.contractItemID"/></th>
                  <%
                  refNamesIterator = refNames.iterator();
                  while (refNamesIterator.hasNext()) {
                  com.sap.isa.businessobject.contract.ContractAttribute attr = (com.sap.isa.businessobject.contract.ContractAttribute) refNamesIterator.next();
                  %>
                  <th><%= attr.getDescription() %></th>
                  <% } %>
                </tr>
                </thead>
                <tbody>
                <%
                String rowColor = "odd";
                java.util.Collection contractRefs = currentItem.getContractItems();
                java.util.Iterator contractItems=contractRefs.iterator();
                while (contractItems.hasNext()) {
                WebCatItem subItem=(WebCatItem)contractItems.next();
                if (rowColor.equals("even")) rowColor="odd";
                   else rowColor="even";
                int q=-1;
%>
                   <tr >
                   <td><input type="text" size="4" name="item[1].quantity" value="<%= subItem.getQuantity() %>"></td>
                   <td><input type="button" value="[ok]" onClick="javascript:addSingleItem('<%= currentItem.getItemID() %>','<%= subItem.getContractDetailedRef().getContractKey().toString()%>','<%= subItem.getContractDetailedRef().getItemKey().toString()%>')"></td>
                   <td><%= subItem.getContractDetailedRef().getContractId()%></td>
                   <td><%= subItem.getContractDetailedRef().getItemID()%></td>
              <%
                refNamesIterator = refNames.iterator();
                while (refNamesIterator.hasNext()) {
                      refNamesIterator.next();
                      q++;
  %>
                            <td><%= subItem.getContractDetailedRef().getAttributeValues().get(q).getValue()+" "+subItem.getContractDetailedRef().getAttributeValues().get(q).getUnitValue() %></td>
                <%} %>
                   </tr>
                <% } %>
                </tbody>
              </table>

            </td>
            <td>&nbsp;</td>
          </tr>
          <tr class="actualDetails">
            <td colspan="4">&nbsp;</td>
          </tr>
        </tbody>
      </table>

  </body>
</html>