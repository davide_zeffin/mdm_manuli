<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />

<jsp:useBean id="createOpptyForm" scope="session"
class="com.sap.isa.auction.actionforms.OpportunityForm" />

<body marginheight="0" marginwidth="0" rightmargin="0" leftmargin="0" topmargin="0" bottommargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/catalog/blank_window.jsp"/></div>
<head>
     <link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">

     <link href="<isa:mimeURL name="auction/css/sellerPC.css"  language=""/>"
          type="text/css" rel="stylesheet">
</head>




<table cellpadding="0" cellspacing="0" border="0" class="backgroundColor95b1c1" height="100%" width="100%">

<tr>
	<td class="backgroundColore8e3d7" width="100%" height="100%">
		<br>
                <table cellpadding="2" cellspacing="1" border="0" width="98%">
                    <tr>
                            <td class="SEC_TB_TD" width="100%">&nbsp;
                            <span style="font-size:x-small;"><isa:translate key="eAuction.seller.selectItemsBeforeGoingToAuctionDetails"/></span>
                            &nbsp;
                            <span style="font-size:x-small;"><jsp:getProperty name="createOpptyForm" property="opportunityName" /></span>
                            </td>
                    </tr>
                </table>
	</td>
	<td class="backgroundColore8e3d7"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language="" />"  width="10" height="100%" border="0" alt=""></td>

</tr>
<!--<tr>
	<Td colspan="2"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>"  width="1" height="15" border="0" alt=""></TD>
</tr> -->
</table>



</body>
