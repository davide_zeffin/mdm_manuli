<!--<div class="module-name"><isa:moduleName name="auction/seller/catalog/ISAPath.jsp"/></div>-->

<script language="JavaScript">
function back() {
  parent.location.href="<isa:webappsURL name="/catalog/ProductsContainer.jsp?toNext=toItemPage"/>";
}
</script>

<jsp:useBean id="thePath" scope="request" type="java.util.ArrayList" />

<br><br>
<table border="0" cellspacing="0" cellpadding="0" align="right">
  <tr>
    <td>
    <%if ( request.getParameter("display_scenario") != null && request.getParameter("display_scenario").equals("products") ) { //I come from catalog %>
    <input type="button" value="<isa:translate key="catalog.isa.backTo"/>: <%= ((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(0)).getAreaName() %>" onClick="parent.location.href='<isa:webappsURL name="/catalog/ProductsContainer.jsp"/>'">
    <%} else { %>
    <input type="button" value="<isa:translate key="catalog.isa.backToQuery"/>" onClick="back();">
    <% } %>
    </td>
  </tr>
</table>
<br>
