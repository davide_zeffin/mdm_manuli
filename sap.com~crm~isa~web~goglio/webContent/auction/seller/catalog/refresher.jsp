<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.*" %>

<jsp:useBean    id="type"
                type="java.lang.String"
                scope="request" >
</jsp:useBean>

<%  // find out, if someone pressed the refresh button of the browser
	String req  = (String) request.getParameter("refresh");
    boolean refresh = (req != null && req.equals("1"));
%>

<head>
<script src="<%=WebUtil.getMimeURL(pageContext,  "", "b2b/jscript/frames.js") %>"
        type="text/javascript">
</script>

<script language="JavaScript">

var tmpWnd = isaTop();

<% com.sap.isa.core.util.Debug.print(type);%>

<% if (!type.equals("compareItems")) { %>
   tmpWnd.organizer_content.location.href="<isa:webappsURL name="/catalog/categories.do"/>";
<% } %>

<% if (type.equals("entry") || type.equals("initial") || type.length() == 0) { 
      com.sap.isa.core.util.Debug.print("Entry screen"); %>
    tmpWnd.work_history.form_input.location.href="<isa:webappsURL name="/b2b/catalogentry.do"/>";
<% } %>

<% if (type.equals("recommendation")) { %>
  tmpWnd.work_history.form_input.location.href="<isa:webappsURL name="/b2b/recommendation.do"/>";
<% } %>

<% if (type.equals("bestseller")) { %>
  tmpWnd.work_history.form_input.location.href="<isa:webappsURL name="/b2b/bestseller.do"/>";
<% } %>

<% if (type.equals("areaList")) { %>
  tmpWnd.work_history.form_input.location.href="<isa:webappsURL name="/b2b/catalogentry.do"/>";
<% } %>
   
<% if (type.equals("itemList")) { %>
  tmpWnd.work_history.form_input.location.href="<isa:webappsURL name="/catalog/ProductsContainer.jsp"/>";
<% } %>

<%if (type.equals("category_query")) { %>
  tmpWnd.work_history.form_input.location.href="<isa:webappsURL name="/catalog/ProductsContainer.jsp?toNext=toItemPage"/>";
<% } %>

<%if (type.equals("itemDetails")) { %>
  tmpWnd.work_history.form_input.location.href="<isa:webappsURL name="/catalog/DetailsContainer.jsp?display_scenario=products"/>";
<% } %>

<%if (type.equals("itemDetailsQuery")) { %>
  tmpWnd.work_history.form_input.location.href="<isa:webappsURL name="/catalog/DetailsContainer.jsp?display_scenario=query"/>";
<% } %>

<% if (!type.equals("compareItems") && !refresh) { %>
  tmpWnd.work_history.documents.location.href="<isa:webappsURL name="/b2b/catalogonp.do"/>?type=<%=type%>";
<% } %>

<% if (type.equals("compareItems")) { %>
     window.parent.close();
<% } %>
</script>
</head>
<body></body>
