<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />

<jsp:useBean id="itemAttributesIterator" scope="request" type="java.util.Iterator" />
<jsp:useBean id="currentItem" scope="request" type="com.sap.isa.catalog.webcatalog.WebCatItem" />

     <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">


<script language="JavaScript1.2">

   function addSingleItem(Itemkey)
    {
      document.productform.next.value = 'addToBasket';
      document.productform.itemkey.value = Itemkey;
      document.productform.submit();
    }

    function getMoreFunctions(Itemkey,Next) {
      document.productform.itemkey.value = Itemkey;
      document.productform.next.value = Next;
      document.productform.target='form_input';
      document.productform.submit();
    }

</script>

<html>
<head>
<title><isa:translate key="catalog.isa.productTitle"/>
</title>
</head>
<body class="organizer" marginheight="0" marginwidth="0" rightmargin="0" leftmargin="0" topmargin="0" bottommargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/catalog/ProductDetailISA.jsp"/></div>

<table cellpadding="0" cellspacing="0" border="0" bgcolor="#95b1c1" height="100%">
<tr>
	<td bgcolor="#e8e3d7" width="100%" height="100%">


<%@ include file="attributeNameParser.jsp" %>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody>
     <tr>
       <td><h2><isa:translate key="catalog.isa.productDescription"/></h2></td>
       <td style="padding-right: 25px">&nbsp;</td>
       <td style="vertical-align:middle">
         <table width="80%" border="0">
           <tbody>
             <tr>
               <td><isa:translate key="catalog.isa.productNo"/>: <%= currentItem.getProduct() %></td>
               <td align="right"><a href="javascript:getMoreFunctions('<%= currentItem.getItemID() %>','MoreFunctions')"><isa:translate key="catalog.isa.moreFunctions"/></a></td>
             </tr>
           </tbody>
         </table>
       </td>
     </tr>
     <tr>
       <td align="left" valign="top">
          <%if (currentItem.getAttribute("DOC_PC_CRM_IMAGE") == null || currentItem.getAttribute("DOC_PC_CRM_IMAGE") == "") { %>
          <font color="ff0000">No Picture</font>
          <% } else { %>
          <img src="<%=currentItem.getItemKey().getParentCatalog().getImageServer()+currentItem.getAttribute("DOC_PC_CRM_IMAGE") %>" alt="Picture for Product" width=100 height=100>
          <% } %>
        <br>
       </td>
       <td>&nbsp;</td>
       <td>
         <table width="80%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
               <tr>
                 <td colspan="2"><isa:translate key="catalog.isa.productDescription"/>: <%= currentItem.getDescription() %></td>
               </tr>
               <% if (currentItem.getAttribute("TEXT_0001") != null && !currentItem.getAttribute("TEXT_0001").equals("")) { %>
               <tr>
                 <td colspan="2"><isa:translate key="catalog.isa.productLongDescr"/>: <%= currentItem.getAttribute("TEXT_0001") %></td>
               </tr>
               <% } %>
               <tr>
<%if (request.getAttribute("detailScenario") == null ||
      request.getAttribute("detailScenario").equals("BestSeller") ||
      request.getAttribute("detailScenario").equals("PersonalReccomendations")) { %>
<FORM name="productform" action="<isa:webappsURL name="/catalog/updateItems.do"/>" method="POST">
    <%if ( request.getParameter("display_scenario") != null && request.getParameter("display_scenario").equals("products") ) { //I come from catalog %>
    <INPUT type="hidden" name="display_scenario" value="products">
    <% } %>
    <%if ( request.getParameter("display_scenario") != null && request.getParameter("display_scenario").equals("query") ) { //I come from catalog %>
    <INPUT type="hidden" name="display_scenario" value="query">
    <% } %>
                 <td><isa:translate key="catalog.isa.price"/>:
                      <table>
                      <% java.util.Iterator pricesIterator = currentItem.getItemPrice().getPrices();
                          while (pricesIterator.hasNext()) {
                          String st = (String) pricesIterator.next();
                      %>
                      <tr><td><%= st %></td></tr>
                      <% } %>
                      </table>
                      </td>
                 <td>
     <% if (currentItem.getAttribute("CONFIG_USE_IPC") != null && currentItem.getAttribute("CONFIG_USE_IPC").equals("X")) { //it is an "old fashion" config%>
          <% if (currentItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null && currentItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("A")) { //a "old fashion" config%>
              <a href="javascript:getMoreFunctions('<%= currentItem.getItemID() %>','','','config')"><isa:translate key="catalog.isa.config"/></a><br>
          <% } %>
          <% if (currentItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null && currentItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("X")) { //a "old fashion" config%>
              <a href="javascript:getMoreFunctions('<%= currentItem.getItemID() %>','','','config')"><isa:translate key="catalog.isa.config"/></a><br>
          <% } %>
          <% if (currentItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null && currentItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("C")) { //a "old fashion" config, with special conditions%>
              <a href="javascript:getMoreFunctions('<%= currentItem.getItemID() %>','','','config_special')"><isa:translate key="catalog.isa.config"/></a><br>
          <% } %>
     <% } %>
     <% if (currentItem.getAttribute("CONFIG_USE_IPC") == null || currentItem.getAttribute("CONFIG_USE_IPC").equals("")) { //it might be a "new fashion" config%>
          <% if (currentItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null && currentItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("B")) { //a "new fashion" config%>
              &nbsp;
          <% } else {%>
              &nbsp;
          <% } %>
     <% } %>
     <% if (currentItem.getAttribute("PRODUCT_VARIANT_FLAG") != null && !currentItem.getAttribute("PRODUCT_VARIANT_FLAG").equals("")) { %>
            <a href="javascript:getMoreFunctions('<%= currentItem.getItemID() %>','','','config_view')"><isa:translate key="catalog.isa.configView"/></a><br>
      <% } else {%>
            &nbsp;
      <% } %>
                 </td>
               </tr>
               <tr>
                 <td colspan="2">&nbsp;</td>
               </tr>
               <tr>
                 <td><isa:translate key="catalog.isa.quantity"/>:&nbsp;<input type="text" size="4" name="item[1].quantity" value="<%= currentItem.getQuantity() %>">&nbsp;
                      <%= currentItem.getAttribute("UNITOFMEASUREMENT") %>
                 </td>
               <td valign="baseline">
               <a class="icon" href="javascript:addSingleItem('<%= currentItem.getItemID() %>')">[WK]</a>
               &nbsp;<a class="icon" href="javascript:addSingleItem('<%= currentItem.getItemID() %>')"><isa:translate key="catalog.isa.addItem"/></a>
               </td>
               </tr>
               <tr>
                 <td colspan="2">&nbsp;</td>
               </tr>
               <tr>
                 <td style="vertical-align:middle;">
                 <%if (request.getAttribute("atpQuantity") != null && request.getAttribute("atpDate") != null ) {
                      if (request.getAttribute("atpQuantity").equals("0")) {
                 %>
                      <isa:translate key="catalog.isa.noAtp"/>
                      <% } else {%>
                          <isa:translate key="catalog.isa.availability"/>:&nbsp;&nbsp;
                          <a class="icon" href="javascript:getMoreFunctions('<%= currentItem.getItemID() %>','seeItemAndContract')"><isa:translate key="catalog.isa.check"/></a>
                          <br></td>
                          <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>
                          <%= request.getAttribute("atpQuantity") %> <%= currentItem.getAttribute("UNITOFMEASUREMENT") %> <isa:translate key="catalog.isa.to"/> <%= request.getAttribute("atpDate") %> <isa:translate key="catalog.isa.available"/>
                 <%      }
                    }else {%>
                 &nbsp;
                 <% } %>
                 </td>
<INPUT type="hidden" name="next" value="">
<INPUT type="hidden" name="itemkey" value="">
<input type="hidden" name="item[1].itemID" value="<%= currentItem.getItemID() %>">
<INPUT type="hidden" name="contractkey" value="">
<INPUT type="hidden" name="contractitemkey" value="">
<% } %>
<%if (request.getAttribute("detailScenario") != null && request.getAttribute("detailScenario").equals("ItemFromBasket")) { %>
<% } %> <!-- please provide here a code which is similar to what I wrote above -->
                 <td>&nbsp;</td>
               </tr>
               <tr>
                 <td colspan="2">&nbsp;</td>
               </tr>
               <tr>
                 <td colspan="2">
                   <table class="list" border="0" cellpadding="3" cellspacing="0">
                     <tbody>
                  <%String rowColor = "even";
                  while (itemAttributesIterator.hasNext()) {
                    com.sap.isa.core.eai.boi.catalog.IAttributeValue item=(com.sap.isa.core.eai.boi.catalog.IAttributeValue) itemAttributesIterator.next();
  %>
                            <% if (!item.getAttribute().isBase() && item.getAttributeDescription() != null && item.getAttributeName().indexOf("_") != 0) {
                                  if (rowColor.equals("even")) rowColor="odd";
                                      else rowColor="even";
                            %>
                               <tr class="<%= rowColor %>">
                                 <td><%= item.getAttributeDescription() %></td>
                                 <td><%= item.getAsString() %>
                                 <%if ( !currentItem.getAttribute("_"+item.getAttributeName()).equals("") ) { %>
                                 <%= currentItem.getAttribute("_"+item.getAttributeName()) %>
                                 <% } %>
                                 </td>
                               </tr>
                            <% } else {
                                String st = parseAttributeName(pageContext, "catalog.isa",item.getAttributeName());
                                if (st != null) {
                                    if (rowColor.equals("even")) rowColor="odd";
                                        else rowColor="even";
                            %>
                               <tr class="<%= rowColor %>">
                                 <td><%= st %></td>
                                 <td><%= item.getAsString() %></td>
                               </tr>
                            <% } } %></td>
  <%} %>
                        </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>


</form>
<% if (request.getAttribute("detailScenario") == null) {%>
<%@ include file="ISAPath.jsp"%>
<%} else { %>
      <% if (request.getAttribute("detailScenario").equals("bestseller")) { %>
          <a href="<isa:webappsURL name="/b2b/bestseller.do"/>" target="form_input"><isa:translate key="catalog.isa.backTo"/> <%= request.getAttribute("detailScenario") %></a>
       <% } %> <!-- please add your if's here in the same way -->

<% } %>

</td>
	<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="100%" border="0" alt=""></td>
</tr>
<tr>
	<Td colspan="2"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="15" border="0" alt=""></TD>
</tr>
</table>

</body>
