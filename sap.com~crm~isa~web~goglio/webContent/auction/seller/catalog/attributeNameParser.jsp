<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%!
public String parseAttributeName(javax.servlet.jsp.PageContext pageContext , String domainKey, String key) {
	try {
	    String text = WebUtil.translate(pageContext, domainKey+"."+key, null);
          if (text != null)
              if (text.equals("null") || text.indexOf("???") == 0) text=null;
        return text;
        }
    catch (java.util.MissingResourceException ex) {return null;}
}
%>
