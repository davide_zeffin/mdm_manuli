<%@ page language="java" %>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<isa:contentType />

<jsp:useBean id="theItems" scope="request" type="java.util.ArrayList" />
<jsp:useBean id="theNames" scope="request" type="java.util.Iterator" />


<%@ include file="attributeNameParser.jsp" %>


<html>
<head>
     <link href="<isa:mimeURL name="b2b/css/b2b.css"  language=""/>"
          type="text/css" rel="stylesheet">
</title>
</head>
<body class="workarea" >
<div class="module-name"><isa:moduleName name="auction/seller/catalog/CompareItemsISA.jsp"/> </div>

<script language="JavaScript1.2">

   function addSingleItem(Itemkey,contractkey,contractitemkey)
    {
      document.compareform.next.value = 'addToBasket';
      document.compareform.itemkey.value = Itemkey;
      document.compareform.contractkey.value=contractkey;
      document.compareform.contractitemkey.value=contractitemkey;
      document.compareform.submit();
    }

   function setNext(Next)
    {
      document.compareform.next.value = Next;
      document.compareform.submit();
    }

</script>


<p>&nbsp;</p>
<h2><isa:translate key="catalog.isa.productCompare"/></h2>
<br>

<form name="compareform" action="<isa:webappsURL name="/catalog/updateItems.do"/>" method="POST">
<INPUT type="hidden" name="next" value="">
<INPUT type="hidden" name="itemkey" value="">
<INPUT type="hidden" name="contractkey" value="">
<INPUT type="hidden" name="contractitemkey" value="">
<table class="list" border="0" cellspacing="0" cellpadding="3" align="center">
  <tbody>
    <tr> <!-- row for showing the picture -->
      <td class="odd">&nbsp;</td>
    <%
    java.util.Iterator itemsIterator;
    int i;
    itemsIterator=theItems.iterator();
    while (itemsIterator.hasNext()) {
        WebCatItem item = (WebCatItem) itemsIterator.next();
    %>
<!-- Retrieving the picture -->
      <td class="odd">
          <%if (item.getAttribute("DOC_PC_CRM_IMAGE") == null || item.getAttribute("DOC_PC_CRM_IMAGE") == "") { %>
          <font color="ff0000">No Picture</font>
          <% } else { %>
          <img src="<%=item.getItemKey().getParentCatalog().getImageServer()+item.getAttribute("DOC_PC_CRM_IMAGE") %>" alt="Picture for Product" width=60 height=60>
          <% } %>
          </td>
<% } %>
    </tr> <!-- end of row for showing the picture -->
    <tr> <!-- row for showing Product No -->
      <td class="odd"><isa:translate key="catalog.isa.productNo"/>:</td>
    <%
    itemsIterator=theItems.iterator();
    while (itemsIterator.hasNext()) {
        WebCatItem item = (WebCatItem) itemsIterator.next();
    %>
      <td class="odd"><%= item.getProduct() %></td>
<% } %>
    </tr> <!-- end of row for showing Product No -->
    <tr> <!-- row for showing Product Description -->
      <td class="odd"><isa:translate key="catalog.isa.productDescription"/>:</td>
    <%
    itemsIterator=theItems.iterator();
    while (itemsIterator.hasNext()) {
        WebCatItem item = (WebCatItem) itemsIterator.next();
    %>
      <td class="odd"><%= item.getDescription() %></td>
<% } %>
    </tr> <!-- end of row for showing Product Description -->
    <tr> <!-- row for showing Price -->
      <td class="odd"><isa:translate key="catalog.isa.price"/>:</td>
    <%
    itemsIterator=theItems.iterator();
    while (itemsIterator.hasNext()) {
        WebCatItem item = (WebCatItem) itemsIterator.next();
    %>
      <td class="odd">
        <table border=0>
        <% java.util.Iterator pricesIterator = item.getItemPrice().getPrices();
           while (pricesIterator.hasNext()) {
                 String st = (String) pricesIterator.next();
        %>
           <tr><td><%= st %></td></tr>
        <% } %>
        </table>
      </td>
<% } %>
    </tr> <!-- end of row for showing Price -->

    <tr> <!-- row for add to basket form elements -->
      <td class="odd" rowspan=2><isa:translate key="catalog.isa.addToBasket"/>:</td>
    <%
    i=-1;
    itemsIterator=theItems.iterator();
    while (itemsIterator.hasNext()) {
        WebCatItem item = (WebCatItem) itemsIterator.next();
        i++;
    %>
 <input type="hidden" name="item[<%= i %>].itemID" value="<%= item.getItemID() %>">
 <%if (item.getContractRef() != null ) { %>
 <input type="hidden" name="item[<%= i %>].contractKey" value="<%= item.getContractRef().getContractKey().toString()%>">
 <input type="hidden" name="item[<%= i %>].contractItemKey" value="<%= item.getContractRef().getItemKey().toString()%>">
 <%} else { %>
 <input type="hidden" name="item[<%= i %>].contractKey" value="">
 <input type="hidden" name="item[<%= i %>].contractItemKey" value="">
 <% } %>
     <td class="odd">
     <input type="text" name="item[<%= i %>].quantity" value="<%= item.getQuantity() %>" size="4" maxlength="4">
<!-- The unit of measurement -->
     <%= item.getAttribute("UNITOFMEASUREMENT") %>
    <%if (item.getContractRef() != null ) { %>
     <input type="button" value="Ok" onClick="javascript:addSingleItem('<%= item.getItemID() %>','<%= item.getContractRef().getContractKey().toString()%>','<%= item.getContractRef().getItemKey().toString()%>')">
    <% } else { %>
     <input type="button" value="Ok" onClick="javascript:addSingleItem('<%= item.getItemID() %>','','')">
    <% } %>
      </td>
<% } %>
    </tr> <!-- end of row for add to basket form elements -->

    <tr> <!-- row for the other add to basket form elements -->
    <%
    i=-1;
    itemsIterator=theItems.iterator();
    while (itemsIterator.hasNext()) {
        WebCatItem item = (WebCatItem) itemsIterator.next();
        i++;
    %>
     <td class="odd">
      <input type="Checkbox" name="item[<%= i %>].selected" value="true" <%= item.isSelectedStr() %>>
     </td>
<% } %>
    </tr> <!-- end of row for the other add to basket form elements -->
<tr>
<td colspan=<%= theItems.size()+1 %> class="odd"><b><isa:translate key="catalog.isa.details"/></b></td>
</tr>
<!-- starting block for the rest of details -->
    <%
    while (theNames.hasNext()) {
        com.sap.isa.core.eai.boi.catalog.IAttribute name = (com.sap.isa.core.eai.boi.catalog.IAttribute) theNames.next();
    %>
      <% if (!name.isBase() && name.getDescription() != null && name.getName().indexOf("_") != 0) {%>
          <tr class="odd">
          <td><%= name.getDescription() %></td>
          <%
            itemsIterator=theItems.iterator();
            while (itemsIterator.hasNext()) {
              WebCatItem item = (WebCatItem) itemsIterator.next();
          %>
          <td class="odd">
            <%= item.getAttribute(name.getGuid()) %>
          <%if ( !item.getAttribute("_"+name.getName()).equals("") ) {%>
            <%= item.getAttribute("_"+name.getName()) %>
          <% } %>
          </td>
          <% } %>
          </tr>
       <% } else {
           String st = parseAttributeName(pageContext, "catalog.isa",name.getName());
        if (st != null) {
       %>
          <tr class="odd">
          <td><%= st %></td>
          <%
            itemsIterator=theItems.iterator();
            while (itemsIterator.hasNext()) {
              WebCatItem item = (WebCatItem) itemsIterator.next();
          %>
          <td class="odd">
            <%= item.getAttribute(name.getGuid()) %>
          </td>
          <% } %>
          </tr>
          <% } } %>
  <%
    } //end of while there are still other names loop
  %>

    <tr> <!-- row for showing contractID -->
      <td class="odd"><isa:translate key="catalog.isa.contractID"/></td>
    <%
    itemsIterator=theItems.iterator();
    while (itemsIterator.hasNext()) {
        WebCatItem item = (WebCatItem) itemsIterator.next();
        if (item.getContractRef() != null) {
    %>
        <td class="odd"><%= item.getContractRef().getContractId()%></td>
    <% } else { %>
        <td class="odd" rowspan=2 align="center" valign="middle"></td>
<% } } %>
    </tr> <!-- end of row for showing contractID -->

    <tr> <!-- row for showing contractItemID -->
      <td class="odd"><isa:translate key="catalog.isa.contractItemID"/></td>
    <%
    itemsIterator=theItems.iterator();
    while (itemsIterator.hasNext()) {
        WebCatItem item = (WebCatItem) itemsIterator.next();
        if (item.getContractRef() != null) {
    %>
        <td class="odd"><%= item.getContractRef().getItemID()%></td>
<% } } %>
    </tr> <!-- end of row for showing contractItemID -->


</tbody>
</table>
</form>

</body>
