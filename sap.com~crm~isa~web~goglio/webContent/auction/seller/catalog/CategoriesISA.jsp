<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatWeightedArea" %>
<isa:contentType />
<jsp:useBean    id="categoriesTree"
                type="java.util.ArrayList"
                scope="request" >
</jsp:useBean>
<jsp:useBean    id="currentArea"
                type="com.sap.isa.catalog.webcatalog.WebCatArea"
                scope="request" >
</jsp:useBean>
<head>
     <link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">

     <link href="<isa:mimeURL name="auction/css/sellerPC.css"  language=""/>"
          type="text/css" rel="stylesheet">
</head>

<body class="organizer" marginheight="0" marginwidth="0" rightmargin="0" leftmargin="0" topmargin="0" bottommargin="0">

<table cellpadding="0" cellspacing="0" border="0" class="backgroundColor95b1c1" height="100%">
<tr>
	<td class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="100%" border="0" alt=""></td>
	<td class="backgroundColore8e3d7" width="100%" height="100%">

<div id="new-doc" class="module">
<div class="module-name"><isa:moduleName name="auction/seller/catalog/CategoriesISA.jsp"/></div>

<font size="-2">	
<table border="0" cellspacing="0" cellpadding="0">
<tr valign="middle">
<td valign="middle">
<br><br>
</td>
</tr>
<tr valign="middle">
<td valign="middle">
<br><br>
      <form action="<isa:webappsURL name="/auction/seller/catalog/query.do"/>" target="productsFrame">
        <table>
          <tr>
            <td><isa:translate key="catalog.isa.quickSearch"/></td>
            <td><input type="text" class="submitDoc" name="query" size="22"></td>
          <tr>
            <td></td>
            <td nowrap><input type="Submit" class="submitDoc" value="<isa:translate key="catalog.isa.search"/>"> </td>
          </tr>
          </tr>
        </table>
      </form>
<br><br>
</td>
</tr>

<% int i=0;
   int neededPosition=0;
   int j;
   int isBrother;
   int indentSize;
   for (int pos=0;pos<categoriesTree.size();pos++) {
%>
<tr valign="bottom">
<td valign="bottom">
<%
   WebCatWeightedArea area = (WebCatWeightedArea)categoriesTree.get(pos);
   i++;
   indentSize=14*area.getPosition();
   String tmpPath;
   %>
  <img src="<isa:mimeURL name="catalog/mimes/isa/graphics/blank.gif"  language=""/>"
border=0 height=14 width=<%= indentSize %>>
<% if (area.getSign() == area.MINUS){
      tmpPath="/auction/seller/catalog/categorieInPath.do?key="+area.getPath()+"&type=minus";
  %>
  <a href="<isa:webappsURL name="<%= tmpPath %>"/>"><img src="<isa:mimeURL name="catalog/mimes/isa/graphics/minus.gif"  language=""/>"
border=0 height=14 width=14></a>
<% } %>
<% if (area.getSign() == area.PLUS){
      tmpPath="/auction/seller/catalog/categorieInPath.do?key="+area.getPath()+"&type=plus";
%>
  <a href="<isa:webappsURL name="<%= tmpPath %>"/>"><img src="<isa:mimeURL name="catalog/mimes/isa/graphics/plus.gif"  language=""/>"
border=0 height=14 width=14></a>
<% } %>
<% if (area.getSign() == area.NOTHING){
%>
  <img src="<isa:mimeURL name="catalog/mimes/isa/graphics/blank.gif"  language=""/>"
border=0 height=14 width=14>
<% } %>
  <% tmpPath="/auction/seller/catalog/categorieInPath.do?key="+area.getPath()+"&type=Full";%>
  <a href="<isa:webappsURL name="<%= tmpPath %>"/>" name="p<%= i %>" target="productsFrame">
  <% if (area.getArea().equals(currentArea)) {%>
  <i><b><%= area.getArea().getAreaName()%></b></i>
  <% } else {%>
  <%= area.getArea().getAreaName()%>
  <% } %>
  </a><br>
</td>
</tr>
  <%
  if (area.getArea().equals(currentArea)) neededPosition=i;
} %>

</table>

</td>
</tr>
<!--<tr>
	<Td colspan="2"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="15" border="0" alt=""></TD>
</tr> -->
</table>




<script language="JavaScript1.2">
  document.location.href="#p<%= neededPosition %>";
</script>
</font>
</div></div>
</body>
