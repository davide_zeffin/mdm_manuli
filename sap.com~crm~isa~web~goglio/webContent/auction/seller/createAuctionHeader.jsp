<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.*,com.sap.isa.auction.bean.*, com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page language="java" %>
<%@ page import="com.sap.isa.businessobject.*,com.sap.isa.core.*" %>
<%@ page import ="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.auction.actionforms.CreateOpportunityForm" %>
<%@ page import="com.sap.isa.auction.actionforms.OpportunityForm" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<html>

<HEAD><title><isa:translate key="eAuctions.title"/></title>


<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script src="<isa:mimeURL name="auction/jscript/calendar.js"  language=""/>"
          type="text/javascript"></script>

<SCRIPT>
function MM_goToURL() { //v3.0
  		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}


function openProduct()
	{

		classPrd = window.open("<isa:webappsURL name="auction/seller/catalog/getCatalog.do"/>", "Start", "status,width=700,height=600,scrollbars");
		which = "start";
}

function fillProduct(a,b,c,d,e) {
     		classPrd.close();
		document.createOpptyForm.productName.value=c;
		document.createOpptyForm.productCatalog.value=d;
		document.createOpptyForm.productId.value=b;
		document.createOpptyForm.productDesc.value=a;
		document.createOpptyForm.productUOM.value=e;

}

function addToOpportunity(a) {
     		classPrd.close();
		document.location.href='<isa:webappsURL name="auction/seller/addtooppty.do?itemkey="/>'+a;

}

function openTarget()
	{
	     classTG = window.open("<isa:webappsURL name="auction/seller/targetgroup.do?nextscreen=define"/>", "targetGroup", "status,scrollbars,width=700,height=400");
		which = "targetGroup";
	}
function fillTargetInfo(targetgroups)
	{
		classTG.close();
		document.createOpptyForm.targetGroup.value=targetgroups;

	}
</SCRIPT>
</head>

<body class="backgroundColor95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/createAuctionHeader.jsp"/></div>

<form>


<%@ include file="webAuctionHeader.html" %> 

<br>
<table cellpadding="0" cellspacing="0" border="0">
 	 <tr>
		<Td class="CONTENT">&nbsp;&nbsp;&nbsp;<isa:translate key="eAuctions.shop.selected.title"/>
                <%  UserSessionData userData = UserSessionData.getUserSessionData(session);
                    BusinessObjectManager bom = (BusinessObjectManager) (BusinessObjectManager)userData.getBOM(BusinessObjectManager.ISACORE_BOM);

               String id="NULL";
				String currency="USD";
				if (bom !=null){
					id = bom.getShop().getId();
			  		currency= bom.getShop().getCurrency();
			  	}
                %>
                   <B><%=id%></B>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<isa:webappsURL name="auction/seller/selectwebshop.jsp"/>" class="CONTENT" target="_parent"><B>
			<isa:translate key="eAuctions.shop.change.title"/></B></a></TD>
	</tr>
</table>

<br>

<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
<%
  CreateOpportunityForm createOpptyForm = (CreateOpportunityForm)pageContext.getSession().getAttribute("createOpptyForm");
  if(createOpptyForm.getMode().equals(OpportunityForm.NEW))  {
%>
          <TD width=28><IMG alt="" border=0 height=19
            src="<isa:mimeURL name="auction/images/layout/fron.gif"  language=""/>"  width=20></TD>
          <TD background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class=TABB noWrap
          width="5%">&nbsp;<isa:translate key="eAuctions.seller.create.title"/>&nbsp;</TD>
          <TD width=24><IMG alt="" border=0 height=19
            src="<isa:mimeURL name="auction/images/layout/onoff.gif"  language=""/>"  width=20></TD>
<%  }  else  {
%>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/froff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>" class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/catsearch.jsp"/>"><isa:translate key="eAuctions.seller.create.title"/></a>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/offoff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
<%  }
%>		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>"  class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/monitorOpportunity.do"/>" target="_parent">
			<isa:translate key="eAuctions.seller.monitor.title"/></a>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkoff.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>


</form>
</body>

<script>
	document.body.scroll="no";
</script>
</html>
