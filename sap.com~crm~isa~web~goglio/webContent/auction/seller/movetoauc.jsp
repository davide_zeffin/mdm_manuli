<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*"%>
<%@ page language="java" %>
<%@ page import="com.sap.isa.businessobject.*,com.sap.isa.core.*" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">

 function getList(action) {
          document.monitorForm.status.value = action;
          monitorForm.submit();
 }


</script>
</head>
<body bgcolor="95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/movetoauc.jsp"/></div>
<%@ include file="webAuctionHeader.html" %> 

<%@ include file="shopChangeLink.jsp" %> 




<br>

<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/froff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>" class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/catsearch.jsp"/>"><isa:translate key="eAuctions.seller.create.title"/></a>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/offon.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;<isa:translate key="eAuctions.seller.monitor.title"/>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkon.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="e8e3d7" height="500">
	<TR>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</TR>
	<tr>
	        <td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<form  name="monitorForm" action='<isa:webappsURL name="auction/seller/getopportunitylist.do"/>'>
		<input type="hidden" name="status" />


		<td>
		        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('open')"><isa:translate key="eAuctions.seller.open.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('published')"><isa:translate key="eAuctions.seller.published.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('activeopportunity')"><isa:translate key="eAuctions.seller.active.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('closed')"><isa:translate key="eAuctions.seller.closed.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('finalized')"><isa:translate key="eAuctions.seller.finalized.title"/></a>
			<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="200" height="1" border="0" alt=""><A href="<isa:webappsURL name="auction/seller/oppsearch.jsp"/>">
			<img src="<isa:mimeURL name="auction/images/layout/button_search.gif"  language=""/>" width="15" height="15" border="0" alt="">&nbsp;&nbsp;<isa:translate key="eAuctions.seller.search.title"/></a>

		</td>

		</form>
        </tr>
        <tr>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<td colspan="2"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="5" border="0" alt=""></td>	</tr>
	
	<tr>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></td>
		<td class="CONTENT">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="30" height="11" border="0" alt="">&nbsp;&nbsp;
		<isa:translate key="eAuctions.seller.confirm.title"/>&nbsp;&nbsp;<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="600" height="11" border="0" alt=""></td>
	</tr>
	
	<tr>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></td>
		<Td>&nbsp;</TD>
	</tr>



	<form  action='<isa:webappsURL name="auction/seller/changestatus.do"/>'>
	<input type=hidden name="editaction" value="active"/>



         <%      HttpSession s = request.getSession();
                                OpportunityBean opbean = (OpportunityBean) s.getAttribute("oppbeanforbid");
         %>
        <tr>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<Td class="CONTENT">&nbsp;&nbsp;&nbsp;<isa:translate key="eAuctions.seller.msg.active.confirm"/></TD>
	</tr>
	<tr> <td bgcolor="95b1c1">&nbsp;  </td>

	<tr>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<td>&nbsp;&nbsp;&nbsp;<input type="submit" name="yes" value='<isa:translate key="eAuctions.seller.msg.yes"/>' class="submitDoc">&nbsp;&nbsp;&nbsp;
		<input type="button" name="no" value='<isa:translate key="eAuctions.seller.msg.no"/>'
 		onClick="javascript:location.replace('published.jsp')" class="submitDoc"></td>
	<tr>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<td height="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</tr>
	</form>
</table>


</body>
</html>
