<%@ page language="java" %>
<%@ page import = "com.sap.isa.auction.bean.*" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<jsp:useBean id="oppbeanforbid" scope="session" type="com.sap.isa.auction.actionforms.OpportunitiesForm"/>
<isa:contentType />

<html>

<head>

<title><isa:translate key="eAuctions.title"/></title>
</head>

<body bgcolor="#DFEEFF" link="#000080" vlink="#000080" alink="#FF9933" style="font-family: MS Sans Serif; font-size: 12pt">
<div class="module-name"><isa:moduleName name="auction/seller/bidproduct.jsp"/></div>
<form action='<isa:webappsURL name="auction/buyer/createresponse.do"/>' name = "oppbeanforbid" name="createResponseForm">
  <p>&nbsp;</p>
  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </p>

  <% try{
      OpportunityBean oppBean = (OpportunityBean) session.getAttribute("oppbeanforbid");
      java.util.HashMap MapObj = oppBean.getListings();
      System.out.println("The listing are" + oppBean.getListings().size());
      Object[] Map = MapObj.values().toArray();

   %>


  <table border="1" width="100%">
    <tr>
      <td width="100%" colspan="4"><B><isa:translate key="opportunity.heading"/></B></td>
    </tr>
    <tr>
      <td width="25%"><isa:translate key="opportunity.name"/></td>
      <td width="25%"><%jsp:getProperty name="oppbeanforbid" property="opportunityName"%></td>
      <td width="25%"><isa:translate key="opportunity.description"/></td>
      <td width="25%"><%jsp:getProperty name="oppbeanforbid" property="opportunityId"%></td>
      <td width="25%">&nbsp;</td>
    </tr>
    <tr>
      <td width="25%"><isa:translate key="opportunity.startprice"/></td>
      <td width="25%"><%= oppBean.getAttributes().getPropertyValue("startprice")%></td>
      <td width="25%"><isa:translate key="opportunity.enddate"/></td>
      <td width="25%"><%jsp:getProperty name="oppbeanforbid" property="endDate"%></td>
    </tr>
    <tr>
      <td width="25%"><isa:translate key="opportunity.currenthighbid"/></td>
      <td width="25">&nbsp;</td>
      <td width="25%"><isa:translate key="opportunity.currenthighestbidder"/></td>
      <td width="25">&nbsp;</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </p>

<table border="1" width="100%">
    <tr>
      <td width="100%" colspan="4"><B><isa:translate key="opportunity.productdetails.heading"/></B></td>
    </tr>
    <%
       for(int i=0; i<Map.length; i++) {
    %>
	    <tr>
		  <td width="25%"><isa:translate key="product.id"/></td>
		  <td ><%= ((ListingBean)Map[i]).getProductBean().getProductId()%></td>
		   <td width="25%"><isa:translate key="product.name"/></td>
		  <td ><%= ((ListingBean)Map[i]).getProductBean().getProductName()%></td>
	    </tr>
          <tr>
		  <td width="25%"><isa:translate key="product.description"/></td>
		  <td ><%= ((ListingBean)Map[i]).getProductBean().getDescription()%></td>
		  <td width="25%"><isa:translate key="product.country"/></td>
		  <td ><%= ((ListingBean)Map[i]).getProductBean().getCountry()%></td>
	    </tr>
          <tr>
	      <td width="25%"><isa:translate key="product.language"/></td>
	      <td ><%= ((ListingBean)Map[i]).getProductBean().getLanguage()%></td>
	    </tr>



    <%} }	catch(NullPointerException e){
	//out.println(e);
	}       %>
</table>
  <div align="center">
    <center>
    <table border="1" width="50%">
      <tr>
        <td width="50%"><isa:translate key="product.pricequote"/></td>
        <td width="50%">&nbsp; <input type="text" name="priceQuote" class="submitDoc" size="15"/></td>
      </tr>
      <tr>
        <td width="100%" colspan="2"><input type="submit" class="submitDoc" name="submit" value="<isa:translate key="eAuctions.seller.auction.button.submit"/>">
</td>
      </tr>
    </table>
    </center>
  </div>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
</form>

</body>

</html>
