<%@ page language="java" %>
<%@ page import  ="com.sap.isa.core.UserSessionData"%>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />


<%    if(UserSessionData.getUserSessionData(session)== null)  UserSessionData.createUserSessionData(session);
	String lang=request.getParameter("language");
	String userid =request.getParameter("userid");


%>


<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">
function getWebShops() {
   startForm.language.value ="<%=lang%>";
   startForm.userid.value="<%=userid%>";
   startForm.submit();
}
</script>
</head>
<body bgcolor="95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"
 onLoad = "getWebShops()">
<div class="module-name"><isa:moduleName name="auction/seller/eAuction.jsp"/></div>

<form name="startForm" action='<isa:webappsURL name="auction/seller/shoplist.do"/>' method="GET">
<input type="hidden" name=language />
<input type="hidden" name=userid />
<br>
</form>

</body>
</html>
