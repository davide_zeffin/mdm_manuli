<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*"%>
<%@ page import= "com.sap.isa.auction.*"%>
<%@ page import="com.sap.isa.auction.businessobject.*,com.sap.isa.core.*" %>


<%@ page import=" com.sap.isa.businessobject.webcatalog.*,com.sap.isa.catalog.webcatalog.*" %>

<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">

function details(externalId,screen) {
	document.openForm.Id.value = externalId;
	document.openForm.show.value = screen;
        openForm.submit();
}

 function getList(action) {
          document.monitorForm.status.value = action;
          monitorForm.submit();
 }



</script>

</head>

<body class="backgroundColor95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">

<div class="module-name"><isa:moduleName name="auction/seller/audetails.jsp"/></div>
<%@ include file="webAuctionHeader.html" %>

<%@ include file="shopChangeLink.jsp" %>
<br>
<table cellspacing="0" cellpadding="0" border="0">

      <tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/froff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>" class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/catsearch.jsp"/>">
		 <isa:translate  key="eAuctions.seller.create.title"/></a>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/offon.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;<isa:translate key="eAuctions.seller.monitor.title"/>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkon.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
      </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="780" class="backgroundColore8e3d7" height="500">
	<TR valign="top">
		<td class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
	</TR>
	<tr valign="top" height="10">
		<td class="backgroundColor95b1c1">&nbsp;</td>
		<td>&nbsp;&nbsp;</td>
		<td>
			<form  name="monitorForm" action='<isa:webappsURL name="auction/seller/getopportunitylist.do"/>'>
			<input type="hidden" name="status" />

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('open')"><isa:translate key="eAuctions.seller.open.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('published')"><isa:translate key="eAuctions.seller.published.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('activeopportunity')"><isa:translate key="eAuctions.seller.active.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('closed')"><isa:translate key="eAuctions.seller.closed.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('finalized')"><isa:translate key="eAuctions.seller.finalized.title"/></a>
			<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="200" height="1" border="0" alt=""><A href="<isa:webappsURL name="auction/seller/oppsearch.jsp"/>">
			<img src="<isa:mimeURL name="auction/images/layout/button_search.gif"  language=""/>" width="15" height="15" border="0" alt="">&nbsp;&nbsp;<isa:translate key="eAuctions.seller.search.title"/></a>

			</form>
		</td>
        </tr>
	<tr valign="top">
		<td class="backgroundColor95b1c1">&nbsp;</td>

		<td>&nbsp;</td>
		<td class="CONTENT">
                <table cellpadding="2" cellspacing="1" border="0" width="98%">
                    <tr>
                            <td class="SEC_TB_TD" width="100%">&nbsp;<isa:translate  key="eAuctions.seller.audetails.title"/>
                            </td>
                    </tr>
                </table>
		
		</td>
	</tr>

	<tr valign="top">
		<td class="backgroundColor95b1c1" valign="top"><img src="layout/sp.gif" width="1" height="1" border="0" alt=""></td>
		<!--td><img src="layout/sp.gif" width="1" height="" border="0" alt=""></td-->
		<td valign="top" colspan="2" align="left">
		<table cellpadding="0" cellspacing="0" class="content" width="100%" border="0">



			<%      HttpSession s = request.getSession();
                            OpportunityBean opbean = (OpportunityBean) s.getAttribute("oppbeanforbid");

                            HashMap tgroups = null;
                            Object[] targetgroups = null;
			    if(opbean!=null) {
				String tgs = (String) opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.TARGET_GROUPS);
				targetgroups = EAUtilities.getInstance().getTargetGroups(tgs,",");
			    }

			    String[]  tdescn=opbean.getTgDescns();

                            //pageContext.setAttribute("oppBean", opbean, PageContext.PAGE_SCOPE);
                            //pageContext.setAttribute("listingBean", lsBean, PageContext.PAGE_SCOPE);


                            // get the start and end date
                            String startDate = opbean.getStartDate()==null ? "" :
                                               EAUtilities.getInstance().timestampToString(opbean.getStartDate());

                            String endDate = opbean.getEndDate()==null ? "" :
                                               EAUtilities.getInstance().timestampToString(opbean.getEndDate());

                            String resExpDate = "";
                            // uncomment later
                            //String resExpDate = (java.sql.Timestamp)opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.EXPIRATION_DATE)==null ? "" :
                            //		   EAUtilities.getInstance().timestampToString((java.sql.Timestamp)opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.EXPIRATION_DATE));

                            String startprice = opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.START_PRICE)==null ?
                                   "" : ((opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.START_PRICE))).toString();
                            String reserveprice = opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.RESERVE_PRICE)==null ?
                                   "" : ((opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.RESERVE_PRICE))).toString();


                            String description = opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.OPP_DESCRIPTION)==null ?
                                   "" : (opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.OPP_DESCRIPTION)).toString();

                            String terms = opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.OPP_TERMS)==null ?
                                   "" : (opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.OPP_TERMS)).toString();




		          CatalogBusinessObjectManager catBom =
			                      (CatalogBusinessObjectManager)userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

                            WebCatItemList items = null;
			    if(catBom != null) {
			    WebCatInfo theCatalog = catBom.getCatalog();
                            items = theCatalog.getCurrentItemList();
			      if (theCatalog !=null){



			    String areaID = "";//(String)(lsBean.getAttributes().getPropertyValue(DynamicAttributeConstant.PRODUCT_AREA_GUID));
                            String itemID = "";//(String)(lsBean.getAttributes().getPropertyValue(DynamicAttributeConstant.PRODUCT_GUID));

			    }
			    }







                           // String currency = bom.getShop().getCurrency()==null?"" : bom.getShop().getCurrency();


			%>




				<tr>
					<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auction.name"/></td>
					<td><img src="layout/sp.gif" width="5" height="1" border="0" alt=""></td>
					<td class="CONTENT"><%=opbean.getOpportunityName()%></td>
				</tr>
				<tr>
					<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.startprice"/></td>
					<td><img src="layout/sp.gif" width="5" height="1" border="0" alt=""></td>
					<td class="CONTENT"><%=startprice%> &nbsp;&nbsp;<%=currency%></td>
				</tr><br>
				<tr>
					<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.reserveprice"/></td>
					<td><img src="layout/sp.gif" width="5" height="1" border="0" alt=""></td>
					<td class="CONTENT"><%=reserveprice%> &nbsp;&nbsp;<%=currency%></td>
				</tr>
				<tr>
					<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.startdate"/></td>
					<td><img src="layout/sp.gif" width="5" height="1" border="0" alt=""></td>
					<td class="CONTENT"><%=startDate%></td>
				</tr>
				<tr>
					<td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.enddate"/></td>
					<td><img src="layout/sp.gif" width="5" height="1" border="0" alt=""></td>
					<td class="CONTENT"><%=endDate%></td>
				</tr>
				<tr>
					<td align="right" class="CONTENT">&nbsp;&nbsp;&nbsp;&nbsp<isa:translate key="eAuctions.seller.auction.termsandconds"/></td>
					<td><img src="layout/sp.gif" width="5" height="1" border="0" alt=""></td>
					<td class="CONTENT"><%=terms%></td>
				</tr>
				<tr>
					<td><img src="layout/sp.gif" width="1" height="10" border="0" alt=""></td>
				</tr>
				<tr>
					<td><img src="layout/sp.gif" width="1" height="10" border="0" alt=""></td>
				</tr>
        <tr valign="top">
            <td colspan="8" valign="top">
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
		<td><img src="layout/sp.gif" width="4" height="1" alt="" border="0"></td>
		<td>
		<table cellpadding="0" width="80%" cellspacing="0" border="0" CLASS="TBDATA_BDR_BG">
		<tr>
			<td>
			<table cellpadding="2" cellspacing="1" border="0">
			<tr>
				<td scope="col" class="CONTENTHEAD" nowrap width="1%">&nbsp;</td>
				<td scope="col" class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.description"/></td>
				<td scope="col" class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.productID"/></td>
				<td scope="col" class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.quantity"/></td>
				<td scope="col" class="CONTENTHEAD" nowrap><isa:translate key="eAuctions.seller.auction.unit"/></td>
				<td scope="col" class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.price"/></td>
				<td scope="col" class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.Picture"/></td>
			</tr>
<%
  WebCatItem item = null;
  java.util.HashMap listingMap = opbean.getListings();
  Object[] listingArr = listingMap.values().toArray();
  ListingBean listing = null;
  IDynamicAttribute attribute = null;
  for(int i = 0; i < listingArr.length; i++)  {
      listing = (ListingBean) listingArr[i];
      attribute = listing.getAttributes();
      if(attribute != null && items != null)  {
            item = items.getItem((String)attribute.getPropertyValue(DynamicAttributeConstant.PRODUCT_AREA_GUID) + (String)attribute.getPropertyValue(DynamicAttributeConstant.PRODUCT_GUID));
            if(item != null)  {
                item.setQuantity(Double.toString(listing.getQuantity()));
            }
      }
      if(item == null)
          continue;

%>
			<tr class="backgroundColoredeae2">
				<td scope="row" class="CONTENT" nowrap valign="top"><input type="checkbox" name="<%=com.sap.isa.catalog.actions.ActionConstants.RA_ITEMKEY%>checkbox" value="<%=item.getItemID()%>"></td>
				<td class="CONTENT" valign="top">
     <%String prodDesc = null;
        if (item.getAttribute("TEXT_0001") != null && !item.getAttribute("TEXT_0001").equals("")) {
 		prodDesc=item.getAttribute("TEXT_0001");
 	} else {
 		prodDesc=item.getAttribute("OBJECT_DESCRIPTION");
 	}
     %>

     <%= prodDesc %> &nbsp;

                                </td>
				<td class="CONTENT" nowrap valign="top">
<%= item.getAttribute("OBJECT_ID") %>&nbsp;
                                </td>
				<td class="CONTENTR" nowrap valign="top">
                                    <%=listing.getQuantity()%>
                                </td>
				<td class="CONTENT" nowrap valign="top"><%=item.getUnit()%></TD>
				<td class="CONTENTR" nowrap valign="top">
  <%if (item.getItemPrice().isScalePrice()) { %>
    <select name="price_combo_box" >
    <% java.util.Iterator pricesIterator = item.getItemPrice().getPrices();
       while (pricesIterator.hasNext()) {
          String st = (String) pricesIterator.next();
    %>
          <option value="lll"><%= st %></option>
    <% } %>
    </select>
  <% } else { %>
  <%= item.getItemPrice().getPrice()%>
  <% }  %>

                                </td>
				<td class="CONTENTC" nowrap valign="top">
    <%if (item.getAttribute("DOC_PC_CRM_THUMB") == null || item.getAttribute("DOC_PC_CRM_THUMB") == "") { %>
    <font color="ff0000" size="2">No Picture</font>
    <% } else { %>
    <img src="<%=item.getItemKey().getParentCatalog().getImageServer()+item.getAttribute("DOC_PC_CRM_THUMB") %>" alt="Thumbnail for product" width=60 height=60>
    <% } %>
                                </td>
			</tr>
<%
    }
%>
			</table>
		</td></tr></table>
        </td>
        </tr>

			</table>
		</td>
	</tr>

</table>


</form>
</body>

</html>
