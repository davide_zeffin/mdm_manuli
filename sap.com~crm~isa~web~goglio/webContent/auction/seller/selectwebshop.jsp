<%@ page language="java" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="java.util.Collection" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">
</script>

</head>

<body class="backgroundColor95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" >
<div class="module-name"><isa:moduleName name="auction/seller/selectwebshop.jsp"/></div>
<%@ include file="webAuctionHeader.html" %> 

<br>

<form  action='<isa:webappsURL name="auction/seller/getshop.do"/>'>

<table cellpadding="0" cellspacing="0" border="0">
 <tr>

		<Td class="CONTENT">&nbsp;&nbsp;&nbsp;<isa:translate key="eAuctions.shop.select.title"/>&nbsp;
                <select name="shopId">

            <%   ResultData shoplist = (ResultData) session.getAttribute("shoplist");
                 //System.out.println("Shop list is  **** " + shoplist);
                 shoplist.first();
                 while(!shoplist.isLast()) {

            %>
                   <option value="<%=shoplist.getRowKey()%>"><%=shoplist.getString(Shop.ID)%>
            <%
                       shoplist.next();
                 }
            %>
		</select>
                &nbsp;&nbsp;<input type="submit" name="go" value="<isa:translate key="eAuctions.seller.gobtn.value"/>"  class="submitDoc"></TD>
	</tr>
</table>
</form>
<br>
<table cellpadding="0" cellspacing="0" border="0" width="780" class="backgroundColore8e3d7" height="500">
	<TR>
		<td class="backgroundColor95b1c1" rowspan="7"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
	</TR>
	<tr>
		<td width="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="100%" height="1" border="0" alt=""></td>
	</tr>
	<tr>
		<td height="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</tr>
</table>



</body>

</html>
