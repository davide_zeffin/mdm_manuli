<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.*,com.sap.isa.auction.bean.*, com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page language="java" %>
<%@ page import="com.sap.isa.businessobject.*,com.sap.isa.core.*" %>
<%@ page import ="com.sap.isa.core.UserSessionData"%>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<html>

<HEAD><title><isa:translate key="error.jsp.title"/></title>


<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">


<SCRIPT>
function MM_goToURL() { //v3.0
  		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}


</SCRIPT>
</head>

<body bgcolor="95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/crmerror.jsp" /></div>
<form>


<%@ include file="webAuctionHeader.html" %> 


<br>

<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/fron.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;<isa:translate  key="eAuctions.seller.create.title"/>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/onoff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>"  class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/monitorOpportunity.do"/>">
			<isa:translate key="eAuctions.seller.monitor.title"/></a>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkoff.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="e8e3d7" height="500">
	<tr>
		<tD bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></td>
		<Td colspan="6" valign="top">


		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
		<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
		<td class="CONTENT" colspan="3" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="30" height="11" border="0" alt="">&nbsp;&nbsp;
		<font color=red><b><isa:translate key="error.jsp.head"/></b></font>&nbsp;&nbsp;
		<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="580" height="11" border="0" alt=""></td>
		</tr>
		<Tr>
		<Td>


		<table cellpadding="0" cellspacing="0" border="0" class="CONTENT">
		<tr>
			<Td rowspan="21"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="1" border="0" alt=""></TD>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td align="right"  class="CONTENT"><isa:translate key="b2b.error.communicationexception"/></td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td class="CONTENT"><A><isa:translate key="b2b.error.backenderror.friendly"/></A></td>
		</tr>
		<Tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
		</Tr>
		</table>


		</td>
		</tr>
		</table>


		</td>
	</tr>
</table>

</form>
</body>

</html>
