<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<isa:contentType />
<html>
<head><title><isa:translate key="error.jsp.title"/></title>
  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">
  <script type="text/javascript">
    function goBack() {
      history.back();
    }
  </script>
</head>
<body>
<div class="module-name"><isa:moduleName name="auction/seller/dpeerror.jsp"/></div>


  <font color=red size="3"><b> <isa:translate key="error.jsp.head"/> </b></font> <p>

  <isa:translate key="eAuctions.error.dpeerror"/>
  <input type="button" onclick="goBack()" class="submitDoc" value="<isa:translate key="error.jsp.back"/>">

</body>
</html>