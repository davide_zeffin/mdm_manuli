<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*"%>
<%@ page import= "com.sap.isa.auction.*,com.sap.isa.auction.util.*"%>
<%@ page import="com.sap.isa.businessobject.*,com.sap.isa.core.*" %>

<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>

<isa:contentType />

<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">


function details(externalId,screen) {
	document.openForm.Id.value = externalId;
	document.openForm.show.value = screen;
        openForm.submit();
}

 function getList(action) {
          document.monitorForm.status.value = action;
          monitorForm.submit();
 }

</script>

</head>

<body bgcolor="95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/activeopportunity.jsp"/></div>
<%@ include file="webAuctionHeader.html" %> 

<%@ include file="shopChangeLink.jsp" %> 
<br>
<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/froff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>" class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/catsearch.jsp"/>"><isa:translate key="eAuctions.seller.create.title"/></a>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/offon.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;<isa:translate key="eAuctions.seller.monitor.title"/>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkon.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="e8e3d7" height="500">
	<TR>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
	</TR>
	<tr>
		<td bgcolor="95b1c1">&nbsp;</td>
		<td>&nbsp;&nbsp;</td>
		<td>
			<form  name="monitorForm" action='<isa:webappsURL name="auction/seller/getopportunitylist.do"/>'>
			<input type="hidden" name="status" />

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('open')"><isa:translate key="eAuctions.seller.open.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('published')"><isa:translate key="eAuctions.seller.published.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('activeopportunity')"><isa:translate key="eAuctions.seller.active.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('closed')"><isa:translate key="eAuctions.seller.closed.title"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:onClick=getList('finalized')"><isa:translate key="eAuctions.seller.finalized.title"/></a>
			<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="200" height="1" border="0" alt=""><A href="<isa:webappsURL name="auction/seller/oppsearch.jsp"/>">
			<img src="<isa:mimeURL name="auction/images/layout/button_search.gif"  language=""/>" width="15" height="15" border="0" alt="">&nbsp;&nbsp;<isa:translate key="eAuctions.seller.search.title"/></a>
			</form>
		</td>
        </tr>

        <tr>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
			<td colspan="2"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="5" border="0" alt=""></td>
	</tr>
	<tr>
		<td bgcolor="95b1c1">&nbsp;</td>

		<td>&nbsp;</td>
		<td class="CONTENT"><img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="30" height="11" border="0" alt="">
			&nbsp;&nbsp;<isa:translate key="eAuctions.seller.activeauctions.title"/>&nbsp;&nbsp;<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="580" height="11"
			border="0" alt=""></td>
	</tr>


	<% HttpSession s = request.getSession();
					   Hashtable hsh = new Hashtable();
					   ReturnObject retobj = (ReturnObject) s.getAttribute("RObject");
					   if (retobj != null){
					       hsh = retobj.getBeanList();
					   }
					   if (hsh != null){ 
					    if(hsh.size()>0) {    %>
		
			<tr>
				<td bgcolor="95b1c1">&nbsp;</td>
				<td>&nbsp;</td>
				<Td align="center" class="content">&nbsp;&nbsp;<img src="<isa:mimeURL name="auction/images/layout/doubleback.gif"  language=""/>" width="12" height="12" border="0" alt="">
					<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="60" height="1" border="0" alt="">
					<img src="<isa:mimeURL name="auction/images/layout/back_gif.gif"  language=""/>" width="12" height="12" border="0" alt="">
					<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="60" height="1" border="0" alt="">
					1/1
					<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="60" height="1" border="0" alt="">
					<img src="<isa:mimeURL name="auction/images/layout/forward.gif"  language=""/>" width="12" height="12" border="0" alt="">
					<img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="60" height="1" border="0" alt="">
					<img src="<isa:mimeURL name="auction/images/layout/doubleforward.gif"  language=""/>" width="12" height="12" border="0" alt="">
				</TD>
			</tr>
			<%} %>
			
			<tr>
				<Td bgcolor="95b1c1"></TD>
				<td>&nbsp;</td>
				<Td align="center">
		
		        	<form  name="openForm" action='<isa:webappsURL name="auction/seller/opplist.do"/>'>
		
				   <input type=hidden name="Id" />
				   <input type=hidden name="show" />
		
				<table cellpadding="5" cellspacing="0" border="1" class="content" bgcolor="edeae2">
				
		
		
		
				
				      
				 <%     if(hsh.size()>0) { %>
				   
				   
				   <Tr>
				   				<td align="center" class="CONTENTLB"><isa:translate key="eAuctions.seller.auctions.auctionname"/></td>
				   				<td align="center" class="CONTENTLB"><isa:translate key="eAuctions.seller.auctions.startdate"/></td>
				   				<td align="center" class="CONTENTLB"><isa:translate key="eAuctions.seller.auctions.enddate"/></td>
				   				<td align="center" class="CONTENTLB"><isa:translate key="eAuctions.seller.auctions.startprice"/></td>
				   				<td align="center" class="CONTENTLB"><isa:translate key="eAuctions.seller.auctions.reserveprice"/></td>
				   				<td align="center" class="CONTENTLB"><isa:translate key="eAuctions.seller.auctions.bidhistory"/></td>
				</TR>
				   <% } %>
				   <% if(hsh.size()==0) { %>
				   <tr><td> <isa:translate key="eAuctions.seller.monitor.message.noauction"/> </td> </tr>
				   <% }%>
				   
				   
				   
				   <%
			   
			   
		   
		      Enumeration enum = hsh.keys();
		      Calendar cal = Calendar.getInstance();
		      while (enum.hasMoreElements()){
			Object key = enum.nextElement();
			String s1 = (String) key ;
			OpportunityBean opbean=(OpportunityBean) hsh.get(key);		
			
			// Get the shop details
			EAUtilities ea = EAUtilities.getInstance();
			String shp = ea.getShopFromCategoryPath((String) opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.CATEGORY_PATH));
			//System.out.println(" the shop from dpe =" + (String) opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.CATEGORY_PATH));
			//System.out.println(" the shop  =" + id);
			
			
			if(opbean.getStatus()==(IStatus.Active) && (shp!=null && shp.equals(id))) {
			    String starttemp = opbean.getStartDate().toString();
			    String startdate = starttemp.substring(0,4) + " / " + starttemp.substring(5,7) + " / " + starttemp.substring(8,10);

			    String endtemp = opbean.getEndDate().toString();
			    String enddate = endtemp.substring(0,4) + " / " + endtemp.substring(5,7) + " / " + endtemp.substring(8,10);

			    String startprice = opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.START_PRICE)==null ?
				   "" : ((opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.START_PRICE))).toString();
			    String reserveprice = opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.RESERVE_PRICE)==null ?
				   "" : ((opbean.getAttributes().getPropertyValue(DynamicAttributeConstant.RESERVE_PRICE))).toString();



		%>
		<tr>
			<!--<td><%=opbean.getOpportunityName()%></td> -->
			<td><A href="javascript:onClick=details(<%=opbean.getInternalOpportunityId()%>,'auctiondetails')">
						   <%=opbean.getOpportunityName()%></a></td>
			<Td><%=startdate%></TD>
			<Td><%=enddate%></TD>
			<td><%=currency%> <%=startprice%></td>
			<Td><%=currency%> <%=reserveprice%></TD>
			<td align="center"><A href="javascript:onClick=details(<%=opbean.getInternalOpportunityId()%>,'opportunitybidhistory')">
				<isa:translate key="eAuctions.seller.view.title"/></A></td>			
		</tr>
		<%}%>
		<% } }%>
	</form>
	</table>


	</td>
	</tr>
	<tr>
		<td bgcolor="95b1c1">&nbsp;</td>
		<td>&nbsp;</td>
		<td height="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</tr>
 </table>

</body>
</html>
