<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.*,com.sap.isa.auction.bean.*, com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page language="java" %>
<%@ page import="com.sap.isa.businessobject.*,com.sap.isa.core.*" %>
<%@ page import  ="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.catalog.webcatalog.*" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.catalog.boi.ICategory" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="com.sap.isa.auction.actionforms.OpportunityForm" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<!--jsp:useBean    id="currentItem"
                scope="session"
                type="com.sap.isa.catalog.webcatalog.WebCatItem"-->
<!--/jsp:useBean-->
<jsp:useBean id="createOpptyForm" scope="session"
class="com.sap.isa.auction.actionforms.CreateOpportunityForm" />


<html>

<%
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        Date gmtTime = new Date(cal.getTime().getTimezoneOffset()*60*1000+cal.getTime().getTime());
%>
<HEAD><TITLE><isa:translate key="eAuctions.title"/></TITLE>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">

<style>
	.calendarArea {
		border: 2px outset white;
	}
</style>
<script src="<isa:mimeURL name="auction/jscript/ourCalendarSeller.js"  language=""/>"
          type="text/javascript"></script>
<SCRIPT>

var currentdate = new  Date("<%=gmtTime.toString()%>");

function showOurCalendar(element)  {
	if(element.value == '')
		element.value = window.currentdate;
	setDateFieldFormatA(element,'false');
	element.value = '';
	var ourCal = document.all("calendarArea");
	//ourCal.innerHTML = calDocTop + calDocBottom;
	document.all("topSection").innerHTML = calDocTop;
	document.all("middleSection").innerHTML = calDocBottom;
	ourCal.style.pixelLeft = event.clientX;
	ourCal.style.pixelTop = event.clientY - 30;
	ourCal.style.display = "block";
	makeDateString(calDay);
}

function MM_goToURL() { //v3.0
  		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function fillProduct(a,b,c,d,e) {
		document.createOpptyForm.productName.value=a;
		document.createOpptyForm.productCatalog.value=d;
		document.createOpptyForm.productId.value=b;
		document.createOpptyForm.productDesc.value=c;
		document.createOpptyForm.productUOM.value=e;

}

function goToCatalog()  {
    window.location = "<isa:webappsURL name="auction/seller/catalog/getCatalog.do" />";
}

function removeItems()  {
    document.createOpptyForm.<%=com.sap.isa.auction.actions.ActionConstants.CANCELORREMOVE%>.value = "<%=com.sap.isa.auction.actions.ActionConstants.REMOVEOPERATION%>";
}

function cancelCreation()  {
        document.createOpptyForm.<%=com.sap.isa.auction.actions.ActionConstants.CANCELORREMOVE%>.value = "<%=com.sap.isa.auction.actions.ActionConstants.CANCELOPERATION%>";
}

function fillTargetInfo(targetgroups)
	{
		classTG.close();
		document.createOpptyForm.targetGroup.value=targetgroups;

	}

function setHiddenFields(hiddenValue)
{
	createOpptyForm.clickType.value=hiddenValue;


}

function checkfields(which){
   if ( createOpptyForm.clickType.value!="SUBMIT"){
       return true;
	}


if ( createOpptyForm.opptyName.value==""){
	alert("<isa:translate key="eAuctions.seller.validation.message.oppname"/>");
	createOpptyForm.opptyName.focus();
	return false;
	}


if ( createOpptyForm.quantity.value<=0){
	alert("<isa:translate key="eAuctions.seller.validation.message.qty"/>");
	createOpptyForm.quantity.focus();
	return false;
	}

if(createOpptyForm.startPrice.value==""){
    alert("<isa:translate key="eAuctions.seller.validation.message.startprice"/>");
    createOpptyForm.startPrice.focus();
    return false;
    }
/*if(createOpptyForm.reservePrice.value==""){
    alert("<isa:translate key="eAuctions.seller.validation.message.reserveprice"/>");
    return false;
    } */

var a = createOpptyForm.startPrice.value;
var b= createOpptyForm.reservePrice.value;
if(b=="")
  b=0;
if(b<0){
  alert("enter positive");
  return false;
  }

if(b>0) {
 var n1= parseFloat(b);
 var n2 =parseFloat(a);
if(n1 <= n2 ){
   alert("<isa:translate key="eAuctions.seller.validation.message.reservepriceless"/>");
   createOpptyForm.reservePrice.focus();
   return false;
   }

 }



if(createOpptyForm.startDate.value==""){
    alert("<isa:translate key="eAuctions.seller.validation.message.startdate"/>");
    createOpptyForm.startDate.focus();
    return false;
    }
if(createOpptyForm.endDate.value==""){
    alert("<isa:translate key="eAuctions.seller.validation.message.enddate"/>");
    createOpptyForm.endDate.focus();
    return false;
    }
if(createOpptyForm.resExpDate.value==""){
   alert("<isa:translate key="eAuctions.seller.validation.message.resExpdate"/>");
    createOpptyForm.resExpDate.focus();
    return false;
    }
/*if (createOpptyForm.quantity.value<=createOpptyForm.availableQty.value){
	alert("<isa:translate key="eAuctions.seller.validation.message.auqty"/>");
	createOpptyForm.quantity.focus();
	return false;
	} */



var startdate= new Date(createOpptyForm.startDate.value);
var tempDate=startdate;
    tempDate.setSeconds(tempDate.getSeconds()+500);
if (tempDate < currentdate){
    alert("<isa:translate key="eAuctions.seller.validation.message.startdateold"/>");
    return false;
  }
var enddate= new Date(createOpptyForm.endDate.value);
 if (enddate < startdate){
	alert("<isa:translate key="eAuctions.seller.validation.message.enddateold"/>");
	return false;
   }
var resexpdate= new Date(createOpptyForm.resExpDate.value);
 if (resexpdate < enddate) {
     alert("<isa:translate key="eAuctions.seller.validation.message.resExpdateold"/>");
	return false;
  }
  return true;
}




</SCRIPT>


<META content="MSHTML 5.00.2314.1000" name=GENERATOR></HEAD>
<BODY class="backgroundColor95b1c1" bottomMargin=0 leftMargin=0 topMargin=0 marginwidth="0"
marginheight="0">
<div class="module-name"><isa:moduleName name="auction/seller/createAuction.jsp" /></div>
<form action='<isa:webappsURL name="auction/seller/createoppty.do"/>' name="createOpptyForm" method="POST"
            onsubmit="return checkfields(this)">
            <input type="hidden" name="clickType" />
            <div style="display:none;">
            <input type="text" name="<%=com.sap.isa.auction.actions.ActionConstants.CANCELORREMOVE%>" value="">
            </div>


<%@ include file="webAuctionHeader.html" %>

<%@ include file="shopChangeLink.jsp" %>

<br>

<TABLE border=0 cellPadding=0 cellSpacing=0>
  <TBODY>
  <TR>
    <TD colSpan=3 vAlign=top width=820>
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR>
          <TD><IMG alt="" border=0 height=1 src="<isa:mimeURL name="auction/images/layout/sp.gif"
             language=""/>"  width=20></TD>

<%  if(createOpptyForm.getMode().equals(OpportunityForm.NEW))  {
%>
          <TD width=28><IMG alt="" border=0 height=19
            src="<isa:mimeURL name="auction/images/layout/fron.gif"  language=""/>"  width=20></TD>
          <TD background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class=TABB noWrap
          width="5%">&nbsp;<isa:translate key="eAuctions.seller.create.title"/>&nbsp;</TD>
          <TD width=24><IMG alt="" border=0 height=19
            src="<isa:mimeURL name="auction/images/layout/onoff.gif"  language=""/>"  width=20></TD>
<%  }  else  {
%>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/froff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>" class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/catsearch.jsp"/>"><isa:translate key="eAuctions.seller.create.title"/></a>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/offoff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
<%  }
%>
          <TD background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>"  class=TAB noWrap
          width="5%">&nbsp;<A href="<isa:webappsURL name="auction/seller/monitorOpportunity.do"/>"><isa:translate key="eAuctions.seller.monitor.title"/></A>&nbsp;&nbsp;</TD>
          <TD width=14><IMG alt="" border=0 height=19
            src="<isa:mimeURL name="auction/images/layout/bkoff.gif"  language=""/>"  width=12></TD>
          <TD vAlign=top width="100%"><IMG height=1
            src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>"
  width=1></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<table cellpadding="0" cellspacing="0" border="0" width="780" class="backgroundColore8e3d7" height="550">
        <tr valign="top">
            <tD class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
            <td colspan=2 align="center">
                <table cellpadding="2" cellspacing="1" border="0" width="98%">
                    <tr>
                            <td class="SEC_TB_TD" width="100%">&nbsp;<isa:translate key="eAuction.seller.auctiondetail"/></td>
                    </tr>
                </table>
              </td>
        </tr>
        <TR height="70">
          <tD class="backgroundColor95b1c1" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
          <TD>
            <TABLE border=0 cellPadding=0 cellSpacing=0 class="CONTENT">
		<tr>
			<td nowrap ALIGN="LEFT" class="CONTENTXXS" colspan="3"> *&nbsp;<isa:translate key="eAuctions.seller.auction.requiredfield.title"/></td>
		</tr>
                <TR>
                <TD align=right><isa:translate key="eAuctions.seller.auction.name"/> * </TD>
                <TD><IMG alt="" border=0 height=1
                  src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>"  width=10></TD>
                <td><input type="text" name="opptyName" class="submitDoc" value="<jsp:getProperty name="createOpptyForm"  property="opptyName"/>" size="15"/></td></TR>
              <TR>
                <TD align=right vAlign=top><isa:translate key="eAuctions.seller.auction.desc"/>&nbsp;&nbsp;&nbsp;</TD>
                <TD></TD>
              <Td><textarea name="description" class="submitDoc" cols="20" rows="5"><jsp:getProperty name="createOpptyForm"  property="description"/></textarea></TD></TR>
		</tr>
              <TR>
                <TD align=right><isa:translate key="eAuctions.seller.auctions.startprice"/> *</TD>
                <TD></TD>
                <Td><input type="text" name="startPrice" class="submitDoc" value="<jsp:getProperty name="createOpptyForm"  property="startPrice"/>" size="10" />&nbsp;<%=currency%></TD></TR>
              <TR>
                <TD align=right><isa:translate key="eAuctions.seller.auctions.bidIncrement"/></TD>
                <TD></TD>
                <Td><input type="text" name="bidIncrement" class="submitDoc" value="<jsp:getProperty name="createOpptyForm"  property="startPrice"/>" size="10" /></TD></TR>
              <TR>
                <TD align=right><isa:translate key="eAuctions.seller.auctions.reserveprice"/>&nbsp;&nbsp;&nbsp;</TD>
                <TD></TD>
                <Td><input type="text" name="reservePrice" class="submitDoc" value="<jsp:getProperty name="createOpptyForm"  property="reservePrice"/>"size="10" />&nbsp;<%=currency%></TD> </TR>
              </table>
      </td>


    <td>
<!-- jsp code C-->
    <table cellpadding="0" cellspacing="0" border="0" class="CONTENT">
	<tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>"  width="10" height="1" border="0" alt=""></td>
		<Td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>"  width="1" height="10" border="0" alt=""></TD>
	</tr>
        <TR>
                <TD align=right><isa:translate key="eAuctions.seller.auctions.startdate"/> * </TD>
                <TD></TD>

		<Td><input type="text" name="startDate" class="submitDoc" value="<jsp:getProperty name="createOpptyForm"  property="startDate"/>" size="15" onFocus="javascript:blur()"/>	<A HREF="javascript:doNothing()"
		                onClick="javascript:showOurCalendar(createOpptyForm.startDate);">
		                <img src="<isa:mimeURL name="auction/images/layout/calendar.gif"  language=""/>"  alt="Calendar" title="Calendar" border="0">
                </A><font size=1><isa:translate key="eAuctions.seller.auctions.calendar"/></font>
                </TD>
        </TR>
        <TR>
                <TD align=right><isa:translate key="eAuctions.seller.auctions.enddate"/> * </TD>
                <TD></TD>
                <Td><input type="text" name="endDate" class="submitDoc" value="<jsp:getProperty name="createOpptyForm"  property="endDate"/>" size="15" onFocus="javascript:blur()" />  <A HREF="javascript:doNothing()"
		                onClick="javascript:showOurCalendar(createOpptyForm.endDate);">
		                <img src="<isa:mimeURL name="auction/images/layout/calendar.gif"  language=""/>"  alt="Calendar" title="Calendar" border="0">
                </A><font size=1><isa:translate key="eAuctions.seller.auctions.calendar"/></font>
                </TD>
		    </TR>
		    <TR>
                <TD align=right nowrap><isa:translate key="eAuctions.seller.auction.resvexprdate"/> *</TD>
                <TD></TD>
                <Td><input type="text" name="resExpDate" class="submitDoc" value="<jsp:getProperty name="createOpptyForm"  property="resExpDate"/>" size="15" onFocus="javascript:blur()" />  <A HREF="javascript:doNothing()"
		                onClick="showOurCalendar(createOpptyForm.resExpDate);">
		                <img src="<isa:mimeURL name="auction/images/layout/calendar.gif"  language=""/>"  alt="Calendar" title="Calendar" border="0">
                </A><font size=1><isa:translate key="eAuctions.seller.auctions.calendar"/></font>
                </TD>
	</TR>
        <TR>
                <TD align=right nowrap><isa:translate key="eAuctions.seller.auction.termsandconds"/>&nbsp;&nbsp;&nbsp;</TD>
                <TD></TD>
                <TD><textarea name="terms"  class="submitDoc" cols="20"  rows="5" value=""><jsp:getProperty name="createOpptyForm"  property="terms"/></textarea></TD></TR>
        <tr>
                <td align="right" nowrap  class="CONTENT"><isa:translate key="eAuctions.seller.auction.email"/></td>
                <td>&nbsp;</td>
                <Td class="CONTENT"><input type="checkbox" name="sendEMail" class="submitDoc" value="sendEMail"/><isa:translate key="eAuctions.seller.auction.invitation"/><br></td>
        </tr>
        </table>
    </td>
</tr>
<tr>
    <tD class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
        <td colspan="2" align="right">
        <input type="submit" name="targetGroups" class="submitDoc" value="<isa:translate key="eAuctions.seller.auction.targetGroup"/>"/>&nbsp;&nbsp;
        </td>
</tr>
<tr valign="top">
    <tD class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
    <td colspan=2 align="center">
        <table cellpadding="2" cellspacing="1" border="0" width="98%">
            <tr>
                    <td class="SEC_TB_TD" width="100%">&nbsp;<isa:translate key="eAuctions.seller.auction.itemlist"/></td>
            </tr>
        </table>
      </td>
</tr>
<tr valign="top">
    <tD class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
    <td colspan="2">
        <img src="layout/sp.gif" width="4" height="1" alt="" border="0">
    </td>
</tr>
<tr valign="top">

<!-- Start of Product Information -->
        <tD class="backgroundColor95b1c1" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="2" height="1" border="0" alt=""></td>
        <td colspan="2" align="center" valign="top">
                &nbsp;&nbsp;&nbsp;
		<table cellpadding="4" width="80%" cellspacing="1" border="0" CLASS="TBDATA_BDR_BG">
			<!--table cellpadding="2" cellspacing="1" border="0"-->
			<tr>
				<td class="CONTENTHEAD" nowrap width="1%">&nbsp;</td>
				<td class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.description"/></td>
				<td class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.productID"/></td>
				<td class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.quantity"/></td>
				<td class="CONTENTHEAD" nowrap><isa:translate key="eAuctions.seller.auction.unit"/></td>
				<td class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.price"/></td>
				<td class="CONTENTHEAD" nowrap><isa:translate key="catalog.isa.Picture"/></td>
			</tr>
<%
  WebCatItem item = null;
  java.util.Collection collection = createOpptyForm.getLineItems().values();
  java.util.Iterator iterator = collection.iterator();
  while(iterator.hasNext())  {
      item = (WebCatItem) iterator.next();

%>
			<tr bgColor="#edeae2">
				<td scope="row" class="CONTENT" nowrap valign="top"><input type="checkbox" name="<%=com.sap.isa.catalog.actions.ActionConstants.RA_ITEMKEY%>checkbox" value="<%=item.getItemID()%>"></td>
				<td class="CONTENT" valign="top">
     <%String prodDesc = null;
        if (item.getAttribute("TEXT_0001") != null && !item.getAttribute("TEXT_0001").equals("")) {
 		prodDesc=item.getAttribute("TEXT_0001");
 	} else {
 		prodDesc=item.getAttribute("OBJECT_DESCRIPTION");
 	}
     %>

     <%= prodDesc %> &nbsp;

                                </td>
				<td class="CONTENTR" nowrap valign="top">
<%= item.getAttribute("OBJECT_ID") %>&nbsp;
                                </td>
				<td class="CONTENT" nowrap valign="top">
                                <input size="4" class="submitDoc" type="text" value="<%=item.getQuantity()%>" name="<%="Quantity_"+item.getItemID()%>"></input>
                                </td>
				<td class="CONTENTR" nowrap valign="top"><%=item.getUnit()%></TD>
				<td class="CONTENTR" nowrap valign="top">
  <%if (item.getItemPrice().isScalePrice()) { %>
    <select name="price_combo_box" >
    <% java.util.Iterator pricesIterator = item.getItemPrice().getPrices();
       while (pricesIterator.hasNext()) {
          String st = (String) pricesIterator.next();
    %>
          <option value="lll"><%= st %></option>
    <% } %>
    </select>
  <% } else { %>
  <%= item.getItemPrice().getPrice()%>
  <% }  %>

                                </td>
				<td class="CONTENTC" nowrap valign="top">
    <%if (item.getAttribute("DOC_PC_CRM_THUMB") == null || item.getAttribute("DOC_PC_CRM_THUMB") == "") { %>
    <font color="ff0000" size="2">No Picture</font>
    <% } else { %>
    <img src="<%=item.getItemKey().getParentCatalog().getImageServer()+item.getAttribute("DOC_PC_CRM_THUMB") %>" alt="Thumbnail for product" width=80 height=70>
    <% } %>
                                </td>
			</tr>
<%
    }
%>
			<tr class="CONTENTL">
				<td colspan="8" class="backgroundColoredeae2" align="right"><input type="button" class="submitDoc" onclick="goToCatalog();" style="font-size:xx-small;height:20;width:60;" value="<isa:translate key="eAuctions.seller.auction.button.addItem"/>" name="">&nbsp;
                                <input type="submit" onclick="setHiddenFields('remove');removeItems();" class="submitDoc" style="font-size:xx-small;height:20;width:75;" value="<isa:translate key="eAuctions.seller.auction.button.remove"/>" name=""></td>
			</tr>

                </table>
<!-- End of Product Information  -->
</td>
</tr>
<tr valign="top">
                <tD class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                <TD colspan="2" align="left">
                <img src="<isa:mimeURL name="auction/images/layout/layout/sp.gif"  language=""/>"  width="1" height="20" border="0" alt=""></img>
                  <input type="submit" class="submitDoc" name="submit"
                  value="<isa:translate key="eAuctions.seller.auction.button.submit"/>"onclick='setHiddenFields("SUBMIT")'>
                <input type="submit" name="cancel" value="<isa:translate key="eAuctions.seller.auction.button.cancel"/>"
                onClick="setHiddenFields('cancel');cancelCreation();" class="submitDoc"></TD>
</TR>
<tr valign="top">
    <tD colspan="3" class="backgroundColor95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
</tr>
</table>

</form>

<div id="calendarArea" class="calendarArea" style="background-color:#e8e3d7;display:none;position:absolute;width:200;height:60;left:200;top;300;">
	<br>
	<table width="100%">
	<tr>
	  <td>
		<div id="topSection">
		</div>
	  </td>
	</tr>
	<tr>
	  <td>
		<div id="middleSection">
		</div>
	  </td>
	</tr>
	</table>
</div>
</body>
<script>



	document.createOpptyForm.productId.style.borderStyle='none';
	document.createOpptyForm.productId.style.backgroundColor='e8e3d7';

	document.createOpptyForm.productName.style.borderStyle='none';
	document.createOpptyForm.productName.style.backgroundColor='e8e3d7';

	document.createOpptyForm.productDesc.style.borderStyle='none';

	document.createOpptyForm.availableQty.style.borderStyle='none';
	document.createOpptyForm.availableQty.style.backgroundColor='e8e3d7';

	document.createOpptyForm.productUOM.style.borderStyle='none';
	document.createOpptyForm.productUOM.style.backgroundColor='e8e3d7';

	document.createOpptyForm.productPrice.style.borderStyle='none';
	document.createOpptyForm.productPrice.style.backgroundColor='e8e3d7';
</script>
</html>
