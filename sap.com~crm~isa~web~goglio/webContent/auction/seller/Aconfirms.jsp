<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.*,com.sap.isa.auction.bean.*, com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page import= "com.sap.isa.auction.actionforms.CreateOpportunityForm"%>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<jsp:useBean id="createOpptyForm" scope="session" type="com.sap.isa.auction.actionforms.CreateOpportunityForm"/>
<html>
<head>
<title><isa:translate key="eAuctions.title"/></title>

<link href="<isa:mimeURL name="auction/css/main.css"  language=""/>"
          type="text/css" rel="stylesheet">
<script language="JavaScript">

function MM_goToURL() { //v3.0
  		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
</script>

</head>

<body bgcolor="95b1c1" bottommargin="0" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div class="module-name"><isa:moduleName name="auction/seller/Aconfirms.jsp" /></div>

<%@ include file="webAuctionHeader.html" %> 
<% String currency = (String) session.getAttribute("Currency");%>

<br>

<table cellspacing="0" cellpadding="0" border="0">

	<tr>
      <td width="820" colspan="3" valign="top">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="20" height="1" border="0" alt=""></td>
		<td width="28"><img src="<isa:mimeURL name="auction/images/layout/fron.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backon.gif"  language=""/>" class="TABB" width="5%" nowrap>&nbsp;<isa:translate key="eAuctions.seller.create.title"/>&nbsp;</td>
		<td width="24"><img src="<isa:mimeURL name="auction/images/layout/onoff.gif"  language=""/>" width="20" height="19" border="0" alt=""></td>
		<td background="<isa:mimeURL name="auction/images/layout/backoff.gif"  language=""/>" class="TAB" width="5%" nowrap>&nbsp;<a href="<isa:webappsURL name="auction/seller/monitorOpportunity.do"/>">
		<isa:translate key="eAuctions.seller.monitor.title"/></a>&nbsp;&#160;</td>
		<td width="14"><img src="<isa:mimeURL name="auction/images/layout/bkoff.gif"  language=""/>" width="12" height="19" border="0" alt=""></td>
		<td width="100%" valign="top"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1"></td>
		</tr>
		</table>
	  </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="e8e3d7" height="500">
	<TR>
		<td bgcolor="95b1c1"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="10" height="0" border="0" alt=""></td>
		<td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
	</TR>
	<tr>
		<td bgcolor="95b1c1">&nbsp;</td>
		<td class="CONTENT">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<isa:mimeURL name="auction/images/layout/pixblack.gif"  language=""/>" width="30"
			height="11" border="0" alt="">&nbsp;&nbsp;<isa:translate key="eAuctions.seller.monitor.title"/>&nbsp;&nbsp;<img src="../images/layout/pixblack.gif" width="600" height="11" border="0" alt=""></td>
	</tr>
	<tr>
		<Td bgcolor="95b1c1">&nbsp;</TD>
		<td valign="top">
		<table cellpadding="0" cellspacing="0" class="content" border="0">
                    <TR>
                        <%Status status=(Status)session.getAttribute("status");
                          if(status.getStatus()!=1) {%>
                            <td colspan="3" class="CONTENT">&nbsp;&nbsp;&nbsp;&nbsp;<isa:translate key="eAuctions.seller.acknowledge.message.failure"/></td>
                          <%}else {%>
                            <td colspan="3" class="CONTENT">&nbsp;&nbsp;&nbsp;&nbsp;<isa:translate key="eAuctions.seller.acknowledge.message.success"/></td>
                          <%}%>

                    </TR>
                    <tr>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
                    </tr>
                    <!--<tr>    <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.auctionid"/></td>
		             <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
		             <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="intOppId"/></td>
                    </tr> -->
                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auction.name"/></td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="opptyName"/></td>
                    </tr>
                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.product.id"/></td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="productId"/></td>
                    </tr>
                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.product.name"/></td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="productName"/></td>
                    </tr>
                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.product.desc"/></td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="productDesc"/></td>
                    </tr>

                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.product.quantity"/></td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="quantity"/></td>
                    </tr>
                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auction.desc"/></td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="description"/></td>
                    </tr><br>

                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.startprice"/>:</td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="startPrice"/>&nbsp;
                            <%=currency%></td>
                    </tr><br>
                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.reserveprice"/>:</td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="reservePrice"/>&nbsp;
                            <%=currency%></td>
                    </tr>
                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.startdate"/>:</td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="startDate"/></td>
                    </tr>
                    <tr>
                            <td align="right" class="CONTENT"><isa:translate key="eAuctions.seller.auctions.enddate"/>:</td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="endDate"/></td>
                    </tr>
                    <tr>
                            <td align="right" class="CONTENT">&nbsp;&nbsp;&nbsp;&nbsp<isa:translate key="eAuctions.seller.auction.termsandconds"/></td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="terms"/></td>
                    </tr>
                    <tr>
                            <td align="right" valign="top" class="CONTENT"><isa:translate key="eAuctions.seller.auction.targetGroup"/></td>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="5" height="1" border="0" alt=""></td>
                            <td class="CONTENT"><jsp:getProperty name="createOpptyForm" property="targetGroupDescn"/></td>
                    </tr>
                    <tr>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
                    </tr>
                    <!-- For sapphire.. changed the fwd from eAuction.jsp -->
                    <tr>
                            <Td align="right"><input type="submit" name="cancel" value="<isa:translate key="eAuctions.seller.button.continue"/>"
                            onClick="MM_goToURL('parent','<isa:webappsURL name="auction/seller/monitorOpportunity.do"/>');return document.MM_returnValue" class="submitDoc"></TD>
                    </tr>
                    <tr>
                            <td><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="10" border="0" alt=""></td>
                    </tr>
                </table>
            </td>
	</tr>


        <tr>
            <td bgcolor="95b1c1">&nbsp;</td>
            <td height="100%"><img src="<isa:mimeURL name="auction/images/layout/sp.gif"  language=""/>" width="1" height="1" border="0" alt=""></td>
	</tr>
    </table>
<% //CreateOpportunityForm createOpptyForm1 = new CreateOpportunityForm();
    session.removeAttribute("createOpptyForm"); // createOpptyForm1);
    session.removeAttribute("selected");%>
</body>

</html>
