<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.UIConstants" %>
<%@ page import="com.sap.isa.auction.util.FormUtility" %>
<%@ page import="com.sap.isa.ui.controllers.ActionForward" %>
<%@ page import="com.sap.isa.ui.controllers.ActionForm" %>
<%@ page import="java.util.Enumeration" %>
<%@ taglib uri="/isa" prefix="isa" %>
<html>
<head>
    <isa:includes/>
</head>
<body class="organizer" left-margin="2">
<%
String loadingStr = WebUtil.translate(pageContext , UIConstants.PAGELOADING , null);

%>
<div id='_our_loading_main' ><%=loadingStr%><img width='16' height='16'
      src='<%=WebUtil.getMimeURL(pageContext , "/auction/mimes/images/clockon.gif")%>'>
      </img>
</div>

<%
    String page1 = (String)pageContext.getRequest().getAttribute(ActionForward.FORWARD);
    if(page1 != null)  {
%>
<jsp:include page="<%=page1%>" flush="true"/>
<script language='JavaScript'>
eval("document.getElementById('_our_loading_main').style.display = 'none'");</script>

<%  }  else  {

    page1 = pageContext.getRequest().getParameter("_loadPage");
    page1 = WebUtil.getAppsURL(pageContext , Boolean.FALSE , page1 , null , null , false);
    Enumeration enums = pageContext.getRequest().getParameterNames();
    StringBuffer url = new StringBuffer(page1);
    String param = "";
    while(enums.hasMoreElements())  {
        param = (String)enums.nextElement();
        if(!param.equals("_loadPage"))  {
            url.append("&");
            url.append(param);
            url.append("=");
            url.append(pageContext.getRequest().getParameter(param));
        }
    }
    page1 = url.toString();
%>

<form method="post" id="form1" name="form1" action="<%=page1%>">
</form>
<script language='JavaScript'>
    document.form1.submit();
</script>
<%
  }
%>
</body>

<style>
     body {margin: 1px}
</style>
</html>
