<%--
********************************************************************************
    File:         auction_entry_fremail.jsp
    Created:      06/11/2001
    This is the jsp page which will be called after an user clicked the link from
    the email and log on to the isa site

********************************************************************************
--%>
<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <title>SAP Internet Sales B2B</title>

  <script type="text/javascript">
  <!--
    function resize(direction, frameObj) {
      if (direction == "min") {
        if (frameObj == "organizer")
          window.secondFS.cols = '0,15,*';
      }
      else {
        if (frameObj == "organizer")
          window.secondFS.cols = '25%,15,*';
      }
    }
  //-->
  </script>

  </head>
  <frameset rows="85,*" id="mainFS" border="1" frameborder="1" framespacing="0">
    <frame name="header" src="<isa:webappsURL name="/b2b/header.do"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
    <frameset cols="25%,15,*" id="secondFS">
      <frameset rows="53,*" id="thirdFS">
        <frame name="organizer_nav" src="<isa:webappsURL name="/b2b/organizer-nav-doc-search.jsp"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
        <frame name="organizer_content" src="<isa:webappsURL name="/auction/bidder/showParticipation.do"/>" frameborder="0" scrolling="auto" marginwidth="0" marginheight="0">
      </frameset>
      <frame name="closer_organizer" src="<isa:webappsURL name="/b2b/closer_organizer.jsp"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
      <frame name="work_history" src=""/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
    </frameset>
    <noframes>
      <body></body>
    </noframes>
  </frameset>
</html>