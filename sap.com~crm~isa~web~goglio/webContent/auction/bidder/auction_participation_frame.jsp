<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<html>
    <head>
      <title><bean:message key="eAuctions.buyer.b2b.participation.title"/></title>
      <script src="<isa:mimeURL name="b2b/jscript/frames.js"  language=""/>"
          type="text/javascript"></script>

      <SCRIPT language=JavaScript>
            function replaceISAFrames() {
                organizer_content().location.href="<isa:webappsURL name="/b2b/auctionsearch.do"/>";
                organizer_nav().location.href="<isa:webappsURL name="b2b/organizer-nav-doc-search.jsp"/>";
                form_input().location.href="<isa:webappsURL name="/b2b/welcome.jsp"/>";
             }
     </SCRIPT>
   </head>
   <body onLoad="replaceISAFrames()">
   <div class="module-name"><isa:moduleName name="auction/bidder/auction_participation_frame.jsp"/></div>
   
   </body>
</html>