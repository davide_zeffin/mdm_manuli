<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page language="java" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<jsp:useBean id="oppform" scope="session" type="com.sap.isa.auction.actionforms.OpportunitiesForm"/>
<html>
  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctiondetails.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">
    <script src="<isa:mimeURL name="b2b/jscript/reload.js"  language=""/>"
          type="text/javascript"></script>
    <script src="<isa:mimeURL name="b2b/jscript/openwin.js"  language=""/>"
          type="text/javascript"></script>

    <script>
    function openAuctionDetails(){
        classPrd = window.open("<isa:webappsURL name="auction/bidder/auction_details_termDesc.jsp"/>", "Start", "status,width=500,height=600,scrollbars");
        which = "start";
    }
    </script>

  </head>

  <body class="organizer">
    <div class="module-name"><isa:moduleName name="auction/bidder/checkout.jsp"/></div>
    <div id="new-doc" class="module">
      <div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.subtitle"/></div>
       <form  action='<isa:webappsURL name="/b2b/checkout.do?opportunity_id=<%=oppform.getOppId()%>"/>'>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td colspan="2"><h2><%=oppform.getOppName()%></h2></td>
              <td style="vertical-align: middle;">
                <table width="80%" border="1">
                  <tbody>
                    <tr>
                        <td><isa:translate key="eAuctions.buyer.b2b.prodNum"/></td>
                        <td><isa:translate key="eAuctions.buyer.b2b.prodDesc"/></td>
                        <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.quantity"/></td>
                    </tr>

                        <%
                        java.util.Iterator iter = oppform.getProductList().values().iterator();
                        while(iter.hasNext()) {
                             ProductForm pForm = (ProductForm) iter.next();
                        %>
                        <tr>
                        <td><%=pForm.getProductId()%></td>
                        <td><%=pForm.getProductName()%></td>
                        <td><%=pForm.getQuantity()%></td>
                        </tr>
                        <%
                        }%>



                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
	    <tr>
              <td><a href="javascript:openAuctionDetails()"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.details"/></a>
              </td>
            </tr>
            <tr>
              <td>&nbsp;<br><br></td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <% String currency = (String) session.getAttribute("Currency");%>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.winningbid"/></td>
                    <td><strong><%=oppform.getCurrentBid()%>&nbsp;<%=currency%></strong></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.started"/></td>
                    <td><%=oppform.getStartDate()%></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.ends"/></td>
                    <td><%=oppform.getEndDate()%></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                  <tr>
		    <td>&nbsp;</td>
		    <td align="right"><input class="submitDoc" type="submit" value="<isa:translate key="eAuctions.buyer.b2b.auctiondetails.checkout"/>"/></td>
		    <td>&nbsp;</td>
		  </tr>
                </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </body>
</html>
