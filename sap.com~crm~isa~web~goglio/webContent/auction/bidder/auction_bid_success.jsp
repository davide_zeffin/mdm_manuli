<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page language="java" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<html>

  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctionbidsuccess.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" 
    		type="text/css" rel="stylesheet">
    
  </head>
  <body class="organizerCatalog">
    <div class="module-name"><isa:moduleName name="auction/bidder/auction_bid_success.jsp"/></div>
    <div id="new-doc" class="module">
      <br><br> 
              
              <table width="60%" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                  <tr>
                  <tr><td class="headerWhite" colspan="2" valign="middle" align="left"><var>&nbsp;<isa:translate key="eAuctions.buyer.b2b.auctionbidsuccess.confirm"/></var></td>
      	    	      <td align="left"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2blue.gif") %>" width="7" height="17" alt="" border="0"></td>     
               </tr>
               </tbody>
               </table>
               <br>
         
      <h3><isa:translate key="eAuctions.buyer.b2b.auctionbidsuccess.msg.thankyou"/></h3>
      
      <!--<div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctionconfirm.bidconfirm"/></div>-->
      <form action='<isa:webappsURL name="auction/bidder/createresponse.do"/>' name="createResponseForm">
        <input type="hidden" name="screenId" value="bidsuccess"/>
        
        
        <br><br>
        <input class="submitDoc" type="submit" value="<isa:translate key="eAuctions.buyer.b2b.auctionbidsuccess.continue"/>"/>
    </div>
  </body>
</html>