<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<html>
    <head>
      <script src="<isa:mimeURL name="b2b/jscript/frames.js"  language=""/>"
          type="text/javascript"></script>
      <SCRIPT language=JavaScript>
            function replaceISAFrames() {
                organizer_content().location.href="<isa:webappsURL name="auction_winner_menu.jsp"/>";
                organizer_nav().location.href="<isa:webappsURL name="b2b/organizer-nav-doc-search.jsp"/>";
             }
      </SCRIPT>
   </head>
   <body onLoad="replaceISAFrames()">
   <div class="module-name"><isa:moduleName name="auction/bidder/auction_winner_frame.jsp"/></div>
   </body>
</html>
