<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* "%>
<%@ page import= "com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*,com.sap.isa.auction.actionforms.ProductForm" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page language="java" %>
<%@ page import ="com.sap.isa.catalog.webcatalog.WebCatItem" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<jsp:useBean id="oppform" scope="session" type="com.sap.isa.auction.actionforms.OpportunitiesForm"/>

<html>
  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctiondetailspublished.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">

    <script src="<isa:mimeURL name="b2b/jscript/reload.js"  language=""/>"
          type="text/javascript"></script>
    <script src="<isa:mimeURL name="b2b/jscript/openwin.js"  language=""/>"
          type="text/javascript"></script>
    <script>
        function openAuctionDetails(){
        classPrd = window.open("<isa:webappsURL name="auction/bidder/auction_details_termDesc.jsp"/>", "Start", "status,width=300,height=400,scrollbars=yes,resizeable=yes");
        which = "start";
    }
    </script>
  </head>
  <body class="organizer">
    <div class="module-name"><isa:moduleName name="auction/bidder/auction_details.published.jsp"/></div>

    <div id="new-doc" class="module">
      <div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctiondetailspublished.subtitle"/></div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td colspan="2"><h2><%=oppform.getOppName()%></h2></td>
              <td style="vertical-align: middle;">
                <table width="80%" border="1">
                  <tbody>
                    <tr>
                        <td><isa:translate key="eAuctions.buyer.b2b.prodNum"/></td>
                        <td><isa:translate key="eAuctions.buyer.b2b.prodDesc"/></td>
                        <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.quantity"/></td>
                    </tr>

                        <%
                        java.util.Iterator iter = oppform.getProductList().values().iterator();
                        while(iter.hasNext()) {
                             ProductForm pForm = (ProductForm) iter.next();
                        %>
                        <tr>
                        <td><%=pForm.getProductId()%></td>
                        <td><%=pForm.getProductName()%></td>
                        <td><%=pForm.getQuantity()%></td>
                        </tr>
                        <%
                        }%>



                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
                <td>[<a href=""><isa:translate key="eAuctions.buyer.b2b.auctiondetails.totheproductdetails"/></a>]
                &nbsp;<a href="javascript:openAuctionDetails()"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.details"/></a>
                </td>
            </tr>
            <tr>
              <td>&nbsp;<br><br></td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                <% String currency = (String) session.getAttribute("Currency");%>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.currentbid"/> </td>
                    <td><strong>--</strong></td>
                    <td>&nbsp;</td>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.firstbid"/></td>
                    <td><%=oppform.getStartBid()%>&nbsp;<%=currency%></td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.timeleft"/></td>
                    <td><%=oppform.getTimeLeft()%></td>
                    <td>&nbsp;</td>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.noofbids"/></td>
                    <td>--</td>
                  </tr>
                  <tr>
                    <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.started"/></td>
                    <td><%=oppform.getStartDate()%></td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.ends"/></td>
                    <td><%=oppform.getEndDate()%></td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="5">&nbsp;</td>
                  </tr>
		  <tr>
		    <td>&nbsp;</td>
                    <td><input class="submitDoc" name=back Onclick="javascript:history.back()" type="button" value="<isa:translate key="eAuctions.backbtn.value"/> "></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>

                </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
    </div>
  </body>
</html>
