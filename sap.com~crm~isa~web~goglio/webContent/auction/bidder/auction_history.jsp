<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*,com.sap.isa.auction.actionforms.*" %>
<%@ page import= "com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page language="java" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<jsp:useBean id="oppform" scope="session" type="com.sap.isa.auction.actionforms.OpportunitiesForm"/>
<html>

  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctionhistory.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">

  </head>
  <body class="organizerCatalog">
    <div class="module-name"><isa:moduleName name="auction/bidder/auction_history.jsp"/></div>
    <div id="new-doc" class="module">

      <br><br>

              <table width="60%" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                  <tr>
                  <tr><td class="headerWhite" colspan="2" valign="middle" align="left"><var>&nbsp;<isa:translate key="eAuctions.buyer.b2b.auctionhistory"/></var></td>
      	    	                       <td align="left"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2blue.gif") %>" width="7" height="17" alt="" border="0"></td>
               </tr>
               </tbody>
               </table>
               <br>




      <!--<div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctionhistory"/></div>-->


      <h3><%=oppform.getOppName()%></h3>
      <%String currency = (String) session.getAttribute("Currency");%>
      <table border="1">
         <tr>
              <td><isa:translate key="eAuctions.buyer.b2b.prodNum"/></td>
              <td><isa:translate key="eAuctions.buyer.b2b.prodDesc"/></td>
              <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.quantity"/></td>
          </tr>

              <%
              java.util.Iterator iter = oppform.getProductList().values().iterator();
              while(iter.hasNext()) {
                   ProductForm pForm = (ProductForm) iter.next();
              %>
              <tr>
              <td><%=pForm.getProductId()%></td>
              <td><%=pForm.getProductName()%></td>
              <td><%=pForm.getQuantity()%></td>
              </tr>
              <%
              }%>


      </table>

      <BR><BR>
      <isa:translate key="eAuctions.buyer.b2b.auctionhistory.currentBid"/>
      <var class="main">
      <%if (oppform.getCurrentBid() >0) {%>
          <%=oppform.getCurrentBid()%>&nbsp;<%=currency%></var>
      <%} else {%>
         <strong>--</strong></var>
      <%}%>
      <br><br>

        <br><br>
        <table class="catalogList" border="0" cellspacing="0" cellpadding="3">
          <thead>
            <tr>
              <th><isa:translate key="eAuctions.buyer.b2b.auctionhistory.bidamount"/></th>
              <th style="border-right: none;"><isa:translate key="eAuctions.buyer.b2b.auctionhistory.biddate"/></th>
            </tr>
          </thead>
          <tbody>
		<%
                OpportunitiesForm opform = (OpportunitiesForm) session.getAttribute("oppform");
		TreeMap bidders=(TreeMap) opform.getBidInfo();
		//out.println(bidders.size());
		if(bidders != null){
		Object[]  keys = bidders.keySet().toArray();
		for(int i=0; i<keys.length; i++) {
		BidHistoryForm bidhist = (BidHistoryForm)bidders.get(keys[i]);

		%>
        <TR>
          <TD><%=bidhist.getBidPrice()%>&nbsp;<%=currency%></TD>
          <TD><%=bidhist.getDate()%></TD>
        </TR>

	<% } } %>

          </tbody>
        </table>
        <br><br>
        <form>
        <input class="submitDoc" onclick="window.close();" type="button" value="<isa:translate key="eAuctions.buyer.b2b.auctionhistory.close"/>"/>
        </form>
    </div>
  </body>
</html>