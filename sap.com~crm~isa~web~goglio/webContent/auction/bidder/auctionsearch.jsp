<%@ taglib uri="/isa"  prefix="isa" %>

<%@ page import="com.sap.isa.core.util.*,com.sap.isa.auction.actionforms.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.auction.actions.AuctionSearchAction" %>
<%@ page import="java.util.Collection,java.util.*" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>


<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<isa:contentType />
<jsp:useBean id="auctionSearchForm"
               class="com.sap.isa.auction.actionforms.AuctionSearchForm"
               scope="session" />


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>SAP Internet Sales - Document Search</title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">
    <script src='<%=WebUtil.getMimeURL(pageContext, "", "b2b/jscript/reload.js" ) %>'
            type="text/javascript">
    </script>
    <script type="text/javascript"><!--
        function submitForOtherDocument(documentType) {
            if (document.selectform.<%=AuctionSearchAction.DOCUMENT_TYPE%>[
                document.selectform.<%=AuctionSearchAction.DOCUMENT_TYPE%>.selectedIndex].value
                != "<%=AuctionSearchAction.AUCTION%>") {
                document.selectform.submit();
            }
        }
        function updateAttributeValue(attribute) {
                document.selectform.attribute.value = <%=auctionSearchForm.getAttribute()%>

	}


    --></script>
</head>
<body class="organizerCatalog" onLoad="javascript:updateAttributeValue('<%=auctionSearchForm.getAttribute()%>')">
<div class="module-name"><isa:moduleName name="auction/bidder/auctionsearch.jsp"/></div>
<div id="organizer-search" class="module">


    <form name="selectform" method="post" action='<isa:webappsURL name="/b2b/auctionsearch.do"/>'>
    <input type="hidden" value="" name="<isa:translate key="eAuctions.search.status"/>"

        <table cellspacing="0" cellpadding="1">
        <tbody>

            <%-- selection of document type --%>
            <tr>
                <td nowrap><isa:translate key="eAuctions.auction.select.showMe"/>&nbsp;</td>
                <td><select class="bigCatalogInput" name="<%=AuctionSearchAction.DOCUMENT_TYPE%>"
                            onChange="submitForOtherDocument()">
                        <% if ((userSessionData.getAttribute(ShopReadAction.QUOTATION_NOT_ALLOWED) == null) ||
                              !((Boolean)userSessionData.getAttribute(ShopReadAction.QUOTATION_NOT_ALLOWED)).booleanValue()) { %>
                            <option value="<%=AuctionSearchAction.QUOTATION%>">&nbsp;<isa:translate key="eAuctions.select.quotations"/></option>
                        <% } %>
                        <option value="<%=AuctionSearchAction.ORDER%>">&nbsp;<isa:translate key="eAuctions.select.orders"/></option>
                        <option value="<%=AuctionSearchAction.ORDERTEMPLATE%>">&nbsp;<isa:translate key="eAuctions.select.ordertemplates"/></option>
                        <% if ((userSessionData.getAttribute(ShopReadAction.QUOTATION_NOT_ALLOWED) == null) ||
                              !((Boolean)userSessionData.getAttribute(ShopReadAction.CONTRACT_NOT_ALLOWED)).booleanValue()) { %>
                            <option value="<%=AuctionSearchAction.CONTRACT%>" >&nbsp;<isa:translate key="eAuctions.select.contracts"/></option>
                        <% } %>
                        <option value="<%=AuctionSearchAction.INVOICE%>">&nbsp;<isa:translate key="eAuctions.select.invoices"/></option>
                        <option value="<%=AuctionSearchAction.CREDITMEMO%>">&nbsp;<isa:translate key="eAuctions.select.creditmemos"/></option>
                        <option value="<%=AuctionSearchAction.DOWNPAYMENT%>">&nbsp;<isa:translate key="eAuctions.select.downpayments"/></option>
                        <option value="<%=AuctionSearchAction.AUCTION%>" selected>&nbsp;<isa:translate key="eAuctions.select.auction"/> </option>
                    </select>
                </td>
            </tr>
            <br>
            <br>
            <%-- selection of search attribute --%>
            <tr>
                <td nowrap><isa:translate key="eAuctions.auction.select.whichare"/></td>
                <td><select class="bigCatalogInput"
                            name="attribute">
                        <option value="<%=auctionSearchForm.NOT_SPECIFIED%>" ><isa:translate key="eAuctions.select.notSpecified"/>
                        <option value="<%=auctionSearchForm.OPEN%>" >&nbsp;<isa:translate key="eAuctions.select.open"/>
                        <option value="<%=auctionSearchForm.WON%>"  >&nbsp;<isa:translate key="eAuctions.select.won"/>
                        <option value="<%=auctionSearchForm.CHECKEDOUT%>" >&nbsp;<isa:translate key="eAuctions.select.checkedout"/>
                        <option value="<%=auctionSearchForm.NEW_ID%>" >&nbsp;<isa:translate key="eAuctions.select.new"/>
                        <option value="<%=auctionSearchForm.ENDTODAY_ID%>" >&nbsp;<isa:translate key="eAuctions.select.endtoday"/>
                        <option value="<%=auctionSearchForm.NORESERVEPRICE_ID%>" >&nbsp;<isa:translate key="eAuctions.select.noreserveprice"/>
                    </select>
                </td>
            </tr>

           <br>

            <tr>
                <td>&nbsp;</td>
                <td>
                    <input class="submitDoc" type=submit
                           name="<%= AuctionSearchAction.AUCTION_SEARCH_BUTTON %>"
                           value='<isa:translate key="eAuctions.select.search"/>' >
                </td>
            </tr>
        </tbody>
        </table>
    <br><br>

    <%-- if requested, table with results (auctions) of selection --%>


    <% if (auctionSearchForm.getSelectRequested()) { %>

    <%


    TreeMap  oppInfo= (TreeMap) request.getAttribute(AuctionSearchAction.AUCTION_LIST);
	        if(oppInfo==null || oppInfo.isEmpty()) { %>
	            <th nowrap style="text-align: left"><isa:translate key="eAuctions.search.nohits"/></th>
	        <%}else { %>


	        	<div class="module-content">
		    		    <table border="0" cellspacing="0" cellpadding="3" class="catalogList">
		    			<thead>
		    			    <tr>
		    				<th><isa:translate key="eAuctions.buyer.b2b.participation.auctiontitle"/></th>
		    				<th><isa:translate key="eAuctions.buyer.b2b.participation.numofbids"/></th>
		    				<th><isa:translate key="eAuctions.buyer.b2b.participation.enddate"/></th>
		    				<th><isa:translate key="eAuctions.buyer.b2b.participation.yourbid"/></th>
		    				<th><isa:translate key="eAuctions.buyer.b2b.participation.currentbid"/></th>

		    			    </tr>
			</thead>



		<%	Object[]  keys = oppInfo.keySet().toArray();
			for(int i=0; i<keys.length; i++) {
			   OpportunitiesForm opp = (OpportunitiesForm)oppInfo.get(keys[i]);


    		%>


				<tbody>
				    <TR>
					<%
                                        	String linkPath="auction/bidder/showDetails.do?opportunity_id="+opp.getOppId();
						UserSessionData userData = UserSessionData.getUserSessionData(session);
						BusinessObjectManager isaCoreBom =  (BusinessObjectManager)userData.getBOM(BusinessObjectManager.ISACORE_BOM);
						String currency = isaCoreBom.getShop().getCurrency();
                                        %>
                                        <%
						if(auctionSearchForm.getAttribute().equals(auctionSearchForm.CHECKEDOUT)) {%>
						  <TD><%= opp.getOppDesc()%></TD>
						<%} else { %>  
						  <TD><A href="<isa:webappsURL name="<%=linkPath%>"/>" target="form_input"><%= opp.getOppDesc()%></A></TD>
					<%}%>
                                        
                                     
					<TD><%=opp.getNoOfBids()%></TD>
					<TD><%=opp.getEndDate()%></TD>
					<TD><%=opp.getMinBid()%>&nbsp;<%= currency %></TD>
					<TD><%=opp.getCurrentBid()%>&nbsp;<%= currency %></TD>
				    </TR>
			    <% } %>
				</tbody>
			    </table>
			  </div>
		<% } %>


    </form>
    <% } %>
</div>
</body>
</html>