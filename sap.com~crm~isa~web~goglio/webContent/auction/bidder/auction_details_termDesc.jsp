<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*,com.sap.isa.auction.actionforms.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page language="java" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<jsp:useBean id="oppform" scope="session" type="com.sap.isa.auction.actionforms.OpportunitiesForm"/>
<html>

  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctionterms.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">

  </head>
  <body class="organizerCatalog">
   <div class="module-name"><isa:moduleName name="auction/bidder/auction_details_termDesc.jsp"/></div>
   <div id="new-doc" class="module">
      <br><br> 
              
              <table width="60%" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                  <tr>
                  <tr><td class="headerWhite" colspan="2" valign="middle" align="left"><var>&nbsp;<isa:translate key="eAuctions.buyer.b2b.auctionterms.title"/></var></td>
      	    	                       <td align="left"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2blue.gif") %>" width="7" height="17" alt="" border="0"></td>     
               </tr>
               </tbody>
               </table>
               <br>
         
      
      
      <!--<div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctionterms.title"/></div> -->
      <h2><%=oppform.getOppName()%></h2>

       <% String oppDesc = oppform.getOppDesc().equals("NoValue")?"":oppform.getOppDesc();
               String oppTerm = oppform.getOppTerm().equals("NoValue")?"":oppform.getOppTerm();
        %>   
            
       <isa:translate key="eAuctions.buyer.b2b.auctionDesc"/>&nbsp;<%=oppDesc%>
       <br><br>
       <isa:translate key="eAuctions.buyer.b2b.auctionTerm"/>&nbsp;<%=oppTerm%>
       <br><br>

        <form>
        <input class="submitDoc" onclick="window.close();" type="button" value="<isa:translate key="eAuctions.buyer.b2b.auctionhistory.close"/>"/>
        </form>
    </div>
  </body>
</html>