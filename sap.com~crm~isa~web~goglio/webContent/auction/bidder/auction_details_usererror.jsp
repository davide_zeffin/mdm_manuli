<%@ page language="java" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<html>

  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctiondetails_usererror.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">

  </head>
  <body class="organizer">
    <div class="module-name"><isa:moduleName name="auction/bidder/auctiondetails_usererror.jsp"/></div>
    <div id="new-doc" class="module">
      <div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctiondetails_usererror.error"/></div>
      
        <p style="margin-top:15px; margin-bottom:15px;"><isa:translate key="eAuctions.buyer.b2b.auctiondetails_usererror.msg"/></p>
    </div>
  </body>
</html>