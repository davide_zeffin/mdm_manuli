<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*,com.sap.isa.auction.actionforms.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<html>

  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctionlist.title"/></title>
    <!--<link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet"> -->



<link href="<isa:mimeURL name="mimes/shared/style/stylesheet.css" language=""/>"
          type="text/css" rel="stylesheet">
  </head>
  <body class="organizerCatalog">
    <div class="module-name"><isa:moduleName name="auction/bidder/auction_items.jsp"/></div>
    <% String catPath=(String)request.getParameter("categoryPath");
       String prod="";
       if(catPath !=null){
       	int index =catPath.lastIndexOf(";");
       	prod=catPath.substring(index+1,catPath.length());
    }
    %>
    <!--<div class="column">-->
    <!--<div id="search-results" class="module"> -->
    <!--<table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--
          <tbody>
            <tr>
              <td colspan="2"><h3><isa:translate key="eAuctions.buyer.b2b.auctionlist.subtitle"/></h3></td>
              <td style="vertical-align: middle;">
                <table width="70%" border="0">
                  <tbody>
                    <tr>
                      <td><isa:translate key="eAuctions.buyer.b2b.prodNum"/><bean:write name="oppform" property="productId"/></td>
                      <td align="right"></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td rowspan="10"><img src="<isa:mimeURL name="auction/images/small.gif"  language=""/>" alt=""></td>
              <td rowspan="10" style="padding-right: 25px;">&nbsp;</td>
              <td><isa:translate key="eAuctions.buyer.b2b.prodDesc"/><bean:write name="oppform" property="prodDesc"/></td>
            </tr>
           </tbody>
        </table>-->
        <!--<tr><td class="headerWhite" colspan="2" valign="middle" align="left"><var>&nbsp;Auction List Forvvv <%= (String)request.getParameter("categoryPath")%></var></td>
	                       <td align="left"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2blue.gif") %>" width="7" height="17" alt="" border="0"></td>
         </tr> -->



        <br><br>

        <table width="60%" border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
            <tr><td class="headerWhite" colspan="2" valign="middle" align="left"><var>&nbsp;<isa:translate key="eAuctions.buyer.b2b.auctionlist.heading"/> <%=prod%></var></td>
	    	                       <td align="left"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2blue.gif") %>" width="7" height="17" alt="" border="0"></td>
         </tr>
         </tbody>
         </table>
         <br>
         <br>
        <table border="0" cellspacing="0" cellpadding="3" class="catalogList">
            <thead>
                <tr>
                 <th><isa:translate key="eAuctions.buyer.b2b.auctionlist.opportunity"/></th>
                 <th><isa:translate key="eAuctions.buyer.b2b.auctionlist.opportunitydesc"/></th>
                 <th><isa:translate key="eAuctions.buyer.b2b.auctionlist.currentbid"/></th>
                 <th><isa:translate key="eAuctions.buyer.b2b.auctionlist.numofbid"/></th>
                 <th><isa:translate key="eAuctions.buyer.b2b.auctionlist.endtime"/></th>
                </tr>
            </thead>
                 <tbody>
                    <%  String currency = (String) session.getAttribute("Currency");
                        TreeMap  oppInfo= (TreeMap) session.getAttribute("oppInfo");
			if(oppInfo !=null){
			Object[]  keys = oppInfo.keySet().toArray();
			for(int i=0; i<keys.length; i++) {
			OpportunitiesForm opp = (OpportunitiesForm ) oppInfo.get(keys[i]);
                        double currentBid = opp.getCurrentBid();

			String linkPath="auction/bidder/showDetails.do?opportunity_id="+opp.getOppId();
			%>

                        <TR>
                            <%  if (opp.isBidbyUser()) {%>
                               <TD>*<%=opp.getOppId() %></TD>
                            <%} else {%>
                                <TD><%=opp.getOppId() %></TD>
                            <%}%>
                            <TD align=middle><A href="<isa:webappsURL name="<%=linkPath%>"/>"><%=opp.getOppDesc()%></A>
                            </TD>
                            <% if (opp.getCurrentBid() >0) {%>
                            <TD><%=opp.getCurrentBid()%>&nbsp;<%=currency%></TD>
                            <%} else {%>
                            <TD>--</TD>
                            <%}%>
		            <TD align="center"><%=opp.getNoOfBids()%></TD>
                            <TD><%=opp.getEndDate() %></TD>
                        </TR>

			<% }} %>
                       </tbody>
                    </table><br>
                    <table>
                    <td><a href="javascript:history.back()"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_mit_rand_blau.gif") %>"
                    width="16" height="16" alt="" border="0" class="display-image"></a></td>
                    <td>&nbsp;<a href="javascript:history.back()"><isa:translate key="eAuctions.backbtn.value"/></a></td>
		    </table>
         </div>
      </div>
  </body>
</html>