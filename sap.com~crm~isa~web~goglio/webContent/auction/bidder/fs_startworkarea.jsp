<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa"  prefix="isa" %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <title><isa:translate key="b2b.fs.title"/></title>

  <script type="text/javascript">

    function resize(direction, frameObj) {
      if (direction == "min") {
        if (frameObj == "history")
          window.fourthFS.cols = '*,15,0';
      }
      else {
        if (frameObj == "history")
          window.fourthFS.cols = '*,15,95';
      }
    }

  </script>

  </head>
      
      <frameset cols="*,15,95" id="fourthFS" border="0" frameborder="0" framespacing="0">
        <frameset rows="70,*" id="workFS">
          <frame name="documents"  src="<isa:webappsURL name="/b2b/updateworkareanav.do"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
          <frame name="form_input" src="<isa:webappsURL name="/auction/bidder/showDetails.do"/>?refresh=1" frameborder="0" scrolling="auto" marginwidth="0" marginheight="0">
        </frameset>
        <frame name="closer_history" src="<isa:webappsURL name="/b2b/closer_history.jsp"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
        <frame name="_history" src="<isa:webappsURL name="/b2b/history/updatehistory.do"/>" frameborder="0" scrolling="auto" marginwidth="0" marginheight="0">
      </frameset>
</html>