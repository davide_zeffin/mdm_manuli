<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*" %>
<%@ page import="com.sap.isa.auction.actionforms.ProductForm"%>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page language="java" %>
<%@ page import ="com.sap.isa.catalog.webcatalog.WebCatItem" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<jsp:useBean id="oppform" scope="session" type="com.sap.isa.auction.actionforms.OpportunitiesForm"/>
<html>
  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctiondetails.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">
    <script src="<isa:mimeURL name="b2b/jscript/reload.js"  language=""/>"
          type="text/javascript"></script>
    <script src="<isa:mimeURL name="b2b/jscript/openwin.js"  language=""/>"
          type="text/javascript"></script>
    <script>
    function openBidHistory(){

        classPrd = window.open("<isa:webappsURL name="auction/bidder/auction_history.jsp"/>", "Start", "status,width=300,height=400,scrollbars=yes,resizeable=yes");
        which = "start";
    }
    function openAuctionDetails(){
        classPrd = window.open("<isa:webappsURL name="auction/bidder/auction_details_termDesc.jsp"/>", "Start", "status,width=300,height=400,scrollbars=yes,resizeable=yes");
        which = "start";
    }

   function checkBid() {

        var priceQuote = parseFloat(createResponseForm.priceQuote.value);
        var currentBid = parseFloat(createResponseForm.currentBid.value);
        var startBid = parseFloat(createResponseForm.startBid.value);
        if (isNaN(priceQuote)) {
            alert("<isa:translate key="eAuctions.buyer.b2b.validation.message.nan"/>");
            return false;
        }
        if (priceQuote >currentBid && priceQuote > startBid)
            return true;
        else {
            if (priceQuote<=0){
                alert("<isa:translate key="eAuctions.buyer.b2b.validation.message.invalidbidamt"/>");
                return false;
            }
            if (priceQuote <= currentBid){
                alert("<isa:translate key="eAuctions.buyer.b2b.validation.message.lessthancurrbid"/>");
                return false;
            }

            if (priceQuote <startBid){
                alert("<isa:translate key="eAuctions.buyer.b2b.validation.message.lessthanstartbid"/>");
                return false;
            }

        }
    }


    </script>

  </head>

  <body class="organizerCatalog">
    <div class="module-name"><isa:moduleName name="auction/bidder/auction_details.jsp"/></div>

    <div id="new-doc" class="module">

    <br><br>

            <table width="60%" border="0" cellpadding="0" cellspacing="0">
            <tbody>

                <tr><td class="headerWhite" colspan="2" valign="middle" align="left"><var>&nbsp;<isa:translate key="eAuctions.buyer.b2b.auctiondetails.subtitle"/></var></td>
    	    	                       <td align="left"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2blue.gif") %>" width="7" height="17" alt="" border="0"></td>
             </tr>
             </tbody>
             </table>
             <br>
         <br>
     <!-- <div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.subtitle"/></div> -->
       <form  action='<isa:webappsURL name="auction/bidder/createresponse.do"/>' name="createResponseForm" onSubmit="return checkBid()">
        <input type="hidden" name="screenId" value="details"/>
        <input type="hidden" name="currentBid" value="<%=oppform.getCurrentBid()%>"/>
        <input type="hidden" name="startBid" value="<%=oppform.getStartBid()%>"/>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td colspan="2"><h2><%=oppform.getOppName()%></h2></td>
              <td style="vertical-align: middle;">
                <table width="80%" border="1">
                  <tbody>
                    <tr>
                        <td><isa:translate key="eAuctions.buyer.b2b.prodNum"/></td>
                        <td><isa:translate key="eAuctions.buyer.b2b.prodDesc"/></td>
                        <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.quantity"/></td>
                    </tr>

                        <%
                        java.util.Iterator iter = oppform.getProductList().values().iterator();
                        while(iter.hasNext()) {
                             ProductForm pForm = (ProductForm) iter.next();
                        %>
                        <tr>
                        <td><%=pForm.getProductId()%></td>
                        <td><%=pForm.getProductName()%></td>
                        <td><%=pForm.getQuantity()%></td>
                        </tr>
                        <%
                        }%>


                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <!--<td>[<a href=""><isa:translate key="eAuctions.buyer.b2b.auctiondetails.totheproductdetails"/></a>]
              &nbsp;<a href="javascript:openAuctionDetails()"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.details"/></a>
              </td> -->
              <td>&nbsp;<a href="javascript:openAuctionDetails()"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.details"/></a>
              </td>
              <td>
              <table>
	                          <td><a href="javascript:history.back()"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_mit_rand_blau.gif") %>"
	                          width="16" height="16" alt="" border="0" class="display-image"></a> </td>
	                          <td>&nbsp;<a href="javascript:history.back()"><isa:translate key="eAuctions.backbtn.value"/></a></td>
	      		    </table>
	      		    </td>
         </div>


            </tr>
            <tr>
              <td>&nbsp;<br><br></td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <% String currency = (String) session.getAttribute("Currency");%>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.currentbid"/></td>
                    <%if (oppform.getCurrentBid() >0) {%>
                    <td><strong><%=oppform.getCurrentBid()%>&nbsp;<%=currency%></strong></td>
                    <%} else {%>
                    <td><strong>--</strong></td>
                    <%}%>
                    <td>&nbsp;</td>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.firstbid"/></td>
                    <td><%=oppform.getStartBid()%>&nbsp;<%=currency%></td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.timeleft"/></td>

                   <% //int timeleft = Integer.parseInt(oppform.getTimeLeft());
                   	if((oppform.getTimeLeft()).startsWith("-")){ %>

                    <td><strong>--</strong)</td>
                    <% } else {%>
                    <td><%=oppform.getTimeLeft()%></td>
                    <% } %>
                    <td>&nbsp;</td>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.noofbids"/></td>
                    <td><%=oppform.getNoOfBids()%>
                    [<a href="javascript:openBidHistory()"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.bidhistory"/></a>]</td>
                  </tr>
                  <tr>
                    <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.started"/></td>
                    <td><%=oppform.getStartDate()%></td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.ends"/></td>
                    <td><%=oppform.getEndDate()%></td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="5">&nbsp;</td>
                  </tr>
                 <% if(!(oppform.getTimeLeft()).startsWith("-")){ %>
                  <tr>
                    <td class="auctionFramed" colspan="4">
                      <table border="0">
                        <thead>
                          <tr>
                            <th colspan="3"><div class="opener"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.bidding"/></div></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.yourmaxbid"/></td>
                            <td align="right"><input class="submitDoc" type="text" name="priceQuote" size="16"/></td>
                            <td><%=currency%></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td align="right"><input class="submitDoc" type="submit" value="<isa:translate key="eAuctions.buyer.b2b.auctiondetails.placebid"/>"/></td>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                    <td>&nbsp;</td>
                  </tr> <% } %>
                </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </body>
</html>
