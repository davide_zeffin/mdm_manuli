<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />
<jsp:useBean id="oppform" scope="session" type="com.sap.isa.auction.actionforms.OpportunitiesForm"/>

<html>
  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctiondetailserrors.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">
    <script src="<isa:mimeURL name="b2b/jscript/reload.js"  language=""/>"
          type="text/javascript"></script>
    <script src="<isa:mimeURL name="b2b/jscript/openwin.js"  language=""/>"
          type="text/javascript"></script>
    <script>
    function openBidHistory()
	{
		classPrd = window.open("<isa:webappsURL name="auction/bidder/auction_history.jsp"/>", "Start", "status,width=300,height=400,scrollbars=yes,resizeable=yes");
		which = "start";
        }
    function openAuctionDetails(){
        classPrd = window.open("<isa:webappsURL name="auction/bidder/auction_details_termDesc.jsp"/>", "Start", "status,width=300,height=400,scrollbars=yes,resizeable=yes");
        which = "start";
    }
    function checkBid() {

        var priceQuote = parseFloat(createResponseForm.priceQuote.value);
        var currentBid = parseFloat(createResponseForm.currentBid.value);
        var startBid = parseFloat(createResponseForm.startBid.value);
        if (isNaN(priceQuote)) {
            alert("<isa:translate key="eAuctions.buyer.b2b.validation.message.nan"/>");
            return false;
        }
        if (priceQuote >currentBid && priceQuote > startBid)
            return true;
        else {
            if (priceQuote<=0){
                alert("<isa:translate key="eAuctions.buyer.b2b.validation.message.invalidbidamt"/>");
                return false;
            }
            if (priceQuote <= currentBid){
                alert("<isa:translate key="eAuctions.buyer.b2b.validation.message.lessthancurrbid"/>");
                return false;
            }

            if (priceQuote <startBid){
                alert("<isa:translate key="eAuctions.buyer.b2b.validation.message.lessthanstartbid"/>");
                return false;
            }

        }
    }

    </script>
  </head>
  <body class="organizer">
    
    <div class="module-name"><isa:moduleName name="auction/bidder/auction_details_errors.jsp"/></div>
    
    <div id="new-doc" class="module">
      <div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctiondetailserrors.outbid" onSubmit="return checkBid()"/></div>
      <form  action='<isa:webappsURL name="auction/bidder/createresponse.do"/>' name="createResponseForm">
        <input type="hidden" name="screenId" value="details"/>
        <input type="hidden" name="currentBid" value="<%=oppform.getCurrentBid()%>"/>
        <input type="hidden" name="startBid" value="<%=oppform.getStartBid()%>"/>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td colspan="2"><h2><%=oppform.getOppName()%></h2></td>
              <td style="vertical-align: middle;">
                <table width="80%" border="0">
                  <tbody>
                    <tr>
                      <td><isa:translate key="eAuctions.buyer.b2b.prodNum"/>&nbsp;<%=oppform.getProductId()%></td>
                      <td align="right"></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td rowspan="10"><img src="<isa:mimeURL name="auction/images/mid.gif"  language=""/>" alt=""></td>
              <td rowspan="10" style="padding-right: 25px;">&nbsp;</td>
              <td><isa:translate key="eAuctions.buyer.b2b.prodDesc"/>&nbsp;<%=oppform.getProdName()%></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
            <td>[<a href=""><isa:translate key="eAuctions.buyer.b2b.auctiondetails.totheproductdetails"/></a>]
              &nbsp;<a href="javascript:openAuctionDetails()"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.details"/></a>
            </td>
            </tr>
            <tr>
              <td>&nbsp;<br><br></td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                   <% String currency = (String) session.getAttribute("Currency");%>
                  <tr>
                     <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.currentbid"/></td>
                   <%if (oppform.getCurrentBid() >0 ){%>
                    <td><strong><%=oppform.getCurrentBid()%>&nbsp;<%=currency%></strong></td>
                    <%} else {%>
                    <td><strong>--</strong></td>
                    <%}%>
                    <td>&nbsp;</td>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.firstbid"/></td>
                    <td><%=oppform.getStartBid()%>&nbsp;<%=currency%></td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetailserrors.timeleft"/></td>
                    <td><%=oppform.getTimeLeft()%></td>
                    <td>&nbsp;</td>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.noofbids"/></td>
                    <td><%=oppform.getNoOfBids()%>
                    [<a href="javascript:openBidHistory()"><isa:translate key="eAuctions.buyer.b2b.auctiondetails.bidhistory"/></a>]</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.quantity"/></td>
                    <td><%=oppform.getQuantity()%></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.started"/></td>
                    <td><%=oppform.getStartDate()%></td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.ends"/></td>
                    <td><%=oppform.getEndDate()%></td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                    <td class="auctionFramed" colspan="4">
                      <table border="0" cellpadding="1" cellspacing="0">
                        <thead>
                          <tr>
                            <th colspan="3"><div class="opener">Bidding:</div></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td colspan="3" class="error"><isa:translate key="eAuctions.buyer.b2b.auctiondetailserrors.msg.bidappreciate"/><br><isa:translate key="eAuctions.buyer.b2b.auctiondetailserrors.msg.outbid"/></td>
                          </tr>
                          <tr>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="3"><strong><isa:translate key="eAuctions.buyer.b2b.auctiondetailserrors.msg.bidagain"/></strong></td>
                          </tr>
                          <tr>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr>
                            <td><isa:translate key="eAuctions.buyer.b2b.auctiondetails.yourmaxbid"/></td>
                            <td align="right"><input class="submitDoc" type="text" name="priceQuote" size="16"/></td>
                            <td><%=currency%></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td align="right"><input class="submitDoc" type="submit" value="<isa:translate key="eAuctions.buyer.b2b.auctiondetailserrors.placebid"/>"/></td>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </body>
</html>
