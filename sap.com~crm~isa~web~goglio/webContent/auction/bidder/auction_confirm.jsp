<%@ page import= "java.util.*,com.sap.isa.auction.util.*,javax.servlet.http.* ,com.sap.isa.auction.bean.*,com.sap.isa.auction.cache.*" %>
<%@ page import="com.sap.isa.auction.actionforms.ProductForm" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page language="java" %>

<%@ taglib uri="/isa"  prefix="isa" %>
<isa:contentType />

<jsp:useBean id="oppform" scope="session" type="com.sap.isa.auction.actionforms.OpportunitiesForm"/>
<jsp:useBean id="respBean" scope="session" type="com.sap.isa.auction.actionforms.CreateResponseForm"/>
<html>

  <head>
    <title><isa:translate key="eAuctions.buyer.b2b.auctionconfirm.title"/></title>
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>"
    		type="text/css" rel="stylesheet">

    <script src="<isa:mimeURL name="b2b/jscript/reload.js"  language=""/>"
          type="text/javascript"></script>
  </head>
  <body class="organizerCatalog">
    <div class="module-name"><isa:moduleName name="auction/bidder/auction_confirm.jsp"/></div>
    <div id="new-doc" class="module">
      <br><br>

              <table width="60%" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                  <tr>
                  <tr><td class="headerWhite" colspan="2" valign="middle" align="left"><var>&nbsp;<isa:translate key="eAuctions.buyer.b2b.auctionconfirm.bidconfirm"/></var></td>
      	    	                       <td align="left"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2blue.gif") %>" width="7" height="17" alt="" border="0"></td>
               </tr>
               </tbody>
               </table>
               <br>



      <!--<div class="module-name-new"><isa:translate key="eAuctions.buyer.b2b.auctionconfirm.bidconfirm"/></div>-->
      <form action='<isa:webappsURL name="auction/bidder/createresponse.do"/>' name="createResponseForm">
        <input type="hidden" name="screenId" value="confirm"/>
        <% String currency = (String) session.getAttribute("Currency");%>
        <h2><%=oppform.getOppName()%></h2>

            <table>
                <tr>
                    <td><isa:translate key="eAuctions.buyer.b2b.prodNum"/></td>
                    <td><isa:translate key="eAuctions.buyer.b2b.prodDesc"/></td>
                </tr>

                <%
                java.util.Iterator iter = oppform.getProductList().values().iterator();
                while(iter.hasNext()) {
                     ProductForm pForm = (ProductForm) iter.next();
                %>
                <tr>
                <td><%=pForm.getProductId()%></td>
                <td><%=pForm.getProductName()%></td>
                </tr>
                <%
                }%>
             </table>
        <h3><isa:translate key="eAuctions.buyer.b2b.auctionconfirm.msg.review"/></h3>
        <p style="margin-top:15px; margin-bottom:15px;"><isa:translate key="eAuctions.buyer.b2b.auctionconfirm.msg.confirm"/></p>
 <isa:translate key="eAuctions.buyer.b2b.auctionconfirm.msg.currentbidvalue"/>&nbsp;<var class="main"><%=respBean.getPriceQuote()%>&nbsp;<%=currency%></var>
        <br><br>
        <input class="submitDoc" type="submit" value="<isa:translate key="eAuctions.buyer.b2b.auctionconfirm.placebid"/>"/>&nbsp;<input class="submitDoc" name="cancel" Onclick="javascript:history.back()" type="button" value="<isa:translate key="eAuctions.buyer.b2b.auctionconfirm.cancelbtn.value"/>"/>
      </form>
    </div>
  </body>
</html>