<%@ page import="com.sap.isa.core.cache.Cache" %>
<%@ page import="com.sap.mw.jco.JCO, com.sap.isa.core.cache.Cache, java.util.Set, java.util.Date" %>
<%@ page import="java.util.Iterator, com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.io.File" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isJCoInfo())
        response.sendRedirect("notsupported.jsp");
%>


  <div class="filter">
	<h6>JCo Cache Statistics</h6>
	
	<p>Default TimeToLive [min]: <% out.println(Cache.Access.getDefaultTimeToLive("JCO")); %></p>       
	<p>Default IdleTime [min]: <% out.println(Cache.Access.getDefaultIdleTime("JCO")); %></p>
  </div>
  
  <div class="filter-result">
	  <table>
	    <tr>
	      <th>Key (Function Module)</th>
	      <th>Hits</th>
	      <th>Last Access Time</th>
	      <th>Expiration Time</th>
	      <th>clear</th>
	    </tr>      

    <%
	  String[] oddEvenClasses = new String[] {"odd", "even"};
	  int oddEvenClassesIndex = 0;
	
      Set keys = Cache.getAccess("JCO").getKeys();
      String key = null;
      Iterator iterator = keys.iterator();
      while(iterator.hasNext()) {
        key = (String)iterator.next();
        out.println("<tr class=" + oddEvenClasses[oddEvenClassesIndex++ % 2] + ">");
        out.println("<td>");
        out.println(key);
        out.println("</td>");
        out.println("<td>");  
        out.println(Cache.Access.getNumAccesses("JCO", key));
        out.println("</td>");
        out.println("<td>");  
        out.println(new Date(Cache.Access.getLastAccessTime("JCO", key)));
        out.println("</td>");
        out.println("<td>");
        out.println(new Date(Cache.Access.getExpirationTime("JCO", key)));
        out.println("</td>");
        out.println("<td>");
        out.println("<a href=\"jco_stats.jsp?clear=" + key + "\">clear</a>");
        out.println("</td>");
        out.println("</tr>");
      }
    %>
	  </table>
  </div>