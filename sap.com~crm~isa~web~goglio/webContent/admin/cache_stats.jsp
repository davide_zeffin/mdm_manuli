<%--
********************************************************************************
    File:         show_stats.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Marek Barwicki
    Created:      21.6.2001
    Version:      1.0

    $Revision: $
    $Date:  $
********************************************************************************
--%>


<%@ page import="com.sap.mw.jco.JCO, com.sap.isa.core.cache.Cache, java.util.Set, java.util.Date" %>
<%@ page import="java.util.Iterator, java.util.Collection, com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory" %>
<%@ page import="java.util.Date" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% if (!AdminConfig.isCoreCache())
        response.sendRedirect("notsupported.jsp");
%>

<%
	boolean isAdmin = AdminConfig.isXCMAdmin(request);
    
	String regionName = request.getParameter("region");
    
    if (regionName != null && isAdmin && !regionName.equals("all") ) {
        try {
          Cache.Access access = Cache.getAccess(regionName);
          if (access != null)
            access.invalidateAllObjects();
          regionName = null;
          
        } catch (Cache.Exception ex) {
        }
    }
    // maybe user clicked on clear all cashes...
	  try{
	    // maybe user clicked on clear all cashes...
		    if(regionName != null && isAdmin && regionName.equals("all") ) {
		        Collection regions = Cache.getAllRegionNames();
	
		        String region = null;
		        Iterator iterRegion = regions.iterator();
		        while(iterRegion.hasNext()) {
		          region = (String)iterRegion.next();
		          Cache.Access access;
		          access = Cache.getAccess(region);
		          if (access != null) {
		              access.invalidateAllObjects();
		          }
		            regionName = null;
		        }
		    }
		} catch (Exception e) {}
%>


<html>
  <head>
    <title>System Cache</title>
      <script type="text/javascript" src="admin.js">
      </script>

  </head>
  <body>

  <div class="filter-result">
	  <table>
	    <tr>
	      <th>Cache Name</th>
	      <th>Num Objects</th>
	      <th>Hits</th>      
	      <th>Time To Live [min]</th>
	      <th>Description</th>
	      <% if (isAdmin) { %>
		      <th>clear</th>
	      <% } %>
	    </tr>      

    <%
    
      String[] oddEvenClasses = new String[] {"odd", "even"};
	  int oddEvenClassesIndex = 0;
       
      Collection regions = Cache.getAllRegionNames();
      Cache.Access cacheAccess = null;
      
      String region = null;
      Iterator iterRegion = regions.iterator();
      while(iterRegion.hasNext()) {
        region = (String)iterRegion.next();
        Cache.Access access = Cache.getAccess(region);
        int numObjects = access.getNumObjects();
        Cache.Attributes attr = access.getRegionAttributes();
        long numHits = access.getNumAccesses(region);
        if (region.equals("DEFAULT"))
            continue;
            out.println("<tr class=" + oddEvenClasses[oddEvenClassesIndex++ % 2] + ">");
            out.println("<td>");
            out.println(region);
            out.println("</td>");
            out.println("<td>");
            out.println(numObjects);
            out.println("</td>");
            out.println("<td align=right>");  
            out.println(numHits);
            out.println("</td>");
            out.println("<td align=right>");  
            out.println(attr.getTimeToLive());
            out.println("</td>");
            out.println("<td>");
            out.println(attr.getDescription());
            out.println("</td>");
            // only the xcm admin is allowed to see the clear links.
            if (isAdmin) {
	            out.println("<td>");
	            out.println("<a href=\"cache_stats.jsp?region=" + region + "\">Clear</a>");
	            out.println("</td>");
            }
            out.println("</tr>");
      }
    %>
  	</table>
  </div>

	<br/>
  	<br/>
	<div id="buttons">
		<ul class="buttons-1">
			<li><a href="cache_stats.jsp">Refresh</a></li>
		</ul>
		<% if(isAdmin) { %>
			<ul class="buttons-1">
				<li><a href="cache_stats.jsp?region=all">Clear all</a></li>
			</ul>
		<% } %>
  	</div>
	<%@ include file="read_only_error.inc" %>
  </body>
</html>