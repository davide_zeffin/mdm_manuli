<%--
********************************************************************************
    File:         showcaches.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      19.6.2001
    Version:      1.0

    $Revision: #49 $
    $Date: 2001/06/19 $
********************************************************************************
--%>

<%@ page import="com.sap.isa.core.util.CacheManager" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isIsacoreCache())
        response.sendRedirect("notsupported.jsp");
%>

<%@ taglib uri="/isa" prefix="isa" %>

<%

	boolean isAdmin = AdminConfig.isXCMAdmin(request);
    String cacheToClear = request.getParameter("clear");

    if (cacheToClear != null && isAdmin && !cacheToClear.equals("all")) {
        CacheManager.clear(cacheToClear);
    }

    // maybe user clicked on clear all cashes...
	  try{
	    // maybe user clicked on clear all cashes...
		    if(cacheToClear != null && isAdmin && cacheToClear.equals("all") ) {
		    	String[] cacheNames = CacheManager.getAllCacheNames();

		    	for (int i = 0; i < cacheNames.length; i++) {
		    		CacheManager.clear(cacheNames[i]);
		    	}
		    }
		} catch (Exception e) {}
%>

<html>
  <head>
    <title>Cache Statistics</title>
    
  <script type="text/javascript" src="admin.js">
  </script>

  </head>
  <body>
  
  
  <% if (CacheManager.isInactive()){%>
    <div class="filter-result-msg">
			<div class="info">
				<table class="message">
					<tr>
						<td>
							<span>CacheManager is inactive!</span>
						</td>
					</tr>
				</table>
			</div>
		</div>
  <%
  }%>

  	<div class="filter-result">
  		<table>
		    <tr>
		      <th>Name</th>
		      <th>Status</th>
		      <th>Max. Size</th>
		      <th>Actual Size</th>
		      <th>Hits</th>
		      <th>Misses</th>
		      <% if (AdminConfig.isXCMAdmin(request)) { %>
			      <th>Clear</th>
			  <% } %>
		    </tr>
<%
	String[] oddEvenClasses = new String[] {"odd", "even"};
	int oddEvenClassesIndex = 0;

    String[] cacheNames = CacheManager.getAllCacheNames();

    for (int i = 0; i < cacheNames.length; i++) {
        out.println("<tr class=" + oddEvenClasses[oddEvenClassesIndex++ % 2] + ">");
        out.println("<td>");
        out.println(cacheNames[i]);
        out.println("</td><td align=\"right\">");
        out.println(CacheManager.isInactiveCache(cacheNames[i])?"inactive":"active");
        out.println("</td><td align=\"right\">");
        out.println(CacheManager.getMaximumSize(cacheNames[i]));
        out.println("</td><td align=\"right\">");
        out.println(CacheManager.getActualSize(cacheNames[i]));
        out.println("</td><td align=\"right\">");
        out.println(CacheManager.getNumberOfHits(cacheNames[i]));
        out.println("</td><td align=\"right\">");
        out.println(CacheManager.getNumberOfMisses(cacheNames[i]));
        out.println("</td>");
        if (AdminConfig.isXCMAdmin(request)) {
        	out.println("<td>");
	        out.println("<a href=\"showcaches.jsp?clear=" + cacheNames[i] + "\">Clear</a>");
			out.println("</td>");
        }
        out.println("</tr>");
    }

%>
  		</table>
  	</div>
  
  	<br/>
  	<br/>
	<div id="buttons">
		<ul class="buttons-1">
			<li><a href="showcaches.jsp">Refresh</a></li>
			<% if(isAdmin) { %>
				<li><a href="showcaches.jsp?clear=all">Clear all</a></li>
			<% } %>
		</ul>
  	</div>
	<%@ include file="read_only_error.inc" %>
  </body>
</html>