<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.maintenanceobject.action.*" %>
<%@ page import="com.sap.isa.maintenanceobject.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%
BaseUI ui = new BaseUI(pageContext);
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

  <title><isa:translate key="system.admintool.title"/></title>
  <script type="text/javascript" src="admin.js">
  </script>
</head>

<body>
  <div class="module-name"><isa:moduleName name="admin/loggingHelp.jsp" /></div>
  <div class="popup">
	<div class="content">    
 <table> 
	<tr>
	   <th>Formatter Pattern</th>
	</tr>
	<tr>
	   <td>
				The pattern that shall be used to write the logs. This option is
				only visible if the TraceFormatter is used.
		   </td>
		</tr>
		<tr>
		   <td>
				Here a few placeholders:
		   </td>
		</tr>
		<tr>
		   <td>
			   <div>
				   <ul class="help">
					   <li>%d = timestamp in readable form</li>
					   <li>%l = the location of origin (eg: com.sap.FooClass.fooMethod)</li>
					   <li>%c = the log controller the message was issued through (eg: com.sap.FooClass)</li>
					   <li>%t = the thread that emitted the message</li>
					   <li>%s = the message severity</li>
					   <li>%m = the formatted message text</li>
					   <li>%I = the message id</li>
					   <li>%p = the time stamp in milliseconds since January 1, 1970 00:00:00 GMT</li>
					   <li>%g = the group identification</li>
				   </ul>
			   </div>
		   </td>
	   </tr>
   </table>

	<%
	String backBtnTxt = WebUtil.translate(pageContext, "error.jsp.close", null);
	%>
	</div> 
	<div class="buttons">   
	 <ul class="buttons-3">
		 <li><a href="#" onclick="window.close();" <% if (ui.isAccessible()) { %> title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>" <% } %> class="showBackButton"><isa:translate key="error.jsp.close"/></a></li>
	 </ul>
	</div>

  </div>
</body>
</html>