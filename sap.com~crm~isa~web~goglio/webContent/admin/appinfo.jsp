<%@ page import="java.net.InetAddress" %>
<%@page import="java.net.URLEncoder, com.sap.isa.core.util.WebUtil" %>
<%@page import="com.sap.isa.core.logging.sla.SessionLoggingSupport" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.io.File" %>

<% if (!AdminConfig.isIsAppInfo())
        response.sendRedirect("notsupported.jsp");
%>


<%! 

    String getFilePathToClass(Class cls)
    {
        String filepath = null;

        try {
            String rsc = cls.getName().replace('.','/') + ".class";
            URL    url = cls.getClassLoader().getResource(rsc);
            String ptc = (url!=null ? url.getProtocol() : null);

            if ("jar".equals(ptc) || "file".equals(ptc))
            {
                filepath = url.getFile();
                filepath = filepath.substring(0, filepath.length() - rsc.length() - ("jar".equals(ptc) ? 2 : 1));
                if (filepath.startsWith("jar:")) filepath = filepath.substring(4);
                if (filepath.startsWith("file:")) filepath = filepath.substring(5);
                filepath = new File(filepath).getCanonicalPath();
            }
        }
        catch (Throwable ex) {
            //do nothing
        }
        finally {
            return filepath;
        }//if
    }

%>

<% 
    String init = (String)session.getAttribute("appinfoInit");
	if (init == null) {
		init = "true";
	}   
    String securityId=request.getParameter("securityId");
	if (securityId == null) {
		securityId = "N/A";
	}   
        
    String appURL =  request.getParameter("url"); 

    String sessionId = request.getSession().getId();
    String servletEngine = application.getServerInfo();    
    String os = System.getProperty("os.name") + " / " + System.getProperty("os.version");
    String host = InetAddress.getLocalHost().toString();

%>

 
<%@ taglib uri="/isa" prefix="isa" %>

  <script type="text/javascript" src="admin.js">
  </script>

<html>
<head>

<script language="JavaScript">
<!--
function openISA(url)
{
 if( this.location.href.indexOf("appinfoLoaded") <0 )
 {
    if(this.location.href.indexOf("?") >=0 )
	    this.location.replace(this.location.href + "&appinfoLoaded=true");
	else
	    this.location.replace(this.location.href + "?appinfoLoaded=true");
	ISA = window.open("<%= appURL %>", "ISA");
    ISA.focus(); 
 }
}
// -->
</script>
</head>

<body class="nodoc">
  	
    <div id="nodoc-first">
    <div id="nodoc-content">
 <div class="filter">
 <h4>Single Session Trace</h4>
</div>
    <body 
    <% if (init.equals("true")) { 
        session.setAttribute("appinfoInit", "false");
    %>
        onLoad="openISA()">
    <% } else { %> > <% } %>
 <div class="filter">
<h6>Application:</h6>
</div>
<div class="filter-result">
    <table border="1" cellspacing="0" cellpadding="3">
        <tr>
          <td bgcolor="#e0e0e0"><b>URL</b></td>
          <td><a href="<%= appURL %>" target="_new"><%= appURL %></a></td>      
        </tr>
    </table>
</div>
 <div class="filter">
<h6>System Info: (<a href="/sap/monitoring/SystemInfo">detailed information</a>)</h6>

</div>
<div class="filter-result">
    <table border="1" cellspacing="0" cellpadding="3">
        <tr>
          <td bgcolor="#e0e0e0"><b>Session ID</b></td>
          <td><%= sessionId %></td>      
        </tr>
        <tr>
          <td bgcolor="#e0e0e0"><b>Security ID</b></td>
          <td><%= securityId %></td>      
        </tr>
        
        <tr>
          <td bgcolor="#e0e0e0"><b>Servlet Engine</b></td>
          <td><%= servletEngine %></td>      
        </tr>
        <tr>
          <td bgcolor="#e0e0e0"><b>Operating System</b></td>
          <td><%= os %></td>      
        </tr>        
        <tr>
          <td bgcolor="#e0e0e0"><b>Server Host</b></td>
          <td><%= host %></td>      
        </tr>    
    </table>
    </div>		
    </div>
    </div>


</body>	
</html>