<%@ page language="java" %>
<%@ page autoFlush="true" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%@ page import="com.sap.isa.core.EnvironmentManager" %>
<%@ page import="com.sap.isa.core.eai.sp.jco.JCoUtil" %>


<isa:contentType />
<html>
<head>
  <title><isa:translate key="system.admintool.title" /></title>
  <script type="text/javascript" src="admin.js">
  </script>
  <script type="text/javascript">
    function reloadWorkarea(wa_header_title, wa_body) {
      parent.workareaHeader.location.href = '<isa:webappsURL name="/admin/workareaHeader.jsp" >
                                               <isa:param name="hdTitle" value=""/>
                                             </isa:webappsURL>' + wa_header_title;
      parent.workarea.location.href = wa_body;
    }
  </script>
</head>

<body class="nodoc">

    <div id="nodoc-first">
    <div id="nodoc-content">
    	<h1>
  			<isa:translate key="system.admintool.menu.title" />
  		</h1>

<%  if (!request.isSecure()) { %>
      <div id="message-content">
        <div class="warn">
          <span><isa:translate key="system.admintool.ssl.message"/></span>
        </div>
      </div>
<%  } %>

      <ul>
        <%-- XCM Tool --%>
        <% if ( AdminConfig.isXCMAdmin() && (AdminConfig.isXCMAdmin_RO(request) || AdminConfig.isXCMAdmin(request))) { %>
			<li>
                <a href="<isa:webappsURL name="/admin/xcm/init.do"/>" target="_blank"
                   title="<isa:translate key="system.admintool.tool.xcm" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.xcm" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.xcm" />
                </a>
                <br/>&nbsp;<br/>&nbsp;<br/>
			</li>
        <% } %>

        <%-- Show Cache Tool --%>
        <% if (AdminConfig.isIsacoreCache()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.cache',
                                                   '<isa:webappsURL name="/admin/showcaches.jsp"/>')"
                   title="<isa:translate key="system.admintool.tool.cache" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.cache" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.cache" />
                </a>
          </li>
        <% } %>

        <%-- Show Catalog Cache Tool --%>
        <% if (AdminConfig.isCatalogCache()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.catalogcac',
                                                   '<isa:webappsURL name="/admin/catalogcache.jsp"/>')"
                   title="<isa:translate key="system.admintool.tool.catalogcac" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.catalogcac" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.catalogcac" />
                </a>
          </li>
        <% } %>

        <%-- Show System Cache Tool --%>
        <% if (AdminConfig.isCoreCache()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.syscache',
                                                   '<isa:webappsURL name="/admin/cache_stats.jsp"/>')"
                   title="<isa:translate key="system.admintool.tool.syscache" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.syscache" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.syscache" />
                </a>
          </li>
        <% } %>

        <%-- Show Java Connector Tool --%>
        <% if (AdminConfig.isJCoInfo()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.jcocache',
                                                   '<isa:webappsURL name="/admin/jco_stats.jsp"/>')"
                   title="<isa:translate key="system.admintool.tool.jcocache" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.jcocache" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.jcocache" />
                </a>
          </li>
        <% } %>

        <%-- Show Java Sharing Connector Tool --%>
        <% if (AdminConfig.isJCoInfo() && JCoUtil.isJCSAvailable()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.jcsstats',
                                                   '<isa:webappsURL name="/admin/jcs_stats.jsp"/>')"
                   title="<isa:translate key="system.admintool.tool.jcsstats" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.jcsstats" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.jcsstats" />
                </a>
          </li>
        <% } %>


        <%-- Show CCMS Heartbeat Customizing --%>
        <% if (AdminConfig.isCcmsHeartbeatCustomize()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.ccmsheartb',
                                                   '<isa:webappsURL name="/admin/ccms/customizinginit.do"/>')"
                   title="<isa:translate key="system.admintool.tool.ccmsheartb" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.ccmsheartb" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.ccmsheartb" />
                </a>
          </li>
        <% } %>

        <%-- Show Lean Basket Data Migration to NetWeaver 04 --%>
        <% if (AdminConfig.isDBMigration() && EnvironmentManager.isSQLJAvailable()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.dbmig',
                                                   '<isa:webappsURL name="/admin/dbmig/start_mig.jsp"/>')"
                   title="<isa:translate key="system.admintool.tool.dbmig" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.dbmig" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.dbmig" />
                </a>
          </li>
        <% } %>

        <%-- Show Scheduler Administration --%>
        <% if (AdminConfig.isScheduler()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.scheduler',
                                                   '<isa:webappsURL name="/admin/scheduler/init.do"/>')"
                   title="<isa:translate key="system.admintool.tool.scheduler" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.scheduler" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.scheduler" />
                </a>
          </li>
        <% } %>

        <%-- Show Shop Data Migration (XML files to SAP J2EE Database) --%>
        <% if (AdminConfig.isShopAdmin()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.uploadshop',
                                                   '<isa:webappsURL name="/admin/uploadShop.do"/>')"
                   title="<isa:translate key="system.admintool.tool.uploadshop" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.uploadshop" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.uploadshop" />
                </a>
          </li>
        <% } %>

        <%-- Show Logging Information --%>
        <% if (AdminConfig.isLogging()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.logging',
                                                   '<isa:webappsURL name="/admin/logging.jsp"/>')"
                   title="<isa:translate key="system.admintool.tool.logging" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.logging" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.logging" />
                </a>
          </li>
        <% } %>

        <%-- Show Version Information --%>
        <% if (AdminConfig.isVersion()) { %>
          <li>
                <a href="javascript:reloadWorkarea('system.admintool.tool.version',
                                                   '<isa:webappsURL name="/admin/version.jsp"/>')"
                   title="<isa:translate key="system.admintool.tool.version" />"
                   onmouseover="window.status='<isa:translate key="system.admintool.tool.version" />';return true"
                   onmouseout="window.status='';return true">
                   <isa:translate key="system.admintool.tool.version" />
                </a>
          </li>
        <% } %>
      </ul>
    </div>
    </div>
</body>

</html>
