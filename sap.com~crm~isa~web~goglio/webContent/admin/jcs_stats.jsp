<%@ page import="com.sap.isa.jcs.JCS" %>
<%@ page import="com.sap.isa.jcs.LeasedConnectionContainer" %>
<%@ page import="java.util.Iterator, java.util.Set, java.util.Map" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isJCoInfo())
        response.sendRedirect("notsupported.jsp");
%>


<%@ taglib uri="/isa" prefix="isa" %>

<% if (!AdminConfig.isJCoInfo())
		response.sendRedirect("notsupported.jsp");
%>


<%
	Set leaseStatus = null;
	int numLeases = JCS.getNumLeasedConnections();
	try {
		leaseStatus = JCS.getConContainer();
	} catch (Throwable t) {
		t.printStackTrace();
	}
    
%>


<html>
  <head>
	<title>JCS Statistics</title>
  <script type="text/javascript" src="admin.js">
  </script>

  </head>
  <body>

  <div class="filter">
 <p>
	Number leased connections: <%=numLeases%>
 </p>
  
  </div>
  
	

   <% for(Iterator iter = leaseStatus.iterator();iter.hasNext();) {
	Map.Entry enty =  (Map.Entry)iter.next();  
	LeasedConnectionContainer cc = (LeasedConnectionContainer)enty.getValue();
	String id = cc.getId();
	String creationDate = cc.getCreatioDate();
	String commonId = cc.getCommonId();
	String conStatus = cc.getConnectionStatus();
	out.println("<div class='filter'>");
	out.println("<p>");
	out.println("jcs id: " + commonId + "<br/>");
	out.println("Connection state/(id): " + conStatus + " (" + id + ")<br/>");
	out.println("Creation date: " + creationDate  + "<br/>");
	//out.println("Internal key: " + cc.getInternalKey() + "<br/>");
	out.println("</p>");
	out.println("</div>");

	Map leaseIds = cc.getIdLeases();
	int index = 1;
	
	out.println("<div class='filter-result'>");
	out.println("<table border='1' cellspacing='0' cellpadding='3'>");
	out.println("<tr>");
	out.println("<th>lease number</td>");
	out.println("<th>component id</td>");
	out.println("<th>last lease date</td>");
	out.println("</tr>");
	out.println("</div>");	
	    
 for (Iterator iter2 = leaseIds.keySet().iterator(); iter2.hasNext();) {
		String compId = (String)iter2.next();
		String compLeaseDate = (String)leaseIds.get(compId);
		out.println("<tr>");
		out.println("<td>");
		out.println(index++);
		out.println("</td>");
		out.println("<td>");
		out.println(compId);
		out.println("</td>");
		out.println("<td>");
		out.println(compLeaseDate);
		out.println("</td>");
		out.println("</tr>");
	} // end for
  
	out.println("</table>");
   
	} // end for
   %>
</div> 
	<div id="buttons">
		<ul class="buttons-1">
			<li><a href="jcs_stats.jsp">Refresh</a></li>
		</ul>
  	</div>
  </body>
</html>