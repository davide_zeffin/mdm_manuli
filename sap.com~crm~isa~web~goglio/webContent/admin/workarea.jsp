<%@ page autoFlush="true" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />
<html>
<head>
	<title><isa:translate key="system.admintool.title" /></title>
  <script type="text/javascript" src="admin.js">
  </script>

</head>

<body class="nodoc">
	<div id="nodoc-first"> 
		<div id="nodoc-content">
  			<p class="p1">
		      <span tabindex=0><isa:translate key="system.admintool.config.descr" /></span>
  			</p>
  			<p class="p2">
		      <span tabindex=0><isa:translate key="system.admintool.config.instr" /></span>
  			</p>
  		</div>
  	</div>
</body>
</html>
