<%@ page contentType="text/xml; charset=UTF-8" %>
<%@ page import="org.apache.struts.action.Action" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.util.monitor.grmg.GrmgRequest" %>
<%@ page import="com.sap.util.monitor.grmg.GrmgResponse" %>
<%@ page import="com.sap.util.monitor.grmg.GrmgScenario" %>
<%    
	UserSessionData userSessionData = 
    			UserSessionData.getUserSessionData(request.getSession());

	// JC: Create GRMG response from incoming scenario
       	GrmgRequest grmgReq = (GrmgRequest)userSessionData.getAttribute("grmg_request");     	   
       	GrmgScenario scenario = grmgReq.getScenario();  	
	GrmgResponse grmgRes = new GrmgResponse(scenario);

	out.println(grmgRes.getOutput().toString());  


%>
	<%-- Include logic to invalidate the session. --%>
	<%@ include file="/user/logon/session_invalidate.inc" %>