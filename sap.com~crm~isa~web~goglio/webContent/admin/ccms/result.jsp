<%@ page import="com.sap.isa.core.util.*" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<html>
   <head>
    <isa:includes/>
      <title><isa:translate key="ccms.customize.status"/></title>
   </head>
   	
   <body>
   <jsp:useBean id="heartbeatCustomizingForm" class="com.sap.isa.core.ccms.HeartbeatCustomizingForm" scope="session" />

   <%if( heartbeatCustomizingForm.getCcmsdir() == null || heartbeatCustomizingForm.getCcmsdir().length() == 0 )
    {%>
		<h1><isa:translate key="ccms.ccmsDir.not.defined"/></h1>
    <%}else if( heartbeatCustomizingForm.getMessage() != null && heartbeatCustomizingForm.getMessage().length()>0 )
    {%>
	<h1><isa:translate key="ccms.customize.failed"/></h1>
    	<p><%= JspUtil.encodeHtml(heartbeatCustomizingForm.getMessage()) %></p>
   <%}else if(heartbeatCustomizingForm.getScenarioCount() > 0 )
   {%>
			<h1><isa:translate key="ccms.customize.success" arg0="<%=heartbeatCustomizingForm.getFilename()%>"/></h1>
			<isa:translate key="ccms.custom.scenarios.processed" arg0="<%=Integer.toString(heartbeatCustomizingForm.getScenarioCount())%>"/>
   <%}else {%>
			<h1><isa:translate key="ccms.customize.failed"/></h1>
			<isa:translate key="ccms.custom.noscenario.selected"/>
  <%}
    request.getSession().invalidate();%>
		<p></p>
   </body>
</html>
