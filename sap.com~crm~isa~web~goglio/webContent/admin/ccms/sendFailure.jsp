<%@ page contentType="text/xml; charset=UTF-8" %>

<%@ page import="com.sap.isa.core.*" %>
<%@ page import="org.apache.struts.action.Action" %>

<?xml version="1.0" encoding="UTF-8"?>
<%    
	UserSessionData userSessionData = 
    			UserSessionData.getUserSessionData(request.getSession());

	//create Error - Component for the CCMS - System
	out.write("<scenario>");
	out.write("<scenname>CRM_B2B</scenname>");
    	out.write("<sceninst>100</sceninst>");
    	out.write("<scenversion>001</scenversion>");
	out.write("<components>");
	out.write("<component>");
	out.write("<compname>00_ERROR</compname>");
	out.write("<compversion>001</compversion>");
	out.write("<comphost></comphost>");
	out.write("<compinst></compinst>");
	out.write("<messages>");
   	out.write("<message>");
	out.write("<messalert>ERROR</messalert>");
 	out.write("<messseverity>255</messseverity>");
	out.write("<messarea>RT</messarea>");
	out.write("<messnumber>null</messnumber>");
	out.write("<messparam1>null</messparam1>");
	out.write("<messparam2>null</messparam2>");
	out.write("<messparam3>null</messparam3>");
	out.write("<messparam4>null</messparam4>");
	out.write("<messtext>");

	out.write("" + userSessionData.getAttribute("failure"));

	out.write("</messtext>");
        out.write("</message>");
	out.write("</messages>");
	out.write("</component>");
	out.write("</components>");
	out.write("</scenario>");
	
%>
<%-- Include logic to invalidate the session. --%>
<%@ include file="/user/logon/session_invalidate.inc" %>
