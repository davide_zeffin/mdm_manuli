<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="org.w3c.dom.Attr" %>
<%@ page import="org.w3c.dom.NamedNodeMap" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="org.w3c.dom.Node" %>
<%@ page import="org.w3c.dom.NodeList" %>
<%if (!com.sap.isa.core.config.AdminConfig.isCcmsHeartbeatCustomize()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}
%>

<isa:contentType />
<html>
   <head>
    <isa:includes/>
    <script type="text/javascript" > 
<!-- 
function cancelSave(f)
    { 
      check=confirm("<isa:UCtranslate key="ccms.customizing.warning.head"/>"+"\n" + "<isa:UCtranslate key="ccms.customizing.warning.head1"/>");
    	if(check == true ) {
    		f.submit();
    		}
    }      
function toggle( idName) {
  target = document.all(idName);
  if (target.style.display == "none") {
    target.style.display = "inline";
  }else {
    target.style.display = "none";
  }
} 
-->
</script>

      <title><isa:translate key="ccms.customize.title"/></title>
   </head>
   <body>
      	<form method="POST"  name="customizing" action="<isa:webappsURL name="/admin/ccms/customizing.do"/>" >
	       <p><isa:translate key="ccms.customize.enter.url"/></p>
		<p>
		<jsp:useBean id="heartbeatCustomizingForm" class="com.sap.isa.isacore.actionform.ccms.B2xHeartbeatCustomizingForm" scope="session" />
			<input type="text" name="url" value="<jsp:getProperty name="heartbeatCustomizingForm" property="url"/>" size="100"  maxlength="250" /><br/>
		</p>
	       <p><isa:translate key="ccms.customize.enter.ccmsDir"/></p>
		<p>
			<input type="text" name="ccmsdir" value="<jsp:getProperty name="heartbeatCustomizingForm" property="ccmsdir"/>" size="100"  maxlength="250" /><br/>
		</p>
		<h2><isa:translate key="ccms.customize.list.xcm.scenario"/></h2>
		<p>
		<table border="0" >
		<% {
			Node scenarios=heartbeatCustomizingForm.getScenariosNode();
			Node scenario=scenarios.getFirstChild();
			int scenarioCount=0;
			while( scenario != null)
			{
				if( scenario.getNodeName().equals("tests"))
				{
					  %>
  <tr>
    <td >
					<%
					NamedNodeMap attr = scenario.getAttributes();
					Node a = attr.getNamedItem("scenario-name");
					String scenName="";
					if( a != null)
					  scenName=a.getNodeValue();
					%>
		<input type="checkbox" name="scenariosSelected[<%=scenarioCount %>]" /><a title="<isa:translate key="ccms.customize.toggle.comp" arg0="<%=heartbeatCustomizingForm.getScenariosName(scenarioCount) %>"/>" href='javascript: toggle("complist<%=scenarioCount%>");'><%=heartbeatCustomizingForm.getScenariosName(scenarioCount) %></a><br/>
	<div id="complist<%=scenarioCount%>" style="display='none'">
      <blockquote >
        <ul>
					<% 
					Node test = scenario.getFirstChild();
			
					while( test != null )
					{
						attr = test.getAttributes();
						String index="";
						String id="";
						String configuration="";
						String ccmsname="";
						a = attr.getNamedItem("index");
						if(a!= null)
							index = a.getNodeValue();
						a = attr.getNamedItem("id");
						if(a!= null)
							id = a.getNodeValue();
						a = attr.getNamedItem("configuration");
						if(a!= null)
							configuration = a.getNodeValue();
						a = attr.getNamedItem("ccmsname");
						if(a!= null)
							ccmsname = a.getNodeValue();
						//Do not list component if critical values are missing
						if (!id.equals("") && !configuration.equals(""))
						{
%> 
		<li><input  type="checkbox" name="componentSelected[<%=index%>]" checked="checked"><%=id%>(<%=configuration%>)</li>
	<%
						}
				test = test.getNextSibling();
			}%>
		<li><input  type="checkbox" name="imsTestSelected[<%=scenarioCount%>]" checked="checked"/>IMS</li>
		<%	scenarioCount++;
			scenario = scenario.getNextSibling();
		}
	}
%>
        </ul>        
      </blockquote>
      </div>
   </td>
  </tr>
		<% } %>
</table>
	</p>
	<p>
		<input type="hidden" name="generate" value=""/>
		<input  type="button" value="<isa:translate key="ccms.customize.generate.button"/>" onclick="cancelSave(document.customizing)" />
		<br/>
	</p>
	</form>
	<p><br/></p>
   </body>
</html>
