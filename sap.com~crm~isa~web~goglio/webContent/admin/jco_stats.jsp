<%@ page import="com.sap.mw.jco.JCO, com.sap.isa.core.cache.Cache, java.util.Set, java.util.Date" %>
<%@ page import="java.util.Iterator, com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.io.File" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<%@ taglib uri="/isa" prefix="isa" %>

<% if (!AdminConfig.isJCoInfo())
        response.sendRedirect("notsupported.jsp");
%>


<%! 

    String getFilePathToClass(Class cls)
    {
        String filepath = null;

        try {
            String rsc = cls.getName().replace('.','/') + ".class";
            URL    url = cls.getClassLoader().getResource(rsc);
            String ptc = (url!=null ? url.getProtocol() : null);

            if ("jar".equals(ptc) || "file".equals(ptc))
            {
                filepath = url.getFile();
                filepath = filepath.substring(0, filepath.length() - rsc.length() - ("jar".equals(ptc) ? 2 : 1));
                if (filepath.startsWith("jar:")) filepath = filepath.substring(4);
                if (filepath.startsWith("file:")) filepath = filepath.substring(5);
                filepath = new File(filepath).getCanonicalPath();
            }
        }
        catch (Throwable ex) {
            //do nothing
        }
        finally {
            return filepath;
        }//if
    }

%>

<%
    
    boolean showJCoCache = true;
    String rfcClear = request.getParameter("clear");
    Cache.Access cacheAccess = null;
    try {
      cacheAccess = Cache.getAccess("JCO");
      cacheAccess.remove(rfcClear);
      
    } catch (Cache.Exception ex) {
    }

    String NOT_FOUND = "Library not found";  
    String SYS_PATH  = "System-defined path";
    String api_version = JCO.getVersion();
    String middleware_version = JCO.getMiddlewareVersion();
    String libjrfc_version = JCO.getMiddlewareProperty("jco.middleware.libjrfc_version");
    String libjrfc_path = JCO.getMiddlewareProperty("jco.middleware.libjrfc_path");
    String librfc_version = JCO.getMiddlewareProperty("jco.middleware.librfc_version");
    String libjco_path = getFilePathToClass(JCO.Client.class);
    String librfc_path        = JCO.getMiddlewareProperty("jco.middleware.librfc_path");  

    if (librfc_version != null) {
        int i1 = librfc_version.indexOf("***")+4;
        int i2 = librfc_version.indexOf("***", i1);
        if (i2>=i1) {
           librfc_version = librfc_version.substring(i1,i2);
        }//if
    }//if


%>


<html>
  <head>
    <title>Java Connector</title>
      <script type="text/javascript" src="admin.js">
      </script>

  </head>
  <body>

  <div class="filter">
  	<p>JRFC Version: <% out.println(api_version); %></p>

  	<h6>JCo Pool Statistics</h6>
  </div>
  
  <div class="filter-result">
	  <table>
	    <tr>
	      <th>pool Name</th>
	      <th>max pool size</th>
	      <th>max cons used</th>
	      <th>num current used cons</th>
	    </tr>
<%
   String[] oddEvenClasses = new String[] {"odd", "even"};
   int oddEvenClassesIndex = 0;
	
   String[] poolNames = JCO.getClientPoolManager().getPoolNames();
   int numPools = poolNames.length;
   JCO.Pool pool;
   int index; 
   String poolName = null;
   byte[] conStates = null;

   for (int i = 0; i < numPools; i++) {
        pool = JCO.getClientPoolManager().getPool(poolNames[i]);
        out.println("<tr class=" + oddEvenClasses[oddEvenClassesIndex++ % 2] + ">");
        out.println("<td>");
        if ((index = poolNames[i].indexOf(JCoManagedConnectionFactory.PASSWD_DELIMITER)) != -1)
            poolName = poolNames[i].substring(0, index) + JCoManagedConnectionFactory.PASSWD_DELIMITER;
        else
            poolName = poolNames[i];
        out.println(poolName);
        out.println("</td><td align=\"right\">");
        out.println(pool.getMaxPoolSize());
        out.println("</td><td align=\"right\">");
        out.println(pool.getMaxUsed());
        out.println("</td><td align=\"right\">");
        out.println(pool.getNumUsed());
        out.println("</td>");
      }
   
%>
  	</table>
  </div>
  
  <!-- show cache only if it available and has some entries -->
  <% try { 
        Cache.Access access = Cache.getAccess("JCO");
        if (access.getNumObjects() != 0) {
        %>
        	<br/><br/>
        	<jsp:include page="jco_cache_stats.jsp" flush="true"/>
        <%
        }
     } catch (Cache.Exception ex) { /* no cache */ }
  %>    


  	<br/>
  	<br/>
	<div id="buttons">
		<ul class="buttons-1">
			<li><a href="jco_stats.jsp">Refresh</a></li>
		</ul>
  	</div>
  </body>
</html>