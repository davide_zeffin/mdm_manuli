<%@ page import="com.sap.isa.core.cache.Cache" %>
<%@ page import="java.util.Iterator, com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory" %>
<%@ page import="com.sap.isa.core.xcm.ConfigContainer" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.sap.isa.core.xcm.ExtendedConfigManager" %>
<%@page import="java.net.URLEncoder, com.sap.isa.core.util.XMLHelper" %>
<%@page import="com.sap.isa.core.xcm.XCMConstants" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>



<%
    
    boolean showJCoCache = true;
    String href = request.getParameter("href");
    String configKey = request.getParameter("key");
    Cache.Access cacheAccess = null;
    String configFile = "";
    String encodedConfigFile = "";
    try {
      cacheAccess = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
      ConfigContainer cc = (ConfigContainer)cacheAccess.get(configKey);
    //  out.println(cc.toString());
    	configFile = cc.getConfigAsString(href);
    	encodedConfigFile = XMLHelper.stringNormalizer(configFile);
    	  
    } catch (Cache.Exception ex) {
    }

%>


<html>
  <head>
    <title>Java Connector</title>
  </head>
  <body>
		<pre>
			<% out.println(encodedConfigFile); %>
		
		
		</pre>

  </body>
</html>