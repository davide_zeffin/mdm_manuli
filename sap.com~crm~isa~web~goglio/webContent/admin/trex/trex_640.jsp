<%@ page language="java" %>
<%@ page autoFlush="true" %>
<%@ page import="com.sapportals.trex.core.TrexFactory" %>
<%@ page import="com.sapportals.trex.TrexException" %>
<%@ page import="com.sapportals.trex.core.TrexConfigManager" %>
<%@ page import="com.sapportals.trex.core.admin.AdminManager" %>
<%@ page import="com.sapportals.trex.core.admin.ServerInfo" %>
<%@ page import="com.sapportals.trex.core.admin.ServerIndexList" %>
<%@ page import="com.sapportals.trex.core.admin.IndexShortDescription" %>
<%@ page import="com.sapportals.trex.core.SearchResult" %>
<%@ page import="com.sapportals.trex.core.ResultDocument" %>
<%@ page import="com.sapportals.trex.core.DocAttribute" %>
<%@ page import="com.sapportals.trex.core.SearchManager" %>
<%@ page import="com.sapportals.trex.core.QueryEntry" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%if (!com.sap.isa.core.config.AdminConfig.isCatalogCache()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}
%>
<%
    TrexConfigManager tcm= TrexFactory.getTrexConfigManager();
    AdminManager tam=TrexFactory.getAdminManager();
    String ns= tcm.getMasterNameServer();
    ServerIndexList theListOfIndices = null;
    SearchResult theSearchResult = null;
%>
<html>
<head>
  <body>
Main  Nameserver=<%=ns%><br/>
<% 
   ArrayList httpServers= tam.showHttpServer();
   try {
	    tam.listIndexes();
	    %>
	<font color="green">       TREX Server is available </font><br/>
	    <%
   }
   catch( TrexException ex )
   {
       %>
<font color="red">       TREX NOT AVAILABLE!!! </font>
       <%
   }
   Iterator iter=httpServers.iterator();
   while(iter.hasNext())
       {
         ServerInfo httpServer= (ServerInfo) iter.next();
 %>
	Availabe Http servers: <%=httpServer.getURL()%><br/>
 <%}
 

 	try {

 		theListOfIndices = tam.getAllIndices();
 		String system = request.getParameter("system");
 		if (system != null)
 			system = system.toLowerCase(); //index IDs are always in lower case
		String client = request.getParameter("client");
		int length = 0;
		try {
			length = (new Integer(request.getParameter("length"))).intValue();
		}
		catch (NumberFormatException ex) {
			%><font color="blue">List length not specified or invalid format, setting to 0 (i.e. no restriction)</font><br/><%
			length = 0;
		}%>
 		<br/><font color="green"> The list of the first up to <%=length%> indices from system <%=system%>(<%=client%>): </font><br/> 		
 		<%
 		IndexShortDescription iSD = theListOfIndices.getFirstIndexShortDescription();
 		int counter = 1;
 		while (iSD != null && (counter <= length || length == 0) ) {
 			if (iSD.getIndexId().endsWith(system+client) || system == null) {%>
 			<br/><br/><%=iSD.getIndexId()%>:
 				
 		<%		try {
 					SearchManager theSearchManager = TrexFactory.getSearchManager();
					QueryEntry aQueryEntry = new QueryEntry();
								
					aQueryEntry.setValue("$ROOT_ISA_ERP", "", "EQ");
					aQueryEntry.setRowType("ATTRIBUTE");
					aQueryEntry.setLocation("AREA");
					aQueryEntry.setTermAction((byte) 69); // 69 = exact
					
					theSearchManager.addIndexId(iSD.getIndexId(),"DRFUZZY");
					theSearchManager.setResultFromTo(1, 10000);
					theSearchManager.addRequestedAttribute("CATALOG_GUID");
					theSearchManager.addRequestedAttribute("P_INDEX_ID");
					theSearchManager.addRequestedAttribute("INDEX_STATUS");
					theSearchManager.addRequestedAttribute("AREA");
					theSearchManager.addQueryEntry(aQueryEntry);
					
					theSearchResult = theSearchManager.search(); //fire the search
					
					if (theSearchResult.getNoOfAllHitsInIndex() == 0) { //most probably a P-Index
						%><font color="blue">No $ROOT_ISA_ERP document, maybe a P-Index?</font> <%
						iSD = theListOfIndices.getNextIndexShortDescription();
						counter++;
						continue;
					}
					ResultDocument aResultDocument = theSearchResult.getFirstResultDocument(); //the ROOT document
					if (aResultDocument != null) {
						DocAttribute aDocAttribute = aResultDocument.getFirstDocAttribute();
							while (aDocAttribute != null) { %>
								<%=aDocAttribute.getAttributeName()%>=<%=aDocAttribute.getValue1()%>; <% //dump all attribute name/value pairs
								 aDocAttribute = aResultDocument.getNextDocAttribute();
						 	}
					}
 				}
 				catch (TrexException ex) {
 					%>
					<font color="red"> search() threw exception: </font>
					<%=ex.getLocalizedMessage()%><br/>
 			 <%}
 			}
 			iSD = theListOfIndices.getNextIndexShortDescription();
 			counter++;
 		}
 		%>
 		<%
 	}
 	catch( TrexException ex ) {
 		%>
 		<font color="red"> getAllIndices() threw exception: </font><br/>
 		<%=ex.getLocalizedMessage()%>
 	<%}%>
</body>
</html>