<%@ page isErrorPage="true" language="java" autoFlush="true" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />
<html>
  <head>
    <title><isa:translate key="system.admintool.error.title"/></title>
	<isa:stylesheets theme=""/>
  </head>

  <body>
  		<div class="filter-result-msg">
			<div class="error">
				<table class="message">
					<tr>
						<td>
							<span><isa:translate key="system.admintool.error.text"/></span>
						</td>
					</tr>
				</table>
			</div>
		</div>
  </body>
</html>
