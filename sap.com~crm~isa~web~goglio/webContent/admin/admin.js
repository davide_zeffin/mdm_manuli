var base_stylesheet = '<link rel="stylesheet" href="../mimes/base_stylesheet.css" type="text/css">';
var stylesheet = '';

if (navigator.userAgent.indexOf("MSIE") >= 0) {
	//alert("Internet Explorer : " + navigator.userAgent);
	// use the same stylesheet as for IE 6.0
	stylesheet = '<link rel="stylesheet" href="../mimes/stylesheet_ie6.css" type="text/css">';
	}
if (navigator.userAgent.indexOf("MSIE 8.0") >= 0) {
	//alert("Explorer 8.0: " + navigator.userAgent);
	stylesheet = '<link rel="stylesheet" href="../mimes/stylesheet_ie8.css" type="text/css">';
	}
if (navigator.userAgent.indexOf("MSIE 7.0") >= 0) {
	//alert("Explorer 7.0: " + navigator.userAgent);
	stylesheet = '<link rel="stylesheet" href="../mimes/stylesheet_ie7.css" type="text/css">';
	}
if (navigator.userAgent.indexOf("MSIE 6.0") >= 0) {
	//alert("Explorer 6.0: " + navigator.userAgent);
	stylesheet = '<link rel="stylesheet" href="../mimes/stylesheet_ie6.css" type="text/css">';
	}
if (navigator.userAgent.indexOf("MSIE 5.5") >= 0) {
	//alert("Explorer 5.5: " + navigator.userAgent);
	stylesheet = '<link rel="stylesheet" href="../mimes/stylesheet_ie55.css" type="text/css">';	
	}
if (navigator.userAgent.indexOf("MSIE 5.01") >= 0) {
	//alert("Explorer 5.01: " + navigator.userAgent);
	stylesheet = '<link rel="stylesheet" href="../mimes/stylesheet_ie5.css" type="text/css">';	
	}
if (navigator.userAgent.indexOf("Opera") >= 0){
	//alert("Opera: " + navigator.appName);
	stylesheet = '<link rel="stylesheet" href="../mimes/stylesheet_op.css" type="text/css">';
	}
if (navigator.userAgent.indexOf("AppleWebKit") >= 0){
	//alert("Opera: " + navigator.appName);
	stylesheet = '<link rel="stylesheet" href="../mimes/stylesheet_wk.css" type="text/css">';
	}
 
 document.writeln(base_stylesheet);
 if (stylesheet.length > 0) {
 	document.writeln(stylesheet);
 }