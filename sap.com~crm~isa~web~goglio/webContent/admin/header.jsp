<%@ page language="java" %>
<%@ page autoFlush="true" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />
<html>

<head>
  <title><isa:translate key="system.admintool.title"/></title>
    <script type="text/javascript" src="admin.js">
    </script>

</head>

<body class="header-basic">
		<div class="module-name"></div>
      
        <div id="header-appl">
        	<div class="header-logo"></div>
            <div class="header-applname"><isa:translate key="system.admintool.title"/></div>
           
            <div id="header-extradiv1">
                <span></span>
            </div>
            
            <div id="header-extradiv2">
                <span></span>
            </div>
            <div id="header-extradiv3">
                <span></span>
            </div>
            <div id="header-extradiv4">
                <span></span>
            </div>
            <div id="header-extradiv5">
                <span></span>
            </div>
            <div id="header-extradiv6">
                <span></span>
            </div>
        </div>
	</body>
</html>
