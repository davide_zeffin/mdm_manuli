<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%@ page language="java" %>
<%@ page autoFlush="true" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%
	if(!AdminConfig.setUserAuthorization(request)) {
		// raise an info message like:
			// The usermapping in web-j2ee-engine.xml or the web.xml has been maintained incorrectly. Please check the xml-files.
		response.sendRedirect("notsupported.jsp");
	}
%>
<isa:contentType />
<html>
<head>
  <title><isa:translate key="system.admintool.title"/></title>
  <script type="text/javascript">
	function resize(direction, frameObj) {
	  if (direction == "closeFrame") {
		if (frameObj == "adminOrganizer")
		  document.getElementById('thirdFS').cols = '0,15,*';
	  }
	  else {
		if (frameObj == "adminOrganizer")
		  document.getElementById('thirdFS').cols = '250,15,*';
	  }
	}
  </script>
</head>

<frameset rows="85,*" frameborder="0" border="0" framespacing="0">
  <frame name="adminHeader" src='<isa:webappsURL name="/admin/header.jsp" />'
		 frameborder="0" border="0" framespacing="0" noresize="noresize" scrolling="no">
  <frameset cols="250,15,*" frameborder="0" border="0" framespacing="0" id="thirdFS">
	<frame name="adminOrganizer" src='<isa:webappsURL name="/admin/organizer.jsp"/>'
		   frameborder="0" border="0" framespacing="0" noresize="noresize">
	<frame name="closer_left" src='<isa:webappsURL name="/admin/closer.jsp" />'
		   frameborder="0" marginheight="0" marginwidth="0" noresize="noresize" scrolling="no">
	<frameset rows="60,*" frameborder="0" border="0" framespacing="0">
	  <frame name="workareaHeader" src='<isa:webappsURL name="/admin/workareaHeader.jsp">
										  <isa:param name="hdTitle" value=""/>
										</isa:webappsURL>'
			 frameborder="0" border="0" framespacing="0" noresize="noresize" scrolling="no">
	  <frame name="workarea" src='<isa:webappsURL name="/admin/workarea.jsp" />'
			 frameborder="0" border="0" framespacing="0" noresize="noresize">
	</frameset>
  </frameset>
</frameset>

</html>