<%@ page language="java" %>
<%@ page autoFlush="true" %>
<%@ page errorPage="error.jsp" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />
<html>

<head>
  <title><isa:translate key="system.admintool.title"/></title>
  <script type="text/javascript" src="admin.js">
  </script>

  <%
    String titleId = request.getParameter("hdTitle");
  %>
</head>

<body>
	<h1>
	<% if (titleId != null && titleId.length() > 0) { %>
  	<isa:translate key="<%= JspUtil.encodeHtml(titleId) %>" />
  	<% } %>
  	</h1>
</body>

</html>
