	<% if (!AdminConfig.isXCMAdmin(request)) { %>
	  <table width="100%">
		  <tr>
		  	<td>
				<div class="filter-result-msg">
					<div class="info">
					
						<table class="message">
							<tr>
								<td>
									<span>Your user has only read only rights.</span>
								</td>
							</tr>
						</table>
					</div>
				</div>
	 		</td>
	 	</tr>
	 </table>
	<% } %>