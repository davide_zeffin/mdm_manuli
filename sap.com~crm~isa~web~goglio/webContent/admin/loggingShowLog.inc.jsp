<%@ page import="com.sap.isa.core.config.AdminConfig"%>
<%@ page import="com.sap.isa.core.util.table.ResultData"%>
<%@ page import="com.sap.isa.core.util.table.TableRow"%>
<%@ page import="com.sap.isa.core.util.*"%>
<%@ page import="com.sap.isa.core.logging.*"%>
<%@ page import="java.net.URLEncoder"%>

<%@ page import="com.sap.isa.isacore.TabStripHelper"%>
<%@ page import="com.sap.isa.isacore.TabStripHelper.TabButton"%>

<%@ page isErrorPage="true" language="java" autoFlush="true"%>
<%@ taglib uri="/isa" prefix="isa"%>

<%
	if (!AdminConfig.isLogging())
		response.sendRedirect("notsupported.jsp");

	//STYLESHEET property
	String[] oddEvenClasses = new String[] { "odd", "even" };
	int oddEvenClassesIndex = 0;

	// set the variable if the user is an admin
	boolean isAdmin = AdminConfig.isXCMAdmin(request);

	//VIEW LOG FILE MODULE
	final String NEXT        = "next";
	final String PREVIOUS    = "previous";
	final String LAST        = "last";
	final String FIRST       = "first";
	final String INIT        = "init";
	final String RELOAD      = "reload";
	final String LINE        = "line";
	final String LINE_NUMBER = "linenumber";
	final String ROW_DELTA   = "rowdelta";
	final String COMMAND     = "command";

   //-------------------------------------
   // START of VIEW LOG FUNCTIONALITY
   //-------------------------------------

	// get admin object if not the first time on this page
	Admin admin = (Admin) session.getAttribute("admin");
	String command = request.getParameter(COMMAND);

	// if there is no admin object forward user to logging.jsp
	// this jsp cannot be used without setting the admin object first.
	if (admin == null || admin.getLogFileName() == null || admin.getLogFileName().equals("") || command == null) {
		response.sendRedirect("logging.jsp");
	}

   ResultData logFileContent = null;

   // initialize Admin object with new log file table
   if (command!=null && command.equals(INIT)) {
    	command = FIRST;
   }

   // reload Admin object
   if (command!=null && command.equals(RELOAD)) {
		try {
		    int rowDelta = Integer.parseInt(request.getParameter(ROW_DELTA));
		    admin.loadLogFile();
		 	command = LINE_NUMBER;
		} catch (Exception ex) {
			logFileContent = admin.current();
		}
   }


	if(command!=null) {
		if (command.equals(FIRST)) {
			logFileContent = admin.first();
		} else if (command.equals(LAST)) {
			logFileContent = admin.last();
		} else if (command.equals(NEXT)) {
			logFileContent = admin.next();
		} else if (command.equals(PREVIOUS)) {
			logFileContent = admin.previous();
		} else if (command.equals(LINE)) {
			try {
				int lineNumber = Integer.parseInt(request.getParameter(LINE_NUMBER));
				logFileContent = admin.line(lineNumber);
			} catch (Exception ex) {
				logFileContent = admin.current();
			}
		} else if (command.equals(LINE_NUMBER)) {
			try {
				int rowDelta = Integer.parseInt(request.getParameter(ROW_DELTA));
				admin.setRowDelta(rowDelta);
				logFileContent = admin.current();
			} catch (Exception ex) {
				logFileContent = admin.current();
			}
	   }
	}
   request.setAttribute("logfilecontent", logFileContent);

   //-------------------------------------
   // END of VIEW LOG FUNCTIONALITY
   //-------------------------------------
%>
<isa:contentType />
<html>
<head>
<title>View log file</title>
<script type="text/javascript" src="admin.js"></script>
<style type="text/css">
<!--
.filter-result a {
	color: #FFFFFF;
	text-decoration: none;
}

.filter-result a:hover {
	background-color: #21598C;
	color: #FFFFFF;
	text-decoration: underline;
}

.filter-result a:visited {
	color:#FFFFFF;
}
-->
</style>
</head>
<body>
		<%  String backUrl  = "/admin/logging.jsp";
			if(admin.isSessionLog()) {
				backUrl += "?tab=session";
    		} %>
		<a href="<isa:webappsURL name="<%=backUrl%>"/>" class="showBackButton" alt="Back to logging destinations">Back to logging destinations</a>
	<h1>View Log File:</h1>
	<div class="filter-result">
	<table border=1 cellspacing=1 bgcolor="#e0e0e0" width=100%>
    <tr>
   		<th width="180">
   			<br /><form name="gotoline" method="get">
	            <input type=hidden name="<%=COMMAND %>" value="line"/>
	            Go to line: <input class="textinput" name="<%=LINE_NUMBER %>" size="10"/>
				<a href="#" onclick="document.gotoline.submit()" class="showLogButton">OK</a>
			</form>
		</th>
        <th width="250">
   			<br /><form name="showlines" method="get">
	            <input type=hidden name="<%=COMMAND %>" value="<%=LINE_NUMBER %>"/>
	            Show number lines: <input class="textinput" name="<%=ROW_DELTA %>" size=10/>
				<a href="#" onclick="document.showlines.submit()" class="showLogButton">OK</a>
			</form>
		</th>
        <th width="60">
        	<br /><form name="reload" method="get">
	            <input type="hidden" name="<%=COMMAND %>" value="<%= RELOAD %>"/>
	            <input type="hidden" name="<%=ROW_DELTA %>" value="<%= admin.getRowDelta() %>"/>
	            <a href="#" onclick="document.reload.submit()" class="showLogButton">Reload</a>
            </form>
        </th width="100">
        <% // The user should see this column only if log file download is enabled
		    if(AdminConfig.isLogfileDownload()) {%>
		        <th>
		        	<br /><form name="dummy">
		        		<a href="<isa:webappsURL name="/servlet/com.sap.isa.core.logging.CoreAdminServlet?fromshowlog=true"/>&logfilename=<%=admin.getLogFileName() %>" class="showLogButton">Download log</a>
		        	</form>
		        </th>
		<% } %>
	</tr>
</table>
</div>

<p>
<div class="filter-result">
<table border=1 cellspacing=1 bgcolor="#e0e0e0">
<tr>
	<th width="60"><a class="logCommandButton" href="<isa:webappsURL name="/admin/loggingShowLog.inc.jsp?"/><%=COMMAND + "=" + FIRST %>">First</a></th>
	<th width="60"><a class="logCommandButton" href="<isa:webappsURL name="/admin/loggingShowLog.inc.jsp?"/><%=COMMAND + "=" + NEXT %>">Next</a></th>
	<th width="90"><a class="logCommandButton" href="<isa:webappsURL name="/admin/loggingShowLog.inc.jsp?"/><%=COMMAND + "=" + PREVIOUS %>">Previous</a></th>
	<th width="60"><a class="logCommandButton" href="<isa:webappsURL name="/admin/loggingShowLog.inc.jsp?"/><%=COMMAND + "=" + LAST %>">Last</a></th>
	<th>File Name:<br /> <%= Admin.wrap(admin.getLogFileName(), 50) %></th>
	<th width="160">File Size [bytes]: <%= admin.getFileSize() %></th>
	<th width="100">Lines: <%= admin.getNumRows() %></th>
</tr>
</table>
</div>
<table border=1 cellspacing=1 bgcolor="#e0e0e0" class="filter-result" width="100%">
<tr>
	<td colspan=8>
		<table >
			    <% String content = null;
			    	 int linenum = admin.getCurrentRow();
			    	 String lightGrey="#e0e0e0";
			    	 String darkGrey="#c0c0c0";
			     %>
		   <isa:iterate name="logfilecontent" id="cursor" type="com.sap.isa.core.util.table.ResultData" >
		   <tr class="<%= oddEvenClasses[oddEvenClassesIndex++ % 2] %>">
		       <td valign=top><b><%= linenum %></b> &nbsp;</td>
		       <td valign=top><%= cursor.getString(1) %></td>
		       <% linenum++; %>
		   </tr>
		   </isa:iterate>
		</table>
	</td>
<tr>
</table>
</body>
</html>