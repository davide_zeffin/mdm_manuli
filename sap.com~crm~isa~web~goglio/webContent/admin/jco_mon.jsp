<%@ page import="com.sap.mw.jco.JCO, com.sap.mw.jco.JCO.Client , com.sap.isa.core.cache.Cache, java.util.Set, java.util.Map" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Iterator, com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory" %>
<%@ page import="com.sap.isa.core.eai.sp.jco.JCoConnectionStateful" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>


<% if (!AdminConfig.isJCoInfo())
        response.sendRedirect("notsupported.jsp");
        
   String mode = (String)request.getParameter("mode");
   
   if (mode == null)
   	mode = "all";
   
   if (mode.equals("clear")) {
   	JCoConnectionStateful.clearConnectionMonitoring();
   	mode = "all";
   }
   
   String monitoring = (String)request.getParameter("monitoring");
   
   if (monitoring != null && monitoring.equals("off")) {
	   JCoConnectionStateful.setJcoMonitoring(false);
   }   
   else if (monitoring != null && monitoring.equals("on")) {
	   JCoConnectionStateful.setJcoMonitoring(true);
   }   
%>


<html>
  <head>
    <title>Connection Monitoring</title>
  </head>
  <body>
  
  <h2>Internet Sales Administration</h1>




</p>
<a href="jco_mon.jsp?mode=all">Show all</a>
<a href="jco_mon.jsp?mode=connected">Show connected</a>
<br/>
<% if (JCoConnectionStateful.isJcoMonitoring()) { %>
	<a href="jco_mon.jsp?monitoring=off">Switch off Monitoring</a>
<% }
   else if (!JCoConnectionStateful.isJcoMonitoring()){ %>
	<a href="jco_mon.jsp?monitoring=on" title="Caution in a productiv system this feature runs into a memory leak!!">Switch on Monitoring</a>
<% } %>   	

  <h3>JCo Connection Statistic: Stateful Connection</h3>

<%
   Map conStateful = JCoConnectionStateful.getConnectionHistory();
   int rowNum = 0;
%>
 
  <table border="1" cellspacing="0" cellpadding="3">
    <tr bgcolor="#a0a0a0">
      <td>Key (connection object id)</td>
      <td>Session Id</td>
      <td>Status</td>      
    </tr>

<%

   for (Iterator iter = conStateful.keySet().iterator(); iter.hasNext();) {
   	String key = (String)iter.next();
   	JCoConnectionStateful.ConnectionMonitoringContainer cc = (JCoConnectionStateful.ConnectionMonitoringContainer)conStateful.get(key);
   	String sessionId  = cc.getSessionId();
   	String state  = cc.getState();
   	if (mode.equals("all") || (mode.equals("connected") && state.equals(JCoManagedConnectionFactory.CON_STATUS_CONNECTED))) {
   		rowNum++;
		out.println("<tr><td>");
		out.println(key);
		out.println("</td><td>");
		out.println(sessionId);
		out.println("</td><td>");
		out.println(state);
		out.println("</td></tr>");
   	}
      }
   
 %>
   </table>
<%
	out.println("<b>rows in table: " + rowNum + "</b></P>");
%>
  </body>
</html>
