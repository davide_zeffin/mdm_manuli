<%@ page import="com.sap.isa.core.cache.Cache" %>
<%@ page import="java.util.Iterator, com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory" %>
<%@ page import="com.sap.isa.core.xcm.ConfigContainer" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="java.util.Map, com.sap.isa.core.*" %>
<%@page import="java.net.URLEncoder, java.net.URL , com.sap.isa.core.util.XMLHelper" %>
<%@page import="org.w3c.dom.Document, java.net.MalformedURLException" %>
<%@page import="com.sap.isa.core.xcm.ExtendedConfigManager" %>
<%@page import="com.sap.isa.core.xcm.XCMConstants" %>
<%@page import="java.util.Set" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% 
    
    boolean showJCoCache = true;
    String xcmClear = request.getParameter("clear");
    Cache.Access cacheAccess = null;
    try {

      cacheAccess = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
      cacheAccess.remove(xcmClear);
      
    } catch (Cache.Exception ex) {
    }

		

%>


<html>
  <head>
    <title>Java Connector</title>
    
    <style type="text/css">
       .h		{ padding-top:4px ; font-size: 10px; font-family: arial; text-align: left;}
    </style>
    
    
  </head>

	<% 
		UserSessionData userData = 
			(UserSessionData)session.getAttribute(SessionConst.USER_DATA);
		if (userData != null) {
			String confkey = (String)userData.getAttribute(SharedConst.XCM_CONF_KEY);
			//out.println("config key for this session : " + confkey + "<BR/>");
		}
	%>

  <body>
  <a href="MainMonitoring.jsp">REFRESH TABLE</a>
  <table border="1" cellspacing="0" cellpadding="3">
    <tr bgcolor="#a0a0a0">
      <td>Scenario Config Params</td>
      <td>XCM Managed Config Files</td>
    </tr>      
    <%
    	Cache.Access access = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
      Set keys = access.getKeys();
      String key = null;
      Iterator iterator = keys.iterator();
      while(iterator.hasNext()) {
        key = (String)iterator.next();
        String urlKey = URLEncoder.encode(key);
				Object storedConfig = access.get(key); 
        ConfigContainer cc = null;
        Document domDoc = null;
        if (storedConfig instanceof ConfigContainer) {
        	cc = (ConfigContainer)storedConfig;
        } else {
        	domDoc = (Document)storedConfig;
        }
        
       	StringTokenizer st = new StringTokenizer(key, "#");
        out.println("<tr>");
        out.println("<td>");
        
        out.println("<table border=1>");
					if (cc != null) {
		       	while(st.hasMoreTokens()) {
				        out.println("<tr>");
								out.println("<td>");
					   		out.println(st.nextElement());
				        out.println("</td>");
								out.println("</tr>");
	  	     	}
	  	    } else {
	  	    	try {
	  	    		int index = key.lastIndexOf("/");
	  	    		out.println(key.substring(index, key.length()));
	  	    	} catch (MalformedURLException murlex) {
	  	    		out.println("ERROR");
	  	    	}
	  	    	
	  	    }
        out.println("</table>");
        out.println("<a href='xcm_stats.jsp?clear=" + urlKey + "'>clear entry</a>");
        out.println("</td>");
        out.println("<td>");  
        out.println("<table border=1>");
		       	if (cc != null) {
		       
			        Map configs = cc.getConfigs();
			        for (Iterator iter = configs.keySet().iterator(); iter.hasNext();) {
				        out.println("<tr font>");
								out.println("<td class='h'>");
								String path = (String)iter.next();
								String encodedPath = URLEncoder.encode(path);
								String encodedKey = URLEncoder.encode(key);
								out.println("<a target='_new' href='xcm_config.jsp?href=" + encodedPath + "&key=" + encodedKey + "'>" + path + "</a>");
			        	out.println();
				        out.println("</td>");
								out.println("</tr>");
			        }
			      } else {
			      	out.println(key);
			      }
        out.println("</table>");        
        out.println("</td>");

        out.println("</tr>");
      }
    %>
	</table>
	
  </body>
</html>