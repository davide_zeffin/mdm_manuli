<%@ page import="com.sapportals.htmlb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.Constants"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.BackForm"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="/htmlb" prefix="hbj" %>

<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<hbj:form id="backForm"
          method="post"
          action="backFormClick.do"
          >
  <style>
        .backgrd  {
                background: none #FFFFFF; none #667186;
        }
        .headLink {
                background: none #DCE3EC; color: #225A8D; font-weight: bold;
        }
        .logo {
                background: none #FFFFFF;
        }
        BODY  {
          margin-top : 0;
          margin-left : 0;
        }
  </style>
  <table  cellpadding="2"
          cellspacing="0"
          border="0"
          align="right"
          width="100%"> 
    <tr>
      <td valign="top">
        <div class="backgrd" style="height:60;" >
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td class="headLink"><img src="mimes/logo.gif" width="43" height="21" alt="SAP" border="0"></td>
                  <td class="headLink"><img src="mimes/headerLineEnd.gif" width="9" height="21" alt="" border="0"></td>
                  <td style="vertical-align:middle;" class="headLink">&nbsp;</td>
                  <td width="75%" class="headLink" align="right">
                  <%
                    String backURL = (String)session.getAttribute("com.sap.isa.services.schedulerservice.ui.Back");
                    if (backURL != null)
                    {
                  %>
                    <hbj:link id="back"
                              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, Constants.BackLink, session)%>"
                              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, Constants.BackLink, session)%>"
                              onClick="<%=BackForm.BackButtonOnClick%>"
                          />
                  <%
                    } else {
                  %>
                  
				  <hbj:link id="back"
						text="<%= WebUtil.translate(Constants.SchedulerResourceBundle, Constants.BackToAdminLink, session)%>"
							reference="../index.htm"/>
                  <%
                    }
                  %>                  
                  </td>
                </tr>
                <tr>
                  <td class="headLink" colspan="3" align="left">
                      Welcome <%=(request.getUserPrincipal()==null)?"Guest" :request.getUserPrincipal().getName()%>
                      <BR>
                      <%SimpleDateFormat df = new SimpleDateFormat("EEE, MMM d, yyyy hh:mm");%>
                      <%=df.format(new Date(System.currentTimeMillis()))%>
                  </td>
                  <td class="headLink" colspan="1" align="right">
                 <hbj:image
		           tooltip="Head Link"
		           width="750"
		           height="41"
		           src="mimes/shopHeader.gif"
		           alt="" />
                  </td>
                </tr>
              </tbody>
            </table>
        </div>
      </td>
    </tr>
  </table>
</hbj:form>
