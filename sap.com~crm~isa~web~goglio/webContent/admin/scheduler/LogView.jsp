<%@ page import="com.sapportals.htmlb.*"%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.LogViewForm"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.model.LogTableModel"%>
<%@ page import="com.sap.isa.services.schedulerservice.JobExecutionHistoryRecord"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.Constants"%>
<%@ taglib uri="/htmlb" prefix="hbj" %>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<%
  JobExecutionHistoryRecord rec = (JobExecutionHistoryRecord)session.getAttribute(Constants.CurrentJobExecution);
  LogTableModel tmodel = new LogTableModel((Locale)session.getAttribute(Constants.Locale));
  if (rec != null)
  {
    try
    {
      tmodel.setJobExecutionHistory(rec);
    }
    catch(Throwable th)
    {
    }
  }
  pageContext.setAttribute("LogViewForm_LogTable", tmodel);
%>
<hbj:form id="logViewForm" method="post" action="logView.do">
  <div style="position:relative;width:100%;height:480;overflow:auto;">
    <hbj:tableView  id = "logTable"
                    model = "LogViewForm_LogTable"
                    design = "ALTERNATING"
                    headerVisible = "true"
                    footerVisible = "false"
                    fillUpEmptyRows = "true"
                    width="100%"
                    headerText = "<%=LogViewForm.LogTableHeader%>"
                    >
    </hbj:tableView>
  </div>
  <hbj:button id="back"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, LogViewForm.BackButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, LogViewForm.BackButtonTooltip, session)%>"
              onClick="<%=LogViewForm.BackButtonOnClick%>"  />
</hbj:form>

