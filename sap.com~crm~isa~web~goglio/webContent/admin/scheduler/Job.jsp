<%@ page import="com.sapportals.htmlb.*"%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.sap.isa.services.schedulerservice.Job"%>
<%@ page import="com.sap.isa.services.schedulerservice.businessobject.SchedulerServiceInitHandler"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.model.JobExecutionHistoryTableModel"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.model.JobGroupListModel"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.model.ClassListModel"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.Constants"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.WebUtil"%>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<%
  Job job = (Job)session.getAttribute(Constants.CurrentJob);
  JobExecutionHistoryTableModel tmodel = new JobExecutionHistoryTableModel((Locale)session.getAttribute(Constants.Locale));
  if (job != null)
  {
    try
    {
      tmodel.setJobExecutionHistory(job.getExecutionHistory());
    }
    catch(Throwable th)
    {
    	WebUtil.addErrors(session,th);
    }
  }
  request.getSession().setAttribute(JobDetailForm.JobExecutionHistoryTable, tmodel);
  String creationMode = (String)session.getAttribute(Constants.JobCreationMode);
  if (creationMode == null)
    creationMode = Constants.ViewJobMode;
  String useGroups = SchedulerServiceInitHandler.getInitializationProperties().getProperty(Constants.UiShowDefinedGroupsOnly);
  String useClasses = SchedulerServiceInitHandler.getInitializationProperties().getProperty(Constants.UiShowDefinedClassessOnly);
  String classListImpl = SchedulerServiceInitHandler.getInitializationProperties().getProperty(Constants.UiClassListImplementor);
  String groupListImpl = SchedulerServiceInitHandler.getInitializationProperties().getProperty(Constants.UiGroupListImplementor);
  if (useGroups.intern() == Boolean.TRUE.toString().intern())
  {
    try
    {
      Class clazz = Class.forName(groupListImpl);
      Object obj = clazz.newInstance();
      pageContext.setAttribute("JobForm_JobGroupList", (JobGroupListModel)obj);
    }
    catch(Throwable th)
    {
      pageContext.setAttribute("JobForm_JobGroupList", new JobGroupListModel());
    }
  }
  else
  {
    pageContext.setAttribute("JobForm_JobGroupList", new JobGroupListModel());
  }
  if (useClasses.intern() == Boolean.TRUE.toString().intern())
  {
    try
    {
      Class clazz = Class.forName(classListImpl);
      Object obj = clazz.newInstance();
      pageContext.setAttribute("JobForm_JobClassList", (ClassListModel)obj);
    }
    catch(Throwable th)
    {
      pageContext.setAttribute("JobForm_JobClassList", new ClassListModel());
    }
  }
  else
  {
    pageContext.setAttribute("JobForm_JobClassList", new ClassListModel());
  }
%>

<hbj:form id="jobDetailForm1" method="post" action="jobDetail.do">
       <%@ include file="scheduleErrors.jsp"%>
<%
  if (creationMode.intern() == Constants.DefineJobGroupAndClassMode.intern())
  {
%>
  <table>

    <tr>
      <td>
        
        <hbj:group  id="jobClass"
                    title="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.JobGroup, session)%>"
                    design="SAPCOLOR"
                    width="100%"
                    tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.JobGroupTooltip, session)%>">
          <table>
            <tr>
              <td>
			<% String labeledComponentClass = "InputField";
			if (useClasses.intern() == Boolean.TRUE.toString().intern())
				labeledComponentClass = "DropDownListBox";
			%>
			
			<hbj:label
			         id="textJobNewClass"
			         required="TRUE"
			         text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.NewClass, session)%>"
			         design="LABEL" labelFor="inputJobClass" labeledComponentClass="<%=labeledComponentClass %>">
			  <%
				if (useClasses.intern() == Boolean.TRUE.toString().intern())			  	
			  		textJobNewClass.setLabeledComponentId("jobClassList");
			  %>
			  </hbj:label>
              </td>
              <td>
              <%
                if (useClasses.intern() == Boolean.TRUE.toString().intern())
                {
              %>
                <hbj:dropdownListBox  id = "jobClassList"
                                      model = "JobForm_JobClassList"
                />
              <%
                }
                else
                {
              %>
                <hbj:inputField id="inputJobClass"
                                value="<%= (job == null) ? Constants.EmptyString : (job.getTaskName() == null) ? Constants.EmptyString: job.getTaskName()%>"
                            />
              <%
                }
              %>
              </td>
            </tr>
            <tr>
              <td>
						<% labeledComponentClass = "InputField";
						if (useGroups.intern() == Boolean.TRUE.toString().intern())
							labeledComponentClass = "DropDownListBox";
							%>

			<hbj:label
			         id="textJobNewGroup"
			         required="FALSE"
			         text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.NewGroup, session)%>"
			         design="LABEL"         labelFor="inputJobGroup"  labeledComponentClass="<%=labeledComponentClass%>"/>
              </td>
              <td>
              <%
                if (useGroups.intern() == Boolean.TRUE.toString().intern())
                {
              %>
                <hbj:dropdownListBox  id = "jobGroupList"
                                      model = "JobForm_JobGroupList"
                />
              <%
                }
                else
                {
              %>
                <hbj:inputField id="inputJobGroup"
                                value="<%= (job == null)? Constants.EmptyString : job.getGroupName()%>"
                            />
              <%
                }
              %>
              </td>
            </tr>
          </table>
        </hbj:group>
      </td>
    </tr>
  </table>
<%
  }
  if (!(creationMode.intern() == Constants.DefineJobGroupAndClassMode.intern()))
  {
%>
  <hbj:tabStrip id="jobDetailTabStrip"
                width="640"
                bodyHeight="<%=Constants.DisplayHeight%>"
                verticalAlignment="top"
                horizontalAlignment="left">
    <hbj:tabStripItem id="basicProperties" 
                      index="1"
                      title="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.TabBasicProperties, session)%>"
                      tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.TabBasicPropertiesTooltip, session)%>">
      <hbj:tabStripItemBody>
        <%@ include file="JobDetailsBasic.jsp" %>
      </hbj:tabStripItemBody>
    </hbj:tabStripItem>
    <hbj:tabStripItem id="extendedProperties"
                      index="2"
                      title="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.TabExtendedProperties, session)%>"
                      tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.TabExtendedPropertiesTooltip, session)%>">
      <hbj:tabStripItemBody>
        <%@ include file="JobDetailsExtended.jsp" %>
      </hbj:tabStripItemBody>
    </hbj:tabStripItem>
<%
  if (creationMode.intern() == Constants.ViewJobMode.intern())
  {
%>
    <hbj:tabStripItem id="jobExecutionHistory"
                      index="3"
                      title="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.TabExecHistory, session)%>"
                      tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.TabExecHistoryTooltip, session)%>">
      <hbj:tabStripItemBody>
        <%@ include file="JobHistory.jsp" %>
      </hbj:tabStripItemBody>
    </hbj:tabStripItem>
<%
  }
%>
  <%
  	Integer i = (Integer)request.getAttribute(JobDetailForm.TabSelection);
  	if(i!=null)
  		jobDetailTabStrip.setSelection(i.intValue());
  %>
  </hbj:tabStrip>
<%
  }
%>


<%
  if (creationMode.intern() == Constants.DefineJobGroupAndClassMode.intern())
  {
%>
  <hbj:button id="nextButton"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.NextButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.NextButtonTooltip, session)%>"
              onClick="<%=JobDetailForm.NextButtonOnClick%>"
  />
<%
  }
  else
  if (!(creationMode.intern() == Constants.ViewJobMode.intern())
  	&& !(creationMode.intern() == Constants.EditJobMode.intern()) )
  {
%>
  <hbj:button id="prevButton"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.PrevButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.PrevButtonTooltip, session)%>"
              onClick="<%=JobDetailForm.PrevButtonOnClick%>"
  />
  <hbj:button id="defineButton"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.CreateJobButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.CreateJobButtonTooltip, session)%>"
              onClick="<%=JobDetailForm.CreateJobButtonOnClick%>"
  />
  <% } else if (creationMode.intern() == Constants.EditJobMode.intern()) { %>
	  <hbj:button id="saveButton"
		      text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.SaveJobButton, session)%>"
		      tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.SaveJobButton, session)%>"
		      onClick="<%=JobDetailForm.SaveJobButtonOnClick%>"
  />
  <% } %>  
<%
  if (!(creationMode.intern() == Constants.ViewJobMode.intern()))
  {
%>
  <hbj:button id="cancel"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.CancelButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.CancelButtonTooltip, session)%>"
              onClick="<%=JobDetailForm.CancelButtonOnClick%>"  />
<%
  }
  else
  {
%>
  <hbj:button id="back"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.BackButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.BackButtonTooltip, session)%>"
              onClick="<%=JobDetailForm.BackButtonOnClick%>"  />
  <hbj:button id="refresh"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.RefreshButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.RefreshButtonTooltip, session)%>"
              onClick="<%=JobDetailForm.RefreshButtonOnClick%>"  />
  <hbj:button id="edit"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.EditButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.EditButtonTooltip, session)%>"
              onClick="<%=JobDetailForm.EditButtonOnClick%>"  />
  <hbj:button id="run"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.RunNowJobButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.RunNowJobButtonTooltip, session)%>"
              onClick="<%=JobDetailForm.RunNowJobButtonOnClick%>"  />
	<%
	  if(job.getExecutionHistory().size() > 0) {
	%>  
      <hbj:button id="deletejobexechistory"
		  text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.DeleteJobExecHistButton, session)%>"
		  tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.DeleteJobExecHistButtonTooltip, session)%>"
		  onClick="<%=JobDetailForm.DeleteJobExecHistButtonOnClick%>"  />
    <%
      }
    %>	      
<%
  }
%>
</hbj:form>