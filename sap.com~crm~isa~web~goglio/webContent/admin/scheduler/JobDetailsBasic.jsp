<%@ page import="com.sap.isa.services.schedulerservice.ui.Constants"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.WebUtil"%>
<%@ page import="com.sap.isa.services.schedulerservice.Job"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.model.*"%>
<%@ page import="com.sap.isa.services.schedulerservice.JobCycleStateManager"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.TaskDetailsFormConstants"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="com.sapportals.htmlb.ItemList"%>

<%@ taglib uri="/htmlb" prefix="hbj" %>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<%
	
	boolean viewJobMode = (creationMode.intern() == Constants.ViewJobMode.intern());
	WebUtil.bindJobToSession(job,session);
	JobDetailForm jobDetailForm = (JobDetailForm)
				session.getAttribute(Constants.JOBDETAILFORM);
	ScheduledTaskTypeListModel taskTypeListModel =
		new ScheduledTaskTypeListModel(session);
	session.setAttribute("TYPELISTMODEL_ID", taskTypeListModel);
	String jobType = jobDetailForm.getJobType();
	taskTypeListModel.setSelection(jobType);	
	
	JobExecMissedPolicyListModel jobExecMissedPolicyListModel =
		new JobExecMissedPolicyListModel(session);
	session.setAttribute("JOBEXECMISSEDPOLICYLISTMODEL_ID", jobExecMissedPolicyListModel);
	String policy = jobDetailForm.getMisJobExecPolicy();
	jobExecMissedPolicyListModel.setSelection(policy);	

	RecurrencesEndDateTableModel recurEndDateTypeListModel =
		new RecurrencesEndDateTableModel(session);

	boolean isEndDateType = true;
	String recurEndDateType = RecurrencesEndDateTableModel.ENDATE_ID;
	if(job!=null) {
		if(job.getMaxRecurrences() >0){
			isEndDateType = false;
			recurEndDateType = RecurrencesEndDateTableModel.RECURRENCES_ID;
		}
	}
	recurEndDateTypeListModel.setSelection(recurEndDateType);	
	session.setAttribute("RECURENDDATEMODEL_ID", recurEndDateTypeListModel);

	String jobNameFromModel =
		(job == null) ? Constants.EmptyString : job.getName();
	String startDateFromModel = Constants.EmptyString;
	String endDateFromModel = Constants.EmptyString;
	String startTimeFromModel = Constants.EmptyString;
	String endTimeFromModel = Constants.EmptyString;
	String nextExecTimeFromModel = Constants.EmptyString;
	String lastExecTimeFromModel = Constants.EmptyString;
	String noTimesRunFromModel = Constants.EmptyString;
	String typeTxt =
		WebUtil
			.translate(
					Constants.SchedulerResourceBundle,
					TaskTableConstants.TYPE,
					session);
	String typeDetailsTxt =
		WebUtil
			.translate(
					Constants.SchedulerResourceBundle,
					TaskTableConstants.TYPEDETAILS,
					session);

	String recurEndDateTypeTxt =
		WebUtil
			.translate(
					Constants.SchedulerResourceBundle,
					RecurrencesEndDateTableModel.RECURRENCES_ENDDATETYPE,
					session);

	String misJobExecPolicyTxt =
		WebUtil
			.translate(
					Constants.SchedulerResourceBundle,
					JobExecMissedPolicyListModel.JOBEXECMISSEDPOLICYLISTMODELTYPE,
					session);
	String formatPreText =
		WebUtil
			.translate(
					Constants.SchedulerResourceBundle,
					TaskDetailsFormConstants.DATE_FORMAT,
					session);


	String scheduledAtTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.SCHEDULEDAT,
			session);
	String periodTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.PERIOD,
			session);
	String maxRecurrencesTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			RecurrencesEndDateTableModel.RECURRENCES,
			session);


	String statusTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskTableConstants.STATUS,
			session);
	String minTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.MINUTES,
			session);
	periodTxt+=" (" + minTxt+")";
	String startTimeTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.STARTTIME,
			session);
	String startDateTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.STARTDATE,
			session);
	String onceInEveryTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.ONCEINEVERY,
			session);
	
	String daysTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.DAYS,
			session);
	onceInEveryTxt += " (" + daysTxt+")";

	String weeksTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.WEEKS,
			session);
	String onTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			TaskDetailsFormConstants.ON,
			session);
	String monTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			WeekdaysListConstants.MONDAY,
			session);
	String tueTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			WeekdaysListConstants.TUESDAY,
			session);	
	String wedTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			WeekdaysListConstants.WEDNESDAY,
			session);	
	String thuTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			WeekdaysListConstants.THURSDAY,
			session);	
	String friTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			WeekdaysListConstants.FRIDAY,
			session);	
	String satTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			WeekdaysListConstants.SATURDAY,
			session);	
	String sunTxt =
		WebUtil.translate(
			Constants.SchedulerResourceBundle,
			WeekdaysListConstants.SUNDAY,
			session);	
																						

	try {
		if (job != null) {
			jobDetailForm.setTimeZone(TimeZone.getDefault().getDisplayName(false,TimeZone.SHORT));
			DateFormat df = null;
			DateFormat tf = null;
			DateFormat dtf = null;
			if (creationMode.intern() == Constants.ViewJobMode.intern()) {
				df = new SimpleDateFormat(Constants.OutputDateFormat);
				tf = new SimpleDateFormat(Constants.OutputTimeFormat);
				dtf = new SimpleDateFormat(Constants.OutputDateTimeFormat);
			} else {
				df = new SimpleDateFormat(Constants.InputDateFormat);
				tf = new SimpleDateFormat(Constants.InputTimeFormat);
				dtf = new SimpleDateFormat(Constants.InputDateTimeFormat);
			}
			startDateFromModel = df.format(jobDetailForm.getStartDate());
			if (job.getEndDate() != null) {
				endDateFromModel = new SimpleDateFormat(Constants.InputDateFormat).format(job.getEndDate());
				endTimeFromModel = tf.format(job.getEndDate());
				if(viewJobMode)
					endTimeFromModel = endTimeFromModel;
			} else if(viewJobMode){
				endDateFromModel =
					WebUtil.translate(
						Constants.SchedulerResourceBundle,
						JobTableModel.NotRelevant,
						session);
				endTimeFromModel =
					WebUtil.translate(
						Constants.SchedulerResourceBundle,
						JobTableModel.NotRelevant,
						session);
			}
			startTimeFromModel = tf.format(jobDetailForm.getStartTime());
			noTimesRunFromModel = "" + job.getNoOfTimesRun();
			if (job.getNextExecutionTime() != -1)
				nextExecTimeFromModel =
					dtf.format(new Date(job.getNextExecutionTime()));
			else
				nextExecTimeFromModel =
					WebUtil.translate(
						Constants.SchedulerResourceBundle,
						JobTableModel.NotRelevant,
						session);
			if (job.getLastExecutionTime() != -1)
				lastExecTimeFromModel =
					dtf.format(new Date(job.getLastExecutionTime()));
		}
	} catch (Throwable th) {
		WebUtil.addErrors(session,th);
	}
	boolean isVolatile = (job == null) ? false : job.isVolatile();
	boolean isStateful = (job == null) ? false : job.isStateful();
	boolean ff = (job == null) ? false : job.isFixedFrequency();
	boolean logEnabled = (job == null) ? false : job.isLoggingEnabled();
	long maxRecurrences = (job == null) ? 0 : job.getMaxRecurrences();
	String imgSrc = Constants.EmptyString;
	String imgAlt = Constants.EmptyString;
	String imgTooltip = Constants.EmptyString;
	int statusFromModel = -1;
	if (job == null
		|| job.getLastCycleStatus() == JobCycleStateManager.INVALIDSTATE)
		statusFromModel = job.getState();
	else
		statusFromModel = job.getLastCycleStatus();
	imgSrc = WebUtil.getImageForStatus(statusFromModel, session);
	imgAlt = WebUtil.getAlternativeForStatus(statusFromModel, session);
	imgTooltip = WebUtil.getTooltipForStatus(statusFromModel, session);
	String componentClass = "InputField";

%>
<hbj:group 
	id="groupSchedule" 
	title="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.ScheduleGroup, session)%>" 
	design="SAPCOLOR" 
	width="100%" 
	tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.ScheduleGroupTooltip, session)%>">
<table>
	<tr>
		<td>
		<%
			if (creationMode.intern() != Constants.ViewJobMode.intern()) {
			 componentClass = "InputField";
			}
			else componentClass = "TextView";
		%>
		
		<hbj:label
		         id="jobName"
		         required="FALSE"
		         text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.JobName, session)%>"
		         design="LABEL"         labelFor="inputJobName" labeledComponentClass="<%=componentClass %>"
		
		/>

		</td>
		<%
			if (creationMode.intern() != Constants.ViewJobMode.intern()) {
		%>

		<td>
			<hbj:inputField 
				id="inputJobName" 
				value="<%=jobNameFromModel%>" 
				/>
		</td>
		<%
			} else {
		%>

		<td>
			<hbj:textView 
				id="inputJobName" 
				text="<%=jobNameFromModel%>" 
				/>
		</td>
		<%
			}
		%>

	</tr>
	<%
		if (creationMode.intern() == Constants.ViewJobMode.intern()) {
	%>

	<tr>
		<td>
		<hbj:label
		         id="jobGroupName"
		         required="FALSE"
				 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.JobGroupName, session)%>" 
		         design="LABEL"         labelFor="textJobGroupName" labeledComponentClass="TextView"
		
		/>
		</td>
		<td>
			<hbj:textView 
				id="textJobGroupName" 
				text="<%= (job == null) ? Constants.EmptyString : (job.getGroupName() == null) ? Constants.EmptyString: job.getGroupName()%>" 
				/>
		</td>
	</tr>
	<tr>
		<td>
		<hbj:label
		         id="jobClassName"
		         required="TRUE"
				 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.JobClassName, session)%>"  
		         design="LABEL"         labelFor="textImageJobClassName" labeledComponentClass="TextView"
		/>	
		</td>
		<td>
			<%
				String clazzStr =
					(job == null)
						? Constants.EmptyString
						: (job.getTaskName() == null)
						? Constants.EmptyString
						: job.getTaskName();
				String clazzImgStr = WebUtil.getImageForClass(clazzStr);
			%>

			<%
				if (clazzImgStr == null) {
			%>

			<hbj:textView 
				id="textImageJobClassName" 
				text="<%=clazzStr%>" 
				/>
			<%
				} else {
			%>

			<hbj:image 
				id="textImageJobClassName" 
				src="<%=clazzImgStr%>" 
				alt="<%=clazzStr%>" 
				tooltip="<%=clazzStr%>" 
				/>
			<%
				}
			%>

		</td>
	</tr>
	<%
		}
	%>

	<%
		if (creationMode.intern() == Constants.ViewJobMode.intern()) {
	%>

	<tr>
		<td>
			<hbj:label
			         id="lastExecutionTime"
			         required="FALSE"
					 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.LastExecutionTime, session)%>"  
			         design="LABEL"         labelFor="textLastExecutionTime" labeledComponentClass="TextView"
			/>
		</td>
		<td>
			<hbj:textView 
				id="textLastExecutionTime" 
				text="<%=lastExecTimeFromModel%>" 
				/>
		</td>
	</tr>
	<tr>
		<td>
			<hbj:label
			         id="nextExecutionTime"
			         required="FALSE"
					 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.NextExecutionTime, session)%>"  
			         design="LABEL"         labelFor="textNextExecutionTime" labeledComponentClass="TextView"
			/>
		</td>
		<td>
			<hbj:textView 
				id="textNextExecutionTime" 
				text="<%=nextExecTimeFromModel%>" 
				/>
		</td>
	</tr>
	<tr>
		<td>
			<hbj:label
			         id="noOfTimesRun"
			         required="FALSE"
					 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.NumberOfTimesRun, session)%>"  
			         design="LABEL"         labelFor="textNoOfTimesRun" labeledComponentClass="TextView"
			/>
		</td>
		<td>
			<hbj:textView 
				id="textNoOfTimesRun" 
				text="<%=noTimesRunFromModel%>" 
				/>
		</td>
	</tr>
	<tr>
		<td>
			<hbj:label
			         id="status"
			         required="FALSE"
					 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.Status, session)%>"  
			         design="LABEL"         labelFor="imageStatus" labeledComponentClass="Image"
			/>
		</td>
		<td>
			<hbj:image 
				id="imageStatus" 
				src="<%=imgSrc%>" 
				alt="<%=imgAlt%>" 
				tooltip="<%=imgTooltip%>" 
				/>
		</td>
	</tr>
	<%
		}
	%>

	<tr>
		<td>
			<hbj:label
			         id="statefulBox"
			         required="FALSE"
					 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.Stateful, session)%>"  
			         design="LABEL"         labelFor="checkBoxStateful" labeledComponentClass="CheckBox"
			/>
		</td>
		<td>
			<hbj:checkbox 
				id="checkBoxStateful">
				<%
					checkBoxStateful.setChecked(isStateful);
					if (creationMode.intern()
						== Constants.ViewJobMode.intern()) {
						checkBoxStateful.setDisabled(true);
					}
				%>

			</hbj:checkbox>
		</td>
	</tr>
	<tr>
		<td>
			<hbj:label
			         id="volatileBox"
			         required="FALSE"
					 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.Volatile, session)%>"  
			         design="LABEL"         labelFor="checkBoxVolatile" labeledComponentClass="CheckBox"
			/>
		</td>
		<td>
			<hbj:checkbox 
				id="checkBoxVolatile">
				<%
					checkBoxVolatile.setChecked(isVolatile);
					if (creationMode.intern()
						== Constants.ViewJobMode.intern()) {
						checkBoxVolatile.setDisabled(true);
					}
				%>

			</hbj:checkbox>
		</td>
	</tr>
	<tr>
		<td>
			<hbj:label
			         id="fixedfrequency"
			         required="FALSE"
					 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.FixedFrequency, session)%>"  
			         design="LABEL"         labelFor="checkBoxFixedFrequency" labeledComponentClass="CheckBox"
			/>	
		</td>
		<td>
			<hbj:checkbox 
				id="checkBoxFixedFrequency">
				<%
					checkBoxFixedFrequency.setChecked(ff);
					if (creationMode.intern()
						== Constants.ViewJobMode.intern()) {
						checkBoxFixedFrequency.setDisabled(true);
					}
				%>

			</hbj:checkbox>
		</td>
	</tr>
	<tr>
		<td>
			<hbj:label
			         id="logEnabledTextView"
			         required="FALSE"
					 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.LogEnabled, session)%>"  
			         design="LABEL"         labelFor="checkBoxLogEnabled" labeledComponentClass="CheckBox"
			/>
		</td>
		<td>
			<hbj:checkbox 
				id="checkBoxLogEnabled">
				<%
					checkBoxLogEnabled.setChecked(logEnabled);
					if (creationMode.intern()
						== Constants.ViewJobMode.intern()) {
						checkBoxLogEnabled.setDisabled(true);
					}
				%>

			</hbj:checkbox>
		</td>
	</tr>
	<tr>
		<td>
		<%
			if (creationMode.intern() != Constants.ViewJobMode.intern())
				componentClass="InputField";
			else componentClass="TextView";
		%>
		<hbj:label
		         id="startDate"
		         required="TRUE"
				 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.StartDate, session)%>"  
		         design="LABEL"         labelFor="inputStartDate" labeledComponentClass="<%=componentClass %>"
		/>
		</td>
		<%
			if (creationMode.intern() != Constants.ViewJobMode.intern()) {
		%>
		<%
				java.text.SimpleDateFormat f = (java.text.SimpleDateFormat) java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT, (Locale) session.getAttribute(Constants.Locale));
				String dateFormatStr = formatPreText + " " +f.toPattern().toString() ;
		%>
		<td>
			<hbj:inputField
				tooltip="<%= dateFormatStr %>" 
				id="inputStartDate" 
				type="date" 
				value="<%=startDateFromModel%>" 
				showHelp="<%=ui.isAccessible()?Boolean.FALSE.toString():Boolean.TRUE.toString() %>" 
				/>
			
		</td>
		<%
			} else {
		%>

		<td>
			<hbj:textView 
				id="inputStartDate" 
				text="<%=startDateFromModel%>" 
				/>
				
		</td>
		<%
			}
		%>

	</tr>
	<tr>
		<td>
			<%
				String startTimeTxt1 = WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.StartTime, session)+" ("+ 
					 jobDetailForm.getTimeZone()+")";
			%>
		<%
			if (creationMode.intern() != Constants.ViewJobMode.intern())
				componentClass="InputField";
			else componentClass="TextView";
		%>			
			<hbj:label
			         id="startTime"
			         required="TRUE"
					 text="<%=startTimeTxt1%>"  
			         design="LABEL"         labelFor="inputStartTime" labeledComponentClass="<%=componentClass %>"
			/>
		</td>
		<%
			if (creationMode.intern() != Constants.ViewJobMode.intern()) {
				java.text.SimpleDateFormat f = (java.text.SimpleDateFormat) java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT, (Locale) session.getAttribute(Constants.Locale));
				String timeFormatStr = formatPreText + " " +f.toPattern().toString() ;
		%>

		<td>
			<hbj:inputField
				tooltip="<%= timeFormatStr %>"
				id="inputStartTime" 
				type="time" 
				value="<%=startTimeFromModel%>" 
				showHelp="<%=ui.isAccessible()?Boolean.FALSE.toString():Boolean.TRUE.toString() %>">
			</hbj:inputField>
		</td>
		<%
			} else {
		%>

		<td>
			<hbj:textView 
				id="inputStartTime" 
				text="<%=startTimeFromModel%>" 
				/>
		</td>

		<%
			}
		%>

	</tr>
	<tr>
			<td>
				<%-- job execution miss policy --%>
			<%
				if(viewJobMode)
					componentClass = "InputField";
				else componentClass = "DropDownListBox";
			%>				
				<hbj:label
				         id="misJobExecPolicyTxtLabel"
				         required="FALSE"
						 text="<%=misJobExecPolicyTxt %>"  
				         design="LABEL"         labelFor="<%=JobExecMissedPolicyListModel.JOBEXECMISSEDPOLICYLISTMODEL_ID%>"
				         labeledComponentClass="DropDownListBox"
				/>
			</td>
			<td>
				<% if(viewJobMode) { %>
					<hbj:inputField 
						id="<%=JobExecMissedPolicyListModel.JOBEXECMISSEDPOLICYLISTMODEL_ID%>" 
						value="<%= jobExecMissedPolicyListModel.getTextForKey(jobExecMissedPolicyListModel.getSingleSelection()) %>" 
						disabled="true"
						/>
				<% } else { %>
				<hbj:dropdownListBox 
					id="<%=JobExecMissedPolicyListModel.JOBEXECMISSEDPOLICYLISTMODEL_ID%>" 
					model="JOBEXECMISSEDPOLICYLISTMODEL_ID" 
					width="230" 
					disabled="<%= Boolean.toString(viewJobMode)%>"
					/>
				<% } %>					
			</td>
	</tr>
<tr><td></td></tr>
	<tr>
			<td>
			<%
				if(viewJobMode)
					componentClass = "InputField";
				else componentClass = "DropDownListBox";
			%>				
				
				<hbj:label
				         id="recurEndDateTxt"
				         required="FALSE"
						 text="<%=recurEndDateTypeTxt %>"  
				         design="LABEL"         labelFor="<%=RecurrencesEndDateTableModel.RECURRENCES_ENDDATETYPE_ID%>" 
				         labeledComponentClass="DropDownListBox"
				/>
			</td>
			<td>
				<% if(viewJobMode) {%>
				<hbj:inputField 
					id="<%=RecurrencesEndDateTableModel.RECURRENCES_ENDDATETYPE_ID%>" 
					value="<%= recurEndDateTypeListModel.getTextForKey(recurEndDateTypeListModel.getSingleSelection()) %>" 
					disabled="true"
					/>					
				<% } else  { %>
				<hbj:dropdownListBox 
					id="<%=RecurrencesEndDateTableModel.RECURRENCES_ENDDATETYPE_ID%>" 
					model="RECURENDDATEMODEL_ID" 
					width="100" 
					onClientSelect="changeRecurrenceEndDateSelection()" 
					disabled="<%= Boolean.toString(viewJobMode)%>"
					/>
					<% } %>
			</td>
	</tr>
</table>
<table>
	<tr>
		<td>
				<%
				if (isEndDateType) {
				%>
					<div id="endDateOfRecurrenceTableModel" style="display:block;"> 
				<% } else { %>
					<div id="endDateOfRecurrenceTableModel" style="display:none;"> 
				<% } %>
				<%-- End Date type text fields --%>
				<hbj:label
				         id="endDate"
				         required="FALSE"
						 text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.EndDate, session) %>"  
				         design="LABEL"         labelFor="inputEndDate" labeledComponentClass="InputField"
				/>
				<%
				java.text.SimpleDateFormat f = (java.text.SimpleDateFormat) java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT, (Locale) session.getAttribute(Constants.Locale));
				String timeFormatStr = formatPreText + " " +f.toPattern().toString() ;
				f = (java.text.SimpleDateFormat) java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT, (Locale) session.getAttribute(Constants.Locale));
				String dateFormatStr = formatPreText + " " +f.toPattern().toString() ;
				
				%>

			<hbj:inputField 
				tooltip="<%=dateFormatStr %>"
				id="inputEndDate" 
				type="date" 
				value="<%=endDateFromModel%>"
				jsObjectNeeded="true"
				disabled="<%= Boolean.toString(viewJobMode)%>"				 
				showHelp="<%=ui.isAccessible()?Boolean.FALSE.toString():Boolean.TRUE.toString() %>" 
				/>
			<%
				String endTimeTxt1 = WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.EndTime, session)+" ("+ 
					 jobDetailForm.getTimeZone()+")";
			%>
			<hbj:label
			         id="endTime"
			         required="FALSE"
					 text="<%=endTimeTxt1%>"  
			         design="LABEL"         labelFor="inputEndTime" labeledComponentClass="InputField"
			/>	
			<hbj:inputField 
				tooltip="<%=timeFormatStr%>"
				id="inputEndTime" 
				type="time" 
				value="<%=endTimeFromModel%>" 
				disabled="<%= Boolean.toString(viewJobMode)%>"		
				showHelp="<%=(ui.isAccessible()?Boolean.FALSE.toString():Boolean.TRUE.toString()) %>"
				/>
				</div>
				<%
					if (!isEndDateType) {
				%>
					<div id="recurrences" style="display:block;"> 
				<% } else { %>
					<div id="recurrences" style="display:none;"> 
				<% } %>
				<hbj:label
				         id="maxRecurrencesTxtLabel"
				         required="FALSE"
						 text="<%=maxRecurrencesTxt %>"  
				         design="LABEL"         labelFor="<%= JobDetailForm.Recurrences %>" labeledComponentClass="InputField"
				/>	
				<%-- Recurrences type text fields --%>
			<hbj:inputField id="<%= JobDetailForm.Recurrences %>" size="10" type="String"
				jsObjectNeeded="true"
				design="STANDARD" value="<%= Long.toString(maxRecurrences)%>" 
				disabled="<%= Boolean.toString(viewJobMode)%>" /> 
			</div>
		</td>
	</tr>
</table>
<br>
<hbj:group
           id="jobTypeGrp"
           design="SAPCOLOR"
           title="<%=typeDetailsTxt%>"
           tooltip="<%=typeTxt%>"
           width="50%"
           >
<hbj:groupBody>
<table>
		<tr>
			<td>
			<%
				if(viewJobMode)
					componentClass = "InputField";
				else componentClass = "DropDownListBox";
			%>
			<hbj:label
			         id="typeTxtLabel"
			         required="TRUE"
					 text="<%=typeTxt %>"  
			         design="LABEL" labelFor="<%=TaskDetailsFormConstants.TYPELISTBOX_ID%>" labeledComponentClass="DropDownListBox"
			/>			
			</td>
			<td>
				<%
				if(viewJobMode){
				%>
				<hbj:inputField 
					id="<%=TaskDetailsFormConstants.TYPELISTBOX_ID%>" 
					value="<%= taskTypeListModel.getTextForKey(taskTypeListModel.getSingleSelection()) %>" 
					disabled="true"
					/>
				
				<%}
				else {
				%>
				<hbj:dropdownListBox 
					id="<%=TaskDetailsFormConstants.TYPELISTBOX_ID%>" 
					model="TYPELISTMODEL_ID" 
					width="100" 
					onClientSelect="changeSelection()" 
					disabled="<%= Boolean.toString(viewJobMode)%>"
					/>
					<% } %>
			</td>
		</tr>	
</table>
<table>
<tr>
<td>
<!-- Start of the grid layout -->
<hbj:gridLayout id="grid11" cellSpacing="0" cellPadding="2">
<hbj:gridLayoutCell columnIndex="2" rowIndex="2" verticalAlignment="TOP">
<%
	if (jobType.equals(ScheduledTaskTypeListConstants.REGULARLY_ID)) {
%>

<!---- START: toggled area to show regularly type task related inputs ---> 
<div id="regularly" style="display:block;"> 
<% } else {%> 
	<div id="regularly" style="display:none;"> 
	<% } %> 
	<hbj:gridLayout id="grid20" cellSpacing="0" cellPadding="2"> 
		<hbj:gridLayoutCell columnIndex="1" rowIndex="1"> 
			<hbj:label
			         id="periodTxtLabel"
			         required="FALSE"
					 text="<%=periodTxt %>"  
			         design="LABEL" labelFor="<%=TaskDetailsFormConstants.PERIODINPUT_ID%>" labeledComponentClass="InputField"
			/>				
		</hbj:gridLayoutCell> 
		<hbj:gridLayoutCell columnIndex="4" rowIndex="1">
			<hbj:inputField id="<%=TaskDetailsFormConstants.PERIODINPUT_ID%>" size="10" type="String"
			design="STANDARD" value="<%= Long.toString(jobDetailForm.getPeriod())%>"
			jsObjectNeeded="true"
			disabled="<%= Boolean.toString(viewJobMode)%>" /> 
		</hbj:gridLayoutCell>
	</hbj:gridLayout> 
	</div> 
	<%if (jobType.equals(ScheduledTaskTypeListConstants.DAILY_ID)) {%> 
	<!----	START: toggled area to show daily type task related inputs ---> 
	<div id="daily"	style="display:block;"> 
	<% } else {%> 
		<div id="daily" style="display:none;"> 
	<% } %>
	<hbj:gridLayout id="grid21" cellSpacing="2" cellPadding="2"> 
		<hbj:gridLayoutCell columnIndex="1" rowIndex="1"> 
			<hbj:label
			         id="onceInEveryTxtLabel"
			         required="FALSE"
					 text="<%=onceInEveryTxt %>"  
			         design="LABEL" labelFor="<%=TaskDetailsFormConstants.DAYSINPUT_ID%>" labeledComponentClass="InputField"
			/>
		</hbj:gridLayoutCell>
		<hbj:gridLayoutCell columnIndex="4" rowIndex="1"> 
			<hbj:inputField id="<%=TaskDetailsFormConstants.DAYSINPUT_ID%>" size="8" design="STANDARD" value="<%=Long.toString(jobDetailForm.getDays())%>"
			jsObjectNeeded="true" disabled="<%= Boolean.toString(viewJobMode)%>"/> 
		</hbj:gridLayoutCell> 
	</hbj:gridLayout> 
	</div> <!---- END: toggled area to show daily type task related inputs ---> 
	<%if (jobType.equals(ScheduledTaskTypeListConstants.WEEKLY_ID)) {%> 
	<!---- START: toggled area to show weekly type task related inputs ---> 
		<div id="weekly" style="display:block;"> 
	<% } else {%> 
		<div id="weekly" style="display:none;"> 
	<% } %> 
	<hbj:gridLayout id="grid22" cellSpacing="2" cellPadding="2"> 
			<hbj:gridLayoutCell columnIndex="1" rowIndex="1"> 
			<hbj:label
			         id="onceInEveryTxtWeekLabel"
			         required="FALSE"
					 text="<%=onceInEveryTxt %>"  
			         design="LABEL" labelFor="<%=TaskDetailsFormConstants.WEEKSONINPUT_ID%>" labeledComponentClass="InputField"
			/>
			</hbj:gridLayoutCell> 
			<hbj:gridLayoutCell columnIndex="4" rowIndex="1" colSpan="6"> 
				<hbj:inputField id="<%=TaskDetailsFormConstants.WEEKSONINPUT_ID%>" design="STANDARD" size="5" value="<%=Long.toString(jobDetailForm.getWeeks())%>"
				 jsObjectNeeded="true" disabled="<%= Boolean.toString(viewJobMode)%>"/> 
			 	<hbj:textView text="<%=weeksTxt%>" design="STANDARD"/> 
			 	<hbj:textView text="<%= onTxt%>" design="STANDARD"/> 
	 		</hbj:gridLayoutCell> 
		<hbj:gridLayoutCell columnIndex="1" rowIndex="2"> 
			<hbj:checkbox id="<%=TaskDetailsFormConstants.MONDAY_ID%>" text="<%=monTxt %>" checked="<%=Boolean.toString(jobDetailForm.isMonSelected()) %>"
			disabled="<%= Boolean.toString(viewJobMode)%>"/>
		</hbj:gridLayoutCell> 
		<hbj:gridLayoutCell columnIndex="4" rowIndex="2"> 
			<hbj:checkbox id="<%= TaskDetailsFormConstants.TUESDAY_ID%>" text="<%=tueTxt %>" checked="<%=Boolean.toString(jobDetailForm.isTueSelected()) %>" 
			disabled="<%= Boolean.toString(viewJobMode)%>"/> 
		</hbj:gridLayoutCell>
		<hbj:gridLayoutCell columnIndex="7" rowIndex="2"> 
			<hbj:checkbox id="<%= TaskDetailsFormConstants.WEDNESDAY_ID%>" text="<%=wedTxt %>" checked="<%=Boolean.toString(jobDetailForm.isWedSelected()) %>" 
			disabled="<%= Boolean.toString(viewJobMode)%>"/> 
		</hbj:gridLayoutCell>
		<hbj:gridLayoutCell columnIndex="1" rowIndex="3"> 
			<hbj:checkbox id="<%=TaskDetailsFormConstants.THURSDAY_ID%>" text="<%=thuTxt %>" checked="<%=Boolean.toString(jobDetailForm.isThuSelected()) %>"
			disabled="<%= Boolean.toString(viewJobMode)%>"/> 
		</hbj:gridLayoutCell>
		<hbj:gridLayoutCell columnIndex="4" rowIndex="3"> 
			<hbj:checkbox id="<%= TaskDetailsFormConstants.FRIDAY_ID%>" text="<%=friTxt %>" checked="<%=Boolean.toString(jobDetailForm.isFriSelected()) %>"
			disabled="<%= Boolean.toString(viewJobMode)%>" /> 
		</hbj:gridLayoutCell>
		<hbj:gridLayoutCell columnIndex="7" rowIndex="3"> 
			<hbj:checkbox id="<%= TaskDetailsFormConstants.SATURDAY_ID%>" text="<%=satTxt %>" checked="<%=Boolean.toString(jobDetailForm.isSatSelected()) %>"
			disabled="<%= Boolean.toString(viewJobMode)%>" /> 
		</hbj:gridLayoutCell>
		<hbj:gridLayoutCell columnIndex="1" rowIndex="4"> 
			<hbj:checkbox id="<%=TaskDetailsFormConstants.SUNDAY_ID%>" text="<%=sunTxt %>"  checked="<%=Boolean.toString(jobDetailForm.isSunSelected()) %>" 
			disabled="<%= Boolean.toString(viewJobMode)%>"/> 
		</hbj:gridLayoutCell>
	</hbj:gridLayout> 
	</div>
	</hbj:gridLayoutCell> 
	</hbj:gridLayout> 
	</td> </tr> 
	</table>
</hbj:groupBody>
</hbj:group>
<script language="Javascript"> 
	function changeSelection()
	{ 
	if(event.srcElement.selectedIndex == 0) { //period selected
		document.getElementById('regularly').style.display = "block"; 
		document.getElementById('daily').style.display = "none"; 
		document.getElementById('weekly').style.display = "none"; 
		if(getHtmlbUIComponent('<%=TaskDetailsFormConstants.PERIODINPUT_ID%>')){
			document.getElementById(getHtmlbUIComponent('<%= TaskDetailsFormConstants.PERIODINPUT_ID %>').id).focus()			

		}
	} 
	else if(event.srcElement.selectedIndex == 1) 
	{ // Daily schedule selected
		document.getElementById('regularly').style.display = "none"; 
		document.getElementById('daily').style.display = "block"; 
		document.getElementById('weekly').style.display = "none"; 
		if(getHtmlbUIComponent('<%=TaskDetailsFormConstants.DAYSINPUT_ID%>')){
			document.getElementById(getHtmlbUIComponent('<%= TaskDetailsFormConstants.DAYSINPUT_ID %>').id).focus()			
		}
		
	} 
	else if(event.srcElement.selectedIndex == 2) { 
	 	document.getElementById('regularly').style.display = "none"; 
	 	document.getElementById('daily').style.display = "none"; 
	 	document.getElementById('weekly').style.display = "block"; 
		if(getHtmlbUIComponent('<%=TaskDetailsFormConstants.WEEKSONINPUT_ID%>')){
			document.getElementById(getHtmlbUIComponent('<%= TaskDetailsFormConstants.WEEKSONINPUT_ID %>').id).focus()
		}
	 }
	}
	function changeRecurrenceEndDateSelection()
	{ 
		if(event.srcElement.selectedIndex == 1) {  // Recurrences selected
			document.getElementById('recurrences').style.display = "block"; 
			document.getElementById('endDateOfRecurrenceTableModel').style.display = "none"; 
			if(getHtmlbUIComponent('<%= JobDetailForm.Recurrences %>')){
				document.getElementById(getHtmlbUIComponent('<%= JobDetailForm.Recurrences %>').id).focus()
			}
		} 
		else if(event.srcElement.selectedIndex == 0) 
		{  // End Date selected
			document.getElementById('recurrences').style.display = "none"; 
			document.getElementById('endDateOfRecurrenceTableModel').style.display = "block"; 
			if(getHtmlbUIComponent('inputEndDate')){
				document.getElementById(getHtmlbUIComponent('inputEndDate').id).focus()
			}
		}
	}
	
 </script>
</hbj:group>