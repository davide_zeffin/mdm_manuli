<%@ page import="com.sapportals.htmlb.*"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.action.InitAction"%>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%@ page import="com.sapportals.htmlb.*"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.Constants"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.WebUtil"%>
<%@ page import="com.sapportals.htmlb.rendering.IPageContext"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sapportals.htmlb.enum.BrowserType" %>
<%@ page import="com.sapportals.htmlb.rendering.RendererManager" %>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm" %>
<%@ taglib uri="/htmlb" prefix="hbj" %>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<%
  TreeNode root = (TreeNode)session.getAttribute(SchedulerTreeForm.TreeNode);
  
  if (root == null)
  {
    root = new TreeNode("Dummy Node");
    session.setAttribute(SchedulerTreeForm.TreeNode, root);
  }
  boolean readonlymode = false;
  if(session.getAttribute(InitAction.READONLYMODE)!=null)
    readonlymode= ((Boolean)session.getAttribute(InitAction.READONLYMODE)).booleanValue();
 
  pageContext.setAttribute(SchedulerTreeForm.TreeNode, session.getAttribute(SchedulerTreeForm.TreeNode));
%>
<hbj:form id="schedulerTreeForm" method="post" action="schedulerTreeClick.do">
	<%@ include file="scheduleErrors.jsp"%>
  <div style="position:relative;width:800;height:480;overflow:auto;">
	<%-- !!! PLEASE DONOT CHANGE THE ID OF THE TREE!!! --%>
    <hbj:tree id="schedulerTree"
              title="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.Title, session)%>"
              rootNode="<%=SchedulerTreeForm.TreeNode%>"
    />
  </div>
<% if(!readonlymode) {  %>
  <hbj:button id="createJob"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.CreateJobButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.CreateJobButtonTooltip, session)%>"
              onClick="<%=SchedulerTreeForm.CreateJobButtonOnClick%>"  />
<% }  %>	      
  <hbj:button id="deleteSelected"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.DeleteButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.DeleteButtonTooltip, session)%>"
              onClick="<%=SchedulerTreeForm.DeleteButtonOnClick%>"  />
<% if(!readonlymode) {  %>
  <hbj:button id="stopSelected"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.StopSelectedButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.StopSelectedButtonTooltip, session)%>"
              onClick="<%=SchedulerTreeForm.StopSelectedButtonOnClick%>"  />
  <hbj:button id="pauseSelected"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.PauseSelectedButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.PauseSelectedButtonTooltip, session)%>"
              onClick="<%=SchedulerTreeForm.PauseSelectedButtonOnClick%>"  />
  <hbj:button id="resumeSelected"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.ResumeSelectedButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.ResumeSelectedButtonTooltip, session)%>"
              onClick="<%=SchedulerTreeForm.ResumeSelectedButtonOnClick%>"  />
  <hbj:button id="runSelected"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.RunSelectedButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.RunSelectedButtonTooltip, session)%>"
              onClick="<%=SchedulerTreeForm.RunSelectedButtonOnClick%>"  />

<% } %>	      
  <hbj:button id="refresh"
              text="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.RefreshButton, session)%>"
              tooltip="<%=WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.RefreshButtonTooltip, session)%>"
              onClick="<%=SchedulerTreeForm.RefreshButtonOnClick%>"  />
 </hbj:form>