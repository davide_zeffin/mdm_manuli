<%@ page import="com.sapportals.htmlb.*"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.Constants"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.WebUtil"%>
<%@ page import="com.sapportals.htmlb.rendering.IPageContext"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sapportals.htmlb.enum.BrowserType" %>
<%@ page import="com.sapportals.htmlb.rendering.RendererManager" %>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.SchedulerTreeForm" %>

<%@ taglib uri="/htmlb" prefix="hbj" %>
<head>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1"> 
</head>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<%
	BaseUI ui = new BaseUI(pageContext);
	if(ui.isAccessible()) {
			RendererManager.addRenderer(
		TableView.UI_ID, 
			BrowserType.DEFAULT, 
				com.sap.isa.ui.renderer.TableViewRenderer.class); 
	
	}
%>
<hbj:content id="schedulerContent" >
	<%
		schedulerContent.setSection508Rendering(true);
		IPageContext context = schedulerContent;
	%> 
  <hbj:page title = "Scheduler">
	<script language="Javascript"> 
		function getHtmlbUIComponent(htmlbComponentId)
		{
		  	  
		  	  var funcName = htmlb_formid +"_getHtmlbElementId";
	          func = window[funcName];
	          return eval(func(htmlbComponentId));
		}
	 </script>  
    <table cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <td>
          <%@ include file="processing.jsp"%>
          <%@ include file="IncludeToolbar.jsp"%>
        </td>
      </tr>

      <tr>
        <td>
          <%
            String mode = (String)session.getAttribute(Constants.SessionMode);
            if (mode != null)
            {
          %>
            <%
              if (mode.intern() == Constants.TreeMode.intern())
              {
            %>
              <%@ include file="SchedulerTree.jsp"%>
            <%
              }
              else
              if (mode.intern() == Constants.JobMode.intern())
              {
            %>
              <%@ include file="Job.jsp"%>
            <%
              }
              else
              if (mode.intern() == Constants.LogMode.intern())
              {
            %>
              <%@ include file="LogView.jsp"%>
            <%
              }
            %>
          <%
            }
            else if(SchedulerServiceInitHandler.isSchedulerRunning())
            {
          %>
	  <%= WebUtil.translate(Constants.SchedulerResourceBundle, SchedulerTreeForm.InvalidSession, session)%>
          <%
            }
          %>
        </td>
      </tr>
    </table>
  </hbj:page>
</hbj:content>