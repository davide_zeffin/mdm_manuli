<%@ page import="com.sapportals.htmlb.*"%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.sapportals.htmlb.table.ICellRenderer"%>
<%@ page import="com.sap.isa.services.schedulerservice.Job"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.Constants"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.model.JobPropertiesTableModel"%>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<%
  JobPropertiesTableModel tableModel = (JobPropertiesTableModel)session.getAttribute(JobDetailForm.ExtendedPropsTable);
  if (tableModel == null)
  {
    tableModel = new JobPropertiesTableModel((Locale)session.getAttribute(Constants.Locale));
    if (job != null)
    {
      tableModel.setJobProperties(job.getJobPropertiesMap(),job.getTaskClass());
    }
    session.setAttribute(JobDetailForm.ExtendedPropsTable, tableModel);
  }
  tableModel.setEditable(creationMode.intern() != Constants.ViewJobMode.intern());
%>
<%@ taglib uri="/htmlb" prefix="hbj" %>
  <div style="position:relative;width:640;height:480;overflow:auto;">
    <hbj:tableView  id = "propertiesTable"
                    model = "JobDetailForm_PropertiesTable"
                    design = "ALTERNATING"
                    headerVisible = "true"
                    footerVisible = "false"
                    fillUpEmptyRows = "true"
                    width="100%"
                    headerText = "<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.ExtendedPropsTableHeader, session)%>"
                    >
    <%
      propertiesTable.setUserTypeCellRenderer((ICellRenderer)propertiesTable.getModel());
    %>
    </hbj:tableView>
  </div>

