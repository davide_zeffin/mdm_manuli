<%@ page import="com.sapportals.htmlb.*"%>
<%@ page import="com.sapportals.htmlb.table.ICellRenderer"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.model.JobPropertiesTableModel"%>
<%@ taglib uri="/htmlb" prefix="hbj" %>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>  
  <div style="position:relative;width:100%;height:480;overflow:auto;">
    <hbj:tableView  id = "jobExecutionHistoryTable"
                    model = "JobDetailForm_JobExecutionHistoryTable"
                    design = "ALTERNATING"
                    headerVisible = "true"
                    footerVisible = "false"
                    fillUpEmptyRows = "true"
                    width="100%"
                    headerText = "<%=WebUtil.translate(Constants.SchedulerResourceBundle, JobDetailForm.JobExecutionHistoryTableHeader, session)%>"
                    >
    <%
      jobExecutionHistoryTable.setUserTypeCellRenderer((ICellRenderer)jobExecutionHistoryTable.getModel());
    %>
    </hbj:tableView>
  </div>

