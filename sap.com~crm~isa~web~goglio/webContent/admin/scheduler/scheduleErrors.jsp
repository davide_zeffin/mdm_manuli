<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.services.schedulerservice.ui.WebUtil"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.Constants"%>
<%@ page import="com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm"%>
<%@ page import="com.sap.isa.core.util.MessageList"%>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.services.schedulerservice.ui.model.SchedulerErrorTableModel" %>
<%@ page import="com.sapportals.htmlb.rendering.IPageContext"%>
<%@ page import="com.sapportals.htmlb.enum.BrowserType" %>
<%@ page import="com.sapportals.htmlb.rendering.RendererManager" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
	<%	
	if( session.getAttribute(Constants.UIERRORS)!=null ) {
		SchedulerErrorTableModel errorModel = new SchedulerErrorTableModel(request.getLocale());
		errorModel.setErrors((MessageList)session.getAttribute(Constants.UIERRORS));
		pageContext.setAttribute("schedulererrors_model",errorModel);
	%>
	 <hbj:tableView  id = "schedulererrorTable"
	                    model = "schedulererrors_model"
	                    design = "STANDARD"
	                    headerVisible = "false"
	                    footerVisible = "false"
	                    fillUpEmptyRows = "true"
	                    width="500"
	                    >
	    </hbj:tableView>
   <%
	pageContext.removeAttribute("schedulererrors_model");
	session.removeAttribute(Constants.UIERRORS);
   	}
  %>