<%@ page import="com.sap.isa.services.schedulerservice.ui.WebUtil"%>
<%
%>
<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<script language="Javascript">
var _submittedRequest = false;

function _submitRequest()  {
    if(_submittedRequest == true)  {
        eval("htmlbevent.cancelSubmit = true");
        this.returnValue = false;
        return false;
    }  else  {
        _showProcessing();
        _submittedRequest = true;
    }
}

function _showProcessing()
{
  if(document.getElementById('_loading_section') != null)
  {
    document.getElementById('_loading_section').style.pixelLeft = document.body.offsetWidth / 2 - 45;
    document.getElementById('_loading_section').style.pixelTop = document.body.offsetHeight / 2 - 5;
    document.getElementById('_loading_section').style.display = 'block';
  }
}

</script>
<div  class="sapBtnEmph"
      id="_loading_section"
      style="width:75;position:absolute;z-index:230;display:none;">
      <div nowrap class="sapGrpTtlWeb1" style="color:#ffffff;">
      Processing...........<img width='16' heigth='16' src='mimes/clockon.gif'/>
      </div>
</div>
