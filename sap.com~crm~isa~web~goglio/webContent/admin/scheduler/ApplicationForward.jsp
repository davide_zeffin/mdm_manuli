<%@ page import="com.sap.isa.catalog.admin.businessobject.CatalogAdminInitHandler"%>
<%--
Only used as an entry in the welcome filelist in the web.xml file
to redirect every access to the logon and to overwrite tomcat
default behaviour.
--%>

<% if (!com.sap.isa.core.config.AdminConfig.isScheduler()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}

%>
<%
  String backURL = (String)session.getAttribute("com.sap.isa.services.schedulerservice.ui.Back");
%>
<jsp:forward page="<%=backURL%>"/>
