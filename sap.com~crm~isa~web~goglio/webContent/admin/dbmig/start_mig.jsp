<%--
********************************************************************************
    File:         start_mig.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.1.2004
    Version:      1.0

    $Revision: $
    $Date:  $
********************************************************************************
--%> 
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%@ page import="com.sap.isacore.action.dbmig.PrepareDBMigrationAction" %>

<% if (!AdminConfig.isDBMigration())
        response.sendRedirect("notsupported.jsp");
		
	String  sourceDBConnErr =
            (String) request.getAttribute(PrepareDBMigrationAction.RC_SOURCEDB_CONN_ERR);
			
	String  targetDBConnErr =
	        (String) request.getAttribute(PrepareDBMigrationAction.RC_TARGETDB_CONN_ERR);
			
	String  objedtIdNotEmpty =
	        (String) request.getAttribute(PrepareDBMigrationAction.RC_OBJECTID_NOT_EMPTY);
			
	String  clientIdsEmpty =
	        (String) request.getAttribute(PrepareDBMigrationAction.RC_CLIENT_SYSTEMID_EMPTY);
%>

<html>

<head>
<title>Data Migration</title>
<style>

</style>
</head>

<body>

<h2>Migrating data from an ISA database for SAP J2EE Engine 6.20 into a SAP Web Application Server 6.40 database</h2>

<p>
This application supports you in 
migrating the data from the database of an existing ISA-installation running 
against a SAP J2EE Engine 6.20 (or a previous version) into the database of an 
Internet Sales installation for SAP Web Application Server Java 6.40 (or higher). The legacy data will be read from 
the 6.20 database and inserted into the 6.40 database. At the same time data from 
the source database is implicitly modified to be compliant 
with the structure of the target database. The data in the old database is not 
changed during this process, thus the migration might be run several times.</p>

<h3>Prerequisites:</h3>

<ul>
  <li>The target database must already be created.</li>
  <li>Source and target database might not be used during the migration.</li>
  <li>The B2B or B2C application must already have been deployed, so the alias 'jdbc/crmjdbcpool' for the target database is available. For newer versions of the applications, you might have to create the alias yourself.</li>
  <li>A datasource pointing to the old database must be created in WEB AS Java 6.40, with the alias 'jdbc/isaolddb' (Please see Upgrade-Guide for more Details)</li>
  <li>The CRM_ISA_OBJECTID table in the in WEB AS Java 6.40 database must be empty, i. e. contain now rows</li>
</ul>
<div style="color:#f00;">
<% if(sourceDBConnErr != null && sourceDBConnErr.equals("true")) { %>
   Migration impossible. Error in source DB connection. Please check prerequisites.<br>
<% } %>
<% if(targetDBConnErr != null && targetDBConnErr.equals("true")) { %>
   Migration impossible. Error in target DB connection. Please check prerequisites.<br>
<% } %>
<% if(objedtIdNotEmpty != null && objedtIdNotEmpty.equals("true")) { %>
   Error. CRM_ISA_OBJECTID table in target DB is not empty. Please check prerequisites.<br>
<% } %>
<% if(clientIdsEmpty != null && clientIdsEmpty.equals("true")) { %>
   Migration impossible. No Client id entries found in source DB.<br> 
<% } %>
</div><br>
<% if (AdminConfig.isXCMAdmin(request)) { %>
  <a href="<isa:webappsURL name="/admin/dbmig/preparemig.do"/>">
     Go
  </a>
<% } %>
  &nbsp;
  <a href="<isa:webappsURL name="/admin/index.htm"/>" target="parent">
    Back
  </a>
	<%@ include file="/admin/read_only_error.inc" %>
</body>

</html>