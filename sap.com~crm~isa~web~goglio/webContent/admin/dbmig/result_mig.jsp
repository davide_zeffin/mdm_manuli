<%--
********************************************************************************
  File:     result_mig.jsp
  Copyright (c) 2004, SAP AG, Germany, All rights reserved.
  Author:    SAP AG
  Created:   27.1.2004
  Version:   1.0

  $Revision: $
  $Date: $
********************************************************************************
--%> 
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%@ page import="com.sap.isacore.action.dbmig.ShowDBMigrationResultsAction" %>

<% if (!AdminConfig.isDBMigration()) 
    response.sendRedirect("notsupported.jsp");
	
	String migAddrRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_ADDRESS_ROWS);
	String errAddrRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_ADDRESS_ROWS);
	String rejAddrRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_ADDRESS_ROWS);
	
	String migBsktRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_BSKT_ROWS);
	String errBsktRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_BSKT_ROWS);
	String rejBsktRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_BSKT_ROWS);
	
	String migObjIdRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_OBJID_ROWS);
	String errObjIdRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_OBJID_ROWS);
	String rejObjIdRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_OBJID_ROWS);
	
	String migItemRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_ITEM_ROWS);
	String errItemRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_ITEM_ROWS);
	String rejItemRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_ITEM_ROWS);
	
	String migBPRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_BUPA_ROWS);
	String errBPRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_BUPA_ROWS);
	String rejBPRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_BUPA_ROWS);
	
	String migExtConfRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_EXTCONF_ROWS);
	String errExtConfRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_EXTCONF_ROWS);
	String rejExtConfRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_EXTCONF_ROWS);
	
	String migShipToRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_SHIPTO_ROWS);
	String errShipToRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_SHIPTO_ROWS);
	String rejShipToRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_SHIPTO_ROWS);
	
	String migExtDatHeadRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_EXTDATHEAD_ROWS);
	String errExtDatHeadRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_EXTDATHEAD_ROWS);
	String rejExtDatHeadRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_EXTDATHEAD_ROWS);
	
	String migExtDatItemRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_EXTDATITEM_ROWS);
	String errExtDatItemRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_EXTDATITEM_ROWS);
	String rejExtDatItemRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_EXTDATITEM_ROWS);
	
	String migTextRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIG_TEXT_ROWS);
	String errTextRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_ERR_TEXT_ROWS);
	String rejTextRows = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_REJ_TEXT_ROWS);
	
	String migFinished = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIRATION_FINISHED);
	
	String migDuration = (String) request.getAttribute(ShowDBMigrationResultsAction.RC_MIRATION_DURATION);
%>

<script language="JavaScript">
	function refresh() {
	  location.href='<isa:webappsURL name="/admin/dbmig/resultmig.do"/>';
	}
</script>

<html>

<head>
<title>Migration Results</title>
</head>

<body>
<h2>Migration Results</h2>
<b><%= migObjIdRows%></b> row(s) transferred from the Objectids table, <font color="#FF0000"> <b><%= rejObjIdRows%></b> row(s) with duplicate keys (not transferred), <b><%= errObjIdRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migBsktRows%></b> row(s) transferred from the Baskets table, <font color="#FF0000"> <b><%= rejBsktRows%></b> row(s) with duplicate keys (not transferred), <b><%= errBsktRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migItemRows%></b> row(s) transferred from the Items table, <font color="#FF0000"> <b><%= rejItemRows%></b> row(s) with duplicate keys (not transferred), <b><%= errItemRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migBPRows%></b> row(s) transferred from the Businesspartners table, <font color="#FF0000"> <b><%= rejBPRows%></b> row(s) with duplicate keys (not transferred), <b><%= errBPRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migExtConfRows%></b> row(s) transferred from the ExtConfig table, <font color="#FF0000"> <b><%= rejExtConfRows%></b> row(s) with duplicate keys (not transferred), <b><%= errExtConfRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migShipToRows%></b> row(s) transferred from the Shiptos table, <font color="#FF0000"> <b><%= rejShipToRows%></b> row(s) with duplicate keys (not transferred), <b><%= errShipToRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migAddrRows%></b> row(s) transferred from the Addresses table, <font color="#FF0000"> <b><%= rejAddrRows%></b> row(s) with duplicate keys (not transferred), <b><%= errAddrRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migExtDatHeadRows%></b> row(s) transferred from the Extdataheader table, <font color="#FF0000"> <b><%= rejExtDatHeadRows%></b> row(s) with duplicate keys (not transferred), <b><%= errExtDatHeadRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migExtDatItemRows%></b> row(s) transferred from the Extdataitem table, <font color="#FF0000"> <b><%= rejExtDatItemRows%></b> row(s) with duplicate keys (not transferred), <b><%= errExtDatItemRows%></b> errorneous row(s) (not transferred)</font><br>
<b><%= migTextRows%></b> row(s) transferred from the Texts table, <font color="#FF0000"> <b><%= rejTextRows%></b> row(s) with duplicate keys (not transferred), <b><%= errTextRows%></b> errorneous row(s) (not transferred)</font><br>
<br>
<b>Please check the logfiles for rows that were not transferred.</b>
<br><br>
<% if (migFinished != null && migFinished.equals("true")) { %>
 <a href="<isa:webappsURL name="/admin/index.htm"/>">
  Done
 </a>
<% } 
  else {%>
  Please wait - Migration running (duration: <%= migDuration %>).<br>
  Screen will be refreshed every 5 seconds.
  <script language="JavaScript">
   window.setTimeout("refresh()",5000);
  </script>
<% } %>
</body>

</html>
