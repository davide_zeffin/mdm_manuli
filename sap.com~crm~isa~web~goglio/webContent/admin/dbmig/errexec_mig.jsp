<%--
********************************************************************************
    File:         errexec_mig.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      06.2.2004
    Version:      1.0

    $Revision: $
    $Date:  $
********************************************************************************
--%> 
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="java.util.*" %>

<html>
<head>
<title>Error</title>
</head>
<body>
<script language="JavaScript">
alert("Error! Could not start migration. Please check log files.");
</script>
</body>
</html>