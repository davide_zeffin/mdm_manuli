<%--
********************************************************************************
    File:         relation_mig.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.1.2004
    Version:      1.0

    $Revision: $
    $Date:  $
********************************************************************************
--%> 
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%@ page import="com.sap.isacore.action.dbmig.PrepareDBMigrationAction" %>
<%@ page import="com.sap.isacore.action.dbmig.StartDBMigrationAction" %>

<% if (!AdminConfig.isDBMigration())
        response.sendRedirect("notsupported.jsp");
		
   	String  systemIdMissing =
            (String) request.getAttribute(StartDBMigrationAction.RC_SYSTEM_MISSING);
	String  errorLogErr =
            (String) request.getAttribute(StartDBMigrationAction.RC_ERROR_LOG_ERR);
	String  duplicLogErr =
            (String) request.getAttribute(StartDBMigrationAction.RC_DUPLIC_LOG_ERR);
	String  errLogName =
            (String) request.getAttribute(PrepareDBMigrationAction.RC_ERRORNEOUS_LOG);
	String  duplicLogName = 
            (String) request.getAttribute(PrepareDBMigrationAction.RC_DUPLIC_LOG);
%>
<html>

<head>
<title>Clients</title>
</head>

<body>

<h2>Assignment between clients und systems</h2>
For every client that was found in the source 
database, you have to specify a related system (SID). 
<br>
In case the migration is run repeatedly, some 
relationships might already be predetermined and can't be changed.
<br><br>
<form method="POST" action="<isa:webappsURL name="/admin/dbmig/startmig.do"/>" name="relations"">
<table border="0" cellpadding="0" cellspacing="0">
  <thead>
      <th height="13" align="left">
          Client
      </th>
      <th height="13" align="left">
         related system Id
      </th>
  </thead>
  <tbody>
  <% int line = 0;
     String client = ""; 
     String systemId = "";
     boolean changeSystemId = false;
   %>
  <isa:iterate id="entry" name="<%= PrepareDBMigrationAction.RC_CLIENT_SYSTEMID %>" type="java.util.Map.Entry">
   <% client = (String) entry.getKey();
      systemId = (entry.getValue() != null) ? ((String) entry.getValue()) : "";
      line++;
   %>
    <tr>
       <td height="11" align="left" width="33%">
           <%= client %>
		   <input type="hidden" class="textInput" name="client[<%=line%>]" value="<%= client %>" >
       </td>
       <td height="11" align="left" width="66%">
         <% if (systemId.length() == 0) { %>
            <input type="text" name="systemid[<%=line%>]" size="20" maxlength="20" value="<%= systemId %>" >
         <% }
	    else { 
	  %> 
	    <%= systemId %>
	    <input type="hidden" class="textInput" name="systemid[<%=line%>]" value="<%= systemId %>" >
	 <% } %>
       </td>
     </tr>
  </isa:iterate>
  </tbody>
</table>
<br><br>
Please specify name and location for the logfiles:
<br><br>
<table>
   <tbody>
     <tr>
        <td>For rows that caused errors:</td> 
	<td><input type="text" name="errorneouslog" size="80" value="<%=errLogName%>"></td>
     </tr>
     <tr>
        <td>For rows that were rejected, due to duplicate keys:</td>
	<td><input type="text" name="duplicatekeyrow" size="80" value="<%=duplicLogName%>"></td>
    </tr>
   </tbody>
</table>
<br><br>
<div style="color:#f00;">
<% if(systemIdMissing != null && systemIdMissing.equals("true")) { %>
   Error. Please specify a system id for al clients.<br>
<% } %>
<% if(errorLogErr != null && errorLogErr.equals("true")) { %>
   Error. Could not create log file for errorneous rows.<br>
<% } %>
<% if(duplicLogErr != null && duplicLogErr.equals("true")) { %>
   Error. Could not create log file for duplicate rows.<br>
<% } %>
</div><br>

    <input type="submit" value="Start Migration" name="migrate">&nbsp;&nbsp;&nbsp; 
    <a href="<isa:webappsURL name="/admin/dbmig/start_mig.jsp"/>">
      Cancel
    </a>
</form>

</body>

</html>