<%--
********************************************************************************
    File:         fs_mig.jsp
    Copyright (c) 2004, SAP AG, All rights reserved.
    Created:      06.02.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/02/06 $
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <title><isa:translate key="b2b.fs.title"/></title>
  </head>
  <frameset rows="0,46" id="migrationFS" border="0" frameborder="0" framespacing="0">
    <frame name="migrate" src='<isa:webappsURL name="/admin/dbmig/execmig.do"/>' frameborder="0">
    <frame name="migrationresults" src='<isa:webappsURL name="/admin/dbmig/resultmig.do"/>' frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
    <noframes>
      <body>&nbsp;</body>
    </noframes>
  </frameset>
</html>