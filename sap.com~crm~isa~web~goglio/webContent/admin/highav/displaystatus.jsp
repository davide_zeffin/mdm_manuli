<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.ccms.action.HighAvailabilityAction" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.sap.isa.core.xcm.XCMTestProperties" %>
<%@ page import="com.sap.isa.core.test.ComponentTestUtils" %>
<%@ page import="com.sap.isa.core.test.ComponentTest" %>
<%@ page import="javax.servlet.http.HttpServletResponse"%>
<isa:contentType />
<html>
   <head>
    <isa:includes/>

      <title><isa:translate key="highav.title"/></title>
   </head>
   <body>
		<h1><isa:translate key="highav.title"/></h1>
		<table>
		<% 
		    boolean allTestStatus=true;	
		    UserSessionData userSessionData =
		            UserSessionData.getUserSessionData(pageContext.getSession());
		     Map allTests=(Map)userSessionData.getAttribute(HighAvailabilityAction.TEST_RESULTS);
		     Iterator tests=null;
		     if( allTests != null )
	           		tests=  allTests.entrySet().iterator();
		     while( tests != null && tests.hasNext())
		     {
		     	 XCMTestProperties component=(XCMTestProperties)((Map.Entry)tests.next()).getValue();
		     	 String bgcolor="#00CC00";
		     	 switch( ComponentTestUtils.getOverAllStatus( component.getTestClass()) )
			 {
				case ComponentTestUtils.OK:  bgcolor="green"; break;
				case ComponentTestUtils.TESTS_FAILED: bgcolor="red"; allTestStatus=false; break;
				case ComponentTestUtils.TESTS_PARTLYEXECUTED: bgcolor="yellow"; allTestStatus=false; break;
				case ComponentTestUtils.TESTS_TIMEOUT: bgcolor="yellow"; allTestStatus=false; break;
			}
		%>
  <tr>
    <td bgcolor="<%=bgcolor%>">
		<%=component.getConfigID() + " (" + component.getComponentID() + ")"%>
		<%if(component.getTestClass().getTimeout()){%> <isa:translate key="ccms.test.timed.out" arg0=""/><%}%>
		<br/>
		<%
			ComponentTest current=component.getTestClass();
			Iterator iter = current.getTestNames().iterator();
			while( iter.hasNext())
			{
				String testName=(String)iter.next();
			%>
				<%=testName%> :&nbsp;<%=(current.getTestResult(testName)?"OK":"ERROR")%><br/>
				<%if(current.getTestResultDescription(testName) != null )
				{ %>
					<%=current.getTestResultDescription(testName)%><br/>
				<%}else{%>
					No Test result Description available
				<%}%>
		<%}%>
   </td>
  </tr>
		<% } %>
</table>
	</p>
	<p><br/></p>
   </body>
</html>
<% if( !allTestStatus && request.getParameter("GUI") == null)
   { 
   	response.setStatus(HttpServletResponse.SC_NOT_FOUND);
   } %>