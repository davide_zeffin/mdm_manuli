<%@ page isErrorPage="true" language="java" autoFlush="true" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isVersion())
        response.sendRedirect("notsupported.jsp");
%>



<isa:contentType />
<html>
  <head>
    <title><isa:translate key="system.admintool.title"/></title>
  <script type="text/javascript" src="admin.js">
  </script>

  </head>

  <body>
  		<div class="filter-result-msg">
			<div class="info">
				<table class="message">
					<tr>
						<td>
							<span><isa:translate key="system.admintool.tool.version.me"/></span>
						</td>
					</tr>
				</table>
			</div>
		</div>
  </body>
</html>
