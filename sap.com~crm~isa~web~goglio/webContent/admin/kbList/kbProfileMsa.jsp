<% if (!com.sap.isa.core.config.AdminConfig.isKBList()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}
%>
<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.spc.remote.client.object.KnowledgeBase"%><%
%><%@ page import = "com.sap.spc.remote.client.object.KBProfile"%><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/admin/kbList/kbProfileMsa.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
%><html><%
    %><head><%
        %><title></title><%
        %><link rel="stylesheet" href="/ipc/mimes/shared/style/stylesheet.css"><%
    %></head><%
    %><body class="ipcBody"><%
        %><form name="currentForm" action="example.do" target="_new" method="post"><%
            %><input type="hidden" name="<%= RequestParameterConstants.KB_LOGSYS %>"><%
            %><input type="hidden" name="<%= RequestParameterConstants.KB_NAME %>"><%
            %><input type="hidden" name="<%= RequestParameterConstants.KB_VERSION %>"><%
            %><input type="hidden" name="<%= RequestParameterConstants.PRODUCT_ID %>"><%
            %><input type="hidden" name="caller" value="msaonlyconfig"><%
            %>
		<%
KnowledgeBase kb = (KnowledgeBase) request.getAttribute( RequestParameterConstants.KNOWLEDGEBASE );
			int colno = 5;
			int rowno = kb == null ? 0 : kb.getProfiles().size();
			String columnNumber = Integer.toString(colno);
			String rowNumber = Integer.toString(rowno);
			String firstRow = Integer.toString(1);
			String lastRow = Integer.toString(rowno);
			String tableTitle = "Profile selection";
		%>
		<%
			if (ui.isAccessible())
			{
		%>
			<a href="#itemstableend" 
				id="itemstablebegin"
				title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
			</a>
		<%
			}
		%>
            <table border="0" cellpadding="0" cellspacing="2"><%
                %><tr><%
                    %><td colspan="4" align="center" style="height:30"><strong>Profile selection</strong></td><%
                %></tr><%
                %><tr><%
                    %><th width="200" align="left" scope="col" title="Name"><strong>Name</strong></th><%
                    %><th width="100" align="left" scope="col" title="Version"><strong>Version</strong></th><%
                    %><th width="100" align="left" scope="col" title="Profil"><strong>Profile</strong></th><%
                    %><th width="40" align="center" scope="col" title="Build"><strong>Build</strong></th><%
                %></tr><%
                if( kb != null ) {
                    for( Iterator profIt = kb.getProfiles().iterator(); profIt.hasNext(); ) {
                        KBProfile kbProfile = (KBProfile) profIt.next();
                        String profileName = kbProfile.getName();
                        String rootObjName = kbProfile.getRootObjName();
                %><tr><%
                    %><td><%
                        %><a href="javascript:document.currentForm.<%= RequestParameterConstants.KB_NAME %>.value='<%= kb.getName()%>';<%
						            %>document.currentForm.<%= RequestParameterConstants.KB_LOGSYS %>.value='<%= kb.getLogsys()%>';<%
                                    %>document.currentForm.<%= RequestParameterConstants.KB_VERSION %>.value='<%= kb.getVersion()%>';<%
                                    %>document.currentForm.<%= RequestParameterConstants.PRODUCT_ID %>.value='<%= rootObjName%>';<%
                                    %>document.currentForm.submit();" title="<%=kb.getName()%>"><%= kb.getName() %></a><%
                    %></td><% 
                    %><td><%= kb.getVersion() %></td><%
                    %><td><%= profileName %></td><%
                    %><td align="center"><%= kb.getBuildNumber() %></td><%
                %></tr><%
                    }
                } else {
                %><tr><%
                    %><td colspan="4">No Knowledge Bases selected or no Profile found</td><%
                %></tr><%
                }
            %></table>
		<%
			// Data Table Postamble
			if (ui.isAccessible())
			{
		%>
				<a id="itemstableend" 
					href="#itemstablebegin" 
					title="<isa:translate key="access.table.end" 
						arg0="<%=tableTitle%>"/>"></a>
		<%
			}
		%>
            <%
            for( Enumeration e = request.getParameterNames(); e.hasMoreElements(); ){
                String parameterName = (String) e.nextElement();
                if( !parameterName.equals( RequestParameterConstants.PRODUCT_ID ) 
                	&& !parameterName.equals(RequestParameterConstants.KB_NAME ) 
                	&& !parameterName.equals(RequestParameterConstants.KB_VERSION ) 
                	&& !parameterName.equals(RequestParameterConstants.CALLER )){
            %><input type="hidden" name="<%= parameterName %>" value="<%= JspUtil.encodeHtml(request.getParameter( parameterName )) %>"><%
                }
            }
        %></form><%
    %></body><%
%></html>
