<% if (!com.sap.isa.core.config.AdminConfig.isKBList()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}
%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.spc.remote.client.object.KnowledgeBase"%><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><isa:moduleName name = "/admin/kbList/kbList.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
%><html><%
    %><head><%
        %><title>IPC Knowledge Base List</title><%
        %><link rel="stylesheet" href="/ipc/mimes/shared/style/stylesheet.css"><%
    %></head><%
    %><body class="ipcBody"><%
        %><form name="currentForm" action="getProfil.do" target="kbProfil" method="post"><%
            %><input type="hidden" name="kbVersion" value=""/><%
            %>
		<%
			List kbList = (List) request.getAttribute( RequestParameterConstants.KNOWLEDGEBASES );
			int colno = 2;
			int rowno = kbList == null ? 0 : kbList.size();
			String columnNumber = Integer.toString(colno);
			String rowNumber = Integer.toString(rowno);
			String firstRow = Integer.toString(1);
			String lastRow = Integer.toString(rowno);
			String tableTitle = " Knowledge Base selection";
		%>
		<%
			if (ui.isAccessible())
			{
		%>
			<a href="#itemstableend" 
				id="itemstablebegin"
				title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
			</a>
		<%
			}
		%>
            <table border="0" cellpadding="0" cellspacing="2" summary="<isa:translate key="access.table.summary" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>"><%
                %><tr><%
                    %><td colspan="2" align="center" style="height:30"><%
                        %><strong> Knowledge Base selection</strong><%
                    %></td><%
                %></tr><%
                %><tr><%
                    %><th width="200" align="left" scope="col" title="Name"><%
                        %><strong>Name</strong><%
                    %></th><%
                    %><th width="100" align="left" scope="col" title="Version"><%
                        %><strong>Version</strong><%
                    %></th><%
                %></tr><%
                if( kbList != null && kbList.size() > 0 ){
                    for( int i = 0; i < kbList.size(); i++ )
                    {
                        KnowledgeBase knowledgeBase = (KnowledgeBase) kbList.get( i );
                %><tr><%
                    %><td><%
                        %><a href="javascript:document.currentForm.<%= RequestParameterConstants.KB_NAME %>.value='<%= knowledgeBase.getName()%>';<%
                                    %>document.currentForm.<%= RequestParameterConstants.KB_VERSION %>.value='<%= knowledgeBase.getVersion()%>';<%
                                    %>document.currentForm.<%= RequestParameterConstants.FORWARD_DESTINATION %>.value='';<%
                                    %>document.currentForm.submit();" title="<%=knowledgeBase.getName()%>"><%
                                    %><%= knowledgeBase.getName() %></a><%
                    %></td><%
                    %><td align="left"><%= knowledgeBase.getVersion() %></td><%
                %></tr><%
                    }
                } else {
                %><tr><%
                    %><td colspan="2">No Knowledge Bases found</td><%
                %></tr><%
                }
            %></table>
		<%
			// Data Table Postamble
			if (ui.isAccessible())
			{
		%>
				<a id="itemstableend" 
					href="#itemstablebegin" 
					title="<isa:translate key="access.table.end" 
						arg0="<%=tableTitle%>"/>"></a>
		<%
			}
		%>
            <%
            for( Enumeration e = request.getParameterNames(); e.hasMoreElements(); ) {
                String parameterName = (String) e.nextElement();
                if( !parameterName.equals( "kbVersion" ) ){
            %><input type="hidden" name="<%= parameterName %>" value="<%= JspUtil.encodeHtml(request.getParameter( parameterName )) %>"><%
                }
            }
        %></form><%
    %></body><%
%></html>
