<% if (!com.sap.isa.core.config.AdminConfig.isKBList()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}
%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.core.util.HttpServletRequestFacade"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.URLEncoder"%><%
%><isa:contentType/><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/admin/kbList/kbMsa.jsp"/><%
request = HttpServletRequestFacade.determinRequest(request);

%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><%

%><html><%
    %><head><%
        %><title> IPC - Knowledge Base Selection [Version 5.0]</title><%
    %></head><%
    %><frameset BORDER="1" FRAMEBORDER="1" FRAMESPACING="0px" rows="120, *" title="<isa:translate key="ipc.kb.frameset"/>"><%
        Enumeration attributes = request.getAttributeNames();
        List attribList = new ArrayList();

        while (attributes.hasMoreElements()) {
            String attribName = (String) attributes.nextElement();
            if (attribName.equals("IPC_HOST") 
                || attribName.equals("IPC_PORT")
                || attribName.equals("DISPATCHER")
                || attribName.equals("ENCODING")) {
                attribList.add(attribName);
            }
        }
        Enumeration paramters = request.getParameterNames();
        StringBuffer parameterString = new StringBuffer();

        while (paramters.hasMoreElements()) {
            String parameter = (String) paramters.nextElement();

            if (attribList.contains(parameter)) {
                attribList.remove(parameter);
            }

            String value = request.getParameter(parameter);
            parameterString.append(parameter);
            parameterString.append("=");
            parameterString.append(URLEncoder.encode(new String(value.getBytes("UTF-8"), "ISO-8859-1")));
            parameterString.append("&");
        }

        for (int i=0; i<attribList.size(); i++) {
            String parameter = (String) attribList.get(i);

            String value = (String) request.getAttribute(parameter);
            parameterString.append(parameter);
            parameterString.append("=");
            parameterString.append(value);
            parameterString.append("&");
        }

        String url = new StringBuffer().append("/admin/kbList/kbHeaderMsa.jsp?").append(parameterString).toString();
        %><frame src="<isa:webappsURL name="<%= url %>"/>" name="kbHeader" scrolling="no" marginwidth="0"
            marginheight="0" title="<isa:translate key="ipc.frameset.kbheader"/>"><%
            %><frameset BORDER="1" FRAMEBORDER="1" FRAMESPACING="0px" cols="50%, 50%"><%
                %><frame name="kbList" src="<isa:webappsURL name="/admin/kbList/kbListMsa.jsp"/>" marginwidth="0"
                    marginheight="0" title="<isa:translate key="ipc.frame.kblist"/>"><%
                %><frame name="kbProfil" src="<isa:webappsURL name="/admin/kbList/kbProfileMsa.jsp"/>" marginwidth="0"
                    marginheight="0" title="<isa:translate key="ipc.frame.kbprofil"/>"><%
            %></frameset><%
    %></frameset><%
%></html>