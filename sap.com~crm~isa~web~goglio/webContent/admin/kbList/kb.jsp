<% if (!com.sap.isa.core.config.AdminConfig.isKBList()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}
%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.core.util.HttpServletRequestFacade"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.URLEncoder"%><%
%><isa:contentType/><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/admin/kbList/kb.jsp"/><%
request = HttpServletRequestFacade.determinRequest(request);

%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><%

%><html><%
	%><head><%
		%><title> IPC - Knowledge Base Selection [Version 5.0]</title><%
	%></head><%
	%><frameset BORDER="1" FRAMEBORDER="1" FRAMESPACING="0px" rows="120, *" title="<isa:translate key="ipc.kb.frameset"/>"><%

		String url = "/admin/kbList/kbHeader.jsp";
		%><frame src="<isa:webappsURL name="<%= url %>"/>" name="kbHeader" scrolling="no" marginwidth="0"
			marginheight="0" title="<isa:translate key="ipc.frameset.kbheader"/>"><%
			%><frameset BORDER="1" FRAMEBORDER="1" FRAMESPACING="0px" cols="50%, 50%"><%
				%><frame name="kbList" src="<isa:webappsURL name="/admin/kbList/kbList.jsp"/>" marginwidth="0"
					marginheight="0" title="<isa:translate key="ipc.frame.kblist"/>"><%
				%><frame name="kbProfil" src="<isa:webappsURL name="/admin/kbList/kbProfil.jsp"/>" marginwidth="0"
					marginheight="0" title="<isa:translate key="ipc.frame.kbprofil"/>"><%
			%></frameset><%
	%></frameset><%
%></html>