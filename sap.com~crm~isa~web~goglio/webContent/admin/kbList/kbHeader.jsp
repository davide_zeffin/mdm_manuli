<% if (!com.sap.isa.core.config.AdminConfig.isKBList()) {
		String errorjsp = com.sap.isa.core.util.WebUtil.getAppsURL(application, request, response, null, "admin/notsupported.jsp", null, null, false);
		response.sendRedirect(errorjsp);
	}
%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page import = "java.util.*"%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/admin/kbList/kbHeader.jsp"/><%

	String language = "EN";
	String kbName = "";

%><html><%
	%><head><%
		%><title></title><%
		%><link rel="stylesheet" href="/ipc/mimes/shared/style/stylesheet.css"><%
	%></head><%
	%><body class="ipcBody"><%
		%><form name="currentForm" action="/ipc/ipc/getKnowledgeBases.do" target="kbList" method="post"><%
			%><input type="hidden" name="forwardDestination" value="list"><%
			%><input type="hidden" name="caller" value="crmproductsimulation"><%
			%><table border="0" cellspacing="0" cellpadding="0"><%
				%><tr><%
					%><td><%
						%><label for="id_kbName">kbName:</label><%
					%></td><%
					%><td><%
						%><input id="id_kbName" type="text" name="kbName" value="<%= JspUtil.encodeHtml(kbName) %>"><%
					%></td><%
					%><td colspan="6"><%
						%>Searchquery for KB Name<%
					%></td><%
				%></tr><%
				%><tr><%
					%><td colspan="8" style="height:5"><%
						%>&nbsp;<%
					%></td><%
				%></tr><%
				%><tr><%
					%><td><%
						%><label for="id_language" title="Mandatory parameter: Language">Language:(*)</label><%
					%></td><%
					%><td><%
						%><input id="id_language" type="text" name="language" value="<%= JspUtil.encodeHtml(language) %>"><%
					%></td><%
					%><td width="20"><%
						%>&nbsp;<%
					%></td><%
					%><td width="20"><%
						%>&nbsp;<%
					%></td><%
					%><td><%
						%>&nbsp;<%
					%></td><%
					%><td><%
						%>&nbsp;<%
					%></td><%
				%></tr><%
				%><tr><%
					%><td colspan="8" align="center"><%
						%><input type="submit" title="<isa:translate key="access.button" arg0="Submit"/>"><%
					%></td><%
				%></tr><%
				%><tr><%
					%><td colspan="8"><%
						%>(*)&nbsp;<isa:translate key="ipc.entryrequired"/><%
					%></td><%
				%></tr><%
			%></table><%
				for (Enumeration e=request.getParameterNames(); e.hasMoreElements();) {
					String parameterName = (String) e.nextElement();
			%><input type="hidden" name="<%= parameterName %>" value="<%= JspUtil.encodeHtml(request.getParameter( parameterName )) %>"><%
				}
		%></form><%
	%></body><%
%></html>