<%@ taglib uri="/isa" prefix="isa"%>
<%@ page import="com.sap.isa.core.config.AdminConfig"%>
<%@ page import="com.sap.isa.core.util.table.ResultData"%>
<%@ page import="com.sap.isa.core.util.table.TableRow"%>
<%@ page import="com.sap.isa.core.util.*"%>
<%@ page import="com.sap.isa.core.logging.*"%>
<%@ page import="com.sap.isa.core.logging.sla.SessionLoggingSupport"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.io.File"%>

<%@ page import="com.sap.isa.isacore.TabStripHelper"%>
<%@ page import="com.sap.isa.isacore.TabStripHelper.TabButton"%>

<%@ page isErrorPage="true" language="java" autoFlush="true"%>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%
BaseUI ui = new BaseUI(pageContext);
%>
<%
	if (!AdminConfig.isLogging())
		response.sendRedirect("notsupported.jsp");

	//STYLESHEET property
	String[] oddEvenClasses = new String[] { "odd", "even" };
	int oddEvenClassesIndex = 0;
	
	// set the variable if the user is an admin
	boolean isAdmin = AdminConfig.isXCMAdmin(request);

	// an object for the messages to display, if needed.
	AdminMessageList messages = new AdminMessageList();
	
	/****************************************************/
	/* All menu entries are defined within this section */
	String activeTab = request.getParameter("tab");
	String sessionLinkClass = "active-last";
	String locLinkClass = "active-first";
	if (activeTab == null) {
		activeTab = "config";
		sessionLinkClass = "inactive-last";
		locLinkClass = "active-first";
	} else {
		if(activeTab.equalsIgnoreCase("session")) {
			sessionLinkClass = "active-last";
			locLinkClass = "inactive-first";
		} else {
			activeTab = "config";
			sessionLinkClass = "inactive-last";
			locLinkClass = "active-first";
		}
	}

	// get admin object if not the first time on this page
	Admin admin = (Admin) session.getAttribute("admin");
	if (admin == null) {
		admin = new Admin();
		session.setAttribute("admin", admin);
	}

	// get location
	// check if the location has changed and set the new one.
	if (request.getParameter("location") != null) {
		if( !admin.getCurrentLocation().equalsIgnoreCase(request.getParameter("location")) ||
			request.getParameter("reload") != null ) {
			admin.setCurrentLocation(request.getParameter("location"));
		}
	} else {
		admin.setCurrentLocation("com.sap.isa");
	}
		
	ResultData logConfig = admin.getCurrentLogConfiguration(null);

	final String MODE_SAVE_CONFIG = "save";
	final String MODE_DEL_CONFIG = "delete";
	final String MODE_NEW_CONFIG = "newdestination";
	final String MODE_EDIT_CONFIG = "edit";
	final String MODE_VIEW_FILES = "showFiles";
	final String MODE_VIEW_LOG = "viewlog";
	
	/****************************************************/
%>

<isa:contentType />
<%@page import="java.io.File"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
<title><isa:translate key="system.admintool.title" /></title>
<script type="text/javascript" src="admin.js">
  </script>

<script type="text/javascript">
	<!--
	  var applInit = false;
	  function startApp()
	  {
	   url = document.startForm.elements.appUrl.value;
	   if (url == "") {
		  alert("Please enter a URL to start your application. Use the following format:\n<protocol>://<server>:<port>/<application>/init.do\ne.g. http://localhost:53000/b2b/init.do");
		  document.startForm.target = "";
		  document.startForm.action = "javascript:startApp();";
		  return;
	   }
	   document.startForm.target = "_blank";
	   document.startForm.action = url; 
	   document.startForm.submit();
	   applInit = true;
	   return true;
	  }
	  function checkFormatter(selBox) {
		if( selBox !=  null && selBox.selectedIndex != 0 ) {
			document.getElementById("pattern").style.display = "none";
		} else {
			document.getElementById("pattern").style.display = "";      	
		}
	  }

	  function startSessionLog() {
	      if(applInit == true) {
	          document.startSessionForm.submit();
	      } else {
	          alert("Please start your application.");
	          return;
	      }
	  }
	  
      function showHelp() {
          var url = '<isa:webappsURL name="/admin/loggingHelp.jsp"/>';
	      var sat = window.open(url, 'Help', 'width=550,height=360,menubar=no,scrollbars=yes,locationbar=no,resizable=yes');
	      sat.focus();
      }	  
	//-->
  </script>


</head>

<body>
<div id="message-content">
  <div class="info">
    <span>More information on logging with E-Commerce can be found in note <b>1090753</b>.</span>
  </div>
</div>

<div class="admin-content">

<!-- Include Tabs -->
<div id="organizer-navigation">
<ul class="navigation-1">
	<li class="<%=locLinkClass%>"><a href="logging.jsp?tab=config">Log Configuration</a></li>
	<li class="<%=sessionLinkClass%>"><a href="logging.jsp?tab=session">Session Logging</a></li>
</ul>
</div>
<br/>
<%--navigator--%>
<% if (activeTab.equals("config")) {

//	############################ content of loggingLogConfig.inc.jsp START ############################
	final String EMPTY = "empty";
	final String EDIT_CONFIG = "editconfig";
	final String VIEW_FILE_LIST = "viewfilelist";
	final String VIEW_LOG = "viewlog";
	final String SESSION_LOG = "sessionlog";
	
	//-------------------------------------
	// START of EDIT CONFIG FUNCTIONALITY
	//-------------------------------------
	String mode = request.getParameter("mode");
	String index = request.getParameter("index");
	
	boolean inChangeMode = false;
	boolean inSaveMode = false;
	boolean inDeleteMode = false;
	boolean inCreateMode = false;
	boolean inViewFilesMode = false;
	boolean inViewLogMode = false;

	if (mode != null) {
		if (mode.equalsIgnoreCase(MODE_EDIT_CONFIG)) {
			inChangeMode = true;
		} else if (mode.equalsIgnoreCase(MODE_SAVE_CONFIG)) {
			inSaveMode = true;
		} else if (mode.equalsIgnoreCase(MODE_DEL_CONFIG)) {
			inDeleteMode = true;
		} else if (mode.equalsIgnoreCase(MODE_NEW_CONFIG)) {
			inCreateMode = true;
		} else if (mode.equalsIgnoreCase(MODE_VIEW_FILES)) {
			inViewFilesMode = true;
		} else if (mode.equalsIgnoreCase(MODE_VIEW_LOG)) {
			// check if all required data is available
			// this is the command and the logfile name.
			String command = request.getParameter("command");
			String viewLogfile = request.getParameter("logfilename");
			String sessionlog = request.getParameter("sessionlog");
			String redirectStr = "loggingShowLog.inc.jsp?command="+ command;

			if(viewLogfile != null) {
			
				if( sessionlog != null) {
					// init the admin object with the necessary file.
					SessionLoggingSupport sessionlogConf = (SessionLoggingSupport) request.getSession().getAttribute("SessionLoggingSupport.sla.logging.core.isa.sap.com");
                    String fullLogFilePath = (String) request.getParameter("logfilepath");
					if(sessionlogConf == null && fullLogFilePath == null) {
						// user wants to view a log for a session that is not active anymore.
						response.sendRedirect("logging.jsp?tab=session&dlerror=true");
					} else {
						if (sessionlogConf != null) {
							admin.initSessionLog(viewLogfile, sessionlogConf.getLogFilePath(), 30);
						} else {
							// should only happen if the session is terminated by the application
							admin.initSessionLog(viewLogfile, fullLogFilePath, 30);
						}
					}
					
				} else {
					// init the admin object with the necessary file.
					admin.init(viewLogfile, 30);
				}

				// forward to loggingShowLog.inc.jsp
				request.getSession().setAttribute("admin", admin); 
				response.sendRedirect(redirectStr);

			} else {
				messages.addErrorMessage(new AdminMessage(AdminMessage.ERROR, "The requested log file is not valid."));
				inViewLogMode = false;
			}
		}
		//----------------------------------------
		// START of SAVE LOG CONFIG FUNCTIONALITY
		//----------------------------------------
	
		if (inDeleteMode) {
			if (index != null && logConfig.absolute(Integer.parseInt(index))) {
				int numLocations = admin.getNumOfLogLacations();
				if(admin.removeFileLog(admin.getCurrentLocation(), logConfig.getString("Destination"))) {
					if(numLocations != admin.getNumOfLogLacations()) {
						messages.addInfoMessage(new AdminMessage(AdminMessage.INFO, "Log configuration deleted successfully."));
					} else {
						messages.addErrorMessage(new AdminMessage(AdminMessage.ERROR, "A configuration cannot be deleted if it has been set on a higher level. Delete the location on the appropriate level (ex. com.sap.isa)."));
					}
				} else {
					messages.addErrorMessage(new AdminMessage(AdminMessage.ERROR, "Error occured when trying to delete Log configuration."));
				}
			}else {
				messages.addErrorMessage(new AdminMessage(AdminMessage.ERROR, "The chosen log index could not be found. Please try again."));
			}
		} 
		
		if (inSaveMode) {
			String rSeverity = request.getParameter("selSeverity");
			String rDestination = request.getParameter("selDest");
			String rLimit = request.getParameter("selLimit");
			String rCount = request.getParameter("selCount");
			String rFormatterType = request.getParameter("selFormatterType");
			String rPattern = request.getParameter("selPattern");
			String rLocationIndex = request.getParameter("selLocationIdx");
			String rConfigName = request.getParameter("selConfigName");
			// boolean array, first one if the change was successful and the other one if we changed something.
			boolean[] saveSuccess = {false, false};
			if(request.getParameter("selLocationIdx") != null) {
				saveSuccess[0] = admin.saveLocationChanges(rLocationIndex, rSeverity, rDestination, rLimit, rCount, rFormatterType, rPattern);
				saveSuccess[1] = true;
			} else if(rConfigName != null) {
				saveSuccess[0] = admin.createNewDestination(rConfigName, rDestination, rSeverity, rLimit, rCount, rFormatterType, rPattern);
				admin.setCurrentLocation(rConfigName);
				saveSuccess[1] = true;
			}
			// did the user made any changes?
			if(saveSuccess[1]) {
				// was the change successful?
				if(saveSuccess[0] && saveSuccess[1]) {
					messages.addInfoMessage(new AdminMessage(AdminMessage.INFO, "Log configuration successfully saved."));
				} else {
					messages.addErrorMessage(new AdminMessage(AdminMessage.ERROR, "Log configuration could not be saved. See log file for more information."));				
				}
			}
		}
		// load the changed configuration again
		logConfig = null;
		logConfig = admin.getCurrentLogConfiguration(null);
	} // end if (mode != null)
	// set parameters for isa tags.
	request.setAttribute("logConfig", logConfig);
%>
<form name="formLocation" action="<isa:webappsURL name="/admin/logging.jsp"/>">
<div class="filter-result">
<input type="hidden" name="reload" value="true"/>
<table> 
	<tr>
	   <th>Location Name</th>		
	</tr>
	<tr>
	  <td>
		<div class="buttons">
			<ul class="buttons-1">
				<li><input type="text" name="location" value="<%=admin.getCurrentLocation()%>" size="50" class="textinput"/></li>
			</ul>
			<ul class="buttons-1">
				<li><a href="#" onclick="document.formLocation.submit();">(Re)Load</a></li>
			</ul>						
			
			<% if(isAdmin) { %>
				<ul class="buttons-1">
					<li><a href="logging.jsp?mode=<%=MODE_NEW_CONFIG %>">Create Configuration</a></li>
				</ul>						
			<% } %>
		</div>
	  </td>	
	 </tr>
	</table> 
</div>
</form>	
<% // check if the log config contains a location of the surrent sessionlog.
   // the functions in this case should be disabled or session log won't work.
   SessionLoggingSupport sessionLogSupp = (SessionLoggingSupport) request.getSession().getAttribute("SessionLoggingSupport.sla.logging.core.isa.sap.com");
   String sessionFilePath = (sessionLogSupp == null) ? "" : sessionLogSupp.getLogFilePath();
   %>
	<div class="filter-result">
		<table> 
			<tr>
			   <th>Location</th> 
			   <th>Effective Severity</th>
			   <th>Destination</th>
			   <th>Path</th>
			   <th>Limit</th>
			   <th>Count</th>
			   <th class="with-button">Formatter Pattern <a href="javascript:showHelp();" class="helpButton">?</a></th>
			   <th>Formatter Type</th>							  
			</tr>
		
			<isa:iterate id="logconfIter" name="logConfig" type="com.sap.isa.core.util.table.ResultData" ignoreNull="true">
							<%
								String strClass = oddEvenClasses[oddEvenClassesIndex++ % 2]; 
								// wrap the destination and path to mach at least a bit the table cell width.
								String strDest = Admin.wrap(logconfIter.getString("Destination"), 50);
								String strPath = Admin.wrap(logconfIter.getString("Path"), 50);
								boolean sessionLogActive = sessionFilePath.equalsIgnoreCase(logconfIter.getString("Path"));
							%>                    						
			<tr class="<%= strClass %>">
				<td>
					<%=JspUtil.encodeHtml(logconfIter.getString("Location"))%>
				</td>
				
				<td>
					<%=JspUtil.encodeHtml(logconfIter.getString("EffectiveSeverity"))%>
				</td>
				<td>
					<%=JspUtil.encodeHtml(strDest)%>
				</td>
				<td>
					<%=JspUtil.encodeHtml(strPath)%>
				</td>
				<td>
					<%=JspUtil.encodeHtml(logconfIter.getString("Limit"))%>
				</td>
				<td>
					<%=JspUtil.encodeHtml(logconfIter.getString("Count"))%>
				</td>
				<td>
					<%=JspUtil.encodeHtml(logconfIter.getString("FormatterPattern"))%>
				</td>
				<td>
					<%=JspUtil.encodeHtml(logconfIter.getString("FormatterType"))%>
					<%if(!sessionLogActive) { %>
						<input type="hidden" name="sessionIndex" value="<%=logconfIter.getRow()%>"/>
					<% } %>
				</td>
				
			</tr>
<%		// if this location is a session log location, don't show the functions
		if(!sessionLogActive) { %>
			<tr class="<%= strClass %>">
				 <td colspan="8">
				 
				 <%
				 /**************************** LINK BUTTONS *******************************/
				 // show files URL
				 String fURL = "logging.jsp?location=" + JspUtil.encodeURL(logconfIter.getString("Location")) + "&mode="+ MODE_VIEW_FILES + "&index=" + logconfIter.getRow();
				 //edit destination URL
				 String eURL = "logging.jsp?location=" + JspUtil.encodeURL(logconfIter.getString("Location")) + "&mode="+ MODE_EDIT_CONFIG +"&index=" + logconfIter.getRow(); 
				 //delete destination URL
				 String dURL = "logging.jsp?location=" + JspUtil.encodeURL(logconfIter.getString("Location")) + "&mode="+ MODE_DEL_CONFIG +"&index=" + logconfIter.getRow(); 
				  %>
	
				 <div class="buttons">
				 
					<% if(isAdmin) { %>
						<ul class="buttons-3">
							<li><a href="<%= eURL %>">Edit Config</a></li>
						</ul>
						<ul class="buttons-3">
							<li><a href="<%= dURL %>">Delete</a></li>
						</ul>
					<% } %>
					<ul class="buttons-3">
						<li><a href="<%= fURL %>">Show Files</a></li>
					</ul>
				   </div>
				 </td>
			</tr>
		<% } %>
			</isa:iterate>
		</table>
		</div>


<%

   //-------------------------------------
   // START of EDIT and CREATE CONFIG FUNCTIONALITY
   //-------------------------------------
   		   
	if(inCreateMode || (inChangeMode && index!=null)){	
		String configHeader = null; %>

		<form action="<isa:webappsURL name="/admin/logging.jsp"/>" method="post">	
	<%
		// set the header
		if(inCreateMode) {
			configHeader = "Create new log configuration: <input width=\"200\" type=\"text\" value=\"" + admin.getCurrentLocation() + "\" class=\"textinput\" name=\"selConfigName\"/>";
		} else {
			configHeader = "Edit Configuration for: " + admin.getCurrentLocation();
		}
	
	%>
<h1><%=configHeader%></h1>

<%
		// set the cursor to the location to be changed.
		if(inChangeMode && !logConfig.absolute(Integer.parseInt(index))){
			messages.addErrorMessage(new AdminMessage(AdminMessage.ERROR, "The chosen log index could not be found. Please choose a valid index."));
		} else {
%>

<div class="filter-result">
	<table> 
		<tr>
		   <th>EffectiveSeverity</th>
		   <th>Path</th>
		   <th>Limit</th>
		   <th>Count</th>				   
		   <th>Formatter Type</th>
		   <th>Formatter Pattern</th>
		</tr>
		<tr>
			<td>
				<select name="selSeverity" class="textinput"> 
					<% String currSeverity = inCreateMode ? "ERROR" : logConfig.getString("EffectiveSeverity"); %>
					<%=admin.getSeverityOptions(currSeverity) %>	
				</select>
		</td>
		<td><input type="text" name="selDest" class="textinput" width="150" 
				   value="<%= (inCreateMode) ? "./log/applications/isa.log" : logConfig.getString("Destination")%>"><br><i>[./log/applications/log_name.log]</i></td>
		<td><input type="text" name="selLimit" class="textinput" value="<%= (inCreateMode) ? "10000000" : logConfig.getString("Limit")%>"> Byte</td>
		<td><input type="text" name="selCount" class="textinput" value="<%= (inCreateMode) ? "10" : logConfig.getString("Count")%>"> Files</td>
		<td><select id="formatterType" name="selFormatterType" class="textinput" onchange="checkFormatter(this)"> 	
				<% 	String[] formatters = {"TraceFormat", "ListFormat", "XMLFormat"}; 
					StringBuffer options = new StringBuffer();
					
					for(int i = 0; i < formatters.length; i++) {
						options.append("<option value=\"" + formatters[i] + "\" ");
						if ((inCreateMode && i == 0) || (inChangeMode && logConfig.getString("FormatterType").equalsIgnoreCase(formatters[i]))) {
							options.append("selected ");
						}
						options.append(">" + formatters[i] + "</option>\n");
					}
				%>
				<%=options.toString() %>
			</select>
		</td>
		<td><input type="text"
					name="selPattern"
					id="pattern"
					class="textinput" 
					value="<%=(inCreateMode) ? "%d,%-3p %t %s %l %m" : logConfig.getString("FormatterPattern")%>"
					style="display:<%=(inCreateMode) ? 
							"" : 
							(logConfig.getString("FormatterType").equalsIgnoreCase("TraceFormat")) ?
								"" :
								"none"%>;"></td>
			</tr> 
			<tr>
				<td colspan="6" align="right">
				<input type=hidden name="mode" value="<%=MODE_SAVE_CONFIG%>"/>
			<% if(inChangeMode) { %>
				<input type=hidden name="selLocationIdx" value="<%=index %>" />
			<% } %>
				<input type=submit value="Save" class="textinput"/>
				</td>
			</tr>
		</table>
		</div>
		</form>

	<%	} // end else if index exists.
	} // end if(inCreateMode || (inChangeMode && index!=null))

   //-------------------------------------
   // END of EDIT and CREATE CONFIG FUNCTIONALITY
   //-------------------------------------

  //-------------------------------------
  // START of VIEW LOG FUNCTIONALITY
  //-------------------------------------
	if(inViewFilesMode) {
		if (index != null && logConfig.absolute(Integer.parseInt(index))) {
	
			ResultData logFileContent = null;
			ResultData logdir = null;
	
			// get the log dir.
			logdir = new ResultData(admin.getLogFilesForLocation(admin.getCurrentLocation(), logConfig.getString("Destination")));
		
			if(logdir!=null){
				// set the attribute for the iterator later
				request.setAttribute("logdir", logdir);
				// set the session parameter, in case the user wants to download a file.
				request.getSession().setAttribute("admin", admin); %>
			 <h1>Log files:</h1>
			 <div class="filter-result">
			 <form action="/admin/logging.jsp" method="get" name="viewLogfile">
				<table border="1" cellspacing="0" cellpadding="3">
				   <tr bgcolor="#e0e0e0">
					 <th>File Name</th>   
					 <th>Size [bytes]</th>
					 <th>Last Modified</th>
				   <% // Theuse should see this column only if log file download is enabled 
					if(AdminConfig.isLogfileDownload()) {%>      
					 <th>Download</th>
					<% } %>
				   </tr>  
				   <isa:iterate id="cursor" name="logdir" type="com.sap.isa.core.util.table.ResultData" >
				    
					<tr class="<%= oddEvenClasses[oddEvenClassesIndex++ % 2] %>">
							<% String encodedFileNamePath = URLEncoder.encode(cursor.getString(4) + "/" + cursor.getString(1));
							   String encodedFileName = URLEncoder.encode(cursor.getString(1));
							   String nextAction = WebUtil.getAppsURL(application, request, response, null, "admin/logging.jsp", null, null, false);
							   String hrefView = "/admin/logging.jsp?location=" + admin.getCurrentLocation() + "&mode="+ MODE_VIEW_LOG + "&command=init&logfilename=" + encodedFileName;
							   session.setAttribute("logfilename.logging.core.isa.sap.com", encodedFileNamePath);
							   String hrefDownload = "";
							   if(AdminConfig.isLogfileDownload())
							   hrefDownload = "/servlet/com.sap.isa.core.logging.CoreAdminServlet?logfilename=" + encodedFileName;%>
						<td><a href="<isa:webappsURL name="<%= hrefView %>"/>"><%= cursor.getString(1) %></a></td>
						<td><%= cursor.getString(2) %></td>    
						<td><%= cursor.getString(3) %></td>
					  <% // The user should see this column only if log file download is enabled 
						if(AdminConfig.isLogfileDownload()) {%>  
						<td><a href="<isa:webappsURL name="<%=hrefDownload %>"/>"><%= cursor.getString(1) %>.zip</a></td>
					  <% } %>
					</tr>
				   </isa:iterate>
				</table>
			</form>
			</div>
			<%}%>
				<%   String key = request.getParameter("key");
		         
	
		} else {
			messages.addErrorMessage(new AdminMessage(AdminMessage.ERROR, "The chosen log index could not be found. Please choose a valid index."));
		} // end if (index != null && logConfig.absolute(Integer.parseInt(index)))
	} // end if(inViewFilesMode)
//-------------------------------------
// END of VIEW LOG FUNCTIONALITY
//-------------------------------------
}//end if activeTab.equals("config")

	//-------------------------------------
	// START of SESSION TRACING
	//-------------------------------------
	if (activeTab.equals("session")) {
		// check if an error occured while downloading the last session log.
		if (request.getParameter("dlerror") != null) {
			messages.addErrorMessage(new AdminMessage(AdminMessage.ERROR, "The requested session log could not be downloaded/shown, because the session is not active anymore.<br />Please only click on \"Start Session Logging\" <b>after</b> you initialized the application (calling the init.do action)."));
		}
		String href;
		
		//for the session log file
		String sessionLogFileName = (String) (
				session.getAttribute("logfilename.sla.logging.core.isa.sap.com") == null ? 
						"" : session.getAttribute("logfilename.sla.logging.core.isa.sap.com"));
		String sessionLogFileNameZip = (sessionLogFileName != null && sessionLogFileName
				.length() > 0) ? sessionLogFileNameZip = sessionLogFileName
				+ ".zip" : "";
		
		String sessionLogFilePath = (String) (
						session.getAttribute("LogFilePath.SessionLoggingSupport.sla.logging.core.isa.sap.com") == null ? 
								"" : session.getAttribute("LogFilePath.SessionLoggingSupport.sla.logging.core.isa.sap.com"));
		String encodedSessionFileName = JspUtil.encodeHtml(sessionLogFileName);
		String encodeSessionLogFilePath = JspUtil.encodeHtml(sessionLogFilePath);
		String hrefViewFile = "/admin/logging.jsp?mode="+ MODE_VIEW_LOG  + "&command=init&logfilename="
				+ encodedSessionFileName + "&sessionlog=true&logfilepath=" + encodeSessionLogFilePath;
		String hrefSessionTraceDownload = "/servlet/com.sap.isa.core.logging.CoreAdminServlet?logfilename="
				+ encodedSessionFileName + "&sessionlog=true&logfilepath=" + encodeSessionLogFilePath;
		
		String appUrl = "http://" + request.getServerName() 
									 + ":" + request.getServerPort() 
									 + request.getContextPath() + "/";  %>
<form name="startForm" method="post" action="javascript:startApp();">
	<div class="filter-result">
	<table  width="640">
	<tr>
	<td width="5%" align="center"><b>1</b></td>
	<td height="40">
		<div class="buttons">
		<ul class="buttons-2">
		<li>
			<input name="appUrl" value="<%= appUrl %>" type="text" size="65"  class="textinput" onchange/>
		</li>
		</ul>
		<ul class="buttons-2">
		<li>							
			<a href="#" onclick="startApp();" >Start Application</a>
		</li>
		</ul>
		</div>
	</td>
	</tr>		
	<tr>
	<td width="5%" align="center">&nbsp;</td>
	<td>Current Session ID: '<%= request.getSession().getId() %>'</td>
	</tr>
	 </table>
  </div>	     
</form>							     

 <form name="startSessionForm" action="<isa:webappsURL name="/admin/sessionLogging.do"/>">
	 <div class="filter-result">     
	 <table width="640">
	 <tr><td width="5%" align="center"><b>2</b></td>
	 <td>
		<div class="buttons">
			<ul class="buttons-2">
			<li>
				<a href="javascript:startSessionLog();">Start Session Logging</a> (anytime after starting application)
			</li>
			</ul>
		</div>    
	 </td>

	 </tr>
	 </table>    

	</div>
 </form>
  <form name="stopSessionForm" action="<isa:webappsURL name="/admin/sessionLogging.do"/>">
	 <input type=hidden name="action" value="stopsessiontrace"/>
	 <div class="filter-result">    
	 <table width="640">
	 <tr><td width="5%" align="center"><b>3</b></td>
	 <td>				 
		<div class="buttons">
			<ul class="buttons-2">
			<li>
				<a href="javascript:document.stopSessionForm.submit();">Stop Session Logging</a> (anytime after starting the session logging)
			</li>
			</ul>
		</div>    
	 </td>

	 </tr>
	 </table>    

	</div>
</form>
<div class="filter-result">
<table width="640>
	<tr bgcolor="#e0e0e0">
	 <th>File Name</th>  
	 <% // The user should see this column only if log file download is enabled 
		if(AdminConfig.isLogfileDownload()) {%>   
	 <th>Download</th>
	 <% } %>
   </tr> 
   <tr> 
	  <td><a href="<isa:webappsURL name="<%=hrefViewFile %>"/>"><%= sessionLogFileName %></a></td>
	  <% // The user should see this column only if log file download is enabled 
		if(AdminConfig.isLogfileDownload()) {%>  
	  <td><a href="<isa:webappsURL name="<%=hrefSessionTraceDownload %>"/>"><%= sessionLogFileNameZip %></a></td>
	  <% } %>
  </tr>    
</table>
</div>
<%	}//end if activeTab.equals("session")

	//-------------------------------------
	// END of SESSION TRACING
	//-------------------------------------

	//-------------------------------------
	// START show messages
	//-------------------------------------

	if(messages.hasErrorMessages() || messages.hasInfoMessages()) {
		String messSeverity[] = null;
		String messStr[] = null;
		// check if the list has info and error messages
		if(messages.hasErrorMessages() && messages.hasInfoMessages()) {
			messSeverity = new String[2];
			messSeverity[0] = AdminMessage.ERROR;
			messSeverity[1] = AdminMessage.INFO;
			
			messStr = new String[2];
			messStr[0] = messages.getErrorMessages();
			messStr[1] = messages.getInfoMessages();
		// check if the list has only error messages.
		} else if(messages.hasErrorMessages()) {
			messSeverity = new String[1];
			messSeverity[0] = AdminMessage.ERROR;
			
			messStr = new String[1];
			messStr[0] = messages.getErrorMessages();
		// the message can only contain info messages.
		} else {
			messSeverity = new String[1];
			messSeverity[0] = AdminMessage.INFO;
			
			messStr = new String[1];
			messStr[0] = messages.getInfoMessages();
		}
%>
</div> <%-- end: class="admin-content" --%>
		<div id="message-content">
		<% for(int i = 0; i < messStr.length; i++) { %>
			<div class="<%=messSeverity[i]%>">
				<span><%=messStr[i]%></span><br/>
			</div>
		<% } %>
		</div>

<%	}
	//-------------------------------------
	//END show messages
	//-------------------------------------
%>

</body>
</html>
