<%@page import="javax.xml.parsers.DocumentBuilder" %>
<%@page import="javax.xml.parsers.DocumentBuilderFactory" %>
<%@page import="org.xml.sax.SAXException" %>
<%@page import="com.sap.isa.core.FrameworkConfigManager" %>
<%@page import="com.sap.isa.core.xcm.ConfigContainer" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>



<title>
Validates XCM files
</title>
<html>
<h3>Validates XCM configuration using DTD</h3>
<%
	String file = request.getParameter("file");
	String xcmadminresult = "";
	String fullPath = null;

	String xcmAdminFile = "/WEB-INF/xcm/sap/system/xcmadmin-config.xml";
	String xcmAdminFullPath = application.getRealPath(xcmAdminFile);

	String xcmISAAdminFile = "/WEB-INF/xcm/sap/system/isa-xcmadmin-config.xml";
	String xcmISAAdminFullPath = application.getRealPath(xcmISAAdminFile);

	
	String xcmsubprojectAdminFile = "/WEB-INF/xcm/sap/system/subproject-xcmadmin-config.xml";
	String subprojectxcmAdminFullPath = application.getRealPath(xcmsubprojectAdminFile);

	

	if (file != null && file.equals("xcmadmin"))
		fullPath = xcmAdminFullPath;
	
	if (file != null && file.equals("subproject-xcmadmin"))
		fullPath = subprojectxcmAdminFullPath;

	if (file != null && file.equals("isa-xcmadmin-config.xml"))
		fullPath = xcmISAAdminFullPath;


	boolean isValid = true;

	if (fullPath != null) {
	
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.inqmy.lib.jaxp.DocumentBuilderFactoryImpl");
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setValidating(true);
		factory.setNamespaceAware(true);
    DocumentBuilder builder = factory.newDocumentBuilder();
    try {
    	builder.parse(fullPath);
    	xcmadminresult = "Document valid";
    } catch (SAXException sex) {
    	xcmadminresult = "ERROR<br/>" + sex.toString();
    	isValid = false;
    }
	}
	
	


%>
<h4>Click on the link to validate</h4>
	<table border=1 width=100%>
		
		<tr>
			<td>
				<b>xcmadmin-config.xml</b>
			</th>
			<td>
				<a href="check_dtd.jsp?file=xcmadmin">			
				<%=xcmAdminFullPath%>
				</a>
			</td>
		</tr>
		<tr>
		<tr>
			<td>
				<b>isa-xcmadmin-config.xml</b>
			</th>
			<td>
				<a href="check_dtd.jsp?file=isa-xcmadmin-config.xml">
				<%=xcmISAAdminFullPath%>
				</a>
			</td>
		</tr>
		<tr>
			<td>
				<b>subproject-xcmadmin-config.xml</b>
			</th>
			<td>
				<a href="check_dtd.jsp?file=subproject-xcmadmin">
				<%=subprojectxcmAdminFullPath%>
				</a>
			</td>
		</tr>
		<tr>		
			<td>
				<b>Result</b>
			</td>
			<td>
				<% if (isValid) {
							out.println(xcmadminresult);
					 }  else {
					 		out.println("<font color='ff0000'>" + xcmadminresult + "</font>");
					 }
				%>				
			</td>
		</tr>
	</table>
</html>