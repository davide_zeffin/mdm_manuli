 
<head><script language="JavaScript" > 
</script>
</head>

<%@ page import="java.util.*" %>
 
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page  import="com.sapportals.htmlb.table.DefaultTableViewModel"%>
<%@ page  import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer"%>
<%@ page  import="com.sap.isa.core.xcm.admin.model.table.HelpItemSummaryTableModel"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.HelpConstants" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>
     
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
 
<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>

 
<%  
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	HelpController mHelpController = appConfigController.getHelpController();
	String scenName = (String)request.getParameter("scenName");
	if (scenName == null)
	    scenName = "R3_YS1_100";
    HelpItem scenDetail = mHelpController.getScenDetailHelpItem(scenName);
    HelpItemContainer optScenParamContainer = mHelpController.getScenParamOptionalContainer(scenName);
    HelpItemContainer manScenParamContainer = mHelpController.getScenParamMandatoryContainer(scenName);

	ComponentConfigController compConfigController = appConfigController.getCompConfigController();
	request.setAttribute("scenName", scenName);
	request.setAttribute("helpController", mHelpController);	

%>
<hbj:content id="xcmAdminContext" >
	<%
		BaseUI helpUi = new BaseUI(pageContext);
		if(helpUi.isAccessible()) {
			xcmAdminContext.setSection508Rendering(true);
		}
	%>
  <hbj:page title = "Application Configuration">
        <hbj:form id="HomeForm">
		<hbj:group id="applicationConfigurationGroup" design="SAPCOLOR" title="XCM Help" tooltip="">

			<hbj:groupBody>  

                <%-- include scenario details --%>
                <% request.setAttribute(HelpConstants.REQ_ATTR_ITEM_DETAIL, scenDetail); %>
                <% request.setAttribute(HelpConstants.REQ_ATTR_ITEM_DETAIL_SHOW_VALUE, HelpConstants.SHOW_NO); %>
                <% request.setAttribute(HelpConstants.REQ_ATTR_ITEM_DETAIL_BASE_CONFIG_NAME, HelpConstants.SHOW_YES); %>
                <% request.setAttribute(HelpConstants.REQ_ATTR_ITEM_CONTAINER_SHOW_LINKS, HelpConstants.SHOW_NO); %>
                 <hbj:textView
                         id="ItemDetailCaptionTextView"
                         text="Configuration Detail"
                         design="HEADER1"
                />                
		  	    <%@ include file="HelpItemDetail.inc" %>  
                <hr/>
                <%-- include mandatory scenario params --%>
                <% 
                    request.setAttribute(HelpConstants.REQ_ATTR_ITEM_CONTAINER, manScenParamContainer); 
                    request.setAttribute(HelpConstants.REQ_ATTR_ITEM_CONTAINER_MODE, HelpItemSummaryTableModel.MODE_SHORTTEXT); 
                    request.setAttribute(HelpConstants.REQ_ATTR_ITEM_CONTAINER_SHOW_LINKS, HelpConstants.SHOW_YES);                    
                %>
                 <hbj:textView
                         id="ItemDetailCaptionTextView"
                         text="Mandatory Configuration Parameter Summary"
                         design="HEADER1"
                />                
                <%@ include file="HelpItemSummary.inc" %>                             
                <hr/>
                <%-- include optional scenario params --%>
                <% 
                    request.setAttribute(HelpConstants.REQ_ATTR_ITEM_CONTAINER, optScenParamContainer); 
                    request.setAttribute(HelpConstants.REQ_ATTR_ITEM_CONTAINER_MODE, HelpItemSummaryTableModel.MODE_SHORTTEXT);                     
                    request.setAttribute(HelpConstants.REQ_ATTR_ITEM_CONTAINER_SHOW_LINKS, HelpConstants.SHOW_YES);                                        
                %>
                 <hbj:textView
                         id="ItemDetailCaptionTextView"
                         text="Optional Configuration Parameter Summary"
                         design="HEADER1"
                />                                
                <%@ include file="HelpItemSummary.inc" %>              
                <hr/>
                <%-- include mandatory scenario params detail --%>
                 <hbj:textView
                         id="ItemDetailCaptionTextView"
                         text="Mandatory Configuration Parameter Detail"
                         design="HEADER1"
                />                                                
                <%
                    Set paramNames = manScenParamContainer.getItemNames();
                    for (Iterator iter = paramNames.iterator(); iter.hasNext();) {
                        String name = (String)iter.next();
                        HelpItem scenParamItem = manScenParamContainer.getItem(name);
                        request.setAttribute("itemDetail", scenParamItem);
                    %>
                        <jsp:include page="HelpAllwedValueDetail.jsp"/>  
                    <%
                    }         
                %>
                
                <hr/>
                <%-- include optional scenario params detail --%>
                 <hbj:textView
                         id="ItemDetailCaptionTextView"
                         text="Optional Configuration Parameter Detail"
                         design="HEADER1"
                />                                                                
                <%
                    paramNames = optScenParamContainer.getItemNames();
                    for (Iterator iter = paramNames.iterator(); iter.hasNext();) {
                        String name = (String)iter.next();
                        HelpItem scenParamItem = manScenParamContainer.getItem(name);
                        request.setAttribute("itemDetail", scenParamItem);
                    %>
                       <jsp:include page="HelpAllwedValueDetail.jsp"/> 
                    <%
                    }         
                
                %>                
                
			</hbj:groupBody>
    	</hbj:group>
      </hbj:form>
  </hbj:page>
</hbj:content>
</body>