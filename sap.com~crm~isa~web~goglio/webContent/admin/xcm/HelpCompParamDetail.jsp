<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer" %>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.HelpConstants" %>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.ControllerUtils"%>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<%@ taglib uri="/htmlb" prefix="hbj" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% 
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	HelpController mHelpController = appConfigController.getHelpController();
	String compId = mHelpController.getCurrentComponentName();
 	HelpItemContainer paramSummaryContainer = mHelpController.getComponentParamContainer(compId); 
%>


<hbj:content id="xcmAdminContext" >
	<%
		BaseUI helpUi = new BaseUI(pageContext);
		if(helpUi.isAccessible()) {
			xcmAdminContext.setSection508Rendering(true);
		}
	%>
  <hbj:page title = "helpItem"> 

    <hbj:form id="xcmHelpItemDetailForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/helpItem.do\" , null , null , false)%>"> 
             <%
                HelpItem helpItem = mHelpController.getCurrentDetailItem();
             %>

            <%
                Set paramNames = paramSummaryContainer.getItemNames();

                for (Iterator iter = paramNames.iterator(); iter.hasNext();) {
                    String name = (String)iter.next();
                    HelpItem compParamItem = paramSummaryContainer.getItem(name);
              			mHelpController.setCurrentDetailItem(compParamItem);                    
             		mHelpController.setCurrentDetailMode(
              				HelpController.MODE_DETAIL_NAME_AS_LINK_DEST, "", "compParam");
              			
                %> 
                <br> 
                <% if(mHelpController.checkCurrentDetailMode(HelpController.MODE_DETAIL_NAME_AS_LINK_DEST)) { %>
             		<% 	String name1 = mHelpController.getCurrentLinkDestPrefix() + name; %>
    				<a name="<%=name1%>" > </a>
    			<% } %>
                <hbj:group id="parameter<%=name%>" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpItemDetailForm.NAME, null) + name %>" tooltip="">
                <hbj:groupBody>
                    <%@ include file="HelpItemDetail.inc" %> 
                    <%-- check if parameter has allowed values --%>
                    <%
                    if (compParamItem.getChildren().getNumItems() > 0) {
                    	HelpItemContainer allowedValues = compParamItem.getChildren();
		                	mHelpController.setCurrentSummaryMode(
		                			HelpController.MODE_SUMMARY_TABLE_SHORTTEXT +
		                			HelpController.MODE_SUMMARY_TABLE_LONGTEXT, "", "");
		                	mHelpController.setCurrentSummaryContainer(allowedValues);
										
										%>
										</BR>
				             <hbj:textView
				                     id="ItemDetailCaptionTextView"
				                     text="Allowed Values Detail"
				                     design="HEADER2"
				            />     
				                          
							<%@ include file="HelpItemSummary.inc" %> 

                    <% } %>
                    
                    
                    </hbj:groupBody>
                    </hbj:group>
                <%
                }         
            %>
             
        </hbj:form>
    </hbj:page>
     
        
</hbj:content>        