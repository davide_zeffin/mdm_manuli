<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.TestResultForm"%>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.table.TestResultTableModel"%>

<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin_RO(request) || !com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% 
 

	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	ComponentConfigController compConfigController = appConfigController.getCompConfigController();
	boolean result = compConfigController.performCurrentCompTest();	
	
	TestResultTableModel testParamsTableModel = new TestResultTableModel(compConfigController);
	pageContext.setAttribute("testParamsTableModel", testParamsTableModel);

	String helpTitle = WebUtil.translate(pageContext, TestResultForm.PAGE_TITLE1, null) + compConfigController.getCurrentTestName() + WebUtil.translate(pageContext, TestResultForm.PAGE_TITLE2, null) ;
	String resultString = compConfigController.getCurrentTestResultDescription();
	resultString.trim();
	String lightHref;
	String result_text;
	if (result) { 
		lightHref = "mimes/ICON_GREEN_LIGHT.gif";
		result_text = WebUtil.translate(pageContext, TestResultForm.SUCCESS, null);
	} else {
		lightHref = "mimes/ICON_RED_LIGHT.gif";
		result_text = WebUtil.translate(pageContext, TestResultForm.FAILED, null);
	}
 
%>


<hbj:content id="xcmHelpDescription" >
	<%
		BaseUI helpdesc = new BaseUI(pageContext);
		if(helpdesc.isAccessible()) {
			xcmHelpDescription.setSection508Rendering(true);
		}
	%>
  <hbj:page title = "<%= helpTitle %>">
		
		
		<hbj:form id="helpForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/compConfig.do\" , null , null , false)%>">		

		<table border=0 height='100%' width='100%'>
			<tr>
				<td align="right">
					 <hbj:button id="closewindow" text="<%=WebUtil.translate(pageContext, TestResultForm.BUTTON_CLOSE, null) %>" onClientClick="close()"/>
				</td>
			</tr>
			<tr>
				<td align="center">
				<% 					
				
				if(!helpdesc.isAccessible()) { %>
					
					<hbj:image
           				id="TrafficLight"
           				tooltip="<%=result_text%>"
          	            src="<%=lightHref%>"
           				alt="<%=WebUtil.translate(pageContext, TestResultForm.LIGHT_ALT1, null) + lightHref + WebUtil.translate(pageContext, TestResultForm.LIGHT_ALT2, null) %>" />
           		<% } %>
           		<hbj:textView text="<%=result_text%>" design = "HEADER1" />
           		</td>
					
			</tr>
 

			<tr height="10%" valign="top">
				<td>
				<hbj:tray    id="testParamsTray"
							title="<%=WebUtil.translate(pageContext, TestResultForm.TRAY1_TEXT, null) %>"
							design="border"
							width="95%"
							isCollapsed="false"
							tooltip="<%=WebUtil.translate(pageContext, TestResultForm.TRAY1_TOOL, null) %>"
				  >
					<hbj:trayBody>
						<hbj:tableView
           					id="testParamsTableView"
           					model="testParamsTableModel"
           					footerVisible="false"
           					design="ALTERNATING"
           					width="100%"
           				/>
					

					</hbj:trayBody>
			
				  </hbj:tray>
			   </td>
			 </tr>
			
				<tr height="80%">
					<td valign='top'>
				
					

				<hbj:tray    id="testResultTray"
							title="<%=WebUtil.translate(pageContext, TestResultForm.TESTMESSAGE_TEXT, null) %>"
							design="border"
							width="95%"
							isCollapsed="false"
							tooltip="<%=WebUtil.translate(pageContext, TestResultForm.TESTMESSAGE_TOOLTIP, null) %>"
				  >			
			      
					<hbj:trayBody>
								  <% if (result)
										 out.println("<table width='100%' height='100%' bgcolor='#00ff00'>");
									  else
										out.println("<table width='100%' height='100%' bgcolor='#FF0000'>");
									%>						
							<tr>
								<td>
												<hbj:textView text="<%=resultString%>" encode="false" wrapping="true"/>
											
							</td>
						</tr>
						</table>

					</hbj:trayBody>
			
				  </hbj:tray>			      

					</td>
				</tr>
			</table>
		</hbj:form>			
	
	</hbj:page>
</hbj:content>
	
