<%@ page import="com.sap.isa.core.cache.Cache" %>
<%@ page import="java.util.Iterator, com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory" %>
<%@ page import="com.sap.isa.core.xcm.ConfigContainer" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="java.util.Map, com.sap.isa.core.*" %>
<%@page import="java.net.URLEncoder, java.net.URL , com.sap.isa.core.util.XMLHelper" %>
<%@page import="org.w3c.dom.Document, java.net.MalformedURLException" %>
<%@page import="com.sap.isa.core.xcm.ExtendedConfigManager" %>
<%@page import="com.sap.isa.core.xcm.XCMConstants" %>
<%@page import="java.util.Set" %>
<%@ page import = "com.sap.isa.core.xcm.admin.actionform.MonitoringForm" %>
<%@page import="com.sapportals.htmlb.DefaultListModel"%>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.MonitoringController"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@page import="com.sapportals.htmlb.table.TableView"%>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
 
<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<hbj:page title = "<%=WebUtil.translate(pageContext, MonitoringForm.PAGE_TITLE, null)%>" >

 
	<%  
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		MonitoringController monController = (MonitoringController)userData.getAttribute(SessionConstants.MONITORING_CONTROLLER);
		String preSelectedItem = monController.getListBoxPreSelection();
		DefaultListModel scenListModel=null;
		TableView monFileTableView = null;
		TableView attributesTableView = null;
		
		if (preSelectedItem == null){
			//first Load of page
				monController.setListModel();
				scenListModel = monController.getListModel();			
				if (scenListModel != null){
					preSelectedItem = scenListModel.getKeyByIndex(0);
					monController.setTableViews(preSelectedItem);
				} 
		} 
		else{ 
			scenListModel = monController.getListModel();
		} 
		
		if (scenListModel != null) {
		
			monFileTableView = monController.getFilesTableView();
			attributesTableView = monController.getAttributesTableView();
			pageContext.setAttribute("scenListModel", scenListModel);

		}
		final String mon_action = MonitoringForm.ON_USEDSCEN_SELECT ;
		final String ON_REFRESH_CLICK = MonitoringForm.ON_REFRESH_CLICK;

	%>    

	   
<hbj:form id="monitoringForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/monitor.do\" , null , null , false)%>">		  

  
 
<% if (scenListModel == null) { %>
	<br><br>
	<hbj:textView text = "<%=WebUtil.translate(pageContext, MonitoringForm.CACHE_ERROR_TEXT, null)%>" design="HEADER3" />
<% } else{ %> 
	
  <hbj:label id="labelUsedScenario" labelFor="UsedScenario" text="<%=WebUtil.translate(pageContext, MonitoringForm.LABEL_TEXT, null)%>"/>
  <hbj:dropdownListBox
           id="UsedScenario"
           tooltip="<%=WebUtil.translate(pageContext, MonitoringForm.LABEL_TOOLTIP, null)%>" 
           model="scenListModel"
           onSelect="<%=mon_action%>"
           selection="<%=preSelectedItem%>" > 
  </hbj:dropdownListBox>   
  <hbj:button id="refreshTable" text="<%=WebUtil.translate(pageContext, MonitoringForm.BUTTON_TEXT, null)%>" onClick="<%=ON_REFRESH_CLICK%>" tooltip="<%=WebUtil.translate(pageContext, MonitoringForm.BUTTON_TEXT, null)%>"/>
<br>
<% if (monFileTableView == null || attributesTableView == null){ %>
	<br><br>
	<hbj:textView text="<%=WebUtil.translate(pageContext, MonitoringForm.ERROR_TEXT, null) +  monController.getNoConfigContainerKey() %>" wrapping="true" design="HEADER3" />	
<%}else{
	  out.println("<br>"); 
	  xcmAdminContext.render(monFileTableView); 
   	  out.println("<br>");
	  xcmAdminContext.render(attributesTableView);
	} 
}	%> 
	 

</hbj:form>	
  </hbj:page>
