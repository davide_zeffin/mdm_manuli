<%@ page import="com.sap.isa.core.xcm.admin.CheckXCMConsitency" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.sap.isa.core.xcm.init.XCMAdminComponentMetadata" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin())
		response.sendRedirect("notsupported.jsp");
%>


<%! 

	String OK = "ok";
	String MISSING = "<b><font color=FF0000>MISSING</font></b>";
	String MISSING_ORANGE = "<b><font color=E0E000>MISSING</font></b>";	

%>
<html>
  <head>
    <title>Check XCM Consistency</title>
  </head>
  <body>
	<h2>Check XCM Component Configuration Consistency</h2>
	
	Perform the following checks:
	For further details refer to the <a href="http://tekteam.wdf.sap.corp:1080/documentation.shtml/" target=_new>XCM Admin tutorial</a></p
	Components Configuration Overview Table
	<ul>
		<li>
			Column <b>meta data</b></br>
			Checks if meta data is maintained in xcmadmin-config.xml for this component
		</li>
		<li>
			Column <b>Shorttext</b></br>
			Mandatory. Maintained in xcmadmin-config.xml
		</li> 
		<li>
			Column <b>Lngext</b></br>
			Optional but recommended. Maintained in xcmadmin-config.xml
		</li> 
	</ul>
	Components Parameter Configuration Overview Table
	<ul>
		<li>
			Column <b>Shorttext</b></br>
			Mandatory. Maintained in xcmadmin-config.xml
		</li> 
		<li>
			Column <b>Lngext</b></br>
			Optional but recommended. Maintained in xcmadmin-config.xml
		</li> 
	</ul>
	
	
	
	
	<h3>Components</h3>
<table border =1>
<!-- CHECK IF COMPONENT METADATA THERE -->
<tr>
<th>
	Component Overview
</th>
<th>
	Meta Data (mandatory)
</th>
<th>
	Shorttext (mandatory) 
</th>
<th>
	Longtext (recommended) 
</th>

<%
{
	Map result = CheckXCMConsitency.getAppCompMetaData();
	for (Iterator iter = result.keySet().iterator(); iter.hasNext();) {
		String compId = (String)iter.next();
		if (compId.length() == 0) 
			continue;
		XCMAdminComponentMetadata metadata =  (XCMAdminComponentMetadata)result.get(compId);
		String isMetadata = OK;
		String shorttext = MISSING; 
		String longtext = MISSING_ORANGE; 
		if (metadata == null) {
		  isMetadata = MISSING;
		} else {
			if (metadata.getShorttext() != null)
			  shorttext = metadata.getShorttext();
			if (metadata.getLongtext() != null)
			  longtext = metadata.getLongtext();
		}		  

%>
<tr>
<tr>
	<td> 
	<a href="#<%=compId%>"><%=compId%></a>
	</td>
	<td>
	<%=isMetadata%>
	</td>
	<td>
		<font size=2>
			<%=shorttext%>
		</font>
	</td>
	<td>
		<font size=2>
			<%=longtext%>
		</font>
	</td>

</tr>
<%
	} }
%>
</table>
	

<!-- CHECK IF COMPONENT PARAMETER METADATA THERE -->
<%
{
	Map result = CheckXCMConsitency.getAppCompMetaData();
	String paramId = null;
	for (Iterator iter = result.keySet().iterator(); iter.hasNext();) {
		String compId = (String)iter.next();
		XCMAdminComponentMetadata metadata =  (XCMAdminComponentMetadata)result.get(compId);
		if (metadata == null)
		  continue;
		Set appProps = CheckXCMConsitency.getApplDefCompParams(compId);
		
		
%>

<h3>Component: <a name="<%=compId%>"><%=compId%></a> </h3>
<h4>Parameter</h4>



<table border=1>
<th>
Param Name
</th>
<th>
Short Text (mandatory)
</th>
<th>
Long Text (recommended)
</th>

<%

		for (Iterator iter2 = appProps.iterator(); iter2.hasNext();) {
		   paramId = (String)iter2.next();
		   String shorttext = metadata.getParamShorttext(paramId);
		   if (shorttext == null || shorttext.length() == 0)
		   	shorttext = MISSING;

		   String longtext = metadata.getParamLongtext(paramId);
		   if (longtext == null || longtext.length() == 0)
		   	longtext = MISSING_ORANGE;


%>


<tr>
	<td>
		<%=paramId%>
		
	</td>
	<td>
		<font size=2>
			<%=shorttext%>
		</font>
	
	</td>

	<td>
		<font size=2>
			<%=longtext%>
		</font>
	
	</td>


</tr>

<% } %>

</table>



<% }} %>	


  </body>
</html>

