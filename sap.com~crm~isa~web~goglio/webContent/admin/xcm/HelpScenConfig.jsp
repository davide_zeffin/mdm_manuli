<head><script language="JavaScript" > 

<!--

if (document.images) {
    nonblank = new Image();
}


function swap() {
    if (document.images)
        document.images['busy'].src = nonblank.src;
}


-->
</script>
</head>
    
<%@ page import="java.util.*" %>

<%@ page import="com.sap.isa.core.xcm.admin.controller.ControllerUtils"%> 
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page  import="com.sapportals.htmlb.table.DefaultTableViewModel"%>
<%@ page  import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer"%>
<%@ page  import="com.sap.isa.core.xcm.admin.model.table.HelpItemSummaryTableModel"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.HelpConstants" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpScenConfigForm"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpSummaryForm"%>
   
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin_RO(request) || !com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>
 
 
<% 
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	HelpController mHelpController = appConfigController.getHelpController();
	String scenName = (String)request.getParameter("scenName");
	
		if (scenName == null)
		    scenName = appConfigController.getScenarioConfigController().getCurrentScenarioName();
		    
		if (scenName == null)
			scenName="r3shop";  
			
	  mHelpController.setCurrentScenarioName(scenName);
    HelpItem scenDetail = mHelpController.getScenDetailHelpItem(scenName);

    HelpItemContainer optScenParamContainer = mHelpController.getScenParamOptionalContainer(scenName);
    HelpItemContainer manScenParamContainer = mHelpController.getScenParamMandatoryContainer(scenName);

	ComponentConfigController compConfigController = appConfigController.getCompConfigController();
	String linkPrefix = "";
	Set paramNames = null;

%> 
<hbj:content id="xcmAdminContext" > 
	<%
		BaseUI helpUi = new BaseUI(pageContext);
		if(helpUi.isAccessible()) {
			xcmAdminContext.setSection508Rendering(true);
		}
	%> 
  <hbj:page title = "Application Configuration">
        <hbj:form id="HomeForm">
		<hbj:group id="applicationConfigurationGroup" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpSummaryForm.GROUP_TITLE, null)%>" tooltip="">
		
  			<hbj:groupBody>  
                <%-- include scenario details --%>
                <% 
                	mHelpController.setCurrentDetailMode(HelpController.MODE_DETAIL_BASE_CONFIG_NAME, "", "");
                	mHelpController.setCurrentDetailItem(scenDetail);
                %>
             <% if(mHelpController.checkCurrentDetailMode(HelpController.MODE_DETAIL_NAME_AS_LINK_DEST)) { %>
             		<% 	String name1 = mHelpController.getCurrentLinkDestPrefix() + scenName; %>
    				<a name="<%=name1%>" > </a>
    		<% } %>
             <hbj:group id="configDetail" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpScenConfigForm.CONFIG_DETAIL_TEXT, null) + scenName%>" tooltip="">
             <hbj:groupBody> 
		  	    <%@ include file="HelpItemDetail.inc" %>  

                <%-- include mandatory scenario params --%>
                <%      
                        mHelpController.setCurrentSummaryMode(HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK, "scenParam", "");
	                	mHelpController.setCurrentSummaryContainer(manScenParamContainer);
                %> 
			<p></p>
              <hbj:tray id="mandParams"
				title="<%=WebUtil.translate(pageContext, HelpScenConfigForm.MAND_CONFIG_PARAMS, null)%>"
				design="border"
				width="95%"
				isCollapsed="false"
				tooltip="<%=WebUtil.translate(pageContext, HelpScenConfigForm.MAND_CONFIG_PARAMS, null)%>">
				<hbj:trayBody> 
               
                <%@ include file="HelpItemSummary.inc" %>
        
                </hbj:trayBody>
              </hbj:tray>
              
                <%-- include optional scenario params --%>
                <% 
	                	mHelpController.setCurrentSummaryMode(HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK, "scenParam", "");
	                	mHelpController.setCurrentSummaryContainer(optScenParamContainer);
                %>
			<p></p>
              <hbj:tray id="optParams"
				title="<%=WebUtil.translate(pageContext, HelpScenConfigForm.OPT_CONFIG_PARAMS, null)%>"
				design="border"
				width="95%"
				isCollapsed="false"
				tooltip="<%=WebUtil.translate(pageContext, HelpScenConfigForm.OPT_CONFIG_PARAMS, null)%>">
			    <hbj:trayBody>
 
                                     
                <%@ include file="HelpItemSummary.inc" %>                             
                <hr/>
                </hbj:trayBody>
              </hbj:tray>

                <%-- include mandatory scenario params detail --%>
			<p></p>
                <hbj:tray id="mandParamDetail" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpScenConfigForm.MAND_PARAM_DETAIL, null)%>" tooltip=""
                	width="95%"
					isCollapsed="false">
             		<hbj:trayBody> 
                                               
                <%  
                    paramNames = manScenParamContainer.getItemNames();

                    for (Iterator iter = paramNames.iterator(); iter.hasNext();) {
                        String name = (String)iter.next();
                        HelpItem scenParamItem = manScenParamContainer.getItem(name);
	                			mHelpController.setCurrentDetailItem(scenParamItem);                    
                    %>
                        <%@ include file="HelpScenParamAllwedValueDetail.inc" %>  
                    <%
                    }        
                %>
                	</hbj:trayBody>
                </hbj:tray>
								
                <%-- include optional scenario params detail --%>
			<p></p>
                <hbj:tray id="optParamDetail" design="SAPCOLOR" 
                	title="<%=WebUtil.translate(pageContext, HelpScenConfigForm.OPT_PARAM_DETAIL, null)%>" tooltip=""
                	width="95%"
					isCollapsed="false">
             		<hbj:trayBody>
                                                                
                <%
                    paramNames = optScenParamContainer.getItemNames();
	                	mHelpController.setCurrentDetailMode(
	                				HelpController.MODE_DETAIL_SHOW_VALUE + 
	                				HelpController.MODE_DETAIL_NAME_AS_LINK_DEST, "scenParam", "");

                    for (Iterator iter = paramNames.iterator(); iter.hasNext();) {
                        String name = (String)iter.next();
                        HelpItem scenParamItem = manScenParamContainer.getItem(name);
	                			mHelpController.setCurrentDetailItem(scenParamItem);                    
                    %>
                       <%@ include file="HelpScenParamAllwedValueDetail.inc" %> 
                    <%
                    }         
                
                %>                
					</hbj:trayBody>
				</hbj:tray>
             </hbj:groupBody>
             </hbj:group>     
			</hbj:groupBody>
    	</hbj:group>
    	</hbj:form>
  </hbj:page>
</hbj:content>
</body> 