<%@ page import="com.sap.isa.core.xcm.admin.actionform.CompDescrForm" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin_RO(request) || !AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<hbj:form id="configDescrForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/configCreate.do\" , null , null , false)%>">

<% 
	String compId = compConfigController.getCurrentCompId(); //
	String compShorttext = compConfigController.getCurrentCompShorttext();
	String compLongtext = compConfigController.getCurrentCompLongtext();
	String descr = compShorttext + "<p/>" + compLongtext;
	String tooltip = WebUtil.translate(pageContext, CompDescrForm.GROUP_DESC_TOOLTIP1, null) + compId + WebUtil.translate(pageContext, CompDescrForm.GROUP_DESC_TOOLTIP2, null);
	String descrTitle = WebUtil.translate(pageContext, CompDescrForm.GROUP_DESC_TITLE, null) + compId + "'";
	String createTitle = WebUtil.translate(pageContext, CompDescrForm.GROUP_CREATE_TITLE, null);
	String disabled = String.valueOf(compConfigController.isSAP() || !appConfigController.isEditMode());
%>  

	<table width="100%">
	<tr>
		<td>
			<% if (appConfigController.isConfigCompFromScenario()) {
			%>
				<% if(mainConfigUi.isAccessible()) { %>
				<hbj:button id="back" disabled="false" text="<%=WebUtil.translate(pageContext, CompDescrForm.BUTTON_BACK_ACCESS, null) %>" onClick="<%=CompConfigForm.ON_SCENARIO_BACK_COMP_CONFIG_EVENT%>" tooltip="<%=WebUtil.translate(pageContext, CompDescrForm.BUTTON_BACK_TOOLTIP, null) %>"/>				
				<% } else { %>
				<hbj:button id="back" disabled="false" text="<%=WebUtil.translate(pageContext, CompDescrForm.BUTTON_BACK_TEXT, null) %>" onClick="<%=CompConfigForm.ON_SCENARIO_BACK_COMP_CONFIG_EVENT%>" tooltip="<%=WebUtil.translate(pageContext, CompDescrForm.BUTTON_BACK_TOOLTIP, null) %>"/>
				<% } %>
			<% } %>
		<hbj:button id="documentation1" text="<%=WebUtil.translate(pageContext, CompDescrForm.BUTTON_DOCU1_TEXT, null) %>" onClientClick="openHelpComponent()" tooltip="<%=WebUtil.translate(pageContext, CompDescrForm.BUTTON_DOCU1_TOOLTIP, null) %>"/>

		</td>
	</tr>
	<tr>
		<td>
			<hbj:group id="ComponentDescrGroup"
			           design="sapcolor"
			           title="<%=descrTitle%>"
			 
			           tooltip="<%=tooltip%>"
			           width="100%">
				<hbj:groupBody>
					<hbj:textView text="<%=descr%>" encode="false" wrapping="true" />
					</BR>
			
				</hbj:groupBody>
			</hbj:group>
		</td>
	</tr>
	<tr>
  </tr>
	<tr>
		<td align="left">
		<% if (!compConfigController.isSAP()) {
		%>
			<hbj:group id="ComponentCreateGroup"
			           design="sapcolor"
			           title="<%=createTitle%>"
			 
			           tooltip="<%=createTitle%>"
			           width="100%">
				<hbj:groupBody>
					<hbj:label id="confignamelabel" labelFor="<%=CompDescrForm.CONFIG_NAME_INPUT_FIELD_ID%>" text="<%=WebUtil.translate(pageContext, CompDescrForm.CONFIGNAME_TEXT, null) %>"/>
			    <hbj:inputField id="<%=CompDescrForm.CONFIG_NAME_INPUT_FIELD_ID%>"
			                    type="String"
			                    value=""
			                    visible="true"
			                    disabled="<%=disabled%>"
			                    required="false"
			                    maxlength="50"
			                    size="25"
			                    design="standard"/>
					
					<hbj:button id="createCompConfig" disabled="<%=disabled%>" text="<%=WebUtil.translate(pageContext, CompDescrForm.BUTTON_CREATE_TEXT, null) %>" onClick="<%=CompDescrForm.ON_CONFIG_CREATE_EVENT%>" tooltip="<%=WebUtil.translate(pageContext, CompDescrForm.BUTTON_CREATE_TOOLTIP, null) %>"/>
					</BR>
				</hbj:groupBody>
			</hbj:group>		
		<% } %>
			
		</td>
	</tr>
	</table>
</hbj:form>