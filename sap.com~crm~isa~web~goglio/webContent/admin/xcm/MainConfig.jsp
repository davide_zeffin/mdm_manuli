<%@ page import="com.sap.isa.core.config.AdminConfig" %>
<%
   String url1 = WebUtil.getAppsURL(pageContext  , new Boolean(request.isSecure()), "admin/xcm/testResult.jsp" , null , null , false);
   
   String url2 = WebUtil.getAppsURL(pageContext  , new Boolean(request.isSecure()), "admin/xcm/helpText.jsp" , "type=" , null , false);

   String url3 = WebUtil.getAppsURL(pageContext  , new Boolean(request.isSecure()), "admin/xcm/HelpSummary.jsp" , null, null , false);

   String url4 = WebUtil.getAppsURL(pageContext  , new Boolean(request.isSecure()), "admin/xcm/HelpScenConfig.jsp" , null , null , false);
   
   String url5 = WebUtil.getAppsURL(pageContext  , new Boolean(request.isSecure()), "admin/xcm/HelpCompConfig.jsp" , null , null , false);

%>   

 

<head><script language="JavaScript" > 
<!--                    
                                        
                                                
function openWindow() 
    { 
       	href = "<%=url1%>";
    	var win = window;
    	win.open(href,'TestResult','resizable=yes,width=500,height=500,screenX=0,screenY=0,scrollbars');
	 		 
     }           
function openHelp(type)   
    { 
    	var win = window;
    	var href = "<%=url2%>" +  type;
    	win.open(href,'TestResult','resizable=yes,width=500,height=500,screenX=0,screenY=0,scrollbars'); 	
    }      
          
function openDocumentation()
    {    
     	var win = window;
     	var href = "<%=url3%>";
     	win.open(href,'Documentation','resizable=yes,width=600,height=500,screenX=0,screenY=0,scrollbars, menubar,toolbar');
    }   
   
function openHelpScenario()
    { 
    	var win = window;
    	var href = "<%=url4%>";
    	win = window.open(href,'TestResult','resizable=yes,width=600,height=500,screenX=0,screenY=0,scrollbars,menubar,toolbar');
    }  
  
function openHelpComponent()
    { 
    	var win = window;
    	var href = "<%=url5%>";
    	win.open(href,'TestResult','resizable=yes,width=600,height=500,screenX=0,screenY=0,scrollbars,menubar,toolbar');
    }


function openHelpScenParam(paramName)
    { 
    	alert(paramName);
    }
<% // the following javascript functions should only be accessable by an XCM Administrator.
	if (AdminConfig.isXCMAdmin(request)) { %>
function openWarningConfigNameExisting()
    { 
    	alert("An configuration with the same name already exists. Please choose an other name");
    }

    
function cancelSave()
    { 
    	retval = confirm("Are you sure?");
    	if (retval == true) {
				htmlbevent.cancelSubmit = false;
			} else {
				htmlbevent.cancelSubmit = true;
			}
    }    

function cancelScenarioSave()
    { 
    	retval = confirm("Save Application Configuration? (note: after saving configuration you have to stop/start web application before you can use it)");
    	if (retval == true) {
				htmlbevent.cancelSubmit = false;
			} else {
				htmlbevent.cancelSubmit = true;
			}
    }    


function cancelDelete()
    { 
    	retval = confirm("Are you sure?");
    	if (retval == true) {
				htmlbevent.cancelSubmit = false;
			} else {
				htmlbevent.cancelSubmit = true;
			}
    }  
<% } %>    

-->
</script> 
</head>
   
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.xcm.admin.Constants" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%> 
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ScenarioConfigController"%>
<%@ page  import="com.sap.isa.core.util.XMLHelper"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.ToolbarForm" %>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HomeForm" %>
<%@ page import = "com.sap.isa.core.xcm.admin.actionform.MainConfigForm" %>
<%@ page import = "com.sap.isa.core.xcm.admin.actionform.HelpTextForm" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel" %>
<%@ page import="com.sapportals.htmlb.IListModel" %>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.OptionsConfigForm"%>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
 
<%  
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	ComponentConfigController compConfigController = appConfigController.getCompConfigController();
	ScenarioConfigController scenConfigController = appConfigController.getScenarioConfigController();
	
	
	if (appConfigController.isScenarioNameAvilable()) {
		appConfigController.isScenarioNameAvilable(false);
		out.println("<body onLoad=\"openWarningConfigNameExisting()\">");
	}
	
	if (scenConfigController.isScenarioParamDescrHelp() || compConfigController.isCompParamHelp()) {
		scenConfigController.isScenarioParamDescrHelp(false);
		compConfigController.isCompParamHelp(false);
		out.println("<body onLoad=\"openHelp('scenParamValue')\">");
	}    
	else if (compConfigController.isCompParamValueHelp()) {
		compConfigController.isCompParamValueHelp(false);
		out.println("<body onLoad=\"openHelp('compParamList')\">");
	}  
	else if (scenConfigController.isScenarioParamHelp()){
		scenConfigController.isScenarioParamHelp(false);
		out.println("<body onLoad=\"openHelp('scenParamList')\">");
	} 
	else	 
		out.println("<body>"); 
	String title = 	WebUtil.translate(pageContext, MainConfigForm.GROUPBOX_TITLE, null);
	final String TRUE = "true";
	final String FALSE = "false";
	Collection msgs = (Collection)request.getAttribute(Constants.MESSAGES);
	String formId = null;
	String selElId = null;
	String contextParamHelpText = WebUtil.translate(pageContext, OptionsConfigForm.HELP_CURRENT_CONFIG, null);	
	IListModel configsListModel = appConfigController.getConfigsListModel();
	pageContext.setAttribute("configsListModel", configsListModel);
	String clientClickSwitchConfgiHelp = "openHelp('" + HelpTextForm.CONFIG_TYPE_ATTR + "')";
	

	// 
%>  
<hbj:content id="xcmAdminContext" > 
<% 
	BaseUI mainConfigUi = new BaseUI(pageContext);
	if(mainConfigUi.isAccessible()) { 
		 xcmAdminContext.setSection508Rendering(true);
	}   
%>
 
  <hbj:page title = "<%=WebUtil.translate(pageContext, MainConfigForm.PAGE_TITLE, null)%>">
		 <table border="0" height="100%" width="100%">
		 	<tr>
		 		<td >
		 			<%@ include file="Toolbar.inc"%>
		 		</td>
		 	</tr>
	  		<% if(msgs != null && msgs.size() > 0) {%>
	  		<tr>
	  		<td>
	  			<hbj:textView text="<%=WebUtil.translate(pageContext, MainConfigForm.MESSAGES_TEXT, null)%>" design="EMPHASIZED"/>
	  		</td>  
	  		</tr>  
		 	<tr>
		 		<td>
				<hbj:itemList id ="messages">
	  			<%Iterator it = msgs.iterator();
	  			while(it.hasNext()) {
	  				if (mainConfigUi.isAccessible()) {
					%>				
	  					<hbj:listItem>
			  					<hbj:textView text="<%=it.next().toString()%>" design="EMPHASIZED"/>
	  					</hbj:listItem>
	  				<%	
	  				} else {
	  				%>
	  					<hbj:listItem>
	  						<font color="red">	  				
								<% out.println(it.next().toString()); %>
							</font>
	  					</hbj:listItem>
	  				<%} } %>
				</hbj:itemList>
				</td> 
			</tr>
	  		<%}%>				 	
			<tr>   
		 		<td height="100%">
		 			<hbj:group id="applicationConfigurationGroup" design="sapcolor" title="<%=title%>" tooltip="<%=WebUtil.translate(pageContext, MainConfigForm.GROUPBOX_TOOLTIP, null)%>">
					<%   
						applicationConfigurationGroup.setHeight("100%");
					%>
						<hbj:groupHeader>
						<hbj:gridLayout id="headerGridLayout" width="100%">
						   <hbj:gridLayoutCell rowIndex="1" columnIndex="1" horizontalAlignment="LEFT">
						   
						  
		<hbj:form id="HomeForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/homeInfo.do\" , null , null , false)%>">		
							<b>
							<hbj:label  id="xcmconfigselectlabel" labelFor="<%=com.sap.isa.core.xcm.admin.actionform.HomeForm.ON_GLOBAL_CONFIGURATION_LIST_BOX_ID%>" labeledComponentClass="DropdownListBox"
						 		text="<%=title%>" encode="true"/>
							</b>
							  <%
							  	if (mainConfigUi.isAccessible() && appConfigController.isEditMode()) { 
							  		String selection = configsListModel.getSingleSelection();
							  	%>
							  				<hbj:inputField id="<%=com.sap.isa.core.xcm.admin.actionform.HomeForm.ON_GLOBAL_CONFIGURATION_LIST_BOX_ID%>"
												type="String"
												value="<%= configsListModel.getTextForKey(selection) %>"
												visible="true"
												disabled ="false"
												design="standard"/>
								<%
		 					  	} else {
							  %>
								  <hbj:dropdownListBox  id="<%=com.sap.isa.core.xcm.admin.actionform.HomeForm.ON_GLOBAL_CONFIGURATION_LIST_BOX_ID%>"
											tooltip="<%=contextParamHelpText%>"
							 				disabled="<%=appConfigController.isEditMode() ? TRUE : FALSE%>"
											model="configsListModel"
											onSelect="<%=com.sap.isa.core.xcm.admin.actionform.HomeForm.ON_GLOBAL_CONFIGURATION_SELECT%>"
								  > 
								  </hbj:dropdownListBox>
							  <% } %>
							  
							  <hbj:button id="ShowConfigSwitchHelp" text="?" onClientClick="<%=clientClickSwitchConfgiHelp %>" tooltip="<%=WebUtil.translate(pageContext, MainConfigForm.XCM_CONFIG_TOOLTIP, null) %>"/>
		</hbj:form>
							  
						    </hbj:gridLayoutCell> 
						   <hbj:gridLayoutCell rowIndex="1" columnIndex="2" horizontalAlignment="RIGHT">
							<hbj:form id="xcmEditToolbarForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/xcmToolbarClick.do\" , null , null , false)%>">		
  					<% // only XCM Administrators are allowed to go into edit mode!
  						if (AdminConfig.isXCMAdmin(request)) { %>
								<hbj:button id="Edit" 
											text="<%= !appConfigController.isEditMode() ? WebUtil.translate(pageContext, MainConfigForm.EDIT_BUTTON_TEXT , null) : WebUtil.translate(pageContext, MainConfigForm.DISPLAY_BUTTON_TEXT,null)%>" 
											tooltip="<%= !appConfigController.isEditMode()? WebUtil.translate(pageContext, MainConfigForm.EDIT_BUTTON_TOOLTIP , null) : WebUtil.translate(pageContext, MainConfigForm.DISPLAY_BUTTON_TOOLTIP , null)%>"
						 					onClick="<%= !appConfigController.isEditMode() ? HomeForm.ON_EDIT_CLICK_EVENT : HomeForm.ON_DISPLAY_CLICK_EVENT %>"
											onClientClick=""/>
						<% } %>														
								<hbj:button id="Refresh" 
											text="<%=WebUtil.translate(pageContext, MainConfigForm.REFRESH_BUTTON_TEXT , null)%>"
											tooltip="<%=WebUtil.translate(pageContext, MainConfigForm.REFRESH_BUTTON_TOOLTIP , null)%>"
											onClick="<%=HomeForm.ON_REFRESH_CLICK_EVENT %>"/>											
									 		 											
							</hbj:form>  
						  </hbj:gridLayoutCell>
						</hbj:gridLayout>
						</hbj:groupHeader>
						<hbj:groupBody> 
						  <hbj:gridLayout id="contentGridLayout" debugMode="true" width="100%">
								<%
							  	contentGridLayout.setCellSpacing(10);
							  	contentGridLayout.setHeightPercentage(100);	 
							  %>
   							<hbj:gridLayoutCell rowIndex="1" columnIndex="1" verticalAlignment="TOP" width="30%">
							  	<%@ include file="ConfigTree.inc" %>
							  </hbj:gridLayoutCell>
							 
				      				<hbj:gridLayoutCell id="contenCell" rowIndex="1" columnIndex="2" verticalAlignment="TOP" horizontalAlignment="CENTER">
                                <% if (appConfigController.getConfigType() == AppConfigController.HOME_MODE) { %>
							  		<jsp:include page="Home.jsp"/>
							  	<% } %>
                                <% if (appConfigController.getConfigType() == AppConfigController.SCENARIO_OVERVIEW_MODE) { %>
							  		<jsp:include page="ScenarioOverview.jsp"/>
							  	<% } %>
                                <% if (appConfigController.getConfigType() == AppConfigController.COMPONENTS_OVERVIEW_MODE) { %>
							  		<jsp:include page="CompOverview.jsp"/>
		 		 			  	<% } %>
                                  <% if (appConfigController.getConfigType() == AppConfigController.OPTIONS_MODE) { %>
		 					  		<jsp:include page="OptionsConfig.jsp"/>
							  	<% } %>							  	
                                <% if (appConfigController.getConfigType() == AppConfigController.COMPONENT_DESCR_MODE) { %>
							  		<%@ include file="CompDescription.inc" %>
							  	<% } %>
                                <% if (appConfigController.getConfigType() == AppConfigController.COMPONENTS_CONFIG_MODE) { %>
							  		<%@ include file="CompConfig.inc" %>
							  	<% } %>

                                <% if (appConfigController.getConfigType() == AppConfigController.SCENARIO_CONFIG_MODE) { %>
							  		<%@ include file="ScenarioConfig.inc" %>
							  	<% } %>
 
							  </hbj:gridLayoutCell>
							</hbj:gridLayout>
						</hbj:groupBody>
					</hbj:group>
				</td>
			</tr>
		</table>

  </hbj:page>
</hbj:content>
</body>
</script>  
