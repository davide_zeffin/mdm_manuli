<head><script language="JavaScript" > 
</script>   
</head>

<%@ page import="java.util.*" %>
 
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page import="com.sapportals.htmlb.table.DefaultTableViewModel"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.table.HelpItemSummaryTableModel"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.HelpConstants" %>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpSummaryForm"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpCompConfigForm"%>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>
    
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
     


<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>
     
     
<% 
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	HelpController mHelpController = appConfigController.getHelpController();
	String compName = (String)request.getParameter("compName");
	if (compName == null)
	   compName = appConfigController.getCompConfigController().getCurrentCompId();
	
	if (compName == null)
		compName = "jdbc";

	HelpItem compDetail = mHelpController.getCompDetailHelpItem(compName);
	HelpItemContainer compConfigContainer = mHelpController.getComponentConfigContainer(compName);
	mHelpController.setCurrentComponentName(compName);
     
%> 
<hbj:content id="xcmAdminContext" >
<%
	BaseUI helpCompConfUi = new BaseUI(pageContext);
	if(helpCompConfUi.isAccessible()) {
		 xcmAdminContext.setSection508Rendering(true);
	}
%>  
  <hbj:page title = "Application Configuration">
       <hbj:form id="HomeForm">
          <hbj:group id="applicationConfigurationGroup" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpSummaryForm.PAGE_TITLE, null)%>" tooltip="">

  			<hbj:groupBody>
  
                <%-- include component details --%>
                <% 
                	mHelpController.setCurrentDetailMode(HelpController.MODE_DETAIL_NONE, "", "");
                	mHelpController.setCurrentDetailItem(compDetail);
                %>
                <hbj:group id="compConfigDetail" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpCompConfigForm.COMP_DETAIL_TEXT, null) + compName %>" tooltip="">
	             <hbj:groupBody>
                                 
		  	    <%@ include file="HelpItemDetail.inc" %>  

                <%-- include component configuraration summary --%>
                <% 
	                	mHelpController.setCurrentSummaryMode(HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK, "compConfig", "");
	                	mHelpController.setCurrentSummaryContainer(compConfigContainer);
                %>
                <br>
                 <hbj:tray id="ItemDetailCaptionTray" title="<%=WebUtil.translate(pageContext, HelpCompConfigForm.CONFIG_SUM_TEXT, null)%>"
                 	design="border"
					width="95%"
					isCollapsed="false"	>
					<hbj:trayBody>
                
		                <%@ include file="HelpItemSummary.inc" %>                             
                
   				
   				</hbj:trayBody>
   				</hbj:tray>
   				<p></p>
   				
                <%-- iterate through component configuraration details--%>
                 <hbj:group id="compConfDetail" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpCompConfigForm.CONF_DETAIL_TEXT, null) %>" tooltip="">
				   <hbj:groupBody>                                               
                <%  
                    Set paramNames = compConfigContainer.getItemNames();

                    for (Iterator iter = paramNames.iterator(); iter.hasNext();) {
                        String name = (String)iter.next();
                        HelpItem scenParamItem = compConfigContainer.getItem(name);
	                			mHelpController.setCurrentDetailItem(scenParamItem);                    
                    %>
                        <jsp:include page="HelpCompConfigDetail.jsp"/>
                    <%
                    }         
                %>
               </hbj:groupBody>
               </hbj:group>
               
               <p></p> 
               <hbj:group id="paramDetail" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpCompConfigForm.PARAM_DETAIL_TEXT, null)%>" tooltip="">
               <hbj:groupBody>
                                              
                <%-- show component param details --%>
                <jsp:include page="HelpCompParamDetail.jsp"/>
               
               		</hbj:groupBody>
               		</hbj:group>
                </hbj:groupBody>
   				</hbj:group> 
			</hbj:groupBody>
    	</hbj:group>
     </hbj:form>
  </hbj:page>
</hbj:content>
</body>     