<%@ page import="com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel" %>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm" %>
<%@ page import="com.sapportals.htmlb.Checkbox" %>
<%@ page import="com.sapportals.htmlb.Button" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin_RO(request) || !com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% 
	String currentScenName = scenConfigController.getCurrentScenarioName();
	String ScenDescrGroupTitle = WebUtil.translate(pageContext, ScenarioConfigForm.TRAY3_TOOLTIP, null) + currentScenName + "'";
	TableView scenarioTableView = 	scenConfigController.getScenarioDetailTableView();	
	String isSAP = String.valueOf(scenConfigController.isSAP());
	String descHeader = WebUtil.translate(pageContext, ScenarioConfigForm.DESCHEADER, null);
	String descCreate = WebUtil.translate(pageContext, ScenarioConfigForm.DESCCREATE1, null);
	String isDescrTrayCollapsed = String.valueOf(scenConfigController.isScenarioDescrTrayCollapsed());
	String isConfigTrayCollapsed = String.valueOf(scenConfigController.isScenarioConfigTrayCollapsed());
	String defaultScenarioHelp = WebUtil.translate(pageContext, ScenarioConfigForm.DEFAULT_SCENARIO_HELP1, null) + currentScenName + WebUtil.translate(pageContext, ScenarioConfigForm.DEFAULT_SCENARIO_HELP2, null) + currentScenName;
	String applConfigGroupTitle = "Application configuration: '" + currentScenName + "'";
	session.setAttribute("defaultScenarioHelp", defaultScenarioHelp);
	String activateScenarioHelp = WebUtil.translate(pageContext, ScenarioConfigForm.ACTIVE_SCENARIO_HELP, null);
	session.setAttribute("activateScenarioHelp", activateScenarioHelp);
	String howToExpand = WebUtil.translate(pageContext, ScenarioConfigForm.HOW_TO_EXPAND_TEXT, null);;
	if (!scenConfigController.isSAP())
		howToExpand = "";
	String disabled = String.valueOf(!appConfigController.isEditMode());
	String clientClickAdvHelp = "openHelp('" + HelpTextForm.ADVANCED_SETTINGS_ATTR + "')";
%>
  
<hbj:form id="scenContentForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/scenConfig.do\" , null , null , false)%>">

<table width="100%">
	<tr>

	<% if (!scenConfigController.isSAP() && appConfigController.isEditMode()) {
	%>

		<th align="left" width="1%">
			<hbj:button id="save" 
				disabled="<%=isSAP%>" 
				text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.SAVE_BUTTON_TEXT, null) %>" 
				onClick="<%=ScenarioConfigForm.ON_SCENARIO_SAVE_EVENT%>" 
				tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.SAVE_BUTTON_TOOLTIP, null) %>"
				onClientClick="cancelSave()" />
		</th>
		<th align="left" width="1%">
			<hbj:button id="delete" 
				disabled="<%=isSAP%>" 
				text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.DELETE_BUTTON_TEXT, null) %>" 
				onClientClick="cancelDelete()" 
				onClick="<%=ScenarioConfigForm.ON_SCENARIO_DELETE_EVENT%>" 
				tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.DELETE_BUTTON_TOOLTIP, null) %>"/> 
		</th>	
	<% } %>
		<th width="98%" align="left">
			<hbj:button id="documentation" text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.CONFIG_BUTTON_TEXT, null) %>" onClientClick="openHelpScenario()" tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.CONFIG_BUTTON_TOOLTIP, null) %>"/> 
		</th>
	</tr>
</table>

	<hbj:group id="ScenarioConfigGroup"
	           design="sapcolor"
	           title="<%=applConfigGroupTitle%>"
	           tooltip="<%=applConfigGroupTitle%>">
		<hbj:groupBody> 

		<table>
			<tr>
				<td>

						<hbj:label  id="labelScenShorttext" labelFor="<%=ScenarioConfigForm.SCEN_SHORT_TEXT_INPUT_FIELD_ID %>"
									labeledComponentClass="DropdownListBox" 
									text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.SCENSHORT_LABEL_TEXT, null) %>"
									tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.SCENSHORT_LABEL_TOOLTIP, null) %>" />
				</td>
				<td>
					
							<hbj:inputField id="<%=ScenarioConfigForm.SCEN_SHORT_TEXT_INPUT_FIELD_ID %>"
										type="String"
										value="<%= scenConfigController.getCurrentScenarioShorttext()%>"
										visible="true"
										disabled="<%= disabled %>"
										required="false"
										maxlength="1024"
										size="100"
										design="standard"/>
						
				</td>
				<td align=left>
					<hbj:button id="scenarioHelpButton" text="?" onClientClick="openHelp('scenario')" tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.SCEN_HELP_TOOLTIP, null) %>"/>
				</td>
			</tr>
			<% if (!scenConfigController.isSAP()) { %>
				<tr>
					<td>
						<hbj:label id="inputlabel2" text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.LABEL_BASE_SCEN_TEXT, null) %>" labelFor="baseScenario" labeledComponentClass="DropdownListBox"/>
					</td>
					<td>
								<hbj:inputField id="baseScenario"
									  type="String"
									  value="<%=scenConfigController.getBaseConfigName()%>"
									  visible="true"
									  disabled="true"
									  required="false"
									  maxlength="50"
									  size="25"
									  design="standard"/>
	                  
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<% 
							Checkbox defaultScenarioCheckBox = new Checkbox("defaultScenarioCheckbox");
							defaultScenarioCheckBox.setTooltip(WebUtil.translate(pageContext, ScenarioConfigForm.CHECKBOX_DEF_CONF_TOOLTIP, null));
							if (scenConfigController.isCurrentScenarioObsolete()) {
								defaultScenarioCheckBox.setEnabled(false);
							} else {
								if (appConfigController.isEditMode() && !scenConfigController.isCurrentScenarioDefault()){
									defaultScenarioCheckBox.setEnabled(true);
								}else{
									defaultScenarioCheckBox.setEnabled(false);
								}
							}
							defaultScenarioCheckBox.setChecked(scenConfigController.isCurrentScenarioDefault());
							defaultScenarioCheckBox.setText(WebUtil.translate(pageContext, ScenarioConfigForm.CHECKBOX_DEF_CONF_TEXT, null));
							xcmAdminContext.render(defaultScenarioCheckBox);
						%>
						  <hbj:button id="defaultScenarioHelpButton" text="?" onClientClick="openHelp('defaultScenario')" tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.BUTTON_DEF_SCEN_HELP_TOOLTIP, null) %>"/>
					</td>
					</tr>
						<% 
							out.println("<tr><td></td><td>");
							Checkbox deactiveScenarioCheckBox = new Checkbox("deactivateScenarioCheckbox");
							deactiveScenarioCheckBox.setTooltip(WebUtil.translate(pageContext, ScenarioConfigForm.CHECKBOX_DEACTIV_TOOLTIP, null));
							if (scenConfigController.isCurrentScenarioObsolete()) {
								deactiveScenarioCheckBox.setEnabled(false);				
								deactiveScenarioCheckBox.setChecked(false);
							} else {
								deactiveScenarioCheckBox.setEnabled(appConfigController.isEditMode());				
								deactiveScenarioCheckBox.setChecked(scenConfigController.isCurrentScenarioActive());
							}
							
							deactiveScenarioCheckBox.setText(WebUtil.translate(pageContext, ScenarioConfigForm.CHECKBOX_DEACTIV_TEXT, null));
							xcmAdminContext.render(deactiveScenarioCheckBox);
						%>	
						<hbj:button id="activateScenarioHelpButton" text="?" onClientClick="openHelp('activateScenario')" tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.BUTTON_ACT_SCEN_HELP_TOOLTIP, null) %>"/>
						</td>
						</tr>
			   <% } %>
			</table>
		</hbj:groupBody> 
	</hbj:group>





	<% if (scenConfigController.isSAP()) {
	%>


	<hbj:group id="ScenarioCreateConfigGroup"
	           design="sapcolor"
	           title="<%=descCreate%>"
	           tooltip="<%=descCreate%>">
		<hbj:groupBody> 

				<hbj:label id="scen_name" text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.LABEL_SCEN_NAME_TEXT, null) %>" labelFor="<%=ScenarioConfigForm.SCEN_NAME_INPUT_FIELD_ID%>" labeledComponentClass="DropdownListBox" />			
				<hbj:inputField id="<%=ScenarioConfigForm.SCEN_NAME_INPUT_FIELD_ID%>"
				  type="String"
				  value=""
				  visible="true"
				  disabled="<%=disabled%>"
				  required="false"
				  maxlength="50"
				  size="25"
				  design="standard"/>
			      
				<hbj:button id="createScenario" 
										disabled="<%=disabled%>" 
										text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.BUTTON_CREATE_TEXT, null) %>"
										onClick="<%=ScenarioConfigForm.ON_SCENARIO_CREATE_EVENT%>"
										tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.BUTTON_CREATE_TOOLTIP, null) %>"/> 
												
					  			
		</hbj:groupBody> 
	</hbj:group>
		
		<% } %> 






	<hbj:group id="ScenarioDetailsConfigGroup"
	           design="sapcolor"
	           title="<%=WebUtil.translate(pageContext, ScenarioConfigForm.TRAY3_TEXT, null) %>"
	           tooltip="<%= ScenDescrGroupTitle%>">
		<hbj:groupBody> 
			<table>
				<tr> 
					<td width=5%>
						<hbj:label  id="advancedSettingsButton" text="Advanced Settings" labelFor="advancedOptionsButton" labeledComponentClass="DropdownListBox"/>
					</td>
					<td> 
						<%   
						Button advancedOptionsButton = new Button("advancedOptionsButton");
						if (scenConfigController.isAdvancedOptionsOn()) {
							advancedOptionsButton.setText(WebUtil.translate(pageContext, ScenarioConfigForm.CHECKBOX_ADVANCED_ON_TEXT, null));
							advancedOptionsButton.setTooltip(WebUtil.translate(pageContext, ScenarioConfigForm.CHECKBOX_ADVANCED_ON_TOOLTIP, null));
						} else {
							advancedOptionsButton.setText(WebUtil.translate(pageContext, ScenarioConfigForm.CHECKBOX_ADVANCED_OFF_TEXT, null));
							advancedOptionsButton.setTooltip(WebUtil.translate(pageContext, ScenarioConfigForm.CHECKBOX_ADVANCED_OFF_TOOLTIP, null));
						}
						advancedOptionsButton.setOnClick(ScenarioConfigForm.ON_ADVANCED_SCENARIO_OPTIONS_EVENT);								
						xcmAdminContext.render(advancedOptionsButton);

						%>
					  <hbj:button id="ShowAdvancedScenarioHelpButton" text="?" onClientClick="<%=clientClickAdvHelp%>" tooltip="<%=WebUtil.translate(pageContext, ScenarioConfigForm.ADVANCED_SETTINGS_TOOLTIP, null) %>"/>
					</td>
				</tr>
				<tr>
					<td colspan=2>										
						<% 
							xcmAdminContext.render(scenarioTableView); 
						%>
					</td>
				</tr>
				<tr>
					<td colspan=3>
						  <%	if(!mainConfigUi.isAccessible()) { %>
								  <hbj:textView text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.LEGEND_TEXT, null) %>" design="EMPHASIZED"/><br>
						 		  <hbj:textView text="<%=WebUtil.translate(pageContext, ScenarioConfigForm.LEGEND_TEXT2, null) %>"/>
						 <% } %>	
					</td>
				</tr>
			</table>									  
		</hbj:groupBody> 
	</hbj:group> 
</hbj:form>