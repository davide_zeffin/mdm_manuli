<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page  import="com.sapportals.htmlb.table.DefaultTableViewModel"%>
<%@ page  import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer"%>


<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% 
    HelpItemContainer helpItemContainer = (HelpItemContainer)request.getAttribute("helpItemContainer");
    String tableCaption = helpItemContainer.getContainerTitle() + " Summary";
    TableView tableView = HelpController.getSummaryTable(helpItemContainer);
%>
 
<hbj:content id="xcmHelpContext" >
	<%
		BaseUI helpmain = new BaseUI(pageContext);
		if(helpmain.isAccessible()) {
			xcmHelpContext.setSection508Rendering(true);
		}
	%>
  <hbj:page title = "Help">
	<hbj:form id="helpForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/compConfig.do\" , null , null , false)%>">		
            <%-- summary tablr --%>
             <hbj:textView
                     id="ItemDetailCaptionTextView"
                     text="<%= tableCaption%>"
                     design="HEADER2"/>
            
            <%
                xcmHelpContext.render(tableView);
            %>
		</hbj:form>			
	</hbj:page>
</hbj:content>
	
