
<%@ page import="com.sap.isa.core.xcm.admin.Constants" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<%
    String errorMsg = (String) request.getAttribute(Constants.ERROR_MSG);
    if (errorMsg == null)
        errorMsg = "See error log file for further details";
    
%>
<hbj:textView text="<h3>XCM Admin Internal Error</h3>" encode="false" />
<hbj:textView text="<h3><%= errorMsg %></h3>" encode="false" />