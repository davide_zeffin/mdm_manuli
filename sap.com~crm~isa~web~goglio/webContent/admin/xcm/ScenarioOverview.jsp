<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.ScenarioOverviewForm"%>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<hbj:content id="xcmAdminScenarioOverview" >
	<%
		BaseUI scenOvervUi = new BaseUI(pageContext);
		if(scenOvervUi.isAccessible()) {
			xcmAdminScenarioOverview.setSection508Rendering(true);
		}
	%>
  <hbj:page title = "<%=WebUtil.translate(pageContext, ScenarioOverviewForm.PAGE_TITLE, null)%>">


<hbj:form id="configContentForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/mainSelect.do\" , null , null , false)%>">	

	<hbj:group id="ScenarioNameGroup"
	           design="sapcolor"
	           title="<%=WebUtil.translate(pageContext, ScenarioOverviewForm.GROUPBOX_TITLE, null)%>"
	 
	           tooltip="<%=WebUtil.translate(pageContext, ScenarioOverviewForm.GROUPBOX_TITLE, null)%>">
		<hbj:groupBody>
		<hbj:textView text="<%=WebUtil.translate(pageContext, ScenarioOverviewForm.TEXTFIELD_TEXT, null)%>" encode="false" wrapping="true"/>
			</p>
		      
		</hbj:groupBody>
	</hbj:group>
</hbj:form>
</hbj:page>
</hbj:content>

