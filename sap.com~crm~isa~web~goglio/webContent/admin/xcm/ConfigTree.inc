<%@ page import="com.sap.isa.core.xcm.admin.model.tree.ConfigTreeModel" %>
<%@ page import="com.sapportals.htmlb.TreeNode" %>
<%@ page import="com.sapportals.htmlb.Component" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants" %>

<% 
	ConfigTreeModel tree = appConfigController.getConfigTreeModel();
	pageContext.setAttribute("configTree", tree);
%> 
<hbj:form id="configTreeForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/configTreeSelect.do\" , null , null , false)%>">
  
<% /*formId = xcmAdminContext.getParamIdForComponent(configTreeForm).substring(0, xcmAdminContext.getParamIdForComponent(configTreeForm).lastIndexOf("_")); */%>

 

	<hbj:tree id="configTree"
	          title=""
	          rootNode="configTree"
	          rootNodeIsVisible="false">
	          <% /*ConfigTreeModel treeModel1 = appConfigController.getConfigTreeModel();
	          TreeNode node = treeModel1.expNode;
	          if (node != null) {
		          Component comp = node.getComponent();
		          //selElId = node.getID();
	 				//selElId = comp.getId();
				  selElId = xcmAdminContext.getParamIdForComponent(comp);
	          } 
	          
	          /*selElId = xcmAdminContext.getParamIdForComponent(configTree_link_2);*/ %>
	</hbj:tree>


</hbj:form>  