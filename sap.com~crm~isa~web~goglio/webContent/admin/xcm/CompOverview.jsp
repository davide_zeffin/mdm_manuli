<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.CompOverviewForm"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>



<hbj:content id="xcmAdminCompOverview" >
	<%
		BaseUI comOvervUi = new BaseUI(pageContext);
		if(comOvervUi.isAccessible()) {
			xcmAdminCompOverview.setSection508Rendering(true);
		}
	%> 
  <hbj:page title = "<%=WebUtil.translate(pageContext, CompOverviewForm.PAGE_TITLE, null) %>">


<hbj:form id="configContentForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/mainSelect.do\" , null , null , false)%>">	

	<hbj:group id="ComponentNameGroup"
	           design="sapcolor"
	           title="<%=WebUtil.translate(pageContext, CompOverviewForm.GROUP_TITLE, null) %>"
	 
	           tooltip="<%=WebUtil.translate(pageContext, CompOverviewForm.GROUP_TOOLTIP, null) %>">
		<hbj:groupBody>
		<hbj:textView text="<%=WebUtil.translate(pageContext, CompOverviewForm.TEXT_TEXT, null) %>" encode="false" wrapping="true"/>
	  	<P/>

      <hbj:tray id="comExampleTray"
                title="<%=WebUtil.translate(pageContext, CompOverviewForm.TRAY_TITLE, null) %>"
                design="border"
                width="97%"
                isCollapsed="false"
                tooltip="<%=WebUtil.translate(pageContext, CompOverviewForm.TRAY_TOOLTIP, null) %>"
      >
        <hbj:trayBody>
        	<hbj:textView text="<%=WebUtil.translate(pageContext, CompOverviewForm.TRAY_TEXT, null) %>" encode="false" wrapping="true"/>
			 			<hbj:itemList id="comExample" ordered="true">
			           <hbj:listItem>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM1, null) %>"/>
			           </hbj:listItem>
			           <hbj:listItem>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM2, null) %>"/>
			           </hbj:listItem>
			           <hbj:listItem>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM3, null) %>"/>
			           </hbj:listItem>
			           <hbj:listItem>
			               <% if (comOvervUi.isAccessible()) { %>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm. LIST_ITEM4_ACCESS, null) %>"/>
			               <% } else { %>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM4, null) %>"/>
			               <% } %>
			               
			           </hbj:listItem>
			           <hbj:listItem>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM5, null) %>"/>
			           </hbj:listItem>
			           <hbj:listItem>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM6, null) %>"/>
			           </hbj:listItem>
			           <hbj:listItem>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM7, null) %>"/>
			           </hbj:listItem>
			           <hbj:listItem>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM8, null) %>"/>
			           </hbj:listItem> 
			           <hbj:listItem>
			               <hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, CompOverviewForm.LIST_ITEM9, null) %>"/>
			           </hbj:listItem> 
				  	</hbj:itemList>

					</hbj:trayBody>
				</hbj:tray>


	  	
			</BR>
	
		</hbj:groupBody>
	</hbj:group>
</hbj:form>
</hbj:page>
</hbj:content>



