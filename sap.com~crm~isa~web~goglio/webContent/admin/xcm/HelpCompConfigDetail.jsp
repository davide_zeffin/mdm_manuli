<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer" %>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.HelpConstants" %>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.ControllerUtils"%>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpItemDetailForm"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpCompConfigForm"%>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<%@ taglib uri="/htmlb" prefix="hbj" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% 
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	HelpController mHelpController = appConfigController.getHelpController();
%>
 

<hbj:content id="xcmAdminContext" >
<%
	BaseUI helpCompConfDetUi = new BaseUI(pageContext);
	if(helpCompConfDetUi.isAccessible()) {
		 xcmAdminContext.setSection508Rendering(true);
	}
%> 
  <hbj:page title = "helpItem">
 
	<hbj:form id="xcmHelpItemDetailForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/helpItem.do\" , null , null , false)%>">
			 <%   
				HelpItem helpItem = mHelpController.getCurrentDetailItem();
					mHelpController.setCurrentDetailMode(
							HelpController.MODE_DETAIL_NAME_AS_LINK_DEST +
							HelpController.MODE_DETAIL_VALUE_AS_LINK, "", "compConfig");
			 %>
			 <br>
			  <% if(mHelpController.checkCurrentDetailMode(HelpController.MODE_DETAIL_NAME_AS_LINK_DEST)) { %>
             		<% 	String name1 = mHelpController.getCurrentLinkDestPrefix() + helpItem.getName(); %>
    				<a name="<%=name1%>" > </a>
    		<% } %>
				 <hbj:group id="compConfigDetail<%=helpItem.getName()%>" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpItemDetailForm.NAME, null) + helpItem.getName()%>" tooltip="">
				 <hbj:groupBody> 
			<!-- parameter details -->
			<%@ include file="HelpItemDetail.inc" %>  
            
			<!-- configuration parameter summary -->
			<br/>  
			<br>
			 <hbj:tray id="ItemDetailCaptionTray" design="SAPCOLOR" 
			 		title="<%=WebUtil.translate(pageContext, HelpCompConfigForm.CONF_PARAM_SUM, null)%>" tooltip=""
                	width="95%"
					isCollapsed="false">  
			 <hbj:trayBody>                                             
			<% 
				String compId = mHelpController.getCurrentComponentName();
				String compConfigName = helpItem.getName();
				HelpItemContainer paramSummaryContainer = mHelpController.getComponentConfigParamSummary(compId, compConfigName); 
							mHelpController.setCurrentSummaryContainer(paramSummaryContainer);              				
				  mHelpController.setCurrentSummaryMode(
					HelpController.MODE_SUMMARY_TABLE_SHORTTEXT + 
						HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK, "compParam", "");                         								
			%>
			<%@ include file="HelpItemSummary.inc" %> 
			</hbj:trayBody>
			</hbj:tray>
			</hbj:groupBody> 
			</hbj:group>
		</hbj:form>  
	</hbj:page>    
	</hbj:content>