<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<%@ taglib uri="/htmlb" prefix="hbj" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<hbj:content id="xcmHelpItemContext" >
<%
	BaseUI helpItemSumUi = new BaseUI(pageContext);
	if(helpSumUi.isAccessible()) {
		 xcmAdminContext.setSection508Rendering(true);
	}
%>
  <hbj:page title = "helpItem">

    <hbj:form id="HomeForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/helpItem.do\" , null , null , false)%>">

            <!-- render item detail -->
             <%
                HelpItem helpItem = (HelpItem)request.getAttribute("scenDetail");
                String caption = helpItem.getTypeName() + " Detail";
             
             %>
       
             <hbj:textView
                     id="ItemDetailCaptionTextView"
                     text="<%= caption%>"
                     design="HEADER1"
            />
            <br/>
             <hbj:textView
                     id="ItemDetailNameTextView"
                     text="Configuration: "
                     design="HEADER3"
            />
             <hbj:textView
                     id="ItemDetailName"
                     text="<%= helpItem.getName()%>"
                     design="STANDARD"
            />
            <br/>
             <%-- base config --%>
             <hbj:textView
                     id="ItemDetailBaseConfig"
                     text="base configuration: "
                     design="HEADER3"
            />        
             <hbj:textView
                     id="ItemDetailBaseConfig"
                     text="<%= helpItem.getBaseConfigName()%>"
                     design="STANDARD"
            />                
            <br/>        
             <%-- shorttext --%>
             <hbj:textView
                     id="ItemDetailShorttext"
                     text="<%= helpItem.getShorttext()%>"
                     design="STANDARD"
            />        
            <br/>
             <%-- longtext --%>
             <hbj:textView text="<%= helpItem.getLongtext()%>" encode="false" wrapping="true" />
  
        </hbj:form>
    </hbj:page>
    
        
</hbj:content>        