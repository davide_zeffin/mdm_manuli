<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer" %>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.HelpConstants" %>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.ControllerUtils"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<%@ taglib uri="/htmlb" prefix="hbj" %>

<% 
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	HelpController mHelpController = appConfigController.getHelpController();
%> 


<hbj:content id="xcmHelpItemDetail" >
<%
	BaseUI helpCompConfUi = new BaseUI(pageContext);
	if(helpCompConfUi.isAccessible()) {
		 xcmAdminContext.setSection508Rendering(true);
	}
%> 
  <hbj:page title = "helpItem">

    <hbj:form id="xcmHelpItemDetailForm">
             <%
                HelpItem helpItem = mHelpController.getCurrentDetailItem();
                String paramName = helpItem.getName();
                String scenName = mHelpController.getCurrentScenarioName();
                // check if parameter depends on component configuration
                String compName = ControllerUtils.getScenarioParamDependentComponentName(paramName);
								if (compName == null)  {
              		mHelpController.setCurrentDetailMode(
              				HelpController.MODE_DETAIL_SHOW_VALUE + 
              				HelpController.MODE_DETAIL_NAME_AS_LINK_DEST +
              				HelpController.MODE_DETAIL_VALUE_AS_LINK);
              	} else {
              		mHelpController.setCurrentDetailMode(
              				HelpController.MODE_DETAIL_SHOW_VALUE + 
              				HelpController.MODE_DETAIL_NAME_AS_LINK_DEST + 
              				HelpController.MODE_DETAIL_VALUE_AS_LINK);
              	}
             %>

            <!-- parameter details -->
            <%@ include file="HelpItemDetail.inc" %>  
             
						<%
		        		HelpItemContainer allowedValuesContainer = mHelpController.getScenParamAllowedValuesContainer(scenName, paramName);
                mHelpController.setCurrentSummaryContainer(allowedValuesContainer);
                mHelpController.setCurrentSummaryMode(
                			HelpController.MODE_SUMMARY_TABLE_SHORTTEXT +
                			HelpController.MODE_SUMMARY_TABLE_LONGTEXT +
                			HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK_DEST);
             %>
             <!-- allowed value summary -->  
             <% if (compName != null) { 
                mHelpController.setCurrentSummaryMode(
                		HelpController.MODE_SUMMARY_TABLE_SHORTTEXT + 
               			HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK_DEST
               	);             
             %>
		             <hbj:textView
		                     id="ItemDetailCaptionTextView"
		                     text="Depends on Component Configuration:"
		                     design="HEADER2"
		            />                                             
		             <hbj:textView
		                     id="ItemDetailCaptionTextView"
		                     text="<%=compName%>"
		                     design="HEADER2"
		            />                                                         
             <% } %>
             <p/>
             <hbj:textView
                     id="ItemDetailCaptionTextView"
                     text="Allowed Values Summary"
                     design="HEADER2"
            />                                
            <%@ include file="HelpItemSummary.inc"/>                                          
             
        </hbj:form>
    </hbj:page>
    
        
</hbj:content>        