<%@ page import="com.sap.isa.core.xcm.admin.actionform.ToolbarForm" %>
<%@ page import="com.sap.isa.core.xcm.admin.ui.ComponentDetailCellRenderer" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin_RO(request) || !com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<%
		String sapLogoHref = "mimes/s_b_wiza.gif";		
		ComponentDetailCellRenderer.setLogoHref(sapLogoHref);
%>



<hbj:form id="xcmToolbarForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/toolbarClick.do\" , null , null , false)%>"> 

  <table  cellpadding="3<"
          cellspacing="2"
          border="0"
          align="right"
          width="100%">
	<tr>
		<td>
			<%
			if( !mainConfigUi.isAccessible()) { //
				String logoHref = "mimes/logo.gif";
			%>
			
				<h3>
				<hbj:image id="Logo" src="<%= logoHref %>"  alt="picture saplogo.gif not found"/>

				<%=WebUtil.translate(pageContext, ToolbarForm.TOOLBAR_TITLE, null)%> 

				</h3>	
			<%}
			else
			{ %>
			<hbj:textView text="<%=WebUtil.translate(pageContext, ToolbarForm.TOOLBAR_TITLE, null)%>" design="HEADER3" />
			<%} %>	
		</td>
	</tr>
  

    <tr>
      <td align="right">
      	<hbj:button id="configuration" 
      							text="<%=WebUtil.translate(pageContext, ToolbarForm.CONFIGURATION_TEXT, null)%>"
      							tooltip="<%=WebUtil.translate(pageContext, ToolbarForm.CONFIGURATION_TOOLTIP, null)%>"
      							onClick="<%=ToolbarForm.CONFIG_ON_CLICK_EVENT %>"/>

      	<hbj:button id="monitoring" 
      							text="<%=WebUtil.translate(pageContext, ToolbarForm.MONITORING_TEXT, null)%>"
      							tooltip="<%=WebUtil.translate(pageContext, ToolbarForm.MONITORING_TOOLTIP, null)%>"
      							onClick="<%=ToolbarForm.MONITORING_ON_CLICK_EVENT %>"/>
				<hbj:button id="Documentation" 
				            text="<%=WebUtil.translate(pageContext, ToolbarForm.DOCUMENTATION_TEXT, null)%>" 
				            onClientClick="openDocumentation()"
				            tooltip="<%=WebUtil.translate(pageContext, ToolbarForm.DOCUMENTATION_TOOLTIP, null)%>"/>
      </td>
    </tr>
  </table>
</hbj:form>

