<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% 

	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	ComponentConfigController compConfigController = appConfigController.getCompConfigController();
	
	String helpText = compConfigController.getCurrentTestDescription();
	String helpTitle = compConfigController.getCurrentTestName();
	if (helpText == null)
		helpText = "";
 
%>

<hbj:content id="xcmHelpDescription" >
	<%
		BaseUI helpdesc = new BaseUI(pageContext);
		if(helpdesc.isAccessible()) {
			xcmHelpDescription.setSection508Rendering(true);
		}
	%>
  <hbj:page title = "<%= helpTitle %>">
		
		<hbj:form id="helpForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/compConfig.do\" , null , null , false)%>">		
 
			<hbj:group id="HelpTextGroup" design="sapcolor" title="<%= helpTitle %>" tooltip="<%= helpTitle %>">
		
				<hbj:groupBody>
					<%= helpText %>
				</hbj:groupBody>
		
	
			</hbj:group>

		</hbj:form>			
	
	</hbj:page>
</hbj:content>
	
