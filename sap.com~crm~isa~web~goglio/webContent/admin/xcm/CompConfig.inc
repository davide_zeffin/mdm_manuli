<%@ page import="com.sap.isa.core.xcm.admin.model.table.ComponentDetailTableModel" %>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sapportals.htmlb.IListModel" %>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.CompConfigForm" %>
<%@ page import="com.sapportals.htmlb.Group" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin_RO(request) || !AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>

 
<%
		String compId   = compConfigController.getCurrentCompId();
		String configId = compConfigController.getCurrentCompId();	
		
		ComponentDetailTableModel compDetailTable = compConfigController.getCompDetailTableModel();
		pageContext.setAttribute("compDetailTable", compDetailTable);
		TableView configTableView = compConfigController.getCompDetailTableView();
		IListModel baseConfigListModel = compConfigController.getBaseConfigListModel();
		pageContext.setAttribute("baseConfigListModel", baseConfigListModel);
		IListModel testListModel = compConfigController.getTestListModel();	
		pageContext.setAttribute("testListModel", testListModel);
		String compConfigGroupTitle = "Component configuration: '" + compConfigController.getCurrentParamConfigId() + "'";
		String compDescr = WebUtil.translate(pageContext, CompConfigForm.COMP_DESCR, null) + compId;
		String helpIconHref = "mimes/sap_hint.gif";	
		String helpCompHref = "xcm/helpText.jsp";		
		String helpTestHref = "testHelpText.jsp";		
		String isBaseListBoxDisabled = String.valueOf(!compConfigController.isCurrentParamConfigNew()); 
		String saveButtonDisabled = String.valueOf(compConfigController.isSAP());
		String deleteButtonDisabled = String.valueOf(compConfigController.isSAP() || compConfigController.isCurrentParamConfigNew());
		String compDescrGroupTitle = WebUtil.translate(pageContext, CompConfigForm.TRAY2_TOOLTIP, null) + compId + "'";
%>
 
<hbj:form id="configContentForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/compConfig.do\" , null , null , false)%>"> 

<table  width="100%" cellspacing="3">

	<tr> 
		<td align="left">
			<% if (appConfigController.isConfigCompFromScenario()) {
			%>
			<% if(mainConfigUi.isAccessible()) { %>
			 <hbj:button id="back" disabled="false" text="<%=WebUtil.translate(pageContext, CompConfigForm.BACK_BUTTON_TEXT_ACCESS, null) %>" onClick="<%=CompConfigForm.ON_SCENARIO_BACK_COMP_CONFIG_EVENT%>" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.BACK_BUTTON_TOOLTIP, null) %>"/>
			<% } else { %>
			 <hbj:button id="back" disabled="false" text="<%=WebUtil.translate(pageContext, CompConfigForm.BACK_BUTTON_TEXT, null) %>" onClick="<%=CompConfigForm.ON_SCENARIO_BACK_COMP_CONFIG_EVENT%>" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.BACK_BUTTON_TOOLTIP, null) %>"/>
			<% } %>
			
		<% } %> 

     
		<% if (!compConfigController.isSAP() && appConfigController.isEditMode()) { %>
		    
			<hbj:button id="save" 
				disabled="<%=saveButtonDisabled%>" 
				text="<%=WebUtil.translate(pageContext, CompConfigForm.SAVE_BUTTON_TEXT, null) %>" 
				onClientClick="cancelSave()" 
				onClick="<%=CompConfigForm.ON_CONFIG_SAVE_EVENT%>" 
				tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.SAVE_BUTTON_TOOLTIP, null) %>"/>
			<% if (!compConfigController.isCompGeneral()) { %>
				<hbj:button id="delete" 
					disabled="<%=deleteButtonDisabled%>" 
					text="<%=WebUtil.translate(pageContext, CompConfigForm.DELETE_BUTTON_TEXT, null) %>" 
					onClientClick="cancelDelete()" 
					onClick="<%=CompConfigForm.ON_CONFIG_DELETE_EVENT%>" 
					tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.DELETE_BUTTON_TOOLTIP, null) %>"/> 
			<% } %>

		<% } 
		   if(compConfigController.isSAP()) { 
			    if (appConfigController.isConfigCompFromScenario()) { %>
		    
			<hbj:textView text="<%=WebUtil.translate(pageContext, CompConfigForm.WARNING1_TEXT1, null) + compId + WebUtil.translate(pageContext, CompConfigForm.WARNING1_TEXT2, null) %>" encode="false" wrapping="true" />
		    
		    <%  } else { %>
		    <hbj:textView text="<%=WebUtil.translate(pageContext, CompConfigForm.WARNING2_TEXT, null) %>" encode="false" wrapping="true"/>
			<%	} 
			} %>		
		<hbj:button id="documentation1" text="<%=WebUtil.translate(pageContext, CompConfigForm.DOCU1_BUTTON_TEXT, null) %>" onClientClick="openHelpComponent()" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.DOCU1_BUTTON_TOOLTIP, null) %>"/>
		</td>
	
	</tr>
</table>

	<hbj:group id="CompConfigGroup"
	           design="sapcolor"
	           title="<%=compConfigGroupTitle%>"
	           tooltip="<%=compConfigGroupTitle%>">
		<hbj:groupBody>

			<table width="100%">
				<tr>
					<td colspan=2>
							<hbj:label  id="labelScenShorttext" labelFor="<%=CompConfigForm.COMP_SHORT_TEXT_INPUT_FIELD_ID%>" 
									labeledComponentClass="DropdownListBox" 
									text="<%=WebUtil.translate(pageContext, CompConfigForm.COMPSHORT_LABEL_TEXT, null) %>"
									tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.COMPSHORT_LABEL_TOOLTIP, null) %>" />
									
							<hbj:inputField id="<%=CompConfigForm.COMP_SHORT_TEXT_INPUT_FIELD_ID %>"
										type="String"
										value="<%= compConfigController.getCurrentCompShorttext()%>"
										visible="true"
										disabled ="true"
										required="false"
										maxlength="1024"
										size="100"
										design="standard"/>
							<hbj:button id="componentHelpButton" text="?" onClientClick="openHelp('component')" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.COMP_HELP_TOOLTIP, null) %>"/>
					</td>				
				</tr>
				<tr>
					<td>
					<table>
						<tr>
							<td>
								<hbj:label id="compid_label" text="<%=WebUtil.translate(pageContext, CompConfigForm.LABEL_COMPID_TEXT, null) %>" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.LABEL_COMPID_TOOLTIP, null) %>" labelFor="<%=CompConfigForm.COMP_NAME_INPUT_FIELD_ID%>" labeledComponentClass="DropdownListBox"/>
							</td>
							<td>
								<hbj:inputField id="<%=CompConfigForm.COMP_NAME_INPUT_FIELD_ID%>" value="<%= compId %>" disabled="true" />
							</td>
						</tr>
						<tr>
							<% if (!compConfigController.isCompGeneral()) { %>
								<td>
									<hbj:label id="baseconfLabel" text="<%=WebUtil.translate(pageContext, CompConfigForm.LABEL_BASECONF_TEXT, null) %>" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.LABEL_BASECONF_TOOLTIP, null) %>" labelFor="<%=CompConfigForm.BASE_LIST_BOX_ID%>" labeledComponentClass="DropdownListBox" />
								</td>
								<td>
								<%
							  	if (mainConfigUi.isAccessible() && !compConfigController.isCurrentParamConfigNew()) { 
							  		String selection = baseConfigListModel.getSingleSelection();
							  	%>
						  				<hbj:inputField id="<%=CompConfigForm.BASE_LIST_BOX_ID%>"
											type="String"
											value="<%= baseConfigListModel.getTextForKey(selection) %>"
											visible="true"
											disabled ="true"
											design="standard"/>
								<%
							  	} else {
							  %>
							      <hbj:dropdownListBox  id="<%=CompConfigForm.BASE_LIST_BOX_ID%>"
							                            tooltip="<%=compConfigController.getCurrentBaseConfigShorttext()%>"
							                            disabled="<%=isBaseListBoxDisabled %>"
							                            model="baseConfigListModel"
							                            onSelect="<%= CompConfigForm.ON_BASE_SELECT_EVENT %>"
							      >
							      </hbj:dropdownListBox>
							  <% } %>							  
                				  <hbj:button id="compConfigHelpButton" text="?" onClientClick="openHelp('compConfig')" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.BASECOMP_HELP_TOOLTIP, null) %>"/>							     
							     </td>

							<% } else { %>
								<td>&nbsp;</td>
							
							<% } %>
						</tr>			
					</table>
				</td>
				<td>
					<table height="100%" width="100%">
		 				<tr>
							<td>
								<hbj:label id="selTestLabel" text="<%=WebUtil.translate(pageContext, CompConfigForm.LABEL_CONFIGTEST_TEXT, null) %>" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.LABEL_CONFIGTEST_TOOLTIP, null) %>" labelFor="<%=CompConfigForm.TEST_LIST_BOX_ID%>" labeledComponentClass="DropdownListBox"/>
							</td>
							<td>
								<%
							  	if (mainConfigUi.isAccessible() && !appConfigController.isEditMode()) { 
							  		String selection = testListModel.getSingleSelection();
							  	%>
						  				<hbj:inputField id="<%=CompConfigForm.TEST_LIST_BOX_ID%>"
											type="String"
											value="<%= testListModel.getTextForKey(selection) %>"
											visible="true"
											disabled ="true"
											design="standard"/>
								<%
							  	} else {
							  %>
								<hbj:dropdownListBox  id="<%=CompConfigForm.TEST_LIST_BOX_ID%>"
                    				tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.DROPDOWN_CONFIGTEST_TOOLTIP, null) %>"
                    				disabled="<%=compConfigController.isTestDisabled() %>"
                    				model="testListModel"
                    				onSelect="<%= CompConfigForm.ON_TEST_SELECT_EVENT %>"
  											>
  								</hbj:dropdownListBox>		
							  <% } %>							  

								<hbj:button id="testHelpButton" disabled="<%=compConfigController.isTestDisabled()%>" text="?" onClientClick="openHelp('test')" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.HELP_CONFIGTEST_TOOLTIP, null) %>"/>
							</td>
						</tr>
						<tr>
							<td>
							 
							 <% if(mainConfigUi.isAccessible()) { %>
							 <hbj:label id="runtestlabel" text="<%=WebUtil.translate(pageContext, CompConfigForm.LABEL_SAVECONF_TEXT, null) %>" labelFor="runtest"/>
							 </td>
							 <td>
							 <hbj:button id="runtest" text="<%=WebUtil.translate(pageContext, CompConfigForm.BUTTON_RUNTEST_TEXT, null) %>" disabled="<%=compConfigController.isTestDisabled()%>"
							 		onClientClick="openWindow()" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.BUTTON_RUNTEST_TOOLTIP, null) %>"/>
							 </td>
							 <% } else { %>
							 <hbj:button id="runtest" text="<%=WebUtil.translate(pageContext, CompConfigForm.BUTTON_RUNTEST_TEXT, null) %>" disabled="<%=compConfigController.isTestDisabled()%>"
							 		onClientClick="openWindow()" tooltip="<%=WebUtil.translate(pageContext, CompConfigForm.BUTTON_RUNTEST_TOOLTIP, null) %>"/>
							 </td>
							 <td>
                             <hbj:textView text="<%=WebUtil.translate(pageContext, CompConfigForm.LABEL_SAVECONF_TEXT, null) %>"/>								 		
							</td>
							<% } %>
						</tr>

					</table>					
				</td>
			</tr>
		</table>
		</hbj:groupBody>
 	</hbj:group>
		<%
		configTableView.setHeaderText(WebUtil.translate(pageContext, CompConfigForm.TRAY2_TEXT, null));
		configTableView.setHeaderVisible(true);
		xcmAdminContext.render(configTableView); 	
		%>
		<table width=100%> 
			<tr>
				<td>
				<%	if(!mainConfigUi.isAccessible()) { %>
					<hbj:textView text="<%=WebUtil.translate(pageContext, CompConfigForm.LEGEND_TEXT, null) %>" encode="false"/>
				<% } %>
		
				</td>
			</tr>
		</table>		
</hbj:form>