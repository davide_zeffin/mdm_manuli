<%@ page import="java.util.*" %>
  
<%@ page import="com.sap.isa.core.xcm.admin.Constants" %>
<%@ page import="com.sap.isa.catalog.admin.util.WebUtil"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.MainNavigationForm" %>
    
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<%    
 	String selectedIndex = (String)session.getAttribute(MainNavigationForm.SELECTED_INDEX);
	if (selectedIndex == null) 
		selectedIndex = "1";	
    
%> 

<hbj:content id="adminHomePageContent" >
  <hbj:page title = "Application Configuration">
  
 <table>
   
 <tr>
 <td><%@ include file="Toolbar.inc"%></td>
 </tr>
 <tr>
 <td>
<hbj:form id="xcmMainForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/mainSelect.do\" , null , null , false)%>">		
      
   
    <hbj:tabStrip id="xcmMainTabStrip"
                  bodyHeight="<%=Constants.DISPLAY_MAIN_TABSTRIP_HEIGHT %>"
                  width="<%=Constants.DISPLAY_MAIN_TABSTRIP_WIDTH %>"
                  horizontalAlignment="CENTER"
                  verticalAlignment="TOP"
                  selection="<%= selectedIndex %>"
                  tooltip="Tooltip for myTabStrip1"
    >



        <hbj:tabStripItem id="xcmConfigurationStripItem"
                          index="1"
                          height="80"
                          width="160"
                          onSelect="<%=MainNavigationForm.TAB_SELECT_EVENT %>"
                          title="Application Configuration"
                          tooltip="Application Configuration"
        >

          <hbj:tabStripItemBody>
            <%@ include file="MainCustomizing.inc"%>
          </hbj:tabStripItemBody> 
</hbj:tabStripItem>  

        <hbj:tabStripItem id="xcmMonitoringStripItem"
                          index="2"
                          height="80"
                          width="160"
                          onSelect="<%=MainNavigationForm.TAB_SELECT_EVENT %>"
                          title="XCM Monitoring"
                          tooltip="XCM Monitoring"
        >

          <hbj:tabStripItemBody>
            <%@ include file="MainMonitoring.inc"%>
          </hbj:tabStripItemBody>
</hbj:tabStripItem>
  

    </hbj:tabStrip>

  
</hbj:form>  
  </td>
  </tr>
  </table>
  </hbj:page>
</hbj:content>
