<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ScenarioConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer" %>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.ScenarioConfigForm"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpTextForm"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.MainConfigForm"%>

<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% 
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	ComponentConfigController compConfigController = appConfigController.getCompConfigController();
	ScenarioConfigController scenConfigController = appConfigController.getScenarioConfigController();
	HelpController mHelpController = appConfigController.getHelpController();
	String scenName = null;
	String scenParamItem = null;
	String compName = null;
	String compParamName = null;
	HelpItemContainer allowedValuesContainer = null;
	HelpItemContainer paramSummaryContainer = null;
	HelpItem compParamItem = null;

	String helpType = (String)request.getParameter("type");
	String helpText = "";
	String title = "Help";
		
  if (helpType.equals("component")) {
  	String compShorttext = compConfigController.getCurrentCompShorttext();
	String compLongtext = compConfigController.getCurrentCompLongtext();
  	helpText = "<b>" + compShorttext + "</b>" + "<p/>" + compLongtext;
  	title = WebUtil.translate(pageContext, HelpTextForm.COMPONENT_TITLE, null) + compConfigController.getCurrentCompId() + "'";
  }

	if (helpType.equals("scenario")) {
	  String scenShorttext = scenConfigController.getCurrentScenarioShorttext();
	  String scenLongtext = scenConfigController.getCurrentScenarioLongtext();
	  helpText = "<b>" + scenShorttext + "</b>" + "<p/>" + scenLongtext;
	  title = WebUtil.translate(pageContext, HelpTextForm.SCENARIO_TITLE_2, null) + scenConfigController.getCurrentScenarioName() + "'";
	}

 
  if (helpType.equals("test")) {
  	helpText = compConfigController.getCurrentTestDescription();
  	title = "Test '" + compConfigController.getCurrentTestName() + "'";
  }  

  if (helpType.equals("scenParamList")) {
	scenParamItem = (String) userData.getAttribute(SessionConstants.PARAM_NAME);
	scenName = (String) userData.getAttribute(SessionConstants.SCEN_NAME);
	allowedValuesContainer = mHelpController.getScenParamAllowedValuesContainer(scenName, scenParamItem);
	mHelpController.setCurrentSummaryContainer(allowedValuesContainer);
	mHelpController.setCurrentSummaryMode(
		HelpController.MODE_SUMMARY_TABLE_SHORTTEXT +
		HelpController.MODE_SUMMARY_TABLE_LONGTEXT +
		HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK_DEST, "", "scenParamValue");
  }
 
 if (helpType.equals("compParamList")) {
	compName = (String) userData.getAttribute(SessionConstants.COMP_NAME);
 	compParamName = (String) userData.getAttribute(SessionConstants.PARAM_NAME);	
	paramSummaryContainer = mHelpController.getComponentParamContainer(compName); 
 	compParamItem = paramSummaryContainer.getItem(compParamName); 
 	allowedValuesContainer = compParamItem.getChildren();
	mHelpController.setCurrentSummaryContainer(allowedValuesContainer);
 }
 /* if (helpType.equals("compConfig")) {
    String shortText = compConfigController.getCurrentBaseConfigShorttext();
    String longText = compConfigController.getCurrentBaseConfigLongtext();
    if (longText != null && longText.length() > 0)
  	    helpText = shortText + "<p/>" + longText;
  	else    
  	    helpText = shortText;
  	
  	if (helpText == null || helpText.length() == 0)
  		helpText = "Description missing for " + compConfigController.getCurrentBaseParamConfigId() + " configuration";
  	title = "Base Component Configuration '" + compConfigController.getCurrentBaseParamConfigId() + "'";
  } */

if (helpType.equals("scenParamValue")) {
	String shorttext = (String)userData.getAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP_SHORTTEXT);
	String longtext = (String)userData.getAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP_LONGTEXT);
	helpText = "<b>" + shorttext + "</b>" + "<p/>" + longtext;
	String paramValue = (String)userData.getAttribute(SessionConstants.SCEN_CONFIG_PARAM_VALUE_HELP);
	title = paramValue;
  }
 
  if (helpType.equals("compConfig")) {
	String shortText = compConfigController.getCurrentBaseConfigShorttext();
	String longText = compConfigController.getCurrentBaseConfigLongtext();
	if (longText != null && longText.length() > 0)
		helpText = shortText + "<p/>" + longText;
	else    
		helpText = shortText;
  	
	if (helpText == null || helpText.length() == 0)
		helpText = WebUtil.translate(pageContext, HelpTextForm.ALT_TEXT_MISSING_DESCRIPTION1, null) + compConfigController.getCurrentBaseParamConfigId() + WebUtil.translate(pageContext, HelpTextForm.ALT_TEXT_MISSING_DESCRIPTION2, null);
	title = WebUtil.translate(pageContext, HelpTextForm.BASE_COMP_TITLE, null) + compConfigController.getCurrentBaseParamConfigId() + "'";
  }
  
  if (helpType.equals("defaultScenario")) {
  	helpText = (String)session.getAttribute("defaultScenarioHelp");
  	if (helpText == null || helpText.length() == 0)
  		helpText = WebUtil.translate(pageContext, HelpTextForm.ALT_TEXT_MISSING, null);
  	title = WebUtil.translate(pageContext, HelpTextForm.DEFAULT_SCEN_TITLE, null);
  }
  
  if (helpType.equals("activateScenario")) {
	helpText = (String)session.getAttribute("activateScenarioHelp");
	if (helpText == null || helpText.length() == 0)
		helpText = WebUtil.translate(pageContext, HelpTextForm.ALT_TEXT_MISSING, null);
	title = WebUtil.translate(pageContext, HelpTextForm.ACTIVATE_SCEN_TITLE, null);  	
  }

	if (helpType.equals(HelpTextForm.ADVANCED_SETTINGS_ATTR)) {
	  helpText = WebUtil.translate(pageContext, ScenarioConfigForm.ADVANCED_SETTINGS_DESCR, null);
	  title = WebUtil.translate(pageContext, ScenarioConfigForm.ADVANCED_SETTINGS_TITLE, null);  	
	}

	if (helpType.equals(HelpTextForm.CONFIG_TYPE_ATTR)) {
	  helpText = WebUtil.translate(pageContext, MainConfigForm.XCM_CONFIG_TEXT, null);
	  title = WebUtil.translate(pageContext, MainConfigForm.XCM_CONFIG_TITLE, null);  	
	}

%>

<hbj:content id="xcmAdminContext" >
<%
	BaseUI helpDescriptionUi = new BaseUI(pageContext);
	if(helpDescriptionUi.isAccessible()) {
		xcmAdminContext.setSection508Rendering(true);
	}
%>
  <hbj:page title = "Help">
		
		<hbj:form id="helpForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/compConfig.do\" , null , null , false)%>">		
	
	<%if (helpType.equals("scenParamList")) {%>
		 <hbj:group id="AllwdValues" title="<%=WebUtil.translate(pageContext, HelpTextForm.SCENARIO_TITLE_1, null) + scenParamItem %>" 
		 			tooltip="<%=WebUtil.translate(pageContext, HelpTextForm.SCENARIO_TOOLTIP, null) + scenParamItem %>"
		 			design="sapcolor"
		 			width="100%">
		 <hbj:groupBody>
			<%@ include file="HelpItemSummary.inc" %>
		</hbj:groupBody>
	</hbj:group>  
	<%}else if (helpType.equals("compParamList")){%>
		<hbj:group id="AllwdValues" title="<%=WebUtil.translate(pageContext, HelpTextForm.SCENARIO_TITLE_1, null) + compParamName %>" 
		 			tooltip="<%=WebUtil.translate(pageContext, HelpTextForm.SCENARIO_TOOLTIP, null) + scenParamItem %>"
		 			design="sapcolor"
		 			width="100%">
		 <hbj:groupBody>
		 <% mHelpController.setCurrentSummaryContainer(allowedValuesContainer); %>
			<%@ include file="HelpItemSummary.inc" %>
		</hbj:groupBody>
	</hbj:group>
	<% } else { %>
	
		<table width="100%">
			<tr>
				<td align="right">
	 				<hbj:button id="closewindow" text="close" onClientClick="close()"/>
	 			</td>
	 		</tr>
	 		<tr>
				<td>	 		
						<hbj:group id="HelpTextGroup" design="sapcolor" title="<%=title%>" tooltip="<%= title %>">
							<hbj:groupBody>
								<hbj:textView text="<%= helpText %>" encode="false" wrapping="true"/>
							</hbj:groupBody>
						</hbj:group>
				</td>
			</tr>
		</table>
	<%} %>
		</hbj:form>			
	
	</hbj:page>
</hbj:content>
	
