<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.HelpConstants" %>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.ui.HelpSummaryCellRenderer"%>
<%@ page import="com.sapportals.htmlb.Link" %>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpItemDetailForm"%>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>


<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% { %>

<% 
	HelpItem helpItem1 = mHelpController.getCurrentDetailItem();
	try {
        
	    String caption = helpItem1.getTypeName() + " Detail";
	    
	} catch (Throwable t) {
	    t.printStackTrace();
	}
 
%>
<p/>
<%
	String scope = helpItem1.getScope();
%>

<% if(!mHelpController.checkCurrentDetailMode(HelpController.MODE_DETAIL_NAME_AS_LINK_DEST)) { %>

<% } else { %>
             
<% 	String name1 = mHelpController.getCurrentLinkDestPrefix() + helpItem1.getName(); %>
    	<a name="<%=name1%>" >
	    </a> 


<% } %>
<table>
<% if (scope != null && scope.length() > 0) {
%>

	<tr> 
		<td>
			<hbj:textView text="<h4>Scope:</h4>" encode="false" />
		
		</td>

		<td>
		<h3>
			<hbj:textView text="<%= scope%>" design="header3" encode="false"/>
		</h3>
		
		</td>
	</tr>
<% } %>

<% if(mHelpController.checkCurrentDetailMode(HelpController.MODE_DETAIL_SHOW_VALUE)) { %>
	          <%-- value --%>
	<tr>
		<td>
			<hbj:textView text="<h4>Value:  </h4>" encode="false" design="header4" />
  	</td>
		<td>
<% if(!mHelpController.checkCurrentDetailMode(HelpController.MODE_DETAIL_VALUE_AS_LINK)) { %>
			<h4>
       <hbj:textView text="<%= helpItem1.getConfigValue()%>" encode="false" design="header4"/>
      </h4>
<% } else { %>
 
<h4>
<% String link = "#" + mHelpController.getCurrentLinkPrefix() + helpItem1.getConfigValue(); 
   Link val_link = new Link("val_link");
   val_link.addText(helpItem1.getConfigValue());
   val_link.setReference(link);
   xcmAdminContext.render(val_link);
%>
  
</h4>  
            

<% } %>                     
    </td>
   </tr>
<% } %>

<% if(mHelpController.checkCurrentDetailMode(HelpController.MODE_DETAIL_BASE_CONFIG_NAME)) { %>
		<tr>
			<td>
             <%-- base config --%> 
             <% String baseText = "Base " + helpItem1.getTypeName() + ":" ; %>
						 <hjb:textView text="<%=baseText%>" design="header4" encode="false"/>
      </td>
			<td>
					<h4>
						<hbj:textView text="<%= helpItem1.getBaseConfigName()%>" design="header4" encode="false"/>
					</H4>
      </td>
    </tr>
<% } %>  
</table>              

             <%-- shorttext --%>
             <hbj:textView text="<%= helpItem1.getShorttext()%>" wrapping="true" encode="false"/>
            <br/>
             <%-- longtext --%>
             <hbj:textView text="<%= helpItem1.getLongtext()%>"  wrapping="true" encode="false" />

<% } %>     
