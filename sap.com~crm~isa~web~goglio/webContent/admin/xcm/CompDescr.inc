<%@ page import="com.sap.isa.core.xcm.admin.actionform.CompDescrForm" %>
<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<hbj:form id="configContentForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/mainSelect.do\" , null , null , false)%>"> 

	<hbj:group id="ComponentNameGroup"
	           design="sapcolor"
	           title="<%=WebUtil.translate(pageContext, CompDescrForm.GROUP_TITLE, null) %>"
	           tooltip="<%=WebUtil.translate(pageContext, CompDescrForm.GROUP_TOOLTIP, null) %>" >
		<hbj:groupBody>
	  	<hbj:textView text="<%=WebUtil.translate(pageContext, CompDescrForm.TEXT, null) %>" />
			</BR>
	
		</hbj:groupBody>
	</hbj:group>
</hbj:form>  