<%@ page import="com.sap.isa.core.xcm.admin.CheckXCMConsitency" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>



<%! 

	String MISSING = "<b><font color=FF0000>MISSING</font></b>";
	String MISSING_ORANGE = "<b><font color=E0E000>MISSING</font></b>";	
	String OK = "ok";
	
%>
<%

%> 
<html>
  <head>
	<title>Check XCM Application Configuration Meta-Data Consistency</title>
  </head>
  <body>
	<h2>Check XCM Application Configuration Meta-Data Consistency</h2>
	This pages are used to check consistency of XCM Admin meta data. The consistency of data in the following files ich checked:
	<ul>
		<li>
			Check if <b>Component Configuration</b> meta-data is maintained correctly in isa-xcmadmin-config.xml or in subproject-xcmadmin-config.xml
		</li>
		<li>
			Check if <b>Application Configuration</b> meta-data is maintained correctly in SAP version of scenario-config.xml
		</li>
		<li>
			Check if meta-data is maintained correctly in SAP version of config-data.xml
		</li>
	</ul>
	Click on the links below to open the two available checks. </br>
	Red coloured <%=MISSING%> entries indicate inconsistencies in the data which MUST be fixed</br>
	Orange coloured <%=MISSING_ORANGE%> indicates that some optional data is missing. It is recommended to provided this data.
	After fixing meta-data you have to restart the web application.
	<ul>
		<li>
			<a href="CheckComponentMetadata.jsp" target="_new">check Component Configuration</a>
		</li>
		<li>
			<a href="CheckScenarioMetadata.jsp" target="_new">check Application Configuration</a>
		</li>
	</ul>

	
  </body>
</html>

