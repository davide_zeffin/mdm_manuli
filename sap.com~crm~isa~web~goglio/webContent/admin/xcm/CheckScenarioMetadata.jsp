<%@ page import="com.sap.isa.core.xcm.admin.CheckXCMConsitency" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator" %>
<%@ page import="com.sap.isa.core.xcm.init.XCMAdminParamConstrainMetadata" %>
<%@ page import="com.sap.isa.core.xcm.XCMUtils" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.CompDataManipulator" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.ScenarioDataManipulator" %>
<%@ page import="com.sap.isa.core.xcm.admin.controller.ControllerUtils" %>
<%@ page import="com.sap.isa.core.xcm.admin.Constants" %>
<%@ page import="com.sap.isa.core.xcm.init.XCMAdminAllowedValueMetadata" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>



<%! 

	String MISSING = "<b><font color=FF0000>MISSING</font></b>";
	String MISSING_ORANGE = "<b><font color=E0E000>MISSING</font></b>";	
	String OK = "ok";
	
%>
<%
	CompDataManipulator compDataMan = CheckXCMConsitency.getCompDataManipulator();
	ScenarioDataManipulator scenDataMan = CheckXCMConsitency.getScenDataManipulator();

%>
<html>
  <head>
	<title>Check XCM Application Configuration Meta-Data Consistency</title>
  </head>
  <body>
	<h2>Check XCM Application Configuration Meta-Data Consistency</h2>
		Perform the following checks:
		For further details refer to the <a href="http://tekteam.wdf.sap.corp:1080/documentation.shtml/" target=_new>XCM Admin tutorial</a>
		
	Application Configuration Overview Table
	<ul>
		<li>
			Check if shorttext AND longtext is maintained for all your application configurations. This is done in SAP version of scenario-config.xml
		</li>
		
	</ul>
	Application configuraration details tables. Check meta data for each of your application configurations. 
	<ul>	
		<li>Column <b>type</b> </BR>
			Check if the type of the parameter is ok. 'mandatory' means that the parameter must be maintained by the customer in order to run the application. 'optional' means that the parameter is hidden. The customer has to press the 'Display Advanced Settings' button to see this parameter. </br>
			This value is centraly maintained for all applications in xcmadmin-config.xml. If you need to overwrite it for a specific application configuration you can do it in scenario-config.xml
		</li>
		<li>
			Column <b>Metadata</b></br>
			Missing if this parameter is used in as application configuration in scenario-config.xml but there is not meta data in xcmadmin-config.xml at all
		</li>
		<li>
			Column <b>Shorttext</b></br>
			Mandatory. Maintained in xcmadmin-config.xml
		</li> 
		<li>
			Column <b>Longext</b></br>
			Optional but recommended. Maintained in xcmadmin-config.xml
		</li> 
		<li>
			Column <b>allowed values</b></br>
			Here you get two types of information:
			<ul>
				<li>
					All AVAILABLE parameters defined in xcmadmin-config.xml. 
					This helps you to get an overview on all available parameters. You can decide if some parameters are missing or obsolete
				</li>	
				<li>
					All PROPOSED parameters for this particular application configuration. 
					This list either has the same entries as the list of all available entries or less entries.
					If you want to have less entries you have to define them in SAP version of scenario-config.xml
					Please note that you can only REDUCE the number of the available entries. You can not introduce new in 
					scenario-config.xml. 
				</li>
			</ul>
			If the parameter is dependent on a component configuration, check for correct base configurations.
			In addition check the type of the SAP configuration the customer extends from:
			<ul>
				<li>
					The SAP configuration AND cutomer extensions of the SAP configuration
				</li>	
				<li>
					Only customer extension of the SAP configuration. The SAP configuration is not shown.
				</li>
			</ul>
			
			
		</li>

	</ul>
	<h2>Application Configurations</h2>
<table border=1>
	<tr>
		<th>
			Scenario
		</th>
		<th>
			Shorttext (man) 
		</th> 
		<th>
			Longtext (man) 
		</th>
	</tr>
<%
		Map result = CheckXCMConsitency.getSAPScenarioConfigs(scenDataMan, compDataMan);
		for (Iterator iter = result.keySet().iterator(); iter.hasNext();) {
			String isShorttext = OK;
			String isLongtext = OK;
			String scenName = (String)iter.next();
			ScenarioDataManipulator.ScenarioConfig sc = (ScenarioDataManipulator.ScenarioConfig)result.get(scenName);
			String longtext = sc.getLongtext();
			String shorttext = sc.getShorttext();
			if (longtext == null || shorttext.length() == 0)
				longtext = MISSING;
			if (shorttext == null || shorttext.length() == 0)
				shorttext = MISSING;

%>
	<tr>
		<td>
			<a href="#<%=scenName%>"><%=scenName%></a>
		</td>
		<td>
			<font size=2>
			<%=shorttext%>
			</font>
		</td>
		<td>
			<font size=2>
			<%=longtext%>
			</font>
		</td>
	</tr>
<%
	}

%>
</table>

<!-- CHECK IF SCENARIO METADATA THERE -->
<%

		for (Iterator iter = result.keySet().iterator(); iter.hasNext();) {
			String isShorttext = OK;
			String isLongtext = OK;
			String scenName = (String)iter.next();	
			ScenarioDataManipulator.ScenarioConfig sc = (ScenarioDataManipulator.ScenarioConfig)result.get(scenName);
			if (sc.isScenarioDerived())
				continue;
			Set paramNames 	= sc.getParmNames();
%>



<hr/>
<h2>Scenario:  <a name="<%=scenName%>"><%=scenName%></a></h2>
<h3>Parameter</h3>
	<table border=1>
		<tr>
			<th>
				name
			</th>
			<th>
				type
			</th>
			<th>
				Metadata (man)
			</th>
			<th>
				<font size=2>
				Shorttext (man)
				</font>
			</th>
			<th>
				<font size=2>
				Longtext (rec)
				</font>
			</th>
			<th>
				Allowed Values</br>
			</th>
			
		</tr>


<%
			for (Iterator iterParamNames = paramNames.iterator(); iterParamNames.hasNext();) {
				String paramName = (String)iterParamNames.next();
				String isParamMetaData = OK;
				String shorttext = OK;
				String longtext = OK;
				String type = "";
				String component = "fixed values";
				XCMAdminParamConstrainMetadata paramMetaData = 	XCMUtils.getScenarioConfigParamMetadata(paramName);
				if (paramMetaData == null) {
					isParamMetaData = MISSING;
					shorttext = MISSING;
					longtext = MISSING_ORANGE;
				} else {
					type = paramMetaData.getType();
					shorttext = XCMUtils.getScenarioConfigParamShorttext(paramName);
					longtext = XCMUtils.getScenarioConfigParamLongtext(paramName);
					if (shorttext.length() == 0)
						shorttext = MISSING;
					if (longtext.length() == 0) 
						longtext = MISSING_ORANGE;
				}
%>

		<tr>
			<td>
				<%=paramName%>
			</td>
			<td>
				<%=type%>
			</td>

			<td>
				<%=isParamMetaData%>
			</td>
			<td>
				<font size=2>
				<%=shorttext%>
				</font>
			</td>
			<td>
				<font size=2>			
				<%=longtext%>
				</font>
			</td>
			<td>



<!-- inline table for allowed values begin -->


		<%
			if (CheckXCMConsitency.isScenarioParamComponentDependent(paramName)) {
		%>

			All <b>available</b> values
			<table border=1 width=100%>
				<tr>
					<th>
						Name
					</th>
					<th>
						type
					</th>
				</tr>

		<%
				Map allowedValues  = CheckXCMConsitency.getScenarioComponentParamAllowedValues(scenName, paramName, scenDataMan, compDataMan, true);

				if (allowedValues.size() == 0) {
					allowedValues = new HashMap();
					allowedValues.put(MISSING, MISSING);
				}


				for (Iterator allowedValuesIter = allowedValues.keySet().iterator(); allowedValuesIter.hasNext();) {
					String allowedValue = (String)allowedValuesIter.next();
					String valueType = (String)allowedValues.get(allowedValue);

					if (valueType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_BASE)) {
						valueType = "This config AND customer extensions";
					} else if (valueType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_DERIVED)) {
						valueType = "Only Customer extensions of this config";
					} else {
						valueType = MISSING;
					}
						
					if (allowedValue.equals(Constants.CREATE_NEW_CONFIG_ID))
						continue;
		%>
				<tr>
					<td>
						<font size=2>
						<%=allowedValue%>
						</font>
					</td>
					<td>
						<font size=2>
						<%=valueType%>
						</font>
					</td>

				</tr>
				
				
					<%
						} // component allowed values
					%>
				</table>



			All <b>PROPOSED</b> values
			<table border=1 width=100%>
				<tr>
					<th>
						Name
					</th>
					<th>
						type
					</th>
					<th>
						shorttext
					</th>
				</tr>

		<%
				allowedValues  = CheckXCMConsitency.getScenarioComponentParamAllowedValues(scenName, paramName, scenDataMan, compDataMan, false);

				if (allowedValues.size() == 0) {
					allowedValues = new HashMap();
					allowedValues.put(MISSING, MISSING);
				}


				for (Iterator allowedValuesIter = allowedValues.keySet().iterator(); allowedValuesIter.hasNext();) {
					String allowedValue = (String)allowedValuesIter.next();
					String ValueType = (String)allowedValues.get(allowedValue);

					if (allowedValue.equals(Constants.CREATE_NEW_CONFIG_ID))
						continue;
					String isAWShorttext = OK;
					String awShorttext = ControllerUtils.getScenarioConfigParamValueShorttext(paramName, allowedValue, compDataMan);
					if (awShorttext== null || awShorttext.length() == 0)
						awShorttext = MISSING;


					String valueType = (String)allowedValues.get(allowedValue);
					if (valueType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_BASE)) {
						valueType = "This config AND customer extensions";
					} else if (valueType.equals(XCMAdminAllowedValueMetadata.VALUE_TYPE_DERIVED)) {
						valueType = "Only Customer extensions of this config";
					} else {
						valueType = MISSING;
					}


		%>
				<tr>
					<td>
						<font size=2>
						<%=allowedValue%>
						</font>
					</td>
					<td>
						<font size=2>
						<%=valueType%>
						</font>
					</td>
					<td>
						<font size=2>
						<%=awShorttext%>
						</font>
					</td>
				</tr>
					<%
						} // component allowed values
					%>
				</table>
				
		<%
			} else {
			
			if (true) {
		%>
				All <b>AVAILABLE</b> values		
				<table border=1 width=100%>
					<tr>
						<th>
							Name
						</th>
						<th>
							Metadata (shorttext or missing)
						</th>
					</tr>

					<%		

						Set allowedValues  = CheckXCMConsitency.getScenarioFixedParamAllowedValues(scenName, paramName, scenDataMan, compDataMan, true);

						if (allowedValues.size() == 0) {
							allowedValues = new HashSet();
							allowedValues.add(MISSING);
						}


						for (Iterator allowedValuesIter = allowedValues.iterator(); allowedValuesIter.hasNext();) {
							String allowedValue = (String)allowedValuesIter.next();

							if (allowedValue.equals(Constants.CREATE_NEW_CONFIG_ID))
								continue;
							String isAWShorttext = OK;
							String awShorttext = ControllerUtils.getScenarioConfigParamValueShorttext(paramName, allowedValue, compDataMan);
							if (awShorttext== null || awShorttext.length() == 0)
								awShorttext = MISSING;
					%>
						<tr>
							<td>
								<font size=2>
								<%=allowedValue%>
								</font>
							</td>
							<td>
								<font size=2>
								<%=awShorttext%>
								</font>
							</td>

						</tr>

							<%
								} // fixed allowed values
							%>

					</table>
			<% } 
			%>

			All <b>USED</b> values		
			<table border=1 width=100%>
				<tr>
					<th>
						Name
					</th>
					<th>
						Metadata (shorttext or missing)
					</th>
				</tr>

				<%		
		
					Set allowedValues  = CheckXCMConsitency.getScenarioFixedParamAllowedValues(scenName, paramName, scenDataMan, compDataMan, false);

					if (allowedValues.size() == 0) {
						allowedValues = new HashSet();
						allowedValues.add(MISSING);
					}


					for (Iterator allowedValuesIter = allowedValues.iterator(); allowedValuesIter.hasNext();) {
						String allowedValue = (String)allowedValuesIter.next();

						if (allowedValue.equals(Constants.CREATE_NEW_CONFIG_ID))
							continue;
						String isAWShorttext = OK;
						String awShorttext = ControllerUtils.getScenarioConfigParamValueShorttext(paramName, allowedValue, compDataMan);
						if (awShorttext== null || awShorttext.length() == 0)
							awShorttext = MISSING;
				%>
					<tr>
						<td>
							<font size=2>
							<%=allowedValue%>
							</font>
						</td>
						<td>
							<font size=2>
							<%=awShorttext%>
							</font>
						</td>

					</tr>

						<%
							} // fixed allowed values
						%>
			
				</table>
				<%
					} // else scenario params
				%>

			
<!-- allewed values end -->			
			<td>
		</tr>

<%
		} // scenario param names
%>


	</table>




<hr/>


<%	
		} // scenario for
%>

  </body>
</html>

