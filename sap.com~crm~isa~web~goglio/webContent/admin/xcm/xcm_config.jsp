<%@ page import="com.sap.isa.core.cache.Cache" %>
<%@ page import="java.util.Iterator, com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory" %>
<%@ page import="com.sap.isa.core.xcm.ConfigContainer" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.sap.isa.core.xcm.ExtendedConfigManager" %>
<%@page import="java.net.URLEncoder, com.sap.isa.core.util.XMLHelper" %>
<%@page import="com.sap.isa.core.xcm.XCMConstants" %>
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<%
    
    boolean showJCoCache = true;
    String href = request.getParameter("href");
    String configKey = request.getParameter("key");
    Cache.Access cacheAccess = null;
    String configFile = "";
    String encodedConfigFile = "";
    try {
      cacheAccess = Cache.getAccess(XCMConstants.CACHE_REGION_NAME);
      ConfigContainer cc = (ConfigContainer)cacheAccess.get(configKey);
    //  out.println(cc.toString());
    	configFile = cc.getConfigAsString(href);
    	encodedConfigFile = XMLHelper.stringNormalizer(configFile);
    	  
    } catch (Cache.Exception ex) {
    }
    
    

%>

<hbj:content id="xcmConfig" > 
<% 
	BaseUI xcmConfigUi = new BaseUI(pageContext);
	if(xcmConfigUi.isAccessible()) { 
		xcmConfig.setSection508Rendering(true);
	}   
%>
 
  <hbj:page title = "Displaying file">


  <hbj:group id="xmlFile" design="sapcolor" title="<%= href%>">
		<pre>
			<% out.println(encodedConfigFile); %>
		
		
		</pre>
</hbj:group>
</hbj:page>
</hbj:content>
</html>