 
<%@ page import="com.sap.isa.core.xcm.admin.controller.ControllerUtils"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpItemDetailForm"%>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<% { %>

             <%
               
                String paramName = mHelpController.getCurrentDetailItem().getName();
                String scenName1 = mHelpController.getCurrentScenarioName();
                // check if parameter depends on component configuration
                String compName = ControllerUtils.getScenarioParamDependentComponentName(paramName);
          		mHelpController.setCurrentDetailMode(
          				HelpController.MODE_DETAIL_SHOW_VALUE + 
          				HelpController.MODE_DETAIL_NAME_AS_LINK_DEST +
          				HelpController.MODE_DETAIL_VALUE_AS_LINK, "scenParamValue", "scenParam");
             %>

            <!-- parameter details -->
			<br>
			<% if(mHelpController.checkCurrentDetailMode(HelpController.MODE_DETAIL_NAME_AS_LINK_DEST)) { %>
             	<% 	String name1 = mHelpController.getCurrentLinkDestPrefix() + paramName; %>
    				<a name="<%=name1%>" > </a>
    		<% } %>
			<hbj:group id="name<%=paramName%>" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpItemDetailForm.NAME, null) +  paramName%>" tooltip="<%=WebUtil.translate(pageContext, HelpItemDetailForm.NAME_TOOLTIP, null) + paramName%>">
             <hbj:groupBody> 
            <%@ include file="HelpItemDetail.inc" %>  
             
		 				<%
		        		HelpItemContainer allowedValuesContainer = mHelpController.getScenParamAllowedValuesContainer(scenName1, paramName);
                mHelpController.setCurrentSummaryContainer(allowedValuesContainer);
                mHelpController.setCurrentSummaryMode(
                			HelpController.MODE_SUMMARY_TABLE_SHORTTEXT +
                			HelpController.MODE_SUMMARY_TABLE_LONGTEXT +
                			HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK_DEST, "", "scenParamValue");
             %> 
             <!-- allowed value summary -->  
             <% if (compName != null) { 
                mHelpController.setCurrentSummaryMode(
                		HelpController.MODE_SUMMARY_TABLE_SHORTTEXT + 
               			HelpController.MODE_SUMMARY_TABLE_NAME_AS_LINK_DEST, "", "scenParamValue"
               	);             
             %>
								<h3>
								<% String linkref="HelpCompConfig.jsp?compName=" + compName; %>
							<hbj:textView text="Depends on Component Configuration:" design="header3"/>
							<hbj:link id="link1" text="<%=compName%>"
										reference="<%=linkref%>"
							/>
 
								</h3>	
             <% } %>
             <p/>
             <hbj:textView
                     id="ItemDetailCaptionTextView"
                     text="Allowed Values Summary"
                     design="HEADER2"
            />                                
            <%@ include file="HelpItemSummary.inc" %>  
</hbj:groupBody>
</hbj:group>             
<% } %>    
        
