
 
<head><script language="JavaScript" > 
</script>
</head>

<%@ page import="java.util.*" %>
 
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.HelpController"%>
<%@ page import="com.sapportals.htmlb.table.TableView" %>
<%@ page import="com.sap.isa.core.xcm.admin.model.help.HelpItem" %>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.ComponentConfigController"%>
<%@ page  import="com.sapportals.htmlb.table.DefaultTableViewModel"%>
<%@ page  import="com.sap.isa.core.xcm.admin.model.help.HelpItemContainer"%>
<%@ page  import="com.sap.isa.core.xcm.admin.model.table.HelpItemSummaryTableModel"%>
<%@ page import="com.sap.isa.core.xcm.admin.model.HelpConstants" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HelpSummaryForm"%>

<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %> 

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin_RO(request) || !com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<%    
	UserSessionData userData = UserSessionData.getUserSessionData(session);
	AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
	HelpController mHelpController = appConfigController.getHelpController();

%> 
<hbj:content id="xcmAdminContext" >
<%
	BaseUI helpSumUi = new BaseUI(pageContext);
	if(helpSumUi.isAccessible()) {
		 xcmAdminContext.setSection508Rendering(true);
	}
%>
<hbj:page title = "<%=WebUtil.translate(pageContext, HelpSummaryForm.PAGE_TITLE, null)%>">
<hbj:form id="HomeForm">
	<hbj:group id="applicationConfigurationGroup" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpSummaryForm.GROUP_TITLE, null)%>" tooltip="">
	  	<hbj:groupBody>
			<hbj:group id="helpConfigs" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpSummaryForm.CONFIG_TEXT, null)%>">
				<hbj:groupBody>
             
					<p/> 
					<hbj:textView text="<%=WebUtil.translate(pageContext, HelpSummaryForm.INTRO_TEXT, null)%>" encode="false" wrapping = "true"/>
					<p/>  
                <%-- include SAP scenario summary --%>
                	<% 
	                	mHelpController.setCurrentSummaryMode(
	                	HelpController.MODE_SUMMARY_TABLE_SHORTTEXT + 
	                	HelpController.MODE_SUMMARY_TABLE_LONGTEXT +
	                	HelpController.MODE_SUMMARY_TABLE_NAME_AS_SCEN_LINK, "", "");
	                	HelpItemContainer sapScenarioSummary = mHelpController.getSAPScenSummaryContainer();
	                	mHelpController.setCurrentSummaryContainer(sapScenarioSummary);
                	%>
                

					<hbj:tray id="comfOverv2"
						title="<%=WebUtil.translate(pageContext, HelpSummaryForm.SAP_CONF_OVERV_TEXT, null)%>"
						design="border"
						width="95%"
						isCollapsed="false"
						tooltip="<%=WebUtil.translate(pageContext, HelpSummaryForm.SAP_CONF_OVERV_TEXT, null)%>">
						<hbj:trayBody> 
           
               		 		<%@ include file="HelpItemSummary.inc" %>                             
               		 	
						</hbj:trayBody>                 
					</hbj:tray>
				</hbj:groupBody>
               </hbj:group>
               <p></p>
           	   <hbj:group id="helpComps" design="SAPCOLOR" title="<%=WebUtil.translate(pageContext, HelpSummaryForm.COMP_TEXT, null)%>" tooltip="">
               	<hbj:gropBody>
                                 
					</p>  
					<hbj:textView id="list" text="<%=WebUtil.translate(pageContext, HelpSummaryForm.LIST_INTRO_TEXT, null)%>" wrapping = "true" encode="false"/>

					<hbj:itemList id="twotypes" ordered="false">
						<hbj:listItem>
							<hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, HelpSummaryForm.LIST_ITEM1_TEXT, null)%>" encode="false" wrapping = "true"/>
						</hbj:listItem>

						<hbj:listItem>
							<hbj:textView wrapping="TRUE" text="<%=WebUtil.translate(pageContext, HelpSummaryForm.LIST_ITEM2_TEXT, null)%>" encode="false" wrapping = "true"/>
						</hbj:listItem>
		
					</hbj:itemList>

					<hbj:textView id="conf-test" text="<%=WebUtil.translate(pageContext, HelpSummaryForm.CONF_TEST_TEXT, null)%>" encode="false" wrapping="true" />
					<p/> 
                	<%-- include SAP general scope component summary --%>
                	<% 
	                	mHelpController.setCurrentSummaryMode(
	                	HelpController.MODE_SUMMARY_TABLE_SHORTTEXT + 
	                	HelpController.MODE_SUMMARY_TABLE_LONGTEXT +
	                	HelpController.MODE_SUMMARY_TABLE_NAME_AS_COMP_LINK, "", "");                
	                	HelpItemContainer sapGeneralScopeCompSummary = mHelpController.getGeneralScopeComponentSummaryContainer();
	                	mHelpController.setCurrentSummaryContainer(sapGeneralScopeCompSummary);
                	%>
                	<p></p>
					<hbj:tray id="compOverv"
						title="<%=WebUtil.translate(pageContext, HelpSummaryForm.COMP_OVERV_TEXT, null)%>"
						design="border"
						width="95%"
						isCollapsed="false"
						tooltip="<%=WebUtil.translate(pageContext, HelpSummaryForm.COMP_OVERV_TEXT, null)%>">
     
						<hbj:trayBody>                
                
			                <%@ include file="HelpItemSummary.inc" %>
            			 

							<p/>
			                <%-- include SAP XCM configuration scope component summary --%>
            			    <% 
	                			mHelpController.setCurrentSummaryMode(
	                			HelpController.MODE_SUMMARY_TABLE_SHORTTEXT + 
	                			HelpController.MODE_SUMMARY_TABLE_LONGTEXT +
	                			HelpController.MODE_SUMMARY_TABLE_NAME_AS_COMP_LINK, "", "");                
			                	HelpItemContainer sapConfigScopeCompSummary = mHelpController.getConfigScopeComponentSummaryContainer();
	    		            	mHelpController.setCurrentSummaryContainer(sapConfigScopeCompSummary);
                			%> 
						</hbj:trayBody> 
					</hbj:tray>
				<p></p>
					<hbj:tray id="compOverv2"
						title="<%=WebUtil.translate(pageContext, HelpSummaryForm.COMP_OVERV2_TEXT, null)%>"
						design="border"
						width="95%"
						isCollapsed="false"
						tooltip="<%=WebUtil.translate(pageContext, HelpSummaryForm.COMP_OVERV2_TEXT, null)%>">
						<hbj:trayBody>                                
			                <%@ include file="HelpItemSummary.inc" %>
            			    
						</hbj:trayBody> 
					</hbj:tray>

	 			</hbj:groupBody>
               </hbj:group>

			</hbj:groupBody>
    	</hbj:group>
      </hbj:form>
  </hbj:page>
</hbj:content>
