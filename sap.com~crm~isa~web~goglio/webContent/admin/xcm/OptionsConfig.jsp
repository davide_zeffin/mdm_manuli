<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>


<%@ page import="com.sap.isa.core.xcm.init.ExtendedConfigInitHandler"%>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sapportals.htmlb.IListModel" %>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import="com.sap.isa.core.xcm.admin.controller.AppConfigController"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.HomeForm"%>
<%@ page import="com.sap.isa.core.xcm.admin.actionform.OptionsConfigForm"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<%
  
 UserSessionData userData = UserSessionData.getUserSessionData(session);
 AppConfigController appConfigController = (AppConfigController)userData.getAttribute(SessionConstants.APP_CONFIG_CONTROLLER);
 IListModel configsListModel = appConfigController.getConfigsListModel();
 pageContext.setAttribute("configsListModel", configsListModel);
 final String DB = "Database"; 
 final String FS = "File System";
 final String TRUE = "true";
 final String FALSE = "false";
%>
  
<hbj:content id="xcmAdminOptions" >
	<%
		BaseUI OptionsUi = new BaseUI(pageContext);
		if(OptionsUi.isAccessible()) {
			 xcmAdminOptions.setSection508Rendering(true);
		}
	%>
  <hbj:page title = "<%=WebUtil.translate(pageContext, OptionsConfigForm.PAGE_TITLE, null)%>">

<%
String helpIconHref = "mimes/sap_hint.gif";	
String contextParamHelpText = WebUtil.translate(pageContext, OptionsConfigForm.HELP_CURRENT_CONFIG, null);
%>

	<hbj:group id="ComponentNameGroup"
			   design="sapcolor"
			   title="<%=WebUtil.translate(pageContext, OptionsConfigForm.GROUP_TITLE, null)%>"
			   tooltip="<%=WebUtil.translate(pageContext, OptionsConfigForm.GROUP_TOOLTIP, null)%>">
		<hbj:groupBody>
		<hbj:form id="HomeForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/homeInfo.do\" , null , null , false)%>">		 
		<table>
			<tr>
				<td>     
					<hbj:label id="StoreTypeLabel" labelFor="TargetDataStoreInputField" text="<%=WebUtil.translate(pageContext, OptionsConfigForm.STORETYPE_LABEL_TEXT, null)%>" labeledComponentClass="DropdownListBox"/>
				</td>
			  <td>
				<hbj:inputField id="TargetDataStoreInputField"
								type="String"
								value="<%=(ExtendedConfigInitHandler.isTargetDatastoreDB() ? DB : FS)%>"
								visible="true"
								disabled="true"
								required="false"
								maxlength="12"
								size="12"
								design="standard"/>
			   </td>
			   <td>
				</td>
			</tr>		
		</table>
		</hbj:form>	
			<% if(!ExtendedConfigInitHandler.isTargetDatastoreDB()) { 
				 %>
			<hbj:form id="dummyForm" method="post" action="">
				<table>
					<tr>
						<td>     
						<hbj:label id ="ConfigDataPath" labelFor="ConfigDataPathInputField" text="<%=WebUtil.translate(pageContext, OptionsConfigForm.COMP_CONF_DATA_LABEL_TEXT, null)%>" tooltip = "<%=WebUtil.translate(pageContext, OptionsConfigForm.COMP_CONF_DATA_LABEL_TOOLTIP, null)%>"/>
					  </td>
					  <td>
						<hbj:inputField id="ConfigDataPathInputField"
										type="String"
										value="<%=appConfigController.getConfigDataPath()%>"
										visible="true"
										disabled="true"
										required="false"
										maxlength="40"
										size="50"
										design="standard"/>
					   </td>
					</tr>
					<tr>
						<td>     
						<hbj:label id="ScenarioConfigPath" labelFor="ScenarioConfigPathInputField" text="<%=WebUtil.translate(pageContext, OptionsConfigForm.CONF_DATA_LABEL_TEXT, null)%>" tooltip = "<%=WebUtil.translate(pageContext, OptionsConfigForm.CONF_DATA_LABEL_TEXT, null)%>"/>
					  </td>
					  <td>
						<hbj:inputField id="ScenarioConfigPathInputField"
										type="String"
										value="<%=appConfigController.getScenarioConfigPath()%>"
										visible="true"
										disabled="true"
										required="false"
										maxlength="40"
										size="50"
										design="standard"/>
					   </td>
					   <td>
						</td>
					</tr>
				</table>
			</hbj:form>
			<% } else { %>
			
				<hbj:gridLayout cellPadding="4" cellSpacing = "2">
				<hbj:gridLayoutCell rowIndex="1" columnIndex="1" >
				<hbj:form id = "uploadForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/configDataUpload.do\" , null , null , false)%>" encodingType="multipart/form-data">
					<hbj:tray id="uploadTray" title="Upload configuration data from file-system" isCollapsed="false">
					<hbj:trayBody>
					<%if(appConfigController.isEditMode()) {%>
					<hbj:gridLayout id="headerGridLayout" cellPadding="4">
					<hbj:gridLayoutCell rowIndex="1" columnIndex="1" horizontalAlignment="LEFT">
						<hbj:label id="compConfigPath" labelFor="configDataFile" text="<%=WebUtil.translate(pageContext, OptionsConfigForm.COMP_CONF_DATA_LABEL_TEXT, null) %>" />
					</hbj:gridLayoutCell>
					<hbj:gridLayoutCell rowIndex="1" columnIndex="2" horizontalAlignment="LEFT">
						<INPUT type="FILE" id="configDataFile" name="configDataFile"/>
					</hbj:gridLayoutCell>
					<hbj:gridLayoutCell rowIndex="2" columnIndex="1" horizontalAlignment="LEFT">
						<hbj:label id="scenarioConfigPath" labelFor= "scenarioConfigFile" text="<%=WebUtil.translate(pageContext, OptionsConfigForm.CONF_DATA_LABEL_TEXT, null)%>"/>
					</hbj:gridLayoutCell>
					<hbj:gridLayoutCell rowIndex="2" columnIndex="2" horizontalAlignment="LEFT">
						<INPUT type="FILE" id="scenarioConfigFile" name="scenarioConfigFile"/>
					</hbj:gridLayoutCell>
					<hbj:gridLayoutCell rowIndex="3" columnIndex="1" horizontalAlignment="LEFT">
						<hbj:button id="upload" text="<%=WebUtil.translate(pageContext, OptionsConfigForm.BUTTON_UPLOAD_TEXT, null) %>" onClick="configDataUpload.do"/>
					</hbj:gridLayoutCell>				
					</hbj:gridLayout>
					<% } else { %>
					<hbj:textView text ="<%=WebUtil.translate(pageContext, OptionsConfigForm.NOT_IN_EDIT_MODE_TEXT, null) %>"/>
					<% } %>
					</hbj:trayBody>
					</hbj:tray>
				</hbj:form>
				</hbj:gridLayoutCell>
				<hbj:gridLayoutCell rowIndex = "2" columnIndex = "1">	
				<hbj:form id="downloadForm" method="post" action="<%=WebUtil.getAppsURL(pageContext  , Boolean.valueOf(request.isSecure()), \"admin/xcm/configData.do\" , null , null , false)%>" target="downloadFrame">
					<script type="text/javascript">
						function update_event_type(eventId) {
							document.getElementById('downloadFormEventType').value = eventId;
						}
					</script>
					<% 
					String onConfigDataDownloadClick = "update_event_type('"  + com.sap.isa.core.xcm.admin.actionform.HomeForm.ON_DOWNLOAD_COMP_CONFIG_DATA_EVENT + "')";
					String onScenarioDataDownloadClick = "update_event_type('"  + com.sap.isa.core.xcm.admin.actionform.HomeForm.ON_DOWNLOAD_SCENARIO_CONFIG_DATA_EVENT + "')";
					%>
					<input type="hidden" id="downloadFormEventType" name="downloadFormEventType" value=""/>
					<hbj:tray id = "downloadTray" title="<%=WebUtil.translate(pageContext, OptionsConfigForm.TRAY_DOWN_TO_FILE_TEXT, null) %>" isCollapsed="false">
					<hbj:trayBody>
					<hbj:gridLayout id="headerGridLayout2" cellPadding="4">
					<hbj:gridLayoutCell rowIndex="1" columnIndex="1" horizontalAlignment="LEFT">
						<hbj:textView text="<%=WebUtil.translate(pageContext, OptionsConfigForm.COMP_CONF_DATA_TEXT, null) %>"/>
					</hbj:gridLayoutCell>
					<hbj:gridLayoutCell rowIndex="1" columnIndex="2" horizontalAlignment="LEFT">
						<hbj:button id="ConfigDataDownloadLink"
									text="<%=WebUtil.translate(pageContext, OptionsConfigForm.BUTTON_DOWNL_TEXT, null) %>"
									tooltip="<%=WebUtil.translate(pageContext, OptionsConfigForm.BUTTON_DOWNL_TOOLTIP, null) %>"
									onClientClick="<%=onConfigDataDownloadClick%>"
									onClick="<%=com.sap.isa.core.xcm.admin.actionform.HomeForm.ON_DOWNLOAD_COMP_CONFIG_DATA_EVENT%>"/>
					</hbj:gridLayoutCell>
					<hbj:gridLayoutCell rowIndex="2" columnIndex="1" horizontalAlignment="LEFT">
						<hbj:textView text="<%=WebUtil.translate(pageContext, OptionsConfigForm.APPL_CONF_DATA_TEXT, null) %>"/>
					</hbj:gridLayoutCell>
					<hbj:gridLayoutCell rowIndex="2" columnIndex="2" horizontalAlignment="LEFT">
						<hbj:button id="ScenarioDataDownloadLink"
									text="<%=WebUtil.translate(pageContext, OptionsConfigForm.BUTTON_DOWNL_TEXT, null) %>"
									tooltip="<%=WebUtil.translate(pageContext, OptionsConfigForm.BUTTON_DOWNL_TOOLTIP, null) %>"
									onClientClick="<%=onScenarioDataDownloadClick%>"
									onClick="<%=com.sap.isa.core.xcm.admin.actionform.HomeForm.ON_DOWNLOAD_SCENARIO_CONFIG_DATA_EVENT%>"/>
					</hbj:gridLayoutCell>
					</hbj:gridLayout>
					</hbj:trayBody>
					</hbj:tray>
				</hbj:form>
				</hbj:gridLayoutCell>
				</hbj:gridLayout>
			
			<% } %>
		</hbj:groupBody>
	</hbj:group>
	<div style="display:none">
		<iframe id="downloadFrame" name="downloadFrame"/>
	</div>
</hbj:page>
</hbj:content>
