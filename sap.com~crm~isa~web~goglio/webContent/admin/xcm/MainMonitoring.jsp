<head><script language="JavaScript" > 
<!-- 
function openDocumentation()
    {   
    	var win = window;
    	win.open('HelpSummary.jsp','Documentation','resizable=yes,width=600,height=500,screenX=0,screenY=0,scrollbars, menubar,toolbar');
    }
-->    
</script>
</head>

<%@ page import="java.util.*" %>
  
<%@ page import="com.sap.isa.core.xcm.admin.Constants" %>
<%@ page import="com.sap.isa.core.util.WebUtil"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page  import="com.sap.isa.core.xcm.admin.controller.MonitoringController"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.core.xcm.admin.SessionConstants"%>
<%@ page import = "com.sap.isa.core.xcm.admin.actionform.MonitoringForm" %>
     
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% if (!com.sap.isa.core.config.AdminConfig.isXCMAdmin())
        response.sendRedirect("notsupported.jsp");
%>


<hbj:content id="xcmAdminContext" >  
	<%    
	 
	//accessibility enabling
	BaseUI mainConfigUi = new BaseUI(pageContext);
	if(mainConfigUi.isAccessible()) { 
		 xcmAdminContext.setSection508Rendering(true);
	} 
	%> 
  <hbj:page title = "<%=WebUtil.translate(pageContext, MonitoringForm.GROUP_TITLE, null)%>">
		 <table border="0" height="100%" width="100%">
		 	<tr>
		 		<td >
		 			<%@ include file="Toolbar.inc"%>
		 		</td>   
		 	</tr> 
			<tr>    
		 		<td height="100%">
					<hbj:group id="xcmMonitoringGroup" design="sapcolor" title="<%=WebUtil.translate(pageContext, MonitoringForm.GROUP_TITLE, null)%>" tooltip="<%=WebUtil.translate(pageContext, MonitoringForm.GROUP_TOOLTIP, null)%>">
					<%
						xcmMonitoringGroup.setHeight("100%");
					%> 
						<hbj:groupBody>
						  <hbj:gridLayout id="contentGridLayout" debugMode="false" width="100%">
								<%
							  	contentGridLayout.setCellSpacing(10);
							  	contentGridLayout.setHeightPercentage(100);	 
							  %> 
							
								<hbj:gridLayoutCell id="contenCell" rowIndex="1" columnIndex="1" colSpan="100" verticalAlignment="TOP" horizontalAlignment="CENTER">
									<%@ include file="xcm_stats.jsp" %>
							  </hbj:gridLayoutCell>
							</hbj:gridLayout>
						</hbj:groupBody> 
					</hbj:group>
				</td>
			</tr>
		</table> 
  </hbj:page>       
</hbj:content>
