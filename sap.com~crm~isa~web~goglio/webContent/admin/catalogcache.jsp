<%--
********************************************************************************
    File:         catalogcache.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Roland Huecking
    Created:      22.6.2001
    Version:      1.0

    $Revision: #49 $
    $Date: 2001/06/19 $
********************************************************************************
--%>

<%@ page import="com.sap.isa.catalog.cache.*" %>
<%@ page import="com.sap.isa.catalog.impl.Catalog" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.sql.Timestamp" %>
<%@ page import="java.net.URLEncoder" %>

<%@ page import="com.sap.isa.core.config.AdminConfig" %>

<% if (!AdminConfig.isCatalogCache())
        response.sendRedirect("notsupported.jsp");
%>


<%@ taglib uri="/isa" prefix="isa" %>


<%
    CatalogCache cache = CatalogCache.getInstance();
    String keyToRemove = request.getParameter("remove");

    if (keyToRemove != null && AdminConfig.isXCMAdmin(request))
        CatalogCache.getInstance().removeInstance(keyToRemove);
%>

<html>
  <head>
    <title>Catalog Cache Statistics</title>
  <script type="text/javascript" src="admin.js">
  </script>

  </head>
  <body>
  
	<div class="filter">
  	  <p>
		Configured expiration time: <%=String.valueOf((cache.getExpirationTime()/60000))%> min
	  </p>

	  <p>
	    Configured removal time:
	    <% if(cache.getRemovalTime() != 0) { %>
	        <%=String.valueOf((cache.getRemovalTime()/60000))%> min
	        (next invalidation in <%=String.valueOf(((((cache.getRemCleanerStartTime()-System.currentTimeMillis())+cache.getRemovalTime()))/60000)+1)%> min)
	    <% } else { %>
	        <%="not set in init file"%>
	    <% } %>
	  </p>
	  
	  <p>
	    Next check of remaining life time in <%=String.valueOf(((((cache.getExpCleanerStartTime()-System.currentTimeMillis())+3600000))/60000)+1)%> min
	    (removal of catalogs with life time <= 0).
	  </p>
    </div>
  
    <div class="filter-result">
	  <table>
	    <tr>
	      <th>Key</th>
	      <th>Catalog Guid</th>
	      <th>Time Stamp (last access)</th>
	      <th>Remaining life time (min)</th>
	      <% if (AdminConfig.isXCMAdmin(request)) { %>
		      <th>Remove</th>
	      <% } %>
	    </tr>
<%
	String[] oddEvenClasses = new String[] {"odd", "even"};
	int oddEvenClassesIndex = 0;
	
    Iterator iter = cache.keys();
    while(iter.hasNext()) {
		String aKey = (String) iter.next();	
        	CachableItem aCacheableItem  = cache.getCachedInstance(aKey);
		
		if(! (aCacheableItem instanceof Catalog))
           	continue;
    	
    	Catalog aCatalog = (Catalog) aCacheableItem;
    	out.println("<tr class=" + oddEvenClasses[oddEvenClassesIndex++ % 2] + ">");
    	out.println("<td>");
    	out.println(aKey);
    	out.println("</td><td align=\"left\">");
    	out.println(aCatalog.getGuid());
    	out.println("</td><td align=\"left\">");
    	out.println(aCatalog.getTimeStamp());
    	out.println("</td><td align=\"left\">");
    	out.println(((cache.getExpirationTime()-(System.currentTimeMillis()-aCatalog.getTimeStamp().getTime()))+1)/60000);
    	out.println("</td>");
    	if (AdminConfig.isXCMAdmin(request)) {
    		out.println("<td>");
	   		out.println("<a href=\"catalogcache.jsp?remove=" + URLEncoder.encode(aKey) + "\">Remove</a>");
	    	out.println("</td>");
    	}
    	out.println("</tr>");
    }

%>
  		</table>
    </div>
  	
  	<br/>
  	<br/>
	<div id="buttons">
		<ul class="buttons-1">
			<li><a href="catalogcache.jsp">Refresh</a></li>
		</ul>
  	</div>
	<%@ include file="read_only_error.inc" %>
  </body>
</html>