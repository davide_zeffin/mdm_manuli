<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />
<html>
  <head>
    <title><isa:translate key="system.admintool.title"/></title>
  <script type="text/javascript" src="admin.js">
  </script>

    <script type="text/javascript">
    	function toggle(open, close) {
			openIcon = document.getElementById(open).style.display = "inline";
			closeIcon = document.getElementById(close).style.display = "none";
		}


		// highlights arrow-image
		function overArrow(dir) {
		  imgDir = '<isa:webappsURL name="/mimes/images/" />';
		  if (dir == 'left')
		    document.images["arrow_left"].src = imgDir + "/left_arrow_h.gif";
		  else
		    document.images["arrow_right"].src = imgDir + "/right_arrow_h.gif";
		}


		// resets arrow-image
		function outArrow(dir) {
		  imgDir = '<isa:webappsURL name="/mimes/images/" />';
		  if (dir == 'left')
		    document.images["arrow_left"].src = imgDir + "/left_arrow.gif";
		  else
		    document.images["arrow_right"].src = imgDir + "/right_arrow.gif";
		}
    </script>
  </head>
  
  <body class="closer" onload="toggle('arrow_left', 'arrow_right');">
	<div id="leftArrow">
        <a href="#" onmouseover="overArrow('left');" onmouseout="outArrow('left');" onclick="parent.resize('closeFrame', 'adminOrganizer'); toggle('arrow_right', 'arrow_left'); return false;"><img src="<isa:webappsURL name="/mimes/images/left_arrow.gif" />" width="7" height="13" border="0" alt="<isa:translate key="system.admintool.label.hideorgan" />" name="arrow_left" id="arrow_left" /></a>
    </div>
	<div id="rightArrow">
        <a href="#" onmouseover="overArrow('right');" onmouseout="outArrow('right');" onclick="parent.resize('openFrame', 'adminOrganizer'); toggle('arrow_left', 'arrow_right'); return false;"><img src="<isa:webappsURL name="/mimes/images/right_arrow.gif" />" width="7" height="13" border="0" alt="<isa:translate key="system.admintool.label.showorgan" />" name="arrow_right" id="arrow_right"/></a>
    </div>
  </body>
</html>