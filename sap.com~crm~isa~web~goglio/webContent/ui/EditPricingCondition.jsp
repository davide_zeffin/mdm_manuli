<%-- **********************************************************************

      Pricing condition detail screen

     ********************************************************************** --%>
<%-- **********************************************************************
      tag libs
     ********************************************************************** --%>
<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/sap-spe.tld"    prefix="spe" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/isa" prefix="isa"%>
<isa:contentType />
<html:html>
	<head>
		<meta 
			http-equiv="content-type" 
			content="text/html"; 
			charset=utf-8">
		<link 
			rel="stylesheet" 
			type="text/css" 
			href="styles/spe.css">
		<script 
			language="JavaScript">
			function cancelForm(){ document.forms[0].submit(); }
		</script>
	</head>
	<body 
		class="tabcontent" 
		leftmargin="0" 
		topmargin="0" 
		marginwidth="0" 
		marginheight="0" 
		text="#000000" 
		link="#424E91" 
		vlink="#7979BD" 
		alink="#336633">
		<html:form action="/showPricingConditionPanel.do">
			<div 
				class="content" 
				width=100%>
			<%@include file="MessagePanel.jsp" %>
			<div 
				class="content" 
				width=100%>
				<%-- **********************************************************************
                     start of content table
                     ********************************************************************** --%>
				<table 
					border="0" 
					cellpadding="5" 
					cellspacing="1" 
					width=100%>
					<%-- **********************************************************************
                         headings
                         ********************************************************************** --%>
					<tr>
						<td class="pageHeader" 
							valign='middle'>
							<logic:equal 
								name="pricingConditionForm" 
								property="action" 
								scope="request" 
								value="Display">
								<isa:translate 
									key="prc.prcCondForm.table.display"/>
							</logic:equal>
						</td>
					</tr>
				</table>
			</div>
			<div class="content" 
				width=100%>
				<%-- **********************************************************************
                     start of item table
                     ********************************************************************** --%>
				<table class="conditionDetailTable" cellpadding="2">
					<tr 
						class="topTable">
						<td class="detailGroup">
							<table class="detailGroupTable" 
								border="0" 
								cellspacing=2 
								cellpadding=2 
								width="100%">
								<tr>
									<td colspan="3" class="tableLabel">
										<isa:translate key="prc.prcCondForm.table.conditionV"/>
									</td>
								</tr>
								<%-- **********************************************************************
                                     condition type
                                     ********************************************************************** --%>
								<tr>
									<td class="detailGroupTableCol" >
										<isa:translate key="prc.prcCondForm.prompt.condType"/>
									</td>
									<td class="detailGroupTableCol" >
										<bean:write 
											name="pricingConditionForm" 
											property="conditionTypeName" 
											scope="request" 
											filter="true"/>
										&nbsp;
										<bean:write 
											name="pricingConditionForm" 
											property="description" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<%-- **********************************************************************
	                                  condition rate
                                     ********************************************************************** --%>
								<tr>
									<td class="detailGroupTableCol" 
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.condRate"/>
									</td>
									<td class="detailGroupTableCol"
										align="right">
										<bean:write 
											name="pricingConditionForm" 
											property="conditionRateValue" 
											scope="request" 
											filter="true"/>
										&nbsp;
										<bean:write 
											name="pricingConditionForm" 
											property="conditionCurrency" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<%-- **********************************************************************
	                                 pricing unit
                                     ********************************************************************** --%>
								<%-- **********************************************************************
	                                 no pricing unit for calculation type 'A' and 'B'
                                     ********************************************************************** --%>
								<spe:checkConditionPropertyValue 
									property="calculationType" 
									value="A" 
									not="true">
								<spe:checkConditionPropertyValue 
									property="calculationType" 
									value="B" 
									not="true">
									<tr>
										<td class="detailGroupTableCol"
											nowrap>
											<isa:translate 
												key="prc.prcCondForm.prompt.prcUnit"/>
										</td>
										<td class="detailGroupTableCol"
											align="right">
											<bean:write 
												name="pricingConditionForm" 
												property="pricingUnitValue" 
												scope="request" 
												filter="true"/>
											&nbsp;
											<bean:write 
												name="pricingConditionForm" 
												property="pricingUnitUnit" 
												scope="request" 
												filter="true"/>
										</td>
									</tr>
								</spe:checkConditionPropertyValue>
								</spe:checkConditionPropertyValue>
								<%-- **********************************************************************
	                                 condition value
                                     ********************************************************************** --%>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.condValue"/>
									</td>
									<td class="detailGroupTableCol"
										align="right">
										<bean:write 
											name="pricingConditionForm" 
											property="conditionValueValue" 
											scope="request" 
											filter="true"/>
										&nbsp;
										<bean:write 
											name="pricingConditionForm" 
											property="documentCurrency" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<%-- **********************************************************************
	                                 quantity conversion
                                     ********************************************************************** --%>
								<%-- **********************************************************************
	                                 check if item condition
                                     ********************************************************************** --%>
								<spe:checkConditionPropertyValue 
									property="headerCondition" 
									value="false">
									<tr>
										<td class="detailGroupTableCol"
											nowrap>
											<isa:translate 
												key="prc.prcCondForm.prompt.quantConv"/>
										</td>
										<td class="detailGroupTableCol"
											align="left">
											<bean:write 
												name="pricingConditionForm" 
												property="conversionNumerator" 
												scope="request" 
												filter="true"/>
											<bean:write 
												name="pricingConditionForm" 
												property="conversionUnit1" 
												scope="request" 
												filter="true"/>
											&nbsp
											<-->
											&nbsp
											<bean:write 
												name="pricingConditionForm" 
												property="conversionDenominator" 
												scope="request" 
												filter="true"/>
											<bean:write 
												name="pricingConditionForm" 
												property="conversionUnit2" 
												scope="request" 
												filter="true"/>
										</td>
									</tr>
								</spe:checkConditionPropertyValue>
								<%-- **********************************************************************
	                                 factor
                                     ********************************************************************** --%>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.factor"/>
									</td>
									<td 
										align="left">
										<bean:write 
											name="pricingConditionForm" 
											property="factor" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<%-- **********************************************************************
	                                 exchange rates
                                     ********************************************************************** --%>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.exRate"/>
									</td>
									<td class="detailGroupTableCol"
										align="left">
										<spe:isDirectExchangeRate>
											<bean:write 
												name="pricingConditionForm" 
												property="directExchangeRate" 
												scope="request" 
												filter="true"/>
										</spe:isDirectExchangeRate>
										<spe:isDirectExchangeRate 
											not="true">
											<bean:write 
												name="pricingConditionForm" 
												property="exchangeRate" 
												scope="request" 
												filter="true"/>
										</spe:isDirectExchangeRate>
									</td>
								</tr>
								<spe:isDirectExchangeRate 
									not="true">
									<tr>
										<td class="detailGroupTableCol"
											nowrap>
											<isa:translate 
												key="prc.prcCondForm.prompt.docExRate"/>
										</td>
										<td 
											align="left">
											<bean:write 
												name="pricingConditionForm" 
												property="docExchangeRate" 
												scope="request" 
												filter="true"/>
										</td>
									</tr>
									<%-- **********************************************************************
	                                     local currency
                                         ********************************************************************** --%>
									<tr>
										<td class="detailGroupTableCol"
											nowrap>
											<isa:translate 
												key="prc.prcCondForm.prompt.localCurr"/>
										</td>
										<td class="detailGroupTableCol"
											align="left">
											<bean:write 
												name="pricingConditionForm" 
												property="localCurrency" 
												scope="request" 
												filter="true"/>
										</td>
									</tr>
								</spe:isDirectExchangeRate>
							</table>
						</td>
						<td 
							class="detailGroup">
							<table  class="detailGroupTable"
								border="0" 
								cellspacing=2 
								cellpadding=2 
								class="conditionTable" 
								width="100%">
								<tr>
									<td 
										colspan="3" 
										class="tableLabel">
										<isa:translate 
											key="prc.prcCondForm.table.controlDat"/>
									</td>
								</tr>
								<%-- **********************************************************************
		                             control data
	                                 ********************************************************************** --%>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.inactive"/>
									</td>
									<td class="detailGroupTableCol"
										align="left">
										<bean:write 
											name="pricingConditionForm" 
											property="inactive" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.condClass"/>
									</td>
									<td class="detailGroupTableCol"
										align="left">
										<bean:write 
											name="pricingConditionForm" 
											property="conditionClass" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.calcType"/>
									</td>
									<td class="detailGroupTableCol"
										align="left">
										<bean:write 
											name="pricingConditionForm" 
											property="calculationType" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<spe:isInitialConditionProperty 
									property="conditionCategory" 
									not="true">
									<tr>
										<td class="detailGroupTableCol"
											nowrap>
											<isa:translate 
												key="prc.prcCondForm.prompt.condCat"/>
										</td>
										<td class="detailGroupTableCol"
											align="left">
											<bean:write 
												name="pricingConditionForm" 
												property="conditionCategory" 
												scope="request" 
												filter="true"/>
										</td>
									</tr>
								</spe:isInitialConditionProperty>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.condContr"/>
									</td>
									<td class="detailGroupTableCol"
										align="left">
										<bean:write 
											name="pricingConditionForm" 
											property="conditionControl" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.condOrig"/>
									</td>
									<td class="detailGroupTableCol"
										align="left">
										<bean:write 
											name="pricingConditionForm" 
											property="conditionOrigin" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<%
									for (int i = 0; i < 4; i++) {
								%>

								<tr>
									<td class="detailGroupTableCol"
										colspan="3">
										&nbsp;
									</td>
								</tr>
								<%
									}
								%>

							</table>
						</td>
						<td 
							class="detailGroup">
							<%-- **********************************************************************
 	                             Scales table
                                 ********************************************************************** --%>
							<table  class="detailGroupTable"
								border="0" 
								cellspacing=2 
								cellpadding=2 
								class="conditionTable" 
								width="100%">
								<tr>
									<td 
										colspan="3" 
										class="tableLabel">
										<isa:translate 
											key="prc.prcCondForm.table.scales"/>
									</td>
								</tr>
								<%-- **********************************************************************
		                             condition base
	                                 ********************************************************************** --%>
								<tr>
									<td class="detailGroupTableCol"
										nowrap>
										<isa:translate 
											key="prc.prcCondForm.prompt.baseVa"/>
									</td>
									<td class="detailGroupTableCol"
										align="left">
										<bean:write 
											name="pricingConditionForm" 
											property="conditionBaseValue" 
											scope="request" 
											filter="true"/>
									</td>
								</tr>
								<tr>
									<td class="detailGroupTableCol">
										<isa:translate 
											key="prc.prcCondForm.prompt.scaleType"/>
									</td>
									<td class="detailGroupTableCol">
										<bean:write 
											name="pricingConditionForm" 
											property="scaleType" 
											scope="request" 
											filter="true" 
											/>
									</td>
								</tr>
								<%
									for (int i = 0; i < 7; i++) {
								%>

								<tr>
									<td class="detailGroupTableCol" 
										colspan="3">
										&nbsp;
									</td>
								</tr>
								<%
									}
								%>

							</table>
						</td>
					</tr>
				</table>
			</div>
			<%-- **********************************************************************
	             end of items table
                 ********************************************************************** --%>
			<%-- **********************************************************************
	             start of buttons
                 ********************************************************************** --%>
			<div 
				class="content" 
				width=100%>
				<table 
					cellspacing="0" 
					cellpadding="0" 
					border="0">
					<tr>
						<td 
							width=12 
							height=19>
							
							
							<input name="submit"  type="submit" class="BLUEButton" 
								value="<isa:translate key="prc.prcPanelForm.button.cancel"/>" tabindex="-1">
							
						</td>
					</tr>
				</table>
				<%-- **********************************************************************
	                 end of buttons
                     ********************************************************************** --%>
			</div>
			</div>
		</html:form>
	</body>
</html:html>
