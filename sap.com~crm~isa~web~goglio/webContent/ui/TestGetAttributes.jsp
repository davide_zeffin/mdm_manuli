<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<TITLE>JBuilder Project webui.jpx</TITLE>
</HEAD>
<BODY>

<%
    String server     = request.getParameter("server");
    String port       = request.getParameter("port");
    String sessionId  = request.getParameter("sessionId");
    String documentId = request.getParameter("documentId");
    String itemId     = request.getParameter("itemId");

    String contextPath = request.getContextPath();
    response.setHeader("pragma", "no-cache");
    String current = "/ui/testList.jsp";
    String actionUrl = contextPath
                       +current+"?"
                       +"server="+server
                       +"&port="+port
                       +"&sessionId="+sessionId
		       +"&documentId="+documentId
		       +"&itemId="+itemId;
%>

<h2>Attach to running session</h2>
<FORM  action=<%=actionUrl%>
       method=post>

<table rules=none bgColor=#8fbc8b>
  <tr>
	<td> IPC-Server </td>
	<td> <input type="text" name="server" value="<%= server  %>"> <br> </td>
  </tr>

  <tr>
	<td> IPC-Port </td>
	<td> <input type="text" name="port" value="<%= port %>"><br> </td>
  </tr>

  <tr>
	<td> IPC-sessionId </td>
	<td> <input type="text" name="sessionId" value="<%= sessionId %>"><br> </td>
  </tr>

  <tr>
	<td> documentId </td>
	<td> <input type="text" name="documentId" size ="35" value="<%= documentId %>"><br> </td>
  </tr>

  <tr>
	<td> itemId </td>
	<td> <input type="text" name="itemId"   size ="35" value="<%= itemId %>"> </td>
  </tr>

  <tr>
	<td> language </td>
	<td> <input type="text" name="language" value="D"><br> </td>
  </tr>

  <tr>
	<td> application </td>
    <td> <input type="text" name="application" value="CRM"><br> </td>
  </tr>

</table>
  <input type=submit value="attach">
</FORM>

</BODY>
</HTML>
