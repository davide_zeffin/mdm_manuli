<%-- **********************************************************************

      Pricing condition panel

     ********************************************************************** --%>

<%-- **********************************************************************
      tag libs
     ********************************************************************** --%>
<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/sap-spe.tld"    prefix="spe" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.ipc.webui.pricing.actionform.PricingConditionPanelForm"%><%
PricingConditionPanelForm pricingConditionPanelForm = (PricingConditionPanelForm)session.getAttribute("pricingConditionPanelForm");
%><isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <isa:stylesheets/>
</head>

<body class="tabcontent" text="#000000" link="#424E91" vlink="#7979BD" alink="#336633">

<spe:fireSAPEvent action="PRC"/>

<div class="content">

<%@include file="MessagePanel.jsp" %><%

%><html:form name="pricingConditionPanelForm" type="com.sap.ipc.webui.pricing.actionform.PricingConditionPanelForm" action="/savePricingConditionPanel.do">

<%-- **********************************************************************
      start of outer frame table
     ********************************************************************** --%><%
%><table class="infoTable"  border="0" cellspacing="0" cellpadding="0" width="100%">
<tr><td>

<%-- **********************************************************************
      start of inner frame table
     ********************************************************************** --%><%
%><table border="0" cellpadding="5" cellspacing="1" width="100%"><%
%><tr><td class="BoxContent" width="100%">

<%-- **********************************************************************
      start of buttons
     ********************************************************************** --%><%
%><table border="0" width="100%">
  <tr>
    <td>
      <table>
        <tr>
          <logic:equal name="pricingConditionPanelForm" property="mode"
             scope="session" value="Edit">
            <td align="left">
                <input name="submit"
                    type="submit" class="BLUEButton" value="<isa:translate key="prc.prcPanelForm.button.save"/>" 
                    tabindex="-1"
                >
            </td>
            <spe:checkConditionPanelPropertyValue property="header" value="false">
              <td align="left">
                <input name="repricing" 
                    type="button" class="BLUEButton" value="<isa:translate key="prc.prcPanelForm.button.pricing"/>" 
                    tabindex="-1" onClick="self.location.href='<isa:webappsURL name="/performPricing.do" />'"
                >
              </td>
            </spe:checkConditionPanelPropertyValue>
          </logic:equal>
<%----
          <td align="left">
            <table cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td width=12 height=19><html:link page="/showPricingAttributes.do" ><img src="mimes/images/buttons/blue_left.gif" width=12 height=19 border=0 alt=" "></html:link></td>
                <td height=19 background="mimes/images/buttons/blue_tile.gif"><html:link page="/showPricingAttributes.do" styleClass="BLUEButton" ><isa:translate key="prc.prcPanelForm.button.attrib"/></html:link></td>
                <td width=12 height=19><html:link page="/showPricingAttributes.do" ><img src="mimes/images/buttons/blue_right.gif" width=12 height=19 border=0 alt=" "></html:link></td>
              </tr>
            </table>
          </td>
---%><%          
          %><spe:checkConditionPanelPropertyValue property="pricingAnalysisActive" value="true">
            <td align="left">
              <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td height=19 >                  	
	                  	<input type=button name="pricingAnal" class="BLUEButton" value="<isa:translate key="prc.prcPanelForm.button.accesses"/>" 
			        	tabindex="-1" onClick="self.location.href='<isa:webappsURL name="/showPricingAnalysis.do" />'">
				  </td>
 	              
                </tr>
              </table>
            </td>
            <td align="left">
              <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td height=19 >
	                  <input type=button name="tteAnal" class="BLUEButton" value="<isa:translate key="prc.prcPanelForm.button.tte"/>" 
				        tabindex="-1" onClick="self.location.href='<isa:webappsURL name="/showTTEAnalysis.do" />'">
                  </td>

                </tr>
              </table>
            </td>
          </spe:checkConditionPanelPropertyValue>

          <spe:checkConditionPanelPropertyValue property="showBackButton" value="true">
            <td align="right">
              <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td height=19 >                  	
	                  	<input type=button name="back" class="BLUEButton" value="<isa:translate key="cmdd.button.back"/>" 
			        	tabindex="-1" onClick="self.location.href='<isa:webappsURL name="/backToConfig.do"/>'">
				  </td>
 	              
                </tr>
              </table>
            </td>
          </spe:checkConditionPanelPropertyValue>

        </tr>
      </table>
    </td>
  </tr>
</table>

<%-- **********************************************************************
      start of content table
     ********************************************************************** --%><%
%><table class="infoTable"  border=0 cellspacing=0 cellpadding=0 width="100%">

<tr>
  <td class="tableName" valign="middle">&nbsp;</td>
</tr>

<tr><td>

<%-- **********************************************************************
      start of item table
     ********************************************************************** --%>
<table class="conditionTable">
<%-- **********************************************************************
      headings
     ********************************************************************** --%><%
  %><tr class="tableHeader">
    <td class="conditionStatus"><%
    %></td>
    <td class="conditionType">
      <isa:translate key="prc.prcPanelForm.head.condType"/>
    </td>
    <td class="description">
      <isa:translate key="prc.prcPanelForm.head.descr"/>
    </td>
    <td class="conditionRate">
      <isa:translate key="prc.prcPanelForm.head.condRate"/>
    </td>
    <td class="priceUnit">
      <isa:translate key="prc.prcPanelForm.head.prcUnit"/>
    </td>
    <td class="conditionValue">
      <isa:translate key="prc.prcPanelForm.head.condValue"/>
    </td>
    <td class="operation">
      &nbsp;
    </td>
  </tr><%
  boolean firstNewLine = true; //indicator to identify the first new empty condition line
  %><logic:iterate id="pricingConditionForm" name="pricingConditionPanelForm" property="pricingConditionForms" indexId="indexId">
      <spe:isConditionSubtotal>
          <tr class="Subtotal">
      </spe:isConditionSubtotal>
      <spe:isConditionSubtotal not="true">
          <tr>
      </spe:isConditionSubtotal>
<%-- **********************************************************************
      condition status
     ********************************************************************** --%><%
     %><td><%
         %><spe:isConditionSubtotal not="true"><%
	         %><spe:isInactiveCondition><%
	             %><img src="<isa:mimeURL name="/mimes/images/s_s_ledy.gif" />" width="16" height="15" border=0 alt="<isa:translate key="condition.inactive" />"><%
	         %></spe:isInactiveCondition><%
	         %><spe:isInactiveCondition not="true"><%
		         %><img src="<isa:mimeURL name="/mimes/images/s_s_ledg.gif" />" width="16" height="15" border="0" alt="<isa:translate key="condition.active" />"><%
	         %></spe:isInactiveCondition><%
	         %></spe:isConditionSubtotal><%
     %></td><%
     
%><%--**********************************************************************
      condition type name
     ********************************************************************** --%>
        <logic:equal name="pricingConditionForm" property="action" value="Create">
            <td  colspan="2" align="left"><%
                String accesskey = "";
                if (firstNewLine) {
                	accesskey=WebUtil.translate(pageContext, "prc.key.lastfocused", null);
                	firstNewLine = false;
                }
                %><html:select name="pricingConditionPanelForm" property='<%="conditionTypeName"+"["+indexId+"]"%>' accesskey="<%= accesskey%>">
                    <html:options name="pricingConditionForm" property="conditionTypeNameSelection" labelName="pricingConditionForm" labelProperty="conditionTypeNameSelectionLabels"/>
                </html:select>
            </td>
	    </logic:equal>
        <logic:notEqual name="pricingConditionForm" property="action" value="Create">
            <td align="left">
                <bean:write name="pricingConditionForm" property="conditionTypeName" filter="true"/>
            </td>    
	    </logic:notEqual>

<%-- **********************************************************************
      description
     ********************************************************************** --%>
		<logic:notEqual name="pricingConditionForm" property="action" value="Create">
			<td align="left">
	            <bean:write name="pricingConditionForm" property="description" filter="true"/>
			</td>
	    </logic:notEqual>

<%-- **********************************************************************
      condition rate
     ********************************************************************** --%>
      <spe:isConditionSubtotal>
        <td>
          <table border=0 cellspacing=0 cellpadding=2 width="100%">
            <tr>
              <td align="right" width="80%" class="Highlighted">
                <bean:write name="pricingConditionForm" property="conditionRateValue" filter="true"/>
              </td>
              <td align="left" width="20%" class="Highlighted">
                <bean:write name="pricingConditionForm" property="conditionCurrency" filter="true"/>
              </td>
            </tr>
          </table>
        </td>
      </spe:isConditionSubtotal>
      <spe:isConditionSubtotal not="true">
	        <logic:equal name="pricingConditionPanelForm" property="mode"
	                                  scope="session" value="Display">
		        <td>
		          <table border=0 cellspacing=0 cellpadding=2 width="100%">
		            <tr>
		              <td align="right" width="80%" class="tableRow">
		                <bean:write name="pricingConditionForm" property="conditionRateValue" filter="true"/>
		              </td>
		              <td align="left" width="20%" class="tableRow">
		                <bean:write name="pricingConditionForm" property="conditionCurrency" filter="true"/>
		              </td>
		            </tr>
		          </table>
		        </td>
	        </logic:equal>
	        <logic:equal name="pricingConditionPanelForm" property="mode"
	                                  scope="session" value="Edit">
		        <spe:changeOfConditionPropertyAllowed property="conditionRateValue" not="true">
			        <td>
			          <table border=0 cellspacing=0 cellpadding=2 width="100%">
			            <tr>
			              <td align="right" width="80%" class="tableRow">
			                <bean:write name="pricingConditionForm" property="conditionRateValue" filter="true"/>
			              </td>
			              <td align="left" width="20%" class="tableRow">
			                <bean:write name="pricingConditionForm" property="conditionCurrency" filter="true"/>
			              </td>
			            </tr>
			          </table>
			        </td>
		        </spe:changeOfConditionPropertyAllowed>
		        <spe:changeOfConditionPropertyAllowed property="conditionRateValue">
			        <td>
			          <table border=0 cellspacing=0 cellpadding=2 width="100%">
			            <tr>
			              <td align="right" width="80%" class="tableRow">
					        <html:text name="pricingConditionPanelForm" property='<%="conditionRateValue"+"["+indexId+"]"%>' maxlength="32" size="16"/>
			              </td>
			              <td align="right" width="20%" class="tableRow">
			   		        <spe:checkConditionPropertyValue property="calculationType" value="A" not="true">
			                  <html:text name="pricingConditionPanelForm" property='<%="conditionCurrency"+"["+indexId+"]"%>' size="3"/>
			  		        </spe:checkConditionPropertyValue>
					        <spe:checkConditionPropertyValue property="calculationType" value="A">
			                  <bean:write name="pricingConditionForm" property="conditionCurrency" filter="true"/>
			     		    </spe:checkConditionPropertyValue>
			              </td>
			            </tr>
			          </table>
			        </td>
		        </spe:changeOfConditionPropertyAllowed>
	        </logic:equal>
      </spe:isConditionSubtotal>


<%-- **********************************************************************
      pricing unit
     ********************************************************************** --%>
      <td align="right" class="tableRow">
        <spe:checkConditionPropertyValue property="calculationType" value="A" not="true">
        <spe:checkConditionPropertyValue property="calculationType" value="B" not="true">
          <table border=0 cellspacing=0 cellpadding=2 width="100%">
            <tr>
              <spe:isConditionSubtotal>
                <td align="right" width="70%" class="Highlighted">
                  <bean:write name="pricingConditionForm" property="pricingUnitValue" filter="true"/>
                </td>
                <td align="left" width="30%" class="Highlighted">
                  <bean:write name="pricingConditionForm" property="pricingUnitUnit" filter="true"/>
                </td>
              </spe:isConditionSubtotal>
              <spe:isConditionSubtotal not="true">
                <logic:equal name="pricingConditionPanelForm" property="mode"
                                  scope="session" value="Display">
                  <td align="right" width="70%" class="tableRow">
                    <bean:write name="pricingConditionForm" property="pricingUnitValue" filter="true"/>
                  </td>
                  <td align="left" width="30%" class="tableRow">
                    <bean:write name="pricingConditionForm" property="pricingUnitUnit" filter="true"/>
                  </td>
                </logic:equal>
                <logic:equal name="pricingConditionPanelForm" property="mode"
                                  scope="session" value="Edit">
                  <spe:changeOfConditionPropertyAllowed property="pricingUnitValue">
                    <td align="right" width="70%"  class="entry">
                      <html:text name="pricingConditionPanelForm" property='<%="pricingUnitValue"+"["+indexId+"]"%>' size="6" maxlength="12"/>
                    </td>
                    <td align="left" width="30%" class="entry">
                      <html:text name="pricingConditionPanelForm" property='<%="pricingUnitUnit"+"["+indexId+"]"%>' size="3"/>
                    </td>
                  </spe:changeOfConditionPropertyAllowed>
                  <spe:changeOfConditionPropertyAllowed property="pricingUnitValue" not="true">

                    <td align="right" width="70%" class="tableRow">
                      <bean:write name="pricingConditionForm" property="pricingUnitValue" filter="true"/>
                    </td>
                    <td align="left" width="30%" class="tableRow">
                      <bean:write name="pricingConditionForm" property="pricingUnitUnit" filter="true"/>
                    </td>
                  </spe:changeOfConditionPropertyAllowed>
                </logic:equal>
              </spe:isConditionSubtotal>
            </tr>
          </table>
        </spe:checkConditionPropertyValue>
        </spe:checkConditionPropertyValue>
      </td>

<%-- **********************************************************************
      create mode: empty condition value and button column
     ********************************************************************** --%>
	  <logic:equal name="pricingConditionForm" property="action" value="Create">
        <td class="tableRow">
        </td>
        <td class="tableRow">
        </td>
      </logic:equal>

      <logic:notEqual name="pricingConditionForm" property="action" value="Create">

<%-- **********************************************************************
      condition value
     ********************************************************************** --%>
      <spe:isInactiveCondition>
        <td align="right" class="tableRow">
          <table border=0 cellspacing=0 cellpadding=2 width="100%">
            <tr>
              <td align="right" width="80%" class="Highlighted">
                <bean:write name="pricingConditionForm" property="conditionValueValue" filter="true"/>
              </td>
              <td align="left" width="20%" class="Highlighted">
                <bean:write name="pricingConditionForm" property="documentCurrency" filter="true"/>
              </td>
            </tr>
          </table>
        </td>
      </spe:isInactiveCondition>
      <spe:isInactiveCondition not="true">
        <td align="right" class="tableRow">
          <table border=0 cellspacing=0 cellpadding=2 width="100%">
            <tr>
              <td align="right" width="80%" class="tableRow">
                <bean:write name="pricingConditionForm" property="conditionValueValue" filter="true"/>
              </td>
              <td align="left" width="20%" class="tableRow">
                <bean:write name="pricingConditionForm" property="documentCurrency" filter="true"/>
              </td>
            </tr>
          </table>
        </td>
      </spe:isInactiveCondition>

<%-- **********************************************************************
      buttons
     ********************************************************************** --%>
      <td align="center" class="tableRow">
        <nobr>
          <spe:isConditionSubtotal>
            <spe:linkPricingCondition page="/gotoConditionDetails.do?action=Display" tabindex="-1" >
              <img src="<isa:mimeURL name="/mimes/images/s_b_detl.gif" />" border="0" alt="<isa:translate key="show.details" />">
            </spe:linkPricingCondition>
          </spe:isConditionSubtotal>
          <spe:isConditionSubtotal not="true">
            <logic:equal name="pricingConditionPanelForm" property="mode"
                                 scope="session" value="Display">
              <spe:linkPricingCondition page="/gotoConditionDetails.do?action=Display" tabindex="-1">
                <img src="<isa:mimeURL name="/mimes/images/s_b_detl.gif" />" border="0"  alt="<isa:translate key="show.details" />">
              </spe:linkPricingCondition>
            </logic:equal>
            <logic:equal name="pricingConditionPanelForm" property="mode"
                      scope="session" value="Edit">
	          <spe:changeOfConditionAllowed>
                <spe:linkPricingCondition page="/gotoConditionDetails.do?action=Display" tabindex="-1">
                  <img src="<isa:mimeURL name="/mimes/images/s_b_chng.gif" />" border="0" alt="<isa:translate key="condition.change" />">
                </spe:linkPricingCondition>
	          </spe:changeOfConditionAllowed>
	          <spe:deletionOfConditionAllowed>
                <spe:linkPricingCondition page="/deletePricingCondition.do?action=Delete" tabindex="-1">
                  <img src="<isa:mimeURL name="/mimes/images/s_b_dele.gif" />" border="0"  alt="<isa:translate key="condition.delete" />">
                </spe:linkPricingCondition>
	          </spe:deletionOfConditionAllowed>
	          <spe:changeOfConditionAllowed not="true">
	            <spe:deletionOfConditionAllowed not="true">
                  <spe:linkPricingCondition page="/gotoConditionDetails.do?action=Display" tabindex="-1">
                    <img src="<isa:mimeURL name="/mimes/images/s_b_detl.gif" />" border="0"  alt="<isa:translate key="show.details" />">
                  </spe:linkPricingCondition>
	            </spe:deletionOfConditionAllowed>
	          </spe:changeOfConditionAllowed>
            </logic:equal>
          </spe:isConditionSubtotal>
        </nobr>
      </td>
	    </logic:notEqual>
    </tr>
  </logic:iterate>

<%-- **********************************************************************
      end of item table
     ********************************************************************** --%>
</table>

</td></tr>
<%-- **********************************************************************
      end of item table
     ********************************************************************** --%>
</table>

</td></tr>
<%-- **********************************************************************
      end of inner frame table
     ********************************************************************** --%>
</table>

</td></tr>
<%-- **********************************************************************
      end of outer frame table
     ********************************************************************** --%>
</table>

</html:form>

</div>

</body>

</html:html>

