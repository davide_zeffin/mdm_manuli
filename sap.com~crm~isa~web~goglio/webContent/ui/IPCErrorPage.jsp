<%@ page import="com.sap.spc.remote.client.object.IPCException" %>
<%@ page import="com.sap.msasrv.socket.shared.ErrorCodes" %>

<%
    IPCException e = (IPCException)request.getAttribute("ipcException");
%>
<HTML>

<HEAD>
<!-- <LINK REL="stylesheet" TYPE="text/css" HREF="$IPC_ROOT/stylesheets/sce.css"> -->
<TITLE>SPE Error</TITLE>
</HEAD>

<BODY BGCOLOR="#DED5C4" LEFTMARGIN="10" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" text="black" link="#000000" vlink="#000000" alink="#000000" >

<br>

<H1>IPC Error</H1>

<p>An error has occured in the IPC:</p>

<center>
<p><hr>
<p><font size="+2"><b>
<%= e.getMessage().toString() %> </b></font></p>
</center>

<p><br>
<p><br>
<p>
<%  if (e.getCode().equals(ErrorCodes.RET_INTERNAL_FATAL)) {                    %>
    This error is fatal. Please restart your session or contact the server administrator.
<%  }                                                                           %>

<%  if (e.getCode().equals(ErrorCodes.RET_UNKNOWN_CMD)) {                       %>
    An unknown command was executed. The client and the server are probably out of sync.
<%  }                                                                           %>

<%  if (e.getCode().equals(ErrorCodes.RET_TIMEDOUT_ID)) {                    %>
    Your server session has been inactive too long and reached a timeout.
<%  }                                                                           %>

<%  if (e.getCode().equals(ErrorCodes.RET_UNKNOWN_ID)) {                    %>
    The server does not know your session. The reason is probably that
    the server has removed your session due to a timeout. Please start a new session.
<%  }                                                                           %>

<%  if (e.getCode().equals("411")) {                    %>
    The servlet encountered a command that it doesn't know.
<%  }                                                                           %>

<%  if (e.getCode().equals("888")) {                    %>
    Trace contains no data. Please check server log.
<%  }                                                                           %>
</p>
<p>
<hr>

</BODY>

</HTML>