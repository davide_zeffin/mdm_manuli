<%@ page language="java" contentType="text/html" %>

<%@ page import="com.sap.spc.remote.client.object.*"%>
<%@ page import="com.sap.spc.remote.client.object.imp.*"%>
<%@ page import="com.sap.spc.remote.client.ClientSupport"%>
<%@ page import="com.sap.sxe.socket.shared.ServerDescription"%>
<%@ page import="com.sap.sxe.socket.client.DispatcherClient"%>
<%@ page import="java.util.TreeSet"%>
<%@ page import="java.util.Iterator"%>

<%
  response.setHeader("pragma", "no-cache");
    
  String contextPath = request.getContextPath();
  String current     = "/ui/BufferOverview.jsp";
  String actionUrl   = contextPath+current;
  String testTreeJsp = "/ui/BufferTree.jsp";

  String server = request.getParameter("server");
  String port = request.getParameter("port");
  String password = request.getParameter("password");
  String dispatcher = request.getParameter("dispatcher");

  String client = request.getParameter("targetClient");

  String view = request.getParameter("view");
   
%>

<HTML>
  <HEAD>
    <LINK REL="stylesheet" TYPE="text/css" HREF="../styles/spe.css">
    <TITLE>BufferOverview.jsp</TITLE>
  </HEAD>

  <%
    if (server == null && port == null && client == null) {
  %>


  <BODY CLASS="tabcontent" LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" TEXT="#000000" LINK="#424E91" VLINK="#7979BD" ALINK="#336633">

  <DIV CLASS="content"  WIDTH=100%>

  <TABLE CLASS="infoTable" BORDER="0" CELLPADDING="0" CELLSPACING="0"  WIDTH=100% >
    <TR>
      <TD>

        <TABLE BORDER="0" CELLPADDING="5" CELLSPACING="1" WIDTH=100%>
          <TR>
	    <TD CLASS='tableName' VALIGN='middle'>Attach to running session</TD>
          </TR>

          <FORM ACTION=<%=actionUrl%> METHOD=post>

          <TR>
            <TD CLASS="BoxContent" WIDTH=100%>

              <TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
                <TR>
	          <TD>IPC-Server</TD>
	          <TD><INPUT TYPE="text" NAME="server" VALUE""></TD>
                </TR>

                <TR>
	          <TD>IPC-Port</TD>
	          <TD><INPUT TYPE="text" NAME="port" VALUE=""></TD>
                </TR>

                <TR>
	          <TD>is Dispatcher</TD>
	          <TD><INPUT TYPE="checkbox" NAME="dispatcher" VALUE="X"></TD>
                </TR>
      
                <TR>
                  <TD>Ziel Mandant</TD>
        	  <TD><INPUT TYPE="text" NAME="targetClient" VALUE=""></TD>
                </TR>

                <TR>
	          <TD>Condition Table - Types - Buffers</TD>
	          <TD><INPUT TYPE="radio" NAME="view" CHECKED VALUE="1"></TD>
                </TR>

                <TR>
	          <TD>Condition Types - Table - Buffers</TD>
	          <TD><INPUT TYPE="radio" NAME="view" VALUE="2"></TD>
                </TR>
                
                
              </TABLE>

              <INPUT TYPE=submit NAME="submit" VALUE="get sessions">
            </TD>
          </TR>

          </FORM>
 
        </TABLE>
      
      </TD>
    </TR>
  </TABLE>

  </DIV>
  </BODY>

  <%
    }//if
    else if (server != null && port != null && client != null) {
  %>

  <BODY CLASS="tabcontent" LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" TEXT="#000000" LINK="#424E91" VLINK="#7979BD" ALINK="#336633">

  <DIV CLASS="content"  WIDTH=100%>

  <TABLE CLASS="infoTable" BORDER="0" CELLPADDING="0" CELLSPACING="0"  WIDTH=100% >
    <TR>
      <TD>

        <TABLE BORDER="0" CELLPADDING="5" CELLSPACING="1" WIDTH=100%>
          <TR>
            <TD CLASS="BoxContent" WIDTH=100%>

              <TABLE CLASS='infoTable'  BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH='100%'>
                <TR>
	          <TD CLASS='tableName' VALIGN='middle'>Available Servers</TD>
                </TR>

                <TR>
                  <TD>
                    <TABLE BORDER=0 WIDTH='100%' CELLSPACING=1 CELLPADDING=2>
                
                      <TR>
                        <TD CLASS=tableHeader>Server</TD>
                        <TD CLASS=tableHeader>Port</TD>
                        <TD CLASS=tableHeader>Program Id</TD>
                        <TD CLASS=tableHeader></TD>
                      </TR>
  <%
    ServerDescription[] serverDescription = null;
    try {
      String encoding = "UnicodeLittle";
      DispatcherClient d = new DispatcherClient(server, port, encoding);
      serverDescription = d.getServerInfo();
    }
    catch (Exception e) {
      request.setAttribute(com.sap.ipc.webui.pricing.UIConstants.Request.Parameter.IPC_EXCEPTION, new IPCException(e));
  %>
      <jsp:forward page="IPCErrorPage.jsp" />
  <%
    }
    String serverBaseUrl =   "http://"
                           + request.getServerName()
                           + ":"
                           + request.getServerPort()
                           + contextPath
                           + testTreeJsp
                           + "?"
                           + "client=" + client
                           + "&view=" + view
                           + "&password=" + password
                         ;
    for (int i = 0; i < serverDescription.length; i++) {
  %>
       
                      <TR>
  <%
    StringBuffer serverUrl = new StringBuffer(serverBaseUrl);
    serverUrl.append("&server="+serverDescription[i].getHost());
    serverUrl.append("&port="+serverDescription[i].getPort());
  %>
                        <TD CLASS="tableRow" ALIGN="left"><%=serverDescription[i].getHost()%></TD>
                        <TD CLASS="tableRow" ALIGN="left"><%=serverDescription[i].getPort()%></TD>
                        <TD CLASS="tableRow" ALIGN="left"><%=serverDescription[i].getProgramId()%></TD>
                        <TD CLASS="tableRow" ALIGN="left"><A HREF='<%=serverUrl.toString()%>'><IMG SRC="../mimes/images/s_b_detl.gif" BORDER=0></A></TD>
                      </TR>
  <%
    }//for
  %>
                    </TABLE>

                  </TD>
                </TR>
              </TABLE>              
            
            </TD>
          </TR>
        </TABLE>

      </TD>
    </TR>
  </TABLE>

  </DIV>
  </BODY>

  <%
    }//else if
  %>

</HTML>
