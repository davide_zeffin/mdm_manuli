<%@ page language="java" contentType="text/html" %>

<%@ page import="java.net.URLEncoder" %>

<%@ page import="com.sap.spc.remote.client.ClientSupport" %>
<%@ page import="com.sap.sxe.socket.client.ClientException" %>
<%@ page import="com.sap.sxe.socket.client.ServerResponse" %>

<%@ page import="com.sap.sxe.socket.shared.ErrorCodes" %>

<%@ page import="com.sap.spc.remote.shared.command.*" %>
<%@ page import="com.sap.spc.remote.client.object.IPCException" %>
<%@ page import="com.sap.ipc.webui.pricing.UIConstants" %>

<%!
  ClientSupport m_client;
  String[][]    m_result;
  String[]      varCond_result;
  String[]      condAcc_result;
  String        product_id_result;
  String        product_description_result;
  String        pricing_timestamp_result;


  String ipcServer;
  String ipcPort;
  String ipcSessionId;
  String ipcDocumentId;
  String ipcItemId;
  HttpSession httpSession;
%>

<%
  ipcServer        = request.getParameter("server");
  ipcPort          = request.getParameter("port");
  ipcSessionId     = request.getParameter("sessionId");
  ipcDocumentId    = request.getParameter("documentId");
  ipcItemId        = request.getParameter("itemId");
  httpSession      = request.getSession();
%>

<HTML>
  <HEAD>
    <LINK REL="stylesheet" TYPE="text/css" HREF="../styles/spe.css">
    <TITLE>ItemInfo.jsp</TITLE>
  </HEAD>


  <BODY CLASS="tabcontent"  LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 TEXT="#000000" LINK="#424E91" VLINK="#7979BD" ALINK="#336633">

    <DIV CLASS="content"  WIDTH=100%>

    <% attachToSession(ipcSessionId);

       m_result = getInfo(ipcDocumentId, ipcItemId);
       if (m_result[0].length == 0) {
         throw new IPCException(new ClientException("888", "No Info found"));
       }

       product_id_result = getProductId(ipcDocumentId, ipcItemId);
       product_description_result = getProductDescription(ipcDocumentId, ipcItemId);
       pricing_timestamp_result = getPricingTimeStamp(ipcDocumentId, ipcItemId);

       varCond_result = getVarcondInfo(ipcDocumentId, ipcItemId);

       condAcc_result = getCondAccInfo(ipcDocumentId, ipcItemId);
       
    %>

    <%
      out.println("<TABLE CLASS=\"infoTable\" BORDER=\"0\" CELLPADDING=\"0\" CELLSPACING=\"0\" WIDTH=100% >");
      out.println("  <TR>");
      out.println("  <TD>");
      out.println("  <TABLE BORDER=\"0\" CELLPADDING=\"5\" CELLSPACING=\"1\" WIDTH=100% >");
      out.println("  <TR>");
      out.println("    <TD CLASS=\"BoxContent\" VALIGN=\"TOP\">");
      out.println("      <TABLE CLASS=\"infoTable\" BORDER=\"0\" CELLSPACING=\"1\" CELLPADDING=\"2\" WIDTH=100% >");
      out.println("        <TR>");
      out.println("          <TD CLASS=\"tableName\"  COLSPAN=\"2\" VALIGN=\"MIDDLE\"> Header Parameters </TD>");
      out.println("        </TR>");
      out.println("        <TR>");
      out.println("          <TD CLASS=\"tableHeader\"><NOBR>Name</NOBR></TD>");
      out.println("          <TD CLASS=\"tableHeader\"><NOBR>Value</NOBR></TD>");
      out.println("        </TR>");
//Additional Information
      out.println("        <TR>");
      out.println("          <TD CLASS=\"tableRow\"><NOBR>PRODUCT_ID</NOBR></TD>");
      out.println("          <TD CLASS=\"tableRow\"><NOBR>" + product_id_result + "</NOBR></TD>");
      out.println("        </TR>");
      out.println("        <TR>");
      out.println("          <TD CLASS=\"tableRow\"><NOBR>PRODUCT_DESCRIPTION</NOBR></TD>");
      out.println("          <TD CLASS=\"tableRow\"><NOBR>" + product_description_result + "</NOBR></TD>");
      out.println("        </TR>");
      out.println("        <TR>");
      out.println("          <TD CLASS=\"tableRow\"><NOBR>PRICING_TIMESTAMP</NOBR></TD>");
      out.println("          <TD CLASS=\"tableRow\"><NOBR>" + pricing_timestamp_result + "</NOBR></TD>");
      out.println("        </TR>");
      for (int i = 0; i < m_result[0].length; i++){
	out.println("      <TR>");
	for (int j = 0; j < 2; j++){
          if (m_result[j][i] != null){
            out.println("    <TD CLASS=\"tableRow\"><NOBR>" + m_result[j][i] + "</NOBR></TD>");
	  //out.println("    <TD><NOBR>" + m_result[j][i] + "</NOBR></TD>");
	  }
	  else{
            out.println("    <TD CLASS=\"tableRow\" ALIGN=LEFT>-</TD>");
	  //out.println("    <TD ALIGN=LEFT>-</TD>");
	  }
	}
        out.println("      </TR>");
      }
      out.println("      </TABLE>");
      out.println("    </TD>");
      out.println("    <TD CLASS=\"BoxContent\" VALIGN=\"TOP\">");
      out.println("      <TABLE CLASS=\"infoTable\" BORDER=\"0\" CELLSPACING=\"1\" CELLPADDING=\"2\" WIDTH=100% >");
      out.println("        <TR>");
      out.println("          <TD CLASS=\"tableName\"  COLSPAN=\"2\" VALIGN=\"MIDDLE\"> Item Parameters </TD>");
      out.println("        </TR>");
      out.println("          <TR>");
      out.println("          <TD CLASS=\"tableHeader\"><NOBR>Name</NOBR></TD>");
      out.println("          <TD CLASS=\"tableHeader\"><NOBR>Value</NOBR></TD>");
      out.println("        </TR>");
      for (int i = 0; i < m_result[2].length; i++) {
	out.println("      <TR>");
	for (int j = 2; j < 4; j++){
	  if (m_result[j][i] != null){
	    out.println("    <TD CLASS=\"tableRow\"><NOBR>" + m_result[j][i] + "</NOBR></TD>");
	  }
	  else{
	    out.println("    <TD CLASS=\"tableRow\" ALIGN=LEFT>-</TD>");
	  }
	}
	out.println("      </TR>");
      }
      if (condAcc_result != null) {
        for (int i = 0; i < condAcc_result.length; i = i +2) {
  %>
                  <TR>
                    <TD CLASS="tableRow"><%=condAcc_result[i]%></TD>
                    <TD CLASS="tableRow"><%=condAcc_result[i+1]%></TD>
                  </TR>
  <%  
        }//for
      }//if
      out.println("      </TABLE>");
      out.println("    </TD>");
      out.println("  </TR>");
      out.println("  </TD>");
      out.println("  </TR>");
      out.println("  </TABLE>");
    %>
      <TR>
        <TD>
          <TABLE BORDER=0 CELLPADDING=5 CELLSPACING=1 WIDTH=100%>
            <TR>
              <TD CLASS="BoxContent" VALIGN="TOP">
                <TABLE CLASS="infoTable" BORDER=0 CELLSPACING=1 CELLPADDING=2 WIDTH=100%>
                  <TR>
                    <TD CLASS="tableName"  COLSPAN=3 VALIGN="MIDDLE">Variant Conditions</TD>
                  </TR>
                  <TR>
                    <TD CLASS="tableHeader"><NOBR>Name</NOBR></TD>
                    <TD CLASS="tableHeader"><NOBR>Description</NOBR></TD>
                    <TD CLASS="tableHeader"><NOBR>Factor</NOBR></TD>
                  </TR>
  <%
    if (varCond_result != null) {

      for (int i = 0; i < varCond_result.length; i = i + 3) {
  %>
                  <TR>
                    <TD CLASS="tableRow"><%=varCond_result[i]%>
                    <TD CLASS="tableRow"><%=varCond_result[i+1]%>
                    <TD CLASS="tableRow"><%=varCond_result[i+2]%>
                  </TR>
  <%
      }//for
    }//if
  %>
                </TABLE>
              </TD>
            </TR>
          </TABLE>
        </TD>        
      </TR>
    <%
      out.println("</TABLE>");
    %>

  </BODY>
</HTML>

<%!
  private ClientSupport getClient(HttpSession httpSession, String server, String port)
    throws IPCException {
    String encoding = "UnicodeLittle";

    ClientSupport client = (ClientSupport)httpSession.getAttribute("CLIENT");

    if (client == null) {
      if (server == null || port == null) {
	throw new IPCException(new ClientException(ErrorCodes.RET_INTERNAL_FATAL,
	  "no parameters for connecting to client: server = " + server + " port = " + port));
      }
      try {
	client = new ClientSupport(server, Integer.parseInt(port), encoding, new Boolean("false"));
      }
      catch (ClientException e) {
	throw new IPCException(e);
      }
    }

    httpSession.setAttribute("CLIENT",client);

    return client;
  }
  
  private void attachToSession(String sessionId)
    throws Exception {
        
    m_client = getClient(httpSession, ipcServer, ipcPort);

    m_client.setSessionId(sessionId);
  }

  public class ItemInfo implements com.sap.spc.remote.shared.command.CommandConstants {

    private String getProductId(String documentId, String itemId)
      throws Exception {

      String i_pid_result = new String();

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_ITEM_INFO, new String[] {
                                                                      GetItemInfo.DOCUMENT_ID, documentId,
                                                                      GetItemInfo.ITEM_ID, itemId,
                                                                    });

        i_pid_result = r.getParameterValue(GetItemInfo.PRODUCT_ID);
      }

      return i_pid_result;
    }

    private String getProductDescription(String documentId, String itemId)
      throws Exception {

      String i_pdesc_result = new String();

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_ITEM_INFO, new String[] {
                                                                      GetItemInfo.DOCUMENT_ID, documentId,
                                                                      GetItemInfo.ITEM_ID, itemId,
                                                                    });

        i_pdesc_result = r.getParameterValue(GetItemInfo.PRODUCT_DESCRIPTION);
      }

      return i_pdesc_result;
    }

    private String getPricingTimeStamp(String documentId, String itemId)
      throws Exception {

      String i_prts_result = new String();

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_ITEM_INFO, new String[] {
                                                                      GetItemInfo.DOCUMENT_ID, documentId,
                                                                      GetItemInfo.ITEM_ID, itemId,
                                                                    });

        i_prts_result = r.getParameterValue(GetItemInfo.PRICING_TIMESTAMP);
      }

      return i_prts_result;
    }

    private String[] getCondAccInfo(String documentId, String itemId)
      throws Exception {

      String[] result = null;
      String[] result_Name;
      String[] result_Value;

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_ITEM_INFO, new String[] {
  	    							      GetItemInfo.DOCUMENT_ID, documentId,
								      GetItemInfo.ITEM_ID, itemId,
								    });

        result_Name = r.getParameterValues(GetItemInfo.CONDITION_ACCESS_TIMESTAMP_NAME);
        result_Value = r.getParameterValues(GetItemInfo.CONDITION_ACCESS_TIMESTAMP_VALUE);

        if (result_Name != null && result_Value != null) {

          result = new String[2 * result_Name.length];

          for (int i = 0; i < result_Name.length; i++) {
            result[i * 2] = result_Name[i];
            result[i * 2 + 1] = result_Value[i];
          }//for
        }//if

      }//if

      return result;
    }//private String[] getCondAccInfo(...

    private String[] getVarcondInfo(String documentId, String itemId)
      throws Exception {

      String[] result = null;
      String[] result_Name;
      String[] result_Desc;
      String[] result_Fac;

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_ITEM_INFO, new String[] {
  	    							      GetItemInfo.DOCUMENT_ID, documentId,
								      GetItemInfo.ITEM_ID, itemId,
								    });

        result_Name = r.getParameterValues(GetItemInfo.ITEM_VARCOND_NAME);
        result_Desc = r.getParameterValues(GetItemInfo.ITEM_VARCOND_DESCRIPTION);
        result_Fac = r.getParameterValues(GetItemInfo.ITEM_VARCOND_FACTOR);

        if (result_Name != null && result_Desc != null && result_Fac != null) {
          result = new String[3 * result_Name.length];

          for (int i = 0; i < result_Name.length; i++) {
            result[i * 3] = result_Name[i];
            result[i * 3 + 1] = result_Desc[i];
            result[i * 3 + 2] = result_Fac[i];
          }//for
        }//if
      }//if

      return result;

    }//private String[] getVarcondInfo(... 

    private String[][] getItemInfo(String documentId, String itemId) 
      throws Exception {

      String[][] i_m_result = new String[4][];
      String[]   m_result_part;

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_ITEM_INFO, new String[] {
  	    							      GetItemInfo.DOCUMENT_ID, documentId,
								      GetItemInfo.ITEM_ID, itemId,
								    });
    
        for (int i = 0; i < i_m_result.length; i++){
          switch (i) {
	    case 0:
		    m_result_part = r.getParameterValues(GetItemInfo.HEADER_BINDING_NAME);
		    i_m_result[i] = new String[m_result_part.length];
		    break;
  	    case 1:
		    m_result_part = r.getParameterValues(GetItemInfo.HEADER_BINDING_VALUE);
		    i_m_result[i] = new String[m_result_part.length];
		    break;
	    case 2:
		    m_result_part = r.getParameterValues(GetItemInfo.ITEM_BINDING_NAME);
		    i_m_result[i] = new String[m_result_part.length];
		    break;
	    case 3:
		    m_result_part = r.getParameterValues(GetItemInfo.ITEM_BINDING_VALUE);
		    i_m_result[i] = new String[m_result_part.length];
		    break;
	    default:
		    m_result_part = new String[0];
          }
          for (int j = 0; j < m_result_part.length; j++){
	    i_m_result[i][j] = m_result_part[j];
          }
        }
      }
      return i_m_result;
    }//private String[][] getItemInfo(...

  }//public class ItemInfo

  private String[] getCondAccInfo(String documentId, String itemId)
    throws Exception {
    
    ItemInfo itemInfo = new ItemInfo();
    String rueck[] = itemInfo.getCondAccInfo(documentId, itemId);

    return rueck;
  }

  private String[] getVarcondInfo(String documentId, String itemId)
    throws Exception {
    
    ItemInfo itemInfo = new ItemInfo();
    String rueck[] = itemInfo.getVarcondInfo(documentId, itemId);

    return rueck;
  }

  private String[][] getInfo(String documentId, String itemId)
    throws Exception {
    
    ItemInfo itemInfo = new ItemInfo();
    String rueck[][] = itemInfo.getItemInfo(documentId, itemId);

    return rueck;
  }

  private String getProductId(String documentId, String itemId)
    throws Exception {

    ItemInfo itemInfo = new ItemInfo();
    String rueck = itemInfo.getProductId(documentId, itemId);

    return rueck;
  }//private String getProductId(...

  private String getProductDescription(String documentId, String itemId)
    throws Exception {

    ItemInfo itemInfo = new ItemInfo();
    String rueck = itemInfo.getProductDescription(documentId, itemId);

    return rueck;
  }//private String getProductDescription(...

  private String getPricingTimeStamp(String documentId, String itemId)
    throws Exception {

    ItemInfo itemInfo = new ItemInfo();
    String rueck = itemInfo.getPricingTimeStamp(documentId, itemId);

    return rueck;
  }//private String getPricingTimeStamp(...

%>