<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/sap-spe.tld"    prefix="spe" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/isa" prefix="isa"%>
<%@ page import = "com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionSelectionPannel"%>
<%@ page import = "com.sap.ipc.webui.conditionmasterdata.model.SelectionField"%>
<%@ include file="/appbase/jscript/calendar.js.inc.jsp"%>
<html>
<head>
	<link 
		rel="stylesheet" 
		type="text/css" 
		href="./styles/spe.css">
	<link 
		rel="stylesheet" 
		type="text/css" 
		href="./styles/show.css">
	<title>
		Condition Master Data Display
	</title>
</head>
<body>
<%--
<h1>
	Selection Screen Condition Master Data Display 
</h1>
--%>
<br>
<%
	javax.servlet.http.HttpSession mySession = request.getSession();
	InitialiseConditionSelectionPannel selectPannel;
	SelectionField selectField;
	selectPannel = (InitialiseConditionSelectionPannel) mySession.getAttribute(InitialiseConditionSelectionPannel.SessionAttributeName);

%>

<html:form 
	action="/convertSelectionToInt">
	
<%--
  Include the JSP for (error) message handing
--%>
<%@include file="../MessagePanel.jsp" %>		
	
<table>
<colgroup>
	<col 
		width="150">
	<col 
		width="250">
	<col 
		width="50">
	<col 
		width="200">
</colgroup>
<% 
    
    boolean firstField = true;
	selectPannel.resetEnumeration();
	while (selectPannel.hasMoreElements()) {
		out.print("<tr>");
		if ( firstField ) {
			// first add the max. hit input field because this is handled different
			selectField = selectPannel.getMaxRecordsToSelect();
			selectPannel.resetEnumeration();
			firstField = false;
		}	
		else {		
			selectField = (SelectionField) selectPannel.nextElement();
		}	
		// out.print("<br>");
		out.print("<td>");
		out.print(selectField.getFieldLabel());
		out.print("</td><td>");
		out.print("<input name=\"" + selectField.getFieldName() + "\""); 
		out.print(" id=\"" + selectField.getFieldName() + "\" "); 		
		// check if we can prefill the input field
		if (selectField.getFieldValue() != null) {
			out.print(" value=\"" + selectField.getFieldValue() + "\"");
		}
		out.print("	type=\"text\" size=\"30\" >");

		// check if this is a date input field
		if (selectField.getFieldType() != null && selectField.getFieldType().equalsIgnoreCase(SelectionField.fieldTypeDate)) {
			String dateFormat = "yyyy.MM.dd";
			// String calendarOpen = WebUtil.translate(pageContext,"appbase.calendar.open", null);
			String calendarOpen = "XYZ";

%>

<a 
	class="icon" 
	href="#" 
	tabindex="0" 
	onClick="openCalendarWindow('<%=dateFormat%>', '<%= selectField.getFieldName()%>');" 
	title="<isa:translate key="access.link" arg0="<%=calendarOpen%>" arg1=""/>
	">
	<%
		
	%>

	<img 
		src="<%=WebUtil.getMimeURL(pageContext, "/mimes/images/calendar.gif") %>
	" width="16" height="16" alt="
	<isa:translate 
		key="appbase.calendar.open"/>
	" border="0" class="display-image">
	<%
		
	%>

</a>
<%
	} else {
		// ending tag of the input field
	}

	out.print("</td>");
	// check if this field is a "from to " field
	if (selectField.isFromToField()) {
		out.print("<td>");

%>

<isa:translate 
	key="cmdd.SelInitForm.rangeTo"/>
<%
	out.print("</td>");
	out.print("<td>");
	out.print("<input name=\"" + selectField.getFieldNameTo() + "\"");
	// check if we can prefill the input field
	if (selectField.getFieldValueTo() != null) {
		out.print(" value=\"" + selectField.getFieldValueTo() + "\"");
	}
	out.print("	type=\"text\" size=\"30\" >");
	out.print("</td>");
}
	out.print("</tr>");
}

%>

<table>
<br>
<br>
<input 
	name="submit" 
	type="submit" 
	class="BLUEButton" 
	value="<isa:translate key="cmdd.SelInitForm.button.runQry"/>
" >
<input 
	name="<%=InitialiseConditionSelectionPannel.FormButtonClearQuery%>" 
	type="submit" 
	class="BLUEButton" 
	value="<isa:translate key="cmdd.selForm.button.clearQry"/>
" >
<input 
	name="<%=InitialiseConditionSelectionPannel.FormButtonCancel%>" 
	type="submit" 
	class="BLUEButton" 
	value="<isa:translate key="cmdd.button.back"/>
" >
</p>
</html:form>
<br>
<br>
<%--
 Print out some notes about the expected date format
--%>
<isa:translate 
key="cmdd.SelInitForm.dateFormat"/>
</body>
</html>
