<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/sap-spe.tld"    prefix="spe" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/isa" prefix="isa"%>
<%@ page import = "com.sap.ipc.webui.conditionmasterdata.model.InitialiseConditionAnalyserPannel"%>
<%@ page import = "com.sap.ipc.webui.conditionmasterdata.model.SelectionField"%>
<html>
<head>
	<link 
		rel="stylesheet" 
		type="text/css" 
		href="./styles/spe.css">
	<link 
		rel="stylesheet" 
		type="text/css" 
		href="./styles/show.css">
	<title>
		Condition Master Data Display
	</title>
</head>
<body>
<%-- 
<h1>
	Initial Screen Condition Master Data Display
</h1>
--%>

<br>
<%
	javax.servlet.http.HttpSession mySession = request.getSession();
	InitialiseConditionAnalyserPannel initPannel;
	SelectionField usageField; 
	SelectionField applicationField;
	SelectionField maintGroupField;

	initPannel = (InitialiseConditionAnalyserPannel) mySession.getAttribute(InitialiseConditionAnalyserPannel.SessionAttributeName);
	usageField = initPannel.getUsageField();
	applicationField = initPannel.getApplicationField();
	maintGroupField = initPannel.getMaintGroupField();

	java.lang.String myApplication = initPannel.getApplication();
	java.lang.String myUsage = initPannel.getUsage();

%>

<html:form 
	action="/getConditionGroupfld">
<%--
  Include the JSP for (error) message handing
--%>
<%@include file="../MessagePanel.jsp" %>	
	
<table>
	<tr>
		<td><%=applicationField.getFieldLabel()%>
		</td>
		<td>
			<input 
				name="<%=applicationField.getFieldName()%>" 
				type="text" 
				size="30" 
				maxlength="30" 
				value=<%=applicationField.getFieldValue()%>
			>
		</td>
	</tr>
	<tr>
		<td><%=usageField.getFieldLabel()%>
		</td>
		<td>
			<input 
				name="<%=usageField.getFieldName()%>" 
				type="text" 
				size="30" 
				maxlength="30" 
				value=<%=usageField.getFieldValue()%>
			>
		</td>
	</tr>
	<tr>
		<td><%=maintGroupField.getFieldLabel()%>
		</td>
		<td>
			<input 
				name="<%=maintGroupField.getFieldName()%>" 
				type="text" 
				size="30" 
				maxlength="30" 
				value=<%=maintGroupField.getFieldValue()%>
			>
		</td>
	</tr>
</table>

<br>
<br>

<input 
	name="submit" 
	type="submit" 
	class="BLUEButton" 
	value="<isa:translate key="cmdd.InitForm.button.selScreen"/>" >

</p>
</html:form>
</body>
</html>
