<%--
/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
--%>


<%--
		 @author i026516
		 Show Inconsistency Details JSP Page
--%>
<%@ page import = "com.sap.ipc.webui.conditionmasterdata.model.*"%>

<%@ page import = "java.util.*"%>
<%@ taglib uri="/isa" prefix="isa"%>

<jsp:useBean id="ConditionRecords" scope="session" type="com.sap.ipc.webui.conditionmasterdata.model.ConditionRecord[]"></

jsp:useBean>
</jsp:useBean>


<HTML>
  <HEAD>
 
  	<link 
		rel="stylesheet" 
		type="text/css" 
		href="../../styles/spe.css">
    <LINK 
	    rel="stylesheet" 
	    type="text/css"
		href="../../styles/show.css">
   
    <TITLE> <isa:translate key="cmdd.CondShow.errorDetailsHeader"/> </TITLE>
  </HEAD>

 <BODY>

<br>
	<h3>
		<B><center> <isa:translate key="cmdd.CondShow.errorDetailsHeader"/> </center> </B>
	</h3>

	<%

		String count = request.getParameter("countVar");
		Iterator errorDetails;
		Iterator details;
		Vector errorDetail = new Vector();
		String	error;
		int index = Integer.parseInt(count);
		ConditionRecord conditionRecord = ConditionRecords[index];
		errorDetails = conditionRecord.getInconsistencyDetails().iterator();
	%>
	<br><br>
	<TABLE  BORDER="1" CELLPADDING="0" CELLSPACING="0"  WIDTH=100%>

	<% 
//				for(int i = 0; i < errorDetails.length; i++){
				while(errorDetails.hasNext()){
					errorDetail = (Vector)errorDetails.next();
					details = errorDetail.iterator();
					
					%><TR><TD><B><%
					while(details.hasNext()){
						error = (String)details.next();
						if (error.startsWith("cmdd.CondShow")){
							%><isa:translate key="<%=error%>"/> <%
						}else{
							%><%=error%> <%
						}
					}
					%></B></TD></TR>
	<%			}
	%>
	</TABLE>
 </BODY>
</HTML>