<%--
/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
--%>

<%@ page contentType="text/html"%>
<%@ page pageEncoding="UTF-8"%>


<%--
	@author i032431
    Show Condition Master JSP Page
--%>

<%@ page import = "com.sap.ipc.webui.conditionmasterdata.model.*"%>

<%@ page import = "java.util.*"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/isa" prefix="isa"%>


<jsp:useBean id="ConditionRecordBluePrintStruct" scope="session" type="java.lang.String[]"></jsp:useBean>
<jsp:useBean id="DisplayBackButton" scope="session" type="java.lang.String"></jsp:useBean>
<jsp:useBean id="ErrorMessages" scope="session" type="java.lang.String[]"></jsp:useBean>
<jsp:useBean id="NoOfRecordsSelected" scope="session" type="java.lang.Integer"></jsp:useBean>

<jsp:useBean id="ConditionRecords" scope="session" type="com.sap.ipc.webui.conditionmasterdata.model.ConditionRecord[]"></jsp:useBean>



<%--
      java script which pop up to show the jsp page ShowScales  
--%>
<script type="text/JavaScript">

	function pop_me_up2(pURL,name,features){
		new_window = window.showModalDialog(pURL,'dialogHeight: 300px; dialogWidth: 400px; dialogTop: 338px; dialogLeft: 300px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: Yes;');
	}
</script>



<HTML>
  <HEAD>
  	<link 
		rel="stylesheet" 
		type="text/css" 
		href="<%=request.getContextPath()%>/styles/spe.css">
    <LINK 
	    rel="stylesheet" 
	    type="text/css"
		href="<%=request.getContextPath()%>/styles/show.css">
   
     <TITLE>
		<isa:translate key="cmdd.CondShow.cndRecdisp"/>
    </TITLE>
  </HEAD>

 <BODY>
 <br>
	<h3>
		<B><center><isa:translate key="cmdd.CondShow.cndRecdisp"/> </center> </B>
	</h3>
 <html:form 
	action="/ShowConditionRecord" > 
	 
<% 
		int numPages = 0;
		int lcount = 0;
		int totalCols = 0;
		int increment = 1;
		int numRows = 0;
		int numRecordsPerPage = 15;
		int totalNoOfPages = 0;
		boolean inconsistentRecord = false;
 %>
<%
String columnName = "";
String page1 = request.getParameter("lindex");
String startIndexString = request.getParameter("startIndex");
//out.println("startIndexString" + startIndexString);
if(startIndexString == null) {
startIndexString = "0";
}
if(page1 == null) {
page1 = "1";
}
int startIndex = Integer.parseInt(startIndexString); 
int pindex = Integer.parseInt(page1); 
	   String fieldName=null;
//	try{
totalCols = 1;
	  if (ConditionRecords[0].getNumberOfRecordReturned() != null){%>
 <TABLE LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 BORDER=0 CELLPADDING=0 CELLSPACING=0>
 <TR><TD>
 <TABLE LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" BORDER="1" CELLPADDING="0" CELLSPACING="0">
     <TR>
<%

//	   while( ConditionRecordBluePrintStruct.hasNext())
	   for(int bluePrintIndex = 0; bluePrintIndex < ConditionRecordBluePrintStruct.length ; bluePrintIndex ++) 	
	   {


	    //fieldName = (String) ConditionRecordBluePrintStruct.next();
              fieldName = ConditionRecordBluePrintStruct[bluePrintIndex];	
              
		      if (fieldName.equalsIgnoreCase("Status") || fieldName.equalsIgnoreCase("Scale Indicator"))
					continue; %>

	  		  <TD> <B><%=fieldName %></B></TD>
			<%
			 }
			%>
		   <TD> <B><isa:translate key="cmdd.CondShow.status"/></B></TD>
		   <TD> <B><isa:translate key="cmdd.CondShow.scaleIndicator"/></B></TD>
		   <TD> <B><isa:translate key="cmdd.CondShow.errorDetails"/></B></TD>
	  </TR>

	  <%
		String fieldValue="";
		numRows = ConditionRecords.length;
		numPages = numRows /numRecordsPerPage ; 
		int remain = numRows % numRecordsPerPage ;
		totalNoOfPages = numRows % numRecordsPerPage ;
		if(remain != 0){
		numPages = numPages +1 ;
		}
		if((startIndex + numRecordsPerPage) <= numRows) {
			increment = startIndex + numRecordsPerPage ;
			}else{
			if (remain == 0){
					increment = startIndex + numRecordsPerPage ;
				}else{
					increment = startIndex + remain;
				     }
				}
		Iterator condRecForDisplay;
		ConditionRecord conditionRecord = new ConditionRecord();
	for(lcount = startIndex; lcount < increment; lcount++) {
	//	for(int index =0; index< ConditionRecords.length ;index++)
	//	{
    
	  %><TR><%
			conditionRecord = ConditionRecords[lcount];
			
			if (conditionRecord.isConditionRecordInConsistent()){
				if (!inconsistentRecord)
					inconsistentRecord = true;
				%><TR bgcolor="YELLOW"><%
			}else{
				%><TR><%				
			}

			if (conditionRecord != null){
 				condRecForDisplay = conditionRecord.getConditionRecord().iterator();

				while(condRecForDisplay.hasNext()){
					fieldValue = (String)condRecForDisplay.next();
					
					if (fieldValue == null){
						fieldValue = "";
						if (conditionRecord.isConditionRecordInConsistent()){
							%><TD bgcolor="YELLOW">&nbsp</TD><%
						}else{
							%><TD bgcolor="#COCOCO">&nbsp</TD><%
						}
					}else{
					   if (fieldValue == "true"){
						  fieldValue = (String)condRecForDisplay.next();	
						  
						  if (fieldValue == null){
								fieldValue = "";
								if (conditionRecord.isConditionRecordInConsistent()){
									%><TD bgcolor="YELLOW">&nbsp</TD><%
								}else{								
									%><TD bgcolor="#COCOCO">&nbsp</TD><%
								}
						  }else{
							  %><TD bgcolor="RED" nowrap><%=fieldValue%></TD><%
						  }
					   }else if (fieldValue == "false"){
							fieldValue = (String)condRecForDisplay.next();	

							if (fieldValue == null){
							  fieldValue = "";
							  if (conditionRecord.isConditionRecordInConsistent()){
								  %><TD bgcolor="YELLOW">&nbsp</TD><%
							  }else{							  
								  %><TD bgcolor="#COCOCO">&nbsp</TD><%
							  }
							}else{
								%><TD nowrap><%=fieldValue%></TD><%
							}
					   }else{
							if (fieldValue == null){
								  fieldValue = "";
	   							  if (conditionRecord.isConditionRecordInConsistent()){
								  		%><TD bgcolor="YELLOW">&nbsp</TD><%
							      }else{								  
								  		%><TD bgcolor="#COCOCO">&nbsp</TD><%
	   							  }
							}else{
								%><TD nowrap><%=fieldValue%></TD><%
							}
					   }
					}
				}
				if (conditionRecord.getReleaseStatus().equals(" ")){
					%><TD><img src="<isa:mimeURL name="/mimes/images/s_s_ledg.gif" />" width="16" height="15" border=0 alt=" "></TD><%
				}else{
					%><TD><img src="<isa:mimeURL name="/mimes/images/s_s_ledr.gif" />" width="16" height="15" border=0 alt=" "></TD><%
				}
				if (conditionRecord.getScaleExist()){
					%><TD>
					 <a href="javascript:pop_me_up2('<%=request.getContextPath()%>/ui/conditionmasterdata/ShowScales.jsp?countVar=<%=lcount%>','776550','width=776,height=550,scrollbars=no,toolbars=no,resizable=yes');">
					<img src="<isa:mimeURL name="/mimes/images/Scales.gif" />" width="16" height="15" border=0 alt=" "></a>
				</TD><% 
				}else{
					%><TD>&nbsp</TD><%
				}
				
				if (conditionRecord.isConditionRecordInConsistent()){
					%><TD>
					 <a href="javascript:pop_me_up2('<%=request.getContextPath()%>/ui/conditionmasterdata/ShowInconsistencyDetails.jsp?countVar=<%=lcount%>','776550','width=776,height=550,scrollbars=no,toolbars=no,resizable=yes');">
					<img src="<isa:mimeURL name="/mimes/images/s_m_erro.gif" />" width="16" height="15" border=0 alt=" "></a>
				</TD><% 
				}else{
					%><TD>&nbsp</TD><%
				}
				
			}
	  %><TR><%
			
		}
%> </TABLE> </TD></TR><%
	}else{
	
		if (ErrorMessages != null){
			
			%><BR><%
			
			if ( !(ErrorMessages[0].equalsIgnoreCase("No Errors"))){
			
				for(int errIndex = 0; errIndex < ErrorMessages.length; errIndex++){
					%>
					   <BR><B><CENTER><%=ErrorMessages[errIndex]%></CENTER></B>
					<%
				}
			}else {
			%>
			<BR><BR><B><CENTER>
					<isa:translate key="cmdd.CondShow.noCondRecs"/></CENTER></B>
			<%     }
		}
    }
	  %><TR><TD><BR><BR></TD><TR><TD>
	  
<TABLE width =100% border=0 cellpadding=0 cellspacing=0>
<TR>

<%if (DisplayBackButton.equalsIgnoreCase("true")){%>

<% if ( numRows <= 0) { %>
<TD align=center nowrap> 
<input 
	name="org.apache.struts.taglib.html.CANCEL"
	type="submit" 
	class="BLUEButton" 
	value="<isa:translate key="cmdd.button.back"/>" >&nbsp;</TD>
<% } %>

<% if ( numRows > 0) { %>
<TD align=right nowrap> 
<input 
	name="org.apache.struts.taglib.html.CANCEL"
	type="submit" 
	class="BLUEButton" 
	value="<isa:translate key="cmdd.button.back"/>" >&nbsp;</TD>
<% } %>

<%}
if (numRows > 0){	
	%>
<TD align=right nowrap> 
<B><isa:translate key="cmdd.CondShow.page"/><B>
<% 
    if(startIndex + numRecordsPerPage < numRows)
	{
%>
	<%= " " + (pindex) %> <B> <isa:translate key="cmdd.CondShow.of"/><B> <%= numPages  %>
<%	}
	else{
%>
	<%= " " + (numPages) %> <B> of<B> <%= numPages %>
<%	
	}
%>
<%	if(startIndex != 0) 
	{
%> 
		<a href="<%=request.getContextPath()%>/ui/conditionmasterdata/ShowConditions.jsp?startIndex=<%=startIndex-numRecordsPerPage%>&lindex=<%=(pindex-1)%>"><img src="<isa:mimeURL name="/mimes/images/backward.gif" />" width="20" height="15" border=0 alt=" "></a>
<%	}
%>
	<%	increment += numRecordsPerPage ;%> 
<%	if(startIndex + numRecordsPerPage < numRows)
	{
%> 
		<a href="<%=request.getContextPath()%>/ui/conditionmasterdata/ShowConditions.jsp?startIndex=<%=startIndex+numRecordsPerPage %>&lindex=<%=(pindex+1)%>"><img src="<isa:mimeURL name="/mimes/images/forward.gif" />" width="20" height="15" border=0 alt=" "></a>
<%
	}
 }
%>
</TR>
</TABLE> </TD></TR>
</TABLE>

<%
	if (inconsistentRecord) {
%>
		<BR>
		<TR><B><isa:translate key="cmdd.CondShow.inconsistentRecord"/></B></TR>
<%
	}
%>

<%
	if (numRows > 0 && NoOfRecordsSelected.intValue() > numRows) {
%>
		<BR>
		<TR><B><isa:translate key="cmdd.CondShow.displaying"/><%=numRows%> <isa:translate key="cmdd.CondShow.of"/> <%=NoOfRecordsSelected.intValue()%> <isa:translate key="cmdd.CondShow.records"/></B></TR>
<%
	}
%>


</html:form>

 </BODY>
</HTML>