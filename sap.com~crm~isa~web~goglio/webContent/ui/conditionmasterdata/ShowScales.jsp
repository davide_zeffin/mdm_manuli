<%--
/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
--%>


<%--
		 @author i032431
		 Show Scales JSP Page
--%>
<%@ page import = "com.sap.ipc.webui.conditionmasterdata.model.*"%>

<%@ page import = "java.util.*"%>
<%@ taglib uri="/isa" prefix="isa"%>

<jsp:useBean id="ConditionRecords" scope="session" type="com.sap.ipc.webui.conditionmasterdata.model.ConditionRecord[]"></

jsp:useBean>
</jsp:useBean>


<HTML>
  <HEAD>
 
  	<link 
		rel="stylesheet" 
		type="text/css" 
		href="../../styles/spe.css">
    <LINK 
	    rel="stylesheet" 
	    type="text/css"
		href="../../styles/show.css">
   
    <TITLE> <isa:translate key="cmdd.CondShow.scaledisp"/> </TITLE>
  </HEAD>

 <BODY>

<br>
	<h3>
		<B><center> <isa:translate key="cmdd.CondShow.scaledisp"/> </center> </B>
	</h3>


		
	
  <TABLE  BORDER="0" CELLPADDING="0" CELLSPACING="0"  WIDTH=100%>
<%

		String count = request.getParameter("countVar");
		int index = Integer.parseInt(count);
		//String scaleHeaderFieldName;
		String pricingScaleDetailFieldName;
		String pricingScaleDetailFieldValue;
		//Iterator scaleHeaForDisplay;
		Vector pricingScaleDet = new Vector();
		Iterator pricingScaleDetail;
		Iterator pricingScale;


		ConditionRecord conditionRecord = ConditionRecords[index];
//		scaleHeaForDisplay = conditionRecord.getScaleHeaderBluePrintSequence().iterator();
//		scaleHeaderFieldName = (String)scaleHeaForDisplay.next();
				%>

    <TR><TD>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp </TD>
    <%
		if (conditionRecord.getScaleBaseTypes() != null){
    %>
	    <TD align="left"><B><isa:translate key="cmdd.CondShow.scaleBaseType"/></B>&nbsp :&nbsp <%=conditionRecord.getScaleBaseTypes()%> </TD>
	 <% } else { %>
	   		<TD align="left"><B><isa:translate key="cmdd.CondShow.scaleBaseType"/></B>&nbsp :&nbsp</TD>	 
	   <%}%>
	    

	<% //     <%scaleHeaderFieldName = (String)scaleHeaForDisplay.next();
		if (conditionRecord.getScaleType() != null){
	 %>  
	   		<TD align="left"><B><isa:translate key="cmdd.CondShow.scaleType"/></B>&nbsp :&nbsp<%=conditionRecord.getScaleType() %></TD>
	 <% } else { %>
	   		<TD align="left"><B><isa:translate key="cmdd.CondShow.scaleType"/></B>&nbsp :&nbsp</TD>	 
	   <%}%>
	</TR></TABLE><br><br>
	<TABLE  BORDER="1" CELLPADDING="0" CELLSPACING="0"  WIDTH=100%>
	<%
				pricingScaleDetail = conditionRecord.getPricingScaleDetailsBluePrintSequence().iterator();

				%><TR><%
				while(pricingScaleDetail.hasNext()){
					pricingScaleDetailFieldName = (String)pricingScaleDetail.next();
					%><TD><isa:translate key="<%=pricingScaleDetailFieldName%>"/></TD><%
				} %>

				</TR>
				 <%
				pricingScale = conditionRecord.getScaleDetails().iterator();

				while(pricingScale.hasNext()){
					pricingScaleDet = (Vector)pricingScale.next();

					pricingScaleDetail = pricingScaleDet.iterator();


					%><TR><%
				
					while(pricingScaleDetail.hasNext()){
						pricingScaleDetailFieldValue = (String)pricingScaleDetail.next();
						if (pricingScaleDetailFieldValue != null){
							%> <TD> <%=pricingScaleDetailFieldValue%> </TD> <%
						}else{
							%><TD>&nbsp</TD> <%
						}

					
					}

					%></TR><%
				}
	%>
</TABLE>
</BODY>
</HTML>