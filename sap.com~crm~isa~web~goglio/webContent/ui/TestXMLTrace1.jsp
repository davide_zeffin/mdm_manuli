<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
<TITLE>JBuilder Project webui.jpx</TITLE>
</HEAD>
<BODY>

<%
    String contextPath = request.getContextPath();
    response.setHeader("pragma", "no-cache");
    String actionUrl = contextPath+"/servlet/com.sap.ipc.webui.pricing.servlet.ShowAdvancedXMLPricingAnalysis";
%>

<h2>Attach to running session</h2>
<FORM  action=<%=actionUrl%>
       method=post>

<table rules=none bgColor=#8fbc8b>
  <tr>
	<td> IPC-Server </td>
	<td> <input type="text" name="server" value="10.17.0.64"><br> </td>
  </tr>

  <tr>
	<td> IPC-Port </td>
	<td> <input type="text" name="port" value="4216"><br> </td>
  </tr>

  <tr>
	<td> IPC-sessionId </td>
	<td> <input type="text" name="sessionId" value="182"><br> </td>
  </tr>

  <tr>
	<td> documentId </td>
	<td> <input type="text" name="documentId" size ="35" value=""><br> </td>
  </tr>

  <tr>
	<td> itemId </td>
	<td> <input type="text" name="itemId"   size ="35" value=""> </td>
  </tr>

  <tr>
	<td> language </td>
	<td> <input type="text" name="language" value="D"><br> </td>
  </tr>

  <tr>
	<td> application </td>
    <td> <input type="text" name="application" value="CRM"><br> </td>
  </tr>

  <tr>
	<td> forwarding to extended display </td>
	<td> <input type="text" name="forwarding" value="false"><br> </td>
  </tr>

  <tr>
	<td> trace ShowAdvancedXMLPricingAnalysis </td>
	<td> <input type="text" name="trace" value="false"><br> </td>
  </tr>

  <tr>
	<td> save XML String to File </td>
	<td> <input type="text" name="save" value="false"><br> </td>
  </tr>
</table>
  <input type=submit value="attach">
</FORM>

</BODY>
</HTML>
