<%@ page language="java" contentType="text/html" %>

<%@ page import="java.net.URLEncoder" %>

<%@ page import="com.sap.spc.remote.client.ClientSupport" %>
<%@ page import="com.sap.sxe.socket.client.ClientException" %>
<%@ page import="com.sap.sxe.socket.client.ServerResponse" %>

<%@ page import="com.sap.sxe.socket.shared.ErrorCodes" %>

<%@ page import="com.sap.spc.remote.shared.command.system.*" %>
<%@ page import="com.sap.spc.remote.client.object.IPCException" %>
<%@ page import="com.sap.ipc.webui.pricing.UIConstants" %>

<%@ page import="java.util.TreeSet"%>
<%@ page import="java.util.Iterator"%>

<%!
  ClientSupport m_client;
  String[][]    allInformation;
  String[][]    allInformationTwo;
  int           allInfoLength;
  int           allInformationLength;

  String[]      conditionTables;
  String[]      conditionTypes;
  String[]      conditionTypeBuffers;
  
  String        getNoInf;
  String        conditionTable;
  String        conditionType;

  String        server;
  String        port;
  String        client;
  String        view;
%>

<%
  allInformationLength = 10;

  response.setHeader("pragma", "no-cache");
  String contextPath = request.getContextPath();
  String current     = "/ui/BufferTree.jsp";

  server             = request.getParameter("server");
  port               = request.getParameter("port");
  client             = request.getParameter("client");
  view               = request.getParameter("view"); 

  getNoInf           = request.getParameter("noInf");
  conditionTable     = request.getParameter("conditionTable");
  conditionType      = request.getParameter("conditionType");
%>

<%-- *** ARRAY F�LLEN *** --%>
  <%
    //BEIM ERSTEN AUFRUF ARRY MIT REMOTE KOMMANDOS F�LLEN
    break1:
    
    if (getNoInf == null) {

      String encoding = "UnicodeLittle";
      m_client = new ClientSupport(server, Integer.parseInt(port), encoding, new Boolean(false));
      if (client == null) {
        client = "700";
      }
      
      createSession(client);

      conditionTables = getConditionTables();
      if (conditionTables == null) {
        view = "500";
        break break1;
        //throw new ClientException ("999", "getConditionTables()");
      }//if

      String[] conditionTablesTmp = new String[conditionTables.length / 7];
      int counter = 0;
      for (int i = 0; i < conditionTables.length; i = i + 7) {
        conditionTablesTmp[counter] = conditionTables[i];
        counter++;
      }//for

      counter = 0;
      String[] conditionTypesTmp;
      for (int i = 0; i < conditionTablesTmp.length; i++) {
        counter++;
      
        conditionTypesTmp = getConditionTypesForConditionTable(conditionTablesTmp[i]);

        if (conditionTypesTmp != null) {
        
          String[] buffersForConditionTypeTmp;
          for (int j = 0; j < conditionTypesTmp.length; j = j + 2) {
            counter++;
          
            buffersForConditionTypeTmp = getBuffersForConditionType(conditionTablesTmp[i], conditionTypesTmp[j]);

            if (buffersForConditionTypeTmp != null) {
              for (int k = 0; k < buffersForConditionTypeTmp.length;) {
                if (buffersForConditionTypeTmp[k].equals("ConditionRecordPreStepBuffer")) {
                  k = k + 4;
                  counter++;
                }//if
                else if (buffersForConditionTypeTmp[k].equals("ConditionDynamicRecordBuffer")) {
                  k = k + 4;
                  counter++;
                }//else if
                else {
                  k++;
                  counter++;
                }//else
              }//for k
            }//if
          }//for j
        }//if
      
      }//for i
      allInfoLength = counter;

      String[][] allInfoTmp = new String[allInfoLength][allInformationLength];
      counter = 0;
      for (int i = 0; i < conditionTables.length; i = i + 7) {
        allInfoTmp[counter][0] = conditionTables[i];                           //value
        allInfoTmp[counter][1] = "parent";                                     //node-specification
        allInfoTmp[counter][2] = null;                                         //node-specification
        allInfoTmp[counter][3] = conditionTables[i+1];                         //results PreStep all >0 / 0
        allInfoTmp[counter][4] = "n";                                          //node-expanded y / n
        allInfoTmp[counter][5] = conditionTables[i+2];                         //results PreStep +
        allInfoTmp[counter][6] = conditionTables[i+3];                         //results PreStep -
        allInfoTmp[counter][7] = conditionTables[i+4];                         //results DynCond all >0 / 0
        allInfoTmp[counter][8] = conditionTables[i+5];                         //results PreStep +
        allInfoTmp[counter][9] = conditionTables[i+6];                         //results PreStep -
        counter++;

        conditionTypesTmp = getConditionTypesForConditionTable(conditionTables[i]);
        if (conditionTypesTmp != null) {
        
          String[] buffersForConditionTypeTmp;

          for (int j = 0; j < conditionTypesTmp.length; j = j + 2) {
            allInfoTmp[counter][0] = conditionTypesTmp[j];
            allInfoTmp[counter][1] = "child";
            allInfoTmp[counter][2] = conditionTables[i];
            allInfoTmp[counter][3] = conditionTypesTmp[j+1];                   //true || false
            allInfoTmp[counter][4] = "n";
            int counterSave = counter;
            counter++;
          
            buffersForConditionTypeTmp = getBuffersForConditionType(conditionTables[i], conditionTypesTmp[j]);
            if (buffersForConditionTypeTmp != null) {
            
              for (int k = 0; k < buffersForConditionTypeTmp.length;) {
                if (buffersForConditionTypeTmp[k].equals("ConditionRecordPreStepBuffer")) {
                  allInfoTmp[counter][0] = buffersForConditionTypeTmp[k];
                  allInfoTmp[counter][1] = conditionTables[i];
                  allInfoTmp[counter][2] = conditionTypesTmp[j];
                  //if (allInfoTmp[counterSave][3].equals("false") != true) { //change because of incorrect count of DynmRecPreStepBuf
                    allInfoTmp[counterSave][3] = buffersForConditionTypeTmp[k+1];     //results PreStep all >0 / 0
                    allInfoTmp[counterSave][5] = buffersForConditionTypeTmp[k+2];     //results PreStep +
                    allInfoTmp[counterSave][6] = buffersForConditionTypeTmp[k+3];     //results PreStep -
                  //}//if
                  k = k + 4;
                  counter++;
                }//if
                else if (buffersForConditionTypeTmp[k].equals("ConditionDynamicRecordBuffer")) { //change because of incorrect count of DynmRecPreStepBuf
                  allInfoTmp[counter][0] = buffersForConditionTypeTmp[k];
                  allInfoTmp[counter][1] = conditionTables[i];
                  allInfoTmp[counter][2] = conditionTypesTmp[j];
                  //if (allInfoTmp[counterSave][3].equals("false") != true) {
                    allInfoTmp[counterSave][7] = buffersForConditionTypeTmp[k+1];     //results DynCond all >0 / 0
                    allInfoTmp[counterSave][8] = buffersForConditionTypeTmp[k+2];     //results PreStep +
                    allInfoTmp[counterSave][9] = buffersForConditionTypeTmp[k+3];     //results PreStep -
                  //}//if
                  k = k + 4;
                  counter++;
                }//else if
                else {
                  allInfoTmp[counter][0] = buffersForConditionTypeTmp[k];
                  allInfoTmp[counter][1] = conditionTables[i];
                  allInfoTmp[counter][2] = conditionTypesTmp[j];
                  k++;
                  counter++;
                }//else
              }//for k
            }//if
          }//for j
        }//if
      
      }//for i

      allInformation = allInfoTmp;

      if (view.equals("2")) {
        TreeSet treeSet = new TreeSet();
        for (int i = 0; i < allInformation.length; i++) {
          if (allInformation[i][1].equals("child")) {
            if (treeSet.isEmpty()) {
              treeSet.add(allInformation[i][0]);
            }//if
            else {
              if (treeSet.contains(allInformation[i][0]) != true) {
                treeSet.add(allInformation[i][0]);
              }//if
            }//else       
          }//if          
        }//for

        counter = 0;
        for (Iterator it = treeSet.iterator(); it.hasNext();) {
          String t = (String) it.next();
          counter++;

          for (int j = 0; j < allInformation.length; j++) {
            if (allInformation[j][1].equals("parent") && allInformation[j][3].equals("0") != true) {
              for (int k = 0; k < allInformation.length; k++) {
                if (allInformation[k][1].equals("child") && allInformation[k][2].equals(allInformation[j][0]) && allInformation[k][0].equals(t)) {
                  counter++;
                  for (int o = 0; o < allInformation.length; o++) {
                    if (allInformation[o][2] != null && allInformation[o][2].equals(t) && allInformation[o][1].equals(allInformation[j][0])) {
                      counter++;
                    }//if
                  }//for
                }
              }//for
            }//if
          }//for
        }//for

        String[][] allInfoTmpTwo = new String[counter][allInformationLength];
        counter = 0;
        for (Iterator it = treeSet.iterator(); it.hasNext();) {
          String t = (String) it.next();

          allInfoTmpTwo[counter][0] = t;
          allInfoTmpTwo[counter][1] = "parent";
          allInfoTmpTwo[counter][3] = "n";
          allInfoTmpTwo[counter][4] = new Integer(0).toString();
          allInfoTmpTwo[counter][5] = new Integer(0).toString();
          allInfoTmpTwo[counter][6] = new Integer(0).toString();
          allInfoTmpTwo[counter][7] = new Integer(0).toString();
          allInfoTmpTwo[counter][8] = new Integer(0).toString();
          allInfoTmpTwo[counter][9] = new Integer(0).toString();
          int counterSaveTwo = counter;
          counter++;

          for (int l = 0; l < allInformation.length; l++) {
            if (allInformation[l][1].equals("parent") && allInformation[l][3].equals("0") != true) {
              for (int m = 0; m < allInformation.length; m++) {
                if (allInformation[m][1].equals("child") && allInformation[m][2].equals(allInformation[l][0]) && allInformation[m][0].equals(t)){
                  allInfoTmpTwo[counter][0] = allInformation[l][0];
                  allInfoTmpTwo[counter][1] = "child";
                  allInfoTmpTwo[counter][2] = t;
                  allInfoTmpTwo[counter][3] = "n";

                  if (allInformation[m][3] != null && allInformation[m][3].equals(allInformation[m][6]) != true) {
                    int tmp = Integer.parseInt(allInformation[m][3]);
                    int tmpTwo = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][4]);
                    int tmpThree = tmp + tmpTwo;
                    allInfoTmpTwo[counterSaveTwo][4] = new Integer(tmpThree).toString();

                    int tmpFour = Integer.parseInt(allInformation[m][5]);
                    int tmpFive = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][5]);
                    int tmpSix = tmpFour + tmpFive;
                    allInfoTmpTwo[counterSaveTwo][5] = new Integer(tmpSix).toString();

                    int tmpSeven = Integer.parseInt(allInformation[m][6]);
                    int tmpEight = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][6]);
                    int tmpNine = tmpSeven + tmpEight;
                    allInfoTmpTwo[counterSaveTwo][6] = new Integer(tmpNine).toString();

                    int tmpTen = Integer.parseInt(allInformation[m][7]);
                    int tmpEleven = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][7]);
                    int tmpTwelve = tmpTen + tmpEleven;
                    allInfoTmpTwo[counterSaveTwo][7] = new Integer(tmpTwelve).toString();

                    int tmpThirteen = Integer.parseInt(allInformation[m][8]);
                    int tmpFourteen = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][8]);
                    int tmpFifteen = tmpThirteen + tmpFourteen;
                    allInfoTmpTwo[counterSaveTwo][8] = new Integer(tmpFifteen).toString();

                    int tmpSixteen = Integer.parseInt(allInformation[m][9]);
                    int tmpSeventeen = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][9]);
                    int tmpEighteen = tmpSixteen + tmpSeventeen;
                    allInfoTmpTwo[counterSaveTwo][9] = new Integer(tmpEighteen).toString();
                  
                  }//if
                  else if (allInformation[m][3] != null && allInformation[m][3].equals(allInformation[m][6])) { ////change because of incorrect count of DynmRecPreStepBuf
                    int tmp = Integer.parseInt(allInformation[m][3]);
                    int tmpTwo = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][4]);
                    int tmpThree = tmp + tmpTwo;
                    allInfoTmpTwo[counterSaveTwo][4] = new Integer(tmpThree).toString();

                    int tmpFour = Integer.parseInt(allInformation[m][5]);
                    int tmpFive = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][5]);
                    int tmpSix = tmpFour + tmpFive;
                    allInfoTmpTwo[counterSaveTwo][5] = new Integer(tmpSix).toString();

                    int tmpSeven = Integer.parseInt(allInformation[m][6]);
                    int tmpEight = Integer.parseInt(allInfoTmpTwo[counterSaveTwo][6]);
                    int tmpNine = tmpSeven + tmpEight;
                    allInfoTmpTwo[counterSaveTwo][6] = new Integer(tmpNine).toString();
                  }//else if
                  int counterSave = counter;
                  counter++;

                  for (int n = 0; n < allInformation.length; n++) {
                    if (allInformation[n][2] != null && allInformation[n][2].equals(t) && allInformation[n][1].equals(allInformation[l][0])) {
                      allInfoTmpTwo[counter][0] = allInformation[n][0];
                      allInfoTmpTwo[counter][1] = t;
                      allInfoTmpTwo[counter][2] = allInformation[l][0];

                      allInfoTmpTwo[counterSave][4] = allInformation[m][3];
                      allInfoTmpTwo[counterSave][5] = allInformation[m][5];
                      allInfoTmpTwo[counterSave][6] = allInformation[m][6];
                      allInfoTmpTwo[counterSave][7] = allInformation[m][7];
                      allInfoTmpTwo[counterSave][8] = allInformation[m][8];
                      allInfoTmpTwo[counterSave][9] = allInformation[m][9];
                      
                      counter++;
                    }//if
                  }//for
                }//if
              }//for
            }//if
          }//for
        }//for

        allInformationTwo = allInfoTmpTwo;
      }//if
       
    }//if

    //BEI JEDEM WEITEREN AUFRUF ARRAY AUS DEM SESSION-BEREICH BESORGEN
    else if (getNoInf != null) {
      if (view.equals("1")) {
        Object oTmp = session.getAttribute("valuesOne");

        allInformation = (String[][]) oTmp;
      }//if
      else if (view.equals("2")) {
        Object oTmp = session.getAttribute("valuesTwo");

        allInformationTwo = (String[][]) oTmp;
      }//else if
    }//else if
  %>

<%-- *** KNOTEN SETZEN *** --%>
  <%
    if (view.equals("1")) {
      if (conditionTable != null && conditionType == null) {
        for (int k = 0; k < allInformation.length; k++) {
          if (allInformation[k][1].equals("parent") && allInformation[k][0].equals(conditionTable)) {
            if (allInformation[k][4].equals("n")) {
              allInformation[k][4] = "y";
            }//if
            else if (allInformation[k][4].equals("y")) {
              allInformation[k][4] = "n";
            }//else if
          }//if
        }//for k
      }//if

      else if (conditionTable != null && conditionType != null) {
        for (int k = 0; k < allInformation.length; k++) {
          if (allInformation[k][1].equals("child") && allInformation[k][0].equals(conditionType) && allInformation[k][2].equals(conditionTable)) {
            if (allInformation[k][4].equals("n")) {
              allInformation[k][4] = "y";
            }//if
            else if (allInformation[k][4].equals("y")) {
              allInformation[k][4] = "n";
            }//else if
          }//if
        }//for
      }//else if
    }//if
    
    else if (view.equals("2")) {
      if (conditionType != null && conditionTable == null) {
        for (int k = 0; k < allInformationTwo.length; k++) {
          if (allInformationTwo[k][1].equals("parent") && allInformationTwo[k][0].equals(conditionType)) {
            if (allInformationTwo[k][3].equals("n")) {
              allInformationTwo[k][3] = "y";
            }//if
            else if (allInformationTwo[k][3].equals("y")) {
              allInformationTwo[k][3] = "n";
            }//else if
          }//if
        }//for k
      }//if

      else if (conditionType != null && conditionTable != null) {
        for (int k = 0; k < allInformationTwo.length; k++) {
          if (allInformationTwo[k][1].equals("child") && allInformationTwo[k][0].equals(conditionTable) && allInformationTwo[k][2].equals(conditionType)) {
            if (allInformationTwo[k][3].equals("n")) {
              allInformationTwo[k][3] = "y";
            }//if
            else if (allInformationTwo[k][3].equals("y")) {
              allInformationTwo[k][3] = "n";
            }//else if
          }//if
        }//for
      }//else if
    }
  %>

<%-- *** AUSGABE *** --%>
<HTML>
  <HEAD>
    <LINK REL="stylesheet" TYPE="text/css" HREF="../styles/spe.css">
  <%
    if (view.equals("2")) {
  %>
    <TITLE>Condition Type-Table-Buffer</TITLE>
  <%
    }//if
    else if (view.equals("1")) {
  %>
    <TITLE>Condition Table-Type-Buffer</TITLE>
  <%
    }//else if
  %>
  </HEAD>

  <BODY CLASS="tabcontent" LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" TEXT="#000000" LINK="#424E91" VLINK="#7979BD" ALINK="#336633">

  <DIV CLASS="content"  WIDTH=100%>

  <%
    if (view.equals("2")) {

      String conditionBaseUrl = "http://"
                              + request.getServerName()
                              + ":"
                              + request.getServerPort()
                              + contextPath
			      + current
                              + "?"
                              + "view=2&"
                              ;

      if (conditionType == null && conditionTable == null || conditionType != null && conditionTable == null || conditionType != null && conditionTable != null) {
  
  %>

  <TABLE>
  <%                          
        for (int i = 0; i < allInformationTwo.length; i++ ) {
          if (allInformationTwo[i][1].equals("parent")) {
  %>
    <TR>
  <%
            StringBuffer conditionUrl = new StringBuffer(conditionBaseUrl);
            conditionUrl.append("noInf=a" + "&conditionType=" + allInformationTwo[i][0]);
            
            if (allInformationTwo[i][3].equals("n")) {
  %>
      <TD><IMG SRC="../mimes/images/table/dot_green.gif" BORDER=0></TD>
      <TD><A HREF="<%=conditionUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_close.gif" BORDER=0></A></TD>
      <TD><B><%=allInformationTwo[i][0]%></B> ConditionRecordPreStepBuffer (= <%=allInformationTwo[i][4]%> / + <%=allInformationTwo[i][5]%> / - <%=allInformationTwo[i][6]%>) DynamicConditionRecordBuffer (= <%=allInformationTwo[i][7]%> / + <%=allInformationTwo[i][8]%> / - <%=allInformationTwo[i][9]%>)</TD>

  <%
            }//if
            else if (allInformationTwo[i][3].equals("y")) {
  %>
      <TD><IMG SRC="../mimes/images/table/dot_green.gif" BORDER=0></TD>
      <TD><A HREF="<%=conditionUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_open.gif" BORDER=0></A></TD>
      <TD><B><%=allInformationTwo[i][0]%></B></TD>

    </TR>
    
    <TR>
      <TD></TD>
      <TD></TD>
      <TD>
        <TABLE>
  <%
    for (int l = 0; l < allInformationTwo.length; l++) {
      if (allInformationTwo[l][1].equals("child") && allInformationTwo[l][2].equals(allInformationTwo[i][0])) {
  %>
          <TR>
  <%
        StringBuffer conditionOneUrl = new StringBuffer(conditionBaseUrl);
        conditionOneUrl.append("noInf=a" + "&conditionType=" + allInformationTwo[i][0] + "&conditionTable=" + allInformationTwo[l][0]);
        
        if (allInformationTwo[l][3].equals("n")) {
          if (allInformationTwo[l][4] != null && allInformationTwo[l][4].equals(allInformationTwo[l][6])) { //***
  %>
            <TD><IMG SRC="../mimes/images/table/dot_red.gif" BORDER=0></TD>
            <TD><A HREF="<%=conditionOneUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_close.gif" BORDER=0></A></TD>
            <TD><B><%=allInformationTwo[l][0]%></B> ConditionRecordPreStepBuffer: (=<%=allInformationTwo[l][4]%> / + <%=allInformationTwo[l][5]%> / - <%=allInformationTwo[l][6]%>) DynamicConditionRecordBuffer: (=<%=allInformationTwo[l][7]%> / + <%=allInformationTwo[l][8]%> / - <%=allInformationTwo[l][9]%>)</TD>
<!--
ConditionRecordPreStepBuffer: (= 1 / + 0 / - 1) DynamicConditionRecordBuffer: (= 0 / + 0 / - 0)
-->
  <%
          }//if
          else if (allInformationTwo[l][4] != null && allInformationTwo[l][4].equals(allInformationTwo[l][6]) != true) { //***
  %>
            <TD><IMG SRC="../mimes/images/table/dot_green.gif" BORDER=0></TD>
            <TD><A HREF="<%=conditionOneUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_close.gif" BORDER=0></A></TD>
            <TD><B><%=allInformationTwo[l][0]%></B> ConditionRecordPreStepBuffer: (=<%=allInformationTwo[l][4]%> / + <%=allInformationTwo[l][5]%> / - <%=allInformationTwo[l][6]%>) DynamicConditionRecordBuffer: (=<%=allInformationTwo[l][7]%> / + <%=allInformationTwo[l][8]%> / - <%=allInformationTwo[l][9]%>)</TD>
  <%
         }//else if
        }//if
        else if (allInformationTwo[l][3].equals("y")) {
          if (allInformationTwo[l][4] != null && allInformationTwo[l][4].equals(allInformationTwo[l][6])) { //***
  %>
            <TD><IMG SRC="../mimes/images/table/dot_red.gif" BORDER=0></TD>
  <%
          }//if
          else if (allInformationTwo[l][4] != null && allInformationTwo[l][4].equals(allInformationTwo[l][6]) != true) { //***
  %>
            <TD><IMG SRC="../mimes/images/table/dot_green.gif" BORDER=0></TD>
  <%
         }//else if
  %>
            <TD><A HREF="<%=conditionOneUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_open.gif" BORDER=0></A></TD>
            <TD><B><%=allInformationTwo[l][0]%></B></TD>

          </TR>

          <TR>
            <TD></TD>
            <TD></TD>
            <TD>
              <TABLE>
  <%
    if (allInformationTwo[l][4] != null && allInformationTwo[l][4].equals(allInformationTwo[l][6]) != true) { //***
      TreeSet s = new TreeSet(new notFoundComp());
      TreeSet t = new TreeSet(new falseComp());
      boolean addFlag = false;
      boolean addFlagTwo = false;
      for (int k = 0; k < allInformationTwo.length; k++) {
        if (allInformationTwo[k][1].equals(allInformationTwo[i][0]) && allInformationTwo[k][2].equals(allInformationTwo[l][0])) {
          if (addFlag == false && addFlagTwo == false) {
  %>
                <TR>
  <%
            if (allInformationTwo[k][0].startsWith("ConditionRecordPreStepBuffer")) {
  %>  
                  <TD><B><%=allInformationTwo[k][0]%></B></TD>
  <%
              addFlag = false;
              addFlagTwo = true;
            }//if
            else if (allInformationTwo[k][0].startsWith("ConditionDynamicRecordBuffer")) {
              Iterator it = t.iterator();
              while (it.hasNext()) {
  %>
                <TR>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%= (String) it.next() %></TD>
                </TR>
  <%
              }//while  
  %>
                  <TD><B><%=allInformationTwo[k][0]%></B></TD>
  <%
              addFlagTwo = false;
              addFlag = true;
            }//else if
            else {
//if (allInformationTwo[k][0].startsWith("ConditionRecordPreStepBuffer") != true && allInformationTwo[k][0].startsWith("ConditionDynamicRecordBuffer") != true && addFlag == false)
  %>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%=allInformationTwo[k][0]%></TD>
  <%
            }//else
  %>
                </TR>
  <%
          }//if
          else if (addFlag == true) {
            s.add(allInformationTwo[k][0]);
          }//else if
          else if (addFlagTwo == true) {
            t.add(allInformationTwo[k][0]);
            if (allInformationTwo[k+1][0].startsWith("ConditionDynamicRecordBuffer")) {
              addFlagTwo = false;
            }
          }//else if
        }//if
      }//for
      if (addFlag == true) {
        Iterator it = s.iterator();
        while (it.hasNext()) {
  %>
                <TR>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%= (String) it.next() %></TD>
                </TR>
  <%
        }//while
      }//if
    }//if
    else if (allInformationTwo[l][4] != null && allInformationTwo[l][4].equals(allInformationTwo[l][6])) { //***
      for (int k = 0; k < allInformationTwo.length; k++) {
        if (allInformationTwo[k][1].equals(allInformationTwo[i][0]) && allInformationTwo[k][2].equals(allInformationTwo[l][0]) && allInformationTwo[k][0].startsWith("ConditionDynamicRecordBuffer") != true) {
  %>
                <TR>
  <%
          if (allInformationTwo[k][0].startsWith("ConditionRecordPreStepBuffer")) {
  %>  
                  <TD><B><%=allInformationTwo[k][0]%></B></TD>
  <%
          }//if
          else {
  %>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%=allInformationTwo[k][0]%></TD>
  <%
          }//else
  %>
                </TR>
  <%
        }//if
      }//for
    }//else if

  %>
              </TABLE>
            </TD>
          </TR>

  <%
        }//else if
  %>
          </TR>
  <%
      }//if
    }//for
  %>
        </TABLE>
      </TD>

  <%
            }//else if
          }//if
  %>
    </TR>
  <%
        }//for
  %>
  </TABLE>

  <%
      }//if

      session.setAttribute("valuesTwo", allInformationTwo);
    }//if

    else if (view.equals("1")) {

      String conditionBaseUrl = "http://"
                              + request.getServerName()
                              + ":"
                              + request.getServerPort()
                              + contextPath
			      + current
                              + "?"
                              + "view=1&"
                              ;

      //3.EBENE
      if (conditionTable == null && conditionType == null || conditionTable != null && conditionType == null || conditionTable != null && conditionType != null) {

  %>

  <TABLE>
  <%                          
        for (int i = 0; i < allInformation.length; i++ ) {
          if (allInformation[i][1].equals("parent")) {
  %>
    <TR>
  <%
            if (allInformation[i][3] != null &&  allInformation[i][3].equals("0") != true) {
              StringBuffer conditionUrl = new StringBuffer(conditionBaseUrl);
              conditionUrl.append("noInf=a" + "&conditionTable=" + allInformation[i][0]);

              if (allInformation[i][4].equals("n")) {
  %>
      <TD><IMG SRC="../mimes/images/table/dot_green.gif" BORDER=0></TD>
      <TD><A HREF="<%=conditionUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_close.gif" BORDER=0></A></TD>
      <TD><B><%=allInformation[i][0]%></B> ConditionRecordPreStepBuffer: (=<%=allInformation[i][3]%> / + <%=allInformation[i][5]%> / - <%=allInformation[i][6]%>) DynamicConditionRecordBuffer: (=<%=allInformation[i][7]%> / + <%=allInformation[i][8]%> / - <%=allInformation[i][9]%>)</TD>
  <%
              }//if
              else if (allInformation[i][4].equals("y")) {
  %>
      <TD><IMG SRC="../mimes/images/table/dot_green.gif" BORDER=0></TD>
      <TD><A HREF="<%=conditionUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_open.gif" BORDER=0></A></TD>
      <TD><B><%=allInformation[i][0]%></B></TD>

    <TR>
      <TD></TD>
      <TD></TD>
      <TD>
        <TABLE>
  <%
    for (int l = 0; l < allInformation.length; l++) {
      if (allInformation[l][1].equals("child") && allInformation[l][2].equals(allInformation[i][0])) {
  %>
          <TR>
  <%
        if (allInformation[l][3] != null &&  allInformation[l][3].equals(allInformation[l][6]) != true) { //***
          StringBuffer conditionOneUrl = new StringBuffer(conditionBaseUrl);
          conditionOneUrl.append("noInf=a" + "&conditionTable=" + allInformation[i][0] + "&conditionType=" + allInformation[l][0]);
          
          if (allInformation[l][4].equals("n")) {
  %>
            <TD><IMG SRC="../mimes/images/table/dot_green.gif" BORDER=0></TD>
            <TD><A HREF="<%=conditionOneUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_close.gif" BORDER=0></A></TD>
            <TD><B><%=allInformation[l][0]%></B> ConditionRecordPreStepBuffer: (=<%=allInformation[l][3]%> / + <%=allInformation[l][5]%> / - <%=allInformation[l][6]%>) DynamicConditionRecordBuffer: (=<%=allInformation[l][7]%> / + <%=allInformation[l][8]%> / - <%=allInformation[l][9]%>)</TD>

  <%
          }//if
          else if (allInformation[l][4].equals("y")) {
  %>
            <TD><IMG SRC="../mimes/images/table/dot_green.gif" BORDER=0></TD>
            <TD><A HREF="<%=conditionOneUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_open.gif" BORDER=0></A></TD>
            <TD><B><%=allInformation[l][0]%></B></TD>

          <TR>
            <TD></TD>
            <TD></TD>
            <TD>
              <TABLE>
  <%
    if (allInformation[l][3] != null && allInformation[l][3].equals(allInformation[l][6]) != true) { //***
      TreeSet s = new TreeSet(new notFoundComp());
      TreeSet t = new TreeSet(new falseComp());
      boolean addFlag = false;
      boolean addFlagTwo = false;
      for (int k = 0; k < allInformation.length; k++) {
        if (allInformation[k][1].equals(allInformation[i][0]) && allInformation[k][2].equals(allInformation[l][0])) {
          if (addFlag == false && addFlagTwo == false) {
  %>
                <TR>
  <%
            if (allInformation[k][0].startsWith("ConditionRecordPreStepBuffer")) {
  %>  
                  <TD><B><%=allInformation[k][0]%></B></TD>
  <%
              addFlag = false;
              addFlagTwo = true;
            }//if
            else if (allInformation[k][0].startsWith("ConditionDynamicRecordBuffer")) {
              Iterator it = t.iterator();
              while (it.hasNext()) {
  %>
                <TR>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%= (String) it.next() %></TD>
                </TR>
  <%
              }//while  
  %>
                  <TD><B><%=allInformation[k][0]%></B></TD>
  <%
              addFlagTwo = false;
              addFlag = true;
            }//else if
            else {
//if (allInformation[k][0].startsWith("ConditionRecordPreStepBuffer") != true && allInformation[k][0].startsWith("ConditionDynamicRecordBuffer") != true && addFlag == false)
  %>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%=allInformation[k][0]%></TD>
  <%
            }//else
  %>
                </TR>
  <%
          }//if
          else if (addFlag == true) {
            s.add(allInformation[k][0]);
          }//else if
          else if (addFlagTwo == true) {
            t.add(allInformation[k][0]);
            if (allInformation[k+1][0].startsWith("ConditionDynamicRecordBuffer")) {
              addFlagTwo = false;
            }
          }//else if
        }//if
      }//for
      if (addFlag == true) {
        Iterator it = s.iterator();
        while (it.hasNext()) {
  %>
                <TR>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%= (String) it.next() %></TD>
                </TR>
  <%
        }//while
      }//if
    }//if
    else if (allInformation[l][3] != null && allInformation[l][3].equals(allInformation[l][6])) { //***
      for (int k = 0; k < allInformation.length; k++) {
        if (allInformation[k][1].equals(allInformation[i][0]) && allInformation[k][2].equals(allInformation[l][0]) && allInformation[k][0].startsWith("ConditionDynamicRecordBuffer") != true) {
  %>
                <TR>
  <%
          if (allInformation[k][0].startsWith("ConditionRecordPreStepBuffer")) {
  %>  
                  <TD><B><%=allInformation[k][0]%></B></TD>
  <%
          }//if
          else {
  %>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%=allInformation[k][0]%></TD>
  <%
          }//else
  %>
                </TR>
  <%
        }//if
      }//for
    }//else if

  %>

              </TABLE>
            </TD>
          </TR>

  <%
          }//else if
        }//if
        else if (allInformation[l][3] != null && allInformation[l][3].equals(allInformation[l][6])) { //***
          StringBuffer conditionOneUrl = new StringBuffer(conditionBaseUrl);
          conditionOneUrl.append("noInf=a" + "&conditionTable=" + allInformation[i][0] + "&conditionType=" + allInformation[l][0]);

          if (allInformation[l][4].equals("n")) {
  %>
            <TD><IMG SRC="../mimes/images/table/dot_red.gif" BORDER=0></TD>
            <TD><A HREF="<%=conditionOneUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_close.gif" BORDER=0></A></TD>
            <TD><B><%=allInformation[l][0]%></B> ConditionRecordPreStepBuffer: (=<%=allInformation[l][3]%> / + <%=allInformation[l][5]%> / - <%=allInformation[l][6]%>) DynamicConditionRecordBuffer: (=<%=allInformation[l][7]%> / + <%=allInformation[l][8]%> / - <%=allInformation[l][9]%>)</TD>
<!--
ConditionRecordPreStepBuffer: (= 1 / + 0 / - 1) DynamicConditionRecordBuffer: (= 0 / + 0 / - 0)
-->

  <%
          }//if
          else if (allInformation[l][4].equals("y")) {
  %>
            <TD><IMG SRC="../mimes/images/table/dot_red.gif" BORDER=0></TD>
            <TD><A HREF="<%=conditionOneUrl.toString()%>"><IMG SRC="../mimes/images/tree/expander_open.gif" BORDER=0></A></TD>
            <TD><B><%=allInformation[l][0]%></B></TD>

          <TR>
            <TD></TD>
            <TD></TD>
            <TD>
              <TABLE>
  <%
    if (allInformation[l][3] != null && allInformation[l][3].equals(allInformation[l][6]) != true) { //***
      TreeSet s = new TreeSet(new notFoundComp());
      TreeSet t = new TreeSet(new falseComp());
      boolean addFlag = false;
      boolean addFlagTwo = false;
      for (int k = 0; k < allInformation.length; k++) {
        if (allInformation[k][1].equals(allInformation[i][0]) && allInformation[k][2].equals(allInformation[l][0])) {
          if (addFlag == false && addFlagTwo == false) {
  %>
                <TR>
  <%
            if (allInformation[k][0].startsWith("ConditionRecordPreStepBuffer")) {
  %>  
                  <TD><B><%=allInformation[k][0]%></B></TD>
  <%
              addFlag = false;
              addFlagTwo = true;
            }//if
            else if (allInformation[k][0].startsWith("ConditionDynamicRecordBuffer")) {
              Iterator it = t.iterator();
              while (it.hasNext()) {
  %>
                <TR>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%= (String) it.next() %></TD>
                </TR>
  <%
              }//while  
  %>
                  <TD><B><%=allInformation[k][0]%></B></TD>
  <%
              addFlagTwo = false;
              addFlag = true;
            }//else if
            else {
//if (allInformation[k][0].startsWith("ConditionRecordPreStepBuffer") != true && allInformation[k][0].startsWith("ConditionDynamicRecordBuffer") != true && addFlag == false)
  %>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%=allInformation[k][0]%></TD>
  <%
            }//else
  %>
                </TR>
  <%
          }//if
          else if (addFlag == true) {
            s.add(allInformation[k][0]);
          }//else if
          else if (addFlagTwo == true) {
            t.add(allInformation[k][0]);
            if (allInformation[k+1][0].startsWith("ConditionDynamicRecordBuffer")) {
              addFlagTwo = false;
            }
          }//else if
        }//if
      }//for
      if (addFlag == true) {
        Iterator it = s.iterator();
        while (it.hasNext()) {
  %>
                <TR>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%= (String) it.next() %></TD>
                </TR>
  <%
        }//while
      }//if
    }//if
    else if (allInformation[l][3] != null && allInformation[l][3].equals(allInformation[l][6])) { //***
      for (int k = 0; k < allInformation.length; k++) {
        if (allInformation[k][1].equals(allInformation[i][0]) && allInformation[k][2].equals(allInformation[l][0]) && allInformation[k][0].startsWith("ConditionDynamicRecordBuffer") != true) {
  %>
                <TR>
  <%
          if (allInformation[k][0].startsWith("ConditionRecordPreStepBuffer")) {
  %>  
                  <TD><B><%=allInformation[k][0]%></B></TD>
  <%
          }//if
          else {
  %>
                  <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0><%=allInformation[k][0]%></TD>
  <%
          }//else
  %>
                </TR>
  <%
        }//if
      }//for
    }//else if

  %>
              </TABLE>
            </TD>
          </TR>

  <%
          }//else if
        }//else if
  %>
          </TR>
  <%
      }//if
    }//for
  %>
        </TABLE>
      </TD>
    </TR>

  <%
              }//else if
            }//if

            else if (allInformation[i][3] != null && allInformation[i][3].equals("0")) {
  %>
      <TD><IMG SRC="../mimes/images/table/dot_red.gif" BORDER=0></TD>
      <TD><IMG SRC="../mimes/images/tree/empty.gif" BORDER=0></TD>
      <TD><B><%=allInformation[i][0]%></B></TD>
  <%
            }//else if
          }//if
  %>
    </TR>
  <%
        }//for
  %>
  </TABLE>


  <%
  
        session.setAttribute("valuesOne", allInformation);

      }//if
    }//else if

    else if (view.equals("500")) {
  %>
  <TABLE CLASS="infoTable" BORDER="0" CELLPADDING="0" CELLSPACING="0"  WIDTH=100% >
    <TR>
      <TD>

        <TABLE BORDER="0" CELLPADDING="5" CELLSPACING="1" WIDTH=100%>
          <TR>
	    <TD CLASS='tableName' VALIGN='middle'>An error occurred</TD>
          </TR>

          <TR>
            <TD CLASS="BoxContent" WIDTH=100%>

              <TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
                <TR>
	          <TD>No Buffers found</TD>
                </TR>
              </TABLE>

            </TD>
          </TR>
        </TABLE>
      </TD>
    </TR>
  </TABLE>
  <%  
    }//else if
  %>

  </BODY>

</HTML>

<%!

  private void createSession(String client) 
    throws Exception {
    
    m_client.cmd("CreateSession",
                 "client", client,
                 "application", "IPC_SYSTEM",
                 "userName", "dummyForIPCBuffer"
                );
  }

  //to be edited
  public class Conditions implements com.sap.spc.remote.shared.command.system.CommandConstants {

    private String[] getConditionTables()
      throws Exception {

      String[] i_m_result = null;

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_CONDITION_NAME,
  	    				GetConditionName.CLIENT, client
				       );

/*
        ServerResponse r = m_client.cmd(GET_CONDITION_NAME, new String[] {
                                                                           GetConditionName.CLIENT, client
                                                                         });
*/
        
        i_m_result = r.getParameterValues(GetConditionName.CONDITION_NAMES);

      }

      return i_m_result;
    }//private String[] getConditionTables()

    private String[] getConditionTypesForConditionTable(String conditionTableName)
      throws Exception {

      String[] i_m_result = null;

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_CONDITION_INFO, 
                                        GetConditionInfo.CLIENT, client,
  	    				GetConditionInfo.CONDITION_TABLE_NAME, conditionTableName
				       );

        i_m_result = r.getParameterValues(GetConditionInfo.BUFFERS);

      }

      return i_m_result;
    }//private String[] getConditionTypesForConditionTable(...

    private String[] getBuffersForConditionType(String conditionTableName, String conditionTypeName)
      throws Exception {

      String[] i_m_result = null;

      if (m_client != null) {
        ServerResponse r = m_client.cmd(GET_CONDITION_TYPE_INFO,
                                        GetConditionTypeInfo.CLIENT, client,
                                        GetConditionTypeInfo.CONDITION_TABLE_NAME, conditionTableName,
                                        GetConditionTypeInfo.CONDITION_TYPE_NAME, conditionTypeName
                                       );

        i_m_result = r.getParameterValues(GetConditionTypeInfo.CONDITION_TYPE_INFO);
      }

      return i_m_result;
    }//private String[] getBuffersForConditionType(...

  }// public class ConditionNames...

  private String[] getConditionTables()
    throws Exception {
    
    Conditions conditions = new Conditions();
    String[] rueck = conditions.getConditionTables();

    return rueck;
  }//private String getConditionTables()

  private String[] getConditionTypesForConditionTable(String conditionTableName)
    throws Exception {

    Conditions conditions = new Conditions();
    String[] rueck = conditions.getConditionTypesForConditionTable(conditionTableName);

    return rueck;
  }//private String[] getConditionTypesForConditionTable(...

  private String[] getBuffersForConditionType(String conditionTableName, String conditionTypeName)
    throws Exception {

    Conditions conditions = new Conditions();
    String[] rueck = conditions.getBuffersForConditionType(conditionTableName, conditionTypeName);

    return rueck;
  }//private String[] getBuffersForConditionType(...

  public class notFoundComp implements Comparator {
    public int compare(Object o1, Object o2) {
      int result = 1;
      String s = (String) o1;
      String t = (String) o2;
    
      if (s.endsWith("not found")) {
        result = -1;
      }//if

      return result;
    }//public int compare
  }//public class

  public class falseComp implements Comparator {
    public int compare(Object o1, Object o2) {
      int result = 1;
      String s = (String) o1;
      String t = (String) o2;
    
      if (s.endsWith("false")) {
        result = -1;
      }//if

      return result;
    }//public int compare
  }//public class
%>