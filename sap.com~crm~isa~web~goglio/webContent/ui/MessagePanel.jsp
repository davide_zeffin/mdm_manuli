<%-- **********************************************************************

      Display message panel

     ********************************************************************** --%>

<spe:errorFrame>
  <table border="0" cellpadding="5" cellspacing="0" width=100%>
    <tr>
      <td width="100%" height="5" bgcolor="#FF9999"></td>
    </tr>
    <spe:errorIterate>
      <tr><td bgcolor="#FFCCCC">
        <spe:errorDisplay/>
      </td></tr>
    </spe:errorIterate>
    <tr>
      <td width="100%" height="5" bgcolor="#FF9999"></td>
    </tr>
  </table>
</spe:errorFrame>
