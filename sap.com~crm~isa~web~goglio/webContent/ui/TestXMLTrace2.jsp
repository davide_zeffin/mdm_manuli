<html>
<head>
<title>
TestXMLPricingTrace
</title>
</head>
<body bgColor=#8fbc8b >
<%@ page import="java.io.*"                                                     %>
<%@ page import="java.util.Properties"                                          %>
<%@ page import="com.sap.ipc.webui.pricing.test.TestXMLPricingTrace"            %>

<%  Properties c_props = new Properties();
    String server               = "";
    String port                 = "";
    String r3Client             = "";
    String dispatcher           = "";
    String language             = "";
    String pricingProcedure     = "";
    String salesOrganisation    = "";
    String division             = "";
    String distributionChannel  = "";
    String departureCountry     = "";
    String departureRegion      = "";
    String country              = "";
    String dateFormat           = "";
    String documentCurrency     = "";
    String localCurrency        = "";
    String productId            = "";
    String sourcePath           = "";
    String fileName             = "currentSettings.properties";
    String filenameForXMLString = "c:/temp/analysis.xml";

    // check weather some data somehow
    sourcePath = config.getServletContext().getRealPath("/");
    TestXMLPricingTrace pricTrace = new TestXMLPricingTrace();
    c_props = pricTrace.readCurrentSettings(sourcePath, fileName);
    if (c_props == null) {                                                  %>
        <p><font color=red>currentSetting.properties file not found => all parameters will be initial</font></p>
<%  }
    else {
        server                       = c_props.getProperty("server");
        port                         = c_props.getProperty("port");
        r3Client                     = c_props.getProperty("r3Client");
        dispatcher                   = c_props.getProperty("dispatcher");
        language                     = c_props.getProperty("language");
        pricingProcedure             = c_props.getProperty("pricingProcedure");
        salesOrganisation            = c_props.getProperty("salesOrganisation");
        division                     = c_props.getProperty("division");
        distributionChannel          = c_props.getProperty("distributionChannel");
        departureCountry             = c_props.getProperty("departureCountry");
        departureRegion              = c_props.getProperty("departureRegion");
        country                      = c_props.getProperty("country");
        dateFormat                   = c_props.getProperty("dateFormat");
        documentCurrency             = c_props.getProperty("documentCurrency");
        localCurrency                = c_props.getProperty("localCurrency");
        productId                    = c_props.getProperty("productId");
        filenameForXMLString         = c_props.getProperty("filenameForXMLString");

    }                                                                           %>

<%
    String contextPath = request.getContextPath();
    response.setHeader("pragma", "no-cache");
    String actionUrl = contextPath+"/servlet/com.sap.ipc.webui.pricing.test.TestXMLPricingTrace";
%>

<form action=<%=actionUrl%>
      method="post">

<h3> Create a document and an item and get XML Trace</h3>
<table border width="80%">

<tr>
       <th bgcolor="#a9a9a9" colspan=4>data for ipc server</th>
</tr>
<tr>
       <td   bgcolor="#a9a9a9">server</td>
       <td > <input type="text" name="server" value="<%=server%>"></td>
       <td   bgcolor="#a9a9a9">dispatcher</td>
<%  if (dispatcher.equals("X")) {                                                %>
        <td  ><input type="checkbox" name="dispatcher" checked value="X"></td>
<%  }
    else {                                                                      %>
        <td ><input type="checkbox" name="dispatcher" value="X"></td>
<%  }                                                                           %>
</tr>
<tr>
       <td bgcolor="#a9a9a9">port</td>
       <td><input type="text" name="port" value="<%= port %>"></td>
       <td bgcolor="#a9a9a9">r3client</td>
       <td> <input type="text" name="r3Client" value="<%=r3Client%>"></td>
</tr>
<tr>
       <td bgcolor="#a9a9a9" colspan=4>&nbsp;</td>
</tr>
<tr>
       <th bgcolor="#a9a9a9" colspan=4>for creating document and item</th>
</tr>
<tr>
       <td bgcolor="#a9a9a9">pricingProcedure</td>
       <td><input type="text" name="pricingProcedure" value="<%=pricingProcedure%>"></td>
       <td bgcolor="#a9a9a9">departure country</td>
       <td><input type="text" name="departureCountry" value="<%=departureCountry%>"></td>
</tr>
<tr>
       <td bgcolor="#a9a9a9">salesOrganisation</td>
       <td><input type="text" name="salesOrganisation" value="<%=salesOrganisation%>"></td>
       <td bgcolor="#a9a9a9">departure region</td>
       <td><input type="text" name="departureRegion" value="<%=departureRegion%>"> </td>
</tr>
<tr>
       <td bgcolor="#a9a9a9">distributionChannel</td>
       <td><input type="text" name="distributionChannel" value="<%=distributionChannel%>"></td>
       <td bgcolor="#a9a9a9">country</td>
       <td><input type="text" name="country" value="<%=country%>"></td>
</tr>
<tr>
       <td bgcolor="#a9a9a9">division</td>
       <td><input type="text" name="division" value="<%=division%>"></td>
       <td bgcolor="#a9a9a9">dateFormat</td>
       <td><input type="text" name="dateFormat" value="<%=dateFormat%>"></td>
</tr>
<tr>
       <td bgcolor="#a9a9a9">documentCurrency</td>
       <td><input type="text" name="documentCurrency" value="<%=documentCurrency%>"></td>
       <td bgcolor="#a9a9a9">product id</td>
       <td><input type="text" name="productId" value="<%=productId%>"></td>
</tr>
<tr>
       <td bgcolor="#a9a9a9">localCurrency</td>
       <td><input type="text" name="localCurrency" value="<%=localCurrency%>"></td>
       <td bgcolor="#a9a9a9">language</td>
       <td><input type="text" name="language" value="<%=language%>"></td>
</tr>
<tr>
       <td bgColor=#8fbc8b colspan=4>&nbsp;</td>
</tr>
<tr>
       <th colspan=4 bgcolor="#a9a9a9">for servlet control</th>
</tr>
<tr>
       <th colspan=2 bgcolor="#a9a9a9">tracing</th>
       <th colspan=2 bgcolor="#a9a9a9">save xml trace file</th>
</tr>
<tr>
       <td bgcolor="#a9a9a9">servlet showadvancedxmlpricinganalysis</td>
       <td><input type="radio" name="trace"  value="trace"></td>
       <td bgcolor="#a9a9a9">flag, save xml trace</td>
       <td><input type="checkbox" name="save" value="X"></td>
</tr>
<tr>
       <td bgcolor="#a9a9a9">servlet saxonxmlviewservlet</td>
       <td><input type="radio" name="trace"  value="info"></td>
       <td bgcolor="#a9a9a9">filename</td>
       <td><input type="text" name="filenameForXMLString" value="<%=filenameForXMLString%>"></td>
</tr>
<tr>
       <th colspan=2>forwarding</th>
       <td></td>
       <td></td>
</tr>
<tr>
       <td bgcolor="#a9a9a9">forwarding to extended view</td>
       <td><input type="checkbox" name="forwarding" checked value="X"></td>
       <td> </td>
       <td></td>
</tr>


</table>


<p><input type="submit" name="Submit" value="run">
<input type="reset" value="Reset Values"></p>
<input type="hidden" name="fileName"   value="currentSettings.properties">
<input type="hidden" name="sourcePath" value= <%= sourcePath %> >
</form>

<servlet
  codebase=""
  code="com.sap.ipc.webui.pricing.test.TestXMLPricingTrace.class" >
    <param name="server"                value="" />
    <param name="port"                  value="" />
    <param name="r3Client"              value="" />
    <param name="dispatcher"            value="" />
    <param name="division"              value="" />
    <param name="pricingProcedure"      value="" />
    <param name="language"              value="" />
    <param name="departureCountry"      value="" />
    <param name="departureRegion"       value="" />
    <param name="country"               value="" />
    <param name= "dateFormat"           value="" />
    <param name="salesOrganisation"     value="" />
    <param name="distributionChannel"   value="" />
    <param name="productId"             value="" />
    <param name="documentCurrency"      value="" />
    <param name="localCurrency"         value="" />
    <param name="fileName"              value="" />
    <param name="sourcePath"            value="" />
    <param name="trace"                 value="" />
    <param name="info"                  value="" />
    <param name="save"                  value="" />
    <param name="filenameForXMLString"  value="" />
    <param name="forwarding"            value="" />

</servlet>

</body>
</html>
