<div class="module-name"><isa:moduleName name="catalog/PageLink.inc.jsp" /></div>
<%  if (itemPage != null && itemPage.getNoOfTotalPages() > 1) {
        int[] pageLinks = itemPage.getPaginationLinks(); 
%>
    <div class="cat-pcat-pages">
        <table class="cat-pcat-pages-tbl">
            <tr>
                <td class="cat-pcat-pages-tbl-pre">
                    <div class="fw-box-pcat-pgs-pre"><div class="fw-box-top-pcat-pgs-pre"><div></div></div><div class="fw-box-i1-pcat-pgs-pre"><div class="fw-box-i2-pcat-pgs-pre"><div class="fw-box-i3-pcat-pgs-pre"><div class="fw-box-content-pcat-pgs-pre">
                        <div class="cat-pcat-pages-title"><isa:translate key="catalog.isa.page"/>:</div>
<%      if (itemPage.showPrevLink()) { %>
                        <div class="cat-pcat-pages-prev">
                            <a href="javascript:setPage(<%= itemPage.getPage()-1 %>)" title="<%= itemPage.getPage()-1 %>: <isa:translate key="catalog.isa.previouspage"/> (<isa:translate key="catalog.isa.page"/> <%= itemPage.getPage()-1 %>)" tabindex="0"><isa:translate key="catalog.isa.previous"/></a>
                        </div>
<%      }
        for (int j=0; j < pageLinks.length; j++) {
            if (pageLinks[j] == itemPage.getPage() || itemPage.getPage() == 0) { 
%>
                    </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-pcat-pgs-pre"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>     
                </td>
                <td class="cat-pcat-pages-tbl-curr">
                    <div class="fw-box-pcat-pgs-curr"><div class="fw-box-top-pcat-pgs-curr"><div></div></div><div class="fw-box-i1-pcat-pgs-curr"><div class="fw-box-i2-pcat-pgs-curr"><div class="fw-box-i3-pcat-pgs-curr"><div class="fw-box-content-pcat-pgs-curr">
                        <div class="cat-pcat-pages-currpg">
                            <%= itemPage.getPage() + ((itemPage.getPage() != 0) ? 0 : 1) %>           
                        </div>
                    </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-pcat-pgs-curr"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>  
                </td> 
                <td class="cat-pcat-pages-tbl-post">
                    <div class="fw-box-pcat-pgs-post"><div class="fw-box-top-pcat-pgs-post"><div></div></div><div class="fw-box-i1-pcat-pgs-post"><div class="fw-box-i2-pcat-pgs-post"><div class="fw-box-i3-pcat-pgs-post"><div class="fw-box-content-pcat-pgs-post">
<%          } 
            else { 
%>
                        <div class="cat-pcat-pages-pg">
                            <a href="javascript:setPage(<%= pageLinks[j] %>)" title="<%= pageLinks[j] %>: <isa:translate key="catalog.isa.page"/> <%= pageLinks[j] %>" tabindex="0"><%= pageLinks[j] %></a>
                        </div>  
<%          }
        }
        if (itemPage.showNextLink()) { %>
                        <div class="cat-pcat-pages-next">
                            <a href="javascript:setPage(<%= itemPage.getPage()+1 %>)" title="<%= itemPage.getPage()+1 %>: <isa:translate key="catalog.isa.nextpage"/> (<isa:translate key="catalog.isa.page"/> <%= itemPage.getPage()+1 %>)" tabindex="0"><isa:translate key="catalog.isa.next"/></a>
                        </div>
<%      } %>
                        &nbsp;
                    </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-pcat-pgs-post"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>     
                </td>
            </tr>
        </table>
     </div>
<% } %>

