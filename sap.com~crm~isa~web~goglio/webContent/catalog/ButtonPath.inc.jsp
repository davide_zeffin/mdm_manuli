<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.catalog.boi.IQueryStatement" %>
<%@ page import="com.sap.isa.core.Constants" %>

<jsp:useBean id="thePath" scope="request" type="java.util.ArrayList" />
<div class="module-name"><isa:moduleName name="catalog/ButtonPath.inc.jsp" /></div>
<%
        BaseUI b2cbuttonui = new BaseUI(pageContext);
        if (b2cbuttonui.isAccessible())
        {
%>
<a href="#"
        id="b2cbuttonarea-start"
        title= "<isa:translate key="b2c.cat.b2cbtnarea"/>"
        accesskey="<isa:translate key="b2c.cat.b2cbtnarea.access"/>"
></a>
<%
        }
%>

<p align="right">
<%
if (currentItem.getItemKey().getParentCatalog().getCurrentItemList() != null && currentItem.getItemKey().getParentCatalog().getCurrentItemList().getQuery() != null)
{
  if (currentItem.getItemKey().getParentCatalog().getCurrentItemList().getAreaQuery() != null)
  {

%>
        <%
                String backBtnTxt = WebUtil.translate(pageContext, "catalog.isa.backToQuery", null);
        %>
    <input type="button"
        class="FancyButtonGrey"
        title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
        value="<isa:translate key="catalog.isa.backToQuery"/>"
        onClick="location.href=
        '<isa:webappsURL name="/catalog/getAttributes.do?isQuery=no">
                <isa:param name="areaid" value="workarea"/>
         </isa:webappsURL>'"
    >
<%
  } else {
%>
    <%
        String basequeryurl = "/catalog/query.do?redirecttolist=true&query=" + currentItem.getItemKey().getParentCatalog().getCurrentItemList().getQuery().toString();
        try
        {
                IQueryStatement qState = currentItem.getItemKey().getParentCatalog().getCurrentItemList().getQuery().getStatement();
        if (qState != null && qState.getStatementAsString() != null)
        {
                        basequeryurl = "/catalog/query.do?redirecttolist=true&query=" + qState.getStatementAsString();
        }
        else
        {
                        basequeryurl = "/catalog/query.do?redirecttolist=true&query=" + currentItem.getItemKey().getParentCatalog().getCurrentItemList().getQuery().toString();
        }
        }
        catch(Throwable th)
        {
                // Ignore
        }
        String queryBtnTxt = WebUtil.translate(pageContext, "catalog.isa.backToQuery", null);
    %>
        <input type="button"
                class="FancyButtonGrey"
        title="<isa:translate key="access.button" arg0="<%=queryBtnTxt%>" arg1=""/>"
                value="<isa:translate key="catalog.isa.backToQuery"/>"
                onClick="location.href=
        '<isa:webappsURL name="<%=basequeryurl%>">
                <isa:param name="areaid" value="workarea"/>
         </isa:webappsURL>'"
        >
<%
  }
}
else
{
        // we came from a catalog categorie
        if (null!=((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(0)).getAreaName())
        {
        String currentPath = ((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(0)).getPathAsString();
        String tmpPath="/catalog/categorieInPath.do?key="+currentPath;
        String backToBtnTxt = WebUtil.translate(pageContext, "catalog.isa.backTo", null);
%>
  <input type="button"
                class="FancyButtonGrey"
        title="<isa:translate key="access.button" arg0="<%=backToBtnTxt%>" arg1=""/>"
                value="<isa:translate key="catalog.isa.backTo"/>: <%= ((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(0)).getAreaName() %>"
                onClick="location.href=
        '<isa:webappsURL name="<%=tmpPath%>">
                <isa:param name="areaid" value="workarea"/>
         </isa:webappsURL>'"
  >&nbsp;
<%
        }
        // we can from some place else unknow so just go back.
        else
        {
                        String backBtnTxt = WebUtil.translate(pageContext, "catalog.isa.back", null);
                %>
                  <input type="button"
                                class="FancyButtonGrey"
                                title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
                                value="<isa:translate key="catalog.isa.back"/>"
                                onClick="history.back()"
                  >&nbsp;
                <%
        }
}
%>
&nbsp;&nbsp;</p>
<%
        if (b2cbuttonui.isAccessible())
        {
%>
        <a href="#b2cbuttonarea-start"
                id="b2cbuttonarea-end"
        ></a>
<%
        }
%>
