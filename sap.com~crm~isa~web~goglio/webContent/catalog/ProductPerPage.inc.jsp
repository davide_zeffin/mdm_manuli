  <div class="module-name"><isa:moduleName name="catalog/ProductPerPage.inc.jsp" /></div>
<% if (itemPage != null && itemPage.size() > 0) { %>
     <div class="module-content">
       <table>
         <tr>
           <td>
             <label for="pageselect"><isa:translate key="catalog.isa.entries"/></label> &nbsp;
               <%@ include file="ProductPerPageSelect.inc.jsp" %>
               &nbsp;
           </td>
        </tr>
      </table>         
    </div>
<% } %>