<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.catalog.actions.DetermineInactiveCatalogInfoAction" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>
  <%= ui.getTitle() %>   
    </title>
    
    <isa:stylesheets/>
  </head>
  <body>
<div class="module-name"><isa:moduleName name="b2c/displayList.inc.jsp" /></div>
    <div id="shoplist">
      <div id="header-appl">
        <div class="header-logo"></div>
        <div class="header-bar"></div> 
      </div> <%-- id="header-appl" --%>
      <div class="content">
        <h1 class="areatitle"><%=ui.getTitle()%></h1>  
    <%  String accessText = "";
        String[] evenOdd = new String[] { "even", "odd" }; 

        String b2c_shoplist_table1_title = ui.getTableTitle();
        if (ui.isAccessible()) {
            int b2c_shoplist_table1_row_nro = 0;
            com.sap.isa.core.util.table.ResultData  b2c_shoplist_table1_items = ui.getTableItems();
            b2c_shoplist_table1_row_nro += b2c_shoplist_table1_items.getNumRows();
            // Accessibility requirement
            int b2c_shoplist_table1_col_nro = 2;
            String b2c_shoplist_table1_col_cnt = Integer.toString(b2c_shoplist_table1_col_nro);
            String b2c_shoplist_table1_row_cnt = Integer.toString(b2c_shoplist_table1_row_nro);	%>
    <a id="b2c_shoplist_table1_begin"
        href="#b2c_shoplist_table1_end"
        tabindex="0"
        title="<isa:translate key="access.table.begin" arg0="<%=b2c_shoplist_table1_title%>" arg1="<%=b2c_shoplist_table1_row_cnt%>" arg2="<%=b2c_shoplist_table1_col_cnt%>" arg3="1" arg4="<%=b2c_shoplist_table1_row_cnt%>"/>">
    </a>		
    <% } 
    /* catalog staging ipc date*/
    if (!ui.isDisplayShopList()) { %>
       <form name="inactiveCatForm" action="<isa:webappsURL name="/app/readinactivecat.do"/>" method="post">
         <input type="hidden" name="techkey" value=""/>
            <script type="text/javascript">
           <!-- 
                function loadCatalog(techkey) {
                   document.inactiveCatForm.techkey.value = techkey;
                   document.inactiveCatForm.submit();
                }
          //-->
         </script>
         <% if (ui.showCatalogueStagingIPCDate()) { %>
         <%DateFormat dateFormatter = new SimpleDateFormat(ui.getDefaultDateFormat());%>
         <%@ include file="/appbase/jscript/calendar.js.inc.jsp"%>
         <br>
         <table DataTable="0">
             <tr>
               <td class="cat-pcat-hinfo">
                   <p><span><isa:translate key="staging.jsp.infoText"/></span></p>
               </td>    
             </tr>      
             <tr>
               <td class="b2c-cart-header-reqdel">
                 <label for="startDate"><isa:translate key="staging.jsp.iPCPricingDate"/></label>
                 <input class="app-inp-txt" type="text" maxlength="24" name="startDate" id="startDate"
                     value="<%= JspUtil.encodeHtml(ui.getToday()) %>" size="24"/>
             <% if (!ui.isAccessible) { %>
                <a class="img" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'startDate');">
                          <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/catalog/images/calendar.gif")  %>" alt="<isa:translate key="b2c.order.head.icon.calendar"/>"/>
                </a>
             <% } %>
               </td>
             </tr>
        </table>
     <% }
     } %>
    <div class="inner">
          <table class="app-std-tbl" summary="<%=b2c_shoplist_table1_title%>">
            <tbody>
              <tr>
                 <% int index=0;
                    for(index=0;index<ui.getTableTitleSize()-1;index++) { %>
                      <th scope="col" tabindex="0"><%=ui.getTableRowTitle(index)%></th>
                 <% } %>    
                <th class="app-std-tbl-th-last" scope="col" tabindex="0"><%=ui.getTableRowTitle(index)%></th>
              </tr>
           <% int line = 0; 
              if(ui.isDisplayShopList() ) {%>
                 <isa:iterate id="shop" name="<%= ShopShowListAction.RK_SHOP_LIST %>"
                           type="com.sap.isa.core.util.table.ResultData">
                   <tr class="app-std-tbl-<%= evenOdd[++line % 2]%>">
                     <td scope="row"><a href="<isa:webappsURL name="/b2c/readshop.do"/>?shopId=<%= shop.getRowKey() %>" title="<%=JspUtil.encodeHtml(shop.getString(Shop.ID))%>" tabindex="0"><%=JspUtil.encodeHtml(shop.getString(Shop.ID))%></a></td>
                     <td class="app-std-tbl-td-last"><a href="<isa:webappsURL name="/b2c/readshop.do"/>?shopId=<%= shop.getRowKey() %>" title="<%=JspUtil.encodeHtml(shop.getString(Shop.DESCRIPTION))%>" tabindex="0"><%=JspUtil.encodeHtml(shop.getString(Shop.DESCRIPTION))%></a></td>
                   </tr>
                 </isa:iterate>
            <% } 
               else { %>
                 <isa:iterate id="inactiveCat" name="<%= DetermineInactiveCatalogInfoAction.RK_INACTIVE_CATALOG_LIST %>"
                           type="com.sap.isa.core.util.table.ResultData"> 
                   <tr class="app-std-tbl-<%= evenOdd[++line % 2]%>">
                   <% String indexState = inactiveCat.getString("REPL_STATE");
                      String resKey = "shop.jsp.indexstate."+indexState; 
                      if (ui.isDisplayLink(indexState)) { %> 
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("VERSION"))%>"     tabindex="0"><%=inactiveCat.getString("VERSION")%></a></td>
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("REPL_STATE"))%>"  tabindex="0"><isa:translate key="<%=resKey%>"/></a></td>
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("REPL_TIME_IO"))%>" tabindex="0"><%=inactiveCat.getString("REPL_TIME_IO")%></a></td>
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("REPL_TIME_T"))%>" tabindex="0"><%=inactiveCat.getString("REPL_TIME_T")%></a></td>
                        <td class="app-std-tbl-td-last"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("REPL_BY"))%>" tabindex="0"><%=inactiveCat.getString("REPL_BY")%></a></td>
                   <% }
                      else { %>
                        <td scope="row"><%=inactiveCat.getString("VERSION")%></td>
                        <td scope="row"><isa:translate key="<%=resKey%>"/></td>
                        <td scope="row"><%=inactiveCat.getString("REPL_TIME_IO")%></td>
                        <td scope="row"><%=inactiveCat.getString("REPL_TIME_T")%></td>
                        <td class="app-std-tbl-td-last"><%=inactiveCat.getString("REPL_BY")%></td>
                   <% } %>   
                   </tr>
                 </isa:iterate>
                 </form>
            <% } %>        
            </tbody>
          </table>
        </div> <%-- class="inner" --%>  
     <% if (ui.isAccessible) { %>
          <a id="b2c_shoplist_table1_end" href="#b2c_shoplist_table1_begin" title="<isa:translate key="access.table.end" arg0="<%=b2c_shoplist_table1_title%>"/>"></a>
     <% } %>              
      </div> <%-- class="content" --%>
    </div> <%-- id="shoplist" --%>
  </body>
</html>