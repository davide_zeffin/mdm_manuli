<%@ page language="java" %>
<%@ taglib uri="/isa"  prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<div id="information" class="module">
  <div class="module-name"><isa:moduleName name="catalog/NoProductsISA.inc.jsp" /></div>

<%  com.sap.isa.catalog.webcatalog.WebCatArea currentArea = (com.sap.isa.catalog.webcatalog.WebCatArea) request.getAttribute("currentArea");
    boolean parent=false;
    if (currentArea != null && !currentArea.getAreaID().equals("$ROOT")) {
        parent=true;
    }
    if (currentArea == null || currentArea.getAreaID().equals("$ROOT")) {
        if (parent) {
%>
  <a href="<isa:webappsURL name="/catalog/categorieInPath.do?key=0"/>"><isa:translate key="catalog.isa.backTo"/><isa:translate key="catalog.isa.products"/></a>
<%      } 
        else {
%>
  &nbsp;
<%      } 
    } 
    else {
        String tmpPath="/catalog/categorieInPath.do?key="+currentArea.getPathAsString(); 
%>
  <a href="<isa:webappsURL name="<%= tmpPath %>"/>"><isa:translate key="catalog.isa.backTo"/> <%= JspUtil.encodeHtml(currentArea.getAreaName()) %></a>
<%  } %>
  <br /><br />
  <span><isa:translate key="catalog.isa.NoProductsISA"/></span>
</div>