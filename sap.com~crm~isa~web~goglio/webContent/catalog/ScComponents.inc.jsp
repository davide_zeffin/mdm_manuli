<%-- 
*****************************************************************************

    Include:      ScComponents.inc.jsp
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      01.01.2006

***************************************************************************** 

 Displaying of sub components of a sales package or combined rate plan.

 Required includes:
 ==================
 ScComponents.js.inc.jsp - contains javascript functions to control customer action
 
 Required global variables:
 ==========================
 WebCatItem currentItem     Package   
                                     
 usage:
 ======
  <%@  include file="ScComponents.inc.jsp"                                    %>
  
*****************************************************************************  
--%> 

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib prefix="isacore" uri="/isacore" %>

<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowCUAAction" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.businessobject.ProductBaseData" %>
<%@ page import="com.sap.isa.catalog.uiclass.SubItemListUI" %>
<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.isa.core.Constants" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItemPrice" %>

<%  ProductBaseData subItem; 
    
    boolean productFound = false;
    String[] evenOdd = new String[] { "app-std-tbl-even", "app-std-tbl-odd" };

    String columnNumber = "6";
    String tableTitle = WebUtil.translate(pageContext, "b2c.catalog.sccomp.tableTitle", null);
    int iGrpItem = 0; 
    int iGrp = -1;
    int iItem = 0;
    int grStart=1;
    int grEnd=1;
    String groupHeader = null;
    String[] argsTitle;
%>

<div class="module-name"><isa:moduleName name="catalog/ScComponents.inc.jsp" /></div>

<div id="cat-pcat-prd-cmp">

<%  if (subItemListUI.isAccessible()) { %>
    <a id="subcomptable-begin" href="#subcomptable-end"
       title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="" arg2="<%=columnNumber%>" arg3="" arg4=""/>">
    </a>
<%  } %>
    <div style="display: none">
        <input type="hidden" name="groupKey" value=""/>
        <input type="hidden" name="anchorKey" value=""/>
    </div> 
   
<%@ include file="js/ScComponents.js.inc.jsp" %>

<%  for (int iScItem = 1; iScItem <= subItemListUI.size(); iScItem++) {
        subItem = subItemListUI.getProduct(iScItem - 1); %>
    <div style="display: none">
        <input type="hidden" name="item[<%=iScItem%>].itemID" value="<%= subItem.getTechKey() %>"/>
        <input type="hidden" name="item[<%=iScItem%>].topItemID" value="<%= currentItem.getItemID()%>"/>
        <input type="hidden" name="item[<%=iScItem%>].unit" value="<%= subItem.getUnit()%>"/>
        <input type="hidden" name="item[<%=iScItem%>].quantity" value="<%= subItem.getQuantityAsStr()%>"/>
    </div>  
    <%-- New Area, with or without a group --%>
    <%  if (subItemListUI.isNewArea(subItem)) { 
            iGrpItem = 0; 
            iGrp++; 
            grStart=grEnd; 
    %> 
    <%-- Group anchor for positioning --%>
    <a name="groupAnchor_<%= iGrp %>"></a>
           <%-- Group Header is not displayed for rate plan combinations --%>
        <% if (!subItem.isRatePlanCombination()) { %>
               <%-- Build header line for new group --%>
           <%  if (subItemListUI.isGroupText()) { %>
                   <%-- Additional group text enhancements for dependent components --%> 
               <%  if (subItem.isDependentComponent()) { 
                       argsTitle = new String[] { subItemListUI.getGroupText(), subItem.getParent().getDescription() , "" };
                       groupHeader = WebUtil.translate(pageContext, "b2c.catalog.sccomp.ExtentionForGroup", argsTitle);
                   } 
                   else {
                       groupHeader = subItemListUI.getGroupText(); 
                   }        
               } 
               else if (subItem.isDependentComponent()) { %>
                   <%-- Group text for a dependent component if there is not a group text --%>
               <%  
                   argsTitle = new String[] { subItem.getParent().getDescription() , "" };
                   groupHeader = WebUtil.translate(pageContext, "b2c.catalog.sccomp.AssociatedProductsFor", argsTitle);
               } 
               else if (subItem.isCombinedRatePlan()) { %>
                   <%-- Group text for a combined rate plan --%>
               <%  groupHeader = WebUtil.translate(pageContext, "b2c.catalog.sccomp.CombinedRatePlan", null);
               } 
               else if (subItem.isSalesComponent()) { %>
                   <%-- Group text for a sales component --%>
               <%  groupHeader = WebUtil.translate(pageContext, "b2c.catalog.sccomp.AssociatedProducts", null);
               } %>        
               <%-- Display header line for new group --%>
    <div class="cat-pcat-prd-sccmp-head">
        <div class="fw-box-sccmp-head"><div class="fw-box-top-sccmp-head"><div></div></div><div class="fw-box-i1-sccmp-head"><div class="fw-box-i2-sccmp-head"><div class="fw-box-i3-sccmp-head"><div class="fw-box-content-sccmp-head">
            <%= groupHeader %>
        </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-sccmp-head"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>    
    </div> <%-- cat-pcat-prd-sccmp-head --%>
        <% } %>
                
    <div class="cat-pcat-prd-sccmp-cont">
        <div class="fw-box-sccmp-cont"><div class="fw-box-top-sccmp-cont"><div></div></div><div class="fw-box-i1-sccmp-cont"><div class="fw-box-i2-sccmp-cont"><div class="fw-box-i3-sccmp-cont"><div class="fw-box-content-sccmp-cont">

           <%-- Messages for mandatory groups without default --%>
        <% if (subItemListUI.isMandatoryGrpWODefaults(iScItem - 1)) { 
               subItemListUI.setMessageText("b2c.catalog.msg.MandatoryGroup"); %>
        <div class="cat-prd-msg" id="Group[<%= iGrp %>].message">
            <isa:message id="messagetext" name="<%= AttributeKeyConstants.MESSAGE_LIST %>" ignoreNull="true">
                <div class="error"><span><%=JspUtil.encodeHtml(messagetext)%></span></div>
            </isa:message>
        </div>
        <% } %>

        <table class= "app-std-tbl" id="subcomptable" width="100%"
                   summary="<isa:translate key="access.table.summary"
                   arg0="<%=tableTitle%>"
                   arg1=""
                   arg2="<%=columnNumber%>"
                   arg3=""
                   arg4=""/>">
            <thead>
            <tr>
                <th id="col.<%=iGrp%>.1" scope="col" title="<isa:translate key="b2c.catalog.sccomp.selection"/>"><span><isa:translate key="b2c.catalog.sccomp.selection"/></span></th>
                <th id="col.<%=iGrp%>.2" scope="col" title="<isa:translate key="b2c.catalog.sccomp.picture"/>"><span><isa:translate key="b2c.catalog.sccomp.picture"/></span></th>
                <th id="col.<%=iGrp%>.3" scope="col" title="<isa:translate key="b2c.catalog.sccomp.description"/>"><span><isa:translate key="b2c.catalog.sccomp.description"/></span></th>
                <th id="col.<%=iGrp%>.4" scope="col" title="<isa:translate key="b2c.catalog.sccomp.price"/>"><span><isa:translate key="b2c.catalog.sccomp.price"/></span></th>
                <th id="col.<%=iGrp%>.5" scope="col" title="<isa:translate key="b2c.catalog.sccomp.quantity"/>"><span><isa:translate key="b2c.catalog.sccomp.quantity"/></span></th>
                <th id="col.<%=iGrp%>.6" class="last" scope="col" title="<isa:translate key="b2c.catalog.sccomp.compare"/>"><span><isa:translate key="b2c.catalog.sccomp.compare"/></span></th>
            </tr>
            </thead>
            <tbody>
            <%-- create a dummy sub item if the group has no default elements --%>
            <%  if (subItemListUI.isNoSelectionDisplayed(iScItem - 1)) { %>
                <tr class="<%= evenOdd[iGrpItem % 2]%>" id="Group[<%= iGrp %>]_[<%= iGrpItem %>]">
                    <td headers="col.<%=iGrp%>.1" class="cat-pcat-prd-sccmp-tbl-selbtn">
                        <div class="cat-prd-facts">
                            <div class="b2c-sel">
                                <div style="display: none">
                                    <input type="hidden" name="subItem[<%= iItem %>].groupKey" value="Group[<%= iGrp %>]"/>
                                    <input type="hidden" name="subItem[<%= iItem %>].iScItem" value="0"/>
                                </div>    
                                <input type="radio" 
                                       title="<isa:translate key="b2c.catalog.sccomp.ScDeSelection"/>" 
                                       name="Group[<%= iGrp %>]"
                                       value="NoSelection" 
                                       onclick="javascript:changeSelForResFlag('<%= iGrp %>', '<%= iGrpItem %>', '<%= subItemListUI.getGroupText() %>')" 
                                       checked="checked"/>
                            </div>
                        </div>
                    </td>
                    <td headers="col.<%=iGrp%>.2" class="cat-pcat-prd-sccmp-tbl-thumb">
                    </td>
                    <td headers="col.<%=iGrp%>.3" class="cat-pcat-prd-sccmp-tbl-prddata">
                        <div class="cat-prd-dsc">
                            <a <% if (subItemListUI.isNoSelectionItemAnchor(subItemListUI.getGroupText())) { %> name="basketAnchor" href=" "<% } %> ><%= WebUtil.translate(pageContext, "b2c.catalog.sccomp.NoSelection", null) %></a>
                        </div>
                    </td>
                    <td headers="col.<%=iGrp%>.4" class="cat-pcat-prd-sccmp-tbl-pricing">
                    </td>
                    <td headers="col.<%=iGrp%>.5" class="cat-pcat-prd-sccmp-tbl-quant">
                    </td>
                    <td headers="col.<%=iGrp%>.6" class="cat-pcat-prd-sccmp-tbl-comp">
                    </td>
                </tr>         
            <%      iItem++;
                    iGrpItem++;
                }         
        } 
        grEnd++; %>

        <%-- Sub components --%>
        <tr class="<%= evenOdd[iGrpItem % 2]%>" id="Group[<%= iGrp %>]_[<%= iGrpItem %>]">
                <%-- column 1 - selection button --%>
                <td headers="col.<%=iGrp%>.1" class="cat-pcat-prd-sccmp-tbl-selbtn">
                    <div class="b2c-sel">
                        <div style="display: none">
                            <input type="hidden" name="subItem[<%= iItem %>].groupKey" value="Group[<%= iGrp %>]"/>
                            <input type="hidden" name="subItem[<%= iItem %>].iScItem" value="<%= iScItem %>"/>
                            <input type="hidden" name="item[<%=iScItem%>].scSelected" value="<%= subItem.isScSelected()%>"/>
                            <input type="hidden" name="item[<%=iScItem%>].selForResLast" value="<%= subItem.getAuthor()%>"/>
                            <input type="hidden" name="item[<%=iScItem%>].selForRes" value=""/>
                            <input type="hidden" name="item[<%=iScItem%>].subItemType" 
                            <%  if (subItem.isCombinedRatePlan()) { %>
                                   value="CRP"/>
                            <%  }
                                else if (subItem.isRatePlanCombination()) { %>
                                   value="RPC"/>
                            <%  }
                                else { %>
                                   value=""/>
                            <%  } %>
                            </div>
         <% if (subItemListUI.isSelectionAsRadioButton(iScItem - 1)) { %>   
                        <input type="radio" 
                               title="<isa:translate key="b2c.catalog.sccomp.ScSelection"/>" 
                               name="Group[<%= iGrp %>]"
                               value="<%=iScItem%>"
                               onclick="javascript:changeSelForResFlag('<%= iGrp %>', '<%= iGrpItem %>', '<%= subItem.getTechKey() %>' )" 
                               <% if (subItem.isScSelected()) { %>
                                      checked="checked"
                               <% } %>       
                               <% if (!subItem.isOptional()) { %>
                                      disabled="disabled"
                               <% } %>
                               />
        <%  } 
            else { %>
                        <input type="checkbox" 
                               title="<isa:translate key="b2c.catalog.sccomp.ScSelection"/>" 
                               name="Group[<%= iGrp %>]"
                               value="<%=iScItem%>"
                               onclick="javascript:changeSelForResFlagCheckbox('Group[<%= iGrp %>]', '<%= iGrpItem %>', '<%=iScItem%>', '<%= subItem.getTechKey() %>')" 
                               <% if (subItem.isScSelected()) { %>
                                      checked="checked"
                               <% } %>       
                               <% if (!subItem.isOptional()) { %>
                                      disabled="disabled"
                               <% } %>
                               />
        <%  } %>
                    </div>
                </td>
                <%-- column 2 - thumb nail --%>
                <td headers="col.<%=iGrp%>.2" class="cat-pcat-prd-sccmp-tbl-thumb">
                    <div class="cat-prd-thumb">
                        <% pageContext.setAttribute("item", subItem); %>
                        <isa:ifThumbAvailable name="item" guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE">
                            <a <% if (subItemListUI.isItemBasketAnchor(subItem.getTechKey())) { %> name="basketAnchor" <% } %> 
                               href="javascript:displayPackageComponentDetails('<%=subItem.getTechKey()%>','<%=currentItem.getItemId()%>')" >
                               <img src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="item" />"
                                    alt="<%= JspUtil.encodeHtml(subItem.getDescription())%>" />
                            </a>
                        </isa:ifThumbAvailable>
                    </div>
                </td>
                <%-- column 3 - product description, inline configuration, product options --%>
                <td headers="col.<%=iGrp%>.3" class="cat-pcat-prd-sccmp-tbl-prddata">
                    <div class="cat-prd-facts">
                        <%-- Calculation of indentation in case of a hierarchical presentation --%>
                    <%  for (int i=0; (i < subItem.getHierarchyLevel()-1 && !subItem.isDependentComponent()); i++) { %>
                            <div>  
                    <%  } %>
                        <%-- product id --%>
                        <div class="cat-prd-id">
                            <%-- show sales components + rate plan combinations --%>
                            <%= subItem.getId()%>
                        </div>
                        <%-- product description --%>
                        <div class="cat-prd-dsc">
                            <a <% if (subItemListUI.isItemBasketAnchor(subItem.getTechKey())) { %> name="basketAnchor" <% } %> href="javascript:displayPackageComponentDetails('<%=subItem.getTechKey()%>','<%=currentItem.getItemId()%>')"><%= JspUtil.encodeHtml(subItem.getDescription()) %></a>
                        </div>
                        <%-- inline configuration for configurable product--%>  
                        <% if (subItem.isConfigurable()) {
                               //these variables should be defined before calling itemconfiginfo.inc.jsp
                               int viewType = 2; 
                               WebCatItem item = (WebCatItem) subItem;
                               if(item.getConfigItemReference() ==null ) {
                                  item.readItemPrice();
                               }
                               boolean isSubItem = true; 
                               IPCItem itemConfig = item.getConfigItemReference(); %>
                               <div class="cat-prd-cfg">
                                   <%@ include file="/catalog/itemconfiginfo.inc.jsp" %>
                               </div> 
                        <% } %>
                        <%-- eye catcher text --%>
                        <div class="b2c-prd-eye">
                            <span><var><%= JspUtil.encodeHtml(subItem.getEyeCatcherText()) %></var></span>
                        </div>
                        <%--  configuration --%>
                      <% if(ui.shouldConfigMessageLinkBeDisplayed((WebCatItem)subItem)) {
                            WebCatItem configItem = (WebCatItem) subItem; 
                            boolean setAnchor = subItemListUI.isCfgLnkBasketAnchor(subItem.getTechKey());
                            boolean displayAsLink = true;
                            String lastVisited = "itemDetails";
                            String formSuffix = "";
                            if(subItem.isScSelected()) { %>
                               <div id="configMessageLinkEnabled[<%=iScItem%>]" style="display:inline">
                         <% }
                            else { %>
                               <div id="configMessageLinkEnabled[<%=iScItem%>]" style="display:none">
                         <% } %>
                        <%@ include file="/catalog/configLink.inc.jsp"%>
                               </div>
                         <% displayAsLink = false;
                            if(subItem.isScSelected()) { %>
                               <div id="configMessageLinkDisabled[<%=iScItem%>]" style="display:none">
                         <% }
                            else { %>
                               <div id="configMessageLinkDisabled[<%=iScItem%>]" style="display:inline">
                         <% } %>       
                            <%@ include file="/catalog/configLink.inc.jsp"%>
                               </div>
                      <% } %>                        
                        <%-- contract duration --%>
                     <% if(ui.shouldContractDurationBeDisplayed((WebCatItem)subItem)) { %>
                            <div class="cat-prd-cntdur-lbl">
                               <isa:translate key="b2c.contractDuration"/>
                            </div>
                            
                         <% //if there are more than one contract duration to display then display them as a list, else as a label
                            if( (((WebCatItem)subItem).getContractDurationArray()).size() > 1) {
                                 if(subItem.isScSelected()) { %>
                                     <div id="contractDurationEnabled[<%=iScItem%>]" style="display:inline">
                              <% } 
                                 else { %>
	    		                     <div id="contractDurationEnabled[<%=iScItem%>]" style="display:none">
                              <% }  %>
                               <select name="item[<%=iScItem%>].selectedContractDuration" size="1" onChange="setContractDuration('<%=currentItem.getItemId()%>', '<%= subItem.getTechKey() %>')">
                            <% Iterator contractDurationIterator = ((WebCatItem)subItem).getContractDurationArray().iterator();
                               ContractDuration contractDuration = null;
                               String selectedContractDuration = ((WebCatItem)subItem).getSelectedContractDuration().toString(ui.getLocale());
                               while(contractDurationIterator.hasNext() ) { 
                                   contractDuration = (ContractDuration)contractDurationIterator.next();
                                   if(contractDuration.toString(ui.getLocale()).equals(selectedContractDuration)) { %>
                                       <option selected="selected" 
                                <% }
                                   else { %>
                                   <option
                                <% } %>   
                                   value="<%=contractDuration.toString(ui.getLocale())%>">
                                   <%= contractDuration.toString(ui.getLocale()).replaceAll(" ","&nbsp;") %></option>                                
                            <% } %>	
                               </select>
                                 </div>
 
                            <% if(subItem.isScSelected()) { %>
                                     <div id="contractDurationDisabled[<%=iScItem%>]" style="display:none">
                            <% }   
                               else { %>
                                     <div id="contractDurationDisabled[<%=iScItem%>]" style="display:inline">
                            <% } %>
                                 <select name="item[<%=iScItem%>].selectedContractDuration" size="1" onChange="setContractDuration('<%=currentItem.getItemId()%>', '<%= subItem.getTechKey() %>')" disabled="disabled">
			      
                            <% contractDurationIterator = ((WebCatItem)subItem).getContractDurationArray().iterator();
                               contractDuration = null;
                               selectedContractDuration = ((WebCatItem)subItem).getSelectedContractDuration().toString(ui.getLocale());
                               while(contractDurationIterator.hasNext() ) { 
                                   contractDuration = (ContractDuration)contractDurationIterator.next();
                                   if(contractDuration.toString(ui.getLocale()).equals(selectedContractDuration)) { %>
                                        <option selected="selected" 
                               <%  }
                                   else { %>
                                        <option
                               <%  } %>   
                                         value="<%=contractDuration.toString(ui.getLocale())%>">
                                 <%= contractDuration.toString(ui.getLocale()).replaceAll(" ","&nbsp;") %></option>                                
                            <% } %>	
                                 </select>
                               </div>
                         <% }   
                            else { /*display contract duration as a label */ %>
                               <div style="display: none">
                                   <input type="hidden" name="item[<%=iScItem%>].itemID" value="<%= subItem.getSelectedContractDuration().toString(ui.getLocale()) %>"/>
                               </div>  
                               <div class="cat-prd-cntdur">
                                    <%= subItem.getSelectedContractDuration().toString(ui.getLocale()).replaceAll(" ","&nbsp;") %>
                               </div>
                         <% } %>
                     <% } %>
                        <%-- Calculation of identation in case of a hierarchical presentation --%>
                     <% for (int i=0; (i < subItem.getHierarchyLevel()-1 && !subItem.isDependentComponent()); i++) { %>
                            </div>  
                     <% } %>
                    <%-- end of cat-prd-facts --%>
                    </div>
                </td>
                <%-- column 4 - price of the sub item --%>
                <td headers="col.<%=iGrp%>.4" class="cat-pcat-prd-sccmp-tbl-pricing">
                <% priceUI.setPriceRelevantInfo(subItem, WebCatItemPrice.SHOW_NO_SUMS, true); %>
                 <%@ include file="pricing.list.inc.jsp" %>
                 </td>
                <%-- column 5 - quantity and unit of the sub item --%>
                <td headers="col.<%=iGrp%>.5" class="cat-pcat-prd-sccmp-tbl-quant">
                    <%-- Quantity --%>
                    <div class="cat-prd-qty">
                        <span><var><%= subItem.getQuantityAsStr()%></var></span>
                    </div>
                    <%-- Unit --%>
                    <div class="cat-prd-unit">
                        <span><var><%= subItem.getUnit()%></var></span>
                    </div>
                </td>
                <%-- column 6 - compare --%>
                <td headers="col.<%=iGrp%>.6" class="cat-pcat-prd-sccmp-tbl-comp">
                    <div class="cat-prd-comp">
                        <%-- <isa:translate key="b2c.catalog.sccomp.compare"/> --%>
                        <input type="checkbox" 
                               title="<isa:translate key="catalog.isa.checkboxTooltip"/>" 
                               id="item[<%=iScItem%>].selected" 
                               name="item[<%=iScItem%>].selected" 
                               style="display:none" 
                               <% if (subItemListUI.isCompCheckboxDisplayed(iScItem - 1)) { %> 
                                      value="true"
                               <% }
                                  else { %>
                                      value="hidden"
                               <% }      
                                  if (subItem.isSelected()) { %>
                                    checked = "checked"
                               <% } %>       
                               />
                    </div>
                </td>
        </tr>
     <% iItem++;
        iGrpItem++; %>
       
        <%-- Set group button at the end of the area --%>
     <% if (subItemListUI.isEndOfArea(iScItem - 1)) { %>
                <tr><td colspan="<%= columnNumber %>"></td></tr>
            </tbody>
        </table>
            <%-- Only show buttons, if the group contains more than one entry --%>
        <%  if (subItemListUI.itemGroupHasMoreThanOneEntry(iScItem - 1)) { %>
                <div style="display: none">
                    <input type="hidden" name="itemGroup[<%= iGrp %>].groupId" value="<%=subItemListUI.getGroupKey()%>"/>
                    <input type="hidden" name="itemGroup[<%= iGrp %>].collapsed" value="<%=subItemListUI.isGroupCollapsed()%>"/>
                </div>
                <%-- set expand button for groups --%>
                <div class="cat-btn-tog" id="expandBtn(Group[<%= iGrp %>])">
                    <a href="#groupAnchor_<%= iGrp %>" 
                       onclick="expandGroup('<%= iGrp %>', '<%= iItem %>')"
                       name="<isa:translate key="b2c.catalog.sccomp.btn.expand"/>"
                    ><img border="0" alt="<isa:translate key="b2c.catalog.sccomp.btn.expand"/>" 
                          src="<%=WebUtil.getMimeURL(pageContext, "mimes/b2c/images/open.gif") %>" 
                    /></a>
                    <a href="#groupAnchor_<%= iGrp %>" 
                       onclick="expandGroup('<%= iGrp %>', '<%= iItem %>')"
                       name="<isa:translate key="b2c.catalog.sccomp.btn.expand"/>"
                    ><isa:translate key="b2c.catalog.sccomp.btn.expand"/></a>
                </div>
                <%-- set collapse button for groups --%>
                <div class="cat-btn-tog" id="collapseBtn(Group[<%= iGrp %>])">
                    <a href="#groupAnchor_<%= iGrp %>"
                       onclick="collapseGroup('<%= iGrp %>', '<%= iItem %>')"
                       name="<isa:translate key="b2c.catalog.sccomp.btn.collapse"/>"
                    ><img border="0" alt="<isa:translate key="b2c.catalog.sccomp.btn.collapse"/>" 
                                 src="<%=WebUtil.getMimeURL(pageContext, "mimes/b2c/images/close.gif") %>"
                    /></a>                    
                    <a href="#groupAnchor_<%= iGrp %>"
                       onclick="collapseGroup('<%= iGrp %>', '<%= iItem %>')"
                       name="<isa:translate key="b2c.catalog.sccomp.btn.collapse"/>"
                    ><isa:translate key="b2c.catalog.sccomp.btn.collapse"/></a>
                </div>
                <%-- compare button --%> 
                <div class="cat-btn-com" id="compareBtn(Group[<%= iGrp %>])">
                         <input type="button" title="<isa:translate key="b2c.catalog.sccomp.btn.compare"/>" 
                               value="<isa:translate key="b2c.catalog.sccomp.btn.compare"/>"
                               onclick="javascript:popUpCompareSC('<%=subItemListUI.getGroupKey()%>',<%=grStart%>, <%=grEnd%>)" 
                               class="FancyButtonGrey"/>
                         <span style="background-color: #fff display: inline; vertical-align: top; color: #fff; margin: 0px 10px 0px 3px;">      
                         <img border="0" alt="" src="<%=WebUtil.getMimeURL(pageContext, "mimes/catalog/images/arrow_tr_bl.gif") %>" />      
                         </span>                               
                </div>   
         <% } %> <%-- itemGroupHasMoreThanOneEntry --%>
         
         </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-sccmp-cont"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>

             </div> <%-- cat-pcat-prd-sccmp-cont --%>

            <%-- hide non selected products --%>
         <% if (subItemListUI.itemGroupHasMoreThanOneEntry(iScItem - 1)) { %>
              <script language="JavaScript">
            <%  if (subItemListUI.isGroupCollapsed()) { %>
                  collapseGroup('<%= iGrp %>', '<%= iItem %>')
             <% } 
                else { %>   
                  expandGroup('<%= iGrp %>', '<%= iItem %>') 
             <% } %>
              </script>			    		    
         <% } %>
            <div style="display: none">
                <input type="hidden" name="Group[<%= iGrp %>].MaxItems" value="<%= iGrpItem %>"/>
            </div>    
     <% } 
    } %>
 
<%-- dynamically set the AddToBasket button active --%>
    <script language="JavaScript">
<%  if (subItemListUI.isPackageCorrect()) { %>
        setAddToBasketActive('true');
<%  } 
    else { %>   
        setAddToBasketActive('false');
<%  } %>
    </script>			    		    
    
<%  if (subItemListUI.isAccessible()) { %>
    <a id="subcomptable-end" href="#subcomptable-begin" title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>"></a>
<%  } %>
     
<%-- end of cat-pcat-prd-cmp --%>
</div>