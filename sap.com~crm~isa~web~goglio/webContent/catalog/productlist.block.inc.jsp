<%-- Include to display a product list, either bestseller, recommendations or catalog area --%>
<%-- ProductListUI ui must be define outside  --%>
<%-- The Javascript library "b2b/jscript/ipc_pricinganalysis.jsp" must be loaded --%>

<%@ page import="com.sap.isa.catalog.webcatalog.*" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="java.lang.Math" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.catalog.AttributeKeyConstants" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>

<%@ include file="/catalog/transferToOrder.inc.jsp" %>

<div class="module-name"><isa:moduleName name="catalog/productlist.block.inc.jsp" /></div>

<%@ include file="/catalog/js/productlist.js.inc.jsp" %>

<form name="productform<%=ui.getFormSuffix()%>" 
      action='<isa:webappsURL name="/catalog/updateItems.do"> <%-- If it is not a query --%> <% if (isQuery != null && isQuery.equals("yes")) { %> <isa:param name="isQuery" value="yes"/> <% } %> <isa:param name="areaid" value="workarea"/> </isa:webappsURL>' 
      method="POST">

    <input type="hidden" name="page" value=""/>
 <% int pageNo = 1;
    int modBlock = 6; /* Variable for modulo calculation of layout blocks */ 
    if (itemPage != null) { 
        pageNo = itemPage.getPage(); %>
        <input type="hidden" name="itemPageSize" value="<%= itemPage.pageSize() %>"/>
 <% } %>
    <input type="hidden" name="next" value=""/>
    <input type="hidden" name="itemkey" value=""/>
    <input type="hidden" name="order" value=""/>
    <input type="hidden" name="contractkey" value=""/>
    <input type="hidden" name="contractitemkey" value=""/>
    <input type="hidden" name="topItemkey" value=""/>
    <input type="hidden" name="lastVisited" value="<%= ui.getLastVisited()%>"/>
    <input type="hidden" name="isQuery" value="<%= (isQuery == null || !isQuery.equals("yes")) ? "yes" : "" %>">    
<% if (isQuery == null || !isQuery.equals("yes")) { %>
        <input type="hidden" name="display_scenario" value="products"/>
 <% } else { %>
        <input type="hidden" name="display_scenario" value="query"/>
 <% } %>
    <%-- Check, if the productlist contains products --%>
 <% if (ui.getProductList().size() > 0) { %>
        <div id="cat-pcat">
     <% if (ui.showPage()) { %>
		<% if (!ui.isCatalogQuery()) { %>
			<%@ include file="/catalog/ProductPerPage.inc.jsp"%>
		<% } %>
        <%@ include file="/catalog/SearchResults.inc.jsp"%>
        <%@ include file="/catalog/PageLink.inc.jsp" %>
     <% } %>

        <div id="cat-pcat-hpv">
         <%  if (ui.isAccessible()) { %>
                <a id="cat-pcat-blockview-start" href="#cat-pcat-blockview-end" 
                   title="<isa:translate key="b2c.cat.blockview.begin.access.inf"/>" 
                   accesskey="<isa:translate key="b2c.cat.blockview.begin.access"/>">
                </a>
         <% } %>
            <table class="cat-pcat-hpv-tbl" DataTable="0" summary="<isa:translate key="b2c.layoutTable.summary"/>">
                <tbody>
                 <% int itemno = -1;
                    int hpvStyle = 0;
                    int colno = 0;
                    int widthTd = 0;
                    if(ui.getMaxCols() != 0) {
                        widthTd = Math.round(100 / ui.getMaxCols());
                    }
                    else {
                        widthTd = 100;
                    }
                    String baseURL;
                    String detailReq = "";
                    Iterator iter = ui.getProductList().iterator();
                    
                    while (iter.hasNext()) {
                        Product product = (Product) iter.next();
                        if (ui.isCalledFromCatalog()) { 
                            baseURL = "/catalog/updateItems.do?next=seeItem&itemkey=" + product.getItemId()
                                    + "&" + ActionConstants.RA_DISPLAYSCENARIO + "=" + ui.getDetailScenario(); 
                        } 
                        else { 
                            baseURL = "catalog/productdetailFromOutside.do";
                            detailReq = ShowProductDetailAction.createDetailRequest(product,ui.getDetailScenario(),ui.getItemIndex(false));
                        } 
                        
                        // according to stylesheet definition of cat-pcat-hpv-prod-<hpvStyle>
                        hpvStyle = (itemno+1) % modBlock;
                        if (itemno++ == ui.pageSize()-1 && ui.pageSize()-1 > 0) {
                            break;
                        }
                        if (colno++ == 0) { %>
                            <tr>
                     <% } %>
                        <input type="hidden" name="item[<%=itemno%>].itemID" value="<%= product.getItemId() %>"/>
                        <input type="hidden" name="item[<%=itemno%>].itemIndex" value="<%= ui.getItemIndex(true) %>"/>
                        <td style="width:<%= Integer.toString(widthTd) %>%">
                        <div id="cat-pcat-hpv-prod-<%=hpvStyle%>">
                            <div class="fw-box">
                                <div class="fw-box-top">
                                    <div></div>
                                </div>
                                <div class="fw-box-i1">
                                    <div class="fw-box-i2">
                                        <div class="fw-box-i3">
                                            <div class="fw-box-content">
                                                
                                            <% pageContext.setAttribute("item", product); %>
                                            <isa:ifThumbAvailable name="item">
                                                <div class="cat-prd-thumb">
                                                    <a href="#" onclick="javascript:displayProdDetails('<isa:webappsURL name="<%=baseURL%>"/><%=detailReq%>', '<%=ui.getFormSuffix()%>')" >                                                        
                                                       <img src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="item" />"
                                                            alt="<%=JspUtil.encodeHtml(product.getDescription())%>"/> 
                                                    </a>
                                                </div>
                                            </isa:ifThumbAvailable>

                                            <div class="cat-prd-facts">
                                                <%-- Product ID --%>
                                                <div class="cat-prd-id">
                                                    <a href="#" onclick="javascript:displayProdDetails('<isa:webappsURL name="<%=baseURL%>"/><%=detailReq%>', '<%=ui.getFormSuffix()%>')" >                                                        
                                                       <%=prodUI.getHighlightedResults(product.getId(), htTerms, AttributeKeyConstants.PRODUCT, "cat-prd-hli")%>
                                                    </a>
                                                </div>
                                                <%-- Description --%>
                                                <div class="cat-prd-dsc">
                                                    <a href="#" onclick="javascript:displayProdDetails('<isa:webappsURL name="<%=baseURL%>"/><%=detailReq%>', '<%=ui.getFormSuffix()%>')" >                                                        
                                                       <%= prodUI.getHighlightedResults(product.getDescription(), htTerms, AttributeKeyConstants.PRODUCT_DESCRIPTION, "cat-prd-hli") %>
                                                    </a>
                                                </div>
                                                <%-- Inline Configuration for Configurable Product--%>
                                                <% if (product.isConfigurable() == true) { %>
                                                <div class="cat-prd-cfg">
                                                <%  //theses variables should be defined before to call itemconfiginfo.inc.jsp
		    					                      int viewType = 2; 
			    				                      IPCItem itemConfig =  product.getCatalogItem().getConfigItemReference();
				    			                      boolean isSubItem = false; %>
                                                      <%@ include file="/catalog/itemconfiginfo.inc.jsp" %> 
                                                </div>                                      
                                                <% } %>
                                                <%-- Default Components of a Sales Package or Combined Rate Plan --%>
                                                <% if (product.hasDefaultComponents()) { %>
                                                <div class="cat-prd-cmp-grp">
                                                <%  Iterator iterSubItem = product.getCatalogItem().getWebCatSubItemList().iteratorOnlyPopulated();
                                                    while (iterSubItem.hasNext()) {
                                                        WebCatSubItem subItem = (WebCatSubItem) iterSubItem.next(); 
                                                        if (!((subItem.isSalesComponent() || subItem.isRatePlanCombination()) &&
                                                           subItem.isScSelected())) {
                                                            continue; 
                                                        } %>
                                                        <div class="cat-prd-cmp">
                                                     <% for (int j=subItem.getHierarchyLevel()-1; j>0 ; j--) { %>
                                                            &nbsp;
                                                     <% } %>
                                                       <%=JspUtil.encodeHtml(subItem.getDescription())%> 
                                                       </div>                          
                                                 <% } %>
                                                 </div>
                                                <% } %> 
                                                <%-- Prices --%> 
                                                 <div class="b2c-prd-prc-row">
                                                  <% priceUI.setPriceRelevantInfo(product.getCatalogItem(), product.isRelevantForExplosion(), false);  %>
                                                  <%@ include file="pricing.list.inc.jsp" %>
                                                 </div>
                                            </div> <%-- class="cat-prd-facts" --%>
                                            <%-- Eye-Catcher text --%>
                                            <div class="cat-prd-eyec">
                                                <%= JspUtil.encodeHtml(product.getCatalogItem().getEyeCatcherText())%>
                                            </div>
                                            <%-- Details button --%>
                                            <div class="cat-btn-det"> 
                                                <a title="<isa:translate key="b2c.catalog.detailButtonQuick"/>" href="#" 
                                                   onclick="javascript:displayProdDetails('<isa:webappsURL name="<%=baseURL%>"/><%=detailReq%>', '<%=ui.getFormSuffix()%>')" >                                                        
                                                   <isa:translate key="b2c.catalog.detailButton"/>
                                                </a>
                                            </div>
                                            <%-- display quantity and unit --%>
                                            <div class="cat-pcat-facts2">
                                             <% if (ui.isQuantityVisible(product)) { %>  
                                                <%-- quantity --%>
                                                <div class="cat-prd-qty">
                                                     <label for="item[<%=itemno%>].quantity"><isa:translate key="catalog.isa.quantity"/>:</label>
                                                     <input type="<%= (ui.isQuantityChangeable(product)) ? "text" : "hidden" %>" class="textInput" id="item[<%=itemno%>].quantity" size="4" maxlength="8" name="item[<%=itemno%>].quantity" value="<%= product.getQuantityAsStr() %>" />
                                                  <% if (!ui.isQuantityChangeable(product)) { %>
                                                         <%= product.getQuantityAsStr() %>
                                                  <% } %>
                                                     &nbsp;
                                                </div>
                                                <%-- Units --%>  
                                                <div class="cat-prd-unit">
                                                 <% if (product.getUnitsOfMeasurement().length == 1 || product.isRelevantForExplosion()) { 
                                                 	    if (product.getUnitsOfMeasurement().length > 0) { %> <%-- it might happen, that the list is empty even the product is relevant for explosion --%>
                                                        <%= product.getUnitsOfMeasurement()[0].trim() %><input type="hidden" name="item[<%=itemno%>].unit" value="<%= product.getUnitsOfMeasurement()[0].trim()%>"/>
                                                        <% }
                                                    } 
                                                    else { %>
                                                        <select name="item[<%=itemno%>].unit" width=4 onChange=setUnit('<%=ui.getFormSuffix()%>')>
                                                     <% for (int j = 0; j < product.getUnitsOfMeasurement().length; j++) { %>
                                                            <option value="<%= product.getUnitsOfMeasurement()[j].trim()%>"
                                                         <% if (ui.shouldThisUnitBeSelected(product, j)) {%>
                                                               SELECTED
                                                         <% } %>
                                                            ><%= product.getUnitsOfMeasurement()[j]%></option> 
                                                     <% } %>
                                                        </select>
                                                 <% } %>
                                                    &nbsp;
                                                </div>
                                             <% } 
                                                else { %>
                                                <input type="hidden" id="item[<%=itemno%>].quantity" name="item[<%=itemno%>].quantity" value="<%= product.getQuantityAsStr() %>" />
                                                <input type="hidden" id="item[<%=itemno%>].unit" name="item[<%=itemno%>].unit" value="<%= product.getUnitsOfMeasurement()[0].trim()%>" />
                                             <% } %>
                                                
                                                <%-- Button List with different buttons --%>
                                                <div class="cat-btn-lst">
                                             <% if (ui.showSelectedProduct()) { %>
                                                    <a class="FancyLinkGrey" href="javascript:addSingleItem('<%= product.getItemId() %>','addToBasket','<%=ui.getFormSuffix()%>')" >
                                                       <isa:translate key="catalog.button.selprod"
                                                    /></a>
                                             <% }
                                                else { %>
                                                    <span class="imgbtn-box">
                                                      <a class="imgbtn-lk"
                                                         href="javascript:addSingleItem('<%= product.getItemId() %>','addToBasket','<%=ui.getFormSuffix()%>')" >
                                                         <img class="cat-imgbtn-addtocart" src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                                                              alt="<isa:translate key="b2c.product.jsp.addToBasket"/>" width="16" height="16" border="1"
                                                      /></a>
                                                    </span>  
                                             <% } %>
                                             <% if (ui.isAddToLeafletAllowed()) { %>
                                                  <span class="imgbtn-box">
                                                    <a class="imgbtn-lk"
                                                       href="javascript:getMoreFunctions('<%= product.getItemId() %>','leaflet','<%=ui.getFormSuffix()%>')"
                                                      ><img class="cat-imgbtn-addtolfl" 
                                                            src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                                                            alt="<isa:translate key="b2c.product.jsp.addToLeaflet"/>" 
                                                            width="16" height="16" border="1"
                                                    /></a>
                                                  </span>
                                             <% } %>
                                                <%-- Config button --%>
                                                <% if (!product.getCatalogItem().isRelevantForExplosion()) {   
                                                     WebCatItem configItem = product.getCatalogItem(); 
                                                     boolean setAnchor = false; 
                                                     boolean displayAsLink = true; 
                                                     String lastVisited = "itemList"; 
                                                     if (isQuery != null && isQuery.equals("yes")) {
                                                         lastVisited = "catalogQuery";
                                                     }
                                                     String formSuffix =ui.getFormSuffix();%>
                                                     <%@ include file="/catalog/configLink.inc.jsp"%>
                                                <% } %>
                                                </div>

                                                <%-- Select for Compare --%>
                                                <div class="cat-prd-comp">
                                                <% if (ui.isCompareAllowed() && itemPage != null) { %>
                                                     <input type="Checkbox" name="item[<%=itemno%>].selected" value="true" <%= product.isSelectedStr() %> 
                                                     <% if (ui.isSubTitleTrue(product)) { %>
                                                            title="<isa:translate key="catalog.isa.checkboxTooltip"/>"  
                                                     <% }
                                                        else { %>
                                                            title="<isa:translate key="catalog.isa.checkboxTooltipAdmin"/>"  
                                                     <% } %>
                                                     >
                                                     <label for="item[<%=itemno%>].selected"><isa:translate key="b2c.catalog.selectForComparison"/></label>
                                                <% } %>
                                                </div>
                                          
                                            </div> <%-- cat-pcat-facts2 --%>
                                            
                                            <%-- Message field --%>
                                         <% String messageText = null;
                                            if( ( messageText= ui.getMessage(product)) != null) { %>
                                            <div class="cat-prd-msg">
                                                <div class="error"><span><%=JspUtil.encodeHtml(messageText)%></span></div>
                                            </div>
                                         <% } %>
                                          
                                            </div><%-- fw-box-content --%>
                                        </div><%-- fw=box=i3 --%>
                                    </div><%-- fw=box=i2 --%>
                                </div><%-- fw=box=i1 --%>
                                <div class="fw-box-bottom">
                                    <div></div>
                                </div>
                                </div><%-- fw=box=bottom--%>
                            </div><%-- b2c=pcat=hpv=prod --%>
                        </td>
                     <% if (colno == ui.getMaxCols()) {
                            colno = 0; %>
                            </tr>
                     <% }
                    } // itemno < pageSize

                    if (colno > 0) { %>
                        </tr>
                 <% } %>
                </tbody>
            </table>
         <% if (ui.isAccessible()) { %>
                <a id="cat-pcat-blockview-end" href="#cat-pcat-blockview-start" 
                   title="<isa:translate key="b2c.cat.blockview.end.access.inf"/>" 
                   accesskey="<isa:translate key="b2c.cat.blockview.end.access"/>">
                </a>
         <% } %>
        </div><%-- b2c=pcat=hpv --%>

        <script type="text/javascript">
            <%-- used for javascript functions --%>
            count = <%= itemno %>;
        </script>

        <%-- Compare Button --%>
        <% if (ui.isCompareAllowed() && itemPage != null) { %>
             <div class="cat-btn-com">
               <% accessText = WebUtil.translate(pageContext, "catalog.isa.compareItems", null); %>     
               <input type="button" title="<isa:translate key="access.button" arg0="<%=accessText%>" arg1=""/>" value="<isa:translate key="catalog.isa.compareItems"/>" onclick="javascript:popUpCompare(<%= ui.getAmountOfSelectedItems(pageNo) %>)" class="FancyButtonGrey">
             </div>
        <% } %>

        <%-- Check, if the productlist contains more products --%>
        <% if (ui.showPage() && itemPage != null) { %>
            <%@ include file="PageLink.inc.jsp" %>
        <% } else if (ui.getProductList().size() > ui.pageSize()) {
               String myAction = "catalog/" + ui.getMoreFoundAction(); %>
        <table border="0" cellpadding="4" cellspacing="0" width="100%" DataTable="0">
            <tbody>
                <tr>
                    <td colspan="4" align="right">
                        <br>
                        <a href="<isa:webappsURL name="<%=myAction%>"/> "><%=ui.getMoreFoundMessage()%></a>
                    </td>
                </tr>
            </tbody>
        </table>
        <% } %>        
    </div><%-- b2c=pcat --%>
 <% }
    else { %>
	   <br />
	   <div class="areainfo"><isa:translate key="catalog.isa.NoItems"/></div>
 <% } %>
</form>
