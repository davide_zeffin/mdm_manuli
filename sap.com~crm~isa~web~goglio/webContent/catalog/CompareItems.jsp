<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page language="java" %>
<%@ page import="com.sap.isa.catalog.uiclass.CompareItemsISAUI" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatSubItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItemPrice" %>
<%@ page import="com.sap.isa.isacore.uiclass.PricingBaseUI" %>
<%@ page import="com.sap.isa.core.ui.context.GlobalContextManager" %>
<%@ page import="com.sap.isa.core.ui.context.ContextManager" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>
<jsp:useBean id="theItems" scope="request" type="java.util.ArrayList" />
<jsp:useBean id="theNames" scope="request" type="java.util.Iterator" />
<isa:stylesheets group="catalog"/>

<%  CompareItemsISAUI ui = new CompareItemsISAUI(pageContext); %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <title><isa:translate key="catalog.isa.compareTitle"/></title>
        <isa:stylesheets/>
        <%@ include file="js/CompareItems.js.inc.jsp" %>
        <%@ include file="pricing.java.inc.jsp" %> 
        <%@ include file="transferToOrder.inc.jsp" %>
        
        <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
            <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' type = "text/javascript"></script>
        </isacore:ifShopProperty>
    </head>
    
    <body>
 
        <div class="module-name"><isa:moduleName name="catalog/CompareItems.jsp" /></div>
 <% if (ui.isAccessible()) {%>
        <a href="#b2ccompareitems-end" id="b2ccompareitems-start" title="<isa:translate key="b2c.cat.compareitems"/>" accesskey="<isa:translate key="b2c.cat.compareitems.access"/>" tabindex="0"></a>
 <% }
    String grpTxt = WebUtil.translate(pageContext, "catalog.isa.productCompare", null); 
    if (ui.isAccessible()){ %>
        <a href="#groupcompareitems-end" id="groupcompareitems-begin" title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>" tabindex="0"></a>
 <% } %>
 
    <%-- Please do not change the id of the form. This is used in CatalogScripts.js.inc.jsp to identify this window --%>
    <form id ="CompareItemsForm" name="compareform" action="<isa:webappsURL name="/catalog/updateItems.do"/>" method="post">
        <input type="hidden" name="next" value="" />
        <input type="hidden" name="itemkey" value="" />
        <input type="hidden" name="addtobasketforward" value="showCatalogArea" /> <%-- forward for add to basket --%>
        <input type="hidden" name="addtoleafletforward" value="showCatalogArea" /> <%-- forward for add to leaflet --%>
        <% if (ui.isCompareCUA()) { %>
		<input type="hidden" name="<%=ActionConstants.RA_COMPARE_CUA%>" value="true">
		<input type="hidden" name="<%=ActionConstants.RA_DETAILSCENARIO %>" value="<%=JspUtil.encodeHtml(ui.getDetailScenario()) %>">
        <% } %>
        
     <% String query = request.getParameter(ActionConstants.RA_CURRENT_QUERY);
        if (null==query && null!=request.getAttribute(ActionConstants.RA_CURRENT_QUERY)) {
            query = request.getAttribute(ActionConstants.RA_CURRENT_QUERY).toString();
        }
        if (null==query && null!=GlobalContextManager.getValue(ActionConstants.CV_CURRENT_QUERY)) {
            ContextManager contextManager =  ContextManager.getManagerFromRequest(request);
            query = contextManager.getContextValue(ActionConstants.CV_CURRENT_QUERY);
        }
        if (null==query) {
            query = "";
        }
        query = JspUtil.encodeHtml(query); %>
        
        <input type="hidden" name="<%=ActionConstants.RA_CURRENT_QUERY%>" value='<%=query%>' />
  
  
     <% String groupKey = null;
        if(request.getAttribute("groupKey") != null) {
           groupKey = request.getAttribute("groupKey").toString(); %>
           <input type="hidden" name="groupKey" value='<%=groupKey%>' />
     <% } %>

    <div id="cat-pcat-prd-comp">
    
<%  String[] evenOdd = new String[] { "app-std-tbl-even", "app-std-tbl-odd" };
    int evenOddValue = 1;
    int colno = 0;
    int rowno = 0;
    colno = theItems.size() + 1;
    String columnNumber = Integer.toString(colno);
    String rowNumber = Integer.toString(rowno);
    String allRows = Integer.toString(rowno);
//	String tableTitle = WebUtil.translate(pageContext, "b2b.cat.UpSelling.table.title", null);
    WebCatItem item = null;
    int firstColumnSize=200;

    if (ui.isAccessible()){ %>
       <a id="compareitemtable-begin" href="#compareitemtable-end" 
       title="<isa:translate key="access.table.begin" arg0="<%=rowNumber%>" arg1="<%=columnNumber%>" arg2="1" arg3="<%=allRows%>"/>" tabindex="0">
       </a>
 <% } %>
       
    <table id="itemstable" class="app-std-tbl">
       <thead>
         <tr>
<%-- column header --%> 
            <% colno = 2;
               String columnid = "";
               java.util.Iterator itemsIterator = theItems.iterator();
               int width = 80 / theItems.size(); %>
               <td headers="col_1" width="<%=firstColumnSize%>">&nbsp;</td>
            <% while (itemsIterator.hasNext()) {
                  item = (WebCatItem) itemsIterator.next();
                  columnid = "col_" + Integer.toString(colno);
                  colno++; 
                  if((colno-2) == theItems.size()) { %>
                     <th id="<%=columnid%>" class="last" scope="col" width="<%=width%>" title="<%=JspUtil.encodeHtml(item.getDescription())%>" tabindex="0"><%=JspUtil.encodeHtml(item.getDescription())%></th>        
               <% } else { %>
                     <th id="<%=columnid%>" scope="col" width="<%=width%>" title="<%=JspUtil.encodeHtml(item.getDescription())%>" tabindex="0"><%=JspUtil.encodeHtml(item.getDescription())%></th>        
               <% } 
            }%>           
         </tr>
       </thead>
       <tbody>
<%-- display the picture --%>
        <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
             <td headers="col_1" width="<%=firstColumnSize%>">&nbsp;</td>
          <% colno = 2;
             itemsIterator=theItems.iterator();
             while (itemsIterator.hasNext()) {
                 item = (WebCatItem) itemsIterator.next();
                 pageContext.setAttribute("item",item);
                 columnid = "col_" + Integer.toString(colno);
                 colno++; 
                 if((colno-2) == theItems.size()) { %> 
                    <td headers="<%=columnid%>" class="last">
              <% } else { %>
                    <td headers="<%=columnid%>">
              <% } %>     
                       <isa:ifThumbAvailable name="item">
                           <div class="cat-prd-thumb">
                               <img alt="<%=item.getDescription()%>" 
                                    src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="item" />" />
                           </div>
                       </isa:ifThumbAvailable>
                 </td>
          <% } %>
        </tr>
            
<%-- display the product Id --%>
        <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
             <td headers="col_1" width="<%=firstColumnSize%>">&nbsp;</td>
          <% colno = 2;
             itemsIterator=theItems.iterator();
             while (itemsIterator.hasNext()) {
                item = (WebCatItem) itemsIterator.next();
                columnid = "col_" + Integer.toString(colno);
                colno++; 
                if((colno-2) == theItems.size()) { %> 
                   <td headers="<%=columnid%>" class="last">
             <% } else { %>     
                   <td headers="<%=columnid%>">     
             <% } %>       
                      <div class="cat-prd-id">
                         <%= item.getProduct()%>
                      </div> 
                </td>
          <% } %>      
        </tr>
           
<%-- display the prices for the main component (case of combined rate plan ) or for the product (no crp) --%>
   <% CompareItemsISAUI pricingProductsUI = ui;
      CompareItemsISAUI priceUI = ui;
      if (priceUI.isPriceVisible()) { %> 
         <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
             <td headers="col_1" width="<%=firstColumnSize%>">&nbsp;</td>
          <% colno = 2;
             itemsIterator=theItems.iterator();
             while (itemsIterator.hasNext()) {
                 item = (WebCatItem) itemsIterator.next();
                 columnid = "col_" + Integer.toString(colno);
                 colno++;
                 if(colno-2 == theItems.size()) { %>
                     <td headers="<%=columnid%>" class="last" align="right">
              <% } 
                 else { %>     
                     <td headers="<%=columnid%>" align="right">
              <% } %>
              <% if( ui.shouldRatePlanCombinationsBeDisplayed() ) { %>          
                     <table>
                       <tr>
                         <td align="right">
              <% }           
                 priceUI.setPriceRelevantInfo(item, ui.getPriceCategoryToDisplay(item), false); %>
                 <%@ include file="pricing.list.inc.jsp" %>
              <% if( ui.shouldRatePlanCombinationsBeDisplayed() ) { %>          
                         </td>
                       </tr>
                     </table>
              <% } %>
                  </td>
          <% } %>
         </tr>
      <% } %>
         
<%-- for a combined rate plan display the prices of all rate plan combinations --%>
<%       if(item.isCombinedRatePlan()) { %> 
         <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
             <td headers="col_1" width="<%=firstColumnSize%>">
                 <div class="cat-prd-cntdur-lbl">
                      <isa:translate key="b2c.cat.compare.rpc"/>&nbsp;
                 </div>
             </td>    
          <% colno = 2;
             itemsIterator=theItems.iterator();
             while (itemsIterator.hasNext()) {
                 item = (WebCatItem) itemsIterator.next();
                 columnid = "col_" + Integer.toString(colno);
                 colno++; 
                 if(colno-2 == theItems.size()) { %>
                     <td headers="<%=columnid%>" class="last">
              <% } 
                 else { %>     
                     <td headers="<%=columnid%>">
              <% } %>
                 <table class="cat-pcat-prd-comp-rpc-ltab">
                  <% Iterator ratePlanCombinations = ui.getRatePlanCombinations(item);
                     if(ratePlanCombinations != null ) {       
                         while(ratePlanCombinations.hasNext()) {
                             item = (WebCatItem) ratePlanCombinations.next(); 
                             priceUI.setPriceRelevantInfo(item, ui.getPriceCategoryToDisplay(item), false); %>
                             <tr>
                               <td class="cat-pcat-prd-comp-rpc-ltab-dsc">
                                 <div class="cat-prd-dsc"><%=item.getDescription() %></div>
                               </td>
                               <td class="cat-pcat-prd-comp-rpc-ltab-prc">
                                 <%@ include file="pricing.list.inc.jsp" %>
                               </td>
                             </tr>
                      <% }
                     } %>
                 </table>
                </td>
          <% } %>    
         </tr>
      <% } %>
        
<%-- display the selected contract duration if available --%>
     <% if(ui.shouldContractDurationBeDisplayed()) { %>
            <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
               <td headers="col_1" width="<%=firstColumnSize%>">
                   <div class="cat-prd-cntdur-lbl">
                       <isa:translate key="b2c.contractDuration"/>:&nbsp;
                   </div>
               </td>    
               <% int it=-1;
                  colno = 2;
                  itemsIterator=theItems.iterator();
                  while (itemsIterator.hasNext()) {
                     item = (WebCatItem) itemsIterator.next();
                     it++;
                     if (item.getSelectedContractDuration() == null) {
                        continue;
                     }
                     columnid = "col_" + Integer.toString(colno);
                     colno++; 
                     if((colno-2) == theItems.size()) { %>
                        <td headers="<%=columnid%>" class="last">
                  <% } else { %>     
                        <td headers="<%=columnid%>">
                  <% } %>
                            <input type="hidden" name="item[<%= it %>].selectedContractDuration" value="<%= item.getSelectedContractDuration().toString(ui.getLocale()) %>" />
                            <div class="cat-prd-cntdur">
                                <%= item.getSelectedContractDuration().toString(ui.getLocale()).replaceAll(" ","&nbsp;") %>
                            </div>
                        </td>
                <% } %>    
           </tr>
     <% } %>

<%-- row for showing inline configuration -- show inline configuration only if there are configured products--%>
    <% if(ui.shouldInlineConfigurationBeDisplayed() ) { %>
           <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
               <td headers="col_1" width="<%=firstColumnSize%>">
                  <div class="cat-prd-cfg-lbl">
                     <isa:translate key="b2c.catalog.isa.inlineConfiguration"/>:
                  </div>
               </td>
            <% itemsIterator=theItems.iterator();
               colno = 2;               
               while (itemsIterator.hasNext()) {
                  item = (WebCatItem) itemsIterator.next();
                  columnid = "col_" + Integer.toString(colno);
                  colno++;
                  if((colno-2) == theItems.size()) { %>
                     <td headers="<%=columnid%>" class="last">
               <% } else { %>      
                     <td headers="<%=columnid%>">
               <% }      
                  if ( item.isConfigurable() == true) { //theses variables should be defined before to call itemconfiginfo.inc.jsp
                      int viewType = 2; 
                      boolean isSubItem = false;
                      IPCItem itemConfig = item.getConfigItemReference(); %>
                       <div class="cat-prd-cfg">
                          <%@ include file="/catalog/itemconfiginfo.inc.jsp" %>
                       </div> 
               <% } %>
                     </td>               
            <% } %>
           </tr>
    <% } %>    
 
<%-- display addToBasket and andToLeaflet button only for package and non package product and not for package component. --%>
   <%  if( ui.shouldAddButtonsBeDisplayed() ) { %>
           <tr class="<%= evenOdd[evenOddValue++ % 2]%>"><%-- row for add to basket form elements --%>
             <td headers="col_1" width="<%=firstColumnSize%>">&nbsp;</td>
            <% int it=-1;
               colno = 2;
               itemsIterator=theItems.iterator();
               while (itemsIterator.hasNext()) {
                   item = (WebCatItem) itemsIterator.next();
                   it++;
                   columnid = "col_" + Integer.toString(colno);
                   colno++; 
                   if((colno-2) == theItems.size()) { %>
                      <td headers="<%=columnid%>" class="last">
                <% } else { %>   
                       <td headers="<%=columnid%>">
                <% } %>   
                      <div class="b2c-btn-bsk">
                         <input type="hidden" name="item[<%= it %>].unit" value="<%= item.getUnit() %>" />
                         <input type="hidden" name="item[<%= it %>].quantity" value="1" />
                         <% if (!item.isContractSubItem()) { %>
                            <% if (ui.showSelectedProduct()) { %>
                               <a class="FancyLinkGrey" title="<isa:translate key="catalog.button.selprod"/>" href="#" onClick="addSingleItemForCompare('<%= item.getItemID() %>', '<%= ui.domainRelaxationRequired() %>', '<%= ui.isCompareCUA() %>')">
                                  <isa:translate key="catalog.button.selprod"
                               /></a>
                            <% }
                               else { %>
                               <span class="imgbtn-box">
                                 <a class="imgbtn-lk" title="<isa:translate key="b2c.cat.additem"/>" href="#" onClick="addSingleItemForCompare('<%= item.getItemID() %>', '<%= ui.domainRelaxationRequired() %>', '<%= ui.isCompareCUA() %>')">
                                    <img class="cat-imgbtn-addtocart" src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                                      alt="<isa:translate key="catalog.isa.addToBasket"/>" tabindex="0" width="16" height="16" border="1"
                                 /></a>
                               </span>
                            <% } %>
                            <% if (ui.isAddToLeafletAllowed()) { %>
                            <span class="imgbtn-box">
                              <a class="imgbtn-lk" title="<isa:translate key="b2c.cat.addleaflet"/>" href="#" onClick="getMoreFunctionsForCompare('<%= item.getItemID() %>','leaflet')">
                                <img class="cat-imgbtn-addtolfl" 
                                     src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                                     alt="<isa:translate key="catalog.isa.leaflet"/>" 
                                     width="16" height="16" border="1"
                              /></a> 
                            </span>  
                            <% } %>
                         <% } 
                            else { %>
                         <span class="imgbtn-box">
                           <a class="imgbtn-lk" title="<isa:translate key="b2c.cat.additem"/>" 
                              href="#" onClick="addSingleContractItemForCompare('<%= item.getItemID() %>', '<%= item.getContractRef().getContractKey().toString() %>', '<%= item.getContractRef().getItemKey().toString() %>')"
                             ><img class="cat-imgbtn-addtocart" 
                                  src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                                  alt="<isa:translate key="catalog.isa.addToBasket"/>" 
                                  tabindex="0"
                                  width="16" height="16" border="1"
                           /></a>
                         </span>  
                         <% } %>
                         
                         
                      </div>
                   </td>
            <% } %>
           </tr>
   <%  } %>    
  
 <%-- details --%>
    <% while (theNames.hasNext()) {
          com.sap.isa.catalog.boi.IAttribute name = (com.sap.isa.catalog.boi.IAttribute) theNames.next();
          if (!name.isBase() && name.getDescription() != null && name.getName().indexOf("_") != 0) { %>
              <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
                 <td headers="col_1" width="<%=firstColumnSize%>" class="cat-prd-cfg-lbl"><%= JspUtil.encodeHtml(name.getDescription()) %></td>
                  <% itemsIterator=theItems.iterator();
                     colno = 1;
                     while (itemsIterator.hasNext()) {
                         item = (WebCatItem) itemsIterator.next();
                         columnid = "col_" + Integer.toString(colno);
                         colno++; %>
                         <td class="odd" headers="<%=columnid%> allcolumns"><%= item.getAttribute(name.getGuid()) %>
                         <% if ( !item.getAttribute("_"+name.getName()).equals("") ) { %>
                               <%= item.getAttribute("_"+name.getName()) %>
                         <% } %>
                         </td>
                  <% } %>
              </tr>
       <% } else {
               String st = ui.parseAttributeName("catalog.isa.attribute",name.getGuid());
               if (st != null) { %>
                 <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
                      <td headers="col_1" width="<%=firstColumnSize%>" class="cat-prd-cfg-lbl"><%= st %></td>
                   <% itemsIterator=theItems.iterator();
                      colno = 1;
                      while (itemsIterator.hasNext()) {
                         item = (WebCatItem) itemsIterator.next();
                         columnid = "col_" + Integer.toString(colno);
                         colno++; %>
                         <td headers="<%=columnid%> allcolumns" class="odd"><%= item.getAttribute(name.getGuid()) %></td>
                   <% } %>
                   </tr>
             <%}
           } 
       } %>     

<%-- row for a new compare selection --%>
       <tr class="<%= evenOdd[evenOddValue++ % 2]%>">
             <td headers="col_1" width="<%=firstColumnSize%>">
                <div class="cat-prd-comp-lbl">
                   <isa:translate key="b2c.catalog.isa.selectNewComparison"/>:
                </div>
             </td>
          <% String checkboxid = "";
             colno = 2;
             int it=-1;
             itemsIterator=theItems.iterator();
             Object o = null;
             WebCatSubItem subItem = null; //we should set topItemID for package component comparison
             while (itemsIterator.hasNext()) {
                o = itemsIterator.next();
                if(o instanceof WebCatSubItem && subItem == null) {
                    subItem = (WebCatSubItem)o;
                }
                item = (WebCatItem) o;
                it++;
                columnid = "col_" + Integer.toString(colno);
                checkboxid = "item_" + Integer.toString(colno);
                colno++; 
                if((colno-2) == theItems.size()) { %>
                   <td headers="<%=columnid%>" class="last">
             <% } else { %>      
                   <td headers="<%=columnid%>">
             <% } %>     
                      <div class="cat-prd-comp">
                         <input type="checkbox" id="<%=checkboxid%>" title="<isa:translate key="catalog.isa.checkboxTooltip"/>" name="item[<%= it %>].selected" value="true" <%= item.isSelectedStr() %> tabindex="0"/>
                         <% if( ui.isAccessible() ) {%>
                         <label for="<%=checkboxid%>"><isa:translate key="catalog.isa.checkboxTooltip"/></label>
                         <% } %>
                         &nbsp;&nbsp;
                         
                <% if(subItem != null) { %>
                         <input type="hidden" name="item[<%= it %>].itemID" value="<%= item.getTechKey() %>" /> 
                         <input type="hidden" name="item[<%= it %>].topItemID" value="<%= subItem.getTopParentItem().getItemKey().getItemID()%>" />
                         <input type="hidden" name="item[<%= it %>].unit" value="<%= subItem.getUnit() %>" />
                         <input type="hidden" name="item[<%= it %>].quantity" value="<%= subItem.getQuantity() %>" />
                <% } 
                else { %>   
                   <input type="hidden" name="item[<%= it %>].itemID" value="<%= item.getItemID() %>" />
                   <% if (item.isContractSubItem()) { %>
                         <input type="hidden" name="item[<%= it %>].contractKey" value="<%= item.getContractRef().getContractKey().toString() %>" />
                         <input type="hidden" name="item[<%= it %>].contractItemKey" value="<%= item.getContractRef().getItemKey().toString() %>" />
             <% } %>
             <% } %>
                      </div>  
                   </td>                        
          <% } %>      
        </tr>


    </tbody>
 </table>
<% if (ui.isAccessible()){ %>
      <a id="compareitemtable-end" href="#compareitemtable-begin" title="<isa:translate key="access.table.end"/>"></a>
<% } %>
  </div>
 </form>
 <br />
   <p>
     <div class="cat-btn-com">
         <input type="button" 
                title="<isa:translate key="catalog.isa.close"/>" 
                value="<isa:translate key="catalog.isa.close"/>"
                onclick="window.close()" 
                class="FancyButtonGrey"/>

         <input type="button" 
                title="<isa:translate key="catalog.isa.newCompare"/>" 
                value="<isa:translate key="catalog.isa.newCompare"/>"
                onclick="javascript:setNext('compareSameWnd')" 
                class="FancyButtonGrey"/>
     </div>
   </p>
      <% if (ui.isAccessible()){ %>
            <a href="#groupcompareitems-begin" id="groupcompareitems-end" title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>" tabindex="0"></a>    
      <% }
         if (ui.isAccessible()){ %>
            <a href="#b2compareitems-start" id="b2ccompareitems-end" tabindex="0"></a>        
      <% } %>

  </body>
</html>