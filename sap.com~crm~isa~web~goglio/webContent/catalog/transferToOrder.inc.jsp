<%-- 
*****************************************************************************

    Include:      transferToOrder.inc.jsp
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      19.03.2008

***************************************************************************** 

*	This include checks the request attributes for an item to be transferred to 
*   an external order. This is created in the standalone catalog scenario, when the
*   AddToBasketAction is performed. If such an item is found, a method in the
*   CatalogScripts.js.inc.jsp is called, which is normally included in the
*   navigationbarCat.inc.jsp.
*	
--%>

<%@ page import="com.sap.isa.catalog.uiclass.TransferToOrderUI" %>

<%  TransferToOrderUI transferToOrderUI = new TransferToOrderUI(pageContext); 
    if (transferToOrderUI.hasTransferOrderItem()) { %>
        <script type="text/javascript">
			//<!--  		
            CatalogScripts.transferToOrder(
                "<%= transferToOrderUI.getProductId() %>", 
                "<%= transferToOrderUI.getProductGuid() %>", 
                "<%= transferToOrderUI.getQuantity() %>", 
                "<%= transferToOrderUI.getUnit() %>", 
                "<%= transferToOrderUI.getConfigXmlEncodedB64() %>",
                "<%= transferToOrderUI.getCountry() %>",
                "<%= transferToOrderUI.getCatalogConfigId() %>",
                "<%= transferToOrderUI.getLanguage() %>");
            //-->
        </script>
<%  } %>	
