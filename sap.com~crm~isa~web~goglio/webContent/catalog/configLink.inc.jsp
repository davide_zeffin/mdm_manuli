<%-- 
*****************************************************************************

    Include:      configLink.inc.jsp
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      13.04.2006

***************************************************************************** 

 Show config links
 
 Required global variables:
 ==========================
 WebCatItem configItem
                                
 usage:
 ======
   <% WebCatItem configItem = item; 
      boolean setAnchor = false;
      boolean displayAsLink=true ;
      String lastVisited = "itemDetails";
      String formSuffix = "";%>
   <%@ include file="/catalog/configLink.inc.jsp"%>
  
*****************************************************************************  
--%>  
      <%-- old fashion config --%>
      <% if (configItem.isItemOldFashionConfigurable()) { %> 
      <div class="cat-pcat-cfg">          
         <% String ressourceText;
            if (configItem.isConfiguredCompletely()) {  
                ressourceText = "b2c.catalog.msg.OptionsCanSelect"; 
            } 
            else { %>
                <span>
                  <img class="cat-cfg-err" 
                       src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                       alt="<isa:translate key="b2c.cat.conf.err"/>" 
                  />
                </span>
             <% ressourceText = "b2c.catalog.msg.OptionsMustSelect"; 
            } %>
            <span class="cat-pcat-cfg-txt">
         <% if (configItem.getParentTechKey() == null && !configItem.isContractSubItem()) { // normal item 
                if(displayAsLink) { %>
                   <a <% if (setAnchor) { %> name="basketAnchor" <% } %> href="javascript:configureItem('<%= configItem.getItemId() %>', 'config', '<%=formSuffix%>','<%=lastVisited%>')">
             <% } %>
                <isa:translate key='<%= ressourceText %>'/>
             <% if(displayAsLink) { %>
                   </a>
             <% }
            }
            else if (configItem.isContractSubItem()) {  // contract sub item
                if(displayAsLink) { %>
                 <a <% if (setAnchor) { %> name="basketAnchor" <% } %> href="javascript:configureContractItem('<%= configItem.getItemId() %>', '<%= configItem.getContractRef().getContractKey().toString() %>', '<%= configItem.getContractRef().getItemKey().toString() %>', 'config', '<%=formSuffix%>','<%=lastVisited%>')">
             <% } %>  
                <isa:translate key='<%= ressourceText %>'/>
             <% if(displayAsLink) { %>   
                   </a>
             <% }
            } 
            else {  // solution configurator sub item
                if(displayAsLink) { %>
                   <a <% if (setAnchor) { %> name="basketAnchor" <% } %> href="javascript:configureSubItem('<%= configItem.getTechKey() %>', '<%= configItem.getTopParentItem().getItemID() %>', 'config', '<%=formSuffix%>','<%=lastVisited%>')">
             <% } %>  
                <isa:translate key='<%= ressourceText %>'/>
             <% if(displayAsLink) { %>   
                   </a>
             <% }
            } %>
            </span>
        </div>   
      <% } else if (configItem.isProductVariant()) { %>
        <%-- product variant --%>
        <div class="cat-pcat-cfg">
           <% if (configItem.getParentTechKey() == null) { 
                if(displayAsLink) { %>
                   <a <% if (setAnchor) { %> name="basketAnchor" <% } %> href="javascript:configureItem('<%= configItem.getItemId() %>','config_view', '<%=formSuffix%>', '<%=lastVisited%>')">
             <% } %>      
                <isa:translate key="b2c.catalog.button.productoptions"/>
             <% if(displayAsLink) { %>
                </a>
             <% }  
            }
            else { 
                if(displayAsLink) { %>
                   <a <% if (setAnchor) { %> name="basketAnchor" <% } %> href="javascript:configureSubItem('<%= configItem.getTechKey() %>', '<%= configItem.getTopParentItem().getItemID() %>', 'config_view', '<%=formSuffix%>','<%=lastVisited%>')">
             <% } %> 
                <isa:translate key="b2c.catalog.button.productoptions"/>
             <% if(displayAsLink) { %>   
                   </a>
             <% }   
            } %>
        </div>
     <%  } %>