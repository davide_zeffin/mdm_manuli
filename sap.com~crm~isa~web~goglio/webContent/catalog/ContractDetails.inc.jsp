<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />

<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%-- obsolete, the contract attribute list is now read from the webcatitemlist
<jsp:useBean    id="refNames"
                type="com.sap.isa.businessobject.contract.ContractAttributeList"
                scope="request" >
</jsp:useBean>
--%>

<%
    boolean isAlone=(currentItem.getContractItems() == null || currentItem.getContractItems().size() == 0);
    java.util.Iterator refNamesIterator = null;
%>
<div id="cat-pcat-contr">
  <div class="module-name"><isa:moduleName name="catalog/ContractDetails.inc.jsp" /></div>

  <div class="cat-pcat-cua-cont">
    <div class="fw-box-cua-cont"><div class="fw-box-top-cua-cont"><div></div></div><div class="fw-box-i1-cua-cont"><div class="fw-box-i2-cua-cont"><div class="fw-box-i3-cua-cont"><div class="fw-box-content-cua-cont">

      <input type="hidden" name="next" value="" />
      <input type="hidden" name="itemkey" value="" />
      <input type="hidden" name="contractkey" value="" />
      <input type="hidden" name="contractitemkey" value="" />

<%-- <% /* get user session data object. Use different names to avoid conflicts with following include!! */
            UserSessionData uSD =
                UserSessionData.getUserSessionData(request.getSession());
                CUAHeaderUI headerUI = new CUAHeaderUI(pageContext);
        request.setAttribute("currentItem",currentItem);
        String activeView = "contracts";
        headerUI.setContracts(true);
        %>
        <%@ include file="CUAHeader_Menu.inc.jsp" %> --%>

<% int iteratorSize = ui.getContractAttributeListSize(); 
   int iteratorTemp = 0; %>
		
      <div class="opener cat-pcat-contr-opener">
        <isa:translate key="catalog.isa.contractDataTo"/> <%= JspUtil.encodeHtml(currentItem.getAttribute("OBJECT_DESCRIPTION")) %>
      </div>
		
      <table class="app-std-tbl">
        <tr>
          <th class="cat-prd-qty"><isa:translate key="catalog.isa.quantity"/></th>
          <th class="cat-btn-lst"><isa:translate key="catalog.isa.buy"/></th>
          <th class="cat-prd-contrid"><isa:translate key="catalog.isa.contractID"/></th>
          <th
<%  refNamesIterator = ui.getContractAttributeList();
    if (! refNamesIterator.hasNext()) { %>
              class="app-std-tbl-th-last cat-prd-contritemid"
<%  } %>          
          >
            <isa:translate key="catalog.isa.contractItemID"/>
          </th>
<%  while (refNamesIterator.hasNext()) {
        com.sap.isa.businessobject.contract.ContractAttribute attr = (com.sap.isa.businessobject.contract.ContractAttribute) refNamesIterator.next();
    iteratorTemp = iteratorTemp + 1;
    if(iteratorTemp == iteratorSize) { %>
          <th class="app-std-tbl-th-last">
     <% }
        else { %>
      <th>
     <% } %>
          <%= JspUtil.encodeHtml(attr.getDescription()) %></th>
<%  } %>
        </tr>
<%  String rowColor = "app-std-tbl-odd";
    java.util.ArrayList contractRefs = currentItem.getContractItemsArray();
    java.util.Iterator contractItems=contractRefs.iterator();
    int contractIndex = 0;
    while (contractItems.hasNext()) {
        WebCatItem subItem=(WebCatItem)contractItems.next();
        if (rowColor.equals("app-std-tbl-even")) rowColor="app-std-tbl-odd";
        else rowColor="app-std-tbl-even";
        if (subItem.getContractDetailedRef() == null) {
            continue;
        }
        int q=-1;
%>
        <tr class="<%= rowColor %>">
          <div style="display: none">
            <%--  <input type="hidden" name="item[<%= contractIndex %>].itemID" value="<%= JspUtil.encodeHtml(currentItem.getItemID()) %>"> --%>
            <input type="hidden" name="item[<%= contractIndex %>].contractKey" value="<%= subItem.getContractRef().getContractKey().toString() %>">
            <input type="hidden" name="item[<%= contractIndex %>].contractItemKey" value="<%= subItem.getContractRef().getItemKey().toString() %>">
          </div>
          <td><input type="text" class="textInput" size="4" name="item[<%= contractIndex %>].quantity" value="<%= JspUtil.encodeHtml(subItem.getQuantity()) %>"></td>
          <td>
            <span class="imgbtn-box">
              <a class="imgbtn-lk" href="javascript:addSingleContractItem('<%= JspUtil.encodeHtml(currentItem.getItemID()) %>','<%= subItem.getContractDetailedRef().getContractKey().toString()%>','<%= subItem.getContractDetailedRef().getItemKey().toString()%>','addToBasket','')"
                ><img class="cat-imgbtn-addtocart"
                      src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                      alt="<isa:translate key="catalog.isa.addToBasket"/>" width="16" height="16" border="1"
              /></a>
            </span>
          </td>
          <td><%= JspUtil.encodeHtml(subItem.getContractDetailedRef().getContractId()) %></td>
          <td><%= JspUtil.encodeHtml(subItem.getContractDetailedRef().getItemID()) %></td>
<%      refNamesIterator = ui.getContractAttributeList();
        while (refNamesIterator.hasNext()) {
            refNamesIterator.next();
            q++; 
%>
          <td
<%          if (q == subItem.getContractDetailedRef().getAttributeValues().size()-1) { %>
              class="app-std-tbl-td-last"
<%          } %>           
          >
<%          if( subItem.getContractDetailedRef().getAttributeValues().size() > q) { %>
            <%= JspUtil.encodeHtml(subItem.getContractDetailedRef().getAttributeValues().get(q).getValue()+" "+subItem.getContractDetailedRef().getAttributeValues().get(q).getUnitValue()) %>
<%          } else { %>
            &nbsp;
<%          } %>
          </td>
<%      } %>
        </tr>
<%  } %>
      </table>
    </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-cua-cont"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
  </div> <%-- end: cat-pcat-cua-cont --%>
</div>