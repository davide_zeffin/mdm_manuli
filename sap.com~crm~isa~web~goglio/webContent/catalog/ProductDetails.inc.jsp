<%@ page language="java" %>
<%  response.setHeader("Pragma", "");
    response.setHeader("Cache-Control", "public"); %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.isacore.action.b2c.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="com.sap.isa.catalog.AttributeKeyConstants" %>
<%@ page import="com.sap.isa.catalog.uiclass.ProductDetailsUI" %>
<%@ page import="com.sap.isa.catalog.uiclass.CatalogBaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.PricingBaseUI" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.CatalogBlockViewUI" %>
<%@ page import="com.sap.isa.businessobject.item.ContractDuration" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowCUABaseAction" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatInfo" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>

<%  ProductDetailsUI ui = new ProductDetailsUI(pageContext);
	ProductDetailsUI priceUI = ui;  
	
	WebCatItem currentItem = ui.getItem();
	Iterator itemAttributesIterator = ui.getItemAttributesIterator(); 
	
	Hashtable htTerms = ui.getSearchTerms(ui.getCurrentQuery());
	String grpTxt = WebUtil.translate(pageContext, "catalog.isa.productTitle", new String[] {currentItem.getDescription()}); 

//GREENORANGE BEGIN
	UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());

	//get Internet Sales core functionality BOM
	BusinessObjectManager isaBOM =
		(BusinessObjectManager) userSessionData.getBOM(
			BusinessObjectManager.ISACORE_BOM);

	String current_user = isaBOM.getUser().getUserId().toUpperCase();	
	String USER_GUEST = (String)userSessionData.getAttribute("user_guest");
//GREENORANGE END	

	if (ui.IsDetailInWindow()) { 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<isa:stylesheets group="catalog"/>
  </head>
  <body>	
<%  } %>

<%@ include file="/catalog/transferToOrder.inc.jsp" %>
    <script src="<isa:webappsURL name="catalog/js/pricing.inc.js"/>" type="text/javascript"></script> 

<%-- The attribute SharedConst.BASKET_LOADED_DIRECT is used to determine if the item should
 be added directly to the basket. This is done when the basket has to be displayed directly
 in case of web crawler access to the shop. This happens only once when the basket has not been displayed
 because as soon as the basket is displayed the above attribute is turned to true. 
--%>
  
<%@ include file="pricing.java.inc.jsp" %>    
  
    <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
      <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' type = "text/javascript"></script>
    </isacore:ifShopProperty>
   
    <div id="cat-pcat">
      <div class="module-name"><isa:moduleName name="catalog/ProductDetails.inc.jsp" /></div>
<%  if (ui.isAccessible()) { %>
      <a href="#groupproductdetail-end"	id="groupproductdetail-begin" title="<isa:translate key="access.grp.begin" arg0="<%= grpTxt %>"/>"></a>
<%  } %>
      <div id="cat-pcat-prd-det">   

<%-- for package components and dependent components we will display : associated product for PACKAGE--%>
<%  String title = WebUtil.translate(pageContext, "catalog.isa.productTitle",new String[]{currentItem.getDescription()});
    if ( currentItem.isSubComponent()) {
        if(currentItem.getParent() != null) {
            title = WebUtil.translate(pageContext, "b2c.catalog.isa.associatedProduct",new String[]{}) +" "+ JspUtil.encodeHtml(currentItem.getParent().getDescription(), new char[] {'\''});
        }
    } 
%>    
        <%-- Top of page --%>
        <a id="b2c-prd-cmp-top"></a>
 
        <form name="productform" 
              action="<isa:webappsURL name="/catalog/updateItems.do"/>" 
              method="POST">
        
          <input type="hidden" name="lastVisited" value=""/>
          <input type="hidden" name="displayedTab" value="<%= ui.getLastActiveTab() %>"/>
          <input type="hidden" name="<%= ActionConstants.RA_CUA_RELATED_PRODUCT %>" value="<%= JspUtil.encodeHtml(ui.getCUARelatedProduct()) %>"/>
          <input type="hidden" name="<%= ActionConstants.RA_DISPLAY_SCENARIO %>" value="<%= JspUtil.encodeHtml(ui.getDisplayScenario()) %>"/>
          <input type="hidden" name="<%= ActionConstants.RA_OLDDETAILSCENARIO %>" value="<%= JspUtil.encodeHtml(ui.getOldDetailScenario()) %>"/>
 		  <input type="hidden" name="<%= ActionConstants.RA_DETAILSCENARIO %>" value="<%= JspUtil.encodeHtml(ui.getDetailScenario()) %>">
 		  <input type="hidden" name="<%= ActionConstants.RA_IS_QUERY %>" value="<%= ui.isCatalogQuery()? "yes" : "" %>">
          <input type="hidden" name="compareCUA" value="" />
        
          <%-- main product --%>
          <div class="cat-prd-block">

            <%-- Message list --%>
<%  MessageList msgList = ui.getMessageList(currentItem);
    pageContext.setAttribute(AttributeKeyConstants.MESSAGE_LIST, msgList);
    if (msgList != null && !msgList.isEmpty()) { 
%>
            <div class="cat-prd-msg">
              <isa:message id="messagetext" name="<%= AttributeKeyConstants.MESSAGE_LIST %>" ignoreNull="true">
                <div class="error"><span><%=JspUtil.encodeHtml(messagetext)%></span></div>
              </isa:message>
            </div> 
<%  } %>
           
            <table class="cat-prd-block-ltab" DataTable="0" summary="<isa:translate key="b2c.layoutTable.summary"/>">
              <tr>
                <td class="cat-prd-block-ltab-col1">
                       
                  <h1 class="areatitle"><%=JspUtil.encodeHtml(title)%></h1>         

                  <%-- image --%>
<%  pageContext.setAttribute("item", currentItem); %>
                  <isa:ifImageAvailable name="item">
                    <div class="cat-prd-img">
                      <img src="<isa:imageAttribute guids="DOC_PC_CRM_IMAGE,DOC_P_CRM_IMAGE" name="item" />"
                           alt="<%= JspUtil.encodeHtml(currentItem.getDescription())%>" />
                    </div> 
                  </isa:ifImageAvailable>
                  <%-- table with details of the product --%>
                  <div class="cat-prd-facts">
                    <%-- product id --%>
                    <div class="cat-prd-id">
                      <p><span><%= ui.getHighlightedResults(currentItem.getProduct(), htTerms, "cat-prd-hli") %></span></p>
                    </div>    
                    <%-- product description --%> 
                    <div class="cat-prd-dsc">
                      <span><%= ui.getHighlightedResults(currentItem.getDescription(), htTerms, "cat-prd-hli") %></span>
                    </div>    
                    <%-- inline configuration for configurable product--%>  
<%  if (ui.showInlineConfig(currentItem)) {  
        //these variables should be defined before calling itemconfiginfo.inc.jsp
        int viewType = 2; 
        IPCItem itemConfig = currentItem.getConfigItemReference();
        boolean isSubItem = false; %>
                    <div class="cat-prd-cfg">
                      <p><span><%@ include file="/catalog/itemconfiginfo.inc.jsp" %></p></span>
                    </div> 
<%  } %>
                    <%-- long description --%>
<%  String ldsc = JspUtil.encodeHtml(currentItem.getLngDesc());
    if (ldsc != null && !ldsc.equals("")) { %>
                    <div class="cat-prd-ldsc">
                      <p><span><%=ldsc%></span></p>
                    </div>
<%  } %>       
                    <%-- contract duration --%>
<%  if (ui.shouldContractDurationBeDisplayed(currentItem)) { %>
                    <div class="cat-prd-cntdur-lbl">
                      <isa:translate key="b2c.contractDuration"/>:&nbsp;
                    </div>
<%      /* for more than one contract duration display a list */ 
        if (currentItem.getParentItem() == null && (((WebCatItem)currentItem).getContractDurationArray()).size() > 1 ) { %>
                    <select name="item[0].selectedContractDuration" size="1" onChange="setContractDuration('<%=currentItem.getItemId()%>')">
<%          Iterator contractDurationIterator = ((WebCatItem)currentItem).getContractDurationArray().iterator();
            ContractDuration contractDuration = null;
            String selectedContractDuration = ((WebCatItem)currentItem).getSelectedContractDuration().toString(ui.getLocale());
            while(contractDurationIterator.hasNext() ) { 
                contractDuration = (ContractDuration)contractDurationIterator.next();
                if(contractDuration.toString(ui.getLocale()).equals(selectedContractDuration)) { 
%>
                      <option selected="selected" 
<%              } else { %>
                      <option
<%              } %>
                              value="<%=contractDuration.toString(ui.getLocale())%>">
                                     <%= contractDuration.toString(ui.getLocale()).replaceAll(" ","&nbsp;") %>
                      </option>                                
<%          } %>	
                    </select>
<%      } else { %>
                    <div class="cat-prd-cntdur">
                      <%= currentItem.getSelectedContractDuration().toString(ui.getLocale()).replaceAll(" ","&nbsp;") %>
                    </div>
<%      } 
    } 
%>
                    <%-- rate plan combination : selected package component--%>
<%  if(currentItem.isCombinedRatePlan() && currentItem.isSubComponent()) { %>
                    <div class="cat-prd-cmp-grp">      
<%      Iterator selectedComponent = currentItem.getDescriptionOfFirstLevelComponent(true);
        while (selectedComponent.hasNext()) { 
%>
                      <div class="b2c-prd-cmp">
                        <%= JspUtil.encodeHtml((String)selectedComponent.next()) %>
                      </div>
<%      } %>
                    </div> 
<%  } %>	

<%-- GREENORANGE BEGIN --%>
	<table name="Z_attribute">
	<%-- if (currentItem.getAttribute("CH0032D") != "" && currentItem.getAttribute("CH0032V") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("CH0032D")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("CH0032V")) %>
		</td>
	</tr>
	<% } --%>
	<% if (currentItem.getAttribute("Z_DESCRIZIONE_ISAD") != "" && currentItem.getAttribute("Z_DESCRIZIONE_ISAV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_DESCRIZIONE_ISAD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_DESCRIZIONE_ISAV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_DESCR_COLORED") != "" && currentItem.getAttribute("Z_DESCR_COLOREV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_DESCR_COLORED")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_DESCR_COLOREV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_FAMMATER_RESD") != "" && currentItem.getAttribute("Z_FAMMATER_RESV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_FAMMATER_RESD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_FAMMATER_RESV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_LARGHEZZA_PF_INTD") != "" && currentItem.getAttribute("Z_LARGHEZZA_PF_INTV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_LARGHEZZA_PF_INTD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_LARGHEZZA_PF_INTV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_LUNGHEZZA_PF_INTD") != "" && currentItem.getAttribute("Z_LUNGHEZZA_PF_INTV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_LUNGHEZZA_PF_INTD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_LUNGHEZZA_PF_INTV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_GR_CADAUNO_TOTD") != "" && currentItem.getAttribute("Z_GR_CADAUNO_TOTV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_GR_CADAUNO_TOTD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_GR_CADAUNO_TOTV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_IMBALLO_INTERNO_NUM_PFD") != "" && currentItem.getAttribute("Z_IMBALLO_INTERNO_NUM_PFV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_IMBALLO_INTERNO_NUM_PFD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_IMBALLO_INTERNO_NUM_PFV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_IMBALLO_ESTERNO_NUM_PFD") != "" && currentItem.getAttribute("Z_IMBALLO_ESTERNO_NUM_PFV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_IMBALLO_ESTERNO_NUM_PFD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_IMBALLO_ESTERNO_NUM_PFV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_PZ_CONTAINER_20D") != "" && currentItem.getAttribute("Z_PZ_CONTAINER_20V") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_PZ_CONTAINER_20D")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_PZ_CONTAINER_20V")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_PZ_CONTAINER_40D") != "" && currentItem.getAttribute("Z_PZ_CONTAINER_40V") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_PZ_CONTAINER_40D")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_PZ_CONTAINER_40V")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_RIENT_PFD") != "" && currentItem.getAttribute("Z_RIENT_PFV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_RIENT_PFD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_RIENT_PFV")) %>
		</td>
	</tr>
	<% } %>
	<% if (currentItem.getAttribute("Z_COD_VALVOLAD") != "" && currentItem.getAttribute("Z_COD_VALVOLAV") != ""){ %>
	<tr>
		<td class="cat-prd-prc-lbl">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_COD_VALVOLAD")) %>:
		</td>
		<td class="cat-prd-prc">
			<%= JspUtil.encodeHtml(currentItem.getAttribute("Z_COD_VALVOLAV")) %>
		</td>
	</tr>
	<% } %>
	</table><br/>
<%-- GREENORANGE END --%>

<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% if( !current_user.equalsIgnoreCase(USER_GUEST) ) { %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>					

<%  if (priceUI.isPriceVisible()) { %>
                    <%-- Include Pricing --%>                  
<%  priceUI.setPriceRelevantInfo(currentItem, WebCatItemPrice.SHOW_NO_SUMS, false); 
    priceUI.setDisplayPriceInDetailsPage(true); 
%>
                    <%@ include file="pricing.list.inc.jsp" %>
<%  } %>		

<%-- GREENORANGE BEGIN --%>
		   <div class="cat-prd-id">
				<%  
					String pcPrice = currentItem.getAttribute("PRICE_V") + " " +
					  				 currentItem.getAttribute("PRICE_D");
				%>
				<table name="pcPrice">
					<tr>
						<td class="cat-prd-prc-lbl"><isa:translate key="customer.productdetails.jsp.pcprice"/></td>
						<td class="cat-prd-prc"><%= pcPrice %></td>
					</tr>
				</table>
		   </div>
<%-- GREENORANGE END --%>

<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% } %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>					

                    <%-- Configuration Button -- no configuration button for package component--%>
<%-- GREENORANGE BEGIN                    
<%  if (ui.showConfigLink(currentItem)) {
        WebCatItem configItem = currentItem; 
        boolean setAnchor = ui.isCfgLnkBasketAnchor(currentItem.getTechKey()); 
        boolean displayAsLink = true;
        String formSuffix =""; 
        String lastVisited = "itemDetails"; 
%>
                    <%@ include file="/catalog/configLink.inc.jsp"%>
<%  } %>
 GREENORANGE END --%>
                    <%-- Technical data --%>
<%  if (itemAttributesIterator != null && itemAttributesIterator.hasNext()) { %>
                    <div class="cat-prd-tech">
                      <table DataTable="0" summary="<isa:translate key="b2c.layoutTable.summary"/>">
<%      while (itemAttributesIterator.hasNext()) {
            com.sap.isa.catalog.boi.IAttributeValue attrVal =( com.sap.isa.catalog.boi.IAttributeValue) itemAttributesIterator.next();
            if (!attrVal.getAttribute().isBase() && attrVal.getAttributeDescription() != null && attrVal.getAttributeName().indexOf("_") != 0) { 
%>
                        <tr>
                          <td><span><%= JspUtil.encodeHtml(attrVal.getAttributeDescription()) %></span></td>
                          <td>
<%              if (currentItem.getAreaAttributes() != null && currentItem.getAreaAttributes().getDescription(attrVal.getAttributeGuid(), attrVal.getAsString()) != null ) {%>
                            <span><%= JspUtil.encodeHtml(currentItem.getAreaAttributes().getDescription(attrVal.getAttributeGuid(),attrVal.getAsString())) %></span>
<%              } else { %>
                            <span><%= JspUtil.encodeHtml(attrVal.getAsString()) %></span>
<%              } 
                if (!currentItem.getAttribute("_" + attrVal.getAttributeName()).equals("")) { 
%>
                            <span><%= currentItem.getAttribute("_" + attrVal.getAttributeName()) %></span>
<%              } %>
                          </td>
                        </tr>
<%          } else {
                String st = ui.parseAttributeName("catalog.isa.attribute", attrVal.getAttributeGuid());
                if (st != null) { 
%>
                        <tr>
                          <td><span><%= JspUtil.encodeHtml(st) %></span></td>
                          <td><span><%= JspUtil.encodeHtml(attrVal.getAsString()) %></span></td>
                        </tr>
<%              }
            }  
        } 
%>
                      </table>
                    </div> <%-- end: cat-prd-tech --%>
<%  } // end: if (itemAttributesIterator.hasNext()) %>

<%-- Category/Hierarchy Data - start --%>
<%  if (ui.showProdCateg()) { %>
                    <div class="cat-prd-det-categ">
                      
<%      if (ui.getNoOfCategoryInfos() == 1) { %>
                      <div class="formresults">
                        <table>  
                          <tr>
                            <td class="label">
                              <span><isa:translate key="catalog.isa.categoryDesc"/>:</span>
                            </td>
                            <td class="data">
                              <span><%= JspUtil.encodeHtml(ui.getCategoryDesc(0)) %></span>
                            </td>
                          </tr>
                          <tr>
                            <td class="label">
                              <span><isa:translate key="catalog.isa.hierarchyDesc"/>:</span>
                            </td>
                            <td class="data">
                              <span><%= JspUtil.encodeHtml(ui.getHierarchyDesc(0)) %></span>
                            </td>
                          </tr>
                        </table>
                      </div>  
<%      } else { %>
<%          if (ui.isAccessible) { %>
                      <a href="#end-table1" title="<isa:translate key="b2b.acc.cat.detail.categories.link"/>"></a> 
<%          } %>	
                      <table class="app-std-tbl" summary="<isa:translate key="b2b.acc.cat.detail.categories"/>">
                        <thead>
                          <tr>
                            <th><isa:translate key="catalog.isa.categoryDesc"/></th>
                            <th class="app-std-tbl-th-last"><isa:translate key="catalog.isa.hierarchyDesc"/></th>
                          </tr>
                        </thead>
                        <tbody>
<%          for (int i = 0; i < ui.getNoOfCategoryInfos(); i++) { %>
                          <tr>
                            <td><%= JspUtil.encodeHtml(ui.getCategoryDesc(i)) %></td>
                            <td class="app-std-tbl-td-last"><%= JspUtil.encodeHtml(ui.getHierarchyDesc(i)) %></td>
                          </tr>
<%          } %>
                        </tbody>
                      </table>
<%          if (ui.isAccessible) { %>
                      <a name="end-table1" title="<isa:translate key="b2b.acc.cat.detail.categories.end"/>"></a>
<%          } %>
<%      } %>
                    </div> <%-- end: cat-prd-det-categ --%>
<%  } %>
<%-- Category/Hierarchy Data - end --%>

<%-- Exchange product information --%>
<%  if ( ui.showExchProds() && ui.readExchBusinessFlag().equals("X")) { %>
                    <div class="cat-prd-det-exchprods">
                      <h2 class="areatitle"><isa:translate key="b2b.cat.ExchangeData"/></h2>
                      <div class="formresults">
                        <table> 
<%      if (ui.readRemanufacturableFlag().equals("X")) { %>
                          <tr>
                            <td class="label"><isa:translate key="catalog.isa.remanufacturable"/></td>
                            <td class="data"><isa:translate key="catalog.isa.yes"/></td>
                          </tr>
<%      } 
        else if (ui.readRemanufacturableFlag().equals("")) { 
%>
                          <tr>
                            <td class="label"><isa:translate key="catalog.isa.remanufacturable"/></td>
                            <td class="data"><isa:translate key="catalog.isa.no"/></td>
                          </tr>
<%      } 
        if (ui.getValidityPeriod() == 0 && ui.getValidityPeriodUnit().equals("")) { 
%>
                          <tr>
                            <td class="label"><isa:translate key="b2b.cat.ValidityPeriod"/></td>
                            <td class="data"><isa:translate key="catalog.isa.NotAvailable"/></td>
                          </tr>
<%      }
        else {
            if (ui.getValidityPeriod() != 0 || !ui.getValidityPeriodUnit().equals("")) { 
%>
                          <tr>
                            <td class="label"><isa:translate key="catalog.isa.ValidityPeriod"/></td>
                            <td class="data"><%= ui.getValidityPeriod() %><%= JspUtil.encodeHtml(ui.getValidityPeriodUnit()) %></td>
                          </tr>
<%          }
        }
        if (ui.getSettlementPeriod()== 0 && ui.getSettlementPeriodUnit().equals("")) { 
%>
                          <tr>
                            <td class="label"><isa:translate key="catalog.isa.settlementPeriod"/></td>
                            <td class="data"><isa:translate key="catalog.isa.NotAvailable"/></td>
                          </tr>
<%      } else {
            if (ui.getSettlementPeriod() != 0 || !ui.getSettlementPeriodUnit().equals("")) { 
%>
                          <tr>
                            <td class="label"><isa:translate key="catalog.isa.settlementPeriod"/></td>
                            <td class="data"><%= ui.getSettlementPeriod() %><%= JspUtil.encodeHtml(ui.getSettlementPeriodUnit()) %></td>
                          </tr>
<%          }
        } 
        java.util.Hashtable exchPriceAttributes = currentItem.readExProductItemPrices();
        if(exchPriceAttributes !=null){
            java.util.Enumeration list = exchPriceAttributes.keys();
            while(list.hasMoreElements()) {
                String Desc = (String) list.nextElement();
%>
                          <tr>
                            <td class="label"><%=Desc%></td>
                            <td class="data"><%=exchPriceAttributes.get(Desc)%>&nbsp;<%=currentItem.getCurrency() %></td>
                          </tr>
<%          } %>
<%      } %>
                        </table>
                      </div><%-- end: formresults --%>
                    </div> <%--end: cat-prd-det-exchprods --%> 
<%  } %>
<%-- Exchange product information End --%>
                  </div> <%-- end: cat-prd-facts --%>
                </td>
                <td class="cat-prd-block-ltab-col2">
                  <div class="cat-prd-facts2">
                    <div class="fw-box-prd-facts2"><div class="fw-box-top-prd-facts2"><div></div></div><div class="fw-box-i1-prd-facts2"><div class="fw-box-i2-prd-facts2"><div class="fw-box-i3-prd-facts2"><div class="fw-box-content-prd-facts2">  

<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% if( !current_user.equalsIgnoreCase(USER_GUEST) ) { %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>					       

                    <%-- total price --%> 
<%  if (priceUI.isPriceVisible()) {
        priceUI.setPriceDivName("cat-prd-price-data");
        if (ui.isCurrentTotalDisplayed(currentItem)) {
            priceUI.setPriceRelevantInfo(currentItem, WebCatItemPrice.SHOW_SUMS_ONLY, false); 
            priceUI.setPriceDivName("cat-prd-total-price");
            priceUI.setDisplayEyeCatcherText(false);
            priceUI.setTitleClass("cat-prd-total-price-title");
            priceUI.setTitleTextKey("b2c.cat.total.price");
            priceUI.setHidePromotionalPrices(true); %>
                      <%@ include file="pricing.list.inc.jsp" %>
<%      }
    } 
%>

<% if (ui.isQuantityVisible(currentItem)) { %>  
<%   if (!currentItem.isSubComponent() && ui.showQuantityUnits()) { %>
                      <div class="cat-prd-det-qtyunit">
                        <%-- quantity --%>
                        <div class="cat-prd-qty">
                          <label for="item_1"><isa:translate key="catalog.isa.quantity"/>:</label>
                          <input type="<%= (ui.isQuantityChangeable(currentItem)) ? "text" : "hidden" %>" 
                                 id="item_1" 
                                 class="textInput" 
                                 size="4" 
                                 maxlength="8" 
                                 name="item[0].quantity" 
                                 value="<%= currentItem.getQuantity() %>"/>
<%      if (!ui.isQuantityChangeable(currentItem)) { %>
                              <%= currentItem.getQuantity() %>
<%      } %>
                            &nbsp;
                        </div>
                        <%-- Units --%>  
<%      if (currentItem.getUnitsOfMeasurement().length > 0 ) { %>
                        <div class="cat-prd-unit">
<%          if (currentItem.getUnitsOfMeasurement().length == 1 || !ui.isQuantityChangeable(currentItem)) { 
%>
                          <%= currentItem.getUnitsOfMeasurement()[0].trim() %>
                          <input type="hidden" name="item[0].unit" value="<%= currentItem.getUnitsOfMeasurement()[0].trim()%>"/>
<%          } else { %>
                          <select name="item[0].unit" width=4 onChange=setUnitForDetails('<%= currentItem.getItemID() %>')>
<%              for (int j = 0; j < currentItem.getUnitsOfMeasurement().length; j++) { %>
                            <option value="<%= currentItem.getUnitsOfMeasurement()[j].trim()%>"
<%                  if (currentItem.getUnitsOfMeasurement()[j].trim().equalsIgnoreCase(currentItem.getUnit().trim()) ) {%>
                                    selected="selected"
<%                  } %>      
                            ><%= currentItem.getUnitsOfMeasurement()[j]%></option> 
<%              } %>
                          </select>
<%          } %>
                        </div> 
<%      } %>
                      </div>
<%   } 
   } 
   else { %>
                      <input type="hidden" id="item_1" name="item[0].quantity" value="<%= currentItem.getQuantity() %>" />
                      <input type="hidden" name="item[0].unit" value="<%= currentItem.getUnitsOfMeasurement()[0].trim()%>" />
<% } %>

                      <%-- itemID --%>
                      <input type="hidden" name="item[0].itemID" value="<%= currentItem.getItemID() %>"/>
                      <%-- Button List -- no button are provided for package components or invalid items --%>
<%  if (ui.showStandardButtons(currentItem)) { %>
                      <%-- Button Add to basket --%>
<%      if (ui.showBasketButton()) { 
            String addSingleItemTxt = (ui.showSelectedProduct()) ? 
            		WebUtil.translate(pageContext, "catalog.button.selprod", null) : WebUtil.translate(pageContext, "catalog.isa.addToBasket", null);
            String addSingleItemCssClass = (ui.showSelectedProduct()) ? "btn-sel-prd" : "btn-add-to-bskt";
%>

                      <div class="btn-box <%= addSingleItemCssClass %>">
                        <a class="btn-lk" href="#" onClick="addSingleItem('<%= currentItem.getItemID() %>', 'addToBasketDet', '')" name="addToBasket">
                          <%= addSingleItemTxt %>
                        </a>
                      </div>
<%      } %>                      <%-- Button Add to favorits --%>
<%      if (ui.showLeaftletButton()) { %>

                      <div class="btn-box btn-add-to-lflt">
                        <a class="btn-lk" href="#" onClick="getMoreFunctions('<%= currentItem.getItemID() %>','leaflet','')"
                          ><isa:translate key="catalog.isa.leaflet"/></a>
                      </div>
<%      } %>
                      <%-- Button Add Lead --%>
<%      if (ui.isShopLeadCreationAllowed()) { %>
                      <div class="btn-box btn-create-lead">
                        <a class="btn-lk" href="#" onClick="createLead('<%= currentItem.getItemID() %>')"
                          ><isa:translate key="lead.create.entry.label"/></a>
                      </div>
<%      } %>
<%  } 
    if (ui.showCompToSim() && !ui.IsCuaList()) { 
%>
                      <isacore:ifShopProperty property = "alternativeAvailable" value = "true" >
                        <div class="btn-box btn-comp-upsel">
                          <a class="btn-lk" href ="javascript:compareUpselling('<%= ui.getItemId() %>')" 
                             title="<isa:translate key="catalog.isa.moreFunctions"/>: <isa:translate key="catalog.isa.selecting.access"/>">
                            <isa:translate key="catalog.isa.moreFunctions"/></a>
                        </div>
                      </isacore:ifShopProperty>
<%  } %>

<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% } %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>					

<%
    if (ui.showButtons(currentItem)) {
%>
                      <div class="cat-det-btn-sc">
                        <%-- update selection button for a package --%>
<%      if (ui.isUpdateButtonDisplayed(currentItem)) { %>
                        <div class="btn-box btn-refr">
                          <a class="btn-lk" href="#" onClick="getMoreFunctions('<%= currentItem.getItemID() %>','seeItem','')"
                          ><isa:translate key="b2c.catalog.button.update"/></a>
                        </div>
<%      } %>
                        <%-- only one back button for the different cases --%>
                        <%-- B2C only - back button for package components  --%>
<%      if (currentItem.isSubComponent()) { %>
                        <div class="btn-box btn-back">
                          <a class="btn-lk" href="#" onclick="backToMainProduct('<%=currentItem.getTopParentItem().getItemID()%>')"
                          ><isa:translate key="b2c.order.checkout.button.back"/></a>
                        </div>
<%      } %>
                        <%-- B2C only - back to basket button for sc relevant items coming from the basket  --%>
<%      if (ui.isSCBasketItem()) { %>
                        <div class="btn-box btn-back-to-bskt">
                          <a class="btn-lk" href="#" onclick="backToBasket()" name="addToBasket"
                          ><isa:translate key="b2c.cat.details.back.to.basket"/></a>
                        </div>
<%      } %>
                        <%-- B2B logic --%>
<%      if (ui.showBackButton()) { 
            if (ui.isDetailScenarioNotSet() || ActionConstants.DS_CATALOG_QUERY.equals(ui.getDetailScenario())) {   
                if (ui.IsQuery()) { 
                    // come from catalog search and has more than one search results
                    if (ui.hasMultipleSearchResults()) { 
%>                
                        <div class="btn-box btn-back">
                          <a href ="<isa:webappsURL name="/catalog/query.do"/>"
                             title="<isa:translate key="catalog.isa.backToQuery"/>: <isa:translate key="catalog.isa.selecting.access"/>"><isa:translate key="catalog.isa.backToQuery"/></a>
                        </div>
<%                  }
                } 
                else { 
                // come from catalog area
                    String tmpPath="/catalog/categorieInPath.do?key="+ui.getCurrPathAsString(); %>
                        <div class="btn-box btn-back">
                          <a href="<isa:webappsURL name="<%= tmpPath %>"/>"><isa:translate key="catalog.isa.backTo"/> <%= JspUtil.encodeHtml(ui.getCurrAreaName()) %></a>
                        </div>
<%              } 
            } 
            else { 
                if (ui.isItemFromBasket()) { %> 
                        <div class="btn-box btn-back-to-bskt">
                          <a href="#" onClick="backToBasket();" title="<isa:translate key="catalog.isa.back"/>: <isa:translate key="catalog.isa.selecting.access"/>"><isa:translate key="catalog.isa.back"/></a>
                        </div>
<%              }
                else { %>
                        <div class="btn-box btn-back">
<%                  if (ui.IsBestSeller()) { %>
                          <a href ="<isa:webappsURL name="/catalog/bestsellerlist.do"/>" 
                             title="<isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.bestseller"/>: <isa:translate key="catalog.isa.selecting.access"/>" ><isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.bestseller"/></a>
                          
<%                  }
                    else if (ui.IsCatalogEntry()) { %>
                          <a href="<isa:webappsURL name="/catalog/catalogForward.do">
			                           <isa:param name="catalogEntry" value="true"/>
			                       </isa:webappsURL>"
                             title="<isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.recommendations"/>: <isa:translate key="catalog.isa.selecting.access"/>"><isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.recommendations"/></a>
                          
<%                  }
                    else if (ui.IsRecommendations()) { %>
                          <a href="<isa:webappsURL name="/catalog/recommendationlist.do?uiarea=workarea"/>"
                             title="<isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.personalizedRecommend"/>: <isa:translate key="catalog.isa.selecting.access"/>"><isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.personalizedRecommend"/></a>

<%                  }
                    else if (ui.IsCuaList()) { %>
				          <a href="<isa:webappsURL name="/catalog/updateItems.do?next=products">
									 <isa:param name="<%=ActionConstants.RA_DETAILSCENARIO%>" value="<%=ui.getValueFromRequest(ActionConstants.RA_DETAILSCENARIO)%>"/>
									 <isa:param name="<%=ActionConstants.RA_DISPLAY_SCENARIO%>" value="<%=ui.getValueFromRequest(ActionConstants.RA_DISPLAY_SCENARIO)%>"/>
									 <isa:param name="<%=ActionConstants.RA_TO_NEXT%>" value="<%=ui.getValueFromRequest(ActionConstants.RA_TO_NEXT)%>"/>
									 <isa:param name="<%=ActionConstants.RA_IS_PRODUCT_LIST%>" value="<%=ActionConstants.DS_CUA%>"/>
                                   </isa:webappsURL>" 
<%						if(ui.isRelatedProducts()){ %>                                   
                             title="<isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.crossSelling"/>: <isa:translate key="catalog.isa.selecting.access"/>"><isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.crossSelling"/></a>
<%						}else if(ui.isAlternatives()){ %>                                   
                             title="<isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.upDown"/>: <isa:translate key="catalog.isa.selecting.access"/>"><isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.upDown"/></a>
<%						}else if(ui.isAccessories()){ %>                                   
                             title="<isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.accesories"/>: <isa:translate key="catalog.isa.selecting.access"/>"><isa:translate key="catalog.isa.backTo"/>: <isa:translate key="catalog.isa.accesories"/></a>
<%						}else{ %>                                   
                             title="<isa:translate key="catalog.isa.backTo"/>:  </a>
<%						} %>
<%                  }
                    else if (ui.IsDetailInWindow()) { %>
                          <a href ="javascript:window.close();" title="<isa:translate key="catalog.isa.close"/>: <isa:translate key="catalog.isa.selecting.access"/>"><isa:translate key="catalog.isa.close"/></a>
<%                  } %>
                        </div>
<%              } 
            }
        } %> 
                      </div>
<%  } %>

                      <%-- ATP --%>
<%  if (ui.showATP()) { %>
                      <div class="cat-prd-det-avw">
                        <div class="cat-prd-det-avw-head">
                          <span class="cat-prd-det-avw-title"><isa:translate key="catalog.isa.availability"/></span>
<%      if (ui.showATPConfigLink()) { %>
                          <span class="cat-prd-det-avw-chk">
                            <a href="javascript:getMoreFunctions('<%= currentItem.getItemId() %>', 'seeItemWithATPInfo')" 
                               title="<isa:translate key="catalog.isa.check"/>: <isa:translate key="catalog.isa.selecting.access"/>"
                              ><isa:translate key="catalog.isa.check"/></a>
                          </span>
<%      } %>
                        </div>
<%      if (ui.hasATPEntries(currentItem)) { %>
                        <div class="info">
                          <%= ui.getATPquantity() %>
                          <%= ui.getUnit() %>
                          <isa:translate key="catalog.isa.availableon"/> <%= ui.getCommittedDate() %>
                        </div>
<%      } else { %> 
                        <div class="warn"><isa:translate key="catalog.isa.noAtp"/></div>
<%      } %>
                      </div>
<%  } %>
                    </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-prd-facts2"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
                  </div> <%-- end: cat-prd-facts2 --%>
                </td>
              </tr>
            </table>
          </div> <%-- end: cat-prd-block --%>
               
<%  if (! currentItem.isSubComponent()) { %>
          <%-- add to basket action for package component --%>
          <input type="hidden" name="itemkey" value="<%= currentItem.getItemID() %>"/>               
          <input type="hidden" name="topItemkey" value=""/>
<%  } else { %>
          <input type="hidden" name="itemkey" value="<%= currentItem.getTechKey() %>"/>
          <input type="hidden" name="topItemkey" value="<%= currentItem.getTopParentItem().getItemID() %>"/>
<%  } %>
          <input type="hidden" name="next" value="seeItem"/>
          <input type="hidden" name="itemRequest" value="<%= currentItem.getItemID() %>"/>
          <input type="hidden" name="productguid" value="<%= currentItem.getItemID() %>"/> 
          <input type="hidden" name="areakey" value="<%= currentItem.getAreaID() %>"/>
          <input type="hidden" name="contractkey" value=""/>
          <input type="hidden" name="contractitemkey" value=""/>
          <input type="hidden" name="topItemID" value=""/>
          <input type="hidden" name="addToBasket" value=""/>
            
          <%-- sub components --%>
<%  if (ui.showScComponents(currentItem)) {
        SubItemListUI subItemListUI = new SubItemListUI(pageContext, currentItem.getWebCatSubItemList()); 
%>
          <%@ include file="/catalog/ScComponents.inc.jsp" %>

          <a name="bottomAnchor" href="#" /></a>

          <div class="cat-prd-det-btns-bot">
            <div class="fw-box-prd-btns-bot"><div class="fw-box-top-prd-btns-bot"><div></div></div><div class="fw-box-i1-prd-btns-bot"><div class="fw-box-i2-prd-btns-bot"><div class="fw-box-i3-prd-btns-bot"><div class="fw-box-content-prd-btns-bot">
            
<%      if (ui.showStandardButtons(currentItem)) { %>
              <%-- Button Add to basket --%>
              <div class="btn-box btn-add-to-bskt">
                <a class="btn-lk" href="#" onClick="addSingleItem('<%= currentItem.getItemID() %>', 'addToBasketDet', '')" name="addToBasket" <% if (!subItemListUI.isPackageCorrect()) { %> disabled <% } %>
                  ><isa:translate key="catalog.isa.addToBasket"/></a>
              </div>
              <%-- Button Add to favorits --%>
<%          if (ui.showLeaftletButton()) { %>
              <div class="btn-box btn-add-to-lflt">
                <a class="btn-lk" href="#" onClick="getMoreFunctions('<%= currentItem.getItemID() %>','leaflet','')"
                  ><isa:translate key="catalog.isa.leaflet"/></a>
              </div>
<%          } %>
          
<%      } %>
              <%-- update selection button for a package --%>
<%      if (ui.isUpdateButtonDisplayed(currentItem)) { %>
              <div class="btn-box btn-refr">
                <a class="btn-lk" href="#" onClick="getMoreFunctions('<%= currentItem.getItemID() %>','seeItem','')"
                  ><isa:translate key="b2c.catalog.button.update"/></a>
              </div>

<%      } %>
              <%-- back to basket button for sc relevant items coming from the basket  --%>
<%      if (ui.isSCBasketItem()) { %>
              <div class="btn-box btn-back-to-bskt">
                <a class="btn-lk" href="#" onclick="backToBasket()" name="addToBasket"
                  ><isa:translate key="b2c.cat.details.back.to.basket"/></a>
              </div>
<%      } %>

            </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-prd-btns-bot"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
          </div>  
<%  } %>
        </form>
        
    <%-- display style sheet for buttons if necessary --%>
<%  if (ui.showContractDetailsButton() || ui.showCUAButtons() && ui.isCUAAvailable() || ui.showExchProds() && ui.isCUAAvailable()) { 
        String catTabStylesheet = ""; %>
        <div class="cat-tabs-head cat-pcat-cuatabs-head">
          <ul>
            <%-- Contract Details Button --%>
<%      if (ui.showContractDetailsButton()) { 
            catTabStylesheet = ui.getCatTabStylesheet("cat-tab-",WebCatInfo.PROD_DET_LIST_CONTRACTS); 
%>
            <li class="<%=catTabStylesheet%>">
              <a href="#" 
                 onclick="displayContractDetails('<%= currentItem.getItemID() %>')" 
                 name="displayAccessories"><span><isa:translate key="catalog.isa.contractDetails"/></span></a>
            </li>
<%      } %>	
        <%-- Display Accessories, Up Selling, Cross Selling Buttons --%>
<%      if (ui.showCUAButtons() && ui.isCUAAvailable()) { %>
            <%-- display accessories --%>
<%          if ( ui.showAccUpDownLink() ) {
                catTabStylesheet = ui.getCatTabStylesheet("cat-tab-",WebCatInfo.PROD_DET_LIST_ACCESSORIES); %>
            <li class="<%=catTabStylesheet%>">
              <a href="#" 
                 onclick="displayAccessories('<%= currentItem.getItemID() %>')" 
                 name="displayAccessories"><span><isa:translate key="catalog.isa.accesories"/></span></a>
            </li>
<%          } %>
            <%-- display related products - cross selling ---%>	
<%          catTabStylesheet = ui.getCatTabStylesheet("cat-tab-",WebCatInfo.PROD_DET_LIST_REL_PROD); %>
            <li class="<%=catTabStylesheet%>">
              <a href="#" 
                 onclick="displayRelProducts('<%= currentItem.getItemID() %>')" 
                 name="displayAccessories"><span><isa:translate key="b2c.cua.jsp.crossselling"/></span></a>
            </li>
            <%-- display alternatives  - up and down selling --%>
<%          if (ui.showAccUpDownLink()) { 
                catTabStylesheet = ui.getCatTabStylesheet("cat-tab-",WebCatInfo.PROD_DET_LIST_ALTERNATIVES); %>
            <li class="<%=catTabStylesheet%>">
              <a href="#" 
                 onclick="displayAlternatives('<%= currentItem.getItemID() %>')" 
                 name="displayAccessories"><span><isa:translate key="b2c.cua.jsp.upselling"/></span></a>
            </li> 
<%          }
        } %>
            <%-- Display Exchange Products Button--%>
<%      if (ui.showExchProds() && ui.isCUAAvailable()) { 
            catTabStylesheet = ui.getCatTabStylesheet("cat-tab-",WebCatInfo.PROD_DET_LIST_EXCH_PROD); %>
            <li class="<%=catTabStylesheet%>">
              <a href="#" 
                 onclick="displayExchProds('<%= currentItem.getItemID() %>')" 
                 name="displayExchProds"><span><isa:translate key="marketing.cua.remanufacture"/></span></a>
            </li>
<%      } %>
          </ul>
        </div>
<%  } %>           

    <%-- CUA (Cross Selling, Up Selling, Accessories)--%>
<%  if (! ui.showCUAButtons()) {
        if (ui.shouldCUAListBeDisplayed()) { 
%>
          <div>
<%          String addToDocumentMethod = "Action";
            String detailScenario = ActionConstants.DS_CUA;
            boolean cuatab_showhead = true;
            boolean cuatab_showtypecolumn = true;
%>
            <%@ include file="/catalog/marketing/cuatable.inc.jsp" %>
          </div>  
<%      }
    } else { 
        switch(ui.getCatalogProductDetailType()){ 
            case WebCatInfo.PROD_DET_LIST_CONTRACTS: 
%>
        <%-- Display the contract details --%>
        <div class="cat-pcat-cuatabs-body">
          <%@ include file="/catalog/ContractDetails.inc.jsp" %>
        </div> 
<%          break;
            default: 
%>    
        <%-- Display the product detail list --%>
        <isacore:ifShopProperty property = "cuaAvailable" value = "true" >
          <div class="cat-pcat-cuatabs-body">
<%          String addToDocumentMethod = "Action";
            String detailScenario = ActionConstants.DS_CUA;
            boolean cuatab_showhead = false;
            boolean cuatab_showtypecolumn = false; %>
            <%@ include file="/catalog/marketing/cuatable.inc.jsp" %>
          </div> <%-- cat-pcat-cuatabs-body --%>                      
        </isacore:ifShopProperty>
<%      } 
    } 

    if (ui.isAccessible()) { %>
        <a href="#groupproductdetail-begin" 
           id="groupproductdetail-end" 
           title="<isa:translate key="access.grp.end" arg0="<%= grpTxt %>"/>"></a>   
<%  } %>
      </div>  <%-- end: cat-pcat-prd-det --%>  
    </div><%-- end cat-pcat --%>  

<%-- get argument for the title --%>  
<%  String titleArg0 = null;
    if (null!=request.getAttribute(ActionConstants.RA_ITEMNAME)) {
        titleArg0 = request.getAttribute(ActionConstants.RA_ITEMNAME).toString();
    }
    // not argument for the title, then make one and set the title using the manual hard code
    if ( null == titleArg0 ) {
        String [] titleArgs = {JspUtil.encodeHtml(currentItem.getDescription())};
        String pageTitle = WebUtil.translate(pageContext, "catalog.isa.productTitle", titleArgs); 
%>
    <script>
        document.title = '<%=pageTitle%>';
        window.name = 'main';
    </script>
        
<%  }
    if (ui.IsDetailInWindow()) { 
%>
  </body>
</html>  
<%  } %>