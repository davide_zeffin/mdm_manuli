<%-- 
*****************************************************************************

    Include:      Categories.inc.jsp
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.

***************************************************************************** 

 Categories 

 Required includes:
 ==================
 Categories.js.inc.jsp - contains javascript functions to control area display 
 
 Required global variables:
 ==========================
 CategoriesUI categoriesui 
 String areaType
    // constants for area types are defined in WebCatArea
    public final static String AREATYPE_REGULAR_CATALOG     // regular catalog area             
    public final static String AREATYPE_LOY_REWARD_CATALOG  // reward catalog area             
    public final static String AREATYPE_LOY_BUY_POINTS      // buy points area             
                                     
 usage:
 ======
  ...
  <%@  include file="pricing.java.inc.jsp"                                    %>
  <%   CategoriesUI categoriesui = <any sub class of CategoriesUI>;           %>
  ... 
  <%   String areaType = WebCatArea.<area type>; %>
  <%@  include file="/catalog/Categories.inc.jsp"                          %>
  
*****************************************************************************  
--%> 

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib prefix="isacore" uri="/isacore" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatWeightedArea" %>

    <div class="module-name"><isa:moduleName name="catalog/Categories.inc.jsp"/></div>

    <a id="b2bBrowseCateg-begin" 
       href="#b2bCategoriesPage-end" 
       title="<isa:translate key="b2c.cat.categoriesBrowse.begin"/>" 
       accesskey="<isa:translate key="b2c.cat.categoriesBrowse.access"/>"></a>
       
    <div class="secnav-navCategories">
      <ul class="cat-secnav-prodareas">  
<%  int neededPosition = 0;
    categoriesui.setMaxPosition();
%>
        <isa:iterate id="area" name="categoriesTree" type="com.sap.isa.catalog.webcatalog.WebCatWeightedArea">
<%  String tmpPath;
    categoriesui.setCount();
    // check area type 
    if (!area.getArea().isAreaOfType(areaType)) {
        continue;
    } 
    if (area.getArea().equals(categoriesui.getCurrentArea())) {
        categoriesui.setIsCurrentArea(true);
    } 
%>       
          <li class="<%= categoriesui.getStyleClass() %> <%--  possible class values: "categ" or "categSelected" --%>
                     secnav-level<%= area.getPosition() %>
<%  if (area.getSign() == area.MINUS) { %>
                     secnav-categShowsSub
<%  }
    if (area.getPosition() > 0) { 
%>
                     secnav-categIsSub
<%  } 
    if (categoriesui.getCount() == 1) { 
%>
                     secnav-categIsFirst
<%  } %>
                    "> 
<%  for (int i=0; i< (area.getPosition()); i++) { %>
            <div class="cat-secnav-spacer"></div>
<%  } %>
            <div class="cat-secnav-bullet">
<%  if (area.getSign() == area.MINUS) {
        tmpPath="/catalog/categorieInPath.do?key="+ JspUtil.encodeHtml(area.getParentPath());
%>
              <a href="<isa:webappsURL name="<%= tmpPath %>"/>" 
                 title="<isa:translate key="access.tree.open" arg0="<%=Integer.toString(area.getPosition())%>"/>"
                ><img class="cat-secnav-minus"  
                      src="<%=WebUtil.getMimeURL(pageContext, "mimes/catalog/images/spacer.gif") %>" 
                      alt="<isa:translate key="access.tree.open" arg0="<%=Integer.toString(area.getPosition())%>"/>" 
                      title="<isa:translate key="access.tree.open" arg0="<%=Integer.toString(area.getPosition())%>"/>"
              /></a>
<%  } 
    if (area.getSign() == area.PLUS) {
        tmpPath="/catalog/categorieInPath.do?key="+ JspUtil.encodeHtml(area.getPath());
%>
              <a href="<isa:webappsURL name="<%= tmpPath %>"/>" 
                 title="<isa:translate key="access.tree.closed" arg0="<%=Integer.toString(area.getPosition())%>"/>"
                ><img class="cat-secnav-plus" 
                      src="<%=WebUtil.getMimeURL(pageContext, "mimes/catalog/images/spacer.gif") %>" 
                      alt="<isa:translate key="access.tree.closed" arg0="<%=Integer.toString(area.getPosition())%>"/>" 
                      title="<isa:translate key="access.tree.closed" arg0="<%=Integer.toString(area.getPosition())%>"/>"
              /></a>
<%  }
    if (area.getSign() == area.NOTHING) { 
%>
              <img class="cat-secnav-neutral"
                   src="<%=WebUtil.getMimeURL(pageContext, "mimes/catalog/images/spacer.gif") %>" 
                   alt="<isa:translate key="access.tree.leaf" arg0="<%=Integer.toString(area.getPosition())%>"/>" 
                   title="<isa:translate key="access.tree.leaf"  arg0="<%=Integer.toString(area.getPosition())%>"/>" />
<%  } %>
            </div>
            <div class="cat-secnav-areaname">
<%  tmpPath="/catalog/categorieInPath.do?key=" + JspUtil.encodeHtml(area.getPath()); %>
              <a href="<isa:webappsURL name="<%= tmpPath %>"/>" name="p<%= categoriesui.getCount() %>">
<%  if (categoriesui.getIsCurrentArea() || categoriesui.get_selectflag()) { 
        categoriesui.set_Selectflag(false); 
%>
                <span title="<isa:translate key="access.tree.selected" arg0="<%= JspUtil.encodeHtml(area.getArea().getAreaName())%>"/>">
                  <%= JspUtil.encodeHtml(area.getArea().getAreaName())%>
                </span>
<%  } else { %>
                <span title="<isa:translate key="access.tree.unselected" arg0="<%= JspUtil.encodeHtml(area.getArea().getAreaName())%>"/>"><%= JspUtil.encodeHtml(area.getArea().getAreaName())%></span>
<%  } %>
              </a>
            </div>
          </li>
<%  if (categoriesui.getAreaId()!= null){
        if (area.getArea().getAreaID().equals(categoriesui.getAreaId())) {
            neededPosition = categoriesui.getCount();
        }
    }
    else {
        if (area.getArea().equals(categoriesui.getCurrentArea())) {
            neededPosition = categoriesui.getCount();
        }
    }
%>
        </isa:iterate>
      </ul>
    </div> <%-- end: secnav-navCategories --%>

<%-- enable this JS code if you want to provide automated page scrolling to the selected area --%>
<%-- <script language="JavaScript1.2">
         document.location.href="#p<%= neededPosition %>";
     </script>
--%>     
    
    <a id="b2bBrowseCateg-end" 
       href="#b2bCategoriesPage-begin" 
       title="<isa:translate key="b2c.cat.categoriesBrowse.end"/>"></a>
       
    