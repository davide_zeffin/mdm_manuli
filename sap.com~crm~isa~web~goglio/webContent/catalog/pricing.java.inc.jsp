<%-- 
 * This file includes all Java related methods and information to show price information.
 *
 * Please call initPriceInclude(PageContext pageContext, IsaLocation log) before using this
 * include.
--%>

<%@ page import="com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo" %>
<%@ page import="com.sap.isa.core.logging.IsaLocation" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="javax.servlet.jsp.PageContext" %>
<%@ page import="com.sap.isa.isacore.uiclass.PricingBaseUI" %>
<%!


/**
 * Returns the translated label to use for the price, depending 
 * on the PriceInfo object
 * 
 * @param priceInfo the priceInfo object to determine label key for
 *
 * @return String the key to use as price label
 */
String getPriceLabel(PricingBaseUI priceUI, PriceInfo priceInfo, boolean hasRecurrentPrices) {
    
    priceUI.log.entering("getPriceLabel()");
    String labelText = "";
    StringBuffer priceLabel = new StringBuffer("b2c.cat");
        
    priceUI.log.debug("priceInfo.getPeriodType() =" + priceInfo.getPeriodType());
        
    if (priceInfo.getPeriodType() == PriceInfo.PER_DAY) {
        priceLabel.append(".daily");
    }
    else if (priceInfo.getPeriodType() == PriceInfo.PER_WEEK) {
        priceLabel.append(".weekly");
    }
    else if (priceInfo.getPeriodType() == PriceInfo.PER_MONTH) {
        priceLabel.append(".monthly");
    }
    else if (priceInfo.getPeriodType() == PriceInfo.PER_YEAR) {
        priceLabel.append(".yearly");
    }
        
    priceUI.log.debug("priceInfo.getPromotionalPriceType() =" + priceInfo.getPromotionalPriceType());
    
    // is it a promotional price 
    if (!priceInfo.getPromotionalPriceType().equals(PriceInfo.NO_PROMOTIONAL_PRICE)) {
        priceLabel.append(".spc");
    }
        
    priceLabel.append(".price");
    
    // In the case, that there are any recurrent prices for an item, the not recurrent price
    // has to be printed as "One-Time Payment".
    if (priceInfo.getPeriodType() == PriceInfo.ONE_TIME && hasRecurrentPrices) {
    	priceLabel.append(".onetime");
    }
    
    labelText = WebUtil.translate(priceUI.getPageContext(), priceLabel.toString(), null);
        
    priceUI.log.debug("determined price label is: " + priceLabel.toString() + " Text: " + labelText);
        
    priceUI.log.exiting();
    
    return labelText;
}
    
/**
 * Returns the translated label to use as price postfix, depending 
 * on the PriceInfo object
 *
 * @param priceInfo the priceInfo object to determine postfix key for
 * 
 * @return String the key to use as price postfix
 */
String getPricePostFix(PricingBaseUI priceUI, PriceInfo priceInfo) {
    
    priceUI.log.entering("getPricePostFix()");
    String postFixText = "";
    StringBuffer pricePostFix = new StringBuffer("b2c.cat");
        
    priceUI.log.debug("priceInfo.getPeriodType() =" + priceInfo.getPeriodType());
    
    if (priceInfo.getPeriodType() != PriceInfo.ONE_TIME) {
        if (priceInfo.getPeriodType() == PriceInfo.PER_DAY) {
            pricePostFix.append(".perday");
        }
        else if (priceInfo.getPeriodType() == PriceInfo.PER_WEEK) {
            pricePostFix.append(".perweek");
        }
        else if (priceInfo.getPeriodType() == PriceInfo.PER_MONTH) {
            pricePostFix.append(".permonth");
        }
        else if (priceInfo.getPeriodType() == PriceInfo.PER_YEAR) {
            pricePostFix.append(".peryear");
        }
        else {
            pricePostFix.append(".onetime");
        }
        
        pricePostFix.append(".price");
        
        postFixText = WebUtil.translate(priceUI.getPageContext(), pricePostFix.toString(), null);
    }
    else {
        pricePostFix = new StringBuffer("");
        priceUI.log.debug("one time price, no postfix defined");
    }
        
 
    priceUI.log.debug("determined price postfix is: " + pricePostFix.toString() +" Text: " + postFixText);
        
    priceUI.log.exiting();
    
    return postFixText;
}
    
/**
 * Returns the CSS class name, that should be used to format 
 * the price, depending on the PriceInfo object
 *
 * @param priceInfo the priceInfo object tp determine the css class for
 * 
 * @return String the CSS class name, that should be used to format the price
 */
String getPriceClass(PricingBaseUI priceUI, PriceInfo priceInfo) {
    
    priceUI.log.entering("getPriceClass()");
    String priceClass = "cat-prd-prc";
        
    priceUI.log.debug("priceInfo.isPayablePrice() =" + priceInfo.isPayablePrice());
        
    if (!priceInfo.isPayablePrice()) {
        priceClass = "cat-prd-prc-inactive";
    }
        
    priceUI.log.exiting();
    return priceClass;
}

/**
 * Returns the CSS class name, that should be used to format 
 * the prices div container
 * 
 * @return String the CSS class name, that should be used to format the price
 */
String getPriceDivClass(PricingBaseUI priceUI, PriceInfo priceInfo) {
    
    priceUI.log.entering("getPriceDivClass()");
    String priceClass = "cat-prd-prc";
        
    priceUI.log.debug("priceInfo.isPayablePrice() =" + priceInfo.isPayablePrice());
        
    if (!priceInfo.isPayablePrice()) {
        priceClass = "cat-prd-prc-inactive";
    }
        
    priceUI.log.exiting();
    return priceClass;
}

%>


