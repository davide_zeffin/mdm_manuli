<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItemPage" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowBestsellerAction" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.businessobject.Product" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.b2c.B2cConstants" %>
<%@ page import="com.sap.isa.isacore.action.b2c.LoginAction" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.BestsellerUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.PricingBaseUI" %>
<%@ page import="com.sap.isa.catalog.uiclass.ProductsUI" %>

	<isacore:ifShopProperty property = "pricingCondsAvailable" value = "true">
		<script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' type = "text/javascript"></script>
	</isacore:ifShopProperty>
	
	<%@ include file="/catalog/pricing.java.inc.jsp" %> 
	<div id="bestseller" class="module">
	  <div class="module-name"><isa:moduleName name="catalog/marketing/bestseller.inc.jsp" /></div>
	   <% BestsellerUI ui = new BestsellerUI(pageContext);
		  ui.setShowPageFlag(false);
		  PricingBaseUI priceUI = ui;
		  WebCatItemPage itemPage = null;
		  String isQuery = null;
		  String accessText = null;
		  Hashtable htTerms = null; 
		  String bestgrpTxt = WebUtil.translate(pageContext, "bestseller.jsp.header", null);
		  if (ui.isAccessible() ) { %>
			   <a href="#groupbestseller-end" id="groupbestseller-begin" title="<isa:translate key="access.grp.begin" arg0="<%=bestgrpTxt%>"/>"></a>
	   <% } %>
		  <h1 class="areatitle"><isa:translate key="bestseller.jsp.header"/></h1>
		  <% /* Check, if the productlist contains products */
		  if (ui.getProductList().size() == 0) { %>
			<isa:translate key="bestseller.jsp.notFound"/><br />
			<isa:translate key="bestseller.jsp.notFound2"/>
	   <% } 
		  else { 
			 ui.setFormSuffix("bs"); 
			 ProductsUI prodUI = new ProductsUI(pageContext); %>
			 <%@ include file="../../catalog/productlist.block.inc.jsp" %>             
	   <% }
		  if (ui.isAccessible()) { %>
			 <a href="#groupbestseller-begin" id="groupbestseller-end"
			   title="<isa:translate key="access.grp.end" arg0="<%=bestgrpTxt%>"/>">
			 </a>
	   <% } %>
	</div>