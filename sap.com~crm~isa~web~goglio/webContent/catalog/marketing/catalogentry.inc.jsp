<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItemPage" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowRecommendationAction" %>
<%@ page import="com.sap.isa.businessobject.Product" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowBestsellerAction" %>
<%@ page import="com.sap.isa.isacore.action.b2c.B2cConstants" %>
<%@ page import="com.sap.isa.isacore.action.b2c.LoginAction" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.isacore.action.marketing.MaintainProfileAction" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.PricingBaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.RecommendationUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.BestsellerUI" %>
<%@ page import="com.sap.isa.core.Constants" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>
<%@ page import="com.sap.isa.catalog.uiclass.ProductsUI" %>

<script src="<isa:webappsURL name="catalog/js/pricing.inc.js"/>" type="text/javascript"></script>


<%@ include file="../../catalog/pricing.java.inc.jsp" %>

<%  boolean nouserprofile  = false; 
    boolean isUserKnown = false;
    boolean isUserLogged = false;

    UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
    String loginStatus = (String) userSessionData.getAttribute(B2cConstants.LOGIN_STATUS);

    if (loginStatus != null && loginStatus.equals(LoginAction.LOGIN_SUCCESS)) {
        isUserLogged = true;
    }

    if (userSessionData.getAttribute(B2cConstants.UNLOGGED_USER_INFO) != null || isUserLogged) {
        isUserKnown = true;
    }

    ProductListUI ui = new RecommendationUI(pageContext);
    BaseUI catentryui = ui;
    PricingBaseUI priceUI = null;
    WebCatItemPage itemPage = null;
    ui.setShowPageFlag(false);
    ui.setIsCompareAllowed(false);
    String isQuery = null;
    String accessText = null;
    Hashtable htTerms = null; %>

<isacore:ifShopProperty property = "userProfileAvailable" value = "false">
        <% nouserprofile = true; %>
</isacore:ifShopProperty>

<isacore:ifShopProperty property = "pricingCondsAvailable" value = "true">
        <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' type = "text/javascript"></script>
</isacore:ifShopProperty>

  <div id="cat-entry" class="module">
    <div class="module-name"><isa:moduleName name="catalog/marketing/catalogentry.inc.jsp" /></div>
<%  String catentrygrpTxt = WebUtil.translate(pageContext, "catalogentry.jsp.header", null);
    if (catentryui.isAccessible()) {
%>
    <a href="#groupcatalogentry-end"
       id="groupcatalogentry-begin"
       title="<isa:translate key="access.grp.begin" arg0="<%=catentrygrpTxt%>"/>"
    ></a>
<%  } %>
    
    <h1 class="areatitle"><isa:translate key="catalogentry.jsp.header"/></h1>
    <div class="areainfo">
      <isa:translate key="catalogentry.jsp.info"/>
    </div>
<%  /* create link to the login JSP, if the user is unknown and the profile is available */
    if (ui.showPersRecLoginLink() && !isUserKnown && !nouserprofile) { %>
    <div class="cat-entry-login-bests">
      <span><isa:translate key="b2c.bestseller.preLoginLink"/></span>
      <a href="<isa:webappsURL name="b2c/displayLogin.do"/>"><isa:translate key="b2c.bestseller.loginLink"/></a>
      <span><isa:translate key="b2c.bestseller.postLoginLink"/></span>
    </div> 
<%  } %>    
    
<%-- recommandation - begin --%>
<%  String recogrpTxt = WebUtil.translate(pageContext, "recommendation.jsp.header", null);
    if (catentryui.isAccessible()) {
%>
    <a href="#grouprecommendation-end"
       id="grouprecommendation-begin"
       title="<isa:translate key="access.grp.begin" arg0="<%=recogrpTxt%>"/>"
    ></a>
<%  } 
    if (ui.getProductList() != null) { 
%>
    <div class="cat-catentry-recomm-head">
      <div class="fw-box-rec-head"><div class="fw-box-top-rec-head"><div></div></div><div class="fw-box-i1-rec-head"><div class="fw-box-i2-rec-head"><div class="fw-box-i3-rec-head"><div class="fw-box-content-rec-head">
        <isa:translate key="recommendation.jsp.header"/>
      </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-rec-head"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
    </div> <%-- cat-catentry-recomm-head --%>
    <div class="cat-catentry-recomm-cont">
      <div class="fw-box-rec-cont"><div class="fw-box-top-rec-cont"><div></div></div><div class="fw-box-i1-rec-cont"><div class="fw-box-i2-rec-cont"><div class="fw-box-i3-rec-cont"><div class="fw-box-content-rec-cont">
        <%  priceUI = ui;                                   %>
        <%@ include file="recommendation.inc_internal.jsp" %>
      </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-rec-cont"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
    </div> <%-- cat-catentry-recomm-cont --%>
<%  } 
    if (catentryui.isAccessible()) {
%>
    <a href="#grouprecommendation-begin"
       id="grouprecommendation-end"
       title="<isa:translate key="access.grp.end" arg0="<%=recogrpTxt%>"/>"
    ></a>
<%  } %>
<%-- recommandation - end --%>

<%-- bestseller - begin --%>
<%  String bestgrpTxt = WebUtil.translate(pageContext, "bestseller.jsp.header", null);
    if (catentryui.isAccessible()) {
%>
    <a href="#groupbestseller-end"
       id="groupbestseller-begin"
       title="<isa:translate key="access.grp.begin" arg0="<%=bestgrpTxt%>"/>"
    ></a>
<%  } 
    int recomSize = ui.getProductListSize();
    ui = new BestsellerUI(pageContext);
    ui.setShowPageFlag(false);
    ui.setIsCompareAllowed(false);
    ui.setItemIndexCounter(recomSize);
    if (ui.getProductList() != null) { 
%>
    <div class="cat-catentry-bests-head">
      <div class="fw-box-bests-head"><div class="fw-box-top-bests-head"><div></div></div><div class="fw-box-i1-bests-head"><div class="fw-box-i2-bests-head"><div class="fw-box-i3-bests-head"><div class="fw-box-content-bests-head">
        <isa:translate key="bestseller.jsp.header"/>
      </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-bests-head"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
    </div> <%-- cat-catentry-bests-head --%>
    <div class="cat-catentry-bests-cont">
      <div class="fw-box-bests-cont"><div class="fw-box-top-bests-cont"><div></div></div><div class="fw-box-i1-bests-cont"><div class="fw-box-i2-bests-cont"><div class="fw-box-i3-bests-cont"><div class="fw-box-content-bests-cont">
<%      priceUI = ui; %>
<%      /* Check, if the productlist contains products */
        if (ui.getProductListSize() == 0) { 
%>
        <span>
          <isa:translate key="bestseller.jsp.notFound"/>
          <isa:translate key="bestseller.jsp.notFound2"/>
        </span>
<%      } 
        else { 
            ui.setFormSuffix("bs");
			ProductsUI prodUI = new ProductsUI(pageContext); %>
            <%@ include file="../../catalog/productlist.block.inc.jsp" %>
<%      } %>
      </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-bests-cont"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
    </div> <%-- cat-catentry-bests-cont --%>
<%  } 
    if (ui.showPersRecLoginLink()) { 
%>
    <isacore:ifShopProperty property = "marketingForUnknownUserAllowed" value = "true">
      <span>
        <isa:translate key="b2c.mkt.uu.preProfilLink1"/>
        <a href="<isa:webappsURL name="/accountProfileForward.do"/>"><isa:translate key="b2c.mkt.uu.profilLink"/></a>
        <isa:translate key="b2c.mkt.uu.postProfilLink"/>
      </span>
    </isacore:ifShopProperty>
<%  } %>
    
<%  if (catentryui.isAccessible()) { %>
    <a href="#groupbestseller-begin"
       id="groupbestseller-end"
       title="<isa:translate key="access.grp.end" arg0="<%=bestgrpTxt%>"/>"
    ></a>
    <a href="#groupcatalogentry-begin"
       id="groupcatalogentry-end"
       title="<isa:translate key="access.grp.end" arg0="<%=catentrygrpTxt%>"/>"
    ></a>
<%  } %>
  </div>