<%--  Display the CUA in the basket. --%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.isacore.action.b2c.*" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowCUAAction" %>
<%@ page import="com.sap.isa.businessobject.marketing.CUAList" %>
<%@ page import="com.sap.isa.businessobject.marketing.CUAProduct" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatSubItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.CuaUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.PricingBaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

  <script src="<isa:webappsURL name="catalog/js/pricing.inc.js"/>" type="text/javascript"></script>

<%@  include file="/catalog/pricing.java.inc.jsp" %>
<%  String[] evenOdd = new String[] { "app-std-tbl-even", "app-std-tbl-odd" };
    CuaUI cuabasketui = new CuaUI(pageContext);
    CuaUI priceUI = cuabasketui; 
%>
  <div class="module-name"><isa:moduleName name="catalog/marketing/cuabasket.inc.jsp" /></div>
  <div id="b2c-basket-cua">  
<%  String cuatitleTxt = WebUtil.translate(pageContext, "b2c.cua.jsp.title", null);
    if (cuabasketui.isAccessible()) {
%>
    <a href="#groupcuabasket-end"
       id="groupcuabasket-begin"
       title="<isa:translate key="access.grp.begin" arg0="<%=cuatitleTxt%>"/>"
    ></a>
<%  } %>
 
    <%-- Display header line for cua list --%>
    <div class="b2c-basket-cua-head">
        <div class="fw-box-cua-head"><div class="fw-box-top-cua-head"><div></div></div><div class="fw-box-i1-cua-head"><div class="fw-box-i2-cua-head"><div class="fw-box-i3-cua-head"><div class="fw-box-content-cua-head">
            <isa:translate key="b2c.cua.jsp.title"/>
        </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-cua-head"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>    
    </div> <%-- b2c-basket-cua-head --%>
            
    <div class="b2c-basket-cua-cont">
        <div class="fw-box-cua-cont"><div class="fw-box-top-cua-cont"><div></div></div><div class="fw-box-i1-cua-cont"><div class="fw-box-i2-cua-cont"><div class="fw-box-i3-cua-cont"><div class="fw-box-content-cua-cont">
         
<%  int colno = 8;
    int rowno = 0;
    String columnNumber = Integer.toString(colno);
    String rowNumber = WebUtil.translate(pageContext, "b2c.compareitem.variable", null);//Integer.toString(rowno);
    String allRows = WebUtil.translate(pageContext, "b2c.compareitem.all", null);//Integer.toString(rowno);
    String tableTitle = WebUtil.translate(pageContext, "b2c.cuabasket.table", null);

    if (cuabasketui.isAccessible()) {
%>
    <a id="cuabaskettable-begin"
       href="#cuabaskettable-end"
       title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="1" arg4="<%=allRows%>"/>">
    </a>
<%  } %>
    <div class="fw-box-cuainc-cont"><div class="fw-box-top-cuainc-cont"><div></div></div><div class="fw-box-i1-cuainc-cont"><div class="fw-box-i2-cuainc-cont"><div class="fw-box-i3-cuainc-cont"><div class="fw-box-content-cuainc-cont">
      <table id="cuabaskettable" class= "app-std-tbl" 
             summary="<isa:translate key="access.table.summary" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="1" arg4="<%=allRows%>"/>"
      >
        <thead>
          <tr>
            <th id="col_1" title="<isa:translate key="b2c.cua.jsp.relatedProduct"/>" scope="col"><span><isa:translate key="b2c.cua.jsp.relatedProduct"/></span></th>
            <th id="col_2" title="<isa:translate key="b2c.cua.jsp.type"/>" scope="col"><span><isa:translate key="b2c.cua.jsp.type"/></span></th>
            <th id="col_3" title="<isa:translate key="b2c.cua.jsp.picture"/>" scope="col"><span><isa:translate key="b2c.cua.jsp.picture"/></span></th>
            <th id="col_4" title="<isa:translate key="b2c.cua.jsp.id"/>" scope="col"><span><isa:translate key="b2c.cua.jsp.id"/></span></th>
            <th id="col_5" title="<isa:translate key="b2c.cua.jsp.price"/>" scope="col"><span><isa:translate key="b2c.cua.jsp.price"/></span></th>
            <th class="app-std-tbl-th-last" id="col_6" title="<isa:translate key="b2c.product.jsp.functions"/>" scope="col"><span>&nbsp;</span></th>
          </tr>
        </thead>
        <tbody>
<%  int cualine = 0; 
    boolean isFirstLine; %>
          <isa:iterate id="cua" name="<%= B2cConstants.RK_BASKET_ITEM_LIST %>"
                       type="com.sap.isa.backend.boi.isacore.marketing.CUAData">
<%  CUAList cuaList = (CUAList) cua.getCUAList();
    if (cuaList != null && cuaList.size() > 0) {
        isFirstLine = true;
        request.setAttribute("cuaList", cuaList); 
%>
            <isa:iterate id="cuaProduct" name="cuaList"
                         type="com.sap.isa.businessobject.marketing.CUAProduct">
            <tr class="<%= evenOdd[++cualine % 2]%>">
              <td headers="col_1">
               <% if (isFirstLine) { %>
                      <%-- Product id --%>
                      <div class="cat-prd-id" title="<isa:translate key="catalog.isa.product"/>"> 
                          <span><%= cua.getProduct() %></span>
                          <br>
                      </div> 
                      <%-- Product Description --%>
                      <div class="cat-prd-dsc">
                         <span><%= cua.getDescription() %></span>
                      </div>
                  <%  isFirstLine = false; 
                  } %>
              </td>
              <td headers="col_2">
<%      if (cuaProduct.getCuaType().equals(CUAProduct.ACCESSORY)) { %>
                <span><isa:translate key="b2c.cua.jsp.accessory"/></span>
<%      }
        else if (cuaProduct.getCuaType().equals(CUAProduct.CROSSSELLING)) {
%>
                <span><isa:translate key="b2c.cua.jsp.crossselling"/></span>
<%      }
        else if (cuaProduct.getCuaType().equals(CUAProduct.UPSELLING) ||
                cuaProduct.getCuaType().equals(CUAProduct.DOWNSELLING) ) {
%>
                <span><isa:translate key="b2c.cua.jsp.upselling"/></span>
<%      } %>
              </td>
              <td class="prd-img" headers="col_3">
                  <isa:ifThumbAvailable name="cuaProduct">
                      <div class="cat-prd-thumb">
                          <a href="<isa:webappsURL name="catalog/productdetailFromOutside.do"/><%=ShowProductDetailAction.createDetailRequest(cuaProduct,"basket") %>" >
                              <img alt="<%=cuaProduct.getDescription() %>" 
                                   src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="cuaProduct" />" />
                          </a>     
                       </div>
                   </isa:ifThumbAvailable>
              </td>
              <%-- Product id, Description, Config --%>
              <td headers="col_4">
                <div class="cat-prd-facts">
                    <%-- Product id --%>
                    <div class="cat-prd-id" title="<isa:translate key="catalog.isa.product"/>"> 
                         <span><%= cuaProduct.getId()%></span>
                         <br>
                    </div> 
                    <%-- Product Description --%>
                    <div class="cat-prd-dsc">
                        <a href="<isa:webappsURL name="catalog/productdetailFromOutside.do"/><%=ShowProductDetailAction.createDetailRequest(cuaProduct,"basket") %>"
                        ><%=JspUtil.encodeHtml(cuaProduct.getDescription()) %></a><br />
                    </div>
                    <%-- Inline Configuration for configurable product--%>   
                 <% if (cuaProduct.isConfigurable() == true) {
                        //these variables should be defined before calling itemconfiginfo.inc.jsp
                        int viewType = 2; 
                        boolean isSubItem = false; 
                        if (cuaProduct.getCatalogItem().getConfigItemReference() == null ) {
                           cuaProduct.getCatalogItem().readItemPrice();
                        }
                        IPCItem itemConfig = cuaProduct.getCatalogItem().getConfigItemReference(); %>
                        <div class="cat-prd-cfg">
                             <p><span><%@ include file="/catalog/itemconfiginfo.inc.jsp" %></p></span>
                        </div> 
                 <% } %>
                    <%-- Default Components of a Sales Package or Combined Rate Plan --%>
                 <% if (cuaProduct.hasDefaultComponents()) { %>
                        <div class="cat-prd-cmp-grp">
                         <% Iterator iterSubItem = cuaProduct.getCatalogItem().getWebCatSubItemList().iteratorOnlyPopulated();
                            while (iterSubItem.hasNext()) {
                                WebCatSubItem subItem = (WebCatSubItem) iterSubItem.next(); 
                                if (!((subItem.isSalesComponent() || subItem.isRatePlanCombination()) &&
                                   subItem.isScSelected())) {
                                    continue; 
                                } %>
                                <div class="b2c-prd-cmp">
                                <% for (int j=subItem.getHierarchyLevel()-1; j>0 ; j--) { %>
                                       &nbsp;
                                <% } %>
                                   <%=JspUtil.encodeHtml(subItem.getDescription())%> 
                                </div>                          
                         <% } %>
                        </div>
                 <% } %> 
                 
                    <%-- Eye Catcher Text --%>
                    <div class="b2c-prd-eye">
                         <span><var><%= JspUtil.encodeHtml(cuaProduct.getEyeCatcherText()) %></var></span>
                    </div>
                </div>
              </td>
              <%-- Prices --%>
              <td headers="col_5" align="right">
               <% priceUI.setPriceRelevantInfo(cuaProduct, true, true);  %>
               <%@ include file="/catalog/pricing.list.inc.jsp" %>
             </td>
              <td headers="col_6" class="app-std-tbl-td-last">
                <span class="icon">
<%      /* upselling products must be replaced in basket */
        /* check, to fill the right parameter */
        if (cuaProduct.getCuaType().equals(CUAProduct.UPSELLING) ||
                cuaProduct.getCuaType().equals(CUAProduct.DOWNSELLING)) { 
%>
                  <span class="imgbtn-box">
                    <a class="imgbtn-lk" href="<isa:webappsURL name="b2c/addproducttodocument.do"/><%= GetTransferItemFromProductAction.createReplaceRequest(cuaProduct,cua.getTechKey().getIdAsString(),1,"cuatobasket") %>"
                      ><img class="cat-imgbtn-addtocart" 
                            src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                            alt="<isa:translate key="b2c.cua.jsp.replace"/>" 
                            width="16" height="16" border="1"
                    /></a>
                  </span>  
<%      }
        else { 
%>
                  <span class="imgbtn-box">
                    <a class="imgbtn-lk" href="<isa:webappsURL name="b2c/addproducttodocument.do"/><%= GetTransferItemFromProductAction.createAddRequest(cuaProduct,1,"cuatobasket") %>"
                      ><img class="cat-imgbtn-addtocart" 
                            src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                            alt="<isa:translate key="b2c.product.jsp.addToBasket"/>" 
                            width="16" height="16" border="1"
                    /></a>
                  </span>  
<%      } %>
                  <span class="imgbtn-box">
                    <a class="imgbtn-lk" href="<isa:webappsURL name="b2c/addproducttodocument.do"/><%=GetTransferItemFromProductAction.createAddRequest(cuaProduct,1,"cualeaflet") %>"
                      ><img class="cat-imgbtn-addtolfl" 
                            src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                            alt="<isa:translate key="b2c.product.jsp.addToLeaflet"/>" 
                            width="16" height="16" border="1"
                    /></a>
                  </span>  
                </span>
              </td>
            </tr>
            </isa:iterate>
<%  } %>
          </isa:iterate>
        </tbody>
      </table>
    </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-cuainc-cont"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>  
      
        </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-cua-cont"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
    </div> <%-- b2c-basket-cua-cont --%>


<%  if (cuabasketui.isAccessible()) { %>
      <a id="cuabaskettable-end" 
         href="#cuabaskettable-begin" 
         title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>"></a>
<%  } %>
      <form>
        <p style="padding-top: 20px">
<%  String cuacontinueBtnTxt = WebUtil.translate(pageContext, "b2c.order.basket.contShopping", null); %>
          <input type="button"
                 name="continueShopping"
                 class="FancyButtonGrey"
                 title="<isa:translate key="access.button" arg0="<%=cuacontinueBtnTxt%>" arg1=""/>"
                 value="<isa:translate key="b2c.order.basket.contShopping"/>"
                 onclick="self.location.href='<isa:webappsURL name="catalog/catalogForward.do"/>'" />
        </p>
      </form>
<%  if (cuabasketui.isAccessible()) { %>
      <a href="#groupcuabasket-begin"
         id="groupcuabasket-end"
         title="<isa:translate key="access.grp.end" arg0="<%=cuatitleTxt%>"/>"
      ></a>
<%  } %>
  </div> <%-- class="b2c-basket-cua" --%>