<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib prefix="isacore" uri="/isacore" %>

<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowCUAAction" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.businessobject.marketing.CUAProduct" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatSubItem" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.CuaUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.core.Constants" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>

<%-- Needed attributes defined outside this include:
	 ===============================================
	 - String addToDocumentMethod must be defined, to decide how to add products 
	 - boolean cuatab_showhead indicates if header text should be displayed 
--%>
<script type="text/javascript">
  function openWinProdDetail( addURL ) {

    var url = '<isa:webappsURL name="b2b/productdetail.do"/>' + addURL;

    var sat = window.open(url, 'artikel_to_order', 'width=700,height=370,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
    sat.focus();
  }
</script>

<%  CuaUI cuatableui = new CuaUI(pageContext);
	String cuatabletitleTxt = WebUtil.translate(pageContext, "b2c.cua.table.title", null);
%>

<div id="cat-pcat-cua">
  <div class="module-name"><isa:moduleName name="catalog/marketing/cuatable.inc.jsp" /></div>

<%  ProductList productList = cuatableui.getProductList();
	String displayType = (String) request.getAttribute(ShowCUAAction.RC_CUATYPE);
	boolean productFound = false;
	String[] evenOdd = new String[] { "app-std-tbl-even", "app-std-tbl-odd" };
    
	int colno = 7;
	int rowno = 0;
	String columnNumber = Integer.toString(colno);
	String rowNumber = WebUtil.translate(pageContext, "b2c.compareitem.variable", null);//Integer.toString(rowno);
	String allRows = WebUtil.translate(pageContext, "b2c.compareitem.all", null);//Integer.toString(rowno);
	String tableTitle = WebUtil.translate(pageContext, "b2c.cua.table.title", null);

   if (cuatableui.isAccessible()) { %>
	   <a id="cuatabletable-begin" href="#cuatabletable-end" title="<isa:translate key="access.table.begin" 
						   arg0="<%=tableTitle%>"
						   arg1="<%=rowNumber%>"
						   arg2="<%=columnNumber%>"
						   arg3="1"
						   arg4="<%=allRows%>"/>"></a>
<% } %>
	
<%-- header line for cua list --%>
<% if (cuatab_showhead == true) { %>	
	 <div class="cat-pcat-cua-head">
	   <div class="fw-box-cua-head"><div class="fw-box-top-cua-head"><div></div></div><div class="fw-box-i1-cua-head"><div class="fw-box-i2-cua-head"><div class="fw-box-i3-cua-head"><div class="fw-box-content-cua-head">
		 <isa:translate key="b2c.cua.jsp.title"/>
	   </div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-cua-head"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
	 </div> <%-- cat-pcat-cua-head --%>
<% } %>  
  
<%-- cua list --%>            
  <div class="cat-pcat-cua-cont">
	<div class="fw-box-cua-cont"><div class="fw-box-top-cua-cont"><div></div></div><div class="fw-box-i1-cua-cont"><div class="fw-box-i2-cua-cont"><div class="fw-box-i3-cua-cont"><div class="fw-box-content-cua-cont">
	<% /* display the table, if the productlist contains products */
	if ( productList != null && productList.size() > 0) { %>
         
	  <table id="cuatabletable" class="app-std-tbl" 
			 summary="<isa:translate key="access.table.summary"
									 arg0="<%=tableTitle%>"
									 arg1="<%=rowNumber%>"
									 arg2="<%=columnNumber%>"
									 arg3="1"
									 arg4="<%=allRows%>"/>">
		<thead>
		  <tr>
			<%-- <th><isa:translate key="b2c.cua.jsp.relatedProduct"/></th>       --%>
<%      if (cuatab_showtypecolumn == true) { %>
			<th id="col_1" scope="col" class="cat-prd-cuatype" title="<isa:translate key="b2c.cua.jsp.type"/>"><span><isa:translate key="b2c.cua.jsp.type"/></span></th>
<%      } %>
			<th id="col_2" scope="col" class="cat-prd-thumb" title="<isa:translate key="b2c.cua.jsp.picture"/>">
<%      if (cuatableui.isAccessible()) { %>
			  <span><isa:translate key="b2c.cua.jsp.picture"/></span>
<%      } %>
			  &nbsp;            
			</th>
			<th id="col_3" scope="col" class="cat-prd-data" title="<isa:translate key="b2c.cua.jsp.id"/>"><span><isa:translate key="b2c.cua.jsp.id"/></span></th>
<%      if (priceUI.isPriceVisible() && !priceUI.isNoPriceUsed()) { %>
			<th id="col_4" scope="col" class="cat-prd-prc" title="<isa:translate key="b2c.cua.jsp.price"/>"><span><isa:translate key="b2c.cua.jsp.price"/></span></th>
<%      } %>
			<th class="cat-btn-lst app-std-tbl-th-last" id="col_5" scope="col" 
				title="<isa:translate key="b2c.product.jsp.addToBasket"/>", 
<%      if (ui.isAddToLeafletAllowed() ) { %> 
					   <isa:translate key="b2c.product.jsp.addToLeaflet"/>"> 
<%      } %>
			  &nbsp;
			</th>
		  </tr>
		</thead>
		<tbody>
<%      int cualine = 0;
		boolean isFirstLine = true;
%>
		  <isa:iterate id="cuaProduct" name="<%= ProductListUI.PC_PRODUCT_LIST %>"
					   type="com.sap.isa.businessobject.marketing.CUAProduct">

<%      if (ShowCUAAction.isProductVisible(cuaProduct.getCuaType(),displayType)) { %>
			<tr class="<%= evenOdd[++cualine % 2]%>">
			  <%--  <td><var><%= isFirstLine?cua.getProduct():"" %></var></td> --%>
<%          isFirstLine = false;
			productFound = true;

			if (cuatab_showtypecolumn == true) {
%>
			  <td headers="col_1" class="cat-prd-cuatype">
<%              if (cuaProduct.getCuaType().equals(CUAProduct.ACCESSORY)) {%>
				<span><isa:translate key="b2c.cua.jsp.accessory"/></span>
<%              } else if (cuaProduct.getCuaType().equals(CUAProduct.CROSSSELLING)) { %>
				<span><isa:translate key="b2c.cua.jsp.crossselling"/></span>
<%              } else if (cuaProduct.getCuaType().equals(CUAProduct.UPSELLING)
							   || cuaProduct.getCuaType().equals(CUAProduct.DOWNSELLING) ) {
%>
				<span><isa:translate key="b2c.cua.jsp.upselling"/></span>
<%              } else if (cuaProduct.getCuaType().equals(CUAProduct.REMANUFACTURED)
							   || cuaProduct.getCuaType().equals(CUAProduct.NEWPART)
							   || cuaProduct.getCuaType().equals(CUAProduct.REMANNEWPART) ) { 
%>
				<span><isa:translate key="marketing.cua.remanufacture"/></span>
<%              } %>
			  </td>
<%          } %>
        
			  <td headers="col_2" class="cat-prd-thumb">
				<isa:ifThumbAvailable name="cuaProduct">
				  <a href="#" onclick="openWinProdDetail('<%= JspUtil.encodeHtml(ShowProductDetailAction.createDetailRequest(cuaProduct,ActionConstants.DS_IN_WINDOW)) %>') "
					><img alt="<%=cuaProduct.getDescription() %>" 
						  src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="cuaProduct" />" 
				  /></a>
				</isa:ifThumbAvailable>
			  </td>
			  <td headers="col_3" class="cat-prd-data">
				<%-- Product id --%>
				<div class="cat-prd-id" title="<isa:translate key="catalog.isa.product"/>"> 
				  <span><%= cuaProduct.getId()%></span>
				</div> 
				<%-- Product Description --%>
				<div class="cat-prd-dsc"> 
<%-- a href="<isa:webappsURL name="catalog/productdetailFromOutside.do"/><%=ShowProductDetailAction.createDetailRequest(cuaProduct,ShowCUAAction.RC_CUATABLE)%>&cuaproducttype=&forward=showcua" --%>
				  <a href="#" onclick="openWinProdDetail('<%= JspUtil.encodeHtml(ShowProductDetailAction.createDetailRequest(cuaProduct,ActionConstants.DS_IN_WINDOW)) %>') "
				  ><%=JspUtil.encodeHtml(cuaProduct.getDescription()) %></a>
				</div> 
				<%-- Inline Configuration for configurable product--%>   
<%          if (cuaProduct.isConfigurable() == true) {
				//these variables should be defined before calling itemconfiginfo.inc.jsp
				int viewType = 2; 
				IPCItem itemConfig =  cuaProduct.getCatalogItem().getConfigItemReference();
				boolean isSubItem = false; 
%>
				<div class="cat-prd-cfg">
				  <p><span><%@ include file="/catalog/itemconfiginfo.inc.jsp" %></span></p>
				</div> 
<%          } %>
			<%-- Default Components of a Sales Package or Combined Rate Plan --%>
<%          if (cuaProduct.hasDefaultComponents()) { %>
				<div class="cat-prd-cmp-grp">
<%              Iterator iterSubItem = cuaProduct.getCatalogItem().getWebCatSubItemList().iteratorOnlyPopulated();
				while (iterSubItem.hasNext()) {
					WebCatSubItem subItem = (WebCatSubItem) iterSubItem.next(); 
					if (!((subItem.isSalesComponent() || subItem.isRatePlanCombination()) &&
							subItem.isScSelected())) {
						continue; 
					} 
%>
				  <div class="b2c-prd-cmp">
<%                  for (int j=subItem.getHierarchyLevel()-1; j>0 ; j--) { %>
					  &nbsp;
<%                  } %>
						<%=JspUtil.encodeHtml(subItem.getDescription())%> 
				  </div>                          
<%              } %>
				</div>
<%          } %> 
				<%-- Eye Catcher Text --%>
				<div class="b2c-prd-eye">
				  <span><var><%= JspUtil.encodeHtml(cuaProduct.getEyeCatcherText()) %></var></span>
				</div>
			  </td>
<%          if (priceUI.isPriceVisible() && !priceUI.isNoPriceUsed()) { %>
			  <td headers="col_4" class="cat-prd-prc">
<%              priceUI.setPriceRelevantInfo(cuaProduct, cuaProduct.isRelevantForExplosion(), true);  %>
				<%@ include file="/catalog/pricing.list.inc.jsp" %>
			  </td>
<%          } %> 
			  <td headers="col_5" class="cat-btn-lst app-std-tbl-td-last">
<%          if (ui.showSelectedProduct()) { %>
			   <input type="button" class="FancyButton" 
					  onclick="javascript:addSingleItemCUA('<%= cuaProduct.getItemId() %>', 'addToBasketDet', '')" 
					  title="<isa:translate key="catalog.button.selprod"/>" 
					  value="<isa:translate key="catalog.button.selprod"/>" />
<%          } else { %>
				<span class="icon">
					<a class="imgbtn-lk" href="#" onClick="addSingleItemCUA('<%= cuaProduct.getItemId() %>', 'addToBasketDet', '')" name="addToBasket"
						><img class="cat-imgbtn-addtocart" 
							  src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
							  alt="<isa:translate key="b2c.product.jsp.addToBasket"/>" 
							  width="16" height="16" border="1"
					/></a>
				</span>    
<%          } %>
<%          if(ui.isAddToLeafletAllowed()) { %>
				<span class="icon">
				  <span class="imgbtn-box">
					<a class="imgbtn-lk" href="#" onClick="getMoreFunctions('<%= cuaProduct.getItemId() %>','leaflet','')"
					  ><img class="cat-imgbtn-addtolfl" 
							src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
							alt="<isa:translate key="b2c.product.jsp.addToLeaflet"/>" 
							width="16" height="16" border="1"
					/></a>
				  </span>
                </span>
<%          } %>   
			  </td>
			</tr>
<%      } %>
		  </isa:iterate>
		</tbody>
	  </table>
<%      if (cuatableui.isAccessible()) { %>
  <a id="cuatabletable-end" href="#cuatabletable-begin" title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>"></a>
<%      }
	} /*if size */
	else { %>
	  <div class="cat-pcat-cua-nores">
		<isa:translate key="b2c.cua.noProductsFound"/>
	  </div>
 <% } %>
	</div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom-cua-cont"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
  </div> <%-- end: cat-pcat-cua-cont --%>
</div> <%-- end: cat-pcat-cua --%>
