<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%-- Inlcude to display a marketing product list. either bestseller or recommendations --%>
<%-- ProductListUI ui must be define outside  --%>

<%-- The Javascript library "b2b/jscript/ipc_pricinganalysis.jsp" must be loaded --%>


<%-- use this include only as a static include --%>

<%-- Check, if the productlist contains products --%>

<%
if (ui.getProductList().size() > 0) { %>
  <div class="module-name"><isa:moduleName name="catalog/marketing/productlist.inc.jsp"/></div>
  <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <% int line = 0; %>
        <isa:iterate id="product" name="<%= ProductListUI.PC_PRODUCT_LIST %>"
                     type="com.sap.isa.businessobject.Product">

          <%
          if (line < 3) {
             line++; %>
             <td align="right" width="16%">
               <% if (product.getThumb().length() > 0) { %>
                    <img border="0" alt="<%=product.getPicture() %>"
                      src="<%=product.getThumb() %>"
                      alt="<%=JspUtil.encodeHtml(product.getDescription())%>">
               <% }
                  else { %>
                    <img border="0" alt="Thumbnail"
                      src="<%=WebUtil.getMimeURL(pageContext, "mimes/catalog/images/small.gif" ) %>"
                      alt="<%=JspUtil.encodeHtml(product.getDescription())%>"
                      >
               <% } %>
             </td>
             <td width="16%">
                <%-- Bestseller Version
                      <td width="16%">
                        <a href='<isa:webappsURL name="catalog/updateItems.do">
                                                        <isa:param name="areaid" value="workarea"/>
                                                        <isa:param name="next" value="seeItem"/>
                                                <isa:param name="<%=ActionConstants.RA_ITEMKEY%>" value="<%=product.getItemId()%>"/>
                                                        <isa:param name="productAreaId" value="<%=product.getArea()%>"/>
                                                        <isa:param name="productId" value="<%=product.getId()%>"/>
                                         </isa:webappsURL>'
                        >
                                                <%=product.getId() %>
                                                </a> <br>
                        <a href='<isa:webappsURL name="catalog/updateItems.do">
                                                        <isa:param name="areaid" value="workarea"/>
                                                        <isa:param name="next" value="seeItem"/>
                                                <isa:param name="<%=ActionConstants.RA_ITEMKEY%>" value="<%=product.getItemId()%>"/>
                                                        <isa:param name="productAreaId" value="<%=product.getArea()%>"/>
                                                        <isa:param name="productId" value="<%=product.getId()%>"/>
                                         </isa:webappsURL>'
                        >
                        <%=product.getDescription() %>
                        </a> <br>
                --%>

               <a href="<isa:webappsURL name="catalog/productdetailFromOutside.do"/><%=ShowProductDetailAction.createDetailRequest(product,ui.getScenario()) %>"><%=product.getId() %></a> <br>
               <a href="<isa:webappsURL name="catalog/productdetailFromOutside.do"/><%=ShowProductDetailAction.createDetailRequest(product,ui.getScenario()) %>"><%=JspUtil.encodeHtml(product.getDescription()) %></a> <br>
               <br>
               <%
               if (ui.isListPriceAvailable()) { %>
                 <br>
                 <span><isa:translate key="catalog.isa.price"/>&nbsp;<%=product.getListPrice() %></span>
               <%
               }
               if (ui.isCustomerSpecificPriceAvailable()) { %>
                 <br>
                 <span><isa:translate key="catalog.isa.IPCPrice"/>&nbsp;<%=product.getCustomerSpecificPrice() %></span>
                 <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
                        <%
                        IPCItemReference genIpcItemRef = product.getIPCItemReference();
						RFCIPCItemReference ipcItemRef = (RFCIPCItemReference)genIpcItemRef;
                        if (ipcItemRef != null) { %>
                          &nbsp;
                          <a href="#" onclick="displayIpcPricingConds('<%= JspUtil.removeNull(ipcItemRef.getConnectionKey()) %>',
                                                                      '<%= JspUtil.removeNull(ipcItemRef.getDocumentId()) %>',
                                                                      '<%= JspUtil.removeNull(ipcItemRef.getItemId()) %>'
                                                                      )"><isa:translate key="b2b.order.display.ipcconds"/></a>
                        <%
                        } %>
                 </isacore:ifShopProperty>
                 <br />
               <%
               }%>

               <br />
               <span class="imgbtn-box">
                 <a class="imgbtn-lk" href="<isa:webappsURL name="b2c/addproducttodocument.do"/><%=GetTransferItemFromProductAction.createAddRequest(product,1) %>"
                   ><img class="cat-imgbtn-addtocart" 
                         src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                         alt="<isa:translate key="b2c.product.jsp.addToBasket"/>" 
                         width="16" height="16" border="1"
                 /></a>
               </span> 
               <span class="imgbtn-box">
                 <a class="imgbtn-lk" href="<isa:webappsURL name="b2c/addproducttodocument.do"/><%=GetTransferItemFromProductAction.createAddRequest(product,1) %>&target=leaflet"
                   ><img class="cat-imgbtn-addtolfl" 
                         src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
                         alt="<isa:translate key="b2c.product.jsp.addToLeaflet"/>" 
                         width="16" height="16" border="1"
                 /></a>
               </span>  
              </td>
           <%
          } /* line < 3 */%>
        </isa:iterate>
      </tr>
      <% /* Check, if the productlist contains more products */
      if (ui.getProductList().size() > 3) { 
		String myAction = "b2c/" + ui.getMoreFoundAction();
      	%>
        <tr>
          <td colspan="4" align="right"><br>
            <a href="<isa:webappsURL name="<%= myAction%>"/> ">
              <%=ui.getMoreFoundMessage()%>
            </a>
          </td>
        </tr>
      <%
      } %>
    </tbody>
 </table>
<%
} %>

