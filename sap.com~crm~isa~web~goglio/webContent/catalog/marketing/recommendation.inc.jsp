<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItemPage" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowRecommendationAction" %>
<%@ page import="com.sap.isa.businessobject.Product" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.isacore.action.b2c.B2cConstants" %>
<%@ page import="com.sap.isa.isacore.action.b2c.LoginAction" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.isacore.action.marketing.MaintainProfileAction" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.PricingBaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.RecommendationUI" %>
																																												
<isacore:ifShopProperty property = "pricingCondsAvailable" value = "true">
	 <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' type = "text/javascript"></script>
</isacore:ifShopProperty>

<%@ include file="/catalog/pricing.java.inc.jsp" %> 

<div id="recommendation" class="module">
  <div class="module-name"><isa:moduleName name="catalog/marketing/recommendation.inc.jsp" /></div>
<%
		RecommendationUI ui = new RecommendationUI(pageContext);
		ui.setShowPageFlag(false);
		PricingBaseUI priceUI = ui;
		WebCatItemPage itemPage = null;
		String isQuery = null;
		String accessText = null;
		Hashtable htTerms = null; 
		String recogrpTxt = WebUtil.translate(pageContext, "recommendation.jsp.header", null);
		if (ui.isAccessible())
		{
%>
						<a
								href="#grouprecommendation-end"
								id="grouprecommendation-begin"
								title="<isa:translate key="access.grp.begin" arg0="<%=recogrpTxt%>"/>"
						></a>
<%
		}
%>
	<h1 class="areatitle"><isa:translate key="recommendation.jsp.header"/></h1>
	<% UserSessionData userSessionData = UserSessionData.getUserSessionData(session); %>
	<%@ include file="recommendation.inc_internal.jsp" %>
<%
		if (ui.isAccessible())
		{
%>
<a
		href="#grouprecommendation-begin"
		id="grouprecommendation-end"
		title="<isa:translate key="access.grp.end" arg0="<%=recogrpTxt%>"/>"
></a>
<%
		}
%>
</div>