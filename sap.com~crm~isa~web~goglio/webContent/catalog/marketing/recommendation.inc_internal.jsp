<%-- RecommendationUI ui must be define outside  --%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.isacore.action.marketing.ShowRecommendationAction" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.isacore.action.marketing.MaintainProfileAction" %>
<%@ page import="com.sap.isa.catalog.uiclass.ProductsUI" %>

 <% boolean nouserprofileavailable  = false; %>
    <isacore:ifShopProperty property = "userProfileAvailable" value = "false">
        <% nouserprofileavailable = true; %>
    </isacore:ifShopProperty>

    <div class="module">
       <div class="module-name"><isa:moduleName name="catalog/marketing/recommendation.inc_internal.jsp"/>
       </div>

        <% /* Check, if the productlist contains products */
           if (ui.getProductList().size() == 0) {
             if (ui.supportPersRec()) { %>
             <table width="98%">
               <tbody>
                 <tr>
                  <td>
                  <% if (nouserprofileavailable) { /* link to the customer's profile */ 
                     }
                     else {
                       if (((userSessionData.getAttribute(MaintainProfileAction.PROFILE_SAVED_DURING_CURRENT_SESSION) != null) 
                           && (userSessionData.getAttribute(MaintainProfileAction.PROFILE_SAVED_DURING_CURRENT_SESSION).equals("true")))) { %>
                           <%-- rock bottom = no recommendations can be determined  --%>
                          <span><isa:translate key="recommendation.jsp.notFound"/><br>
                          <isa:translate key="recommendation.jsp.notFound2"/></span>
                    <% }
                       else { %>
                         <%-- rock bottom1: user should maintain profile first  --%>
                          <span><isa:translate key="b2c.recommendation.rockBottom1"/>
                          <isa:translate key="b2c.recommendation.rockBottom2"/>
                          <isa:translate key="b2c.marketing.preProfilLink1"/><br>
                       <% if (ui.showPersRecLoginLink()) { %>
                             <isa:translate key="b2c.marketing.preProfilLink2"/></span>
	                         <a href="<isa:webappsURL name="/accountProfileForward.do"/>"><isa:translate key="b2c.marketing.profilLink"/></a>
                             <isa:translate key="b2c.marketing.postProfilLink"/>
                       <% } %>
                    <% }
                    }
                 } %>
                 </td>
              </tr>
            </tbody>
          </table>
      <% } 
         else { 
           ui.setFormSuffix("rc"); 
		   ProductsUI prodUI = new ProductsUI(pageContext); %>
		  <%@ include file="../../catalog/productlist.block.inc.jsp" %>
     <% } %>
   </div>