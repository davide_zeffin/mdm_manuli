<%@ include file="/catalog/transferToOrder.inc.jsp" %>

<%  String[] rowValues = {"odd", "even"};
	JspUtil.Alternator oddEven = new JspUtil.Alternator(rowValues);
	String oddEvenStr = ""; 
    
	String configurableFlag = "";
	WebCatItem configItem = null; 
	boolean setAnchor = false; 
	boolean displayAsLink = true; 
	String lastVisited = "itemList";
	if(prodUI.isCatalogQuery()) {
		lastVisited = "catalogQuery";
	}
	String formSuffix = ui.getFormSuffix();
	String baseCUA = new String();
	
	boolean isMarketingFuncs = false;
	if(	(prodUI.IsProductList() != null && !prodUI.IsProductList().equals(""))
			&&
		(
			prodUI.isBestSeller() ||
			prodUI.isRecommendations() ||
			(prodUI.isCUAList() && prodUI.isCUAProductId())
		)
		) {
		isMarketingFuncs = true;
	}
%>
<div class="module-name"><isa:moduleName name="catalog/productlist.list.inc.jsp" /></div>
<div id="cat-pcat-lstv">
	<%@ include file="/catalog/js/productlist.js.inc.jsp" %>

	<form name="productform" action='<isa:webappsURL name="/catalog/updateItems.do"> <isa:param name="areaid" value="workarea"/></isa:webappsURL>'
		  method="POST">
          
		<% if (!prodUI.isCatalogQuery()) { %>
			<%@ include file="/catalog/ProductPerPage.inc.jsp"%>
		<% } %>
		<%@ include file="/catalog/SearchResults.inc.jsp"%>
		<%@ include file="/catalog/PageLink.inc.jsp"%>
		<%-- Catalog Search Result Enhancements --%>
		 <input type="hidden" name="page" value="">
		 <input type="hidden" name="itemPageSize" value="<%= prodUI.getItemPage().pageSize() %>">
		 <input type="hidden" name="next" value="">
		 <input type="hidden" name="itemkey" value="">
		 <input type="hidden" name="order" value="">
		 <input type="hidden" name="contractkey" value="">
		 <input type="hidden" name="contractitemkey" value="">
		 <input type="hidden" name="itemId" value="">
		 <input type="hidden" name="topItemkey" value="">
		 <input type="hidden" name="lastVisited" value="<%=ui.getLastVisited()%>"/>
		 <input type="hidden" name="display_scenario" value="<%= (prodUI.isCatalogQuery()) ? "query" : "products"%>">    
<%  if(prodUI.isCUAList()) { %>
        <input type="hidden" name="<%=ActionConstants.RA_TRANSFER_CUA%>" value="true" />
<%  } %>
		 <%-- isQuery has to be set only if it's a catalog query; do not fill with space --%>
         <% if (prodUI.isCatalogQuery()) { %>
         <input type="hidden" name="isQuery" value="yes">
         <% } %>
         
		<input type="hidden" name="isProductList" value="<%=JspUtil.encodeHtml(prodUI.IsProductList())%>" />
		<% if(prodUI.getDetailScenario() != null) { %>
		<input type="hidden" name="<%=ActionConstants.RA_DISPLAYSCENARIO %>" value="<%=JspUtil.encodeHtml(prodUI.getDetailScenario())%>" />
		<% } %>
         
         
		 <%-- Start: Table --%>
	  <% if (prodUI.getItemPage().size() > 0)  {
			String tableTitle = WebUtil.translate(pageContext, "b2c.cat.items.table", null);
			if (prodUI.isAccessible()) { %>
			   <a id="itemstablebegin" href="#itemstableend" title="<isa:translate key="access.table.begin" arg0="<%= tableTitle %>" arg1="<%=Integer.toString(prodUI.getRowNumber())%>" arg2="<%=Integer.toString(prodUI.getColNumber())%>" arg3="<%=Integer.toString(prodUI.getFirstRowNumber())%>" arg4="<%=Integer.toString(prodUI.getLastRowNumber())%>"/>"></a>
		 <% } %>

		 <table id="itemstable" class="app-std-tbl" summary="<%= tableTitle %>">
			<thead>
				<tr>
					<th id="col_1" class="cat-prd-thumb top-left"
						scope="col" title="<isa:translate key="catalog.isa.image"/>">
						<span>
						<% if( ui.isAccessible() ) { %>
							 <isa:translate key="catalog.isa.image"/>
						<% } else { %>
							  &nbsp;
						<% } %>
						</span>
					</th>
					<% String headerText = WebUtil.translate(pageContext, "catalog.isa.description", null);
					   if (priceUI.isBuyPointsArea()) { 
						   headerText = WebUtil.translate(pageContext, "catalog.isa.descr.buyPts", new String[] { String.valueOf(JspUtil.encodeHtml(priceUI.getPointsUnitDescr())) } );
					   } %>
					<th id="col_2" 
						class="cat-prd-data"
						scope="col" 
						title="<%= JspUtil.encodeHtml(headerText) %>"
						colspan="<%=prodUI.getContrAttrDescsSize()+1%>">
					  <span>
					 <% if(prodUI.getRowNumber() > 1 ) { %>
						  <a href="javascript:order('OBJECT_DESCRIPTION')"><%= JspUtil.encodeHtml(headerText) %></a>
					 <% } 
						else { %>
						   <%= JspUtil.encodeHtml(headerText) %>
					 <% } %>
					  </span>
					</th>
<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% if( !current_user.equalsIgnoreCase(USER_GUEST) ) { %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>					
					<%-- Header of Prices Column --%>
 			 <% if (prodUI.isPriceVisible() && !prodUI.isNoPriceUsed()) { %>
					<th id="col_3" class="cat-prd-prc" scope="col" 
					    title="<isa:translate key="catalog.isa.price"/>">
					    <span><isa:translate key="catalog.isa.price"/></span>
					</th>
			  <% } %>
			  <% if (prodUI.isQuantityVisible()) { %>  
					<th id="col_4" class="cat-prd-qty"
						scope="col" title="<isa:translate key="catalog.isa.quantity"/>">
						<span><isa:translate key="catalog.isa.quantity"/></span>
					</th>
			  <% } %>
					<th id="col_5" class="cat-btn-lst"
						scope="col" title="<isa:translate key="catalog.isa.functions"/>">
						<span>
					 <% if( ui.isAccessible() ) { %>
						  <isa:translate key="catalog.isa.functions"/>
					 <% } else { %>     
						  &nbsp;
					 <% } %>
						</span>
					</th>
				 <% if (prodUI.showMultiSelection() || ( prodUI.isCRM() && ui.isCompareAllowed())) { %>
					<th id="col_6" class="cat-prd-comp top-right app-std-tbl-th-last" 
						scope="col" title="<isa:translate key="table.row.select"/>">
						<span>
					 <% if (ui.isAccessible()) { %>
						   <isa:translate key="table.row.select"/>
					 <% } 
					    else { 
							if (prodUI.showMultiSelection()) { 
								   String checkBoxTitle = 
										WebUtil.translate(pageContext, "catalog.isa.selectAll", null) + 
										" / " + 
										WebUtil.translate(pageContext, "catalog.isa.unselectAll", null);
					 %>    
						   <input type="checkbox" 
								  name="allitemsselected" 
								  value="true" 
								  alt="<%= checkBoxTitle %>"
								  title="<%= checkBoxTitle %>"
								  onclick="javascript:selectAllItemsToggled()" />
					 <%    }
						} %> 
						</span>
					</th>
				 <% } %>   
<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% } %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>				                  
				</tr>
			</thead>
			<tbody>
			  <% int i = -1; %>
			  <isa:iterate id="item" name="itemPage" type="com.sap.isa.catalog.webcatalog.WebCatItem">      
				<% i++; %>
				 <div style="display: none"> <%-- make hidden fields invisible because of IE bug --%>
					 <input type="hidden" name="item[<%= i %>].itemID" value="<%= item.getItemID() %>">
				 </div>    

				<%-- Here starts the code for the main item data --%>
				<% String baseURL = "/catalog/updateItems.do?next=seeItem&itemkey=" + item.getItemID();
				   String oddEvenCssClass = "";
				   oddEvenStr = oddEven.next();
				   if (prodUI.isSubTitleTrue(item)) {
					   oddEvenCssClass = "app-std-tbl-" + oddEvenStr + " b2c-prd-with-subt";
				   } else {     
					   oddEvenCssClass = "app-std-tbl-" + oddEvenStr;
				   }
				%>
				<tr class="<%= oddEvenCssClass %>
				<% prodUI.setWebCatItem(item);
				   if (prodUI.showContracts() && prodUI.getContractItemsArray() != null) {
					  if ( prodUI.getContractItemsAmount() > 0){ %>
						   cat-prd-cntr-itmhascntr
				   <% }
				   } %>
				">
					<%-- image --%>
					<isa:ifThumbAvailable name="item">
						<td headers="col_1" class="cat-prd-thumb">
							<a href="#" onclick="javascript:displayProdDetails('<isa:webappsURL name="<%=baseURL%>"/>', '<%=ui.getFormSuffix()%>')"                                                         
							><img src="<isa:imageAttribute guids="DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="item" />" 
								  alt="<%=item.getDescription()%>" 
							 /></a>
						</td>
					</isa:ifThumbAvailable>
					<isa:ifThumbNotAvailable name="item">
						<td headers="col_1" class="cat-prd-nothumb">   
							<img class="nothumb" src="<%= WebUtil.getMimeURL(pageContext, "mimes/catalog/images/spacer.gif") %>" alt="" />
						</td>
					</isa:ifThumbNotAvailable>
					<%-- product description --%>
					<td headers="col_2" 
						class="cat-prd-data"
						colspan="<%=prodUI.getContrAttrDescsSize()+1%>">

						<%-- Product id --%>
						<div class="cat-prd-id" title="<isa:translate key="catalog.isa.product"/>"> 
							<%= prodUI.getHighlightedResults(item.getAttributeByKey(AttributeKeyConstants.PRODUCT), htTerms, AttributeKeyConstants.PRODUCT, "cat-prd-hli") %>
						</div> 

						<%-- Description --%>
						<div class="cat-prd-dsc">
							<a href="#" onclick="javascript:displayProdDetails('<isa:webappsURL name="<%=baseURL%>"/>', '<%=ui.getFormSuffix()%>')"                                                         
							><%= prodUI.getHighlightedResults(item.getDescription(), htTerms, AttributeKeyConstants.PRODUCT_DESCRIPTION, "cat-prd-hli") %></a
							>
						</div>
						
<%-- GREENORANGE BEGIN --%>
						<div class="cat-prd-var">
						<%-- if (item.getAttribute("CH0032D") != "" && item.getAttribute("CH0032V") != ""){ %>
						<div class="cat-prd-var">
							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("CH0032D")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("CH0032V")) %>
							</span>
						</div>
						<% } --%>
						<% if (item.getAttribute("Z_DESCRIZIONE_ISAD") != "" && item.getAttribute("Z_DESCRIZIONE_ISAV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_DESCRIZIONE_ISAD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_DESCRIZIONE_ISAV")) %>
							</span><br/>

						<% } %>
						<% if (item.getAttribute("Z_DESCR_COLORED") != "" && item.getAttribute("Z_DESCR_COLOREV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_DESCR_COLORED")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_DESCR_COLOREV")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_FAMMATER_RESD") != "" && item.getAttribute("Z_FAMMATER_RESV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_FAMMATER_RESD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_FAMMATER_RESV")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_LARGHEZZA_PF_INTD") != "" && item.getAttribute("Z_LARGHEZZA_PF_INTV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_LARGHEZZA_PF_INTD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_LARGHEZZA_PF_INTV")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_LUNGHEZZA_PF_INTD") != "" && item.getAttribute("Z_LUNGHEZZA_PF_INTV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_LUNGHEZZA_PF_INTD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_LUNGHEZZA_PF_INTV")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_GR_CADAUNO_TOTD") != "" && item.getAttribute("Z_GR_CADAUNO_TOTV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_GR_CADAUNO_TOTD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_GR_CADAUNO_TOTV")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_IMBALLO_INTERNO_NUM_PFD") != "" && item.getAttribute("Z_IMBALLO_INTERNO_NUM_PFV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_IMBALLO_INTERNO_NUM_PFD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_IMBALLO_INTERNO_NUM_PFV")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_IMBALLO_ESTERNO_NUM_PFD") != "" && item.getAttribute("Z_IMBALLO_ESTERNO_NUM_PFV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_IMBALLO_ESTERNO_NUM_PFD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_IMBALLO_ESTERNO_NUM_PFV")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_PZ_CONTAINER_20D") != "" && item.getAttribute("Z_PZ_CONTAINER_20V") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_PZ_CONTAINER_20D")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_PZ_CONTAINER_20V")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_PZ_CONTAINER_40D") != "" && item.getAttribute("Z_PZ_CONTAINER_40V") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_PZ_CONTAINER_40D")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_PZ_CONTAINER_40V")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_RIENT_PFD") != "" && item.getAttribute("Z_RIENT_PFV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_RIENT_PFD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_RIENT_PFV")) %>
							</span><br/>
							
						<% } %>
						<% if (item.getAttribute("Z_COD_VALVOLAD") != "" && item.getAttribute("Z_COD_VALVOLAV") != ""){ %>

							<span class="cat-prd-id"> 
								<%= JspUtil.encodeHtml(item.getAttribute("Z_COD_VALVOLAD")) %>:
							</span>
							<span class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getAttribute("Z_COD_VALVOLAV")) %>
							</span><br/>
							
						<% } %>
						</div>						
<%-- GREENORANGE END --%>

						<% if (prodUI.showProductVariantInfo(item)) {%>
						<div class="cat-prd-var">
							<span class="cat-prd-var-title"><isa:translate key="catalog.isa.currentVariant"/>:</span>
							<div class="cat-prd-id" title="<isa:translate key="catalog.isa.product"/>"> 
								<%= JspUtil.encodeHtml(item.getConfigItemReference().getProductId()) %>
							</div>
							<div class="cat-prd-dsc">
								<%= JspUtil.encodeHtml(item.getConfigItemReference().getProductDescription()) %>
							</div>
						<% } %>
<%-- GREENORANGE BEGIN
						<%-- Inline Configuration for Configurable Product-%>
						<% if (item.isConfigurable() == true) { %>
							  <div class="cat-prd-cfg">
								 <%  //theses variables should be defined before to call itemconfiginfo.inc.jsp
								 int viewType = 2; 
								 boolean isSubItem = false; 
								 IPCItem itemConfig = item.getConfigItemReference(); %>
								 <%@ include file="/catalog/itemconfiginfo.inc.jsp" %> 
							  </div>                                      
						<% } %>

						<%-- config links -%>   
						<% configItem = item; %>
						<%@ include file="/catalog/configLink.inc.jsp"%>
 GREENORANGE END --%>
						<%-- display Accessories or related products link --%>
<% if(!isMarketingFuncs) { %>
						<% if (prodUI.showCatListCUALink()) { %>
						  <isacore:ifShopProperty property = "accessoriesAvailable" value = "true" >
						  <% if ( ui.showAccessoriesLink(item) ) {
							   baseCUA = "/catalog/updateItems.do?next=accesories&itemkey=" + item.getItemID();
						  %>
							<div class="cat-prd-data-cualink">
							  <a href="#" onclick="javascript:displayProdDetails('<isa:webappsURL name="<%=baseCUA%>"/>', '<%=ui.getFormSuffix()%>')" title="<isa:translate key="catalog.isa.accesories"/>"><isa:translate key="catalog.isa.accesories"/></a>
							</div>
						  <% } %>
						  </isacore:ifShopProperty>
						  <isacore:ifShopProperty property = "crossSellingAvailable" value = "true" >
						  <% baseCUA = "/catalog/updateItems.do?next=crossSelling&itemkey=" + item.getItemID();%>
							<div class="cat-prd-data-cualink">
							  <a href="#" onclick="javascript:displayProdDetails('<isa:webappsURL name="<%=baseCUA%>"/>', '<%=ui.getFormSuffix()%>')" title="<isa:translate key="catalog.isa.crossSelling"/>"><isa:translate key="catalog.isa.crossSelling"/></a>
							</div>
						  </isacore:ifShopProperty>                         
						<% } %>
<%      	} // end if !isMarketingFuncs %>

					</td>
<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% if( !current_user.equalsIgnoreCase(USER_GUEST) ) { %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>
					<%-- Prices --%>
				 <% if (prodUI.isPriceVisible() && !prodUI.isNoPriceUsed()) { %>  
					<td headers="col_3" class="cat-prd-prc">
					   <% priceUI.setPriceRelevantInfo(item, item.isRelevantForExplosion(), true);  %>
					   <%@ include file="pricing.list.inc.jsp" %>
					   <%-- GREENORANGE BEGIN --%>
					   <div class="cat-prd-id">
							<%  
								String pcPrice = item.getAttribute("PRICE_V") + " " +
								  				item.getAttribute("PRICE_D") +
								  				"/PC";
							%>
							<%= pcPrice %>
					   </div>
					   <%-- GREENORANGE END --%>
					</td>
				 <% } %>
					<%-- Quantity and UOM with Combo-box for multiple units if applicable --%>
				 <% if (prodUI.isQuantityVisible(item)) { %>  
					<td headers="col_4" class="cat-prd-qty">
						<input type="<%= (prodUI.isQuantityChangeable(item)) ? "text" : "hidden" %>" class="textInput" name="item[<%= i %>].quantity" value="<%= item.getQuantity() %>" size="4" maxlength="8">
					 <% if (!prodUI.isQuantityChangeable(item)) { %>
					   <%= item.getQuantity() %>
					 <% } %>
					 <% if (item.getUnitsOfMeasurement().length == 1 || !prodUI.isQuantityChangeable(item)) { %>
						<%=item.getUnit().trim()%> 
						<input type="hidden" name="item[<%= i %>].unit" value="<%= item.getUnit().trim()%>"/>
					 <% } 
						else { %>
						<select name="item[<%= i %>].unit" width=4 onChange=setUnit('<%=ui.getFormSuffix()%>')>
						<% for (int j = 0; j < item.getUnitsOfMeasurement().length; j++) { %>
							<option value="<%= item.getUnitsOfMeasurement()[j].trim()%>"
							<% if (item.getUnitsOfMeasurement()[j].trim().equalsIgnoreCase(item.getUnit().trim()) ) {%> 
							   SELECTED 
							<% } %>
							   ><%= JspUtil.encodeHtml(item.getUnitsOfMeasurement()[j])%></option> 
						<% } %>
						</select>
					 <% } %>
					</td>
				 <% } else { %>
						<input type="hidden" class="textInput" name="item[<%= i %>].quantity" value="<%= item.getQuantity() %>">
						<input type="hidden" name="item[<%= i %>].unit" value="<%= item.getUnit().trim()%>"/>
				 <% } %>
                 
					<%-- Icons --%>
					<td headers="col_5" class="cat-btn-lst">
				  <% if (ui.showSelectedProduct()) { %>
						<input type="button" class="FancyButton" 
							   onclick="javascript:addSingleItem('<%= item.getItemID() %>','addToBasket','<%=ui.getFormSuffix()%>')" 
							   title="<isa:translate key="catalog.button.selprod"/>" 
							   value="<isa:translate key="catalog.button.selprod"/>" />
				   <% }
					  else { %>
						<span class="imgbtn-box">
						  <a class="imgbtn-lk" 
							 href="javascript:addSingleItem('<%= item.getItemID() %>','addToBasket','<%=ui.getFormSuffix()%>')" 
							 title="<isa:translate key="b2c.product.jsp.addToBasket"/>" >
							 <img class="cat-imgbtn-addtocart" 
								  src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
								  alt="<isa:translate key="b2c.product.jsp.addToBasket"/>" 
								  width="16" height="16" border="1"
						  /></a>
						</span>  
					<% } %>
					<% if (ui.isAddToLeafletAllowed()) { %>
						<span class="imgbtn-box">
						  <a class="imgbtn-lk" href="javascript:getMoreFunctions('<%= item.getItemID() %>','leaflet','<%=ui.getFormSuffix()%>')" 
							 title="<isa:translate key="catalog.isa.leaflet"/>"
							><img class="cat-imgbtn-addtolfl" 
								  src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
								  alt="<isa:translate key="catalog.isa.leaflet"/>" 
								  width="16" height="16" border="1"
						  /></a>
						</span>  
					<% } %>
					<%-- AVW --%>
					<% if (prodUI.showAVWLink()) { 
						   prodUI.setWebCatItem(item);%>
					<%     if (item.getEAuctionLink() != null && !item.getEAuctionLink().equals("false")) { %>
						<a href="<isa:webappsURL name="/buyer/showauctions.do"> 
									 <isa:param name="_productID" value="<%=item.getEAuctionLink()%>"/>
								 </isa:webappsURL>" 
						   title="<isa:translate key="eAuctions.buyer.auctioncatalog.title"/>" 
						  ><img class="cat-imgbtn-convtoauc" 
								src="<isa:mimeURL name="auction/images/layout/convertToAuction.gif" />"
								alt="<isa:translate key="catalog.isa.auctions" />"
								width="16" height="16" border=0  
						/></a>
					<%     } %>
					<% } %>
                    
					</td>
					<%-- Check Box --%>
				 <% if (prodUI.showMultiSelection() || (prodUI.isCRM() && ui.isCompareAllowed())) { %>
					<td headers="col_6" class="cat-prd-cmp app-std-tbl-td-last">
						<% if (prodUI.showMultiSelection()) { %>
							   <input type="checkbox" 
							   name="item[<%= i %>].selected" 
							   value="true" 
							   <%= item.isSelectedStr() %>
							   onclick="javascript:checkAllitemsselected()"
							   title="<isa:translate key="catalog.isa.checkboxTooltipMult"/>"  
						<% }
						   else if (prodUI.isCRM() && ui.isCompareAllowed()) { %>
							   <input type="checkbox" 
							   name="item[<%= i %>].selected" 
							   value="true" 
							   <%= item.isSelectedStr() %>
							   title="<isa:translate key="catalog.isa.checkboxTooltip"/>"  
						<% } %>
					</td>
				<% } %>  
<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% } %>
<%-- GREENORANGE END (showing only if user is not user_guest) --%>				                  
			  </tr>
			  <%-- Message field --%>
			  <% String messageText = null;
				 if( ( messageText= ui.getMessage(item)) != null) { %>
			  <tr>
				  <td colspan="6" class="app-std-tbl-th-last">
					  <div class="cat-prd-msg">
						  <div class="error"><span><%=JspUtil.encodeHtml(messageText)%></span></div>
					  </div>
				  </td>
			  </tr>
			  <% } %>  
<%-- End of code for the main item data --%>
                
<%-- Start of code for Contract Subitems --%>
<%  if (prodUI.showContracts()) {
		prodUI.setWebCatItem(item);
		int contractItemAmount = prodUI.getContractItemsAmount();
		if (prodUI.getContractItemsArray() != null) {
			java.util.Iterator contractItems=prodUI.getContractItemsArray().iterator();
			if (contractItems.hasNext()) {
%>            	
			  <tr class="<%= oddEvenCssClass %> cat-prd-cntr-rowtitle">
				<td headers="col_1" class="cat-prd-nothumb">&nbsp;</td>
				<td headers="col_2" class="cat-prd-data">
				  <div class="cat-prd-ctr-title">
					<isa:translate key="catalog.isa.contracts" />
				  </div>
				</td>
<%              boolean lastContrAttr = false;
				if (prodUI.getContrAttrDescsSize() > 0) { 
					for (int j=0; j < prodUI.getContrAttrDescsSize(); j++) { 
						if ( prodUI.getContrAttrDescsSize() == (j+1) ) {
							lastContrAttr = true;
						}
						if(lastContrAttr){ %>
						  <th class="cat-prd-cntr-attr-last">
					 <% }
						else{ %>
						  <th>
					 <% } %>
					 <%= JspUtil.encodeHtml(prodUI.getContrAttrDesc(j)) %></th>
<%                  }
				} 
%>
				<td headers="col_3" class="cat-prd-prc">&nbsp;</td>
				<td headers="col_4" class="cat-prd-qty">&nbsp;</td>
				<td headers="col_5" class="cat-btn-lst">&nbsp;</td>
				<td headers="col_6" class="cat-prd-cmp app-std-tbl-td-last">&nbsp;</td>
			  </tr>
<%          } 
			boolean firstContractItem = true;
			boolean lastContractItem = false;
			int contractItemNumber = 0;
			while (contractItems.hasNext()) {
				i++;
				contractItemNumber++;
				if(contractItemNumber == contractItemAmount){
					lastContractItem = true;
				}
				prodUI.setSubItem((WebCatItem)contractItems.next());
%>
			  <div style="display: none"> <%-- make hidden fields invisible because of IE --%>
				<input type="hidden" name="item[<%= i %>].itemID" value="<%= prodUI.getContractItem().getItemID() %>" />
				<input type="hidden" name="item[<%= i %>].contractKey" value="<%= prodUI.getContractkey()%>" />
				<input type="hidden" name="item[<%= i %>].contractItemKey" value="<%= prodUI.getContractItemKey()%>" />
				<input type="hidden" name="item[<%= i %>].unit" value="<%= prodUI.getContractItem().getUnit().trim() %>" />
			  </div>
			  <tr class="<%= oddEvenCssClass %>
			  <% if(lastContractItem){ %>
					cat-prd-cntr-rowitem-last              	
			  <% }
				 else{
					if(firstContractItem){%>
					  cat-prd-cntr-rowitem-first
				  <%}
					else { %>
					   cat-prd-cntr-rowitem
				 <% }
				 } %>
			  ">
				<%-- do not show image for contracts --%>
				<td headers="col_1" class="cat-prd-nothumb" >&nbsp;</td>
				<td headers="col_2" class="cat-prd-data">
				  <div class="cat-prd-ctr-data">
                  
					<%-- ContractItemId & ContractId --%> 
					<isa:translate key="catalog.isa.contractitem" />: <%= prodUI.getContractId()%> / <%= prodUI.getContractItemId()%>

					<%-- ConfigLink --%>
			
<%              if (prodUI.showConfLinkCol()) { %>
					<% configItem = prodUI.getSubItem(); %>
					<%@ include file="/catalog/configLink.inc.jsp"%>
<%              } %>
 
				  </div>
				</td>
				<%-- At this point it might be that you should iterate over all the detail names for a contract that should be displayed --%>
<%              if (prodUI.getContrAttrDescsSize() > 0) { 
					for (int q=0; q < prodUI.getContrAttrDescsSize(); q++) { %>
				<td><%= prodUI.getContractItemValue(q)+ " " + JspUtil.encodeHtml(prodUI.getContractUnitValue(q)) %></td>
<%                  } 
				} %>

				<%-- Prices --%>  
<%              if (prodUI.isPriceVisible() && !prodUI.isNoPriceUsed()) { %>    
				<td headers="col_3" class="cat-prd-prc">
<%                  priceUI.setPriceRelevantInfo(prodUI.getSubItem(), prodUI.getSubItem().isRelevantForExplosion(), true);  %>
				  <%@ include file="pricing.list.inc.jsp" %>
				</td>
<%              } %>
                
				<%-- Quantity --%>
				<td headers="col_4" class="cat-prd-qty">
				  <input type="<%= (prodUI.isQuantityChangeable(item)) ? "text" : "hidden" %>" 
						 class="textInput" 
						 name="item[<%= i %>].quantity" 
						 value="<%= item.getQuantity(prodUI.getContractItemKey()) %>" size="4" maxlength="8" />
<%              if (!prodUI.isQuantityChangeable(item)) { %>
				  <%= item.getQuantity(prodUI.getContractItemKey()) %>
<%              } %>
				</td>
				<%-- Icons --%>
				<td headers="col_5" class="cat-btn-lst">
				  <span class="imgbtn-box">
					<a class="imgbtn-lk" href="javascript:addSingleContractItem('<%= item.getItemID() %>','<%= prodUI.getContractkey()%>','<%= prodUI.getContractItemKey()%>', 'addToBasket','<%=ui.getFormSuffix()%>')" 
					   title="<isa:translate key="b2c.product.jsp.addToBasket"/>" >
					  <img class="cat-imgbtn-addtocart" src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
						   alt="<isa:translate key="b2c.product.jsp.addToBasket"/>" width="16" height="16" border="1"
					/></a>
				  </span>  
				</td>
				<%-- Check Box --%>
				<td headers="col_6" class="cat-prd-cmp app-std-tbl-td-last">
				  <input type="checkbox" name="item[<%= i %>].selected" value="true" <%= prodUI.getSubItem().isSelectedStr() %> />
				</td>                
<%             firstContractItem = false;
			  } // end of while loop %>
			  </tr>
<%      } 
	}  
%>
<%-- End of code for Contract Subitems --%>
        
			  </isa:iterate>
			</tbody>
		 </table>

	  <% if (prodUI.isAccessible()) {%>
		 <a id="itemstableend" href="#itemstablebegin" title="<isa:translate key="access.table.end" arg0="<%= tableTitle %>"/>"></a>
	  <% } %>
	  <%-- End Box--%>

		 <script type="text/javascript">
			<%-- used for javascript functions --%>
			count = <%= i %>;
		 </script>
	  <% if (prodUI.showMultiSelection()) { %>
			  <script type="text/javascript">
			  <%-- calculate if allitemsselected should be checked --%>
				 function checkAllitemsselected() {
					var allitemsselected = true;
					for(i=0; i <= count; i++) {
					   var checkboxname = "item[" + i + "].selected";
					   if(!document.getElementsByName(checkboxname)[0].checked) {
						   allitemsselected = false;
						   break;
					   }
					}
					if(allitemsselected){
					   document.productform.allitemsselected.checked = true;
					}
					else {
					   document.productform.allitemsselected.checked = false;
					}
				 }
				 checkAllitemsselected(); <%-- initially calculate as well... --%>
			  </script>
	  <% } %>

		 <%-- Start: Add selected items to cart Button and Compare Button --%>
		 <table class="cat-prodlist-btnlist">
			<tbody>
				<tr>
					<td>
					<% if (prodUI.showMultiSelection()) { 
						   if(ui.isAccessible()) { 
							   accessText = WebUtil.translate(pageContext, "catalog.isa.selectAll.access", null); 
						   } %>       
						<input type="button" title="<isa:translate key="access.button" arg0="<%=accessText%>" arg1=""/>" value="<isa:translate key="catalog.isa.selectAll"/>" onclick="javascript:setNext('selectAll')" class="FancyButtonGrey">
						<% if( ui.isAccessible() ) {
							   accessText = WebUtil.translate(pageContext, "catalog.isa.unselectAll.access", null);
						   } %>      
						<input type="button" title="<isa:translate key="access.button" arg0="<%=accessText%>" arg1=""/>" value="<isa:translate key="catalog.isa.unselectAll"/>" onclick="javascript:setNext('unselectAll')" class="FancyButtonGrey">
						<% if (ui.isAccessible() ) {
							   accessText = WebUtil.translate(pageContext, "catalog.isa.addItems.access", null);        
						   } %>
						<input type="button" title="<isa:translate key="access.button" arg0="<%=accessText%>" arg1=""/>" value="<isa:translate key="catalog.isa.addItems"/>" onclick="javascript:setNext('addToBasket')" class="FancyButtonGrey">
					<% }
					   if (prodUI.isCRM() && ui.isCompareAllowed()) { 
					       if( ui.isAccessible() ) {
							   accessText = WebUtil.translate(pageContext, "catalog.isa.compareItems", null);
						   } %>
						<input type="button" title="<isa:translate key="access.button" arg0="<%=accessText%>" arg1=""/>" value="<isa:translate key="catalog.isa.compareItems"/>" onclick="javascript:popUpCompare(<%= prodUI.getAmountOfSelectedItems() %>)" class="FancyButtonGrey">
					<% }
					   if (prodUI.showMultiSelection() || (ui.isCompareAllowed() && prodUI.isCRM())) { %>
						<span class="cat-chkbox-arrow">      
							<img class="cat-prodlist-chkbox-arrow" 
								 src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
								 alt="" />
						</span>
					<% } %>
					</td>
				</tr>
				<%-- End of Add selected items to cart end Compare Button --%>
				<br /> 
			</tbody>
		 </table>
		 <%@ include file="/catalog/PageLink.inc.jsp"%>
	</form>
<% } 
   else { %>
	  <br />
	  <div class="areainfo"><isa:translate key="catalog.isa.NoItems"/></div>
<% }
   // not argument for the title, then make one and set the title using the manual hard code
   if (!prodUI.isTitleArgSet()) { %>
	 <script>
	   document.title = '<%= prodUI.getDocumentTitle() %>';
	   window.name = 'main';
	 </script>
<% } %>
</div>