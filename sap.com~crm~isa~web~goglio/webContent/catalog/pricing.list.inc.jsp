<%-- 
*****************************************************************************

    Include:      pricing.list.inc.jsp
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      19.04.2006

***************************************************************************** 

 Price rendering for catalog items.

 Required includes:
 ==================
 pricing.java.inc.jsp - has to be initialized with the initPriceInclude() method.
 pricing.inc.js - contains javascript functions to toggle scale price display 
 
 Required global variables:
 ==========================
 PricingBaseUI priceUI
                                     
 usage:
 ======
  ...
  <script src="<isa:webappsURL name="catalog/js/pricing.inc.js"/>" type="text/javascript"></script>
  ...
  <%@  include file="pricing.java.inc.jsp"                                    %>
  <%   PricingBaseUI priceUI = <any sub class of PricingBaseUI>;              %>
  ... 
  <%   priceUI.setPriceRelevantInfo(priceProd, sumsOnly, showLabelAsPostfix); %>
  <%   initPriceInclude(priceUI.getPageContext(), priceUI.log);               %>
  <%@  include file="pricing.list.inc.jsp"                                    %>
  
*****************************************************************************  
--%> 
 
<%@ page import="com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo" %>
<%@ page import="com.sap.isa.core.logging.IsaLocation" %>
<%@ page import="com.sap.isa.core.util.*" %>

<%@ taglib uri="/isa" prefix="isa" %>

<%-- Pricing --%>
<%  
{
    String groupId = null;
    int startIndex = 0;
    boolean pricesAvailable = false;
	PriceInfo[] prices = priceUI.getPricesForPriceProduct();
    int noOfPrices = 0;
    String colspan = null;

    if (!priceUI.isNoPriceUsed()) {
        prices = priceUI.getPricesForPriceProduct();
        
        if (prices != null && prices.length > 0) {
            pricesAvailable	= true;
            noOfPrices = prices.length;
        }
        colspan = priceUI.getNonScalePriceColSpan(prices);
    }
	
    priceUI.log.debug("priceInclude: priceUI.isShowLabelAsPostfix()=" + priceUI.isShowLabelAsPostfix());
	
        // Price eye catcher
        if (priceUI.displayEyeCatcherText() && priceUI.getPriceEyeCatcherText() != null && priceUI.getPriceEyeCatcherText().trim().length() > 1) { %>
         <div class="cat-prd-peyec">
             <%= JspUtil.encodeHtml(priceUI.getPriceEyeCatcherText()) %> 
         </div>
<%      } %>

<%      if (pricesAvailable) { %>   
     	
    	<div class="<%= priceUI.getPriceDivName() %>">
    	
    	<%-- Display title for current total --%>
    	<% if (priceUI.getTitleTextKey() != null) { %>
	    	<p class="<%= priceUI.getTitleClass() %>"> <isa:translate key="<%= priceUI.getTitleTextKey() %>"/></p>
    	<% } %>

     	<table summary="<isa:translate key="b2c.acc.cat.prices"/>">
<%      } %>

<%      for (int iPr=0; iPr < noOfPrices; iPr++) {
	    
            priceUI.log.debug("priceInclude: processing Price = " + prices[iPr]);

		    // hide promotional prices, if requested
		    if (priceUI.getHidePromotionalPrices() && !prices[iPr].isPayablePrice()) {
		        continue;
		    }
		    
            // style for output of prices
            String priceStyle = getPriceClass(priceUI, prices[iPr]); 

            // Display Points instead of prices in reward catalogs
            if (prices[iPr].isPointsPrice()) { 
                // Display only points with identical points unit
                if (priceUI.isWrongPtUnit(prices[iPr].getCurrency())) {
                    continue; 
                }
	            priceUI.log.debug("priceInclude: Points Price"); %>       
            <tr> 
                <% if (!priceUI.isShowLabelAsPostfix()) { %> <%-- Print a label, if wanted --%>
    		        <td class="cat-prd-prc-lbl"><%= getPriceLabel(priceUI, prices[iPr], false) %>&nbsp;</td>		
    		    <% } %>
    		    <%-- Print the price with currency --%>
    		    <td class="cat-prd-prc" colspan="<%= colspan %>">
    		    <%-- Price analysis link if necessary is not displayed --%>
                   <%= prices[iPr].getPrice() %>&nbsp;<%= priceUI.getPointsUnitDescr() %>
		        </td>  
   			    <% if (priceUI.isShowLabelAsPostfix()) { // Print a postfix, if wanted
   			           String pricePostFixTxt = getPricePostFix(priceUI, prices[iPr]);
   			           String pricePostFixCssClass = "cat-prd-prc-lbl-post";
   			           if(pricePostFixTxt == null || pricePostFixTxt.equals("")) {
   			               pricePostFixCssClass += "-empty";
   			           }
   			    %>
		        <td class="<%= pricePostFixCssClass %>"> <%= pricePostFixTxt %></td>		
		        <% } %>		        
   		    </tr>
<%              continue;
            }
            
			// non-scale-type prices
	        if (!prices[iPr].isScalePrice()) { 
	            priceUI.log.debug("priceInclude: Non Scale Price"); %>       
            <tr> 
                <% if (!priceUI.isShowLabelAsPostfix()) { %> <%-- Print a label, if wanted --%>
    		        <td class="cat-prd-prc-lbl"><%= getPriceLabel(priceUI, prices[iPr], priceUI.hasRecurrentPrices(prices)) %>&nbsp;</td>		
    		    <% } %>
    		    <%-- Print the price with currency --%>
    		    <td class="<%= priceStyle %>" colspan="<%= colspan %>">
    		    <%-- Price analysis link if necessary --%>
    		    <% if (priceUI.showPriceAnalysisLink(prices[iPr])) { %>
                     <a href="#" onclick="displayIpcPricingConds('<%= JspUtil.removeNull(priceUI.getIPCItemRef(prices[iPr]).getConnectionKey()) %>',
                                                                 '<%= JspUtil.removeNull(priceUI.getIPCItemRef(prices[iPr]).getDocumentId()) %>',
                                                                 '<%= JspUtil.removeNull(priceUI.getIPCItemRef(prices[iPr]).getItemId()) %>')">
                <% } %>
		        <%= prices[iPr].getPrice() %>&nbsp;<%= prices[iPr].getCurrency() %>
		        <% if (priceUI.showPriceAnalysisLink(prices[iPr])) { %>
                     </a>
                <% } %>
		        </td>  
   			    <% if (priceUI.isShowLabelAsPostfix()) { // Print a postfix, if wanted
   			           String pricePostFixTxt = getPricePostFix(priceUI, prices[iPr]);
   			           String pricePostFixCssClass = "cat-prd-prc-lbl-post";
   			           if(pricePostFixTxt == null || pricePostFixTxt.equals("")) {
   			               pricePostFixCssClass += "-empty";
   			           }
   			    %>
		        <td class="<%= pricePostFixCssClass %>"> <%= pricePostFixTxt %></td>		
		        <% } %>		        
   		    </tr>
<%          } 

			// scale-type prices
	        if (prices[iPr].isScalePrice()) {   
	            priceUI.log.debug("priceInclude: Scale Price");
	            
	            // Special case: scale price shall not be displayed
	            if (priceUI.noScalePrice()) { 
	                
	                // show correct row matching qunatity of scale price as normal price
	                if (priceUI.isScalePriceForQuantity(prices, iPr)) {
	                	priceUI.log.debug("priceInclude: No Display of Scale Price"); %>       
               <tr> 
                	<% if (!priceUI.isShowLabelAsPostfix()) { %> <%-- Print a label, if wanted --%>
    		        	<td class="cat-prd-prc-lbl"><%= getPriceLabel(priceUI, prices[iPr], priceUI.hasRecurrentPrices(prices)) %>&nbsp;</td>		
    		    	<% } %>
    		    	<%-- Print the price with currency --%>
    		    	<td class="<%= priceStyle %>" colspan="<%= colspan %>">
		        	<%= prices[iPr].getPrice() %>&nbsp;<%= prices[iPr].getCurrency() %>
		        	</td>  
   			        <% if (priceUI.isShowLabelAsPostfix()) { // Print a postfix, if wanted
   			               String pricePostFixTxt = getPricePostFix(priceUI, prices[iPr]);
   			               String pricePostFixCssClass = "cat-prd-prc-lbl-post";
   			               if(pricePostFixTxt == null || pricePostFixTxt.equals("")) {
   			                   pricePostFixCssClass += "-empty";
   		    	           }
   			        %>
		            <td class="<%= pricePostFixCssClass %>"> <%= pricePostFixTxt %></td>		
		            <% } %>		        
   		    	</tr>
                <%
	                } // first row
	            } // Special case
            
	            // normal case
	            else {
	            	// first line is always printed
	            	if (priceUI.isScalePriceBegin(prices, iPr)) { 
	                	groupId = "scale-price-group-" + priceUI.createNextScalePriceDivId();         
	                	startIndex = iPr;
	            	}
	                boolean isLastScalePrice = priceUI.isScalePriceEnd(prices, iPr);
%>
                <tr <% if (startIndex != iPr) { %> id="<%= groupId + "-" + iPr %>" <% } %> >
                
                    <% if (!priceUI.isShowLabelAsPostfix()) { %> <%-- Print a label, if wanted --%>               
			            <td class="cat-prd-prc-lbl"> <% if (startIndex == iPr) { %> <%= getPriceLabel(priceUI, prices[iPr], priceUI.hasRecurrentPrices(prices)) %> <% } %>&nbsp;</td>	
			        <% } %>	    
		            <td class="<%= priceStyle %>"> <%= prices[iPr].getPrice() %>&nbsp;<%= prices[iPr].getCurrency() %></td>  
		            
		            <%-- scale type from --%>
			        <% if (prices[iPr].getScaletype().equals(PriceInfo.SCALE_TYPE_FROM) ) { %>
			        <%   if (startIndex != iPr) { %>
    			           <td class="<%= priceStyle %>">
    			               <isa:translate key="b2c.cat.scale.type.from" 
    			                              arg0="<%= prices[iPr].getQuantity() %>" 
    			                              arg1="<%= prices[iPr].getUnit() %>" /></td>
    			    <%   } %>
    			        
    			    <%-- scale type to --%>
			        <% } else if (prices[iPr].getScaletype().equals(PriceInfo.SCALE_TYPE_TO)) { %>
			        <%      if (!isLastScalePrice) { %>
        			           <td class="<%= priceStyle %>">
        			               <isa:translate key="b2c.cat.scale.type.to"
        			                              arg0="<%= prices[iPr].getQuantity() %>" 
 	                                              arg1="<%= prices[iPr].getUnit() %>" /></td>
        			<%      } else { %>
        					   <td class="<%= priceStyle %>">
        					       <isa:translate key="b2c.cat.scale.type.to.more"
        					                      arg0="<%= prices[iPr-1].getQuantity() %>" 
 	                                              arg1="<%= prices[iPr].getUnit() %>" /></td>
        			<%      } %>
    	    					        
			        <%-- scale type interval to --%>
			        <% } else if (prices[iPr].getScaletype().equals(PriceInfo.SCALE_TYPE_TO_INTERVALL)) { %>
    			    <%      if (!isLastScalePrice) { %>
        			           <td class="<%= priceStyle %>">
        			               <isa:translate key="b2c.cat.scale.type.interval.to"
        			                              arg0="<%= prices[iPr].getQuantity() %>" 
 	                                              arg1="<%= prices[iPr].getUnit() %>" /></td>
        			<%      } else { %>
        					   <td class="<%= priceStyle %>">
        					       <isa:translate key="b2c.cat.scale.type.interval.to.more"
        					                      arg0="<%= prices[iPr-1].getQuantity() %>" 
 	                                              arg1="<%= prices[iPr].getUnit() %>" /></td>
        			<%      } %>
      			        
			        <% } 
			           else { %>
			        
    			    <%-- unknown scale type --%>
    			        <td class="cat-prd-prc-lbl"> &nbsp;x&nbsp; </td>
       			        <td class="<%= priceStyle %>"> <%= prices[iPr].getQuantity() %></td>
    			        <td class="<%= priceStyle %>"> <%= prices[iPr].getUnit() %></td>
			        <% } %>

   			        <% if (priceUI.isShowLabelAsPostfix()) { // Print a postfix, if wanted
   			               String pricePostFixTxt = "";
                           if (startIndex == iPr) {
                               pricePostFixTxt += getPricePostFix(priceUI, prices[iPr]);
                           }
   			               String pricePostFixCssClass = "cat-prd-prc-lbl-post";
   			               if(pricePostFixTxt == null || pricePostFixTxt.equals("")) {
   			                   pricePostFixCssClass += "-empty";
   		    	           }
   			        %>
		            <td class="<%= pricePostFixCssClass %>"> <%= pricePostFixTxt %></td>		
		            <% } %>

			        <%-- Print the link to toggle the display of scale prices --%>	 
			        <% if (startIndex == iPr) { %>   
    			    <td class="cat-prd-prc-lbl">
    			        <a href="#" onclick='toggleScalePriceDisplay("<%= groupId %>", <%= iPr + 1 %>, <%= priceUI.getScalePriceGroupSize(prices, iPr) - 1 %>, "<isa:translate key="b2c.cat.scale.price.group.hide"/>", "<isa:translate key="b2c.cat.scale.price.group.show"/>")';>
    			        <div id="<%= groupId %>"> </div></a>    			        
    			    </td> 
    			    <% } %>
    		    </tr> 
    		    
<%                  if (priceUI.isScalePriceEnd(prices, iPr)) { %>
                <%-- hide scale price lines, if javascript is enabled. --%>
                <script language="JavaScript">
                    <!-- 
                        toggleScalePriceDisplay("<%= groupId %>", <%= startIndex + 1 %>, <%= iPr - startIndex %>, "<isa:translate key="b2c.cat.scale.price.group.hide"/>", "<isa:translate key="b2c.cat.scale.price.group.show"/>");
                     //--> 
                </script>			    
<%                  }  
	            } // normal case
            }  // scale-type price 
        } // for 
        if(pricesAvailable) {
%>    	
        </table>
    </div>
<%      }
} // block 
%>
