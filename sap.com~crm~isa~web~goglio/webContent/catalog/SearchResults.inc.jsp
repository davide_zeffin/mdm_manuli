<div class="module-name"><isa:moduleName name="catalog/SearchResults.inc.jsp" /></div>
<%-- Catalog Search Result Enhancements --%>
<% if (prodUI != null && prodUI.isCatalogQuery()) { %>
       <table border="0" cellspacing="0" cellpadding="0" width="100%">
       <tr>
         <td style="border: 0px">
      <% if ( prodUI.getItemPage() != null && 
              prodUI.getItemPage().getItemList() != null && 
              prodUI.getItemPage().getItemList().getNoOfMainItems() != 0) { %>
            <div class="areainfo"><label for="Products"><%= prodUI.getItemPage().getItemList().getNoOfMainItems() %> <isa:translate key="catalog.isa.productsfound"/></label></div>
      <% } 
         else { %>
            <div class="areainfo"><label for="Products"><isa:translate key="catalog.isa.NoItems"/></label></div>
      <% } %>
         </td>
         <td style="text-align:right; border: 0px"><span><isa:translate key="catalog.isa.entries"/> &nbsp;</span>
         <span>
              <%@ include file="ProductPerPageSelect.inc.jsp" %>
         </span>
         </td>
       </tr>
       </table>
<% } %>