<%-- 
*****************************************************************************

    Include:      AdvancedSearch.inc.jsp
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      01.03.2008

***************************************************************************** 

 Displaying the advanced search criteria

 Required includes:
 ==================
 AdvancedSearch.js.inc.jsp - contains javascript functions to control customer action
 
 Required global variables:
 ==========================
                                     
 usage:
 ======
  <%@  include file="AdvancedSearch.inc.jsp"                                    %>
  
*****************************************************************************  
--%> 

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>
<%@ taglib uri="/isa" prefix="isa"  %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.catalog.actions.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.actionform.b2c.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.catalog.uiclass.AdvancedSearchUI" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.*" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatArea" %>

<%  AdvancedSearchUI ui = new AdvancedSearchUI(pageContext);
    FieldSupport field;
    String accessText = "";

    UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
            
    BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    Shop shop = bom.getShop();            
     
    // only loyalty program registration
    User user = bom.getUser();
    boolean isOnlyLoyaltyProgramRegistration = user.isUserLogged();   
%>
    <div id="b2c-advs">
      <div class="module-name"><isa:moduleName name="catalog/AdvancedSearch.inc.jsp"/></div>
	  
<%  accessText = WebUtil.translate(pageContext, "b2c.advSearch.title", null);
    if (ui.isAccessible()) {
%>
      <a href="#grp1End"
         id="grp1Begin"
         title="<isa:translate key="access.grp.begin" arg0="<%= accessText %>"/>" 
      ></a>  
<%  } %>

<%@ include file="js/AdvancedSearch.js.inc.jsp" %>


      <h1 class="areatitle"><isa:translate key="b2c.advSearch.title"/></h1>

        <%-- text --%>
        <div class="formfields">
            <form name="advSearchForm" action="<isa:webappsURL name="catalog/advSearchExecute.do"/>" method="post" >
                <isacore:requestSerial mode="POST"/>
                <table cols=3>
                    <tbody>
                        <%-- Find Product --%>
                        <tr>
                            <td class="label">
                                <label for="advSquery"><isa:translate key="b2c.advSearch.findProduct"/></label>
                            </td>
                            <td class="input">
                                <input class="textInput" type="Text" size="80" name="advSQuery" id="advSQuery" value="<%= ui.getAdvSQuery() %>" >
                            </td>
                            <% accessText = WebUtil.translate(pageContext, "b2c.navigationbar.go", null); %>
                            <td class="label">
                                <input type="submit" value="<%= accessText %>" class="header-search-btn"
                                       title="<isa:translate key="access.button" arg0="<%= accessText %>" arg1=""/>"
                                />
                            </td>
                        </tr>
                            
                        <%-- Search In --%>
                        <% int i = 0; %>
                        <isa:iterate id="attrib" name="<%= ActionConstants.RA_ATTRIB_LIST %>"
                                                 type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
                                                 
                            <input type="hidden" name="advSattrKey[<%= i %>]" id="advSattrKey[<%= i %>]" value="<%= attrib.getString("ID") %>" >
                            
                            <tr>
                              <td class="label">
                                <% if (i==0) { %>
                                     <p><isa:translate key="b2c.advSearch.SearchIn"/></p>
                                <% } else { %>
                                     &nbsp;
                                <% } %>
                              </td>
                              <td class="input">
                                <% String attribDescr = WebUtil.translate(pageContext, attrib.getString("resKey") , null); %>
                                <input type="checkbox" 
                                   title="<%= attribDescr %>" 
                                   id="advSattrSelected[<%= i %>]" 
                                   name="advSattrSelected[<%= i %>]" 
                                   value="true"
                                <% if (ui.isAttSelected(attrib.getString("ID"))) { %>
                                   checked="checked"
                                <% } %>   
                                ><%= attribDescr %>
                              </td>
                              <td class="label">
                                 &nbsp;
                              </td>
                            </tr>
                            <% i++; %>
                        </isa:iterate>
                        
                        <%-- Catalog Selection --%>
<% if (ui.isCatSelectDisp()) { %>
                        <%-- Catalog Selection - Checkbox for Standard Catalog --%>
                        <tr>
                          <td class="label">
                            <p><isa:translate key="b2c.advSearch.catalog"/></p>
                          </td>
                          <td class="input">
                            <input type="checkbox" 
                                   title="<isa:translate key="b2c.advSearch.catalog.standard"/>"
                                   name="advSStdCat" 
                                   value="true" 
    <% if (ui.isSearchStdCat()) { %>
                                   checked="checked"
    <% } %>       
                                   onclick="toggleCheckbox('advSStdCat')"
                            ><isa:translate key="b2c.advSearch.catalog.standard"/>
                          </td>
                          <td class="label">
                            &nbsp;
                          </td>
                        </tr>
<% } 
   else { %>
      <input type="hidden" id="advSStdCat" name="advSStdCat" value="true"> 
<% } %>
          
                        <%-- Catalog Areas for Standard Catalog --%>
                        <tr id="advSStdAreaRow" 
<% if (ui.isSearchStdCat()) { %>
                            >
<% } 
   else { %>
                            style="display:none">
<% } %>
                          <td class="label">
                            <p><isa:translate key="b2c.advSearch.catArea"/></p>
                          </td>
                          <td class="input">
                                  <div class="filter-2">
                                    <%-- Drop down list box --%>
							        <select id= "advSStdArea" name = "advSStdArea" >
							          <option 
<% if (ui.isDefStdAreaSelected()) { %>
							            selected="selected" 
<% } %>     
                                        value="">
                                        <isa:translate key="b2c.advSearch.allAreas"/>
							          </option>
							          <isa:iterate id="stdAreas" name="stdAreas"
							                       type= "com.sap.isa.catalog.webcatalog.WebCatArea" ignoreNull="true">
<% if (!ui.isStandardArea(stdAreas)) {
       continue; 
   }
   String areaId = stdAreas.getAreaID(); %>                   
							            <option 
<% if (ui.isStdAreaSelected(areaId)) { %>
							              selected="selected" 
<% } %>     
                                          value="<%= JspUtil.encodeHtml(areaId) %>">
							              <%= JspUtil.encodeHtml(stdAreas.getAreaName()) %>
							            </option>
							          </isa:iterate>
							        </select>
                                  </div>
                          </td>  
                          <td class="label">
                            &nbsp;
                          </td>
                        </tr>
                        
<% if (ui.isCatSelectDisp()) { %>
                        <%-- Catalog Selection - Checkbox for Reward Catalog --%>
                        <tr>
                          <td class="label">
                            &nbsp;
                          </td>
                          <td class="input">
                            <input type="checkbox" 
                                   title="<isa:translate key="b2c.advSearch.catalog.reward"/>"
                                   name="advSRewCat" 
                                   value="true" 
    <% if (ui.isSearchRewCat()) { %>
                                   checked="checked"
    <% } %>       
                                   onclick="toggleCheckbox('advSRewCat')"
                            ><isa:translate key="b2c.advSearch.catalog.reward"/>
                          </td>
                          <td class="label">
                            &nbsp;
                          </td>
                        </tr>
<% } %>       
                        <tr id="advSRewAreaRow" 
<% if (ui.isSearchRewCat()) { %>
                            >
<% } 
   else { %>
                            style="display:none">
<% } %>
                          <td class="label">
                            <p><isa:translate key="b2c.advSearch.catArea"/></p>
                          </td>
                          <td class="input">
                                  <div class="filter-2">
                                    <%-- Drop down list box --%>
							        <select id= "advSRewArea" name = "advSRewArea" >
							          <option 
<% if (ui.isDefRewAreaSelected()) { %>
							            selected="selected" 
<% } %>     
							             value="">
							            <isa:translate key="b2c.advSearch.allAreas"/>
							          </option>
							          <isa:iterate id="rewAreas" name="rewAreas"
							                       type= "com.sap.isa.catalog.webcatalog.WebCatArea" ignoreNull="true">
<% if (!ui.isRewardArea(rewAreas)) {
     continue; 
   }
   String areaId = rewAreas.getAreaID(); %>                   
							            <option 
<% if (ui.isRewAreaSelected(areaId)) { %>
							              selected="selected" 
<% } %>     
                                          value="<%= JspUtil.encodeHtml(areaId) %>">
							              <%= JspUtil.encodeHtml(rewAreas.getAreaName()) %>
							            </option>
							          </isa:iterate>
							        </select>
                                  </div>  
                          </td>  
                          <td class="label">
                            &nbsp;
                          </td>
                        </tr>
                        

                        <%-- Point Selection --%>
                        <tr id="advSPtsRow" 
<% if (ui.isSearchRewCat()) { %>
                            >
<% } 
   else { %>
                            style="display:none">
<% } %>
                          <td class="label">
	                        &nbsp;
                          </td>
                          <td>
                            <table cols=4>
                              <tr>
	                            <td class="label" colspan=4>
	                              <% String label = WebUtil.translate(pageContext, "b2c.advSearch.reward.pts.title", 
	                                                                  new String[] { String.valueOf(ui.getPointsUnitDescr()) } ); %>                                    
                                  <%= JspUtil.encodeHtml(label) %>
	                            </td>
	                          </tr>
                              <tr>
	                            <td class="label">
	                              <isa:translate key="b2c.advSearch.reward.pts.from"/>
	                            </td>
	                            <td class=input>
	                              <input type="text" id="advSPtsFrom" name="advSPtsFrom">
                                </td>
  	                            <td class="label">
	                              <isa:translate key="b2c.advSearch.reward.pts.to"/>
	                            </td>
	                            <td class=input>
	                              <input type="text" id="advSPtsTo" name="advSPtsTo">
                                </td>
	                          </tr>
                            </table>
                          </td>
                          <td class="label">
	                        &nbsp;
                          </td>
	                    </tr>
                      
<%  if (ui.isCatSelectDisp()) { %>
                        <%-- GO button on bottom of the page --%>
                        <tr>
                          <td class="label" colspan=2>
                            &nbsp;
                          </td>
                          <% accessText = WebUtil.translate(pageContext, "b2c.navigationbar.go", null); %>
                          <td class="label">
                            <input type="submit" value="<%= accessText %>" class="header-search-btn"
                                   title="<isa:translate key="access.button" arg0="<%= accessText %>" arg1=""/>"
                            />
                          </td>
                        </tr>
<%  } %>
                    </tbody>
                </table>       
                
<%  if (ui.isAccessible()) {
       accessText = WebUtil.translate(pageContext, "b2c.advSearch.title", null);
%>
                <a href="#grp1Begin"
                   id="grp1End"
                   title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>" 
                ></a>	
<%  } %>
            </form>
        </div> <%-- end: formfields --%>
    </div> <%-- end: b2c-advs --%>