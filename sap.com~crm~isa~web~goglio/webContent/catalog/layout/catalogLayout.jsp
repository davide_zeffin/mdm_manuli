<%-- import the taglibs used on this page --%>
<%@ taglib prefix="isa" uri="/isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.LayoutUI" %>


<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
  <head>
    <isa:includes/>
    <title><isa:translate key="b2b.fs.title"/></title>
  </head>

<% LayoutUI ui = new LayoutUI(pageContext); %>
  <body bgcolor="#ffffff" text="#000000" link="#023264" alink="#023264" vlink="#023264">
<%
	if (ui.isAccessible())
	{
%>
<a href="#" title= "<isa:translate key="b2c.cat.beginpage"/>" accesskey="<isa:translate key="b2c.cat.beginpage.access"/>"></a>
<%
	}
%>
  <div id="header">
    <% String pageName = ui.getAreaPage("header"); %>
    <jsp:include page="<%=pageName%>" flush="false"/>
  </div>
  <div id="navigator">
    <% pageName = ui.getAreaPage("navigator"); %>
    <jsp:include page="<%=pageName%>" flush="false"/>
  </div>
  <div id="workarea">
    <% pageName = ui.getAreaPage("workarea"); %>
    <jsp:include page="<%=pageName%>" flush="false" />
  </div>
  <div id="minibasket">
    <% pageName = ui.getAreaPage("miniBasket"); %>
    <jsp:include page="<%=pageName%>" flush="true"/>
  </div>

<%
	if (ui.isAccessible())
	{
%>
<a href="#" title= "<isa:translate key="b2c.cat.endpage"/>" accesskey="<isa:translate key="b2c.cat.endpage.access"/>"></a>
<%
	}
%>
 </body>
</html>
