<script type="text/javascript">
    var allowedValues=new Array();
    var allowedValuesLength=new Array();
    var noOfElements;

<% java.util.Iterator attributes = attributesList.iterator();
   java.util.Iterator values;
   int i=-1;

   while (attributes.hasNext()) {
      WebCatAreaAttribute attribute = (WebCatAreaAttribute) attributes.next();
      if (attribute.getStyle() == WebCatAreaAttribute.INPUT_FIELD) {
          i++;
  %>
      allowedValues[<%= i %>]=new Array();
  <%
          values = attribute.getValues().iterator();
          int q=0;
          while (values.hasNext()) {
            q++;
            String value = (String) values.next();
%>
      allowedValues[<%= i %>][<%= q-1 %>]="<%= value %>";
<%
          } //end while values.hasNext()
%>
      allowedValuesLength[<%= i %>]=<%= q %>;
<%
        } // end if the attribute is an input field
      } // end loop on attributes
%>
      noOfElements=<%= i+1 %>;

    function verifier() {
      i=-1;
      for (l=0;l<document.filterForm.length;l++)
        if (document.filterForm.elements[l].type == "text")
        {
        i++;
//        if (document.filterForm.elements[l].value == '' ) document.filterForm.elements[l].value='<isa:translate key="catalog.isa.all"/>';
//        if (document.filterForm.elements[l].value.toUpperCase() == '<isa:translate key="catalog.isa.allUpper"/>' ) t=1;
        if (document.filterForm.elements[l].value == '' ) t=1;
            else t=0;
        endata=document.filterForm.elements[l].value.toUpperCase();
        chdata=document.filterForm.elements[l].value.toUpperCase();
        chdata=chdata.replace("*","");
        for (j=0;j<allowedValuesLength[i];j++) {
            aldata=allowedValues[i][j].toUpperCase();
              if ( (endata != chdata && aldata.search(chdata) != -1) || (endata == aldata)) t=1;
        }
        if (allowedValuesLength[i] == 0) t=1;
        if (t == 0) {
//            st="<isa:translate key="catalog.isa.all"/>";
            st="";
            for (j=0;j<allowedValuesLength[i];j++) {
                if (j == 0) st=allowedValues[i][j];
                    else st+=(", "+allowedValues[i][j]);
            }
            alert('<isa:translate key="catalog.isa.noValidValues"/> '+st);
            document.filterForm.elements[l].focus();
            document.filterForm.elements[l].select();
            return false;
        }
      }
//    alert('everything is ok');
    return true;
  }
</script>