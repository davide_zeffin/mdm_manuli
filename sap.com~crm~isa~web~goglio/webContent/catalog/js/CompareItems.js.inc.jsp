<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<script type="text/javascript">

	  function addSingleItemForCompare(Itemkey, domainRelaxation, compareCUA) {
		
		if(domainRelaxation == 'true' ) {
           <%-- Handle the domain relaxing --%>
  		   var liBehindFirstDot = location.hostname.indexOf(".")+1;
           if (liBehindFirstDot > 0) {
               document.domain = location.hostname.substr(liBehindFirstDot);
           }
        }
        
        <%-- just call the function from the parent window --%>
        if ( compareCUA == "true") {
            window.opener.addSingleItemCUA(Itemkey, 'addToBasket', '');
        }
        else {
            window.opener.addSingleItem(Itemkey, 'addToBasket', '');
        }
        
        <%-- Close the popup in any case --%>
        window.close();
	  }
	  
	  function addSingleContractItemForCompare(Itemkey, contractkey, contractItemKey) { 
        //just call the function from the parent window
        window.opener.addSingleContractItem(Itemkey, contractkey, contractItemKey, 'addToBasket', '');
	  }

	  function setNext(Next) {
	    document.compareform.itemkey.value = '';
		l=0;
/*		for (i=0;i<<%= theItems.size() %>;i++) {
			if (document.compareform.elements["item["+i+"].selected"].checked == true) l++;
		}
		if ( (l<2 && Next == 'compareSameWnd') )
		  {
			alert("<isa:UCtranslate key="catalog.isa.selectSomething"/>");
			return;
		  }
*/
		document.compareform.target = '';
		document.compareform.next.value = Next;
		document.compareform.submit();
		if (Next == 'addToBasket') {
	       parent.window.close();
	    }
	  }

	  function getMoreFunctionsForCompare(Itemkey,Next) {
		if (Next == 'leaflet') {
		  document.compareform.target = window.opener.name;
		} else {
		  document.compareform.target = '';
		}
		document.compareform.itemkey.value = Itemkey;
		document.compareform.next.value = Next;
		document.compareform.submit();
	  }

</script>
