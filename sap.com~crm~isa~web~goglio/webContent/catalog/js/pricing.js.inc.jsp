<%-- 
*****************************************************************************

    Include:      pricing.inc.js
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      19.03.2006

***************************************************************************** 
--%> 

<%@ taglib uri="/isa" prefix="isa" %>

<%-- 
 * Toggles the display of the scale prices.
 * Rows are expanded / compressed and the text of the link
 * to toggle is switched. 
 --%> 
function toggleScalePriceDisplay(groupId, firstRow, numberOfRows) {

    var text = document.getElementById(groupId);
    if (!text.firstChild) {
       text.appendChild(document.createTextNode("-"));   
    }

    for (var i = firstRow; i < firstRow + numberOfRows; i++) { 
    
        var rowId = groupId + "-" + i; 
        row = document.getElementById(rowId);
		
        if (row.style.display == "none") {
            row.style.display = "";
            text.firstChild.nodeValue = '<isa:UCtranslate key="b2c.cat.scale.price.group.hide"/>';
        }
        else {
            row.style.display = "none";
            text.firstChild.nodeValue = '<isa:UCtranslate key="b2c.cat.scale.price.group.show"/>';
        }
    }
}