<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatAreaAttribute" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.catalog.AttributeKeyConstants" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.Constants" %>
<%@ page import="com.sap.isa.core.ui.context.GlobalContextManager" %>
<%@ page import="com.sap.isa.core.ui.context.ContextManager" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>
<%
	String isQuery = request.getParameter(ActionConstants.RA_IS_QUERY);
	
	if (null==isQuery && null!=request.getAttribute(ActionConstants.RA_IS_QUERY)) {
		isQuery = request.getAttribute(ActionConstants.RA_IS_QUERY).toString();
	}
	if (null!=isQuery){
		isQuery = JspUtil.encodeHtml(isQuery);
	}
	
%>
<script type="text/javascript">
  var count = -1;

	function setPage(Zahl) {
	  document.productform.target = '';
	  document.productform.page.value = Zahl;
<%if (isQuery == null) { %>
	  document.productform.next.value = "products";
<% } else { %>
	  document.productform.next.value = "setPageSize";
<% } %>
	  document.productform.submit();
	}
    
	function setPageSize(Zahl) {
		document.productform.target = '';
		document.productform.itemPageSize.value = Zahl;
		document.productform.next.value = "setPageSize";
		document.productform.submit();
	}
    
	function setNext(Next) {
/*		l = noOfchecks();
		if (l<1 && Next == 'addToBasket') {
		   alert("No product selected");
		   return;
		}
*/
		document.productform.target = '';
		document.productform.next.value = Next;
		document.productform.submit();
	}
    
	function selectAllItemsToggled() {
		if(document.productform.allitemsselected.checked) {
			setNext('selectAll');
		}
		else {
			setNext('unselectAll');
		}
	}

	function doQueryAlt(queryStr) {
		document.NavForm.query.value = queryStr;
		location.href = '<isa:webappsURL name="/catalog/query.do"/>' + '?query=' + escape(queryStr);
	}

	function noOfchecks() {
	   var no=0;
	   for (i=0;i<=count;i++) {
    	  if (document.productform.elements["item["+i+"].selected"].checked) {
        	  no=no+1;
	      }    
	   }
	   return no;
	}

	function seeSingleItem(Itemkey) {
	  document.productform.target = '';
	  document.productform.next.value = 'seeItem';
	  document.productform.itemkey.value = Itemkey;
	  document.productform.submit();
	}

	function order(sort_order) {
	  document.productform.target = '';
   <% if (isQuery != null && ActionConstants.RA_IS_QUERY_VALUE_YES.equals(isQuery)) { %>
		 document.productform.next.value = 'orderQuery';
   <% }
	  else { %>
		 document.productform.next.value = 'order';
   <% } %>
	  document.productform.order.value = sort_order;
	  document.productform.submit();
	}

	function popUpCompare(noOfSelItems) {
      var l = noOfchecks() + noOfSelItems;
	  if (l<2) {
		  alert("<isa:UCtranslate key="catalog.isa.selectSomething"/>");
		  return;
		} 
    
	  window.open("","compareWindow","menubar=no, directories=no, height=500, width=750, scrollbars=yes, status=no, toolbar=no, resizable = yes ");
      
	  document.productform.target = 'compareWindow';
	  document.productform.next.value ='compareItems';
	  document.productform.action='<isa:webappsURL name="catalog/updateItems.do"/>'
	  document.productform.submit();
	}
</script>
<isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
   <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' type = "text/javascript"></script>
</isacore:ifShopProperty>


