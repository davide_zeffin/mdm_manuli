<script type="text/javascript">
<!--  
    function addSingleItem(Itemkey, nextAction, formSuffix) {
      getMoreFunctions(Itemkey, nextAction, formSuffix);      
    }
    
    function addSingleItemCUA(Itemkey, nextAction, formSuffix) {
      getMoreFunctionsCUA(Itemkey, nextAction, formSuffix);      
    }
    
    function addSingleContractItem(Itemkey, contractkey, contractitemkey, nextAction, formSuffix) {
      getMoreContractFunctions(Itemkey, contractkey, contractitemkey, nextAction, formSuffix);      
    }
    
    function configureItem(Itemkey, Next, formSuffix, lastVisited) {
      setLastAction(formSuffix, lastVisited);
      getMoreFunctions(Itemkey, Next, formSuffix);
    }
    
    function configureContractItem(Itemkey, contractkey, contractitemkey, Next, formSuffix, lastVisited) {
      setLastAction(formSuffix, lastVisited);
      getMoreContractFunctions(Itemkey, contractkey, contractitemkey, Next, formSuffix);
    }    
    
    function configureSubItem(itemTechKey, topItemId, Next, formSuffix, lastVisited) {
      setLastAction(formSuffix, lastVisited);
      getMoreSubItemFunctions(itemTechKey, topItemId, Next, formSuffix);
    }
    
    function setLastAction(formSuffix, lastVisited) {
      if ( formSuffix == 'bs') { 
         document.productformbs.lastVisited.value = lastVisited;
      }
      else {
         if( formSuffix == 'rc') {
            document.productformrc.lastVisited.value = lastVisited;
         }
         else {
            document.productform.lastVisited.value = lastVisited;
         }
      }
    }

	function displayBusyOverlay2(styleClass) {
		 		
 		var body = document.getElementsByTagName("body")[0];
   		var node = document.createElement('div');
        	
       	node.id        = 'IsaOverlayShadowObject';                   
		node.classname = styleClass;

		body.appendChild(node);        		                     
    }  

    function getMoreFunctions(Itemkey, Next, formSuffix) {
    
      displayBusyOverlay2('cat-busy-overlay');
    
      if ( formSuffix == 'bs') { 
        document.productformbs.target = '';
        document.productformbs.next.value = Next;
        document.productformbs.itemkey.value = Itemkey;
        document.productformbs.action = '<isa:webappsURL name="/catalog/updateItems.do"/>';
        document.productformbs.submit();
      } 
      else {
         if(formSuffix == 'rc') {
           document.productformrc.target = '';
           document.productformrc.next.value = Next;
           document.productformrc.itemkey.value = Itemkey;
           document.productformrc.action = '<isa:webappsURL name="/catalog/updateItems.do"/>';
           document.productformrc.submit();
         }
         else {
           document.productform.target = '';
           document.productform.next.value = Next;
           document.productform.itemkey.value = Itemkey;
           document.productform.action = '<isa:webappsURL name="/catalog/updateItems.do"/>';
           document.productform.submit();
         }
      }
    }
    
    function getMoreFunctionsCUA(Itemkey, Next, formSuffix) {
    
      displayBusyOverlay2('cat-busy-overlay');
    
      if ( formSuffix == 'bs') { 
        document.productformbs.target = '';
        document.productformbs.next.value = Next;
        document.productformbs.itemkey.value = Itemkey;
        document.productformbs.action = '<isa:webappsURL name="/catalog/updateItems.do"/>';
        document.productformbs.submit();
      } 
      else {
         if(formSuffix == 'rc') {
           document.productformrc.target = '';
           document.productformrc.next.value = Next;
           document.productformrc.itemkey.value = Itemkey;
           document.productformrc.action = '<isa:webappsURL name="/catalog/updateItems.do"/>';
           document.productformrc.submit();
         }
         else {
           document.productform.target = '';
           document.productform.next.value = Next;
           document.productform.itemkey.value = Itemkey;
           document.productform.compareCUA.value = 'true'
           document.productform.action = '<isa:webappsURL name="/catalog/updateItems.do"/>';
           document.productform.submit();
         }
      }
    }
    
    function getMoreContractFunctions(Itemkey, contractkey, contractitemkey, Next, formSuffix) {
      if ( formSuffix == 'bs') { 
        document.productformbs.target = '';
        document.productformbs.next.value = Next;
        document.productformbs.itemkey.value = Itemkey;
        document.productformbs.contractkey.value=contractkey;
        document.productformbs.contractitemkey.value=contractitemkey;
        document.productformbs.submit();
      } 
      else {
         if(formSuffix == 'rc') {
           document.productformrc.target = '';
           document.productformrc.next.value = Next;
           document.productformrc.itemkey.value = Itemkey;
           document.productformrc.contractkey.value=contractkey;
           document.productformrc.contractitemkey.value=contractitemkey;
           document.productformrc.submit();
         }
         else {
           document.productform.target = '';
           document.productform.next.value = Next;
           document.productform.itemkey.value = Itemkey;
           document.productform.contractkey.value=contractkey;
           document.productform.contractitemkey.value=contractitemkey;
           document.productform.submit();
         }
      }
    }

    function getMoreSubItemFunctions(itemTechKey, topItemId, Next, formSuffix) {
      if ( formSuffix == 'bs') {
         document.productformbs.target = '';
         document.productformbs.itemkey.value = itemTechKey;
         document.productformbs.topItemkey.value = topItemId;
         document.productformbs.next.value = Next;
         document.productformbs.submit();
      }
      else {
         if(formSuffix == 'rc') {
            document.productformrc.target = '';
            document.productformrc.itemkey.value = itemTechKey;
            document.productformrc.topItemkey.value = topItemId;
            document.productformrc.next.value = Next;
            document.productformrc.submit();
         }
         else {
            document.productform.target = '';
            document.productform.itemkey.value = itemTechKey;
            document.productform.topItemkey.value = topItemId;
            document.productform.next.value = Next;
            document.productform.submit();
         }
      }
    }
    
    function setUnit(formSuffix) {
      var prodForm = "productform"+formSuffix;
      document.forms[prodForm].target = '';      
      document.forms[prodForm].next.value = "itemUnits";
      document.forms[prodForm].submit();
    }

    function displayProdDetails(action, formSuffix) {
      var prodForm = "productform"+formSuffix;
      document.forms[prodForm].target = '';      
      document.forms[prodForm].action = action;
      document.forms[prodForm].submit();
      return false;
    }
    
    
    function displayProdDetailsCUA(itemId, action, formSuffix) {
      var prodForm = "productform"+formSuffix;
      document.forms[prodForm].itemkey.value = itemId;
      document.forms[prodForm].display_scenario.value = "query";
      document.forms[prodForm].itemId.value = itemId;
      document.forms[prodForm].target = '';      
      document.forms[prodForm].action = action;
      document.forms[prodForm].submit();
      return false;
    }
    
//-->
</script>

