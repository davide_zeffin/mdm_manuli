<%-- 
*****************************************************************************

    Include:      CatalogScripts.js.inc.jsp
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      19.03.2008

***************************************************************************** 
--%> 

<%@ taglib uri="/isa" prefix="isa" %>


<script type="text/javascript">

<%--
/** 
 * Defines the scripts to transfer the items to be added to the WebClientUI.
 * It registers an unload event handler for ther parent window, if we are not
 * running in portal environment.
 *
 * During the unload event, the catalog is logged off.
 */
--%>
var CatalogScripts = {

    counter: 0,
    
<%-- --------------------------------------------------------------
/**
 * Calls the logoff action to close the catalog session.  
 *
 * This method is registered as event handler for the window.unload
 * event. It returns without any action, if we are running in portal.
 * See CatalogScripts.init()
 *
 */
----------------------------------------------------------------- --%>
    unload: function() {
	
	    var frm = CatalogScripts.getWebClientFormObject();
		if (frm) {
			return;
		}
		
		window.location = "/catalog/catalog/invalidateSession.do";
	},
	
<%-- --------------------------------------------------------------
/**
 * Handles domain relaxing and other initialization  of the class.
 *
 * See http://help.sap.com/saphelp_nw70/helpdata/en/28/7c383f717b5f03e10000000a114084/frameset.htm 
 *
 * This method is registered as event handler for the window.unload
 * event. It returns without any action, if we are running in portal.
 * See CatalogScripts.init()
 *
 */
----------------------------------------------------------------- --%>
        init: function() {


		<%-- Handle the domain relaxing --%>
		var liBehindFirstDot = location.hostname.indexOf(".") + 1;
        if (liBehindFirstDot > 0) {
            document.domain = location.hostname.substr(liBehindFirstDot);
        }

		<%  	if(ui.isPortal() == false) { %>
		
        <%-- On the compare items popup, we have an opener --%>
        if (document.getElementById("CompareItemsForm")) {
            var doc = window.opener.parent.document;
			var win = window.opener.parent;
        } else {    
            var doc = parent.document;
			var win = parent;
        }
		if (!doc) {
		    return;
		}
           
		var frm = CatalogScripts.getWebClientFormObject();
		if (!frm) {
			return;
		}
		    
		if (window.name == 'PCAT_Browsing') {
			window.onunload = CatalogScripts.unload;
		}
		
		<%    	}	%>
	},

	
	getWebClientFormObject: function() {
	
	    <%-- The hidden field with the name "htmlb_first_form_id" contains the name of the form. --%>
		if (!parent.document) {
		    return undefined;
		}
		
		var form = parent.document.getElementById("htmlb_first_form_id");
		if (!form) {
		    return undefined;
		}
		
		var frm = form.form;
		if (!frm) {
		    return undefined;
		}
		
		var index_product_id   = this.getFormFieldIndex(frm, 'ordertransferitem_ordered_prod$');
		if (!index_product_id) {
			return undefined;
		}
		
		return frm;
	},
	
<%-- --------------------------------------------------------------
/**
 * Transfers a catalog item to a WebClientUI form and generates the
 * ADDTOCART event there.
 */
----------------------------------------------------------------- --%>
    transferToOrder: function(productId, productGuid, quantity, uom, configXml, country, shopId, langu) {

		var frm = CatalogScripts.getWebClientFormObject();
		if (!frm) {
			return;
		}
				
		<%-- Find the indices of the form fields in the WebClientUI application --%>
        var index_product_id   = this.getFormFieldIndex(frm, 'ordertransferitem_ordered_prod$');
        var index_product_guid = this.getFormFieldIndex(frm, 'ordertransferitem_product$');
        var index_quantity     = this.getFormFieldIndex(frm, 'ordertransferitem_quantity$');
		var index_uom          = this.getFormFieldIndex(frm, 'ordertransferitem_uom$');
		var index_config       = this.getFormFieldIndex(frm, 'ordertransferitem_xml_ipc_config$');
		
		var index_country      = this.getFormFieldIndex(frm, 'pcatmetadata_country$');
		var index_shop_id      = this.getFormFieldIndex(frm, 'pcatmetadata_shop_id$');
		var index_langu        = this.getFormFieldIndex(frm, 'pcatmetadata_langu$');
		
		<%-- Fill the WebClientUI form with the provided parameters. --%>
		frm.elements[index_product_guid].value  = productGuid;
		parent.thtmlb_toggleInput(frm.elements[index_product_guid]);
    	frm.elements[index_product_id].value    = productId;
    	parent.thtmlb_toggleInput(frm.elements[index_product_id]);
		frm.elements[index_quantity].value      = quantity;
		parent.thtmlb_toggleInput(frm.elements[index_quantity]);
		frm.elements[index_uom].value           = uom;
		parent.thtmlb_toggleInput(frm.elements[index_uom]);
		frm.elements[index_config].value        = configXml;
		parent.thtmlb_toggleInput(frm.elements[index_config]);
		frm.elements[index_country].value       = country;
		parent.thtmlb_toggleInput(frm.elements[index_country]);
		frm.elements[index_shop_id].value       = shopId;
		parent.thtmlb_toggleInput(frm.elements[index_shop_id]);
		frm.elements[index_langu].value         = langu;
		parent.thtmlb_toggleInput(frm.elements[index_langu]);

		var link = this.getTransferButton();
		parent.RtpCatAddToOrder2(link.id);

 		return false;
	},
	
// ============================================
	getFormFieldIndex: function(frm,field) {
// ============================================
        var len = frm.elements.length;
        for (var i = 0; i < len; i++) {
            var name = frm.elements[i].id;
		    var re = new RegExp(field);
            if (name.search(re) != -1) {
		        return i;
	        }
        }
  
        return -1;
	},
	
	
// ============================================
	getTransferButton: function() {
// ============================================
        var searchExpr = "IsaTransferToOrderButton4$";
        var len = parent.document.links.length;
        for (var i = 0; i < len; i++) {
            var name = parent.document.links[i].id;
		    var re = new RegExp(searchExpr);
            if (name.search(re) != -1) {
		        return parent.document.links[i];
	        }
        }
  
        alert('Script error, Button ' + searchExpr + ' not found!');
        return undefined;
	}	
};

	// Call the onstructor
    CatalogScripts.init();
    
</script>