<%-- 
*****************************************************************************

    Include:      ScComponents.js.inc.js
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      01.01.2006

***************************************************************************** 
--%> 

<%@ page import="com.sap.isa.isacore.action.b2c.*" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<script type="text/javascript">
<!--
    function expandGroup(iGrp, maxItems) {
        var rowId;
        var row;
        var j = 0;
        var fieldName;
        var field;
        var iScItem;
        var iCountComp = 0;
        var groupKey = "Group["+iGrp+"]";
        
        for (var i = 0; i < maxItems ; i++) { 
            var newgk = document.productform.elements["subItem["+i+"].groupKey"].value;
            if (newgk != groupKey) {
               continue;
            }
            rowId = groupKey+"_["+j+"]";
            row = document.getElementById(rowId);
            row.style.display = "";
            iScItem = document.productform.elements["subItem["+i+"].iScItem"].value;
            if (iScItem != 0) {
                fieldName = "item["+iScItem+"].selected";
                field = document.getElementById(fieldName);
                if (field.value == "true" ) { 
                    field.style.display = "";
                    iCountComp++;
                }
                else {
                    field.style.display = "none";
                }
            }
            j++;
        }
        document.productform.elements["itemGroup["+iGrp+"].collapsed"].value = 'false';
        rowId = "collapseBtn("+groupKey+")";
        row = document.getElementById(rowId);
        row.style.display = "";
        rowId = "expandBtn("+groupKey+")";
        row = document.getElementById(rowId);
        row.style.display = "none";
        if (iCountComp > 1) {
            rowId = "compareBtn("+groupKey+")";
            row = document.getElementById(rowId);
            row.style.display = "";
        }
        rowId = "itemGroup["+iGrp+"].anchor";
    }

    function collapseGroup(iGrp, maxItems) { 
        var cTypeRPC = "RPC";
        var rowId;
        var row;
        var fieldName;
        var field;
        var j = 0;
        var iScItem;
        var groupKey = "Group["+iGrp+"]";

        for (var i = 0; i < maxItems ; i++) { 
            var newgk = document.productform.elements["subItem["+i+"].groupKey"].value;
            if (newgk != groupKey) {
               continue;
            }
            iScItem = document.productform.elements["subItem["+i+"].iScItem"].value;
            if (document.getElementsByName(groupKey)[j].type  == "checkbox" &&
                document.productform.elements["item["+iScItem+"].subItemType"].value != cTypeRPC) {
               j++;
               continue;
            }
            
            if (document.getElementsByName(groupKey)[j].checked == true) {
                rowId = groupKey+"_["+j+"]";
                row = document.getElementById(rowId);
                row.style.display = "";
                if (iScItem != 0) {
                    fieldName = "item["+iScItem+"].selected";
                    field = document.getElementById(fieldName);
                    if (field.value == "true" ) { 
                        field.style.display = "none";
                    }
                }
            }
            else {    
                rowId = groupKey+"_["+j+"]";
                row = document.getElementById(rowId);
                row.style.display = "none";
            }
            j++;
        }
        document.productform.elements["itemGroup["+iGrp+"].collapsed"].value = 'true';
        rowId = "collapseBtn("+groupKey+")";
        row = document.getElementById(rowId);
        row.style.display = "none";
        rowId = "expandBtn("+groupKey+")";
        row = document.getElementById(rowId);
        row.style.display = "";
        rowId = "compareBtn("+groupKey+")";
        row = document.getElementById(rowId);
        row.style.display = "none";
        rowId = "itemGroup["+iGrp+"].anchor";
    }

    function changeSelForResFlag(iGrp, iGrpItem, anchorKey) {
   	    var cSelectedForExp = "true";
    	var cDeselectedForExp = "false";
        var cTypeCRP = "CRP";
        var cTypeRPC = "RPC";
    
        var iScItem;
        var crpSelected = "false";
        var maxItems = document.productform.elements["Group["+iGrp+"].MaxItems"].value;
        var groupKey = "Group["+iGrp+"]";

        for (var i = 0; i < maxItems ; i++) { 
            iScItem = document.getElementsByName(groupKey)[i].value;
            if (iScItem == "NoSelection") {
                continue;
            }
            
            if (i == iGrpItem) {
                document.productform.elements["item["+iScItem+"].selForRes"].value = cSelectedForExp;
                if (document.productform.elements["item["+iScItem+"].subItemType"].value == cTypeCRP) {
				    crpSelected = "true";
                }
				else {
				    crpSelected = "false";
				}                
            }
            else {            
                if (document.productform.elements["item["+iScItem+"].subItemType"].value == cTypeCRP) {
				    crpSelected = "false";
				}                
                else if (document.getElementsByName(groupKey)[i].type  == "checkbox" &&
                         document.productform.elements["item["+iScItem+"].subItemType"].value == cTypeRPC) {
                         if (crpSelected == "true") {
                             document.getElementsByName(groupKey)[i].checked  = true;
                         }
                         else {
                             document.getElementsByName(groupKey)[i].checked  = false;
                         }
                }
                document.productform.elements["item["+iScItem+"].selForRes"].value = cDeselectedForExp;
            }
            
            if ( document.getElementsByName(groupKey)[i].checked == true) {
                showEnabledContractDuration(iScItem);
                showEnabledConfigMessageLink(iScItem);
            }
            else {
                showDisabledContractDuration(iScItem);
                showDisabledConfigMessageLink(iScItem);
            }
        }
        document.productform.anchorKey.value = anchorKey;
        triggerExplosion();
    }

    
    function changeSelForResFlagCheckbox(groupKey, iGrpItem, iScItem, anchorKey) {
   	    var cSelectedForExp = "true";
    	var cDeselectedForExp = "false";

        if (document.getElementsByName(groupKey)[iGrpItem].checked  == true) {
            document.productform.elements["item["+iScItem+"].selForRes"].value = cSelectedForExp;
            showEnabledContractDuration(iScItem);
            showEnabledConfigMessageLink(iScItem);
        }
        else {
            document.productform.elements["item["+iScItem+"].selForRes"].value = cDeselectedForExp;
            showDisabledContractDuration(iScItem);
            showDisabledConfigMessageLink(iScItem);
        }
        document.productform.anchorKey.value = anchorKey;
        triggerExplosion();
    }
    
    function noOfchecks(grStart, grEnd) {
        no=0;
        for (i=grStart;i<grEnd;i++) {
           if ( document.productform.elements["item["+i+"].selected"].checked == true) {
              no++;
           }
        }
        return no;    
    }

    function popUpCompareSC(groupKey,grStart, grEnd) {
      noSelProduct = noOfchecks(grStart, grEnd);
      if (noSelProduct<2) {
          alert("<isa:translate key="catalog.isa.selectSomethingGroup"/>");
      	  return;
	  } 
      
      CompareWindow = window.open("","compareWindow","menubar=no, directories=no, height=500, width=750, scrollbars=yes, status=no, toolbar=no, resizable=yes ");
      CompareWindow.focus();
      
      document.productform.target = 'compareWindow';
      document.productform.next.value ='compareItems';
      document.productform.groupKey.value = groupKey;
      document.productform.action='<isa:webappsURL name="catalog/updateItems.do"/>'
      document.productform.submit();
    }

    function triggerExplosion() {
      if (<%= subItemListUI.isAutomaticPackageExplosion(currentItem) %>) { 
           getMoreFunctions("<%= currentItem.getItemId() %>", "seeItem", '');
      }
    }
    
    function setContractDuration(itemKey, anchorKey) {
       document.productform.anchorKey.value = anchorKey;
       document.productform.target = '';
       document.productform.itemkey.value = itemKey;
       document.productform.next.value = 'seeItem';
       document.productform.submit();
    }
    
    function showEnabledContractDuration(iScItem) {
       if (document.getElementById("contractDurationEnabled[" + iScItem + "]") != null) {
          document.getElementById("contractDurationEnabled[" + iScItem + "]").style.display = "inline";
          document.getElementById("contractDurationDisabled[" + iScItem + "]").style.display = "none";
       }
    }

    function showDisabledContractDuration(iScItem) {
       if (document.getElementById("contractDurationEnabled[" + iScItem + "]") != null) {
          document.getElementById("contractDurationEnabled[" + iScItem + "]").style.display = "none";
          document.getElementById("contractDurationDisabled[" + iScItem + "]").style.display = "inline";
       }
    }
         
   function showEnabledConfigMessageLink(iScItem) {
       if (document.getElementById("configMessageLinkEnabled[" + iScItem + "]") != null) {
          document.getElementById("configMessageLinkEnabled[" + iScItem + "]").style.display = "inline";
          document.getElementById("configMessageLinkDisabled[" + iScItem + "]").style.display = "none";
       }
    }

    function showDisabledConfigMessageLink(iScItem) {
       if (document.getElementById("configMessageLinkEnabled[" + iScItem + "]") != null) {
          document.getElementById("configMessageLinkEnabled[" + iScItem + "]").style.display = "none";
          document.getElementById("configMessageLinkDisabled[" + iScItem + "]").style.display = "inline";
       }
    }
    
    function setAddToBasketActive(active) { 
       if (active == "true") {
          document.getElementById("addToBasket").disabled = false;
       }
       else {        
          document.getElementById("addToBasket").disabled = true;
       }
    }
//-->         
</script>	
      
      