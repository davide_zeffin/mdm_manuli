<%-- 
*****************************************************************************

    Include:      AdvancedSearch.js.inc.js
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      01.04.2008

***************************************************************************** 
--%> 

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<script type="text/javascript">
<!--
    function enableStdArea() {
       if (document.getElementById("advSStdAreaRow") != null) {
          document.getElementById("advSStdAreaRow").style.display = "inline";
       }
    }

    function disableStdArea() {
       if (document.getElementById("advSStdAreaRow") != null) {
          document.getElementById("advSStdAreaRow").style.display = "none";
       }
    }
         
    function enableRewArea() {
       if (document.getElementById("advSRewAreaRow") != null) {
          document.getElementById("advSRewAreaRow").style.display = "inline";
       }
       if (document.getElementById("advSPtsRow") != null) {
          document.getElementById("advSPtsRow").style.display = "inline";
       }
    }

    function disableRewArea() {
       if (document.getElementById("advSRewAreaRow") != null) {
          document.getElementById("advSRewAreaRow").style.display = "none";
       }
       if (document.getElementById("advSPtsRow") != null) {
          document.getElementById("advSPtsRow").style.display = "none";
       }
    }

    function toggleCheckbox(checkbox) {
       if (checkbox == "advSStdCat") {
           if (document.getElementById("advSStdCat").checked == true) {
               enableStdArea();
           }
           else {
               disableStdArea();
           }
       }
       else {
           if (document.getElementById("advSRewCat").checked == true) {
               enableRewArea();
           }
           else {
               disableRewArea();
           }
       }
    }

//-->         
</script>	
      
      