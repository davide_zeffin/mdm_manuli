<%@ page import="com.sap.isa.isacore.action.b2c.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ page import="com.sap.isa.businessobject.lead.Lead" %>
<%@ page import="com.sap.isa.core.SharedConst" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<jsp:useBean id="currentItem" scope="request" type="com.sap.isa.catalog.webcatalog.WebCatItem" />
<%
	// The attribute SharedConst.BASKET_LOADED_DIRECT is used to determine if the item should
	// be added directly to the basket. This is done when the basket has to be displayed directly
	// in case of web crawler access to the shop. This happens only once when the basket hasn't been displayed
	// because as soon as the basket is displayed the above attribute is turned to true. 
	
	UserSessionData userSessionDatajs = UserSessionData.getUserSessionData(request.getSession());
	String strjs = (String) userSessionDatajs.getAttribute(B2cConstants.BASKET_LOADED_DIRECT);
%>

<%@ include file="/catalog/js/productlist.js.inc.jsp" %>

<script type="text/javascript">
    
      function displayPackageComponentDetails(Itemkey, TopItemID) {
          document.productform.target = '';
          document.productform.itemkey.value = Itemkey;
          document.productform.topItemkey.value = TopItemID;
          document.productform.next.value = 'seeSubItemDet';
          document.productform.submit();
      }
      
      function createLead(Itemkey) {
          getMoreFunctions(Itemkey, 'createLead','');
      }
      
      function setUnitForDetails(Itemkey) {
          getMoreFunctions(Itemkey, 'seeItem','');
      }
      
      function backToBasket() {
         isDocumentView = true;
         isaTop().parent.location.href="<isa:webappsURL name="/b2b/catalogend.do"/>";
          //document.productform.target = '';
          //document.productform.action = '<isa:webappsURL name="/b2b/catalogend.do?display=true"/>';
          //document.productform.submit();
      }

      function open_details() {
          <% String stt="/catalog/product.do?isDetailOnly=yes&itemkey="+currentItem.getItemID()+"&areakey="+currentItem.getAreaID(); %>
          window.open('<isa:webappsURL name="<%= stt %>"/>', 'technicalData', 'width=400,height=350,scrollbars=yes');
          return false;
      }
    
      <%-- set focus on anchor point and further on config link if respective targets are availabe --%>
      function setfocus() {
          if (document.getElementById("basketAnchor") != null) {
              //location.hash="poshelp";
              document.getElementById("bottomAnchor").focus();
              document.getElementById("basketAnchor").focus();

              //if (document.getElementById("posfg") != null) {
              //   document.getElementById("posfg").focus();
              //}
          }
      }
      
      function onLoadProductDetails() {
          setfocus();
      <% if(strjs != null && strjs.equalsIgnoreCase("false")) { %>
          addSingleItem('<%= currentItem.getItemID() %>', 'addToBasket', '');
      <% }%>  
      }
      
       function setContractDuration(itemKey) {
          document.productform.target = '';
          document.productform.itemkey.value = itemKey;
          document.productform.next.value = 'seeItem';
          document.productform.submit();
       }
       
       function backToMainProduct(parentId) {
          document.productform.target = '';
          document.productform.itemkey.value = parentId;
          document.productform.topItemkey.value = "";
          document.productform.action = '<isa:webappsURL name="/catalog/showLastVisited.do?itemDetails=true"/>';
          document.productform.submit();
       }
       
       function compareUpselling(itemKey) {
          window.open("","compareWindow","menubar=no, directories=no, height=500, width=750, scrollbars=yes, status=no, toolbar=no, resizable = yes ");
      
          document.productform.target = 'compareWindow';
          document.productform.itemkey.value = itemKey;
          document.productform.next.value ='compareItems';
          document.productform.action='<isa:webappsURL name="/catalog/compareToSimilar.do?forward=CompareUpSelling"/>'
          document.productform.submit();
       }
       
       function displayAccessories(itemKey){
          document.productform.target = '';
          document.productform.itemkey.value = itemKey;
          document.productform.topItemkey.value = "";
          document.productform.displayedTab.value = "2";
          document.productform.action = '<isa:webappsURL name="/catalog/accesories.do?inFrame=yes"/>';         
          document.productform.submit();
       }
       
       function displayRelProducts(itemKey){
          document.productform.target = '';
          document.productform.itemkey.value = itemKey;
          document.productform.topItemkey.value = "";
          document.productform.displayedTab.value = "4";
          document.productform.action = '<isa:webappsURL name="/catalog/crossupSelling.do?inFrame=yes"/>';   
          document.productform.submit();
       }
       
       function displayAlternatives(itemKey){
          document.productform.target = '';
          document.productform.itemkey.value = itemKey;
          document.productform.topItemkey.value = "";
          document.productform.displayedTab.value = "3";
          document.productform.action = '<isa:webappsURL name="/catalog/upSelling.do?inFrame=yes"/>';    
          document.productform.submit();
       }
       
       function displayExchProds(itemKey){
          document.productform.target = '';
          document.productform.itemkey.value = itemKey;
          document.productform.topItemkey.value = "";
          document.productform.displayedTab.value = "5";
          document.productform.action = '<isa:webappsURL name="/catalog/remannewpart.do?inFrame=yes"/>';                                     
          document.productform.submit();
       }
       
       function displayContractDetails(itemKey) {
          document.productform.target = '';
          document.productform.itemkey.value = itemKey;
          document.productform.topItemkey.value = "";
          document.productform.displayedTab.value = "1";
          document.productform.action = '<isa:webappsURL name="/catalog/contractDetails.do"/>';                                     
          document.productform.submit();
       }
          
</script>	
      