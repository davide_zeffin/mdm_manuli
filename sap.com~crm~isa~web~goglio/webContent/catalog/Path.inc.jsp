<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<jsp:useBean id="thePath" scope="request" type="java.util.ArrayList" />

<div class="module-name"><isa:moduleName name="catalog/Path.inc.jsp" /></div>
<% BaseUI b2cpathui = new BaseUI(pageContext); %>
<% if (b2cpathui.isAccessible()) { %>
      <a href="#b2cpatharea-end" id="b2cpatharea-start"
          title= "<isa:translate key="b2c.cat.b2cpatharea.access.inf"/>" 
          accesskey="<isa:translate key="b2c.cat.b2cpatharea.access"/>"
      ></a>
<% } %>
<div class="module-content">
        <table border="0" cellspacing="0" cellpadding="0" >
          <tbody>
            <tr>
              <td>
                <% String rootCategory = WebUtil.translate(pageContext, "catalog.isa.products", null); 
                   String action = null;
                   String keyhomelink = "catalog.isa.products";
                   if (prodUI.isBuyPointsCategory()) {
                       action = "b2c/loyalty/gotoBuyPoints.do";
                   }
                   else if (prodUI.isRewardCategory()) {
                       action = "/b2c/loyalty/gotoRedeemPoints.do";
                   }
                   else { 
                     action = "/catalog/categorieInPath.do?key=0";
                   } %>
                  <span title="<isa:translate key="b2c.cat.category" arg0="<%=rootCategory%>" arg1=""/>">
                  	<isa:translate key="b2c.catalog.area.youarehere" />
                  	<a href="<isa:webappsURL name="<%= action %>"/>"><isa:translate key="<%= keyhomelink %>"/></a>
                  </span>
              </td>
           <% for (int i=thePath.size()-1;i>=0;i--) {
                 String temporaryPath="0";
                 for (int j=thePath.size()-1;j>=i;j--) {
                    temporaryPath+=("/"+((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(j)).getAreaID());
                 }
                 String arg="/catalog/categorieInPath.do?key="+temporaryPath; %>
                 <td><span title="<isa:translate key="b2c.cat.separator"/>">&nbsp;::&nbsp;</span>
                 </td>
                 <td>
              <% if (i == 0) {
                    String selected = WebUtil.translate(pageContext, "b2c.cat.selected", null); %>
                  <span title="<isa:translate key="b2c.cat.category" arg0="<%=JspUtil.encodeHtml( ((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(i)).getAreaName())%>" arg1="<%=selected%>"/>">
                <%= JspUtil.encodeHtml(((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(i)).getAreaName())%>
					</span>              
              <% } else { %>
                  <span title="<isa:translate key="b2c.cat.category" arg0="<%=JspUtil.encodeHtml( ((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(i)).getAreaName())%>" arg1=""/>">
                <a href="<isa:webappsURL name="<%= arg %>"/>" >
                <%= JspUtil.encodeHtml( ((com.sap.isa.catalog.webcatalog.WebCatArea)thePath.get(i)).getAreaName())%>
                </a>
					</span>              
              <% } %>
              </td>
            <% }%>
            </tr>
          </tbody>
        </table>
</div>
<% if (b2cpathui.isAccessible()) { %>
	<a href="#b2cpatharea-start"
		id="b2cpatharea-end"
	></a>        
<% } %>