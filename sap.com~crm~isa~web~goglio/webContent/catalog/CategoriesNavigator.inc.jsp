<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib prefix="isacore" uri="/isacore" %>

<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatArea" %>
<%@ page import="com.sap.isa.core.ui.context.GlobalContextManager" %>
<%@ page import="com.sap.isa.core.ui.context.ContextManager" %>
<%@ page import="com.sap.isa.core.Constants" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.RecommendationUI" %>
<%@ page import="com.sap.isa.catalog.uiclass.CategoriesUI" %>

<jsp:useBean    id="currentArea"
				type="com.sap.isa.catalog.webcatalog.WebCatArea"
				scope="request" >
</jsp:useBean>

<% String lastVisited="";
   if (request.getAttribute("lastVisited") != null) {
	   lastVisited=(String)request.getAttribute("lastVisited");
   }	
   String navCategoriesSuffix = "";
   RecommendationUI recUI = new RecommendationUI(pageContext);
   CategoriesUI categoriesui = new CategoriesUI(pageContext);
   
   String query = request.getParameter(ActionConstants.RA_CURRENT_QUERY);
   if (null==query && null!=request.getAttribute(ActionConstants.RA_CURRENT_QUERY)) {
	   query = request.getAttribute(ActionConstants.RA_CURRENT_QUERY).toString();
   }
   if (null==query && null!=GlobalContextManager.getValue(ActionConstants.CV_CURRENT_QUERY)) {
	   ContextManager contextManager =  ContextManager.getManagerFromRequest(request);	
	   query = contextManager.getContextValue(ActionConstants.CV_CURRENT_QUERY);
   }		
   if (null==query) {
	   query = "";
   }
   query = JspUtil.encodeHtml(query);
%>

<%@ include file="/catalog/js/Categories.js.inc.jsp"%>  

<div id="cat-navigator">
  <div class="module-name"><isa:moduleName name="catalog/CategoriesNavigator.inc.jsp" /></div>
<% 
if (categoriesui.isAccessible()) {
	if (currentArea != null && !currentArea.getAreaID().equals(WebCatArea.ROOT_AREA)) { %>
  <a href="#categoriesarea-end" id="categoriesarea-start" 
	 title="<isa:translate key=".cat.catarea" arg0="<%= JspUtil.encodeHtml(currentArea.getAreaName())%>"/>" 
	 accesskey="<isa:translate key=".cat.catarea.access"/>" />
<% }
	else { %>
  <a href="#categoriesarea-end" id="categoriesarea-start" 
	 title="<isa:translate key=".cat.catarea1"/>" 
	 accesskey="<isa:translate key=".cat.catarea.access"/>" />
<% } 
} %>

<% if (categoriesui.showQuickSearchInCat() || categoriesui.isPortal) { %>
  <div class="cat-navigator-qsrch">
	<div class="fw-box"><div class="fw-box-top"><div></div></div><div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3"><div class="fw-box-content">
	  <form name="QuickSearchForm" method="get" action="<isa:webappsURL name="/catalog/query.do" />">   
		<div class="cat-navigator-qsrch-label">
		  <label for="navQSearch"><isa:translate key="catalog.isa.quickSearch"/></label>
		</div>
		<div class="cat-navigator-qsrch-inp">
		  <input class="textInput"
				 type="text" 
				 id="navQSearch"
				 name="<%=ActionConstants.RA_CURRENT_QUERY%>" 
				 value='<%=query%>' />
		</div>
		<div class="cat-navigator-qsrch-submit">
		  <input class="FancyButtonGrey"
				 type="submit" 
				 value="&nbsp;<isa:translate key="catalog.isa.search"/>&nbsp;" />
		</div>
	  </form>
	  <%-- display advanced search link --%>
	  <% String accessText = WebUtil.translate(pageContext, "b2c.navigationbar.advsearch", null); %>
	  <div class="cat-navigator-qsrch-adv">
		<a class="header-search" 
		   href="<isa:webappsURL name="/catalog/advSearchForward.do"/>" 
		   title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>"><span><%=accessText%></span></a>
	  </div>     
	</div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
  </div> <%-- cat-navigator-qsrch --%>
<% } %>


<%  if (categoriesui.showOciLink()) { %>
  <div class="cat-navigator-extcat">
	<div class="fw-box"><div class="fw-box-top"><div></div></div><div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3"><div class="fw-box-content">
	  <a class="viewSelectionInactive"  
		 name="linkcatview" 
		 href="#" 
		 onclick="openWin('<%=categoriesui.getExternalCatalogURL() %>'); return false;" 
		 title="<isa:translate key="b2b.header.extern.catalog.access"/>" 
	  ><isa:translate key="b2b.header.extern.catalog" /></a>
	</div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
  </div>
<%  } %>


<%--  Categories --%>
<div id="secnav">
<%  String rootText = WebUtil.translate(pageContext, "catalog.isa.products", null); %>

  <ul class="cat-secnav-home-area">
	<li class="secnav-home">
	  <a href="<isa:webappsURL name="/catalog/catalogForward.do">
			   <isa:param name="catalogEntry" value="true"/>
			   </isa:webappsURL>"
		 title="<isa:translate key="access.link" arg0="<%= rootText %>" arg1=""/>"
	  ><%= rootText %></a>
	</li>
  </ul>
  <div class="secnav-contbox">
<%  String areaType = WebCatArea.AREATYPE_REGULAR_CATALOG; %>
	<%@ include file="/catalog/Categories.inc.jsp"%>
  </div>  
</div> <%-- secnav --%>


<%  if (recUI.showPersRec() || recUI.showSpecialOffers() || categoriesui.showCamapignFields()) { %>
  <div class="cat-secnav-mkt">
	<div class="fw-box"><div class="fw-box-top"><div></div></div><div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3"><div class="fw-box-content">
<%      if (recUI.showPersRec()) { %>
	  <div id="recomm">
<%          if (!lastVisited.equals("recommendations")) { %>
		<a href="<isa:webappsURL name="/catalog/gotorecommendations.do?uiarea=workarea"/>"><isa:translate key="catalog.isa.getPersonalizedRecommend"/></a>
<%          } else { %>
		<span><isa:translate key="catalog.isa.getPersonalizedRecommend"/></span>
<%          } %>
	  </div>
<%      } %>
<%      if (recUI.showSpecialOffers()) { %>
	  <div id="bests">
<%          if (!lastVisited.equals("bestseller")) { %>
		<a href="<isa:webappsURL name="/catalog/gotobestseller.do"/>"
		><isa:translate key="catalog.isa.bestseller"/></a>
<%          } else { %>
		<span><isa:translate key="catalog.isa.bestseller"/></span>
<%          } %>
	  </div>
<%      } %>
	  <%-- Campaign Code --%>
<%      if (categoriesui.showCamapignFields()) { %>
	  <div id="camp"> 
		<form name="campCodeForm" action="<isa:webappsURL name="/catalog/campaign.do"/>" method="post">
		  <div class="cat-secnav-mkt-camplabel">
			<label for="CampaignCode"><isa:translate key="catalog.isa.campaignCode"/></label>
		  </div>  
		  <div class="cat-secnav-mkt-campinp">
			<input type="text" 
				   class="textInput" 
				   name="campaigncode" 
				   id="CampaignCode" 
				   value="<%= JspUtil.encodeHtml(currentArea.getCatalog().getCampaignId()) %>" />
			<span class="imgbtn-box">
			  <a class="imgbtn-lk" href="javascript:deleteCampCode()">
				<img class="cat-imgbtn-delete"
					 src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
					 alt="<isa:translate key="b2c.cat.campaign.delete"/>"
					 title="<isa:translate key="b2c.cat.campaign.delete"/>"
					 width="16" height="16" border="1"
			  /></a>
			</span>  
			<span class="imgbtn-box">
			  <img class="cat-imgbtn-hint" 
				   src="<isa:mimeURL name="mimes/catalog/images/spacer.gif" />" 
				   alt="<isa:translate key="b2c.cat.campaign.desc"/>"
				   title="<isa:translate key="b2c.cat.campaign.desc"/>"
				   width="16" height="16" border="1"
			  />
			</span>  
		  </div>
		  <div class="cat-secnav-mkt-campsubmit">
			<input class="FancyButtonGrey"
				   type="Submit" value="&nbsp;<isa:translate key="catalog.isa.apply"/>&nbsp;" />
		  </div>       
		</form>
	  </div>
<%      } %>
	</div><%-- fw-box-content --%></div><%-- fw-box-i3 --%></div><%-- fw-box-i2 --%></div><%-- fw-box-i1 --%><div class="fw-box-bottom"><div></div></div><%-- fw-box-bottom --%></div><%-- fw-box --%>
  </div> <%-- cat-secnav-mkt --%>
<%  } %>


<%  if (categoriesui.isAccessible()) { %>
	<a href="#ategoriesarea-start"
	   id="categoriesarea-end"
	></a>        
<%  } %>
</div>