<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:stylesheets group="catalog"/>

<div id="bestseller" class="module">

<div class="module-name"><isa:moduleName name="catalog/blank_window.inc.jsp" /></div>

<% if (request.getParameter("UpSelling") != null) {%>
      <isa:translate key="catalog.isa.noRelated"/>
      <p align="right">
         <a href="#" class="button" title="<isa:translate key="catalog.isa.close"/>" name="<isa:translate key="catalog.isa.close"/>" onclick="parent.window.close();"><isa:translate key="catalog.isa.close"/></a>
	  </p>
 <%}
   else { %>
	  <script type="text/javascript">
		alert("<isa:UCtranslate key="catalog.isa.selectSomething"/>");
		window.parent.close();
	  </script>
 <%}%>
</div>
