<%@ page language="java" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatArea" %>
<%@ page import="com.sap.isa.backend.crm.webcatalog.pricing.PriceCalculatorInitDataCRMIPC" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatAreaAttribute" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference" %>
<%@ page import="com.sap.isa.businessobject.Product" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.catalog.AttributeKeyConstants" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.catalog.uiclass.ProductsUI" %>
<%@ page import="com.sap.isa.core.Constants" %>
<%@ page import="com.sap.isa.core.ui.context.GlobalContextManager" %>
<%@ page import="com.sap.isa.core.ui.context.ContextManager" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.CatalogBlockViewUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.PricingBaseUI" %>
<%@ page import="com.sap.isa.catalog.actions.ActionConstants" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>

<%--@ include file="/catalog/js/Products.js.inc.jsp"--%>

<script src="<isa:webappsURL name="catalog/js/pricing.inc.js"/>" type="text/javascript"></script>

<%@ include file="pricing.java.inc.jsp" %>

<% String accessText = "";
   ProductsUI prodUI = new ProductsUI(pageContext);
   String isQuery = prodUI.getIsQuery();
   CatalogBlockViewUI ui = new CatalogBlockViewUI(pageContext, prodUI.isCatalogQuery());
   PriceCalculatorInitDataCRMIPC priceCalcInitData = (PriceCalculatorInitDataCRMIPC) prodUI.getPriceCalcInitData(); 
   
   // required for some of the includes JSP or their includes
   WebCatItemPage itemPage = prodUI.getItemPage();
   Hashtable htTerms = prodUI.getSearchTerms(prodUI.getItemPage()); 

// GREENORANGE BEGIN
	UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());

	//get Internet Sales core functionality BOM
	BusinessObjectManager isaBOM =
		(BusinessObjectManager) userSessionData.getBOM(
			BusinessObjectManager.ISACORE_BOM);

	String current_user = isaBOM.getUser().getUserId().toUpperCase();	
	String USER_GUEST = (String)userSessionData.getAttribute("user_guest");
// GREENORANGE END	
  %>

<div class="module-name"><isa:moduleName name="catalog/Products.inc.jsp" /></div>

<%  if (prodUI.getPopUpCompare()) { %>
       function loadPage() {
	  <script type="text/javascript">
        <%if(prodUI.getnotEnoughProducts()) {%>alert('<isa:UCtranslate key="catalog.isa.selectSomething"/>')<% }else{ %>popUpCompare()<%}%>;
       }
	  </script>
 <% } else if(prodUI.getnotEnoughProductsForTransfer()) { %>
	  <script type="text/javascript">
       function loadPage() {
        alert('<isa:UCtranslate key="catalog.isa.selectSomethingForTransfer"/>');
       }
	  </script>
 <% } %>

 
<%  if (prodUI.isAccessible()) { %>
       <a id="prodUIheaderarea-start" href="#prodUIheaderarea-end" title= "<isa:translate key="b2c.cat.prodheadarea.access.inf"/>" 
          accesskey="<isa:translate key="b2c.cat.b2cprodheadarea.access"/>">
       </a>
<%  }

    MessageList msgList = prodUI.getCampaignMessageList();
    pageContext.setAttribute(AttributeKeyConstants.CAMPAIGN_MESSAGE_LIST, msgList);
    if (msgList != null && !msgList.isEmpty()){ 
%>
    <div class="cat-prd-msg">
      <isa:message id="messagetext" name="<%= AttributeKeyConstants.CAMPAIGN_MESSAGE_LIST %>" ignoreNull="true">
        <div class="error"><span><%=JspUtil.encodeHtml(messagetext)%></span></div>
      </isa:message>
    </div>  
<%  } 

   if (!prodUI.isCatalogQuery()) { /* If it is not a query */
      if (prodUI.isCategorySpecificSearch()) { %>
      <div style="float: left">
       <h1 class="areatitle"><isa:translate key="b2c.catalog.areatitle" arg0="<%= JspUtil.encodeHtml(prodUI.getCurrentArea().getAreaName()) %>" /></h1>
      </div>
   <% } 
      else { /* it's a normal list */ %>
      <div style="float: left">
       <h1 class="areatitle"><isa:translate key="b2c.catalog.areatitle" arg0="<%= JspUtil.encodeHtml(prodUI.getCurrentArea().getAreaName()) %>" /></h1>
      </div>
     <% } 
   } 
   else if (itemPage.getListType() == ActionConstants.CV_LISTTYPE_BESTSELLER) { /* it's a special offers list */ %>
      <div style="float: left">
       <h1 class="areatitle"><isa:translate key="bestseller.jsp.title"/></h1>
      </div>
<% } 
   else if (itemPage.getListType() == ActionConstants.CV_LISTTYPE_RECOMMENDATIONS) { /* it's a recommendations list */ %>
      <div style="float: left">
       <h1 class="areatitle"><isa:translate key="recommendation.jsp.header"/></h1>
      </div>
<% } 
   else if (itemPage.getListType() == ActionConstants.CV_LISTTYPE_CUA) { /* it's a cua list */ %>

<%            if (prodUI.isCUAProductId()) { 
%>
    <div style="float: left">
    <h1 class="areatitle">
<%              if (prodUI.isAccessories()){ %>
      <isa:translate key="cua.jsp.accessory"/>
<%              }
                else if (prodUI.isRelatedProducts()) {
%>
      <isa:translate key="cua.jsp.crossselling"/>
<%              }
                else if (prodUI.isAlternatives()) {
%>
      <isa:translate key="cua.jsp.upselling"/>
<%              }
                else {
%>
      <isa:translate key="cua.jsp.header"/>
<%              } %>
      &nbsp; <%= JspUtil.encodeHtml(prodUI.getCuaProductId()) %>
    </h1>
    </div>

<%           } %>

<% }  
   else { /* it's a catalog query */ %>
      <div style="float: left">
          <h1 class="areatitle"><isa:translate key="catalog.isa.searchResults"/></h1>
      </div>
  <% } %>

<!-- Back link -->
      <div style="text-align: right; padding: 4px 0px 4px 0px">
<%      if (prodUI.getTheCatalog().getSavedItemList() != null) { %>
         <a href ="<isa:webappsURL name="/catalog/query.do"/>" class="FancyLinkGrey"><isa:translate key="catalog.isa.backTo"/> <isa:translate key="catalog.isa.searchResults"/></a>
       <%} else if (prodUI.getCurrentArea().getAreaID().equals("$ROOT")) { %>
         <a href="<isa:webappsURL name="/catalog/categorieInPath.do?key=0"/>" class="FancyLinkGrey"><isa:translate key="catalog.isa.backTo"/> <isa:translate key="catalog.isa.products"/></a>
       <% } %>
      </div>
      <div style="clear: left;"></div>
<!-- end Back link -->  

<!-- breadcrumb -->
<%   if (!prodUI.isCatalogQuery() && ! prodUI.isCategorySpecificSearch()) { %>
       <%@ include file="Path.inc.jsp"%>
<%   } %>
<!--  end breadcrumb -->

<%    if ( prodUI.areaSpecificSearchShouldBeDisplayed() ) { %>
       <%@ include file="ProductsHeader.inc.jsp"%>
  <% }
  
     if (prodUI.isAccessible()) { %>
       <a id="prodUIheaderarea-end" href="#prodUIheaderarea-start"></a>
  <% } %>
  
<%-- END OF CODE for the upper part of page (ie: Categorie Picture, Categorie Name, Categorie Description, And Filter --%>

   <% if (ui.showAsBlockView()) {
          ui.setShowPageFlag(true);
          ui.setIsCompareAllowed(prodUI.isCRM());
          ui.setCalledFromCatalog(true);
          CatalogBlockViewUI priceUI = ui; %>
         <%@ include file="productlist.block.inc.jsp"%>
   <% } 
      else {
          ProductsUI priceUI = prodUI; %>
         <%@ include file="productlist.list.inc.jsp"%>
   <% } %>
   