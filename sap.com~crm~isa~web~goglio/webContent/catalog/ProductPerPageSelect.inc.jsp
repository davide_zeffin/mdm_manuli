          <select id="pageselect" name="pageselect" size="1" onChange="setPageSize(document.productform.pageselect[document.productform.pageselect.selectedIndex].value)" valign="middle">
            <option value="<%=itemPage.calculatePageSize(1)%>" <% if (itemPage.pageSize() == itemPage.calculatePageSize(1)) { %> selected="selected" <% } %>><%=itemPage.calculatePageSize(1)%></option>
            <option value="<%=itemPage.calculatePageSize(2)%>" <% if (itemPage.pageSize() == itemPage.calculatePageSize(2)) { %> selected="selected" <% } %>><%=itemPage.calculatePageSize(2)%></option>
            <option value="<%=itemPage.calculatePageSize(3)%>" <% if (itemPage.pageSize() == itemPage.calculatePageSize(3)) { %> selected="selected" <% } %>><%=itemPage.calculatePageSize(3)%></option>
            <option value="<%=itemPage.calculatePageSize(4)%>" <% if (itemPage.pageSize() == itemPage.calculatePageSize(4)) { %> selected="selected" <% } %>><%=itemPage.calculatePageSize(4)%></option>
            <option value="0" <% if (itemPage.pageSize() == 0) { %> selected="selected" <% } %>><isa:translate key="catalog.isa.all"/></option>
          </select>
