<%--
********************************************************************************
    File:         itemconfiginfo.inc.jsp
    Copyright (c) 2004, SAP AG
    Author:       SAP AG
    Created:      17.17.2004
    Version:      1.0

    Display of item configuration information in the item details

	OBSOLETE
    Prerequisites: itemConfig - must point to the current item (ItemSalesDoc or IPCItem)
                   showDetailView - true(boolean), if detailled information should be displayed

    $Revision: #1 $
    $Date: 2004/12/06$
    
    $Revision: #2 $
    $Date: 2006/03/15$
    Prerequisites: itemConfig - must point to the current item (ItemSalesDoc or IPCItem)
                   viewType - int value
                         0 if information should be displayed (order context)
                         1 if detailled information should be displayed (order context)
                         2 if detailled information should be displayed (catalog context)
                   isSubItem -- true if the item is subitem -- relevant for the case 0
********************************************************************************
--%>
<%@ page import="com.sap.isa.isacore.uiclass.ItemConfigurationInfoHelper" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sap.spc.remote.client.object.Characteristic" %>
<%@ page import="com.sap.spc.remote.client.object.CharacteristicGroup" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>


<%  ItemConfigurationInfoHelper itemConfigInfo = ui.getItemConfigInfoHelper();
    if (itemConfig != null && itemConfig.isConfigurable()) { 
        itemConfigInfo.setItem(itemConfig);
        if (itemConfigInfo.isConfigurationAvailable()) { 
	      switch (viewType) {
	         case 0 : 
                if ( ui.getOrderView() != ' ' && isSubItem ) { %>
			       <%-- in the short view only the configuration of the rootitem should be displayed --%>
                   <%= JspUtil.encodeHtml(itemConfigInfo.getItemConfigValues(ui.getOrderView())) %>                            
             <% }
                break;
             case 1 :           
                if (ui.getOrderDetailView() != ' ') { %>
                   <%-- the detailled view displays the characteristics sorted by groups --%>
                   <%-- first line  --%>
                   <isa:translate key="b2b.display.prod.config"/>                
                   <% ArrayList charGroups = itemConfigInfo.getItemConfigGroups(ui.getOrderDetailView());
                      if (charGroups.size() == 1) {  %>
                         <%-- group name will not be displayed if there is not more than one group--%>                 	
                         <% ArrayList characteristics = itemConfigInfo.getCharacteristics((CharacteristicGroup)charGroups.get(0), ui.getOrderDetailView());
                         Characteristic characteristic;  %>                    
					     <%-- characteristics and values --%>                         
                         <% for (int idx = 0; idx < characteristics.size(); idx++) {
   						    characteristic = (Characteristic)characteristics.get(idx);  %>
                            <br margin-left:10px > <%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicName(characteristic)) %>:&nbsp;<%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicValues(characteristic)) %>            
                         <% } %>
                    <% } else { %>
                         <%  ArrayList characteristics = null;
					         Characteristic characteristic; 
                             for (int j = 0; j < charGroups.size(); j++) { 
                                characteristics = itemConfigInfo.getCharacteristics((CharacteristicGroup)charGroups.get(j), ui.getOrderDetailView()); %>                
   						        <%-- group name  --%>
   						        <br margin-left:10px > <%= itemConfigInfo.getCharacteristicGroupName((CharacteristicGroup)charGroups.get(j)) %>
						        <%-- characteristics  and values --%>   
                                <% for (int idx = 0; idx < characteristics.size(); idx++) {
   						           characteristic = (Characteristic)characteristics.get(idx);  %>
                                   <br margin-left:20px>
                                      <%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicName(characteristic)) %>:&nbsp;<%= JspUtil.encodeHtml(itemConfigInfo.getCharacteristicValues(characteristic)) %>
                                <% } %>   <%-- end characteristics --%>   
                           <% } %>   <%-- end group names --%>
                     <% } %>   <%-- end else --%>
                <% } %>  <%-- if (configInfoDetailView )   --%>
                <% break;
             case 2:
                if ( ui.getCatalogConfigView() != ' ' ) { %>
                   <%-- in the catalog view only the configuration of the rootitem should be displayed --%>
                   <%= JspUtil.encodeHtml(itemConfigInfo.getItemConfigValues(ui.getCatalogConfigView())) %>                            
             <% }
                break;
          } %> <%--end switch --%>
   <% } %>  <%-- if (!itemConfigInfo.isConfigurationAvailable())   --%> 							                         
<% } %>   <%-- if (itemConfig.isconfigurable..   --%>