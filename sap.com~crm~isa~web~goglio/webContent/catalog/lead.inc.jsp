<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/cic" prefix="cic" %>

<%@ page import= "java.util.*,javax.servlet.http.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<jsp:useBean id="currentItem" scope="request" type="com.sap.isa.catalog.webcatalog.WebCatItem" />

<%
	BaseUI leadui = new BaseUI(pageContext);
%>

	  <div class="module-name"><isa:moduleName name="catalog/lead.inc.jsp" /></div>
	  <form action="<isa:webappsURL name="lead/creation.do" />" name="leadForm" method="POST">

<%
	if (leadui.isAccessible())
	{
%>
<a href="#" title= "<isa:translate key="b2c.cat.lead.jsp.main"/>" accesskey="<isa:translate key="b2c.cat.lead.jsp.main.access"/>"></a>
<%
	}
%>
	  <TABLE cellpadding="0" cellspacing="0" border="0" width ="100%">
                <tr>
		  <TD width="2%">
         	  </TD>
                  <td><span><strong><isa:translate key="catalog.isa.productNo"/>:</strong></span></td>
		  <td><span><%= JspUtil.encodeHtml(currentItem.getProduct()) %></span></td>
		</tr>
		<tr>
		  <TD width="2%">
         	  </TD>
		  <td><span><isa:translate key="catalog.isa.productDescription"/>:</span></td>
		  <td><span><%= JspUtil.encodeHtml(currentItem.getDescription()) %></span></td>
		</tr>
		<tr>
		  <TD width="2%">
         	  </TD>
		  <td><span><isa:translate key="catalog.isa.quantity"/>:</span></td>
		  <td><span><%= currentItem.getQuantity() %></span></td>
		</tr>
                <tr>
		  <TD width="2%">
         	  </TD>
                  <td><span><strong><isa:translate key="catalog.isa.price"/>:</strong></span></td>
                  <td>
		    <table border="0" cellpadding="0" cellspacing="0">
		      <tbody>
			<% java.util.Iterator pricesIterator = currentItem.readItemPrice().getPrices();
			   while (pricesIterator.hasNext()) {
			     String st = (String) pricesIterator.next();
			%>
			<tr><td><span>&nbsp;<%= st %></span></td></tr>
                        <% } %>
                      </tbody>
                    </table>
		  </td>
                </tr>

	  <TR height="10">
		&nbsp;
	  </TR>
	  <TR>
		<TD width="2%">
         	</TD>
	  	<TD colspan="2">
	  		<label for="leadText">
			<isa:translate key="b2b.lead.create.label" />
			</label>
		</TD>
	  </TR>
	  <TR>
		<TD width="2%">
         	</TD>
	  	<TD colspan="2">
	  		<textarea name="leadText" class="textInput" rows="16" cols="60"></textarea>
	  	</TD>
	  </TR>
	  <TR height="15">
		&nbsp;
	  </TR>
	  </TABLE>
<%
	if (leadui.isAccessible())
	{
%>
<a href="#" title= "<isa:translate key="b2c.cat.lead.jsp.btnarea"/>" accesskey="<isa:translate key="b2c.cat.lead.jsp.btnarea.access"/>"></a>
<%
	}
%>
      <%
        String saveBtnTxt = WebUtil.translate(pageContext, "cic.button.submit", null);
        String cancelBtnTxt = WebUtil.translate(pageContext, "cic.button.cancel", null);
      %>          
		 <TABLE cellpadding="0" cellspacing="0" border="0" width ="100%" >
		    <TR>
			<TD width="20%">
         		</TD>
         		<TD width="20%">
          			<input type="submit" name="Save" class="FancyButton"  
          				title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>" 
          				value="<isa:translate key="cic.button.submit" />">
			</TD>
         		<TD>
          			<input type="button" name="Cancel" class="FancyButton" 
          				title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>" 
          				value="<isa:translate key="cic.button.cancel" />" onClick="history.back();return false;">
 			</TD>
		    </TR>
		</TABLE>
          </form>
