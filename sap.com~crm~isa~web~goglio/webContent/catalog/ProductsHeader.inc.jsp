<%@ page import="com.sap.isa.catalog.webcatalog.WebCatAreaQuery" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItemPage" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<jsp:useBean    id="attributesList"
                type="java.util.ArrayList"
                scope="request" >
</jsp:useBean>

<div class="module-name"><isa:moduleName name="catalog/ProductsHeader.inc.jsp" /></div>
<%@ include file="/catalog/js/ProductsHeader.js.inc.jsp" %>
  <div>

<!-- BEGIN OF CODE for the upper part of page (ie: Categorie Picture, Categorie Name, Categorie Description, And Filter -->
  <table width="100%" border="0">
    <tbody>
      <tr>
        
        <% WebCatArea area = prodUI.getCurrentArea();
           pageContext.setAttribute("area", area); %>
              <isa:ifThumbAvailable name="area" guids="DOC_PC_CRM_IMAGE,DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE">
                     <img src="<isa:imageAttribute guids="DOC_PC_CRM_IMAGE,DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" name="area" />"
                            alt="<%= area.getAreaName() %>" />
              </isa:ifThumbAvailable>
        
        <td align="right"> <!-- Here is the filter data  -->
<%  if (attributesList.size() > 0) {
       BaseUI b2cheaderui = new BaseUI(pageContext);
       String b2cheadergrpTxt = WebUtil.translate(pageContext, "catalog.isa.areaFilters", null); %>
            <form action="<isa:webappsURL name="/catalog/filter.do"/>" method="POST" name="filterForm" onSubmit="return verifier();">
<%  if (b2cheaderui.isAccessible()) { %>
			<a href="#groupb2cheader-end" id="groupb2cheader-begin" title="<isa:translate key="access.grp.begin" arg0="<%=b2cheadergrpTxt%>"/>" ></a>
<% } %>
              <table border="0">
                <thead>
                  <tr>
                    <td colspan="2" align="left"><isa:translate key="catalog.isa.areaFilters"/></td>
                  </tr>
                </thead>
                <tbody>

         <% attributes = attributesList.iterator();
            while (attributes.hasNext()) {
              WebCatAreaAttribute attribute = (WebCatAreaAttribute) attributes.next();
              if (attribute.getDescription() != null) {
                  String selectedValue = prodUI.getSelectedValue(attribute, prodUI.getItemPage());%>
                  <tr>
                    <td align="left"><label for="<%=attribute.getGuid()%>"><%= JspUtil.encodeHtml(attribute.getDescription()) %></label></td>

              <%  if (attribute.getStyle() == WebCatAreaAttribute.DROP_DOWN && attribute.getValues() != null) { %>
                    <td align="left"><select style="width: 100px;" id="<%= attribute.getGuid() %>" name="<%= attribute.getGuid() %>" >
                          <option
                          <% if (selectedValue == null) { %>
                          selected="selected"
                          <% } %>
                          value=""><isa:translate key="catalog.isa.all"/></option>
                   <% values = attribute.getValues().iterator();
                      while (values.hasNext()) {
                        String value = (String) values.next(); %>
                          <option
                          <% if (selectedValue != null && selectedValue.equals(value)) { %>
                          selected="selected"
                          <% } %>
                          value="<%= value %>"><%= value %></option>
                   <% } %>
                        </select></td>
               <% } %>

             <% if (attribute.getStyle() == WebCatAreaAttribute.RADIO_BUTTON && attribute.getValues() != null) { %>
                    <td align="left">
                      <input type="radio" id="<%= attribute.getGuid() %>" name="<%= attribute.getGuid() %>" value=""
                          <% if (selectedValue == null) { %>
                            checked="checked"
                          <% } %>
                    > <isa:translate key="catalog.isa.all"/><br>
                 <% values = attribute.getValues().iterator();
                    int _index=0;
                    while (values.hasNext()) {
                       String value = (String) values.next();
                       String idforradio = "radio_" + attribute.getGuid() + Integer.toString(_index);
                       _index++; %>
                          <input type="radio" id="<%=idforradio%>" name="<%= attribute.getGuid() %>" value="<%= value %>"
                          <% if (selectedValue != null &&  selectedValue.equals(value)) { %>
                            checked="checked"
                          <% } %>
                          > <label for="<%=idforradio%>"><%= value %></label><br>
                <% } %>
                    </td>
             <% } 

               if (attribute.getStyle() == WebCatAreaAttribute.INPUT_FIELD && attribute.getValues() != null) { %>
                    <td align="left"><input type="text" class="textInput" id="<%= attribute.getGuid() %>" name="<%= attribute.getGuid() %>" maxlength="20" style="width: 100px;"
                        <% if (selectedValue != null) { %>
                            value="<%= JspUtil.encodeHtml(selectedValue) %>"
                        <% } %>
                    ></td>
            <% } %>
               </tr>
        <% } 
        } %>
                  <tr>
        <% String filterBtnTxt = WebUtil.translate(pageContext, "catalog.isa.Filter", null);
           String resetBtnTxt = WebUtil.translate(pageContext, "catalog.isa.reset", null); %>
           <td align="center">
           <input type="Submit" 
		    	title="<isa:translate key="access.button" arg0="<%=filterBtnTxt%>" arg1=""/>"
               	value="<isa:translate key="catalog.isa.Filter"/>" class="FancyButtonGrey">
               	</td>
                <td align="center">
             <% if (prodUI.getCurrentArea().getAreaID().equals("$ROOT")) { %>
                       <input type="button" 
				    	title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>"
                       	value="<isa:translate key="catalog.isa.reset"/>" class="FancyButtonGrey" onClick="location.href='<isa:webappsURL name="/catalog/categorieInPath.do?key=0"/>'">
             <% } else {
                     String st = "/catalog/categorieInPath.do?key="+prodUI.getCurrentArea().getPathAsString();  %>
                     <input type="button" 
				    	title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>"
                       	value="<isa:translate key="catalog.isa.reset"/>" class="FancyButtonGrey" onClick="location.href='<isa:webappsURL name="<%= st %>"/>'">
             <% } %>
                </td>
                  </tr>
                </tbody>
              </table>
       <% if (b2cheaderui.isAccessible()) { %>
             <a href="#groupb2cheader-begin" id="groupb2cheader-end" title="<isa:translate key="access.grp.end" arg0="<%=b2cheadergrpTxt%>"/>"></a>	
       <% } %>
            </form>
<% } 
   else { %>
            &nbsp;
<% } %>
            </td> <!-- End of filter data  -->
          </tr>
          <tr>
            <td colspan=2> <!--Here is category long text -->
            <span><%= JspUtil.encodeHtml(prodUI.getCurrentArea().getCategory().getDescription()) %></span>
            </td> <!-- End of category long text -->
          </tr>
        </tbody>
      </table>
  </div>

