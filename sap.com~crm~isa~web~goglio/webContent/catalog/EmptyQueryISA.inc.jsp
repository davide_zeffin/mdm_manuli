<%@ page language="java" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<div id="information" class="module">

<div class="module-name"><isa:moduleName name="catalog/EmptyQueryISA.inc.jsp" /></div>

      <div style="float: left">
          <h1 class="areatitle"><isa:translate key="catalog.isa.searchResults"/></h1>
      </div>
      <div style="text-align: right; padding: 4px 0px 4px 0px">
<%  com.sap.isa.catalog.webcatalog.WebCatArea currentArea = (com.sap.isa.catalog.webcatalog.WebCatArea) request.getAttribute("currentArea");
    if (currentArea == null || currentArea.getAreaID().equals("$ROOT")) {
%>
        <a href="<isa:webappsURL name="/catalog/categorieInPath.do?key=0"/>" class="FancyLinkGrey"><isa:translate key="catalog.isa.backTo"/> <isa:translate key="catalog.isa.products"/></a>
<%  } 
    else { 
        String tmpPath="/catalog/categorieInPath.do?key="+currentArea.getPathAsString();
%>
        <a href="<isa:webappsURL name="<%= tmpPath %>"/>" class="FancyLinkGrey"><isa:translate key="catalog.isa.backTo"/> <%= JspUtil.encodeHtml(currentArea.getAreaName()) %></a>
<%  } %>
      </div>
      <div style="clear: left;"></div>
      <div class="areainfo"><isa:translate key="catalog.isa.QueryNoData"/></div>
</div>
