<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="com.sap.isa.core.SessionConst" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.catalog.webcatalog.WebCatInfo" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<br><br><br>

<% if (!productFound){
    String type = (String) request.getAttribute("type");
    type = JspUtil.encodeHtml(type);
%>
    <a href="javascript:history.back();" class="FancyLinkGrey">
    <% if (type != null) {%>
        <% if (type.equals("query")) {%>
            <isa:translate key="catalog.isa.backToQuery"/>
        <% } %>
        <% if (type.equals("products")) {%>
            <isa:translate key="catalog.isa.backTo"/> &nbsp; <isa:translate key="catalog.isa.products"/>
        <% } %>
        <% if (!type.equals("products") && !type.equals("query") ) {%>
            <isa:translate key="catalog.isa.backTo"/> &nbsp; <%= type %>
        <% }%>
    <% } else { %>
        <isa:translate key="catalog.isa.backTo"/> &nbsp;<isa:translate key="catalog.isa.products"/>
    <% } %>
    </a>
<br><br>
<%} else {%>
<% if (request.getAttribute("backPath") != null) {
    String st = (String) request.getAttribute("backPath");
	st = JspUtil.encodeHtml(st);
    String type = (String) request.getAttribute("type");
	type = JspUtil.encodeHtml(type);
%>
    <a href="<isa:webappsURL name="<%= st %>"/>" class="FancyLinkGrey">
    <% if (type.equals("query")) {%>
    <isa:translate key="catalog.isa.backToQuery"/>
    <% } %>
    <% if (type.equals("products")) {%>
    <isa:translate key="catalog.isa.backTo"/> &nbsp; <isa:translate key="catalog.isa.products"/>
    <% } %>
    <% if (!type.equals("products") && !type.equals("query") )  {%>
    <isa:translate key="catalog.isa.backTo"/> &nbsp; <%= type %>
    <% }%>
    </a>
<br><br>
<% } %>
<% } %>