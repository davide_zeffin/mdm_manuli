<%@ page import="com.sap.isa.core.util.WebUtil,
			com.sap.isa.core.Constants,
			com.sapportals.htmlb.table.*,
			com.sapportals.htmlb.enum.TableColumnType,
			com.sapportals.htmlb.enum.TableCellStyle,
			com.sap.isa.core.xcm.ConfigContainer,
			com.sap.isa.core.xcm.scenario.ScenarioConfig,
			com.sap.isa.core.xcm.scenario.ScenarioManager,
			com.sap.isa.core.xcm.init.ExtendedConfigInitHandler" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.init.InitializationHandler" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Collections" %>
<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%
	String urlAdmin = WebUtil.getAppsURL(pageContext, null, "/admin/index.jsp", null, null, false);        
	String applicationURL = "/init.do";
	String highavURL = "/admin/highav/init.do";
	String showStartJspString = WebUtil.getApplicationConfigParameter( InitializationHandler.getApplicationName()+"config" ,"show.start.jsp");
	boolean showStartJsp = ( showStartJspString != null && (showStartJspString.equals("true") || showStartJspString.equals( "yes")));
	String  applicationName=WebUtil.translate(pageContext, "startpage.header", null);
	String  applicationAdmin=WebUtil.translate(pageContext, "xcm.admin.link.text", null);
	String highAvText= WebUtil.translate(pageContext, "xcm.highav.link.text",null);
	String sessiontraceText=  WebUtil.translate(pageContext, "xcm.single.sess.trace",null);
	String errorMessage  = WebUtil.translate(pageContext, "start.no.xcmscenarios", null);
	String errorMessage2  =  WebUtil.translate(pageContext, "start.page.disabled", null);

	if (ExtendedConfigInitHandler.isTargetDatastoreDB() 
					&& !ExtendedConfigInitHandler.isDBInitSuccessful()) {
		errorMessage = WebUtil.translate(pageContext, "system.xcm.db.init.failed", null);
	}

    
 %>
<hbj:content id="myContext"> 
	<hbj:page title="<%=applicationName%>">
		<style>
.sapAppBrandTopLvl2
{
	BORDER-RIGHT: #ffffff 1px solid;
	PADDING-RIGHT: 0px;
	BORDER-TOP: #ffffff 1px solid;
	PADDING-LEFT: 5px;
	FONT-SIZE: 1px;
	PADDING-BOTTOM: 0px;
	VERTICAL-ALIGN: top;
	BORDER-LEFT: #ffffff 0px;
	WIDTH: 6px;
	PADDING-TOP: 0px;
	BORDER-BOTTOM: #ffffff 0px;
	BACKGROUND-COLOR: #225a8d
}
.sapAppTitle
{
	PADDING-RIGHT: 5px;
	PADDING-LEFT: 5px;
	FONT-WEIGHT: bold;
	FONT-SIZE: medium;
	PADDING-BOTTOM: 1px;
	COLOR: #225a8d;
	PADDING-TOP: 1px;
	FONT-FAMILY: arial,sans-serif;
	BACKGROUND-COLOR: #ffffff
}
.sapAppBrandLeftLvl1
{
	BORDER-RIGHT: #ffffff 1px solid;
	BORDER-TOP: #ffffff 0px;
	FONT-SIZE: 1px;
	BORDER-LEFT: #ffffff 0px;
	BORDER-BOTTOM: #ffffff 1px solid;
	HEIGHT: 1px;
	BACKGROUND-COLOR: #225a8d
}
.sapAppBrandCenterLvl1
{
	BORDER-RIGHT: #ffffff 0px;
	BORDER-TOP: #ffffff 0px;
	FONT-SIZE: 1px;
	BORDER-LEFT: #ffffff 0px;
	BORDER-BOTTOM: #ffffff 1px solid;
	HEIGHT: 1px;
	BACKGROUND-COLOR: #225a8d
}
		</style>
		<hbj:form id="form">
		<%
			com.sap.mw.jco.JCO.Table table= new com.sap.mw.jco.JCO.Table("APP_CONF");
			com.sapportals.htmlb.table.JCOTableViewModel tableModel = new com.sapportals.htmlb.table.JCOTableViewModel( table, false);
				table.addInfo("APPCONF",29,0);
				table.addInfo("APPCONFLINK",29,0);
				table.addInfo("DESCRIPTION",29,0);
				table.addInfo("HIGHAV",29,0);
				table.addInfo("HIGHAVLINK",29,0);
				table.addInfo("SESTRACE",29,0);
				table.addInfo("SESTRACELINK",29,0);
				table.addInfo("SESTRACETOOLTIP",29,0);
			Vector v = new Vector(ScenarioManager.getScenarioManager().getScenarioNames());
			Collections.sort(v);

			Iterator iterator = v.iterator();
			StringBuffer url = new StringBuffer(WebUtil.getAppsURL(pageContext , 
							null , applicationURL , null , null , false));
			url.append("?");
			url.append(Constants.XCM_SCENARIO_RP);
			url.append("=");
            
			StringBuffer highAvUrlBuffer = new StringBuffer(WebUtil.getAppsURL(pageContext , 
							null , highavURL , null , null , false));
                            
			highAvUrlBuffer.append("?");
			highAvUrlBuffer.append(Constants.XCM_SCENARIO_RP);
			highAvUrlBuffer.append("=");
                        
			ScenarioConfig config1 = null;
			String name;
			int i = 0;
			pageContext.setAttribute("tableModel", tableModel );
			while(iterator.hasNext())  {
					name = (String)iterator.next();
					if(ScenarioManager.getScenarioManager().isSAPScenario(name))
						continue;
					config1 = ScenarioManager.getScenarioManager().getScenarioConfig(name);
					table.appendRow();
					table.setValue(name, 0);
					table.setValue(url.toString() + URLEncoder.encode(name),1);
					table.setValue( config1.getShorttext(), 2);
						table.setValue( highAvText, 3);
						table.setValue( highAvUrlBuffer.toString() + URLEncoder.encode(name), 4);
						table.setValue( sessiontraceText, 5);
						table.setValue( url.toString() + URLEncoder.encode(name) + "&appinfo=true", 6);

						String param[]=new String[1];
						param[0]=name;
						String tooltipText=  WebUtil.translate(pageContext, "start.tooltip.sess.trace", param);
						table.setValue(tooltipText, 7);
					i++;
				}
				if(showStartJsp && i >0 )
				{
						TableColumn conf= tableModel.addColumn("APPCONF");
						conf.setType(TableColumnType.LINK);
						conf.setTitle("Application Configuration");
						conf.setLinkColumnKey("APPCONFLINK");

						conf= tableModel.addColumn("SESTRACE");
						conf.setType(TableColumnType.LINK);
						conf.setTitle("");
						conf.setLinkColumnKey("SESTRACELINK");
						conf.setTooltipColumnKey("SESTRACETOOLTIP");
						conf= tableModel.addColumn("DESCRIPTION");
						conf.setTitle("Description");

						conf= tableModel.addColumn("HIGHAV");
						conf.setType(TableColumnType.LINK);
						conf.setTitle("");
						conf.setLinkColumnKey("HIGHAVLINK");
			%>
		  <hbj:tableView  id="myTableView1"
						  design="ALTERNATING"
						  model="tableModel"
						  headerVisible="true"
						  footerVisible="false"
						  fillUpEmptyRows="true"
						  navigationMode="BYLINE"
						  selectionMode="NONE"
						  headerText="<%=applicationName%>"
						  onNavigate="myOnNavigate"
						  visibleFirstRow="1"
						  rowCount="1"
						  width="800 px"
		  />
		  <%    }else 
				{ 
				if(showStartJsp)
				{
				%>
				 <p>
				   <%=errorMessage %>
				 </p>
				<%
				}else
				{
				%>
				<p>
				   <%=errorMessage2 %>
				</p>                
				<%
				}
				} %>
		  <% 
				if(showStartJsp)
				{
				%>
	  <br>
					<hbj:link id="link2" reference="<%=urlAdmin%>" tooltip="<%=applicationAdmin%>" text="<%=applicationAdmin%>"/>
	  </br>
	   <%}
	   %>      
		</hbj:form>
	</hbj:page>
</hbj:content>
