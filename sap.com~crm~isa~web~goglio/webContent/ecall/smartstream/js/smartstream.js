var callbackID;

//defines an object to store name, value pairs of a property
function Property(name, value){
	this.name = name;
	this.value = value;
}

/*
   Arguments are an array where args[i] is name and args[i+1] is value
*/
function ServerEvent(args) {
   // Member variable setup; the Map stores the N/V pairs
   this.map = new Array();
   this.len = 0;
		//member function assignment
   this.getSubject = getSubject
   this.get = getValue

   // Put the arguments' name/value pairs in the Map
   for (var i=0; i < args.length; i++) {
     this.map[this.len++]=new Property(args[i], args[++i]);
   }
}

//return the value corresponding to the name passed as arg
function getValue(name){
   for (var i=0; i < this.len; i++) {
   	if (this.map[i].name == name) {
   	  return this.map[i].value;
   	}
   }
   return '';
}

//return the subject of the server event
function getSubject(){
	return this.get('subject');
}

//method to handle the notification sent by the server
function notify(){
      var serverEvent = new ServerEvent(notify.arguments);
      myHandler(serverEvent); //call the implementation specific to the page which is subscribing to smartlets
}

function callback(time){
      callbackID = setTimeout("refreshSubscriptionFrame()",time);
}

function refreshSubscriptionFrame(){
	top.subscriberFrame.location = top.subscriberFrame.location; //refresh subscription frame
			//check for portability
	clearTimeout(callbackID);
}
