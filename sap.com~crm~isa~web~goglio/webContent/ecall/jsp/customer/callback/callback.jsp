<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.cic.comm.call.logic.CCallType" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.cic.core.context.CContext" %>
<%@ page import="com.sap.isa.cic.customer.sos.SessionObjectManager"%>
<%@ page import="java.net.InetAddress" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import= "com.sap.isa.core.FrameworkConfigManager" %>

<% BaseUI ui = new BaseUI(pageContext); %>


<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<head>
	<title><isa:translate key="cic.callback.title"/></title>
	<isa:includes/>

    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
	          type="text/javascript">
    </script>
</head>
<%
    String phoneValidationKey = (String)session.getAttribute("phoneValidation");
	String ipValidationKey = (String)session.getAttribute("ipAddressValidation");
	boolean isSecure = "true".equals(FrameworkConfigManager.XCM.getApplicationScopeConfig().getComponentConfig("lwc", "lwcconfig").getAllParams().getProperty("securemode"));
%>


<%
	String channelType = request.getParameter("interactionType");
	CCallType callType = (CCallType) session.getAttribute("channelType");
	boolean voip=false;
	if((callType!=null) && (callType == CCallType.VOIP))
		voip = true;
	if((channelType!=null) && channelType.equalsIgnoreCase(CCallType.VOIP.toString()))
		voip = true;
	// add special case for IP address
	String remoteAddr = "";
    	String phoneNumber = "";
	boolean isProxy = false;
	try {
		remoteAddr = request.getRemoteAddr();
		if (remoteAddr != null){
		      InetAddress hostIp = InetAddress.getLocalHost();
			String hostAddr = hostIp.getHostAddress();
			if (remoteAddr.equalsIgnoreCase("127.0.0.1")){
				  isProxy = true;
			}
			if (hostAddr.equalsIgnoreCase("127.0.0.1")){
				 isProxy = true;
			}
		}
    		SessionObjectManager som = (SessionObjectManager)
			session.getAttribute(SessionObjectManager.SO_MANAGER);

    		if(som == null){
			 //forward to session expired page
	 		return;
    		}
    		CContext context = som.getContext();
    		if (context != null ) {
			phoneNumber = context.getPhoneNumber();
    		}
		if (phoneNumber == null) phoneNumber = "";
	}catch (Exception ex) {

	}

%>

<body class="help">
<div class="module-name"><isa:moduleName name="ecall/jsp/customer/callback/callback.jsp" /></div>
  <% if (isSecure){
  %>
	<isa:translate key="cic.cic.voip.disabled"/>
  <%
  	}
  	else{
  %>
  <form action="<isa:webappsURL name="ecall/callBack.do" />" name="callbackForm" >



  <table cellpadding="0" cellspacing="0" border="0">

         <% if(phoneValidationKey!=null ||  ipValidationKey!=null) { %>
                 <%  if (ui.isAccessible()) { %>
                 <tr>
                 <td>
		 <a id="messages" href="#messages-list-start" accesskey="<isa:translate key="access.messages.key"/>"
			      title="<isa:translate key="access.messages.header" arg0="1"/>">
				<isa:translate key="access.messages.header" arg0="1"/>
		 </a>
		 <br>
		 <a class= "messageText" name="messages-list-start" href="#messages-list-end"
			      title="<isa:translate key="access.messages.start" arg0="1"/>">
			      <isa:translate key="access.messages.start" arg0="1"/>
		 </a>
		 </td>
		 </tr>
		 <% } %>
		 <% if(phoneValidationKey!=null){ %>
		      <tr>
		        <%  if (ui.isAccessible()) { %>
			<td colspan="2"><a href="javascript:document.callbackForm.phone.focus(); " id="err">
			  <font color = "red"><isa:translate key="cic.callback.phone.validation"/></font></a></TD>
			 <% } else { %>
			 <td colspan="2"><font color = "red"><isa:translate key="cic.callback.phone.validation"/></font></a></TD>
			  <% } %>
		      </tr>

		      <% session.removeAttribute("phoneValidation");
		 }%>



		 <%if(ipValidationKey!=null){ %>
		      <tr>
		        <%  if (ui.isAccessible()) { %>
			   <td colspan="2"><a href="javascript:document.callbackForm.ip.focus(); " id="err">
			   <font color = "red"><isa:translate key="cic.callback.ipAddressError"/></font></A></TD>
			<% } else { %>
			    <td colspan="2"><font color = "red"><isa:translate key="cic.callback.ipAddressError"/></font></A></TD>
			 <% } %>
		      </tr>

		      <% session.removeAttribute("ipAddressValidation");
  	 	 }%>

  	 	 <%  if (ui.isAccessible()) { %>
  	 	 <tr>
		        <td>
			<a class= "messageText" id="messages-list-end" name="messages-list-end"
			       href="#messages-list-start" title="<isa:translate key="access.messages.end"/>">
			      <isa:translate key="access.messages.end"/>
			</a>
			</td>
       		 </tr>
       		 <% } %>

	  <%}%>


	<tr>
		<td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1" height="30" border="0" alt=""></td>
	</tr>
	   <% if (!voip) {	%>
			<tr class="bodylight"><td><h3><isa:translate key="cic.callback.optionsLabel"/></h3></td></tr>
			<tr><td colspan="2" class="CONTENT"><isa:translate key="cic.callback.heading"/></td></tr>
	   <% } else { %>
			<tr class="bodylight"><td><h3><isa:translate key="cic.callback.voipLabel"/></h3></td></tr>
			<tr><td colspan="2" class="CONTENT"><isa:translate key="cic.callback.phoneMessage"/></td></tr>
	   <% }
	      if (isProxy && voip) {
	   %>
			<tr><td colspan="2" class="CONTENT"><isa:translate key="cic.callback.voipProxyError"/></td></tr>
	   <% } %>


	<tr>
		<td background="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/pixblack.gif") %>"
		colspan="2"><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>"
		width="100%" height="20" border="0" alt=""></td>
	</tr>

	<tr>
		<td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>"
		width="1" height="20" border="0" alt=""></td>
	</tr>

	<tr>
	    <td align="right"><label for="sub"><isa:translate key="cic.support.type.subject"/>&nbsp;</label></td>
	    <td align="left"><jsp:include page="/ecall/jsp/customer/supporttype.jsp" /></td>
	</tr>

	<tr>
		<%
		//////////////////////////Added to show different labels for callback and VoIP
		if(!voip) { %>
			<td align="right" class="CONTENT"><label for="phone">
			    <isa:translate key="cic.callback.phonenumber"/>&nbsp;</label></td>
		<% } else { %>
			<td align="right" class="CONTENT"><label for="ip">
			   <isa:translate key="cic.prompt.voip.IPAddress"/>&nbsp;</label></td>
		<% }
		   ////////////////////////End the label change///////
		 %>

		<td align="left">
		   <% if(voip) { %>
			<input type="text" name="ipAddress" id="ip" class="submitDoc" size="31" value="<%=JspUtil.encodeHtml(remoteAddr) %>">
		   <% }	else {	%>
			<input type="text" name="phoneNumber" id="phone" class="submitDoc" size="31" value="<%= JspUtil.encodeHtml(phoneNumber) %>">
		   <% } %>
		</td>
	</tr>


	<tr>
		<td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1" height="20" border="0" alt=""></td>
	</tr>

	<!-- If this is just a VOIP, the time will be always ASAP, so we don't show any options	-->


	<tr>
		<td align="right" class="CONTENT"></td>
		<td><input type="hidden" name="timeAsap" value="true" /></td>
		<% if(voip) { %>
			<td><input type="hidden" name="channelType" value="VOIP" /></td>
		<% } else { %>
			<td><input type="hidden" name="channelType" value="PHONE" /></td>
		<% } %>
	</tr>
	<tr>
		<td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1" height="20" border="0" alt=""></td>
	</tr>
	<tr>
		<td align="right" valign="top" class="CONTENT"><label for="quest"><isa:translate key="cic.callback.questionLabel"/>&nbsp;</label></td>
		<td>
		<TEXTAREA name="question"  cols="40" rows="4"  class="submitDoc" id="quest"></TEXTAREA>
	</td>
	</tr>
	<tr>
		<td colspan="2"><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1" height="5" border="0" alt=""></TD>
	</tr>
	<tr>
		<td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1" height="1" border="0" alt=""></td>
		<td>&nbsp;

		<%
			String sendBtnTxt = WebUtil.translate(pageContext, "cic.button.send", null);
			String resetBtnTxt = WebUtil.translate(pageContext, "cic.button.reset", null);
			String exitBtnTxt = WebUtil.translate(pageContext, "cic.button.exit", null);
            if(ui.isAccessible()) {
		%>

				<input type="submit" name="actionSend" class="green"
					value="<isa:translate key="cic.button.send"  />"
					title="<isa:translate key="access.button" arg0="<%=sendBtnTxt%>" arg1=""/>">

				 &nbsp;
					<input type="reset" class="green"
					value="<isa:translate key="cic.button.reset" />"
					title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>">

				&nbsp;
					<input type="submit" name="actionExit" class="green"
					value="<isa:translate key="cic.button.exit"  />"
					title="<isa:translate key="access.button" arg0="<%=exitBtnTxt%>" arg1=""/>">
		<%
		    } else {	// ui is not accessible
		%>
				<input type="submit" name="actionSend" class="green"
					value="<isa:translate key="cic.button.send"  />">

				 &nbsp;
					<input type="reset" class="green"
					value="<isa:translate key="cic.button.reset" />">

				&nbsp;
					<input type="submit" name="actionExit" class="green"
					value="<isa:translate key="cic.button.exit"  />">
		<%
			}
		%>
		</td>
	</tr>
</table>
	<script type="text/javascript">
	 <% if(phoneValidationKey!=null ||  ipValidationKey!=null) {
	            if (ui.isAccessible()) { %>
			    	getElement('messages').focus();
	 <%         }
	 } else { %>
		    document.callbackForm.SupportType.focus();
	 <% } %>
	 </script>
</form>

	<% } //end isSecure
	%>
</body>
</html>


