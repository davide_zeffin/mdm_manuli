<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>


<%@ page import="org.apache.struts.action.Action" %>
<%@ page import="org.apache.struts.util.MessageResources" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import= "com.sap.isa.core.FrameworkConfigManager" %>

<% 
	BaseUI ui = new BaseUI(pageContext); 
	boolean isSecure = "true".equals(FrameworkConfigManager.XCM.getApplicationScopeConfig().getComponentConfig("lwc", "lwcconfig").getAllParams().getProperty("securemode"));
%>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
	<!-- cic:base/ -->
	<%

    MessageResources resources =(MessageResources)
                                 application.getAttribute(Action.MESSAGES_KEY);


	String indexLabel = resources.getMessage("cic.prompt.indexpage.label");

	String indexpage= WebUtil.getAppsURL(pageContext, new Boolean(false), "ecall/customer/index.do","","",false);	//request.getContextPath() + "/ecall/customer/index.do";
	String status = request.getParameter("status");
	if(!status.equals("voip"))
	{
	%>
		<META http-equiv="refresh" content="40; URL=<%= indexpage %>" >
	<%
	}
	else
	{ //load netmeeting
	%>

		<% if (!isSecure ) {%>
			<object WIDTH="100%" HEIGHT="100%" ID="NetMeeting" CLASSID="CLSID:3E9BAF2D-7A79-11d2-9334-0000F875AE17">
		        <PARAM NAME="MODE" VALUE="telephone">
      		</object>
      	<% } %>
	<%
	} //end else
	%>
	<isa:includes/>
	<script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
		          type="text/javascript">
    </script>

	</head>



	<%
	if(status.equals("success"))
	{
		%>
		<body class="help"  leftmargin="0" topmargin="0" marginheight="0" marginwidth="0"
			    onload=javascript:getElement("back").focus()>
		<div class="module-name"><isa:moduleName name="ecall/jsp/customer/callback/callbackStatus.jsp" /></div>
		<a href=<%= indexpage %> id="back" ><%=indexLabel %></a>
		<br>
		        <isa:translate key="cic.prompt.callback.success"/>
		        <form action="<isa:webappsURL name="ecall/callBack/dropRequest.do" />"
                      name="dropRequestForm" >

					<%
						String dropBtnTxt = WebUtil.translate(pageContext, "cic.prompt.droprequest", null);
						if(ui.isAccessible()) {
					%>
							<input type="submit" name="dropRequest" class="green"
								   value="<isa:translate key="cic.prompt.droprequest"/>"
								   title="<isa:translate key="access.button" arg0="<%=dropBtnTxt%>" arg1=""/>">
					<%
						} else {
					%>
							<input type="submit" name="dropRequest" class="green"
									value="<isa:translate key="cic.prompt.droprequest"/>">
					<%
						}
					%>
                </form>
		<%
	}
	else if(status.equals("failure"))
	{
		%>
		<body class="help"  leftmargin="0" topmargin="0" marginheight="0" marginwidth="0"
			    onload=javascript:getElement("back").focus()>
		<div class="module-name"><isa:moduleName name="ecall/jsp/customer/callback/callbackStatus.jsp" /></div>
		<a href=<%= indexpage %> id="back" ><%=indexLabel %></a>
		<br>
			<isa:translate key="cic.prompt.callback.failure"/>


		<%
	}
	else{

		%>
		<body class="help"  leftmargin="0" topmargin="0" marginheight="0" marginwidth="0"
			    onload=javascript:document.dropRequestForm.dropRequest.focus()>
		<div class="module-name"><isa:moduleName name="ecall/jsp/customer/callback/callbackStatus.jsp" /></div>
		<isa:translate key="cic.prompt.netmeeting.loading"/>


		<form action="<isa:webappsURL name="ecall/callBack/dropRequest.do" />"
			  name="dropRequestForm" >

		<%
			String dropBtnTxt = WebUtil.translate(pageContext, "cic.prompt.droprequest", null);
			if(ui.isAccessible()) {
		%>
				<input type="submit" name="dropRequest" class="green"
					   value="<isa:translate key="cic.prompt.droprequest"/>"
					   title="<isa:translate key="access.button" arg0="<%=dropBtnTxt%>" arg1=""/>">
		<%
			} else {
		%>
				<input type="submit" name="dropRequest" class="green"
						value="<isa:translate key="cic.prompt.droprequest"/>">
		<%
			}
		%>


		</form>
		<%
	}

	%>
	</body>
</html>