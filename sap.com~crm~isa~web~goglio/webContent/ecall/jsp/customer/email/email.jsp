
<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa"  %>

<%@ page import="com.sap.isa.cic.core.context.CContext" %>
<%@ page import="com.sap.isa.cic.customer.sos.SessionObjectManager"%>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.cic.core.context.CContext" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType />
<%
    String emailAdd = "";
    SessionObjectManager som = (SessionObjectManager)
			session.getAttribute(SessionObjectManager.SO_MANAGER);

    	if(som == null){
		som = new SessionObjectManager();
		session.setAttribute(SessionObjectManager.SO_MANAGER , som );
		//forward to session expired page
	 	//return;
    	}
    CContext context = som.getContext();
    if (context != null ) {
	emailAdd = context.getEmail();
    }
    if(emailAdd == null)
	emailAdd = "";

%>
  <div class="module-name"><isa:moduleName name="ecall/jsp/customer/email/email.jsp" /></div>
  <table width="100%">
    <tr>
    	<td><img src="<%= WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1" height="30" alt=""/></td>
    </tr>

        <tr><td><h3><isa:translate key="cic.prompt.email.heading"/></h3></td></tr>
        <tr><td colspan="2" class="CONTENT"><isa:translate key="cic.prompt.index.mail.Message"/></td></tr>

      <tr><td background="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/pixblack.gif") %>" colspan="1"><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="100%" height="10" alt=""/></td></tr>
      <tr><td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1" height="20" alt=""/></td></tr>
      <tr><td>

        <form action="<isa:webappsURL name="ecall/email.do" />" name="emailForm">
        <table border="0" width="100%">
          <tr>
            <td align="right"><label for="fromEmail"><isa:translate key="cic.prompt.email.from"/></label></td>
            <td align="left">
              <input type="text" name="email" id="fromEmail" class="submitDoc" size="35" maxlength="75" value="<%= JspUtil.encodeHtml(emailAdd) %>"/>
            </td>
          </tr>
          <tr>
            <td align="right"><label for="ccEmail"><isa:translate key="cic.prompt.email.cc"/></label></td>
            <td align="left">
              <input type="text" id="ccEmail" name="CC" class="submitDoc" size="35" maxlength="75"/>
            </td>
          </tr>
          <tr>
            <td align="right"><label for="sub"><isa:translate key="cic.prompt.email.subject"/></label></td>
            <td align="left">
		<jsp:include page="/ecall/jsp/customer/supporttype.jsp" />

            </td>
          </tr>
          <tr>
            <td align="right"><label for="msg"><isa:translate key="cic.prompt.email.message"/></label></td>
            <td align="left">

              <table border="0" width="100%" class="green">
                <tr>
                  <td align="left">
                    <textarea name="emailMessage"  id="msg" rows="10" cols="50"> </textarea>
                  </td>
                </tr>
                <tr>
                  <td width="200" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left">
                   <%
                   		String sendBtnTxt = WebUtil.translate(pageContext, "cic.button.send", null);
						String resetBtnTxt = WebUtil.translate(pageContext, "cic.button.reset", null);
						String exitBtnTxt = WebUtil.translate(pageContext, "cic.button.exit", null);
                   %>
                   <% if (ui.isAccessible()) { %>
                    	<input type="submit" name="actionSend" class="green"
							value="<isa:translate key="cic.button.send"  />"
							title="<isa:translate key="access.button" arg0="<%=sendBtnTxt%>" arg1=""/>">

						 &nbsp;
							<input type="reset" class="green"
							value="<isa:translate key="cic.button.reset" />"
							title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>">

						&nbsp;
							<input type="submit" name="actionExit" class="green"
							value="<isa:translate key="cic.button.exit"  />"
							title="<isa:translate key="access.button" arg0="<%=exitBtnTxt%>" arg1=""/>">
                   <% } else { %>
						<input type="submit" name="actionSend" class="green"
						  value="<isa:translate key="cic.button.send" />"/>
						<input type="reset" class="green"
						  value="<isa:translate key="cic.button.reset" />"/>
						<input type="submit" name="actionExit" class="green"
						  value="<isa:translate key="cic.button.exit" />"/>
                   <% } %>
                  </td>
                </tr>
              </table>

            </td>
          </tr>
        </table>
        </form>

      </td>
    </tr>
  </table>
