<%@ page language="java" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.cic.util.MailStatus" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
	<title><isa:translate key="cic.customer.emailAckTitle"/></title>
	<isa:includes/>
</head>
<jsp:useBean id="status" scope="request" class="com.sap.isa.cic.util.MailStatus" />

<BODY class="help" text="#172972" link="#172972"  alink="#8e236b" vlink="#808080" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
	<div class="module-name"><isa:moduleName name="ecall/jsp/customer/email/ackn.jsp" /></div>
	<table width=600>
	<tr>
		<td width=50 nowrap></td>
		<td>
		        <% if(status.getStatus()==MailStatus.SUCCESS ){%>
					<h3><isa:translate key="cic.customer.emailSucessText"/></h3>
				<% }else { %>
					<h3><isa:translate key="cic.customer.emailFailText"/></h3>
				<% } %>

		</td>
	</tr>
	<tr>
		<td width=20 nowrap></td>
		<td><font size="-1">
			<a href="<isa:webappsURL name="ecall/customer/index.do"/>" > <isa:translate key="cic.prompt.indexpage.label"/></a>
		    </font></td>
	</tr>
	</table>
</body>
</html>
