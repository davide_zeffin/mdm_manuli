<%-- This is the common file for routing attributes definition
	 You can create/modify routing attribute support type from this file.
	 add/change the value about option value=" " and add/change the resource
	 key for the description of the drop-down box
	 March 11, 2002
  --%>

<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>


<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top">
			<select class="green" size="1" id="sub" name="SupportType">
				<option value='<isa:translate key="cic.support.type.general"/>'><isa:translate key="cic.support.type.general"/></option>
				<option value='<isa:translate key="cic.support.type.company.info"/>'><isa:translate key="cic.support.type.company.info"/></option>
				<option value='<isa:translate key="cic.support.type.product.catalog"/>'><isa:translate key="cic.support.type.product.catalog"/></option>
				<option value='<isa:translate key="cic.support.type.sales.order"/>'><isa:translate key="cic.support.type.sales.order"/></option>
				<option value='<isa:translate key="cic.support.type.shopping.basket"/>'><isa:translate key="cic.support.type.shopping.basket"/></option>
				<option value='<isa:translate key="cic.support.type.product.config"/>'><isa:translate key="cic.support.type.product.config"/></option>
				<option value='<isa:translate key="cic.support.type.quotation"/>'><isa:translate key="cic.support.type.quotation"/></option>
			</select>
		</td>
	</tr>
</table>
