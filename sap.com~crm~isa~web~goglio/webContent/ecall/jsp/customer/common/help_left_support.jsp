<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import= "java.util.Properties" %>
<%@ page import= "com.sap.isa.cic.core.init.LWCConfigProvider" %>
<%@ page import= "com.sap.isa.cic.comm.core.CommConstant" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext); %>

<jsp:useBean    id="channelsForm"
                scope="session"
                class="com.sap.isa.cic.customer.actionforms.ChannelsForm">
</jsp:useBean>

    <div class="module">
      <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/help_left_support.jsp" /></div>
      <ul>

      <%
      	// get all the lwc properties
	       Properties allProps = LWCConfigProvider.getInstance().getAllProps();
	       String icwcEnabled = allProps.getProperty(CommConstant.LWC_SPICE_ENABLED);
      %>

      <%  if (ui.isAccessible()) { %>
		<%
			if (channelsForm.isMail())
			{
	    %>
			     <li><a href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=email"/>"
			     title = "<isa:translate key="cic.prompt.index.mail"/>:<isa:translate key="cic.prompt.index.mail.Message"/>"
				accesskey="<isa:translate key="cic.accessKey.email"/>">
			     <isa:translate key="cic.customer.common.supportEMail"/></a></li>
	   <%
			}
		%>
<%-- GREENORANGE BEGIN --%>
	<%--				
			if (channelsForm.isChat())
			{
	  %>
	  			<li><a href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=chat"/>"
			    title = "<isa:translate key="cic.prompt.index.chat"/>:<isa:translate key="cic.prompt.index.chatMessage"/>"
				accesskey="<isa:translate key="cic.accessKey.chat"/>">
			    <isa:translate key="cic.customer.common.supportChat"/></a></li>
	  <%
			}
	  --%>
<%-- GREENORANGE END --%>	  
      <% } else { %>
		<%
			if (channelsForm.isMail())
			{
	    %>
			     <li><a href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=email"/>"
			     title = "<isa:translate key="cic.prompt.index.mail.Message"/>"
				accesskey="<isa:translate key="cic.accessKey.email"/>">
			     <isa:translate key="cic.customer.common.supportEMail"/></a></li>
		<%
			}
		%>
<%-- GREENORANGE BEGIN --%>
		<%--		
			if (channelsForm.isChat())
			{
		%>
			    <li><a href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=chat"/>"
			    title = "<isa:translate key="cic.prompt.index.chatMessage"/>"
				accesskey="<isa:translate key="cic.accessKey.chat"/>">
			    <isa:translate key="cic.customer.common.supportChat"/></a></li>
		<%
			}
		--%>
<%-- GREENORANGE END --%>
      <% } %>
      </ul>
    </div>

