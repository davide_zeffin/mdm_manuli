<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
	<title><isa:translate key="cic.customer.common.title"/></title>
	<isa:includes/>
</head>
  <body class="help">

    <a href="#" tabindex=0 accesskey="<isa:translate key="cic.accessKey.navigation"/>"></a>

    <div id="new-doc" class="module">
      <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/help_1.jsp" /></div>
      <div style="padding-left:15px;">
        <span tabindex=0><strong><isa:translate key="cic.customer.common.helpMessage"/></strong></span>
        <br><br>
        &nbsp;1. <a href="<isa:webappsURL name="ecall/jsp/customer/common/frameset_help.jsp?pageType=guided"/>"
           accessKey="<isa:translate key="cic.accessKey.guided"/>">
           <isa:translate key="cic.customer.common.tourText"/></a><br>
        &nbsp;2. <a href="<isa:webappsURL name="ecall/jsp/customer/common/frameset_help.jsp?pageType=faq"/>"
           accessKey="<isa:translate key="cic.accessKey.faq"/>">
           <isa:translate key="cic.customer.common.faqsText"/></a><br>
        &nbsp;3. <a href="<isa:webappsURL name="ecall/jsp/customer/common/frameset_help.jsp?pageType=glossary"/>"
           accessKey="<isa:translate key="cic.accessKey.glossary"/>">
           <isa:translate key="cic.customer.common.glossaryText"/></a><br>
        &nbsp;4. <a href="<isa:webappsURL name="ecall/jsp/customer/common/frameset_help.jsp?pageType=support"/>"
           accessKey="<isa:translate key="cic.accessKey.support"/>">
           <isa:translate key="cic.customer.common.supportText"/></a>
        <br><br><br><br>
      </div>
      <div align="right">
        <form action="#">
        <%
			String closeBtnTxt = WebUtil.translate(pageContext, "cic.button.close", null);
			if(ui.isAccessible()) {
		%>

			  <input type="Button" class="green" onclick="top.close();" value="<isa:translate key="cic.button.close" />"
				title="<isa:translate key="access.button" arg0="<%=closeBtnTxt%>" arg1=""/> " ><br><br>
		<%
			} else {
		%>
			  <input type="Button" class="green" onclick="top.close();" value="<isa:translate key="cic.button.close" />"><br><br>
		<%
			}
		%>
        </form>
      </div>
    </div>
  </body>
</html>