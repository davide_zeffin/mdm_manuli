<%@ page language="java" %>
<%@ page import="com.sap.isa.ui.uiclass.AccessKeyUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.GenericFactory" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ taglib uri="/isa" prefix="isa" %>

<%
	AccessKeyUI ui = (AccessKeyUI)GenericFactory.getInstance("accessKeyUI");
	if (ui == null)
		ui = new AccessKeyUI();
	ui.initContext(pageContext);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<isa:contentType />
  <head>
  	<isa:includes/>
  </head>
  <body class="help">
	<div id="accesskeys" class="module" style="position:relative;width:100%;height:100;overflow:auto">
		<div class="module-name"><isa:moduleName name="jsp/customer/common/help_accesskey.jsp" /></div>
		<br><isa:translate key="access.accesskey.note"/><br><br>
<%
	if (ui.isAccessible())
	{
%>
<a href="#accesskey-end"
	id="accesskey-start" 
	title= "<isa:translate key="accesskey.pagetitle"/>" 
	accesskey="<isa:translate key="accesskey.pagetitle.access"/>"
>
</a>
<%
	}
%>
<%
	request.setAttribute("accessKeyApps", ui.getAccessKeysForApplication());
	int tableIndex = 0;
%>
   <isa:iterate id="accesskeyapp"
                name="accessKeyApps"
                type="com.sap.isa.ui.accessibility.AccessKeysForApplication">
<%
	tableIndex++;
	int colno = 2;
	// Please change this based on the number of keys that you have
	int rowNo = 0;
	String columnNumber = Integer.toString(colno);
	String rowNumber = Integer.toString(rowNo);
	String allRows = Integer.toString(rowNo);
	String tableTitle = WebUtil.translate(pageContext, "accesskey.table", null);
	
	String tableBeginId = "accesskeytable" + Integer.toString(tableIndex) + "-begin";
	String tableEndId = "accesskeytable" + Integer.toString(tableIndex) + "-end";
	String tableBeginHref = "#" + tableBeginId;
	String tableEndHref = "#" + tableEndId;
%>
<%
	if (ui.isAccessible())
	{
%>
		<a href="<%=tableEndHref%>" id="<%=tableBeginId%>"
			title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="1" arg4="<%=allRows%>"/>">
		</a>
<%
	}
%>
<table border="0" cellspacing="0" cellpadding="3" width="100%">
	<tr>
		<td>
			<h3><isa:translate key="<%=accesskeyapp.getAppname()%>"/></h3>
		</td>
	</tr>
</table>
<table id="accesskeytable" title="<isa:translate key="<%=accesskeyapp.getAppname()%>"/>" summary="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="1" arg4="<%=allRows%>"/>" class="list" border="0" cellspacing="0" cellpadding="3" width="100%">
	<thead>
	  	<tr> 
		<th id="col_0" title="<isa:translate key="accesskey.table.key"/>" width="20%"><span><isa:translate key="accesskey.table.key"/></span></th>
		<th id="col_1" title="<isa:translate key="accesskey.table.key.represent"/>" width="80%"><span><isa:translate key="accesskey.table.key.represent"/></span></th>
		</tr> 
	</thead>
	<tbody>
		<%	request.setAttribute("accessKeyList", accesskeyapp.getAccesskeys());%>
	   <isa:iterate id="accesskeydef"
                name="accessKeyList"
                type="com.sap.isa.ui.accessibility.AccessKeyDefinition">
		<tr>
			<td headers="col_0" class="odd" width="20%">
				<span><isa:translate key="<%=accesskeydef.getAccesskey()%>"/></span>
			</td>
			<td headers="col_1" class="odd" width="20%">
				<span><isa:translate key="<%=accesskeydef.getAccesskeyrepresents()%>"/></span>
			</td>
	 	</tr>
		</isa:iterate>
	</tbody>
</table>
<%
	if (ui.isAccessible())
	{
%>
		<a id="<%=tableEndId%>" href="<%=tableBeginHref%>" title="<isa:translate key="access.table.end" arg0="accesskeytable"/>"></a>
<%
	}
%>
<br>
	</isa:iterate>

<%
	if (ui.isAccessible())
	{
%>
	<a href="#accesskey-start"
		id="accesskey-end"
	></a>        
<%
	}
%>
	</div>
</body>
</html>