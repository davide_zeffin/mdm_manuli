<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>

    <div class="module">
      <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/help_right_faq.jsp" /></div>
      <h2><isa:translate key="cic.customer.common.faqText"/></h2>
      <isa:translate key="cic.customer.common.faqChoose"/>
    </div>
