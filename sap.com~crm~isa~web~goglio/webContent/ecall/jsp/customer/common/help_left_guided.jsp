<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>

		<div class="module">
			<div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/help_left_guided.jsp" /></div>
			<ul>
				<li><isa:translate key="cic.customer.common.tourText.1"/></li>
				<li><isa:translate key="cic.customer.common.tourText.2"/></li>
				<li><isa:translate key="cic.customer.common.tourText.3"/></li>
				<li><isa:translate key="cic.customer.common.tourText.4"/></li>
			</ul>
		</div>
