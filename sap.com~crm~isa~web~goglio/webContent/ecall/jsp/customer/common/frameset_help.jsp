<%@ page language="java" %>
<%@ page pageEncoding="UTF8" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import= "java.util.*,javax.servlet.http.*" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>

<%

        //Set the pageContext to highlight the right link
    	String pageType = null;
    	String nonisascenario = application.getInitParameter("scenario.implementation.nonisa");
        if(nonisascenario != null && nonisascenario.equalsIgnoreCase("true")){
			//request.setAttribute("pageType","support");
			pageType="support";
		} else {
		    //request.setAttribute("pageType",pageType);
		    pageType=request.getParameter("pageType");

        }

%>


			<%if(pageType==null) {%>
				<jsp:include page="channelLayout.jsp?navigation=help_1" />
			<%} else if(pageType.equals("guided")) {%>
				<jsp:include page="channelLayout.jsp?navigation=left_guided&workarea=right_guided" />
			<%} else if(pageType.equals("faq")) {%>
				<jsp:include page="channelLayout.jsp?navigation=left_faq&workarea=right_faq" />
			<%} else if(pageType.equals("glossary")) {%>
				<jsp:include page="channelLayout.jsp?navigation=left_glossary&workarea=right_glossary" />
			<%}  else if(pageType.equals("support")) {%>
				<jsp:include page="/ecall/customer/index.do" />
			<%}%>

