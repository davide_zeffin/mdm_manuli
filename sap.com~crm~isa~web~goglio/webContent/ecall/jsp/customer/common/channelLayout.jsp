<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>
<%

    String navigation = null;
	String workarea = null;

    String pageType = request.getParameter("pageType");
    String header   = request.getParameter("header");
	String nav      = request.getParameter("navigation");
	String work     = request.getParameter("workarea");
	String portal	= request.getParameter("portalBaseUrl");

	//Set the request to highlight the right link on the header.
	request.setAttribute("pageType",pageType);
	
	
	if(header == null)
	    header = "help_header_no_select.jsp";
	    


	if(nav != null) {
		if(nav.equals("support")) {
		    navigation = "help_left_support.jsp";
		    if(pageType == null)
		       request.setAttribute("pageType","support");
		}
		else if(nav.equals("left_guided"))
		    navigation = "help_left_guided.jsp";
		else if(nav.equals("left_faq"))
		    navigation = "help_left_faq.jsp";
		else if(nav.equals("left_glossary"))
		    navigation = "help_left_glossary.jsp";
		else if(nav.equals("help_1"))
		    navigation = "help_1.jsp";
	}

	if(work != null) {
		if(work.equals("callback"))
		    workarea = "../callback/callback.jsp";
		else if(work.equals("callback_status"))
		    workarea = "../callback/callbackStatus.jsp";
		else if(work.equals("email"))
		    workarea = "../email/email.jsp";
		else if(work.equals("ackn"))
		    workarea = "../email/ackn.jsp";
		else if(work.equals("chatheader"))
		    workarea = "../chat/chatheader.jsp";
		else if(work.equals("chatrequest"))
		    workarea = "../chat/chatrequest.jsp";
		else if(work.equals("chatroom"))
		    workarea = "../chat/chatroom.jsp";
		else if(work.equals("messagearea"))
		    workarea = "../chat/messagearea.jsp";
		else if(work.equals("messageform"))
		    workarea = "../chat/messageform.jsp";
		else if(work.equals("upload"))
		    workarea = "../upload/upload.jsp";
		else if(work.equals("upload_status"))
		    workarea = "../upload/upload_status.jsp";
		else if(work.equals("index"))
		    workarea = "../index.jsp";
		else if(work.equals("downloaderror"))
		    workarea = "downloaderror.jsp";
		else if(work.equals("noagent"))
		    workarea = "noagent.jsp";
		else if(work.equals("userInfo"))
		    workarea = "../logon/userInfo.jsp";
		else if(work.equals("register"))
		    workarea = "../logon/register.jsp";
		else if(work.equals("right_guided"))
		    workarea = "help_guided.jsp";
		else if(work.equals("right_faq"))
		    workarea = "help_right_faq.jsp";
		else if(work.equals("right_glossary"))
		    workarea = "help_right_glossary.jsp";
		else if(work.equals("accesskey"))
			workarea = "help_accesskey.jsp";
		else if(work.equals("notsupported"))
		    workarea = "not_supported.jsp";
	}
%>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<title><isa:translate key="cic.customer.common.title"/></title>
	<isa:stylesheets/>
	<isa:includes/>
</head>

<body class="help">

  <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/channelLayout.jsp" /></div>
	<%if(portal ==null) {%>  	
		<table style="width: 100%; border-collapse: collapse;">
        	<tr>
           		<td style="padding: 0px;"><jsp:include page="<%=header%>"/></td>
        	</tr>
    	</table>
    <%}%>

    <div id="scroll">
    
        <table width="93%" border="0" >
		    <tr>
			        <%if(navigation !=null && portal ==null) {%>
			            <td><a href="#" tabindex="0" accesskey="<isa:translate key="cic.accessKey.navigation"/>"></a> </td>
				        <td><jsp:include page="<%=navigation%>" /></td>
			        <%}%>
			        <%if(workarea!=null) { %>
			            <td><a href="#" tabindex="0" accesskey="<isa:translate key="cic.accessKey.content"/>"></a> </td>
				        <td><jsp:include page="<%=workarea%>" flush="true" /></td>
			        <%}%>
            </tr>
        </table>
        
    </div> <%-- end scroll --%>
    
</body>
</html>
