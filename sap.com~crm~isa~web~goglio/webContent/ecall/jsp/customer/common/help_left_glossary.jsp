<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>

<isa:contentType />

    <div class="module">
      <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/help_left_glossary.jsp" /></div>
      <form action="">
        <table>
          <tbody>
            <tr>
              <td colspan="2"><isa:translate key="cic.customer.common.searchLbl"/></td>
            </tr>
            <tr>
              <td><input class="submitDoc" type="text" size="20"/></td>
              <td><input class="green" type="submit" name="search"
              value="<isa:translate key="cic.customer.common.searchBtn" />"
              title="<isa:translate key="cic.customer.common.searchBtn" />:<isa:translate key="cic.customer.common.searchLbl"/>"/></td>
            </tr>
            <tr>
              <td colspan="2"><hr/></td>
            </tr>
            <tr>
              <td colspan="2">
                <ul>
                  <li><a href="#common_agent"><isa:translate key="cic.customer.common.agent"/></a></li>
                  <li><a href="#common_alt"><isa:translate key="cic.customer.common.alt"/></a></li>
                  <li><a href="#common_contract"><isa:translate key="cic.customer.common.contract"/></a></li>
                  <li><a href="#common_credit"><isa:translate key="cic.customer.common.credit"/></a></li>
                  <li><a href="#common_delivery"><isa:translate key="cic.customer.common.delivery"/></a></li>
                  <li><a href="#common_payment"><isa:translate key="cic.customer.common.payment"/></a></li>
                  <li><a href="#common_voip"><isa:translate key="cic.customer.common.voip"/></a></li>
                  <li><a href="#common_invoice"><isa:translate key="cic.customer.common.invoice"/></a></li>
                  <li><a href="#common_order"><isa:translate key="cic.customer.common.order"/></a></li>
                  <li><a href="#common.ordersta"><isa:translate key="cic.customer.common.ordersta"/></a></li>
                  <li><a href="#common.template"><isa:translate key="cic.customer.common.template"/></a></li>
                  <li><a href="#common.catalog"><isa:translate key="cic.customer.common.catalog"/></a></li>
                  <li><a href="#common.quotation"><isa:translate key="cic.customer.common.quotation"/></a></li>
                  <li><a href="#common.related"><isa:translate key="cic.customer.common.related"/></a></li>
                  </ul>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
