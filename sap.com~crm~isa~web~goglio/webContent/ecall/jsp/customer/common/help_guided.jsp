<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>

    <div class="module">
      <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/help_guided.jsp" /></div>
      <isa:translate key="cic.customer.common.tourMessage"/>
      <ul>
        <li><isa:translate key="cic.customer.common.tourHowToBuy"/></li>
        <li><isa:translate key="cic.customer.common.tourHowToBid"/></li>
        <li>...</li>
      </ul>
    </div>
