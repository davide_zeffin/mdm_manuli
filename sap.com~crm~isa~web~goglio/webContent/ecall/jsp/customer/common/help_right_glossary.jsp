<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>
<isa:contentType />

    <div class="module">
      <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/help_right_glossary.jsp" /></div>

      <a name="common_agent"></a>
      <h3><isa:translate key="cic.customer.common.agent"/></h3>
      <p><isa:translate key="cic.customer.common.agent.1"/>
         <isa:translate key="cic.customer.common.agent.2"/>
      </p>
      <a name="common_alt"></a>
      <h3><isa:translate key="cic.customer.common.alt"/></h3>
      <p><isa:translate key="cic.customer.common.alt.1"/>
      </p>
      <a name="common_contract"></a>
      <h3><isa:translate key="cic.customer.common.contract"/></h3>
      <p><isa:translate key="cic.customer.common.contract.1"/>
      </p>
      <a name="common_credit"></a>
      <h3><isa:translate key="cic.customer.common.credit"/></h3>
      <p><isa:translate key="cic.customer.common.credit.1"/>
      </p>
      <a name="common_delivery"></a>
      <h3><isa:translate key="cic.customer.common.delivery"/></h3>
      <p><isa:translate key="cic.customer.common.delivery.1"/>
      </p>
      <a name="common_payment"></a>
      <h3><isa:translate key="cic.customer.common.payment"/></h3>
      <p><isa:translate key="cic.customer.common.payment.1"/>
      </p>
      <a name="common_voip"></a>
      <h3><isa:translate key="cic.customer.common.voip"/></h3>
      <p><isa:translate key="cic.customer.common.voip.1"/>
        <isa:translate key="cic.customer.common.voip.2"/>
        <isa:translate key="cic.customer.common.voip.3"/>
      </p>
      <a name="common_invoice"></a>
      <h3><isa:translate key="cic.customer.common.invoice"/></h3>
      <p><isa:translate key="cic.customer.common.invoice.1"/>
      </p>
      <a name="common_order"></a>
      <h3><isa:translate key="cic.customer.common.order"/></h3>
      <p><isa:translate key="cic.customer.common.order.1"/>
      </p>
      <a name="common.ordersta"></a>
      <h3><isa:translate key="cic.customer.common.ordersta"/></h3>
      <p><isa:translate key="cic.customer.common.ordersta.1"/>
        <isa:translate key="cic.customer.common.ordersta.2"/>
        <isa:translate key="cic.customer.common.ordersta.3"/>
        <isa:translate key="cic.customer.common.ordersta.4"/>
      </p>
      <a name="common.template"></a>
      <h3><isa:translate key="cic.customer.common.template"/></h3>
      <p><isa:translate key="cic.customer.common.template.1"/>
      </p>
      <a name="common.catalog"></a>
      <h3><isa:translate key="cic.customer.common.catalog"/></h3>
      <p><isa:translate key="cic.customer.common.catalog.1"/>
      </p>
      <a name="common.quotation"></a>
      <h3><isa:translate key="cic.customer.common.quotation"/></h3>
      <p><isa:translate key="cic.customer.common.quotation.1"/>
      </p>
      <a name="common.related"></a>
      <h3><isa:translate key="cic.customer.common.related"/></h3>
      <p><isa:translate key="cic.customer.common.related.1"/>
      </p>
    </div>
