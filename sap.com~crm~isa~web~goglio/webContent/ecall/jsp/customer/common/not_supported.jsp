<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
	<isa:includes/>
  </head>
    <body>
	  <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/not_supported.jsp" /></div>
       <font ="red"> <isa:translate key="cic.error.notsupported" /> </font>
    </body>
</head>