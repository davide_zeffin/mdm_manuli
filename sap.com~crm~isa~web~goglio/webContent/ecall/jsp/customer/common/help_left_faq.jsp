<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<% BaseUI ui = new BaseUI(pageContext);%>


    <div class="module">
      <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/help_left_faq.jsp" /></div>
      <form action="">
        <table>
            <tr>
              <td colspan="2"><isa:translate key="cic.customer.common.faqSearch"/></td>
            </tr>
            <tr>
              <td><input class="submitDoc" type="text" size="20"/></td>
              <td><input class="green" type="submit" name="search"
                   value="<isa:translate key="cic.customer.common.faqSearchBtn" />"
                <% if(ui.isAccessible()) {
                      String faqBtnTxt = WebUtil.translate(pageContext, "cic.customer.common.faqSearchBtn", null);
                %>
                   	  title="<isa:translate key="access.button" arg0="<%=faqBtnTxt%>" arg1=""/>"
                <% } %>
                /></td>
            </tr>
            <tr>
              <td colspan="2"><hr/></td>
            </tr>
            <tr>
              <td colspan="2">
                <ul style="width: 120px;">
                  <li><a href="#"><isa:translate key="cic.customer.common.faqDelItem"/></a></li>
                  <li><a href="#"><isa:translate key="cic.customer.common.faqHowCanI"/></a></li>
                </ul>
              </td>
            </tr>
        </table>
      </form>
    </div>
