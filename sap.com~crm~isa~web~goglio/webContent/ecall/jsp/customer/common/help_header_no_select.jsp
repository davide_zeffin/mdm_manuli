<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import= "com.sap.isa.core.ContextConst" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);%>





    <% String appName = application.getInitParameter(ContextConst.APP_NAME);   %>

  <!-- B2B -->
  <% if(appName.equalsIgnoreCase(ContextConst.ISA_SCENARIO_B2B)) { %>
		<% if(ui.isPortal) { %>
			<div id="header-portal">
		<% } else { %>
			<div id="header-appl">
		<% } %>
				<div class="header-logo"></div>
				<div class="header-applname">
					<isa:translate key="cic.prompt.index.heading"/>
				</div>
				<div class="header-username"></div>
				<div id="header-extradiv1">
					<span></span>
				</div>

				<div id="header-extradiv2">
					<span></span>
				</div>

				<div id="header-extradiv3">
					<span></span>
				</div>
				<div id="header-extradiv4">
					<span></span>
				</div>
				<div id="header-extradiv5">
					<span></span>
				</div>
				<div id="header-extradiv6">
					<span></span>
				</div>
			</div>

  <%} else if(appName.equalsIgnoreCase(ContextConst.ISA_SCENARIO_B2C)) { %>
      <!-- B2C -->
      <div id="header-appl">
        <div class="header-logo"></div>
        <div class="header-bar"></div>
      </div>
  <%}%>

 <%
	// if ICSS then do not show any links, only the customer support exits,
	// as per requirements of the ICSS for 50.

	String nonisascenario = application.getInitParameter("scenario.implementation.nonisa");
	if(nonisascenario == null || (!nonisascenario.equalsIgnoreCase("true"))) {

 %>

	  <!-- Decide which header to select -->
		<%

		   String guided="helpNavTab";
		   String faq="helpNavTab";
		   String glossary="helpNavTab";
		   String support="helpNavTab";
		   String accesskeyhelp="helpNavTab";

		   String pageType = (String) request.getAttribute("pageType");

		   if(pageType == null) {
			  // select nothing, do nothing
		   } else if(pageType.equals("guided")) {
			guided="actualHelpNavTab";
		   } else if(pageType.equals("faq")) {
			faq="actualHelpNavTab";
		   } else if(pageType.equals("glossary")) {
			glossary="actualHelpNavTab";
		   } else if(pageType.equals("support")) {
			support="actualHelpNavTab";
		   }
			else if(pageType.equals("accesskey")) {
			 accesskeyhelp="actualHelpNavTab";
			}
		%>


		<a href="#" tabindex="0" title= "<isa:translate key="cic.customer.common.helpTitle"/>"
				   accesskey="<isa:translate key="cic.accessKey.header"/>"></a>

			<table style="width: 100%; border-collapse: collapse;">
      		    <tr>
      		      <td class="<%=guided%>" align="center">
      		         <a href="<isa:webappsURL name="ecall/jsp/customer/common/frameset_help.jsp?pageType=guided"/>"
      		            accesskey="<isa:translate key="cic.accessKey.guided"/>" id="guided">
      		            <isa:translate key="cic.customer.common.tourText"/></a></td>
      		      <td class="<%=faq%>" align="center">
      		         <a href="<isa:webappsURL name="ecall/jsp/customer/common/frameset_help.jsp?pageType=faq"/>"
      		           accesskey="<isa:translate key="cic.accessKey.faq"/>" id="faq">
      		           <isa:translate key="cic.customer.common.faqsText"/></a></td>
      		      <td class="<%=glossary%>" align="center">
      		         <a href="<isa:webappsURL name="ecall/jsp/customer/common/frameset_help.jsp?pageType=glossary"/>"
      		           accesskey="<isa:translate key="cic.accessKey.glossary"/>" id="glossary">
      		           <isa:translate key="cic.customer.common.glossaryText"/></a></td>
      		      <td class="<%=support%>" align="center">
      		         <a href="<isa:webappsURL name="ecall/jsp/customer/common/frameset_help.jsp?pageType=support"/>"
      		           accesskey="<isa:translate key="cic.accessKey.support"/>" id="support">
      		           <isa:translate key="cic.customer.common.supportText"/></a></td>
				<%
				if(ui.isAccessible())
				{
				%>      		           
      		      <td class="<%=accesskeyhelp%>" align="center">
			    <a href="<isa:webappsURL name="/ecall/jsp/customer/common/channelLayout.jsp?workarea=accesskey&amp;pageType=accesskey"/>"
			    	title = "<isa:translate key="accesskey.pagetitle"/>">
					     <isa:translate key="accesskey.pagetitle"/></a>
      		          </td>
      		     <%
				}
      		     %>
      		    </tr>
        	</table>




<% } /* not ICSS */ %>






