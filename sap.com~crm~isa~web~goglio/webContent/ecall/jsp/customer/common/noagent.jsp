<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<% BaseUI ui = new BaseUI(pageContext);%>


<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<head>

	<title><isa:translate key="cic.customer.common.title"/></title>
	<isa:includes/>
	<script type="text/javascript">
		function setFocus() {
			document.getElementById("agent").focus();

		}
	</script>
</head>
  <body class="help" onload="setFocus()">
    <div class="module-name"><isa:moduleName name="ecall/jsp/customer/common/noagent.jsp" /></div>
    <div id="new-doc" class="module">
      <ul>
	  <A href="<isa:webappsURL name="ecall/customer/index.do"/>" id="agent"><isa:translate key="cic.customer.notify.NoAgent"/></A><br />
  	  <A href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=email"/>" ><isa:translate key="cic.customer.sendEmailMsg"/></A>
      </ul>
    </div>
  </body>
</html>