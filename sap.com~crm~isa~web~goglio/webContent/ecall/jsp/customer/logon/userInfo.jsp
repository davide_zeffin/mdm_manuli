<%-- import the taglibs used on this page --%>
<%@ taglib prefix="isa" uri="/isa" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%-- to be removed only for testing --%>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.isacore.action.b2c.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.businessobject.User" %>
<%@ page import="com.sap.isa.cic.customer.actions.DisplayInteractionAction" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%@ page import= "com.sap.isa.cic.core.init.LWCConfigProvider" %>
<%@ page import= "com.sap.isa.cic.comm.core.CommConstant" %>


<isa:contentType />

<%@ include file="/b2c/messages-declare.inc.jsp" %>

<% BaseUI ui = new BaseUI(pageContext);%>
<% String accessText = ""; %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

  <head>
    <title><isa:translate key="b2c.login.title"/></title>
    <isa:includes/>

    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
		          type="text/javascript">
    </script>

  </head>


  <body class="body-mid">
    <div class="module">
      <div class="module-name"><isa:moduleName name="/ecall/customer/logon/userInfo.jsp"/></div>
  	<% User user = (User) session.getAttribute(B2cConstants.USER); %>
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td>
		<!-- LOGIN reusing the b2c login Action in a new flow node by copying the JSP code -->

                    <form method="post" action="<isa:webappsURL name="/ecall/customer/interaction6.do" />" name="userExistsForm">

                        <% if (user != null){ %>
                            <table border="0" cellspacing="0" cellpadding="3" >
                               <%  if (ui.isAccessible()) { %>
				       <tr>
					<td>&nbsp;</td>
					<td colspan="4">
					   <a id="messages" href="#messages-list-start" accesskey="<isa:translate key="access.messages.key"/>"
					      title="<isa:translate key="access.messages.header" arg0="1"/>">
						<isa:translate key="access.messages.header" arg0="1"/>
					   </a>
					   <br>
					   <a class= "messageText" name="messages-list-start" href="#messages-list-end"
					      title="<isa:translate key="access.messages.start" arg0="1"/>">
					      <isa:translate key="access.messages.start" arg0="1"/>
					   </a>
					</td>
				       </tr>
				       <tr>
					<td>&nbsp;</td>
					<td colspan="4" >
					    <isa:message id="errortext" name="<%=B2cConstants.USER %>" type="<%=Message.ERROR%>">
					      <a href="javascript:document.userExistsForm.userId.focus()"><div class="error"> <%=errortext%><br> </div></a>
					    </isa:message>
					</td>
				       </tr>
				       <tr>
				       <td>&nbsp;</td>
				       <td colspan="4" >
					<a class= "messageText" id="messages-list-end" name="messages-list-end"
					       href="#messages-list-start" title="<isa:translate key="access.messages.end"/>">
					      <isa:translate key="access.messages.end"/>
					</a>
					</td>
				       </tr>
		      	       <% } else {%>
		      	       <tr>
					<td>&nbsp;</td>
					<td colspan="4" >
					    <isa:message id="errortext" name="<%=B2cConstants.USER %>" type="<%=Message.ERROR%>">
					      <div class="error"> <%=errortext%><br> </div></a>
					    </isa:message>
					</td>
				       </tr>
		      	       <% } %>
			       </table>
                      	 <% }%>
                      <table border="0" cellspacing="0" cellpadding="3" >
                      <tr class="emphasize">
			<td>
				<table  border="0" cellspacing="0" cellpadding="0" width="100%">
				  <tr>
				    <td width="23" >
				    	<% accessText = WebUtil.translate(pageContext, "b2c.login.login", null); %>
					<a
						href="#grp1End"
						id="grp1Begin"
						title="<isa:translate key="access.grp.begin" arg0="<%=accessText%>"/>"></a>
				    </td>
				  </tr>
				</table>
			</td>
				<td colspan="4"><span><strong><isa:translate key="b2c.login.alreadyregister"/></strong></span></td>
		      </tr>
                      <% if (user != null) { %>
                           <tr class="emphasize">
                             <td>&nbsp;</td>
                             <td><label for="email"><isa:translate key="b2c.login.email"/>&nbsp;</label></td>
                             <td><input class="textInput" id="email" type="Text" name="userId" size="40" value="<%= JspUtil.encodeHtml(JspUtil.removeNull(user.getUserId()))%>"></td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                           </tr>
                      <% }
                         else { %>
                           <tr class="emphasize">
                             <td>&nbsp;</td>
                             <td><label for="email"><isa:translate key="b2c.login.email"/>&nbsp;</label></td>
                             <td><input class="textInput" id="email" type="Text" name="userId" size="40"></td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                           </tr>
                      <% } %>
                      <tr class="emphasize">
                        <td>&nbsp;</td>
                        <td><label for="pwd"><isa:translate key="b2c.login.password"/>&nbsp;</label></td>
                        <td><input class="textInput" id="pwd" type="Password" name="password" size="25"></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr class="emphasize">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <%
							  String logonLinkTxt = WebUtil.translate(pageContext, "b2c.login.login", null);
							if(ui.isAccessible()) {
						%>
                        	<td><input type="Submit" value="<isa:translate key="b2c.login.login"/>" class="FancyButton"
                        	title="<isa:translate key="access.button" arg0="<%=logonLinkTxt%>" arg1=""/>">
                        	<br><br></td>
                        <% } else { %>
							<td><input type="Submit" value="<isa:translate key="b2c.login.login"/>" class="FancyButton">
						<% } %>
                        <td><img src="<%= WebUtil.getMimeURL(pageContext, "../mimes/images/spacer.gif") %>" alt="" width="50" height="1" border="0"></td>
                      </tr>


                    </form>
<!-- REGISTER -->
                    <tr>
		       <td colspan="4">
			 <% accessText = WebUtil.translate(pageContext, "b2c.login.login", null); %>
			<a
				href="#grp1Begin"
				id="grp1End"
				title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>"
			></a>

		       </td>
                     </tr>
                     <tr>
                        <td>&nbsp;</td>
                          <td colspan="4"><br>

				  <span><strong><isa:translate key="b2c.login.newuser"/>: </strong></span>&nbsp;
				  <a href="<isa:webappsURL name="ecall/customer/interaction10.do"/>"
				  title="<isa:translate key="b2c.login.register"/>:<isa:translate key="cic.prompt.index.registration"/>">
				  <isa:translate key="b2c.login.register"/></a>

			 </td>
                         <% session.removeAttribute(B2cConstants.USER); %>
                     </tr>

  <%
	// get all the lwc properties
	Properties allProps = LWCConfigProvider.getInstance().getAllProps();
	if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("no")) { %>

<!-- ALIAS -->
		     <tr class="emphasize">
		   		<td>
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
					  <tr>
					    <td width="23" >
					   	<% accessText = WebUtil.translate(pageContext, "cic.prompt.alias", null); %>
						<a
							href="#grp2End"
							id="grp2Begin"
							title="<isa:translate key="access.grp.begin" arg0="<%=accessText%>"/>"
						></a>
					    </td>
					   </tr>
					</table>
				</td>
				  <td colspan="4"><span><strong><isa:translate key="cic.prompt.alias"/></strong></span></td>
           	</tr>
            		<form method="post" action="<isa:webappsURL name="/ecall/customer/interaction8.do" />" >
			<tr class="emphasize">
				<td>&nbsp;</td>
				<td><label for="alias"><isa:translate key="cic.customer.logon.aliasLabel"/>&nbsp;</label></td>
				<td><input class="textInput" id="alias" type="Text" name="userId" size="40"></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr class="emphasize">
				<td>&nbsp;</td>
				<td><label for="fname"><isa:translate key="cic.customer.logon.firstNameLbl"/>&nbsp;</label></td>
				<td><input class="textInput" id="fname" type="Text" name="firstName" size="40"></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr class="emphasize">
				<td>&nbsp;</td>
				<td><label for="lname"><isa:translate key="cic.customer.logon.lastNameLabel"/>&nbsp;</label></td>
				<td><input class="textInput" id="lname" type="Text" name="lastName" size="40"></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr class="emphasize">
				<td>&nbsp;</td>
				<td><label for="emaillbl"><isa:translate key="cic.customer.logon.emailLabel"/>&nbsp;</label></td>
				<td><input class="textInput" id="emaillbl" type="Text" name="email" size="40"></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr class="emphasize">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<%
				   String aliasBtnTxt = WebUtil.translate(pageContext, "cic.customer.logon.aliasButton", null);
				   if (ui.isAccessible()) {
				%>
						<td><input type="Submit" name="actionSave" value="<isa:translate key="cic.customer.logon.aliasButton"/>"
						class="FancyButton"	title="<isa:translate key="access.button" arg0="<%=aliasBtnTxt%>" arg1=""/>"> <br><br></td>
				<% } else { %>
				        <td><input type="Submit" name="actionSave" value="<isa:translate key="cic.customer.logon.aliasButton"/>"
						class="FancyButton" > <br><br></td>
				<% } %>
				<td><img src="<%= WebUtil.getMimeURL(pageContext, "../mimes/images/spacer.gif") %>"
				alt="" width="50" height="1" border="0"></td>
			</tr>
			</form>
<!--ANONIMOUS-->
                	<tr>
			  <td colspan="4">
				 <% accessText = WebUtil.translate(pageContext, "cic.prompt.alias", null); %>
				<a
					href="#grp2Begin"
					id="grp2End"
					title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>"
				>
			 </td>
			</tr>

            		<form method="post" action="<isa:webappsURL name="/ecall/customer/interaction8.do" />" >

			<tr>
				<input type="hidden" name="userId" values="anonymous"></td>
				<input type="hidden" name="firstName" values=""></td>
				<input type="hidden" name="lastName" values=""></td>
				<input type="hidden" name="email" values=""></td>
				<%
				    String anonBtnTxt = WebUtil.translate(pageContext, "cic.customer.logon.anonymousLbl", null);
				    if (ui.isAccessible()) {
				%>
					<td><input type="Submit" name="actionSave" value="<isa:translate key="cic.customer.logon.anonymousLbl"/>"
					 class="FancyButton" title="<isa:translate key="access.button" arg0="<%=anonBtnTxt%>" arg1=""/>">
					 <br><br></td>
				<% } else { %>
				        <td><input type="Submit" name="actionSave" value="<isa:translate key="cic.customer.logon.anonymousLbl"/>"
					  class="FancyButton">
					 <br><br></td>
				<% } %>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<script type="text/javascript">
			 <% if(user != null) {
			           if (ui.isAccessible()) {  %>
				     		getElement('messages').focus();
			 <%        } %>
			 <% } else { %>
				    document.userExistsForm.userId.focus();
			 <% } %>
			 </script>
			</form>
<% } // if lwc_spice is not enabled  %>
                  </table>
                  <br><br>
            </td>
          </tr>
      </table>
    </div>
  </body>
</html>
