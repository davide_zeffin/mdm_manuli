<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.isacore.action.b2c.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.actionform.b2c.*" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%@ taglib uri="/isa" prefix="isa"  %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="/b2c/messages-declare.inc.jsp" %>

<%
BaseUI ui = null;
%>
<% ui = new BaseUI(pageContext);%>
<% String accessText = ""; %>



<% // Are there error messages? An existing messageList instance within the request context indicates
   // an error, otherwise it wouldn't be here.
   boolean isErrorOccurred = false;
   if (request.getAttribute(B2cConstants.MESSAGE_LIST) != null) {
       isErrorOccurred = true;
   }

   // The address data are contained within the address formular. This object should never be 'null'.
   AddressFormular addressFormular = (AddressFormular) request.getAttribute(B2cConstants.ADDRESS_FORMULAR);
   // flag that indicates whether the user represents a person or an organization
   boolean isPerson;

   // list of regions may be 'null'
   ResultData listOfRegions = (ResultData) request.getAttribute(B2cConstants.POSSIBLE_REGIONS);
   // list of counties may be 'null'
   ResultData listOfCounties = (ResultData) request.getAttribute(B2cConstants.POSSIBLE_COUNTIES);

   // the index variable will be used to fill a JavaScript array
   int index;
%>

<%
     UserSessionData userSessionData =
            UserSessionData.getUserSessionData(pageContext.getSession());

     BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
     Shop shop = bom.getShop();

     //are we in the brandowner scenario
     boolean brandOwnerScenario = false;
     //are we in the OCI scenario
     boolean basketForwarding = false;
%>
  <isacore:ifShopProperty property = "scenario" value ="CCH">
     <% brandOwnerScenario = true; %>
  </isacore:ifShopProperty>
  <isacore:ifShopProperty property = "ociBasketForwarding" value ="true">
     <% basketForwarding = true; %>
  </isacore:ifShopProperty>



<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

  <head>
    <title><isa:translate key="b2c.register.title"/></title>
    <isa:includes/>


	<script type="text/javascript">
	   <%@ include file="/b2c/jscript/addressdetails_maintenance.js" %>
	</script>

  </head>
  <body class="body-mid" style="background-image: url('<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/bg_bottom.gif") %>');">
    <div id="register" class="module">
      <div class="module-name"><isa:moduleName name="ecall/customer/logon/register.jsp"/></div>
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td width="8"><img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" alt="" width="8" height="1" border="0"></td>
            <td>

              <table class="navPath" border="0" cellspacing="0" cellpadding="0" width="100%">
                  <tr>
                    <td width="23" class="navPath">
                    <img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/dec_ecke_navPath.gif") %>" alt="" width="23" height="26" border="0">
			<% accessText = WebUtil.translate(pageContext, "b2c.register.heading", null); %>
			<a
				href="#grp1End"
				id="grp1Begin"
				title="<isa:translate key="access.grp.begin" arg0="<%=accessText%>"/>"
			></a>
                    </td>
                    <td width="2000" class="navPath"><span><isa:translate key="b2c.register.heading"/></span></td>
                  </tr>
              </table>
            </td>
          </tr>
          <% if ((brandOwnerScenario) && (!basketForwarding))  {
           	     String step = "4"; %>
          <tr>
            <td width="8"><img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" alt="" width="8" height="1" border="0"></td>
            <td width="100%">
               <br><%@ include file="/dealerlocator/step_by_step.inc.jsp" %><br>
            </td>
          </tr>
          <% } %>
          <tr>
            <td width="8"><img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" alt="" width="8" height="1" border="0"></td>
            <td>


             <form action="<isa:webappsURL name="ecall/customer/interaction11.do"/>" name="registration" method="post" >
              <isacore:requestSerial mode="POST"/>
              <table cellpadding="2" cellspacing="0" border="0">
                  <tr>
                    <td><img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" alt="" width="23" height="1" border="0"></td>
                    <td width="20%"><img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" alt="" width="1" height="1" border="0"></td>
                    <td width="40%"><img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" alt="" width="1" height="1" border="0"></td>
                    <td width="40%"><img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" alt="" width="1" height="1" border="0"></td>
                  </tr>
<!-- text -->
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">
                      <br>
                      <h4>
                        <% isPerson = addressFormular.isPerson();
                           if ( isPerson){%>
                             <span><isa:translate key="b2c.register.heading.person"/></span>
                        <% } else { %>
                             <span><isa:translate key="b2c.register.heading.firm"/></span>
                        <% } %>
                      </h4>
                    </td>
                  </tr>
<!--general error messages -->
                    <% if (isErrorOccurred){ %>
<%-- error handling --%>
<%-- print out a localized errors --%>
<%@ include file="/b2c/messages-begin.inc.jsp" %>
<isa:message id="errortext" name="<%=B2cConstants.MESSAGE_LIST %>" type="<%=Message.ERROR%>" >
<%
addMessage(Message.ERROR, errortext);
%>
</isa:message>
<%
if(!messages.isEmpty())
{
%>
                           <tr>
                             <td class="emphasize">&nbsp;</td>
                             <td class="emphasize" colspan="2">
							 	<%@ include file="/b2c/messages-end.inc.jsp" %>


                             </td>
                             <td class="emphasize">&nbsp;</td>
                           </tr>
<%
}
%>
                    <% }%>

<!-- title -->
                  <tr>
                    <td class="emphasize">&nbsp;</td>
                    <td class="emphasize">
						<label for="titleKey">
	                    	<isa:translate key="b2c.register.formOfAddress"/>
						</label>
                    </td>
                    <td class="emphasize">
                      <%
                         // The result of the following decision will be passed as a parameter of the
                         // 'send_tileChange' JavaScript function which is defined within the file
                         // 'addressdetails_maintenance.js'.
                         String fromTitle = "P";
                         if (!isPerson) {
                             fromTitle = "O";
                         } %>
                      <select name="titleKey" id="titleKey" onChange="send_titleChange('<%= fromTitle %>')" >
                        <% // build up the selection options
                           String defaultTitleKey =(String) addressFormular.getAddress().getTitleKey(); %>
                        <isa:iterate id="formsOfAddress" name="<%= B2cConstants.FORMS_OF_ADDRESS %>"
                                     type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
                          <% // Should the current title be marked as selected?
                             String selected="";
                             if  (formsOfAddress.getString("ID").equals(defaultTitleKey)) {
                                 selected="SELECTED";
                             } %>

                             <OPTION VALUE="<%= formsOfAddress.getString("ID") %>" <%= selected %>>
                               <%= JspUtil.encodeHtml(formsOfAddress.getString("DESCRIPTION")) %>
                             </OPTION>
                        </isa:iterate>
                      </select>
                      <script type="text/javascript">
                        <% index = 0; %>
                        <isa:iterate id="formsOfAddress" name="<%= B2cConstants.FORMS_OF_ADDRESS %>"
                                     type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
                        <% // fill a JavaScript array to remember which title key represents a person and
                           // which one represents an organization

                           String arrayValue = "P";
                           if (!formsOfAddress.getBoolean("FOR_PERSON")) {
                               arrayValue = "O";
                           } %>

                          toTitle[<%= index++ %>] = "<%= arrayValue %>";
                        </isa:iterate>
                      </script>
                    </td>
                    <td  class="emphasize">&nbsp;</td>
                  </tr>
                <%-- show errors concerning the title of the user --%>
                <% if (isErrorOccurred){ %>
<%-- error handling --%>
<%-- print out a localized errors --%>
<%@ include file="/b2c/messages-begin.inc.jsp" %>
<isa:message id="errortext" name="<%=B2cConstants.MESSAGE_LIST %>" type="<%=Message.ERROR%>" property="titleKey">
<%
addMessage(Message.ERROR, errortext);
%>
</isa:message>
<%
if(!messages.isEmpty())
{
%>
			   <tr>
			     <td class="emphasize">&nbsp;</td>
			     <td class="emphasize" colspan="2">
								<%@ include file="/b2c/messages-end.inc.jsp" %>
			     </td>
			     <td class="emphasize">&nbsp;</td>
			   </tr>
<%
}
%>
                <% }%>
<!-- include the specific address format -->
                  <% if (addressFormular.getAddressFormatId() == 0) { %>
                       <%@ include file="/b2c/inputaddressformat_default.inc" %>
                  <% }
                     else { %>
                       <%@ include file="/b2c/inputaddressformat_us.inc" %>
                  <% } %>

<!-- text -->
                  <tr>
                    <td >&nbsp;</td>
                    <td colspan="3"><br>
                      <span >
                      <isa:translate key="b2c.register.longtext"/>
                      <isa:translate key="b2c.register.longtext2"/>
                      <isa:translate key="b2c.register.longtext3"/>
                      </span>
                      <ul>
                      	<span><li><isa:translate key="b2c.register.longtext4"/></li></span>
                        <span><li><isa:translate key="b2c.register.recomendations"/></li></span>
                        <span><li><isa:translate key="b2c.register.ordertracking"/></li></span>
                        <span><li><isa:translate key="b2c.register.savedbaskets"/></li></span>
                      </ul><br>
                    </td>
                  </tr>
<!-- eMail -->
                  <tr>
                    <td class="emphasize">&nbsp;</td>
                    <td class="emphasize" nowrap>
						<label for="<%=SaveRegisterAction.PN_PASSWORD%>">
                    		<isa:translate key="b2c.register.email"/>
						</label>
                    </td>
                    <td class="emphasize"><input class="textInput" type="Text" size="80" name="email" id="email" value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getEMail()))%>" ></td>
                    <td class="emphasize">&nbsp;</td>
                  </tr>
          <%-- show errors concerning the e-mail information of the user/company --%>
          <% if (isErrorOccurred){ %>
<%-- error handling --%>
<%-- print out a localized errors --%>
<%@ include file="/b2c/messages-begin.inc.jsp" %>
<isa:message id="errortext" name="<%=B2cConstants.MESSAGE_LIST %>" type="<%=Message.ERROR%>" property="eMail">
<%
addMessage(Message.ERROR, errortext);
%>
</isa:message>
<%
if(!messages.isEmpty())
{
%>
                           <tr>
                             <td class="emphasize">&nbsp;</td>
                             <td class="emphasize" colspan="2">
							 	<%@ include file="/b2c/messages-end.inc.jsp" %>
                             </td>
                             <td class="emphasize">&nbsp;</td>
                           </tr>
<%
}
%>
          <% }%>
<!-- password -->
                  <tr>
                    <td class="emphasize">&nbsp;</td>
                    <td class="emphasize">
						<label for="<%=SaveRegisterAction.PN_PASSWORD%>">
                    		<isa:translate key="b2c.register.password"/>
						</label>
                    </td>
                    <td class="emphasize">
                      <input class="textInput" type="Password" size="25" name="<%=SaveRegisterAction.PN_PASSWORD%>"
                             value="<%= JspUtil.encodeHtml(JspUtil.removeNull(request.getAttribute(SaveRegisterAction.PN_PASSWORD))) %>">
                    </td>
                    <td class="emphasize">&nbsp;</td>
                  </tr>
          <%-- show errors concerning the password information of the user/company --%>
          <% if (isErrorOccurred){ %>
<%-- error handling --%>
<%-- print out a localized errors --%>
<%@ include file="/b2c/messages-begin.inc.jsp" %>
<isa:message id="errortext" name="<%=B2cConstants.MESSAGE_LIST %>" type="<%=Message.ERROR%>" property="password">
<%
addMessage(Message.ERROR, errortext);
%>
</isa:message>
<%
if(!messages.isEmpty())
{
%>
                           <tr>
                             <td class="emphasize">&nbsp;</td>
                             <td class="emphasize" colspan="2">
							 	<%@ include file="/b2c/messages-end.inc.jsp" %>
                             </td>
                             <td class="emphasize">&nbsp;</td>
                           </tr>
<%
}
%>
          <% }%>
<!-- passwordVerify -->
                  <tr>
                    <td class="emphasize">&nbsp;</td>

                    <td class="emphasize">
						<label for="<%=SaveRegisterAction.PN_PASSWORD_VERIFY%>">
		                    <isa:translate key="b2c.register.verifyPassword"/>
						</label>
                    </td>
                    <td class="emphasize">
                      <input class="textInput" type="Password" size="25" name="<%=SaveRegisterAction.PN_PASSWORD_VERIFY%>"
                             value="<%= JspUtil.encodeHtml(JspUtil.removeNull(request.getAttribute(SaveRegisterAction.PN_PASSWORD_VERIFY))) %>">
                    </td>
                    <td class="emphasize">&nbsp;</td>
                  </tr>
          <%-- show errors concerning the repeatPassword information of the user/company --%>
          <% if (isErrorOccurred){ %>
<%-- error handling --%>
<%-- print out a localized errors --%>
<%@ include file="/b2c/messages-begin.inc.jsp" %>
<isa:message id="errortext" name="<%=B2cConstants.MESSAGE_LIST %>" type="<%=Message.ERROR%>" property="passwordVerify">
<%
addMessage(Message.ERROR, errortext);
%>
</isa:message>
<%
if(!messages.isEmpty())
{
%>
                           <tr>
                             <td class="emphasize">&nbsp;</td>
                             <td class="emphasize" colspan="2">
							 	<%@ include file="/b2c/messages-end.inc.jsp" %>
                             </td>
                             <td class="emphasize">&nbsp;</td>
                           </tr>
<%
}
%>
          <% }%>
<!-- Buttons -->
                  <tr>
                    <td>&nbsp;
				<% accessText = WebUtil.translate(pageContext, "b2c.register.heading", null); %>
				<a
					href="#grp1Begin"
					id="grp1End"
					title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>"
				></a>
		        <a
		        	href="#" title= "<isa:translate key="b2c.register.jsp.btnarea"/>"
		        	accesskey="<isa:translate key="b2c.register.jsp.btnkey"/>"
		        ><img
		        	src="<%=WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" alt="<isa:translate key="b2c.register.jsp.btnarea"/>" border="0" width="1" height="1"
		        ></a>
                    </td>
                    <td colspan="2"><br>
                      <p align="right">
		        <%
		            String saveBtnTxt = WebUtil.translate(pageContext, "b2c.register.saveButton", null);
		            String cancelBtnTxt = WebUtil.translate(pageContext, "b2c.register.cancelButton", null);
		            String dataLinkTxt = WebUtil.translate(pageContext, "b2c.register.datainformation", null);
		            if(ui.isAccessible()) {
		        %>
                        <input type="Button" value="<isa:translate key="b2c.register.saveButton"/>"
                               title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>"
                               onclick="send_save()" class="FancyButton">
                        <input type="Button" value="<isa:translate key="b2c.register.cancelButton"/>"
                               title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>"
                               onclick="send_cancel()" class="FancyButton">&nbsp;&nbsp;&nbsp;</p><br><br>

                        <p><a href="#" title="<%=dataLinkTxt%>" onclick="window.open('<isa:webappsURL name="b2c/dataInformation.jsp"/>', 'Information', 'width=400,height=400,scrollbars=yes'); return false"><isa:translate key="b2c.register.datainformation"/></a></p>
                <%
					} else {
		        %>
		        		<input type="Button" value="<isa:translate key="b2c.register.saveButton"/>"
							   onclick="send_save()" class="FancyButton">
						<input type="Button" value="<isa:translate key="b2c.register.cancelButton"/>"
							   onclick="send_cancel()" class="FancyButton">&nbsp;&nbsp;&nbsp;</p><br><br>

                        <p><a href="#" onclick="window.open('<isa:webappsURL name="b2c/dataInformation.jsp"/>', 'Information', 'width=400,height=400,scrollbars=yes'); return false"><isa:translate key="b2c.register.datainformation"/></a></p>
		        <%
					}
		        %>
                    </td>
                    <td>&nbsp;</td>
              </table>
              <!-- hidden fields -->
              <input type="hidden" name="titleChange"   value="">
              <input type="hidden" name="countryChange"  value="">
              <input type="hidden" name="cancelClicked"  value="">

            </form>

            </td>
          </tr>
      </table>
    </div>
  </body>
</html>
