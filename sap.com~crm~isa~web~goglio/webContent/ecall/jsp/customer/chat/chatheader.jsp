<!-- start of chatheader.jsp -->
<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.cic.customer.sos.SessionObjectManager" %>
<%@ page import="com.sap.isa.cic.core.context.CContext" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  
    BaseUI ui = new BaseUI(pageContext); 
    SessionObjectManager cicCustomerBom = (SessionObjectManager )session.getAttribute(SessionObjectManager.SO_MANAGER);
	CContext contxt = cicCustomerBom.getContext();
%>

<div class="module-name"><isa:moduleName name="ecall/jsp/customer/chat/chatheader.jsp" /></div>

<table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr bordercolor="#999999">
    <td colspan="3">
		<div align="center">
		    <b><font face="Verdana, Arial, Helvetica, sans-serif" >
			    <span><isa:translate key="cic.prompt.welcome" /> <%= JspUtil.encodeHtml(contxt.getFirstName()) %></span>
		    </b></font>
		</div>
    </td>
  </tr>
</table>
<!-- end of chatheader.jsp -->