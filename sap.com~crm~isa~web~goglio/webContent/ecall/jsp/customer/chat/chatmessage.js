
function updateFormat(isbold,isitalic,isunderline,isanchor,colorstyle)
{
	setBold(isbold);
	setItalic(isitalic);
	setUnderline(isunderline);
	setAnchor(isanchor);
	selectColor(colorstyle);
	selectSize();
	selectFont();
}

function chatprint(url)
{
	if(window.print)
	{
		chatarea = window.open(url);
		setTimeout("chatarea.print();", 500);
	}
}


var bAnchored = false;
var bUnderlined = false;
var bBold = false;
var bItalic = false;

function toggle(e)
{
	if(! e)
		e = event;
		
	var curElement = e.target || event.srcElement;
	if(curElement.id == "bold")
	{
		setBold(!bBold);
	}
	if(curElement.id  == "underlined")
	{
		setUnderline(!bUnderlined);
	}
	if(curElement.id == "italic")
	{
		setItalic(!bItalic);

	}
	if(curElement.id == "Anchor")
	{
		setAnchor(!bAnchored);
	}
}

function showColors(e)
{
	if(! e)
		e = event;

	var colorDIV = getElement("colorTable");
	if(colorDIV)
	{
		colorDIV.style.left = e.clientX + "px";
		colorDIV.style.top = (e.clientY + 12) + "px";
		colorDIV.style.display = "block";
	}

}



function setBold(flag)
{
	var element = getElement("bold");
	var messageInput = getElement("message");
	if (flag)
	{
		element.style.backgroundColor = "#EEE8F2";
		messageInput.style.fontWeight = "bold";
	}
	else
	{
		element.style.backgroundColor = "";
		messageInput.style.fontWeight = "";
	}
	bBold = flag;
	var boldStyle = getElement("boldStyle");
	if(boldStyle != null)
		boldStyle.value = bBold;
}

function setUnderline(flag)
{
	var element = getElement("underlined");
	var messageInput = getElement("message");
	if (flag)
	{
		element.style.backgroundColor = "#EEE8F2";
		messageInput.style.textDecoration = "underline";
	}
	else
	{
		element.style.backgroundColor = "";
		messageInput.style.textDecoration = "";
	}
	bUnderlined = flag;
	// set directives to be processed by the server logic
	var underStyle = getElement("underStyle");
	if(underStyle != null)
		underStyle.value = bUnderlined;
}

function setItalic(flag)
{
	var element = getElement("italic");
	var messageInput = getElement("message");
	if (flag)
	{
		element.style.backgroundColor = "#EEE8F2";
		messageInput.style.fontStyle = "italic";
	}
	else
	{
		element.style.backgroundColor = "";
		messageInput.style.fontStyle = "";
	}
	bItalic = flag;
	// set directives to be processed by the server logic
	var italicStyle = getElement("italicStyle");
	if(italicStyle != null)
		italicStyle.value = bItalic;
}

function setAnchor(flag)
{
	var element = getElement("Anchor");
	var messageInput = getElement("message");
	if (flag)
	{
		messageInput.style.fontStyle = "";
		messageInput.style.color = "blue";
		messageInput.style.fontWeight = "normal";
		messageInput.style.fontSize = "12px";
		messageInput.style.fontFamily = "Arial";
		messageInput.style.textDecoration = "underline";
		element.style.backgroundColor = "#EEE8F2";
	}
	else
	{
		setItalic(bItalic);
		setBold(bBold);
		setUnderline(bUnderlined);
		messageInput.style.color = getElement("textcolor").style.backgroundColor;
		messageInput.style.fontSize = getElement("sizeValue").options[getElement("sizeValue").selectedIndex].value;
		messageInput.style.fontFamily = getElement("fontValue").options[getElement("fontValue").selectedIndex].value;
	}
	bAnchored = flag;
	// set directives to be processed by the server logic
	var anchorStyle = getElement("anchorStyle");
	if(anchorStyle != null)
		anchorStyle.value = bAnchored;
}

function selectColor(color)
{
	var messageInput = getElement("message");
	var colorStyle = getElement("colorStyle");
	if(colorStyle != null)
		colorStyle.value = color;
	getElement("colorTable").style.display = "none";
	getElement("textcolor").style.backgroundColor = color;
	if(messageInput!= null)
		messageInput.style.color = color;
	// todo: set directives to be processed by the server logic
}

function selectFont()
{
	var messageInput = getElement("message");
	var x = getElement("fontValue");
	var val = null;
	if(x != null)
		val = x.options[x.selectedIndex].text;
	if(messageInput != null && val != null)
	{
		messageInput.style.fontFamily = val;
		messageInput.style.width="350";
	}
}

function selectSize()
{
	var messageInput = getElement("message");
	var x = getElement("sizeValue");
	var val = null;
	if(x != null)
		val = x.options[x.selectedIndex].value;
	if(messageInput != null && val != null)
	{
		messageInput.style.fontSize = val;
		messageInput.style.width="350";
	}
}


/**
* Build the message to the Exit
*/
function onExitChat()
{
	return true;
}
/**
* Build the message as an URL Text
*/
function onSendURLAction()
{
	// this is an URL to pass, so we wrapped around and ANCHOR as the HREF
	var msg = "<A target='_blank' style='text-decoration:underline;' href='" + getElement("chatMessage").value + "'>" + getElement("chatMessage").value + "<" + "/A>";
	// Now we send the real chat message with the URL
	getElement("message").value = msg;
	return true;
}
/**
* Build the message to the Chat URL with the selected styles
*/
function onSendTextAction()
{
	// this is an Message to pass, so we got to encode the selected styles.
	// I'm not sure about if this should be necesary, we should just get the full HTML inside the field
	// maybe it's needed because we are reading the text from a text input that doesn't keep a HTML tree, just text.
	var msg = "<span style=' ";
	if(bItalic == true)
		msg = msg + "font-style:italic;";
	if(bBold == true)
		msg = msg + "font-weight:bold;";
	if(bUnderlined == true)
		msg = msg + "text-decoration:underline;";
	msg = msg + "color:" + getElement("textcolor").style.backgroundColor + ";";
	msg = msg + "font-size:" + getElement("sizeValue").options[getElement("sizeValue").selectedIndex].value + ";";
	msg = msg + "font-family:" + getElement("fontValue").options[getElement("fontValue").selectedIndex].value + ";'>";
	//if(getElement('typeOfSend').value == "Send")
	msg = msg + getElement("message").value + "<" + "/span>";
	// Now we send the real chat message with the styles
	getElement("message").value = msg;
	return true;
}
/**
* Push a file
*/
function uploadFile()
{
	window.open('<isa:webappsURL name="ecall/jsp/agent/upload/upload.jsp" />',
	'upload','width=220,height=330,screenX=200,screenY=300,titlebar=yes statusbar=yes');
}


function oldToggle()
{
	var curElement = event.srcElement;
	if(curElement.style.backgroundColor == "")
	{
		curElement.style.backgroundColor = "EEE8F2";
	}
	else
	{
		curElement.style.backgroundColor = "";
	}
	var messageInput = getElement("message");
	if(curElement.id == "bold")
	{
		if(bBold == true)
		{
			messageInput.style.fontWeight = "";
			bBold = false;
		}
		else
		{
			messageInput.style.fontWeight = "bold";
			bBold = true;
		}
	}
	if(curElement.id  == "underlined")
	{
		if(bUnderlined == true)
		{
			messageInput.style.textDecoration = "";
			bUnderlined = false;
		}
		else
		{
			messageInput.style.textDecoration = "underline";
			bUnderlined = true;
		}
	}
	if(curElement.id == "italic")
	{
		if(bItalic)
		{
			messageInput.style.fontStyle = "";
			bItalic = false;

		}
		else
		{
			messageInput.style.fontStyle = "italic";
			bItalic = true;
		}

	}
	if(curElement.id == "Anchor")
	{
		if(bAnchored == false)
		{
			messageInput.style.fontStyle = "";
			messageInput.style.color = "blue";
			messageInput.style.fontWeight = "normal";
			messageInput.style.fontSize = "12px";
			messageInput.style.fontFamily = "Arial";
			messageInput.style.textDecoration = "underline";
			bAnchored = true;
		}
		else
		{
			if(bItalic == true)
				messageInput.style.fontStyle = "italic";
			else
				messageInput.style.fontStyle = "";
			if(bBold == true)
				messageInput.style.fontWeight = "bold";
			else
				messageInput.style.fontWeight = "";
			if(bUnderlined == true)
				messageInput.style.textDecoration = "underline";
			else
				messageInput.style.textDecoration = "";

			messageInput.style.color = getElement("textcolor").style.backgroundColor;
			messageInput.style.fontSize = getElement("sizeValue").options[getElement("sizeValue").selectedIndex].value;
			messageInput.style.fontFamily = getElement("fontValue").options[getElement("fontValue").selectedIndex].value;
			bAnchored = false;
		}
	}
	// set directives to be processed by the server logic
	//
	var anchorStyle = getElement("anchorStyle");
	if(anchorStyle != null)
		anchorStyle.value = bAnchored;
	var italicStyle = getElement("italicStyle");
	if(italicStyle != null)
		italicStyle.value = bItalic;
	var boldStyle = getElement("boldStyle");
	if(boldStyle != null)
		boldStyle.value = bBold;
	var underStyle = getElement("underStyle");
	if(underStyle != null)
		underStyle.value = bUnderlined;
	//
}

