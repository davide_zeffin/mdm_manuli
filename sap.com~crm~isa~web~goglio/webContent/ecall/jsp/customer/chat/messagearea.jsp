<!-- start of messagearea.jsp -->
<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import= "java.util.*,javax.servlet.http.*" %>
<%@ page import="org.apache.struts.action.Action,org.apache.struts.util.MessageResources" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import= "com.sap.isa.cic.customer.actionforms.LogonForm" %>
<%@ page import= "com.sap.isa.cic.comm.chat.logic.*" %>
<%@ page import= "com.sap.isa.cic.customer.beans.*" %>
<%@ page import="com.sap.isa.cic.comm.core.CRequestRouterBean" %>
<%@ page import="com.sap.isa.cic.comm.core.CCommRequest,com.sap.isa.cic.core.context.*" %>
<%@ page import="com.sap.isa.cic.comm.core.ICommRequestListener" %>
<%@ page import= "com.sap.isa.cic.customer.sos.SessionObjectManager" %>
<%@ page import= "com.sap.isa.cic.businessobject.customer.BusinessPartner,com.sap.isa.cic.comm.core.CCommSupportType"%>
<%@ page import="com.sap.isa.cic.core.context.CContext" %>
<%@ page import="com.sap.isa.core.SessionConst,com.sap.isa.core.UserSessionData" %>
<%@ page import= "com.sap.isa.cic.core.init.LWCConfigProvider" %>
<%@ page import= "com.sap.isa.cic.comm.core.CommConstant" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% 
    BaseUI ui = new BaseUI(pageContext); 
	CRequestRouterBean router = null;

	ChatRequestCustomer listener = null;
	CContext context = null;
    MessageResources resources = (MessageResources)application.getAttribute(Action.MESSAGES_KEY );

	UserSessionData userData =
	UserSessionData.getUserSessionData(request.getSession());
	Locale locale = userData.getLocale();

	SessionObjectManager som = (SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
	String sessionId = session.getId();
	if((som == null) ) {
		//forward to error page
		return;
	}
	else {
		context = som.getContext();
		if(context == null) return; //forward to session expired page
		router = som.getRouter();
	}
	
	if (router != null && router.getRequest() instanceof CChatRequest) {
		listener = (ChatRequestCustomer )router.getListener();
	}
	else { //  for the first time

        CChatRequest chatRequest = null;

        // get all the lwc properties
        Properties allProps = LWCConfigProvider.getInstance().getAllProps();

        // TODO check to see that email is not null
        if (allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes")) {
			chatRequest = new CChatRequest(new CCommSupportType( som.getSupportType()), context.getEmail(), context);
        }
        else {
			chatRequest = new CChatRequest(new CCommSupportType( som.getSupportType()), context.getUserId(), context);
        }
        
        chatRequest.setDescription(som.getQuestions());
        som.setQuestions(null);
        if(som.isChatWithLastAgent()) {
            chatRequest.setAgentSpecific(true);
            chatRequest.setTargetAgentID(som.getLastAgent());
        }

		router = new CRequestRouterBean(chatRequest);
		listener = new ChatRequestCustomer(locale);
        listener.setSom(som);
		listener.setSessionId(sessionId);
		router.addRequestListener(listener);
		som.createRouter(router);
		session.setAttribute(SessionObjectManager.SO_MANAGER, som);
	} // if (router != null && ..

	IChatServer chatServer = null;
	String roomName = null;
	if ((listener !=null)&&(listener instanceof ChatRequestCustomer )) {
			chatServer = (IChatServer)((ChatRequestCustomer ) listener).getChatServer();
	}
	else {
	    %> <isa:log priority="DEBUG" key="request came out to be null or the request is non chat request" /> <%
		return;
	}
	
	boolean isChating = false;
	String chatHistory = "";

	if (chatServer !=null) {
	    
	    Enumeration entries = chatServer.getChatEvents();
		if (entries.hasMoreElements()) {
			isChating = true;
		}

		while(entries.hasMoreElements()) {
		    
	        String chatMessage = null;
			CChatEvent event = (CChatEvent) entries.nextElement();
			if (event==null) { //FIXIT - shouldnot arise
				continue;
			}

			// Join
			if (event instanceof CChatJoinEvent) {
				chatMessage = ((CChatJoinEvent)event).decorate(locale);
			}
			
			// Text
			else if (event instanceof CChatTextEvent) {
				CChatTextEvent textEvent = (CChatTextEvent) event;
				String entryName = textEvent.getSource().toString();
				String message = textEvent.getText();
				if(message != null) {
				    chatMessage = textEvent.decorate(locale);
				}
			}

			// Push
			else if(event instanceof CChatPushEvent) {
			    CChatPushEvent pushEvent = (CChatPushEvent) event;
				String entryName = pushEvent.getSource().toString();
				String message = pushEvent.decorateForCustomer(locale);
				if(message != null) {
					chatMessage = message;
				}
			}
			    
			// End
			else if (event instanceof CChatEndEvent) {
			    isChating = false;
				CChatEndEvent endEvent = (CChatEndEvent) event;
				String message = endEvent.getText();
				if (message != null) {
				    chatMessage = message;
				}
			}
			
			// Append the message
			chatHistory += chatMessage;
		}
	}
	
	//add variable to notify that the chat is on/off
	session.setAttribute("isChatting", new Boolean (isChating));
%>

<div class="module-name"><isa:moduleName name="ecall/jsp/customer/chat/messagearea.jsp" /></div>
<table width="100%" border="0" cellspacing="0" cellpadding="3">

	<tr bordercolor="#999999">
		<td width="90%" valign="top">
			<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
			<div id='waitForAgent' class='content'>
			<%if (chatHistory.length()==0) { %>
			 	<span><isa:translate key= "cic.prompt.chat.wait" /></span>
			<%}%>
			</font>
			</div>
		</td>
	</tr>
	
	<tr bordercolor="#999999">
		<td colspan="3">
		       <span><h3><isa:translate key="cic.prompt.chat.messagearea" /></h3></span>
		</td>
	</tr>
</table>

<BR>

<%  if (ui.isAccessible()) { %>
	<span><div id='textDisplay' class='content'><%=chatHistory%> </div></span>
	<br>
	<br>
	<label for="agentextDisplay"><isa:translate key="cic.last.agent.msg"/></label>
	   <br>
	   <TEXTAREA name="agentextDisplay" id="agentextDisplay" rows="1" cols="63" ></TEXTAREA>
	   <input type="hidden" name="isaccessible" id="accessible" value="yes">

 <% } else {%>
	<div id='textDisplay' class='content'><%=chatHistory%></div></span>
 <% } %>

<script type="text/javascript">
	// move to the end of the chat
	this.scrollTo(10000000,10000000);
</script>
<!-- end of messagearea.jsp -->
