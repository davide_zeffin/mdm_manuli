var agentID="";
var isChating = false;
var msgType;

var firstTime = true;	
var i =0;

function myHandler(serverEvent){
	 var event = serverEvent;                 
	if(event.getSubject().indexOf('newMessage')!= -1)
	{			
		entryText = event.get('entryText');
		msgType =  event.get('type');
		if(msgType=="push")
		{
			window.open(event.get('downloadURL'));
			return;
		}
		if(msgType=="agentJoin")
		{
		     if(disableChatButtons != null)
			   disableChatButtons(false);
		     agentID=event.get('source').toLowerCase();
		}
		var display = getElement("textDisplay");
		if( display.insertAdjacentHTML ) {
		    display.insertAdjacentHTML( 'beforeEnd', entryText );
  		} else if( typeof( document.body.innerHTML ) != 'undefined' ) {
   		    display.innerHTML += entryText;
  		} 
		
		if(getElement("accessible") != null) { // only if accessible
		   		        
			
			var agentdisplay = getElement("agentextDisplay");
			var text = stripHtml(entryText);
			var text = stripAgentName(text);

			// work around since when agent joins the customer question also gets posted
			// first time.			
			i++;			
			if(i != 2) {				
			   agentdisplay.value=text;   			
			   agentdisplay.focus();
			}
   		}
		
		if(msgType=="exit"){
			agentID="";
			disableChatButtons(true);
		}
		// clear the wait message
		var waitForAgent = getElement("waitForAgent");
		waitForAgent.innerHTML = "";
	}
}   	

function stripHtml(htmltext){ 
	var re= /<\S[^>]*>/g; 
	htmltext = htmltext.replace(re,""); 
	return htmltext
}

function stripAgentName(text) {
	var ind = text.indexOf(":");
	if(text.charAt(ind+1)==' ')
		text = text.substring(ind+2,text.length);
	else
		text = text.substring(ind+1,text.length);
	return text;
}


