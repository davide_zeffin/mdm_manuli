<!-- start of chatroom.jsp -->
<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import= "java.util.*,javax.servlet.http.*" %>
<%@ page import= "com.sap.isa.cic.comm.chat.logic.*,com.sap.isa.cic.customer.IConstant" %>
<%@ page import= "com.sap.isa.cic.customer.beans.* " %>
<%@ page import= "com.sap.isa.cic.comm.core.*,com.sap.isa.cic.businessobject.customer.BusinessPartner,com.sap.isa.cic.comm.chat.logic.* " %>
<%@ page import= "com.sap.isa.cic.customer.sos.SessionObjectManager" %>
<%@ page import="com.sap.isa.cic.core.context.CContext" %>
<%@ page import= "com.sap.isa.core.util.WebUtil" %>
<%@ page import= "com.sap.isa.cic.core.init.LWCConfigProvider" %>
<%@ page import= "com.sap.isa.cic.comm.core.CommConstant" %>
<%@ page import= "com.sap.isa.core.FrameworkConfigManager" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext); 
   boolean isSecure = "true".equals(FrameworkConfigManager.XCM.getApplicationScopeConfig().getComponentConfig("lwc", "lwcconfig").getAllParams().getProperty("securemode"));
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<HEAD>
   <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
   <script  type="text/javascript" src="<%=WebUtil.getMimeURL(pageContext, "", "mimes/jscript/EComBase.js")%>">
	</script>
   <SCRIPT type="text/javascript" src="<isa:webappsURL name="ecall/smartstream/js/smartstream.js" />" > </SCRIPT>
   <SCRIPT type="text/javascript" src="<isa:webappsURL name="ecall/jsp/customer/chat/chatmessage.js" />"> </SCRIPT>
   <SCRIPT type="text/javascript" src="<isa:webappsURL name="ecall/jsp/customer/chat/chatroomSubscribe.js" />"> </SCRIPT>


   <script>

    function announceBold(flag) {
		var curElement = document.all("bold");
		if(flag)
		   curElement.title="<isa:translate key="cic.format.bold"/>" + " "
		   + "<isa:translate key="cic.font.selected"/>";
		else
		   curElement.title="<isa:translate key="cic.format.bold"/>" + " "
		   + "<isa:translate key="cic.font.unselected"/>";
		 }

	function announceUnderline(flag) {
		var curElement = document.all("underlined");

		if(flag)
		   curElement.title="<isa:translate key="cic.format.underline"/>" + " "
		   + "<isa:translate key="cic.font.selected"/>";
		else
		   curElement.title="<isa:translate key="cic.format.underline"/>" + " "
		   + "<isa:translate key="cic.font.unselected"/>";

		 }

	function announceItalic(flag) {
			var curElement = document.all("italic");

		if(flag)
		   curElement.title="<isa:translate key="cic.format.italics"/>" + " "
		   + "<isa:translate key="cic.font.selected"/>";
		else
		   curElement.title="<isa:translate key="cic.format.italics"/>" + " "
		   + "<isa:translate key="cic.font.unselected"/>";

		 }

	function announceAnchor(flag) {
		var curElement = document.all("Anchor");

		if(flag)
			curElement.title="<isa:translate key="cic.prompt.chat.linkStyle"/>" + " " +
			"<isa:translate key="cic.font.selected"/>";
		else
			curElement.title="<isa:translate key="cic.prompt.chat.linkStyle"/>" + " " +
			"<isa:translate key="cic.font.unselected"/>";

	}


	function announce() {
	
		var curElement = event.srcElement;
		if(curElement.id == "bold") {
			announceBold(bBold);
		}
		if(curElement.id  == "underlined") {
			announceUnderline(bUnderlined);
		}
		if(curElement.id == "italic") {
			announceItalic(bItalic);
		}
		if(curElement.id == "Anchor") {
			announceAnchor(bAnchored);
		}
	}

	function disableChatButtons(boolValue) {
	
	    getElement("message").disabled = boolValue; //disable text entry
		getElement("send").disabled = boolValue; //disable send button
		
		// Note to customer: uncomment the following comment to enable the customer uploading file to agent scenario in LWC
		<% if (!isSecure ) {%>
			// getElement("sendfile").disabled = boolValue;
	    <% } %>
	    
		   getElement("exit").disabled = boolValue;
			 
			 <%if (ui.isAccessible()) {
				 String sendBtnTxt = WebUtil.translate(pageContext, "cic.button.send", null);
				 String exitBtnTxt = WebUtil.translate(pageContext, "cic.button.exit", null);
				 String sendfileBtnTxt = WebUtil.translate(pageContext, "cic.button.sendfile", null);
				 String unavailable = WebUtil.translate(pageContext, "access.unavailable", null);

			  %>
				if(boolValue) {
					getElement("message").title = "<isa:translate key="cic.prompt.chat.messageLbl" />" + " " + "<isa:translate key="cic.field.unavailable" />" ;
					getElement("send").title = "<isa:translate key="access.button" arg0="<%=sendBtnTxt%>" arg1="<%=unavailable%>"/>";

					<% if (!isSecure ) {%>
					       //getElement("sendfile").title = "<isa:translate key="access.button" arg0="<%=sendfileBtnTxt%>" arg1="<%=unavailable%>"/>";;
					<% }   %>
					getElement("exit").title = "<isa:translate key="access.button" arg0="<%=exitBtnTxt%>" arg1="<%=unavailable%>"/>";;
					
				 } else {
 				       getElement("message").disabled = false;
		               getElement("send").disabled    = false;
		               getElement("exit").disabled    = false;
		               
					   getElement("message").title = "<isa:translate key="cic.prompt.chat.messageLbl" />";
					   getElement("send").title = "<isa:translate key="access.button" arg0="<%=sendBtnTxt%>" arg1=""/>";

					<% if (!isSecure ) {%>
					     //getElement("sendfile").title = "<isa:translate key="access.button" arg0="<%=sendfileBtnTxt%>" arg1=""/>";;
					<% } %>
					   getElement("exit").title = "<isa:translate key="access.button" arg0="<%=exitBtnTxt%>" arg1=""/>";;
				 }

			 <%} %>

			 if(!boolValue) {
				 getElement("message").focus();
			 }

	   }
   </script>


	<STYLE>
		.btn
		{
			white-space: nowrap;
			padding: 0 2px 0 2px;
			border: 1px solid;
			text-decoration: none;
			font-size: x-small;
			height: 18px;
			font-weight: normal;
			background-color: #C1D3E0;
			font-weight: normal;
			background-color: #C1D3E0;
			text-align: center;
			font-style: normal;
			border-color: #BAE3FE #3474B5 #3474B5 #BAE3FE;
		}
		.paneltable
		{
			background-color: #C1D3E0; <!--lightgrey;-->
			layer-background-color: lightgrey;
			border: 2px outset white;
		}
	</STYLE>

</HEAD>


<%

/**
 * @todo - check the user bean from the isa and not the business partner from cic
 */
	String roomName=null;
	CContext contxt = null;

	SessionObjectManager som =
		(SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);

	String sessionId = session.getId();

	if(som == null) {
		String url = WebUtil.getAppsURL(pageContext, new Boolean(false), "ecall/jsp/customer/session_expired.jsp","","",false);
%>
		<isa:log priority="DEBUG" key="cic.session.expired" arg0=" forwarding to logon page" arg1="<%=url%>" />
<% 		response.sendRedirect(url);
	}


	contxt = som.getContext();

		// get all the lwc properties
		Properties allProps = LWCConfigProvider.getInstance().getAllProps();

		// TODO check to see that email is not null
		if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("Yes"))
		roomName = contxt.getEmail();
		else
		roomName = contxt.getUserId();
%>


<%

String params = "type=push&subjects=newMessage"+roomName+sessionId;
String subscription = WebUtil.getAppsURL(pageContext, null, "ecall/smartstream",params,null,false);
 %>
<BODY class="help">
<table width="100%" border="0" cellpadding="4" cellspacing="1">
	<tr><td><jsp:include page="../chat/chatheader.jsp" /></td></tr>
	<tr><td width=50%><jsp:include page="../chat/messagearea.jsp" /></td></tr>
	<tr><td><jsp:include page="../chat/messageform.jsp" /></td></tr>
	<tr><td><IFRAME SRC="<%=subscription%>" NAME="subscriberFrame" BORDER=0 SCROLLING=no width=0/></td></tr>

</table>

<!-- end of chatroom.jsp -->







