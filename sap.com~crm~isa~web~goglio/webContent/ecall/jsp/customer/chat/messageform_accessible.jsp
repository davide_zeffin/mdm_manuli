<!-- start of messageform_accessible.jsp -->
<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<jsp:useBean id="chatForm" scope="request" class="com.sap.isa.cic.customer.actionforms.ChatForm" />
<DIV id="toolbarDivision" class="btn" style="text-align:left;width:55px;">

	<TABLE cellSpacing=0 cellPadding=0 width="50%">
		<TR>
			<TD>
			<% String accessText = WebUtil.translate(pageContext, "cic.chat.format", null); %>
			<a
				href="#grp1End"
				id="grp1Begin"
				title="<isa:translate key="access.grp.begin" arg0="<%=accessText%>"/>"
			></a>
		    </TD>
		</TR>

		<TR>
			<TD><DIV id="spacing">&nbsp;&nbsp;</DIV></TD>
			<TD><DIV tabindex=0 id="bold" onKeyPress="toggle(); announce()" onClick="toggle(); announce()"
				     title="<isa:translate key="cic.format.bold"/>" style="cursor:hand;width:4;">&nbsp;B&nbsp;</DIV></TD>
			<TD><DIV tabindex=0 id="underlined" onKeyPress="toggle();announce()" onClick="toggle();announce()" 
			         style="text-decoration:underline;cursor:hand;width:2;">&nbsp;U&nbsp;</DIV></TD>
			<TD><DIV tabindex=0 id="italic" onKeyPress="toggle();announce()" onClick="toggle();announce()" 
			         style="font-style:italic;cursor:hand;width:2;">&nbsp;I&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV nowrap id="size" style="width:2; ">
				 	 <label for="sizeValue"><isa:translate key="cic.format.size"/></label>
					 <SELECT id="sizeValue" name="sizeValue" style="font-size: x-small;" onChange="selectSize();">
					<%
						  for (int i=10; i <= 16; i++) {   
						      String size = Integer.toString(i)+"pt"; %>
							  <OPTION <%=(chatForm.getSizeValue()!=null && chatForm.getSizeValue().equals(size)?"selected":"")%> value="<%=size%>"><%=size%></OPTION><%
						  }
					%>
					</SELECT>
				</DIV>
			</TD>

			<TD>
				<DIV nowrap id="font" style="width:2;">
				 	<label for="fontValue"><isa:translate key="cic.format.font"/></label>
					<SELECT id="fontValue" name="fontValue" style="font-size: x-small;" onChange="selectFont();">
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("sans-serif")?"selected":"")%> value="sans-serif">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontArial" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("Verdana")?"selected":"")%> value="Verdana">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontVerdana" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("fantasy")?"selected":"")%> value="fantasy">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontComic" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("monospace")?"selected":"")%> value="monospace">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontCourierNew" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("serif")?"selected":"")%> value="serif">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontTNewRoman" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("script")?"selected":"")%> value="script">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontScript" /></SPAN>
						</OPTION>
					</SELECT>
				</DIV>
			</TD>
			
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV tabindex=0 id="Anchor" onKeyPress="toggle(); announce()" onClick="toggle(); announce()" 
			         style="cursor:hand;text-decoration:underline;color:blue;"><isa:translate key="cic.prompt.chat.linkStyle" /></DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
		</TR>
		
		<tr>
			<td colspan="10"><div id="colorTable" class="paneltable" style="position:absolute;display:none;z-index:201" source="" nowrap> </div></td>
		<tr>
		<tr>
			<td>
			<a
				href="#grp1Begin"
				id="grp1End"
				title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>"
			></a>

		       </td>
		</tr>
	</TABLE>
</DIV>


<BR/>


<INPUT type='hidden' id='anchorStyle' name='anchorStyle' value='false' />
<INPUT type='hidden' id='italicStyle' name='italicStyle' value='false' />
<INPUT type='hidden' id='boldStyle' name='boldStyle'  value='false' />
<INPUT type='hidden' id='underStyle' name='underStyle'  value='false' />
<INPUT type='hidden' id='colorStyle' name='colorStyle'  value='black' />
<INPUT type='hidden' id='textcolor' name='textcolor'   />
<!-- end of messageform_accessible.jsp -->