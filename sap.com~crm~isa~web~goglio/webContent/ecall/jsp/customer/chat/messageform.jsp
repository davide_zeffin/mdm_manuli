<!-- start of messageform.jsp -->
<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import= "com.sap.isa.cic.core.init.LWCConfigProvider" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import= "java.util.*,javax.servlet.http.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import= "com.sap.isa.core.FrameworkConfigManager" %>

<% BaseUI ui = new BaseUI(pageContext); 
   boolean isSecure = "true".equals(FrameworkConfigManager.XCM.getApplicationScopeConfig().getComponentConfig("lwc", "lwcconfig").getAllParams().getProperty("securemode"));
%>

<jsp:useBean id="chatForm" scope="request" class="com.sap.isa.cic.customer.actionforms.ChatForm" />
<DIV class="module-name"><isa:moduleName name="ecall/jsp/customer/chat/messageform.jsp" /></DIV>

<!-- start of formatting thing -->
<FORM action="<isa:webappsURL name="ecall/chat.do" />" name="chatForm" >

<%  if (ui.isAccessible()) { %>
	<jsp:include page="messageform_accessible.jsp" flush="true"/>
<%  } else { %>

<!-- Toolbar -->
<DIV id="toolbarDivision" class="btn" style="text-align:left;width:55px;">

	<TABLE cellSpacing=0 cellPadding=0 width="50%">
		<TR>
			<TD><DIV id="spacing">&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="bold" onClick="toggle(event);" style="cursor:hand;width:4;">&nbsp;B&nbsp;</DIV></TD>
			<TD><DIV id="underlined" onClick="toggle(event);" style="text-decoration:underline;cursor:hand;width:2;">
			         &nbsp;U&nbsp;</DIV></TD>
			<TD><DIV id="italic" onClick="toggle(event);" style="font-style:italic;cursor:hand;width:2;">&nbsp;I&nbsp;</DIV></TD>
			<TD><DIV id="textcolor" onClick="showColors(event);" style="background-color:black;cursor:hand;width:2;height:2">&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			
			<!-- Font Size -->
			<TD><DIV nowrap id="size" style="cursor:hand;width:2;">
					<SELECT id="sizeValue" name="sizeValue" style="font-size: x-small;" onChange="selectSize();">
					<%
						  for (int i=10; i <= 16; i++) {   
						      String size = Integer.toString(i)+"pt"; %>
							  <OPTION <%=(chatForm.getSizeValue()!=null && chatForm.getSizeValue().equals(size)?"selected":"")%> 
							  value="<%=size%>"> <%=size%></OPTION><%
						  }
					%>
					</SELECT>
				</DIV>
			</TD>
			
			<!-- Font Style -->
			<TD>
				<DIV nowrap id="font" style="cursor:hand;width:2;">
					<SELECT id="fontValue" name="fontValue" style="font-size: x-small;" onChange="selectFont();">
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("sans-serif")?"selected":"")%> value="sans-serif">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontArial" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("Verdana")?"selected":"")%> value="Verdana">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontVerdana" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("fantasy")?"selected":"")%> value="fantasy">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontComic" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("monospace")?"selected":"")%> value="monospace">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontCourierNew" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("serif")?"selected":"")%> value="serif">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontTNewRoman" /></SPAN>
						</OPTION>
						<OPTION <%=(chatForm.getFontValue()!=null && chatForm.getFontValue().equals("script")?"selected":"")%> value="script">
							<SPAN style="font-size: x-small;"><isa:translate key="cic.prompt.chat.fontScript" /></SPAN>
						</OPTION>
					</SELECT>
				</DIV>
			</TD>
			
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="Anchor" onClick="toggle(event);" style="cursor:hand;text-decoration:underline;color:blue;">
			         <isa:translate key="cic.prompt.chat.linkStyle" /></DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
			<TD><DIV id="space">&nbsp;&nbsp;&nbsp;&nbsp;</DIV></TD>
		</TR>
	</TABLE>
</DIV> <!-- toolbar -->

<BR/>

<!-- Color Menu Component -->
<DIV id="colorTable" class="paneltable" style="position:absolute;display:none;z-index:201" source="" nowrap>

  <TABLE cellPadding=1 cellSpacing=1 width=50>
	<TR>
		<TD><DIV class="lastOptElement" onClick="selectColor('#FF0000');" style="background-color:#FF0000;">&nbsp;&nbsp;</DIV></TD>
		<TD><DIV class="lastOptElement" onClick="selectColor('#FFFF00');" style="background-color:#FFFF00;">&nbsp;&nbsp;</DIV></TD>
		<TD><DIV class="lastOptElement" onClick="selectColor('#808080');" style="background-color:#808080;">&nbsp;&nbsp;</DIV></TD>
		<TD><DIV class="lastOptElement" onClick="selectColor('#008000');" style="background-color:#008000;">&nbsp;&nbsp;</DIV></TD>
	</TR>
	
	<TR>
	    <TD><DIV class="lastOptElement" onClick="selectColor('#0000FF');" style="background-color:#0000FF;">&nbsp;&nbsp;</DIV></TD>
		<TD><DIV class="lastOptElement" onClick="selectColor('#A52A2A');" style="background-color:#A52A2A;">&nbsp;&nbsp;</DIV></TD>
		<TD><DIV class="lastOptElement" onClick="selectColor('#000000');" style="background-color:#000000;">&nbsp;&nbsp;</DIV></TD>
		<TD><DIV class="lastOptElement" onClick="selectColor('#FFA500');" style="background-color:#FFA500;">&nbsp;&nbsp;</DIV></TD>
	</TR>
  </TABLE>
</DIV> <!-- color menu component -->

<!--
BUILD THE MESSAGE WITH THE SELECTED STYLES
THESE ARE THE HIDDEN DIRECTIVES TO BE SEND
-->
<INPUT type='hidden' id='anchorStyle' name='anchorStyle' value='false' />
<INPUT type='hidden' id='italicStyle' name='italicStyle' value='false' />
<INPUT type='hidden' id='boldStyle' name='boldStyle'  value='false' />
<INPUT type='hidden' id='underStyle' name='underStyle'  value='false' />
<INPUT type='hidden' id='colorStyle' name='colorStyle'  value='black' />

<% } // not accessible %>
<!-- end of formatting thing -->



<!-- Input area -->
<TABLE align="left" width="100%" height="80%" border="0" >
	<TR><TD colspan="3" height="1"><label for="message"><isa:translate key="cic.prompt.chat.messageLbl" /></label></TD></TR>
	<TR><TD colspan="3">
			<% if(ui.isAccessible()) { %>
			       <TEXTAREA name="message" id="message" class="content" rows="2" cols="63"
			                 accesskey="<isa:translate key="cic.accessKey.chatTxt"/>"></TEXTAREA>
		    <% } else { %>
			       <TEXTAREA name="message" id="message" class="content" rows="2" cols="63"></TEXTAREA>
		    <% } %>
<TABLE>
<TR>
<br>
<br>
<br>


<%
	String sendBtnTxt = WebUtil.translate(pageContext, "cic.button.send", null);
    String exitBtnTxt = WebUtil.translate(pageContext, "cic.button.exit", null);
	String printBtnTxt = WebUtil.translate(pageContext, "cic.button.print", null);
	String saveBtnTxt = WebUtil.translate(pageContext, "cic.button.save", null);
	String sendfileBtnTxt = WebUtil.translate(pageContext, "cic.button.sendfile", null);
	String closeBtnTxt = WebUtil.translate(pageContext, "cic.button.close", null);

	if(ui.isAccessible()) {  %>
	
	    <TD align='left'>

				<INPUT	type="submit" name="sendAction"	class="green" id="send"
						title="<isa:translate key="access.button" arg0="<%=sendBtnTxt%>" arg1=""/>"
						value="<isa:translate key="cic.button.send" />" />
						
				<INPUT type="button" name="print" class="green"  onclick='chatprint('<isa:webappsURL name="ecall/jsp/customer/chat/messagearea.jsp"/>');'
					   title="<isa:translate key="access.button" arg0="<%=printBtnTxt%>" arg1=""/>"
					   value="<isa:translate key="cic.button.print" />" />

				<INPUT type="button" name="save" class="green"
						onclick="window.open('<isa:webappsURL name="ecall/jsp/customer/chat/messagearea.jsp"/>' );"
						title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>"
						value="<isa:translate key="cic.button.save" />" />
						
				<% if (!isSecure) {%>

				<!--INPUT	type="button" name="sendfile" class="green"	id="sendfile"
						onclick="window.open('<isa:webappsURL name="ecall/jsp/customer/upload/upload.jsp" />','upload','width=220,height=330,screenX=200,screenY=300,titlebar=yes statusbar=yes');return;"
						title="<isa:translate key="access.button" arg0="<%=sendfileBtnTxt%>" arg1=""/>"
						value="<isa:translate key="cic.button.sendfile" />"
				/-->
			    <% }%>
		</TD>
		</FORM>


		<!-- exit -->
		<TD align='right'>
				<FORM	action="<isa:webappsURL name="ecall/chat.do"/>" name="exitForm" method="POST">
					<INPUT	type="submit" name="exitAction" id="exit" class="green"
							title="<isa:translate key="access.button" arg0="<%=exitBtnTxt%>" arg1=""/>"
							value="<isa:translate key="cic.button.exit" />"	/>
				</FORM>
		</TD>
			
		<!-- close -->
		<TD align='right'>
				<FORM	action="<isa:webappsURL name="ecall/chat.do" />" name="closeControl" scope="session">
					<INPUT type="submit" name="closeAction" class="green"
							title="<isa:translate key="access.button" arg0="<%=closeBtnTxt%>" arg1=""/>"
							value="<isa:translate key="cic.button.close" />" />
				</FORM>
		</TD>
			
<% } else { %>
		
    <!-- send -->
	<TD align='left'>

				<INPUT	type="submit" name="sendAction"	class="green" id="send"
						value="<isa:translate key="cic.button.send" />"	/>
						
				<INPUT type="button" name="print" class="green"
					   onclick="chatprint('<isa:webappsURL name="ecall/jsp/customer/chat/messagearea.jsp"/>');"
					   value="<isa:translate key="cic.button.print" />"	/>

				<INPUT type="button" name="save" class="green"
						onclick="window.open('<isa:webappsURL name="ecall/jsp/customer/chat/messagearea.jsp"/>' );"
						value="<isa:translate key="cic.button.save" />"	/>
						
				<% if (!isSecure) {%>

				        <!--INPUT	type="button" name="sendfile" class="green"	id="sendfile"
						onclick="window.open('<isa:webappsURL name="ecall/jsp/customer/upload/upload.jsp" />','upload','width=220,height=330,screenX=200,screenY=300,titlebar=yes statusbar=yes');return;"
						value="<isa:translate key="cic.button.sendfile" />"
				        /-->
				<% }%>

			</FORM>
			</TD>

			<!-- exit -->
			<TD align='right'>
				<FORM	action="<isa:webappsURL name="ecall/chat.do"/>" name="exitForm" method="POST">
					<INPUT	type="submit" name="exitAction" id="exit" class="green"
							value="<isa:translate key="cic.button.exit" />" />
				</FORM>
			</TD>
			
			<!-- submit -->
			<TD align='right'>
				<FORM	action="<isa:webappsURL name="ecall/chat.do" />" name="closeControl" scope="session">
					<INPUT type="submit" name="closeAction" class="green"
							value="<isa:translate key="cic.button.close" />" />
				</FORM>
			</TD>

		<% } // ui is accessible%>

		</TR>
		</TABLE>
	</TD>
</TR>
</TABLE>


<script type="text/javascript">
	  updateFormat(<jsp:getProperty name='chatForm' property='boldStyle'   />,
			       <jsp:getProperty name='chatForm' property='italicStyle' />,
			       <jsp:getProperty name='chatForm' property='underStyle'  />,
			       <jsp:getProperty name='chatForm' property='anchorStyle' />,
			       '<jsp:getProperty name='chatForm' property='colorStyle'  />');

   <% if(ui.isAccessible()) { %>
		  announceBold(<jsp:getProperty name='chatForm' property='boldStyle' />);
		  announceItalic(<jsp:getProperty name='chatForm' property='italicStyle' />);
		  announceUnderline(<jsp:getProperty name='chatForm' property='underStyle' />);
		  announceAnchor(<jsp:getProperty name='chatForm' property='anchorStyle' />);

   <% } %>


  // set focus to the message box
  if(getElement("message").disabled == false) {
	getElement("message").focus();
  }
</script>


<% if(!"yes".equalsIgnoreCase(request.getParameter("activate"))) { %>
	<script type="text/javascript">
		if(isChating == false) {
			disableChatButtons(true);
		}
	</script>
<% } %>
<% 
//get variable to check if chat is on/off
boolean isChatting = false;
if (session.getAttribute("isChatting") != null)
	isChatting = ((Boolean)session.getAttribute("isChatting")).booleanValue();
if (isChatting == false) {
%>
<script type="text/javascript">
	disableChatButtons(true);
</script>
<%
}
%>
<!-- end of messageform.jsp -->