<!-- start of chatrequest.jsp -->
<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.cic.comm.call.logic.CCallType" %>
<%@ page import="com.sap.isa.cic.customer.actions.ChatRequestAction" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import= "com.sap.isa.cic.customer.sos.SessionObjectManager" %>
<%@ page import= "com.sap.isa.cic.comm.core.AgentEventsReceiver" %>
<%@ page import= "com.sap.isa.cic.core.init.LWCConfigProvider" %>
<%@ page import= "com.sap.isa.cic.comm.core.CommConstant" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% 
    BaseUI ui = new BaseUI(pageContext); 
    SessionObjectManager som = (SessionObjectManager) session.getAttribute(SessionObjectManager.SO_MANAGER);
    String questionValidationKey = (String)session.getAttribute("questionValidation");
%>

<div class="module-name"><isa:moduleName name="ecall/jsp/customer/callback/chatrequest.jsp" /></div>

<form action="<isa:webappsURL name="ecall/customer/callRequest.do"/>"  name="chatForm">

<table cellpadding="0" cellspacing="0" border="0">

    <% if(questionValidationKey!=null) { %>
    
        <tr>
           <td>
		       <a id="messages" href="#messages-list-start" accesskey="<isa:translate key="access.messages.key"/>"
			      title="<isa:translate key="access.messages.header" arg0="1"/>">
				  <isa:translate key="access.messages.header" arg0="1"/>
		       </a><br>
		       
		       <a class= "messageText" name="messages-list-start" href="#messages-list-end"
			      title="<isa:translate key="access.messages.start" arg0="1"/>">
			      <isa:translate key="access.messages.start" arg0="1"/>
		       </a>
		   </td>
        </tr>
       
	    <tr>
           <td colspan="2">
             <a href="javascript:document.chatForm.quest.focus(); " id="err">
			    <font color = "red"><isa:translate key="cic.chat.question.isNull"/></font></a>
		   </td>
        </tr>

 	    <% session.removeAttribute("questionValidation");%>
		<tr>
		    <td>
			    <a class= "messageText" id="messages-list-end" name="messages-list-end"
			       href="#messages-list-start" title="<isa:translate key="access.messages.end"/>">
			       <isa:translate key="access.messages.end"/></a>
			</td>
       	</tr>

	 <% } %>

    <tr>
	    <td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1px" height="30px" border="0" alt=""></td>
	</tr>

	<tr>
	    <td><span><h3><isa:translate key="cic.callback.chatTitle"/></h3></span></td>
    </tr>

	<tr>
        <td colspan="2" class="CONTENT"><span><isa:translate key="cic.chat.note.message"/></span></td>
	</tr>
	
	<tr>
		<td background="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/pixblack.gif") %>" colspan="2">
		    <img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="100%" height="20px" border="0px" alt=""></td>
	</tr>

	<tr>
		<td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1px" height="20px" border="0px" alt=""></td>
	</tr>

    <tr>
        <td align="right"><label for="sub"><isa:translate key="cic.support.type.subject"/>&nbsp;</label></td>
        <td align="left">
			<jsp:include page="/ecall/jsp/customer/supporttype.jsp" />
	    </td>
    </tr>

	<tr>
		<td align="right" valign="top" class="CONTENT"><label for="quest"><isa:translate key="cic.chat.request.question"/>&nbsp;</td>
		<td><TEXTAREA name="<%=ChatRequestAction.LWC_CHAT_QUESTION%>"  cols="40" rows="4" id="quest"  class="submitDoc" ></TEXTAREA></td>
	</tr>
	
	<tr>
		<td colspan="2"><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1px" height="5px" border="0px" alt=""></td>
	</tr>
	
<%
    // get all the lwc properties
    Properties allProps = LWCConfigProvider.getInstance().getAllProps();
    
    if(allProps.getProperty(CommConstant.LWC_SPICE_ENABLED).equalsIgnoreCase("no")) {
        
	    String lastAgent = som.getLastAgent();
	    if(lastAgent!=null && AgentEventsReceiver.getInstance().getAvaliablity(lastAgent)) {
%>
		    <tr>
			    <td>
			        <INPUT TYPE="checkbox" id="lastAgent" NAME="<%=ChatRequestAction.LWC_LAST_AGENT%>"
			               value="<%=ChatRequestAction.LWC_LAST_AGENT%>" UNCHECKED>
			        <label for="lastAgent"> <isa:translate key="cic.prompt.lastagent"/></label>
			    </td>
		    </tr>
<%
  	    }
    }
%>

    <tr>
	    <td><img src="<%=WebUtil.getMimeURL(pageContext, "ecall/mimes/layer/sp.gif") %>" width="1px" height="1px" border="0px" alt=""></td>

		<td>&nbsp;

		<%
			String sendBtnTxt  = WebUtil.translate(pageContext, "cic.button.send", null);
			String resetBtnTxt = WebUtil.translate(pageContext, "cic.button.reset", null);
			String exitBtnTxt  = WebUtil.translate(pageContext, "cic.button.exit", null);

			if(ui.isAccessible()) {
		%>

				<input type="submit" name="actionSend" class="green"
					value="<isa:translate key="cic.button.send"  />"
					title="<isa:translate key="access.button" arg0="<%=sendBtnTxt%>" arg1=""/>">

				 &nbsp;
					<input type="reset" class="green"
					value="<isa:translate key="cic.button.reset" />"
					title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>">

				&nbsp;
					<input type="submit" name="actionExit" class="green"
					value="<isa:translate key="cic.button.exit"  />"
					title="<isa:translate key="access.button" arg0="<%=exitBtnTxt%>" arg1=""/>">
		<%
			} else {	// ui is not accessible
		%>
				<input type="submit" name="actionSend" class="green"
					value="<isa:translate key="cic.button.send"  />">

				 &nbsp;
					<input type="reset" class="green"
					value="<isa:translate key="cic.button.reset" />">

				&nbsp;
					<input type="submit" name="actionExit" class="green"
					value="<isa:translate key="cic.button.exit"  />">
		<%
			}
		%>

		</td>
	</tr>
</table>

<script>
  document.chatForm.sub.focus();
</script>

</form>
<!-- end of chatrequest.jsp -->

