<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ page import= "java.util.Properties" %>
<%@ page import= "com.sap.isa.cic.core.init.LWCConfigProvider" %>
<%@ page import= "com.sap.isa.cic.comm.core.CommConstant" %>

<% BaseUI ui = new BaseUI(pageContext); %>


<jsp:useBean    id="channelsForm"
                scope="session"
                type="com.sap.isa.cic.customer.actionforms.ChannelsForm">
</jsp:useBean>

    <div class="module">
      <div class="module-name"><isa:moduleName name="ecall/jsp/customer/index.jsp" /></div>

      <%
			// get all the lwc properties
		   Properties allProps = LWCConfigProvider.getInstance().getAllProps();
		   String icwcEnabled = allProps.getProperty(CommConstant.LWC_SPICE_ENABLED);
      %>

       	 <%  if (ui.isAccessible()) { %>
		<span tabindex=0><h2><isa:translate key="cic.prompt.index.heading"/></h2></span>
	 <% } else {%>
		<h2><isa:translate key="cic.prompt.index.heading"/></h2>
	 <% } %>

      <table border="0" cellpadding="4" cellspacing="0">
        <% if (!ui.isAccessible()) { %>
		<% if (channelsForm.isMail()) { %>
			  <tr>
<%-- GREENORANGE BEGIN --%>			  
			    <%-- td><a href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=email"/>" --%>
			    <td><a href="mailto:info@fresco-onlinesales.com"
			                     accesskey="<isa:translate key="cic.accessKey.email"/>">
			                     <isa:translate key="cic.prompt.index.mail"/></a></td>
			    <td><isa:translate key="cic.prompt.index.mail.Message"/></td>
			  </tr>
		<% } %>
		<%-- if (channelsForm.isChat()) { %>
			  <tr>
			    <td><a href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=chat"/>"
			    		accesskey="<isa:translate key="cic.accessKey.chat"/>">
			    		<isa:translate key="cic.prompt.index.chat"/></a></td>
			    <td><isa:translate key="cic.prompt.index.chatMessage"/></td>
			  </tr>
		<% } --%>	
	    <% } else { %>
	    	<% if (channelsForm.isMail()) { %>
			  <tr>		  
			    <%-- td><a href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=email"/>" --%>
				<td><a href="mailto:info@fresco-onlinesales.com"
			    	title = "<isa:translate key="cic.prompt.index.mail"/>:<isa:translate key="cic.prompt.index.mail.Message"/>"
					     accesskey="<isa:translate key="cic.accessKey.email"/>">
					     <isa:translate key="cic.prompt.index.mail"/></a></td>
			    <td><isa:translate key="cic.prompt.index.mail.Message"/></td>
			  </tr>
		<% } %>
		<%-- if (channelsForm.isChat()) { %>
			  <tr>
			    <td><a href="<isa:webappsURL name="ecall/customer/interaction0.do?interactionType=chat"/>"
			    	title = "<isa:translate key="cic.prompt.index.chat"/>:<isa:translate key="cic.prompt.index.chatMessage"/>"
					accesskey="<isa:translate key="cic.accessKey.chat"/>">
					<isa:translate key="cic.prompt.index.chat"/></a></td>
			    <td><isa:translate key="cic.prompt.index.chatMessage"/></td>
			  </tr>
		<% } --%>
<%-- GREENORANGE END --%>		
	    <%} // is accessible%>
      </table>
    </div>

