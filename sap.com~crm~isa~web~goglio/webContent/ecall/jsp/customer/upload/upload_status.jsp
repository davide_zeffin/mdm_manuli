<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<% BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

	<head>
	<isa:includes/>

	</head>
	<body class="help">
	<div class="module-name"><isa:moduleName name="ecall/jsp/customer/upload/upload_status.jsp" /></div>

	<table  border="1" align="center" >
		<tr>
			<td class="content" align="left">
			  <b>
				<span><isa:translate key="cic.prompt.upload.filename" /></span>
			  </b>
			</td>
			<td class="content">
				 <span><%= request.getAttribute("fileName") %></span>
			</td>
		</tr>
		<tr>
			<td class="content" align="left">
			  <b>
				<span><isa:translate key="cic.prompt.upload.filetype" /></span>
			  </b>
			</td>
			<td class="content">
				<span><%= request.getAttribute("contentType") %></span>
			</td>
		</tr>
		<tr>
			<td class="content" align="left">
			  <b>
				<span><isa:translate key="cic.prompt.upload.filesize" /></span>
			  </b>
			</td>
			<td class="content">
				 <span><%= request.getAttribute("size") %></span>
			</td>
		</tr>
		<tr >
			<td colspan="2" align ="CENTER" class="content">

		<%
		    String closeBtnTxt = WebUtil.translate(pageContext, "cic.button.close", null);
			if(ui.isAccessible()) {
		%>
				<input type="button" name="close" class="green" onclick="self.close()"
			  			title="<isa:translate key="access.button" arg0="<%=closeBtnTxt%>" arg1=""/>"
						value="<isa:translate key="cic.button.close" />" >

		<%
			} else {
		%>
			    <input type="button" name="close" class="green" onclick="self.close();return;"
						value="<isa:translate key="cic.button.close" />" >
		<%
			}
		%>


			</td>
		</tr>
	</table>
	</body>
</html>