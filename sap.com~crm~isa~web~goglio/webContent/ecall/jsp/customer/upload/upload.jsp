<%@ page language="java" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

	<head>
	<isa:includes/>

	</head>
	<body class="help" onload="javascript:document.uploadForm.chosenFile.focus()">
		<div class="module-name"><isa:moduleName name="ecall/jsp/customer/upload/upload.jsp" /></div>
		<form name="uploadForm" action="<isa:webappsURL name="ecall/upload.do" />"
		    enctype="multipart/form-data" method="POST">
			<label for="uploadText"><isa:translate key="cic.prompt.upload.message" /></label>

		<br />
		<input type="file" name="chosenFile" class="submitDoc" id="uploadText" /><br /><br />

		<%
		    String uploadBtnTxt = WebUtil.translate(pageContext, "cic.button.upload", null);
			String closeBtnTxt = WebUtil.translate(pageContext, "cic.button.close", null);
			if(ui.isAccessible()) {
		%>
				<input type="submit" name="upload" class="green"
						title="<isa:translate key="access.button" arg0="<%=uploadBtnTxt%>" arg1=""/>"
						value="<isa:translate key="cic.button.upload" />" >

			  	<input type="button" name="close" class="green" onclick="self.close();return;"
			  			title="<isa:translate key="access.button" arg0="<%=closeBtnTxt%>" arg1=""/>"
						value="<isa:translate key="cic.button.close" />" >

		<%
			} else {
		%>
			  <input type="submit" name="upload" class="green"
			  			value="<isa:translate key="cic.button.upload" />" >

			  <input type="button" name="close" class="green" onclick="self.close();return;"
						value="<isa:translate key="cic.button.close" />" >
		<%
			}
		%>


		</form>
	</body>
</html>