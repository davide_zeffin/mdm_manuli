<%-- 
This JSP invalidates the session and displays the Logoff screen to 
restart the application. 
Additionally the request parameter "loginOutOfIsaTop" will be read.
If this parameter is set to "true", a logoff within "isaTop" frame 
structure will be proceeded.
--%>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.user.action.LogoffBaseAction" %>
<%@ page import="com.sap.security.api.UMFactory" %>
<%@ page import="com.sap.engine.services.servlets_jsp.server.exceptions.InvalidSessionException" %>

<%  BaseUI ui = new BaseUI(pageContext);

    String reloginBtnTxt = WebUtil.translate(pageContext, "um.clc.logoff.jsp.logon", null);
    String loginOutOfIsaTop = request.getParameter("loginOutOfIsaTop");
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="startpage.header"/></title>
        <isa:stylesheets/>
     
        <script type="text/javascript">
        <!--
            var counter = 0; 
            function loginOutOfIsaTop() {
              
                var tmpWnd = window;
                while (tmpWnd.parent != tmpWnd && tmpWnd.name != "isaTop") {
                    tmpWnd = tmpWnd.parent;
                }
                tmpWnd=tmpWnd.parent;
                if (counter == 0) {
                 counter++;
                 tmpWnd.location.href="<isa:reentryURL name="/init.do"/>";
                 return true;
                } else {
                 return false;
               }  
            }
            
            function loginOut() {
            
                if (counter == 0) {
                  counter++;
                  window.location.href="<isa:reentryURL name="/init.do"/>"; 
                  return true;
                }  
                else {
                  return false;
                } 
            }
            
        //-->
        </script>
    </head>
 
    <body class="header-body">

        <div id="header-appl">
            <div class="header-logo"></div>
            <div class="header-applname">
                <isa:translate key="startpage.header"/>
            </div>
            <div class="header-username"></div>
            <%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
            <%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
            <%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
                 so make sure to test your work in these browsers. --%>
            <div id="header-extradiv1">
                <span></span>
            </div>
            <div id="header-extradiv2">
                <span></span>
            </div>
            <div id="header-extradiv3">
                <span></span>
            </div>
            <div id="header-extradiv4">
                <span></span>
            </div>
            <div id="header-extradiv5">
                <span></span>
            </div>
            <div id="header-extradiv6">
                <span></span>
            </div>
        </div> <%-- header-appl --%>

        <div class="selection">
            <div class="module-name"><isa:moduleName name="user/logon/logoff.jsp" /></div>
            <h1><isa:translate key="um.clc.logoff.jsp.thxvisit"/></h1>
            <div class="login">
                <ul>
                    <br />
                    <li class="label-input">
                        <div class="button1">
                        <%
                        if(loginOutOfIsaTop != null && loginOutOfIsaTop.equalsIgnoreCase("true")) { %>
                            <a href="#" 
                               onclick="loginOutOfIsaTop();" 
                               class="button" 
                               title="<isa:translate key="access.button" arg0="<%=reloginBtnTxt%>" arg1=""/>"
                            ><isa:translate key="um.clc.logoff.jsp.logon"/></a>
                        <% 
                        }
                        else { %>
                            <a href="#" 
                               onclick="loginOut();"
                               class="button" 
                               title="<isa:translate key="access.button" arg0="<%=reloginBtnTxt%>" arg1=""/>" 
                            ><isa:translate key="um.clc.logoff.jsp.logon"/></a>
                        <%
                        } %>  
                        </div>
                    </li>
                </ul>
            </div> <%-- login --%>
        </div> <%-- selection --%>
    </body>
</html>
<%-- Include logic to invalidate the session. --%>
<%@ include file="/user/logon/session_invalidate.inc" %>
