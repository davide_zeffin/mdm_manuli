<%-- 
This JSP displays the PasswordChange screen to enter a new password. 
--%>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.LoginUI"%>
<%@ page import="com.sap.isa.user.action.UserActions" %>

<%  LoginUI ui = new LoginUI(pageContext);
    String accessText = "";
    String headerText = ""; 

    if ( ((String) request.getAttribute(UserActions.RC_ACTION_NAME)).equals(UserActions.ACTION_US)) {
        headerText = WebUtil.translate(pageContext, "um.clc.pwchg.jsp.pwlength", null);
    }
    else {
        headerText = WebUtil.translate(pageContext, "um.clc.pwchg.jsp.pwchang", null);
    }

    String chngpwdBtnTxt = WebUtil.translate(pageContext, "um.clc.pwchg.jsp.submit", null);
    String cancelBtnTxt = WebUtil.translate(pageContext, "um.clc.pwchg.jsp.cancel", null);
    
	// Get the max password length, depending on the backend.
	int passwdLength = ui.getPasswordLength();
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="startpage.header"/></title>
        <isa:stylesheets/>
        <script src="<%=WebUtil.getMimeURL(pageContext, "", "user/jscript/buttons.js") %>"
                type="text/javascript">
        </script>

        <script language="JAVAScript">
        <!--
            function return_event(){
                document.getElementById("changepw").submit();
            }
        //-->
        </script>
    </head>

    <body class="header-body"
            onload="document.changepw.<%=UserActions.PN_OLDPASSWORD%>.focus();">

    <% 
    if (ui.isPortal) { %>
        <div id="header-portal">
    <% 
    } else { %>
        <div id="header-appl">
    <% 
    } %> 

            <div class="header-logo"></div>
	
            <div class="header-applname">
                <isa:translate key="startpage.header"/>
            </div>

            <div class="header-username"></div>

            <%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
            <%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
            <%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
                 so make sure to test your work in these browsers. --%>
            <div id="header-extradiv1">
                <span></span>
            </div>
            <div id="header-extradiv2">
                <span></span>
            </div>
            <div id="header-extradiv3">
                <span></span>
            </div>
            <div id="header-extradiv4">
                <span></span>
            </div>
            <div id="header-extradiv5">
                <span></span>
            </div>
            <div id="header-extradiv6">
                <span></span>
            </div>
        </div> <%-- header-appl --%>
        
        <div class="selection">
            <div class="module-name"><isa:moduleName name="user/logon/pwchange.jsp" /></div>
            <h1><isa:translate key="um.clc.pwchg.jsp.head"/></h1>

            <form method="post" action="<isa:webappsURL name="user/pwchange.do"/>" name="changepw">
                <%-- control the forward after the call with an hidden field --%>
                <input type="hidden" name="<%=UserActions.PN_FORWARD_NAME%>" value="<%=JspUtil.encodeHtml((String)request.getAttribute(UserActions.PN_FORWARD_NAME))%>" />
                <input type="hidden" name="<%=UserActions.PN_ACTION_NAME%>" value="<%=JspUtil.encodeHtml((String)request.getAttribute(UserActions.RC_ACTION_NAME))%>" />
                <% 
                if (ui.isAccessible) { %>
                <a href="#end-login" name="start-login" title="<isa:translate key="um.clc.pwchg.jsp.endpwchan"/>"></a>
                <% 
                } %>
                <div class="login">
                    <ul>
                        <%
                        if(ui.isAccessible()) {
                        %>
                        <a href="#grp1End"
                           id="grp1Begin"
                           title="<isa:translate key="access.grp.begin" arg0="<%=headerText%>"/>" 
                        ></a>
                        <%
                        }
                        %>
                        <li class="text">
                            <span><%=headerText%></span>
                        </li>
                        <li class="label-input">
                            <div>
                                <label for="oldpw"><isa:translate key="um.clc.pwchg.jsp.oldpwd"/></label><br/>
                                <input type="password" id="oldpw" class="input-1" name="<%=UserActions.PN_OLDPASSWORD%>" onkeypress="checkreturn(event)"/>
                            </div>
                        </li>
                        <li class="label-input">
                            <div>
                                <label for="newpw"><isa:translate key="um.clc.pwchg.jsp.passwd"/></label><br/>
                                <input type="password" id="newpw" class="input-1" name="<%=UserActions.PN_PASSWORD%>" maxlength="<%=passwdLength%>" onkeypress="checkreturn(event)"/>
                            </div>
                        </li>
                        <li class="label-input">
                            <div>
                                <label for="reppw"><isa:translate key="um.clc.pwchg.jsp.passwdver"/></label><br/>
                                <input type="password" id="reppw" class="input-1" name="<%=UserActions.PN_PASSWORD_VERIFY%>" maxlength="<%=passwdLength%>" onkeypress="checkreturn(event)"/>
                            </div>
                        </li>
                        <li class="label-input">
                            <div class="button1">
                                <a href="#" 
                                   onclick="document.changepw.submit();" 
                                   title="<isa:translate key="access.button" arg0="<%=chngpwdBtnTxt%>" arg1=""/>" 
                                   class="button" 
                                   name="<%=JspUtil.encodeHtml((String)request.getAttribute(UserActions.RC_ACTION_NAME))%>" 
                                ><isa:translate key="um.clc.pwchg.jsp.submit"/></a>
                                <a href="#" 
                                   onclick="location.href='<isa:reentryURL name="/init.do"/>'" 
                                   title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>" 
                                   class="button" 
                                   name="backward"
                                ><isa:translate key="um.clc.pwchg.jsp.cancel"/></a>
                            </div>
                        </li>
                        <%
                        if(ui.isAccessible()) {
                        %>
                        <a href="#grp1Begin"
                           id="grp1End"
                           title="<isa:translate key="access.grp.end" arg0="<%=headerText%>"/>" 
                        ></a>	
                        <%
                        }
                        %>                        
                        <li>
                            <div>&nbsp;</div>
                        </li>
                        <isa:message id="errortext" name="<%=UserActions.RC_USER%>"
                                type="<%=Message.ERROR%>">
                        <li>
                            <div class="error">
                                <span><%=errortext%></span>
                            </div>
                        </li>	
                        </isa:message>
                        <isa:message id="infotext" name="<%=UserActions.RC_USER%>"
                                type="<%=Message.INFO%>">
                        <li>
                            <div class="error">
                                <span><%=infotext%></span>
                            </div>
                        </li>
                        </isa:message>
                    </ul>
                    <% 
                    if (ui.isAccessible) { %>
                    <a href="#start-login" name="end-login" title="<isa:translate key="um.clc.pwchg.jsp.startpwch"/>"></a>
                    <% 
                    } %>		
                </div> <%-- class="login" --%>
            </form>
        </div> <%-- class="selection" --%>
    </body>
</html>