<%-- 
This JSP displays the login screen to enter the login data. After pressing Submit,
the Login Parameter will be given to the LoginBaseAction, which verifies the login in
the backend system. Afterwards the User will either be logged on or informed by a message,
why he was not logged on the system.
--%>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.user.action.UserActions" %>
<%@ page import="com.sap.isa.user.businessobject.UserBase" %>

<%@ page import="java.util.Collection" %>

<%  BaseUI ui = new BaseUI(pageContext);
    String accessText = ""; 
//--- GREENORANGE START ---//	
 	String zgo_newuser ="zgo_newuser";
//--- GREENORANGE STOP ---//
    String loginBtnTxt = WebUtil.translate(pageContext, "um.clc.login.jsp.login", null);
    String pwchangeBtnTxt = WebUtil.translate(pageContext, "um.clc.login.jsp.pwchange", null);
//--- GREENORANGE START ---//	
  	String zgo_newuserBtnTxt = WebUtil.translate(pageContext, "customer.login.jsp.zgo_newuser", null);
//--- GREENORANGE STOP ---//	

%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="startpage.header"/></title>

        <isa:stylesheets/>

        <script src="<%=WebUtil.getMimeURL(pageContext, "", "user/jscript/buttons.js") %>"
            type="text/javascript">
        </script>
        <script language="JavaScript">
        <!--
            var submitcount=0;
            function checkFields() { 
            
                if (submitcount == 0){
                    submitcount++;
                    return true;
                }
                else {
                    return false;
                }
            }

            function startLogin(name, value){
                document.getElementById("hiddenfield").name = name;
                document.getElementById("hiddenfield").value = value;
                document.login_form.submit();
            }

            function return_event(){
                startLogin('<%=UserActions.PN_LOGIN%>', '<isa:translate key="um.clc.login.jsp.login"/>');
            }
        //-->
        </script>

    </head>
	
    <body class="header-body"
            onload="document.login_form.<%=UserActions.PN_USERID%>.focus();">
        <div class="module-name"><isa:moduleName name="user/logon/Zlogin.jsp" /></div>
        <% 
        if (ui.isPortal) { %>
        <div id="header-portal">
        <% 
        } 
        else { %>
        <div id="Zheader-appl">
        <% 
        } %> 

            <div class="header-logo"></div>
            <div class="header-applname">
                <isa:translate key="startpage.header"/>
            </div>
            <div class="header-username"></div>

            <%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
            <%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
            <%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
                 so make sure to test your work in these browsers. --%>
            <div id="header-extradiv1">
                <span></span>
            </div>
            <div id="header-extradiv2">
                <span></span>
            </div>
            <div id="header-extradiv3">
                <span></span>
            </div>
            <div id="header-extradiv4">
                <span></span>
            </div>
            <div id="header-extradiv5">
                <span></span>
            </div>
            <div id="header-extradiv6">
                <span></span>
            </div>
        </div> <%-- header-appl --%>

        <div class="selection">
            <%-- create a form to read username, password and locale --%>
            <% 
            UserBase user = (UserBase) request.getAttribute(BusinessObjectBase.CONTEXT_NAME);
            if (user == null) {
                user = new UserBase();
            } %>
	
            <%-- determine the browser version --%>
            <script type="text/javascript">
            <!--
                document.write('<input type="hidden" name="browsername" value="' +  escape(navigator.userAgent.toLowerCase()) + '">');
                document.write('<input type="hidden" name="browsermajor" value="' + escape(parseInt(navigator.appVersion)) + '">');
                document.write('<input type="hidden" name="browserminor" value="' + escape(parseFloat(navigator.appVersion)) + '">');
            //-->
            </script>

            <h1><isa:translate key="um.clc.login.jsp.welcome"/></h1>

            <form method="POST" action='<isa:webappsURL name="user/login.do"/>' name="login_form"
                    onSubmit="return checkFields()">
                <input type="hidden" id="hiddenfield">
			  
                <% 
                if (ui.isAccessible) { %>
                <a href="#end-login" name="start-login" title="<isa:translate key="um.clc.login.jsp.endlogin"/>"></a>
                <% 
                } %>
                <div class="login">
                    <ul>
                        <isa:message id="UserInfo"
                                     name="<%=UserActions.RC_USER%>"
                                     type="<%=Message.INFO%>"
                                     ignoreNull="true">
                        <li>
                            <div class="info">
                                <span><%=UserInfo%></span>	
                            </div>
                        </li>
                        </isa:message>
                        <%
                        if(ui.isAccessible()) {
                            accessText = WebUtil.translate(pageContext, "um.clc.login.jsp.moduleHead", null);
                        %>
                        <a href="#grp1End"
                           id="grp1Begin"
                           title="<isa:translate key="access.grp.begin" arg0="<%=accessText%>"/>" 
                        ></a>
                        <%
                        }
                        %>
                        <li class="label-input">
                            <div>
                                <label for="userid"><isa:translate key="um.clc.login.jsp.userid"/></label><br>
                                <input class="input-1" type="text" id="userid" name="<%=UserActions.PN_USERID%>" value="<%=JspUtil.encodeHtml(user.getUserId())%>" onkeypress="checkreturn(event)"/>
                            </div>
                        </li>
                        <li class="label-input">
                            <div>
                                <label for="password"><isa:translate key="um.clc.login.jsp.password"/></label><br>
                                <input class="input-1" type="password" id="password" name="<%=UserActions.PN_PASSWORD%>" onkeypress="checkreturn(event)"/>
                            </div>
                        </li>
                        <% 
                        HashMap langHash = (HashMap) request.getAttribute(UserActions.RC_LANGUAGES_HASH);
                        // get sorted view with TreeSet:
                        TreeSet langTree = new TreeSet(langHash.keySet());
                        if(langHash != null && langHash.size() > 0) { %>
                        <li class="label-input">
                            <div>
                                <label for="language"><isa:translate key="um.clc.login.jsp.lang"/></label><br>				
                                <select size="1" name="<%=UserActions.PN_LANGUAGE%>" id="language">
                                <%
                                String def_language = (String)request.getAttribute(UserActions.RC_LANGUAGE);

                                Iterator it = langTree.iterator();
                                while (it.hasNext()) {
                                    String alias = (String)it.next();
                                    String value = (String)langHash.get(alias);

                                    String lang_selected = "";
                                    if(def_language != null && def_language.equalsIgnoreCase(alias)){
                                        lang_selected = "selected=\"selected\"";
                                    } %>
                                    <option value="<%=JspUtil.encodeHtml(alias)%>" <%=lang_selected%> ><isa:translate key="<%=JspUtil.encodeHtml(value)%>"/></option>
                                <% 
                                } %>
                                </select>
                            </div>
                        </li>
                        <%
                        } %>
                        <li>
                            <div class="button1">
                                <a href="#" 
                                   onclick="startLogin('<%=UserActions.PN_LOGIN%>', '<isa:translate key="um.clc.login.jsp.login"/>');" 
                                   title="<isa:translate key="access.button" arg0="<%=loginBtnTxt%>" arg1=""/>" 
                                   class="button" name="<%=UserActions.PN_LOGIN%>" 
                                ><isa:translate key="um.clc.login.jsp.login"/></a>
                                <a href="#" 
                                   onclick="startLogin('<%=UserActions.PN_PASSWORD_CHANGE%>', '<isa:translate key="um.clc.login.jsp.pwchange"/>');" 
                                   title="<isa:translate key="access.button" arg0="<%=pwchangeBtnTxt%>" arg1=""/>" 
                                   class="button" name="<%=UserActions.PN_PASSWORD_CHANGE%>" 
                                ><isa:translate key="um.clc.login.jsp.pwchange"/></a>
<%-- GREENORANGE START --%>                            
								<a href="#" 
                                   onclick="startLogin('<%=zgo_newuser%>', '<isa:translate key="customer.login.jsp.zgo_newuser"/>');" 
                                   title="<isa:translate key="access.button" arg0="<%=zgo_newuserBtnTxt%>" arg1=""/>" 
                                   class="button" name="<%=zgo_newuser%>"><isa:translate key="customer.login.jsp.zgo_newuser"/></a>
<%-- GREENORANGE STOP --%> 
                            </div>
                        </li>
                        <%
                        if(ui.isAccessible()) {
                            accessText = WebUtil.translate(pageContext, "um.clc.login.jsp.moduleHead", null);
                        %>
                        <a href="#grp1Begin"
                           id="grp1End"
                           title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>" 
                        ></a>	
                        <%
                        }
                        %>
                        <li>
                            <div>&nbsp;</div>
                        </li>
                    </ul>    
                    <isa:message id="UserError"
                                 name="<%=UserActions.RC_USER%>"
                                 type="<%=Message.ERROR%>"
                                 ignoreNull="true">
                        <div class="error">
                            <span><%=UserError%></span>	
                        </div>
                    </isa:message>
			        <% 
			        if (ui.isAccessible) { %>
                    <a href="#start-login" name="end-login" title="<isa:translate key="um.clc.login.jsp.startlogi"/>"></a>
                    <% 
                    } %>
                </div><%-- filter-result --%>
            </form>
        </div> <%-- selection --%>
    </body>
</html>