<%@ page import="com.sap.security.api.UMFactory" %>
<%@ page import="com.sap.engine.services.servlets_jsp.server.exceptions.InvalidSessionException" %>
<%  
	// logoff UME user
    UMFactory.getAuthenticator().logout(request, response);
    
    // invalidate the session
    try {
        if(session != null) {
            session.invalidate();
        }
    }
    catch (InvalidSessionException ise) {
       // this exception must be catched because UMFactory.getAuthenticator().logout can make the session invalid before
   }
%> 