<%-- 
This JSP displays the error message if UME login is enabled and after successful
UME login the E-Commerce specific login failed.
--%>

<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.user.action.UserActions" %>

<%  BaseUI ui = new BaseUI(pageContext);

    String logonBtnTxt = WebUtil.translate(pageContext, "um.clc.umeerr.jsp.logon", null);
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="startpage.header"/></title>
        <isa:stylesheets theme=""/>
    </head>
    <body class="header-body">
    <% 
    if (ui.isPortal) { %>
        <div id="header-portal">
    <% 
    } 
    else { %>
        <div id="header-appl">
    <% 
    } %> 
            <div class="header-logo"></div>
            <div class="header-applname">
                <isa:translate key="startpage.header"/>
            </div>
            <div class="header-username"></div>
            <%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
            <%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
            <%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
                 so make sure to test your work in these browsers. --%>
            <div id="header-extradiv1">
                <span></span>
            </div>
            <div id="header-extradiv2">
                <span></span>
            </div>
            <div id="header-extradiv3">
                <span></span>
            </div>
            <div id="header-extradiv4">
                <span></span>
            </div>
            <div id="header-extradiv5">
                <span></span>
            </div>
            <div id="header-extradiv6">
                <span></span>
            </div>
        </div> <%-- header-appl --%>
        <div class="selection">
            <div class="module-name"><isa:moduleName name="user/base/eror_ume.jsp" /></div>
            <h1><isa:translate key="um.clc.errume.jsp.head"/></h1>
            <div class="login">
                <br />
                <div class="message">
                    <isa:translate key="user.ume.error.logon"/>
                </div>
                <ul>
                    <isa:message id="errortext" name="<%=UserActions.RC_USER%>"
                            type="<%=Message.ERROR%>" >
                    <li>
                        <div class="error"><%=errortext%><br /></div>
                    </li>
                    </isa:message>
                    <isa:message id="infotext" name="<%=UserActions.RC_USER%>"
                            type="<%=Message.INFO%>" >
                    <li>
                        <div class="message"><%=infotext%><br /></div>
                    </li>
                    </isa:message>
                    <li class="label-input">
                        <div class="button1">
                            <a href="#" 
                               onclick="location.href='<isa:reentryURL name="/init.do"/>'" 
                               class="button" 
                               title="<isa:translate key="access.button" arg0="<%=logonBtnTxt%>" arg1=""/>" 
                             ><isa:translate key="um.clc.umeerr.jsp.logon"/></a>
                        </div>
                    </li>
                </ul>
            </div> <%-- login --%>
        </div> <%-- selection --%>
    </body>
</html>