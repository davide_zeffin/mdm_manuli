<%-- 
--%>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.user.action.UserActions" %>
<%@ page import="com.sap.isa.user.businessobject.UserBase" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>
<%@ page import="com.sap.isa.businessobject.Address" %>


<%@ page import="java.util.Collection" %>

<%  BaseUI ui = new BaseUI(pageContext);
	String accessText = ""; 
	
//--- GREENORANGE START ---//	
 	String zgo_newuser ="zgo_newuser";
  	String zgo_newuserBtnTxt = WebUtil.translate(pageContext, "customer.login.jsp.znewuser", null);
	String zgo_homepage ="zgo_homepage";
	String zgo_homepageBtnTxt = WebUtil.translate(pageContext, "customer.confirmcreateuser.jsp.homepage", null);
  	
	//UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
			
	//AddressFormular addressFormular = (AddressFormular)userSessionData.getAttribute("registering_address");
	AddressFormular addressFormular = (AddressFormular)request.getAttribute(ActionConstants.RC_ADDRESS_FORMULAR);
	
	/* read the address from the addressFormular */
	Address address = addressFormular.getAddress();
	ResultData titleList = (ResultData) request.getAttribute("titleList");
	
//--- GREENORANGE STOP ---//	
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<%-- Add the jcaptcha taglib--%>
<%@ taglib uri="/jcaptcha" prefix="jcaptcha"%>
                        
                        
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<title><isa:translate key="startpage.header"/></title>

		<isa:stylesheets/>

		<script src="<%=WebUtil.getMimeURL(pageContext, "", "user/jscript/buttons.js") %>"
			type="text/javascript">
		</script>
		<script language="JavaScript">
		<!--
			function checkFields() { 
			
			}

			function createUser(){
				document.newuser_form.submit();
			}

			function resetForm(){
				document.newuser_form.RegionList.value="clear";
				document.newuser_form.submit();
			}
			
			function return_event(){
				startLogin('<%=UserActions.PN_LOGIN%>', '<isa:translate key="um.clc.login.jsp.login"/>');
			}
			
			function loadRegionList() {
      			document.newuser_form.RegionList.value="load";
				document.newuser_form.submit();
			}
		//-->
		</script>

	</head>
	
	<body class="header-body">
		<div class="module-name"><isa:moduleName name="user/customer/createUser.jsp" /></div>
		<% 
		if (ui.isPortal) { %>
		<div id="header-portal">
		<% 
		} 
		else { %>
		<div id="Zheader-appl">
		<% 
		} %> 

			<div class="header-logo"></div>
			<div class="header-applname">
				<isa:translate key="startpage.header"/>
			</div>
			<div class="header-username"></div>

			<%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
			<%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
			<%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
				 so make sure to test your work in these browsers. --%>
			<div id="header-extradiv1">
				<span></span>
			</div>
			<div id="header-extradiv2">
				<span></span>
			</div>
			<div id="header-extradiv3">
				<span></span>
			</div>
			<div id="header-extradiv4">
				<span></span>
			</div>
			<div id="header-extradiv5">
				<span></span>
			</div>
			<div id="header-extradiv6">
				<span></span>
			</div>
		</div> <%-- header-appl --%>

		<div class="selection">
			<%-- 
			UserBase user = (UserBase) request.getAttribute(BusinessObjectBase.CONTEXT_NAME);
			if (user == null) {
				user = new UserBase();
			} --%>
	
			<%-- determine the browser version --%>
			<script type="text/javascript">
			<!--
				document.write('<input type="hidden" name="browsername" value="' +  escape(navigator.userAgent.toLowerCase()) + '">');
				document.write('<input type="hidden" name="browsermajor" value="' + escape(parseInt(navigator.appVersion)) + '">');
				document.write('<input type="hidden" name="browserminor" value="' + escape(parseFloat(navigator.appVersion)) + '">');
			//-->
			</script>

			<h1><isa:translate key="customer.login.jsp.znewuser"/></h1>

<%-- GREENORANGE START --%>  
			
			<%-- captcha error message --%>
			<% if(request.getAttribute("captcha_error") == "true") { %>
				<div class="error">
	        		<span><isa:translate key="customer.createuser.jsp.captcha_error"/></span>
	         	</div>
	        <% } %>
			
			<%-- general error message --%>
	 		<isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="" >
	 			<div class="error">
	        		<span><%= JspUtil.removeNull(errortext) %></span>
	         	</div>
	        </isa:message>
	        
			<form method="POST" action='<isa:webappsURL name="user/createUser.do"/>' name="newuser_form" >
<%-- GREENORANGE STOP --%> 			  
				<% 
				if (ui.isAccessible) { %>
				<a href="#end-login" name="start-login" title="<isa:translate key="um.clc.login.jsp.endlogin"/>"></a>
				<% 
				} %>
				<div class="login">
					<ul>
						<li class="label-input">
							<div>
<%-- GREENORANGE START --%> 
								<table>
									<tr>
										<td><label for="title"><isa:translate key="customer.createuser.jsp.title"/></label></td>
										<td><!-- input class="textinput-large" type="box" name="title" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getTitle()))%>"/-->
											
			                                <select name="title" id="titleKey" onchange="loadRegionList()"> 
												<%  // build up the selection options 
												    String defaultTitleKey =(String) address.getTitleKey(); 
												%>
			                                    <isa:iterate id="title" name="titleList"
			                                    	type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
													<%  // Should the current title be marked as selected?
													    String selected="";
													    if  (title.getString("ID").equals(defaultTitleKey)) {
													        selected="SELECTED";
													    } 
													%>
				                                    <option value="<%= title.getString("ID") %>" <%= selected %>>
				                                    	<%= JspUtil.encodeHtml(title.getString("DESCRIPTION")) %>
				                                    </option>
			                                    </isa:iterate>
			                                </select>
										</td>
									</tr>
									<tr>
										<td><label for="name1"><isa:translate key="customer.createuser.jsp.name1"/>*</label></td>
										<td>
											<input class="textinput-large" maxlength="35" type="text" name="name1" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getName1()))%>"/>
											<isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="NAME1">
										    	<div class="error">
										        	<%=errortext%><br/>
										        </div>
											</isa:message>
										</td>
									</tr>
									<tr>
										<td><%--label for="name2"><isa:translate key="customer.createuser.jsp.name2"/></label--%></td>
										<td>
											<input class="textinput-large" maxlength="35" type="text" name="name2" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getName2()))%>"/>
											<isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="NAME2">
										    	<div class="error">
										        	<%=errortext%><br/>
										        </div>
											</isa:message>
										</td>
									</tr>
									<tr><%-- INTERLOCUTOR --%>
										<td><label for="coName"><isa:translate key="customer.createuser.jsp.interlocutor"/></label></td>
										<td>
											<input class="textinput-large" type="text" name="coName" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getCoName()))%>"/>
											<isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="CONAME">
										    	<div class="error">
										        	<%=errortext%><br/>
										        </div>
											</isa:message>
										</td>
									</tr>	
									<tr>
										<td><label for="street"><isa:translate key="customer.createuser.jsp.addr"/>*</label></td>
									    <td>
									      <input type="text" maxlength="35" class="textinput-large" name="street" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getStreet()))%>"/>
									      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="STREET" >
									        <div class="error">
									          <%=JspUtil.removeNull(errortext)%><br />
									        </div>
									      </isa:message>									      
									    </td>
									</tr>
									<tr>
										<td><label for="postalCode"><isa:translate key="customer.createuser.jsp.pstlz"/>*</label></td>
									    <td>
									      <input class="textinput-small" maxlength="10" type="text" name="postalCode" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getPostlCod1()))%>" />
									      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
									                   property="POSTL_COD1" >
									        <div class="error">
									          <%=JspUtil.removeNull(errortext)%><br />
									        </div>
									      </isa:message>									      
									    </td>
									</tr>
									<tr>
										<td><label for="city"><isa:translate key="customer.createuser.jsp.ort01"/>*</label></td>
									    <td>									      
									      <input class="textinput-large" maxlength="35" type="text" name="city" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getCity()))%>" />
									      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
									                   property="CITY" >
									        <div class="error">
									          <%=errortext%><br />
									        </div>
									      </isa:message>
									    </td>
									</tr>	
									<tr>
										<td><label for="country"><isa:translate key="customer.createuser.jsp.land1"/>*</label></td>
										<td><input type="hidden" name="RegionList" value="" />
											<select class="textinput" name="country" id="country" onchange="loadRegionList()">
											   	<isa:iterate id="country" name="<%= ActionConstants.RC_COUNTRY_LIST %>"
										            type="com.sap.isa.core.util.table.ResultData"
										            resetCursor="true">
										            <option value="<%=JspUtil.encodeHtml(country.getString(Shop.ID))%>"
														<%= address.getCountry().equals(country.getString(Shop.ID))?"selected=\"selected\"":"" %> >
										            <%=JspUtil.encodeHtml(country.getString(Shop.DESCRIPTION))%>										            
										        	</option>
										    	</isa:iterate>
											</select>
											<isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
										                   property="COUNTRY" >
										        <div class="error">
										          <%=JspUtil.removeNull(errortext)%><br />
										    	</div>
											</isa:message>
										</td>
									</tr>
								  	<% ResultData regionList = (ResultData) request.getAttribute(ActionConstants.RC_REGION_LIST);
									   if (regionList != null && regionList.getNumRows() > 0) {
									%>
									<tr>
										<td><label for="region"><isa:translate key="customer.createuser.jsp.regio"/></label></td>
										<td>
								        	<select class="textinput" name="region" id="region">
								            	<isa:iterate id="region" name="<%= ActionConstants.RC_REGION_LIST %>"
										            type="com.sap.isa.core.util.table.ResultData"
										            resetCursor="true">
										            <option value="<%=JspUtil.encodeHtml(region.getString(Shop.ID))%>"
														<%= address.getRegion().equals(region.getString(Shop.ID))?"selected=\"selected\"":"" %> >
										            <%=JspUtil.encodeHtml(region.getString(Shop.DESCRIPTION))%>
										            </option>
										        </isa:iterate>
								    		</select>
								    		<isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
										                   property="REGION" >
										        <div class="error">
										          <%= JspUtil.removeNull(errortext) %><br />
										        </div>
											</isa:message>
								    	</td>
								    </tr>
									<% } %>
									<tr>
										<td><label for="email"><isa:translate key="customer.createuser.jsp.email"/>*</label></td>
										<td>
											<input class="textinput-large" type="text" name="email" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getEMail()))%>"/>
											<isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="E_MAIL">
										    	<div class="error">
										        	<%=errortext%><br/>
										        </div>
											</isa:message>
										</td>
									</tr>
									<tr>
										<td><label for="telephoneNumber"><isa:translate key="customer.createuser.jsp.phone"/>*</label></td>
										<td>
											<input class="textinput-large" type="text" name="telephoneNumber" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getTel1Numbr()))%>"/>
											<isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="TEL1NUMBR">
										    	<div class="error">
										        	<%=errortext%><br/>
										        </div>
											</isa:message>
										</td>
									</tr>
									<tr>
										<td><label for="faxNumber"><isa:translate key="customer.createuser.jsp.fax"/></label></td>
										<td>
											<input class="textinput-large" type="text" name="faxNumber" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getFaxNumber()))%>"/>
											<isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="FAXNUMBR">
										    	<div class="error">
										        	<%=errortext%><br/>
										        </div>
											</isa:message>
										</td>
									</tr>
									<tr>
										<td><label for="tels1Ext"><isa:translate key="customer.createuser.jsp.mobile"/></label></td>
										<td>
											<input class="textinput-large" type="text" name="tels1Ext" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getTel1Ext()))%>"/>
											<isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="TEL1EXT">
										    	<div class="error">
										        	<%=errortext%><br/>
										        </div>
											</isa:message>
										</td>
									</tr>
									<tr><%-- VAT-NUMBER --%>
										<td><label for="strSuppl1"><isa:translate key="customer.createuser.jsp.piva"/></label></td>
										<td><input class="textinput-large" type="text" name="strSuppl1" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getStrSuppl1()))%>"/></td>
									</tr>
									<tr><%-- NOTES --%>
										<td><label for="strSuppl2"><isa:translate key="customer.createuser.jsp.notes"/></label></td>
										<td>
											<textarea rows="5" cols="35" class="textinput-large" name="strSuppl2"><%=JspUtil.encodeHtml(JspUtil.removeNull(address.getStrSuppl2()))%></textarea>
										</td>
									</tr>
									<tr>
										<td colspan="2"><br/></td>
									</tr>
									<tr>
										<td colspan="2">
											<input type="checkbox" name="GTC" />
											<label for="GTC"><isa:translate key="customer.createuser.jsp.GTC"/> <a href="http://<%= request.getServerName() %>:50000/ImgGoglio/GTC.pdf" target="_blank">GTC</a></label>
										</td>
									</tr>
									<tr>
										<td colspan="2"><br/><br/></td>
									</tr>
									<tr>
										<td colspan="2"><isa:translate key="customer.createuser.jsp.captcha"/></td>
									</tr>
									<tr>
										<%-- Add the image--%>
										<td><img src="jcaptcha.do"/></td>
										<%-- Add the input tag--%>
										<td><input class="textinput-large" type="text" name="jcaptcha_response"/></td>
									</tr>
								</table>
<%-- GREENORANGE STOP --%> 			  								
							</div>
						</li>
						<%--
						HashMap langHash = (HashMap) request.getAttribute(UserActions.RC_LANGUAGES_HASH);
						// get sorted view with TreeSet:
						TreeSet langTree = new TreeSet(langHash.keySet());
						if(langHash != null && langHash.size() > 0) { %>
						<li class="label-input">
							<div>
								<label for="language"><isa:translate key="um.clc.login.jsp.lang"/></label><br>				
								<select size="1" name="<%=UserActions.PN_LANGUAGE%>" id="language">
								<%
								String def_language = (String)request.getAttribute(UserActions.RC_LANGUAGE);

								Iterator it = langTree.iterator();
								while (it.hasNext()) {
									String alias = (String)it.next();
									String value = (String)langHash.get(alias);

									String lang_selected = "";
									if(def_language != null && def_language.equalsIgnoreCase(alias)){
										lang_selected = "selected=\"selected\"";
									} %>
									<option value="<%=JspUtil.encodeHtml(alias)%>" <%=lang_selected%> ><isa:translate key="<%=JspUtil.encodeHtml(value)%>"/></option>
								<% 
								} %>
								</select>
							</div>
						</li>
						<%
						} --%>
						<li>
							<div class="button1">
<%-- GREENORANGE START --%>                            
								<a href="#" 
                                   onclick="createUser()" 
                                   title="<isa:translate key="access.button" arg0="<%=zgo_newuserBtnTxt%>" arg1=""/>" 
                                   class="button" name="<%=zgo_newuser%>"><isa:translate key="customer.login.jsp.znewuser"/></a>
                                <a href="#" 
                                   onclick="resetForm()" 
                                   title="<isa:translate key="customer.createuser.jsp.reset"/>" 
                                   class="button"
                                   name="reset"><isa:translate key="customer.createuser.jsp.reset"/></a>
                                <a href="<isa:webappsURL name="init.do"/>"
									title="<isa:translate key="access.button" arg0="<%=zgo_homepageBtnTxt%>" arg1=""/>" 
					            	class="button" name="<%=zgo_homepage%>"><isa:translate key="customer.confirmcreateuser.jsp.homepage"/></a>
<%-- GREENORANGE STOP --%> 
							</div>
						</li>
						<%
						if(ui.isAccessible()) {
							accessText = WebUtil.translate(pageContext, "um.clc.login.jsp.moduleHead", null);
						%>
						<a href="#grp1Begin"
						   id="grp1End"
						   title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>" 
						></a>	
						<%
						}
						%>
						<li>
							<div>&nbsp;</div>
						</li>
					</ul>    
					<isa:message id="UserError"
								 name="<%=UserActions.RC_USER%>"
								 type="<%=Message.ERROR%>"
								 ignoreNull="true">
						<div class="error">
							<span><%=UserError%></span>	
						</div>
					</isa:message>
					<% 
					if (ui.isAccessible) { %>
					<a href="#start-login" name="end-login" title="<isa:translate key="um.clc.login.jsp.startlogi"/>"></a>
					<% 
					} %>
				</div><%-- filter-result --%>
			</form>
		</div> <%-- selection --%>
	</body>
</html>