<%-- 
--%>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.user.action.UserActions" %>
<%@ page import="com.sap.isa.user.businessobject.UserBase" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>
<%@ page import="com.sap.isa.businessobject.Address" %>

<%@ page import="java.util.Collection" %>

<%  BaseUI ui = new BaseUI(pageContext);
	String accessText = ""; 
	
//--- GREENORANGE START ---//	
	String zgo_homepage ="zgo_homepage";
	String zgo_homepageBtnTxt = WebUtil.translate(pageContext, "customer.confirmcreateuser.jsp.homepage", null);
	
	//AddressFormular addressFormular = (AddressFormular)userSessionData.getAttribute("registering_address");
	AddressFormular addressFormular = (AddressFormular)request.getAttribute(ActionConstants.RC_ADDRESS_FORMULAR);
	
	/* read the address from the addressFormular */
	Address address = addressFormular.getAddress();
	
//--- GREENORANGE STOP ---//	
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
                       
                        
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<title><isa:translate key="startpage.header"/></title>

		<isa:stylesheets/>

		<script src="<%=WebUtil.getMimeURL(pageContext, "", "user/jscript/buttons.js") %>"
			type="text/javascript">
		</script>

	</head>
	
	<body class="header-body">
		<div class="module-name"><isa:moduleName name="user/customer/confirmCreateUser.jsp" /></div>
		<% 
		if (ui.isPortal) { %>
		<div id="header-portal">
		<% 
		} 
		else { %>
		<div id="Zheader-appl">
		<% 
		} %> 

			<div class="header-logo"></div>
			<div class="header-applname">
				<isa:translate key="startpage.header"/>
			</div>
			<div class="header-username"></div>

			<%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
			<%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
			<%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
				 so make sure to test your work in these browsers. --%>
			<div id="header-extradiv1">
				<span></span>
			</div>
			<div id="header-extradiv2">
				<span></span>
			</div>
			<div id="header-extradiv3">
				<span></span>
			</div>
			<div id="header-extradiv4">
				<span></span>
			</div>
			<div id="header-extradiv5">
				<span></span>
			</div>
			<div id="header-extradiv6">
				<span></span>
			</div>
		</div> <%-- header-appl --%>

		<div class="selection">
			<%-- 
			UserBase user = (UserBase) request.getAttribute(BusinessObjectBase.CONTEXT_NAME);
			if (user == null) {
				user = new UserBase();
			} --%>
	
			<%-- determine the browser version --%>
			<script type="text/javascript">
			<!--
				document.write('<input type="hidden" name="browsername" value="' +  escape(navigator.userAgent.toLowerCase()) + '">');
				document.write('<input type="hidden" name="browsermajor" value="' + escape(parseInt(navigator.appVersion)) + '">');
				document.write('<input type="hidden" name="browserminor" value="' + escape(parseFloat(navigator.appVersion)) + '">');
			//-->
			</script>

			<h1><isa:translate key="customer.confirmcreateuser.jsp.title"/></h1>
		
	 		<isa:message id="confirm" name="address" type="<%=Message.SUCCESS%>" property="" >
	 			<div class="info">
	        		<span><%= JspUtil.removeNull(confirm) %></span>
	        		<p><isa:translate key="customer.confirmcreateuser.jsp.text"/></p>
	        	</div>
	        </isa:message>
	        <div class="login">       
	        	<ul>
		        <li>
			        <div class="button1">
						<a href="<isa:webappsURL name="init.do"/>"
							title="<isa:translate key="access.button" arg0="<%=zgo_homepageBtnTxt%>" arg1=""/>" 
			            	class="button" name="<%=zgo_homepage%>"><isa:translate key="customer.confirmcreateuser.jsp.homepage"/>
			            </a>
					</div>
				</li>
				</ul>
			</div>
		</div> <%-- selection --%>	 
	</body>
</html>