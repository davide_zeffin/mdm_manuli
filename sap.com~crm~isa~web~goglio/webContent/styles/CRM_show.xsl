<?xml version="1.0"?>
<xsl:stylesheet id="CRM_show" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="counter" select="4"/>
	<xsl:variable name="constant_yes">yes</xsl:variable>
	<xsl:variable name="constant_no">no</xsl:variable>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<link rel="stylesheet" type="text/css" href="../styles/CRM_show_style.css"/>
			</HEAD>
			<BODY>
				<xsl:for-each select="//Condition_Determination | //Step | //Access | //Attributes | //Attribute ">
					<xsl:if test="position()=$counter">
						<TABLE cellspacing="5">
							<colgroup width="33%" span="3"/>
							<xsl:if test="name()='Condition_Determination'">
								<CAPTION ALIGN="LEFT">
									<NOBR>
										<xsl:value-of select="concat(./@PricingProcedure,': ',//Root/Legend/Label[@tagName = 'Condition_Determination' and @attributeName = 'Condition_Determination']/@attributeDescription,' ',./@TracerNo)"/>
									</NOBR>
								</CAPTION>
							</xsl:if>
							<xsl:if test="name()='Step'">
								<CAPTION ALIGN="LEFT">
									<NOBR>
										<xsl:if test="./@CondTypeName != '' and ./@CondTypeDesc != '' and ./@StepText != ''">
											<xsl:value-of select="concat(./@CondTypeName ,' (',./@CondTypeDesc,'): ',./@StepText)"/>
										</xsl:if>
										<xsl:if test="./@CondTypeName = '' or ./@CondTypeDesc = '' or ./@StepText = ''">
											<xsl:value-of select="concat(./@StepDesc, ' ',./@CondTypeName ,' ',./@CondTypeDesc,' ',./@StepText)"/>
										</xsl:if>
									</NOBR>
								</CAPTION>
							</xsl:if>
							<xsl:if test="name()='Access'">
								<CAPTION ALIGN="LEFT">
									<NOBR>
										<xsl:value-of select="./@CondTableDesc"/>
									</NOBR>
								</CAPTION>
							</xsl:if>
							<xsl:if test="name()='Attributes'">
								<CAPTION ALIGN="LEFT">
									<NOBR>
										<xsl:value-of select="./@AccessText"/>
									</NOBR>
								</CAPTION>
							</xsl:if>
							<xsl:if test="name()='Attribute'">
								<CAPTION ALIGN="LEFT">
									<NOBR>
										<xsl:value-of select="./@AttributeClassDesc"/>
									</NOBR>
								</CAPTION>
							</xsl:if>
							<!-- </TABLE> -->
							<xsl:for-each select="ancestor-or-self::node()">
								<xsl:for-each select="@*">
									<!--	<TABLE COLS="3"> -->
									<xsl:variable name="attributeName" select="name()"/>
									<xsl:variable name="showAttribute">
										<xsl:if test="$attributeName != 'TracerNo' 			and
                                  							  $attributeName != 'TracerTimeStamp' 		and
							                                $attributeName != 'PricingProcedureDesc' 	and
							                                $attributeName != 'CondTypeDesc'         	and
							                                $attributeName != 'CondTableDesc'            	and
							                                $attributeName != 'AccessSeqDesc' 		and
							                                $attributeName != 'StepDesc'                 	and
      											  $attributeName != 'StepText'                 	and
							                                $attributeName != 'AttributeClassDesc'  	and
                                                  				  $attributeName != 'AccessText' 			and
                                                  				  $attributeName != 'StepSuccess'   		and
												  $attributeName != 'AccessSuccess'   		and
                                                  				  $attributeName != 'AttributeIsConstant' ">
											<xsl:value-of select="$constant_yes"/>
										</xsl:if>
										<xsl:if test="$attributeName='AccessKnumh'   and
                                                . =  ''">
											<xsl:value-of select="$constant_no"/>
										</xsl:if>
									</xsl:variable>
									<!-- <TD><xsl:value-of select="concat($attributeName,':',$showAttribute,':',.)"/></TD> -->
									<xsl:if test="$showAttribute=$constant_yes">
										<TR VALIGN="LEFT">
											<!-- 1. column -->
											<xsl:for-each select="//Root/Legend/Label">
												<xsl:if test="$attributeName = ./@attributeName">
													<TD ALIGN="LEFT" id="ATTRDESC">
														<xsl:if test="./@attributeDescription != ''">
															<xsl:value-of select="./@attributeDescription"/>
														</xsl:if>
														<xsl:if test="./@attributeDescription = ''">
															<xsl:value-of select="./@attributeName"/>
														</xsl:if>
													</TD>
												</xsl:if>
											</xsl:for-each>
											<!-- 2. column -->
											<xsl:if test="$attributeName != 'StepTimeStamp' ">
												<TD ALIGN="LEFT">
													<xsl:value-of select="."/>
												</TD>
											</xsl:if>
											<xsl:if test="$attributeName = 'StepTimeStamp' ">
												<TD ALIGN="LEFT">
													<xsl:call-template name="formatDate">
														<xsl:with-param name="timestamp" select="."/>
													</xsl:call-template>
												</TD>
											</xsl:if>
											<!-- 3. column -->
											<xsl:if test="$attributeName != 'PricingProcedure'  	 and
		                          									$attributeName != 'CondTypeName'     	  and
										                           $attributeName != 'AccessSeqName'    	  and
								                                  	$attributeName != 'AttributeClassValue' 	and
										                           $attributeName != 'CondTableName'     ">
												<TD ALIGN="LEFT"/>
											</xsl:if>
											<xsl:if test="$attributeName = 'PricingProcedure' ">
												<xsl:for-each select="parent::*">
													<TD ALIGN="LEFT">
														<nobr>
															<xsl:value-of select="@PricingProcedureDesc"/>
														</nobr>
													</TD>
												</xsl:for-each>
											</xsl:if>
											<xsl:if test="$attributeName = 'CondTypeName' ">
												<xsl:for-each select="parent::*">
													<TD ALIGN="LEFT">
														<nobr>
															<xsl:value-of select="@CondTypeDesc"/>
														</nobr>
													</TD>
												</xsl:for-each>
											</xsl:if>
											<xsl:if test="$attributeName = 'AccessSeqName' ">
												<xsl:for-each select="parent::*">
													<TD ALIGN="LEFT">
														<nobr>
															<xsl:value-of select="@AccessSeqDesc"/>
														</nobr>
													</TD>
												</xsl:for-each>
											</xsl:if>
											<xsl:if test="$attributeName = 'AttributeClassValue' ">
												<xsl:for-each select="parent::*">
													<TD ALIGN="LEFT">
														<nobr>
															<xsl:value-of select="@AttributeClassDesc"/>
														</nobr>
													</TD>
												</xsl:for-each>
											</xsl:if>
											<xsl:if test="$attributeName = 'CondTableName' ">
												<xsl:for-each select="parent::*">
													<TD ALIGN="LEFT">
														<nobr>
															<xsl:value-of select="@CondTableDesc"/>
														</nobr>
													</TD>
												</xsl:for-each>
											</xsl:if>
										</TR>
									</xsl:if>
								</xsl:for-each>
							</xsl:for-each>
						</TABLE>
					</xsl:if>
				</xsl:for-each>
			</BODY>
		</HTML>
	</xsl:template>
	<xsl:template name="formatDate">
		<xsl:param name="timestamp" select="20010417000000"/>
		<xsl:variable name="year" select="substring($timestamp,1,4)"/>
		<xsl:variable name="month" select="substring($timestamp,5,2)"/>
		<xsl:variable name="day" select="substring($timestamp,7,2)"/>
		<xsl:if test="$timestamp != '' and
                  $timestamp != ' '">
			<xsl:variable name="dateFormat">
				<xsl:for-each select="//Root/Legend/Label">
					<xsl:if test="'DateFormat' = ./@attributeName ">
						<xsl:value-of select="./@attributeDescription"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="$dateFormat = 'dd.MM.yyyy'">
					<nobr>
						<xsl:value-of select="$day"/>
						<xsl:text>.</xsl:text>
						<xsl:value-of select="$month"/>
						<xsl:text>.</xsl:text>
						<xsl:value-of select="$year"/>
					</nobr>
				</xsl:when>
				<xsl:when test="$dateFormat = 'MM/dd/yyyy'">
					<nobr>
						<xsl:value-of select="$month"/>
						<xsl:text>/</xsl:text>
						<xsl:value-of select="$day"/>
						<xsl:text>/</xsl:text>
						<xsl:value-of select="$year"/>
					</nobr>
				</xsl:when>
				<xsl:when test="$dateFormat = 'MM-dd-yyyy'">
					<nobr>
						<xsl:value-of select="$month"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="$day"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="$year"/>
					</nobr>
				</xsl:when>
				<xsl:when test="$dateFormat = 'yyyy.MM.dd'">
					<nobr>
						<xsl:value-of select="$year"/>
						<xsl:text>.</xsl:text>
						<xsl:value-of select="$month"/>
						<xsl:text>.</xsl:text>
						<xsl:value-of select="$day"/>
					</nobr>
				</xsl:when>
				<xsl:when test="$dateFormat = 'yyyy/MM/dd'">
					<nobr>
						<xsl:value-of select="$year"/>
						<xsl:text>/</xsl:text>
						<xsl:value-of select="$month"/>
						<xsl:text>/</xsl:text>
						<xsl:value-of select="$day"/>
					</nobr>
				</xsl:when>
				<xsl:when test="$dateFormat = 'yyyy-MM-dd'">
					<nobr>
						<xsl:value-of select="$year"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="$month"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="$day"/>
					</nobr>
				</xsl:when>
				<xsl:otherwise>
					<nobr>
						<xsl:value-of select="$day"/>
						<xsl:text>.</xsl:text>
						<xsl:value-of select="$month"/>
						<xsl:text>.</xsl:text>
						<xsl:value-of select="$year"/>
					</nobr>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
