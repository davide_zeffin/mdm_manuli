<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/*">
		<root>
			<xsl:variable name="nameConditionDetermination">
				<xsl:for-each select="//Root/Legend/Label">
					<xsl:if test="'Condition_Determination' = ./@attributeName ">
						<xsl:value-of select="./@attributeDescription"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<!-- Alle Condition Determination Elemente -->
			<xsl:for-each select="//Condition_Determination">
				<xsl:variable name="s_condition" select="concat(./@PricingProcedure,': ',//Root/Legend/Label[@tagName = 'Condition_Determination' and @attributeName = 'Condition_Determination']/@attributeDescription,' ',./@TracerNo)"/>
				<xsl:element name="{name()}">
					<xsl:attribute name="TreeText"><xsl:value-of select="$s_condition"/></xsl:attribute>
					<xsl:for-each select="./child::Step">
						<xsl:variable name="s_step">
							<xsl:if test="./@CondTypeName =''">
								<xsl:value-of select="./@StepDesc"/>
							</xsl:if>
							<xsl:if test="./@CondTypeName !=''">
								<xsl:value-of select="concat(./@CondTypeName,' ',./@CondTypeDesc,' ',./@StepText)"/>
							</xsl:if>
						</xsl:variable>
						<xsl:element name="{name()}">
							<xsl:attribute name="Success"><xsl:value-of select="./@StepSuccess"/></xsl:attribute>
							<xsl:attribute name="TreeText"><xsl:value-of select="$s_step"/></xsl:attribute>
							<xsl:for-each select="./child::Access">
								<xsl:variable name="s_access" select="concat(./@CondTableDesc,' ' ,@AccessText) "/>
								<xsl:element name="{name()}">
									<xsl:attribute name="Success"><xsl:value-of select="./@AccessSuccess"/></xsl:attribute>
									<xsl:attribute name="TreeText"><xsl:value-of select="$s_access"/></xsl:attribute>
									  <xsl:for-each select="./child::Attributes">
									     <xsl:element name="{name()}">
									    	<xsl:attribute name="TreeText"><xsl:value-of select="./@AccessText"/></xsl:attribute>
									   		  <xsl:for-each select="./child::Attribute">
											     <xsl:variable name="s_attribute" select="concat(./@AttributeClassDesc, ' ',./@AttributeClassValue )"/>
							         			  	<xsl:element name="{name()}">
											          <xsl:attribute name="TreeText"><xsl:value-of select="$s_attribute"/></xsl:attribute>
											        </xsl:element>
										      <!-- Attribute -->
										      </xsl:for-each>
										      <!-- Attribute -->
										  </xsl:element>     
									  <!-- Attributes -->
									  </xsl:for-each>
									  <!-- Attributes -->	
								</xsl:element>
								<!-- Access -->
							</xsl:for-each>
							<!-- Access -->
						</xsl:element>
						<!-- Step -->
					</xsl:for-each>
					<!-- Step -->
				</xsl:element>
				<!-- Condition_Determination-->
			</xsl:for-each>
			<!-- Condition_Determination-->
		</root>
	</xsl:template>
</xsl:stylesheet>
