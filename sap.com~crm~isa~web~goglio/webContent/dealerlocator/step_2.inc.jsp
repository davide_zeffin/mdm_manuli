<%  String sumColumnSpan = "5";
    String accessText = WebUtil.translate(pageContext, "b2c.brndowner.step.1", null);
    String tabText = WebUtil.translate(pageContext, "b2c.step.inactivetab", null);
       if (ui.isOciBasketForwarding()) {
       	sumColumnSpan = "4";
       } %>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="step">
  <tbody>
    <tr>
       <td>
          <table border="0" cellspacing="0" cellpadding="0" width="100%">
        	<tbody>
	           <tr>
                  <td width="23" class="navPath"><img src="<%= WebUtil.getMimeURL(pageContext, "b2c/mimes/images/dec_ecke_navPath.gif") %>" alt="" width="23" height="26" border="0"></td>
                  <td width="2000" class="navPath">
                      <% if (!ui.isPartnerSearch() && !ui.getDealerLocator().isComingFromBasket()) { %>
                          <isa:translate key="b2c.navigationbar.storeLocator"/></td>
                      <% } else { %>
                          <isa:translate key="xbut_b2c.brndowner.srchres"/></td>
                      <% } %>
               </tr>
            </tbody>
          </table>		  
  	   </td>
    </tr>
    <% if ((ui.isCollaborativeShowroom()) && (ui.getDealerLocator().isComingFromBasket() || ui.isProductSearch())) { %>
	<tr>
      <td><br/>
        <table cellspacing="0" width="100%" border="0">
          <tr>  
	        <td><div class="step"><a href="<isa:webappsURL name="b2c/maintainBasket.do">
                                     <isa:param name="display" value="true"/>
                                  </isa:webappsURL>;" class="menuclass" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1="<%=tabText%>"/>" ><isa:translate key="b2c.brndowner.step.1"/></a></div></td>
	        <td tabindex="0" class="menuclass_selected" <% if (ui.isAccessible) { %> title="<isa:translate key="b2c.step.activetab"/>" <% } %> ><div class="step"><isa:translate key="b2c.brndowner.step.2"/></div></td>
	        <td tabindex="0" class="menuclass_next" <% if (ui.isAccessible) { %> title="<isa:translate key="b2c.step.unavailabletab"/>" <% } %> ><div class="step"><isa:translate key="b2c.brndowner.step.3"/></div></td>
	        <% if (ui.isOciBasketForwarding()) { %>
	          <td tabindex="0" class="menuclass_next" <% if (ui.isAccessible) { %> title="<isa:translate key="b2c.step.unavailabletab"/>" <% } %> ><div class="step"><isa:translate key="b2c.brndowner.step.4b"/></div></td>
         	<% } else { %> 	        
	          <td tabindex="0" class="menuclass_next" <% if (ui.isAccessible) { %> title="<isa:translate key="b2c.step.unavailabletab"/>" <% } %> ><div class="step"><isa:translate key="b2c.brndowner.step.4"/></div></td>
              <td tabindex="0" class="menuclass_next" <% if (ui.isAccessible) { %> title="<isa:translate key="b2c.step.unavailabletab"/>" <% } %> ><div class="step"><isa:translate key="b2c.brndowner.step.5"/></div></td>
	        <% } %>
	      </tr>
	      <tr>
	        <td colspan="<%= JspUtil.encodeHtml(sumColumnSpan) %>" height="8" bgcolor="#003366"></td>
	      </tr>
        </table><br/>
      </td>
    </tr>
    <% } %>	                       				  
  </tbody>
</table>   