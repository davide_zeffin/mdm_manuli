<%--
******************************************************************************** 
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.07.2004

    Display an generic maintenance of a dealer object.

    !! This JSP is part of the eCommerceBase branch, and with that is has to have
       references only to object available in that branch. !!
       
    $Revision: #1 $
    $Date: 2004/07/27 $
********************************************************************************
--%>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType/>         


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="dealerlocator.jsp.title"/></title>
       <isa:stylesheets theme=""/>    
  </head>
  <body class="partner-search">

	<%@ include file="/dealerlocator/searchdealer.inc.jsp" %>

  </body>
</html>