		<div>
			<input type="hidden" name="scrollForward" value="" />
			<input type="hidden" name="scrollBack" value="" />
			<input type="hidden" name="scrollStart" value="" />		
		</div>
<%-- Hier muss noch was getan werden --%>		
	<% if (ui.isPopupWindow() || ui.isScrollBar()) { %>
		<div class="buttons-scroll">
		<% if (ui.isAccessible) { %>
			<a href="#end-scroll-button" title="<isa:translate key="ecm.acc.sec.scroll.button"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
		<% } %>	      		
			<ul class="buttons-1">
		 <%  if ((ui.getDealerLocator().getHasMoreDealer().equalsIgnoreCase("X")) && (ui.getDealerLocator().getStartIndex() == 0)) { %>
				<li class="disabled"><a href="#" onclick="" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollSt.dis"/>" <% } %> ><isa:translate key="dealerlocator.jsp.startScroll"/></a></li> 
				<li class="disabled"><a href="#" onclick="" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollB.dis"/>" <% } %> ><isa:translate key="dealerlocator.jsp.back"/></a></li>
				<li><a href="#" onclick="Forward();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollF"/>" <% } %> ><isa:translate key="dealerlocator.jsp.forward"/></a></li>
		 <%  } else if ((ui.getDealerLocator().getHasMoreDealer().equalsIgnoreCase("X")) && (ui.getDealerLocator().getStartIndex() > 0) && (ui.getDealerLocator().getStartIndex() == ui.getDealerLocator().getMaxHitsPerPage())) { %>
				<li class="disabled"><a href="#" onclick="" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollSt.dis"/>" <% } %> ><isa:translate key="dealerlocator.jsp.startScroll"/></a></li> 
				<li><a href="#" onclick="Back();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollB"/>" <% } %> ><isa:translate key="dealerlocator.jsp.back"/></a></li>
				<li><a href="#" onclick="Forward();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollF"/>" <% } %> ><isa:translate key="dealerlocator.jsp.forward"/></a></li>
		 <%  } else if ((ui.getDealerLocator().getHasMoreDealer().equalsIgnoreCase("X")) && (ui.getDealerLocator().getStartIndex() > 0) && (ui.getDealerLocator().getStartIndex() > ui.getDealerLocator().getMaxHitsPerPage())) { %>
				<li><a href="#" onclick="Start();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollSt"/>" <% } %> ><isa:translate key="dealerlocator.jsp.startScroll"/></a></li> 
				<li><a href="#" onclick="Back();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollB"/>" <% } %> ><isa:translate key="dealerlocator.jsp.back"/></a></li>
				<li><a href="#" onclick="Forward();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollF"/>" <% } %> ><isa:translate key="dealerlocator.jsp.forward"/></a></li>
		 <%  } else if ((ui.getDealerLocator().getHasMoreDealer().equals("")) && (ui.getDealerLocator().getStartIndex() > ui.getDealerLocator().getMaxHitsPerPage())) { %>
				<li><a href="#" onclick="Start();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollSt"/>" <% } %> ><isa:translate key="dealerlocator.jsp.startScroll"/></a></li> 
				<li><a href="#" onclick="Back();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollB"/>" <% } %> ><isa:translate key="dealerlocator.jsp.back"/></a></li>
				<li class="disabled"><a href="#" onclick="" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollF.dis"/>" <% } %> ><isa:translate key="dealerlocator.jsp.forward"/></a></li>
		 <%  } else if ((ui.getDealerLocator().getHasMoreDealer().equals("")) && (ui.getDealerLocator().getStartIndex() == ui.getDealerLocator().getMaxHitsPerPage())) { %>
				<li><a href="#" onclick="Start();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollSt"/>" <% } %> ><isa:translate key="dealerlocator.jsp.startScroll"/></a></li> 
				<li><a href="#" onclick="Back();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollB"/>" <% } %> ><isa:translate key="dealerlocator.jsp.back"/></a></li>
				<li class="disabled"><a href="#" onclick="" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.scrollF.dis"/>" <% } %> ><isa:translate key="dealerlocator.jsp.forward"/></a></li>
		 <% } else { %>
				<li></li>		
		 <% } %>
			</ul>
		 <% if (ui.isPopupWindow()) { %>
		 	<ul class="buttons-3">
		 		<li><a href="javascript:window.close();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.button.close"/>" <% } %> ><isa:translate key="dealerlocator.jsp.win.close"/></a></li>
		 	</ul>
		 <% } %>
		<% if (ui.isAccessible) { %>
			<a name="end-scroll-button" title="<isa:translate key="ecm.acc.sec.end.scroll.button"/>."></a>
		<% } %>						 
		</div> <%-- End scroll-buttons --%>
	<% } %>				    