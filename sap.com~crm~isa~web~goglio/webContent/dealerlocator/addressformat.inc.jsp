    <% int line = 0; %>
			<div class="result">
				<ul class="result-list">
    			<isa:iterate id="dealer" name="<%= ActionConstants.RC_DEALER_LIST %>"
                	type="com.sap.isa.core.util.table.ResultData">				
					<li class="<%= evenOdd[++line % 2]%>">
						<ul class="address">
							<%-- Name --%>
							<li class="name">
							<% if (ui.isTitleAvailable(dealer)) { %>
								<%= ui.getTitle(dealer) %>
							<% }
          					   if (ui.isPerson(dealer)) { %>
                   				<%= ui.getPersonName(dealer) %>
          					<% } else if (ui.isCompany(dealer)) { %>
                 				<%= ui.getCompanyName(dealer) %>
          					<% } %>
							</li>
							<%-- End Name --%>
							<%-- Street and Housenumber --%>
							<li>
								<% if (ui.isStreetAvailable(dealer)) { %>
									<%= ui.getStreet(dealer) %>
								<% }
								   if (ui.isHouseNrAvailable(dealer)) { %>
									<%= ui.getHouseNr(dealer) %>
								<% } %>
							</li>
							<%-- End Street and Housenumber --%>
							<%-- specific address format --%>
							<% if (ui.getAddressFormular().getAddressFormatId() == 0) { %>
			 					<%@ include file="/dealerlocator/addressformat_default.inc.jsp" %>
	        				<% } else { %>
			 					<%@ include file="/dealerlocator/addressformat_us.inc.jsp" %>
		     				<% } %> 
		     				<%-- End specific address format --%>
						</ul>
						<ul class="telecomunication">
							<%-- Phone --%>
							<li>
								<% if (ui.isPhoneAvailable(dealer) || ui.isPhoneExtensionAvailable(dealer)) { %>
     								<isa:translate key="dealerlocator.jsp.dealerPhone"/>:&nbsp;
     								<% if (ui.isPhoneAvailable(dealer)) { %>
     									<%= ui.getPhone(dealer) %>
     								<% }
     							   	   if (ui.isPhoneExtensionAvailable(dealer)) { %>
     									<%= ui.getPhoneExtension(dealer) %>
      								<% }
								   } %>
							</li>
							<%-- End Phone --%>
							<%-- Fax --%>
							<li>
								<% if (ui.isFaxAvailable(dealer) || ui.isFaxExtensionAvailable(dealer)) { %>
     	 							<isa:translate key="dealerlocator.jsp.dealerFax"/>:&nbsp;
     								<% if (ui.isFaxAvailable(dealer)) { %>
     									<%= ui.getFax(dealer) %>
     								<% }
     							   	   if (ui.isFaxExtensionAvailable(dealer)) { %>
     									<%= ui.getFaxExtension(dealer) %>
      								<% }
      							   } %>
							</li>
							<%-- End Fax --%>
							<%-- E-Mail --%>
							<li>
								<% if (ui.isEmailAvailable(dealer)) { %>
	              					<isa:translate key="dealerlocator.jsp.dealerEMail"/>:<a href="mailto:<%= ui.getEmail(dealer) %>">&nbsp;<%= JspUtil.encodeHtml(ui.getEmail(dealer)) %></a>  
	              				<% } %>
							</li>
							<%-- End E-Mail --%>
						</ul>
						<ul class="links">
							<%-- Homepage --%>
							<li>
								<% if (ui.isURLHomePageAvailable(dealer)) { %>
									<a href="#" onclick="openWindow('<%= ui.getURLHomePage(dealer) %>');"><isa:translate key="dealerlocator.jsp.dealerHomepage"/></a>
								<% } %>
							</li>
							<%-- End Homepage --%>
							<%-- Map --%>
							<li>
								<% if (ui.isURLMapAvailable(dealer)) { %>
	   								<a href="#" onclick="openWindow('<%= ui.getURLMap(dealer) %>');"><isa:translate key="dealerlocator.jsp.dealerJourney"/></a>
	   							<% } %>
							</li>
							<%-- End Map --%>
						</ul>
						<% if(ui.isCollaborativeShowroom() && ui.getDealerLocator().isComingFromBasket()) { %>
						<ul class="buttons-1">
          					<% if ((dealer.getString(DealerLocator.IN_ASSORTMENT).equals("B")) || (dealer.getString(DealerLocator.IN_ASSORTMENT).equals("C"))) {
          	       				if(ui.isPartnerAvailabilityInfoAvailable()) {
                					String trafficLight[] = ui.getTrafficLight((List) dealer.getObject(DealerLocator.SCHEDLIN_LINE));
                					String path = "b2c/mimes/images/" + trafficLight[0]; %>
                				<li>
              						<img src="<%= WebUtil.getMimeURL(pageContext,  path) %>" alt="<%= trafficLight[1] %>" height="13" width="41" border=0 align="middle" />&nbsp;
              					</li>
          					<% } 
               				  } %>
               			   <li>
               			   <% if (ui.getDealerLocator().getItemTechKey() != null && ui.getDealerLocator().getItemTechKey().length() > 0) { %>
                      				<a href="<isa:webappsURL name="/b2c/basketaddsoldfrom.do"/>?com.sap.isa.isacore.action.b2c.orderMaintainBasket.B2CAddSoldFromAction.soldFromTechKey=<%= dealer.getString(DealerLocator.BPGUID) %>&amp;com.sap.isa.isacore.action.b2c.orderMaintainBasket.B2CAddSoldFromAction.itemTechKey=<%= ui.getDealerLocator().getItemTechKey() %>"   
          	   			   <% } else { %>
                      				<a href="<isa:webappsURL name="/b2c/basketaddsoldfrom.do"/>?com.sap.isa.isacore.action.b2c.orderMaintainBasket.B2CAddSoldFromAction.soldFromTechKey=<%= dealer.getString(DealerLocator.BPGUID) %>" 
          	   			   <% } 
               				  if(ui.isOciBasketForwarding()) { %>
                      				title="<isa:translate key="dealerloc.jsp.partnershop.sel"/>">
            			   <% } else { %>
                      				title="<isa:translate key="dealerloc.jsp.partner.sel"/>">
            			   <% } %>
            					<isa:translate key="dealerloc.jsp.select.partner"/></a>
							
							</li>
						</ul>
						<% } %>						
					</li>
				</isa:iterate>
				</ul>
			</div> <%-- End result --%>
