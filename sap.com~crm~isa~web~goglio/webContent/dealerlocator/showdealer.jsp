<%--
**************************************************************************************
    Display an generic maintenance of a dealer object.

    The maintenance object consists of property groups and every group consists
    of properties.
    The iterate tag is used to loop over groups and properties.
***************************************************************************************    
--%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.dealerlocator.businessobject.*" %>
<%@ page import="com.sap.isa.dealerlocator.action.ActionConstants" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.AddressFormularBase" %>
<%@ page import="com.sap.isa.isacore.uiclass.PartnerLocatorUI" %>

<% String[] evenOdd = new String[] { "even", "odd" }; 
   PartnerLocatorUI ui = new PartnerLocatorUI(pageContext); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="dealerlocator.jsp.title"/></title>
       <isa:stylesheets />
  </head>
	<body class="partnerLocBody" onload="javascript:self.focus()">
	  <table width="90%" cellpadding="0" cellspacing="0" border="0">
  		<tr>
    		<td>
    		<%-- show errors concern the maintenance --%>
    		<div class="error">
    		<isa:message id="errortext" name="<%=ActionConstants.RC_DEALER%>" type="<%=Message.ERROR%>">
    			<%=errortext%> <br/>
    		</isa:message>
    		</div>
    		</td>
  		</tr>
  		<tr>
  			<td valign="top">
  				<table class="partnerlist2" width="100%" border="0" cellspacing="0" cellpadding="5">
   					<tr>
     					<td class="odd"><div class="bold">
              			<% if (!ui.getDealer().getFirstName().equals("") || !ui.getDealer().getLastName().equals("")    ) { %>
                            <%= ui.getDealer().getTitle() %>&nbsp;<%= ui.getDealer().getFirstName() %>&nbsp;<%= ui.getDealer().getLastName() %>
              			<% } else { %>
              				<%= ui.getDealer().getTitle() %>&nbsp;<%= ui.getDealer().getName1() %>&nbsp;<%= ui.getDealer().getName2() %><br/>
              				<%= ui.getDealer().getName3() %>&nbsp;<%= ui.getDealer().getName4() %>
              			<% } %>
              			</div>
              			</td>
    				</tr>
    				<tr>
    					<td valign="top">
    					<%= ui.getDealer().getStreet() %>&nbsp;<%= ui.getDealer().getHouseNumber() %><br/>
    					<%-- specific address format --%>
    					<% if (ui.getAddressFormular().getAddressFormatId() == 0) { %>
	   						<%= ui.getDealer().getZipCode() %>&nbsp;<%= ui.getDealer().getCity() %><br/>
	   						<%-- Region --%>       
	   						<% if (!ui.getDealer().getRegionText().equals("")) { %>		     
								<%= ui.getDealer().getRegionText() %> / 
	   						<% } %>
					        <%-- Country --%>       
	   						<% if (!ui.getDealer().getCountryText().equals("")) { %>	       
								<%= ui.getDealer().getCountryText() %><br/><br/>
	   						<% } 
	   					   } else { %>
	   							<%= ui.getDealer().getCity() %>&nbsp;
	   						<% if (!ui.getDealer().getRegion().equals("")) { %>		     
        						<%= ui.getDealer().getRegion() %>
	   						<% } %>&nbsp;
	   							<%= ui.getDealer().getZipCode() %><br/>
	   						<%-- Country --%>       
	   						<% if (!ui.getDealer().getCountryText().equals("")) { %>	       
								<%= ui.getDealer().getCountryText() %><br/><br/>
	   						<% } 
       					   } %>
     					<%-- Phone --%>
      					<% if (!ui.getDealer().getPhone().equals("")) { %>       
     						<isa:translate key="dealerlocator.jsp.dealerPhone"/>: <%= ui.getDealer().getPhone() %>&nbsp;<%= ui.getDealer().getPhoneExtension() %><br/>
      					<% } %>
      					<%-- Fax --%>
      					<% if (!ui.getDealer().getFax().equals("")) { %> 
     	 					<isa:translate key="dealerlocator.jsp.dealerFax"/>: <%= ui.getDealer().getFax() %>&nbsp;<%= ui.getDealer().getFaxExtension() %><br/>
      					<% } %>
     					<%-- E-Mail --%>
      					<% if (!ui.getDealer().getEMail().equals("")) { %>
              				<isa:translate key="dealerlocator.jsp.dealerEMail"/>: <a href="mailto:<%= ui.getDealer().getEMail() %>"><%= ui.getDealer().getEMail() %></a><br/>  
      					<% } %>
      					<%-- Homepage --%>
      					<% if (!ui.getDealer().getURLHomepage1().equals("")) { %>
      						<% if (ui.getDealer().getURLHomepage1().startsWith("http://")) { %>
      							<isa:translate key="dealerlocator.jsp.dealerVisit"/> <a href="<%= ui.getDealer().getURLHomepage1() %> <%= ui.getDealer().getURLHomepage2() %> <%= ui.getDealer().getURLHomepage3() %> <%= ui.getDealer().getURLHomepage4() %>" target="_blank"> <isa:translate key="dealerlocator.jsp.dealerHomepage"/> </a><br/>
      						<% } else { %>
      							<isa:translate key="dealerlocator.jsp.dealerVisit"/> <a href="http://<%= ui.getDealer().getURLHomepage1() %> <%= ui.getDealer().getURLHomepage2() %> <%= ui.getDealer().getURLHomepage3() %> <%= ui.getDealer().getURLHomepage4() %>" target="_blank"> <isa:translate key="dealerlocator.jsp.dealerHomepage"/></a><br/>
      						<% } 
      					   } %> <br/>
      					</td>
     				</tr>
     				<tr>
      					<td>
      					<%-- Map --%> 
      					<% if (!ui.getDealer().getURLMap1().equals("")) { 
          					   if (ui.getDealer().getURLMap1().startsWith("http://")) { %> 
          					   		<a href="<%= ui.getDealer().getURLMap1() %> <%= ui.getDealer().getURLMap2() %> <%= ui.getDealer().getURLMap3() %> <%= ui.getDealer().getURLMap4() %>" target="_blank"> <isa:translate key="dealerlocator.jsp.dealerJourney"/></a><br/>
          					<% } else { %>
          							<a href="http://<%= ui.getDealer().getURLMap1() %> <%= ui.getDealer().getURLMap2() %> <%= ui.getDealer().getURLMap3() %> <%= ui.getDealer().getURLMap4() %>" target="_blank"> <isa:translate key="dealerlocator.jsp.dealerJourney"/></a><br/>
          					<% }
          				   } %>
          				</td>
          			</tr>  					   
     				<tr>
      					<td align="right">
        					<p>
        					<a href="javascript:self.close()" class="FancyLinkGrey"><isa:translate key="dealerlocator.jsp.close"/></a>
        					</p>
      					</td>
     				</tr>
     			</table>
    		</td>
   		</tr>
	  </table>	
	</body>
</html>