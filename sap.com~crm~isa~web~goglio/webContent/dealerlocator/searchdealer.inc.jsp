<%--
******************************************************************************** 
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      02.12.2004

    Display an generic maintenance of a dealer object.

    !! This JSP is part of the eCommerceBase branch, and with that is has to have
       references only to object available in that branch. !!
       
    $Revision: #1 $
    $Date: 2004/12/02 $
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>  
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.core.ContextConst" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>

<%@ page import="com.sap.isa.dealerlocator.action.ActionConstants" %>
<%@ page import="com.sap.isa.dealerlocator.businessobject.DealerLocator" %>
<%@ page import="com.sap.isa.dealerlocator.backend.boi.DealerLocatorData" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.isacore.AddressFormularBase" %> 
<%@ page import="com.sap.isa.isacore.uiclass.PartnerLocatorUI" %>

<% String[] evenOdd = new String[] { "odd", "even" }; 
   PartnerLocatorUI ui = new PartnerLocatorUI(pageContext); 
   String foundUsage = "";
%>
   
        <script type="text/javascript">
        	<%@ include file="/appbase/jscript/urlrewrite.inc"  %>
        </script>

	    <script type="text/javascript">
			<%@ include file="/ecombase/jscript/addressdetails.js.inc.jsp" %>
    	</script>     
    
	<div class="module-name"><isa:moduleName name="dealerlocator/searchdealer.inc.jsp" /></div>
<% int index;
   // flag that indicates whether the user represents a person or an organization
   boolean isPerson; %>
	<% if (ui.isCollaborativeShowroom()) { 
		   if (ui.isAccessible) { %>
			<a href="#end-step-by-step" title="<isa:translate key="ecm.acc.sec.step-by-step"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
		<% } %>
		<div class="step-by-step">
	    	<%@ include file="/dealerlocator/step_2.inc.jsp" %>
		</div> <%-- End step-by-step --%>
		<% if (ui.isAccessible) { %>
	 		<a name="end-step-by-step" title="<isa:translate key="ecm.acc.sec.end.step-by-step"/>."></a>
	 	<% }     
	   } %>

<div class="partner-locator" >
    <%-- Accesskey for the search section. --%>
    <a id="access-search" href="#access-result" title="<isa:translate key="storeloc.acc.store.title"/>" accesskey="<isa:translate key="storeloc.acc.search.key"/>"></a>
<% if ((ui.isB2B()) && (ui.isPopupWindow() || ui.isPortal)) {
	   if (ui.isPortal) { %>
		<div id="header-portal">
	<% } else { %>
		<div id="header-appl">
	<% } %>  
	
		<div class="header-logo">
		</div>

		<div class="header-applname">
			<isa:translate key="dealerlocator.jsp.title"/>
		</div>

		<div class="header-username">
			<span></span>
		</div>

	<%-- These extra divs/spans may be used as catch-alls to add extra imagery. 
		 Add a background image to each and use width and height to control sizing, place with absolute positioning
		 There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
		 so make sure to test your work in these browsers. --%>
		<div id="header-extradiv1">
			<span></span>
		</div>
		<div id="header-extradiv2">
			<span></span>
		</div>
		<div id="header-extradiv3">
			<span></span>
		</div>
		<div id="header-extradiv4">
			<span></span>
		</div>
		<div id="header-extradiv5">
			<span></span>
		</div>
		<div id="header-extradiv6">
			<span></span>
		</div>
	</div> <%-- header-appl --%>
<% } %>
  <% if (!ui.isProductSearch()) { %>
  	<form method="get" action='<isa:webappsURL name="dealerlocator/searchdealer.do"/>' id="addressform">		
  <% } else { %>
  	<form method="get" action="<isa:webappsURL name="dealerlocator/searchpartnerforproduct.do">
  				                   <isa:param name="comeFromBasket" value="true"/>
	                           </isa:webappsURL>" id="addressform">
  <% } %>
	<div class="content" >
	
		<div class="filter">
		  <%--  S E A R C H - Fields --%> 
		  <% if (ui.isAccessible) { %>
		  	<a href="#end-search" title="<isa:translate key="ecm.acc.sec.search"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
		  <% } %>
			<table class="filter">
				<tr class="title">
					<td colspan="2">
					<% if (ui.isCollaborativeShowroom() && ui.getDealerLocator().isComingFromBasket()) { 
						if (ui.isProductSearch()) { %>
							<isa:translate key="dealerloc.jsp.searchPartForProd" arg0="<%= JspUtil.encodeHtml(ui.getProduct()) %>" />
					 <%	} else { %>
							<isa:translate key="dealerloc.jsp.searchPartForBask"/>
					 <% } 
                 	   } %>
					</td>                 	
				</tr>
			<% if (ui.isRelationshipSelectable() &&
					(!ui.getDealerLocator().isComingFromBasket() && !ui.isPartnerSearch())) { %>
				<tr>
					<td class="identifier">
						<label for="relation"><isa:translate key="dealerlocator.jsp.searchRelation"/>:</label>
					</td>
					<td class="value">
					<select class="selectDealer" name="relation" id="relation" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.relship"/>" <% } %> >
		        	<isa:iterate id="reltypes" name="<%= ActionConstants.RC_RELATIONSHIPS %>"
		                     type= "com.sap.isa.core.util.table.ResultData" resetCursor="true">
		             	<option value="<%= reltypes.getString(DealerLocatorData.RELTYPE) %>"
		          	<% // Should the current relationship be marked as selected?
		            	 if  (reltypes.getString(DealerLocatorData.RELTYPE).equals(ui.getDealerLocator().getRelation())) { 
							foundUsage = ui.getUsage(reltypes.getString(DealerLocatorData.USAGE)); %>
		             	    selected="selected"
		          	<% } %>		             
		               		><%= JspUtil.encodeHtml(ui.getUsage(reltypes.getString(DealerLocatorData.USAGE))) %>
		             	</option>
		        	</isa:iterate>
                    </select>
					</td>
				</tr>
			<% } %>	
				<%-- Name of the dealer --%>			
				<tr>
					<td class="identifier">
						<label for="dealerName"><isa:translate key="dealerlocator.jsp.searchName"/>:</label>
					</td>
					<td class="value">
						<input type="text" class="textinput-large" maxlength="30"
                           				name="dealerName" id="dealerName"
                           				value="<%= JspUtil.encodeHtml(ui.getDealerLocator().getName())%>"/>
					</td>
				</tr>
				<%-- Zip-Code --%>
				<tr>
					<td class="identifier">
						<label for="dealerZipCode"><isa:translate key="dealerlocator.jsp.searchZipCode"/>:</label>
					</td>
					<td class="value">
						<input type="text" class="textinput-small" maxlength="10"
                           				name="dealerZipCode" id="dealerZipCode"
                           				value="<%= JspUtil.encodeHtml(ui.getDealerLocator().getZipCode())%>"/>
					</td>
				</tr>
				<%-- City --%>
				<tr>
					<td class="identifier">
						<label for="dealerCity"><isa:translate key="dealerlocator.jsp.searchCity"/>:</label>
					</td>
					<td class="value">
						<input type="text" class="textinput-large" maxlength="30"
                           				name="dealerCity" id="dealerCity"
                           				value="<%= JspUtil.encodeHtml(ui.getDealerLocator().getCity())%>"/>
					</td>
				</tr>
				<%-- Country --%>
				<tr>
					<td class="identifier">
						<label for="dealerCountry"><isa:translate key="dealerlocator.jsp.searchCountry"/>:</label>
					</td>
					<td class="value">
			      <% // The result of the following decision will be passed as a parameter of the 'send_countryChange'
			         // JavaScript function which is defined within the file 'addressdetails_maintenance.js'.
			         String fromCountry = "F";
			         if (ui.getPossibleRegions() != null) {
			             fromCountry = "T";
			         } %>					
						<select class="select" name="dealerCountry" id="dealerCountry" onchange="send_countryChange('<%= fromCountry %>')" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.country"/>" <% } %> >
				        <% // build up the selection options 
				           String defaultCountryId ="";
				           if (ui.getDealerLocator().getCountry().equals(""))  {
					   			defaultCountryId = "";
				           } else {
				                defaultCountryId = JspUtil.replaceSpecialCharacters(ui.getDealerLocator().getCountry()); 
				           }%>
			        		<isa:iterate id="country" name="<%= ActionConstants.RC_POSSIBLE_COUNTRIES %>"
			             		        type= "com.sap.isa.core.util.table.ResultData" resetCursor="true">
			             		<option value="<%= country.getString("ID") %>"
			          		<% // Should the current country be marked as selected?
			             		if  (country.getString("ID").equals(defaultCountryId)) { %>
			               		  selected="selected"
			          		<% } %>		             
			               		><%= JspUtil.encodeHtml(country.getString("DESCRIPTION")) %>
			             		</option>
			        		</isa:iterate>
		        		<% if (defaultCountryId.equals("")) { %>
		        			<option value="<%= defaultCountryId %>" selected="selected">
		        	   			<isa:translate key="dealerlocator.jsp.allCountry"/>
		        			</option>
		        		<% } else {%>   
		        			<option value="">
		        	   			<isa:translate key="dealerlocator.jsp.allCountry"/>
		        			</option>   
		        		<% } %>         	     	            	     
						</select>
		      			<script type="text/javascript">
		        			<% index = 0; %>    
		        			<isa:iterate id="country" name="<%= ActionConstants.RC_POSSIBLE_COUNTRIES %>"
		             			type= "com.sap.isa.core.util.table.ResultData" resetCursor="true">
		          			<% // fill a JavaScript array to remember which country has regions that has to be displayed
		             			String arrayValue = "F";
		             			if (country.getBoolean("REGION_FLAG") == true) {
		                 			arrayValue = "T"; 
		             			} %>
		          				toCountry[<%= index++ %>] = "<%= arrayValue %>";
		        			</isa:iterate>
		      			</script>						
					</td>
				</tr>
				<%-- Regions --%>
			<% if ((ui.getPossibleRegions() != null) &&
					 (ui.getPossibleRegions().getNumRows() > 0) &&
			 		   (!ui.getDealerLocator().getCountry().equals(""))) { %>																
				<tr>
					<td class="identifier">
						<label for="dealerRegion"><isa:translate key="dealerlocator.jsp.searchRegion"/>:</label>
					</td>
					<td class="value">
						<select class="select" name="dealerRegion" id="dealerRegion" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.region"/>" <% } %> >
		             		<% // build up the selection options           
		                	String defaultRegionId ="";
		                	if (ui.getDealerLocator().getRegion().equals("")) {
			         			defaultRegionId = "";
		             		} else {
		             			defaultRegionId = JspUtil.replaceSpecialCharacters(ui.getDealerLocator().getRegion()); 
		             		} %>      
		             		<isa:iterate id="region" name="<%= ActionConstants.RC_POSSIBLE_REGIONS %>"
		                         	type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
		                  		<option value="<%= region.getString("ID")%>" 
		               			<% // Should the current region be marked as selected?
		                  		   if  (region.getString("ID").equals(defaultRegionId)) { %>
		                      			selected="selected"
		               			<% } %>
		                    	   ><%= JspUtil.encodeHtml(region.getString("DESCRIPTION"))%>
		                  		</option>
		             		</isa:iterate>
		          			<% if (defaultRegionId.equals("")) { %>
		              			<option value="<%= defaultRegionId %>" selected="selected">
		                    		<isa:translate key="dealerlocator.jsp.allRegion"/>
		        				</option>
		         			<% } else {%>   
		              			<option value="">
		                    		<isa:translate key="dealerlocator.jsp.allRegion"/>
		              			</option>   
			     			<% } %>       
						</select>
					</td>
				</tr>
			<% } %>				
			</table>
			<% if (ui.isAccessible) { %>
				<a name="end-sarch" title="<isa:translate key="ecm.acc.sec.end.search"/>."></a>
			<% } %>
			<% if (ui.isAccessible) { %>
				<a href="#end-search-button" title="<isa:translate key="ecm.acc.sec.search.button"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
		  	<% } %>
			<div class="buttons-search">
				<ul class="buttons-2">
					<li><a href="#" onclick="send_search();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.buttons.search"/>" <% } %> ><isa:translate key="dealerlocator.jsp.startSearch"/></a></li>
					<li><a href="#" onclick="send_searchAll();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.buttons.search.all"/>" <% } %> ><isa:translate key="dealerlocator.jsp.searchAll"/></a></li>
					<li><a href="#" onclick="send_reset();" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.buttons.reset"/>" <% } %> ><isa:translate key="dealerlocator.jsp.reset"/></a></li>
				</ul>
			<% if (ui.isCollaborativeShowroom() && ui.getDealerLocator().isComingFromBasket()) { %>
				<ul class="buttons-2">
					<li><a href="<isa:webappsURL name="b2c/maintainBasket.do">
                                     <isa:param name="display" value="true"/>
                                 </isa:webappsURL>;" <% if (ui.isAccessible) { %> title="<isa:translate key="storeloc.acc.buttons.basket"/>" <% } %> ><isa:translate key="xbut_b2c.brndowner.tobasket"/></a></li>
				</ul>
			<% } %>
			</div> <%-- End buttons --%>
			<% if (ui.isAccessible) { %>
				<a name="end-sarch-button" title="<isa:translate key="ecm.acc.sec.end.search.button"/>."></a>
			<% } %>			
			<div>
		   	<% if (ui.getDealerLocator() != null &&
                   ui.getDealerLocator().getRelationShips() != null &&
                   ui.getDealerLocator().getRelationShips().getNumRows() == 0) { %>
		   		<input type="hidden" name="relation" value="<%= ui.getDealerLocator().getRelation() %>" />
		   	<% } %>	
		   		<input type="hidden" name="relationChange"  value="" />    
		        <input type="hidden" name="countryChange"  value="" />
		        <input type="hidden" name="Reset" value="" />
		        <input type="hidden" name="StartSearch" value="" />
		        <input type="hidden" name="SearchAll" value="" />
		        <%-- hidden field: regionRequired --%>
		        <% if (ui.getAddressFormular().isRegionRequired()) { %>
		        	<input type="hidden" name="regionRequired" value="true" />
		        <% } else { 
		   	        // the empty string will be interpreted as 'false' by the request parser %>
		            <input type="hidden" name="regionRequired" value="" />
		        <% } %> 
		    </div>       
		</div> <%-- End filter --%>
		<%--  R E S U L T S  --%>
		<a id="access-result" href="#access-end" title= "<isa:translate key="storeloc.acc.result.title"/>" accesskey="<isa:translate key="storeloc.acc.result.key"/>"></a>
		<%-- include the specific address format --%>
		<% if (ui.getDealerList() != null) {
			String size = "" + ui.getDealerList().getNumRows();  %>
			<% if (ui.isAccessible) { %>
				<a href="#end-message" title="<isa:translate key="ecm.acc.sec.message"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
		  	<% } %>							
			<div class="filter-msg">
			<% String errorMessage = ui.getMessageText(Message.ERROR);
			   String infoMessage = ui.getMessageText(Message.INFO);
			   if (errorMessage != null && errorMessage.length() > 0) { %>
       			  <div class="error">
    			     <span><%= JspUtil.encodeHtml(errorMessage) %></span>
				  </div> <%-- End error --%>
			<% } else { %>
				<div class="hits">
					<span>
						<% if (foundUsage != null && foundUsage.length() == 0) { %>
							<% foundUsage = "dealerloc.jsp.relation." + ui.getDealerLocator().getRelation().toLowerCase().trim(); 
							   foundUsage = ui.getUsageDescr(foundUsage);
							   if (ui.isCollaborativeShowroom() && 
							   		(ui.getDealerLocator().isComingFromBasket() || ui.isPartnerSearch())) { 
							   			foundUsage = "dealerloc.jsp.partner";
										foundUsage = ui.getUsageDescr(foundUsage);
							   }
						   } 
						   if (ui.isScrollBar()) { %>
							<isa:translate key="dealerlocator.jsp.resultDisp" arg0="<%= size %>" arg1="<%= JspUtil.encodeHtml(foundUsage)%>"/>
						<% } else { %>
							<isa:translate key="dealerlocator.jsp.resultText1" arg0="<%= size %>" arg1="<%= JspUtil.encodeHtml(foundUsage)%>"/>
						<% } %>
					</span>
				</div> <%-- End hits --%> 
				<% if ((ui.getDealerList().getNumRows() == 0) && (ui.getDealerLocator().isSelectionCriteria()))  { %>
					<div class="info">
		      			<span><isa:translate key="dealerloc.jsp.result.Expl1" arg0="<%= JspUtil.encodeHtml(foundUsage)%>"/></span><br/>
        			</div> <%-- End info --%>
        		<% } 
        		   if (infoMessage != null && infoMessage.length() > 0) { %>
        			<div class="info">
    			    	<span><%= JspUtil.encodeHtml(infoMessage) %></span>
					</div> <%-- End info --%>
				<% }
			   } %>
			</div> <%-- End filter-msg --%>
			<% if (ui.isAccessible) { %>
				<a name="end-message" title="<isa:translate key="ecm.acc.sec.end.message"/>."></a>
			<% } %>						
		  	<% if (ui.getDealerList().getNumRows() > 0) {
		  		   if (ui.isAccessible) { %>
					<a href="#end-result" title="<isa:translate key="ecm.acc.sec.result"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
		  		<% } %>
				<%@ include file="/dealerlocator/addressformat.inc.jsp" %>
				<% if (ui.isAccessible) { %>
					<a name="end-result" title="<isa:translate key="ecm.acc.sec.end.result"/>."></a>
				<% }
		       }
		   } %>
	</div> <%-- End content --%> 
		<%@ include file="/dealerlocator/dealerlocatorscroll.inc.jsp" %>
	</form>
    <a id="access-end" href="#access-search" title="<isa:translate key="storeloc.acc.end"/>" accesskey="<isa:translate key="storeloc.acc.end.key"/>"></a>     
</div> <%-- End partner-locator --%>
