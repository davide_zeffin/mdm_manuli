							<%-- City, Region and ZipCode --%>
							<% if (ui.isZipCodeAvailable(dealer) || ui.isCityAvailable(dealer) || ui.isRegionTextAvailable(dealer)) { %>
							<li>
          						<% if (ui.isCityAvailable(dealer)) { %>
									<%= ui.getCity(dealer) %>
          						<% } 
          						   if (ui.isRegionTextAvailable(dealer)) { %>
        	           				<%= ui.getRegionText(dealer) %>
								<% }
          						   if (ui.isZipCodeAvailable(dealer)) { %>
                   					<%= ui.getZipCode(dealer) %>
          						<% } %>
							</li>
							<% } %>
							<%-- End City, Region and ZipCode --%>
							<%-- Country --%>
							<% if (ui.isCountryAvailable(dealer)) { %>
							<li>
                   				<%= ui.getCountry(dealer) %>
							</li>
							<% } %>
							<%-- End Country --%>