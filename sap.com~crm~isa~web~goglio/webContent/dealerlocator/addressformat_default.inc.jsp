							<%-- ZipCode and City --%>
							<% if (ui.isZipCodeAvailable(dealer) || ui.isCityAvailable(dealer)) { %>
							<li>
          						<% if (ui.isZipCodeAvailable(dealer)) { %>
                   					<%= ui.getZipCode(dealer) %>
          						<% }
          						   if (ui.isCityAvailable(dealer)) { %>
									<%= ui.getCity(dealer) %>
          						<% } %>
							</li>
							<% } %>
							<%-- End ZipCode and City --%>
							<%-- Country --%>
							<% if (ui.isCountryAvailable(dealer)) { %>
							<li>
                   				<%= ui.getCountry(dealer) %>
							</li>
							<% } %>
							<%-- End Country --%>
							<%-- Regiontext --%>
							<% if (ui.isRegionTextAvailable(dealer)) { %>
							<li>
                   				<%= ui.getRegionText(dealer) %>
							</li>
							<% } %>
							<%-- End Regiontext --%>