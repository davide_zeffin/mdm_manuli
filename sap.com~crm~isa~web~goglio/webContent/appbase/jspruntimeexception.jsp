<%--
********************************************************************************
    File:         jspruntimeexception.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      19.4.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/28 $
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page isErrorPage="true" %>

<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.MessageDisplayer" %>
<%@ page import="com.sap.isa.ui.uiclass.MessageUI" %>

<% MessageUI ui = new MessageUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title></title>
  <isa:stylesheets/>
</head>

<body class="message-page">

  <div class="message-content">
    <div class="module-name"><isa:moduleName name="appbase/jspruntimeexception.jsp" /></div>

    <h1 class="fw-msg-err-title"><isa:translate key="msg.error.runtimeexception"/></h1>

    <div class="fw-msg-area">
        <div class="error">
            <p><span><strong>URL</strong>: <%= JspUtil.encodeHtml(HttpUtils.getRequestURL(request).toString()) %></span></p>
            <p><span><%= JspUtil.encodeHtml(exception.getMessage()) %></span></p>
        </div>
    </div>
    
    <div class="fw-msg-area">
        <div class="info">
            <span><isa:translate key="msg.runtimeexception.friendly" /></span>
        </div>
    </div>

    <pre>
    <% MessageDisplayer.printStackTrace((Exception)exception,out,request); %>
    </pre>

  </div>
</body>
</html>
<%-- Include logic to invalidate the session. --%>
<%@ include file="/user/logon/session_invalidate.inc" %>

