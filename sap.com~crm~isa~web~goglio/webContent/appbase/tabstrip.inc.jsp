<%-- Function: generic functionality to render a tabstrip --%>
<%-- Type: static --%>
<%-- Interface: TabStripHelper tabStrip --%>

<%-- the end of the div container and the ul tag must be set in the calling page!! --%>
<div id="organizer-navigation">
  <ul class="navigation-1">

    <%-- The following java coding loops over all existings tabstrip entries --%>
    <%

    TabStripHelper.TabButton nextButton;
    TabStripHelper.TabButton tabButton;
    int stripSize = tabStrip.size();
    String strstripSize = Integer.toString(stripSize);
    int index = 0;

    while (tabStrip.hasNext()) {
      index++;
      tabButton = tabStrip.next();

      String className ="";
      String usedImage ="";
      String imageWidth ="";

      /* active button */
      if (tabStrip.isActiveButton()) {

        /* the following java coding determines the correct image for the transition to the next button */
        if (tabStrip.isLastButton()) {
          /* set class */
          className = "active-last";
        }
        else {
           /* set class */
          className = "active-middle";
        }

        /* active tab menu entry  */
        if (tabStrip.isFirstButton()) {
          /* set class */
          className = "active-first";
        }

        String tabItemTxt = WebUtil.translate(pageContext, tabButton.getText(), null);
        String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);

        %>
        <li class="<%=className%>">
            <a href="#" title="<isa:translate key="access.tab.selected" arg0="<%=tabItemTxt%>" arg1="<%=Integer.toString(index)%>" arg2="<%=strstripSize%>" arg3="<%=unavailableTxt%>" />"
             ><isa:translate key="<%=tabButton.getText()%>" /></a>
        </li>
      <%
      }
      /* inactive button */
        else {
          /* inactive menu entry  */
          
          /* the following java coding determines the correct image for the transition to the next button */
          if (tabStrip.isLastButton()) {
            className = "inactive-last";
          }
          else {
            /* current tab is not the last tab in row */
            className = "inactive-middle";
          }

          if (tabStrip.isFirstButton()) {
            /* set class */
            className = "inactive-first";
          }

          String tabItemTxt = WebUtil.translate(pageContext, tabButton.getText(), null);

        %>

        <%-- cell with tab text --%>
        <li class="<%=className%>">
          <a href="<%=tabButton.getLink()%>" title="<isa:translate key="access.tab.unselected" arg0="<%=tabItemTxt%>" arg1="<%=Integer.toString(index)%>" arg2="<%=strstripSize%>" arg3="" /> "
            ><isa:translate key="<%=tabButton.getText()%>" />
          </a>
        </li>
      <%
      } //if
    } // while   %>

