<%-- A generic page to diplay messages, which use the DisplayMessage object --%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>


<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.MessageUI" %>

<%
MessageUI ui = new MessageUI(pageContext);

if (ui.displaySpecialMessage() != null) { %>
  <%= ui.displaySpecialMessage() %>
<%
return; } %>


  <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComLogin.js") %>"
          type="text/javascript">
  </script>

  <script type="text/javascript">
  <!--
    function goBack() {
      history.back();
    }

    function login() {
      loginInTopFrame("isaTop","<isa:reentryURL name="<%= ui.getInitAction() %>"/>");
    }

    function winClose() {
      window.close();
    }

    function submitForm() {
      document.forms["form"].submit();
    }

    function generateBackButton() {
        var title = '';
      if (history.length == 0) {
        <%
        if (ui.isAccessible) { %>
              title="<isa:UCtranslate key="error.jsp.close"/>"
            <%
            } %>
        document.write('<a class="button" href="javascript:winClose();"' + title + '><isa:UCtranslate key="error.jsp.close"/></a>')
      } else {
        <%
        if (ui.isAccessible) { %>
              title="<isa:UCtranslate key="error.jsp.back"/>"
            <%
            } %>
        document.write('<a class="button" href="javascript:goBack()"' + title + '><isa:UCtranslate key="error.jsp.back"/></a>')
      }
    }
   -->
  </script>

  <div id="message-content">
    <div class="module-name"><isa:moduleName name="appbase/message.jsp" /></div>

	<% ui.includeSimpleHeader(); %>

    <%
    if (ui.showErrorPageHeader()) { %>
      <h1 class="fw-msg-err-title"><isa:translate key="error.jsp.head"/></h1>
    <%
    }%>
    <%
    if (ui.isApplicationError) { %>
      <h1 class="fw-msg-err-title"><isa:translate key="error.jsp.application"/></h1>
    <%
    }%>
    <%
    if (ui.isMessageDisplayerAvailable) { %>

      <%-- Iterate over exception messages --%>
      <div class="fw-msg-area">
        <isa:message id="messagetext" type="<%=Message.ERROR%>" name="<%=MessageDisplayer.CONTEXT_NAME%>" ignoreNull="true">
          <div class="error">
            <span><%=JspUtil.encodeHtml(messagetext)%></span>
          </div>
        </isa:message>
      </div>
      <div class="fw-msg-area">
        <isa:message id="messagetext" type="<%=Message.WARNING%>" name="<%=MessageDisplayer.CONTEXT_NAME%>" ignoreNull="true">
          <div class="info">
            <span><%=JspUtil.encodeHtml(messagetext)%></span>
          </div>
        </isa:message>
      </div>
      <div class="fw-msg-area">
        <isa:message id="messagetext" type="<%=Message.INFO%>" name="<%=MessageDisplayer.CONTEXT_NAME%>" ignoreNull="true">
          <div class="info">
            <span><%=JspUtil.encodeHtml(messagetext)%></span>
          </div>
        </isa:message>
      </div>
      <div class="fw-msg-area">
        <isa:message id="messagetext" type="<%=Message.SUCCESS%>" name="<%=MessageDisplayer.CONTEXT_NAME%>" ignoreNull="true">
          <div class="info">
            <span><%=JspUtil.encodeHtml(messagetext)%></span>
          </div>
        </isa:message>
      </div>  
      <%
    }%>

    <isa:ifPortal>
      <div class="fw-msg-area">
        <div class="info"><span><isa:translate key="msg.error.loginPortal" /></span></div>
      </div>  
    </isa:ifPortal>

  </div>

  <div id="buttons">
    <ul class="buttons-1">
    <%
    if (ui.isMessageDisplayerAvailable) { %>
      <%
      String action = ui.getMessageAction();
      if (action.length()>0) {
        String forward = ui.messageDisplayer.getForward();
        %>
        <form name="form" action="<isa:webappsURL name="<%=action%>">
           <%
           for (int i = 0; i < ui.getParameterList().size(); i++) {
             String[] parameter = (String[])ui.getParameterList().get(i); %>
             <isa:param  name="<%=parameter[0]%>"  value="<%=parameter[1]%>" />
           <%
           }%>
           </isa:webappsURL>" method="POST">
            <input type="hidden" name="forward" value="<%=forward%>" >
            <li><a class="button" href="javascript:submitForm();" <% if (ui.isAccessible) { %> title="<isa:translate key="error.jsp.continue"/>" <% } %>
            ><isa:translate key="error.jsp.continue"/></a></li>
        </form>
      <%
      }
      else {
        if (ui.messageDisplayer.isBackAvailable()) { %>
         <li>
         <script type="text/javascript">
            generateBackButton();
         </script>
         </li>
        <%
        }
      }

      if (ui.messageDisplayer.isLoginAvailable()) { %>
        <isa:ifPortal value="false">
          <li><a class="button" href="javascript:login();" <% if (ui.isAccessible) { %> title="<isa:translate key="error.jsp.continue"/>" <% } %>
          ><isa:translate key="msg.error.login"/></a></li>
          <%-- Include logic to invalidate the session. --%>
		  <%@ include file="/user/logon/session_invalidate.inc" %>
        </isa:ifPortal>
      <%
      }
    }%>
    </ul>
  </div>
