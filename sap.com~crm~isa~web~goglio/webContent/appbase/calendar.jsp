<%--
********************************************************************************
	File:         /appbase/calendar.jsp
	Copyright (c) 2001, SAP LLCS Europe GmbH, Germany, All rights reserved.
	Author:       i802842                                                       
	Created:      20041101
	Version:      1.0
	$Revision: #1 $
	$Date: 2004/11/01 $
********************************************************************************
pass 3 URL parameters, these are not required
dateFormat, using java.text.SimpleDateFormat formats, the default is 'yyyyyMMdd'
dateValue, based in the format, the default is the current browser date
weekDay, start day from the month table, the default is Sunday
set  1 javascript variable, this is required
dateField, the HTML document input field to fill with the selected value
this is how to call the calendar
<a  class="icon" href="#" 
	onClick="calendarWindow = window.open('<isa:webappsURL name="/appbase/calendar.jsp"/>'+'?dateFormat='+document.orderForm.dateFormat.value+'&dateValue='+document.orderForm.dateField.value,'calendarWindow','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes');
		 calendarWindow.dateField=document.orderForm.dateField;
		 calendarWindow.focus();"
><img src="<%=WebUtil.getMimeURL(pageContext, "/mimes/images/calendar.gif") %>" width="16" height="16" alt="Choose date" border="0" class="display-image"
></a> 
--%>
<%@page import="java.util.Collection" %>
<%@page import="java.util.Vector" %>
<%@page import="java.util.Date" %>
<%@page import="java.util.Calendar" %>
<%@page import="java.util.GregorianCalendar" %>
<%@page import="java.text.DateFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="com.sap.isa.core.util.*" %>
<%@page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%!
// return the date if the format works 
private Calendar testDateFormat(String dateFormat, String dateValue)
{
	Calendar date = null;
	DateFormat dateFormatter = null;
	// check that the format is OK, else use default
	if (!(null==dateFormat || dateFormat.length()==0))
	try
	{
		dateFormatter = new SimpleDateFormat(dateFormat);
		dateFormatter.setLenient(false);
	}
	catch(Exception calendarEx1)
	{
		// no a format
		return date;
	}
	else
	{
		// no a format
		return date;
	}
	// if the format is OK, try to get the date.
	if (dateFormatter!= null && !(null==dateValue || dateValue.length()==0))
	try
	{
		Date parsedDate = dateFormatter.parse(dateValue);
		date = new GregorianCalendar();
		date.setTime(parsedDate);
	}
	catch(Exception calendarEx2){}
	return date;
}
// return the date if the format works 
// and check if the value is equal to the text return by the format
private Calendar checkDateFormat(String dateFormat, String dateValue)
{
	Calendar date = testDateFormat(dateFormat,dateValue);
	if (null == date )
		return date;
	DateFormat dateFormatter = null;
	// check that the format is OK, else use default
	if (!(null==dateFormat || dateFormat.length()==0))
	try
	{
		dateFormatter = new SimpleDateFormat(dateFormat);
		dateFormatter.setLenient(false);
	}
	catch(Exception calendarEx1)
	{
		// no a format
		return date;
	}
	else
	{
		// no a format
		return date;
	}
	// if the format is OK, check the date.
	if (dateFormatter!= null)
	try
	{
		String dateToString = dateFormatter.format(date);
		// check that the date value is the same that 
		// the date as a string. if it is set it, if not the check fails
		if (!dateToString.equalsIgnoreCase(dateValue))
		{
			return null;
		}
	}
	catch(Exception calendarEx2){}
	return date;
}
// return the date using one format, 
// gets the right day, month, year sequence to try
// using the original separator, not others separators are used.
private Calendar tryDateFormat(String dateFormat, String dateValue)
{
	Calendar date = null;
	String myValue = new String(dateValue);
	myValue = myValue.trim();
	myValue = myValue.replaceAll(" ", ""); 
	String myFormat =  new String(dateFormat);
	String [] separatorMarkers =  {"/",":",",",".","-"};
	String separatorMarker =  "";
	String [] dayMarkers =  {"d","dd"};
	String [] monthMarkers =  {"M","MM"};
	String [] yearMarkers =  {"yy","yyyy"};
	String [] marker1 =  null;
	String [] marker2 =  null;
	String [] marker3 =  null;
	int i1= 0;
	int i2= 0;
	int i3= 0;
	int i4= 0;
	// get separator
	int indexSeparator =-1;
	for (i1=0;i1<separatorMarkers.length;i1++)
	{
		indexSeparator= myFormat.indexOf(separatorMarkers[i1]);
		if ( -1 < indexSeparator)
		{
			separatorMarker = separatorMarkers[i1];
			break;
		}
	}
	// check if there is separator marker, that the marker is in the value
	// if not then the value isn't a valid date
	if (separatorMarker.length()>0)
	{
		indexSeparator = myValue.indexOf(separatorMarker);
		if (indexSeparator==-1)
			return null;
	}
	// get sequence
	int indexDay = myFormat.indexOf('d');
	int indexMonth = myFormat.indexOf('M');
	int indexYear = myFormat.indexOf('y');
	// get marker1
	if (marker1 == null && (indexDay < indexMonth && indexDay < indexYear))
		marker1 = dayMarkers;
	if (marker1 == null && (indexMonth < indexDay && indexMonth < indexYear))
		marker1 = monthMarkers;
	if (marker1 == null && (indexYear < indexDay && indexYear < indexMonth))
		marker1 = yearMarkers;
	// get marker2
	if (marker2 == null && 
	   (
	   (indexMonth<indexDay && indexDay<indexYear)||
	   (indexMonth>indexDay && indexDay>indexYear)
	   ))
		marker2 = dayMarkers;
	if (marker2 == null && 
	   (
	   (indexDay<indexMonth && indexMonth<indexYear)||
	   (indexDay>indexMonth && indexMonth>indexYear)
	   ))
		marker2 = monthMarkers;
	if (marker2 == null && 
	   (
	   (indexDay<indexYear && indexYear<indexMonth)||
	   (indexDay>indexYear && indexYear>indexMonth)
	   ))
		marker2 = yearMarkers;
	// get marker3
	if (marker3 == null && (indexDay > indexMonth && indexDay > indexYear))
		marker3 = dayMarkers;
	if (marker3 == null && (indexMonth > indexDay && indexMonth > indexYear))
		marker3 = monthMarkers;
	if (marker3 == null && (indexYear > indexDay && indexYear > indexMonth))
		marker3 = yearMarkers;
	// try sequence
	try
	{
		// test sequence
		for (i2=0;i2<marker1.length;i2++)
			for (i3=0;i3<marker2.length;i3++)
				for (i4=0;i4<marker3.length;i4++)
		{
			// test sequence
			String testFormat = marker1[i2]+separatorMarker+marker2[i3]+separatorMarker+marker3[i4];
			date = testDateFormat(testFormat, myValue);
			if (null!= date)
				return date;
		}
	}
	catch(Exception calendarEx1)
	{
		// no a format
	}
	return date;
}
// return the date that works with different formats.
private Calendar tryDateFormats(String dateValue)
{
	dateValue = dateValue.trim();
	dateValue= dateValue.replaceAll(" ", ""); 
	Calendar date = null;
	String dateFormat =  null;
	String [] separatorMarker =  {"/",":",",",".","-",""};
	String [] day =  {"dd","d"};
	String [] month =  {"MM","M"};
	String [] year =  {"yyyy","yy"};
	int i1= 0;
	int i2= 0;
	int i3= 0;
	int i4= 0;
	// test day, month, year permutations
	for (i1=0;i1<separatorMarker.length;i1++)
		for (i2=0;i2<day.length;i2++)
			for (i3=0;i3<month.length;i3++)
				for (i4=0;i4<year.length;i4++)
	{
		// test 1, day, month, year
		dateFormat = day[i2]+separatorMarker[i1]+month[i3]+separatorMarker[i1]+year[i4];
		date = testDateFormat(dateFormat, dateValue);
		if (null!=date)
			return date;
		// test 2, month, day, year
		dateFormat = month[i3]+separatorMarker[i1]+day[i2]+separatorMarker[i1]+year[i4];
		date = testDateFormat(dateFormat, dateValue);
		if (null!=date)
			return date;
		// test 3, month, year, day
		dateFormat = month[i3]+separatorMarker[i1]+day[i2]+separatorMarker[i1]+year[i4];
		date = testDateFormat(dateFormat, dateValue);
		if (null!=date)
			return date;
		// test 4, year, month, day
		dateFormat = year[i4]+separatorMarker[i1]+month[i3]+separatorMarker[i1]+day[i2];
		date = testDateFormat(dateFormat, dateValue);
		if (null!=date)
			return date;
		// test 5, year, day, month
		dateFormat = year[i4]+separatorMarker[i1]+day[i2]+separatorMarker[i1]+month[i3];
		date = testDateFormat(dateFormat, dateValue);
		if (null!=date)
			return date;
		// test 6, day, year, month
		dateFormat = day[i2]+separatorMarker[i1]+year[i4]+separatorMarker[i1]+month[i3];
		date = testDateFormat(dateFormat, dateValue);
		if (null!=date)
			return date;
	}
	return date;
}

%>
<%
BaseUI calendarUI = new BaseUI(pageContext);
// CALENDAR COLORS
String topBackground    = "#336699";         // BG COLOR OF THE TOP FRAME
String bottomBackground = "#E7EEF0";         // BG COLOR OF THE BOTTOM FRAME
String tableBGColor     = "#336699";         // BG COLOR OF THE BOTTOM FRAME'S TABLE
String cellColor        = "#E7EEF0";     // TABLE CELL BG COLOR OF THE DATE CELLS IN THE BOTTOM FRAME
String cellBGColor      = "#E7EEF0";     // TABLE CELL BG COLOR OF THE DATE CELLS IN THE BOTTOM FRAME
String headingCellColor = "#336699";         // TABLE CELL BG COLOR OF THE WEEKDAY ABBREVIATIONS
String headingTextColor = "#FFFFFF";         // TEXT COLOR OF THE WEEKDAY ABBREVIATIONS
String dateColor        = "blue";          // TEXT COLOR OF THE LISTED DATES (1-28+)
String focusColor       = "#ff0000";       // TEXT COLOR OF THE SELECTED DATE (OR CURRENT DATE)
String hoverColor       = "darkred";       // TEXT COLOR OF A LINK WHEN YOU HOVER OVER IT
String fontStyle        = "12pt arial, helvetica";           // TEXT STYLE FOR DATES
String headingFontStyle = "10pt arial, helvetica";      // TEXT STYLE FOR WEEKDAY ABBREVIATIONS	
//months
Vector months = new Vector();
months.add(WebUtil.translate(pageContext, "appbase.calendar.jan", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.feb", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.mar", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.apr", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.may", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.jun", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.jul", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.aug", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.sep", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.oct", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.nov", null));  
months.add(WebUtil.translate(pageContext, "appbase.calendar.dec", null));  
//weekdays, starts from sunday to saturday
Vector weekdays = new Vector();
weekdays.add(WebUtil.translate(pageContext, "appbase.calendar.sunday", null));  
weekdays.add(WebUtil.translate(pageContext, "appbase.calendar.monday", null));  
weekdays.add(WebUtil.translate(pageContext, "appbase.calendar.tuesday", null));  
weekdays.add(WebUtil.translate(pageContext, "appbase.calendar.wednesday", null));  
weekdays.add(WebUtil.translate(pageContext, "appbase.calendar.thursday", null));  
weekdays.add(WebUtil.translate(pageContext, "appbase.calendar.friday", null));  
weekdays.add(WebUtil.translate(pageContext, "appbase.calendar.saturday", null));  
// domain relaxation?
String domainRelaxation = JspUtil.encodeHtml(request.getParameter("relaxDomain"));
boolean relaxDomain = false;
if (domainRelaxation != null && domainRelaxation.equals("T")){
    relaxDomain = true;
}

// embedded display?
String embeddedDisplay = JspUtil.encodeHtml(request.getParameter("embedded"));
boolean embedded = false;
if (embeddedDisplay != null && embeddedDisplay.equals("T")){
    embedded = true;
}
// id of the div-area for the embedded calendar
String divId = JspUtil.encodeHtml(request.getParameter("divId"));

String dateFieldId = JspUtil.encodeHtml(request.getParameter("dateFieldId"));

// default SAP date format
String defaultFormat = "yyyyMMdd";
String dateFormat = JspUtil.encodeHtml(request.getParameter("dateFormat"));
String dateValue = JspUtil.encodeHtml(request.getParameter("dateValue"));
int weekDay = 0;//0 Sunday....
DateFormat dateFormatter = null;
// check that the format is OK, else use default
if (!(null==dateFormat || dateFormat.length()==0))
try
{
	dateFormatter = new SimpleDateFormat(dateFormat);
}
catch(Exception calendarEx1)
{
	// default SAP date format
	dateFormat = defaultFormat;
}
else
{
	// default SAP date format
	dateFormat = defaultFormat;
}
// check that the date is OK, else use default today date in the client browser
Calendar date = null;
if (date == null && !defaultFormat.equals(dateFormat))
{
	date = 	testDateFormat(dateFormat, dateValue);
	/*
	%><%="testDateFormat(dateFormat, dateValue) = "+date%><br><%
	*/
}
if (date == null && !defaultFormat.equals(dateFormat))
{
	date = 	tryDateFormat(dateFormat, dateValue);
	/*
	%><%="tryDateFormat(dateFormat, dateValue) = "+date%><br><%
	*/
}
if (date == null)
{
	date = 	checkDateFormat(defaultFormat, dateValue);
	/*
	%><%="checkDateFormat(defaultFormat, dateValue) = "+date%><br><%
	*/
}
/*
if (date == null)
{
	date = 	tryDateFormat(defaultFormat, dateValue);
	//
	%><%="tryDateFormat(defaultFormat, dateValue) = "+date%><br><%
	//
}
if (date == null)
{
	date = 	tryDateFormats(dateValue);
	//
	%><%="tryDateFormats(dateValue) = "+date%><br><%
	//
}
*/
//
// try to get the weekday
String weekDayValue = request.getParameter("weekDay");
if (weekDayValue != null)
try
{
	weekDay =  Integer.parseInt(weekDayValue);
	// make it a number from 0-6
	weekDay = weekDay%7;
	// rotate weekdays if the weekday is not 0, sunday
	if (weekDay>0)
	{
		Vector newdays = new Vector(7);
		for (int i = weekDay; i<7; i++ )
			newdays.add(weekdays.get(i));
		for (int i = 0; i<weekDay; i++ )
			newdays.add(weekdays.get(i));
		weekdays = newdays;
	}
}
catch(Exception calendarEx1)
{
}
//
// check for time input
boolean hasHour   = false;
int minHour = 1;
int maxHour = 12;
boolean hasMinute = false;
boolean hasAMPM   = false;
//H  Hour in day (0-23)  
if (dateFormat.indexOf('H')!=-1)
{
	hasHour = true;
	minHour = 0;
	maxHour = 23;
}
//k  Hour in day (1-24) 
if (dateFormat.indexOf('k')!=-1)
{
	hasHour = true;
	minHour = 1;
	maxHour = 24;
}
//K  Hour in am/pm (0-11) 
if (dateFormat.indexOf('K')!=-1)
{
	hasHour = true;
	minHour = 0;
	maxHour = 11;
}
//h  Hour in am/pm (1-12)  
if (dateFormat.indexOf('h')!=-1)
{
	hasHour = true;
	minHour = 1;
	maxHour = 12;
}
if (dateFormat.indexOf('m')!=-1)
{
	hasMinute = true;
}
if (dateFormat.indexOf('a')!=-1)
{
	hasAMPM = true;		
}
boolean hasTime   = hasHour|hasMinute|hasAMPM;
%>
<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
<head>
	<title><isa:translate key="appbase.calendar.title"/></title>
	<style>
		TD.heading { text-decoration: none; color:<%= headingTextColor%>; font: <%=headingFontStyle %>; }
		.focusDay { color: <%= focusColor %>; text-decoration: none; font: <%= fontStyle %>; }
		.focusDay:hover { color: <%= focusColor %>; text-decoration: none; font: <%= fontStyle %>; }
		.weekDay { color: <%= dateColor %>; text-decoration: none; font: <%=  fontStyle %> ; }
		.weekDay:hover { color: <%= hoverColor %>; font: <%= fontStyle %>; }
	</style>
	<script type="text/javascript" 
		src="<isa:webappsURL name="/mimes/jscript/EComBase.js"/>"
		noscript="<isa:translate key="text.script.ecombase"/>"
	>	
	</script>    
	<script type="text/javascript"
		noscript="<isa:translate key="appbase.calendar.js1"/>"
	>
	<%
	// relax the domain if necessary
	if (relaxDomain){
	%>	
	var posBehindFirstDot = location.hostname.indexOf(".")+1;
	if (posBehindFirstDot > 0) {
		document.domain = location.hostname.substr(posBehindFirstDot);
	}
	<%
	}
	%>	

	// global date
	var calendarDate = new Date();
	<%
	if (null!=date)
	{
		int year = date.get(Calendar.YEAR);
		int month = date.get(Calendar.MONTH);
		int day = date.get(Calendar.DAY_OF_MONTH);
		int hour = date.get(Calendar.HOUR);
		int minute = date.get(Calendar.MINUTE);
	%>
	// set date
	calendarDate.setYear(<%=year%>);
	calendarDate.setMonth(<%=month%>);
	calendarDate.setDate(<%=day%>);
	<%
	if (date.get(Calendar.AM_PM) == Calendar.AM)
	{
	%>
	calendarDate.setHours(<%=hour%>);
	<%
	}
	else
	{
	%>
	calendarDate.setHours(<%=hour+12%>);
	<%
	}
	%>
	calendarDate.setMinutes(<%=minute%>);
	<%
	}
	%>
	var calendarDay  = calendarDate.getDate();
	var calendarWeekDay = "<%=weekDay%>";
	var calendarDateFormat = "<%=dateFormat%>";
	//
	var monthArray = new Array
	(
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.jan", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.feb", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.mar", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.apr", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.may", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.jun", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.jul", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.aug", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.sep", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.oct", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.nov", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.dec", null)) %>'
	);
	var weekdayList = new Array
	(
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.sunday", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.monday", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.tuesday", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.wednesday", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.thursday", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.friday", null)) %>',
		'<%= JspUtil.encodeHtml(WebUtil.translate(pageContext, "appbase.calendar.saturday", null)) %>'
	);    
	// variable that should be define using parameter for now we keep it this this
	// CALENDAR COLORS
	topBackground    = "<%=topBackground%>";         // BG COLOR OF THE TOP FRAME
	bottomBackground = "<%=bottomBackground%>";         // BG COLOR OF THE BOTTOM FRAME
	tableBGColor     = "<%=tableBGColor%>";         // BG COLOR OF THE BOTTOM FRAME'S TABLE
	cellColor        = "<%=cellColor%>";     // TABLE CELL BG COLOR OF THE DATE CELLS IN THE BOTTOM FRAME
	cellBGColor      = "<%=cellBGColor%>";     // TABLE CELL BG COLOR OF THE DATE CELLS IN THE BOTTOM FRAME
	headingCellColor = "<%=headingCellColor%>";         // TABLE CELL BG COLOR OF THE WEEKDAY ABBREVIATIONS
	headingTextColor = "<%=headingTextColor%>";         // TEXT COLOR OF THE WEEKDAY ABBREVIATIONS
	dateColor        = "<%=dateColor%>";          // TEXT COLOR OF THE LISTED DATES (1-28+)
	focusColor       = "<%=focusColor%>";       // TEXT COLOR OF THE SELECTED DATE (OR CURRENT DATE)
	hoverColor       = "<%=hoverColor%>";       // TEXT COLOR OF A LINK WHEN YOU HOVER OVER IT
	fontStyle        = "<%=fontStyle%>";           // TEXT STYLE FOR DATES
	headingFontStyle = "<%=headingFontStyle%>";      // TEXT STYLE FOR WEEKDAY ABBREVIATIONS	
	// FORMATTING PREFERENCES
	bottomBorder  = false;        // TRUE/FALSE (WHETHER TO DISPLAY BOTTOM CALENDAR BORDER)
	tableBorder   = 0;            // SIZE OF CALENDAR TABLE BORDER (BOTTOM FRAME) 0=none
	// DETERMINE BROWSER BRAND
	var isNav = false;
	var isIE  = false;

	// ASSUME IT'S EITHER NETSCAPE OR MSIE
	if (navigator.appName == "Netscape") 
	{
		isNav = true;
	}
	else 
	{
		isIE = true;
	}
	//
	// SET THE INITIAL VALUE OF THE GLOBAL DATE FIELD
	// START THE CALENDAR USE
	function init() 
	{
		document.forms[0].month.selectedIndex = calendarDate.getMonth();
		document.forms[0].year.value = calendarDate.getFullYear();
<%
if (hasTime)
{
%>
		// SET THE TIME
		var hour = calendarDate.getHours();
		var minute = calendarDate.getMinutes();
		var ampmField = "pm";
<%
if (hasHour && hasAMPM)
{
%>		if (hour == 0) 
		{
			hour = 12;
			ampmField = "am";
		}
		else if (hour < 12) 
		{
			ampmField = "am";
		}
		else if (hour > 12) 
		{
			hour -= 12;
			ampmField = "pm";
		}
		else 
		{
			ampmField = "pm";
		
		}
<%
}
%>
<%
if (hasHour)
{
%>
		document.forms[0].hourlist.value = hour;
<%
}
%>
<%
if (hasMinute)
{
%>
		document.forms[0].minlist.value = minute;
<%
}
%>
<%
if (hasAMPM)
{
%>
		document.forms[0].ampmlist.value = ampmField;
<%
}
%>
<%
}
%>
		writeCalendar();
	}
	//
	// REDRAW THE CALENDAR
	// fill the calendar cells with the days depending on current year and month
	function writeCalendar()
	{
		// GET MONTH, AND YEAR FROM GLOBAL CALENDAR DATE
		month   = calendarDate.getMonth();
		year    = calendarDate.getFullYear();
		// GET GLOBALLY-TRACKED DAY VALUE (PREVENTS JAVASCRIPT DATE ANOMALIES)
		day     = calendarDay;
		var i   = 0;
		// DETERMINE THE NUMBER OF DAYS IN THE CURRENT MONTH
		var days = getDaysInMonth();
		// IF GLOBAL DAY VALUE IS > THAN DAYS IN MONTH, HIGHLIGHT LAST DAY IN MONTH
		if (day > days) 
		{
			day = days;
		}
		// DETERMINE WHAT DAY OF THE WEEK THE CALENDAR STARTS ON
		var firstOfMonth = new Date (year, month, 1);
		// GET THE DAY OF THE WEEK THE FIRST DAY OF THE MONTH FALLS ON
		var startingPos  = firstOfMonth.getDay();
		// ROTATE TO THE CALENDAR WEEK DAY
		if (startingPos >= calendarWeekDay)
			startingPos = startingPos - calendarWeekDay;
		else
			startingPos = 7 - calendarWeekDay;		
		days += startingPos;
		// KEEP TRACK OF THE COLUMNS, START A NEW ROW AFTER EVERY 7 COLUMNS
		var columnCount = 0;
		// MAKE BEGINNING NON-DATE CELLS BLANK
		for (i = 0; i < startingPos; i++) 
		{
			cell = getElement('cell_'+columnCount);
            cell.innerHTML = "&nbsp;&nbsp;&nbsp;";
            // cell.innerHTML = "<p class='weekDay'>"+" &nbsp;&nbsp;&nbsp; "+"</p>"; // leads to wrong layout in FireFox
			//cell.innerHTML = '&nbsp;&nbsp;&nbsp;';
			columnCount++;
		}  
		// SET VALUES FOR DAYS OF THE MONTH
		var currentDay = 0;
		var dayType    = "weekday";
		var dayColor   =  dateColor;
		// DATE CELLS CONTAIN A NUMBER
		for (i = startingPos; i < days; i++) 
		{
			// GET THE DAY CURRENTLY BEING WRITTEN
			currentDay = i-startingPos+1;
			// SET THE TYPE OF DAY, THE focusDay GENERALLY APPEARS AS A DIFFERENT COLOR
			cellText  = "";
			if (currentDay == day) 
			{
				dayType = "focusDay";
				dayColor=  "red";
			}
			else 
			{
				dayType = "weekDay";
				dayColor=  "blue";
			}
			cellText += "<a class='" + dayType + "' href='javascript:writeCalendar2(" + currentDay + ")'>";
			cellText += "<font color='" + dayColor + "'>";
			cellText += "&nbsp;" +currentDay+ "&nbsp;";
			cellText += "</font>";
			cellText += "</a>";
			// ADD THE DAY TO THE CALENDAR STRING
			cell = getElement('cell_'+columnCount);
			cell.innerHTML = cellText;
			columnCount++;
		}
		// MAKE REMAINING NON-DATE CELLS BLANK
		for (i=days; i<42; i++)  
		{
			cell = getElement('cell_'+columnCount);
            cell.innerHTML = "&nbsp;&nbsp;&nbsp;";
            // cell.innerHTML = "<p class='weekDay'>"+" &nbsp;&nbsp;&nbsp; "+"</p>"; // leads to wrong layout in FireFox
			//cell.innerHTML = '&nbsp;&nbsp;&nbsp;';
			columnCount++;
		}
		//
		//
		makeDateString(day);
	}
	//
	// 
	// THIS IS USED TO REWRITE THE CALENDAR EVERY TIME A DATE CLICKED
	// THIS DOES THE SAME JOB AS THE WRITECALENDAR FUNCTION BUT ACCEPTS A
	// DATE ARGUMENT.  THIS WAS CREATED TO MINIMIZE THE IMPACT ON THE CODE.
	function writeCalendar2(clickedDate) 
	{
		calendarDay = clickedDate;
		setYear();
		writeCalendar();
	}
	//
	// REPLACE ALL INSTANCES OF find WITH replace
	// inString: the string you want to convert
	// find:     the value to search for
	// replace:  the value to substitute
	//
	// usage:    jsReplace(inString, find, replace);
	// example:  jsReplace("To be or not to be", "be", "ski");
	//           result: "To ski or not to ski"
	//
	function jsReplace(inString, find, replace) 
	{
		var outString = "";
		if (!inString) 
		{
			return "";
		}
		// REPLACE ALL INSTANCES OF find WITH replace
		if (inString.indexOf(find) != -1) 
		{
			// SEPARATE THE STRING INTO AN ARRAY OF STRINGS USING THE VALUE IN find
			t = inString.split(find);
			// JOIN ALL ELEMENTS OF THE ARRAY, SEPARATED BY THE VALUE IN replace
			return (t.join(replace));
		}
		else 
		{
			return inString;
		}
	}
	//
	// ENSURE THAT VALUE IS TWO DIGITS IN LENGTH
	function makeTwoDigit(inValue) 
	{
		var numVal = parseInt(inValue, 10);
		// VALUE IS LESS THAN TWO DIGITS IN LENGTH
		if (numVal < 10) 
		{
			// ADD A LEADING ZERO TO THE VALUE AND RETURN IT
			return("0" + numVal);
		}
		else 
		{
			return numVal;
		}
	}    
	//
	// SET FIELD VALUE TO THE DATE SELECTED AND CLOSE THE CALENDAR WINDOW
	function makeDateString(inDay)
	{
		calendarDay = inDay;
		calendarDate.setDate(inDay);
		// inDay = THE DAY THE USER CLICKED ON
		calendarDate.setDate(inDay);

		// SET THE DATE RETURNED TO THE USER
		var day           = calendarDate.getDate();
		var month         = calendarDate.getMonth()+1;
		var year          = calendarDate.getFullYear();
		var hour          = calendarDate.getHours();
		var minute        = calendarDate.getMinutes();

		var monthString   = monthArray[calendarDate.getMonth()];
		var monthAbbrev   = monthString.substring(0,3);
		var weekday       = weekdayList[calendarDate.getDay()];
		var weekdayAbbrev = weekday.substring(0,3);

		outDate = calendarDateFormat;

		// RETURN TWO DIGIT DAY
		if (calendarDateFormat.indexOf("DD") != -1) 
		{
			day = makeTwoDigit(day);
			outDate = jsReplace(outDate, "DD", day);
		}
		// RETURN TWO DIGIT DAY
		else if (calendarDateFormat.indexOf("dd") != -1) 
		{
			day = makeTwoDigit(day);
			outDate = jsReplace(outDate, "dd", day);
		}
		// RETURN ONE OR TWO DIGIT DAY
		else if (calendarDateFormat.indexOf("d") != -1) 
		{
			outDate = jsReplace(outDate, "d", day);
		}

		// RETURN TWO DIGIT MONTH
		if (calendarDateFormat.indexOf("MM") != -1) 
		{
			month = makeTwoDigit(month);
			outDate = jsReplace(outDate, "MM", month);
		}
		// RETURN TWO DIGIT MONTH
		else if (calendarDateFormat.indexOf("mm") != -1) 
		{
			month = makeTwoDigit(month);
			outDate = jsReplace(outDate, "mm", month);
		}
		// RETURN ONE OR TWO DIGIT MONTH
		else if (calendarDateFormat.indexOf("m") != -1) 
		{
			outDate = jsReplace(outDate, "m", month);
		}

		// RETURN FOUR-DIGIT YEAR
		if (calendarDateFormat.indexOf("yyyy") != -1) 
		{
			outDate = jsReplace(outDate, "yyyy", year);
		}
		// RETURN FOUR-DIGIT YEAR
		if (calendarDateFormat.indexOf("YYYY") != -1) 
		{
			outDate = jsReplace(outDate, "YYYY", year);
		}
		// RETURN TWO-DIGIT YEAR
		else if (calendarDateFormat.indexOf("yy") != -1) 
		{
			var yearString = "" + year;
			var yearString = yearString.substring(2,4);
			outDate = jsReplace(outDate, "yy", yearString);
		}
		// RETURN FOUR-DIGIT YEAR
		else if (calendarDateFormat.indexOf("YY") != -1) 
		{
			var yearString = "" + year;
			var yearString = yearString.substring(2,4);
			outDate = jsReplace(outDate, "YY", year);
		}

		// RETURN DAY OF MONTH (Initial Caps)
		if (calendarDateFormat.indexOf("Month") != -1) {
			outDate = jsReplace(outDate, "Month", monthString);
		}
		// RETURN DAY OF MONTH (lowercase letters)
		else if (calendarDateFormat.indexOf("month") != -1) {
			outDate = jsReplace(outDate, "month", monthString.toLowerCase());
		}
		// RETURN DAY OF MONTH (UPPERCASE LETTERS)
		else if (calendarDateFormat.indexOf("MONTH") != -1) {
			outDate = jsReplace(outDate, "MONTH", monthString.toUpperCase());
		}

		// RETURN DAY OF MONTH 3-DAY ABBREVIATION (Initial Caps)
		if (calendarDateFormat.indexOf("Mon") != -1) {
			outDate = jsReplace(outDate, "Mon", monthAbbrev);
		}
		// RETURN DAY OF MONTH 3-DAY ABBREVIATION (lowercase letters)
		else if (calendarDateFormat.indexOf("mon") != -1) {
			outDate = jsReplace(outDate, "mon", monthAbbrev.toLowerCase());
		}
		// RETURN DAY OF MONTH 3-DAY ABBREVIATION (UPPERCASE LETTERS)
		else if (calendarDateFormat.indexOf("MON") != -1) {
			outDate = jsReplace(outDate, "MON", monthAbbrev.toUpperCase());
		}

		// RETURN WEEKDAY (Initial Caps)
		if (calendarDateFormat.indexOf("Weekday") != -1) {
			outDate = jsReplace(outDate, "Weekday", weekday);
		}
		// RETURN WEEKDAY (lowercase letters)
		else if (calendarDateFormat.indexOf("weekday") != -1) {
			outDate = jsReplace(outDate, "weekday", weekday.toLowerCase());
		}
		// RETURN WEEKDAY (UPPERCASE LETTERS)
		else if (calendarDateFormat.indexOf("WEEKDAY") != -1) {
			outDate = jsReplace(outDate, "WEEKDAY", weekday.toUpperCase());
		}

		// RETURN WEEKDAY 3-DAY ABBREVIATION (Initial Caps)
		if (calendarDateFormat.indexOf("Wkdy") != -1) {
			outDate = jsReplace(outDate, "Wkdy", weekdayAbbrev);
		}
		// RETURN WEEKDAY 3-DAY ABBREVIATION (lowercase letters)
		else if (calendarDateFormat.indexOf("wkdy") != -1) 
		{
			outDate = jsReplace(outDate, "wkdy", weekdayAbbrev.toLowerCase());
		}
		// RETURN WEEKDAY 3-DAY ABBREVIATION (UPPERCASE LETTERS)
		else if (calendarDateFormat.indexOf("WKDY") != -1) 
		{
			outDate = jsReplace(outDate, "WKDY", weekdayAbbrev.toUpperCase());
		}
		// RETURN AM/PM
		var ampm = "<isa:translate key="appbase.calendar.timeAM"/>";
		if (calendarDateFormat.indexOf("a") != -1)
		{ 
			if (hour == 0) 
				hour = 12;
			if (hour > 11)
				ampm = "<isa:translate key="appbase.calendar.timePM"/>";
			if (hour > 12)
				hour -= 12;
			outDate = jsReplace(outDate, "a", ampm);
		}
		// for javascript hour start at 0 and mean 12 am
		// RETURN HOUR 2 digits
		if (calendarDateFormat.indexOf("kk") != -1)
		{ 
			hour = makeTwoDigit(hour);
			outDate = jsReplace(outDate, "k", hour);
		}else
		// RETURN HOUR
		if (calendarDateFormat.indexOf("k") != -1)
		{ 
			outDate = jsReplace(outDate, "k", hour);
		}
		// RETURN HOUR 2 digits
		if (calendarDateFormat.indexOf("hh") != -1)
		{ 
			if (hour > 12)
				hour -= 12;		
			hour = makeTwoDigit(hour);
			outDate = jsReplace(outDate, "hh", hour);
		}else
		// RETURN HOUR
		if (calendarDateFormat.indexOf("h") != -1)
		{ 
			if (hour > 12)
				hour -= 12;
			outDate = jsReplace(outDate, "h", hour);
		}
		// RETURN HOUR 2 digits
		if (calendarDateFormat.indexOf("HH") != -1)
		{ 
			hour = makeTwoDigit(hour);
			outDate = jsReplace(outDate, "HH", hour);
		}else
		// RETURN HOUR
		if (calendarDateFormat.indexOf("H") != -1)
		{ 
			outDate = jsReplace(outDate, "H", hour);
		}
		// RETURN MINUTE 2 digits
		if (calendarDateFormat.indexOf("mm") != -1)
		{ 
			minute = makeTwoDigit(minute);
			outDate = jsReplace(outDate, "mm", minute);
		}else
		// RETURN HOUR
		if (calendarDateFormat.indexOf("m") != -1)
		{ 
			outDate = jsReplace(outDate, "m", minute);
		}
		// STORE THE VALUE OF THE SELECTED DATE IN THE DISABLE DATE FIELD IN THE BOTTOM
		document.forms[0].calendarDateField.value = outDate;
	}
	//
	// GET NUMBER OF DAYS IN MONTH
	function getDaysInMonth()
	{
		var days;
		var month = calendarDate.getMonth()+1;
		var year  = calendarDate.getFullYear();
		// RETURN 31 DAYS
		if (month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)  
		{
			days=31;
		}
		// RETURN 30 DAYS
		else if (month==4 || month==6 || month==9 || month==11) 
		{
			days=30;
		}
		// RETURN 29 DAYS
		else if (month==2)  
		{
			if (isLeapYear(year)) 
			{
				days=29;
			}
			// RETURN 28 DAYS
			else 
			{
				days=28;
			}
		}
		return (days);
	}
	//
	// GET NUMBER OF DAYS IN MONTH (SECOND FUNCTION WHICH ACCEPTS AN ARGUMENT)
	function getDaysInMonth2(monthNum, yearArg)  
	{
		var days;
		var month = monthNum+1;
		var year  = yearArg;
		// RETURN 31 DAYS
		if (month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)  
		{
			days=31;
		}
		// RETURN 30 DAYS
		else if (month==4 || month==6 || month==9 || month==11) 
		{
			days=30;
		}
		// RETURN 29 DAYS
		else if (month==2)  
		{
			if (isLeapYear(year)) 
			{
				days=29;
			}
			// RETURN 28 DAYS
			else 
			{
				days=28;
			}
		}
		return (days);
	}
	//
	// CHECK TO SEE IF YEAR IS A LEAP YEAR
	function isLeapYear (Year) 
	{
		if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0)) 
		{
			return (true);
		}
		else 
		{
			return (false);
		}
	}
	//
	// ENSURE THAT THE YEAR IS FOUR DIGITS IN LENGTH
	function isFourDigitYear(year) {

		if (((typeof year == "string" && year.length != 4) || (isNumber(year))) ||
		    (typeof year == "number" && year < 1000))  
		{
			document.forms[0].year.value = calendarDate.getFullYear();
			document.forms[0].year.select();
			document.forms[0].year.focus();
		}
		else 
		{
			return true;
		}
	}
	// Ensure that the year is a number
	function isNumber(year) {
	
	    year = parseInt(year, 10);
	    if (isNaN(year)) {
	        return true;
	    }
	    else {
	        return false;
	    }
	}	
	//
	// EVENT HANDLERS
	// SET THE CALENDAR TO TODAY'S DATE AND DISPLAY THE NEW CALENDAR
	function setToday() 
	{
		// SET GLOBAL DATE TO TODAY'S DATE
		calendarDate = new Date();
		calendarDay = calendarDate.getDate();
		// SET DAY MONTH AND YEAR TO TODAY'S DATE
		var month = calendarDate.getMonth();
		var year  = calendarDate.getFullYear();
		// SET MONTH IN DROP-DOWN LIST
		document.forms[0].month.selectedIndex = month;
		// SET YEAR VALUE
		document.forms[0].year.value = year;
		// DISPLAY THE NEW CALENDAR
		writeCalendar();
	}
	//
	// SET THE GLOBAL DATE TO THE NEWLY ENTERED YEAR AND REDRAW THE CALENDAR
	function setYear() 
	{
		// GET THE NEW YEAR VALUE
		var year  = document.forms[0].year.value;
		// IF IT'S A FOUR-DIGIT YEAR THEN CHANGE THE CALENDAR
		if (isFourDigitYear(year)) 
		{
			var month = calendarDate.getMonth();
			// DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
			var days = getDaysInMonth2(month, year);
			if (days < calendarDay) 
			{
				calendarDate.setDate(days);
				calendarDay = days;
			}
			calendarDate.setFullYear(year);
			writeCalendar();
		}
		else 
		{
			// HIGHLIGHT THE YEAR IF THE YEAR IS NOT FOUR DIGITS IN LENGTH
			document.forms[0].year.focus();
			document.forms[0].year.select();
		}
	}
	//
	// SET THE GLOBAL DATE TO THE SELECTED MONTH AND REDRAW THE CALENDAR
	// this handle the month selection in the pull down
	function setMonth() 
	{
		// GET THE NEWLY SELECTED MONTH AND CHANGE THE CALENDAR ACCORDINGLY
		var month = document.forms[0].month.selectedIndex;
		// DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
		var days = getDaysInMonth2(month, calendarDate.getFullYear());
		if (days < calendarDay) 
		{
			calendarDate.setDate(days);
			calendarDay = days;
		}
		calendarDate.setMonth(month);
		writeCalendar();
	}
<%
if (hasTime)
{
%>
	//
	// SET THE GLOBAL DATE TO THE SELECTED TIME AND REDRAW THE CALENDAR
	// this handle the hour, minute and am/pm selection in the pull down(s)
	function setTime() 
	{
		var hour          = calendarDate.getHours();
		var minute        = calendarDate.getMinutes();
<%
if (hasHour)
{
%>
		var hour = document.forms[0].hourlist.options[document.forms[0].hourlist.selectedIndex].value;
<%
}
%>
<%
if (hasMinute)
{
%>
		var minute = document.forms[0].minlist.options[document.forms[0].minlist.selectedIndex].value;
<%
}
%>
<%
if (hasAMPM)
{
%>
		var ampm = document.forms[0].ampmlist.options[document.forms[0].ampmlist.selectedIndex].value;
<%
}
%>
<%
if (hasAMPM && hasHour)
{
%>

		if (ampm == "am") 
		{
			if(hour == 12) 
			{
				hour = 0;
			}		
		}
		else 
		{	
			if (hour < 12) 
			{
				hour = parseInt(hour) + 12;
			}
		}
<%
}
%>
		calendarDate.setHours(hour);
		calendarDate.setMinutes(minute);
		writeCalendar();
	}	
<%
}
%>	
	//
	// SET THE GLOBAL DATE TO THE PREVIOUS YEAR AND REDRAW THE CALENDAR
	function setPreviousYear() 
	{
		var year  = document.forms[0].year.value;
		if (isFourDigitYear(year) && year > 1000) 
		{
		year--;
		var month = calendarDate.getMonth();
		// DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
		var days = getDaysInMonth2(month, year);
		if (days < calendarDay) 
		{
			calendarDate.setDate(days);
			calendarDay = days;
		}
		calendarDate.setFullYear(year);
		document.forms[0].year.value = year;
		writeCalendar();
		}
	}
	//
	// SET THE GLOBAL DATE TO THE PREVIOUS MONTH AND REDRAW THE CALENDAR
	function setPreviousMonth() 
	{
		var year  = document.forms[0].year.value;
		if (isFourDigitYear(year)) 
		{
		var month = document.forms[0].month.selectedIndex;
		// IF MONTH IS JANUARY, SET MONTH TO DECEMBER AND DECREMENT THE YEAR
		if (month == 0) 
		{
			month = 11;
			if (year > 1000) 
			{
			year--;
			calendarDate.setFullYear(year);
			document.forms[0].year.value = year;
			}
		}
		else 
		{
			month--;
		}
		// DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
		var days = getDaysInMonth2(month, calendarDate.getFullYear());
		if (days < calendarDay) 
		{
			calendarDate.setDate(days);
			calendarDay = days;
		}
		calendarDate.setMonth(month);
		document.forms[0].month.selectedIndex = month;
		writeCalendar();
		}
	}
	//
	// SET THE GLOBAL DATE TO THE NEXT MONTH AND REDRAW THE CALENDAR
	function setNextMonth() 
	{

		var year = document.forms[0].year.value;
		if (isFourDigitYear(year)) 
		{
		var month = document.forms[0].month.selectedIndex;
		// IF MONTH IS DECEMBER, SET MONTH TO JANUARY AND INCREMENT THE YEAR
		if (month == 11) 
		{
			month = 0;
			year++;
			calendarDate.setFullYear(year);
			document.forms[0].year.value = year;
		}
		else 
		{
			month++;
		}

		// DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH
		var days = getDaysInMonth2(month, calendarDate.getFullYear());

		if (days < calendarDay) 
		{
			calendarDate.setDate(days);
			calendarDay = days;
		}
		calendarDate.setMonth(month);
		document.forms[0].month.selectedIndex = month;
		writeCalendar();
		}
	}
	
	<%-- SET THE GLOBAL DATE TO THE NEXT YEAR AND REDRAW THE CALENDAR --%>
	function setNextYear() 
	{
		var year  = document.forms[0].year.value;
		if (isFourDigitYear(year)) 
		{
		year++;
		var month = calendarDate.getMonth();
		<%-- DETERMINE THE NUMBER OF DAYS IN THE SELECTED MONTH --%>
		var days = getDaysInMonth2(month, year);
		if (days < calendarDay) 
		{
			calendarDate.setDate(days);
			calendarDay = days;
		}
		calendarDate.setFullYear(year);
		document.forms[0].year.value = year;
		writeCalendar();
		}
	}
	
	<%-- SET THE DATE TO THE CALLING WINDOW --%>
	function returnDate() 
	{
		makeDateString(calendarDay);
		var fieldId = '<%= dateFieldId %>';
<%            
        if (!embedded){
            // JS-code if the calendar control is not embedded, i.e. it is displayed as pop-up
%>      
			if (opener.dateField != null){
				<%-- SET THE VALUE OF THE FIELD THAT WAS PASSED TO THE CALENDAR --%>
				opener.dateField.value = document.forms[0].calendarDateField.value;
				
		        <%-- GIVE CHANGE EVENT TO THE DATE FIELD --%>
		        if (opener.dateField.onchange != null){
		           opener.dateField.onchange();
		        }
		
				<%-- GIVE FOCUS BACK TO THE DATE FIELD --%>
				opener.dateField.focus();
			}
			else {
				var newDateField = opener.document.getElementById(fieldId);
				<%-- SET THE VALUE OF THE FIELD THAT WAS PASSED TO THE CALENDAR --%>
				newDateField.value = document.forms[0].calendarDateField.value;
				
		        <%-- GIVE CHANGE EVENT TO THE DATE FIELD --%>
		        if (newDateField.onchange != null){
		        	newDateField.onchange();
		        }
		
				<%-- GIVE FOCUS BACK TO THE DATE FIELD --%>
				newDateField.focus();
				
			}
			<%-- CLOSE THE CALENDAR WINDOW --%>
			window.self.close();
<%
		}
        else {
            // JS-code if the calendar control is embedded 
%>            
            <%-- SET THE VALUE OF THE FIELD THAT WAS PASSED TO THE CALENDAR --%>
            dateField.value = document.forms[0].calendarDateField.value;
		
            <%-- GIVE CHANGE EVENT TO THE DATE FIELD --%>
            if (dateField.onchange != null){
               dateField.onchange();
            }

            <%-- GIVE FOCUS BACK TO THE DATE FIELD --%>
            dateField.focus();
	        <%-- HIDE THE DIV-AREA --%>
	        parent.toggleCal('<%= divId %>');
<%
        }
%>              
	}
	</script>  
</head>
<isa:includes/>
<body bgcolor='<%=bottomBackground%>' onLoad='init()'>
<div class="module-name"><isa:moduleName name="/appbase/calendar.jsp" /></div>
<%            
	if (calendarUI.isAccessible())
	{
%>
<a id="calendar-start" href="#"	
title= "<isa:translate key="appbase.calendar.title"/>" 
accesskey="<isa:translate key="appbase.calendar.key"/>"
onkeypress="if(event.keyCode=='115') {getElement('calendar-end').focus();}"
><img src="<%=WebUtil.getMimeURL(pageContext, "/mimes/images/1x1.gif") %>" alt="<isa:translate key="appbase.calendar.title"/>" border="0" width="1" height="1"
></a>
<%
	}
%>
	<form action="#" onSubmit='return false;'>
	<table align='center' width='100%' cellpadding='0' cellspacing='0' border='0'>
	<tr bgcolor='<%=topBackground%>' align='center' width='100%'>
		<center>
<%
	String monthTitle = WebUtil.translate(pageContext, "appbase.calendar.monthTitle", null);
	String yearTitle = WebUtil.translate(pageContext, "appbase.calendar.yearTitle", null);
%>
		<table bgcolor='<%=topBackground%>' align='center' width='100%' cellpadding='0' cellspacing='1' border='0'>
		<tr bgcolor='<%=topBackground%>' align='center' width='100%'>
		<td>&nbsp;&nbsp;&nbsp;<td>
		</tr>
			<tr bgcolor='<%=topBackground%>' align='center' width='100%'>
				<td colspan='7' nowrap>
					<center>
						<select name='month' onChange='setMonth()' title='<%=monthTitle%>'>
						<%
						for (int i=0; i < months.size(); i++)
						{   
						%><option value='<%=i%>'><%=months.get(i)%></option><%
						}
						%>
						</select>
						<input name='year' value='' type='text' size=4 maxlength='4' 
						onChange='setYear()' 
						title='<%=yearTitle%>'
						class='textInput'
						/>
					</center>
				</td>
			</tr>
<%
	String prevYearBtnTxt = WebUtil.translate(pageContext, "appbase.calendar.prevYearButton", null);
	String nextYearBtnTxt = WebUtil.translate(pageContext, "appbase.calendar.nextYearButton", null);
	String prevYearTitle = WebUtil.translate(pageContext, "appbase.calendar.prevYearTitle", null);
	String nextYearTitle = WebUtil.translate(pageContext, "appbase.calendar.nextYearTitle", null);
	String prevMonthBtnTxt = WebUtil.translate(pageContext, "appbase.calendar.prevMonthButton", null);
	String nextMonthBtnTxt = WebUtil.translate(pageContext, "appbase.calendar.nextMonthButton", null);
	String prevMonthTitle = WebUtil.translate(pageContext, "appbase.calendar.prevMonthTitle", null);
	String nextMonthTitle = WebUtil.translate(pageContext, "appbase.calendar.nextMonthTitle", null);
	String todayBtnTxt = WebUtil.translate(pageContext, "appbase.calendar.todayButton", null);
%>
			<tr bgcolor='<%=topBackground%>' align='center' width='100%'>
			<td colspan='7' align='center'>
				<input type='button' name='previousYear' value='<%=prevYearBtnTxt%>'  onClick='setPreviousYear()' class='FancyButtonGrey' 
				title="<isa:translate key="access.button" arg0="<%=prevYearTitle%>" arg1=""/>" 
				/>
				<input type='button' name='previousMonth' value='<%=prevMonthBtnTxt%>' onClick='setPreviousMonth()' class='FancyButtonGrey' 
				title="<isa:translate key="access.button" arg0="<%=prevMonthTitle%>" arg1=""/>" 
				/>
				<input type='button' name='today' value='<%=todayBtnTxt%>' onClick='setToday()' class='FancyButtonGrey' 
				title="<isa:translate key="access.button" arg0="<%=todayBtnTxt%>" arg1=""/>" 
				/>
				<input type='button' name='nextMonth' value='<%=nextMonthBtnTxt%>' onClick='setNextMonth()' class='FancyButtonGrey' 
				title="<isa:translate key="access.button" arg0="<%=nextMonthTitle%>" arg1=""/>" 
				/>
				<input type='button' name='nextYear' value='<%=nextYearBtnTxt%>' onClick='setNextYear()' class='FancyButtonGrey'
				title="<isa:translate key="access.button" arg0="<%=nextYearTitle%>" arg1=""/>" 
				/>
			</td>
			</tr>
		<tr bgcolor='<%=topBackground%>' align='center' width='100%'>
		<td>&nbsp;&nbsp;&nbsp;<td>
		</tr>
		</table>
		</center>
	</tr>
	<tr bgcolor='<%=bottomBackground%>' align='center' width='100%'>
	<td>&nbsp;&nbsp;&nbsp;<td>
	</tr>

<%
if (hasTime)
{
%>
	<!-- time -->
	<tr align='center' width='100%'>
		<center>
		<table align='center' cellpadding='0' cellspacing='1' border='0'>
		<!-- time headers -->
		<tr align='center'>
		<td></td>
<%
if (hasHour)
{
%>
		<td><b><font face=arial size=-1><label for="hourlist"><isa:translate key="appbase.calendar.hourLabel"/></label></font></b></td>
<%
}
%>
<%
if (hasMinute)
{
%>
		<td><b><font face=arial size=-1><label for="minlist"><isa:translate key="appbase.calendar.minuteLabel"/></label></font></b></td>
<%
}
%>
<%
if (hasAMPM)
{
%>
		<td><b><font face=arial size=-1><label for="ampmlist"><isa:translate key="appbase.calendar.ampmLabel"/></label></font></b></td>
<%
}
%>
		</tr>
		<!-- imputs -->
		<tr align='center'>
		<td><b><font face=arial size=-1><isa:translate key="appbase.calendar.timeLabel"/></font></b></td>
<%
if (hasHour)
{
%>
		<td><select onChange='setTime()' name='hourlist' id='hourlist'>
			<%
			for (int hour=minHour; hour<= maxHour; hour++)
			{   
			%><option value='<%=hour%>'><%=(hour<10)?("0"+hour):(""+hour)%></option><%
			}
			%>
		</select></td>
<%
}
%>
<%
if (hasMinute)
{
%>
		<td><select onChange='setTime()' name='minlist' id='minlist'>
			<%
			for (int minute=0; minute< 60; minute++)
			{   
			%><option value='<%=minute%>'><%=(minute<10)?("0"+minute):(""+minute)%></option><%
			}
			%>
		</select></td>
<%
}
%>
<%
if (hasAMPM)
{
%>
		<td><select onChange='setTime()' name='ampmlist' id='ampmlist'>
			<option value='pm'><isa:translate key="appbase.calendar.timePM"/></option>
			<option value='am'><isa:translate key="appbase.calendar.timeAM"/></option>
		</select></td>
<%
}
%>
		</tr>
		</table>		
		</center>
	</tr>
	<tr bgcolor='<%=bottomBackground%>' align='center' width='100%'>
	<td>&nbsp;&nbsp;&nbsp;<td>
	</tr>
<%
}
%>
	<!-- days -->
	<tr bgcolor='<%=bottomBackground%>' align='center' width='100%'>
		<center>
<%
	int colno = 7;
	int rowno = 6;
	String columnNumber = Integer.toString(colno);
	String rowNumber = Integer.toString(rowno);
	String allRows = Integer.toString(rowno);
	String tableTitle = WebUtil.translate(pageContext, "appbase.calendar.tableTitle", null);
%>
<%
	if (calendarUI.isAccessible())
	{
%>
	<a id="calendartable-begin" href="#calendartable-end" onkeypress="if(event.keyCode=='115') {getElement('calendartable-end').focus();}"
	   title="<isa:translate key="access.table.begin" arg0=""/>"
	><img src="<%=WebUtil.getMimeURL(pageContext, "/mimes/images/1x1.gif") %>" 
		  alt="<isa:translate key="access.table.summary" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="1" arg4="<%=allRows%>"/>"
	/></a>
<%
	}
%>        
		<table bgcolor='<%=headingCellColor%>' align='center' cellpadding='0' cellspacing='1' border='0'>
		<!-- weekdays headers -->
		<tr bgcolor='<%=headingCellColor%>'>
		<%
		for (int i=0; i < weekdays.size(); i++)
		{   
		%><td scope='col' align='center' class='heading'><%=weekdays.get(i)%></td><%
		}
		%>
		</tr>
		<!-- month days empty data to be filled by javascript -->
		<%
		int cell = 0;
		for (int rows=0;rows<6;rows++)
		{
		%>
		<tr bgcolor='<%=bottomBackground%>' align='center' >
			<%
			for (int cols=0;cols<7;cols++)
			{
			%><td id='cell_<%=cell++%>' align='center' bgcolor='<%=bottomBackground%>'></td>
			<%
			}
			%>
		</tr>
		<%
		}
		%>        
		</table>
<%
	if (calendarUI.isAccessible())
	{
%>
	<a id="calendartable-end" href="#calendartable-begin" title="<isa:translate key="access.table.end" arg0=""/>"></a>
<%
	}
%>
		</center>		
		<!-- fields -->
		<center>
		<table cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='<%=bottomBackground%>'>
<%
	String dateTitle = WebUtil.translate(pageContext, "appbase.calendar.dateTitle", null);
%>
		<tr>
			<td colspan='2' align='center'>
				<input type=text name='calendarDateField' value='' readonly
				onfocus='this.blur()' 
				title='<%=dateTitle%>'
				class='textInput'
				/>
			</td>
		</tr>
		<tr><br></tr>
		<tr><br></tr>
		<tr>
<%
	String acceptBtnTxt = WebUtil.translate(pageContext, "appbase.calendar.acceptButton", null);
	String cancelBtnTxt = WebUtil.translate(pageContext, "appbase.calendar.cancelButton", null);
%>
		<td valign='top' align='center'>
<%            
	if (calendarUI.isAccessible())
	{
%>
<a id="calendar-end" href="#calendar-start"
><img src="<%=WebUtil.getMimeURL(pageContext, "/mimes/images/1x1.gif") %>" alt="<isa:translate key="appbase.calendar.title"/>" border="0" width="1" height="1"
></a>
<%
	}
%>        
		<input type='button' name='acceptButton' value='<isa:translate key="appbase.calendar.acceptButton"/>' 
		onClick='returnDate()' class='FancyButtonGrey' 
		title="<isa:translate key="access.button" arg0="<%=acceptBtnTxt%>" arg1=""/>" 
		/>
<%            
    if (!embedded){
        // show the cancel button only if the calendar control is not embedded
%>
		<input type='button' name='cancelButton' value='<isa:translate key="appbase.calendar.cancelButton"/>' 
		onClick='top.close()' class='FancyButtonGrey' 
		title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>" 
		/>
<%      
    }
%>
		</td>
		</tr>
		</table>
		</center>
	</tr>
	</table>
	</form>
</body>
</html>