<%--
******************************************************************************
        JSP:      genericsearch.jsp
        Copyright (c) 2003, SAP AG, Germany, All rights reserved.
******************************************************************************
--%>

<%-- 

  TODO: 
    - no border support for <img> tags because of XHTML conflics

--%>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="java.util.Map" %>
<%@ page import="java.lang.reflect.Method" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>

<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIFactory" %>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIDynamicContentData" %>
<%@ page import="com.sap.isa.isacore.action.GenericSearchSortAction" %>
<%@ page import="com.sap.isa.isacore.GenericSearchComparator" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ include file="/appbase/checksession.inc" %>

<%
  int line = 0;
  String[] evenOdd = new String[] { "even", "odd" };
  BaseUI baseui = new BaseUI(pageContext);

  // Get UI class via Factory
  GenericSearchUIData ui = GenericSearchUIFactory.getUIClassInstance(pageContext);
  ui.setScreenElements();

  // Screen element counter
  int scrElmCnt = 0;

  // Get search request memory
  Map srm = ui.getSearchRequestMemory();
  // Add ui Class instance to request to enable includes to read it
  request.setAttribute(GenericSearchUIData.RC_UICLASSINSTANCE, ui);
%>

<% if (ui.isFullScreenEnabled()) { %>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="pragma" content="no-cache" />
        <title>Generic Search Framework</title>

        <isa:stylesheets />

        <script type="text/javascript">
        <!--
                <%@ include file="/appbase/jscript/urlrewrite.inc"  %>
        //-->
        </script>
<% } %>
<% if (ui.isSelectOptionEnabled()) { %>
        <script src="<isa:webappsURL name="appbase/jscript/calendar.js"/>"
                type="text/javascript">
        </script>
        <script src="<isa:webappsURL name="appbase/jscript/dateformatcheck.js"/>"
                type="text/javascript">
        </script>
        
         <script src="<isa:webappsURL name="appbase/jscript/genericsearch.js"/>"
                type="text/javascript">
        </script>
        
       <script type="text/javascript"> 
          function dfc_confirmError(object, code) {
             var back;
             if (dfc_noPopUp == false) {
                if (parseInt(code) <= 800) {
                   alert("'" + dfc_msgTmpl + "' <isa:translate key="dfc.date.invalid.format"/>");
                } else {
                   alert("'" + object.value + "' <isa:translate key="dfc.date.invalid"/>");
                }
             }
             dfc_code = code;
             if (dfc_noPopUp == false) {
               object.focus();
             }
         }
       </script>
       
       
        <script type="text/javascript">
        <!--             
                var reqGo = false;     <%-- //Since every request rebuilds the html page, this will also reset the reqGo to false --%>
                function multipleInvocationHandler() {
                    if (reqGo != false) {  <%-- // Has request already been started !?! Block second request --%>
                        return true;
                    }
                    if (document.getElementById('gsbuttonstart') != null) {
                        document.getElementById('gsbuttonstart').title = '<isa:translate key="gs.but.start.secondgo"/>';
                    }
                    reqGo = true;
                    return false;
                }
                <%-- //@param String targetFrame is the name of the Frame where the response should be located --%>
                function sendRequest(targetFrame) {
                        if (multipleInvocationHandler() == true) { return true; }
                        var request = "<isa:webappsURL name="/genericsearch.do"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=<%=ui.getScreenName()%>&";
                        request = request + "<%=GenericSearchUIData.RC_REQUESTSTARTED%>=true&";
                        request = request + "<%=GenericSearchUIData.RC_DATEFORMAT%>=<%=ui.getDateFormat()%>&";
                        request = request + "<%=GenericSearchUIData.RC_NUMBERFORMAT%>=<%=WebUtil.encodeURL(ui.getNumberFormat())%>&";
                        request = request + "<%=GenericSearchUIData.RC_LANGUAGE%>=<%=ui.getLanguage()%>&";
                        request = request + "<%=GenericSearchUIData.RC_DOCUMENTHANDLERNOADD%>=<%=JspUtil.encodeURL(request.getParameter(GenericSearchUIData.RC_DOCUMENTHANDLERNOADD))%>&"
                        for(var i = 0; i < scnElms.length; i ++) {
                                var scnElmx = scnElms[i];
                                if (! scnElmx) break;

                                if (scnElmx.isVisible == true) {
                                        <%-- // Add attribute and its value to the request, but only if attirubte has a NAME --%>
                                        if (scnElmx.isRange == false) {
                                                var objL = document.getElementById(scnElmx.s_id);
                                                if (objL && objL.type == "radio") {
                                                    var ridx = 0;
                                                    <%-- // Find checked radio button --%>
                                                    while(objL && !objL.checked) {
                                                        ridx++;
                                                        objL = document.getElementById((scnElmx.s_id + "+" + ridx)); 
                                                    }
                                                }
                                                if ( objL  &&  objL.name != 'null') {
                                                        request = request + ((objL.name) + "=" + (encodeURIComponent(objL.value)) + "&");
                                                }
                                                if (objL) {
                                                        <%-- // Block any further inputs --%>
                                                        objL.disabled = true;
                                                }
                                        } else {
                                                <%-- // For ranges add _LOW and _HIGH to the HTTP request parameter name --%>
                                                var idLow = scnElmx.s_id  + "_LOW";
                                                var idHigh = scnElmx.s_id + "_HIGH";
                                                var objL = document.getElementById(idLow);
                                                var objH = document.getElementById(idHigh);
                                                if ( objL.name != 'null') {
                                                        request = request + ((objL.name) + "=" + (encodeURIComponent(objL.value)) + "&");
                                                        objL.disabled = true;
                                                }
                                                if ( objH.name != 'null') {
                                                        request = request + ((objH.name) + "=" + (encodeURIComponent(objH.value)) + "&");
                                                        objH.disabled = true;
                                                }
                                        }
                                }
                        } <%-- end for --%>
                        
                        //alert("Target Frame: " + targetFrame +"\r\nRequest: " + request); //return true;
                        if (targetFrame != null && targetFrame.length > 0) {
                            var targetLoc = findTargetLocation( null,targetFrame);
                            request = request + "&<%=GenericSearchUIData.RC_UI_NOSELECTOPTIONS%>=true&<%=GenericSearchUIData.RC_ENABLESELECTOPTIONS_FRAMENAME%>=" + window.name;
                            targetLoc.location.href = request;
                        } else {
                            document.location.href = request;
                        }
                        return true;
                } <%-- end function --%>
                <%-- check if the return key was pressed  --%>
                function checkReturnKeyPressedX(evt){   
                    var pressedKeyCode;
                    if (navigator.appName == 'Netscape') {
                        // Netscape 
                        pressedKeyCode = evt.which;
                    } else {
                        // Microsoft 
                        pressedKeyCode = window.event.keyCode;
                    }
                    if (pressedKeyCode == 13) {
                        var evtObj = (evt.srcElement ? evt.srcElement : evt.target);
                        if (evtObj.outerHTML && evtObj.outerHTML.indexOf("dateFormatCheck") > 0) {
                            <%-- input field of type date which needs to be checked first--%>
                            var dfcB = dateFormatCheck(evtObj, '<%=ui.getDateFormat()%>', '', true); <%-- true for no popup on format error --%>
                            if (dfcB) {
                                var goBut = document.getElementById("gsbuttonstart");
                                if(goBut) goBut.onclick(); <%-- Fire onclick event of the Go Button --%>
                            }
                        } else if (evtObj.nameProp && evtObj.nameProp.indexOf("genericsearchsortrequest") > 0) {
                            <%-- Link for search request pressed --%>
                            evtObj.onclick();
                        } else if (evtObj.nodeName && evtObj.nodeName == 'A' && 
                                   evtObj != document.getElementById("gsbuttonstart")) {
                            <%-- Any other link clicked (e.g. see a documents detail)  do nothing! --%>
                        } else {
                            var goBut = document.getElementById("gsbuttonstart");
                            if(goBut) goBut.onclick(); <%-- Fire onclick event of the Go Button --%>
                        }
                    }
                    return true;
                }

                var isExpandedResultlist = <%=ui.isExpandedResultlist()%>;
                <% if (ui.isExpandedResultlist()) { %>
                <% /*
                   * Handles the call to the expanding or collaping JScript function
                   */ %>
                function expandedResultlistHandler(pressedButtonId, Execution) {
                    var expBut = document.getElementById("gsbuttonexpand");
                    var colBut = document.getElementById("gsbuttoncollapse");
                    if (pressedButtonId == expBut.id) {
                        // Expand button pressed
                       colBut.style.display = "inline";
                       expBut.style.display = "none";
                       if (Execution) {
                           <%=ui.getJScriptOnResultExpand()%>;
                       }
                    } else {
                       // Collapse button pressed
                       colBut.style.display = "none";
                       expBut.style.display = "inline";
                       if (Execution) {
                           <%=ui.getJScriptOnResultCollapse()%>;
                       }
                    }
                }
                <% }   // end function %>
                <%-- // Visibility functionality is currently not in use
                function SwitchVisibility() {
                        if (gv_whole_visibility == true) {
                                document.getElementById("select_attrib").style.display="block";
                                document.getElementById("select_attrib_unhide").style.display="block";
                                document.getElementById("select_attrib_hide").style.display="none";
                                gv_whole_visibility = false;
                        } else {
                                document.getElementById("select_attrib").style.display="none";
                                document.getElementById("select_attrib_unhide").style.display="none";
                                document.getElementById("select_attrib_hide").style.display="block";
                                gv_whole_visibility = true;
                        }
                } --%>
        -->
        </script>
<% } %>
        <script type="text/javascript">
        <!--
<%-- // Following functions are necessary even select options are disabled !! --%>
                function sendSortRequest(columnName, sortSequence, objType) {
                        var request = "<isa:webappsURL name="/genericsearchsortrequest.do"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=<%=ui.getScreenName()%>&";
                        request = request + "genericsearch.sort=true" + "&";
                        request = request + "<%=GenericSearchSortAction.RC_GENERICSEARCH_SORT_DATEFORMAT%>=" + "<%=ui.getDateFormat()%>" + "&";
                        request = request + "<%=GenericSearchSortAction.RC_GENERICSEARCH_SORT_NUMBERFORMAT%>=" + "<%=WebUtil.encodeURL(ui.getNumberFormat())%>" + "&";
                        request = request + "<%=GenericSearchSortAction.RC_GENERICSEARCH_SORT_LANGUAGE%>=" + "<%=ui.getLanguage()%>" + "&";
                        request = request + "<%=GenericSearchUIData.RC_DOCUMENTHANDLERNOADD%>=" + "<%=JspUtil.encodeURL(request.getParameter(GenericSearchUIData.RC_DOCUMENTHANDLERNOADD))%>" + "&";
                        request = request + "<%=GenericSearchSortAction.RC_GENERICSEARCH_SORT_COLUMNNAME%>=" + columnName + "&";
                        request = request + "<%=GenericSearchSortAction.RC_GENERICSEARCH_SORT_SEQUENCE%>=" + sortSequence + "&";
                        request = request + "<%=GenericSearchSortAction.RC_GENERICSEARCH_SORT_OBJECTTYPE%>=" + objType    + "&";
                        request = request + "<%=GenericSearchUIData.RC_UI_NOSELECTOPTIONS%>=" + "<%=JspUtil.encodeURL(request.getParameter(GenericSearchUIData.RC_UI_NOSELECTOPTIONS))%>" + "&";
                        request = request + "<%=GenericSearchUIData.RC_UI_INCLUDESTOSHOW%>=" + "<%=JspUtil.encodeURL(request.getParameter(GenericSearchUIData.RC_UI_INCLUDESTOSHOW))%>";
                        //alert("SortRequest: " + request);
                        document.location.href = request;
                }
               
                
               
                <%-- /*
                * Select / De-select all checkboxes with given request name prefix
                * @param Object  input field of type 'checkbox'
                * @param String  name prefix of the input field (e.g "rc_name" -> final name -> "rc_name[1]")
                */ --%>
                function checkboxSelectAll(headerCheckBox, rqNamePrefix) {
                    var doing = false;
                    if (headerCheckBox.checked) {
                        // had been check by user
                        doing = true;
                    }
                    for(var i = 1; i <= <%= ui.getNumberOfReturnedDocuments(1) %>; i++) {
                        var finalName = (rqNamePrefix + "[" + i + "]");
                        var boxElm = document.getElementsByName(finalName)[0];  <%-- // we only have ONE of the name !! --%>
                        if (boxElm != null) {
                            boxElm.checked = doing;
                        }
                    }
                }
        -->
        </script>
<% if (ui.isSelectOptionEnabled()) { %>
        <script type="text/javascript">
        <!--
                <%-- // GLOBAL VARIABLES --%>
                var gv_whole_visibility = false;
                <%-- // First build all JScript objects (ScreenElements) --%>
                var scnElms = new Array();
                <% scrElmCnt = 0; %>
                <%-- // Define ALL ScreenElements --%>
                <isa:iterate id="screlm" name="<%=GenericSearchUIData.RC_SCREENELEMENTS%>"
                        type="com.sap.isa.maintenanceobject.businessobject.GSProperty">
                         <% if (! "dark".equals(screlm.getType()) && ! "unknown".equals(screlm.getType()) ) { %>
                                <%-- // Screen Elements without Dependencies (reverse Point of View of Followup-Elements) are fix ones --%>
                                var hasDependencies = <%= (ui.hasDependencies(screlm) ? "false" : "true") %>;
                                var isRange = <%= ((screlm.getType() != null && screlm.getType().startsWith("daterange")) ? "true" : " false") %>;
                                scnElms[<%=scrElmCnt%>] = new ScnElm('TRID<%=screlm.hashCode()%>', 'ID<%=screlm.hashCode()%>', '<%=screlm.getName()%>',hasDependencies, isRange );
                                <% ui.setIndexToScreenElement(screlm, scrElmCnt); %>
                                <% scrElmCnt++; %>
                        <% } %>
                </isa:iterate>

                <%-- // Define ALL Dependencies of the ScreenElements --%>
                <% ui.setScreenElements();          // Set Elements again to refresh iterator %>
                <isa:iterate id="screlm" name="<%=GenericSearchUIData.RC_SCREENELEMENTS%>"
                        type="com.sap.isa.maintenanceobject.businessobject.GSProperty">
                        <% if (! "dark".equals(screlm.getType()) && ! "unknown".equals(screlm.getType())) { %>
                                <% if (screlm.getType() != null && (screlm.getType().startsWith("box")  || screlm.getType().startsWith("radio")) ) { %>
                                        <% boolean contentExists = ui.setOptions(screlm); %>   
                                        <%-- // Loop over all Options / Entries --%>
                                        <% int entryCnt = 0; %>
                                        <isa:iterate id="option" name="<%=GenericSearchUIData.RC_OPTIONS%>"
                                                type="com.sap.isa.core.util.table.ResultData">
                                                <% if (option.getBoolean("HIDDEN") != true) { %>
                                                        var new_entry = new Entry();
                                                        <% ui.setFollowupElements(screlm.getName(), option.getString("VALUE")); %>
                                                        <isa:iterate id="flwElm" name="<%=GenericSearchUIData.RC_FOLLOWUPELEMENTS%>"
                                                                type="com.sap.isa.maintenanceobject.businessobject.GSProperty">
                                                                <% if (! "dark".equals(flwElm.getType()) && ! "unknown".equals(flwElm.getType())) { %>
                                                                    new_entry.addFlwElm(scnElms[<%= ui.getIndexOfScreenElement(flwElm) %>]);
                                                                <% } %>
                                                        </isa:iterate>
                                                        <%-- // Add Entry to ScreenElement --%>
                                                        scnElms[<%= ui.getIndexOfScreenElement(screlm) %>].addEntry(<%= entryCnt %>, new_entry);
                                                        <% entryCnt++; %>
                                                 <% } %>
                                        </isa:iterate>
                                        <% entryCnt = 0;          // Reset Entry Counter %>
                                <% } /* if (screlm.getType()... */ %>
                        <% } /* if (! "dark".equals ... */ %>
                </isa:iterate>

                <% ui.setScreenElements();          // Set Elements again to refresh iterator %>
                <isa:iterate id="screlm" name="<%=GenericSearchUIData.RC_SCREENELEMENTS%>"
                        type="com.sap.isa.maintenanceobject.businessobject.GSProperty">
                        <%= ui.getHelpValuesSearch(screlm) %>
                </isa:iterate>
        //-->
        </script>
       <%@ include file="/appbase/jscript/calendar.js.inc.jsp"%>
<% } %>

        <%-- Get customized JScript Files --%>
        <% ResultData jScriptNamesTab = ui.getJScriptFilesTab();
                jScriptNamesTab.beforeFirst();
                while(! jScriptNamesTab.isLast()) {
                        jScriptNamesTab.next(); %>
                        <script src="<isa:webappsURL name="<%=jScriptNamesTab.getString(\"FILENAME\")%>"/>"
                                type="text/javascript">
                        </script>
             <% } %>
<% if (ui.isFullScreenEnabled()) { %>
</head>


<body class="<%=ui.getCssBodyTagClassName()%>" onload="<%=ui.writeJScriptBodyOnload()%>" onkeypress="checkReturnKeyPressedX(event);">
<% } %>
        <div class="module-name"><%=ui.getModuleName()%></div>
        <isa:debugmsg headtxt="Processed select options">
           <%=request.getAttribute("genericsearch.debug.selopt")%>
        </isa:debugmsg>
        <isa:debugmsg headtxt="Backend debugging infos">
           <%=ui.getDebugInfo()%>
        </isa:debugmsg>
        
        <% if (ui.getBodyIncludeTop() != null) { %>
                <jsp:include page="<%= ui.getBodyIncludeTop() %>" flush="true" /> <%-- BODY TOP INCLUDE --%>
        <% } %>

    <div id="<%=ui.getCssMainDivID()%>">
    
<% if (ui.isSelectOptionEnabled()) { %>
        

                <% if (baseui.isAccessible) { %>
                <a id="gensearchacc" href="#" title="<isa:translate key="gs.acc.acctext"/>" accesskey="<isa:translate key="gs.acc.acckey.searchcrit"/>" ></a>
                <% } %>
                <form name="GENERICSEARCH" id="gensearch" action="dummy" onSubmit="return false;">  <%-- This form declaration is not valid XHTML because of the "name" attribute. But "name" is required for the helpValues --%>
                <%
                        int maxScreenAreas = ui.getMaxScreenAreas();
                        for (int i = 1; i <= maxScreenAreas; i++) { 
                                String cssFilterClass = "filter-" +  String.valueOf(i);   // Generate CSS class name e.g. filter-1, filter-2, ... 
                %>
                                <div class="<%=cssFilterClass%>">
                                        <% if (baseui.isAccessible) { %>
                                                <a href="#end-search" title="<isa:translate key="gs.acc.beginsearch"/> <isa:translate key="access.lnk.jump"/>"></a> 
                                        <% } 

                                    if (baseui.isAccessible) { %>
                                       <table summary="<isa:translate key="access.layouttable.summary"/>" >
                                 <% } 
                                    else { %>      
                                       <table summary="<isa:translate key="access.layouttable.summary"/>" border="0">
                                 <% }      
                                                ui.setScreenElements();     // Set Elements again to refresh iterator %>
                                                <isa:iterate id="screlm" name="<%=GenericSearchUIData.RC_SCREENELEMENTS%>"
                                                        type="com.sap.isa.maintenanceobject.businessobject.GSProperty">
                                                        <% if (ui.isBelongingToScreenArea(cssFilterClass, screlm)) { %>
                                                                <% if (screlm.getType() != null  && screlm.getType().startsWith("box")) { %>
                                                                        <%      boolean contentExists = ui.setOptions(screlm);
                                                                                String jScriptToCall = ( screlm.getUIJScriptToCall() != null ? screlm.getUIJScriptToCall() : "" );
                                                                                if (contentExists)   { %>
                                                                                        <tr id="TRID<%=screlm.hashCode()%>">
                                                                                                <td class="label"><%= ui.writeSearchElementLabel("left", screlm)%></td>
                                                                                                <td class="input"><%= ui.writeSearchElementLabel("top", screlm)%>
                                                                                                        <select id="<%="ID"+screlm.hashCode()%>" name="<%=screlm.getRequestParameterName()%>" <%=ui.getJScriptOnChangeFinal(screlm, "evaluateX()")%> <%=jScriptToCall%> >
                                                                                                                <isa:iterate id="option" name="<%=GenericSearchUIData.RC_OPTIONS%>" type="com.sap.isa.core.util.table.ResultData">
                                                                                                                        <% if (option.getBoolean("HIDDEN") != true) { %>
                                                                                                                                <option value="<%=option.getString("VALUE")%>" <%=ui.writeObjSelectedInfo(srm, screlm, option)%> ><%=option.getString("DESCRIPTION")%></option>
                                                                                                                        <% } %>
                                                                                                                </isa:iterate>
                                                                                                        </select>
                                                                                                </td>
                                                                                        </tr>
                                                                                <% } // ContentExists %>
                                                                <% } /* if (screlm.getType() != null ....  */ %>
                                                        <% } /* if (ui.isBelongingToScreenArea ...  */ %>

                                                        <% if (ui.isBelongingToScreenArea(cssFilterClass, screlm)) { %>
                                                                <% if (screlm.getType() != null  && screlm.getType().startsWith("radio")) { %>
                                                                        <%      boolean contentExists = ui.setOptions(screlm);
                                                                                String jScriptToCall = ( screlm.getUIJScriptToCall() != null ? screlm.getUIJScriptToCall() : "" );
                                                                                int rdcnt = 0;  // Count to build the id (e.g IDhashcode+1)
                                                                                if (contentExists)   { %>
                                                                                        <tr id="TRID<%=screlm.hashCode()%>">
                                                                                                <td class="label"><%= ui.writeSearchElementLabel("left", screlm)%></td>
                                                                                                <td class="input"><%= ui.writeSearchElementLabel("top", screlm)%>
                                                                                                   <isa:iterate id="option" name="<%=GenericSearchUIData.RC_OPTIONS%>" type="com.sap.isa.core.util.table.ResultData">
                                                                                                       <% if (option.getBoolean("HIDDEN") != true) { %>
                                                                                                               <% String inputID = (rdcnt == 0 ? ("ID"+screlm.hashCode()) : ("ID"+screlm.hashCode()+"+"+String.valueOf(rdcnt)));%>
                                                                                                               <input type="radio" id="<%=inputID%>" name="<%=screlm.getRequestParameterName()%>" value="<%=option.getString("VALUE")%>" <%=ui.getJScriptOnChangeFinal(screlm, "evaluateX()")%> <%=jScriptToCall%> <%=ui.writeObjSelectedInfo(srm, screlm, option)%> ><%=option.getString("DESCRIPTION")%><br/>
                                                                                                       <% } %>
                                                                                                       <% rdcnt++; %>
                                                                                                   </isa:iterate>
                                                                                                </td>
                                                                                        </tr>
                                                                                <% } // ContentExists %>
                                                                <% } /* if (screlm.getType() != null ....  */ %>
                                                        <% } /* if (ui.isBelongingToScreenArea ...  */ %>

                                                        <% if (ui.isBelongingToScreenArea(cssFilterClass, screlm)) { %>
                                                                <% if (screlm.getType() != null && screlm.getType().startsWith("input")) { %>
                                                                        <tr id="TRID<%=screlm.hashCode()%>">
                                                                                <td class="label"><%= ui.writeSearchElementLabel("left", screlm)%></td>
                                                                                <td class="input">
                                                                                        <% String cssClass = ((screlm.getHelpValuesMethod() != null && screlm.getHelpValuesMethod().length() > 0) ? "input-3" : "input-1"); 
                                                                                           String jScriptToCall = ( screlm.getUIJScriptToCall() != null ? screlm.getUIJScriptToCall() : "" );
                                                                                           String inputAlt = ((screlm.getHelpValuesMethod() != null && screlm.getHelpValuesMethod().length() > 0) ? ("alt=\""+WebUtil.translate(pageContext, "access.help.values", new String[] {"1"})+"\"") : ""); %>
                                                                                        <%= ui.writeSearchElementLabel("top", screlm)%>
                                                                                        <input <%=inputAlt%> id="<%="ID"+screlm.hashCode()%>" type="text" name="<%=screlm.getRequestParameterName()%>" class="<%=cssClass%>" <%=jScriptToCall%> <%=ui.writeInputValueInfo(srm, screlm, null)%> size="30" maxlength="40" />
                                                                                        <% if (screlm.getHelpValuesMethod() != null  &&  screlm.getHelpValuesMethod().length() > 0) { %>
                                                                                                <%= HelpValuesSearchUI.getJSCallPopupCoding(screlm.getHelpValuesMethod(),"GENERICSEARCH","", pageContext) %> 
                                                                                        <% } /* if (screlm.getHelpValuesMethod() ...  */ %>

                                                                                </td>
                                                                        </tr>
                                                                <% } /* if (screlm.getType() ... */ %>
                                                        <% } /* if (ui.isBelongingToScreenArea ... */ %>

                                                        <% if (ui.isBelongingToScreenArea(cssFilterClass, screlm)) { %>
                                                                <% if (screlm.getType() != null && (screlm.getType().startsWith("date")  && ! screlm.getType().startsWith("daterange"))) { %>
                                                                        <tr id="TRID<%=screlm.hashCode()%>">
                                                                                <td class="label"><%= ui.writeSearchElementLabel("left", screlm)%></td>
                                                                                <td class="input">
                                                                                        <% String transKeyDateFormat = "gs.search.dtfr." + ui.getDateFormat(); %>
                                                                                        <%= ui.writeSearchElementLabel("top", screlm)%>
                                                                                        <input id="<%="ID"+screlm.hashCode()%>" type="text" name="<%=screlm.getRequestParameterName()%>" class="input-3" onblur="dateFormatCheck(this, '<%=ui.getDateFormat()%>', '<isa:translate key="<%=transKeyDateFormat%>"/>')" <%=ui.writeDateFormatToolTip()%> <%=ui.writeInputValueInfo(srm, screlm, null)%> maxlength="12" />
                                                                                        <% if ( ! baseui.isAccessible) { // calendar control is not yet accessable, so don't show it %>
                                                                                          <a href="#" class="icon" onclick="openCalendarWindow('<%=ui.getDateFormatForCalendarControl()%>', '<%="ID"+screlm.hashCode()%>');">
                                                                                                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/calendar.gif") %>" width="16" height="16" alt="<% // isa:translate key="b2b.order.shipto.icon.calendar"/ %>" />
                                                                                          </a>
                                                                                        <% } %>
                                                                                </td>
                                                                        </tr>
                                                                <% } /* if (screlm.getType() */ %>
                                                        <% } /* if (ui.isBelongingToScreenArea ... */ %>

                                                        <% if (ui.isBelongingToScreenArea(cssFilterClass, screlm)) { %>
                                                                <% if (screlm.getType() != null && screlm.getType().startsWith("daterange") && ! screlm.getType().startsWith("daterange2L")) { %>
                                                                        <tr id="TRID<%=screlm.hashCode()%>">
                                                                                <td class="label"><%= ui.writeSearchElementLabel("left", screlm)%></td>
                                                                                <td class="input">
                                                                                        <% String transKeyDateFormat = "gs.search.dtfr." + ui.getDateFormat(); %>
                                                                                        <%= ui.writeSearchElementLabel("top", screlm)%>
                                                                                        <input id="<%="ID"+screlm.hashCode()%>_LOW"  type="text" name="<%=screlm.getRequestParameterName()%>_low"  class="input-2" onblur="dateFormatCheck(this, '<%=ui.getDateFormat()%>', '<isa:translate key="<%=transKeyDateFormat%>"/>')" <%=ui.writeDateFormatToolTip()%> <%=ui.writeInputValueInfo(srm, screlm, "_low")%> size="11" maxlength="12" />
                                                                                        <% if ( ! baseui.isAccessible) { // calendar control is not yet accessable, so don't show it %>
                                                                                          <a href="#" class="icon" onclick="openCalendarWindow('<%=ui.getDateFormatForCalendarControl()%>', '<%="ID"+screlm.hashCode()%>_LOW');">
                                                                                                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/calendar.gif") %>" width="16" height="16" alt="<% // isa:translate key="b2b.order.shipto.icon.calendar"/ %>" />
                                                                                          </a><% } %>&nbsp;
                                                                                        <input id="<%="ID"+screlm.hashCode()%>_HIGH" type="text" name="<%=screlm.getRequestParameterName()%>_high" class="input-2" onblur="dateFormatCheck(this, '<%=ui.getDateFormat()%>', '<isa:translate key="<%=transKeyDateFormat%>"/>')" <%=ui.writeDateFormatToolTip()%> <%=ui.writeInputValueInfo(srm, screlm, "_high")%> size="11" maxlength="12" />
                                                                                        <% if ( ! baseui.isAccessible) { // calendar control is not yet accessable, so don't show it %>
                                                                                          <a href="#" class="icon" onclick="openCalendarWindow('<%=ui.getDateFormatForCalendarControl()%>', '<%="ID"+screlm.hashCode()%>_HIGH');">
                                                                                                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/calendar.gif") %>" width="16" height="16" alt="<% // isa:translate key="b2b.order.shipto.icon.calendar"/ %>" />
                                                                                          </a>
                                                                                        <% } %>
                                                                                </td>
                                                                        </tr>
                                                                <% } /* if (screlm.getType()... */ %>
                                                        <% } /* if (ui.isBelongingToScreenArea ... */ %>

                                                        <% if (ui.isBelongingToScreenArea(cssFilterClass, screlm)) { %>
                                                                <% if (screlm.getType() != null && screlm.getType().startsWith("daterange2L")) { %>
                                                                        <tr id="TRID<%=screlm.hashCode()%>">
                                                                                <td class="label"><%= ui.writeSearchElementLabel("left", screlm, "_LOW")%><div class="separator"></div><%= ui.writeSearchElementLabel("left", screlm, "_HIGH")%></td>
                                                                                <td class="input">
                                                                                        <% String transKeyDateFormat = "gs.search.dtfr." + ui.getDateFormat(); %>
                                                                                        <%= ui.writeSearchElementLabel("top", screlm, "_LOW")%>
                                                                                        <input id="<%="ID"+screlm.hashCode()%>_LOW"  type="text" name="<%=screlm.getRequestParameterName()%>_low"  class="input-3" onblur="dateFormatCheck(this, '<%=ui.getDateFormat()%>', '<isa:translate key="<%=transKeyDateFormat%>"/>')" <%=ui.writeDateFormatToolTip()%> <%=ui.writeInputValueInfo(srm, screlm, "_low")%> size="11" maxlength="12"/>
                                                                                        <% if ( ! baseui.isAccessible) { // calendar control is not yet accessable, so don't show it %>
                                                                                          <a href="#" class="icon" onclick="openCalendarWindow('<%=ui.getDateFormatForCalendarControl()%>', '<%="ID"+screlm.hashCode()%>_LOW');">
                                                                                                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/calendar.gif") %>" width="16" height="16" alt="<% // isa:translate key="b2b.order.shipto.icon.calendar"/ %>" />
                                                                                          </a><% } %><div class="separator"></div><%= ui.writeSearchElementLabel("top", screlm, "HIGH")%>
                                                                                        <input id="<%="ID"+screlm.hashCode()%>_HIGH" type="text" name="<%=screlm.getRequestParameterName()%>_high" class="input-3" onblur="dateFormatCheck(this, '<%=ui.getDateFormat()%>', '<isa:translate key="<%=transKeyDateFormat%>"/>')" <%=ui.writeDateFormatToolTip()%> <%=ui.writeInputValueInfo(srm, screlm, "_high")%> size="11" maxlength="12"/>
                                                                                        <% if ( ! baseui.isAccessible) { // calendar control is not yet accessable, so don't show it %>
                                                                                          <a href="#" class="icon" onclick="openCalendarWindow('<%=ui.getDateFormatForCalendarControl()%>', '<%="ID"+screlm.hashCode()%>_HIGH');">
                                                                                                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/calendar.gif") %>" width="16" height="16" alt="<% // isa:translate key="b2b.order.shipto.icon.calendar"/ %>" />
                                                                                          </a>
                                                                                        <% } %>
                                                                                </td>
                                                                        </tr>
                                                                <% } /* if (screlm.getType()... */ %>
                                                        <% } /* if (ui.isBelongingToScreenArea ... */ %>

                                                        <% if (ui.isBelongingToScreenArea(cssFilterClass, screlm)) { %>
                                                                <% if (screlm.getType() != null && screlm.getType().startsWith("text")) { %>
                                                                        <tr id="TRID<%=screlm.hashCode()%>">
                                                                                <td class="input" colspan="2"><isa:translate key="<%=screlm.getString()%>"/>
                                                                                </td>
                                                                        </tr>
                                                                <% } /* if ("text".equals(screlm ... */ %>
                                                        <% } /* if (ui.isBelongingToScreenArea ... */ %>

                                                </isa:iterate>
                                        </table>

                                        <%-- Add hidden fields, which shouldn't be placed into the <table> --%>
                                        <% ui.setScreenElements();     // Set Elements again to refresh iterator %>
                                        <isa:iterate id="screlm" name="<%=GenericSearchUIData.RC_SCREENELEMENTS%>"
                                                type="com.sap.isa.maintenanceobject.businessobject.GSProperty">
                                                <% if (screlm.getType() != null && screlm.getType().startsWith("hidden")) { %>
                                                        <input id="<%="ID"+screlm.hashCode()%>" tabindex="-1" type="hidden" name="<%=screlm.getRequestParameterName()%>" size="255" value="<%=screlm.getValue()%>" />
                                                <% } %>
                                        </isa:iterate>

                                </div>
                        <% } // END for %>

                        <%-- // START BUTTON --%>
                        <div class="buttons">
                                <% if (baseui.isAccessible) { %> <label for="gsbuttonstart"><isa:translate key="gs.acc.but.start"/> </label><% } %>
                                <a class="button" id="gsbuttonstart" href="#" onclick="this.className='button-disabled';<%=ui.getJScriptOnStartButton()%>" <%=ui.writeStartButtonTargetInfo()%> <% if (!baseui.isAccessible) { %>title="<isa:translate key="gs.acc.but.start"/>" <% } %> ><isa:translate key="gs.but.start"/></a>
                                <% if (ui.isExpandedResultlist()) {%>
                                <a class="button" id="gsbuttonexpand" href="#" style="display: none" onclick="expandedResultlistHandler(this.id, true)"><isa:translate key="gs.but.expand"/></a>
                                <a class="button" id="gsbuttoncollapse" href="#"  onclick="expandedResultlistHandler(this.id, true)"><isa:translate key="gs.but.collapse"/></a>
                                <% } %>
                                
                        </div>
                        
                        <% if (baseui.isAccessible) { %>
                                <a name="end-search" title="<isa:translate key="b2b.gs.acc.endsearch"/>"></a>
                        <% } %>
                </form>
<% } %>


                <%-- // +++++++++++++++ RESULT LIST START +++++++++++++ --%>
           <% if (ui.isResultListEnabled()) { 
                   String messagesFromBackend = ui.getMessagesFromBackend(1);
                   if(messagesFromBackend != null) { %>
                      <div class="filter-result-msg" >
                         <%= messagesFromBackend %>
                      </div>
                <% }
                   // In case of backend messages exits, write result message only if documents found! 
                   // If no backend messages exist, write also result message about zero documents found!
                   if ( ( (messagesFromBackend != null || "".equals(messagesFromBackend)) &&  ui.getNumberOfReturnedDocuments(1) > 0 ) || 
                        ( (messagesFromBackend == null || "".equals(messagesFromBackend)) && (ui.isRequestStarted()  ||  ui.getNumberOfReturnedDocuments(1) > 0) ) ) { %>
                        <div class="filter-result-msg" >
                           <% if (baseui.isAccessible) { %><a href="#" id="GSACCRESULTMSG"></a><table summary="<isa:translate key="access.layouttable.summary"/>"><td <%=ui.getTabIndex(true)%> title="<isa:translate key="gs.acc.search.result"/>"> <% } %>
                            <%=ui.writeEventMessages(1)%>
                           <% if (baseui.isAccessible) { %></td></table> <% } %>
                        </div>
                <% } /* if (ui.isRequestStarted()) */ %>

                
                <% if (ui.getNumberOfReturnedDocuments(1) > 0) {%>

                        <div class="filter-result">
                                <% if (ui.getBodyIncludeBeforeResult() != null) { %>
                                                <jsp:include page="<%= ui.getBodyIncludeBeforeResult()%>" flush="true" /> <%-- BODY BEFORE RESULT LIST INCLUDE --%>
                                <% } %>
                                <% String appbase_gs_table1_title = WebUtil.translate(pageContext, "appbase.gs.table1", null);
                                   // Accessibility requirement
                                   if (baseui.isAccessible())
                                   {
                                       int appbase_gs_table1_row_nro = ui.getNumberOfReturnedDocuments(1);
                                       int appbase_gs_table1_col_nro = ui.getNumberOfVisibleColumns();
                                       String appbase_gs_table1_col_cnt = Integer.toString(appbase_gs_table1_col_nro);
                                       String appbase_gs_table1_row_cnt = Integer.toString(appbase_gs_table1_row_nro);
                                %>
                                <a id="appbase_gs_table1_begin"
                                    href="#appbase_gs_table1_end"
                                    title="<isa:translate key="access.table.begin" arg0="<%=appbase_gs_table1_title%>" arg1="<%=appbase_gs_table1_row_cnt%>" arg2="<%=appbase_gs_table1_col_cnt%>" arg3="1" arg4="<%=appbase_gs_table1_row_cnt%>"/>">
                                </a>        
                                <%
                                }
                                %>                                
                                <table cellspacing="0" cellpadding="0" summary="<%=appbase_gs_table1_title%>">

                                        <% // GENERATE HEADER %>
                                        <tr>
                                            <% ui.setResultListHeader(); %>
                                            <% String colHeadFinal = "";
                                               String thCssClass = "first";
                                               boolean firstPty = true; %>
                                            <isa:iterate id="listheader" name="<%=GenericSearchUIData.RC_RESULTLIST_HEADER%>"
                                                type="com.sap.isa.maintenanceobject.businessobject.GSProperty">
                                                <% if ( ! "hidden".equals(listheader.getType())   &&  
                                                        ! "$MSGLINE".equals(listheader.getName()) &&
                                                        ! "$MSGTYPE".equals(listheader.getName())) { %>
                                                    <% if ( listheader.getWriteUnderProperty() == null  ||  listheader.getWriteUnderProperty().length() <= 0  || baseui.isAccessible) { %>
                                                        <% if ( ! firstPty) { 
                                                               thCssClass = ""; %>
                                                               </th>
                                                        <% }
                                                           firstPty = false;
                                                        %>
                                                        <th scope="col" <%=ui.getTabIndex(true)%> class="<%= thCssClass %>">
                                                            <%=ui.writeResultListHeader(listheader)%>
                                                    <% } else { %>
                                                            <div style="margin: 4px"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/1x1.gif") %>" alt="Strich" width="100%" height="1px" /></div>
                                                            <%=ui.writeResultListHeader(listheader)%>
                                                    <% } %>
                                                <% } %>
                                            </isa:iterate>
                                          </th> <%-- Close last opend <th> --%>
                                        </tr>

                                        <% // GENERATE ROWS %>
                                        <% ui.setResultListRows(); %>
                                        <isa:iterate id="singlerow" name="<%=GenericSearchUIData.RC_RESULTLIST_ROWS%>"
                                                type="com.sap.isa.core.util.table.ResultData">
                                                <tr class="<%= evenOdd[++line % 2]%>">
                                                        <% ui.setResultListFields();
                                                           firstPty = true; 
                                                           String msgLine = "";
                                                           String msgType = ""; %>
                                                        <isa:iterate id="rowfields" name="<%=GenericSearchUIData.RC_RESULTLIST_FIELDS%>"
                                                                type="com.sap.isa.maintenanceobject.businessobject.GSProperty">
                                                                <% if ( ! "hidden".equals(rowfields.getType())) {
                                                                        String fieldType = "FIELD";
                                                                        String fieldValue = "";
                                                                        String iconPath = ""; 
                                                                        boolean fieldReadOnly = false;
                                                                        ResultData outField = ui.fieldOutputHandler(singlerow, rowfields);
                                                                        if (outField != null && outField.getNumRows() > 0) {
                                                                                outField.first();             // ONLY the FIRST row will be used
                                                                                fieldType = outField.getString("TYPE");
                                                                                fieldValue = outField.getString("VALUE");
                                                                                fieldValue = ("FIELD".equals(fieldType) ? ui.encodeHTML(outField.getString("VALUE"))  /* note 1340113 */
                                                                                                                        : outField.getString("VALUE"));
                                                                                fieldReadOnly = outField.getBoolean("READONLY");
                                                                                iconPath = outField.getString("ICONPATH");
                                                                                if (iconPath != null && iconPath.length() > 0) {
                                                                                        iconPath = WebUtil.getMimeURL(pageContext, iconPath); 
                                                                                }
                                                                        }
                                                                        if ("$MSGLINE".equals(rowfields.getName())) {
                                                                            msgLine = fieldValue;
                                                                            continue;  // Special field which will not be processed here
                                                                        }
                                                                        if ("$MSGTYPE".equals(rowfields.getName())) {
                                                                            msgType = fieldValue;
                                                                            continue;  // Special field which will not be processed here
                                                                        }
                                                                        boolean isWriteUnderProperty = (rowfields.getWriteUnderProperty() != null  && rowfields.getWriteUnderProperty().length() > 0  && !baseui.isAccessible ? true : false); 
                                                                        %>
                                                                        <% if ( ! firstPty  &&  ! isWriteUnderProperty) {   // firstPty and isWrite.. are disjunct !! %> 
                                                                               </td>
                                                                        <% } 
                                                                           firstPty = false; 
                                                                         %>
                                                                        <% if ( rowfields.getLinkParamClass() == null ) { %>
                                                                                <% if (! isWriteUnderProperty) { %>
                                                                                    <td <%=ui.writeResultTitlePerTD(rowfields)%> <%=ui.getTabIndex(true)%> <%=(iconPath !=null && iconPath.length() > 0 ? "style=\"text-align: center\"": "")%> class="<%=rowfields.getCssClassName() != null ? rowfields.getCssClassName() : ""%>">
                                                                                <% } else { %>
                                                                                    <div style="margin: 4px 0px;"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/justadot.gif") %>" alt="Strich" width="100%" height="1px" /><div>
                                                                                <% } %>
                                                                                <% if (iconPath != null && iconPath.length() > 0) { %>
                                                                                     <img src="<%=iconPath%>" alt="<%=outField.getString("ICONTITLE")%>" border="<%=outField.getString("ICONBORDER")%>" width="<%=outField.getString("ICONWIDTH")%>" height="<%=outField.getString("ICONHIGHT")%>" />
                                                                                <% } else if ( rowfields.isReadOnly()  ||  fieldReadOnly ) { %>
                                                                                     <%=ui.writeResultlistValue(rowfields, fieldValue)%>
                                                                                <% } else { %>
                                                                                     <%=ui.writeResultlistInputField(rowfields, fieldValue, line, fieldReadOnly)%>
                                                                                <% } %>
                                                                        <% } else { %>
                                                                                <%-- // Dynamic Content Link Parameter Build up --%>
                                                                                <% String linkParameter = ui.getLinkParameter(singlerow, rowfields); 
                                                                                String appsUrl = (rowfields.getHyperlink() != null ? WebUtil.getAppsURL(pageContext, null, rowfields.getHyperlink(), null, null, false) : "");
                                                                                if (appsUrl != null  &&  appsUrl.length() > 0) {
                                                                                    appsUrl = ((appsUrl + linkParameter).indexOf("?") > 0 ? appsUrl : (appsUrl + "?")); 
                                                                                } %>
                                                                                <% if ( ! isWriteUnderProperty) { %>
                                                                                    <td <%=ui.writeResultTitlePerTD(rowfields)%> <%=ui.getTabIndex(true)%> <%=(iconPath !=null && iconPath.length() > 0 ? "style=\"text-align: center\"": "")%> class="<%=rowfields.getCssClassName() != null ? rowfields.getCssClassName() : ""%>">
                                                                                <% } else { %>
                                                                                   <div style="padding: 4px"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/justadot.gif") %>" alt="Strich" width="100%" height="1px" /></div>
                                                                                <% } %>
                                                                                        <a href="<%=appsUrl%><%=linkParameter%>" <%=(rowfields.getLinkTargetFrameName() != null ? ("target=\""+rowfields.getLinkTargetFrameName()+"\"") : "")%> <%= (ui.isExpandedResultlist() ? " onclick=\"expandedResultlistHandler('gsbuttoncollapse', true)\" " : "") %>>
                                                                                                <% if (iconPath != null && iconPath.length() > 0) { %> 
                                                                                                        <img src="<%=iconPath%>"  alt="<%=outField.getString("ICONTITLE")%>" border="<%=outField.getString("ICONBORDER")%>" width="<%=outField.getString("ICONWIDTH")%>" height="<%=outField.getString("ICONHIGHT")%>" />
                                                                                                <% } else { %>
                                                                                                        <%=ui.writeResultlistValue(rowfields , fieldValue)%>
                                                                                                <% } %>
                                                                                        </a>
                                                                        <% } /* if ( rowfields.getLinkParamClass */ %>
                                                                <% } /* if ( ! "hidden".equals */ %> 
                                                        </isa:iterate>
                                                   </td> <%-- Close last opend <td> --%>
                                                </tr>
                                                <% if (msgLine != null  &&  msgLine.length() > 0  &&  !"D".equals(msgType)) { %>
                                                <tr class="<%= evenOdd[line % 2] %>">  <% /* Output error line <<<<<<<<<<<<<<<<< */%>
                                                  <td colspan="<%=Integer.toString(ui.getNumberOfVisibleColumns() - 1)%>">
                                                    <div class="<%=("E".equals(msgType) ? "error-items" : "info") %>"><span><%=msgLine%></span></div>
                                                  </td>
                                                </tr>
                                                <% } %>
                                        </isa:iterate>

                                </table>
                                <% 
                                if (baseui.isAccessible()) 
                                { 
                                %>
                                    <a id="appbase_gs_table1_end"
                                        href="#appbase_gs_table1_begin"
                                        title="<isa:translate key="access.table.end" arg0="<%=appbase_gs_table1_title%>"/>"></a>        
                                <% 
                                } 
                                %>
                                <% if (ui.getBodyIncludeAfterResult() != null) { %>
                                                <jsp:include page="<%= ui.getBodyIncludeAfterResult()%>" flush="true" /> <%-- BODY AFTER RESULT LIST INCLUDE --%>
                                <% } %>
                  </div> <%--filter-result --%>

                <% } /* if (ui.getNumberOfReturnedDocuments */ %>
<% } /* if (ui.isResultListEnabled */%>

    </div> 
<%-- GREENORANGE BEGIN --%>
	<div id="zgo_sign">
		<b><font color="01663E">Fres-co Online Sales</font></b><br/>
		<b><font color="01663E">a Division of Goglio SpA</font></b><br/>
		Via Dell'Industria 7<br/>
		21020 Daverio (VA) - Italy<br/>
		Phone: +39 0332 940240 <br/>
		Fax: +39 0332 940716 <br/>
		P.I. IT00870210150 <br/>
		Capitale sociale Euro 10.449.000<br/>
		Registro Imprese Milano n.00870210150<br/>
		C.C.I.A.A. Milano Rea N. 1856<br/>
		(Mecc.Export MIO78374)<br/>
	</div>
<%-- GREENORANGE END --%>    
    <%-- end: id="<%=ui.getCssMainDivID()%>" --%>
               <% if (ui.getBodyIncludeBottom() != null) { %>
                               <jsp:include page="<%= ui.getBodyIncludeBottom()%>" flush="true" /> <%-- BODY BOTTOM INCLUDE --%>
               <% } %>
    <% if (ui.isExpandedResultlist()) {%>
          <script type="text/javascript">
             <%=ui.getJScriptOnResultExpand()%>;
          </script>
    <% } else { %>
          <script type="text/javascript">
             <%=ui.getJScriptOnResultCollapse()%>;
          </script>
    <% } %>
<% if (ui.isFullScreenEnabled()) { %>
</body>
</html>
<% } %>