<%-- A generic page to diplay messages, which use the DisplayMessage object --%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>


<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title><isa:translate key="message.jsp.title"/></title>
  <isa:stylesheets/>

</head>

 <body class="message-page">
   <% ui.includePage("/appbase/message.inc.jsp");%>
 </body>
</html>

