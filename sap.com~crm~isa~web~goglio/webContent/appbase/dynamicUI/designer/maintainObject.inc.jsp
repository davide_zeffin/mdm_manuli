<%--
    Display an generic maintenance of a UI property object.
    Please define out this JSP a UI class like this:
	MaintenanceObjectDynamicUI dynamicUI = new PropertyMaintenanceUI(pageContext);

--%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.maintenanceobject.businessobject.*" %>
<%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>


<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=dynamicUI.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title><%= dynamicUI.getPageTitle() %></title>
  <isa:stylesheets/>


  <script type="text/javascript">
  
		<%-- Handle the domain relaxing --%>
		var liBehindFirstDot = location.hostname.indexOf(".") + 1;
        if (liBehindFirstDot > 0) {
            document.domain = location.hostname.substr(liBehindFirstDot);
        }  

    function clearOnLoad() {
        document.forms["<%=dynamicUI.getFormName()%>"].HelpValues.value='';
    }

    function confirmObject() {
           document.forms["<%=dynamicUI.getFormName()%>"].<%= ActionConstants.RC_CONFIRM %>.value='true';
           submitForm();
    }

    function cancelObject() {
           document.forms["<%=dynamicUI.getFormName()%>"].<%= ActionConstants.RC_CANCEL %>.value='true';
           submitForm();
    }
    
    function onSaveUIModelButtonClick() {
           document.forms["<%=dynamicUI.getFormName()%>"].<%= ActionConstants.RC_CONFIRM_SAVE %>.value='true';
           submitForm();    
    }

    function submitForm() {
      document.forms["<%=dynamicUI.getFormName()%>"].submit();
      return true;
    }
  </script>
  
<%@ include file="/appbase/dynamicUI/dynamicUI.js.inc.jsp"%> 

</head>

<body class=""<%=dynamicUI.getBodyClass()%>"">

<div id="content">

  <div class="module-name"><isa:moduleName name="/appbase/dynamicUI/designer/maintainObject.jsp" /></div>


  <div id="object-detail">
    <form method="get" action="<%=dynamicUI.getFormAction()%>" id="<%=dynamicUI.getFormName()%>">
      <div>
      <input type="hidden" name="readOnly" value="<%=dynamicUI.getReadOnly()%>"/>
      <input type="hidden" name="<%= ActionConstants.RC_COLLAPSE_GROUP %>" />
      <input type="hidden" name="<%= ActionConstants.RC_EXPAND_GROUP %>" />
      <input type="hidden" name="<%= ActionConstants.RC_PARENT_GROUP %>" />
      <input type="hidden" name="<%= ActionConstants.RC_FOCUS_CURRENT_ELEMENT %>" />
      <input type="hidden" name="<%= ActionConstants.RC_CURRENT_PAGE %>" value="<%=dynamicUI.getActivePage() %>"/>
      <input type="hidden" name="<%= ActionConstants.RC_NAVIGATE_TO_PAGE %>" value=""/>
      <input type="hidden" name="<%= ActionConstants.RC_CONFIRM %>" />
      <input type="hidden" name="<%= ActionConstants.RC_CANCEL %>" />
      <input type="hidden" name="<%= ActionConstants.RC_CONFIRM_SAVE %>" />      
      </div>



	<% dynamicUI.exitBeforeMaintenance(); %>

<% 
int groupindex = 0; 
%>


<div class="fw-du-uitype_assignblock"> <!-- start prop group -->                 
  <div class="fw-box">
  <div class="fw-box-top">
  <div></div></div>
  <div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3">     
  <div class="fw-box-content">    
  <h1>
     <%
     if (dynamicUI.currentUIElement.isRessourceKeyUsed()) { 
     if (!dynamicUI.currentUIElement.getDescription().equals("") ){%>   
	    <isa:translate key="<%=dynamicUI.currentUIElement.getDescription()%>"/> 
	 <%
	 }
     } 
     else { 		 
     %> 
       <%=dynamicUI.currentUIElement.getDescription()%>
     <%
     }%>
     
  </h1>

  <%@ include file="/appbase/dynamicUI/parts/partElementIteratorWithTable.inc.jsp"%>
 
  </div> <!-- fw-box-content End prop group --> 
  </div></div></div> <!-- fw-box-i1 -->
  <div class="fw-box-bottom"><div></div></div>
  </div> <!-- fw-box -->  
</div> 
            
</div> <%-- end: id="assignmentblock" --%>

	<% dynamicUI.exitAfterMaintenance(); %>

<%-- Buttons --%> 
		<div class="fw-button">
		<table class="fw-button-row" cellspacing="0" cellpadding="0" border="0" summary="<isa:translate key="fw.du.access.tab.design"/>">
		<tbody>
			<tr>
				<td class="fw-button-rowleft">
					<a href="#" onclick="confirmObject();" <% if (dynamicUI.isAccessible()) { %> title="<isa:translate key="fw.du.title.save.ipc.prop"/>" <% } %> >
					<span><b><isa:translate key="fw.du.but.confirm"/></b></span>
					</a>
				<span class="fw-button-spacer"></span>
					<a href="#" onclick="cancelObject();" <% if (dynamicUI.isAccessible()) { %> title="<isa:translate key="fw.du.but.cancel"/>" <% } %> >
					<span><b><isa:translate key="fw.du.but.cancel"/></b></span>
					</a>
				<span class="fw-button-spacer"></span>
				</td>
				<td class="fw-button-rowright">
				</td>
			</tr>
		</tbody>
		</table>  <%-- fw-button-row --%>
		</div>  <%-- fw-button --%>
  </form>
  </div>

  </div> <!-- content-->
</body>
</html>
