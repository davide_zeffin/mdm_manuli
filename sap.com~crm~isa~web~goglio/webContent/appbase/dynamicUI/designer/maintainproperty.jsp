<%--
    Display an generic maintenance of a UI property object.

--%>

<%@ page import="com.sap.isa.ui.uiclass.PropertyMaintenanceUI" %>
<%@ page import="com.sap.isa.ui.uiclass.MaintenanceObjectDynamicUI" %>


<% MaintenanceObjectDynamicUI dynamicUI = new PropertyMaintenanceUI(pageContext); %>


<%@ include file="/appbase/dynamicUI/designer/maintainObject.inc.jsp"%> 

