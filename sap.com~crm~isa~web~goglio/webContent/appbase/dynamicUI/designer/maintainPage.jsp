<%--
    Display an generic maintenance of a UI page object.

--%>

<%@ page import="com.sap.isa.ui.uiclass.PageMaintenanceUI" %>
<%@ page import="com.sap.isa.ui.uiclass.MaintenanceObjectDynamicUI" %>


<% MaintenanceObjectDynamicUI dynamicUI = new PageMaintenanceUI(pageContext); %>


<%@ include file="/appbase/dynamicUI/designer/maintainObject.inc.jsp"%> 

