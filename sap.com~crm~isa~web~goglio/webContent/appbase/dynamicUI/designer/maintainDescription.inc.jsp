<%--
     This include contains all common functionality to render the description maintenance.
     This include could only be used as an static include.
--%>
<%@ taglib uri="/isa" prefix="isa" %>


<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.TreeSet"%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.DescriptionData"%>
<%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants"%>
<%@ page import="com.sap.isa.core.util.*"%>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI"%>
<%@ page import="com.sap.isa.ui.uiclass.MaintenanceObjectDynamicUI" %>

<%
	MaintenanceObjectDynamicUI ui = (MaintenanceObjectDynamicUI)BaseUI.getIncludeContextFromRequest(request); 
%>

<script type="text/javascript">
<!--
function addNewLines() {
    document.forms["<%=ui.getFormName()%>"].<%= ActionConstants.RC_ADD_DESCRIPTION_LINE %>.value="true";
    submitFwDuForm();
}


function deleteDescr(language) {
    document.forms["<%=ui.getFormName()%>"].<%= ActionConstants.RC_DELETE_DESCRIPTION %>.value=language;
    submitFwDuForm();
}

function submitFwDuForm() {
     document.forms["<%=ui.getFormName()%>"].submit();
      return true;
}
//-->
</script>

          <div class="fw-areabox-static">                           <%-- start [areabox(static)] --%>
            <div class="fw-areabox-top"><div></div></div>
            <div class="fw-areabox-i1"> 
              <div class="fw-areabox-i2">
                <div class="fw-areabox-i3">                         <%-- end [areabox(static)] --%>
                <%-- content of the areabox --%>

                  <div class="fw-asmblock">                         <%-- [asmblock] --%>
                  
                    <div class="fw-asmblock-title">                      <%-- start [asmtitle] --%>
                      <table class="fw-asmblock-title">
                        <tr>                                        <%-- end [asmtitle] --%>
                          <td class="fw-asmblock-title-left">            <%-- [asm_title_left] --%>
                            Page Texts
                          </td>                                     <%-- [/asm_title_left] --%>
                          <td class="fw-asmblock-title-right">           <%-- [asm_title_right] --%>
                          </td>                                     <%-- [/asm_title_right] --%>
                        </tr>                                       <%-- start [/asmtitle] --%>
                      </table>
                    </div>                                          <%-- end [/asmtitle] --%>
                    <%-- content of the assignment block --%>
                    <div class="fw-asmblock-content">                    <%-- start [asmcontent] --%>
                      <table class="fw-group">
                        <tbody>
                          <tr>
                            <td class="fw-group-editcol">
                            </td>
                            <td class="fw-group-header" colspan="3">
                            Maintain page descriptions
                            </td>
                          </tr>
                          
<%	
	int ind = 0;
	String index = "" + ind;
%>
	<isa:iterate id="description" name="<%=ActionConstants.RC_DESCRIPTION_LIST%>"
			  type="com.sap.isa.maintenanceobject.backend.boi.DescriptionData">
		<%
		ind++;
		index = "" + ind;	%>
                     <tr>
                            <td class="fw-group-editcol">
			    <a href="#" onclick="deleteDescr('<%=description.getLanguage()%>');">
			        <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/fw-du-delete.gif") %>" alt="<isa:translate key="du.delete.description" />" /> 
			    </a>                            
                            </td>
			    <% HashMap langHash = (HashMap) request.getAttribute(ActionConstants.RC_LANGUAGE_MAP);
	               // get sorted view with TreeSet:
	               TreeSet langTree = new TreeSet(langHash.keySet());
	               if(langHash != null && langHash.size() > 0) { %>                            
                            <td class="fw-group-label">
                              <label for="language"><isa:translate key="um.clc.login.jsp.lang" /></label>
                            </td>
                            <td class="fw-group-input">
			               <select size="1" name="<%=ActionConstants.RC_DESCRIPTION_LIST_LANGU + "[" + index + "]" %>" id="language">
				               <% String def_language = description.getLanguage();
	                              Iterator it = langTree.iterator();
	                              while (it.hasNext()) {
	                                  String alias = (String)it.next();
	                                  String value = (String)langHash.get(alias);
	                                  String lang_selected = "";
	                                  if(def_language != null && def_language.equalsIgnoreCase(alias)){
	                                      lang_selected = "selected=\"selected\"";
	                                  } %>
				                      <option value="<%=JspUtil.encodeHtml(alias)%>" <%=lang_selected%>>
				                          <isa:translate key="<%=JspUtil.encodeHtml(value)%>" />
				                      </option>
				               <% } %>
			               </select>
                            </td>
			    <% } %> 
                            <td class="fw-group-input">
                              <input class="textinput" type="text"
			              id="<%= ActionConstants.RC_DESCRIPTION_LIST_TEXT +"[" +index + "]" %>"
			              name="<%= ActionConstants.RC_DESCRIPTION_LIST_TEXT+"[" + index+"]" %>"
			              size="30" 
			              maxlength="80"
			              value="<%= JspUtil.encodeHtml(description.getDescription()) %>" />                           
			            </td>			                               
                          </tr>		
	</isa:iterate>	
		<%
		for (int i = 1; i <= 2; i++) {
		ind++;
		index = "" + ind;	%>
		
		<tr>
			<td class="fw-group-editcol">
			</td>
			
			    <% HashMap langHash = (HashMap) request.getAttribute(ActionConstants.RC_LANGUAGE_MAP);
	               // get sorted view with TreeSet:
	               TreeSet langTree = new TreeSet(langHash.keySet());
	               if(langHash != null && langHash.size() > 0) { %>
	               <td class="fw-group-label">
			           <label for="language"><isa:translate key="um.clc.login.jsp.lang" /></label>
			       </td>
			       <td class="fw-group-input">
			               <select size="1" name="<%=ActionConstants.RC_DESCRIPTION_LIST_LANGU + "[" + index + "]" %>" id="language">
				               <% String def_language = ui.getLanguage();
	                              Iterator it = langTree.iterator();
	                              while (it.hasNext()) {
	                                  String alias = (String)it.next();
	                                  String value = (String)langHash.get(alias);
	                                  String lang_selected = "";
	                                  if(def_language != null && def_language.equalsIgnoreCase(alias)){
	                                      lang_selected = "selected=\"selected\"";
	                                  } %>
				                      <option value="<%=JspUtil.encodeHtml(alias)%>" <%=lang_selected%>>
				                          <isa:translate key="<%=JspUtil.encodeHtml(value)%>" />
				                      </option>
				               <% } %>
			               </select>
			    <% } %>
			</td>
			<td class="fw-group-input">
		           <input class="textinput" type="text"
			              id="<%= ActionConstants.RC_DESCRIPTION_LIST_TEXT +"[" +index + "]" %>"
			              name="<%= ActionConstants.RC_DESCRIPTION_LIST_TEXT+"[" + index+"]" %>"
			              size="30" 
			              maxlength="80"
			              value="" />
			</td>
		</tr>		
		<% } // end for %>
                        </tbody>
                      </table>
                    </div>                                          <%-- end [/asmcontent] --%>
                  </div>                                            <%-- [/asmblock] --%>
                  
		<div class="fw-button">
		<table class="fw-button-row" cellspacing="0" cellpadding="0" border="0" summary="<isa:translate key="fw.du.access.tab.design"/>">
		<tbody>
			<tr>
				<td class="fw-button-rowleft">
					<a href="#" onclick="addNewLines();">
					<span><b><isa:translate key="fw.du.but.add.lines"/></b></span>
					</a>
				</td>
				<td class="fw-button-rowright">
				</td>
			</tr>
		</tbody>
		</table>  <%-- fw-button-row --%>
		</div>  <%-- fw-button --%>                  
    <div>
        <input type="hidden" name="<%= ActionConstants.RC_ADD_DESCRIPTION_LINE %>" />
        <input type="hidden" name="<%= ActionConstants.RC_DELETE_DESCRIPTION %>" />
    </div>
                </div> <%-- fw-areabox-i3 --%>     
              </div> <%-- fw-areabox-i2 --%>
            </div> <%-- fw-areabox-i1 --%>
            <div class="fw-areabox-bottom"><div></div></div>
          </div> <%-- fw-areabox-static --%>       <%-- end [/areabox(static)] --%>

