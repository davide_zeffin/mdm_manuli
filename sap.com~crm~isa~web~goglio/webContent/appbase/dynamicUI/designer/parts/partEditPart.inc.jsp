<%-- This include contains all common functionality to render a thumbnail.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.businessobject.Property" %>
<%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %>

<%
   String styleDisplay = "style=\"display: none\"";
%>
<% if (dynamicUI.currentIteratorElement instanceof Property) { 
       styleDisplay = "";
   }
%>   
   <a href="<isa:webappsURL name="dynamicUIMaintenance/editProperty.do"/>?<%=ActionConstants.RC_EDIT_PROPERTY%>=<%= dynamicUI.currentIteratorElement.getTechKey().getIdAsString()%>">
        <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/fw_du_edit.gif") %>" width="16" height="16" title="<isa:translate key="du.editproperty"/>" alt="<isa:translate key="du.editproperty"/>" <%= styleDisplay %> />
   </a>
