<%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %>


<% String checked = (dynamicUI.getObject().isWizardMode())?"checked=\"checked\"":""; 
   String exitField = "onclick=\"submitFwDuForm();\""; %>
	  <input type="checkbox"
			 id="<%= ActionConstants.RC_WIZARD_MODE %>"
			 name="<%= ActionConstants.RC_WIZARD_MODE %>"
			 value="true" <%=checked %> <%=exitField%>/>
	  <label id="<%="label_" + ActionConstants.RC_WIZARD_MODE %>" for="<%= ActionConstants.RC_WIZARD_MODE %>"></label>
	  <isa:translate key="fw.du.lab.wizard.mode"/>
