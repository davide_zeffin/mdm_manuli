<%-- This include contains all common functionality to render a thumbnail.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %>


	<a href="<isa:webappsURL name="dynamicUIMaintenance/moveDownAllowedValue.do"/>?<%= ActionConstants.RC_EDIT_PROPERTY %>=<%= dynamicUI.currentIteratorElement.getTechKey().getIdAsString() %>&<%= ActionConstants.RC_CURRENT_ALLOWED_VALUE %>=<%=value.getValue()%>">
      <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/fw_du_movedown.gif") %>" width="14" height="16" title="<isa:translate key="du.movedown"/>" alt="<isa:translate key="du.movedown"/>" /> </a>    