
<% if (dynamicUI.isEditable()) { %>
	<table class="fw-du-designer-nav">
	  <tr>
	    <td class="input">
	        <%@ include file="/appbase/dynamicUI/designer/parts/partWizardMode.inc.jsp"%>
	    </td>
	    <td class="input">
	        <%@ include file="/appbase/dynamicUI/designer/parts/partChangeUIType.inc.jsp"%>
	    </td>
	  </tr>
	</table>
<% } %>
