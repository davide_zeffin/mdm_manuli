<%--
******************************************************************************** 
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008

    Display an generic maintenance of a description object.

       
    $Revision: #1 $
    $Date: 2008/04/18 $
********************************************************************************
--%>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType/>         


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="fw.du.title.maint.descr"/></title>
       <isa:stylesheets theme=""/>    
  </head>
  <body class="maintain-description">

	<%@ include file="/appbase/dynamicUI/designer/maintainDescription.inc.jsp"%>

  </body>
</html>