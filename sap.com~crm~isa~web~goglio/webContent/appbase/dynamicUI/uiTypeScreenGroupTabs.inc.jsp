<%-- This include contains all common javascript functionality to maintain or display a
     maintenance object.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
     Also the java script function: openWin must be defined.
--%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.TabStripHelper" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI"%> 

<%
String disabled;
String tabTxt = WebUtil.translate(pageContext, "appbase.jsp.propertypages", null);
if (dynamicUI.isAccessible()){
    TabStripHelper mtabStrip = dynamicUI.getTabStrip();
    int tabItems = mtabStrip.size();
    String strTabItems = Integer.toString(tabItems);
%>
<div>
    <a href="#tabmaintainance-end"
       id="tabmaintainance-begin"
       title="<isa:translate key="access.tab.begin" arg0="<%=tabTxt%>" arg1="<%=strTabItems%>"/>"
    ></a>
</div>
<%
}
%>
<div id="objectDataInput">
<%-- included table for displaying navigator tab strips --%>
<%
int mo_line = 0;
if (dynamicUI.showMenu()) {
	DynamicShopUI mo_ui = dynamicUI;
%>
<%@ include file="/appbase/maintenanceobject/maintenanceobject.menu.inc.jsp" %>
<%
}
int groupindex = 0;
%>
    <div id="formfields">

  <div class="objectDataGroup"> <!-- start prop group -->                 
    <div class="fw-box">
    <div class="fw-box-top"><div></div></div>
    <div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3">     
    <div class="fw-box-content">    
    <table>
	<% while (dynamicUI.hasNextElement()) {
			  dynamicUI.useNextElement(); %>
			          
    <%
    groupindex++;
    if (!dynamicUI.currentIteratorElement.isHidden()) {
        String grpTxt = WebUtil.translate(pageContext, dynamicUI.currentIteratorElement.getDescription(), null);
        String groupEndId = "group" + Integer.toString(groupindex) + "-end";
        String groupBeginId = "group" + Integer.toString(groupindex) + "-begin";
        String groupEndRef = "#" + groupEndId;
        String groupBeginRef = "#" + groupBeginId;
        if (dynamicUI.isAccessible()) {
    %>
            <div>
                <a href="<%=groupEndRef%>"
                   id="<%=groupBeginId%>"
                   title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"
                ></a>
            </div>
        <%
        }
        %>
        
            

		<%-- Include here the next JSP-include   --%>		
		<%
		 pageContext.getOut().flush(); 
		 dynamicUI.includeUIElement(dynamicUI.currentIteratorElement, DynamicUIObjectUI.MAIN_JSP_INCLUDE); 
		 %>
<%
    } // end: if (!dynamicUI.currentIteratorElement.isHidden()) {
    %>
    
        
<% } %> <%-- End while (dynamicUI.hasNextElement() --%>
    </table>
     </div> <!-- fw-box-content End prop group --> 
     </div></div></div> <!-- fw-box-i1 -->
     <div class="fw-box-bottom"><div></div></div>
     </div> <!-- fw-box -->  
   </div> <%-- end: class="objectDataGroup" --%>    
    </div> <%-- end: id="formfields" --%>


<%
if (dynamicUI.isAccessible()) {
%>
    <div>
        <a href="#tabmaintainance-begin"
               id="tabmaintainance-end"
               title="<isa:translate key="access.tab.end" arg0="<%=tabTxt%>"/>"
        ></a>
    </div>
<%
}
%>
</div> <%-- end: id="objectDataInput" --%>