<%-- This include contains all common functionality to render the calendar icon and provide
     the calendar control .
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%> 

  <%
  if (dynamicUI.getReadOnly().equals("false")&& !property.isReadOnly()) { %>

 	<a href="#" onclick="openCalendarWindowDomainRelaxed('<%= property.getDefaultDateFormat() %>', '<%= property.getRequestParameterName() %>')">
 			<img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/calendar.gif") %>" title="<isa:translate key="fw.du.title.calendar"/>" alt="<isa:translate key="fw.du.title.calendar"/>" />
	</a>
  <%
  }%>
  		
