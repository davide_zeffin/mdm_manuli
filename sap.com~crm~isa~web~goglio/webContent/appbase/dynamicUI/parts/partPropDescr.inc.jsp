<%-- This include contains all common functionality to render a Property description.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
	  <%
	   String helpAvailTxt = WebUtil.translate(pageContext, "access.help.available", new String[]{"1"});
	   if (!(!helpDisplayed && property.isHelpAvailable())) {
			  helpAvailTxt = "";
	   }
	  %>
      
	  <%= dynamicUI.getPropertyDescription(property)%>
	  <a href="#" title="<isa:translate key="access.radio.grp.begin" arg0="<%=property.getDescription()%>"/>"></a>