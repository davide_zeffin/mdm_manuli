<%-- This include contains all common functionality to render the page navigation buttons.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>


<% if (dynamicUI.isPageNavigationSupported()) { 
       String disabledP = "";
       String disabledN = "";
       String jsFunctionP = "";
       String jsFunctionN = "";
       String aClassP = "";
       String aClassN = "";
       String prevPage = "";
       List pageList = dynamicUI.getPageList();
           int pageNumber = 1;
           for (int i = 0; i < pageList.size(); i++) {
			   UIElementGroupData uiPage = (UIElementGroupData)pageList.get(i); 
			   if (uiPage.getName().equals(dynamicUI.getActivePage()) && i == 0) { 
				   disabledP = "disabled=\"disabled\"";
				   aClassP = "class=\"disabled\"";
				   jsFunctionN = "onclick=\"navigateTo('";
			       jsFunctionN = jsFunctionN + uiPage.getName();
			       jsFunctionN = jsFunctionN + "');\"";				   
			   }
			   else if (uiPage.getName().equals(dynamicUI.getActivePage()) && pageNumber == pageList.size()) {
				   disabledN = "disabled=\"disabled\"";
				   aClassN = "class=\"disabled\"";
			   }
			   if (i > 0 && uiPage.getName().equals(dynamicUI.getActivePage())) {
				   jsFunctionP = "onclick=\"navigateTo('";
			       jsFunctionP = jsFunctionP + prevPage;
			       jsFunctionP = jsFunctionP + "');\"";
			   }
			   if (pageNumber <= pageList.size() && prevPage.equals(dynamicUI.getActivePage())) {
				   jsFunctionN = "onclick=\"navigateTo('";
			       jsFunctionN = jsFunctionN + uiPage.getName();
			       jsFunctionN = jsFunctionN + "');\"";				   
			   }
			   prevPage = uiPage.getName();
			   pageNumber++;
           } %>
		<div class="fw-button">
		<table class="fw-button-row" cellspacing="0" cellpadding="0" border="0" summary="<isa:translate key="fw.du.access.tab.design"/>">
		<tbody>
			<tr>
				<td class="fw-button-rowleft">
					<a <%= aClassP %> href="#" <%= jsFunctionP %> <%= disabledP %>>
					<span><b><isa:translate key="fw.du.but.previous"/></b></span>
					</a>
				<span class="fw-button-spacer"></span>
					<a <%= aClassN %> href="#" <%= jsFunctionN %> <%= disabledN %>>
					<span><b><isa:translate key="fw.du.but.next"/></b></span>
					</a>
				<span class="fw-button-spacer"></span>
				</td>
				<td class="fw-button-rowright">
				</td>
			</tr>
		</tbody>
		</table>  <!-- fw-button-row -->
		</div>  <!-- fw-button -->
<% } %>		