<%-- This part calculate the colspan 
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>

<%
	// render additional column for edit mode
    if (dynamicUI.isEditable()) {
    	colspanmessage++;
    }	
	
    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.THUMBNAIL)) { 
    	colspan++;
    	colspanmessage++;
    }
    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.ADD_DOC_LINKS)) {
    	colspan++;
    	colspanmessage++;
    }
    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.RESET_VALUE)) {
    	colspan++;
    	colspanmessage++;
    }    
    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.DISPLAY_ALL_OPTION)) {
    	colspan++;
    	colspanmessage++;
    }    
    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.DISPLAY_HELP_LINK)) {
    	colspan++;
    	colspanmessage++;
    }
%>