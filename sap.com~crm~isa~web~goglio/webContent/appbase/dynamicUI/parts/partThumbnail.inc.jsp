<%-- This include contains all common functionality to render a thumbnail.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCImageLinkData" %>


	<%  
	if (currentUIObject instanceof IPCImageLinkData) {
		String imageURL = ((IPCImageLinkData)currentUIObject).getImageURL();
		String thumbnailURL = ((IPCImageLinkData)currentUIObject).getThumbnailURL();
		String width = ((IPCImageLinkData)currentUIObject).getThumbnailWidth();
		String height = ((IPCImageLinkData)currentUIObject).getThumbnailHeight();
	%>
	<% if (((IPCImageLinkData)currentUIObject).getDisplayImageType().equals(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_NOTHING)) {  %>
	       <%-- nothing TODO --%>
	<% } else if (((IPCImageLinkData)currentUIObject).getDisplayImageType().equals(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_HYPERLINK)) {  %>
	       <%-- display link to image --%>
	       <a href="<%= imageURL %>" title="<isa:translate key="fw.du.title.disp.image"/>"><isa:translate key="fw.du.lnk.disp.image"/></a>
	<% } else { %>
	       <%-- display thumbnail and link to image --%>	
    	       <a href="<%= imageURL %>"><img src="<%= thumbnailURL %>" title="<isa:translate key="fw.du.title.enl.image"/>" alt="<isa:translate key="fw.du.title.enl.image"/>" width="<%= width %>" height="<%= height %>" /></a>
    	<% } %>
    <%
	} %>