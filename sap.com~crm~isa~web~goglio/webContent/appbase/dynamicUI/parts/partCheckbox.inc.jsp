<%-- This include contains all common functionality to render a checkbox.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
	<%
	if (dynamicUI.isAccessible()) {
	   String fieldTxt = WebUtil.translate(pageContext, property.getName(), null);
	   String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
	   if (disabled.length() == 0) {
		   unavailableTxt = "";
	   }
	%>
		<span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
	<%
	}
	  boolean displayStar = true;
	  if (dynamicUI.currentIteratorElement.getStatus() != null) {
	     if (dynamicUI.currentIteratorElement.getStatus().getImageURL() != null &&
	             dynamicUI.currentIteratorElement.getStatus().getImageURL().length() > 0) { 
	    	 displayStar = false;
	     }
	  }	
	%>
	  <input type="checkbox"
			 id="<%= property.getRequestParameterName() %>"
			 name="<%= property.getRequestParameterName() %>"
			 value="true"
			 <%=checked %> <%= dynamicUI.getKeyPressEventFunction() %>
			 <%=exitField%> <%=disabled%> <%= focusField %>/>
			 <label id="<%="label_" + property.getName()%>" for="<%= property.getName() %>"><%= dynamicUI.getPropertyDescription(property)%><% if (displayStar) { %> <%=required %> <% } %></label>			 
	  <%
	  if (dynamicUI.isAccessible()) {
	  %>
		</span>
	  <%
	  }
	  %>