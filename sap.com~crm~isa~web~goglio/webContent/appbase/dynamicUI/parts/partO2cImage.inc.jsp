<%-- This include contains all common functionality to render a o2c image.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCImageLinkData" %>


	<%  
	if (currentUIObject instanceof IPCImageLinkData) {
		String o2cImageURL = ((IPCImageLinkData)currentUIObject).getO2cImageURL();
	%>
	<% if (((IPCImageLinkData)currentUIObject).getO2cImageURL() != null && ((IPCImageLinkData)currentUIObject).getO2cImageURL().length() > 0) {  %>
	       <%-- display link to o2c image --%>
	       <a href="<%= o2cImageURL %>" title="<isa:translate key="fw.du.title.3d.image"/>"><isa:translate key="fw.du.lnk.3d.image"/></a>
            <object classid = "CLSID:BF3CD111-6278-11D2-9EA3-00A0C9251384"
                codebase = "http://www.o2c.de/download/o2cplayer.cab#version=1,9,9,135" 
                name = "O2CPlayer">
                <param name = "ObjectURL" value = "<%= o2cImageURL %>">
                <param name = "BorderStyle" value = 1>
                <param name = "Appearance" value = 1>
                <param name = "AnimPlaying" value = true>
                <param name = "BackColor" value = 16777215>
                <embed codebase = "http://www.o2c.de/dl_plugin.htm" type = "application/x-o2c-object"
                       name = "O2CPlayer"
                       src = "<%= o2cImageURL %>" PARAM_BorderStyle = 1
                       PARAM_Appearance = 1 PARAM_AnimPlaying = true PARAM_BackColor = #ffffff>
            </object>
   	<% } %>
    <%
	} %>