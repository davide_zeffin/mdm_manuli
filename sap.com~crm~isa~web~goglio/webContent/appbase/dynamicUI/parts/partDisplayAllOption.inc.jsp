<%-- This include contains all common functionality to render a link to display all option.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCPropertyData" %>


	<%  
	if (property instanceof IPCPropertyData) {
		String allOptionURL = ((IPCPropertyData)property).getAllOptionsLink();
	%>
	<% if (allOptionURL != null && allOptionURL.length() > 0) {  %>
	       <a href="<%= allOptionURL %>" title="<isa:translate key="fw.du.title.all.option"/>"><isa:translate key="fw.du.lnk.all.option"/></a>
   	<% } %>
    <%
	} %>
