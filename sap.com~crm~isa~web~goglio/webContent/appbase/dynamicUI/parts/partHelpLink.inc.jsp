<%-- This include contains all common functionality to render a help link.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCPropertyData" %>


	<%  
	if (property instanceof IPCPropertyData) {
	    if (((IPCPropertyData)property).isDisplayHelpLink()) {
		    String helpLinkURL = ((IPCPropertyData)property).getHelpLinkURL();
    	    String helpLinkLabel = ((IPCPropertyData)property).getHelpLinkLabel();
	%>
	    <% if (helpLinkURL != null && helpLinkURL.length() > 0) {  %>
	           <a href="<%= helpLinkURL %>" title="<isa:translate key="fw.du.title.help"/>"><%= helpLinkLabel %></a>
   	    <% } %>
   	 <% } %>
    <%
	} %>