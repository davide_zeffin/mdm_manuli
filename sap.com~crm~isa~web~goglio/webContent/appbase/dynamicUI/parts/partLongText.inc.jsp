<%-- This include contains all common functionality to render aa additional long text.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>

<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCPropertyData" %>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCAllowedValueData" %>
	<% 
	{ // make longText a local variable
	String longText = "";
	if(currentUIObject instanceof IPCPropertyData) {
		longText = ((IPCPropertyData)currentUIObject).getLongText();
	%>
 <% }
	else if (currentUIObject instanceof IPCAllowedValueData) { 
		longText = ((IPCAllowedValueData)currentUIObject).getLongText();
    }
	
	if (longText != null && longText.length() > 0) {%>
	    <%= longText %>
<%  }
	} %>	