<%-- This include contains all common functionality to render a pattern for an input field.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>

<% String pattern = property.getInputPattern();
   if (pattern != null && pattern.length() > 0) { %>
       <%= pattern %>
<% } %>