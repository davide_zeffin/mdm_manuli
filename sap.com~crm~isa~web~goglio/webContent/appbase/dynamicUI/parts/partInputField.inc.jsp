<%-- This include contains all common functionality to render a input field.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
            <%
              if (dynamicUI.isAccessible()) {
              String fieldTxt = dynamicUI.getPropertyDescription(property);
              String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
              if (disabled.length() == 0) {
                   unavailableTxt = "";
               }
            %>
            <span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
            <%
              }

              String helpAvailTxt = WebUtil.translate(pageContext, "access.help.available", new String[]{"1"});
              if (!(!helpDisplayed && property.isHelpAvailable())) {
                   helpAvailTxt = "";
              }
              String helpValuesTxt = "";
              if (dynamicUI.getReadOnly().equals("false") &&
			  	property.getHelpValuesMethod()!= null &&
				property.getHelpValuesMethod().length() >0) {
                  String tabOrder = "2";
                  if (!(!helpDisplayed && property.isHelpAvailable())) {
                      tabOrder = "1";
                  }
                  helpValuesTxt = WebUtil.translate(pageContext, "access.help.values", new String[]{tabOrder});
              }
              String helpSearchTxt = "";
               if (dynamicUI.getReadOnly().equals("false") &&
				property.getHelpValuesMethod()!= null &&
				property.getHelpValuesMethod().length() >0) {
                   String strTabOrder = "3";
                   int tabOrder = 3;
                   if (!(dynamicUI.getReadOnly().equals("false") &&
				     property.getHelpValuesMethod()!= null &&
				     property.getHelpValuesMethod().length() >0)) {
                       tabOrder--;
                   }
                   if (!(!helpDisplayed && property.isHelpAvailable())) {
                       tabOrder--;
                   }
                   strTabOrder = Integer.toString(tabOrder);
                   helpSearchTxt = WebUtil.translate(pageContext, "access.help.search", new String[]{strTabOrder});
               }
               %>
            <input class="textinput"
                   type="text"
                   id="<%= property.getRequestParameterName() %>"
                   name="<%= property.getRequestParameterName() %>"
                   size="<%= property.getSize() %>"
                   maxlength="<%= property.getMaxLength() %>"
                   value="<%= JspUtil.encodeHtml(property.getString()) %>"
                   title="<%=helpAvailTxt + helpValuesTxt + helpSearchTxt%>"
                   <%=exitField%> <%=disabled%> <%= dynamicUI.getKeyPressEventFunction() %>
                   <%= focusField %>
            />	
            <%
            if (dynamicUI.isAccessible()) {
            %>
            </span>
           <% } %>