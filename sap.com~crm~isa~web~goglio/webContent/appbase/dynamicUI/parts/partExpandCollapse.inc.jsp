
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData" %>

<%-- Expand/Collapse the Assignment Block   --%>
<% if (((UIElementGroupData)dynamicUI.currentUIElement).isExpanded()) { %>
       <a href="#" onclick="collapseElement('<%= dynamicUI.parentUIElement.getTechKey().getIdAsString() %>', '<%= dynamicUI.currentUIElement.getName() %>');" title="<isa:translate key="fw.du.title.hide" arg0="<%= groupName %>"/>"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/fw-du-collapse.gif") %>" title="<isa:translate key="fw.du.title.hide" arg0="<%= groupName %>"/>" alt="<isa:translate key="fw.du.title.hide" arg0="<%= groupName %>"/>" /><%= groupName %></a>
<% }else { %>
       <a href="#" onclick="expandElement('<%= dynamicUI.parentUIElement.getTechKey().getIdAsString() %>', '<%= dynamicUI.currentUIElement.getName() %>');" title="<isa:translate key="fw.du.title.show" arg0="<%= groupName %>"/>"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/fw-du-expand.gif") %>" title="<isa:translate key="fw.du.title.hide" arg0="<%= groupName %>"/>" alt="<isa:translate key="fw.du.title.hide" arg0="<%= groupName %>"/>" /><%= groupName %></a>
<% } %>
