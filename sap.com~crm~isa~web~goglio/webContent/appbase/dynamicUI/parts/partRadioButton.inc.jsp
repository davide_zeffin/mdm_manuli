<%-- This include contains all common functionality to render radio button.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
	  <%
	  String checked = value.getValue().equals(property.getString())?"checked=\"checked\"":"";
	  disabled = (dynamicUI.getReadOnly().equals("true") || 
				  property.isDisabled() || property.isReadOnly() || 
				  value.isReadOnly()) ?"disabled=\"disabled\"":"";
	  
	  %>
	  <input type="radio"
			 id="<%= property.getRequestParameterName() + "_" + Integer.toString(indexOfValue) %>"
			 name="<%= property.getRequestParameterName() %>"
			 value="<%= value.getValue() %>"
			 <%=checked %> <%=disabled%> <%=exitField%> <%= dynamicUI.getKeyPressEventFunction() %> <%= focusField %>/>
	  <label id="<%= "label_" + property.getRequestParameterName() + "_" + Integer.toString(indexOfValue) %>" for="<%= property.getRequestParameterName() + "_" + Integer.toString(indexOfValue) %>">
                 <% if (value.isRessourceKeyUsed()) { %>
                        <isa:translate key="<%= value.getDescription() %>"/> 
                 <%
                    }
                    else { %>
                         <%=value.getDescription()%>
                 <%
                    }
   			  boolean displayStar = true;
			  if (dynamicUI.currentIteratorElement.getStatus() != null) {
			     if (dynamicUI.currentIteratorElement.getStatus().getImageURL() != null &&
			             dynamicUI.currentIteratorElement.getStatus().getImageURL().length() > 0) { 
			    	 displayStar = false;
			     }
			  }                 
                 %>
	  <% if (displayStar) { %> <%=required %> <% } %></label>
