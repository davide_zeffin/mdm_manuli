


  <% if (!dynamicUI.isPageNavigationSupported()) { 
		 List elementList = 
			 dynamicUI.getObject().getElementGroupList(dynamicUI.parentUIElement.getName());
		 if (elementList != null && elementList.size() > 0) {
			 UIElementGroupData uiElementGroup = (UIElementGroupData)elementList.get(0);
			 List groupList = uiElementGroup.getElementList();
			 if (groupList != null && groupList.size() > 1) {
				 UIElementGroupData firstGroup = (UIElementGroupData)groupList.get(0);
				 if (firstGroup.getName().equals(dynamicUI.currentUIElement.getName())) { %>
				 <table>
				     <tr>
			      <% int listSize = groupList.size();
		             for (int i = 1; i < groupList.size(); i++) { 
		                 UIElementGroupData uiGroup = (UIElementGroupData)groupList.get(i); 
		                 if (!uiGroup.isHidden()) {%>
		                 <td>
		                 <% String navGroupName = uiGroup.getDescription();
		                    if (navGroupName.lastIndexOf('>') > 0) {
		                    	int rem = navGroupName.lastIndexOf('>');
		                    	rem++;
		                        navGroupName = navGroupName.substring(rem); 
		                    } %>
		                 <% if (uiGroup.isExpanded()) { %>
		                        <a href="#" onclick="navigateToSubComponent('<%= uiGroup.getName() %>');"><%= navGroupName %></a>		                 
		                 <% } 
		                    else {%>
		                        <a href="#" onclick="navigateExpandSubComponent('<%= dynamicUI.parentUIElement.getTechKey().getIdAsString() %>', '<%= uiGroup.getName() %>');"><%= navGroupName %></a>
		                 <% } %>
		                 <% if (i < (listSize - 1)) { %>
		                        &nbsp;|&nbsp;
		                 <% } %>
		                 </td>
		              <% } %>
		          <% } %>
		              </tr>
		          </table>
		      <% } %>
		  <% } %>
	  <% } %>
  <% } %>  
