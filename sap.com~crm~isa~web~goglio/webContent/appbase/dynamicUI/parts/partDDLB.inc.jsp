<%-- This include contains all common functionality to render a drop down list box.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
    <%
     if (disabled.length() > 0) {
         int indexOfValue = 0;
    %>
          <isa:iterate id="value" name="property"
                       type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">    

			  
         <%
           if (value.getValue().equals(property.getString())) {
              if (dynamicUI.isAccessible()) {
                 String fieldTxt = WebUtil.translate(pageContext, property.getName(), null);
                 String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
                 if (disabled.length() == 0) {
                 	unavailableTxt = "";
                 }
           %>
                <span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
             <%
              }
             %>
                <input class="textinput"
                       type="text"
                       id="<%= property.getRequestParameterName() %>"
                       name="<%= property.getRequestParameterName() %>"
                       <% if (value.isRessourceKeyUsed()) { %>
                              value="<isa:translate key="<%= value.getDescription() %>"/>" 
                       <%
                          }
                          else { %>
                               value="<%=value.getDescription()%>"
                       <%
                          }
                       %>                              
                       
                       <%=disabled%> />
               <%
               if (dynamicUI.isAccessible()) { %>
                </span>
               <%
              }
          }
          %>
      </isa:iterate>
            
   <%
    }
    else { %>
    <%
          if (dynamicUI.isAccessible()) {
               String fieldTxt = WebUtil.translate(pageContext, property.getName(), null);
               String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
               if (disabled.length() == 0) {
                   unavailableTxt = "";
               }%>                       
               <span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
          <%
          }
          String helpAvailTxt = WebUtil.translate(pageContext, "access.help.available", new String[]{"1"});
          if (!(!helpDisplayed && property.isHelpAvailable())) {
            helpAvailTxt = "";
          }
          %>
          	<select id="<%= property.getRequestParameterName() %>" name="<%= property.getRequestParameterName() %>"
            	title="<%=helpAvailTxt%>" <%=disabled%> <%=exitField%> <%= dynamicUI.getKeyPressEventFunction() %>
            	<%= focusField %>>
                	<isa:iterate id="value" name="property"
                    	type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">
                        <%
                        if (!value.isHidden()) {
                            String selected = value.getValue().equals(property.getString())?"selected=\"selected\"":"";
                        %>
                            <option value="<%= value.getValue() %>"
                                   <%=selected %> >
                                   <% if (property.isRessourceKeyUsed()) { %>
                                      <isa:translate key="<%=value.getDescription()%>"/>
                                   <%
                                   }
                                   else { %>
                                       <%=value.getDescription()%>
                                   <%
                                   }
                                   %>                                      
                            </option>
                        <%
                        } %>
                     </isa:iterate>
            </select>
          <%
          if (dynamicUI.isAccessible()) {%>
          	</span>
          <%
          }
	}
	%>