<%-- This include contains all common functionality to render a checkbox.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
	<%
	if (dynamicUI.isAccessible()) {
	   String fieldTxt = WebUtil.translate(pageContext, property.getName(), null);
	   String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
	   if (disabled.length() == 0) {
		   unavailableTxt = "";
	   }
	%>
		<span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
	<%
	}
	%>

           <isa:iterate id="value" name="property"
                    	type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">
	<% if ((!value.getValue().equals("")) && (!value.isHidden())){ %>                    	
       <tr>      
       <% 
         if (dynamicUI.isEditable()) { 
       %>
           <td class="fw-du-edit">
                <%@ include file="/appbase/dynamicUI/designer/parts/partMoveDownAllowedValue.inc.jsp"%>
                <%@ include file="/appbase/dynamicUI/designer/parts/partMoveUpAllowedValue.inc.jsp"%>
                <%@ include file="/appbase/dynamicUI/designer/parts/partSpacer.inc.jsp"%>
           </td>
       <%
         }
       %>  	
      	   <td>
      	   </td>      
           <td class="inputcheck_singleline" colspan="2">
               <%
               String checked = (property.getList().indexOf(value.getValue()) >= 0) ?"checked=\"checked\"":""; 
			   disabled = (dynamicUI.getReadOnly().equals("true") || 
			               property.isDisabled() || property.isReadOnly() || 
			              value.isReadOnly()) ?"disabled=\"disabled\"":"";
			   currentUIObject = value;
               %>

				  <input type="checkbox"
						 id="<%= property.getRequestParameterName() %>"
						 name="<%= property.getRequestParameterName() %>"
						 value="<%= value.getValue() %>"
						 <%=checked %> <%= dynamicUI.getKeyPressEventFunction() %>
						 <%=exitField%> <%=disabled%> <%= focusField %>/>
						 <label id="<%="label_" + property.getName()%>" for="<%= property.getName() %>">
			             <% if (value.isRessourceKeyUsed()) { %>
			                    <isa:translate key="<%=value.getDescription()%>"/>
			             <%
			                }
			                else { %>
			                    <%=value.getDescription()%>
			             <%
			                }
			             %>
						 <%=required %></label>
			   <%@ include file="/appbase/dynamicUI/parts/partLongText.inc.jsp"%>
		       <%@ include file="/appbase/dynamicUI/parts/partElementAnchor.inc.jsp"%>				 
           </td>
	  <% 
	    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.THUMBNAIL)) { 
	  %>
	       <td>
	       </td>
	  <%
	    }
	    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.ADD_DOC_LINKS)) {
	  %>
	       <td>	  
	       </td>
	  <%
	    } 
	    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.RESET_VALUE)) {
	  %>	
	       <td>	    
	       </td>
	  <%
	    }
	  %>           
       </tr>        
     <% } %>
           </isa:iterate> 			 
	  <%
	  if (dynamicUI.isAccessible()) {
	  %>
		</span>
	  <%
	  }
	  %>