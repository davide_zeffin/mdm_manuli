<%-- This include contains all common functionality to render the help icon and provide
     the corresponding help text.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%> 
         <% if (!helpDisplayed && property.isHelpAvailable()) { %>
            <a href="javascript:showHelp('<%= property.getName() %>')"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/help.gif") %>" title="<isa:translate key="mob.jsp.help"/>" alt="<isa:translate key="mob.jsp.help"/>" /></a>
         <%
                helpDisplayed = true;
            } %>