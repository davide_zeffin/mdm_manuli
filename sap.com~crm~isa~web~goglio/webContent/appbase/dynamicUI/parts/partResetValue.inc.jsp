<%-- This include contains all common functionality to render a link to reset the maintained value.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCPropertyData" %>


	<%  
	if (property instanceof IPCPropertyData) {
		String resetURL = ((IPCPropertyData)property).getResetURL();
	%>
	<% if (resetURL != null && resetURL.length() > 0) {  %>
	       <a href="<%= resetURL %>" title="<isa:translate key="fw.du.title.reset"/>"><isa:translate key="fw.du.lnk.reset"/></a>
   	<% } %>
    <%
	} %>
