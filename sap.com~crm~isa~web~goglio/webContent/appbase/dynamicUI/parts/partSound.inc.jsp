<%-- This include contains all common functionality to render a link to an sound file .
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCImageLinkData" %>


	<%  
	if (currentUIObject instanceof IPCImageLinkData) {
		String soundURL = ((IPCImageLinkData)currentUIObject).getSoundURL();
	%>
	<% if (((IPCImageLinkData)currentUIObject).getSoundURL() != null && ((IPCImageLinkData)currentUIObject).getSoundURL().length() > 0) {  %>
	       <%-- display link to sound file --%>
	       <a href="<%= soundURL %>" title="<isa:translate key="fw.du.title.play.sound"/>"><isa:translate key="fw.du.lnk.play.sound"/></a>
           <embed src="<%= soundURL %>" height = "2" width = "70" autostart = "false">
               <noembed>
                   <bgsound src="<%= soundURL %>" loop = "infinite">
               </noembed>
           </embed>
   	<% } %>
    <%
	} %>