<%-- This include contains all common functionality to render a link to a document.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCPropertyData" %>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.IPCAllowedValueData" %>

	<%  
	if(currentUIObject instanceof IPCPropertyData) {
		if(((IPCPropertyData)currentUIObject).isDisplayDocumentLink()){
			String docURL = ((IPCPropertyData)currentUIObject).getAdditionalDocumentURL();
			if(!docURL.equals("") && docURL != null) { %>
				<%-- display link to additional document --%>
		        <a href="<%= docURL %>" target="_blank" title="<isa:translate key="fw.du.title.display.doc"/>"><isa:translate key="fw.du.lnk.display.doc"/></a>
	    	<% } %>
	    <% } %>	
    <% } else if(currentUIObject instanceof IPCAllowedValueData) {
		if(((IPCAllowedValueData)currentUIObject).isDisplayDocumentLink()){
			String docURL = ((IPCAllowedValueData)currentUIObject).getAdditionalDocumentURL();
			if(!docURL.equals("") && docURL != null) { %>
				<%-- display link to additional document --%>
		        <a href="<%= docURL %>" target="_blank" title="<isa:translate key="fw.du.title.display.doc"/>"><isa:translate key="fw.du.lnk.display.doc"/></a>
	    	<% } %>
	    <% } %>	
    <% } %> 