<%-- This include contains all common functionality to render a multiple drop down list box.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
    <%
     if (disabled.length() > 0) {
         int indexOfValue = 0;
         boolean selectedValue = false;
    %>
          <isa:iterate id="value" name="property"
                       type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">    
<%
          if (!value.isHidden()) {
              String selected = (property.getList().indexOf(value.getValue()) >= 0) ?"selected=\"selected\"":"";
              if (selected != null && selected.length() > 0) { 
                  selectedValue = true;%>
                <input class="textinput"
                       type="text"
                       id="<%= property.getRequestParameterName() %>"
                       name="<%= property.getRequestParameterName() %>"
                       <% if (value.isRessourceKeyUsed()) { %>
                              value="<isa:translate key="<%= value.getDescription() %>"/>" 
                       <%
                          }
                          else { %>
                               value="<%=value.getDescription()%>"
                       <%
                          }
                       %>                              
                       
                       <%=disabled%> /><br/>  
           <% } %>
          
       <% } %>
          </isa:iterate>
       <% if (!selectedValue) { %>
           <input class="textinput"
                       type="text"
                       id=""
                       name=""
                       <%=disabled%> />       
       <% } %>            
   <%
    }
    else { %>
    <%
          if (dynamicUI.isAccessible()) {
               String fieldTxt = WebUtil.translate(pageContext, property.getName(), null);
               String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
               if (disabled.length() == 0) {
                   unavailableTxt = "";
               }%>                       
               <span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
          <%
          }
          String helpAvailTxt = WebUtil.translate(pageContext, "access.help.available", new String[]{"1"});
          if (!(!helpDisplayed && property.isHelpAvailable())) {
            helpAvailTxt = "";
          }
          %>
          	<select id="<%= property.getRequestParameterName() %>" name="<%= property.getRequestParameterName() %>"
            	title="<%=helpAvailTxt%>" size="<%= property.getHeight() %>" multiple="multiple" <%=disabled%> 
            	<%=exitField%> <%= dynamicUI.getKeyPressEventFunction() %> <%= focusField %>>
                	<isa:iterate id="value" name="property"
                    	type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">
                        <%
                        if (!value.isHidden()) {
                            String selected = (property.getList().indexOf(value.getValue()) >= 0) ?"selected=\"selected\"":"";
					        disabled = (dynamicUI.getReadOnly().equals("true") || 
								    property.isDisabled() || property.isReadOnly() || 
									value.isReadOnly()) ?"disabled=\"disabled\"":"";
                        
                        %>
                          <option value="<%= value.getValue() %>"
                                   <%=selected %> >
                                   <% if (value.isRessourceKeyUsed()) { %>
                                      <isa:translate key="<%=value.getDescription()%>"/>
                                   <%
                                   }
                                   else { %>
                                       <%=value.getDescription()%>
                                   <%
                                   }
                                   %> 
                          </option>
                     <% } %>
                     </isa:iterate>
            </select>
          <%
          if (dynamicUI.isAccessible()) {%>
          	</span>
          <%
          }
          %>
  <% } %>