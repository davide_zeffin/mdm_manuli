<%-- This include contains all common functionality to render a input interval for an input field.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>

<% String interval = property.getInputInterval();
   if (interval != null && interval.length() > 0) { %>
       <%= interval %>
<% } %>