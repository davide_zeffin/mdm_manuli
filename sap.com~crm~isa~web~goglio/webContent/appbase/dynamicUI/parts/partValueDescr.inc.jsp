<%-- This include contains all common functionality to render the description for the value.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
	  <%-- Description for the value --%>
	  <%
	  if (dynamicUI.isAccessible()) {
		String propDesc = WebUtil.translate(pageContext, property.getDescription(), null);
		String propValue = "";
        
		if (property.getValue() != null) {
			propValue = property.getValue().toString();
		}
		%>
		<span title="<isa:translate key="mob.value.description" arg0="<%=propDesc%>" arg1="<%=propValue%>"/>">
		<%
	  }
	  %>
	  <%=property.getValueDescription()%>
	  <%
	  if (dynamicUI.isAccessible()) {
	  %>
	    </span>
	  <%
	  }       
	  %>