<%-- This include contains all common functionality to render error and info messages.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
     The Java variable colspan must be defined.
--%>
      <isa:message id="errortext" name="<%=DynamicUIObjectUI.RA_MESSAGES%>" type="<%=Message.ERROR%>" property="<%=dynamicUI.getElementMessageKey(property)%>">
          <tr>
              <td colspan="<%= colspanmessage %>">
                  <div class="error"><span><%=errortext%></span></div>
              </td>
          </tr>
      </isa:message>
      <isa:message id="infotext" name="<%=DynamicUIObjectUI.RA_MESSAGES%>" type="<%=Message.INFO%>" property="<%=dynamicUI.getElementMessageKey(property)%>" >
          <tr>
              <td colspan="<%= colspanmessage %>">
                  <div class="info"><span><%=infotext%></span></div>
              </td>
          </tr>
      </isa:message>