<%-- This include contains all common functionality to render a icon for help values.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
      <%-- Help values method --%>
      <%
        if (dynamicUI.getReadOnly().equals("false") && property.getHelpValuesMethod()!= null && property.getHelpValuesMethod().length() >0) {
      %>
            <a href="javascript:helpValues('<%= property.getName() %>')"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/helpValues.gif") %>" title="<isa:translate key="mob.jsp.helpValues"/>" alt="<isa:translate key="mob.jsp.helpValues"/>" /></a>
      <%
        }
      /* Help values search */
        if (dynamicUI.getReadOnly().equals("false") && property.getHelpValuesSearch()!= null && property.getHelpValuesSearch().length() >0) { 
      %>
           <script type="text/javascript">
               <%= HelpValuesSearchUI.getJSPopupCoding(property.getHelpValuesSearch(), pageContext) %>
           </script>
               <%= HelpValuesSearchUI.getJSCallPopupCoding(property.getHelpValuesSearch(), dynamicUI.getFormName(), "", pageContext) %>
      <%
        }
      %>