<%-- This include contains all common functionality to render the status of the given element.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>

<% if (dynamicUI.currentIteratorElement.getStatus() != null) {
       if (dynamicUI.currentIteratorElement.getStatus().getImageURL() != null &&
               dynamicUI.currentIteratorElement.getStatus().getImageURL().length() > 0) { %>
        <% String statusText = "";
           if (dynamicUI.currentIteratorElement.getStatus().isRessourceKeyUsed()) { 
               statusText = WebUtil.translate(pageContext, dynamicUI.currentIteratorElement.getStatus().getDescription(), null);
           }
           else { 
               statusText = dynamicUI.currentIteratorElement.getStatus().getDescription();
           } %>               
               <img src="<%=WebUtil.getMimeURL(pageContext, dynamicUI.currentIteratorElement.getStatus().getImageURL()) %>" title="<%= statusText %>" alt="<%= statusText %>" />
    <% } %>
<% } %>
