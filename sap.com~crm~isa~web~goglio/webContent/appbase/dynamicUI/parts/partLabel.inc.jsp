<%-- This include contains all common functionality to render a label for an input field.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
	    <label id="<%="label_" + property.getRequestParameterName()%>" for="<%=property.getRequestParameterName()%>">
			<%  // d041773, Note 1043774
			   // check if the customizing contains language specific entries defined in customizingr3.xml
			  //String transLabel = property.getDescription();
    		  String transLabel = dynamicUI.getPropertyDescription(property); %>
    		  <%--    		  
			  String lang = "";
			  if (transLabel.charAt(transLabel.length()-3) == '_') {
			     lang = transLabel.substring(transLabel.length()-2);
				 String resourceKey = transLabel.substring(0, transLabel.length()-3);
				 transLabel = WebUtil.translate(pageContext, resourceKey, null);
				 transLabel = transLabel + " ( " + lang.toUpperCase() + " )";
			  }
			  --%>
			  <%
			  boolean displayStar = true;
			  if (dynamicUI.currentIteratorElement.getStatus() != null) {
			     if (dynamicUI.currentIteratorElement.getStatus().getImageURL() != null &&
			             dynamicUI.currentIteratorElement.getStatus().getImageURL().length() > 0) { 
			    	 displayStar = false;
			     }
			  }
			   %>
			   <%=transLabel%> <% if (displayStar) { %> <%=required %> <% } %>
	     </label>
