<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="java.util.List" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.TabStripHelper" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI"%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData" %>


<% 	DynamicUIObjectUI dynamicUI = (DynamicUIObjectUI)DynamicUIObjectUI.getFromRequest(request); 
		
   boolean abEdit = false;
   boolean showButtons = false;
   
   if (dynamicUI.isEditable()) {

	 List pageList = dynamicUI.getObject().getPageList(); 
     if (pageList.size() > 1) {
    	   showButtons = true;   
     }

	 List groupList = dynamicUI.getObject().getElementGroupList(dynamicUI.parentUIElement.getName()); 
     if (groupList != null && groupList.size() > 0) {
       UIElementGroupData uiElementGroup = (UIElementGroupData)groupList.get(0);
	   if (uiElementGroup.getElementList() != null && uiElementGroup.getElementList().size() > 1) {
           abEdit = true;
           showButtons = true;
	   }
     }
   }

   int groupindex = 0; 

   String groupName = "";
      if (!dynamicUI.currentUIElement.getDescription().equals("") ) { 
          if (dynamicUI.currentUIElement.isRessourceKeyUsed()) { 
	          groupName = WebUtil.translate(pageContext, dynamicUI.currentUIElement.getDescription(), null); 
 	      } else { 
	          groupName = dynamicUI.currentUIElement.getDescription();
	    }
      } 
  String statusStyleClass = (dynamicUI.currentIteratorElement.getStatus() != null) ? dynamicUI.currentIteratorElement.getStatus().getStyle() : ""; %>

<div class="fw-du-<%=dynamicUI.currentUIElement.getUiType() %> <%= statusStyleClass %>"> <!-- start prop group -->                 

  <% 
  if (showButtons) { %>
	<table>
	  <tr>
	    <td class="fw-edit-buttons" nowrap>
          <%@ include file="/appbase/dynamicUI/designer/parts/partDelete.inc.jsp"%>
          <%@ include file="/appbase/dynamicUI/designer/parts/partEditPart.inc.jsp"%>
  		  <% 
    	  if (abEdit) { %>
            <%@ include file="/appbase/dynamicUI/designer/parts/partMoveDown.inc.jsp"%>
            <%@ include file="/appbase/dynamicUI/designer/parts/partMoveUp.inc.jsp"%>
          <%
          }%>
          <%@ include file="/appbase/dynamicUI/designer/parts/partDesignerCheckbox.inc.jsp"%>
        </td>
        <td class="fw-edit-data" width=100%>  
  <%
  }%>
 
  <div class="fw-box">
  <div class="fw-box-top">
  <div></div></div>
  <div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3">     
  <div class="fw-box-content">    
  <h1>
   <%-- anchor point to set the focus --%>
      <a name="grouponfocus_<%= dynamicUI.currentUIElement.getName() %>" id="grouponfocus_<%= dynamicUI.currentUIElement.getName() %>" href="#"></a>
     <%-- Expand/Collapse the Assignment Block if it is allowed  --%>
     <% if (((UIElementGroupData)dynamicUI.currentUIElement).isToggleAllowed()) { %>
          <%@ include file="/appbase/dynamicUI/parts/partExpandCollapse.inc.jsp"%>
     <% }
        else { %>  
            <%= groupName %>
	<% } %>
	 <%-- Display the status image for the given element --%>
	 <%@ include file="/appbase/dynamicUI/parts/partElementStatus.inc.jsp"%>
  </h1> 
  
    <%-- Display the sub component links --%> 
<% if (((UIElementGroupData)dynamicUI.currentUIElement).isExpanded()) { %>
  	  <div class="fw-du-tablegroup"> 
      <%@ include file="/appbase/dynamicUI/parts/partSubComponentLinks.inc.jsp"%> 
      <%@ include file="/appbase/dynamicUI/parts/partElementIteratorWithTable.inc.jsp"%>
      </div> 
<% } %> 
  </div> <!-- fw-box-content End prop group --> 
  </div></div></div> <!-- fw-box-i1 -->
  <div class="fw-box-bottom"><div></div></div>
  </div> <!-- fw-box -->  
            
  <% 
  if (showButtons) { %>
	</tr>
	</table>
  <%
  }%>	

</div> <%-- end: id="assignmentblock" --%>
<br>

            