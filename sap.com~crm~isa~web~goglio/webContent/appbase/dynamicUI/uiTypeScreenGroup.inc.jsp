<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="java.util.List" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.TabStripHelper" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI"%>
<%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %>

<div>
<input type="hidden" name="<%= ActionConstants.RC_COLLAPSE_GROUP %>" />
<input type="hidden" name="<%= ActionConstants.RC_EXPAND_GROUP %>" />
<input type="hidden" name="<%= ActionConstants.RC_PARENT_GROUP %>" />
<input type="hidden" name="<%= ActionConstants.RC_CURRENT_PAGE %>" value="<%=dynamicUI.getActivePage() %>" />
<input type="hidden" name="<%= ActionConstants.RC_FOCUS_CURRENT_ELEMENT %>" />
</div>

<div class="fw-du-<%=dynamicUI.currentUIElement.getUiType() %>"> <!-- start assignment group -->                 
  <div class="fw-box">
  <div class="fw-box-top"><div></div></div>
  <div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3">     
  <div class="fw-box-content">    
  <h1>
     <%
     if (!dynamicUI.currentUIElement.getDescription().equals("") ) { 
         if (dynamicUI.currentUIElement.isRessourceKeyUsed()) { %>
	         <isa:translate key="<%=dynamicUI.currentUIElement.getDescription()%>"/> 
 	   <%
	     } else { %>
	         <%=dynamicUI.currentUIElement.getDescription()%>
	   <%
	   }
	   %>
	   
	 <%
	 } %>
  </h1>

  <%@ include file="/appbase/dynamicUI/parts/partElementIteratorWithTable.inc.jsp"%>
 
  </div> <!-- fw-box-content End prop group --> 
  </div></div></div> <!-- fw-box-i1 -->
  <div class="fw-box-bottom"><div></div></div>
  </div> <!-- fw-box -->  
</div>
