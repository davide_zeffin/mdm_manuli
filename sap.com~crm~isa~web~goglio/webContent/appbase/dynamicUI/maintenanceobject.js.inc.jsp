<%-- This include contains all common javascript functionality to maintain or display a 
     maintenance object.
     This include could only be used as an dynamic include. 
     An instance of  dynamicUI must be defined in the request context. 
     Also the java script function: openWindow must be defined.
--%>
	<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

  <%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %>
  <%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>

  <% DynamicUIObjectUI dynamicUI = (DynamicUIObjectUI)DynamicUIObjectUI.getIncludeContextFromRequest(request); %>
  
  <%@ include file="/appbase/dynamicUI/dynamicUI.js.inc.jsp"%>

  <script type="text/javascript">

	function myGetHelpValues(name, formName, index) {
	    exitByUser = true;
	    return getHelpValues(name, formName, index);
	}

    function submitForm() {
	  submitFwDuForm();
     }

    function changeObject() {
      document.forms["<%= dynamicUI.getFormName() %>"].ChangeShop.value='true';
      submitForm();
    }

    function helpValues(propertyName) {
      document.forms["<%= dynamicUI.getFormName() %>"].HelpValues.value=propertyName;
      submitForm();
      document.forms["<%= dynamicUI.getFormName() %>"].HelpValues.value="";
    }

    function SelectTabStrip(key) {
      document.forms["<%= dynamicUI.getFormName() %>"].<%=ActionConstants.RC_NAVIGATE_TO_PAGE%>.value=key;
      submitForm();
    }

    function exitPage() {
	  <% 
	  if (dynamicUI.getReadOnly().equals("false")) {  %>
		 if (exitByUser == false) {
	  		openWindow('closewindow');
  			}		
  	  <%
  	  } %>
    }
    
    function showHelp(propertyName) {
      var url = '<isa:webappsURL name="<%= dynamicUI.getHelpAction() %>"/>' + '?property=' + propertyName;
	  var sat = window.open(url, 'Help', 'width=550,height=370,menubar=no,scrollbars=yes,locationbar=no,resizable=yes');
	  sat.focus();
    }
    
  </script>
