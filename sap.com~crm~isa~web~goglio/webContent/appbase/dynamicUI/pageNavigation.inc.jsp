<%-- This include contains all common functionality to render the page navigation
     This include could only be used as an static include.
--%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.maintenanceobject.ui.UITypeRendererData" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI"%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData" %>
<%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %>


<%-- Show links and buttons for page navigation  --%>
<script type="text/javascript">
<!--
    
function copyMoveProperties(selectedPage, action) {
    document.forms["<%= dynamicUI.getFormName()%>"].<%= ActionConstants.RC_SELECTED_PAGE %>.value=selectedPage;
    document.forms["<%= dynamicUI.getFormName()%>"].action = action;
    document.forms["<%= dynamicUI.getFormName()%>"].submit();
    return true;    
}

//-->
</script>


<% if (dynamicUI.isPageNavigationSupported()) { %>

<div>
    <input type="hidden" name="<%= ActionConstants.RC_NAVIGATE_TO_PAGE %>" />
    <input type="hidden" name="<%= ActionConstants.RC_SELECTED_PAGE %>" />
</div>
<div class="fw-pagenav">
  <table class="fw-pagenav" cellPadding="0" cellSpacing="0">
  <% String trClass = ""; %>
  <% for (int r = 0; r < 3 ;r++) { 
     if (r == 0) {
    	 trClass = "fw-pagenav-nav";
     }
     else if (r == 1) {
    	 trClass = "fw-pagenav-description";
     }
     else {
    	 trClass = "fw-pagenav-functions";
     } %>
    <tr class="<%= trClass %>">
      <td class="fw-pagenav-left">
        <div><span></span></div>
      </td> <%-- fw-pagenav-left --%>
        <% List pageList = dynamicUI.getPageList();
           int pageNumber = 1;
		   boolean onlyOneNextPage = false;
    	   boolean displayLink = true;
		   boolean linkActiveInWizardMode = true;
           for (int i = 0; i < pageList.size(); i++) {
        	   String jsFunction = "";
        	   String aClass = "";
			   UIElementGroupData uiPage = (UIElementGroupData)pageList.get(i); 
			   if (uiPage.getName().equals(dynamicUI.getActivePage())) { 
			       jsFunction = "";
			       aClass = "selected";
			   } else {
				   aClass = "active";
			       jsFunction = "onclick=\"navigateTo('";
			       jsFunction = jsFunction + uiPage.getName();
			       jsFunction = jsFunction + "');\"";
			   } %>
	      <td class="fw-pagenav-step">
	        <%	
			  displayLink = true;
	        // First render two rows with the number and the page name
	          if (r == 0 || r == 1) { %>
	          <div>          
			<%	  if (dynamicUI.getObject().isWizardMode()) {
					  if (!(linkActiveInWizardMode || onlyOneNextPage )) {
					      displayLink = false;	
					  }	
				  }	
				  if (displayLink) {
					  onlyOneNextPage = false;
				%>
					      <a class="<%= aClass %>" href="#" <%= jsFunction %> title="<%= uiPage.getName() %>"><span><% if (r == 0) { %><%= pageNumber %><%} else {%><%= dynamicUI.getPageDescription(i) %><% }%></span></a>
			<%    }
				  else {
				%>
				      <a class="inactive" href="#"><span><% if (r == 0) { %><%= pageNumber %><%} else {%><%= dynamicUI.getPageDescription(i) %><% }%></span></a>
		   		<%
		   		  }
	          }
			%>
			<%-- Render in third row the buttons --%>
			<% if (r == 2) { 
			       if (dynamicUI.isEditable() && !uiPage.getName().equals(dynamicUI.getActivePage())) { %>
			           <div>
			           <a href="<isa:webappsURL name="/dynamicUIMaintenance/deletePage.do"/>?<%=ActionConstants.RC_SELECTED_PAGE%>=<%= uiPage.getTechKey().getIdAsString() %>" ><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/fw-du-delete.gif") %>" alt="<isa:translate key="fw.du.delete" arg0="<%= uiPage.getName() %>"/>" /></a>
			           </div>
			           <div class="fw-button">
	                          <% 
	                            String action = WebUtil.getAppsURL(pageContext,
	                                                             null,
	                                                             "/dynamicUIMaintenance/copyProperty.do",
	                                                             null,
	                                                             null,
	                                                             false);
	                          %>
	                          <a href="#" onClick="copyMoveProperties('<%= uiPage.getTechKey().getIdAsString() %>', '<%= action %>')" ><span><isa:translate key="fw.du.copy"/></span></a> 
	 		           </div>
			           <div class="fw-button">                         
	                          <% 
	                            action = WebUtil.getAppsURL(pageContext,
	                                                             null,
	                                                             "/dynamicUIMaintenance/moveProperty.do",
	                                                             null,
	                                                             null,
	                                                             false);
	                          %>
	                          
	                          <a href="#" onClick="copyMoveProperties('<%= uiPage.getTechKey().getIdAsString() %>', '<%= action %>')" ><span><isa:translate key="fw.du.move"/></span></a>		           
	            <% }
	               if (dynamicUI.isEditable()) {
	                       String action = WebUtil.getAppsURL(pageContext,
	                                                             null,
	                                                             "/dynamicUIMaintenance/editPage.do",
	                                                             null,
	                                                             null,
	                                                             false);
	                          %>
			           </div>
			           <div class="fw-button">                          
	                          <a href="#" onClick="copyMoveProperties('<%= uiPage.getName() %>', '<%= action %>')" ><span><isa:translate key="fw.du.texts"/></span></a>
	            <% } 		           
			   } %>
	        </div>
	      </td> <%-- fw-pagenav-step --%>
	           <%-- Check if the next page should be rendered with a link to navigate in wizard mode  --%>
               <%
               if (uiPage.getName().equals(dynamicUI.getObject().getWizardPage())) {
				   linkActiveInWizardMode = false;
				   onlyOneNextPage = true;
			   }
               pageNumber++; %>
        <% } %> <%-- end for pageList --%>
      <td class="fw-pagenav-right">
        <div><span></span></div>
      </td> <%-- fw-pagenav-right --%>
    </tr> <%-- fw-pagenav-nav --%>
     <% } %>    
  </table> <%-- fw-pagenav --%>
</div> <%-- fw-pagenav --%>
<% } %>