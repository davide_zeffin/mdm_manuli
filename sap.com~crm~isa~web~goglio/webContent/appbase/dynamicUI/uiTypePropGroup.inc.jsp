<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="java.util.Map" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.TabStripHelper" %>
<%@ page import="com.sap.isa.maintenanceobject.ui.UITypeRendererData" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI"%>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData" %>

<% 	DynamicUIObjectUI dynamicUI = (DynamicUIObjectUI)DynamicUIObjectUI.getFromRequest(request); 
%>		

<% 
Map invisibleParts = dynamicUI.getInvisibleParts();

int colspan = 2;
int colspanmessage = 2;
	
%>
       <%@ include file="/appbase/dynamicUI/parts/partCalculateColspan.inc.jsp"%>
       
   <% String groupName = "";
      if (!dynamicUI.currentUIElement.getDescription().equals("") ) { 
          if (dynamicUI.currentUIElement.isRessourceKeyUsed()) { 
	          groupName = WebUtil.translate(pageContext, dynamicUI.currentUIElement.getDescription(), null); 
 	      } else { 
	          groupName = dynamicUI.currentUIElement.getDescription();
	    }
      } %>
<tr>
       <% 
         if (dynamicUI.isEditable()) { 
       %>
           <td class="fw-du-edit">
               <%@ include file="/appbase/dynamicUI/designer/parts/partDelete.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/designer/parts/partEditPart.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/designer/parts/partMoveDown.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/designer/parts/partMoveUp.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/designer/parts/partDesignerCheckbox.inc.jsp"%>
           </td>
       <%
         }
       %>
       <% String statusStyleClass = (dynamicUI.currentIteratorElement.getStatus() != null) ? dynamicUI.currentIteratorElement.getStatus().getStyle() : ""; %>                                     
   <td class="fw-du-<%=dynamicUI.currentUIElement.getUiType()%> <%= statusStyleClass %>" colspan = "<%=colspan %>">
     <%-- Expand/Collapse the element group if it is allowed  --%>
     <% if (((UIElementGroupData)dynamicUI.currentUIElement).isToggleAllowed()) { %>
          <%@ include file="/appbase/dynamicUI/parts/partExpandCollapse.inc.jsp"%>
     <% }
        else { %>
            <%= groupName %>
	 <% } %>
	 <%-- Display the status image for the given element --%>
	 <%@ include file="/appbase/dynamicUI/parts/partElementStatus.inc.jsp"%>
  <%-- anchor point to set the focus --%>
  <div>
      <a name="grouponfocus_<%= dynamicUI.currentUIElement.getName() %>" id="grouponfocus_<%= dynamicUI.currentUIElement.getName() %>" href="#"></a>
  </div>  	 	 	 
   </td> 
</tr>

    <%
    // String grpTxt = WebUtil.translate(pageContext, dynamicUI.currentIteratorElement.getDescription(), null);
    if (dynamicUI.isAccessible()) {
    %>
        <div>
            </div>
    <%
    }
    %>    
<% if (((UIElementGroupData)dynamicUI.currentUIElement).isExpanded()) { %>
    <%@ include file="/appbase/dynamicUI/parts/partElementIterator.inc.jsp"%>
<% } %>            
<%
if (dynamicUI.isAccessible()) {%>
<div>
</div>
<%
} %>

            