

<script type="text/javascript">
<!--
var exitByUser = false;

function collapseElement(parentElement, elementName) {
    document.forms["<%= dynamicUI.getFormName()%>"].<%= ActionConstants.RC_COLLAPSE_GROUP %>.value=elementName;
    document.forms["<%= dynamicUI.getFormName()%>"].<%= ActionConstants.RC_PARENT_GROUP %>.value=parentElement;
    submitFwDuForm();
}

function expandElement(parentElement, elementName) {
   document.forms["<%= dynamicUI.getFormName()%>"].<%= ActionConstants.RC_EXPAND_GROUP %>.value=elementName;
   document.forms["<%= dynamicUI.getFormName()%>"].<%= ActionConstants.RC_PARENT_GROUP %>.value=parentElement;
   setCurrentElementInFocus(elementName);
   submitFwDuForm();
}

function submitFwDuForm() {
	 exitByUser = true;
     document.forms["<%= dynamicUI.getFormName()%>"].submit();
     return true;
}


function setCurrentElementInFocus(elementName) {
    document.forms["<%= dynamicUI.getFormName()%>"].<%= ActionConstants.RC_FOCUS_CURRENT_ELEMENT %>.value=elementName;
    return true;
} 


function navigateTo(navigateToPage) {
    document.forms["<%= dynamicUI.getFormName()%>"].<%= ActionConstants.RC_NAVIGATE_TO_PAGE %>.value=navigateToPage;
    return submitFwDuForm();
}    


<%-- set focus on anchor point and further on config link if respective targets are availabe --%>
function setElementFocus() {
        <% String anker1 = "elementonfocus_" + dynamicUI.getObject().getCurrentElementOnFocus();  %>
        if (document.getElementById('<%= anker1 %>') != null) {
            <%-- document.getElementById('<%= anker1 %>').focus(); --%>
            location.href="#<%= anker1 %>";
        }
        <% String anker = "grouponfocus_" + dynamicUI.getObject().getCurrentElementOnFocus();  %>
        if (document.getElementById('<%= anker %>') != null) {
            <%-- document.getElementById('<%= anker %>').focus(); --%>
            location.href="#<%= anker %>";
        }        
}

function navigateExpandSubComponent(parentElement, elementName) {
    expandElement(parentElement, elementName);
}

function navigateToSubComponent(elementName) {
    setCurrentElementInFocus(elementName);
    var ankerGr = "grouponfocus_" + elementName; 
    if (document.getElementById('ankerGr') != null) {
        document.getElementById('ankerGr').focus();
    }    
}

//-->
</script>