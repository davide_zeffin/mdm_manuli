<%--
********************************************************************************
    File:         runtimeexception.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      19.4.2001
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/06/28 $
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>


<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.MessageDisplayer" %>
<%@ page import="com.sap.isa.ui.uiclass.MessageUI" %>

<% MessageUI ui = new MessageUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

  <title><isa:translate key="error.jsp.title"/></title>
  <isa:stylesheets/>

  <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComLogin.js") %>"
          type="text/javascript">
  </script>

  <script type="text/javascript">
    function loginOutOfIsaTop() {
      loginInTopFrame("isaTop","<isa:reentryURL name="<%= ui.getInitAction() %>"/>");
      return true;
    }
  </script>

</head>

<body class="message-page">

  <div id="message-content">
    <div class="module-name"><isa:moduleName name="appbase/runtimeexception.jsp" /></div>

    <% ui.includeSimpleHeader(); %>

    <div class="fw-msg-area">
        <div class="error">
            <span><isa:translate key="msg.error.runtimeexception"/></span>
        </div>
    </div>    

    <div class="fw-msg-area">
        <div class="info">
            <span><isa:translate key="msg.runtimeexception.friendly" /></span>
        </div>
    </div>    

    <%
    if (ui.isPortal()) {  %>
      <div class="info">
        <span><isa:translate key="msg.error.loginPortal" /></span>
      </div>
        <%
        } %>
  </div>

  <%
  if (!ui.isPortal()) {  %>
    <div id="buttons">
      <ul class="buttons-1">
          <li><a href="javascript:loginOutOfIsaTop();" <% if (ui.isAccessible) { %> title="<isa:translate key="msg.error.login"/>" <% } %>
          ><isa:translate key="msg.error.login" /></a></li>
      </ul>
    </div>
  <%
  } %>

  <div style="clear: both;">
    <pre>
      <% MessageDisplayer.printStackTrace(ui.getException(), out, request); %>
    </pre>
  </div>  

</body>

</html>
<%-- Include logic to invalidate the session. --%>
<%@ include file="/user/logon/session_invalidate.inc" %>
