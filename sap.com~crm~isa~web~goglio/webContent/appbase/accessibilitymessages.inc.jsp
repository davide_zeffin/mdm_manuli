<%-- Insert this container at the beginning of your document page. --%>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.isa.core.util.Message" %>

<%

//fill MessageList with Messages for Accessibility
ui.generateAccMessages();
MessageList messagelist = ui.getAccessibilityMessages();


if(messagelist != null && messagelist.size() > 0){ %>
  <div class="messages">

    <div class="messages-header">
      <h1>
        <a href="#m2" accesskey="<isa:translate key="access.messages.key" />">
                <isa:translate key="access.messages.start" arg0="<%=Integer.toString(messagelist.size())%>"/>
        </a>
      </h1>
    </div>

    <div class="messages-list">
      <a name="tabletop" href="#tableend">
        <isa:translate key="access.messages.start" arg0="<%=Integer.toString(messagelist.size())%>"/></a>
      <ul>
        <%
        for(int i=0; i < messagelist.size(); i++){
          Message message = messagelist.get(i);
          String location = "access.message.location." + message.getPageLocation();
          String type = "access.message.type." + Integer.toString(message.getType());
        %>
        <li>
          <%
          if(message.getPosition().equals("")) { %>
            <a href="#"><isa:translate key="<%=location%>" /> <%= JspUtil.encodeHtml(message.getPosition())%> - <isa:translate key="<%=type%>" />: <%= JspUtil.encodeHtml(message.getDescription())%></a>
          <%
          }
          else { %>
            <a href="#item_<%=JspUtil.encodeHtml(message.getPosition())%>"><isa:translate key="<%=location%>" /> <%= JspUtil.encodeHtml(message.getPosition())%> - <isa:translate key="<%=type%>" />: <%= JspUtil.encodeHtml(message.getDescription())%>
          <%
          } %>
        </li>
      <%
      }%>
      </ul>
      <a name="tableend" href="#tabletop">
              <isa:translate key="access.messages.end" /></a>
    </div>

</div> <%-- end messages --%>
<% } //if(uimessagelist.size() > 0) %>
