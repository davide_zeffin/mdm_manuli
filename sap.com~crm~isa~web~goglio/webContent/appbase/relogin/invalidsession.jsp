<%@ taglib uri="/isa" prefix="isa" %>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.StartupParameter" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.InvalidSessionUI" %>

<%@ page session="false" %>

<% InvalidSessionUI ui = new InvalidSessionUI(pageContext); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title><isa:translate key="error.jsp.title"/></title>

  <isa:stylesheets/>

  <script type="text/javascript">
   // <!--
    function getTopWindow() {
      var tmpWnd = window;
      while (tmpWnd.parent != tmpWnd && tmpWnd.name != "isaTop") {
           tmpWnd = tmpWnd.parent;
      }
      tmpWnd=tmpWnd.parent;
      return tmpWnd;
    }

    function loginOutOfIsaTop() {
      var tmpWnd = getTopWindow();
      tmpWnd.location.href="<isa:reloginURL/>";
      return true;
    }

    function reloginOutOfIsaTop() {
      var tmpWnd = getTopWindow();
      tmpWnd.location.href="<isa:webappsURL name="<%= ui.getInitAction() %>">
        <%
        for (int i = 0; i < ui.getParameterList().size(); i++) {
          StartupParameter.Parameter parameter = (StartupParameter.Parameter)ui.getParameterList().get(i); %>
          <isa:param  name="<%=parameter.getName()%>"  value="<%=parameter.getValue()%>" />
        <%
        }
        %>
      </isa:webappsURL>";

      return true;
    }
    // -->
  </script>

</head>

<body class="message-page">

  <%-- ui.includeSimpleHeader(); --%>

  <div id="message-content">
    <div class="module-name"><isa:moduleName name="appbase/relogin/invalidsession.jsp" /></div>

    <div class="error"><span><isa:translate key="msg.error.invalidsession"/></span></div>

    <% if (ui.isCookieFound && ui.isPortal) { %>
        <div class="info"><span><isa:translate key="msg.error.loginPortal" /></span></div>
      <%
      }%>
  </div>

  <%
  if (ui.isCookieFound) {
    if (!ui.isPortal) { %>
    <div id="buttons">
      <ul class="buttons-1">
        <li><a href="javascript:reloginOutOfIsaTop();" <% if (ui.isAccessible) { %> title="<isa:translate key="msg.error.login"/>" <% } %>
        ><isa:translate key="msg.error.login" /></a></li>
      </ul>
    </div>
    <%
    }
  }
  else { %>
  <div id="buttons">
    <ul class="buttons-1">
      <li><a href="javascript:loginOutOfIsaTop();" <% if (ui.isAccessible) { %> title="<isa:translate key="msg.error.login"/>" <% } %>
      ><isa:translate key="msg.error.login" /></a></li>
    </ul>
  </div>
  <%
  }%>

</body>
</html>