<%-- import the taglibs used on this page --%>
<%@ taglib prefix="isa" uri="/isa" %>
<%@ page import="com.sapmarkets.isa.core.util.WebUtil" %>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>isa:translate key="b2c.staticText.title"/></title>
    <isa:includes/>
    <!--
    <link rel="stylesheet" type="text/css" href="<isa:webappsURL name="mimes/shared/style/stylesheet.css" />">
    -->
    
  </head>
  <body class="body-mid">
    <div class="column">
      <div id="text" class="module">
        <div class="module-name"><isa:moduleName name="b2c/empty.jsp"/></div>
      </div>
    </div>
  </body>
</html>