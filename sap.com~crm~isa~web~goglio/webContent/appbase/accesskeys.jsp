<%@ page language="java" %>
<%@ page import="com.sap.isa.ui.uiclass.AccessKeyUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.GenericFactory" %>
<%@ page import="com.sap.isa.core.util.GenericFactory" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ taglib uri="/isa" prefix="isa" %>

<%
	AccessKeyUI ui = (AccessKeyUI)GenericFactory.getInstance("accessKeyUI");
	if (ui == null)
		ui = new AccessKeyUI();
	ui.initContext(pageContext);
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<title><isa:translate key="accesskey.pagetitle"/></title>
	<isa:stylesheets/>
  </head>
  <body class="workarea" >
	<div id="accesskeys" class="module">
		<div class="module-name"><isa:moduleName name="appbase/accesskeys" /></div>
		<br /><isa:translate key="access.accesskey.note"/><br /><br />
<% if (ui.isAccessible()) { %>
<a href="#accesskey-end"
	id="accesskey-start" 
	title= "<isa:translate key="accesskey.pagetitle"/>" 
	accesskey="<isa:translate key="accesskey.pagetitle.access"/>"
>
</a>
<% } %>
<%
	request.setAttribute("accessKeyApps", ui.getAccessKeysForApplication());
	int tableIndex = 0;
%>
   <isa:iterate id="accesskeyapp"
                name="accessKeyApps"
                type="com.sap.isa.ui.accessibility.AccessKeysForApplication">
<%
	tableIndex++;
	int colno = 2;
	// Please change this based on the number of keys that you have
	int rowNo = 0;
	String columnNumber = Integer.toString(colno);
	String rowNumber = Integer.toString(rowNo);
	String allRows = Integer.toString(rowNo);
	String tableTitle = WebUtil.translate(pageContext, "accesskey.table", null);
	
	String tableBeginId = "accesskeytable" + Integer.toString(tableIndex) + "-begin";
	String tableEndId = "accesskeytable" + Integer.toString(tableIndex) + "-end";
	String tableBeginHref = "#" + tableBeginId;
	String tableEndHref = "#" + tableEndId;
%>
<% if (ui.isAccessible()) { %>
		<a href="<%=JspUtil.encodeHtml(tableEndHref)%>" id="<%=JspUtil.encodeHtml(tableBeginId)%>"
			title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="1" arg4="<%=allRows%>"/>">
		</a>
<% } %>
<table border="0" cellspacing="0" cellpadding="3" width="100%">
	<tr>
		<td>
			<h3><isa:translate key="<%=accesskeyapp.getAppname()%>"/></h3>
		</td>
	</tr>
</table>

<div class="filter-result">

 <table id="accesskeytable<%= tableIndex %>" title="<isa:translate key="<%=accesskeyapp.getAppname()%>"/>" summary="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="1" arg4="<%=allRows%>"/>" class="list" border="0" cellspacing="0" cellpadding="3" width="100%">
	<thead>
	  	<tr> 
		<th id="col_0<%= tableIndex %>" title="<isa:translate key="accesskey.table.key"/>" width="20%"><span><isa:translate key="accesskey.table.key"/></span></th>
		<th id="col_1<%= tableIndex %>" title="<isa:translate key="accesskey.table.key.represent"/>" width="80%"><span><isa:translate key="accesskey.table.key.represent"/></span></th>
		</tr> 
	</thead>
	<tbody>
	   
		<%	request.setAttribute("accessKeyList", accesskeyapp.getAccesskeys());%>
	   <% int rowIndex = 0; %>
	   <isa:iterate id="accesskeydef"
                name="accessKeyList"
                type="com.sap.isa.ui.accessibility.AccessKeyDefinition">		
		<tr class="<%= (rowIndex%2==0) ? "even" : "odd" %>">
			<td headers="col_0<%= tableIndex %>" class="odd" width="20%">
				<span><isa:translate key="<%=accesskeydef.getAccesskey()%>"/></span>
			</td>
			<td headers="col_1<%= tableIndex %>" class="odd" width="20%">
				<span><isa:translate key="<%=accesskeydef.getAccesskeyrepresents()%>"/></span>
			</td>
	 	</tr>
	 	<% rowIndex++; %>
		</isa:iterate>
	</tbody>
 </table>

</div>
<% if (ui.isAccessible()) { %>
		<a id="<%= JspUtil.encodeHtml(tableEndId) %>" href="<%= JspUtil.encodeHtml(tableBeginHref) %>" title="<isa:translate key="access.table.end" arg0="accesskeytable"/>"></a>
<% } %>
<br />
	</isa:iterate>

<% if (ui.isAccessible()) { %>
	<a href="#accesskey-start"
		id="accesskey-end"
	></a>        
<% } %>
	</div>
  </body>
</html>