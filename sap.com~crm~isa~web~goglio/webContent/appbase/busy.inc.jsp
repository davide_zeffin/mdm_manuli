<%--
******************************************************************************** 
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      07.03.2005

    Display the waiting screen

    !! This JSP is part of the appbase branch, and with that is has to have
       references only to object available in that branch. !!
       
    $Revision: #1 $
    $Date: 2005/03/07 $
********************************************************************************
--%>

<%-- proload busy graphik --%>
<div>
	<img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/busy.gif") %>" alt="" style="display: none;" />
</div>

<div id="busy" style="display:none;">
    <p><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/busy.gif") %>" alt="<isa:translate key="busy.message"/>"/></p>
    <p><isa:translate key="busy.message"/></p>
</div>
