<%-- This page generates UILayouts, which are definied in the layout-config.xml file --%>

<%-- import the taglibs used on this page --%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="isa" uri="/isa" %>
<%@ taglib prefix="appbase" uri="/appbase" %>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="java.util.Iterator" %>
<%@ page import="com.sap.isa.core.ui.layout.UIArea" %>
<%@ page import="com.sap.isa.core.ui.layout.HtmlAttribute" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.LayoutUI" %>
<%@ page import="com.sap.isa.isacore.action.EComDispatcherAction" %>


<% 
request.setCharacterEncoding("UTF-8");
LayoutUI ui = new LayoutUI(pageContext); %>

<isa:contentType />

<%
if (ui.isXHTMLSupported()) { %>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <%
  }
else {%>
  <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <html>
  <%
  }%>

  <head>
    <%-- expires is necessary to inhibit caching on a proxy server --%>
    <%-- otherwise we might get problems, e.g. when jumping back   --%>
    <%-- from the config UI to the details of a package            --%>
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    
    <% ui.includeStylesheets(); %>
    
    <title><%=ui.getTitle()%></title>
    <% ui.includeHeaderJavaScript(); %>
    <%
    if (ui.isGlobalHTMLFormDispatcherAvailable()) { %>
      <script type="text/javascript">
        function startGlobalDispatcherValue (dispatcherValue) {
          document.forms["<%=ui.getHTMLForm().getId()%>"].<%= EComDispatcherAction.PARAMETER_NAME%>.value =
            dispatcherValue;
          document.forms["<%=ui.getHTMLForm().getId()%>"].submit();
        }
      </script>
    <%
    } %>
    <!-- External header data -->   
    <% ui.includeExternalHeaderData(); %>
  </head>

  <body class="<% if(ui.isPortal) { %>fw-genlay-inportal <% } %><%=ui.getBodyClass()%>" 
        onload="genericLayoutPageOnLoad();"
        <%
        for (Iterator bodyAttributeIt = ui.getBodyAttributes(); bodyAttributeIt.hasNext(); ) {
          HtmlAttribute bodyAttribute = (HtmlAttribute)bodyAttributeIt.next();
          String attributeEntry = bodyAttribute.getName() + "=\"" + bodyAttribute.getValue() + "\"";%>
          <%= attributeEntry %>
        <%
        }
        %>><%
        if (ui.isLayoutAccessibilityAvailable()) { %>
                <a class="" href="#" title= "<%=ui.getLayoutAccessibilityTitle()%>"
                   accesskey="<%=ui.getLayoutAccessibilityKey()%>"></a>
        <%
        }
        if (ui.isGlobalHTMLFormAvailable()) { %>
          <form id="<%=ui.getHTMLForm().getId()%>" name="<%=ui.getHTMLForm().getName()%>"
            action="<isa:webappsURL name="<%=ui.getHTMLForm().getAction()%>" />"
            method="<%=ui.getHTMLForm().getMethod()%>"
            <%
            for (Iterator attributeIt = ui.getFormAttributes(); attributeIt.hasNext(); ) {
              HtmlAttribute attribute = (HtmlAttribute)attributeIt.next();
              String attributeEntry = attribute.getName() + "=\"" + attribute.getValue() + "\"";%>
              <%= attributeEntry %>
            <%
            }
            %>>
            <div>
              <%
              if (ui.isGlobalHTMLFormDispatcherAvailable()) { %>
                <input type="hidden" name="<%= EComDispatcherAction.PARAMETER_NAME%>" value=""/>

              <%
              } %>
            <appbase:requestSerial mode="POST"/>
            <div>
        <%
        }
    Iterator iter = ui.getAreaIterator();
    while (iter.hasNext()) {
      UIArea area = (UIArea)iter.next();

      if (ui.isAreaAccessibilityAvailable(area)) { %>
        <a class="" href="#" title= "<%=ui.getAreaAccessibilityTitle(area)%>"
           accesskey="<%=ui.getAreaAccessibilityKey(area)%>"></a>
      <%
      }%>

          <%
          Iterator containerIter = ui.getContainerStartsWithArea(area.getName());
            while (containerIter.hasNext()) { %>
              <div id="<%= (String)containerIter.next() %>">
      <%
      }    
      %>      
      <div id="<%= area.getCssIdName() %>">
        <% ui.includeAreaInfo(area.getName()); %>
        <div class="fw-box">
          <div class="fw-box-top">
            <div></div>
          </div>
          <div class="fw-box-i1">
            <div class="fw-box-i2">
              <div class="fw-box-i3">
                <div class="fw-box-content">
                  <div class="inner">
                  <% ui.includeArea(area.getName()); %>
                  </div> <%-- inner --%>
                </div> <%-- fw-box-content --%>
              </div> <%-- fw-box-i3 --%>
            </div> <%-- fw-box-i2 --%>
          </div> <%-- fw-box-i1 --%>
          <div class="fw-box-bottom">
            <div></div>
          </div> <%-- fw-box-bottom --%>
        </div> <%-- fw-box --%>
      </div>  
          <%
          containerIter = ui.getContainerEndsWithArea(area.getName());
            while (containerIter.hasNext()) { %>
              </div> <!-- end of container: <%= (String)containerIter.next() %> -->
      <%
      }
    } /* end-while */
        if (ui.isGlobalHTMLFormAvailable()) { %>
        </form>
        <%
        }
        if (ui.isLayoutBottomAccessibilityAvailable()) { %>
                <a class="" href="#" title= "<%=ui.getLayoutBottomAccessibilityTitle()%>"
                   accesskey="<%=ui.getLayoutBottomAccessibilityKey()%>"></a>
        <%
        }%>
   </body>
</html>
