<%--
********************************************************************************
    File:         fieldwithhelpvalues.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      15.01.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/01/15 $

    Needed variables:
    - A instance of the FieldWithValuesSearch with name field must be avaiable.
    
    The value of the field can be pre selected by field.setDefValue(String defValue)

********************************************************************************
--%>
<div class="fieldWithHelpValue">
  <%
  if (false) { %>
    <div class="identifier">
  <%
  } %>
    <label for="<%=field.getFieldName()%>">
      <%
      if (field.getLabel().equals("")) { %>
<%-- GREENORANGE        &nbsp;&nbsp; --%>
      <%
      } 
      else { %>
        <isa:translate key="<%=field.getLabel()%>"/>:
      <%
      } %>
    </label>
  <%
  if (false) { %>
    </div>
    <div class="value">
  <%
  } %>
    <%
  if (field.displayDropDownList(pageContext)) { %>
    <%-- Drop down list box --%>
    <select id= "<%= field.getFieldId()%>" name = "<%= field.getFieldName() %>" >
      <% field.activateAdditionalValues(pageContext); %>
      <isa:iterate id="additionalValue" name="<%= FieldSupport.PC_ADDVALUES %>"
                   type= "java.lang.String[]" ignoreNull="true">
        <option 
          <% if (field.isAddValueSelected(additionalValue[1])) { %>
                selected="selected" 
          <% } %>     
          value="<%=additionalValue[0]%>">
          <%=JspUtil.encodeHtml(additionalValue[1])%>
        </option>
      </isa:iterate>
      <% field.performSearch(pageContext); %>
      <isa:iterate id="helpValues" name="<%= FieldSupport.PC_HELPVALUES %>"
                   type= "com.sap.isa.core.util.table.ResultData" ignoreNull="true">
              <option 
                <% if (field.isHelpValueSelected(field.getValue())) { %>
                     selected="selected" 
                <% } %>     
                value="<%= JspUtil.encodeHtml(field.getValue()) %>">
                <%= JspUtil.encodeHtml(field.getDescription()) %>
              </option>
      </isa:iterate>
    </select>
  <%
  }
  else {%>
  <%-- Help value search --%>
    <script language="JavaScript">
      <%=HelpValuesSearchUI.getJSPopupCoding(field.getSearchName(),
                                           pageContext,
                                           field.getParameterMapping(),
                                           field.getNameSuffix()) %>
    </script>

    <input type="text" id= "<%= field.getFieldId()%>" name ="<%= field.getFieldName() %>"
           value ="<%= HelpValuesSearchUI.getFieldValueFromHelpResult(field.getFieldName(), 
                                                                      field.getDefValue(),
                                                                      pageContext)%>">
           <%= field.getSearchIcon(pageContext) %>
  <%
  }%>

  <%
  if (false) { %>
  </div>
  <%
  } %>
</div>

