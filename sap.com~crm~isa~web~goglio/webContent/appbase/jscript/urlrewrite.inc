<%-- 
    File:         urlrwrite.inc
    Copyright (c) 2003, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      March 2003
    Version:      1.0
    
    This file must be inculded in a jsp via the inculde tag
    
      ...
         @ include file="/appbase/jscript/urlrewrite.inc" 
      ...
    
    otherwise it won't work correctly. 
    
    Also it must be included before all other Javascript files that are 
    imported via script src="
    
     ...
        script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/some_name.js" ) %>"
                   type="text/javascript"
     ...
     
    if the function rewriteUrl is called inside those javascript file.

    $Revision: #2 $
    $Date: 2003/04/09 $
--%>

<%@ page import="com.sap.isa.core.util.*" %>

<%@ taglib uri="/isa" prefix="isa" %>

<%-- 

 * global parameters, to store infos for rewrite
   
--%>
 
 var urlRewAppPath = "";   // stores application path
 var urlRewSessionId = ""; // store session id

<%-- 

 * determine path and urlRewSessionId by calling webappsURL

--%>
function determineRewriteParameters() {

    var rewrite = "<isa:webappsURL name=""/>"; 
    if (rewrite.length > 0) {
        var colpos = rewrite.indexOf(";");
        if (colpos > -1) {
            urlRewAppPath = rewrite.substring(0, colpos);
            urlRewSessionId = rewrite.substring(colpos, rewrite.length);
        }
        else {
            urlRewAppPath = rewrite;
        }
     }   

    return true;
}

<%-- 

 * returns the given URL as url-rewritten String

--%>
function rewriteURL(srcUrl) {
	
    var rewUrl;
    var paramStart = srcUrl.indexOf("?");
    
    // are there already any request parameters, so we have to insert the urlRewSessionId before them
    if (paramStart == -1) {
        rewUrl = srcUrl + urlRewSessionId;
    } 
    else {
        rewUrl = srcUrl.substring(0, paramStart) + urlRewSessionId + srcUrl.substring(paramStart, srcUrl.length);   
    }
    
    if (urlRewAppPath.length > 0) {
    	if (rewUrl.charAt(0) == "/") {
            rewUrl = urlRewAppPath.substring(0, urlRewAppPath.length -1) + rewUrl;
        }
        else {
            rewUrl = urlRewAppPath + rewUrl;
        }
    }

    return rewUrl;

}

<%-- 

 * call the function, to determine parameters initially

--%>
determineRewriteParameters();