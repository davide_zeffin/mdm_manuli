<%@ taglib prefix="isa" uri="/isa" %>
<script type="text/javascript">
function openAcccessKeysWindow()
{
	var accessKeysWindow = window.open('<isa:webappsURL name="/appbase/accesskeys.jsp"/>',
									 'accessKeysWindow',
									 'width=500,height=400,screenX=200,screenY=300,titlebar=yes,resizable=yes,scrollbars=yes');
	accessKeysWindow.focus();
	return accessKeysWindow;
}
</script>
