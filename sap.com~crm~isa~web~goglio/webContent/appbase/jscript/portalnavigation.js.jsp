/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       portalnavigation                   */
/* Description:    call jScript function for Portal   */
/*                 navigation                         */
/* Version:        1.0                                */
/* Author:         SAP AG                             */
/* Creation-Date:  3.08.2004                          */
/* Last-Update:    3.08.2004                          */
/*                                                    */
/*                           (c) SAP AG, Walldorf     */
/*----------------------------------------------------*/

<%-- Use this script for domain relaxing (remove computer name from domain xxx.wdf.sap.corp -> wdf.sap.corp) --%>
function setDomain(){
    liBehindFirstDot = location.hostname.indexOf( "." ) + 1;
    if (liBehindFirstDot > 0) {
        document.domain = location.hostname.substr( liBehindFirstDot );
    }
}

// function parameter: 'urlFinal' String containing the URL which should be called
function doNavigate(urlFinal) {
    setDomain();
    return EPCMPROXY_EX.doNavigate(urlFinal);
}
