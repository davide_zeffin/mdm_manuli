<%--
/*-----------------------------------------------------------------------------------------------*/
/* Javascript for the "SAP E-commerce Project"                                                          */
/*                                                                                               */
/* Document:       ipc_pricinganalysis.jsp                                                       */
/* Description:    This document contains a function which is used to call the IPC               */ 
/*                 Pricing Analysis Tool.                                                        */
/* Version:        1.0                                                                           */
/* Author:         SAP AG                                                                     */
/* Creation-Date:  16.07.2003                                                                    */
/*                                                                                               */
/*                                                                               (c) SAP AG      */
/*-----------------------------------------------------------------------------------------------*/
--%>

<%@ taglib uri="/isa" prefix="isa" %>

function displayIpcPricingConds( connectionkey, docid, itmid ) {
    var urlforward = "<isa:webappsURL name='/price/preparepriceanalysisshow.do'/>?conKey=" + connectionkey + "&IPCdocID=" + docid + "&IPCitemID=" + itmid + "&mode=Display";
    var ipccondswinTest = window.open(urlforward, 'ipccondswin','width=650,height=420,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
    ipccondswinTest.focus();
    return true;
}
