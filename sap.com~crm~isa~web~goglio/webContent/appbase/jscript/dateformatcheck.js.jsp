/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       dateFormatCheck                    */
/* Description:    call jScript function to check the */
/*                 format of a given date             */
/* Version:        1.0                                */
/* Author:         SAP AG                             */
/* Creation-Date:  31.08.2004                         */
/* Last-Update:    31.08.2004                         */
/*                                                    */
/*                           (c) SAP AG, Walldorf     */
/*----------------------------------------------------*/
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="/isa" prefix="isa" %>

<%-- START Date format checking --%>
// For debugging purposses. After an error occured the code points to the coding location which noticed the problem.
var dfc_code = "000";
var dfc_msgTmpl = "";
var dfc_noPopUp = false;
// Use this function to check your date.
// @param object should be the html object where the value attribute should contain a date (e.g. 20/12/1967)
// @param template what is the format which should be checked (e.g. dd/mm/yyyy or yyyy.dd.mm).
// @param date format message template which will by used if an error message must be issued
// @param noPopUp set to false to avoid a popup about an invald input
/*public*/function dateFormatCheck(object, template, msgTmpl, noPopUp) {
  dfc_code = "000";
  dfc_msgTmpl = msgTmpl;
  dfc_noPopUp = (noPopUp ? noPopUp : false);
  if (!object) alert("System error: Object is not set");
  object.value = trim(object.value);
  if (object.value == '') return true;         // Accept empty field
  var splitChar = dfc_getSplitCharacter(template);
  if (dfc_code != '000') return false;
  template = template.toUpperCase();
  if (object.value == template) return true;  // Accept Date Template as field value
  if (object.value == msgTmpl.toUpperCase()) return true;  // This is needed for non-english languages
  if (object.value.indexOf(splitChar) < 0) dfc_confirmError(object, "100");
  if (dfc_code != '000') return false;
  var day = dfc_checkDay(object, template, splitChar);
  if (dfc_code != '000') return false;
  var month = dfc_checkMonth(object, template, splitChar);
  if (dfc_code != '000') return false;
  var year = dfc_checkYear(object, template, splitChar);
  if (dfc_code != '000') return false;
  dfc_checkDateRanges(day, month, year, object);
  if (dfc_code != '000') return false;
  object.value = dfc_getFormattedDate(day, month, year, splitChar, template);
  return true;
}
/*public*/function trim(str) {
  if (!str) return "";
  var retStr = str;
  while(retStr.substring(0, 1) == ' ') {
    retStr = retStr.substring(1, retStr.length);
  }
  while(retStr.substring(retStr.length - 1, retStr.length) == ' ') {
    retStr = retStr.substring(0, retStr.length - 1)
  }
  return retStr;
}
/*private*/function dfc_checkDateRanges(day, month, year, object) {
  if (month == '02' && day > 29) dfc_confirmError(object, "801");
  if (dfc_code != '000') return false;
  if (   (month == '04' || month == '06' || month == '09' || month == '11')
      &&  day > 30) dfc_confirmError(object, "802");
  if (dfc_code != '000') return false;
  if (day > 31) dfc_confirmError(object, "803");
  if (dfc_code != '000') return false;
  if (month > 12) dfc_confirmError(object, "804");
  if (dfc_code != '000') return false;
}
/*private*/function dfc_getFormattedDate(day, month, year, splitChar, template) {
  var aTmp = template.split(splitChar);
  var retDate = "";
  for (var i = 0; i < aTmp.length; i++) {
    if (aTmp[i] == 'DD') {
      if (i == 0) retDate = (day + splitChar);
      if (i == 1) retDate = (retDate + day + splitChar);
      if (i == 2) retDate = (retDate + day);
    }
    if (aTmp[i] == 'MM') {
      if (i == 0) retDate = (month + splitChar);
      if (i == 1) retDate = (retDate + month + splitChar);
    }
    if (aTmp[i] == 'YYYY') {
      if (i == 0) retDate = (year + splitChar);
      if (i == 2) retDate = (retDate + year);
    }
  }
  return retDate;
}
/*private*/function dfc_checkDay(object, template, splitChar) {
  var aTmp = template.split(splitChar);
  var posDay = -1;
  for (var i = 0; i < aTmp.length; i++) {
    if (aTmp[i] == 'DD') {
      posDay = i;
      break;
    }
  }
  var aOVal = object.value.split(splitChar);
  var day = aOVal[i];
  if (day.length > 2) dfc_confirmError(object, "200");
  if (dfc_code != '000') return false;
  if (day.length == 0) dfc_confirmError(object, "201");
  if (dfc_code != '000') return false;
  if (day.length == 1) day = "0" + day;
  dfc_checkDigitsOnly(object, day);
  if (dfc_code != '000') return false;
  return day;
}
/*private*/function dfc_checkYear(object, template, splitChar) {
  var aTmp = template.split(splitChar);
  var posYear = -1;
  for (var i = 0; i < aTmp.length; i++) {
    if (aTmp[i] == 'YYYY') {
      posYear = i;
      break;
    }
  }
  var aOVal = object.value.split(splitChar);
  var year = aOVal[i];
  if (year.length > 4) dfc_confirmError(object, "400");
  if (dfc_code != '000') return false;
  if (year.length == 3) dfc_confirmError(object, "402");
  if (dfc_code != '000') return false;
  if (year.length < 2) dfc_confirmError(object, "401");
  if (dfc_code != '000') return false;
  dfc_checkDigitsOnly(object, year);
  if (dfc_code != '000') return false;
  if (year.length == 2) {
    if (parseInt(year) > 50) {
      year = "19" + year;
    } else {
      year = "20" + year;
    }
  }
  return year;
}
/*private*/function dfc_checkMonth(object, template, splitChar) {
  var aTmp = template.split(splitChar);
  var posMonth = -1;
  for (var i = 0; i < aTmp.length; i++) {
    if (aTmp[i] == 'MM') {
      posMonth = i;
      break;
    }
  }
  var aOVal = object.value.split(splitChar);
  var month = aOVal[i];
  if (month.length > 2) dfc_confirmError(object, "250");
  if (dfc_code != '000') return false;
  if (month.length == 0) dfc_confirmError(object, "251");
  if (dfc_code != '000') return false;
  if (month.length == 1) month = "0" + month;
  dfc_checkDigitsOnly(object, month);
  if (dfc_code != '000') return false;
  return month;
}
/*private*/function dfc_checkDigitsOnly(object, inStr) {
  for (i = 0; i < inStr.length; i++) {
    ch = inStr.substring(i, i+1);
    if (ch < "0" || ch > "9") dfc_confirmError(object, "300");
    if (dfc_code != '000') break;        
  }
}
/*private*/function dfc_getSplitCharacter(template) {
  if (template.indexOf(".") > -1) return ".";
  if (template.indexOf("-") > -1) return "-";
  if (template.indexOf("/") > -1) return "/";
  alert("System error: Wrong date template '" + template + "'");
  code = "950";
}
/*private*/function dfc_confirmError(object, code) {
  var back;
  if (dfc_noPopUp == false) {
    if (parseInt(code) <= 800) {
      alert("'" + dfc_msgTmpl + "' <isa:UCtranslate key="dfc.date.invalid.format"/>");
    } else {
      alert("'" + object.value + "' <isa:UCtranslate key="dfc.date.invalid"/>");
    }
  }
  dfc_code = code;
  if (dfc_noPopUp == false) {
    object.focus();
  }
}
/* END Date format checking */