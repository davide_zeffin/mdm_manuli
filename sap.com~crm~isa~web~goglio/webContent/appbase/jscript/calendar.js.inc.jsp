<%@page import="com.sap.isa.core.util.WebUtil" %>
<%--
/*-----------------------------------------------------------------------------------------------*/
/* Javascript for the "SAP APPBASE Project"                                                      */
/*                                                                                               */
/* Document:       /appbase/jscript/calendar.js.inc.jsp                                          */
/* Description:    This document contains a function which is used to call the Calendar Control  */ 
/* Version:        1.0                                                                           */
/* Author:         i802842                                                                       */
/* Creation-Date:  20031115                                                                      */
/*                                                                                               */
/*                                                                               (c) SAP AG      */
/*-----------------------------------------------------------------------------------------------*/
--%>
<%@ taglib uri="/isa" prefix="isa" %>

    <script type="text/javascript" 
	    src="<%=WebUtil.getMimeURL(pageContext, null, "/mimes/jscript/EComBase.js")%>"
    >
    </script>    
    <script type="text/javascript">
    <!--
    // call with the basic parameters dateFormat, dateFieldId
    function openCalendarWindow( dateFormat, dateFieldId ) 
    {
		dateField = getElement(dateFieldId);
		return openCalendarWindow3( dateFormat, dateField.value, dateField );
    }
    // call with the basic parameters dateFormat, dateFieldId
    // and execute domain relaxation in the pop-up
    function openCalendarWindowDomainRelaxed( dateFormat, dateFieldId ) 
    {
        dateField = getElement(dateFieldId); 
        dateValue = dateField.value;
        var calReady = false;
        var calendarWindow = window.open('<isa:webappsURL name="/appbase/calendar.jsp"/>'+'?dateFormat='+dateFormat+'&dateValue='+dateValue+'&dateFieldId='+dateFieldId+'&relaxDomain=T',
                                         'calendarWindow','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes');

	    var startTime = new Date();
	    var timeDiff = 0;
	    var stopIt = false;		
	    
        while(!calReady && !stopIt){ 
            try {
                calendarWindow.dateField=dateField;
                calReady = true;
            }
            catch(e){
            }
	        // If accessing calendar pop-up is not successful within 10 seconds we stop in order to avoid an endless loop.
	        var currentTime = new Date();
	        timeDiff = currentTime - startTime;
			if (timeDiff > 100000){
				stopIt = true;
			}
        }
        try {
        	calendarWindow.focus();
        }
        catch(e){
        }        
        return calendarWindow;
    }
    
    // call with the basic parameters dateFormat, dateFieldId 
    // and start the calendar month table on the weekDay
    function openCalendarWindowOnWeekDay( dateFormat, dateFieldId, weekDay ) 
    {
		dateField = getElement(dateFieldId); 
		return openCalendarWindow4( dateFormat, dateField.value, dateField, weekDay ); 
    }
    // call with all the parameters dateFormat, dateValue, dateField, weekDay
    function openCalendarWindow4( dateFormat, dateValue, dateField, weekDay ) 
    {
		var calendarWindow = window.open('<isa:webappsURL name="/appbase/calendar.jsp"/>'+'?dateFormat='+dateFormat+'&dateValue='+dateValue+'&weekDay='+weekDay,
				      					 'calendarWindow','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes');
		calendarWindow.dateField=dateField;
		calendarWindow.focus();
		return calendarWindow;
    }
    // call with the most common 3 parameters dateFormat, dateValue, dateField
    function openCalendarWindow3( dateFormat, dateValue, dateField ) 
    {
		var calendarWindow = window.open('<isa:webappsURL name="/appbase/calendar.jsp"/>'+'?dateFormat='+dateFormat+'&dateValue='+dateValue,
				      					 'calendarWindow','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes');
		calendarWindow.dateField=dateField;
		calendarWindow.focus();
		return calendarWindow;
    }

    // call with the parameters dateFormat, dateField. The dateValue is taken from the dateField
    function openCalendarWindow2( dateFormat, dateField ) 
    {
		return openCalendarWindow3( dateFormat, dateField.value, dateField ) 
    }

    // call with the dateField, The dateFormat is the default SAP date, The dateValue is taken from the dateField
    function openCalendarWindow1( dateField ) 
    {
		var calendarWindow = window.open('<isa:webappsURL name="/appbase/calendar.jsp"/>'+'?dateValue='+dateField.value,
				      					 'calendarWindow','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes');
		calendarWindow.dateField=dateField;
		calendarWindow.focus();
		return calendarWindow;
    }
    
    // display the calendar control embedded not as pop-up (needs modification of the calling JSP)
    function showCal(dateFormat, dateFieldId, calDivId, calFrameName){
        dateField = getElement(dateFieldId); 
        var calendarFrame = getElement(calFrameName);
        // display the calendar div-area
        toggleCal(calDivId);
        // set the inputfield object at the calendarFrame
        var calReady = false;
        var startTime = new Date();
        var timeDiff = 0;
        var stopIt = false;     
        
        while(!calReady && !stopIt){ 
            try {
                calendarFrame.contentWindow.dateField = dateField;
                calReady = true;
            }
            catch(e){
            }
            // If accessing calendar frame is not successful within 10 seconds we stop in order to avoid an endless loop.
            var currentTime = new Date();
            timeDiff = currentTime - startTime;
            if (timeDiff > 100000){
                stopIt = true;
            }
        }
        
    }

    // display or hide the div-area of the embedded calendar control
    function toggleCal(divId){
        getElement(divId).style.display=(getElement(divId).style.display=='block') ? 'none' : 'block';
    }
    
    //-->
    </script>