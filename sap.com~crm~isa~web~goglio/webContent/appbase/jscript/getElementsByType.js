/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       getElementsByType.js               */
/* Description:    getElementsByTagName replacement   */
/*                 for Netscape 4.x                   */
/* Version:        1.0                                */
/* Author:         ralf.witt@sap.com                  */
/* Creation-Date:  10.07.2002                         */
/* Last-Update:    10.07.2002                         */
/*                                                    */
/*                           (c) SAP AG, Walldorf     */
/*----------------------------------------------------*/

// function parameter: 'sType' can be: checkbox, hidden, password ...
//                     'sForm' is the form name in which will be searched
function getElementsByType(sForm, sType) {
    var objList = new Array(0);                        
    var oIdx = 0;                                      
                                                       
    var ende = false                                   
    var idx = 0;                                       
                                                       
    var vForm = document.forms[sForm];      // get Form object                        
    while (! ende  &&  vForm != null) {                
        var obj = vForm.elements[idx];                 
        if (obj == null) {                             
            ende = true;                               
        } else {                                       
            if (obj.type == sType) {                   
                objList[oIdx] = obj;                   
                oIdx++;                                
            }                                          
        }                                              
        idx++;                                         
    }
    // Return collected objects (inputs with type 'sType')
    return objList;
}                                          