// ----- OBJECT  Entry (Option)

function Entry() {
    function addFlwElm(scnElm) {
         this.i_flw++;
         this.flwElms[this.i_flw] = scnElm;
    }
    function setAllVisible() {
         for (var i = 0; i <= this.i_flw; i++) {
             this.flwElms[i].isVisible = true;
             // Check Sub-Dependencies -> Recursive Call !!
             this.flwElms[i].check();            
         }
    }
                   
    // -- Define Methods
    this.addFlwElm = addFlwElm;
    this.setAllVisible = setAllVisible;
                        
    // -- Define Variables
    this.i_flw = -1;
    this.flwElms = new Array();
}

// ----- OBJECT  ScreenElement
function ScnElm(div_id, select_id, pty_name ,fixElement, isRange) {
    function check() {
        // get Selected Index
        var obj = document.getElementById(this.s_id);
        var idx = 0;
        if (obj == null) return;   // Not all Objects have an ID, so not all are accessable
        // Handle select boxes
        if (obj.type == "select-one")  {
             idx = obj.selectedIndex;
        }
        // Handle radio button. First radio button has the usual id. All following ones have suffixes +1, +2, ...
        if (obj.type == "radio") {
             while(obj && !obj.checked) {
             idx++;
             obj = document.getElementById((this.s_id + "+" + idx)); 
             }
        }
                               
        // get assigned entry to find dependend Screen Elements
        var a_entry = this.entries[idx];
        if (! a_entry) return;    // Check object exists
                                
        a_entry.setAllVisible();
    }
                        
    function addEntry(indexpos, entry) {
        this.entries[indexpos] = entry;
    }
                        
    // -- Define Methods
    this.check = check;
    this.addEntry = addEntry;
                                
    // -- Define Variables
    (fixElement != null) ? this.isFixElement = fixElement : this.isFixElement = false;
    (fixElement != null  &&  fixElement == true) ? this.isVisible = true : this.isVisible = false;
    (isRange != null && isRange == true) ? this.isRange = true : this.isRange = false;
    this.d_id = div_id;
    this.s_id = select_id;
    this.pty_name = pty_name;
    this.entries = new Array();
}

                       
// END OBJECT DEFINITION 


// ----- GENERAL FUNCTIONS

function evaluateX() {
    setAllInvisible();
    // Loop over all fix Screen Elements, so at least all fix Screen Elements and their dependencies are checked !
    for(var i = 0; i < scnElms.length; i ++) {
         var checkelm = scnElms[i];
         if (! checkelm) break;  // .length function delivers entire size even not all elements are filled, so check element isn't zero reference !

         if (checkelm.isFixElement) {
              checkelm.check();   // Call checkfunction on the element itself
         }
    }

    // Set visibly marked Screen Elements to display: block
    for(var i = 0; i < scnElms.length; i ++) {
         var scnElmx = scnElms[i];
         if (! scnElmx) break;

             if (scnElmx.isVisible == true) {
                 if (document.getElementById(scnElmx.d_id)) {
                      if (navigator.userAgent.indexOf("Gecko") != -1) {                                                                                                      	                                       	
                            document.getElementById(scnElmx.d_id).style.display="table-row";
                      } else {
                            document.getElementById(scnElmx.d_id).style.display="block";
                      }        
                 }
             } else {
                 if (document.getElementById(scnElmx.d_id)) {
                      document.getElementById(scnElmx.d_id).style.display="none";
                 }
             }
     }
} //function evaluateX()

function setAllInvisible() {
    for(var i = 0; i < scnElms.length; i++) {
         if (!scnElms[i]) break;
         if ( ! scnElms[i].isFixElement) {
             scnElms[i].isVisible = false;
         }
    }
}


//Use this function to determine a HTML Element by name of the property
//@param String  name of the property
//@return Object null or object with that name
function getElementByPtyName(pty_name) {
     var obj = null;
     for(var i = 0; i < scnElms.length; i ++) {
         if (! scnElms[i]) break;
         if (scnElms[i].pty_name == pty_name) {
             obj = scnElms[i];
         }
     }
     if (obj != null) {
         return document.getElementById(obj.s_id);
     }
}

// Find a target frame. Recursive function, set parameter 'inWin' to null for initial call --%>
function findTargetLocation(inWin, target) {
	 var tmpWnd = (inWin != null ? inWin : window);
     var retVal = null;
     // Find top window
     while (inWin == null && tmpWnd.parent != tmpWnd && retVal == null) {
         tmpWnd = tmpWnd.parent;
     }
     for (var i = 0; i < tmpWnd.frames.length; i++) {
		try {
			if (tmpWnd.frames[i].name == target) {
				retVal = tmpWnd.frames[i];
				break;
			}
			// Search in sub windows for the right frame
			if (retVal == null) {
				retVal = findTargetLocation(tmpWnd.frames[i], target);
			}
		} catch(e){
			// this usually means the app is not standalone and the app tried
			// to access a frame it does not belong to. Check the sub frames.
			retVal = findTargetLocation(tmpWnd.frames[i], target);
		}
     }
     if (inWin == null && retVal == null) {
         // If target frame couldn't be found, return current window
         retVal = window;
     }
     return retVal;
}

//In accessibility mode set focus on the search result message after the GO button had been pressed
function accSetFocusonMsg() {
     var msgObj = document.getElementById("GSACCRESULTMSG");
     if (msgObj != null) {
         msgObj.focus();
     }
}

function enableSelectOptions(target) {
     var frame = findTargetLocation(null, target);
     if (frame == null) return true;// Frame could not be found
     with (frame) {
         for(var i = 0; i < scnElms.length; i ++) {
             var scnElmx = scnElms[i];
             if (! scnElmx) break;

             if (scnElmx.isVisible == true) {
                  // Add attribute and its value to the request, but only if attirubte has a NAME
                  if (scnElmx.isRange == false) {
                      var objL = document.getElementById(scnElmx.s_id);
                      if ( objL ) {
                           objL.disabled = false;
                      }
                  } else {
                      // For ranges add _LOW and _HIGH to the HTTP request parameter name
                      var idLow = scnElmx.s_id  + "_low";
                      var idHigh = scnElmx.s_id + "_high";
                      var objL = document.getElementById(idLow);
                      var objH = document.getElementById(idHigh);
                      if ( objL.name != 'null') {
                          objL.disabled = false;
                      }
                      if ( objH.name != 'null') {
                          objH.disabled = false;
                      }
                  }
              }
         } // end for
         // Finally enable Start button
         var butStart = document.getElementById('gsbuttonstart');
         if (butStart) {
             butStart.className="button";
         }
     } // end with
}
