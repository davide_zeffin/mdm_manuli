<%--
********************************************************************************
    File:         menu.jsp.inc
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      November 2002
    Version:      1.0

    $Revision: #12 $
    $Date: 2002/03/01 $
********************************************************************************
* This include handle the display of the menu.
* The the MaintenanceObjectUI ui objects
* must be defined outside the include to use this include.
* The JavaScript function SelectTabStrip(key) must also be defined outside.
* It describes the active screen group.
* The include uses the java object TabStribHelper to diplay the menu entries.
* See class documentation for further hints.
********************************************************************************
--%>

<%@ page import="com.sap.isa.isacore.TabStripHelper" %>
<%@ page import="com.sap.isa.maintenanceobject.businessobject.*" %>
<%@ page import="java.util.*" %>

<%@ page import="com.sap.isa.core.SessionConst" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>


<%-- the page use the TabStripHelper class to show the the menu --%>
<%-- see java doc for more infos --%>

<%-- All menu entries are defined within this section --%>
<%
  TabStripHelper tabStrip = mo_ui.getTabStrip();
%>

  <%@ include file="/appbase/tabstrip.inc.jsp" %>
  <%-- closing open brackets --%>
        </ul>
  </div> <%-- navigator --%>