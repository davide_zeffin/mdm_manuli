<%-- This include contains all common javascript functionality to maintain or display a 
     maintenance object.
     This include could only be used as an static include. 
     The java variable ui must be defined and extended the MaintenanceObjectUI class. 
     Also the java script function: openWin must be defined.
     This assumes that the isa translation tag is used by the page
--%>


  <script type="text/javascript">

	var exitByUser = false;

	function myGetHelpValues(name, formName, index) {
	    exitByUser = true;
	    return getHelpValues(name, formName, index);
	}

    function submitForm() {
	  exitByUser = true;
      document.forms["<%= mo_ui.getFormName() %>"].submit();
      return true;
    }

    function changeObject() {
      document.forms["<%= mo_ui.getFormName() %>"].ChangeShop.value='true';
      submitForm();
    }

    function helpValues(propertyName) {
      document.forms["<%= mo_ui.getFormName() %>"].HelpValues.value=propertyName;
      submitForm();
      document.forms["<%= mo_ui.getFormName() %>"].HelpValues.value="";
    }

    function SelectTabStrip(key) {
      document.forms["<%= mo_ui.getFormName() %>"].activeScreenGroup.value=key;
      submitForm();
    }

    function exitPage() {
	  <% 
	  if (mo_ui.getReadOnly().equals("false")) {  %>
		 if (exitByUser == false) {
	  		openWindow('closewindow');
  			}		
  	  <%
  	  } %>
    }
    
    function showHelp(propertyName) {
      var url = '<isa:webappsURL name="<%= mo_ui.getHelpAction() %>"/>' + '?property=' + propertyName;
	  var sat = window.open(url, 'Help', 'width=550,height=370,menubar=no,scrollbars=yes,locationbar=no,resizable=yes');
	  sat.focus();
    }
    
    function setCurrentElementInFocus(elementName) {
        <%--
        document.forms["<%= dynamicUI.getFormName()%>"].<%= ActionConstants.RC_FOCUS_CURRENT_ELEMENT %>.value=elementName;
        --%>
        return true;
    }    	
  </script>
