<%-- ThIn include contains all common javascript functionality to maintain or display a
     maintenance object.
     This include could only be used as an static include.
     The java variable ui must be defined and extended the MaintenanceObjectUI class.
     Also the java script function: openWin must be defined.
--%>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.TabStripHelper" %>
<%@ page import="com.sap.isa.ui.uiclass.MaintenanceObjectUI" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI"%>

<%
String disabled;
String tabTxt = WebUtil.translate(pageContext, "appbase.jsp.propertypages", null);
if (mo_ui.isAccessible()){
    TabStripHelper mtabStrip = mo_ui.getTabStrip();
    int tabItems = mtabStrip.size();
    String strTabItems = Integer.toString(tabItems);
%>
<div>
    <a href="#tabmaintainance-end"
       id="tabmaintainance-begin"
       title="<isa:translate key="access.tab.begin" arg0="<%=tabTxt%>" arg1="<%=strTabItems%>"/>"
    ></a>
</div>
<%
}
%>
<div id="objectDataInput">
<%-- included table for displaying navigator tab strips --%>
<%
int mo_line = 0;
if (mo_ui.showMenu()) {
%>
<%@ include file="/appbase/maintenanceobject/maintenanceobject.menu.inc.jsp" %>
<%
}
int groupindex = 0;
%>
    <div id="formfields">
<isa:iterate id="propertyGroup" name="<%=MaintenanceObjectUI.RA_PROPERTY_GROUPS%>"
             type="com.sap.isa.maintenanceobject.businessobject.PropertyGroup">
             
  <div class="objectDataGroup">                  
    <div class="fw-box">
    <div class="fw-box-top"><div></div></div>
    <div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3">     
    <div class="fw-box-content">    
        
    <%
    groupindex++;
    if (!propertyGroup.isHidden()) {
        String grpTxt = WebUtil.translate(pageContext, propertyGroup.getDescription(), null);
        String groupEndId = "group" + Integer.toString(groupindex) + "-end";
        String groupBeginId = "group" + Integer.toString(groupindex) + "-begin";
        String groupEndRef = "#" + groupEndId;
        String groupBeginRef = "#" + groupBeginId;
        if (mo_ui.isAccessible()) {
    %>
            <div>
                <a href="<%=groupEndRef%>"
                   id="<%=groupBeginId%>"
                   title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"
                ></a>
            </div>
        <%
        }
        %>
        
            
		<%  
		if (!propertyGroup.getDescription().equals("") ){
		%>
		<h4>
			 <isa:translate key="<%=propertyGroup.getDescription()%>"/> 
		</h4>
		<%
		}
		%>	
            
            
            <table>
        <%
        /* Set the current propertyGroup to pageContext for the iterator tag */
        request.setAttribute("propertyGroup", propertyGroup);
        %>
        <isa:iterate id="property" name="propertyGroup"
                     type="com.sap.isa.maintenanceobject.businessobject.Property">
            <%
            disabled = (mo_ui.getReadOnly().equals("true") || property.isDisabled())  ?"disabled=\"disabled\"":"";
            boolean helpDisplayed = false;
            boolean closeTR = true;

            if (!property.isHidden()) {
                String required = property.isRequired()?" *":"";
                String labelCssClass = property.isRequired() ? "labelObl" : "label";
            %>
                <isa:message id="errortext" name="<%=MaintenanceObjectUI.RA_MESSAGES%>" type="<%=Message.ERROR%>" property="<%=property.getName()%>">
                <tr>
                    <td colspan="2">
                        <div class="error"><span><%=errortext%></span></div>
                    </td>
                </tr>
                </isa:message>
                <isa:message id="infotext" name="<%=MaintenanceObjectUI.RA_MESSAGES%>" type="<%=Message.INFO%>" property="<%=property.getName()%>">
                <tr>
                    <td colspan="2">
                        <div class="info"><span><%=infotext%></span></div>
                    </td>
                </tr>
                </isa:message>
                <%
                mo_line++;
				if (property.isReadOnly() == true ) {
				%>
					<tr>
						<td class="<%=labelCssClass%>">
							<%= mo_ui.getPropertyDescription(property)%>
						</td>
						<td class="mo-output">
							<%= JspUtil.encodeHtml(property.getString()) %>   
				<%
				}
                else if (property.getType().equals(Property.TYPE_STRING)
                    || property.getType().equals(Property.TYPE_INTEGER)) {

                    /*  *** CASE 1:  String or int value *** */
                    int numberAllowedValue = property.countAllowedValues();

                    /* Set the current property to pageContext for the iterator tag */
                    /* request.setAttribute("property", property); */

                    String exitField = property.isExitField()?"onBlur=\"submitForm();\"":"";

                    if ( numberAllowedValue == 0) {
                %>
                <tr>
                    <td class="<%=labelCssClass%>">
                        <label id="<%="label_" + property.getName()%>" for="<%=property.getName()%>">
                             <%= mo_ui.getPropertyDescription(property)%> <%=required %>
                        </label>
                    </td>
                    <td class="input">
                        <%
                        if (mo_ui.isAccessible()) {
                            String fieldTxt = mo_ui.getPropertyDescription(property);
                            String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
                            if (disabled.length() == 0) {
                                unavailableTxt = "";
                            }
                        %>
                        <span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
                        <%
                        }

                        String helpAvailTxt = WebUtil.translate(pageContext, "access.help.available", new String[]{"1"});
                        if (!(!helpDisplayed && property.isHelpAvailable())) {
                            helpAvailTxt = "";
                        }
                        String helpValuesTxt = "";
                        if (mo_ui.getReadOnly().equals("false") &&
                            property.getHelpValuesMethod()!= null &&
                            property.getHelpValuesMethod().length() >0) {

                            String tabOrder = "2";
                            if (!(!helpDisplayed && property.isHelpAvailable())) {
                                tabOrder = "1";
                            }
                            helpValuesTxt = WebUtil.translate(pageContext, "access.help.values", new String[]{tabOrder});
                        }
                        String helpSearchTxt = "";
                        if (mo_ui.getReadOnly().equals("false") &&
                            property.getHelpValuesMethod()!= null &&
                            property.getHelpValuesMethod().length() >0) {

                            String strTabOrder = "3";
                            int tabOrder = 3;
                            if (!(mo_ui.getReadOnly().equals("false") &&
                                  property.getHelpValuesMethod()!= null &&
                                  property.getHelpValuesMethod().length() >0)) {

                                tabOrder--;
                            }
                            if (!(!helpDisplayed && property.isHelpAvailable())) {
                                tabOrder--;
                            }
                            strTabOrder = Integer.toString(tabOrder);
                            helpSearchTxt = WebUtil.translate(pageContext, "access.help.search", new String[]{strTabOrder});
                        }
                        %>
                        <input class="textinput"
                               type="text"
                               id="<%= property.getName() %>"
                               name="<%= property.getName() %>"
                               size="<%= property.getSize() %>"
                               maxlength="<%= property.getMaxLength() %>"
                               value="<%= JspUtil.encodeHtml(property.getString()) %>"
                               title="<%=helpAvailTxt + helpValuesTxt + helpSearchTxt%>"
                               <%=exitField%> <%=disabled%>
                        />
                        <%
                        if (mo_ui.isAccessible()) {
                        %>
                        </span>
                        <%
                        }
                    } // end: if ( numberAllowedValue == 0)
                    else if ( numberAllowedValue < 4) {
                    %>
                <tr>
                        <%
                        exitField = property.isExitField()?"onclick=\"submitForm();\"":"";
                        %>
                    <td class="<%=labelCssClass%>" colspan="2">
                        <%
                        String helpAvailTxt = WebUtil.translate(pageContext, "access.help.available", new String[]{"1"});
                        if (!(!helpDisplayed && property.isHelpAvailable())) {
                            helpAvailTxt = "";
                        }
                        %>
                        <%= mo_ui.getPropertyDescription(property)%>
                        <a href="#" title="<isa:translate key="access.radio.grp.begin" arg0="<%=property.getDescription()%>"/>"></a>
                        <%
                        if (!helpDisplayed && property.isHelpAvailable()) { %>
                        <a href="javascript:showHelp('<%= property.getName() %>')"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/help.gif") %>" alt="<isa:translate key="mob.jsp.help"/>" /></a>
                        <%
                            helpDisplayed = true;
                        }
                        %>
                    </td>
                </tr>
                        <%
                        int indexOfValue = 0;
                        %>
                        <isa:iterate id="value" name="property"
                                     type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">
                            <%
                            if (!value.isHidden()) {
                                indexOfValue++;
                            %>
                <tr>
                    <td class="inputradio_singleline" colspan="2">
                                <%
                                String checked = value.getValue().equals(property.getString())?"checked=\"checked\"":"";
                                %>
                        <input type="radio"
                               id="<%= property.getName() + "_" + Integer.toString(indexOfValue) %>"
                               name="<%= property.getName() %>"
                               value="<%= value.getValue() %>"
                               <%=checked %> <%=disabled%> <%=exitField%> />
                        <label id="<%= "label_" + property.getName() + "_" + Integer.toString(indexOfValue) %>" for="<%= property.getName() + "_" + Integer.toString(indexOfValue) %>"><isa:translate key="<%=value.getDescription()%>"/><%=required %></label>
                    </td>
                </tr>
                            <%
                            }
                            %>
                        </isa:iterate>
                    <%
                        closeTR = false;
                    } // end: else if ( numberAllowedValue < 4)
                    else {
                    %>
                <tr>
                    <td class="<%=labelCssClass%>">
                        <label id="<%="label_" + property.getName()%>" for="<%=property.getName()%>">
                        <%= mo_ui.getPropertyDescription(property)%><%=required %>
                        </label>
                    </td>
                    <td class="input">
                      <%
                      if (disabled.length() > 0) {
                          int indexOfValue = 0;
                      %>
                          <isa:iterate id="value" name="property"
                                       type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">
                              <%
                              indexOfValue++;

                              if (value.getValue().equals(property.getString())) {
                                  if (mo_ui.isAccessible()) {
                                      String fieldTxt = WebUtil.translate(pageContext, property.getName(), null);
                                      String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
                                      if (disabled.length() == 0) {
                                          unavailableTxt = "";
                                      }
                              %>
                      <span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
                                  <%
                                  }
                                  %>
                      <input class="textinput"
                             type="text"
                             id="<%= property.getName() %>"
                             name="<%= property.getName() %>"
                             value="<isa:translate key="<%= value.getDescription() %>"/>" disabled="disabled" />
                        <%
                        if (mo_ui.isAccessible()) { %>
                          </span>
                        <%
                        }
                      }
                      %>
                          </isa:iterate>
                      <%
                      }
                      else {
                        if (mo_ui.isAccessible()) {
                          String fieldTxt = WebUtil.translate(pageContext, property.getName(), null);
                          String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
                          if (disabled.length() == 0) {
                              unavailableTxt = "";
                          }%>
                          <span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
                        <%
                        }
                        String helpAvailTxt = WebUtil.translate(pageContext, "access.help.available", new String[]{"1"});
                        if (!(!helpDisplayed && property.isHelpAvailable())) {
                            helpAvailTxt = "";
                        }
                        %>
                        <select id="<%= property.getName() %>" name="<%= property.getName() %>"
                              title="<%=helpAvailTxt%>">
                          <isa:iterate id="value" name="property"
                                       type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">
                              <%
                              String selected = value.getValue().equals(property.getString())?"selected=\"selected\"":"";
                              %>
                          <option value="<%= value.getValue() %>"
                                   <%=selected %> <%=disabled%> >
                                      <isa:translate key="<%=value.getDescription()%>"/></option>
                          </isa:iterate>
                        </select>
                        <%
                        if (mo_ui.isAccessible()) {%>
                          </span>
                        <%
                        }
                      }
                      if (!helpDisplayed && property.isHelpAvailable()) { %>
                      <a href="javascript:showHelp('<%= property.getName() %>')"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/help.gif") %>" alt="<isa:translate key="mob.jsp.help"/>" /></a>
                          <%
                          helpDisplayed = true;
                      }
                    } // end: else (if ( numberAllowedValue == 0))
                } // end: if (property.getType().equals(Property.TYPE_STRING)
                else if (property.getType().equals(Property.TYPE_BOOLEAN) ) {

                    String checked = property.getBoolean()?"checked=\"checked\"":"";
                    String exitField = property.isExitField()?"onclick=\"submitForm();\"":"";
                %>
                <tr>
                    <td class="<%=labelCssClass%>" colspan="2">
                    <%
                    if (mo_ui.isAccessible()) {

                        String fieldTxt = WebUtil.translate(pageContext, property.getName(), null);
                        String unavailableTxt = WebUtil.translate(pageContext, "access.unavailable", null);
                        if (disabled.length() == 0) {
                            unavailableTxt = "";
                        }
                    %>
                        <span title="<isa:translate key="access.input" arg0="<%=fieldTxt%>" arg1="<%=unavailableTxt%>"/>">
                    <%
                    }
                    %>
                        <input type="checkbox"
                               id="<%= property.getName() %>"
                               name="<%= property.getName() %>"
                               value="true"
                               <%=checked %>
                               <%=exitField%> <%=disabled%> />
                        <label id="<%="label_" + property.getName()%>" for="<%= property.getName() %>"><%= mo_ui.getPropertyDescription(property)%><%=required %></label>
                    <%
                    if (mo_ui.isAccessible()) {
                    %>
                        </span>
                <%
                    }
                 
				  
                } 
              

                if (!helpDisplayed && property.isHelpAvailable()) {
                %>
                        <a href="javascript:showHelp('<%= property.getName() %>')"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/help.gif") %>" alt="<isa:translate key="mob.jsp.help"/>" /></a>
                <%
                }
                %>
                <%-- Help values method --%>
                <%
                if (mo_ui.getReadOnly().equals("false") && property.getHelpValuesMethod()!= null && property.getHelpValuesMethod().length() >0) {
                %>
                        <a href="javascript:helpValues('<%= property.getName() %>')"><img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/helpValues.gif") %>" alt="<isa:translate key="mob.jsp.helpValues"/>" /></a>
                <%
                }
                /* Help values search */
                if (mo_ui.getReadOnly().equals("false") && property.getHelpValuesSearch()!= null && property.getHelpValuesSearch().length() >0) { 
                    %>
                    <script type="text/javascript">
                      <%= HelpValuesSearchUI.getJSPopupCoding(property.getHelpValuesSearch(),pageContext) %>
                    </script>
                      <%= HelpValuesSearchUI.getJSCallPopupCoding(property.getHelpValuesSearch(), mo_ui.getFormName(), "", pageContext) %>
                <%
                }
                %>

                <%-- Description for the value --%>
                <%
                if (mo_ui.isAccessible()) {
                    String propDesc = WebUtil.translate(pageContext, property.getDescription(), null);
                    String propValue = "";
                    if (property.getValue() != null) {
                        propValue = property.getValue().toString();
                    }
                %>
                        <span title="<isa:translate key="mob.value.description" arg0="<%=propDesc%>" arg1="<%=propValue%>"/>">
                <%
                }
                                %>
                <%=property.getValueDescription()%>
                <%
                if (mo_ui.isAccessible()) {
                %>
                        </span>
                <%
                }
                if(closeTR) {
                %>
                    </td>
                </tr>
            <%
                }
            } // end: if (!property.isHidden())
            %>
        </isa:iterate>

            </table>
        <%
        if (mo_ui.isAccessible()) {
        %>
            <div>
                <a href="<%=groupBeginRef%>"
                   id="<%=groupEndId%>"
                   title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"
                ></a>
            </div>
        <%
        }
    } // end: if (!propertyGroup.isHidden()) {
    %>
    
     </div> <%-- fw-box-content --%> 
     </div></div></div> <!-- fw-box-i1 -->
     <div class="fw-box-bottom"><div></div></div>
     </div> <!-- fw-box -->  
   </div> <%-- end: class="objectDataGroup" --%>      
        
</isa:iterate>

    </div> <%-- end: id="formfields" --%>
<%
if (mo_ui.isAccessible()) {
%>
    <div>
        <a href="#tabmaintainance-begin"
               id="tabmaintainance-end"
               title="<isa:translate key="access.tab.end" arg0="<%=tabTxt%>"/>"
        ></a>
    </div>
<%
}
%>
</div> <%-- end: id="objectDataInput" --%>
