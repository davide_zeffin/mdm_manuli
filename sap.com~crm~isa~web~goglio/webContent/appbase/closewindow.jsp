<%-- import the taglibs used on this page --%>
<%@ taglib prefix="isa" uri="/isa" %>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Close Window</title>
    <isa:includes/>
        <script type="text/javascript">
        <!--
          function closeWindow() {
            window.close();
          }
        //-->
        </script>

  </head>
  <body onload="closeWindow()">
    <div class="column">
      <div id="text" class="module">
        <div class="module-name"><isa:moduleName name="appbase/closewindow.jsp"/></div>
      </div>
    </div>
  </body>
</html>