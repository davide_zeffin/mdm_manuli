<%--
********************************************************************************
    File:         checksession.inc
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      24.4.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/03/12 $
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>

<%-- Check for an invalid session and forward to error page if session is invalid --%>
<% if (session.isNew()) { %>
    <% session.invalidate(); %>
    <jsp:forward page="/appbase/relogin/invalidsession.jsp"/>
<% } %>
<%-- End of Check --%>