<%-- 
This JSP peforms an HTML redirect. This could be necessery if an redirect 
with SSL switch will be needed. In this case a "standrd" redirect isn't possible
because it exists an IE bug where the port number has the wrong value.

This JSP should be used together with com.sap.isa.core.action.StartSecureAction
--%>

<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.action.StartSecureAction" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); 
    // the encoding should works because we're running XHTML
    String redirectPath = JspUtil.encodeHtml((String) request.getAttribute(StartSecureAction.REDIRECT_PATH)); 
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta http-equiv="refresh" content="0; URL=<%=redirectPath%>">
        <script language="JavaScript">
        <!--
        function doRedirect() {
            location.href="<%=redirectPath%>";
        }
        //-->
        </script>
    </head>
    <body onload="setTimeout ('doRedirect()',3000)">

    </body>
</html>