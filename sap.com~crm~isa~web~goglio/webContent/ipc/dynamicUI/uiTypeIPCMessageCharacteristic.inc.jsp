<%-- This include contains all common functionality to render a table row containing a
     message characteristic.
     This include could only be used as an dynamic include.
--%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="java.lang.Object" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>
<%@ page import="com.sap.isa.maintenanceobject.backend.boi.PropertyData" %>
<%@ page import="com.sap.isa.maintenanceobject.ui.UITypeRendererData" %>
<%@ page import="com.sap.isa.maintenanceobject.businessobject.Property" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI"%>

<% 
    DynamicUIObjectUI dynamicUI = (DynamicUIObjectUI)DynamicUIObjectUI.getFromRequest(request); 
    
    Property property = (Property) dynamicUI.currentIteratorElement;
    Object currentUIObject = property;
    request.setAttribute("property", property);
    String labelCssClass = property.isRequired() ? "labelObl" : "label";
    String exitFieldFunction = property.getExitFieldFunction();
    String exitField = property.isExitField()?("onblur=\"" + exitFieldFunction + ";\""):"";
    String required = property.isRequired()?" *":"";
    String disabled = (dynamicUI.getReadOnly().equals("true") || property.isDisabled() || property.isReadOnly())  ?"disabled=\"disabled\"":"";
    String focusField = "onfocus=\"setCurrentElementInFocus('" + property.getRequestParameterName() + "')\"";
    boolean helpDisplayed = false;
    boolean firstLine = true;
    Map invisibleParts = dynamicUI.getInvisibleParts();
    
    int colspan = 2;
    int colspanmessage = 2;
%>
	</table>
	<table>
		<%@ include file="/appbase/dynamicUI/parts/partCalculateColspan.inc.jsp"%>
       	<%@ include file="/appbase/dynamicUI/parts/partMessage.inc.jsp"%>
       	<isa:iterate id="value" name="property"
                       type="com.sap.isa.maintenanceobject.businessobject.AllowedValue"> <%
			if (!value.isHidden() || dynamicUI.isEditable()) {
				String selected = null;
				if (property.getType().equals(PropertyData.TYPE_LIST)){
					selected = (property.getList().indexOf(value.getValue()) >= 0) ?"selected=\"selected\"":"";
				}
				else {
					selected = value.getValue().equals(property.getString())?"selected=\"selected\"":"";
				}
				if ((selected != null && selected.length() > 0)|| dynamicUI.isEditable()) { %>
					<tr><%
						if (dynamicUI.isEditable()) { 
							if (firstLine){%>
								<td class="fw-du-edit">
							    	<%@ include file="/appbase/dynamicUI/designer/parts/partDelete.inc.jsp"%>
									<%@ include file="/appbase/dynamicUI/designer/parts/partEditPart.inc.jsp"%>
									<%@ include file="/appbase/dynamicUI/designer/parts/partMoveDown.inc.jsp"%>
									<%@ include file="/appbase/dynamicUI/designer/parts/partMoveUp.inc.jsp"%>
									<%@ include file="/appbase/dynamicUI/designer/parts/partDesignerCheckbox.inc.jsp"%>
								</td><%
						        firstLine = false;
							}
							else { %>
								<td class="fw-du-edit">
						        </td><%                 
							}
						}%>
						<%@ include file="/ipc/dynamicUI/parts/IPCpartMessageCharacteristic.inc.jsp"%>
					</tr><%
				}
			}%>
		</isa:iterate>      
	</table>
    <table> 