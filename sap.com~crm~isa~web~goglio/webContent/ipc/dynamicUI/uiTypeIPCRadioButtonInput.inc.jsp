<%-- This include contains all common functionality to render table rows containing a
     group of input fields type of radio button.
     This include could only be used as an dynamic include.
--%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="java.lang.Object" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>
<%@ page import="com.sap.isa.maintenanceobject.ui.UITypeRendererData" %>
<%@ page import="com.sap.isa.maintenanceobject.businessobject.Property" %>
<%@ page import="com.sap.isa.ipc.ui.jsp.dynamicui.IPCProperty" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI"%>

<% 
    DynamicUIObjectUI dynamicUI = (DynamicUIObjectUI)DynamicUIObjectUI.getFromRequest(request); 

    Property property = (Property) dynamicUI.currentIteratorElement;
    Object currentUIObject = property;
	request.setAttribute("property", property);
	String labelCssClass = property.isRequired() ? "labelObl" : "label";
	String exitFieldFunction = property.getExitFieldFunction();
	String exitField = property.isExitField()?("onclick=\"" + exitFieldFunction + ";\""):"";
	String exitFieldInput = property.isExitField()?("onblur=\"" + exitFieldFunction + ";\""):"";
	boolean helpDisplayed = false;
	String disabled = (dynamicUI.getReadOnly().equals("true") || property.isDisabled() || property.isReadOnly())  ?"disabled=\"disabled\"":"";
	String required = property.isRequired()?" *":"";
	String focusField = "onclick=\"setCurrentElementInFocus('" + property.getRequestParameterName() + "')\"";
	Map invisibleParts = dynamicUI.getInvisibleParts();
	
	int colspan = 2;
	int colspanmessage = 2;
%>
       <%@ include file="/appbase/dynamicUI/parts/partCalculateColspan.inc.jsp"%>
       
               <%@ include file="/appbase/dynamicUI/parts/partMessage.inc.jsp"%>
       <tr>
       <% 
         if (dynamicUI.isEditable()) { 
       %>
           <td class="fw-du-edit">
               <%@ include file="/appbase/dynamicUI/designer/parts/partDelete.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/designer/parts/partEditPart.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/designer/parts/partMoveDown.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/designer/parts/partMoveUp.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/designer/parts/partDesignerCheckbox.inc.jsp"%>
           </td>
       <%
         }
       %>       
       <% String statusStyleClass = (dynamicUI.currentIteratorElement.getStatus() != null) ? dynamicUI.currentIteratorElement.getStatus().getStyle() : ""; %>                                            
           <td class="<%=labelCssClass%>  <%= statusStyleClass %>" >       
	           <%-- Display the status image for the given element --%>
	           <%@ include file="/appbase/dynamicUI/parts/partElementStatus.inc.jsp"%>           
               <%@ include file="/appbase/dynamicUI/parts/partPropDescr.inc.jsp"%>
               <%@ include file="/appbase/dynamicUI/parts/partHelpIcon.inc.jsp"%>
	       </td> 
	       <td>
           	&nbsp;
           </td>
	       
   		  <% 
		    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.THUMBNAIL)) { 
		  %>
		        <td>
		            <%@ include file="/appbase/dynamicUI/parts/partThumbnail.inc.jsp"%>
		            <%@ include file="/appbase/dynamicUI/parts/partO2cImage.inc.jsp"%>
		            <%@ include file="/appbase/dynamicUI/parts/partSound.inc.jsp"%>
		        </td>
		  <%
		    } 
		    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.ADD_DOC_LINKS)) {
		  %>
		        <td>
		            <%@ include file="/appbase/dynamicUI/parts/partAddDocLinks.inc.jsp"%>
		        </td>
		  <%
		    }
		    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.DISPLAY_HELP_LINK)) {
	  	  %>	
	  	       <td>	    
	  	           <%@ include file="/appbase/dynamicUI/parts/partHelpLink.inc.jsp"%>
	  	       </td>
	  	  <%
		    }		    
		    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.RESET_VALUE)) {
		  %>	
		        <td>    
		            <%@ include file="/appbase/dynamicUI/parts/partResetValue.inc.jsp"%>
		        </td>
		  <%
		    }
		    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.DISPLAY_ALL_OPTION)) {
			%>	
		  	      <td>	    
		  	          <%@ include file="/appbase/dynamicUI/parts/partDisplayAllOption.inc.jsp"%>
		  	      </td>
	  	  <%	    
		    }
		  %>       
	   </tr>  
	   <%
          int indexOfValue = 0;
       %>	     
	      <isa:iterate id="value" name="property"
			  type="com.sap.isa.maintenanceobject.businessobject.AllowedValue">
	     
       <%
	      if (!value.isHidden()) {
		      indexOfValue++;
		      currentUIObject = value;
       %>
       <tr>
       <% 
         if (dynamicUI.isEditable()) { 
       %>
           <td class="fw-du-edit">
                <%@ include file="/appbase/dynamicUI/designer/parts/partMoveDownAllowedValue.inc.jsp"%>
                <%@ include file="/appbase/dynamicUI/designer/parts/partMoveUpAllowedValue.inc.jsp"%>
                <%@ include file="/appbase/dynamicUI/designer/parts/partSpacer.inc.jsp"%>
           </td>
       <%
         }
       %>  
           <td class="<%=labelCssClass%> <%= statusStyleClass %>">
	           <%-- Display the status image for the given element --%>
               <%@ include file="/appbase/dynamicUI/parts/partLongText.inc.jsp"%>
           </td>

           <td class="inputradio_singleline" >
	               <%@ include file="/appbase/dynamicUI/parts/partRadioButton.inc.jsp"%>
	               <%@ include file="/appbase/dynamicUI/parts/partElementAnchor.inc.jsp"%>
	       </td>  
   		  <% 
		    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.THUMBNAIL)) { 
		  %>
		        <td>
		            <%@ include file="/appbase/dynamicUI/parts/partThumbnail.inc.jsp"%>
		            <%@ include file="/appbase/dynamicUI/parts/partO2cImage.inc.jsp"%>
		            <%@ include file="/appbase/dynamicUI/parts/partSound.inc.jsp"%>		            
		        </td>
		  <%
		    }
		    if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.ADD_DOC_LINKS)) {
		  %>
		        <td>
		            <%@ include file="/appbase/dynamicUI/parts/partAddDocLinks.inc.jsp"%>
		        </td>
		  <%
		    }
	      %>
       </tr>
	   <%
	      } //End of !value.isHidden()
       %>
	      </isa:iterate>
	   <tr> 
	     <% 
	     if (dynamicUI.isEditable()) { %>
		   <td></td>
		 <%	    
		 }%>
		   <td></td>	
	       <td class="input" colspan="<%= colspan %>">
  	           <%@ include file="/ipc/dynamicUI/parts/IPCpartEmptyInputField.inc.jsp"%>
  	           <%@ include file="/appbase/dynamicUI/parts/partInputPattern.inc.jsp"%>
  	           <%@ include file="/appbase/dynamicUI/parts/partElementAnchor.inc.jsp"%>
  	       </td>	      
	   </tr>
