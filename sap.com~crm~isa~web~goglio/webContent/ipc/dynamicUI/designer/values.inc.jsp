<%-- This include contains all common functionality to render the allowed values for a property
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %>
<%@ page import="com.sap.isa.ipc.ui.jsp.dynamicui.IPCAllowedValue" %>
<%@ page import="com.sap.isa.ui.uiclass.DynamicUIObjectUI" %>


<div class="fw-du-uitype_assignblock"> <!-- start prop group -->                 
<%-- Auf- und Zuklappen   --%>
  <div class="fw-box">
  <div class="fw-box-top">
  <div></div></div>
  <div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3">     
  <div class="fw-box-content">    
  <h1>Edit the sequence and properties for the values</h1>

<div class="fw-du-tablegroup"> 
	<table>
		<tr><th></th>	<!-- column for moveUp/moveDown buttons -->
			<th>Value</th>
			<th>Display Image</th>
			<th>Display Document</th>
			</tr>
      
		<% List allowedValues = (List) request.getAttribute(ActionConstants.RC_ALLOWED_VALUES);
		  
    	 if (allowedValues != null && allowedValues.size() > 0) {
			
 		   Iterator allowedValuesIter = allowedValues.iterator();
		   IPCAllowedValue ipcAllowedValue = null;
		   
		   while (allowedValuesIter.hasNext()) {
			   ipcAllowedValue = (IPCAllowedValue)allowedValuesIter.next();
			   if (ipcAllowedValue.getValue().length() == 0) {
				   continue;
			   }  %>
			   	   
			<tr>
			  <td>
			    <a href="<isa:webappsURL name="ipc/moveDownAllowedValue.do"/>?<%= ActionConstants.RC_CURRENT_ALLOWED_VALUE %>=<%= ipcAllowedValue.getValue() %> " >
                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/fw_du_movedown.gif") %>" width="14" height="16" alt="<isa:translate key="du.movedown"/>" />
          
			    <a href="<isa:webappsURL name="ipc/moveUpAllowedValue.do"/>?<%= ActionConstants.RC_CURRENT_ALLOWED_VALUE %>=<%= ipcAllowedValue.getValue() %> " >
	            <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/fw_du_moveup.gif") %>" width="14" height="16" alt="<isa:translate key="du.moveup"/>" />          
			  </td>
    		  
    		  <td>
				<!-- Allowed Value -->
				<%= ipcAllowedValue.getDescription() %>
			  </td>		  

    		  <td>
  				<!-- Display Image -->
  				<select id="ipc-dt[<%= ipcAllowedValue.getValue() %>]" name="ipc-dt[<%= ipcAllowedValue.getValue() %>]">
                            <option value="" <%=ipcAllowedValue.getDisplayImageType().equals("")?"selected=\"selected\"":"" %>>Thumbnail </option>
                            <option value="1"<%=ipcAllowedValue.getDisplayImageType().equals("1")?"selected=\"selected\"":"" %>>Hyperlink </option>
                            <option value="2"<%=ipcAllowedValue.getDisplayImageType().equals("2")?"selected=\"selected\"":"" %>>Do not display </option>
                            
                </select>
  				
			  </td>		  

    		  <td>
				<!-- Display Document -->    		  
				<input type="checkbox" id="ipc-dd[<%= ipcAllowedValue.getValue() %>]" name="ipc-dd[<%= ipcAllowedValue.getValue() %>]" value="true" <%=ipcAllowedValue.isDisplayDocumentLink()?"checked=\"checked\"":"" %>>
			  </td>		   
			</tr>  
			  
		<% }
		   } %>
	</table>
</div>


