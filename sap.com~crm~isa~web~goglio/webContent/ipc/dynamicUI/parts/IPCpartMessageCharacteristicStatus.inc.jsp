<%-- This include contains all common functionality to render the status of a message characteristic.
     This include could only be used as an static include.
     The java variable value must be defined and extended the AllowedValue class.
--%>

<% if (value.getStatus() != null) {
       if (value.getStatus().getImageURL() != null &&
               value.getStatus().getImageURL().length() > 0) { %>
        <% String statusText = "";
           if (value.getStatus().isRessourceKeyUsed()) { 
               statusText = WebUtil.translate(pageContext, value.getStatus().getDescription(), null);
           }
           else { 
               statusText = value.getStatus().getDescription();
           } %>               
               <img src="<%=WebUtil.getMimeURL(pageContext, value.getStatus().getImageURL()) %>" alt="<%= statusText %>" />
    <% } %>
<% } %>
