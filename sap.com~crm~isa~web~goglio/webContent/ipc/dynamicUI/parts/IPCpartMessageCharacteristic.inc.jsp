<%-- This include contains all common functionality to render a message characteristic.
     This include could only be used as an static include.
     The java variable dynamicUI must be defined and extended the DynamicUIObjectUI class.
--%>
	<td><%
		String statusStyleClass = (value.getStatus() != null) ? value.getStatus().getStyle() : ""; %>                                            
		<table class="<%=labelCssClass%> <%= statusStyleClass %>">
			<tr>
		    	<td class="ipcMessageCsticLabel">
					<%-- Display the status image for the given element --%>
					<%@ include file="/ipc/dynamicUI/parts/IPCpartMessageCharacteristicStatus.inc.jsp"%>           
					<%@ include file="/appbase/dynamicUI/parts/partLabel.inc.jsp"%><br/>	           
				</td>
		        <td class="ipcMessageCsticMessageText"><%
	            	if (value.isRessourceKeyUsed()) { %>
			        	<isa:translate key="<%= value.getDescription() %>"/>" <%
	              	}
	              	else { %>
	                	<%=value.getDescription()%><%
			      	}%>
 				  	<br/>
				</td><%
				if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.THUMBNAIL)) { %>
					<td>	  
						<%@ include file="/appbase/dynamicUI/parts/partThumbnail.inc.jsp"%>
					</td><%
				} 
				if (invisibleParts != null && !invisibleParts.containsKey(UITypeRendererData.DISPLAY_HELP_LINK)) {%>
					<td>	    
						<%@ include file="/appbase/dynamicUI/parts/partHelpLink.inc.jsp"%>
					</td><%
				} %>
			</tr>
		</table>
	</td>
        