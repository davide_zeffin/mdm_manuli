<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <link type="text/css" rel="stylesheet" href="/ipc/mimes/shared/style/stylesheet.css" />
<link type="text/css" rel="stylesheet" href="/ipc/ipc/mimes/style/stylesheet.css" />
<link type="text/css" rel="stylesheet" href="/ipc/mimes/stylesheet_ie6.css" />

    <title>Interactive Product Configuration</title>
    <script type="text/javascript">var currentInstanceIdInputField = 'cInstId';var instanceTreeStatusChangeInputField = 'instanceTreeStatusChange';var currentCharacteristicGroupNameInputField = 'cCharGroupName';var currentScrollCharacteristicGroupNameInputField = 'cScrollCharGroupName';var selectedCharacteristicGroupNameInputField = 'sCharGroupName';var currentCharacteristicNameInputField = 'cCharName';var characteristicStatusChangeInputField = 'characteristicStatusChange';var currentConflictIdInputField = 'cConflictId';var currentConflictParticipantIdInputField = 'cConflictParticipantId';var customizationListExpandedInstancesInputField = 'customizationlistInstancesexpanded';var currentProductVariantIdInputField = 'cProdVarId';var parameterValidParamName = 'parameterValid';var startExportFlag = 'false';var exportURL = '/ipc/ipc/tiles/exportConfiguration.jsp';</script><script src="/ipc/ipc/mimes/scripts/jscripts.js" type="text/javascript"></script><script src="/ipc/ipc/mimes/scripts/autosubmit.inc.js" type="text/javascript"></script><script type="text/javascript">
<!--
function genericLayoutPageOnLoad() {
startExport();
}
-->
</script>

  </head>

  <body class="ipcBody" onload="genericLayoutPageOnLoad();"
         onmouseleave="configUILeft(event);" >
          <form id="currentForm" name="currentForm"
            action=""
            method="POST">
            <input type="hidden" name="requestserial" value="10">
        

      <div id="globalarea">
        <div id="box1"></div>
        <div id="box2"></div>
        <div id="box3"></div>
        <div id="box4"></div>
        <div id="box5"></div>
        <div id="box6"></div>
        <div id="box7"></div>
        <div id="box8"></div>
        <div id="inner">
        </div>
        globalarea
      </div>
    

      <div id="configheader">
        <div id="box1"></div>
        <div id="box2"></div>
        <div id="box3"></div>
        <div id="box4"></div>
        <div id="box5"></div>
        <div id="box6"></div>
        <div id="box7"></div>
        <div id="box8"></div>
        <div id="inner">
        </div>
        configheader
      </div>
    

      <div id="configstatus">
        <div id="box1"></div>
        <div id="box2"></div>
        <div id="box3"></div>
        <div id="box4"></div>
        <div id="box5"></div>
        <div id="box6"></div>
        <div id="box7"></div>
        <div id="box8"></div>
        <div id="inner">
        </div>
        configstatus
      </div>
    

      <div id="configmessages">
        <div id="box1"></div>
        <div id="box2"></div>
        <div id="box3"></div>
        <div id="box4"></div>
        <div id="box5"></div>
        <div id="box6"></div>
        <div id="box7"></div>
        <div id="box8"></div>
        <div id="inner">
        </div>
        configmessages
      </div>
    
      <div id="mfatabarea">
        <div id="box1"></div>
        <div id="box2"></div>
        <div id="box3"></div>
        <div id="box4"></div>
        <div id="box5"></div>
        <div id="box6"></div>
        <div id="box7"></div>
        <div id="box8"></div>
        <div id="inner">
        </div>
        mfatabarea
      </div>
    
      <div id="multifunctionalarea">
        <div id="box1"></div>
        <div id="box2"></div>
        <div id="box3"></div>
        <div id="box4"></div>
        <div id="box5"></div>
        <div id="box6"></div>
        <div id="box7"></div>
        <div id="box8"></div>
        <div id="inner">
        </div>
        multifunctionalarea
      </div>
    

      <div id="singlelevelmfaworkarea">
        <div id="box1"></div>
        <div id="box2"></div>
        <div id="box3"></div>
        <div id="box4"></div>
        <div id="box5"></div>
        <div id="box6"></div>
        <div id="box7"></div>
        <div id="box8"></div>
        <div id="inner">
        </div>
        singlelevelmfaworkarea
      </div>

        </form>
        
   </body>
</html>
