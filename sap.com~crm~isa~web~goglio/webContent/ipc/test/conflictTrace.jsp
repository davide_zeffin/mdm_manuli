<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConflictUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConflictTraceUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/test/conflictTrace.jsp"/><%
%><%@ page import = "com.sap.spc.remote.client.object.Conflict"%><%

%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <link type="text/css" rel="stylesheet" href="/ipc/mimes/shared/style/stylesheet.css" />
<link type="text/css" rel="stylesheet" href="/ipc/ipc/mimes/style/stylesheet.css" />
<link type="text/css" rel="stylesheet" href="/ipc/mimes/stylesheet_ie6.css" />

    <title>Interactive Product Configuration</title>
    <script type="text/javascript">var currentInstanceIdInputField = 'cInstId';var instanceTreeStatusChangeInputField = 'instanceTreeStatusChange';var currentCharacteristicGroupNameInputField = 'cCharGroupName';var currentScrollCharacteristicGroupNameInputField = 'cScrollCharGroupName';var selectedCharacteristicGroupNameInputField = 'sCharGroupName';var currentCharacteristicNameInputField = 'cCharName';var characteristicStatusChangeInputField = 'characteristicStatusChange';var currentConflictIdInputField = 'cConflictId';var currentConflictParticipantIdInputField = 'cConflictParticipantId';var customizationListExpandedInstancesInputField = 'customizationlistInstancesexpanded';var currentProductVariantIdInputField = 'cProdVarId';var parameterValidParamName = 'parameterValid';var startExportFlag = 'false';var exportURL = '/ipc/ipc/tiles/exportConfiguration.jsp';</script><script src="/ipc/ipc/mimes/scripts/jscripts.js" type="text/javascript"></script><script src="/ipc/ipc/mimes/scripts/autosubmit.inc.js" type="text/javascript"></script><script type="text/javascript">
<!--
function genericLayoutPageOnLoad() {
startExport();
}
-->
</script>

  </head>

  <body class="ipcBody" onload="genericLayoutPageOnLoad();"
         onmouseleave="configUILeft(event);" >
          <form id="currentForm" name="currentForm"
            action=""
            method="POST">
            <input type="hidden" name="requestserial" value="10"><%
            
            %><%
            UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
            UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
			List conflictTrace = (List) request.getAttribute( RequestParameterConstants.CONFLICT_TRACE );
			request.removeAttribute( RequestParameterConstants.CONFLICT_TRACE );
			Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);
			
			ConflictTraceUI.include(pageContext,
								   uiContext.getJSPInclude("tiles.conflictTrace.jsp"),
								   uiContext,
								   conflictTrace,
								   customerParams);
        


		  %></form>
        
   </body>
</html>
