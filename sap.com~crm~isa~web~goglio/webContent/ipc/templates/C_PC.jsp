<%
%><%@ page import="java.util.*, com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.DynamicProductPictureTemplateUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.DynamicProductPictureShowUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.MasterdataUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
	DynamicProductPictureTemplateUI.IncludeParams includeParams =
		DynamicProductPictureTemplateUI.getIncludeParams(pageContext);
	UIContext uiContext = includeParams.getIpcBaseUI();
	InstanceUIBean instance = includeParams.getInstance();
    Hashtable customerParams = includeParams.getCustomerParams();

	int k = 23;
	String[] imgId = new String[k];
	String[] imgSrc = new String[k];
	String[] imgStyle = new String[k];
	String[] imgSize = new String[k];

	k = 0;
	imgId[k] = "C_PC_FLEXDISK.C_PC_FLEXDISK_A";
	imgSrc[k] = "/ipc/lib/images/CampusPC/05_Floppy.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_FLEXDISK.C_PC_FLEXDISK_C";
	imgSrc[k] = "/ipc/lib/images/CampusPC/06_DAT.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_FLEXDISK.C_PC_FLEXDISK_B";
	imgSrc[k] = "/ipc/lib/images/CampusPC/07_ZIP.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SPEAKER.C_PC_SPEAKER_C";
	imgSrc[k] = "/ipc/lib/images/CampusPC/01_Laut1.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SPEAKER.C_PC_SPEAKER_B";
	imgSrc[k] = "/ipc/lib/images/CampusPC/02_Laut2.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SPEAKER.C_PC_SPEAKER_A";
	imgSrc[k] = "/ipc/lib/images/CampusPC/03_Laut3.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_MONITOR.C_PC_MON_A";
	imgSrc[k] = "/ipc/lib/images/CampusPC/08_Monitor15.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_MONITOR.C_PC_MON_B";
	imgSrc[k] = "/ipc/lib/images/CampusPC/09_Monitor17.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_MONITOR.C_PC_MON_C";
	imgSrc[k] = "/ipc/lib/images/CampusPC/10_Monitor19.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_MONITOR.C_PC_MON_D";
	imgSrc[k] = "/ipc/lib/images/CampusPC/11_Monitor21.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_KEYB.C_PC_KEYB_A";
	imgSrc[k] = "/ipc/lib/images/CampusPC/12_Tastatur.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_KEYB.C_PC_KEYB_B";
	imgSrc[k] = "/ipc/lib/images/CampusPC/13_Handauflage.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_MEM.C_PC_MEM_A";
	imgSrc[k] = "/ipc/lib/images/CampusPC/14_RAM64.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "SCE_MEM.SCE_96MB";
	imgSrc[k] = "/ipc/lib/images/CampusPC/15_RAM96.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_MEM.C_PC_MEM_B";
	imgSrc[k] = "/ipc/lib/images/CampusPC/16_RAM128.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "SCE_MEM.SCE_192MB";
	imgSrc[k] = "/ipc/lib/images/CampusPC/17_RAM192.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_MEM.C_PC_MEM_C";
	imgSrc[k] = "/ipc/lib/images/CampusPC/18_RAM256.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SOFTWARE.C_PC_SOFTWARE_B";
	imgSrc[k] = "/ipc/lib/images/CampusPC/19_Pack1_3DStudio.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SOFTWARE.C_PC_SOFTWARE_C";
	imgSrc[k] = "/ipc/lib/images/CampusPC/20_Pack2_Lightscape.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SOFTWARE.C_PC_SOFTWARE_D";
	imgSrc[k] = "/ipc/lib/images/CampusPC/21_Pack3_Characterstudio.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SOFTWARE.C_PC_SOFTWARE_E";
	imgSrc[k] = "/ipc/lib/images/CampusPC/22_Pack4_AfterEffects.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SOFTWARE.C_PC_SOFTWARE_F";
	imgSrc[k] = "/ipc/lib/images/CampusPC/23_Pack5_Photoshop.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";
	k = k + 1;
	imgId[k] = "C_PC_SOFTWARE.C_PC_SOFTWARE_A";
	imgSrc[k] = "/ipc/lib/images/CampusPC/24_Pack6_Office2000.gif";
	imgStyle[k] =
		"position:absolute; top:32px; left:5px; width:200px; visibility:visible";
	imgSize[k] = "width=\"355\" height=\"480\"";

	String mimeURL = MasterdataUtil.getMimeURL(pageContext, "", null, null, "ipc/lib/images/CampusPC/00_Pc.jpg");

/*	 Remark: The following <div>-element is used to reserve the right height for the dynamic product picture.
	 Set the height of this element to the height that your picture has. In this example it is set to the 
	 height of the background: 500px.
	 This is necessary due to a bug in Internet Explorer: Without reserving the height the picture won't be displayed.
	 If you are using exclusively other browsers like Firefox you don't have to use this dummy <div>-element.
*/
%>
<div id="dummyForIE" style="height:500px"></div>
<div 
	id="Background" 
	style="position:absolute; top:0px; left:0px; width:380px; visibility:visible">
	<img 
		src='<%= mimeURL %>' 
		width="380" 
		height="500">
</div><%
	DynamicProductPictureShowUI.include(
		pageContext,
		uiContext.getJSPInclude("tiles.dynamicProductPictureShow.jsp"),
		uiContext,
		instance,
		imgId,
		imgSize,
		imgSrc,
		imgStyle,
		customerParams);

%>
