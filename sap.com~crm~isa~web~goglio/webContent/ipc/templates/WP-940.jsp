<%
%><%@ page import="java.util.*, com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%	
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.DynamicProductPictureTemplateUI"%><%	
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.DynamicProductPictureShowUI"%><%	
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%	
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.MasterdataUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%	
%><%@ taglib uri="/isa" prefix="isa"%><%
	DynamicProductPictureTemplateUI.IncludeParams includeParams =
		DynamicProductPictureTemplateUI.getIncludeParams(pageContext);
	UIContext uiContext = includeParams.getIpcBaseUI();
	InstanceUIBean instance = includeParams.getInstance();
    Hashtable customerParams = includeParams.getCustomerParams();

	int k = 11;
	String[] imgId = new String[k];
	String[] imgSrc = new String[k];
	String[] imgStyle = new String[k];
	String[] imgSize = new String[k];

	k = 0;
	imgId[k] = "WP_SCREEN.001";
	imgSrc[k] = "ipc/lib/images/WEB_PHONE/Screen/001_NEW.gif";
	imgStyle[k] =
		"position:absolute; top:20px; left:10px; width:200px; visibility:visible";
	imgSize[k] = "width=\"125\" height=\"75\"";
	k = k + 1;
	imgId[k] = "WP_SCREEN.002";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Screen/002_NEW.gif";
	imgStyle[k] =
		"position:absolute; top:20px; left:10px; width:200px; visibility:visible";
	imgSize[k] = "width=\"125\" height=\"75\"";
	k = k + 1;
	imgId[k] = "WP_CONNECTION.001";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Connection/001.gif";
	imgStyle[k] =
		"position:absolute; top:100px; left:10px; width:200px; visibility:visible";
	imgSize[k] = "width=\"50\" height=\"25\"";
	k = k + 1;
	imgId[k] = "WP_CONNECTION.002";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Connection/002.gif";
	imgStyle[k] =
		"position:absolute; top:100px; left:10px; width:200px; visibility:visible";
	imgSize[k] = "width=\"50\" height=\"25\"";
	k = k + 1;
	imgId[k] = "WP_CONNECTION.003";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Connection/003.gif";
	imgStyle[k] =
		"position:absolute; top:100px; left:10px; width:200px; visibility:visible";
	imgSize[k] = "width=\"50\" height=\"25\"";
	k = k + 1;
	imgId[k] = "WP_OPTIONS.001";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Options/001.gif";
	imgStyle[k] =
		"position:absolute; top:150px; left:10px; width:200px; visibility:visible";
	imgSize[k] = "width=\"50\" height=\"25\"";
	k = k + 1;
	imgId[k] = "WP_OPTIONS.002";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Options/002.gif";
	imgStyle[k] =
		"position:absolute; top:150px; left:100px; width:200px; visibility:visible";
	imgSize[k] = "width=\"50\" height=\"25\"";
	k = k + 1;
	imgId[k] = "WP_OPTIONS.003";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Options/003_part.gif";
	imgStyle[k] =
		"position:absolute; top:20px; left:40px; width:200px; visibility:visible";
	imgSize[k] = "width=\"50\" height=\"25\"";
	k = k + 1;
	imgId[k] = "WP_OPTIONS.004";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Options/004.gif";
	imgStyle[k] =
		"position:absolute; top:190px; left:100px; width:200px; visibility:visible";
	imgSize[k] = "width=\"25\" height=\"12\"";
	k = k + 1;
	imgId[k] = "WP_OPTIONS.005";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Options/005.gif";
	imgStyle[k] =
		"position:absolute; top:190px; left:10px; width:200px; visibility:visible";
	imgSize[k] = "width=\"50\" height=\"25\"";
	k = k + 1;
	imgId[k] = "WP_OPTIONS.006";
	imgSrc[k] = "/ipc/lib/images/WEB_PHONE/Options/006.gif";
	imgStyle[k] =
		"position:absolute; top:230px; left:120px; width:200px; visibility:visible";
	imgSize[k] = "width=\"50\" height=\"25\"";

	String mimeURL = MasterdataUtil.getMimeURL(pageContext, "", null, null, "ipc/lib/images/WEB_PHONE/boden.jpg");

/*   Remark: The following <div>-element is used to reserve the right height for the dynamic product picture.
	 Set the height of this element to the height that your picture has. In this example it is set to 260px because
	 the last element is placed 230px from the top and has a height of 25px 
	 This is necessary due to a bug in Internet Explorer: Without reserving the height the picture won't be displayed.
	 If you are using exclusively other browsers like Firefox you don't have to use this dummy <div>-element.
*/	 
%>
<div id="dummyForIE" style="height:260px"></div>

<!-- example for a background <div id="Background" style="position:absolute; top:0px; left:0px; width:200px; visibility:visible"> 
<img src='<%= mimeURL %>' width="250" height="800"> </div> --><%
	DynamicProductPictureShowUI.include(
		pageContext,
		uiContext.getJSPInclude("tiles.dynamicProductPictureShow.jsp"),
		uiContext,
		instance,
		imgId,
		imgSize,
		imgSrc,
		imgStyle,
		customerParams);

%>

