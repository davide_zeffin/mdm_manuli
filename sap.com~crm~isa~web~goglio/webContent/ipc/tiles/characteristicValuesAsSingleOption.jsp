<%-- THIS INCLUDE CONTAINS THE UI FOR THE EXPANDED DISPLAY OF
     CHARACTERISTIC VALUES --%>
<%--  important for characteristics:
      1. not of type NUM with intervals as domain
      2. have values which can be assigned by user
--%>
<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValueUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ValueUIBean" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/characteristicValuesAsSingleOption.jsp"/><%
%><table width = "100%" class="ipcValueSingleOption"><%
    CharacteristicValuesUI.IncludeParams includeParams = CharacteristicValuesUI.getIncludeParams(pageContext);
    UIContext uiContext = includeParams.getIpcBaseUI();
    InstanceUIBean currentInstance = includeParams.getInstance();
    GroupUIBean currentGroup = includeParams.getGroup();
    CharacteristicUIBean currentCharacteristic = includeParams.getCurrentCharacteristic();
    List values = includeParams.getValues();
    boolean showDescriptions = includeParams.isShowDescriptions();
    UIArea csticsArea = includeParams.getUiArea();
    boolean readOnlyMode = includeParams.isReadOnlyMode();
    boolean detailsMode = includeParams.isDetailsMode();
    Hashtable customerParams = includeParams.getCustomerParams();

    if( values.size() > 0 ) {
        //defines how many characteristic values should be arranged in one line (i.e. the columns)
        // int columnsDetail = uiContext.getPropertyAsInteger(RequestParameterConstants.CHARACTERISTIC_VALUE_NO_COLUMNS );
		int columnsDetail = 1;
        int countDetail = 0;
        for (Iterator characteristicValuesIt = values.iterator(); characteristicValuesIt.hasNext(); ) {
            ValueUIBean characteristicValue = (ValueUIBean) characteristicValuesIt.next();
            if( (!uiContext.getAssignableValuesOnly() || characteristicValue.isAssignable()
                      || characteristicValue.isAssigned())
                    && !(currentCharacteristic.hasIntervalAsDomain() && !characteristicValue.isAssigned()) ) {
                //uiContext.setCurrentCharacteristicValue(characteristicValue);
                if( countDetail % columnsDetail == 0 ) {
%><tr class="ipcValue"><%
                }
    %><td width = "<%= 100 / columnsDetail %>%" class="ipcValue"><%                
                CharacteristicValueUI.include(pageContext,
                                                             uiContext.getJSPInclude("tiles.characteristicValue.jsp"),
                                                             uiContext,
                                                             currentInstance,
                                                             currentGroup,
                                                             currentCharacteristic,
                                                             characteristicValue,
                                                             showDescriptions,
                                                             csticsArea,
                                                             readOnlyMode,
                                                             detailsMode,
                                                             customerParams);
    %></td><%
                if( countDetail % columnsDetail == (columnsDetail - 1) || !characteristicValuesIt.hasNext() ) {
%></tr><%
                }
            }
            ++countDetail;
        } //end of for
        if( currentCharacteristic.allowsMultipleValues() ) {
            //add an hidden input field with empty value to be sure the characteristic is in the request
            //submitted by the form, otherwise deselecting all options will result in keeping the
            //previously set values assigned.
%><input type = hidden name = "characteristic_<%= currentCharacteristic.getName() %>" value = ""><%
        }        
    }
%></table>