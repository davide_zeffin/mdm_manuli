<%-- This include contains the UI of one product variant
     using the object "currentProductVariant" you can get all the
     information about the current product variant.
     In the html form using this include you should add the following field:
     <input type=hidden name="<%= RequestParameterConstants.CURRENT_PRODUCT_VARIANT_ID %>" value=""/>
     to allow the communication with the IPC server. --%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ProductVariantUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ProductVariantUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/productVariant.jsp"/><%
//TODO: remove access to business objects
//Accessibility Notes: Seems to be a called JSP as a row from another JSP, will need to check
//read parameter from caller
ProductVariantUI.IncludeParams includeParams = ProductVariantUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
ProductVariantUIBean currentProductVariant = includeParams.getProductVariant();
Hashtable customerParams = includeParams.getCustomerParams();

String productName;
if (currentProductVariant.getItem() != null 
    && uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_LANGUAGE_DEPENDENT_NAMES)) {
    productName = currentProductVariant.getItem().getProductDescription();
}
else {
    productName = currentProductVariant.getId();
}
%><TD><%
    // product variant name
    %><%= productName %><%
%></TD><%
if (!uiContext.getHidePrices()) {
%><TD class="price"><%
    // price 
    %><%= currentProductVariant.getConfiguration().getRootInstance().getPrice() %><%
%></TD><%
}
%><TD class="compare" align="center"><%
    %><input type="checkbox" name="<%= RequestParameterConstants.PRODUCT_VARIANTS_TO_COMPARE %>" value="<%= currentProductVariant.getId() %>"><%
%></TD><%
if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLY_VAR_FIND)) {
%><TD class="action"><%
    // Assign the name of the selected product variant to the field CURRENT_PRODUCT_VARIANT_ID to make this 
    // product variant known to the IPC server.
    // We submit the form to replace the root instance by the selected product variant.
    String variantID = currentProductVariant.getId();
    String action = WebUtil.getAppsURL(pageContext,
                                        null,
                                        "/ipc/applyProductVariant.do",
                                        null,
                                        null,
                                        false);
	String hrefStart = "";
	String hrefEnd = "";
	// show link only if not in read-only mode
	if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)){
	    StringBuffer hrefStartBuffer = new StringBuffer();
	    hrefStartBuffer.append("<a href= \"javascript:changeToProductVariant('");
   	    hrefStartBuffer.append(variantID);
   	    hrefStartBuffer.append("', '");
   	    hrefStartBuffer.append(action);
   	    hrefStartBuffer.append("');\" title=\"");
   	    hrefStartBuffer.append(WebUtil.translate(pageContext, "ipc.apply", null));
   	    hrefStartBuffer.append("\">");
   	    hrefStart = hrefStartBuffer.toString();
   	    hrefEnd = "</a>";
	}
    %><%= hrefStart %><%
        %><isa:translate key="ipc.apply"/><%
    %><%= hrefEnd %><%
%></TD><%
}
%>