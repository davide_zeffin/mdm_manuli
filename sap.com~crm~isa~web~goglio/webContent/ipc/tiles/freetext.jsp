<%--
/**
 * TEXT FIELD FOR ADDITIONAL VALUES<%
 * in case the characteristic allows the input of additional values
 * we should add a separate text field to enable the user the
 * entrance of additional data
 **/
--%>  
<%@ taglib uri = "/isa" prefix = "isa"%><% 
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Enumeration"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIElement"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ValueUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/freetext.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
//read passed variables from request
CharacteristicValuesUI.IncludeParams includeParams = CharacteristicValuesUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
InstanceUIBean currentInstance = includeParams.getInstance();
GroupUIBean currentGroup = includeParams.getGroup();
CharacteristicUIBean currentCharacteristic = includeParams.getCurrentCharacteristic();
List values = includeParams.getValues();
boolean showDescriptions = includeParams.isShowDescriptions();
UIArea csticsArea = includeParams.getUiArea();
Hashtable customerParams = includeParams.getCustomerParams();

String cachedAddValue = null;
boolean readOnlyMode = includeParams.isReadOnlyMode();
// include for calendar control; only included if necessary
if (currentCharacteristic.showCalendarControl()) {
    %><%@ include file="/appbase/jscript/calendar.js.inc.jsp"%><%
}
//local variables
Hashtable valueBuffer = null;
if( !uiContext.getOnlineEvaluate() ) {
    Hashtable characteristicBuffer = uiContext.getCharacteristicsBuffer();
    if( characteristicBuffer != null ){
        valueBuffer = (Hashtable) characteristicBuffer.get(
                          currentCharacteristic.getBusinessObject().getInstance().getId() + "." + currentCharacteristic.getName() );
    }
}
if( !uiContext.getOnlineEvaluate() && valueBuffer != null ) {
    Enumeration valueEnum = valueBuffer.elements();
    while( valueEnum.hasMoreElements() ) {
        Object cValue = valueEnum.nextElement();
        if( cValue instanceof String ) {
            cachedAddValue = (String) cValue;
            break;
        }
    }
}
String textValue = "";
if( cachedAddValue != null ){
    //online_evaluate == false && a buffered value exists
    textValue = JspUtil.encodeHtml(cachedAddValue);
}else if (currentCharacteristic.hasAssignedValuesByUser() && !currentCharacteristic.allowsMultipleValues()) {
    textValue = ((ValueUIBean)(currentCharacteristic.getAssignedValues().get(0))).getLanguageDependentName();
}
if( !currentCharacteristic.getValidationError().isOk() ){
    textValue = currentCharacteristic.getErroneousValue();
}
String maxlengthAttr = "";
if (currentCharacteristic.getMaxLength() != 0) {
    maxlengthAttr = "maxlength = \"" + currentCharacteristic.getMaxLength() + "\"";
}
String currentCsticName = currentCharacteristic.getName();
String name = "input_" + currentCsticName;
String fieldId = name + "_ID";
int size = currentCharacteristic.getTypeLength();
StringBuffer actionBuffer = new StringBuffer().append(WebUtil.getAppsURL(
                                                      pageContext,
                                                      null,
                                                      "/ipc/dispatchSetValuesOnCaller.do",
                                                      null,
                                                      null,
                                                      false));
//actionBuffer = actionBuffer.append("#C_").append(currentCsticName);
String instanceId = currentInstance.getId();
String groupName = currentGroup.getName();
// assemble onChange attribute
StringBuffer onChangeBuffer = new StringBuffer().append("javascript:onCsticValueChange('");
onChangeBuffer.append(instanceId);
onChangeBuffer.append("','");
onChangeBuffer.append(groupName);
onChangeBuffer.append("','");
onChangeBuffer.append(currentCsticName);
onChangeBuffer.append("','");
onChangeBuffer.append(actionBuffer);
onChangeBuffer.append("');");
String onChange = onChangeBuffer.toString();
// assemble onkeypress attribute
StringBuffer onKeypressBuffer = new StringBuffer().append("javascript:returnKeyForCurrentForm(event, '");
onKeypressBuffer.append(actionBuffer);
onKeypressBuffer.append("','");
onKeypressBuffer.append(instanceId);
onKeypressBuffer.append("','");
onKeypressBuffer.append(groupName);        
onKeypressBuffer.append("');");
String onKeypress = onKeypressBuffer.toString(); 
// Check whether characteristic was the last focused. If true add a shortcut.        
String accessKeyAttribute = "";
if (currentCharacteristic.isLastFocused()) {
    UIElement lastFocusedCstic = new UIElement("lastfocused", uiContext);
    String lastFocusedAccessKey = lastFocusedCstic.getShortcutKey();
    accessKeyAttribute = "accesskey=\"" + WebUtil.translate(pageContext, lastFocusedAccessKey, null) + "\"";
}
// don't show this accessibility text if the layout is L2 (selectionList + inputField)
// otherwise the accessibility text would be displayed twice
if (ui.isAccessible() && (currentCharacteristic.determineLayoutNumber() != currentCharacteristic.LAYOUT_2) ) {
	String csticName = currentCharacteristic.getLanguageDependentName();
%>
	<label for="<%= fieldId %>"><isa:translate key="ipc.acc.value" arg0="<%=csticName%>"/></label> 
<%
}
	boolean showInputField = CharacteristicValuesUI.showInputField(readOnlyMode, currentCharacteristic);
	if (showInputField) { // (id=3)
%>
<input type="text" id="<%= fieldId %>" class="ipcText" name="<%= name %>" size = "<%= size %>" <%= maxlengthAttr %> value = "<%= textValue %>" onChange="<%= onChange%>" onkeypress="<%= onKeypress %>" onFocus="onFocus('<%= currentCsticName%>')" <%= accessKeyAttribute %> 
	tabindex="<%= csticsArea.getTabIndex()%>"<%
		if (ui.isAccessible() && currentCharacteristic.showCalendarControl())
		{
			String dateFormat = currentCharacteristic.getDateFormatForCalendarControl();
	%>title="<isa:translate key="access.input.dateformat" arg0="<%=dateFormat%>"/>"<%
		}
	%>
><%

		boolean embeddedCal = uiContext.getShowCalendarControlEmbedded();
		String dateFormat = "";
		String divId = "";
		String frameName = "";
		if (currentCharacteristic.showCalendarControl() && !ui.isAccessible()) {
	        frameName = "calframe_" + fieldId;
	        divId = "calarea_" + fieldId;     
	    	dateFormat = currentCharacteristic.getDateFormatForCalendarControl();
			String calendarOpen = WebUtil.translate(pageContext,"appbase.calendar.open", null);
			String onClickFunction = "openCalendarWindowDomainRelaxed('" + dateFormat + "', '" + fieldId + "');";
			if (embeddedCal) {
			    // use a different JS-function for the embedded calendar control
			    onClickFunction = "showCal('" +  dateFormat + "', '" + fieldId + "', '" + divId + "', '" + frameName + "');";
			}
    %><a class="icon" href="#" tabindex="<%= csticsArea.getTabIndex() %>" onClick="<%= onClickFunction %>" title="<isa:translate key="access.link" arg0="<%=calendarOpen%>" arg1=""/>"><%
		%><img src="<%=WebUtil.getMimeURL(pageContext, "/mimes/images/calendar.gif") %>" width="16" height="16" alt="<isa:translate key="appbase.calendar.open"/>" border="0" class="display-image"><%
	%></a><%
		}
		String entryFieldMask = currentCharacteristic.getInputFieldMask(pageContext);
		if (!entryFieldMask.equals("") ){
			%>&nbsp;<%= entryFieldMask %><%
		}
		// add a div-area for the embedded calendar control only if "embedded" is enabled
		if (currentCharacteristic.showCalendarControl() && !ui.isAccessible() & embeddedCal) {
	        String embeddedCalJspUrl = WebUtil.getAppsURL(pageContext, 
    		                                                null,
            		                                        "/appbase/calendar.jsp",
                    		                                null,
                            		                        null,
                                    		                false);
	        String embeddedCalUrl = embeddedCalJspUrl + "?dateFormat=" + dateFormat + "&dateValue=" + textValue + "&relaxDomain=T&embedded=T&divId=" + divId;
			%><div id="<%= divId%>" style="display:none;"><%
				%><iframe src="<%= embeddedCalUrl %>" width="300px" height="370px" id="<%= frameName%>" scrolling="no" marginheight="0" marginwidth="0" frameborder="0"><%
		        %></iframe><%
			%></div><%
		}
	} // (id=3)
	// don't show the input-field if
	// 	+ the value is assigned by system 
	// 	+ OR the UI is in display-mode 
	// 	+ OR the characteristic is read-only
	else { // (id=3)
	    // we don't show the value at all if the current layout case is "selectionList + inputField" (Layout 2)
	    // reason: in this case the read-only value is already shown in the selectionList-tile
	    // if we would show it in this freetext-tile as read-only again this would lead to the display 
	    // of two times the same value
	    if (currentCharacteristic.determineLayoutNumber() != currentCharacteristic.LAYOUT_2) { // (id=6)
			if (currentCharacteristic.hasAssignedValues()) { // (id=1)
			    ValueUIBean value = (ValueUIBean)currentCharacteristic.getAssignedValues().get(0);
			    String cssClassSpan = "";
			    // if the value is assigned by system we display the value italic
			    if (value.isAssignedBySystem()) { // (id=2)
					cssClassSpan = "systemAssigned";
			    } // (id=2)
				if (ui.isAccessible()) {
			%>(<isa:translate key="ipc.readonly"/>)&nbsp;<%
				}
			%><span class="<%= cssClassSpan %>"><%= value.getLanguageDependentName() %>&nbsp;&nbsp;&nbsp;</span><%
			} // (id=1)	    
		}// (id=6)
	} // (id=3)

FormatValidator.FormatError error = currentCharacteristic.getValidationError();
if( !error.isOk() ){
%>&nbsp;<font color = "red" title="<isa:translate key="ipc.error.message"/>"><%
    if( error.isValueTooLong()){
    %><isa:translate key = "ipc.validate.too_long"/><%
    %><br /><%
    } else if( error.isInvalid() ) {
    %><isa:translate key = "ipc.validate.invalid"/><%
    %><br /><%
    } else if( error.isDateInvalid()) {
    %><isa:translate key = "ipc.validate.date.invalid"/><%
    %><br /><%
    } else {
        if( error.isIntTooLong()) {
    %><isa:translate key = "ipc.validate.int_too_long"/><%
    %><br /><%
        }
        if( error.isFractTooLong()) {
    %><isa:translate key = "ipc.validate.fract_too_long"/><%
    %><br /><%
        }
        if( error.isMonthInvalid()) {
    %><isa:translate key = "ipc.validate.date.month"/><%
    %><br /><%
        }
        if( error.isDayInvalid()) {
    %><isa:translate key = "ipc.validate.date.day"/><%
    %><br /><%
        }
    }
%></font><%
}
%>