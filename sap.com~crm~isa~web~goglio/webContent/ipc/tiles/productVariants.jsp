<%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*" %><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ProductVariantUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ProductVariantsUI"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/productVariants.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
ProductVariantsUI.IncludeParams includeParams = ProductVariantsUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
List productVariantList = includeParams.getProductVariants();
Hashtable customerParams = includeParams.getCustomerParams();

%><input type="hidden" name="<%= Constants.CURRENT_PRODUCT_VARIANT_ID %>"><%
%>
<table class="ipcAreaFrame" width="100%" cellpadding="0" cellspacing="0"><%
    %><tr><%
        %><th><%
            %><%
			int colno = 4;
			if (uiContext.getHidePrices())
				colno--;
			if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLY_VAR_FIND))
				colno--;
			int rowno = productVariantList == null ? 0 : productVariantList.size();
			String columnNumber = Integer.toString(colno);
			String rowNumber = Integer.toString(rowno);
			String firstRow = Integer.toString(1);
			String lastRow = Integer.toString(rowno);
			String tableTitle = WebUtil.translate(pageContext,"ipc.productvariant.list", null);
		%>
		<%
			if (ui.isAccessible())
			{
		%>
			<a href="#itemstableend" 
				id="itemstablebegin"
				title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
			</a>
		<%
			}
		%>
            <table class="ipcProductVariants" width="100%" cellpadding="0" cellspacing="0" summary="<isa:translate key="access.table.summary" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>"><%
                %><tr><%
                    int noOfCols = 0;
                    %><th scope="col" title="<isa:translate key="ipc.name"/>"><%
                        %><isa:translate key="ipc.name"/><%
                        noOfCols++;
                    %></th><%
                    if (!uiContext.getHidePrices()) {
                    %><th scope="col" title="<isa:translate key="ipc.price"/>"><%
                        %><isa:translate key="ipc.price"/><%
                        noOfCols++;
                    %></th><%
                    }
                    %><th scope="col" title="<isa:translate key="ipc.product_variants.compare"/>"><%
                        %><isa:translate key="ipc.product_variants.compare"/><%
                        noOfCols++;
                    %></th><%
                    if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLY_VAR_FIND)){
                    %><th scope="col" title="<isa:translate key="ipc.action"/>"><%
                        %><isa:translate key="ipc.action"/><%
                        noOfCols++;
                    %></th><%
                    }
                %></tr><%
                if (productVariantList != null && !productVariantList.isEmpty()) { // (id=1)
                    Iterator productVariants = productVariantList.iterator();
                    while (productVariants.hasNext()) { // (id=2)
                        ProductVariantUIBean currentProductVariant = ((ProductVariantUIBean) productVariants.next());
                // here begins the layout of one single product variant 
                %><tr><%
                    ProductVariantUI.include(pageContext,
                                             uiContext.getJSPInclude("tiles.productVariant.jsp"),
                                             uiContext,
                                             currentProductVariant,
                                             customerParams);
                %></tr><%
                // here ends the layout of one single product variant
                    } // (id=2)
                }
                else { // (id=1)
                %><tr><%
                    %><td colspan="<%= noOfCols %>"><%
                        %><isa:translate key="ipc.product_variants.notfound"/><%
                    %></td><%
                %></tr><%
                } // (id=1)
            %></table>
<%
	// Data Table Postamble
	if (ui.isAccessible())
	{
%>
		<a id="itemstableend" 
			href="#itemstablebegin" 
			title="<isa:translate key="access.table.end" 
				arg0="<%=tableTitle%>"/>"></a>
<%
	}
%>
            <%
            if (productVariantList != null && !productVariantList.isEmpty()) {
            %><table><%
                %><tr><%
                    %><td class="compareBtn"><%
                    String compareBtnTxt = WebUtil.translate(pageContext, "ipc.product_variants.compare", null);
                        %><a href="#" title="<isa:translate key="access.button" arg0="<%=compareBtnTxt%>" arg1=""/>" class="ipcButton"
                            onClick="javascript:compareProductVariants('<isa:webappsURL name ="/ipc/compareProductVariants.do"/>');"><isa:translate key="ipc.product_variants.compare"/></a> <%
                    %></td><%
                %></tr><%
			    %><tr><%    
			        %><td height="20px"><%
			            %><img src='<isa:mimeURL name="ipc/mimes/images/sys/spacer.gif"/>' height="20" alt=""><%
			        %></td><%
				%></tr><%                
            %></table><%
            }
        %></th><%
    %></tr><%
%></table>
<%
	if (ui.isAccessible())
	{
		String tabTxt = WebUtil.translate(pageContext, "ipc.mfa.tabtitle", null); 
%>
<a 
	href="#tabmfa-begin"
	id="tabmfa-end"
	title="<isa:translate key="access.tab.end" arg0="<%=tabTxt%>"/>" 
></a>	
<%
	}
%>
<%
%>