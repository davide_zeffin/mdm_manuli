<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.InstanceUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><isa:moduleName name = "/ipc/tiles/instance.jsp"/><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
InstanceUI.IncludeParams includeParams = InstanceUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
InstanceUIBean currentInstance = includeParams.getInstance();
GroupUIBean currentCharacteristicGroup = includeParams.getGroup();
UIArea instancesArea = includeParams.getUiArea();
Hashtable customerParams = includeParams.getCustomerParams();


// If the instance is not consistent we want to show it
// using the stylesheet "ipcConflict"
%><td class="<%= currentInstance.getStyleClass() %>" title="<isa:translate key="<%= currentInstance.getStyleTooltip() %>"/>"><%

    // Here we set the link to submit the form "instances" on click
    // Before we must assign the id of the selected instance to the field
    // CURRENT_INSTANCE_ID to make this instance known to the IPC server.
    // Then we submit the form instances.
    // With "onclick" we refresh the MultifunctionalityArea so that always
    // the assigned values of the selected instance are shown.
    if (currentInstance.hasVisibleCharacteristics()) { // (id=1)
        String href = null;
        String action = null;
        String forwardDestination = null;
        String target = null;
        //click on instance in order to select the instance
        action =  WebUtil.getAppsURL(pageContext,
                                        null,
                                        ActionForwardConstants.CHANGE_INSTANCE,
                                        null,
                                        null,
                                        false);
        StringBuffer hrefBuffer = new StringBuffer();
        hrefBuffer.append("javascript:onInstanceChange(");
        hrefBuffer.append("'").append(currentInstance.getId()).append("',");;
        hrefBuffer.append("'',");
        hrefBuffer.append("'").append(currentCharacteristicGroup.getName()).append("',");
        hrefBuffer.append("'").append(action).append("');");
        href = hrefBuffer.toString();
        String onClick = "";

        StringBuffer onClickBuffer = new StringBuffer();
        onClickBuffer.append("javascript:onInstanceChange(");
        onClickBuffer.append("'").append(currentInstance.getId()).append("',");
        onClickBuffer.append("'',");
        onClickBuffer.append("'").append(currentCharacteristicGroup.getName()).append("',");
        onClickBuffer.append("'").append(action).append("');");
        onClick = onClickBuffer.toString();
        String cssClass = "";
        String cssClassTt = "ipc.instance.no_sel";
        if (currentInstance.isSelected()) {
            cssClass = "selected";
            cssClassTt = "ipc.instance.sel";
        } else {
        	cssClass = "";
        }
        cssClassTt = WebUtil.translate(pageContext, cssClassTt, null);
    %><a href="<%= href %>" class="<%= cssClass %>" onClick="<%= onClick %>" tabindex="<%= instancesArea.getTabIndex()%>"
    title="<%= WebUtil.translate(pageContext, "ipc.instance.selected", new String[]{currentInstance.getLanguageDependentName(), cssClassTt}) %>"><%
        %><%= currentInstance.getLanguageDependentName() %><%
    %></a><%
    }
    else { // (id=1)
        %><%= currentInstance.getLanguageDependentName() %><%
    } // (id=1)
    // if we disabled status lights by setting the parameter
    // SHOW_STATUS_LIGHTS to "F" (default setting)
    // and the the instance is not complete
    // we mark it with (*) after the instance name
    if (uiContext.getShowStatusLights()) {
    %>&nbsp;<img style="display:inline"  src="<isa:mimeURL name="<%= currentInstance.getStatusImage().getImagePath() %>" />" alt = "<isa:translate key="<%= currentInstance.getStatusImage().getResourceKey() %>" />"><%
    } else {
        if (!currentInstance.isComplete()){
    %>&nbsp;<isa:translate key="ipc.inst.incomplete.sign" /><%
        }
    }      
%></td><%
%><td><%
    %><%= currentInstance.getQuantityForInstanceTree() %><%
%></td><%
%>
