<%
%><%-- this include contains the UI of the conflict handling shortcut --%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConflictHandlingShortcutUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConflictHandlingShortcutsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.spc.remote.client.object.ExplanationTool"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/conflictHandlingShortcuts.jsp"/><%

BaseUI ui = new BaseUI(pageContext);
ConflictHandlingShortcutsUI.IncludeParams includeParams = ConflictHandlingShortcutsUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
ConflictHandlingShortcutUIBean conflictHandlingShortcut = includeParams.getConflictHandlingShortcut();
Hashtable customerParams = includeParams.getCustomerParams();

%><div class="ipcStatusBarInHeader"><%
%><table width="100%" class="ipcConflictStatusbar" title="<isa:translate key="ipc.tt.conflict"/>"><%
    %><colspan><%
        %><col="30%"><%
        %><col="30%"><%
        %><col="30%"><%
        %><col="10%"><%
    %></colspan><%
    %><tr><%
        %><td align="left"><%
            %><isa:translate key="ipc.conflict.options"/><%
        %></td><%
        %><td align="left"><%
            %><isa:translate key="ipc.conflict.selection"/><%
        %></td><%
        %><td align="left"><%
            %><isa:translate key="ipc.conflict.recommendation"/><%
        %></td><%
        %><td><%
        %></td><%
    %></tr><%
    %><tr><%
        %><td align="left" title="<isa:translate key="ipc.conflict.options"/>"><%
            %><%= conflictHandlingShortcut.getObservable() %><%
        %></td><%
        %><td align="left" title="<isa:translate key="ipc.conflict.selection"/>"><%
            %><%= conflictHandlingShortcut.getValueToDrop() %><%
        %></td><%
        %><td align="left" title="<isa:translate key="ipc.conflict.recommendation"/>"><%
            { //localize variables
                String action = WebUtil.getAppsURL(pageContext,
                                                    null,
                                                    "/ipc/removeConflictParticipant.do",
                                                    null,
                                                    null,
                                                    false);
                String currentConflictId = conflictHandlingShortcut.getConflictId();
                String currentConflictParticipant = conflictHandlingShortcut.getConflictParticipantId();
	            if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)){
	            	// show link only in edit-mode
	            	%><a href="javascript:removeConflictParticipant('<%= action %>', '<%= currentConflictId %>', '<%= currentConflictParticipant %>');"
	            	title="<isa:translate key="ipc.conflict.adopt" arg0="<%= ExplanationTool.VALUE_LAYOUT_TAG_BEGIN %>" arg1="<%= conflictHandlingShortcut.getValueToKeep() %>" arg2="<%= ExplanationTool.VALUE_LAYOUT_TAG_END %>"/>"><%
	            }
	            %><isa:translate key="ipc.conflict.adopt" arg0="<%= ExplanationTool.VALUE_LAYOUT_TAG_BEGIN %>" arg1="<%= conflictHandlingShortcut.getValueToKeep() %>" arg2="<%= ExplanationTool.VALUE_LAYOUT_TAG_END %>"/><%
				if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)){
		            %></a><%
				}
            }
        %></td><%
        %><td align="right"><%
            { //localize variables
				if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)){
					// show link only in edit-mode

	                String currentCsticName = "";
	                String action = WebUtil.getAppsURL(pageContext,
	                                                    null, 
	                                                    "/ipc/showConflictDetails.do", 
	                                                    null, 
	                                                    null,
	                                                    false);                                                    
		            %><a href="javascript:openConflictSolver('<%= action %>');"
		            title="<isa:translate key="ipc.details"/>"><%
		                %><isa:translate key="ipc.details"/><%
		            %></a><%
				}
            }
        %></td><%
    %></tr><%
%></table><%
%></div><%
%>