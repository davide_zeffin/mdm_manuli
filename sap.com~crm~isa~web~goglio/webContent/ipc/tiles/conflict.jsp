<%--    This include contains the UI of one conflict in the ConflictSolver
         Using the object "conflict" you can get all the
         information about the conflict.--%>
<%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "org.apache.struts.util.MessageResources"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConflictUI"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/conflict.jsp"/><%

//TODO: remove access to business objects
%><%@ page import = "com.sap.spc.remote.client.object.Conflict"%><%
%><%@ page import = "com.sap.spc.remote.client.object.ConflictParticipant"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%

BaseUI ui = new BaseUI(pageContext);
ConflictUI.IncludeParams includeParams = ConflictUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
Conflict conflict = includeParams.getConflict();
Hashtable customerParams = includeParams.getCustomerParams();


List conflictParticipantsList = conflict.getParticipants();
%>
<%            
String grpBeginId = "group" + conflict.getId() + "-begin";
String grpEndId = "group" + conflict.getId() + "-end";
String grpBeginHref = "#" + grpBeginId;
String grpEndHref = "#" + grpEndId;
	String grpTxt = WebUtil.translate(pageContext, "ipc.conflict.deselect.option", null); 
	if (ui.isAccessible())
	{
%>
			<a 
				href="<%=grpEndHref%>"
				id="<%=grpBeginId%>"
				title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>" 
			></a>
<%
	}
%><table border="0" cellpadding="0" cellspacing="0" width="100%" class="conflictGroup"><%
    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.CONFLICT_SOLVER_SHOW_CONFLICT_SOLUTION_RATE)) {
    %><colgroup><%
        %><col width="85%"><%
        %><col width = "15%"><%
    %></colgroup><%
    }
    %><tr><%
        %><th title="<isa:translate key = "ipc.conflict.deselect.option"/>" class="csticName"><%
            %><isa:translate key = "ipc.conflict.deselect.option"/><%
        %></th><%
        if (uiContext.getPropertyAsBoolean(RequestParameterConstants.CONFLICT_SOLVER_SHOW_CONFLICT_SOLUTION_RATE)) {
        %><th title="<isa:translate key = "ipc.conflict.solution.rate"/>"><%
            %><isa:translate key = "ipc.conflict.solution.rate"/><%
        %></th><%
        }
    %></tr><%
    Iterator conflictParticipants = conflictParticipantsList.iterator();

    while (conflictParticipants.hasNext()) {
        ConflictParticipant conflictParticipant = (ConflictParticipant) conflictParticipants.next();
    %><tr><%
        %><td><%
            String checked = "";

            if (conflictParticipant.isSuggestedForDeletion()) {
                checked = "checked";
            }
		    ServletContext context = pageContext.getServletContext();			    
		    MessageResources resources = WebUtil.getResources(context);
            %><input type="radio" id="ipc_conflict_<%= conflict.getId() %>" name="ipc.conflict_<%= conflict.getId() %>" <%= checked %>
                        value="<%= conflictParticipant.getId() %>"> <label for="ipc_conflict_<%= conflict.getId() %>"><%= conflictParticipant.getText(uiContext.getLocale(),
                                                                                                    resources) %></label><%
        %></td><%
            if (uiContext.getPropertyAsBoolean(RequestParameterConstants.CONFLICT_SOLVER_SHOW_CONFLICT_SOLUTION_RATE)) {
        %><td><%
            %><%= conflictParticipant.getSolutionRate() %>%<%
        %></td><%
            }
    %></tr><%
    }
    %><tr><%
        %><td><%
            { //enclose local variables
                String action = WebUtil.getAppsURL(pageContext,
                                                       null,
                                                       "/ipc/removeConflictParticipant.do",
                                                       null,
                                                       null,
                                                       false);
                String currentConflictId = conflict.getId();
                String currentConflictParticipant = ""; // set by radio-button 
            %><a href="javascript:removeConflictParticipant('<%= action %>', '<%= currentConflictId %>', '<%= currentConflictParticipant %>');"
            	title="<isa:translate key = "ipc.conflict.deselect"/>"><%
                %><isa:translate key = "ipc.conflict.deselect"/><%
            %></a><%
            }
        %></td><%
    %></tr><%
%></table>
<%
	if (ui.isAccessible())
	{
%>
<a 
	href="<%=grpBeginHref%>"
	id="<%=grpEndId%>"
	title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>" 
></a>	
<%
	}
%>