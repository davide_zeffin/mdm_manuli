<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.HttpServletRequestFacade"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIButton"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIElement"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/shortcutInfo.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
%><html><%
%><head><%
    %><title> IPC - Internet Pricing And Configuration [Version 5.0]</title><%
    //include stylsheet
	%><isa:stylesheets/><%
    %><script src="<isa:mimeURL name ="/ipc/mimes/scripts/jscripts.js"/>" type="text/javascript"><%
    %></script><%
%></head><%
%><body class = "ipcBody">
<%
			int colno = 2;
			int rowno = 11;
			String columnNumber = Integer.toString(colno);
			String rowNumber = Integer.toString(rowno);
			String firstRow = Integer.toString(1);
			String lastRow = Integer.toString(rowno);
			String tableTitle = WebUtil.translate(pageContext,"ipc.key.info.title", null);
		%>
		<%
			if (ui.isAccessible())
			{
		%>
			<a href="#itemstableend" 
				id="itemstablebegin"
				title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
			</a>
		<%
			}
		%>
<%    
%><strong><isa:translate key="ipc.key.info.title"/></strong><br><br><%
String requiredSign = WebUtil.translate(pageContext, "ipc.cstic.required.sign", null);
%><isa:translate key="ipc.info.text1" arg0="<%= requiredSign %>"/><br><br><%
%><isa:translate key="ipc.info.text2"/><br><br><%
%><isa:translate key="ipc.info.text5"/>&nbsp;<isa:translate key="ipc.info.text6"/>&nbsp;<isa:translate key="ipc.info.text7"/>&nbsp;<isa:translate key="ipc.info.text8"/><br><br><%
%><isa:translate key="ipc.info.text9"/><br><br><%
%><isa:translate key="ipc.info.text3"/><br><br><%
%><isa:translate key="ipc.info.text4"/><br><br><%
	%>
        	<table cellpadding="0" style="font-size:10px;" cellspacing="0" width="100%" border="0" summary="<isa:translate key="access.table.summary" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>"><%
		%><tr><%
			%><th scope="col" title="<isa:translate key="ipc.key.info.title.target"/>"><isa:translate key="ipc.key.info.title.target"/></td><%
			%><th scope="col" title="<isa:translate key="ipc.key.info.title.shortcut"/>"><isa:translate key="ipc.key.info.title.shortcut"/></td><%
		%></tr><%
        %><tr><%
			UIArea buttonsArea = new UIArea("buttons", uiContext);
			String buttonsAreaAccessKey = buttonsArea.getShortcutKey();                        
	        %><td><isa:translate key="ipc.area.buttons"/></td><%
			%><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= buttonsAreaAccessKey %>"/></td><%
        %></tr><%
		%><tr><%
		    UIArea csticsArea = new UIArea("cstics", uiContext);
		    String csticsAreaAccessKey = csticsArea.getShortcutKey();                        
		    %><td><isa:translate key="ipc.area.cstics"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= csticsAreaAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIArea groupsArea = new UIArea("groups", uiContext);
		    String groupsAreaAccessKey = groupsArea.getShortcutKey();                        
		    %><td><isa:translate key="ipc.area.groups"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= groupsAreaAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIArea groupListArea = new UIArea("grouplist", uiContext);
		    String groupListAreaAccessKey = groupListArea.getShortcutKey();                        
		    %><td><isa:translate key="ipc.area.grouplist"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= groupListAreaAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIArea instancesArea = new UIArea("instances", uiContext);
		    String instancesAreaAccessKey = instancesArea.getShortcutKey();                        
		    %><td><isa:translate key="ipc.area.instances"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= instancesAreaAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIArea mfaArea = new UIArea("mfa", uiContext);
		    String mfaAreaAccessKey = mfaArea.getShortcutKey();                        
		    %><td><isa:translate key="ipc.area.mfa"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= mfaAreaAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIArea searchArea = new UIArea("search", uiContext);
		    String searchAreaAccessKey = searchArea.getShortcutKey();                        
		    %><td><isa:translate key="ipc.area.search"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= searchAreaAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIButton acceptButton = new UIButton("acceptbutton", uiContext);
		    String acceptButtonAccessKey = acceptButton.getShortcutKey();                        
		    %><td><isa:translate key="ipc.key.info.button.accept"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= acceptButtonAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIButton cancelButton = new UIButton("cancelbutton", uiContext);
		    String cancelButtonAccessKey = cancelButton.getShortcutKey();                        
		    %><td><isa:translate key="ipc.key.info.button.cancel"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= cancelButtonAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIButton resetButton = new UIButton("resetbutton", uiContext);
		    String resetButtonAccessKey = resetButton.getShortcutKey();                        
		    %><td><isa:translate key="ipc.key.info.button.reset"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= resetButtonAccessKey %>"/></td><%
		%></tr><%
		%><tr><%
		    UIElement lastFocusedCstic = new UIElement("lastfocused", uiContext);
		    String lastFocusedAccessKey = lastFocusedCstic.getShortcutKey();                        
		    %><td><isa:translate key="ipc.key.info.element.lastfocused"/></td><%
		    %><td><isa:translate key="ipc.key.info.prefix"/>&nbsp;<isa:translate key="<%= lastFocusedAccessKey %>"/></td><%
		%></tr><%
    %></table>
<%
	// Data Table Postamble
	if (ui.isAccessible())
	{
%>
		<a id="itemstableend" 
			href="#itemstablebegin" 
			title="<isa:translate key="access.table.end" 
				arg0="<%=tableTitle%>"/>"></a>
<%
	}
%>
    <%
%><br><a href="javascript:window.close();" title="<isa:translate key="ipc.key.info.windowclose"/>"><isa:translate key="ipc.key.info.windowclose"/></a><%
%></body><%
%></html>
		