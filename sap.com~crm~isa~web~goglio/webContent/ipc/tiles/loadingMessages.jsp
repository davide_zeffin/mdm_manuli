<%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.LoadingMessageUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.LoadingMessagesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.LoadingMessageUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/loadingMessages.jsp"/><%
LoadingMessagesUI.IncludeParams includeParams = LoadingMessagesUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
List messages = includeParams.getMessages();
Hashtable customerParams = includeParams.getCustomerParams();
CharacteristicUIBean csticDelta = includeParams.getCsticDelta();

// loop over list of loading messages
for (int i=0; i<messages.size(); i++){
    LoadingMessageUIBean message = (LoadingMessageUIBean)messages.get(i);
    LoadingMessageUI.include(pageContext,
                        uiContext.getJSPInclude("tiles.loadingMessage.jsp"),
                        uiContext,
                        message,
                        csticDelta,
                        customerParams);
}%>