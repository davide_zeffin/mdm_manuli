<%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MessagesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.MessageUIBean" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/messages.jsp"/><%
// Here starts the display of messages from Import/Export and Search/Set
MessagesUI.IncludeParams includeParams = MessagesUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
MessageUIBean message = includeParams.getMessage();
Hashtable customerParams = includeParams.getCustomerParams();

String translateKey = message.getMessageTranslateKey();
String shortText = message.getMessageShortText();
String messageLongText = message.getMessageLongText();
String[] arguments = message.getMessageArguments();
%><table class="ipcMessages"><%
	%><tr><%
    	%><td class="messageIcon"><%
        	%><img style="display:inline" src="<isa:mimeURL name="<%= message.getStatusImage().getImagePath() %>" />" alt="<isa:translate key="<%= message.getStatusImage().getResourceKey() %>"/>"><%
        %></td><%
        if (translateKey != null && !translateKey.equals("")){
        %><td class="messageKey"><%= WebUtil.translate(pageContext, translateKey, arguments) %></td><%       
        }
        if (shortText != null && !shortText.equals("")){
        %><td class="messageShortText"><%= JspUtil.replaceSpecialCharacters(shortText) %></td><%        
        }
        if (messageLongText != null && !messageLongText.equals("")){
        %><td class="messageLongText"><isa:translate key="ipc.message.details"/></td><%
        }
        %><td>&nbsp;</td><%
        List messageActions = message.getActions();
        for (Iterator it = messageActions.iterator(); it.hasNext(); ) {
        	MessageUIBean.MessageAction action = (MessageUIBean.MessageAction)it.next();
        	%><td class="messageActionText"><%
        	    %><a href="<%= action.getAction() %>('<isa:webappsURL name="<%= action.getActionUrl() %>"/>')" title="<isa:translate key="<%= action.getKey() %>"/>"><%
        	        %><isa:translate key="<%= action.getKey() %>"/><%
        	    %></a><%
        	%></td><%
        }
    %></tr><%
%></table><%        
%>