<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.StringUtil"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
%><BODY><%
    %><FORM NAME="back2hookUrl" METHOD="POST" ACTION="<%= JspUtil.encodeHtml(uiContext.getHookUrl()) %>"
        TARGET="<%= uiContext.getPropertyAsString( RequestParameterConstants.TOPFRAME ) %>"><%
        %><INPUT TYPE="HIDDEN" NAME="RETCODE" VALUE="1"><%
        for (Enumeration e = request.getAttributeNames(); e.hasMoreElements();) {
            String nextParam = (String)e.nextElement();
            if (nextParam.startsWith("CFG-") ||
				nextParam.startsWith("extConfig") ||
            	nextParam.startsWith("HEADER-") ||
                nextParam.startsWith("INS-") ||
                nextParam.startsWith("extInst") ||
                nextParam.startsWith("INSTANCES") ||
                nextParam.startsWith("PRT-") ||
                nextParam.startsWith("extPart") ||
                nextParam.startsWith("PART_OF-") ||
                nextParam.startsWith("VAL-") ||
                nextParam.startsWith("extValue") ||
                nextParam.startsWith("VALUES-") ||
                nextParam.startsWith("VK-")  ||
                nextParam.startsWith("extPrice") ||
                nextParam.startsWith("VARIANT_CONDITIONS-") ||
                nextParam.startsWith("CONTEXT-") ||
                nextParam.startsWith("COND-") ||
                nextParam.startsWith("cond") ||
                nextParam.startsWith("CURRENCY_") ||
                nextParam.startsWith("currency") ||
                nextParam.startsWith("NET_") ||
                nextParam.startsWith("net") ||
                nextParam.startsWith("TAX_") ||
                nextParam.startsWith("tax") ||
                nextParam.startsWith("GROSS_") ||
                nextParam.startsWith("gross") ||
                nextParam.startsWith("TOTAL_") ||
                nextParam.startsWith("total") ||
                nextParam.startsWith("SUBTOTAL") ||
                nextParam.startsWith("subtotal")) {

					String value = (String) request.getAttribute(nextParam);
					if(nextParam.startsWith("INS-OBJ_TXT") || nextParam.startsWith("VAL-CHARC_TXT") || nextParam.startsWith("VAL-VALUE_TXT") ) {
						value = StringUtil.toUTF16String(value);
					}
 			    
					%><INPUT TYPE="HIDDEN" NAME="<%=nextParam%>" VALUE="<%=value%>" ><%	
            }
        }
    %></FORM><%
    %><SCRIPT LANGUAGE="JavaScript"><%
        %>document.back2hookUrl.submit();<%
    %></SCRIPT><%
%></BODY>