<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.Hashtable" %><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.MimeUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MimeObjectUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/mimeDetailsPopUp.jsp"/><%

BaseUI ui = new BaseUI(pageContext);
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
Hashtable customerParams = (Hashtable)request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);
%><html><%
%><head><%
    %><title> IPC - Internet Pricing And Configuration [Version 5.0]</title><%
    //include stylsheet
    %><script src="<isa:mimeURL name ="/ipc/mimes/scripts/jscripts.js"/>" type="text/javascript"><%
    %></script><%
%></head><%
%><body class = "ipcBody"><%
	%><br><%
	%><center><%
	    MimeUIBean mimeBean = (MimeUIBean) (request.getAttribute(RequestParameterConstants.CURRENT_MIME_OBJECT));
	    MimeObjectUI.include(pageContext,
	                         uiContext.getJSPInclude("tiles.mimeObject.jsp"),
	                         uiContext,
	                         null,
	                         mimeBean,
	                         MimeUIBean.C_NULL,
	                         null,
	                         null,
	                         customerParams);
		%><br><a href="javascript:window.close();" title="<isa:translate key="ipc.close"/>"><isa:translate key="ipc.close"/></a><%
	%></center><%
%></body><%
%></html>
