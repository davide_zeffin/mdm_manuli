<%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblCsticUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/comparisonTblCstic.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
ComparisonTblCsticUI.IncludeParams includeParams = ComparisonTblCsticUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
CharacteristicDeltaUIBean cstic = includeParams.getCharacteristicDeltaBean();
Hashtable customerParams = includeParams.getCustomerParams();
List values = cstic.getValues();
boolean csticNameDisplayed = false;
// loop over value deltas
for (int m=0; m<values.size(); m++){ // id=5
    ValueDeltaUIBean value = (ValueDeltaUIBean) values.get(m);
    StatusImage img = value.getStatusImage();

	%><tr><%
	    if (!csticNameDisplayed){
	        csticNameDisplayed = true;
	    %><td class="configObjects" title="<isa:translate key="ipc.compare.tt.cstic" />"><%
	        %><%= cstic.getLanguageDependentName() %><%
	    %></td><%
	    }
	    else {
	    %><td class="configObjects" title="<isa:translate key="ipc.compare.tt.cstic.delta" arg0="<%= cstic.getLanguageDependentName() %>"/>"><%
	        %>&nbsp;<%
	    %></td><%               
	    }
	    %><td class="statusIcon" title="<isa:translate key="ipc.compare.tt.icon" />"><%
	        // If a single-valued cstic has loading messages and a value-delta. We replace
	        // the status image of the delta by the status image of the loading message.
	        if (!cstic.allowsMultipleValues() && cstic.getLoadingMessages().size()>0){
	            List loadingMessages = cstic.getLoadingMessages();
	            LoadingMessageUIBean msg = (LoadingMessageUIBean) loadingMessages.get(0);
	            img = msg.getStatusImage();
	        }		            
	        %><img style="display:inline"  src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
	    %></td><%
	    %><td><%
	        %><%= value.getFormatStartTagName1() %><%
	        	%><%= value.getLanguageDependentName(pageContext) %><%
	        %><%= value.getFormatStartTagName1() %><%
	    %></td><%
	    %><td><%
	    	%><%= value.getFormatStartTagName2() %><%
	            %><%= value.getLanguageDependentName2(pageContext) %><%
	        %><%= value.getFormatStartTagName2() %><%	                
	    %></td><%
	    %><td><%
	    	String remark = "";
	    	if (!value.getRemark().equals("")){
	    	    remark = WebUtil.translate(pageContext, value.getRemark(), null);
	    	}
	        %><%= remark %><%
	        // If a single-valued cstic has loading messages and a value-delta. The first
	        // loading message should be displayed in the same line as the value delta.
	        if (!cstic.allowsMultipleValues() && cstic.getLoadingMessages().size()>0){
	            List loadingMessages = cstic.getLoadingMessages();
	            LoadingMessageUIBean msg = (LoadingMessageUIBean) loadingMessages.get(0);
	            img = msg.getStatusImage();
	        %>&nbsp;<img style="display:inline"  align="top" src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
	        %>&nbsp;<%
	        %><%= msg.getText() %><%		                    
	        }
	    %></td><%
	%></tr><%                   
} // id=5
        
// check for loading messages
List loadingMessages = cstic.getLoadingMessages();
for (int m=0;m<loadingMessages.size(); m++){ // id=6
    LoadingMessageUIBean msg = (LoadingMessageUIBean) loadingMessages.get(m);
    StatusImage img = msg.getStatusImage();
    // If a single-valued cstic has loading messages and a value-delta. The first
    // loading message has already been displayed.
    // So we skip it here.
    if (!cstic.allowsMultipleValues() && cstic.getValues().size()>0){
        continue;
    }
	%><tr><%
	    if (!csticNameDisplayed){
	        csticNameDisplayed = true;
	    %><td class="configObjects" title="<isa:translate key="ipc.compare.tt.cstic" />"><%
	        %><%= cstic.getLanguageDependentName() %><%
	    %></td><%
	    }
	    else {
	    %><td class="configObjects" title="<isa:translate key="ipc.compare.tt.cstic.loadingmsg" arg0="<%=cstic.getLanguageDependentName()%>"/>"><%
	        %>&nbsp;<%
	    %></td><%               
	    }
	    %><td class="statusIcon" title="<isa:translate key="ipc.compare.tt.icon" />"><%
	        %><img style="display:inline"  src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
	    %></td><%
	    %><td><%
	        %>&nbsp;<%
	    %></td><%
	    %><td><%
	        %>&nbsp;<%
	    %></td><%
	    %><td title="<isa:translate key="ipc.compare.tt.cstic.loadingmsg" arg0="<%= cstic.getLanguageDependentName()%>" />"><%
	        %><img style="display:inline"  align="top" src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
	        %>&nbsp;<%
	        %><%= msg.getText() %><%
	    %></td><%
	%></tr><%                       
} // id=6 
%>