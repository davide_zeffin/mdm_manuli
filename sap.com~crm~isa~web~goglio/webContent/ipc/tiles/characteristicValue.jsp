<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MimeObjectUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValueUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIElement"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ValueUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.MimeUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "java.util.ArrayList"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><% 
%><isa:moduleName name = "/ipc/tiles/characteristicValue.jsp"/><% 
BaseUI ui = new BaseUI(pageContext);
//read parameter passed from caller
CharacteristicValueUI.IncludeParams includeParams = CharacteristicValueUI.getIncludeParams(pageContext);
InstanceUIBean currentInstance = includeParams.getInstance();
GroupUIBean currentGroup = includeParams.getGroup();
CharacteristicUIBean currentCharacteristic = includeParams.getCharacteristic();
ValueUIBean characteristicValue = includeParams.getValue();
UIContext uiContext = includeParams.getIpcBaseUI();
boolean showDescriptions = includeParams.isShowDescriptions();
UIArea csticsArea = includeParams.getUiArea();
boolean readOnlyMode = includeParams.isReadOnlyMode();
boolean detailsMode = includeParams.isDetailsMode();
Hashtable customerParams = includeParams.getCustomerParams();
String descr = "";
if (detailsMode){
    // in the characterstic details display we want to show the whole description
	descr = characteristicValue.getDescription();
}
else {
	// if we are not in details-mode we show the description length-limited
    descr = characteristicValue.getLengthLimitedDescription();
}

// style classes
String cssTable = "ipcValueSingleOption";
String cssTR = "ipcValue";
String cssTD = "ipcValue";

//local variables 
ArrayList mimes = characteristicValue.getMimes();
// This include contains the UI of one characteristic value
// assigned to one characteristic
// using the object "characteristicValue" you can get all the
// information about the value
// using the object "characteristic" you can get all the
// information about the characteristic

// Highlight the complete value block onMousOver:
// Generate the name for the onMouseOver-highlighting link.
// That has to be used for the onMouseOver-event of the label of the input-tag (because moving the mouse over the text didn't highlight)
String hoverName = characteristicValue.getHighlightingName();
%><a href="#" title="<isa:translate key="ipc.csticvalue.effect"/>" name="<%= hoverName%>" style="color: #000; text-decoration: none;"><%
%><table width="100%" class="<%= cssTable %>"><%
	%><tr class="<%= cssTR %>"><%
		%><td class="<%= cssTD %>"><%
			%><table class="<%= cssTable %>" width="100%"><%
				if ((showDescriptions == true) && ((descr != null) && (descr.length() > 0))) {
				%><colgroup><%
					%><col width="50%"><% // image, language dependent text, units, ?...
					%><col width="50%"><% // descriptions 
				%></colgroup><%
				}
				%><tr class="<%= cssTR %>"><%
					%><td class="<%= cssTD %>"><%
						%><table class="<%= cssTable %>"><%
							%><tr class="<%= cssTR %>"><%                            
								if (CharacteristicValueUI.indentValues(currentGroup, currentCharacteristic)) {
								//the width should be higher than the width of the picture in mimeObject.jsp
								%><td width="80" class="<%= cssTD %>"><%                                
									// get the appropriate mime for the work-area: icon, o2c, sound (see ValueUIBean)
									MimeUIBean mime = characteristicValue.getMimeForWorkArea();
									// get the enlarged Mime for this value
									MimeUIBean enlargedMime = characteristicValue.getEnlargedMime();
									// include mimeObject.jsp only if mime is not null-object
									if (mime != MimeUIBean.C_NULL) {
										MimeObjectUI.include(pageContext,
																	   uiContext.getJSPInclude("tiles.mimeObject.jsp"),
																	   uiContext,
																	   mimes,
																	   mime,
																	   enlargedMime,
																	   currentInstance,
																	   currentGroup,
																	   customerParams);
									}
								%></td><%
								}
								// overwrite css if value is system assigned
								if (characteristicValue.isAssignedBySystem()){
									cssTD = "systemAssigned";
								}
								%><td class="<%= cssTD %>"><%

								String idForValue = "";
								String titleForInput = "";
								String enabledString = "";
								String descText = "";
								if (showDescriptions)
									descText = characteristicValue.getDescription();
								// INPUT TYPE (RADIO -> select single values or CHECKBOX -> select multiple values)
								{ // lokal variables for input field
									String inputType = "";
                                    
									String currentCsticName = currentCharacteristic.getName();
									String name = new StringBuffer(currentCsticName).insert(0, "characteristic_").toString();
									idForValue = name;
									String value = StringUtil.convertStringToHtml(characteristicValue.getName());
									idForValue = idForValue + "_" + value;
									String isChecked = "";
									String isDisabled = "";
									StringBuffer actionBuffer = new StringBuffer().append(WebUtil.getAppsURL(
																						  pageContext,
																						  null,
																						  "/ipc/dispatchSetValuesOnCaller.do",
																						  null,
																						  null,
																						  false));
									String action = actionBuffer.toString();
									String instanceId = currentInstance.getId();
									String groupName = currentGroup.getName();

									if (currentCharacteristic.allowsMultipleValues()) {
										inputType = "checkbox";
									}
									else {
										inputType = "radio";
									}

									// if the characteristicValue is selected it should be shown as checked
									if (characteristicValue.isSelected()) {
										isChecked = "checked";
									}

									// The characteristic value must not be changed by the user if:
									//   1. The characteristicValue is assigned by the system,
									//      because otherwise it would cause an inconsistency
									//   2. The configuration is shown in display mode
									//   3. The characteristic value is not assignable
									if (characteristicValue.isAssignedBySystem() 
											|| uiContext.getDisplayMode()
											|| readOnlyMode
											|| currentCharacteristic.isReadOnly()
											|| !characteristicValue.isAssignable()) {
										isDisabled = "disabled";
										enabledString = WebUtil.translate(pageContext, "access.unavailable", null);
									}

									titleForInput = WebUtil.translate(pageContext, "access.input.help", new String[] { name, enabledString, "" , "" , descText } );

									//we add the onClick event to cause the setValueAction in the form "currentForm".
									//   To do it we need to change the action of the form
									//   note:  only if you are working with frames you should send the name of your topframe
									//          in the request parameter TOPFRAME.
									//          When you select a value at presence all frames must be updated
									//          starting with the topframe.
									%><input id="<%= idForValue %>" type="<%= inputType %>" name="<%= name %>" value="<%= value %>" <%= isChecked %> <%= isDisabled %>
										title="<%=titleForInput%>"
										onMouseOver="javascript:hoverEffectOver('<%= hoverName%>');" onMouseOut="javascript:hoverEffectOut('<%= hoverName%>');"
										onClick="javascript:onCsticValueChange('<%=instanceId %>', '<%=groupName %>', '<%=currentCsticName %>', '<%= action %>')" onkeypress="javascript:returnKeyForCurrentForm(event, '<%= action %>', '<%= instanceId %>', '<%= groupName %>' );" onFocus="onFocus('<%= currentCsticName%>')" tabindex="<%= csticsArea.getTabIndex()%>"><%
								}
								%></td><%
								//language dependent name of the value as text in the html page
								%><td class="<%= cssTD %>"><%
									%><label for="<%=idForValue%>" onMouseOver="javascript:hoverEffectOver('<%= hoverName%>');" onMouseOut="javascript:hoverEffectOut('<%= hoverName%>');"><%
									%><%= characteristicValue.getDisplayName() %><%
									if (uiContext.getShowOneToOneConditions()
											&& characteristicValue.getPrice() != null
											&& !characteristicValue.getPrice().equals("")) {
									%>&nbsp;(<isa:translate key="ipc.surcharge"/> <%= characteristicValue.getPrice() %>)<%
									}
									// link additional mime of value
									if (characteristicValue.hasAdditionalMimes() && uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_LINK_TO_ADD_VALUE_MIMES)) {
										String href = characteristicValue.getLinkToAdditionalMime();
										String target = "_blank";
										String masterdataURL = uiContext.getMasterdataMimesURL();
										String mimeURL = MasterdataUtil.getMimeURL(pageContext, masterdataURL, null, null, href);
                                        
									%>&nbsp;<a href="<%= mimeURL %>" target="<%= target %>" title="<isa:translate key = "ipc.cstic.value.link.mime"/>"><%
										%><isa:translate key = "ipc.cstic.value.link.mime"/><%
									%></a><%
									}
								%></label></td><%
							%></tr><%
						%></table><%
					%></td><%
					if (showDescriptions == true){
						// only add the following coding if there is a description
                     	if ((descr != null) && (descr.length() > 0)) {
							%><td class="<%= cssTD %>"><%
								%><table class="<%= cssTable %>"><%
									%><tr class="<%= cssTR %>"><%
										%><td class="<%= cssTD %>"><%                        
											%><%= descr %><%
										%></td><%
									%></tr><%
								%></table><%
							%></td><%
	                    }
					}
				%></tr><%
			%></table><%
		%></td><%
	%></tr><%
%></table><%
//close: highlight the complete value block
%></a>
