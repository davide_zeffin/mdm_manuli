<%-- this include contains the UI of the ConflictSolver --%>
<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConflictUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/conflictSolver.jsp"/><%
//TODO: remove access to business objects
%><%@ page import = "com.sap.spc.remote.client.object.Conflict"%><%

%><table class="ipcConflictSolverInner" border="0" cellpadding="0" cellspacing="0" width="100%"><%
    %><tr><%
        %><td><%
            UIContext uiContext = (UIContext)request.getAttribute("uiContext");
            request.removeAttribute("uiContext");
            List conflictSolverConflictsList = (List) request.getAttribute( RequestParameterConstants.CONFLICTS );
            request.removeAttribute(RequestParameterConstants.CONFLICTS);
            Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);
            if (conflictSolverConflictsList.isEmpty()) {
            %><table class="ipcStatusBar"><%
                %><tr><%
                    %><td><%
                        %><isa:translate key = "ipc.conflict.without.deselect1"/><%
                        %><ol><%
                            %><isa:translate key = "ipc.conflict.without.deselect2"/><%
                            %><li><%
                                %><isa:translate key = "ipc.conflict.without.deselect3"/><%
                        %></ol><%
                    %></td><%
                %></tr><%
            %></table><%
            }
        %></td><%
    %></tr><%
    Iterator conflictSolverConflicts = conflictSolverConflictsList.iterator();

    while (conflictSolverConflicts.hasNext()) {
        Conflict conflict = (Conflict) conflictSolverConflicts.next();      
        // we include the page "conflict.jsp"
        // which shows the layout of one single conflict.
        // Most possibly you would like to change the layout of one single conflict.
        // To do this please copy the file conflict.jsp, give this file another name,
        // make your layout changes there and include your file here instead of "conflict.jsp".
        // Avoid to make any changes to the file "conflict.jsp" itself in order to keep your
        // changes when receiving a new update of the application. 
    %><tr><%
        %><td><%
            %>&nbsp;<%
        %></td><%
    %></tr><%
    %><tr><%
        %><td align="left"><%
            ConflictUI.include(pageContext,
                                   uiContext.getJSPInclude("tiles.conflict.jsp"),
                                   uiContext,
                                   conflict,
                                   customerParams);
        %></td><%
    %></tr><%
    }
%></table>