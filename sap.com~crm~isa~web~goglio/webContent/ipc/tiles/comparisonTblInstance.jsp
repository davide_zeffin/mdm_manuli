<%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "java.util.ArrayList"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblGroupUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblInstanceUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/comparisonTblInstance.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
ComparisonTblInstanceUI.IncludeParams includeParams = ComparisonTblInstanceUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
InstanceDeltaUIBean inst = includeParams.getInstanceDeltaBean();
Hashtable customerParams = includeParams.getCustomerParams();
boolean instNameDisplayed = false;
StatusImage img = inst.getStatusImage();   

// check for loading messages
List messages = inst.getLoadingMessages();
for (int j=0; j<messages.size(); j++){ // id=2
    LoadingMessageUIBean msg = (LoadingMessageUIBean)messages.get(j);
    img = msg.getStatusImage();
	// check for old values (e.g. message no. 241: deleted cstic that had values assigned)
    String oldValues = "";
	ArrayList oldValuesList = msg.getOldValues();
	if ((oldValuesList != null) && (oldValuesList.size() > 0)){
	    for (int i=0; i<oldValuesList.size(); i++){
	        if (i > 0) {
				// add <br> tag before, if it is not the first value
				oldValues = oldValues + "<br>";	            
	        }
	        oldValues = oldValues + oldValuesList.get(i);
	    }
	}
	else {
	    // just add space
	    oldValues = "&nbsp";
	}
		                
	%><tr><%
		if(!instNameDisplayed){
		    instNameDisplayed = true;
	    %><td class="configObjectsProduct" title="<isa:translate key="ipc.compare.tt.instance" />"><%
	        %><%= inst.getLanguageDependentName() %><%
	    %></td><%
		}
		else {
	    %><td class="configObjects" title="<isa:translate key="ipc.compare.tt.inst.loadingmsg" arg0="<%= inst.getLanguageDependentName()%>" />"><%
	        %>&nbsp;<%
	    %></td><%  
		}
	    %><td class="statusIcon" title="<isa:translate key="ipc.compare.tt.icon.loadingmessage" />"><%
	        %><img src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
	    %></td><%
	    %><td><%
	        %><%= oldValues %><%
	    %></td><%
	    %><td><%
	        %>&nbsp;<%
	    %></td><%
	    %><td title="<isa:translate key="ipc.compare.tt.inst.loadingmsg" arg0="<%= inst.getLanguageDependentName() %>"/>"><%
	        %><img style="display:inline"  align="top" src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
	        %>&nbsp;<%
	        %><%= msg.getText() %><%
	    %></td><%
	%></tr><%                   
} // id=2
	
// we have to add the line for the instance name only if it has not been displayed 
// already (e.g. for loading messages)
if(!instNameDisplayed){ // id=9
    instNameDisplayed = true;
	%><tr><%
	    %><td class="configObjectsProduct" title="<isa:translate key="ipc.compare.tt.instance" />"><%
	        %><%= inst.getLanguageDependentName() %><%
	    %></td><%
	    %><td class="statusIcon"><%
	    if (img.getImagePath().equals("")){
	        %>&nbsp;<%		                
	    }
	    else {
	        %><img src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
	    }
	    %></td><%
	    %><td><%
	        %>&nbsp;<%
	    %></td><%
	    %><td><%
	        %>&nbsp;<%
	    %></td><%
	    %><td><%
	    	String remark = "";
	    	if (!inst.getRemark().equals("")){
	    	    remark = WebUtil.translate(pageContext, inst.getRemark(), null);
	    	}
	        %><%= remark %><%
	    %></td><%
	%></tr><%           
} // id=9

// loop over groups
List groups = inst.getGroups();
for (int j=0; j<groups.size(); j++){ // id=3
    GroupDeltaUIBean grp = (GroupDeltaUIBean)groups.get(j);
	ComparisonTblGroupUI.include(pageContext,
	                       uiContext.getJSPInclude("tiles.comparisonTblGroup.jsp"),
	                       uiContext,
	                       grp,
	                       customerParams);
} // id=3
%>