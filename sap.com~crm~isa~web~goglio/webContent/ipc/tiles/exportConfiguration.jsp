<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "javax.servlet.ServletOutputStream"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.StringUtil"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/exportConfiguration.jsp"/><%

//Use SingleThreadModel until the application is checked for Threadsafeness
%><%@ page isThreadSafe = "true"%><%

UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
String exportData = (String)uiContext.getProperty(RequestParameterConstants.EXPORT_DATA);
String fileName = (String)uiContext.getProperty(RequestParameterConstants.EXPORT_DATA_FILENAME);
response.setContentType("application/download");
response.setHeader("Content-Disposition", "attachment; filename="+ fileName);
ServletOutputStream sOS = response.getOutputStream();
sOS.print(exportData);
sOS.close();
uiContext.removeProperty(RequestParameterConstants.EXPORT_DATA);
%>