<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CurrentConfigurationUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConditionsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ValueUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConditionUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.container.ConditionUIBeanSet"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/currentConfiguration.jsp"/><%

BaseUI ui = new BaseUI(pageContext);
//get the parameter from the caller
CurrentConfigurationUI.IncludeParams includeParams = CurrentConfigurationUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
boolean fromConflicts = includeParams.isFromConflicts();
ConfigUIBean configuration = includeParams.getConfig();
List instances = configuration.getInstances();
InstanceUIBean currentInstance = includeParams.getInstance();
GroupUIBean currentGroup = includeParams.getGroup();
CharacteristicUIBean currentCharacteristic = includeParams.getCharacteristic();
Hashtable customerParams = includeParams.getCustomerParams();

Hashtable expandedInstances = (Hashtable) uiContext.getProperty(InternalRequestParameterConstants.CUSTOMIZATIONLIST_EXPANDED_INSTANCES);
UIArea mfaArea = new UIArea("mfa", uiContext);
%><input type="hidden" name="<%= InternalRequestParameterConstants.CUSTOMIZATIONLIST_EXPANDED_INSTANCES%>" value=""/><%
%><center><%
%><table><tr><td><%
%><table class="ipcAreaFrame" cellpadding="0" cellspacing="0"><%
    %><tr><%
        %><td align="left"><%
            %><strong><isa:translate key="ipc.customization"/><%
            %></strong><%
            %><p></p><%
                // if no values are set display a message to the customer
                if (!configuration.hasAssignedValues()) {
                %><isa:translate key="ipc.no_values"/><%
                }else { // (id = -1)
				%>
				<%
				if (ui.isAccessible())
				{
					String treeTxt = WebUtil.translate(pageContext, "ipc.customization", null);
				%>
			<a 
				href="#tree_instances-end"
				id="tree_instances-begin"
				title="<isa:translate key="access.tree.begin" arg0="<%=treeTxt%>"/>" 
			></a>
				<%
				}
				%>
				<ul><% // open an unordered list for the instances 
                    for (Iterator instIt = instances.iterator(); instIt.hasNext(); ) { // (id=0)
	                %><% // list entry for instance
                        InstanceUIBean loopedInstance = (InstanceUIBean)instIt.next();
						boolean instanceIsExpanded = expandedInstances.get(loopedInstance.getId()) != null;
	                        { //localize variables (id=6)
	                            String action = "";
                                action = WebUtil.getAppsURL(pageContext, null, "/ipc/expandCustomizationListInstance.do", null, null, false);
	                            String loopedInstanceId = loopedInstance.getId();
	                            String currentInstanceId, currentGroupName, currentCharName;
	                            if (currentInstance != null) currentInstanceId = currentInstance.getId(); else currentInstanceId = "";
	                            if (currentGroup != null) currentGroupName = currentGroup.getName(); else currentGroupName = "";
	                            if (currentCharacteristic != null) currentCharName = currentCharacteristic.getName(); else currentCharName = "";
	                    %>
	                    <li
	                    <%
	                    if (ui.isAccessible() && instanceIsExpanded)
	                    {
	                    	String levelTxt = WebUtil.translate(pageContext, "ipc.config.level.instance", new String[]{loopedInstance.getLanguageDependentName()});
	                    %>
	                    title="<isa:translate key="access.tree.open" arg0="<%=levelTxt%>"/>"
	                    <%
	                    }
	                    else
	                    if (ui.isAccessible())
	                    {
	                    	String levelTxt = WebUtil.translate(pageContext, "ipc.config.level.instance", new String[]{loopedInstance.getLanguageDependentName()});
	                    %>
	                    title="<isa:translate key="access.tree.closed" arg0="<%=levelTxt%>"/>"
	                    <%
	                    }
	                    %>
	                    >
	                    	<a href="javascript:expandInstanceInCustomizationList('<%= loopedInstanceId %>',<%
	                                                                          %>'<%= currentInstanceId %>',<%
	                                                                          %>'<%= currentGroupName %>',<%
								                                              %>'<%= currentCharName %>',<%
	                                                                          %>'<%= action %>');" tabindex="<%= mfaArea.getTabIndex()%>" title="<%= loopedInstance.getLanguageDependentName() %>"><%
	                        %><strong><%
	                            %><%= loopedInstance.getLanguageDependentName() %><%
	                        %></strong><%
	                    %></a><%
	                        } // (id=6)
	                    	ConditionUIBeanSet conditions = loopedInstance.getConditions();
							if (!conditions.isEmpty()) {
	                    	    ConditionsUI.include(pageContext,
	                    	                         uiContext.getJSPInclude("tiles.conditions.jsp"),
	                    	                         uiContext,
	                    	                         conditions,
	                    	                         customerParams);
							}
						// check if the instance is collapsed
						if (instanceIsExpanded && loopedInstance.hasAssignedValues()) { // (id=5)
	                   	%><ul><%-- open list for characteristic groups --%><%
	                    	for (Iterator groupIt = loopedInstance.getGroups().iterator(); groupIt.hasNext(); ) { //(id=5.1)
	                    		GroupUIBean group = (GroupUIBean)groupIt.next();
	                    		if (group.hasAssignedValues()) { // (id=5.2)
	                   		%><li
	                    <%
	                    if (ui.isAccessible())
	                    {
	                    	String levelTxt = WebUtil.translate(pageContext, "ipc.config.level.group", new String[]{group.getLanguageDependentName(pageContext)});
	                    %>
	                    title="<isa:translate key="ipc.tree.open" arg0="<%=levelTxt%>"/>"
	                    <%
	                    }
	                    %>
	                   		><%
	                   		    %><%= group.getLanguageDependentName(pageContext) %><%
   		                        // iterate over all characteristics with assigned values
		                        %><ul> <%-- open list for characteristics --%><%
   		                            for (Iterator characteristicsIt = group.getCharacteristics().iterator();characteristicsIt.hasNext();) { // (id=1)
		                                CharacteristicUIBean cstic = (CharacteristicUIBean) characteristicsIt.next();
		                                if (cstic.isVisible() && cstic.hasAssignedValues()) {
		                            // list entry for characteristic
		                            %><li
	                    <%
	                    if (ui.isAccessible())
	                    {
	                    	String levelTxt = WebUtil.translate(pageContext, "ipc.config.level.cstic", new String[]{cstic.getLanguageDependentName()});
	                    %>
	                    title="<isa:translate key="ipc.tree.open" arg0="<%=levelTxt%>"/>"
	                    <%
	                    }
	                    %>
		                            ><%
		                            %><%= cstic.getLanguageDependentName() %><%
		                                %><ul><% // open list for characteristic values 
		                                    Iterator assignedValues = ((List)cstic.getAssignedValues()).iterator();
		                                    while (assignedValues.hasNext()) { // (id=4)
		                                        ValueUIBean value = (ValueUIBean) assignedValues.next();
		                                    // list entry for characteristic value
		                                    %><li
	                    <%
	                    if (ui.isAccessible())
	                    {
	                    	String levelTxt = WebUtil.translate(pageContext, "ipc.config.level.value", new String[]{value.getLanguageDependentName()});
	                    %>
	                    title="<isa:translate key="access.tree.leaf" arg0="<%=levelTxt%>"/>"
	                    <%
	                    }
	                    %>
		                                    ><%
		                                        %><%= value.getLanguageDependentName() %><%
		                                        ConditionUIBean varcond = value.getVariantCondition();
		                                        if (varcond != null) {
													%><br><%
													%><table class="conditions"><%
														%><tr class="conditions"><%
															%><td class="conditions_description"><%
																%><%= varcond.getDescription() %><%
															%></td><%
															%><td class="conditions_value"><%
																%><%= varcond.getValue() %><%
															%></td><%
															%><td class="conditions_currency"><%
																%><%= varcond.getDocumentCurrency() %><%
															%></td><%
														%></tr><%
													%></table><%
		                                        }
		                                    %></li><%
		                                   } // (id=4)
		                                %></ul><% // close the value list
		                            %></li><%
		                                }
		                            } // (id=1)
		                        %></ul><% // close the characteristic list
		                    %></li><% //close the group list entry
	                    		} // (id=5.2)
	                    	} // (id=5.1)
	                    %></ul><% // close the group list
	                    } // (id=5)
		            %></li><% // close the instance list entry
                    } // (id=0)
				%></ul><% // close the instance list
                } // (id = -1)
        %>
				<%
				if (ui.isAccessible())
				{
					String treeTxt = WebUtil.translate(pageContext, "ipc.customization", null);
				%>
			<a 
				href="#tree_instances-begin"
				id="tree_instances-end"
				title="<isa:translate key="access.tree.end" arg0="<%=treeTxt%>"/>" 
			></a>
				<%
				}
				%>
        </td><%
    %></tr><%
    %><tr><%
        %><td><%
			ConditionUIBeanSet conditions = configuration.getConditions();
			if (!conditions.isEmpty()) {
				%><p><isa:translate key="ipc.conditions.total"/><%
				ConditionsUI.include(pageContext,
				                     uiContext.getJSPInclude("tiles.conditions.jsp"),
				                     uiContext,
				                     conditions,
				                     customerParams);
			}
        %></td><%
    %></tr><%
%></table> <%-- close the instance list --%>
</td></tr></table><%
%></center><%
%><%
	if (ui.isAccessible())
	{
		String tabTxt = WebUtil.translate(pageContext, "ipc.mfa.tabtitle", null); 
%><%
%><a 
	href="#tabmfa-begin"
	id="tabmfa-end"
	title="<isa:translate key="access.tab.end" arg0="<%=tabTxt%>"/>" 
></a><%
%><%
	}
%>
