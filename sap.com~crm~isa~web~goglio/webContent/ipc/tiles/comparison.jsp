<%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblGenLoadMsgUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblHeaderUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblInstanceUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MessagesUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/comparison.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
ComparisonUI.IncludeParams includeParams = ComparisonUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
ComparisonResultUIBean resultBean = includeParams.getResultBean();
Hashtable customerParams = includeParams.getCustomerParams();

%><input type="hidden" name="<%= Constants.COMPARISON_SHOW_ALL %>"><%
%><input type="hidden" name="<%= Constants.COMPARISON_FILTER_FLAG %>"><%
%><center><%
    %><br /><%

%><table class="ipcConfigComparisonHidden"><%
	if (!resultBean.hasAnyValueDeltas() && !resultBean.hasAnyLoadingMessages()) { // id=0
	%><tr><%
		%><td class="messageRow"><%
			%><isa:translate key="ipc.message.compare.no.diff" /><%
		%></td><%
	%></tr><%
	} // id=0
	// if we are in the "compare to stored" display we want to show also messages (if available)
	if (uiContext.getPropertyAsString(InternalRequestParameterConstants.COMPARISON_ACTION).equals(InternalRequestParameterConstants.COMPARISON_ACTION_STORED)) { // id=15
		// check wether there are message that should be displayed (e.g. "New KB has been applied")
		List messages = (List) uiContext.getProperty(InternalRequestParameterConstants.MESSAGES);
		if (messages != null && messages.size() > 0){ // id=16
	%><tr><%	
		%><td class="messageRow"><%	    
			for (int i=0; i<messages.size(); i++){
				Object message = messages.get(i);
				if (message instanceof MessageUIBean) {
					MessageUIBean messageUIBean = (MessageUIBean)message;
					MessagesUI.include(
					    pageContext,
					    uiContext.getJSPInclude("tiles.messages.jsp"),
					    uiContext,
					    messageUIBean,
					    customerParams
					);
				}		
			}
		%></td><%		
	%></tr><%		
		}  // id=16
	} // id=15
    if (resultBean.getInstanceDeltas().size()>0 || resultBean.hasAnyLoadingMessages()) { // id=7	
	%><tr><%
        String action = WebUtil.getAppsURL(pageContext,
                                        null,
                                        "/ipc/refreshComparisonDisplay.do",
                                        null,
                                        null,
                                        false);
        String filterLinkText = ComparisonUI.getFilterLinkText(pageContext, uiContext);
		String filterFlag = ComparisonUI.getInverseFilterFlag(uiContext);
        String showAllFlag = ComparisonUI.getInverseShowAllFlag(uiContext);
        String showAllLinkText = ComparisonUI.getShowAllLinkText(pageContext, uiContext);        
		%><td class="featureRow"><%
			// show the filter-link only if there are loading messages
			if (resultBean.hasAnyLoadingMessages()){
				%><a href="javascript:changeFilter('<%= filterFlag %>', '<%=action %>');" title="<%= filterLinkText %>"><%
					%><%= filterLinkText %><%
				%></a><%
			}
			// display the separator only if both links are displayed
			if (resultBean.hasAnyLoadingMessages() && !uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.COMPARISON_FILTER)){
				%>&nbsp;|&nbsp;<%
			}
			// don't display the showAll/showDifferences-link if the filter is set
			if (!uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.COMPARISON_FILTER)){
				%><a href="javascript:changeShowAll('<%= showAllFlag %>', '<%=action %>');" title="<%= showAllLinkText %>"><%
					%><%= showAllLinkText %><%
				%></a><%
			}			
		%></td><%
	%></tr><%
	%><tr><%
		%><td><%
			String tableSummary = "";
			String tableTitle = WebUtil.translate(pageContext, "ipc.compare.title", null);
			// for performance reasons we calculate the table-summary only in accessibility mode
			if (ui.isAccessible()){ //id=13
			    String[] tableInfo = ComparisonUI.getTableInfo(resultBean);
				String rowNumber = tableInfo[0];
				String columnNumber = tableInfo[1];
				String firstRow = tableInfo[2];
				String lastRow = tableInfo[3];			    
				tableSummary = WebUtil.translate(pageContext, "access.table.summary", new String[]{tableTitle, rowNumber, columnNumber, firstRow, lastRow});			    
				%><a href="#itemstableend" 
					id="itemstablebegin"
					title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>"><%
				%></a><%
			} // id=13
		    %><table class="ipcConfigComparison" title="<%= tableSummary%>"><%
		    	// include the table header
				ComparisonTblHeaderUI.include(pageContext,
				                       uiContext.getJSPInclude("tiles.comparisonTblHeader.jsp"),
				                       uiContext,
				                       resultBean,
				                       customerParams);
				// loop over general loading messages
				List generalLoadingMessages = resultBean.getLoadingMessages();
    		    boolean titleDisplayed = false;
				for (int i=0; i<generalLoadingMessages.size(); i++){ // id=10
                    LoadingMessageUIBean msg = (LoadingMessageUIBean)generalLoadingMessages.get(i);
					ComparisonTblGenLoadMsgUI.include(pageContext,
					                       uiContext.getJSPInclude("tiles.comparisonTblGenLoadMsg.jsp"),
					                       uiContext,
					                       msg,
					                       titleDisplayed,
					                       customerParams);
					// the title should only be displayed once, so we set the flag to true
					titleDisplayed = true;
					// add an empty line after the last general loading message
					if(i == generalLoadingMessages.size()-1){ // id=11
						%><jsp:include page="/ipc/tiles/comparisonTblSeparatorRow.jsp" flush="true"/><%
					} // id=11
				} // id=10
				// loop over instance deltas		        
		        List instances = resultBean.getInstanceDeltas();
		        for (int i=0; i<instances.size(); i++){ // id=1
		            InstanceDeltaUIBean inst = (InstanceDeltaUIBean)instances.get(i);
					ComparisonTblInstanceUI.include(pageContext,
					                       uiContext.getJSPInclude("tiles.comparisonTblInstance.jsp"),
					                       uiContext,
					                       inst,
					                       customerParams);
					// add an empty line after each instance, but not if this is the last instance
					if(i != instances.size()-1){ // id=8
					    %><jsp:include page="/ipc/tiles/comparisonTblSeparatorRow.jsp" flush="true"/><%
					} // id=8					                       
		        } // id=1
		    %></table><%   
			if (ui.isAccessible()){ // id=14
				%><a id="itemstableend" 
					href="#itemstablebegin" 
					title="<isa:translate key="access.table.end" 
					arg0="<%=tableTitle%>"/>"></a><%
			} // id=14
		%></td><%
	%></tr><%
	} // id=7    	
%></table><%	
%></center>