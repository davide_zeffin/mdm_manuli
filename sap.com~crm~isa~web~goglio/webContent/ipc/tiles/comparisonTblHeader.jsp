<%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblHeaderUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/comparisonTblHeader.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
ComparisonTblHeaderUI.IncludeParams includeParams = ComparisonTblHeaderUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
ComparisonResultUIBean resultBean = includeParams.getResultBean();
Hashtable customerParams = includeParams.getCustomerParams();

%><tr><%
    %><th title="<isa:translate key="ipc.compare.tt.obj.names" />"><%
        %>&nbsp;<%
    %></th><%
    %><th class="statusIcon" title="<isa:translate key="ipc.compare.tt.icon" />"><%
        %>&nbsp;<%
    %></th><%
    %><th><%
    	String columnHeading = WebUtil.translate(pageContext, "ipc.compare.snap", null);
    	if (uiContext.getPropertyAsString(InternalRequestParameterConstants.COMPARISON_ACTION).equals(InternalRequestParameterConstants.COMPARISON_ACTION_STORED)) {
    	    // compare to stored
    	    columnHeading = WebUtil.translate(pageContext, "ipc.compare.stored", null);
    	}
        %>&nbsp;&nbsp;<%= columnHeading %>&nbsp;&nbsp;<%
    %></th><%
    %><th><%
        %>&nbsp;&nbsp;<isa:translate key="ipc.compare.current" />&nbsp;&nbsp;<%
    %></th><%
    %><th><%
        %><isa:translate key="ipc.compare.remark" /><%
    %></th><%
%></tr><%

// only show the apply-snapshot-link if it is a snapshot comparison
if (uiContext.getPropertyAsString(InternalRequestParameterConstants.COMPARISON_ACTION).equals(InternalRequestParameterConstants.COMPARISON_ACTION_SNAP)) { // id=12
%><tr><%
    %><th class="applySnapLine" title="<isa:translate key="ipc.compare.tt.obj.names"/>"><%
		%>&nbsp;<%
    %></th><%
    %><th class="statusIconApplySnapLine" title="<isa:translate key="ipc.compare.tt.icon" />"><%
        %>&nbsp;<%
    %></th><%
    %><th class="applySnapLine"><%
        String applySnapAction = WebUtil.getAppsURL(pageContext,
                null,
                "/ipc/applySnapshot.do",
                null,
                null,
                false);
		%><a href="javascript:applySnapshot('<%=applySnapAction %>');" title="<isa:translate key="ipc.compare.tt.snap.apply" />"><%
			%><isa:translate key="ipc.compare.snap.apply" /><%
		%></a><%		                
    %></th><%
    %><th class="applySnapLine"><%
        %>&nbsp;<%            
    %></th><%
    %><th class="applySnapLine"><%
        %>&nbsp;<%
    %></th><%
%></tr><%
} // id=12

%><tr><%
    %><th class="secondLine" title="<isa:translate key="ipc.compare.tt.obj.names"/>" ><%
        %>&nbsp;<%
    %></th><%
    %><th class="statusIconSecondLine" title="<isa:translate key="ipc.compare.tt.icon" />"><%
        %>&nbsp;<%
    %></th><%
    %><th class="secondLine"><%
    	String timeAndPrice1 = "";
    	if (uiContext.getPropertyAsString(InternalRequestParameterConstants.COMPARISON_ACTION).equals(InternalRequestParameterConstants.COMPARISON_ACTION_SNAP)) {
    	    // show time and price only if it is CompareToSnapshot
    	    timeAndPrice1 = resultBean.getSnap1Time() + ", " + resultBean.getSnap1Price();
    	}
        %>&nbsp;&nbsp;<%= timeAndPrice1 %>&nbsp;&nbsp;<%
    %></th><%
    %><th class="secondLine"><%
    	String timeAndPrice2 = "";
    	if (uiContext.getPropertyAsString(InternalRequestParameterConstants.COMPARISON_ACTION).equals(InternalRequestParameterConstants.COMPARISON_ACTION_SNAP)) {		            	    // show time and price only if it is CompareToSnapshot
    	    timeAndPrice2 = resultBean.getSnap2Time() + ", " + resultBean.getSnap2Price();
    	}
        %>&nbsp;&nbsp;<%= timeAndPrice2 %>&nbsp;&nbsp;<%            
    %></th><%
    %><th class="secondLine"><%
        %>&nbsp;<%
    %></th><%
%></tr>