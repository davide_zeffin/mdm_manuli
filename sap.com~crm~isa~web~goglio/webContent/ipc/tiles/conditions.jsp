<%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConditionsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConditionUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.container.ConditionUIBeanSet"%><%
%><isa:moduleName name = "/ipc/tiles/conditions.jsp"/><%
//read parameter from enclosing page
ConditionsUI.IncludeParams includeParams = ConditionsUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
ConditionUIBeanSet conditions = includeParams.getConditions();
Hashtable customerParams = includeParams.getCustomerParams();

%><table class="conditions"><%
for (Iterator it = conditions.getAll().iterator(); it.hasNext(); ) {
	%><tr class="conditions"><%
	ConditionUIBean condition = (ConditionUIBean)it.next();
		%><td class="conditions_description"><%
	        %><%= condition.getDescription() %><%
		%></td><%
		%><td class="conditions_value"><%
	        %><%= condition.getValue() %><%
		%></td><%
		%><td class="conditions_currency"><%
	        %><%= condition.getDocumentCurrency() %><%
	    %></td><%
	%></tr><%
}
%></table><%
%>