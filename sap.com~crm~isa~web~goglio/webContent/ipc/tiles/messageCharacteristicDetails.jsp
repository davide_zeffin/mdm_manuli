<%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.spc.remote.client.object.OriginalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.StringUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.*"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.util.WebUtil" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/messageCharacteristicDetails.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
MessageCharacteristicDetailsUI.IncludeParams includeParams = MessageCharacteristicDetailsUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
InstanceUIBean instance = includeParams.getInstance();
GroupUIBean characteristicGroup = includeParams.getCharGroup();
CharacteristicUIBean characteristic = includeParams.getCstic();
String valueName = includeParams.getValueName();
Hashtable customerParams = includeParams.getCustomerParams();
List values = characteristic.getAssignedValues();
ValueUIBean value = null;
// search for the right value
for (int i=0; i<values.size(); i++){
    value = (ValueUIBean) values.get(i);
    if (value.getName().equals(valueName)){
        break;
    }
}
UIArea csticsArea = new UIArea("cstics", uiContext);

String detailClass = "ipcDetailStatusbar";
String detailClassTooltipKey = "ipc.tt.cstic";
String grpBeginId = "group" + characteristic.getName() + "-begin";
String grpEndId = "group" + characteristic.getName() + "-end";
String grpBeginHref = "#" + grpBeginId;
String grpEndHref = "#" + grpEndId;
String grpHeader = characteristic.getLanguageDependentName();

String grpTxt = WebUtil.translate(pageContext, "ipc.grp.messagecstic", new String[] { grpHeader }); 
if (ui.isAccessible()){
%><a <%
	%> href="<%=grpEndHref%>" <%
	%> id="<%=grpBeginId%>"<%
	%> title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>" <%
%>></a><%
}
%><table class="<%= detailClass %>" title="<isa:translate key="<%=detailClassTooltipKey%>"/>" border="0" cellpadding="3" cellspacing="0" width="100%"><%
    %><tr><%
    	%><td class="messageIcon"><%
        	%><img style="display:inline" src="<isa:mimeURL name="<%= value.getStatusImage().getImagePath() %>" />" alt="<isa:translate key="<%= value.getStatusImage().getResourceKey() %>"/>"><%
        %></td><%		                
        %><td><%
            %><%=characteristic.getLanguageDependentName()%><%
        %></td><%
    %></tr><%
%></table><%
String messageCsticStyleClass = value.getMessageCsticStyleClass();
%><table border="0" width="100%" cellspacing="0" cellpadding="5px"><%
	%><tr><%
        %><td><%    
			%><table class="<%= messageCsticStyleClass %>" title="<isa:translate key="<%=characteristic.getTableStyleTooltip()%>"/>" border="0" width="100%" cellspacing="0"><%
				%><tr><%
					// if the value has a mime-object for the work area, we show it
					if (value.hasMimesForWorkArea()){ // id=01
					%><td width="80"><%                                
						// get the appropriate mime for the work-area: icon, o2c, sound (see ValueUIBean)
						MimeUIBean mime = value.getMimeForWorkArea();
						ArrayList mimes = value.getMimes();
						// get the enlarged Mime for this value
						MimeUIBean enlargedMime = value.getEnlargedMime();
						// include mimeObject.jsp only if mime is not null-object
						if (mime != MimeUIBean.C_NULL) {
							MimeObjectUI.include(pageContext,
														   uiContext.getJSPInclude("tiles.mimeObject.jsp"),
														   uiContext,
														   mimes,
														   mime,
														   enlargedMime,
														   instance,
														   characteristicGroup,
														   customerParams);
						}
					%></td><%
					} // id=01
					%><td><%
					// if enabled we show also the language independent name
					if (uiContext.getPropertyAsBoolean(OriginalRequestParameterConstants.MESSAGE_CSTICS_SHOW_ID)){
					    %>(<%= value.getName() %>)&nbsp;<%
					}
					%><%= value.getLanguageDependentName() %><%
					%></td><%
					%><td><%
					%>&nbsp;&nbsp;&nbsp;<%
					%></td><%
					%><td><%
					%><%= value.getDescription() %><%
					%></td><%
				%></tr><%
			%></table><%
        %></td><%
    %></tr><%
%></table><%			
if (ui.isAccessible()){
%><a <%
	%> href="<%=grpBeginHref%>"<% 
	%> id="<%=grpEndId%>" <%
	%> title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>" <%
%>></a><%
}%>