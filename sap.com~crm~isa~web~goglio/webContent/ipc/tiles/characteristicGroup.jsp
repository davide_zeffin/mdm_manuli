<%-- This include contains the UI of all available characteristic groups of one single instance --%>
<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicGroupsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/characteristicGroup.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
CharacteristicGroupsUI.IncludeParams includeParams = CharacteristicGroupsUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
InstanceUIBean currentInstance = includeParams.getInstance();
//List characteristicGroupsList = includeParams.getGroups();
GroupUIBean characteristicGroup = includeParams.getCharGroup();
Hashtable customerParams = includeParams.getCustomerParams();

UIArea csticsArea = new UIArea("cstics", uiContext);
String accessKey = csticsArea.getShortcutKey();

%><%            
	String grpBeginId = "group" + characteristicGroup.getName() + "-begin";
	String grpEndId = "group" + characteristicGroup.getName() + "-end";
	String grpBeginHref = "#" + grpBeginId;
	String grpEndHref = "#" + grpEndId;
	
	if (ui.isAccessible())
	{
%>
			<a 
				href="<%=grpEndHref%>"
				id="<%=grpBeginId%>"
				title="<isa:translate key="access.grp.begin" arg0="<%=characteristicGroup.getLanguageDependentName(pageContext)%>"/>" 
			></a>
<%
	}
%><table width="100%" class="ipcCharacteristicGroupTable"><%
    Hashtable groupStatus = (Hashtable) uiContext.getProperty( Constants.GROUP_STATUS_CHANGE );
    %><tr><%
        %><td class="ipcCharacteristicGroup"><%
            %><strong><%
                %><a href="#" accesskey="<isa:translate key="<%= accessKey %>"/>" title="<isa:translate key="ipc.area.cstics"/>" tabindex="<%= csticsArea.getTabIndex()%>"><%= characteristicGroup.getNameForWorkArea(pageContext) %></a><%
            %></strong><%
            %><%-- we include the page "characteristics.jsp" to show all characteristics
                   of the current characteristicGroup --%><%
                CharacteristicsUI.include(pageContext,
                                              uiContext.getJSPInclude("tiles.characteristics.jsp"),
                                              uiContext,
                                              currentInstance,
                                              characteristicGroup,
                                              csticsArea,
                                              customerParams);
        %></td><%
    %></tr><%
%></table><%
	if (ui.isAccessible())
	{
%>
<a 
	href="<%=grpBeginHref%>"
	id="<%=grpEndId%>"
	title="<isa:translate key="access.grp.end" arg0="<%=characteristicGroup.getLanguageDependentName(pageContext)%>"/>" 
></a>	
<%
	}
%><%-- here ends the layout of one single characteristic group --%>