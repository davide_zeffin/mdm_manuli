<%--    This include contains the UI of the conflict trace
         Using the object "conflictTrace" and conflictInfo you can get all the
         information about the conflict.--%>
<%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "org.apache.struts.util.MessageResources"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConflictTraceUI"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/conflictTrace.jsp"/><%

%><%@ page import = "com.sap.spc.remote.client.object.ConflictInfo"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%

BaseUI ui = new BaseUI(pageContext);
ConflictTraceUI.IncludeParams includeParams = ConflictTraceUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
List conflictTrace = includeParams.getConflictTrace();
Hashtable customerParams = includeParams.getCustomerParams();


%><table border="1" cellpadding="0" cellspacing="5" width="100%" class="conflictGroup"><%
    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.CONFLICT_SOLVER_SHOW_CONFLICT_SOLUTION_RATE)) {
    %><colgroup><%
        %><col width="15%"><%
        %><col width = "35%"><%
		%><col width="25%"><%
		%><col width = "25%"><%
    %></colgroup><%
    }
    %><tr><%
        %><td title="<isa:translate key = "ipc.conflict.deselect.option"/>" class="csticName"><%
            %><isa:translate key = "ipc.conflict.info.instanceName"/><%
        %></td><%
        %><td title="<isa:translate key = "ipc.conflict.deselect.option"/>" class="csticName"><%
            %><isa:translate key = "ipc.conflict.info.text"/><%
        %></td><%
		%><td title="<isa:translate key = "ipc.conflict.deselect.option"/>" class="csticName"><%
			%><isa:translate key = "ipc.conflict.info.conflictExplanation"/><%
		%></td><%
		%><td title="<isa:translate key = "ipc.conflict.deselect.option"/>" class="csticName"><%
			%><isa:translate key = "ipc.conflict.info.conflictDocumentation"/><%
		%></td><%
    %></tr><%
    Iterator conflictTraceIterator = conflictTrace.iterator();

    while (conflictTraceIterator.hasNext()) {
        ConflictInfo conflictInfo = (ConflictInfo) conflictTraceIterator.next();
    %><tr><%
        %><td><%
            %><%= conflictInfo.getConflictingInstanceName() %><%
        %></td><%
		%><td><%
        	%><%= conflictInfo.getConflictText() %><%
		%></td><%
        %><td><%
            %><%= conflictInfo.getConflictExplanation() %><%
        %></td><%
		%><td><%
			%><%= conflictInfo.getConflictDocumentation() %><%
		%></td><%
    %></tr><%
    }
%></table>
