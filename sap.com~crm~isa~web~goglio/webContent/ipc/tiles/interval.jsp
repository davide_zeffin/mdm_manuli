<%--
/**
 * INTERVAL DOMAIN<%
 * in case the characteristic has intervals as domain the intervals are displayed 
 * together with the other domain values
 **/
--%>
<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ValueUIBean" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/interval.jsp"/><%
//read passed variables from request
CharacteristicValuesUI.IncludeParams includeParams = CharacteristicValuesUI.getIncludeParams(pageContext);
CharacteristicUIBean currentCharacteristic = includeParams.getCurrentCharacteristic();
boolean readOnlyMode = includeParams.isReadOnlyMode();
Hashtable customerParams = includeParams.getCustomerParams();

boolean showInputField = CharacteristicValuesUI.showInputField(readOnlyMode, currentCharacteristic);

// we only show the interval if also an input-field is shown (otherwise it doesn't make sense)
if (showInputField) {
	String characteristicValuesAssignedValueName = "";
	if (currentCharacteristic.getAssignableValues().size() > 0){
	    for (Iterator it = currentCharacteristic.getAssignableValues().iterator(); it.hasNext();){
	        ValueUIBean value = (ValueUIBean)it.next();
	%><%= value.getDisplayName() %><%
	        if (it.hasNext()) {
	%>;&nbsp;<%
	        }
	    }
	}
}
%>