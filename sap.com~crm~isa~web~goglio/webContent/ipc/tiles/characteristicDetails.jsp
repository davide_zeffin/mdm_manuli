<%-- include import commands for jsp and page header for the html page --%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.StringUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesControllerUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import="com.sap.isa.core.util.WebUtil" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDetailsUI" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/characteristicDetails.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
CharacteristicDetailsUI.IncludeParams includeParams = CharacteristicDetailsUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
InstanceUIBean instance = includeParams.getInstance();
GroupUIBean characteristicGroup = includeParams.getCharGroup();
CharacteristicUIBean characteristic = includeParams.getCstic();
Hashtable customerParams = includeParams.getCustomerParams();
String descr = characteristic.getDescription();

UIArea csticsArea = new UIArea("cstics", uiContext);


String detailClass = "ipcDetailStatusbar";
String detailClassTooltipKey = "ipc.tt.cstic";
if (!uiContext.getShowStatusLights() && !characteristic.isConsistent()) {
    detailClass = "ipcConflictStatusbar";
	detailClassTooltipKey = "ipc.tt.cstic.conflict";
}
%><%            
	String grpBeginId = "group" + characteristic.getName() + "-begin";
	String grpEndId = "group" + characteristic.getName() + "-end";
	String grpBeginHref = "#" + grpBeginId;
	String grpEndHref = "#" + grpEndId;
	String grpHeader = characteristic.getDisplayName();
	
	String invisibleTxt = characteristic.isVisible() ? "" : WebUtil.translate(pageContext, "ipc.cstic.invisible", null);
	String hasUnitTxt = characteristic.hasUnit() ? WebUtil.translate(pageContext, "ipc.cstic.hasunit", null) : "";
	String requiredTxt = (!uiContext.getShowStatusLights() && characteristic.isRequired()) ? WebUtil.translate(pageContext, "ipc.cstic.required", null) : "";
	String grpTxt = WebUtil.translate(pageContext, "ipc.grp.cstic", new String[] { grpHeader , invisibleTxt, hasUnitTxt, requiredTxt}); 
	if (ui.isAccessible())
	{
%>
			<a 
				href="<%=grpEndHref%>"
				id="<%=grpBeginId%>"
				title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>" 
			></a>
<%
	}
%><div class="ipcStatusBarInCstic">
<table class="<%= detailClass %>" title="<isa:translate key="<%=detailClassTooltipKey%>"/>" border="0" cellpadding="3" cellspacing="0" width="98%"><%
%><%-- if (uiContext.getShowStatusLights()) {
    %><colgroup><%
        %><col width="5%"><%
        %><col width="44%"><%
        %><col width="49%"><%
    %></colgroup><%
}
else { --%><%
    %><colgroup><%
        %><col width="49%"><%
        %><col width="49%"><%
    %></colgroup><%
	%>
	<%-- } --%><%
    %><tr><% // status information (icon)
%><%--    if (uiContext.getShowStatusLights()) {
        %><td align="center"><%
            %><img style="display:inline"  src='<isa:mimeURL name="<%= characteristic.getStatusImage().getImagePath() %>" />' alt="<isa:translate key="<%= characteristic.getStatusImage().getResourceKey() %>"/>"><%
        %></td><%
    }
    --%><%

    // We show the language dependent name of the characteristic as text in the html page.
    // Feel free to show other data of the characteristic using the
    // methods of the API com.sap.spc.remote.client.object.Characteristic

        %><td class="csticName"><%
            %><a style="text-decoration: none; color: #000;"><%= StringUtil.encodeLanguageIndependentName(characteristic.getDisplayName(), uiContext) %><%
            if (!characteristic.getUnit().equals("")) {
                %>&nbsp;(<%= characteristic.getUnit() %>)<%
            }
            if (!uiContext.getShowStatusLights() && characteristic.isRequired()) {
                %>&nbsp;(<isa:translate key="ipc.cstic.required.sign"/>)<%
            }
            %></a><% // status information (text)
			//status information (icon)
 			if (uiContext.getShowStatusLights()) {
		 		%><img style="display:inline"  src='<isa:mimeURL name="<%= characteristic.getStatusImage().getImagePath() %>" />' alt="<isa:translate key="<%= characteristic.getStatusImage().getResourceKey() %>"/>"><%
 			}	
		%></td><%      
        %><td align="right"><%
            %><a style="text-decoration: none; color: #000;" title="<isa:translate key="<%= characteristic.getStatusImage().getResourceKey() %>"/>"><isa:translate key="<%= characteristic.getStatusImage().getResourceKey() %>"/></a><%
        %></td><%
    %></tr><%
%></table>
</div><%

if (descr != null && !descr.equals("")) {
%><table id="top" class="ipcDetailBody" border="0" cellpadding="0" cellspacing="0" width="98%"><%
    // characteristic longtext
    %><tr><%
        %><td><%
            %><table class="ipcLongtext" title="<isa:translate key="ipc.cstic.longtext"/>"><%
                %><tr><%
                    %><th><%
                        %><strong><isa:translate key="ipc.description"/></strong><%
                    %></th><%
                %></tr><%
                %><tr><%
                    %><td><%
                            %><%= descr %><%
                    %></td><%
                %></tr><%
                %><tr><%
                %></tr><%
            %></table><%
        %></td><%
    %></tr><%
%></table><%
}
// display the characteristic options
%><table id="bottom" class="ipcDetailBody" width="98%" border="0"><%
    %><tr><%
        %><td class="bottom"><%
            %><input type="hidden" name="detail" value="true"><%
                // here we show the values of the characteristic
			    CharacteristicValuesControllerUI.include(pageContext,
                                         uiContext.getJSPInclude("tiles.characteristicValues.jsp"),
			                             uiContext,
			                             instance,
                                         characteristicGroup,
			                             characteristic,
			                             true,
			                             true,
			                             csticsArea,
			                             true,
			                             true,
                                         customerParams);
        %></td><%
    %></tr><%
%></table>
<%
	if (ui.isAccessible())
	{
%>
<a 
	href="<%=grpBeginHref%>"
	id="<%=grpEndId%>"
	title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>" 
></a>	
<%
	}
%>