<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
%><body><%
    %><form name="back2hookUrl" method="post" action="<%= JspUtil.encodeHtml(uiContext.getHookUrl()) %>"
        target="<%= uiContext.getPropertyAsString( RequestParameterConstants.TOPFRAME ) %>"><%
        %><input type="hidden" name="RETCODE" value="0"><%
        %><input type="hidden" name="SPC_ERROR_CODE" value="`SPC_ERROR_CODE`"><%
    %></form><%
    %><SCRIPT LANGUAGE="JavaScript"><%
            %>document.back2hookUrl.submit();<%
    %></SCRIPT><%
%></body>