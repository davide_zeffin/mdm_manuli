<%
// This include contains the UI of a messagecharacteristic
// using the object "characteristic" you can get all the
// information about the characteristic.
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.spc.remote.client.object.OriginalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesControllerUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDescriptionUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MimeObjectUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIElement"%><% 
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*" %><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/messageCharacteristic.jsp"/><% 
BaseUI ui = new BaseUI(pageContext);
CharacteristicUI.IncludeParams includeParams = CharacteristicUI.getIncludeParams(pageContext);
InstanceUIBean currentInstance = includeParams.getInstance();
GroupUIBean characteristicGroup = includeParams.getCharGroup();
CharacteristicUIBean characteristicUIBean = includeParams.getCstic();
UIContext uiContext = includeParams.getIpcBaseUI();
UIArea csticsArea = includeParams.getUiArea();
Hashtable customerParams = includeParams.getCustomerParams();

List assignedValues = characteristicUIBean.getAssignedValues();


%><%            
	String grpBeginId = "group" + characteristicUIBean.getName() + "-begin";
	String grpEndId = "group" + characteristicUIBean.getName() + "-end";
	String grpBeginHref = "#" + grpBeginId;
	String grpEndHref = "#" + grpEndId;
	String grpHeader = characteristicUIBean.getLanguageDependentName();
	
	String grpTxt = WebUtil.translate(pageContext, "ipc.grp.messagecstic", new String[] { grpHeader}); 
	if (ui.isAccessible()){
			%><a<% 
				%> href="<%=grpEndHref%>"<%
				%> id="<%=grpBeginId%>"<%
				%> title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>" <%
			%>></a><%
	}
	for (int i=0; i<assignedValues.size(); i++){
	    ValueUIBean value = (ValueUIBean) assignedValues.get(i);
	    String messageCsticStyleClass = value.getMessageCsticStyleClass();
%><table class="ipcMessageCsticBar" title="<isa:translate key="<%=value.getDisplayNameForMessageCstic()%>"/>" border="0" width="100%" cellspacing="0"><%
	%><tr><%
        %><td align="left"><%    
	        %><table border="0" align="left"><%
                %><tr><%
			    	%><td class="messageIcon"><%
			        	%><img style="display:inline" src="<isa:mimeURL name="<%= value.getStatusImage().getImagePath() %>" />" alt="<isa:translate key="<%= value.getStatusImage().getResourceKey() %>"/>"><%
			        %></td><%		                
                    %><td class="csticName"><%
	                    %><%=characteristicUIBean.getLanguageDependentName()%><%
                    %></td><%
                %></tr><%
            %></table><%            
        %></td><%
        %><td align="right"><%
            %><table border="0" align="right"><%
                %><tr><%
                    %><td align="left"><%
						//start showDetailsLink
						String onClick = "";
					    String action = WebUtil.getAppsURL( pageContext,
					                                null,
					                                "/ipc/showMessageCharacteristicDetails.do",
					                                null,
					                                null,
					                                false );
					    String href = "javascript:getMessageCsticDetails(" +
					                              "'" + currentInstance.getId() + "'," +
						                          "'" + characteristicGroup.getName() + "'," +
					                              "'" + characteristicUIBean.getName() + "'," +
					                              "'" + value.getName() + "'," +
					                              "'" + action + "');";
						%><a href = "<%= href %>" onClick = "<%= onClick %>" title="<isa:translate key = "ipc.details"/>"><%
						    %><isa:translate key = "ipc.details"/><%
						%></a><%
						//end showDetailsLink
                    %></td><%
                %></tr><%
            %></table><%
        %></td><%
    %></tr><%
%></table><%
%><table class="<%= messageCsticStyleClass %>" title="<isa:translate key="<%=characteristicUIBean.getTableStyleTooltip()%>"/>" border="0" width="100%" cellspacing="0"><%
	%><tr><%
		// if the value has a mime-object for the work area, we show it
		if (value.hasMimesForWorkArea()){ // id=01
		%><td width="80"><%                                
			// get the appropriate mime for the work-area: icon, o2c, sound (see ValueUIBean)
			MimeUIBean mime = value.getMimeForWorkArea();
			ArrayList mimes = value.getMimes();
			// get the enlarged Mime for this value
			MimeUIBean enlargedMime = value.getEnlargedMime();
			// include mimeObject.jsp only if mime is not null-object
			if (mime != MimeUIBean.C_NULL) {
				MimeObjectUI.include(pageContext,
											   uiContext.getJSPInclude("tiles.mimeObject.jsp"),
											   uiContext,
											   mimes,
											   mime,
											   enlargedMime,
											   currentInstance,
											   characteristicGroup,
											   customerParams);
			}
		%></td><%
		} // id=01
		%><td><%
		%><%= value.getDisplayNameForMessageCsticValues() %><%
		%></td><%
	%></tr><%
%></table><%
	}
	if (ui.isAccessible()){
%><a <%
	%> href="<%=grpBeginHref%>"<%
	%> id="<%=grpEndId%>"<%
	%> title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"<%
%>></a><%
	}
%>