<%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ProductVariantComparisonUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/productVariantsComparison.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
ProductVariantComparisonUI.IncludeParams includeParams = ProductVariantComparisonUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
InstanceUIBean instance = includeParams.getConfigInstance();
List productVariantList = includeParams.getProductVariantsToCompare();
Hashtable customerParams = includeParams.getCustomerParams();

%><input type="hidden" name="<%= Constants.CURRENT_PRODUCT_VARIANT_ID %>"><%
%><center><%
    %><br /><%
    %><%
			int colno = 2;
			if (productVariantList != null)
				colno += productVariantList.size();
			int rowno = 1 + instance.getCharacteristics().size();
			if (!uiContext.getHidePrices()) 
				rowno++;
			if (!uiContext.getPropertyAsBoolean( RequestParameterConstants.ONLY_VAR_FIND)) 
				rowno++;
			String columnNumber = Integer.toString(colno);
			String rowNumber = Integer.toString(rowno);
			String firstRow = Integer.toString(1);
			String lastRow = Integer.toString(rowno);
			String tableTitle = WebUtil.translate(pageContext,"ipc.prod_variants.compare.title", null);
		%>
		<%
			if (ui.isAccessible())
			{
		%>
			<a href="#itemstableend" 
				id="itemstablebegin"
				title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
			</a>
		<%
			}
		%>
        <table class="ipcProductVariantsHead" summary="<isa:translate key="access.table.summary" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>"><%
        %><caption><%
            %><strong><isa:translate key="ipc.prod_variants.compare.title"/></strong><%
        %></caption><%
        %><tr><%
            %><td><%
                %><table class="ipcProductVariants"><%
                    // this is the header row of the product variants
                    %><tr><%
                        int numCols = 0;
                        %><th scope="col" title="<isa:translate key="ipc.characteristic"/>"><%
                            %><isa:translate key="ipc.characteristic"/><%
                            numCols++;
                        %></th><%
                        %><th scope="col" title="<%= instance.getLanguageDependentName() %>"><%
                            %><%= instance.getLanguageDependentName() %><%
                            numCols++;
                        %></th><%
                        if (productVariantList != null) { // (id=2)
                            for (Iterator productVariants = productVariantList.iterator(); productVariants.hasNext();) {  // (id=1)
                                ProductVariantUIBean currentProductVariant = ((ProductVariantUIBean) productVariants.next());
                                String productName;
                                if (currentProductVariant.getItem() != null
                                        && uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_LANGUAGE_DEPENDENT_NAMES)) {
                                    productName = currentProductVariant.getItem().getProductDescription();
                                }
                                else {
                                    productName = currentProductVariant.getId();
                                }
                        %><th scope="col" title="<%= productName %>"><%
                            %><%= productName %><%
                            numCols++;
                        %></th><%
                            } // (id=1)
                        } // (id=2)
                    %></tr><%
                    // loop through all characteristics of the configurable material and see whether the
                    // product variant has values for the characteristic
                    List characteristics = instance.getCharacteristics();
                    for (Iterator characteristicsit = characteristics.iterator(); characteristicsit.hasNext();) { // (id=3)
                        CharacteristicUIBean characteristic = (CharacteristicUIBean) characteristicsit.next();
                    %><tr><%
                        %><td title="<%= characteristic.getLanguageDependentName() %>"><%
                            %><%= characteristic.getLanguageDependentName() %><%
                        %></td><%
                        %><td><%
                        Iterator valuesIt = characteristic.getAssignedValues().iterator();
                        if (valuesIt.hasNext()) { // (id=4)
                            %><%
                            int counter = 0;
                            while (valuesIt.hasNext()) { // (id=5)
                                ValueUIBean currentValue = (ValueUIBean) valuesIt.next();
                                if (counter > 0){
                                    %><br><%
                                }
                                %><%
                                    %><%= currentValue.getLanguageDependentName() %><%
                                %><%
                                counter++;
                            } // (id=5)
                            %><%
                        } // (id=4)
                        %></td><%
                        for (Iterator productVariants = productVariantList.iterator(); productVariants.hasNext();) { // (id=6)
                            ProductVariantUIBean currentProductVariant = ((ProductVariantUIBean) productVariants.next());
                            InstanceUIBean prodVarRoot = currentProductVariant.getInstance();
                            CharacteristicUIBean currentCharacteristic = prodVarRoot.getCharacteristic(characteristic.getName());
                        %><td><%
                            if (currentCharacteristic != null) { // (id=7)
                                List currentValues = currentCharacteristic.getAssignedValues();
                                valuesIt = currentValues.iterator();
                                if (valuesIt.hasNext()) {
                            %><%
		                            int count = 0;
                                    while (valuesIt.hasNext()) { // (id=8)
                                        ValueUIBean currentValue = (ValueUIBean) valuesIt.next();
                                        if (count > 0){
                                            %><br><%
                                        }
                                %><%
                                    %><%= currentValue.getLanguageDependentName() %><%
                                %><%
                                		count++;
                                    } // (id=8)
                            %><%   
                                }
                            } // (id=7)
                        %></td><%
                        } // (id=6)
                    %></tr><%
                    } // (id=3)
                    if (!uiContext.getHidePrices()) {
                    %><tr><%
                        %><td title="<isa:translate key="ipc.price"/>"><%
                            %><isa:translate key="ipc.price"/><%
                        %></td><%
                        %><td><%
                            %><%= instance.getPrice() %><%
                        %></td><%
                        for (Iterator productVariants = productVariantList.iterator(); productVariants.hasNext();) {
                            ProductVariantUIBean currentProductVariant = ((ProductVariantUIBean) productVariants.next());
                        %><td><%
                            %><%= currentProductVariant.getPrice() %><%
                        %></td><%
                        }
                    %></tr><%
                    }
                    if (!uiContext.getPropertyAsBoolean( RequestParameterConstants.ONLY_VAR_FIND)) {
                    %><tr><%
                        %><td title="<isa:translate key="ipc.action"/>"><%
                            %><isa:translate key="ipc.action"/><%
                        %></td><%
                        %><td><%
                        %></td><%
                        for (Iterator productVariants = productVariantList.iterator(); productVariants.hasNext();) {
                            ProductVariantUIBean currentProductVariant = ((ProductVariantUIBean) productVariants.next());
                        %><td class="action"><%
                            // Here we set the link to submit the form "currentForm" on click.
                            // Before we must assign the name of the selected product variant to the field
                            // CURRENT_PRODUCT_VARIANT_ID to make this product variant known to the IPC server.
                            // Then we submit the form to replace the root instance by the selected product variant.
                            String variantID = currentProductVariant.getId();
                            String action = WebUtil.getAppsURL(pageContext,
                                                                null,
                                                                "/ipc/applyProductVariant.do",
                                                                null,
                                                                null,
                                                                false);
							String hrefStart = "";
							String hrefEnd = "";
							// show link only if not in read-only mode
							if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)){
							    StringBuffer hrefStartBuffer = new StringBuffer();
							    hrefStartBuffer.append("<a href= \"javascript:changeToProductVariant('");
						   	    hrefStartBuffer.append(variantID);
						   	    hrefStartBuffer.append("', '");
						   	    hrefStartBuffer.append(action);
						   	    hrefStartBuffer.append("');\" title=\"");
						   	    hrefStartBuffer.append(WebUtil.translate(pageContext, "ipc.apply", null));
						   	    hrefStartBuffer.append("\">");
						   	    hrefStart = hrefStartBuffer.toString();
						   	    hrefEnd = "</a>";
							}
						    %><%= hrefStart %><%
						        %><isa:translate key="ipc.apply"/><%
						    %><%= hrefEnd %><%
                        %></td><%
                        }
                    %></tr><%
                    }
                %></table><%
            %></td><%
        %></tr><%
    %></table><%
	// Data Table Postamble
	if (ui.isAccessible())
	{
%>
		<a id="itemstableend" 
			href="#itemstablebegin" 
			title="<isa:translate key="access.table.end" 
				arg0="<%=tableTitle%>"/>"></a>
<%
	}
%><%
%></center><%
%>