<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MimeObjectUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.MasterdataUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/mimeObject.jsp"/><%
    //read passed parameter
    MimeObjectUI.IncludeParams includeParams = MimeObjectUI.getIncludeParams(pageContext);
    List mimes = includeParams.getMimes();
    MimeUIBean mime = includeParams.getMime();
    MimeUIBean enlargedMime = includeParams.getEnlargedMime();
    UIContext uiContext = includeParams.getIpcBaseUI();
    InstanceUIBean currentInstance = includeParams.getInstance();
    GroupUIBean currentGroup = includeParams.getCharGroup();
    Hashtable customerParams = includeParams.getCustomerParams();


    if( uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_PICTURE ) 
        && (mime.getType().equalsIgnoreCase(MimeUIBean.ICON ) 
            || mime.getType().equalsIgnoreCase(MimeUIBean.IMAGE ))){
        if( mime.getParameter( MimeUIBean.IS_ENLARGED, MimeUIBean.NO ).equals( MimeUIBean.NO ) ){
            if( uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_MIMES_IN_NEW_WINDOW ) ){
                String url = WebUtil.getAppsURL(pageContext,
                                                null,
                                                "/ipc/enlargeMimeObject.do",
                                                null,
                                                null,
                                                false);
                url = url + "?" + enlargedMime.getURLEncodedRepresentation(0);
                String onClick = "openMimeInNewWindow('" + url + "');";
%><a href = "#" onClick = "<%= onClick %>"><%
            } else {
                String groupName = "";
                if (currentGroup != null) {
	                groupName = currentGroup.getName();
                }
                String instanceId = "";
                if (currentInstance != null) {
	            	instanceId = currentInstance.getId();
                }
                String action = WebUtil.getAppsURL( pageContext, null, "/ipc/enlargeMimeObject.do",
                                                    null, null,false );
                                                    //TODO
                String addUrl = "?" + enlargedMime.getURLEncodedRepresentation(0);
                String href = "javascript:openMimeInSameWindow('" + action + "', '" + addUrl + "', '" + groupName + "', '" + instanceId + "');";
%><a href = "<%= href %>"><%
            } 
        }   
        String width = "";
        String height = "";
        String alt = "";
        String masterdataURL = uiContext.getMasterdataMimesURL();
		String mimeURL = MasterdataUtil.getMimeURL(pageContext, masterdataURL, null, null, mime.getURL());

        if( mime.getParameter( MimeUIBean.IS_ENLARGED, MimeUIBean.NO ).equals( MimeUIBean.NO ) ){
            width = mime.getParameter( "width", "70" );
            height = mime.getParameter( "height", "50" );
            alt = WebUtil.translate( pageContext, "ipc.click2enlarge", null );
    %><img border = "0" src = "<%= mimeURL %>" width = "<%= width %>" height = "<%= height %>" alt = "<%= alt %>"><%            
        }
        else {
    %><img border = "0" src = "<%= mimeURL %>" alt = "<%= alt %>"><%                        
        }
%></a><%
    } else if( mime.getType().equalsIgnoreCase( MimeUIBean.SOUND ) ){
        String masterdataURL = uiContext.getMasterdataMimesURL();
		String mimeURL = MasterdataUtil.getMimeURL(pageContext, masterdataURL, null, null, mime.getURL());
%><embed src = "<%= mimeURL %>" height = "2" width = "70" autostart = "false"><%
    %><noembed><%
        %><bgsound src = "<%= mimeURL %>" loop = "infinite"><%
    %></noembed><%
%></embed><%
    } else if( mime.getType().equalsIgnoreCase( MimeUIBean.O2C ) ) {
        if( uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_CHARACTERISTIC_VALUE_3D_PICTURE, true ) ){
            String width = "";
            String height = "";
            String masterdataURL = uiContext.getMasterdataMimesURL();
     		String mimeURL = MasterdataUtil.getMimeURL(pageContext, masterdataURL, null, null, mime.getURL());
            if( mime.getParameter( MimeUIBean.IS_ENLARGED, MimeUIBean.NO ).equals( MimeUIBean.YES ) ){
                width = mime.getParameter( "width", "620" );
                height = mime.getParameter( "height", "460" );
            } else {
                width = mime.getParameter( "width", "70" );
                height = mime.getParameter( "height", "50" );
            }
%><table border = "0"><%
    %><tr><%
        %><td><%            
            %><object classid = "CLSID:BF3CD111-6278-11D2-9EA3-00A0C9251384"<%
                %>codebase = "http://www.o2c.de/download/o2cplayer.cab#version=1,9,9,135" width = "<%= width %>"<%
                %>height = "<%= height %>" name = "O2CPlayer"><%
                %><param name = "ObjectURL" value = "<%= mimeURL %>"><%
                %><param name = "BorderStyle" value = 1><%
                %><param name = "Appearance" value = 1><%
                %><param name = "AnimPlaying" value = true><%
                %><param name = "BackColor" value = 16777215><%
            if( mime.getParameter( MimeUIBean.IS_ENLARGED, MimeUIBean.NO ).equals( MimeUIBean.YES ) ) {
                width = mime.getParameter( "width", "620" );
                height = mime.getParameter( "height", "460" );
            } else {
                width = mime.getParameter( "width", "70" );
                height = mime.getParameter( "height", "50" );
            }
                %><embed codebase = "http://www.o2c.de/dl_plugin.htm" type = "application/x-o2c-object"<%
                    %>width = "<%= width %>" height = "<%= height %>" name = "O2CPlayer"<%
                    %>src = "<%= mimeURL %>" PARAM_BorderStyle = 1<%
                    %>PARAM_Appearance = 1 PARAM_AnimPlaying = true PARAM_BackColor = #ffffff><%
            %></object><%
        %></td><%
    %></tr><%
            if( mime.getParameter( MimeUIBean.IS_ENLARGED, MimeUIBean.NO ).equals( MimeUIBean.NO ) ){
    %><tr><%
        %><td><%
                if( uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_MIMES_IN_NEW_WINDOW ) ){
                    String url = "?" + enlargedMime.getURLEncodedRepresentation( 0 );
                    String onClick = "openMimeInNewWindow('" + url + "');";
            %><a onClick = "<%= onClick %>" href = "#"><%
                } else {
					String groupName = "";
	                if (currentGroup != null) {
		                groupName = currentGroup.getName();
	                }
	                String instanceId = "";
	                if (currentInstance != null) {
		            	instanceId = currentInstance.getId();
	                }                    
                    String action = WebUtil.getAppsURL( pageContext, null, "/ipc/bufferCharacteristicsValues.do",
                                                        null, null,false );
                    String addUrl = "?" + RequestParameterConstants.FORWARD_DESTINATION
                                      + "=enlargeMimeObject&"
                                      + mime.getURLEncodedRepresentation( 0 );
					String href = "javascript:openMimeInSameWindow('" + action + "', '" + addUrl + "', '" + groupName + "', '" + instanceId + "');";
            %><a href = "<%= href %>"><%
                }
                %><isa:translate key = "ipc.click2enlarge"/><%
            %></a><%
        %></td><%
    %></tr><%
            }
%></table><%
        }
    } else {
%><%-- The mime object of type "<%= mime.getType() %>" is not supported by the mimeObject.jsp page.
If you want this type to be supported, just edit the mimeObject.jsp page and add a new "else if" - branch
for the desired type. --%><%
    }
%>
