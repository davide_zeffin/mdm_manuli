<%--this include contains the UI of the status bar
    using the attribute "configuration" you can get all the
    information about the configuration
    This attribute is defined in the file pageheader.jsp
    which you always should include in your global page 
--%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.StatusBarUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.MessageUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/statusbar.jsp"/><%
	BaseUI ui = new BaseUI(pageContext);
//read parameter from caller
StatusBarUI.IncludeParams includeParams = StatusBarUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
ConfigUIBean configuration = includeParams.getConfiguration();
Hashtable customerParams = includeParams.getCustomerParams();

// using the attribute "rootInstance" you can get all the information about the root instance
// This attribute is defined in the file pageheader.jsp which you always should include in your global page.
InstanceUIBean rootInstance = configuration.getRootInstance();
String tableClass = "ipcStatusbar";
%>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.statusbar", null); 
%>
	<a 
		href="#statusbar_group-end"
		id="statusbar_group-begin"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
<table class="<%= tableClass %>" 
<%
	if (ui.isAccessible())
	{
%>
	title="<isa:translate key="ipc.tt.statusbar"/>"
<%
	}
%>
	><%
    %><tr><%
        %><td colspan="1"><%
            %><table border="0" cellpadding="0" cellspacing="0" width="100%"><%
                %><tr><%
                    // we show the language dependent name of the root instance
                    // feel free to show other data of the root instance using the
                    // methods of the API com.sap.spc.remote.client.object.Instance
                    %><td nowrap align="left"><%
                        %><isa:translate key="ipc.product"/>:&nbsp;<%= rootInstance.getLanguageDependentName() %><%
                    %></td></tr><%
                        if (!uiContext.getHidePrices() && rootInstance.supportsPricing()) {
                    %><tr><td nowrap align="left"><%
                        %><isa:translate key="ipc.price_total"/>:&nbsp;<%= rootInstance.getPrice() %><%
                        if (uiContext.isPriceAnalysisEnabled()) {
							%>&nbsp;<a href="<isa:webappsURL name="/ipc/preparepriceanalysisshow.do"/>"><isa:translate key="ipc.priceanalysis"/></a><%
                        }
                    %></td></tr><%
                        }
                    %><tr><td nowrap align="left"><%
                        // we show the state of configuration as an image
                        // if the configuration is complete and consistent the traffic lights show green
                        // if the configuration is not complete, but consistent the traffic lights turn yellow
                        // if the configuration is not consistent the traffic lights turn red and yellow
                        if (uiContext.getShowStatusLights()) {
                        %><isa:translate key="ipc.status"/>:&nbsp;<img style="display:inline" alt="<isa:translate key="<%= configuration.getStatusImage().getResourceKey() %>"/>" src="<isa:mimeURL name="<%= configuration.getStatusImage().getImagePath() %>" />"><%
                        }
                        // or we show the configuration status using text
                        else {
                        %><isa:translate key="ipc.status"/>:&nbsp;<isa:translate key="<%= configuration.getStatusText() %>"/><%
                        }
                    %></td><%
                %></tr><%
            %></table><%
        %></td><%
    %></tr><%
%></table><%
%>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.statusbar", null); 
%>
	<a 
		href="#statusbar_group-begin"
		id="statusbar_group-end"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
