<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.core.util.HttpServletRequestFacade"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/importConfiguration.jsp"/><%
%><%@ page isThreadSafe = "true"%><%

request = HttpServletRequestFacade.determinRequest(request);

UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
String importFlag = (String)request.getAttribute(RequestParameterConstants.IMPORT_FLAG);
%><html><%
%><head><%
    %><title> IPC - Interactive Product Configuration [Version 5.0]</title><%
    //include stylsheet
	%><isa:stylesheets/><%
	%><script type="text/javascript"><%
		%>var posBehindFirstDot = location.hostname.indexOf(".")+1;<%
		%>if (posBehindFirstDot > 0) {<%
			%>document.domain = location.hostname.substr(posBehindFirstDot);<%
		%>}<%
	%></script><%	
    %><script src="<isa:mimeURL name ="/ipc/mimes/scripts/importconfiguration.inc.js"/>" type="text/javascript"><%
    %></script><%
%></head><%
if (importFlag != null && importFlag.equals(RequestParameterConstants.T)){
	String url= WebUtil.getAppsURL(pageContext,
									null,
									 "/ipc/initLayout.do",
									 null,
									 null,
									 false);
%><body class = "ipcBody" onLoad="javascript:importFinished('<%= url %>');"><%       
} else {
%><body class = "ipcBody"><%    
    //we want to show all configuration data in a html form with the name "currentForm"
    %><form name="importForm" enctype="multipart/form-data" action="<isa:webappsURL name ="ipc/importConfiguration.do"/>" target="_self" method="post"><%
		%><table class="ipcAreaFrame" cellpadding="0" cellspacing="0" width="100%"><%
			%><tr><%
        		%><td height="30" valign="middle"><label for="myFile_id"><isa:translate key="ipc.import.dialog"/></label></td><%
    		%></tr><%
		    %><tr><%
	    	    %><td height="50" valign="middle"><%
	        	    %><input id="myFile_id" type="file" name="myFile"  ><%
	            	%><input type="button" value="<isa:translate key="ipc.import.upload"/>" <%
	            		String uploadBtnTxt = WebUtil.translate(pageContext, "ipc.tt.import.upload", null);
				        %> title="<isa:translate key="access.button" arg0="<%=uploadBtnTxt%>" arg1=""/>" onClick="javascript:onUploadButtonClick()"><%
		        %></td><%
		    %></tr><%
		    %><tr><%
		        %><td height="30" valign="middle"><isa:translate key="ipc.import.dialog.warning"/></td><%
		    %></tr><%
		%></table><%
	%></form><%
}	
%></body><%
%></html>
		