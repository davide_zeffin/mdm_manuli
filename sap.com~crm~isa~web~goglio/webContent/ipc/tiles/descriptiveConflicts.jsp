<%-- this include contains the UI of all available descriptiveConflicts/
     using the object "descriptiveConflict" you can get all the
     information about one single descriptive conflict 
--%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "org.apache.struts.util.MessageResources"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.DescriptiveConflictsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import="com.sap.isa.core.util.WebUtil" %><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/descriptiveConflicts.jsp"/><%
//TODO: remove access to business objects
%><%@ page import = "com.sap.spc.remote.client.object.DescriptiveConflict"%><%
%><%@ page import = "com.sap.spc.remote.client.object.DescriptiveConflictType"%><%
BaseUI ui = new BaseUI(pageContext);
DescriptiveConflictsUI.IncludeParams includeParams = DescriptiveConflictsUI.getIncludeParams(pageContext);
List descriptiveConflictsList = includeParams.getDescriptiveConflicts();
UIContext uiContext = includeParams.getIpcBaseUI();
Hashtable customerParams = includeParams.getCustomerParams();


Iterator descriptiveConflicts = descriptiveConflictsList.iterator();
boolean descriptiveConflictsFirstEntry = true;
%><table class="ipcConflictSolverInner" border="0" cellpadding="0" cellspacing="0" width="100%"><%
    while (descriptiveConflicts.hasNext()) {
        DescriptiveConflict descriptiveConflict = (DescriptiveConflict) descriptiveConflicts.next();
    %><tr><%
        %><td><%
            %>&nbsp;<%
        %></td><%
    %></tr><%
    %><tr><%
        %><td><%
			%>
<%            
	String grpTxt = WebUtil.translate(pageContext, "ipc.grp.descconflict", new String[]{descriptiveConflict.getCharacteristicLangDepName()}); 
	if (ui.isAccessible())
	{
%>
			<a 
				href="#groupdescriptiveconflict-end"
				id="groupdescriptiveconflict-begin"
				title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>" 
			></a>
<%
	}
%><table border="0" cellpadding="0" cellspacing="0" width="100%" class="conflictGroup"><%
			    %><colgroup><%
			        %><col width="60%"><%
			        %><col width="40%"><%
			    %></colgroup><%
			    %><tr><%
			        %><th class="csticName" align="left" colspan="3"><%
			            %><%= descriptiveConflict.getCharacteristicLangDepName() %>:<%
			        %></th><%
			    %></tr><%
		        ServletContext context = pageContext.getServletContext();			    
		        MessageResources resources = WebUtil.getResources(context);
			    List descriptiveConflictTypes = descriptiveConflict.getConflictTypes(
			        uiContext.showConflictSolutionRate(),
			        uiContext.getLocale(),
			        resources,
			        uiContext.getConflictExplanationTextLineTag(),
			        uiContext.useConflictHandlingShortcuts(),
			        uiContext.useValueConflictShortcut(),
			        uiContext.useExplanationTool()
			    );
			    Iterator iDescriptiveConflictTypes = descriptiveConflictTypes.iterator();
			    int descrConflCounter = 0;
			    String descrConflLabelId = new StringBuffer().append(descriptiveConflict.getCharacteristicName()).append("Label").toString();
			    String descrConflLinkId = new StringBuffer().append(descriptiveConflict.getCharacteristicName()).append("Link").toString();
			
			    while (iDescriptiveConflictTypes.hasNext()) { // (id=1)
			        DescriptiveConflictType descriptiveConflictType = (DescriptiveConflictType) iDescriptiveConflictTypes.next();
			    %><tr><%
			        %><td align="left"><%
			            %><%= descriptiveConflictType.getName() %><%
			        %></td><%
			        if (!descriptiveConflictType.hasText()) {
			            continue;
			        }
			        descrConflCounter++;
			        descrConflLabelId = descrConflLabelId + descrConflCounter;
			        String explTitle = "";
			        String explTitle2 = "";
					if (HttpUtils.getRequestURL(request).toString().endsWith("conflictHandlingShortcuts.jsp")) {
						explTitle = WebUtil.translate(pageContext, "ipc.conflict.explain", null);
						explTitle2 = WebUtil.translate(pageContext, "ipc.conflict.explain", null);
					}
					else {
						explTitle = WebUtil.translate(pageContext, "ipc.conflict.hide", null);
						explTitle2 = WebUtil.translate(pageContext, "ipc.conflict.explain", null);						
					}
			        %><td align="right"><%
			            %><a href="javascript:toggleConflictExplanation('<%= descrConflLabelId %>', '<%=descrConflLinkId%>', '<%=explTitle%>', '<%=explTitle2%>');" id="<%=descrConflLinkId%>"<%
					    if (descriptiveConflictsFirstEntry) {
			               %>title="<%=explTitle%>"><%=explTitle%></a><%
			            }else{
							%>title="<%=explTitle2%>"><%=explTitle2%></a><%
			            }	  
			        %></td><%
			    %></tr><%
			    %><tr><%
			        %><td colspan="2"><%
			        if (HttpUtils.getRequestURL(request).toString().endsWith("conflictHandlingShortcuts.jsp")) {
			            %><label id="<%= descrConflLabelId %>" style="display:none"><%
			        }
			        else {
			            if (descriptiveConflictsFirstEntry) {
			            %><label id="<%= descrConflLabelId %>" style="display:block"><%
			            }
			            else {
			            %><label id="<%= descrConflLabelId %>" style="display:none"><%
			            }
			        }
			                %><%= descriptiveConflictType.getText() %><%
			            %></label><%
			        %></td><%
			    %></tr><%
			    } // (id=1)
			%></table>
<%            
	if (ui.isAccessible())
	{
%>
			<a 
				href="#groupdescriptiveconflict-begin"
				id="groupdescriptiveconflict-end"
				title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>" 
			></a>
<%
	}
%><%
        %></td><%
    %></tr><%
    %><tr><%
    %></tr><%
        descriptiveConflictsFirstEntry = false;
    }
%></table>