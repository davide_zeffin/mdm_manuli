<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.MimeUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MimeObjectUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.StatusBarUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.HeaderUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIButton"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "java.util.List" %><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><% 
%><isa:moduleName name = "/ipc/tiles/header.jsp"/><%
	BaseUI ui = new BaseUI(pageContext);
%><table border="0" cellpadding="0" cellspacing="0" class="ipcHeader"><%
    //read parameter from enclosing page
    HeaderUI.IncludeParams includeParams = HeaderUI.getIncludeParams(pageContext);
    UIContext uiContext = includeParams.getIpcBaseUI();
    ConfigUIBean configuration = includeParams.getConfiguration();
    InstanceUIBean currentInstance = includeParams.getInstance();
    GroupUIBean currentGroup = includeParams.getGroup();
    Hashtable customerParams = includeParams.getCustomerParams();
    InstanceUIBean rootInstance = configuration.getRootInstance();
    List mimes = rootInstance.getMimes();

    // The calling application has the possibility to show a message in the header
    if (uiContext.getPropertyAsString(RequestParameterConstants.CALLER_MESSAGE) != null 
        && !uiContext.getPropertyAsString(RequestParameterConstants.CALLER_MESSAGE).equals("")) {
    %><tr><%
        %><td colspan="3"><%
            %><%= uiContext.getPropertyAsString(RequestParameterConstants.CALLER_MESSAGE) %><%
        %></td><%
    %></tr><%
   }
    %><tr><%
    	%><table class="novaHeader"><%
        %><td><%
            %><table class="ipcHeaderButtons"><%
                %><tr><%
                    // Shortcut for buttons area
                    UIArea buttonsArea = new UIArea("buttons", uiContext);
                    String buttonsAreaAccessKey = buttonsArea.getShortcutKey();                        
                    %><td><%    
                        %><a href="#" accesskey="<isa:translate key="<%= buttonsAreaAccessKey %>"/>" title="<isa:translate key="ipc.area.buttons"/>" tabindex="<%= buttonsArea.getTabIndex()%>"><%
                            %><img src="<isa:mimeURL name="ipc/mimes/images/sys/spacer.gif"/>" alt="" border="0" align="middle"><%
                        %></a><%
                    %></td><%
                    // customer button 1, if desired
                    if (uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_CUSTOMER_BUTTON1)
                        && !(uiContext.getPropertyAsBoolean(RequestParameterConstants.CUSTOMER_BUTTON1_AVAILABLE_IF_COMPLETE_ONLY)
                        && !uiContext.getCurrentConfiguration().isComplete())) {
                    %><td><%
                        String value = "";
                        if (uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON1_LABEL) == null) {
                            value = value + WebUtil.translate(pageContext, "ipc.customer_button1_label", null);
                        }
                        else {
                            value = value + WebUtil.translate(pageContext,
                                                                uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON1_LABEL),
                                                                null);
                        }
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                             null,
                                                             uiContext.getPropertyAsString(
                                                                 RequestParameterConstants.CUSTOMER_BUTTON1_ACTION),
                                                             null,
                                                             null,
                                                             false);
                        String forwardDestination = uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON1_FORWARD);
                        String target = uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON1_TARGET);
                        %><a href="#" 
							title="<isa:translate key="access.button" arg0="<%=value%>" arg1=""/>"
                            class="<%= cssClass %>" 
							tabindex="<%= buttonsArea.getTabIndex()%>" 
                			onClick="javascript:onCustomerButtonClick('<%= action %>', '<%= target %>');"><%
	                    %><%= value %></a><%
                    %></td><%
                    }
                    // customer button 2, if desired
                    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_CUSTOMER_BUTTON2)
                        && !(uiContext.getPropertyAsBoolean(RequestParameterConstants.CUSTOMER_BUTTON2_AVAILABLE_IF_COMPLETE_ONLY)
                        && !uiContext.getCurrentConfiguration().isComplete())) {
                    %><td><%
                        String value = "";
                        if (uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON2_LABEL) == null) {
                            value = WebUtil.translate(pageContext, "ipc.customer_button2_label", null);
                        }
                        else {
                            value = WebUtil.translate(pageContext,
                                                        uiContext.getPropertyAsString(
                                                            RequestParameterConstants.CUSTOMER_BUTTON2_LABEL),
                                                        null);
                        }
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                             null,
                                                             uiContext.getPropertyAsString(
                                                                 RequestParameterConstants.CUSTOMER_BUTTON2_ACTION),
                                                             null,
                                                             null,
                                                             false);
                        String forwardDestination = uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON2_FORWARD);
                        String target = uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON2_TARGET);
                        %><a href="#" 
							title="<isa:translate key="access.button" arg0="<%=value%>" arg1=""/>"
                            class="<%= cssClass %>" 
							tabindex="<%= buttonsArea.getTabIndex()%>" 
                			onClick="javascript:onCustomerButtonClick('<%= action %>', '<%= target %>');"><%
	                    %><%= value %></a><%
                    %></td><%
                    }
                    // customer button 3, if desired
                    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_CUSTOMER_BUTTON3)
                        && !(uiContext.getPropertyAsBoolean(RequestParameterConstants.CUSTOMER_BUTTON3_AVAILABLE_IF_COMPLETE_ONLY)
                        && !uiContext.getCurrentConfiguration().isComplete())) {
                    %><td><%
                        String value = "";
                        if (uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON3_LABEL) == null) {
                            value = value + WebUtil.translate(pageContext, "ipc.customer_button3_label", null);
                        }
                        else {
                            value = value + WebUtil.translate(pageContext,
                                                                uiContext.getPropertyAsString(
                                                                    RequestParameterConstants.CUSTOMER_BUTTON3_LABEL),
                                                                null);
                        }
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                             null,
                                                             uiContext.getPropertyAsString(
                                                                 RequestParameterConstants.CUSTOMER_BUTTON3_ACTION),
                                                             null,
                                                             null,
                                                             false);
                        String forwardDestination = uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON3_FORWARD);
                        String target = uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMER_BUTTON3_TARGET);                        
                        %><a href="#" 
							title="<isa:translate key="access.button" arg0="<%=value%>" arg1=""/>"
                            class="<%= cssClass %>" 
							tabindex="<%= buttonsArea.getTabIndex()%>" 
                			onClick="javascript:onCustomerButtonClick('<%= action %>', '<%= target %>');"><%
	                    %><%= value %></a><%
                    %></td><%
                    }
                    // check button
                    if ((!uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE))
                        && !uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)) {
                    %><td><%
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                             null,
                                                             "/ipc/dispatchSetValuesOnCaller.do",
                                                             null,
                                                             null,
                                                             false);
                   		String checkBtnTxt = WebUtil.translate(pageContext, "ipc.tt.check", null);
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=checkBtnTxt%>" arg1=""/>"
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
                            onClick="onCheckButtonClick('<%= action %>');" ><%
	                    %><isa:translate key="ipc.check"/></a><%
                    %></td><%
                    }
                    // reset button
                    if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)) {
                    %><td><%
	                    UIButton resetButton = new UIButton("resetbutton", uiContext);
    	                String accessKey = resetButton.getShortcutKey();                        
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                             null,
                                                             "/ipc/resetConfiguration.do",
                                                             null,
                                                             null,
                                                             false);
                        //after reset the rootinstance and the first group + characteristic of the
                        //rootinstance to be selected
                        String currentInstanceId = rootInstance.getId();
                        String currentCsticName = "";
                        String currentGroupName = "";
                        String resetBtnTxt = WebUtil.translate(pageContext, "ipc.conflict.tt.reset", null);
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>"
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
                            accesskey="<isa:translate key="<%= accessKey %>"/>"
                            onClick="javascript:onResetButtonClick('<%= currentInstanceId %>', '<%= currentCsticName %>', '<%= currentGroupName %>', '<%= action %>', '<isa:translate key="ipc.warning.reset"/>');"><%
                        %><isa:translate key="ipc.reset"/></a><%
                    %></td><%
                    }
                    // apply button
                    if ((!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)
                        && uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_APPLY_BUTTON))
                        || (!uiContext.getPropertyAsBoolean(RequestParameterConstants.ORIGINAL_DISPLAY_MODE)
                        && uiContext.getPropertyAsBoolean(RequestParameterConstants.PRODUCT_VARIANT_MODE)
                        && uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_APPLY_BUTTON))
                        || (uiContext.getPropertyAsBoolean(RequestParameterConstants.CHANGEABLE_PRODUCT_VARIANT_MODE)
                         && (!uiContext.getDisplayMode())
                         && uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_APPLY_BUTTON)
                           ) 
                       ) {
                        // show apply button in the following cases:
                        // 1. UI is not in display-mode AND the uiContext-Property "applybutton.show" is T
                        // 2. The UI is in the product-variant-mode AND the original display-mode is F
                        // 3. The UI is in the changeable pv mode
                    %><td><%
	                    UIButton acceptButton = new UIButton("acceptbutton", uiContext);
    	                String accessKey = acceptButton.getShortcutKey();            
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                             null,
                                                             uiContext.getPropertyAsString(
                                                                RequestParameterConstants.APPLY_BUTTON_ACTION),
                                                             null,
                                                             null,
                                                             false);
                        String target = "";
                        String currentInstanceId = "";
                        String currentCsticName = "";
                        String currentGroupName = "";
                        if (uiContext.getTopframe() != null) {
                            target = uiContext.getTopframe();
                        }
                   		String acceptBtnTxt = WebUtil.translate(pageContext, "ipc.conflict.tt.accept", null);                        
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=acceptBtnTxt%>" arg1=""/>"
                            accesskey="<isa:translate key="<%= accessKey %>"/>"
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
                            onClick="javascript:onAcceptButtonClick('<%= action %>');"><%                            
                        %><isa:translate key="ipc.apply"/></a><%
                    %></td><%
                    }
                    // change to KMAT button (customize)
                    if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.ORIGINAL_DISPLAY_MODE)
                        && uiContext.getPropertyAsBoolean(RequestParameterConstants.PRODUCT_VARIANT_MODE)
                        && !uiContext.getPropertyAsBoolean(RequestParameterConstants.CHANGEABLE_PRODUCT_VARIANT_MODE)) {
                        // show button only if pv-mode and the original display-mode is the edit-mode. 
                        // If the original display-mode is read-only it does not make sense to switch to the KMAT.
                        // Also do not show this button for changeable product variants
                    %><td><%
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                             null,
															 "/ipc/changeToConfigProduct.do",
                                                             null,
                                                             null,
                                                             false);
                        String target = "";
                        String currentInstanceId = "";
                        String currentCsticName = "";
                        String currentGroupName = "";
                        if (uiContext.getTopframe() != null) {
                            target = uiContext.getTopframe();
                        }
                   		String customizeBtnTxt = WebUtil.translate(pageContext, "ipc.tt.customize", null);                        
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=customizeBtnTxt%>" arg1=""/>"
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
							onClick="javascript:onCustomizeButtonClick('<%= action %>');"><%
                        %><isa:translate key="ipc.customize"/></a><%
                    %></td><%
                    }
					// import/export button
					String securityLevel = (String)uiContext.getProperty(RequestParameterConstants.SECURITY_LEVEL);
					boolean insecureImportExport = false;
					if (securityLevel != null && securityLevel.equals("0")){
                        insecureImportExport = true;
					}
                    boolean editMode = true;
                    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.DISPLAY_MODE)) {
                        editMode = false;
                    }
					String importExportMode = uiContext.getPropertyAsString( RequestParameterConstants.IMPORT_EXPORT_MODE);
                    // The import button should be displayed only if
                    // 1. The UI's security-level is set to "insecure" (i.e. "0")
                    // 2. The UI is in edit-mode (behavior.display=F)
                    // 3. The appropriate XCM settings for the import have been maintained
                    if (insecureImportExport
                        && editMode
                        && (importExportMode.equalsIgnoreCase(RequestParameterConstants.IMPORT_XCM) 
					    || importExportMode.equalsIgnoreCase(RequestParameterConstants.IMPORT_EXPORT_XCM))){
					%><td><%
					    String cssClass = "ipcButton";
						String url = "";
						url = WebUtil.getAppsURL(pageContext,
															 null,
															 "/ipc/tiles/importConfiguration.jsp",
															 null,
															 null,
															 false);
						String importBtnTxt = WebUtil.translate(pageContext, "ipc.tt.import", null);
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=importBtnTxt%>" arg1=""/>"
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
							onClick="javascript:openImportDialog('<%= url %>');"><%							
                        %><isa:translate key="ipc.import"/></a><%
					%></td><%                    	
					}
					if (insecureImportExport && (importExportMode.equalsIgnoreCase(RequestParameterConstants.EXPORT_XCM) 
					    || importExportMode.equalsIgnoreCase(RequestParameterConstants.IMPORT_EXPORT_XCM))){
					%><td><%
					    String cssClass = "ipcButton";
						String action = "";
						if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE)){
							action = WebUtil.getAppsURL(pageContext,
																 null,
																 "/ipc/exportConfiguration.do",
																 null,
																 null,
																 false);
						}
						else {						
							action = WebUtil.getAppsURL(pageContext,
																 null,
																 "/ipc/dispatchSetValuesOnCaller.do",
																 null,
																 null,
																 false);						

						}
                   		String exportBtnTxt = WebUtil.translate(pageContext, "ipc.tt.export", null);
						%><input type="hidden" name="<%= InternalRequestParameterConstants.EXPORT_FLAG %>" value="" /><%
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=exportBtnTxt%>" arg1=""/>"
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
 							onClick="javascript:onExportButtonClick('<%= currentInstance.getId() %>',<%
                                                        %>'<%= currentGroup.getName() %>',<%
                            							%>'<%= action %>');"><%							
                        %><isa:translate key="ipc.export"/></a><%
					%></td><%                    	
					}
                    // back button
                    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_BACK_BUTTON)) {
                    %><td><%
                        String title = "";
                        String value = "";
                        UIButton cancelButton = new UIButton("cancelbutton", uiContext);
                        String accessKey = cancelButton.getShortcutKey();
                        String showPopUp = "true";
                        if (uiContext.getPropertyAsBoolean( RequestParameterConstants.DISPLAY_MODE)) {
                            title = WebUtil.translate(pageContext, "ipc.tt.back", null);
                            value = WebUtil.translate(pageContext, "ipc.leave.config", null);
                            // don't show a warning pop-up if display-mode=T (i.e. read-only mode)
                            showPopUp = "false";
                        }
                        else {
                            title = WebUtil.translate(pageContext, "ipc.conflict.tt.cancel", null);
                            value = WebUtil.translate(pageContext, "ipc.cancel", null);
                        }
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                             null,
                                                             uiContext.getPropertyAsString(
                                                                RequestParameterConstants.BACK_BUTTON_ACTION),
                                                             null,
                                                             null,
                                                             false);
                        String forwardDestination = "";
                        String target = "";
                        String currentInstanceId = "";
                        String currentCsticName = "";
                        String currentGroupName = "";
                        if (uiContext.getTopframe() != null) {
                            target = uiContext.getTopframe();
                        }
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=title%>" arg1=""/>" 
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
                            accesskey="<isa:translate key="<%= accessKey %>"/>"
                            onClick="javascript:onBackButtonClick('<%= currentInstanceId %>', '<%= currentCsticName %>', '<%= currentGroupName %>', '<%= action %>', '<isa:translate key="ipc.warning.cancel"/>', '<%= showPopUp %>');"><%
                        %><%= value %></a><%
                    %></td><%
                    }
                    // settings button
                    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.ENABLE_SETTINGS)) {
                    %><td><%
                        String cssClass = "ipcButton";
                        String action = "";
                        action = WebUtil.getAppsURL(pageContext, 
                                                        null,
                                                        "/ipc/editSettings.do",
                                                        null,
                                                        null,
                                                        false);
						String settingsBtnTxt = WebUtil.translate(pageContext, "ipc.tt.settings", null);
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=settingsBtnTxt%>" arg1=""/>" 
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
                            onClick="javascript:onSettingsButtonClick('<%= currentInstance.getId() %>',
                                                                      '<%= currentGroup.getName() %>',
                                                                      '<%= action %>');"><%
                        %><isa:translate key="ipc.settings"/></a><%
                    %></td><%
                    }
// show conflict trace button
if (!configuration.isConsistent() && uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_CONFLICT_TRACE)) {
%><td><%
	String cssClass = "ipcButton";
	String action = "";
	action = WebUtil.getAppsURL(pageContext, 
									null,
									"/ipc/gotoConflictTrace.do",
									null,
									null,
									false);
	String showConflictTraceBtnTxt = WebUtil.translate(pageContext, "ipc.tt.showConflictTrace", null);
	%><a href="#" 
		title="<isa:translate key="access.button" arg0="<%=showConflictTraceBtnTxt%>" arg1=""/>" 
		class="<%= cssClass %>"
		tabindex="<%= buttonsArea.getTabIndex()%>"
		onClick="javascript:onShowConflictTraceButtonClick('<%= currentInstance.getId() %>',
												  '<%= currentGroup.getName() %>',
												  '<%= action %>');"><%
	%><isa:translate key="ipc.showConflictTrace"/></a><%
%></td><%
}
					// save and Add page button for designer
					if (uiContext.isDesignerMode()) {
					%><td><%
						String cssClass = "ipcButton";
						String action = "";
						action = WebUtil.getAppsURL(pageContext, 
														null,
														"dynamicUIMaintenance/save.do",
														null,
														null,
														false);
						String settingsBtnTxt = WebUtil.translate(pageContext, "ipc.tt.settings", null);
                        %><a href="#" 
                            title="<isa:translate key="access.button" arg0="<%=settingsBtnTxt%>" arg1=""/>" 
                            class="<%= cssClass %>"
                            tabindex="<%= buttonsArea.getTabIndex()%>"
                            onClick="javascript:onSaveUIModelButtonClick();"><%
						%>Save</a><%
					%></td><%
					%><td><%
                    %><a href="<isa:webappsURL name="/dynamicUIMaintenance/addPage.do"/>" 
                        title="Add new Page"
                        class="<%= cssClass %>"
                        tabindex="<%= buttonsArea.getTabIndex()%>"><%
					%>Add Page</a><%
				%></td><%					
					}
                %></tr><%
            %></table><%
        %></td> 
        <%-- Not used anymore ==> page picture
	    if (!mimes.isEmpty()) {    
        %><td class="productPicture"><%
            // the following code tries to display an image for the root instance (if available)
                // Only display the first MimeObject
                MimeUIBean mime = (MimeUIBean) mimes.get(0);
                // get the enlarged Mime for this instance
                MimeUIBean enlargedMime = rootInstance.getEnlargedMime();

                MimeObjectUI.include(pageContext,
                                 uiContext.getJSPInclude("tiles.mimeObject.jsp"),
                                 uiContext,
                                 mimes,
                                 mime,
                                 enlargedMime,
                                 currentInstance,
                                 currentGroup,
                                 customerParams);
        %></td><%
	    }%> --%>
	    <%
        // search/set area
		String searchSetMode = uiContext.getPropertyAsString( RequestParameterConstants.SEARCH_SET_MODE);
        %><td align="right"><%
            %><table class="ipcHeaderButtons"><%
                %><tr><%		
					if (searchSetMode != null && !searchSetMode.equals("") 
					    && !searchSetMode.equals(RequestParameterConstants.SEARCH_SET_XCM_DISABLED)){ // id=01
					        
					    String searchSetInputReturn = (String)uiContext.getProperty(RequestParameterConstants.SEARCH_SET_VALUE);
			            uiContext.removeProperty(RequestParameterConstants.SEARCH_SET_VALUE);
					    if (searchSetInputReturn == null)
			            	searchSetInputReturn = "";
					    StringBuffer lable = new StringBuffer("ipc.searchset.lable.");
					    lable.append(searchSetMode);
			            String cssClass = "ipcButton";
			            String action = "";
			            if (uiContext.getPropertyAsBoolean( RequestParameterConstants.ONLINE_EVALUATE)){
			                action = WebUtil.getAppsURL(pageContext,
			                                                     null,
			                                                     "/ipc/searchSet.do",
			                                                     null,
			                                                     null,
			                                                     false);
			            } else {
			                action = WebUtil.getAppsURL(pageContext,
			                                                     null,
			                                                     "/ipc/dispatchSetValuesOnCaller.do",
			                                                     null,
			                                                     null,
			                                                     false);	
			            }	
			            UIArea searchArea = new UIArea("search", uiContext);
			            String searchAreaAccessKey = searchArea.getShortcutKey();                        
			            // assemble onkeypress attribute
			            StringBuffer onKeypressBuffer = new StringBuffer();
			            onKeypressBuffer.append("javascript:returnKeyForSearch('");
			            onKeypressBuffer.append(currentInstance.getId());
			            onKeypressBuffer.append("','");
			            onKeypressBuffer.append(currentGroup.getName());
			            onKeypressBuffer.append("',event, '");
			            onKeypressBuffer.append(action);
			            onKeypressBuffer.append("');");
			            String onKeypress = onKeypressBuffer.toString();                  
                	%><td><label for="<%= RequestParameterConstants.SEARCH_SET_VALUE %>"><isa:translate key="<%=lable.toString()%>"/></label></td><%
                	%><td><input type="text" class="ipcText" id="<%= RequestParameterConstants.SEARCH_SET_VALUE %>" name="<%= RequestParameterConstants.SEARCH_SET_VALUE %>" accesskey="<isa:translate key="<%= searchAreaAccessKey %>"/>" tabindex="<%=searchArea.getTabIndex()%>" onkeypress="<%= onKeypress %>" <%
                	       %> value="<%=JspUtil.encodeHtml(searchSetInputReturn)%>"/></td><%
		            %><td><%
				        %><input type="hidden" name="<%= InternalRequestParameterConstants.SEARCH_SET_FLAG %>" value="" /><%
						String searchsetBtnTxt = WebUtil.translate(pageContext, "ipc.tt.searchset.button", null);
                        %><a href="#" 
							title="<isa:translate key="access.button" arg0="<%=searchsetBtnTxt%>" arg1=""/>"
                            class="<%= cssClass %>" 
                            tabindex="<%=searchArea.getTabIndex()%>" 
                			onClick="javascript:onSearchSetButtonClick('<%= currentInstance.getId() %>',<%
                			                                            %>'<%= currentGroup.getName() %>',<%
                			                                            %>'<%= action %>');"><%
	                    %><isa:translate key="ipc.searchset.button"/></a><%						
		            %></td><%
					} // id=01 		            
                    // links for taking the a snapshot and navigating to the comparison
                    %><td><%            
                        if ((uiContext.getPropertyAsBoolean(RequestParameterConstants.ENABLE_SNAPSHOT))
                            && (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE))) {
                            
                            String snapAction = "";
                            if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE)){
                                snapAction = WebUtil.getAppsURL(pageContext,
                                                                     null,
                                                                     "/ipc/takeSnapshot.do",
                                                                     null,
                                                                     null,
                                                                     false);
                            }
                            else {                      
                                snapAction = WebUtil.getAppsURL(pageContext,
                                                                     null,
                                                                     "/ipc/dispatchSetValuesOnCaller.do",
                                                                     null,
                                                                     null,
                                                                     false);                        
                            }                           
                            String snapLinkText = WebUtil.translate(pageContext,"ipc.snap.link", null);
       						String snapLinkToolTip = WebUtil.translate(pageContext,"ipc.tt.snap.link", null);
                        %><input type="hidden" name="<%= InternalRequestParameterConstants.SNAPSHOT_ACTION %>" value="" /><%                            
                        %><a href="#" onClick="javascript:onTakeSnapshotLinkClick('<%= currentInstance.getId() %>',
                                                                                  '<%= currentGroup.getName() %>',
                                                                                  '<%= snapAction %>');" 
                                                                                  tabindex="<%= buttonsArea.getTabIndex()%>" 
                                                                                  title="<isa:translate key="access.link" arg0="<%= snapLinkToolTip %>" arg1=""/>"><%
                            %><%= snapLinkText %><%
                        %></a>&nbsp;<%    
                        }
                        boolean separatorNeeded = false;
                        // determine whether the separator has to be displayed
                        if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ENABLE_SNAPSHOT)
                            && uiContext.getPropertyAsBoolean(RequestParameterConstants.ENABLE_COMPARISON)
                            && (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE))){

                            separatorNeeded = true;
                        }
                        if (separatorNeeded){
                            // display a separator if needed
                            %>|<%
                            separatorNeeded = false; // at this point in time we don't need a separator
                        }
                        if ((uiContext.getPropertyAsBoolean(RequestParameterConstants.ENABLE_COMPARISON))
                            && (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE))) {

                            String compareToSnapAction = "";
                            if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE)){
                                compareToSnapAction = WebUtil.getAppsURL(pageContext,
                                                                     null,
                                                                     "/ipc/compareToSnapshot.do",
                                                                     null,
                                                                     null,
                                                                     false);
                            }
                            else {                      
                                compareToSnapAction = WebUtil.getAppsURL(pageContext,
                                                                     null,
                                                                     "/ipc/dispatchSetValuesOnCaller.do",
                                                                     null,
                                                                     null,
                                                                     false);                        
                            }
                            String compareToSnapLinkText = WebUtil.translate(pageContext,"ipc.compare.snap.link", null);
                        %><input type="hidden" name="<%= InternalRequestParameterConstants.COMPARISON_ACTION_SNAP %>" value="" /><%
                        %>&nbsp;<a href="#" onClick="javascript:onCompareToSnapshotLinkClick('<%= currentInstance.getId() %>',
                                                                                                '<%= currentGroup.getName() %>',
                                                                                                '<%= compareToSnapAction %>');" 
                                                                                                tabindex="<%= buttonsArea.getTabIndex()%>"><%
                            %><%= compareToSnapLinkText %><%
                        %></a>&nbsp;<%
                            // the compareToSnapshot link has been displayed -> we need another separator
                            separatorNeeded = true;
                        }
                        boolean displayCompareToStoredLink = HeaderUI.displayCompareToStoredLink(uiContext, configuration);
                        if (!displayCompareToStoredLink){
                            // the compareToStored link will not be displayed: we don't need a separator
                            separatorNeeded = false;
                        }
                        else if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ENABLE_SNAPSHOT)
                            && !uiContext.getPropertyAsBoolean(RequestParameterConstants.ENABLE_COMPARISON)
                            && displayCompareToStoredLink){
                            // takeSnap link has been displayed, comparison is disabled but compareToStored link
                            // will be displayed (because of loading messages) -> we need a separator

                            separatorNeeded = true;
                        }    
                        if (separatorNeeded){
                            // display a separator if needed
                            %>|<%
                        }
                        if (displayCompareToStoredLink) {
                            String compareToStoredAction = "";
                            if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE)){
                                compareToStoredAction = WebUtil.getAppsURL(pageContext,
                                                                     null,
                                                                     "/ipc/compareToStored.do",
                                                                     null,
                                                                     null,
                                                                     false);
                            }
                            else {                      
                                compareToStoredAction = WebUtil.getAppsURL(pageContext,
                                                                     null,
                                                                     "/ipc/dispatchSetValuesOnCaller.do",
                                                                     null,
                                                                     null,
                                                                     false);                        
                            }                           
                            String compareToStoredLinkText = WebUtil.translate(pageContext,"ipc.compare.stored.link", null);
                            %><input type="hidden" name="<%= InternalRequestParameterConstants.COMPARISON_ACTION_STORED %>" value="" /><%
                            %>&nbsp;<a href="#" onClick="javascript:onCompareToStoredLinkClick('<%= currentInstance.getId() %>',
                                                                                                '<%= currentGroup.getName() %>',
                                                                                                '<%= compareToStoredAction %>');" 
                                                                                                tabindex="<%= buttonsArea.getTabIndex()%>"><%
                                %><%= compareToStoredLinkText %><%
                            %></a>&nbsp;<%
                        }
                    %></td><%
                    // link to shorcut-info/help page
                    if (uiContext.getEnableHelpPopup()) {
                    %><td><%
						String infoPageURL = WebUtil.getAppsURL(pageContext, 
                                null,
                                "/ipc/tiles/shortcutInfo.jsp",
                                null,
                                null,
                                false);                                                        
						String shortcutInfo = WebUtil.translate(pageContext,"ipc.key.info", null);
                    	%><a class="icon" href="#" tabindex="<%= buttonsArea.getTabIndex()%>" onClick="openInfoPage('<%= infoPageURL %>');" title="<isa:translate key="access.link" arg0="<%= shortcutInfo %>" arg1=""/>"><%
						    %><img src="<%=WebUtil.getMimeURL(pageContext, "/mimes/images/help.gif") %>" width="16" height="16" alt="<isa:translate key="ipc.key.info"/>" border="0" class="display-image"><%
						%></a><img src="<%=WebUtil.getMimeURL(pageContext, "/ipc/mimes/images/sys/spacer.gif") %>" width="15" height="1"><%                    	
                    %></td><%
                    }                                                        
                %></tr><%
            %></table><%
        %></td><%
        %></table><%	
    %></tr><%
%></table><%
%>