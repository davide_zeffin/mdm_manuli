<%
// This include contains the UI of one characteristic
// using the object "characteristic" you can get all the
// information about the characteristic.
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.spc.remote.client.object.OriginalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesControllerUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDescriptionUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MimeObjectUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIElement"%><% 
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*" %><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/characteristic.jsp"/><% 
BaseUI ui = new BaseUI(pageContext);
CharacteristicUI.IncludeParams includeParams = CharacteristicUI.getIncludeParams(pageContext);
InstanceUIBean currentInstance = includeParams.getInstance();
GroupUIBean characteristicGroup = includeParams.getCharGroup();
CharacteristicUIBean characteristicUIBean = includeParams.getCstic();
UIContext uiContext = includeParams.getIpcBaseUI();
UIArea csticsArea = includeParams.getUiArea();
Hashtable customerParams = includeParams.getCustomerParams();

List characteristicValues = characteristicUIBean.getValues();

//The below given section is commented out and replaced by a call to the CharacteristicUIBean
//The method isExpanded method of CharacteristicUIBean now handles the logic for the status of expand/collapse
boolean showExpandMode = true;

/*Hashtable csticStatus = (Hashtable) uiContext.getProperty(RequestParameterConstants.CHARACTERISTIC_STATUS_CHANGE);
Object cs = csticStatus.get( currentInstance.getId() + "." + characteristicUIBean.getName() );
if ((cs == null && !uiContext.getCharacteristicValuesExpanded())
    || (cs != null && uiContext.getCharacteristicValuesExpanded())) {
    showExpandMode = false;
}*/

//This is the call to the CharacteristicBeanUI method
showExpandMode=characteristicUIBean.isExpanded();

// Here we want to show the configuration status of the characteristic
// At the beginning we assume that the characteristic is consistent and complete,
// we select the green dot.
// Then if the characteristic cannot be changed the dot turns gray.
// Then if the characteristic is requir and still has no values assigned the dot turns yellow.
// Finally if the characteristic is not consistent the dot turns red. */
%><a name="C_<%= characteristicUIBean.getName() %>"></a><%
%><%            
	String grpBeginId = "group" + characteristicUIBean.getName() + "-begin";
	String grpEndId = "group" + characteristicUIBean.getName() + "-end";
	String grpBeginHref = "#" + grpBeginId;
	String grpEndHref = "#" + grpEndId;
	String grpHeader = characteristicUIBean.getDisplayName();
	
	String invisibleTxt = characteristicUIBean.isVisible() ? "" : WebUtil.translate(pageContext, "ipc.cstic.invisible", null);
	String hasUnitTxt = characteristicUIBean.hasUnit() ? WebUtil.translate(pageContext, "ipc.cstic.hasunit", null) : "";
	String requiredTxt = CharacteristicUI.showIndicatorForRequiredCstic(uiContext, characteristicUIBean) ? WebUtil.translate(pageContext, "ipc.cstic.required", null) : "";
	String grpTxt = WebUtil.translate(pageContext, "ipc.grp.cstic", new String[] { grpHeader , invisibleTxt, hasUnitTxt, requiredTxt}); 
	if (ui.isAccessible())
	{
%>
			<a 
				href="<%=grpEndHref%>"
				id="<%=grpBeginId%>"
				title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>" 
			></a>
<%
	}
%><table class="<%= characteristicUIBean.getTableStyleClass() %>" title="<isa:translate key="<%=characteristicUIBean.getTableStyleTooltip()%>"/>" border="0" width="100%" cellspacing="0"><%
    if (uiContext.getShowStatusLights()) {
    %><colgroup><%
        %><col width="5%"><% 
        %><col ><%
        %><col ><%
    %></colgroup><%
    %><tr><%
        %><td align="center"><%
	        %><table border="0" align="center" style="border-spacing:0px; border-collapse:collapse"><%
                %><tr><%
                    %><td><%	        
			            %><img style="display:inline" src="<isa:mimeURL name="<%= characteristicUIBean.getStatusImage().getImagePath() %>" />" alt="<isa:translate key="<%= characteristicUIBean.getStatusImage().getResourceKey() %>"/>"><%
                    %></td><%
                %></tr><%
            %></table><% 			            
        %></td><%
    }
    else {
    %><colgroup><%
        %><col ><%
        %><col ><%
    %></colgroup><%
    %><tr><%
    }
    String shortcutAnchorTag = "";
    String endShortcutAnchorTag = "";
    // Check whether characteristic was the last focused. If true add a shortcut.
    if (characteristicUIBean.isLastFocused()) {	
        UIElement lastFocusedCstic = new UIElement("lastfocused", uiContext);    
        String lastFocusedAccessKey = lastFocusedCstic.getShortcutKey();
		StringBuffer titleBuffer = new StringBuffer(WebUtil.translate(pageContext, "ipc.accesskey.last", null));
		titleBuffer.append(" ");
		titleBuffer.append(WebUtil.translate(pageContext, "access.accesskey.note", null));
        StringBuffer shortcutAnchorTagBuffer = new StringBuffer();
        shortcutAnchorTagBuffer.append("<a href=\"#\" style=\"text-decoration: none; color: #000;\" accesskey=\"");
        shortcutAnchorTagBuffer.append(WebUtil.translate(pageContext, lastFocusedAccessKey, null));
        shortcutAnchorTagBuffer.append("\" ");
        shortcutAnchorTagBuffer.append("tabindex=\"");
        shortcutAnchorTagBuffer.append(csticsArea.getTabIndex());
        shortcutAnchorTagBuffer.append("\" ");
        shortcutAnchorTagBuffer.append("title=\"");        
        shortcutAnchorTagBuffer.append(titleBuffer);
        shortcutAnchorTagBuffer.append("\">");
        shortcutAnchorTag = shortcutAnchorTagBuffer.toString();
        endShortcutAnchorTag = "</a>"; 
    }
    
        %><td align="left"><%    
	        %><table border="0" align="left" style="border-spacing:0px; border-collapse:collapse"><%
                %><tr><%
                    %><td class="csticName"><%	        
            if (!characteristicUIBean.isVisible()) {
			            %><em><%
            }
			 	           %><%= shortcutAnchorTag %><%= characteristicUIBean.getDisplayName() %><%= endShortcutAnchorTag %><%
            if (characteristicUIBean.hasUnit()) {
		        	        %>&nbsp;(<%= characteristicUIBean.getUnit() %>)<%
            }
            if (CharacteristicUI.showIndicatorForRequiredCstic(uiContext, characteristicUIBean)) {
        		    	    %>&nbsp;<isa:translate key="ipc.cstic.required.sign"/><%
            }
            if (!characteristicUIBean.isVisible()) {
			            %></em><%
            }
                    %></td><%
                %></tr><%
            %></table><%            
        %></td><%
        %><td align="right"><%
            %><table border="0" align="right" style="border-spacing:0px; border-collapse:collapse"><%
                %><tr><%
                    %><td><%
                        //start clearValuesLink
						if (CharacteristicUI.showClearValuesLink(uiContext, characteristicUIBean)) {
						    // only if you are working with frames
						    // you should send the name of the topframe in the request parameter.
						    // Deleting values mean at presence the update of all
						    // the frames starting with the topframe
						    StringBuffer hrefBuffer = new StringBuffer();
						    hrefBuffer.append("javascript:clearCharVals('");
						    hrefBuffer.append(WebUtil.getAppsURL(pageContext,
						                                            null,
						                                            "/ipc/deleteCharacteristicsValues.do",
						                                            null,
						                                            null,
						                                            false));
							hrefBuffer.append("','");
							hrefBuffer.append(currentInstance.getId());
							hrefBuffer.append("','");
							hrefBuffer.append(characteristicGroup.getName());
						    hrefBuffer.append("','");
						    hrefBuffer.append(characteristicUIBean.getName());
						    hrefBuffer.append("');");
						    String href = hrefBuffer.toString();
						%><a href="<%= href %>" title="<isa:translate key="ipc.reset"/>"><%
						    %><isa:translate key="ipc.reset"/><%
						%></a><%
						}
						//end clearValuesLink
                    %></td><%
                    //start allOptionsLink
					if (characteristicValues != null && characteristicValues.size() > 0 && 
									uiContext.showCharacteristicExpandLink()) {
                    %><td width="5"><%
                    %></td><%									
                    %><td width="90" align="center"><% 										
						    String groupName = null;
						    if (characteristicGroup != null) {
						        groupName = characteristicGroup.getName();
						    }
						    String action = null;
						    String forwardDestination = null;
						    action = WebUtil.getAppsURL(pageContext,
						                                 null,
						                                 ActionForwardConstants.EXPAND_CHARACTERISTIC,
						                                 null,
						                                 null,
						                                 false);
						    forwardDestination = "";
						    StringBuffer href = new StringBuffer();
						    href.append("javascript:showAllOptions('");
						    href.append(currentInstance.getId());
						    href.append("','");
						    href.append(groupName);
						    href.append("','");
						    href.append(characteristicUIBean.getName());
						    href.append("','");
						    href.append(action);
						    href.append("');");
						    String optionsString = (!showExpandMode) ? WebUtil.translate(pageContext, "ipc.all_options", null) : WebUtil.translate(pageContext, "ipc.all_options_close", null);
						%><a href="<%= href.toString() %>" title="<%=optionsString%>"><%
						%><%=optionsString%></a><%
					%></td><%	
					}				
					//end allOptionsLink
                    if (uiContext.getShowDetails()) {
                    %><td width="5"><%
                    %></td><%                        
                    %><td align="left"><%
						//start showDetailsLink
						String href = "";
						String onClick = "";
						String action = null;
						String forwardDestination = null;
						String target = null;
						if( uiContext.getShowDetailsInNewWindow() ){
						    String url = "?" + Constants.CURRENT_CHARACTERISTIC_NAME + "="
						                     + URLEncoder.encode(
						                           new String(
						                               characteristicUIBean.getName().getBytes( "UTF-8" ),
						                               "ISO-8859-1" ) ) + "&amp;"
						                     + Constants.CURRENT_CHARACTERISTIC_GROUP_NAME + "="
						                     + URLEncoder.encode(
						                           new String(
						                               characteristicGroup.getName().getBytes( "UTF-8" ),
						                               "ISO-8859-1" ) ) + "&amp;" + Constants.CURRENT_INSTANCE_ID + "="
						                     + currentInstance.getId();
						
						    href = href + "#";
						    onClick = onClick + "openDetail('" + url + "');";
						} else{
						    action = WebUtil.getAppsURL( pageContext,
						                                null,
						                                "/ipc/showCharacteristicDetails.do",
						                                null,
						                                null,
						                                false );
						    href = href + "javascript:getDetails(" +
						                              "'" + currentInstance.getId() + "'," +
							                          "'" + characteristicGroup.getName() + "'," +
						                              "'" + characteristicUIBean.getName() + "'," +
						                              "'" + action + "');";
						}
						%><a href = "<%= href %>" onClick = "<%= onClick %>" title="<isa:translate key = "ipc.details"/>"><%
						    %><isa:translate key = "ipc.details"/><%
						%></a><%
						//end showDetailsLink
                    %></td><%
                    }
                    if (characteristicUIBean.hasAdditionalMimes() && uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_LINK_TO_ADD_CSTIC_MIMES)) {
                    %><td width="5"><%
                    %></td><%									                        
                    %><td align="left"><%
						//start additional mime link
						String href = characteristicUIBean.getLinkToAdditionalMime();
						String target = "_blank";
				        String masterdataURL = uiContext.getMasterdataMimesURL();
						String mimeURL = MasterdataUtil.getMimeURL(pageContext, masterdataURL, null, null, href);
					
						%><a href="<%= mimeURL %>" target="<%= target %>" title="<isa:translate key = "ipc.cstic.link.mime"/>"><%
							%><isa:translate key = "ipc.cstic.link.mime"/><%
						%></a><%
						//end additional mime link
                    %></td><%
                    }
                %></tr><%
            %></table><%
        %></td><%
    %></tr><%
    /* Include long text if neccessary */
    if(uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.iDAndLongDescriptionTextId) || 
       uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortAndLongDescriptionTextId) ||
       uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.shortAndLongDescriptionTextId))
       {
	             CharacteristicDescriptionUI.include(pageContext,
			uiContext.getJSPInclude("tiles.characteristicDescription.jsp"),
	             	uiContext,
       	      characteristicUIBean,
                   csticsArea,
                   customerParams);
           
      }%>
</table><%

if (characteristicValues != null) {
    if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE ) && !characteristicUIBean.isReadOnly()) {
    //Indicator whether submitted values should be considered or not
%><input type="hidden" name="<%= RequestParameterConstants.VALIDATE_CSTIC_VALUES %>_<%= characteristicUIBean.getName() %>" value="T"><%
    }
%><table><%
	%><tr><%
	 	// decide: indentation of values for mimes necessary on cstic-level;
		// further decision will be done in characteristicValue.jsp/CharacteristicValueUI on value-level
		if (CharacteristicUI.indentValues(characteristicGroup, characteristicUIBean, uiContext.getPropertyAsBoolean(RequestParameterConstants.CSTIC_INDENTATION), showExpandMode)) {
		%><td valign='middle' width="80"><%
            // get the appropriate mime for the work-area: icon, o2c, sound (see ValueUIBean)
            MimeUIBean mime = characteristicUIBean.getMimeForWorkArea(showExpandMode);
            // get the enlarged Mime for this csic
            MimeUIBean enlargedMime = characteristicUIBean.getEnlargedMime();
            ArrayList mimes = characteristicUIBean.getMimes();
            // include mimeObject.jsp only if mime is not null-object
            if (mime != MimeUIBean.C_NULL) {
                MimeObjectUI.include(pageContext,
                                               uiContext.getJSPInclude("tiles.mimeObject.jsp"),
                                               uiContext,
                                               mimes,
                                               mime,
                                               enlargedMime,
                                               currentInstance,
                                               characteristicGroup,
                                               customerParams);
            }
        %></td><%
		}
		%><td valign='middle'><%
	boolean readOnlyMode = uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE, false);
    boolean showValueDescription = (uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndLongDescriptionTextId) || 
       uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortAndLongDescriptionTextId) ||
       uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.shortAndLongDescriptionTextId));

    CharacteristicValuesControllerUI.include(pageContext,
                             uiContext.getJSPInclude("tiles.characteristicValues.jsp"),
                             uiContext,
                             currentInstance,
                             characteristicGroup,
                             characteristicUIBean,
                             showExpandMode,
                             showValueDescription,
                             csticsArea,
                             readOnlyMode,
                             false,
                             customerParams);
		%></td><%
	%></tr><%
%></table><%
	if (ui.isAccessible())
	{
%>
<a 
	href="<%=grpBeginHref%>"
	id="<%=grpEndId%>"
	title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>" 
></a>	
<%
	}
%><%                             
}
%>