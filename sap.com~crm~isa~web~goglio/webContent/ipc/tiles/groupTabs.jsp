<%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.StringUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.StatusImage"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.GroupTabsUI" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/groupTabs.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
String hasConflict = WebUtil.translate(pageContext, "ipc.has_conflict", null);
String hasNoConflict = WebUtil.translate(pageContext, "ipc.has_noconflict", null);
String hasRequiredCstic = WebUtil.translate(pageContext, "ipc.hasrequiredcstic", null);
String tabSelectionKey = "access.tab.unselected";
String currentTabTitle = "";
String totalNoOfTabs = "";
String currentTab = "1";
%><%
GroupTabsUI.IncludeParams includeParams = GroupTabsUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
InstanceUIBean instance = includeParams.getInstance();
GroupUIBean characteristicGroup = includeParams.getGroup(); // current selected group
Hashtable customerParams = includeParams.getCustomerParams();

List characteristicGroups = instance.getGroups(); // list of all groups
// get the first group in the scroll area (i.e. that is currently shown)
GroupUIBean firstGroupInScrollArea = GroupTabsUI.getFirstGroupInScrollArea(characteristicGroups);
// the list of the chosen groups according to the given number how many groups are to be shown.
//int groupQuantity = uiContext.getPropertyAsInteger( RequestParameterConstants.GROUP_QUANTITY );
int groupQuantity = 3;

if (characteristicGroups != null)
{
	if (characteristicGroups.size() > groupQuantity)
	{
		totalNoOfTabs = Integer.toString(characteristicGroups.size());
	}
	else
	{
		totalNoOfTabs = Integer.toString(characteristicGroups.size());
	}
}

UIArea groupArea = new UIArea("groups", uiContext);
// the list of the chosen groups according to the given number how many groups are to be shown.
%>
<%            
	String tabTxt = WebUtil.translate(pageContext, "ipc.groups.tabtitle", null); 
	if (ui.isAccessible()&& !(characteristicGroups == null || (characteristicGroups.isEmpty()) || (characteristicGroups.size() <= 1)))
	{
%>
			<a 
				href="#tabcsticgroups-end"
				id="tabcsticgroups-begin"
				title="<isa:translate key="access.tab.begin" arg0="<%=tabTxt%>" arg1="1"/>" 
			></a>
<%
	}
%>
<table border="0" cellspacing="0" cellpadding="0" width="100%"><% 
	%><tr style="height:18px"><%


		// if there are no groups or only one group, don't show tabs; (id=1)
		if (characteristicGroups == null || (characteristicGroups.isEmpty())
			|| (characteristicGroups.size() <= 1)){
		%><td><%
		%></td><%
		}
		else { // (id=1)
			int widthGroup = 0;

			currentTabTitle = firstGroupInScrollArea.getLanguageDependentName(pageContext);
			if (firstGroupInScrollArea.isConsistent())
			{
				if (firstGroupInScrollArea.isRequired())
				{
					currentTabTitle = WebUtil.translate(pageContext, "ipc.cstictab.title", new String[] {currentTabTitle, hasNoConflict, hasRequiredCstic});
				}
				else
				{
					currentTabTitle = WebUtil.translate(pageContext, "ipc.cstictab.title", new String[] {currentTabTitle, hasNoConflict, ""});
				}
			}
			else
			{
				if (firstGroupInScrollArea.isRequired())
				{
					currentTabTitle = WebUtil.translate(pageContext, "ipc.cstictab.title", new String[] {currentTabTitle, hasConflict, hasRequiredCstic});
				}
				else
				{
					currentTabTitle = WebUtil.translate(pageContext, "ipc.cstictab.title", new String[] {currentTabTitle, hasConflict, ""});
				}
			}
			// define the first image of the first tab
			// (the triangle which opens the tab or connecting image)
			HashMap tabInfo = GroupTabsUI.getStartInfoOfTabs(characteristicGroups, 
                                            				firstGroupInScrollArea,
                                            				characteristicGroup,
                                            				uiContext);
			String width = (String)tabInfo.get("width");
			String title = (String)tabInfo.get("title");
			String imageName = "ipc/mimes/images/sys/" + (String)tabInfo.get("imageName");
        %><td width="<%= width %>" title="<isa:translate key="<%= title %>" arg0="<%=currentTabTitle%>" arg1="<%=currentTab%>" arg2="<%=totalNoOfTabs%>" arg3="<%=currentTabTitle%>"/>"><%
            %><img src='<isa:mimeURL name="<%= imageName %>" />' border="0" alt=""><%
        %></td><%
			
			// here starts the loop over the tabs to be displayed
			GroupUIBean currentProcessingGroup = (GroupUIBean) characteristicGroups.get(0);
			for (int counter=0; counter<characteristicGroups.size(); counter++) { // (id=18)
				currentProcessingGroup = (GroupUIBean) characteristicGroups.get(counter);
				if (!currentProcessingGroup.isInScrollArea()){
				    continue;
				}
				// the title for accessibility is generated
				currentTabTitle = currentProcessingGroup.getLanguageDependentName(pageContext);
				if (currentProcessingGroup.isConsistent())
				{
					if (currentProcessingGroup.isRequired())
					{
						currentTabTitle = WebUtil.translate(pageContext, "ipc.cstictab.title", new String[] {currentTabTitle, hasNoConflict, hasRequiredCstic});
					}
					else
					{
						currentTabTitle = WebUtil.translate(pageContext, "ipc.cstictab.title", new String[] {currentTabTitle, hasNoConflict, ""});
					}
				}
				else
				{
					if (currentProcessingGroup.isRequired())
					{
						currentTabTitle = WebUtil.translate(pageContext, "ipc.cstictab.title", new String[] {currentTabTitle, hasConflict, hasRequiredCstic});
					}
					else
					{
						currentTabTitle = WebUtil.translate(pageContext, "ipc.cstictab.title", new String[] {currentTabTitle, hasConflict, ""});
					}
				}
				currentTab = Integer.toString(counter+1);

				// define the width of the <td> which includes the background-image of the
				// tab depending on the number of displayed groups
				if (groupQuantity>0 && characteristicGroups.size()>groupQuantity) {
					widthGroup = 100 / (groupQuantity + 2);
				}
				else {
					widthGroup = 12;
				}

				// define the background-image of the tabs (the part of the tabs where the text is shown)
				String backgroundImage = null;
				String styleClass = null; 

				if (currentProcessingGroup.isConsistent() || uiContext.getShowStatusLights()) {
					if ((counter == 0 && characteristicGroup == null)
						|| (characteristicGroup != null && currentProcessingGroup.getName().equals(characteristicGroup.getName()))) {
						backgroundImage = "ipc/mimes/images/sys/backon.gif";
						styleClass = "ipcActiveGroupTab";
						tabSelectionKey = "access.tab.selected";
					}
					else {
						backgroundImage = "ipc/mimes/images/sys/backoff.gif";
						styleClass = "ipcInactiveGroupTab";
						tabSelectionKey = "access.tab.unselected";
					}
				}
				else {
					// if the group is conflicting and showStatusLights = F, then colour the background red
					backgroundImage = "ipc/mimes/images/sys/backconflict.gif";
					styleClass = "ipcConflictGroupTab";
					tabSelectionKey = "access.tab.unselected";
					if (characteristicGroup != null && 
						currentProcessingGroup.getName().equals(characteristicGroup.getName()))
						{
						tabSelectionKey = "access.tab.selected";
						}
				}
		%><td class="<%= styleClass %>" style='background-repeat:repeat-x;background-image:url(<isa:mimeURL name="<%=backgroundImage%>" />)' nowrap="nowrap" width="<%= widthGroup %>%"
		title="<isa:translate key="<%=tabSelectionKey%>" arg0="<%=currentTabTitle%>" arg1="<%=currentTab%>" arg2="<%=totalNoOfTabs%>" arg3="<%=currentTabTitle%>"/>"><%
			String instanceId = instance.getId();
//				if (!uiContext.getCurrentConfiguration().isSingleLevel() && instance.getId() != null) {
//					instanceId = instance.getId();
//				}
				String action = WebUtil.getAppsURL(pageContext, null, "/ipc/changeCharacteristicGroup.do", null, null, false);
				String groupName = currentProcessingGroup.getName();
				String scrollGroupName = firstGroupInScrollArea.getName();
				// add the shortcut key (as dummy-link to avoid automatic submit using Firefox) if it is the first tab that is currently shown 
				String accessKeyAttribute = "";
				if (groupName.equals(scrollGroupName)) {
					String groupAreaAccessKey = groupArea.getShortcutKey();                        
					accessKeyAttribute = " accesskey=\"" + WebUtil.translate(pageContext, groupAreaAccessKey, null) + "\" ";
			%><a href="#" <%= accessKeyAttribute %> tabindex="<%= groupArea.getTabIndex()%>"></a><%
				}				
			%><a href="javascript:onGroupChange('<%= instanceId %>', '<%= groupName %>', '<%= scrollGroupName %>', '<%= action %>');" tabindex="<%= groupArea.getTabIndex()%>"><%
				%><%= currentProcessingGroup.getLanguageDependentName(pageContext) %><%
				// show the star if the group has required characteristics
				// if showStatusLights = T don't show the star
				if (currentProcessingGroup.isRequired() && !uiContext.getShowStatusLights()) {
					%><isa:translate key="ipc.group.required.sign"/><%
				}
			%></a><%
		%></td><%
				// if showStatusLights = T show the statusLights for required or conflicting
				// groups instead of the star and the red background
				if (uiContext.getShowStatusLights()) { // (id=11)
		%><td background='<isa:mimeURL name="<%=backgroundImage%>" />' style="background-repeat:repeat-x" width="35"><%
			StatusImage statusImage = currentProcessingGroup.getStatusImage();
			%><img alt="<isa:translate key="<%= statusImage.getResourceKey() %>"/>" src="<isa:mimeURL name="<%= statusImage.getImagePath() %>" />" border="0" alt=""><%
		%></td><%
				} // (id=11)

                // get the information for the connection image of the tab (that could 
                // also be the end-image if there are no more groups)
                HashMap connectionInfo = GroupTabsUI.getConnectionInfoOfTabs(characteristicGroups, 
                                                                        currentProcessingGroup,
                                                                        characteristicGroup,
                                                                        uiContext);     
                String connectionWidth = (String)connectionInfo.get("width");
                String connectionTitle = (String)connectionInfo.get("title");
                String connectionImageName = "ipc/mimes/images/sys/" + (String)connectionInfo.get("imageName");                                                           
        // show image (either connection- or end-image)
        %><td width="<%= connectionWidth %>" title="<isa:translate key="<%= connectionTitle %>" arg0="<%=currentTabTitle%>" arg1="<%=currentTab%>" arg2="<%=totalNoOfTabs%>" arg3="<%=currentTabTitle%>"/>"><%
            %><img src='<isa:mimeURL name="<%= connectionImageName %>" />' border="0" alt=""><%
        %></td><%
			} // end of for (id=18)

		/*********************************************************************************************/
		// Here starts the code for the scrolling of the group tabs.
		int widthScroll = 100 - (widthGroup * groupQuantity);

		// show the scrolling only if there are more groups than the number of displayed groups; id=2
		if (groupQuantity > 0 && characteristicGroups.size() > groupQuantity) {
		%><td align="right" valign="top" width="<%= widthScroll %>%"><%
			%><table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-size:10px;"><%
				%><tr><%
					%><td valign="top"><%
						%><table border="0" cellspacing="0" style="font-size:10px;"><%
							%><tr><%
								// define the targets of the scroll-Buttons
								// (first / last group, one step forward, one step backward)
								GroupUIBean firstShownGroup = firstGroupInScrollArea;
								int firstShownGroupIndex = characteristicGroups.indexOf(firstShownGroup);
								GroupUIBean firstGroup = (GroupUIBean) characteristicGroups.get(0);
								GroupUIBean scrollLeft = (GroupUIBean) characteristicGroups.get(0);
								if (firstShownGroupIndex > 0) {
									scrollLeft = (GroupUIBean) characteristicGroups.get(firstShownGroupIndex - 1);
								}
								GroupUIBean scrollRight = (GroupUIBean) characteristicGroups.get(characteristicGroups.size() - 1);
								if ((firstShownGroupIndex + groupQuantity) < (characteristicGroups.size() - 1)) {
									scrollRight = (GroupUIBean) characteristicGroups.get(firstShownGroupIndex + 1);
								}
								GroupUIBean lastGroup = (GroupUIBean) characteristicGroups.get((characteristicGroups.size() - 1));
								%><td valign="top" nowrap="nowrap" title="<isa:translate key='ipc.group_first'/>"><%
								{
									// define the links for the 4 scroll-arrows
									// deactivate the first arrow, if the first group is actually shown
									if (firstShownGroup != null && firstGroup.equals(firstShownGroup)) {
									%><div><%
										%>&nbsp;<%
										%><img src='<isa:mimeURL name="ipc/mimes/images/sys/seitennav_links_inactive.gif" />' width="7" style="height:12" border="0" alt="<isa:translate key='ipc.group_first'/>"><%
										%>&nbsp;<%
									%></div><%
									}
									else {
											//String action = WebUtil.getAppsURL(pageContext, null, "/ipc/getCharacteristicGroups.do", null, null, false);
											String instanceId = instance.getId();
//											if (!uiContext.getCurrentConfiguration().isSingleLevel() && instance.getId() != null) {
//												instanceId = instance.getId();
//											}
											String action = WebUtil.getAppsURL(pageContext, null, "/ipc/changeCharacteristicGroup.do", null, null, false);
											String forwardDestination = "groupTabs";
											String selectedGroup = characteristicGroup.getName() ;
											String scrollGroupName = firstGroup.getName();
											String target = "groupTabs";
									%><a href="javascript:onGroupScroll('<%= instanceId %>', '<%= selectedGroup %>', '<%= scrollGroupName %>', '<%= action %>');" style="text-decoration:none" tabindex="<%= groupArea.getTabIndex()%>">&nbsp;<%
										%><img src='<isa:mimeURL name="ipc/mimes/images/sys/seitennav_links.gif" />' width="7" style="height:12" border="0" alt="<isa:translate key='ipc.group_first'/>"><%
										%>&nbsp;<%
									%></a><%
									}
								}
								%></td><%
								%><td valign="top" nowrap="nowrap" title="<isa:translate key='ipc.group_previous'/>"><%
								{
									if (firstShownGroup != null && firstGroup.equals(firstShownGroup)) {
									%><div><%
										%><img src='<isa:mimeURL name="ipc/mimes/images/sys/seitennav_links_klein_inactive.gif" />' width="6" style="height:12" border="0" alt="<isa:translate key='ipc.group_previous'/>"><%
										%>&nbsp;<%
									%></div><%
									}
									else {
										//String action = WebUtil.getAppsURL(pageContext, null, "/ipc/getCharacteristicGroups.do", null, null, false);
										String instanceId = instance.getId();
//										if (!uiContext.getCurrentConfiguration().isSingleLevel() && instance.getId() != null) {
//											instanceId = instance.getId();
//										}
										String action = WebUtil.getAppsURL(pageContext, null, "/ipc/changeCharacteristicGroup.do", null, null, false);
										String forwardDestination = "groupTabs";
										String selectedGroup = characteristicGroup.getName();
										String scrollGroupName = scrollLeft.getName();
										String target = "groupTabs";
									%><a href="javascript:onGroupScroll('<%= instanceId %>', '<%= selectedGroup %>', '<%= scrollGroupName %>', '<%= action %>', '<%= forwardDestination %>', '<%= target %>');" style="text-decoration:none" tabindex="<%= groupArea.getTabIndex()%>"><%
										%><img src='<isa:mimeURL name="ipc/mimes/images/sys/seitennav_links_klein.gif" />' width="6" style="height:12" border="0" alt="<isa:translate key='ipc.group_previous'/>"><%
										%>&nbsp;<%
									%></a><%
									}
								}
								%></td><%
								%><td valign="top" nowrap="nowrap"  title="<isa:translate key='ipc.group_next'/>"><%
								if (characteristicGroups.get(firstShownGroupIndex).equals(characteristicGroups.get(characteristicGroups.size() - groupQuantity ))) {
									%><div><%
										%><img src='<isa:mimeURL name="ipc/mimes/images/sys/seitennav_rechts_klein_inactive.gif" />' width="6" style="height:12" border="0" alt="<isa:translate key='ipc.group_next'/>"><%
										%>&nbsp;<%
									%></div><%
								}
								else {
									//String action = WebUtil.getAppsURL(pageContext, null, "/ipc/getCharacteristicGroups.do", null, null, false);
									String instanceId = instance.getId();
//									if (!uiContext.getCurrentConfiguration().isSingleLevel() && instance.getId() != null) {
//										instanceId = instance.getId();
//									}
									String action = WebUtil.getAppsURL(pageContext, null, "/ipc/changeCharacteristicGroup.do", null, null, false);
									String forwardDestination = "groupTabs";
									String selectedGroup = characteristicGroup.getName();
									String scrollGroupName = scrollRight.getName();
									String target = "groupTabs";
									%><a href="javascript:onGroupScroll('<%= instanceId %>', '<%= selectedGroup %>', '<%= scrollGroupName %>', '<%= action %>', '<%= forwardDestination %>', '<%= target %>');" style="text-decoration:none" tabindex="<%= groupArea.getTabIndex()%>"><%
										%><img src='<isa:mimeURL name="ipc/mimes/images/sys/seitennav_rechts_klein.gif" />' width="6" style="height:12" border="0" alt="<isa:translate key='ipc.group_next'/>"><%
										%>&nbsp;<%
									%></a><%
								}
								%></td><%
								%><td valign="top" nowrap="nowrap" title="<isa:translate key='ipc.group_last'/>"><%
								// deactivate the last arrow, if the last group is actually shown
								if (characteristicGroups.get(firstShownGroupIndex).equals(characteristicGroups.get(characteristicGroups.size() - groupQuantity ))) {								
									%><div><%
										%><img src='<isa:mimeURL name="ipc/mimes/images/sys/seitennav_rechts_inactive.gif" />' width="7" style="height:12" border="0" alt="<isa:translate key='ipc.group_last'/>"><%
										%>&nbsp;<%
									%></div><%
								}
								else {
									//String action = WebUtil.getAppsURL(pageContext, null, "/ipc/getCharacteristicGroups.do", null, null, false);
									String instanceId = instance.getId();
//									if (!uiContext.getCurrentConfiguration().isSingleLevel() && instance.getId() != null) {
//										instanceId = instance.getId();
//									}									
									String action = WebUtil.getAppsURL(pageContext, null, "/ipc/changeCharacteristicGroup.do", null, null, false);
									String forwardDestination = "groupTabs";
									String selectedGroup = characteristicGroup.getName();
									String scrollGroupName = lastGroup.getName();
									String target = "groupTabs";
									%><a href="javascript:onGroupScroll('<%= instanceId %>', '<%= selectedGroup %>', '<%=scrollGroupName %>', '<%= action %>', '<%= forwardDestination %>', '<%= target %>');" style="text-decoration:none" tabindex="<%= groupArea.getTabIndex()%>"><%
										%><img src='<isa:mimeURL name="ipc/mimes/images/sys/seitennav_rechts.gif" />' width="7" style="height:12" border="0" alt="<isa:translate key='ipc.group_last'/>"><%
									%></a>&nbsp;<%
								}
								%></td><%
								%><td style="padding:0px; vertical-align:top; font-size:10px;" nowrap="nowrap"><%
								{
									// drop-down-list for group-selection
									String instanceId = instance.getId();
//									if((!uiContext.getCurrentConfiguration().isSingleLevel()) && (instance.getId() != null)) {
//										instanceId = instance.getId();
//									}
									String action = WebUtil.getAppsURL(pageContext, null, "/ipc/changeCharacteristicGroup.do", null, null, false);
									UIArea groupListArea = new UIArea("grouplist", uiContext);
									String groupListAreaAccessKey = groupListArea.getShortcutKey();                        
									%><%if(ui.isAccessible()){%><label for="cstic_group_tab_id"><isa:translate key="ipc.csticgrp.choose"/></label><%}%>
									<select id="cstic_group_tab_id" name="<%= Constants.CURRENT_CHARACTERISTIC_GROUP_NAME %>" onChange="javascript:onGroupChange('<%= instanceId %>', this.options[this.selectedIndex].value, this.options[this.selectedIndex].value, '<%= action %>');" accesskey="<isa:translate key="<%= groupListAreaAccessKey %>"/>" tabindex="<%= groupListArea.getTabIndex()%>"><%
									String groupName = "";
									for (int i=0; i<characteristicGroups.size(); i++) { // id=4
										GroupUIBean characteristicGroupInList = (GroupUIBean) characteristicGroups.get(i);
										if (characteristicGroup.getName().equals(characteristicGroupInList.getName())) {
										%><option value="<%= characteristicGroupInList.getName() %>" selected><%
										} else {
										%><option value="<%= characteristicGroupInList.getName() %>"><%
										}
										if (characteristicGroupInList.isBaseGroup()) {
											%><isa:translate key="ipc.defaultgroup"/><%
										} else {
											%><%= StringUtil.encodeLanguageDependentName(characteristicGroupInList.getName(), uiContext) %><%
										}
										if (!characteristicGroupInList.isConsistent()){
										    // here you can add a symbol that represents inconsistency if you want (e.g. using resouce key ipc.group.inconsistent.sig)
										}
										if (characteristicGroupInList.isRequired()){
											%>&nbsp;<isa:translate key="ipc.group.required.sign"/><%
										}																				
										%></option><%
									} // end for (id=4)
									%></select><%
									// END of drop-down-list for group-selection
								}
									%>&nbsp;<%
								%></td><%
							%></tr><%
						%></table><%
					%></td><%
				%></tr><%
			%></table><%
		%></td><%                                                                                               
		}
		// No Groups -----------------------------------------------------------------------
		else { // (id=2)
		%><td><%
			%>&nbsp;<%
		%></td><%
		} // end of 'if groupQuantity' (id=2)
		} //end of if (id=1)
	%></tr><%
%></table><%
//table for blue line below tabs 
%><table class="LineNavTabOrganizer" width="100%" border="0" cellspacing="0" cellpadding="0"><%
	%><tr><%
		%><td><% 
		%></td><%
	%></tr><%
%></table>
<%
%>