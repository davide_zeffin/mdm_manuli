<%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblGroupUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblCsticUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/comparisonTblGroup.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
ComparisonTblGroupUI.IncludeParams includeParams = ComparisonTblGroupUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
GroupDeltaUIBean grp = includeParams.getGroupDeltaBean();
Hashtable customerParams = includeParams.getCustomerParams();

%><tr><%
    %><td class="configObjectsGroup" title="<isa:translate key="ipc.compare.tt.group" />"><%
        %><%= grp.getLanguageDependentName(pageContext) %><%
    %></td><%
    %><td class="statusIconGroup"><%
        %>&nbsp;<%
    %></td><%
    %><td class="deltaGroup"><%
        %>&nbsp;<%
    %></td><%
    %><td class="deltaGroup"><%
        %>&nbsp;<%
    %></td><%
    %><td class="deltaGroup"><%
        %>&nbsp;<%
    %></td><%
%></tr><%   
// loop over characteristic deltas  
List cstics = grp.getCharacteristics();
for (int k=0; k<cstics.size(); k++){ // id=4
    CharacteristicDeltaUIBean cstic = (CharacteristicDeltaUIBean) cstics.get(k);
	ComparisonTblCsticUI.include(pageContext,
	                       uiContext.getJSPInclude("tiles.comparisonTblCstic.jsp"),
	                       uiContext,
	                       cstic,
	                       customerParams);
} // id=4
%>