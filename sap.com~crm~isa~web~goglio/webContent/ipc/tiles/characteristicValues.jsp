<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesControllerUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/characteristicValues.jsp"/><%

	//THIS INCLUDE CONTAINS THE UI OF ALL AVAILABLE CHARACTERISTIC VALUES
    //OF ONE SINGLE CHARACTERISTIC
    //using the object "characteristicValue" you can get all the
    //information about the value
    //using the object "currentCharacteristic" you can get all the
    //information about the characteristic
    //parameters passed from outside
    CharacteristicValuesControllerUI.IncludeParams parameters = CharacteristicValuesControllerUI.getIncludeParams(pageContext);
    UIContext uiContext = parameters.getIpcBaseUI();
    InstanceUIBean currentInstance = parameters.getInstance();
    GroupUIBean currentGroup = parameters.getGroup();
    CharacteristicUIBean currentCharacteristic = parameters.getCurrentCharacteristic();
    boolean showExpandMode = parameters.isShowExpandMode();
    boolean showDescriptions = parameters.isShowDescriptions();
    UIArea csticsArea = parameters.getUiArea();
    boolean readOnlyMode = parameters.isReadOnlyMode();
	boolean detailsMode = parameters.isDetailsMode();    
    Hashtable customerParams = parameters.getCustomerParams();


    String valuesTile = "";
    List values = currentCharacteristic.getValues();
    if (values.size() > 0) {
	    if( showExpandMode == true || currentCharacteristic.hasIntervalAsDomain()){
	    	valuesTile = uiContext.getJSPInclude("tiles.characteristicValuesAsSingleOption.jsp");            
	    } else {
        	valuesTile = uiContext.getJSPInclude("tiles.characteristicValuesInADropDownBox.jsp");
	    }
    }
    else {
        // fallback if layout is not correctly determined (can be removed as soon as layout determination works
        valuesTile = uiContext.getJSPInclude("tiles.characteristicValuesInADropDownBox.jsp");
    }
    String layout = currentCharacteristic.determineLayout();
    String freeTextTile = uiContext.getJSPInclude("tiles.freetext.jsp");
    String intervalTile = uiContext.getJSPInclude("tiles.interval.jsp");
    
    CharacteristicValuesUI.include(pageContext,
        layout,
        valuesTile,
        freeTextTile,
        intervalTile,
        uiContext,
        currentInstance,
        currentGroup,
        currentCharacteristic,
        values,
        showDescriptions,
        csticsArea,
        readOnlyMode,
        detailsMode,
        customerParams);  
%>    