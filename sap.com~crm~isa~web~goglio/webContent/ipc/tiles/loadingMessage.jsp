<%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.LoadingMessageUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.LoadingMessageUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.StatusImage" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/loadingMessage.jsp"/><%
LoadingMessageUI.IncludeParams includeParams = LoadingMessageUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
LoadingMessageUIBean message = includeParams.getMessage();
Hashtable customerParams = includeParams.getCustomerParams();
CharacteristicUIBean csticDelta = includeParams.getCsticDelta();
String text = message.getText();
StatusImage img = message.getStatusImage();
%><table class="ipcMessages"><%
	%><tr><%
        %><td class="statusIcon" title="<isa:translate key="ipc.compare.tt.icon.loadingmessage" />"><%
            %><img style="display:inline" src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
        %></td><%
        %><td class="messageShortText"><%
        	%><%= message.getText() %><%
        %></td><%        
    %></tr><%
%></table>