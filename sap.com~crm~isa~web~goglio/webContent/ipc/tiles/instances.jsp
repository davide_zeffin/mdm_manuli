<%-- this include contains the UI of all available instances in the configuration
     to collapse and expand tree nodes, the next input field will
     be filled with the name of the clicked instance  --%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.InstanceUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.InstancesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.logging.IsaLocation"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/instances.jsp"/><%
%><%! private static IsaLocation loc = IsaLocation.getInstance("instances.jsp");%><%
BaseUI ui = new BaseUI(pageContext);
InstancesUI.IncludeParams includeParams = InstancesUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
List instanceList = includeParams.getInstances();
InstanceUIBean instance = includeParams.getInstance();
GroupUIBean currentCharacteristicGroup = includeParams.getGroup();
Hashtable customerParams = includeParams.getCustomerParams();
UIArea instancesArea = new UIArea("instances", uiContext);
String accessKey = instancesArea.getShortcutKey();
%><input type="hidden" name="<%= Constants.INSTANCE_TREE_STATUS_CHANGE %>" value=""/><%
%><table class="ipc"><%
    %><tr><%    
        %><td colspan="2"><%
            %><a href="#" style="text-decoration: none; color: #000;" accesskey="<isa:translate key="<%= accessKey %>"/>" title="<isa:translate key="ipc.area.instances"/>" tabindex="<%= instancesArea.getTabIndex()%>"><isa:translate key="ipc.instances"/></a><%
        %></td><%
        %><td><%
            %><isa:translate key="ipc.quantity"/><%
        %></td><%
    %></tr><%
    for (int counter = 0; counter < instanceList.size(); counter++) { // (id=1)
        InstanceUIBean currentInstance = (InstanceUIBean) instanceList.get(counter);
        //check which of the instances in the list is the original instance
        if ((currentInstance != null && instance != null) && currentInstance.getId().equals(instance.getId())){
            currentInstance.setSelected(true);
        }
        else {
            currentInstance.setSelected(false);
        }
        // First of all we create the graphical tree node for the instance, if
        // there is more than just one instance
//TODO: remove
//         GroupUIBean currentCharacteristicGroup = (GroupUIBean) currentInstance.getBusinessObject().getCharacteristicGroups(
//         uiContext.getShowInvisibleCharacteristics(),
//         uiContext.getPropertyAsBoolean( InternalRequestParameterConstants.SHOW_GENERAL_TAB_FIRST)).get(0);
//            if (currentInstance.getBusinessObject().getCharacteristicGroups().size() > 1) {
//the group to be displayed is to be determined in the GetCharacteristicGroupsAction
//                // set currentCharacteristicGroup to the first group with visible characteristics
//                boolean groupWithVisibleCsticsFound = false;
//                for (int i=0; i<currentInstance.getBusinessObject().getCharacteristicGroups().size() && !groupWithVisibleCsticsFound; i++) {
//                    GroupUIBean
//                        group = (GroupUIBean) currentInstance.getBusinessObject().getCharacteristicGroups(
//                                    uiContext.getShowInvisibleCharacteristics(),
//                                    uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.SHOW_GENERAL_TAB_FIRST)).get(i);
//                    if (!group.getCharacteristics().isEmpty()) {
//                        for (int j=0; j<group.getCharacteristics().size(); j++) {
//                            Characteristic charac = (Characteristic) group.getCharacteristics().get(j);
//                            if( charac.isVisible()) {
//                                groupWithVisibleCsticsFound = true;
//                                currentCharacteristicGroup = group;
//                            }
//                        }
//                    }
//                }
//            }
            Hashtable instanceTreeStatus = (Hashtable) uiContext.getProperty(Constants.INSTANCE_TREE_STATUS_CHANGE);
            InstanceUIBean recInstance = currentInstance;
            int instanceLevel = 0;
            while (!recInstance.getBusinessObject().isRootInstance() 
                && (!(instanceTreeStatus.get(recInstance.getId()) != null && instanceLevel > 0))) {
                instanceLevel++;
                recInstance = recInstance.getParent();
            }
            // Continue if the currentInstance isn't child or subchild of a collapsed instance
            if ((instanceTreeStatus.get(recInstance.getId()) == null)
                || (instanceTreeStatus.get(recInstance.getId()) != null && instanceLevel == 0)) { // (id=3)
                // here begins the layout of one single instance
                // each instance should be shown in an extra row
                // the layout should be of the table header data
    %>
	<%
	if (ui.isAccessible())
	{
		String treeTxt = currentInstance.getLanguageDependentName();
		String grpBeginId = "tree_" + currentInstance.getName() + "-begin";
		String grpEndId = "tree_" + currentInstance.getName() + "-end";
		String grpBeginHref = "#" + grpBeginId;
		String grpEndHref = "#" + grpEndId;
	%>
<a 
	href="<%=grpEndHref%>"
	id="<%=grpBeginId%>"
	title="<isa:translate key="access.tree.begin" arg0="<%=treeTxt%>"/>" 
></a>
	<%
	}
	%>
    <tr><%
        %><td><%
            %><table><%
                %><tr><%
                for (int i=0; i<instanceLevel; i++) {
                    %><td width="1"><%
                        %><img style="display:inline" src='<isa:mimeURL name="ipc/mimes/images/sys/spacer.gif"/>' width="1" alt=""><%
                    %></td><%
                }
                    %><td style="height:19" width="57"><%
                // Now show the correct folder for this instance
                String folderImage;
                String nodeState;
				String levelTxt = WebUtil.translate(pageContext, "ipc.config.level", new String[] { Integer.toString(instanceLevel) });
                boolean hasVisibleChildren = false;
                java.util.Iterator children = currentInstance.getChildren().iterator();
                while (!hasVisibleChildren && children.hasNext()) {
                    InstanceUIBean theChild = (InstanceUIBean) children.next();
                    if (theChild != null) {
                        hasVisibleChildren = !theChild.isClosed();
                    }
                }
                if (hasVisibleChildren) { // (id=4)
                    if (instanceTreeStatus.get(currentInstance.getId()) != null) {
                        folderImage = "ipc/mimes/images/sys/closed.gif";
                        nodeState = "access.tree.closed";
                    }
                    else {
                        folderImage = "ipc/mimes/images/sys/open.gif";
						nodeState = "access.tree.open";
                    }
                    //click on tree node in order to change the status of the tree
                    String action = "";
                    action = WebUtil.getAppsURL(pageContext,
                                     null,
                                     ActionForwardConstants.EXPAND_INSTANCE,
                                     null,
                                     null,
                                     false);
                    StringBuffer hrefBuffer = new StringBuffer();
                    hrefBuffer.append("javascript:onInstanceExpandOrCollaps(");
                    hrefBuffer.append("'").append(currentInstance.getId()).append("',");
                    hrefBuffer.append("'").append(currentInstance.getId()).append("',");
                    hrefBuffer.append("'").append(currentCharacteristicGroup.getName()).append("',");
                    hrefBuffer.append("'").append(action).append("');");
                    String href = hrefBuffer.toString();
                        %><a class="tree" href="<%= href %>" tabindex="<%= instancesArea.getTabIndex()%>" title="<isa:translate key="<%=nodeState%>" arg0="<%=levelTxt%>"/>"><%
                            %><img border="0" src='<isa:mimeURL name="<%=folderImage%>"/>' alt=""><%
                        %></a><%
                }
                else { // (id=4)
                        %><img src='<isa:mimeURL name="ipc/mimes/images/sys/leaf.gif"/>' alt="<isa:translate key="access.tree.leaf"/>"><%
                } // (id=4)
                // If the user clicks on a folder, the associated instance becomes selected and
                // the tree will expand or collapse according to the current status.
                    %></td><%
                %></tr><%
            %></table><%
        %></td><%
                InstanceUI.include(pageContext,
                                    uiContext.getJSPInclude("tiles.instance.jsp"),
                                    uiContext,
                                    currentInstance,
                                    currentCharacteristicGroup,
                                    instancesArea,
                                    customerParams); 
    %></tr>
	<%
	if (ui.isAccessible())
	{
		String treeTxt = currentInstance.getLanguageDependentName();
		String grpBeginId = "tree_" + currentInstance.getName() + "-begin";
		String grpEndId = "tree_" + currentInstance.getName() + "-end";
		String grpBeginHref = "#" + grpBeginId;
		String grpEndHref = "#" + grpEndId;
	%>
<a 
	href="<%=grpBeginHref%>"
	id="<%=grpEndId%>"
	title="<isa:translate key="access.tree.end" arg0="<%=treeTxt%>"/>" 
></a>
	<%
	}
	%>
    <%
            } // (id=3)
    } // (id=1)
    %><tr><%    
        %><td height="20px" colspan="4"><%
            %><img src='<isa:mimeURL name="ipc/mimes/images/sys/spacer.gif"/>' height="20" alt=""><%
        %></td><%
	%></tr><%
%></table>