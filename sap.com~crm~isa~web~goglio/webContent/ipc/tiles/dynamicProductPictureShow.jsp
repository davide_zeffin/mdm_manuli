<%-- this include shows the pictures of the dynamicProductPictures --%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.DynamicProductPictureShowUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ValueUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/dynamicProductPictureShow.jsp"/><%

DynamicProductPictureShowUI.IncludeParams includeParams = DynamicProductPictureShowUI.getIncludeParams(pageContext);
InstanceUIBean instance = includeParams.getInstance();
UIContext uiContext = includeParams.getIpcBaseUI();
String[] imgId = includeParams.getImgIds();
String[] imgSrc = includeParams.getImgSrcs();
String[] imgStyle = includeParams.getImgStyles();
String[] imgSize = includeParams.getImgSizes();
Hashtable customerParams = includeParams.getCustomerParams();

    for (int i = 0; i < imgId.length; i++) { // (id=2)
        if (imgId[i] == null) {
            continue;
        }
        for (Iterator csticIt = instance.getCharacteristics().iterator(); csticIt.hasNext();) { // (id=3)
        	CharacteristicUIBean cstic = (CharacteristicUIBean)csticIt.next();
        	for(Iterator valueIt = cstic.getAssignedValues().iterator(); valueIt.hasNext(); ) { // (id=3.1)
        		ValueUIBean value = (ValueUIBean)valueIt.next();
                StringBuffer dynamicProductPictureShowCsticIdentifierSB = new StringBuffer(cstic.getName());
                dynamicProductPictureShowCsticIdentifierSB.append(".");
                dynamicProductPictureShowCsticIdentifierSB.append(value.getName());
                String dynamicProductPictureShowCsticIdentifier = dynamicProductPictureShowCsticIdentifierSB.toString();
                if (dynamicProductPictureShowCsticIdentifier == null) {
                    continue;
                }
                if (!imgId[i].equals(dynamicProductPictureShowCsticIdentifier)) {
                    continue;
                }
		        String masterdataURL = uiContext.getMasterdataMimesURL();
				String mimeURL = MasterdataUtil.getMimeURL(pageContext, masterdataURL, null, null, imgSrc[i]);
                
%><div id="<%= imgId[i] %>" style="<%= imgStyle[i] %>"><%
    %><img src='<%= mimeURL %>' alt="<%=value.getDisplayName()%>" <%= imgSize[i] %>><%
%></div><%
        	} // (id=3.1)
        } // (id=3)
    } // (id=2)
%>