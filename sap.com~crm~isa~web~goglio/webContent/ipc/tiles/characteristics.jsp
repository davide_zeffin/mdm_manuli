<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*" %><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/characteristics.jsp"/><%
	BaseUI ui = new BaseUI(pageContext);
    CharacteristicsUI.IncludeParams includeParams = CharacteristicsUI.getIncludeParams(pageContext);
    UIContext uiContext = includeParams.getIpcBaseUI();
    InstanceUIBean currentInstance = includeParams.getInstance();
    GroupUIBean characteristicGroup = includeParams.getCharGroup();
    Hashtable customerParams = includeParams.getCustomerParams();

    UIArea csticsArea = includeParams.getUiArea();
    // this include contains the UI of all available characteristics/
    //	of all available characteristics of one single characteristic group //
%><table width = "100%" class="ipcCharacteristicsTable" ><%
    //defines the number of columns the characteristics should be shown in
    // int columns = uiContext.getPropertyAsInteger( RequestParameterConstants.CHARACTERISTIC_NO_COLUMNS );
	int columns = 1;
    int count = 0;
    // check whether there are visible and required characteristics
    boolean visibleChar = false;
    boolean required = false;
    Iterator characteristics;
    characteristics = characteristicGroup.getCharacteristics().iterator();
    if (characteristics != null) {
        while( characteristics.hasNext() ){
            CharacteristicUIBean characteristicUIBean = (CharacteristicUIBean)characteristics.next();
            //here begins the layout of one single characteristic
            //we want to show each characteristic in an extra row
            //the layout should be of the table header data type
            if( count % columns == 0 ){
    %><tr><%
            }
            // decide whether it is a normal cstic or a message-cstic
            if (characteristicUIBean.isMessageCstic()){
	            // It will be shown only if it is visible and has assigned values.
	            // The first condition (visibility) is taken into account during UIBean generation)
	            if (characteristicUIBean.hasAssignedValues()){
        %><td width = "<%= (100 / columns) %>%"><%
    	    CharacteristicUI.include(pageContext,
    	                              uiContext.getJSPInclude("tiles.messageCharacteristic.jsp"),
	                                  uiContext,
	                                  currentInstance,
	                                  characteristicGroup,
	                                  characteristicUIBean,
	                                  csticsArea,
	                                  customerParams);
        %></td><%                
	            }
            }
            else {
        %><td width = "<%= (100 / columns) %>%"><%
    	    CharacteristicUI.include(pageContext,
    	                              uiContext.getJSPInclude("tiles.characteristic.jsp"),
	                                  uiContext,
	                                  currentInstance,
	                                  characteristicGroup,
	                                  characteristicUIBean,
	                                  csticsArea,
	                                  customerParams);
        %></td><%
            }
            if( count % columns == (columns - 1) || !characteristics.hasNext() ){
    %></tr><%
            }
            visibleChar = true;
            //here ends the layout of one single characteristic
            ++count;
            if( characteristicUIBean.isRequired() )
            {
                required = true;
            }            
        }
        
		//If resetbutton flag was set, reset it to false
        if(uiContext.isResetButtonClicked()) {
        	uiContext.setResetButtonClicked(false);
        }
    } else {
    %><tr><%
        %><td><%
        %></td><%
    %></tr><%
    }
%></table><%
%><table class="ipcLegend" title="<isa:translate key="ipc.tt.legend"/>"><%
    %><tr><%
    // show the "required"-text only when the group has visible and required characteristics.
    if (visibleChar && required) {
        if (uiContext.getShowStatusLights()) {
        // the additional table cell is neccessary for IE5, which creates a line break
        // in front of and behind the image if text and image are in the same table cell.
        %><td><%
            %><img src='<isa:mimeURL name="ipc/mimes/images/table/dot_yellow.gif" />' alt="<isa:translate key="ipc.cstic.sign.required"/>"><%
        %></td><%
        %><td><%
            %><isa:translate key="ipc.cstic.sign.required"/><%
        %></td><%
    }
    else {
        %><td><%
            %><isa:translate key="ipc.cstic.required.sign"/>&nbsp;<isa:translate key = "ipc.cstic.sign.required"/><%
        %></td><%
    }
}
    %></tr><%
%></table>
<%         
	List characteristicGroups = currentInstance.getGroups(); 
	if (ui.isAccessible()&& !(characteristicGroups == null || (characteristicGroups.isEmpty()) || (characteristicGroups.size() <= 1)))
	{
		String tabTxt = WebUtil.translate(pageContext, "ipc.groups.tabtitle", null); 
%>
			<a 
				href="#tabcsticgroups-begin"
				id="tabcsticgroups-end"
				title="<isa:translate key="access.tab.end" arg0="<%=tabTxt%>"/>" 
			></a>
<%
	}
%>
