<%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ComparisonTblGenLoadMsgUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/comparisonTblGenLoadMsg.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
ComparisonTblGenLoadMsgUI.IncludeParams includeParams = ComparisonTblGenLoadMsgUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
LoadingMessageUIBean msg = includeParams.getMessageBean();
boolean titleDisplayed = includeParams.isTitleDisplayed();
Hashtable customerParams = includeParams.getCustomerParams();
StatusImage img = msg.getStatusImage();
				    
%><tr><%
	if(!titleDisplayed){
    %><td class="configObjectsProduct" ><%
        %><isa:translate key="ipc.compare.loadingmessages.gen" /><%
    %></td><%
	}
	else {
    %><td class="configObjectsProduct" title="<isa:translate key="ipc.compare.tt.general.lmessage" />"><%
        %>&nbsp;<%
    %></td><%		        	    
	}
    %><td class="statusIcon" title="<isa:translate key="ipc.compare.tt.icon.loadingmessage" />"><%
        %><img style="display:inline"  src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
    %></td><%
    %><td><%
        %>&nbsp;<%
    %></td><%
    %><td><%
        %>&nbsp;<%
    %></td><%
    %><td title="<isa:translate key="ipc.compare.tt.general.lmessage" />"><%
        %><img style="display:inline"  align="top" src="<isa:mimeURL name="<%= img.getImagePath() %>" />" alt="<isa:translate key="<%= img.getResourceKey()%>" />"><%
        %>&nbsp;<%
        %><%= msg.getText() %><%
    %></td><%
%></tr>