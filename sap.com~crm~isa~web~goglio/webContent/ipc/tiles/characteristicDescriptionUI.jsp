<%-- include import commands for jsp and page header for the html page --%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDescriptionUI" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/characteristicDetails.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
CharacteristicDescriptionUI.IncludeParams includeParams = CharacteristicDescriptionUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
CharacteristicUIBean characteristic = includeParams.getCstic();
Hashtable customerParams = includeParams.getCustomerParams();
String descr = characteristic.getLengthLimitedDescription();
// only add the following coding if there is a description
if ((descr != null) && (descr.length() > 0)) {
	%><tr><%
  		// add this column only if status lights are enabled
      	if (uiContext.getShowStatusLights()) {
	    	%><td width="5%" ><%
	      	%></td><%
      	}
      	%><td align="left" colspan="2"><%
        	%><table border="0" align="left" style="border-spacing:0px; border-collapse:collapse"><%
            	%><tr><%
                	%><td><%            
                    	%><%= descr%><%
                  	%></td><%
              	%></tr><%
          	%></table><%
      	%></td><%
	%></tr><%   
}%>
