<%--
/**
 * Base layout for characteristic values pages.
 * Display selection list, input text field and interval domain.
 **/
--%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/layouts/selectionListWithInputFieldAndInterval.jsp"/><%
    CharacteristicValuesUI.IncludeParams includeParams = CharacteristicValuesUI.getIncludeParams(pageContext);
    String domainvaluesTile = includeParams.getValuesTile();
    String freeTextTile = includeParams.getFreeTextTile();
    String intervalTile = includeParams.getIntervalTile();
    Hashtable customerParams = includeParams.getCustomerParams();
    
%><table><%
    %><tr><%
        %><td class="layout" colspan="2"><%
        	%><jsp:include page="<%= domainvaluesTile %>" flush="true"/><%
        %></td><%     
    %></tr><%
    %><tr><%
        %><td class="layout"><%
        	%><jsp:include page="<%= freeTextTile %>" flush="true"/><%
        %></td><%
        %><td class="layout"><%
        	%><jsp:include page="<%= intervalTile %>" flush="true"/><%
        %></td><%        
    %></tr><%
%></table>