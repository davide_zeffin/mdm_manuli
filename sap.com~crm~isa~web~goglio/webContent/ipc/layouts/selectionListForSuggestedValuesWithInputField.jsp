<%--
/**
 * Display selection list for suggested values and input text field.
 * 
 * This layout is for a special case:
 * If a characteristic has a static domain and allows additional values, 
 * the static domain turns into a list of suggested values. 
 * That means that both a selectionList and an inputField has to be displayed.
 * But different to other cases the inputField must not contain the assigned value.
 * Only the selectionList displays the assigned values.
 * Therefore this layout-JSP overwrites the passed freeTextTile with another one that
 * does not show the assigned value.
 *
 **/
--%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/layouts/selectionListForSuggestedValuesWithInputField.jsp"/><%
    CharacteristicValuesUI.IncludeParams includeParams = CharacteristicValuesUI.getIncludeParams(pageContext);
    String domainvaluesTile = includeParams.getValuesTile();
    UIContext uiContext = includeParams.getIpcBaseUI();
    Hashtable customerParams = includeParams.getCustomerParams();

    // String freeTextTile = includeParams.getFreeTextTile();
    // freeTextTile is overwritten (see explanation above).
    String freeTextTile = uiContext.getJSPInclude("tiles.freetextForSuggestedValues.jsp");;
%><table width = "100%"><%   
    %><tr><%
        %><td class="layout"><%  
            %><jsp:include page="<%= domainvaluesTile %>" flush="true"/><%
        %></td><%
    %></tr><%
    %><tr><%
        %><td class="layout"><%  
            %><jsp:include page="<%= freeTextTile %>" flush="true"/><%
        %></td><%
    %></tr><%
%></table>