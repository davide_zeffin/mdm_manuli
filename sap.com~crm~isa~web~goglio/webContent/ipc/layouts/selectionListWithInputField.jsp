<%--
/**
 * Base layout for characteristic values pages.
 * Display selection list and input text field.
 **/
--%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/layouts/selectionListWithInputField.jsp"/><%
    CharacteristicValuesUI.IncludeParams includeParams = CharacteristicValuesUI.getIncludeParams(pageContext);
    String domainvaluesTile = includeParams.getValuesTile();
    String freeTextTile = includeParams.getFreeTextTile();
    Hashtable customerParams = includeParams.getCustomerParams();

%><table width = "100%"><%
    %><tr><%
        %><td class="layout"><%
        	%><jsp:include page="<%= domainvaluesTile %>" flush="true"/><%
        %></td><%
    %></tr><%
    %><tr><%
        %><td class="layout"><%
        	%><jsp:include page="<%= freeTextTile %>" flush="true"/><%
        %></td><%
    %></tr><%
%></table>
    