<%--
/**
 * Base layout for characteristic values pages.
 * Display input text field and interval domain.
 **/
--%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/layouts/inputFieldWithInterval.jsp"/><%
    CharacteristicValuesUI.IncludeParams includeParams = CharacteristicValuesUI.getIncludeParams(pageContext);
    String freeTextTile = includeParams.getFreeTextTile();
    String intervalTile = includeParams.getIntervalTile(); 
    Hashtable customerParams = includeParams.getCustomerParams();
   
%><table width = "100%"><%
    %><tr><%
        %><td class="layout"><%
        	%><jsp:include page="<%= freeTextTile %>" flush="true"/><%
        %></td><%
        %><td class="layout"><%
        	%><jsp:include page="<%= intervalTile %>" flush="true"/><%
        %></td><%        
    %></tr><%
%></table>