<%--
/**
 * Base layout for characteristic values pages.
 * Display only selection list.
 **/
--%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/layouts/selectionList.jsp"/><%
    String domainvaluesTile = CharacteristicValuesUI.getIncludeParams(pageContext).getValuesTile();
    Hashtable customerParams = CharacteristicValuesUI.getIncludeParams(pageContext).getCustomerParams();

%><table width = "100%"><%
    %><tr><%
        %><td class="layout"><%
        	%><jsp:include page="<%= domainvaluesTile %>" flush="true"/><%
        %></td><%
    %></tr><%
%></table>