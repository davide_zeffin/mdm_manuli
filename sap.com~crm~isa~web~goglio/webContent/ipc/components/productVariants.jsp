<%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.ProductVariantsRequestParameterConstants" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ProductVariantsUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "java.util.*"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><isa:moduleName name = "/ipc/components/productVariants.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/productvariants.inc.js"/>" type="text/javascript"><%
%></script><%
List productVariantList = (List) request.getAttribute(ProductVariantsRequestParameterConstants.PRODUCT_VARIANTS);

ProductVariantsUI.include(pageContext,
                          uiContext.getJSPInclude("tiles.productVariants.jsp"),
                          uiContext,
                          productVariantList,
                          customerParams);
%>