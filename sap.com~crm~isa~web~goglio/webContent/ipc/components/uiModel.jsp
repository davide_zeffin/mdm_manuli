<% 
%><%@ page import = "com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIObjectUI"%><%
%><%@ page import="com.sap.isa.maintenanceobject.action.ActionConstants" %><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.core.util.Message" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%
%><%@ taglib uri="/isa" prefix="isa" %><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%>
<%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);%><%
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);%><%
IPCDynamicUIObjectUI dynamicUI = new IPCDynamicUIObjectUI(pageContext); 

%>
<%-- Message area --%>
<%@ include file="/appbase/dynamicUI/objectMessages.inc.jsp"%>

<%-- Designer Navigation bar --%>
<%@ include file="/appbase/dynamicUI/designer/parts/partDesignerNavHeader.inc.jsp"%>

<%-- Navigation bar --%>
<%@ include file="/appbase/dynamicUI/pageNavigation.inc.jsp"%>

<%-- Additional Info AB --%>
<%@ include file="/ipc/components/additionalInfo.jsp"%>


<%-- Main UI --%>
<%@ include file="/appbase/dynamicUI/uiTypePage.inc.jsp"%>

<%-- Footer --%>

<%
// preview button
String prevAction;
String prevBtnTxt = WebUtil.translate(pageContext, "ipc.tt.preview", null);
if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE)){			  		 
	prevAction = WebUtil.getAppsURL(pageContext, null, "/ipc/showPreview.do", null, null, false);
} else {                      
	prevAction = WebUtil.getAppsURL(pageContext, null, "/ipc/dispatchSetValuesOnCaller.do", null, null, false);                        
}         
	   
%>
<br>
<input type="hidden" name="<%= InternalRequestParameterConstants.PREVIEW_ACTION_STORED %>" value="" />
<a href="#"
	onClick="javascript:onPreviewButtonClick('<%= prevAction %>');" 
	title="<isa:translate key="access.button" arg0="<%=prevBtnTxt%>" arg1=""/>" >
	<isa:translate key="ipc.preview"/>
</a>
<br><br>




 