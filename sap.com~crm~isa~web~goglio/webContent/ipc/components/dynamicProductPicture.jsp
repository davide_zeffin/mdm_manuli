<%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.MasterdataUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIObjectUI"%><%
%><%@ page import = "com.sap.isa.maintenanceobject.businessobject.UIPage"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.DynamicProductPictureTemplateUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.DynamicProductPictureRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.spc.remote.client.object.OriginalRequestParameterConstants"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/dynamicProductPicture.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/dynamicproductpicture.inc.js"/>" type="text/javascript"><%
%></script><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
InstanceUIBean instance = (InstanceUIBean) request.getAttribute(DynamicProductPictureRequestParameterConstants.CURRENT_INSTANCE);
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);
IPCDynamicUIObjectUI dynamicUI = new IPCDynamicUIObjectUI(pageContext); 

%>

<%-- page Picture --%>
<%
UIPage uiPage = (UIPage) dynamicUI.getObject().getActivePage();
String url    = uiPage.getImageURL();
int int_width = 0;
int int_height = 0;
String str_width = "";
String str_height = "";

// is there a picture set?
if (url != null && url.length() > 0){
	
	try{
  		int_width = Integer.parseInt(uiPage.getImageWidth());
		str_width = int_width + "px";	
		if (int_width > 0) {
			str_width = "width=\"" + int_width + "px" + "\"";
		}	
	}catch (NumberFormatException e){ 
  		int_width = 0;
  		str_width="";
 	} 

	try{
		int_height = Integer.parseInt(uiPage.getImageHeight());
		if (int_height > 0) {
			str_height = "height=\"" + int_height + "px" + "\"";
		}	
	}catch (NumberFormatException e){ 
  		int_height = 0;
		str_height="";
	} 

	%>
	<div class="ipcPageImage" >
	<img class="ipcPageImage" src="<%= url %>" <%= str_width %>  <%= str_height %> />
	</div>
<%
}

// only show pic if enabled and available
if (DynamicProductPictureTemplateUI.instanceHasDynamicProductPicture(uiContext, instance)){
	%>
	<div class="ipcDynProdPic" style="position:relative;">
	<%
	StringBuffer dynamicProductTemplateBuffer = new StringBuffer();
	dynamicProductTemplateBuffer.append("/ipc/templates/");
	dynamicProductTemplateBuffer.append(instance.getName());
	dynamicProductTemplateBuffer.append(".jsp");
	String dynamicProductTemplate = dynamicProductTemplateBuffer.toString();
	DynamicProductPictureTemplateUI.include(pageContext,
								dynamicProductTemplate,
								uiContext,
								instance,
								customerParams);
     %>
     </div>
     <%
}								
	 %>