<%@ taglib uri="/isa" prefix="isa" %>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.StartupParameter" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.InvalidSessionUI" %>

<%@ page session="false" %>

<% InvalidSessionUI ui = new InvalidSessionUI(pageContext); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title><isa:translate key="error.jsp.title"/></title>

  <isa:stylesheets/>


</head>

<body class="message-page">

  <% ui.includeSimpleHeader(); %>

  <div class="message-content">
    <div class="module-name"><isa:moduleName name="ipc/components/invalidsession.jsp" /></div>

    <div class="error"><span><isa:translate key="msg.error.invalidsession"/></span></div>

  </div>

</body>
</html>