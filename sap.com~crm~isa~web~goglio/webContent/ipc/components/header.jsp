<%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.HeaderRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.HeaderUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/header.jsp"/><%
%><script type="text/javascript"><%
	%>var searchSetFlagParamName = '<%= InternalRequestParameterConstants.SEARCH_SET_FLAG %>';<%
	%>var searchSetValueParamName = '<%= InternalRequestParameterConstants.SEARCH_SET_VALUE %>';<%
    %>var exportFlagParamName = '<%= InternalRequestParameterConstants.EXPORT_FLAG %>';<%
    %>var comparisonSnapFlagParamName = '<%= InternalRequestParameterConstants.COMPARISON_ACTION_SNAP %>';<%
    %>var comparisonStoredFlagParamName = '<%= InternalRequestParameterConstants.COMPARISON_ACTION_STORED %>';<%
	%>var previewStoredFlagParamName = '<%= InternalRequestParameterConstants.PREVIEW_ACTION_STORED %>';<%
    %>var takeSnapshotFlagParamName = '<%= InternalRequestParameterConstants.SNAPSHOT_ACTION %>';<%
%></script><%
%><script 	src="<isa:mimeURL name ="/ipc/mimes/scripts/header.inc.js"/>" type="text/javascript"><%
%></script><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
ConfigUIBean configuration = (ConfigUIBean)request.getAttribute(HeaderRequestParameterConstants.CURRENT_CONFIGURATION);    
InstanceUIBean instance = (InstanceUIBean)request.getAttribute(HeaderRequestParameterConstants.CURRENT_INSTANCE);    
GroupUIBean group = (GroupUIBean)request.getAttribute(HeaderRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP);    
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);

HeaderUI.include(pageContext,
                 uiContext.getJSPInclude("tiles.header.jsp"),
                 uiContext,
                 configuration,
                 instance,
                 group,
                 customerParams);
%>