<%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/prodVarComparisonHeader.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/settingsHeader.inc.js"/>" type="text/javascript"><%
%></script><%
    // back and cancel-buttons 
    %>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.settingsheader", null); 
%>
	<a 
		href="#settingsheader_group-end"
		id="settingsheader_group-begin"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
	<table class="ipcDetailHeader"><%
		%><tr><%
			%><td align="left"><%
                String backAction = WebUtil.getAppsURL(pageContext,
                                                    null,
                                                    "/ipc/restorePreviousLayout.do",
                                                    null,
                                                    null,
                                                    false);
				String backBtnTxt = WebUtil.translate(pageContext, "ipc.back", null); 
	            %><a href="#" 
	                title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
	                class="ipcButton"
					onClick="javascript:submitForm('<%= backAction %>');"><%
				%><%= backBtnTxt %></a><%				
			%></td><%
		%></tr><%
	%></table>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.settingsheader", null); 
%>
	<a 
		href="#settingsheader_group-begin"
		id="settingsheader_group-end"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
	></a>
<%
	}
%>
    <%
%>