<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import = "com.sap.isa.core.util.WebUtil"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIButton"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.container.GridLayout"%>
<%@ page import = "com.sap.isa.core.util.HttpServletRequestFacade"%>
<%@ page import = "com.sap.isa.core.SessionConst"%>
<%@ page import = "com.sap.isa.core.UserSessionData"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%> 
<%@ page import = "com.sap.isa.core.util.JspUtil"%>
<%@ page errorPage="/appbase/jspruntimeexception.jsp"%>
<%
	UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
	UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
	
    //get the request object
    request = HttpServletRequestFacade.determinRequest(request);

    //set grid layout that contains all info and method about the grid
    GridLayout gridLayout = (GridLayout) (request.getSession()).getAttribute("gridLayout");
    
    //display mode
    String display_mode = "F";
    String disable_entry = "";
    if(gridLayout.getDisplayMode()){
   	    display_mode = "T";
	    disable_entry = "disabled";
    }	
%>
<%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/grid.js"/>" type="text/javascript"><%
%><script src="/ipc/ipc/mimes/scripts/autosubmit.inc.js" type="text/javascript"></script><%
%></script><%
%><table border="0" cellpadding="0" cellspacing="0" class="ipcHeader"><%
    %><tr><%
        %><td align="left"><%
            %><table><%
                %><tr><%
                    %><td><%
            	        //check button
                        UIArea buttonsArea = new UIArea("buttons", uiContext);
    	        	    String buttonsAreaAccessKey = buttonsArea.getShortcutKey();
                        String cssClass = "ipcButton";
                        String action = WebUtil.getAppsURL(pageContext,
                                                                 null,
                                                                 "/ipc/createItems.do",
                                                                 null,
                                                                 null,
                                                                 false);
                                                     
                        String checkBtnTxt = WebUtil.translate(pageContext, "ipc.grid.tt.transfer.cfg", null);
            			
					if (disable_entry == "") { //msg 2850785 2008
                        %><a href="#"
                                title="<isa:translate key="access.button" arg0="<%=checkBtnTxt%>" arg1=""/>"
                                class="<%= JspUtil.encodeHtml(cssClass) %>"
                                accesskey="<isa:translate key="<%= buttonsAreaAccessKey %>"/>"
                                onClick="javascript:gridSubmit('<%= JspUtil.encodeHtml(action) %>');" 
                                tabindex="<%= buttonsArea.getTabIndex()%>"
                                <%= JspUtil.encodeHtml(disable_entry)%>><%
					}%><isa:translate key="ipc.grid.transfer"/><%
                    %></td><%
                        // reset button
                    %><td><%
	                    UIButton resetButton = new UIButton("resetbutton", uiContext);
        	            String accessKey = resetButton.getShortcutKey();                        
                        cssClass = "ipcButton";
                        action = WebUtil.getAppsURL(pageContext,
                                                                 null,
                                                                 "/ipc/readGridVariants.do",
                                                                 null,
                                                                 null,
                                                                 false);
                                                     
                        String resetBtnTxt = WebUtil.translate(pageContext, "ipc.conflict.tt.reset", null);

					if (disable_entry == "") { //msg 2850785 2008                                                     
                        %><a href="#"
                                title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>"
                                class="<%= cssClass %>" 
                                accesskey="<isa:translate key="<%= accessKey %>"/>"
                                onClick="javascript:gridSubmit('<%= JspUtil.encodeHtml(action) %>');" 
                                tabindex="<%= buttonsArea.getTabIndex()%>"
                                <%= JspUtil.encodeHtml(disable_entry)%>><%
					}%><isa:translate key="ipc.reset"/><%
                    %></td><%            
                %></tr><%
            %></table><%
		%></td><%            
	%></tr><%
%></table><%
%>