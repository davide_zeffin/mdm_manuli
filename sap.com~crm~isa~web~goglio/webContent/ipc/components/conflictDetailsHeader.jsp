<%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/conflictDetailsHeader.jsp"/><%
%><%UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
            %><table class="ipcDetailHeader" width="100%" style="height:47"><%
                %><tr><%
                    %><td align="left"><%
                    {
                        String action = WebUtil.getAppsURL(pageContext,
                                                            null,
                                                            "/ipc/restorePreviousLayout.do",
                                                            null,
                                                            null,
                                                            false);
						String backBtnTxt = WebUtil.translate(pageContext, "ipc.conflict.tt.back", null);                                                                
			            %><a href="#" 
			                title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
			                class="ipcButton"
							onClick="javascript:submitForm('<%= action %>');"><%
						%><isa:translate key="ipc.back"/></a><%				                
                     }

                    if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE)) {
                        String action = WebUtil.getAppsURL(pageContext,
                                                            null,
                                                            "/ipc/resetConfiguration.do",
                                                            null,
                                                            null,
                                                            false);
						String resetBtnTxt = WebUtil.translate(pageContext, "ipc.conflict.tt.reset", null);               
			            %><a href="#" 
			                title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>"
			                class="ipcButton"
							onClick="javascript:submitForm('<%= action %>');"><%
						%><isa:translate key="ipc.reset"/></a><%				                
                    }
                    if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.DISPLAY_MODE) && uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_APPLY_BUTTON)) {
                        String action = WebUtil.getAppsURL(pageContext,
                                                            null,
                                                            uiContext.getPropertyAsString(RequestParameterConstants.APPLY_BUTTON_ACTION),
                                                            null,
                                                            null,
                                                            false);
						String acceptBtnTxt = WebUtil.translate(pageContext, "ipc.conflict.tt.accept", null);               
			            %><a href="#" 
			                title="<isa:translate key="access.button" arg0="<%=acceptBtnTxt%>" arg1=""/>"
			                class="ipcButton"
							onClick="javascript:submitForm('<%= action %>');"><%
						%><isa:translate key="ipc.apply"/></a><%				                
                    }
                    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_BACK_BUTTON)) {
                        String title = "";
                        String value = "";
                        if (uiContext.getPropertyAsBoolean( RequestParameterConstants.DISPLAY_MODE)) {
                            title = title + WebUtil.translate(pageContext, "ipc.conflict.tt.back", null);
                            value = value + WebUtil.translate(pageContext, "ipc.back", null);
                        }
                        else {
                            title = title + WebUtil.translate(pageContext, "ipc.conflict.tt.cancel", null);
                            value = value + WebUtil.translate(pageContext, "ipc.cancel" , null);
                        }                       
                        String action = WebUtil.getAppsURL(pageContext,
                                                            null,
                                                            uiContext.getPropertyAsString(RequestParameterConstants.BACK_BUTTON_ACTION),
                                                            null,
                                                            null,
                                                            false);
			            %><a href="#" 
			                title="<isa:translate key="access.button" arg0="<%=title%>" arg1=""/>"
			                class="ipcButton"
							onClick="javascript:submitForm('<%= action %>');"><%
						%><%= value %></a><%				                
                    }
                    %></td><%                    
                    %><td class="ipcConflictHandlingHeader" align="right"><%
                    	String conflictHandlingHeaderTitle = "";
						if (uiContext.getPropertyAsBoolean(RequestParameterConstants.USE_CONFLICT_SOLVER)) 
						{
							conflictHandlingHeaderTitle = WebUtil.translate(pageContext, "ipc.conflicts", null);
						}
						else
						{
							conflictHandlingHeaderTitle = WebUtil.translate(pageContext, "ipc.conflict.explain", null);
						}
                        %><a name="pageTop" title="<%=conflictHandlingHeaderTitle%>"><%
                        %><%=conflictHandlingHeaderTitle%></a><%
                    %></td><%                    
                %></tr><%
            %></table><%
%>
