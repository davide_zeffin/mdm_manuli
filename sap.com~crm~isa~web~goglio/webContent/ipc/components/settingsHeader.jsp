<%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/settingsHeader.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/settingsHeader.inc.js"/>" type="text/javascript"><%
%></script><%
%><table border="0" cellpadding="0" cellspacing="0" class="ipcDetailHeader"><%
  %><tr><td><%
    // back and cancel-buttons 
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.settingsheader", null); 
%>
	<a 
		href="#settingsheader_group-end"
		id="settingsheader_group-begin"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
    %><table class="ipcHeaderButtons"><%
        %><tr><%
            %><td align="left"><%
                {
                    String action = WebUtil.getAppsURL(pageContext,
                                                        null,
                                                        "/ipc/setSettings.do",
                                                        null,
                                                        null,
                                                        false);                 
					String backBtnTxt = WebUtil.translate(pageContext, "ipc.back", null); 
	            %><a href="#" 
	                title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
	                class="ipcButton"
					onClick="javascript:submitForm('<%= action %>');"><%
				%><%= backBtnTxt %></a><%				                
                }
			%></td><%
			%><td align="left"><%
                {
                    String cancelAction = WebUtil.getAppsURL(pageContext,
                                                        null,
                                                        "/ipc/restorePreviousLayout.do",
                                                        null,
                                                        null,
                                                        false);
					String cancelBtnTxt = WebUtil.translate(pageContext, "ipc.cancel", null); 
	            %><a href="#" 
	                title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>"
	                class="ipcButton"
					onClick="javascript:submitForm('<%= cancelAction %>');"><%
				%><%= cancelBtnTxt %></a><%				
                }
            %></td><%
        %></tr><%
    %></table><%
  %></td></tr><%
%></table><%

	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.settingsheader", null); 
%>
	<a 
		href="#settingsheader_group-begin"
		id="settingsheader_group-end"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
	></a>
<%
	}
%>
    <%
%>