<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.ui.context.ContextManager"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
//------------------------------------------form input fields-------------------------------------------//
ContextManager contextManager = ContextManager.getManagerFromRequest(request);
String currentInstanceId = contextManager.getContextValue(Constants.CURRENT_INSTANCE_ID);
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
%><input type="hidden" name="<%= Constants.SELECTED_INSTANCE_ID %>" value="<%= currentInstanceId %>"/><%
%><input type="hidden" name="<%= Constants.CURRENT_INSTANCE_ID %>" value="<%= currentInstanceId %>"/><%
String currentCsticGroupName = contextManager.getContextValue(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
if (currentCsticGroupName == null) currentCsticGroupName = "";
%><input type="hidden" name="<%= Constants.CURRENT_CHARACTERISTIC_GROUP_NAME %>" value="<%= currentCsticGroupName %>"/><%
String currentScrollGroupName = contextManager.getContextValue(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
if (currentScrollGroupName == null) currentScrollGroupName = "";
%><input type="hidden" name="<%= Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME %>" value="<%= currentScrollGroupName %>"/><%
%><input type="hidden" name="<%= Constants.SELECTED_CHARACTERISTIC_GROUP_NAME %>" value="<%= currentScrollGroupName %>"/><%
String currentCsticName = contextManager.getContextValue(Constants.CURRENT_CHARACTERISTIC_NAME);
if (currentCsticName == null) currentCsticName = "";
%><input type="hidden" name="<%= Constants.CURRENT_CHARACTERISTIC_NAME %>" value="<%= currentCsticName %>"/><%
String currentConflictId = contextManager.getContextValue(Constants.CURRENT_CONFLICT_ID);
if (currentConflictId == null) currentConflictId = "";
%><input type="hidden" name="<%= Constants.CURRENT_CONFLICT_ID %>" value="<%= currentConflictId %>"/><%
String currentConflictPartId = contextManager.getContextValue(Constants.CURRENT_CONFLICT_PARTICIPANT_ID);
if (currentConflictPartId == null) currentConflictPartId = "";
%><input type="hidden" name="<%= Constants.CURRENT_CONFLICT_PARTICIPANT_ID %>" value="<%= currentConflictPartId %>"/><%
%><input type="hidden" name="<%= Constants.CURRENT_VALUE_NAME %>" value=""/><%
%><input type="hidden" name="<%= InternalRequestParameterConstants.TECHKEY %>" value=""/><%
//------------------------------------------end form input fields-------------------------------------------//
// Concatenated model information
%><!-- <%= uiContext.getModelInfo(userData) %> --><%
// Coding for progress indicator (display only if enabled)
if (uiContext.getShowProgressIndicator()){
// We need both: an iframe and a div-area because select-elements does not support the z-index attribute and therefore would
// shine through the progress indicator. See Microsoft knowledgebase article 177378.
%><IFRAME id="submitInProgressI" src="javascript:void(0);" scrolling="no" style="display:none;position:absolute;z-index:9997;padding-top:15px;width: 150px;   height: 65px;" marginheight="0" marginwidth="0" frameborder="0"></iframe><%
%><div id="submitInProgress" style="display:none;position:absolute;z-index:9997;padding-top:15px;width: 150px;    height: 65px;   border: 1px solid #666666;  border-top: 1px solid #999999;  border-left: 1px solid #999999; background-color: #ffffff;  text-align: center; vertical-align: middle;"><%
    %><table cellspacing="0" cellpadding="0" border="0" style="text-align:center; width:100%;" ><%
        %><tr><%
            %><td style="vertical-align:middle; text-align:center;"><%
                %><span title="<isa:translate key="ipc.loading" />"><br><isa:translate key="ipc.loading" /><br></span><%
            %></td><%
        %></tr><%
    %></table><%
%></div><%
}%>
