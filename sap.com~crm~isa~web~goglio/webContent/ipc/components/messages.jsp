<%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.MessagesRequestParameterConstants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConflictHandlingShortcutsUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MessagesUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ConflictUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.MessageUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConflictHandlingShortcutUIBean" %><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/messages.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/messages.inc.js"/>" type="text/javascript"><%
%></script><%
List messages = (List) request.getAttribute(MessagesRequestParameterConstants.MESSAGES);
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);

if (messages != null && messages.size() > 0){
	for (int i=0; i<messages.size(); i++){
		Object message = messages.get(i);
		if (message instanceof ConflictHandlingShortcutUIBean) {  //TODO create own object for container of conflict handling shortcuts
			ConflictHandlingShortcutUIBean conflictShortcut = (ConflictHandlingShortcutUIBean)message;
			ConflictHandlingShortcutsUI.include(
			    pageContext,
			    uiContext.getJSPInclude("tiles.conflictHandlingShortcuts.jsp"),
   			    uiContext,
			    conflictShortcut,
			    customerParams			    
			);
		}else if (message instanceof MessageUIBean) {
			MessageUIBean messageUIBean = (MessageUIBean)message;
			MessagesUI.include(
			    pageContext,
			    uiContext.getJSPInclude("tiles.messages.jsp"),
			    uiContext,
			    messageUIBean,
			    customerParams
			);
		}		
	}
}
%>