<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import = "com.sap.isa.core.util.WebUtil"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIButton"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%>
<%@ page import = "com.sap.isa.core.SessionConst"%>
<%@ page import = "com.sap.isa.core.UserSessionData"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%> 
<%@ page import = "com.sap.isa.core.util.JspUtil"%>
<%@ page errorPage="/appbase/jspruntimeexception.jsp"%>
<%@ include file="/b2b/usersessiondata.inc" %>
<%
	UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
	UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
	
    String enablePrevious = "disabled", enableNext = "disabled", itemTechKey = "";

		if (userSessionData.getAttribute("gridprev") != null &&
			userSessionData.getAttribute("gridprev").toString().length() > 0){
			enablePrevious = "";
		}
		if (userSessionData.getAttribute("gridnext") != null &&
			userSessionData.getAttribute("gridnext").toString().length() > 0){
			enableNext = "";
		}
		if (userSessionData.getAttribute("com.sap.isa.isacore.action.order.ItemConfigurationAction.iteminconfig") != null &&
			userSessionData.getAttribute("com.sap.isa.isacore.action.order.ItemConfigurationAction.iteminconfig").toString().length() > 0){
			itemTechKey = userSessionData.getAttribute("com.sap.isa.isacore.action.order.ItemConfigurationAction.iteminconfig").toString();
		}
%>	
<%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/grid.js"/>" type="text/javascript"><%
%></script><%

%><script>
			function send_cancel(action) {
				document.currentForm.exitconfig.value = "X";
				document.currentForm.action = action;
				document.currentForm.submit();
				return true;
			}
	
			function send_previous(action) {
				document.currentForm.gridprev.value = document.currentForm.techKey.value;
				document.currentForm.action = action;
				document.currentForm.submit();
				return true;
			}

			function send_next(action) {
				document.currentForm.gridnext.value = document.currentForm.techKey.value;
				document.currentForm.action = action;
				document.currentForm.submit();
				return true;
			}

			function send_transfer(action) {
				document.currentForm.returnconfig.value = "X";
				document.currentForm.action = action;
				document.currentForm.submit();
				return true;
			}
	
			function send_newitem(action) {
				<%--Dont process when the product input field is empty--%>
				if (document.currentForm.elements['newproductid'].value == ""){
					alert("<isa:translate key="b2b.grid.msg.entprod"/>");
			        return false ;
				}
		        <%--Confirm to add a new product into the basket--%>
				check = confirm("<isa:translate key="b2b.grid.newproductmsg1"/>" + document.currentForm.elements['newproductid'].value + "<isa:translate key="b2b.grid.newproductmsg2"/>");
				if (check){
					document.currentForm.gridnewprod.value=document.currentForm.elements['newproductid'].value;
					document.currentForm.action = action;
					document.currentForm.submit();
					return true;
				} else {
				   return false;
				}
			}
</script><%

	%><div id="gridbuttons"><%
        %><table><%
            %><tr><%
                %><td><%
                	%><ul class="buttons-1"><%
                    	%><li><%
                            %><input type="text" class="textInput" size="20" name="newproductid" value="" onKeyPress="">&nbsp;&nbsp;&nbsp;<%
                        %></li><%
						%><li><%
                              	//new item button
                                UIArea buttonsArea = new UIArea("buttons", uiContext);
            		            String buttonsAreaAccessKey = buttonsArea.getShortcutKey();
                                String cssClass = "ipcButton";
                                String action = WebUtil.getAppsURL(pageContext,
                                                                         null,
                                                                         "/ipc/createItems.do",
                                                                         null,
                                                                         null,
                                                                         false);
                                                     
                                String checkBtnTxt = WebUtil.translate(pageContext, "ipc.grid.tt.new.prod", null);    		        
                                %><!--<input type="button" value="<isa:translate key="b2b.grid.button.newprod"/>"
                                        title="<isa:translate key="access.button" arg0="<%=checkBtnTxt%>" arg1=""/>"
                                        class="<%= JspUtil.encodeHtml(cssClass) %>"
                                        onClick="javascript:send_newitem('<%= JspUtil.encodeHtml(action) %>');" 
                                        tabindex="<%= buttonsArea.getTabIndex()%>">--><%
                                %><a href="#" onClick="javascript:send_newitem('<%= JspUtil.encodeHtml(action) %>');" tabindex="<%= buttonsArea.getTabIndex()%>" title="<isa:translate key="access.button" arg0="<%=checkBtnTxt%>" arg1=""/>"><%
                                    %><isa:translate key="b2b.grid.button.newprod"/><%
                                %></a><%
                        %></li><%
                    %></ul><%
                %></td><%
                %><td><%
                    %><ul class="buttons-2"><%
						%><li><%
                    //transfer button
				    action = WebUtil.getAppsURL(pageContext,
												     null,
												     "/ipc/createItems.do",
												     null,
												     null,
												     false);
                                                     
				    checkBtnTxt = WebUtil.translate(pageContext, "ipc.grid.tt.transfer", null);
            
				    %><!--<input type="button" value="<isa:translate key="b2b.grid.button.tranf"/>"
						    title="<isa:translate key="access.button" arg0="<%=checkBtnTxt%>" arg1=""/>"
						    class="<%= JspUtil.encodeHtml(cssClass) %>"
						    onClick="javascript:send_transfer('<%= JspUtil.encodeHtml(action) %>');" 
						    tabindex="<%= buttonsArea.getTabIndex()%>">--><%
                                %><a href="#" onClick="javascript:send_transfer('<%= JspUtil.encodeHtml(action) %>');" tabindex="<%= buttonsArea.getTabIndex()%>" title="<isa:translate key="access.button" arg0="<%=checkBtnTxt%>" arg1=""/>"><%
									%><isa:translate key="b2b.grid.button.tranf"/><%
								%></a><%
                        %></li><%
						%><li><%
					//reset button	  
	                UIButton resetButton = new UIButton("resetbutton", uiContext);
            	    String accessKey = resetButton.getShortcutKey();
                    action = WebUtil.getAppsURL(pageContext,
                                                             null,
                                                             "/ipc/dispatchExitOnCaller.do",
                                                             null,
                                                             null,
                                                             false);
                                                     
                    String resetBtnTxt = WebUtil.translate(pageContext, "ipc.grid.tt.cancel", null);
                                                     
                    %><!--<input type="button" value="<isa:translate key="b2b.order.display.submit.cancel"/>"
                            title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>"
                            class="<%= JspUtil.encodeHtml(cssClass) %>" 
                            accesskey="<isa:translate key="<%= accessKey %>"/>"
                            onClick="javascript:send_cancel('<%= JspUtil.encodeHtml(action) %>');" 
                            tabindex="<%= buttonsArea.getTabIndex()%>">--><%
                                %><a href="#" onClick="javascript:send_cancel('<%= JspUtil.encodeHtml(action) %>');" tabindex="<%= buttonsArea.getTabIndex()%>" title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>"><%
									%><isa:translate key="b2b.order.display.submit.cancel"/><%
								%></a><%
                        %></li><%
					%></ul><%
                %></td><%
                %><td><%
                    %><ul class="buttons-3"><%
						%><li><%
					//prev button
					action = WebUtil.getAppsURL(pageContext,
													 null,
													 "/ipc/createItems.do",
													 null,
													 null,
													 false);
                    String navBtnTxt = "";
                                                     
                    if(enablePrevious.equals("")) {
                    	navBtnTxt = WebUtil.translate(pageContext, "ipc.grid.tt.prev.active", null);
                    }
                    else{
                    	navBtnTxt = WebUtil.translate(pageContext, "ipc.grid.tt.prev.inactive", null);
                    }
            
					%><!--<input type="button" value="<isa:translate key="b2b.grid.button.prev"/>"
							title="<isa:translate key="access.button" arg0="<%=checkBtnTxt%>" arg1=""/>"
							class="<%= JspUtil.encodeHtml(cssClass) %>"
							onClick="javascript:send_previous('<%= JspUtil.encodeHtml(action) %>');" 
							tabindex="<%= buttonsArea.getTabIndex()%>"
							<%= enablePrevious %>>--><%
                                %><a <% if(enablePrevious.equals("")) {%> href="#" onClick="javascript:send_previous('<%= JspUtil.encodeHtml(action) %>');" <%}%> tabindex="<%= buttonsArea.getTabIndex()%>" title="<isa:translate key="access.button" arg0="<%=navBtnTxt%>" arg1=""/>" <%= JspUtil.encodeHtml(enablePrevious) %>><%
									%><isa:translate key="b2b.grid.button.prev"/><%
								%></a><%
                        %></li><%
						%><li><%
					//next button	  
					action = WebUtil.getAppsURL(pageContext,
													 null,
													 "/ipc/createItems.do",
													 null,
													 null,
													 false);
                                                     
                    if(enableNext.equals("")) {
					    navBtnTxt = WebUtil.translate(pageContext, "ipc.grid.tt.next.active", null);
                    }
                    else{
                        navBtnTxt = WebUtil.translate(pageContext, "ipc.grid.tt.next.inactive", null);
                    }
                                                     
					%><!--<input type="button" value="<isa:translate key="b2b.grid.button.next"/>"
							title="<isa:translate key="access.button" arg0="<%=resetBtnTxt%>" arg1=""/>"
							class="<%= JspUtil.encodeHtml(cssClass) %>" 
							accesskey="<isa:translate key="<%= accessKey %>"/>"
							onClick="javascript:send_next('<%= JspUtil.encodeHtml(action) %>');" 
							tabindex="<%= buttonsArea.getTabIndex()%>"
							<%= enableNext %>>--><%
                                %><a <% if(enableNext.equals("")) {%> href="#" onClick="javascript:send_next('<%= JspUtil.encodeHtml(action) %>');" <%}%> tabindex="<%= buttonsArea.getTabIndex()%>" title="<isa:translate key="access.button" arg0="<%=navBtnTxt%>" arg1=""/>" <%= JspUtil.encodeHtml(enableNext) %>><%
									%><isa:translate key="b2b.grid.button.next"/><%
								%></a><%
                        %></li><%
					%></ul><%
				%></td><%            
            %></tr><%
        %></table><%
		
		%><input type="hidden" name="gridprev" value=""/><%
		%><input type="hidden" name="gridnext" value=""/><%
		%><input type="hidden" name="gridnewprod" value=""/><%
		%><input type="hidden" name="returnconfig" value=""/><%
		%><input type="hidden" name="exitconfig" value=""/><%
		%><input type="hidden" name="techKey" value="<%= itemTechKey%>"/><%
	%></div>

