<%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.InstancesRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.InstancesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicGroupsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/instances.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/instances.inc.js"/>" type="text/javascript"><%
%></script><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
ConfigUIBean configuration = (ConfigUIBean) request.getAttribute(InstancesRequestParameterConstants.CURRENT_CONFIGURATION);
List instances = configuration.getInstances();
InstanceUIBean instance = (InstanceUIBean) request.getAttribute(InstancesRequestParameterConstants.CURRENT_INSTANCE);
GroupUIBean currentCharacteristicGroup = (GroupUIBean)request.getAttribute(InstancesRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP);
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);

// HERE WE WANT TO SHOW THE INSTANCE TREE
InstancesUI.include(pageContext,
                    uiContext.getJSPInclude("tiles.instances.jsp"),
                    uiContext,
                    instances,
                    instance,
                    currentCharacteristicGroup,
                    customerParams); 
                    
%>
