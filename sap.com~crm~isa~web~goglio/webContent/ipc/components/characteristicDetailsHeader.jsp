<%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.CharacteristicDetailsRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDetailsUI" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/characteristicDetailsHeader.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/characteristicdetailsheader.inc.js"/>" type="text/javascript"><%
%></script><%
%><%UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
GroupUIBean characteristicGroup = (GroupUIBean) request.getAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP);
%><table class="ipcDetailHeader"><%
    %><tr><% // we implement the cancel button 
        if (uiContext.getShowDetailsInNewWindow()) {
        	String closeBtnTxt = WebUtil.translate(pageContext, "ipc.close", null);
        %><td align="right" valign="middle"><%
            %><a href="#" 
                title="<isa:translate key="access.button" arg0="<%=closeBtnTxt%>" arg1=""/>"
                class="ipcButton"
				onClick="javascript:parent.window.close()"><%
			%><%= closeBtnTxt %></a><%				
        }
        else {
        %><td align="left" valign="middle"><%
            String currentCsticGroupName = "";
            if (characteristicGroup !=  null){
                currentCsticGroupName = characteristicGroup.getName();
            }
			String backBtnTxt = WebUtil.translate(pageContext, "ipc.back", null);
            %><a href="#" 
                title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
                class="ipcButton"
				onClick = "javascript:backToConfig('<%= currentCsticGroupName %>', '<isa:webappsURL name="/ipc/restorePreviousLayout.do" />');"><%
			%><%= backBtnTxt %></a><%				
        %></td><%        
        %><td align="right"><%
            %><strong><isa:translate key="ipc.details"/></strong><%
        }
        %></td><%
    %></tr><%
%></table><%
%>
