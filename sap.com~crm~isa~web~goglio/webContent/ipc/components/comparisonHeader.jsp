<%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/comparisonHeader.jsp"/><%
BaseUI ui = new BaseUI(pageContext);

	if (ui.isAccessible()){
		String grpTxt = WebUtil.translate(pageContext, "ipc.compare.header", null); 
%>
	<a 
		href="#comparisonheader_group-end"
		id="comparisonheader_group-begin"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
	%><table class="ipcDetailHeader"><%
	    %><tr><%
	        %><td align="left"><%
	            {
	                String action = WebUtil.getAppsURL(pageContext,
	                                                    null,
	                                                    "/ipc/leaveComparison.do",
	                                                    null,
	                                                    null,
	                                                    false);                 
					String backBtnTxt = WebUtil.translate(pageContext, "ipc.back", null); 
	            %><a href="#" 
	                title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
	                class="ipcButton"
					onClick="javascript:submitForm('<%= action %>');"><%
				%><%= backBtnTxt %></a><%				
	            }
	        %></td><%
            %><td  align="right"><%
            	String comparisonHeaderTitle = WebUtil.translate(pageContext, "ipc.compare.title", null);
                %><strong><%=comparisonHeaderTitle%></strong><%
            %></td><%			            
        %></tr><%
    %></table><%
	if (ui.isAccessible()){
		String grpTxt = WebUtil.translate(pageContext, "ipc.compare.header", null); 
%>
	<a 
		href="#comparisonheader_group-begin"
		id="comparisonheader_group-end"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
	></a>
<%
	}
%>