<%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.*"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.ProductVariantComparisonRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.ProductVariantComparisonUI"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/productVariantComparison.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/productvariantcomparison.inc.js"/>" type="text/javascript"><%
%></script><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
List productVariantList = (List) request.getAttribute(ProductVariantComparisonRequestParameterConstants.PRODUCT_VARIANTS_TO_COMPARE);
InstanceUIBean instance = (InstanceUIBean) request.getAttribute(ProductVariantComparisonRequestParameterConstants.CURRENT_INSTANCE);
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);

ProductVariantComparisonUI.include(pageContext,
                                   uiContext.getJSPInclude("tiles.productVariantComparison.jsp"),
                                   uiContext,
                                   instance,
                                   productVariantList,
                                   customerParams);
%>