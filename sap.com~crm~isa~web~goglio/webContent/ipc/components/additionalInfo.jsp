<%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><isa:moduleName name = "/ipc/components/additionalInfo.jsp"/><%

Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);%><%
boolean showMFA = uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_MULTI_FUNCTIONALITY_AREA);
boolean showPV  = uiContext.getPropertyAsBoolean(RequestParameterConstants.ENABLE_VARIANT_SEARCH);
boolean showCT  = uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_CUSTOMER_TAB);
if ( !showPV && !showCT){
		// nothing to show
		showMFA = false; 							
	}
%>
<script src="<isa:webappsURL name ="/ipc/mimes/scripts/addInfo.inc.js"/>" type="text/javascript"> </script>

<%-- display Additional Info AB --%>
<%if(showMFA){   
%>  
	<div class="fw-du-uitype_assignblock"> <!-- start prop group -->                 
    <div class="fw-box">
    <div class="fw-box-top">
    <div></div></div>
    <div class="fw-box-i1"><div class="fw-box-i2"><div class="fw-box-i3">     
    <div class="fw-box-content">  	

 	<%
	String collapseText  = WebUtil.translate(pageContext, "ipc.hide", null); 
	String expandText  = WebUtil.translate(pageContext, "ipc.expand", null); 
	String collapseImg = WebUtil.getMimeURL(pageContext, "mimes/images/fw-du-collapse.gif");
	String expandImg   = WebUtil.getMimeURL(pageContext, "mimes/images/fw-du-expand.gif");
	   
	String switchAction;
	if (uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE)){			  		 
		switchAction = WebUtil.getAppsURL(pageContext, null, "/ipc/displayLayout.do", null, null, false);
	} else {                      
		switchAction = WebUtil.getAppsURL(pageContext, null, "/ipc/dispatchSetValuesOnCaller.do", null, null, false);                        
	} 
		
 	boolean expandAddInfo = uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.EXPAND_ADD_INFO,
 															 InternalRequestParameterConstants.DEFAULT_ADD_INFO);													  
 	String addInfoImg;
 	String addInfoText;
 	if (expandAddInfo){
		addInfoImg  = collapseImg;
		addInfoText = collapseText;
 	}else{
		addInfoImg  = expandImg;
		addInfoText = expandText;
 	}	
	
	String nameAddInfo	= WebUtil.translate(pageContext, "ipc.additonal_Info", null); 
	String titleAddInfo = addInfoText + " " + nameAddInfo;
	String switchAddInfoParamName = InternalRequestParameterConstants.SWITCH_ADD_INFO;        
    %>
 
	<h1>
		<input type="hidden" name="<%= switchAddInfoParamName %>" value="" />
    	<a href="#" onclick="javascript:onAddInfoClick('<%= switchAction %>', '<%= switchAddInfoParamName %>')" title="<%= titleAddInfo %>">
    		<img src="<%= addInfoImg %>" alt="<%= addInfoText %>"/>
    		<%= nameAddInfo %>
    	</a>   
  	</h1>
  	
  	<%-- Expand Additonal Info AB --%>
  	<%
  	if(expandAddInfo){
  	%>	
    	<div class="fw-du-tablegroup"> 
    	
    	<%-- display productVaraints --%>
		<%
  		if(showPV){    
		%> 

			<table>	
				 <tr>
       	   	 		<td class="fw-du-uitype_propgroup" colspan = "2">
       	   	 	 		<%
			
						boolean expandProdVar = uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.EXPAND_PROD_VAR,
																		InternalRequestParameterConstants.DEFAULT_PROD_VAR); 
						String prodVarImg;
						String prodVarText;
 						if (expandProdVar){
							prodVarImg  = collapseImg;
							prodVarText = collapseText;
						}else{
							prodVarImg  = expandImg;
							prodVarText = expandText;
						}
 	
 						String nameProdVar	= WebUtil.translate(pageContext, "ipc.product_variants", null); 
 						String titleProdVar = prodVarText + " " + nameProdVar;
 						String switchProdVarParamName = InternalRequestParameterConstants.SWITCH_PROD_VAR;
 		   				%>
       		   	 		<input type="hidden" name="<%= switchProdVarParamName %>" value="" />
    					<a href="#" onclick="javascript:onAddInfoClick('<%= switchAction %>', '<%= switchProdVarParamName %>')" title="<%= titleProdVar %>">
    						<img src="<%= prodVarImg %>"  alt="<%= prodVarText %>"/>
    						<%= nameProdVar %>
    					</a>    
  					 </td> 
				</tr>
				
				<%-- Expand Product Variants --%>
				<%
  				if(expandProdVar){
  				%>
					<tr>
						<td>	
							<%@ include file="/ipc/components/productVariants.jsp"%>
						</td>
					</tr>		
				<%
  				} // end if expand PV   
				%>
	
	 	   </table>
		<%
  		} // end if PV   
		%>
	
		<%-- display customerTab --%>
		<%
  		if(showCT){    
		%> 
			<table>	
				 <tr>
       	   	 		<td class="fw-du-uitype_propgroup" colspan = "2">
	       		     	<%
						boolean expandCustTab = uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.EXPAND_CUST_TAB,
																		InternalRequestParameterConstants.DEFAULT_CUST_TAB); 
						String custTabImg;
						String custTabText;
						if (expandCustTab){
							custTabImg  = collapseImg;
							custTabText = collapseText;
						}else{
							custTabImg  = expandImg;
							custTabText = expandText;
						}
 	
 						String nameCustTab	= WebUtil.translate(pageContext, "ipc.customer_tab", null); 
 						String titleCustTab = custTabText + " " + nameCustTab;
 						String switchCustTabParamName = InternalRequestParameterConstants.SWITCH_CUST_TAB;
    					%>
       		   	 		<input type="hidden" name="<%= switchCustTabParamName %>" value="" />
    					<a href="#" onclick="javascript:onAddInfoClick('<%= switchAction %>', '<%= switchCustTabParamName %>')" title="<%= titleCustTab %>">
    						<img src="<%= custTabImg %>"  alt="<%= custTabText %>"/>
    						<%= nameCustTab %>
    					</a>
  				 	</td> 
				</tr>
				
				<%-- Expand Customer Tab --%>
				<%
  				if(expandCustTab){
  				%>
				<tr>
					<td>	
						<%@ include file="/ipc/customer/components/customerTab.jsp"%>
					</td>
				</tr>
				
				<%
  				} // end if expand CT  
				%>		
	  		</table>

			<%
 		 	} // end if CT   
			%>
		</div>
		<%
  		} // end if expand AddInfo   
		%>
		
	</div> <!-- fw-box-content End prop group --> 
	</div></div></div> <!-- fw-box-i1 -->
	<div class="fw-box-bottom"><div></div></div>
	</div> <!-- fw-box -->  
	</div>         
	</div> <%-- end: id="assignmentblock" --%>

<%
 } // end if Additional Info   
%>  