<%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.CharacteristicDetailsRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDetailsUI" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/mimeDetailsHeader.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/characteristicdetailsheader.inc.js"/>" type="text/javascript"><%
%></script><%
%><%UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
String currentCsticGroupName = (String) request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);


%><table class="ipcDetailHeader"><%
    %><tr><%
		%><td align="left" valign="middle"><%
		    String backBtnTxt = WebUtil.translate(pageContext, "ipc.back", null);
		    %><a href="#" 
		        title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
		        class="ipcButton"
		        onClick = "javascript:backToConfig('<%= currentCsticGroupName %>', '<isa:webappsURL name="/ipc/restorePreviousLayout.do" />');"><%
		    %><%= backBtnTxt %></a><%				
		%></td><%        
        %><td align="right"><%
        %></td><%
    %></tr><%
%></table><%
%>
