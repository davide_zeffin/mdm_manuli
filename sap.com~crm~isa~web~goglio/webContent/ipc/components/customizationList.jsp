<%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CurrentConfigurationUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.CustomizationListRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/customizationList.jsp"/><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/customizationlist.inc.js"/>" type="text/javascript"><%
%></script>
<script type="text/javascript">
acceptButtonAction = '<%= WebUtil.getAppsURL(pageContext,
                                                     null,
                                                     "/ipc/dispatchReturnOnCaller.do",
                                                     null,
                                                     null,
                                                     false) %>';
</script>
<%
boolean fromConflicts = false;
ConfigUIBean currentConfig = (ConfigUIBean)request.getAttribute(CustomizationListRequestParameterConstants.CURRENT_CONFIGURATION);
InstanceUIBean currentInstance = (InstanceUIBean)request.getAttribute(CustomizationListRequestParameterConstants.CURRENT_INSTANCE);
GroupUIBean currentGroup = (GroupUIBean)request.getAttribute(CustomizationListRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP);
CharacteristicUIBean currentCstic = (CharacteristicUIBean)request.getAttribute(CustomizationListRequestParameterConstants.CURRENT_CHARACTERISTIC);
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);

CurrentConfigurationUI.include(pageContext,
                            uiContext.getJSPInclude("tiles.currentConfiguration.jsp"),
                            uiContext,
                            currentConfig,
                            currentInstance,
                            currentGroup,
                            currentCstic,
                            fromConflicts,
                            customerParams);
%>