<%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.MFATabRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.container.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/mfaTabs.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/mfatab.inc.js"/>" type="text/javascript"><%
%></script><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
String selectedTabName = (String)request.getAttribute(MFATabRequestParameterConstants.SELECTED_MFA_TAB_NAME);
List mfaTab = (List)request.getAttribute(MFATabRequestParameterConstants.MFA_TAB_LIST);

BaseUI ui = new BaseUI(pageContext);
%><input type="hidden" name="<%= Constants.SELECTED_MFA_TAB_NAME %>" value=""/><%
%>
<%            
	String tabTxt = WebUtil.translate(pageContext, "ipc.mfa.tabtitle", null); 
	if (ui.isAccessible())
	{
%>
			<a 
				href="#tabmfa-end"
				id="tabmfa-begin"
				title="<isa:translate key="access.tab.begin" arg0="<%=tabTxt%>" arg1="<%=Integer.toString(mfaTab.size())%>"/>" 
			></a>
<%
	}
%>
<table border="0" cellspacing="0" cellpadding="0"><%
	%><tr style="height:18px"><%   
		UIArea mfaArea = new UIArea("mfa", uiContext);
		String currentTabTitle = "";
		String tabSelectionKey = "access.tab.unselected";
		if (mfaTab != null) { // (id=4)
			if (mfaTab.size() > 1) { // (id=3)
				for (int counter=0; counter<mfaTab.size(); counter++) { // (id=2)
					MfaTab mfaList = (MfaTab) mfaTab.get(counter);
					String mfaName = mfaList.getName();
					String mfaForwDest = mfaList.getForwardDest();
					currentTabTitle = WebUtil.translate(pageContext, mfaList.getResourceKey(), null);
					boolean mfaSelected = false;
					if (selectedTabName.equals(mfaName)) {
						mfaSelected = true;
						tabSelectionKey = "access.tab.selected";
					}
					else
					{
						tabSelectionKey = "access.tab.unselected";
					}
					// define the first image of the first tab
					if (counter == 0) {
						if (mfaSelected) {
		%><td title="<isa:translate key="access.tab.selected" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
			%><img src='<isa:mimeURL name="ipc/mimes/images/sys/fron.gif"/>' border="0" alt="" width="17" style="height:18"><%
		%></td><%
						}
						else {
		%><td title="<isa:translate key="access.tab.unselected" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
			%><img src='<isa:mimeURL name="ipc/mimes/images/sys/froff.gif"/>' border="0" alt="" width="17" style="height:18"><%
		%></td><%
						}
					}
					// define the background-image of the tabs
					if (mfaSelected) {
		%><td class="ipcActiveTab" nowrap="nowrap" style="background-repeat:repeat-x;background-image:url(<isa:mimeURL name="ipc/mimes/images/sys/backon.gif" />)" title="<isa:translate key="access.tab.selected" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
					}
					else {
		%><td class="ipcInactiveTab" nowrap="nowrap" style="background-repeat:repeat-x;background-image:url(<isa:mimeURL name="ipc/mimes/images/sys/backoff.gif" />)" title="<isa:translate key="access.tab.unselected" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
					}
					String onlineEvaluate = "true";
					String action = WebUtil.getAppsURL(pageContext,
														null,
														mfaList.getForwardDest(),
					                                    null,
														null,
														false);
					// if it the first tab add the shortcut key (as dummy-link to avoid automatic submit using Firefox)
					String accessKeyAttribute = "";
					if (counter == 0) {
						String mfaAccessKey = mfaArea.getShortcutKey();                        
						accessKeyAttribute = " accesskey=\"" + WebUtil.translate(pageContext, mfaAccessKey, null) + "\" ";                      
		%><a href="#" <%= accessKeyAttribute %> tabindex="<%= mfaArea.getTabIndex()%>"></a><%						
					}
		%><a href="javascript:onMfaTabChange('<%= action %>', '<%= onlineEvaluate %>');" tabindex="<%= mfaArea.getTabIndex()%>" title="<isa:translate key="<%=tabSelectionKey%>" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
			%><isa:translate key="<%= mfaList.getResourceKey() %>"/>&nbsp;<%
		%></a><%
		%></td><%
					// define the end-image of the tabs
					if ((counter + 1) == mfaTab.size()) {
						if (mfaSelected) {
		%><td title="<isa:translate key="<%=tabSelectionKey%>" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
			%><img src='<isa:mimeURL name="ipc/mimes/images/sys/bkon.gif"/>' width="8" style="height:18" border="0" alt=""><%
		%></td><%
						}
						else {
		%><td title="<isa:translate key="<%=tabSelectionKey%>" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
			%><img src='<isa:mimeURL name="ipc/mimes/images/sys/bkoff.gif"/>' width="8" style="height:18" border="0" alt=""><%
		%></td><%
						}
					}
					else {
						if (mfaSelected) {
		%><td title="<isa:translate key="<%=tabSelectionKey%>" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
			%><img src='<isa:mimeURL name="ipc/mimes/images/sys/onoff.gif"/>' width="20" style="height:18" border="0" alt=""><%
		%></td><%
						}
						else if ((((MfaTab) mfaTab.get(counter + 1)).getName()).equals(selectedTabName)) {
		%><td title="<isa:translate key="<%=tabSelectionKey%>" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
			%><img src='<isa:mimeURL name="ipc/mimes/images/sys/offon.gif"/>' width="20" style="height:18" border="0" alt=""><%
		%></td><%
						}
						else {
		%><td title="<isa:translate key="<%=tabSelectionKey%>" arg0="<%=currentTabTitle%>" arg1="<%=Integer.toString(counter+1)%>" arg2="<%=Integer.toString(mfaTab.size())%>" arg3="<%=currentTabTitle%>"/>"><%
			%><img src='<isa:mimeURL name="ipc/mimes/images/sys/offoff.gif"/>' width="20" style="height:18" border="0" alt=""><%
		%></td><%
						}
					}
				} // (id=2)
			}
			else { // (id=3), if only one area is available no tab should be displayed, therefore we add an empty table field
		%><td><%
		%></td><%
			} // (id=3)
		} // (id=4)
	%></tr><%
%></table>