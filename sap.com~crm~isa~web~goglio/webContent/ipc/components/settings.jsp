<%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/settings.jsp"/><%
BaseUI ui = new BaseUI(pageContext);
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/settings.inc.js"/>" type="text/javascript"><%
%></script><%
%><br /><br /><br /><%
%><input type="hidden" name="<%= RequestParameterConstants.PARAMETER_VALID %>" value="<%= String.valueOf(RequestParameterConstants.T) %>"><%
%><center><%
    %><table border="0" style="text-align:left"><%
        %>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.info", null); 
%>
	<a 
		href="#ipcinfo_group-end"
		id="ipcinfo_group-begin"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
		%><tr><%
			%><td><%
				%><isa:translate key="ipc.ipc_scenario"/>:<%
			%></td><%
			%><td><%
				%><%= request.getAttribute(RequestParameterConstants.IPC_UI_VERSION) %><br /><%
			%></td><%
		%></tr><%
        %><tr><%
            %><td><%
                %><isa:translate key="ipc.kb_name"/>:<%
            %></td><%
            %><td><%
                %><%= request.getAttribute(RequestParameterConstants.KB_NAME) %><br /><%
            %></td><%
        %></tr><%
        %><tr><%
            %><td><%
                %><isa:translate key="ipc.kb_version"/>:<%
            %></td><%
            %><td><%
                %><%= request.getAttribute(RequestParameterConstants.KB_VERSION) %><br /><%
            %></td><%
        %></tr><%
        %><tr><%
            %><td><%
                %><isa:translate key="ipc.kb_profile"/>:<%
            %></td><%
            %><td><%
                %><%= request.getAttribute(RequestParameterConstants.KB_PROFILE) %><br /><%
            %></td><%
        %></tr><%
        %><tr><%
            %><td><%
                %><isa:translate key="ipc.kb_build_number"/>:<%
            %></td><%
            %><td><%
                %><%= request.getAttribute(RequestParameterConstants.KB_BUILD_NUMBER) %><br /><%
            %></td><%
        %></tr>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.info", null); 
%>
	<a 
		href="#ipcinfo_group-begin"
		id="ipcinfo_group-end"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
        <%
        %>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.settings", null); 
%>
	<a 
		href="#settings_group-begin"
		id="settings_group-end"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
        <tr><%
            %><td colspan="2"><%
                %><hr><%
            %></td><%
        %></tr><%
        // checkbox for display of invisible characteristics 
        %><tr><%
            %><td colspan="2"><%
            {
                String value = String.valueOf(request.getAttribute(RequestParameterConstants.SHOW_INVISIBLE_CHARACTERISTICS));
                String checked = "";
                if (String.valueOf(request.getAttribute(RequestParameterConstants.SHOW_INVISIBLE_CHARACTERISTICS)).equals(String.valueOf(RequestParameterConstants.T))) {
                    checked = "checked";
                }
                %><input type="checkbox" id="<%= String.valueOf(RequestParameterConstants.SHOW_INVISIBLE_CHARACTERISTICS) %>" name="<%= String.valueOf(RequestParameterConstants.SHOW_INVISIBLE_CHARACTERISTICS) %>" value="<%= value %>" <%= checked %>><%
            }
                %><label for="<%= String.valueOf(RequestParameterConstants.SHOW_INVISIBLE_CHARACTERISTICS) %>">&nbsp;<isa:translate key="ipc.display_invisible_cstics"/></label><br /><%
            %></td><%
        %></tr><%
        // checkbox for display of language dependent names 
        %><tr><%
            %><td colspan="2"><%
            {
                String value = String.valueOf(request.getAttribute(RequestParameterConstants.T));
                String checked = "";
                if (request.getAttribute( RequestParameterConstants.SHOW_LANGUAGE_DEPENDENT_NAMES ).equals( RequestParameterConstants.T)) {
                    checked = "checked";
                }
                %><input type="checkbox" id="<%= RequestParameterConstants.SHOW_LANGUAGE_DEPENDENT_NAMES %>" name="<%= RequestParameterConstants.SHOW_LANGUAGE_DEPENDENT_NAMES %>" value="<%= value %>" <%= checked %>><%
            }
                %>&nbsp;<label for="<%= RequestParameterConstants.SHOW_LANGUAGE_DEPENDENT_NAMES %>"><isa:translate key="ipc.show_lang_dep_names"/></label><br /><%
            %></td><%
        %></tr><%
        // checkbox for display of use group information 
        %><tr><%
            %><td colspan="2"><%
            {
                String value = String.valueOf(request.getAttribute(RequestParameterConstants.T));
                String checked = "";
                if (request.getAttribute(RequestParameterConstants.USE_GROUP_INFORMATION).equals(RequestParameterConstants.T)) {
                    checked = "checked";
                }
                %><input type="checkbox" id="<%= RequestParameterConstants.USE_GROUP_INFORMATION %>" name="<%= RequestParameterConstants.USE_GROUP_INFORMATION %>" value="<%= value %>" <%= checked %>><%
            }
                %>&nbsp;<label for="<%= RequestParameterConstants.USE_GROUP_INFORMATION %>"><isa:translate key="ipc.use_group_information"/></label><br /><%
            %></td><%
        %></tr><%
        // checkbox for expanded display of characteristics 
        %><tr><%
            %><td colspan="2"><%
            {
                String value = String.valueOf(request.getAttribute(RequestParameterConstants.T));
                String checked = "";
                if (request.getAttribute(RequestParameterConstants.SHOW_CSTIC_VALUES_EXPANDED).equals(RequestParameterConstants.T)) {
                    checked = "checked";
                }
                %><input type="checkbox" id="<%= RequestParameterConstants.SHOW_CSTIC_VALUES_EXPANDED %>" name="<%= RequestParameterConstants.SHOW_CSTIC_VALUES_EXPANDED %>" value="<%= value %>" <%= checked %>><%
            }
                %>&nbsp;<label for="<%= RequestParameterConstants.SHOW_CSTIC_VALUES_EXPANDED %>"><isa:translate key="ipc.show_cstic_values_expanded"/></label><br /><%
            %></td><%
        %></tr><%
        // advanced settings
        %>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.settings", null); 
%>
	<a 
		href="#settings_group-end"
		id="settings_group-begin"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
        <tr>
        <%
            %><td colspan="2"><%
                %><hr><%
            %></td><%
        %></tr>
        <%
        %>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.advanced.settings", null); 
%>
	<a 
		href="#advsettings_group-end"
		id="advsettings_group-begin"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
        <tr><%
            %><td colspan="2"><%
                %><isa:translate key="ipc.advanced_settings"/><%
            %></td><%
        %></tr><%
        // checkbox for online evaluate
        %><tr><%
            %><td colspan="2"><%
            {
                String value = String.valueOf(request.getAttribute(RequestParameterConstants.T));
                String checked = "";
                if (request.getAttribute(RequestParameterConstants.ONLINE_EVALUATE).equals(RequestParameterConstants.T)) {
                    checked = "checked";
                }
                %><input type="checkbox" id="<%= RequestParameterConstants.ONLINE_EVALUATE %>" name="<%= RequestParameterConstants.ONLINE_EVALUATE %>" value="<%= value %>" <%= checked %>><%
            }
                %>&nbsp;<label for="<%= RequestParameterConstants.ONLINE_EVALUATE %>"><isa:translate key="ipc.online_evaluate"/></label><br /><%
            %></td><%
        %></tr>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.advanced.settings", null); 
%>
	<a 
		href="#advsettings_group-begin"
		id="advsettings_group-end"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
        <%
        %><tr>
        <%
            %><td colspan="2"><%
                %><hr><%
            %></td><%
        %></tr><%
    %></table><%
%></center><%
%>