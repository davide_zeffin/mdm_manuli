<%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.CharacteristicDetailsRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDetailsUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.MessageCharacteristicDetailsUI" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/characteristicDetails.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/characteristicdetails.inc.js"/>" type="text/javascript"><%
%></script><%
%><%UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
InstanceUIBean instance = (InstanceUIBean)request.getAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_INSTANCE);
CharacteristicUIBean characteristic = (CharacteristicUIBean) request.getAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_CHARACTERISTIC);
GroupUIBean characteristicGroup = (GroupUIBean) request.getAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP);
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);
String currentValue = (String) request.getAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_VALUE);
UIArea csticsArea = new UIArea("cstics", uiContext);

// only include the tile if a group-object exists (otherwise a runtime error is displayed)
if (characteristicGroup != null){
	// decide whether it is a normal cstic or a message-cstic
	if (characteristic.isMessageCstic()){
		MessageCharacteristicDetailsUI.include(
		                                pageContext,
		                                uiContext.getJSPInclude("tiles.messageCharacteristicDetails.jsp"),
		                                uiContext,
		                                instance,
		                                characteristicGroup,
		                                characteristic,
		                                csticsArea,
		                                currentValue,
		                                customerParams
		                                );
	}
	else {
		CharacteristicDetailsUI.include(
		                                pageContext,
		                                uiContext.getJSPInclude("tiles.characteristicDetails.jsp"),
		                                uiContext,
		                                instance,
		                                characteristicGroup,
		                                characteristic,
		                                csticsArea,
		                                customerParams
		                                );
	}
}
%>