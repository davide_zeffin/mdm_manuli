<%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/conflictTraceHeader.jsp"/><%
BaseUI ui = new BaseUI(pageContext);

	if (ui.isAccessible()){
		String grpTxt = WebUtil.translate(pageContext, "ipc.conflictTrace.header", null); 
%>
	<a 
		href="#conflicttraceheader_group-end"
		id="conflicttraceheader_group-begin"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
	%><table class="ipcDetailHeader"><%
	    %><tr><%
	        %><td align="left"><%
	            {
	                String action = WebUtil.getAppsURL(pageContext,
	                                                    null,
	                                                    "/ipc/restorePreviousLayout.do",
	                                                    null,
	                                                    null,
	                                                    false);                 
					String backBtnTxt = WebUtil.translate(pageContext, "ipc.back", null); 
	            %><a href="#" 
	                title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>"
	                class="ipcButton"
					onClick="javascript:submitForm('<%= action %>');"><%
				%><%= backBtnTxt %></a><%				
	            }
	        %></td><%
            %><td  align="right"><%
            	String conflictTraceHeaderTitle = WebUtil.translate(pageContext, "ipc.conflictTrace.title", null);
                %><strong><%=conflictTraceHeaderTitle%></strong><%
            %></td><%			            
        %></tr><%
    %></table><%
	if (ui.isAccessible()){
		String grpTxt = WebUtil.translate(pageContext, "ipc.conflictTrace.header", null); 
%>
	<a 
		href="#conflicttraceheader_group-begin"
		id="conflicttraceheader_group-end"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
	></a>
<%
	}
%>