<%@ taglib uri="/htmlb" prefix="hbj" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import = "java.util.*" %>
<%@ page import = "com.sap.isa.core.util.WebUtil"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIButton"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%>
<%@ page import = "com.sap.isa.core.SessionConst"%>
<%@ page import = "com.sap.isa.core.UserSessionData"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%>
<%@ page import = "com.sap.isa.ipc.ui.jsp.container.GridLayout"%>
<%@ page import = "com.sap.isa.core.util.HttpServletRequestFacade"%>
<%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import = "com.sap.isa.core.util.JspUtil"%>
<%@ page errorPage="/appbase/jspruntimeexception.jsp"%>
<%@ taglib uri="/htmlb" prefix="hbj" %>

<isa:contentType/>
<isa:moduleName name = "/ipc/tiles/grid.jsp"/>




<%-- ******************************************************
        Start of java objects and parameter declaration
     ******************************************************
--%>

<%
	//required when pressed enter after entering qty
	String returnAction = WebUtil.getAppsURL(pageContext, null, "/ipc/createItems.do", null, null, false);
	
	//get ui object
	BaseUI ui = new BaseUI(pageContext);
	UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
	UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);

	//get the request object
	request = HttpServletRequestFacade.determinRequest(request);

    //set grid layout that contains all info and method about the grid
    GridLayout gridLayout = (GridLayout) (request.getSession()).getAttribute("gridLayout");

    //set header data
    String productId = gridLayout.getProductId();
    String productDesc = gridLayout.getProductDesc();
    String unit = gridLayout.getUnit();

	//ui mode: 'G' = grid; 'P' = grid with prices; 'S' = grid with stock indicator; 'C' = 'P' & 'S'
	String uiMode = gridLayout.getUiMode();
	
	// Note :1029344 :Checking if grid screen is reached from Catalog
	//Required for navigation in grid screen as the parameter "isGridItem" is set again
	String fromGridCatalog = null;
	if (request.getSession().getAttribute("gridcatalog") != null) {
		fromGridCatalog = request.getSession().getAttribute("gridcatalog").toString();
	}
	
	//display mode
	String display_mode = "F";
	String disable_entry = "";
	if(gridLayout.getDisplayMode()){
		display_mode = "T";
		disable_entry = "disabled";
	}

    //read all lists that contain all infos about the variant table
	List validComb = gridLayout.getValidCombinations();
    List qties = gridLayout.getQuantities();
	List indies = gridLayout.getIndicators();
    List prices = gridLayout.getPrices();
	List spreadValues = gridLayout.getSpreadValues();

	String showSpread = "";
    boolean spreadInput = true;
    boolean spreadButton = true;
	// Note - 1122720 fixes the issue of Quantity field and 'Distribute' button 
	// appearing as active in Display mode
    if( !gridLayout.isSpreadUsed() || display_mode.equals("T") ) {    
    	showSpread="disabled";
		spreadInput = false;
		spreadButton = false;
    }
	String mainQty = gridLayout.getMainQty();
	String invalidQty = "ipc.grid.inputField.error";

    //setting the values for the axes according to the dimension (x, y, z)
	String[] variantIds = gridLayout.getVariantIds();
    String[] csticNames = gridLayout.getCsticsNames();
	int dims = csticNames.length;

    String[] csticValuesX = gridLayout.getCsticValuesValid(0);
	String[] csticValuesY = {""};
    String[] csticValuesZ = {""};
	String[] variantCountZ = {""};

    if (dims == 2) {
      	csticValuesY = gridLayout.getCsticValuesValid(1);
	}
    else if(dims == 3){
       	csticValuesY = gridLayout.getCsticValuesValid(1);
	    csticValuesZ = gridLayout.getCsticValuesValid(2);
		variantCountZ = gridLayout.getVariantsFocusPos(csticValuesZ);
	}

    //counters for grid display
	int counterDims = 1;
    int variantCounter = 1;
	int qtyCounter = 1;

	boolean activeTabstrip=true,focusField = true, browserIE=false;
	int tabCount = 0;

	//Determine the client-browser(IE or NS)
	String whichBrowser = request.getHeader("USER-AGENT");
	if ( whichBrowser !=null && whichBrowser.indexOf("MSIE")!= -1 ) {
		browserIE = true; // IE = true, NE = false
	}

	//read messages to see if any warnings / errors occured
	HashMap error = gridLayout.getError();
%>

<%!

    String getTabStripStyle(boolean activeTabstrip, boolean browserIE) {
    String returnStyle="";
    if (activeTabstrip) {
        if (browserIE){ //if IE
            returnStyle="";
        }else{                        //If other than IE
           returnStyle="visibility:visible;";
        }
    }else{
       if (browserIE){
           returnStyle="display:none;";
       }else{
           returnStyle="visibility:hidden;";
       }
    }
    return returnStyle;
    }
%>

<%-- ******************************************************
          End of java objects and parameter declaration
     ******************************************************



     ******************************************************
         		 Start of HTML / grid body
     ******************************************************
--%>

<%
    %><script src="<isa:mimeURL name ="/ipc/mimes/scripts/grid.js"/>" type="text/javascript"><%
    %></script><%

    %><%-- Start table level:1 --%><%
    %><table border="0" width=100%><%
        %><tr><%
            %><td><%

%><%-- ********************************************************************
		Start header information: product and distribute (spread) options
	   ******************************************************************** --%><%
                //this is similar to the statusbar in the standard configuration screen,
                //therefore the same style sheets, etc. are used

                String tableClass = "ipcStatusbar";

                %><%-- Start table level: 2--%><%
                %><table class="<%= JspUtil.encodeHtml(tableClass) %>" border="0" width=100% cellspacing="0" cellpadding="0"><%
                    %><tr><%
                        %><td align="left"><%= JspUtil.encodeHtml(productId) %> &nbsp : &nbsp <%=JspUtil.encodeHtml(productDesc)%><%
                        %></td><%
                        //Display the Spread-Input-Field
                        %><td align="right"><%
                            if(!gridLayout.isSpreadUsed()) {
                                %><span title= "<isa:translate key="ipc.grid.nospread"/>"><%
                            }

								String spreadTitle = "";
								String checkBtnTxt = "";
								String invalidInput = "ipc.grid.spread.wrongInput";

								if(spreadInput){
									spreadTitle = "ipc.grid.active.spread.input";
									checkBtnTxt = WebUtil.translate(pageContext, "ipc.grid.active.spread.button", null);
								}
								else{
									spreadTitle = "ipc.grid.inactive.spread.input";
									checkBtnTxt = WebUtil.translate(pageContext, "ipc.grid.inactive.spread.button", null);
								}

                                %><label for="spreadInput"><isa:translate key="ipc.quantity"/></label><%
                                %><input type="text" size="5"
                                   		name="spreadInput"
                                   		id="spreadInput"
                                   		value="<%= JspUtil.encodeHtml(mainQty)%>"
                                   		maxlength="10"
                                   		onChange="validateIput(this, 'spreadInput', '<isa:translate key="<%= invalidInput %>"/>')"
                                   		title="<isa:translate key="<%= spreadTitle%>"/>" <%= JspUtil.encodeHtml(showSpread) %> ><%

				         		//distribute (spread) button
								UIArea buttonsArea = new UIArea("buttons", uiContext);
								String buttonsAreaAccessKey = buttonsArea.getShortcutKey();
								String cssClass = "ipcButton";
								if (display_mode.equals("F")) { //msg 2850785 2008
							    %><a href="#"
							    	  name="spreadButton"
							  		  title="<isa:translate key="access.button" arg0="<%=checkBtnTxt%>" arg1=""/>"
									  class="<%= JspUtil.encodeHtml(cssClass) %>"
									  onClick="javascript:spreadCalc(document.getElementById('spreadInput'), <%= validComb.size() %>, 'spreadInput', '<isa:translate key="<%= invalidQty %>"/>');"
									  tabindex="<%= JspUtil.encodeHtml(buttonsArea.getTabIndex())%>"
									   <%= JspUtil.encodeHtml(showSpread) %>/><%
								}
                            if(!gridLayout.isSpreadUsed()) {
                                %></span><%
                            }
							%><isa:translate key="ipc.grid.spread"/><%
                        %></td><%
                    %></tr><%
                %></table><%
                %><%-- End table level: 2 --%><%

%><%-- ******************************************************************
		End header information: product and distribute (spread) options
	   ****************************************************************** --%><%


%><%-- ******************************************************************
		Start distribute inconsistent warning
	   ****************************************************************** --%><%

            if(error.get("2") != null && gridLayout.isSpreadUsed()){
            	%><%-- Start table level: 2 --%><%
                %><table border="0" width=100% cellspacing="0" cellpadding="0"><%
                    %><tr><%
                        %><td><%
                            %><img src='<isa:mimeURL name="ipc/mimes/images/sys/ico12_error.gif"/>'border="0" alt="spread error" width="12" height="12">&nbsp;<%
                            %><isa:translate key="ipc.grid.spread.inconsistent"/><%
                        %></td><%
                    %></tr><%
                %></table><%
                %><%-- End table level: 2 --%><%
            }

%><%-- ******************************************************************
		End distribute inconsistent warning
	   ****************************************************************** --%><%

            %></td><%
        %></tr><%
        %><tr><%
            %><td colspan="3"><%

%><%-- ******************************************************************
		Start tabstrips for 3 dimensional products
	   ****************************************************************** --%><%
	   			//this is similar to the grouptabs in the standard configuration screen,
                //therefore the same style sheets, etc. are used

                if (dims == 3) {
                    counterDims = csticValuesZ.length;
                    %><%-- Display tabstrips--%><%

					String tabTxt = WebUtil.translate(pageContext, "ipc.grid.tabtitle", null);
					if (ui.isAccessible()||true){
					%>
					        <a
							    href="#tabcsticgroups-end"
								id="tabcsticgroups-begin"
								title="<isa:translate key="access.tab.begin" arg0="<%=tabTxt%>" arg1="<%= Integer.toString(counterDims)%>"/>">
							</a>
					<%
						}

                    %><%-- Start table level: 2 --%><%
                    %><table border="0" cellspacing="0" cellpadding="0"><%
                        %><tr align="center" style="height:17px"><%

							UIArea groupArea = new UIArea("groups", uiContext);
							String styleClass = "";
							String backgroundImage = "";
							String tabSelectionKey = "";
							String imagePath = "ipc/mimes/images/";

                            int noOfTabs = csticValuesZ.length;
                            %><%-- Start loop level: 1 --%><%
        					String titleZ = "";
                            for (int i = 0; i<csticValuesZ.length; i++) {
                                if (i == 0) {
									%><td><%
                                        %><img src='<isa:mimeURL name="ipc/mimes/images/sys/fron.gif"/>' name="navi<%= i %>" border="0" alt="" width="17" height="17"><%
                                    %></td><%
                                }

                                if (i == 0) {
                                    styleClass = "ipcActiveGroupTab";
									backgroundImage = "ipc/mimes/images/sys/backon.gif";
									tabSelectionKey = "access.tab.selected";
                                } else {
									styleClass = "ipcInactiveGroupTab";
									backgroundImage = "ipc/mimes/images/sys/backoff.gif";
									tabSelectionKey = "access.tab.unselected";
                                }

								titleZ = csticNames[0] + ": " + csticValuesZ[i];  // 2 Anil
								%><td class="<%= JspUtil.encodeHtml(styleClass) %>" style='background-repeat:repeat-x;background-image:url(<isa:mimeURL name="<%= JspUtil.encodeHtml(backgroundImage)%>" />)'
								            nowrap="nowrap"
								            id="tab<%= tabCount%>"
								            tabindex="<%= JspUtil.encodeHtml(groupArea.getTabIndex())%>"
								            title="<isa:translate key="<%=tabSelectionKey%>" arg0="<%= titleZ%>" arg1="<%= Integer.toString(i+1) %>" arg2="<%= Integer.toString(counterDims)%>" arg3="<%= titleZ%>"/>"><%

                                        %><a href="#" onClick="chgTabStrip(<%= noOfTabs %>,<%= tabCount%>,<%= JspUtil.encodeHtml(variantCountZ[tabCount])%>, '<isa:mimeURL name="<%=imagePath%>"/>', '<%= display_mode%>')" title = "<%= JspUtil.encodeHtml(csticValuesZ[i]) %>" STYLE = "text-decoration: none"><%
                                            %><b><%= JspUtil.encodeHtml(csticValuesZ[i])%></b><%
                                        %></a><%
                                    %></td><%

                                if (i == csticValuesZ.length -1) {
                                    %><td><%
                                        %><img src='<isa:mimeURL name="ipc/mimes/images/sys/bkoff.gif"/>' name="navi<%= i+1 %>" border="0" alt="" width="8" height="17"><%
                                    %></td><%

                                } else {
                                    if (i == 0) {
                                        %><td><%
                                            %><img src='<isa:mimeURL name="ipc/mimes/images/sys/onoff.gif"/>' name="navi<%= i+1 %>" border="0" alt="" width="20" height="17"><%
                                        %></td><%
                                    } else {
                                        %><td><%
                                            %><img src='<isa:mimeURL name="ipc/mimes/images/sys/offoff.gif"/>' name="navi<%= i+1 %>" border="0" alt="" width="20" height="17"><%
                                        %></td><%
                                    }
                                }
                                tabCount++;
                            }
                            %><%-- End loop level: 1 --%><%

                        %></tr><%
                    %></table><%
					%><%-- End table level: 2 --%><%

					if (ui.isAccessible()||true){
					%>
    					<a
	    					href="#tabcsticgroups-begin"
							id="tabcsticgroups-end"
							title="<isa:translate key="access.tab.end" arg0="<%=tabTxt%>" arg1="1"/>">
						</a>
					<%
						}

                    %><%-- Display line for Tab Organizer --%><%
					%><%-- Start table level: 2 --%><%
                    %><table class="LineNavTabOrganizer" width="100%" border="0" cellspacing="0" cellpadding="0"><%
                        %><tr><%
                            %><td><%

                            %></td><%
                        %></tr><%
                    %></table><%
					%><%-- End table level: 2 --%><%
                }%>
            <%
%><%-- ******************************************************************************************
		End of tabstrip area for grid display; similar to group tabs of standard configuration
	   ****************************************************************************************** --%><%

%><%-- ******************************************************************************************
		Start of grid table
	   ****************************************************************************************** --%><%
					String tableTitle = WebUtil.translate(pageContext, "ipc.grid.tabtitle", null);
					if (ui.isAccessible()||true){
					%>
							<a href="#itemstableend"
				                id="itemstablebegin"
                				title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=Integer.toString(csticValuesY.length)%>" arg2="<%=Integer.toString(csticValuesX.length)%>" arg3="1" arg4="<%=Integer.toString(csticValuesY.length)%>"/>">
			                </a>
					<%
						}
                    %><div id="gridtablecontent"><%
                    %><%-- Start table level: 2 --%><%
            	    %><table class=""><%
                        %><tr><%
                            %><td><%
                                tabCount = 0;
                                activeTabstrip = true;
                                %><%-- Start loop level: 1 --%><%
                                for (int z=0; z<counterDims; z++) {

                                    %><div class="" ID="tabcontent<%=tabCount%>" style="<%= JspUtil.encodeHtml(getTabStripStyle(activeTabstrip, browserIE))%>"><%
                                        //Flag used to push the first editable qty cell index to javascript array
                                        boolean fieldFocusIter = true;
                                        if (activeTabstrip) {
                                            activeTabstrip=false;//only one tab should be active
                                        }

                                        //Display the 1/2D Grid
    									String gridTableClass = "ipcProductVariants";
									    %><%-- Start table level: 3 --%><%
                                        %><table class="<%= JspUtil.encodeHtml(gridTableClass) %>" BORDER="1" CELLSPACING="0" CELLPADDING="3" ><%
                                            %><tr><%
                                                %><th class="empty"> &nbsp;</th><%
                                                String titleX = "";
                                                String tableCol = "ipc.grid.table.column";
                                                for (int i=0; i<csticValuesX.length; i++){
                                                	titleX = csticNames[0] + ": " + csticValuesX[i];
                                                    %><th width=40 tabindex=0 title="<isa:translate key="<%=tableCol%>"/><%= JspUtil.encodeHtml(titleX) %>"><%
                                                        %><%= JspUtil.encodeHtml(csticValuesX[i]) %><%
                                                    %></th><%
                                                }
                                            %></tr><%

                                            boolean isValid = false;
                                            %><%-- Start loop level: 2 --%><%
        									String titleY = "";
        									String tableRow = "ipc.grid.table.row";
                                            for (int i=0; i<csticValuesY.length; i++) {
												titleY = csticNames[0] + ": " + csticValuesY[i];  //1 Anil
                                                %><tr><%
                                                    %><th tabindex=0 title="<isa:translate key="<%=tableRow%>"/><%= JspUtil.encodeHtml(titleY) %>"><%
                                                        %><%= JspUtil.encodeHtml(csticValuesY[i]) %><%
                                                    %></th><%
                                                    int counter = 1;

    												%><%-- Start loop level: 3 --%><%
                                                    //display the inputfields
                                                    for (int j=0; j<csticValuesX.length; j++){
                                                        isValid = false;
                                                        String varId="";
                                                        String qty="";
                                                        String ind="";
                                                        String spreadVal="0";
                                                        String price="";
                                                        String valueX="";
                                                        String valueY="";
                                                        String valueZ="";
                                                        String titleInput="";
                                                        String inputField="";

														%><%-- Start loop level: 4 --%><%
                                                        for (int iValid=0; iValid<validComb.size(); iValid++){
                                                            String[] size = (String[])validComb.get(iValid);

                                                            if (dims == 1) {
                                                                if (csticValuesX[j].equals(size[0])){
                                                                    isValid = true;
                                                                    valueX = csticValuesX[j];
                                                                    varId = variantIds[iValid];
                                                                    qty = (String)qties.get(iValid);
                                                                    ind = (String)indies.get(iValid);
                                                                    price = (String)prices.get(iValid);
                                                                    titleInput = csticNames[0] + ": " + valueX;
                                                                    if(spreadValues != null)spreadVal = (String)spreadValues.get(iValid);
                                                                }
                                                            }
                                                            else if (dims == 2) {
                                                                if (csticValuesY[i].equals(size[1]) && csticValuesX[j].equals(size[0])){
                                                                    isValid = true;
                                                                    valueX = csticValuesX[j];
                                                                    valueY = csticValuesY[i];
                                                                    varId = variantIds[iValid];
                                                                    qty = (String)qties.get(iValid);
                                                                    ind = (String)indies.get(iValid);
                                                                    price = (String)prices.get(iValid);
																	titleInput = "1." + csticNames[0] + ": " + valueX + "; 2." + csticNames[1] + ": " + valueY;
								        							if(spreadValues != null)spreadVal = (String)spreadValues.get(iValid);
                                                                }
                                                            }
                                                            else {
                                                                if (csticValuesZ[z].equals(size[2]) && csticValuesY[i].equals(size[1]) && csticValuesX[j].equals(size[0])){
                                                                    isValid = true;
                                                                    valueX = csticValuesX[j];
                                                                    valueY = csticValuesY[i];
                                                                    valueZ = csticValuesZ[z];
                                                                    varId = variantIds[iValid];
                                                                    qty = (String)qties.get(iValid);
                                                                    ind = (String)indies.get(iValid);
                                                                    price = (String)prices.get(iValid);
                                                                    titleInput = "1." + csticNames[0] + ": " + valueX + "; 2." + csticNames[1] + ": " + valueY + "; 3." + csticNames[2] + ": " + valueZ; 
				        											if(spreadValues != null)spreadVal = (String)spreadValues.get(iValid);
                                                                }
                                                            }
                                                        } //End of iValid
														%><%-- End loop level: 4 --%><%
														inputField = "ipc.grid.inputField";
														invalidQty = "ipc.grid.inputField.error";
                                                        if (isValid){
                                                            %><td><%
															%><%-- set no focus in display mode --%><%
															if(!gridLayout.getDisplayMode()){
                                                                %><input class="editableGrid" type="text" size="5" name="qty[<%= qtyCounter %>]" id="qty[<%= qtyCounter %>]" value="<%= JspUtil.encodeHtml(qty) %>" maxlength="10" onChange="validateIput(this, 'qty[<%= qtyCounter %>]', '<isa:translate key="<%= invalidQty %>"/>')" onKeyPress="checkReturnKeyPressed(event,'<%=returnAction%>',this, 'qty[<%= qtyCounter %>]', '<isa:translate key="<%= invalidQty %>"/>');" title = "<isa:translate key="<%=inputField%>"/><%= JspUtil.encodeHtml(titleInput) %>" <%= JspUtil.encodeHtml(disable_entry) %>><%
                                                                %><br><%
                                                                    %><%-- set the focus on the first inputfield --%><%
                                                                    %><script type="text/javascript"><%
                                                                    %>document.getElementById("qty[1]").focus();<%
    											                    %></script><%
	    												            if (fieldFocusIter){
                                                                        fieldFocusIter = false;
                                                                        %><%--Every first editable qty cell index of tab-strip pushed into jscript array--%><%
                                                                        %><script type="text/javascript"><%
                                                                            %>var focusFieldArr = new Array();<%
                                                                            %>focusFieldArr.push("<%=counter%>");<%
                                                                        %></script><%
                                                                    }
																}
																else{
																	%><input class="displayGrid" type="text" size="5" name="qty[<%= qtyCounter %>]" id="qty[<%= qtyCounter %>]" value="<%= JspUtil.encodeHtml(qty) %>" maxlength="10" title = "<isa:translate key="<%=inputField%>"/><%= JspUtil.encodeHtml(titleInput) %>" <%= JspUtil.encodeHtml(disable_entry) %>><%
																	%><br><%
																}
                                                                //Display the UI-Mode
                                                                if (uiMode.equals("S") || uiMode.equals("C")) {
                                                                    if(ind.equals("A")) {
                                                                        %><img src = '<isa:mimeURL name="ipc/mimes/images/sys/ipc_completeness.gif"/>' name="indStatus" border="0" alt="" width="33" height="15"><%
                                                                    } else if(ind.equals("B")) {
                                                                        %><img src = '<isa:mimeURL name="ipc/mimes/images/sys/ipc_incompleteness.gif"/>' name="indStatus" border="0" alt="" width="33" height="15"><%
                                                                    } else if(ind.equals("C")) {
                                                                        %><img src = '<isa:mimeURL name="ipc/mimes/images/sys/ipc_inconsist.gif"/>' name="indStatus" border="0" alt="" width="33" height="15"><%
                                                                    } else if(ind.equals("E")) {
                                                                        %><img src = '<isa:mimeURL name="ipc/mimes/images/sys/ipc_empty_group.gif"/>' name="indStatus" border="0" alt="" width="33" height="15"><%
                                                                    }
                                                                }
                                                                if (uiMode.equals("P") || uiMode.equals("C")) {
                                                                    %><%= JspUtil.encodeHtml(price) %>&nbsp;<%= JspUtil.encodeHtml(gridLayout.getCurrency()) %><%
                                                                }

                                                                // hidden fields for used values
                                                                %><input type="hidden" name="indicators[<%= variantCounter %>]" id="indicators[<%=variantCounter %>]" value="<%= JspUtil.encodeHtml(ind) %>"><%
                                                                %><input type="hidden" name="price[<%= variantCounter %>]" id="price[<%=variantCounter %>]" value="<%= JspUtil.encodeHtml(price) %>"><%
                                                                %><input type="hidden" name="varIds[<%= variantCounter %>]" id="varIds[<%=variantCounter %>]" value="<%= JspUtil.encodeHtml(varId) %>"><%
                                                                %><input type="hidden" name="spread[<%= variantCounter %>]" id="spread[<%=variantCounter %>]" value="<%= JspUtil.encodeHtml(spreadVal) %>"><%
                                                                variantCounter++; qtyCounter++;
                                                            %></td><%
                                                        } //end of if(isValid)
                                                        else {
                                                            %><td>&nbsp;</td><%
                                                        }
                                                    } //end of j
												    %><%-- End loop level: 3 --%><%
                                                %></tr><%
                                            } //end of i
        									%><%-- End loop level: 2 --%><%
                                        %></table><%
                                        %><%-- End table level: 3 --%><%
                                    %></div><%
                                    tabCount++;
                                } //Ende z
                                %><%-- End loop level: 4 --%><%
                            %></td><%
                        %></tr><%
                    %></table><%
                    %><%-- End table level: 2 --%><%
                    %></div><%
					if (ui.isAccessible()||true){
					%>
							<a href="#itemstablebegin"
								id="itemstableend"
								title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>">
							</a>
					<%
						}

%><%-- ******************************************************************************************
		End of grid table
	   ****************************************************************************************** --%><%

                %></td><%
            %></tr><%
        %></table><%
        %><%-- End table level: 1 --%><%
%>
<%-- Hidden fields for general data --%>
<%
    %><input type="hidden" name="unit" value="<%= JspUtil.encodeHtml(unit) %>"><%
    %><input type="hidden" name="uiMode" value="<%= JspUtil.encodeHtml(uiMode) %>"><%
    %><input type="hidden" name="configType" value="G"><%
		// Note :1029344 -to navigate in grid screen when came from Catalog	    
		if (fromGridCatalog != null && fromGridCatalog.length() > 0) {
			%><input type="hidden" name="isGridItem" value="X"><%
		}
%>

<%-- ******************************************************
         		   End of HTML / grid body
     ******************************************************
--%>