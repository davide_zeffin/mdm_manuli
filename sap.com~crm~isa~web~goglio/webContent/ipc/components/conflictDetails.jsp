<%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.DescriptiveConflictsUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/components/conflictDetails.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/conflictdetails.inc.js"/>" type="text/javascript"><%
%></script><%
BaseUI ui = new BaseUI(pageContext);
%><%UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
List conflicts = (List) request.getAttribute( RequestParameterConstants.CONFLICTS );
request.removeAttribute(RequestParameterConstants.CONFLICTS);
List descriptiveConflicts = (List) request.getAttribute( RequestParameterConstants.DESCRIPTIVE_CONFLICTS );
request.removeAttribute(RequestParameterConstants.DESCRIPTIVE_CONFLICTS);
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);


%><table width="100%" cellspacing="0"><%
    // conflict explanation...
    %><tr><%
        %><td><%
            %><table border="0" cellpadding="2px" cellspacing="2px" width="100%"><%
                %><tr><%
                    if (uiContext.getPropertyAsBoolean( RequestParameterConstants.USE_CONFLICT_EXPLANATION)) {
                    %><td align="left"><%
                        %><table width="100%" class="ipcAreaFrame"><%
                            %><tr><%
                                %><td><%
                                    %>
<%            
	String grpTxt = WebUtil.translate(pageContext, "ipc.conflict.option.raising", null); 
	if (ui.isAccessible())
	{
%>
			<a 
				href="#groupconflict-end"
				id="groupconflict-begin"
				title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>" 
			></a>
<%
	}
%>									<table border="0" width="100%" class="ipcCharacteristicBar"><%
                                        %><colgroup><%
                                            %><col width="90%"><%
                                            %><col width = "10%"><%
                                        %></colgroup><%
                                        %><tr><%
                                            %><td><%
                                                %><a style="text-decoration: none; color: #000;" name="explain" title="<isa:translate key="ipc.conflict.option.raising"/>"><%
                                                    %><isa:translate key="ipc.conflict.option.raising"/><%
                                                %></a><%
                                            %></td><%
                                            %><td align="right"><%
                                                %><a name="explainToTop" href="#pageTop" title="<isa:translate key="ipc.conflict.tt.pagetop"/>"><%
                                                    %><isa:translate key="ipc.conflict.pagetop"/><%
                                                %></a><%
                                            %></td><%
                                        %></tr><%
                                    %></table><%                                        
									%><table class="ipcConflictSolverOuter" border="0" width="100%" ><%
                                        %><tr><%
                                            %><td align="left"><%
                                                DescriptiveConflictsUI.include(
                                                pageContext,
                                                uiContext.getJSPInclude("tiles.descriptiveConflicts.jsp"),
                                                uiContext,
                                                descriptiveConflicts,
                                                customerParams);
                                            %></td><%
                                        %></tr><%
                                    %></table>
<%            
	if (ui.isAccessible())
	{
%>
			<a 
				href="#groupconflict-begin"
				id="groupconflict-end"
				title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>" 
			></a>
<%
	}
%><%
                                %></td><%
                            %></tr><%
                        %></table><%
                    %></td><%
                    }
                %></tr><%
                %><tr><%
                    if (uiContext.getPropertyAsBoolean(RequestParameterConstants.USE_CONFLICT_SOLVER)) {
                    %><td align="left"><%
                        %><table width="100%" class="ipcAreaFrame"><%
                            %><tr><%
                                %><td><%
                                    %>
<%            
	String sgrpTxt = WebUtil.translate(pageContext, "ipc.conflict.solve", null); 
	if (ui.isAccessible())
	{
%>
			<a 
				href="#groupconflictsolve-end"
				id="groupconflictsolve-begin"
				title="<isa:translate key="access.grp.begin" arg0="<%=sgrpTxt%>"/>" 
			></a>
<%
	}
									%><table border="0" width="100%" class="ipcCharacteristicBar"><%
                                        %><colgroup><%
                                            %><col width="90%"><%
                                            %><col width="10%"><%
                                        %></colgroup><%
                                        %><tr><%
                                            %><td><%
                                                %><a style="text-decoration: none; color: #000;" name="solve" title="<isa:translate key="ipc.conflict.solve"/>"><%
                                                    %><isa:translate key="ipc.conflict.solve"/><%
                                                %></a><%
                                            %></td><%
                                            %><td align="right"><%
                                                %><a name="explainToTop" href="#pageTop" title="<isa:translate key="ipc.conflict.tt.pagetop"/>"><%
                                                    %><isa:translate key="ipc.conflict.pagetop"/><%
                                                %></a><%
                                            %></td><%
                                        %></tr><%
                                    %></table><%                                        
									%><table class="ipcConflictSolverOuter" border="0" width="100%"><%                                        
                                        %><tr><%
                                            %><td align="left" colspan="2"><%
                                                request.setAttribute("uiContext", uiContext);
                                                request.setAttribute(RequestParameterConstants.CONFLICTS, conflicts);
                                                %><jsp:include page = "/ipc/tiles/conflictSolver.jsp" flush="true" /><%
                                            %></td><%
                                        %></tr><%
                                    %></table>
<%            
	if (ui.isAccessible())
	{
%>
			<a 
				href="#groupconflictsolve-begin"
				id="groupconflictsolve-end"
				title="<isa:translate key="access.grp.end" arg0="<%=sgrpTxt%>"/>" 
			></a>
<%
	}
%>                                    <%
                                %></td><%
                            %></tr><%
                        %></table><%
                    %></td><%
                    }
                %></tr><%
            %></table><%
        %></td><%
    %></tr><%
%></table><%
%>