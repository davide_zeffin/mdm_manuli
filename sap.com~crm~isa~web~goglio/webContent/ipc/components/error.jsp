<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.ContextConst"%><%
%><%@ page import = "com.sap.isa.core.FrameworkConfigManager"%><%
%><%@ page import = "com.sap.spc.remote.client.object.IPCException"%><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "java.util.ArrayList"%><%
%><isa:moduleName name = "/ipc/components/error.jsp"/><%
%><isa:contentType/><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
String showStackTrace = "false";
// get the interactionConfigParameter "showstacktrace.isacore" 
if (userData != null) {
	showStackTrace = FrameworkConfigManager.Servlet.getInteractionConfigParameter(session,"ui", ContextConst.IC_SHOW_STACK_TRACE, ContextConst.IC_SHOW_STACK_TRACE + ".isa.sap.com");
}

// this page contains the UI of an IPC error
// using the attribute "ipcException" you can get all the
// information about the IPC error
IPCException ipcException = (IPCException) request.getAttribute( RequestParameterConstants.IPC_EXCEPTION );
// If no ipc exception was found, error display is displayed in runtimeexception.jsp
if( ipcException == null )
{
    response.sendRedirect( "/ipc/appbase/runtimeexception.jsp" );
    return;
}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><%
%><HTML><%
    %><HEAD><%
        %><TITLE> IPC - Internet Pricing And Configuration</TITLE><%
        %><%-- define stylesheet for this page only because the uiContexts method getStylesheet can not be accessed --%><%
        %><style type = "text/css"><%
            %><!--<%
                %>body {<%
                    %>color: #000;<%
                    %>background: none #d6d6d6;<%
                    %>font-family: Verdana, Geneva, sans-serif;<%
                    %>font-size: 10px;<%
                    %>font-weight: normal;<%
                    %>margin: 0px 0px 0px 0px;<%
                    %>padding: 0px;<%
                %>}<%
                %>h3 { font-size: 12px; }<%
            %>--><%
        %></style><%
    %></HEAD><%
    %><BODY><%
        %><%-- here comes the code and the message of the error --%><%
        %><h3><isa:translate key = "ipc.error"/> : <%= ipcException.getCode() %>- <%
        if( ipcException != null ){
            String key = ipcException.getKey();
            if( key != null ){
                ArrayList placeHolders = ipcException.getPlaceHolders();
                if( placeHolders != null ){
                    int size = placeHolders.size();
                    if( size <= 0 ){
            %><isa:translate key = "<%= ipcException.getKey() %>"/><%
                     } 
                     else {
                        if( size == 1 ){
            %><isa:translate key = "<%= ipcException.getKey() %>" arg0 = "<%= JspUtil.encodeHtml((String) placeHolders.get(0)) %>"/><%
                        }
                        else {
            %><isa:translate key = "<%= ipcException.getKey() %>" arg0 = "<%= JspUtil.encodeHtml((String) placeHolders.get(0)) %>" arg1 = "<%= JspUtil.encodeHtml((String) placeHolders.get(1)) %>"/><%
                        }
                    }
                }
            }
            else {
            %><%= JspUtil.encodeHtml(ipcException.getMessage()) %><%
            }
        }
        %></h3><%
        // decide whether to show the stacktrace
        if (showStackTrace.equalsIgnoreCase("true")) {
        %><textarea readonly rows = 20 cols = 70><%
        // here comes the java stack trace of the error
            ipcException.printStackTrace( new java.io.PrintWriter( out ) );
        %></textarea><%
        }
    %></BODY><%
%></HTML>
