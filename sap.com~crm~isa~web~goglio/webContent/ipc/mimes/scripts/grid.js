/*----------------------------------------------------*/
/* Javascript for the "SAP IPC" 		              */
/*                                                    */
/* Document:       gtid.js              	          */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/	

/*------------- function to change the tabstrips dynamicly on client side -------------*/	

			function chgTabStrip(numTabs,tab,focusPos, imagePath, display_mode) {
                var tabCount = numTabs-1;

                fron=new Image(17,17);
                fron.src= imagePath + "sys/fron.gif";
                froff=new Image(17,17);
                froff.src= imagePath + "sys/froff.gif";

                onoff=new Image(20,17);
                onoff.src= imagePath + "sys/onoff.gif";
                offon=new Image(20,17);
                offon.src= imagePath + "sys/offon.gif";
                offoff=new Image(20,17);
                offoff.src= imagePath + "sys/offoff.gif";

                bkon=new Image(8,17);
                bkon.src= imagePath + "sys/bkon.gif";
                bkoff=new Image(8,17);
                bkoff.src= imagePath + "sys/bkoff.gif";

                                
                //set all tabs on default
                for (intCtr=0; intCtr < numTabs; intCtr++) {
                    if(intCtr == 0 ) {
                        document.images["navi"+intCtr].src=froff.src;
                        intCtr++;
                    } 
                    if(intCtr == tabCount) {
                        document.images["navi"+numTabs].src=bkoff.src;
                        document.images["navi"+intCtr].src=offoff.src;
                    } 
                    else {
                        document.images["navi"+intCtr].src=offoff.src;
                    }
                }

                for (intCtr=0; intCtr < numTabs; intCtr++) {
                    var tabName = "tab" +intCtr;
                    var tabContentName = "tab" + "content" +intCtr;
                    var imgCountAfter = intCtr + 1;
                    var imgCount = intCtr;

                    if (intCtr != tab) {
                    	var bgImage = imagePath + "sys/backoff.gif";
                    	var bgImgUrl = "url("+bgImage+")";
                        document.getElementById(tabName).className ="ipcInactiveGroupTab";
                        document.getElementById(tabName).style.backgroundImage = bgImgUrl;
                        document.getElementById(tabName).style.backgroundRepeat = "repeat-x";
                        document.getElementById(tabContentName).style.background = "";

                        if (document.all){ //World of IEs
                            document.getElementById(tabContentName).style.display = 'none';
                        }else{ // World of NSs
                            document.getElementById(tabContentName).style.visibility = 'hidden';
                        }
                    }
    
                    //set the tabs on active and display the div-layers
                    else {
                        if(imgCount == 0 ) {
                            document.images["navi"+imgCount].src=fron.src;
                            document.images["navi"+imgCountAfter].src=onoff.src;
                            if(numTabs>2) {
                                imgCount++;                                
                            }
                        }

                        if (imgCount == tabCount) {
                            document.images["navi"+imgCount].src=offon.src;
                            document.images["navi"+imgCountAfter].src=bkon.src;
                        }
                        else if(imgCount!=0) {                        
                            document.images["navi"+imgCount].src=offon.src;
                            document.images["navi"+imgCountAfter].src=onoff.src;
                        }

						var bgImage = imagePath + "sys/backon.gif";
						var bgImgUrl = "url("+bgImage+")";
                        document.getElementById(tabName).className = "ipcActiveGroupTab";
                        document.getElementById(tabName).style.backgroundImage = bgImgUrl;
                        document.getElementById(tabName).style.backgroundRepeat = "repeat-x";
                        document.getElementById(tabContentName).style.background = "";
                        
                        if (document.all){ //World of IEs
                            document.getElementById(tabContentName).style.display = '';
                        }else{ // World of NSs
                            document.getElementById(tabContentName).style.visibility = 'visible';
                        }
                    } 
                }                
                //Set the focus to the first editable field in the Tab
                if (display_mode == "F"){
                    var fieldName = "qty["+focusPos+"]";
                    document.getElementById(fieldName).focus();    
                }
            }
        
/*---------------------------- check if browser button was pressed ---------------------------*/
            function checkReturnKeyPressed(evt, action, inputField, field, errorMsg){
                // check if the return key was pressed
                var BrowserName = navigator.appName;

                var pressedKeyCode;
                if (BrowserName == "Netscape") {
                    // Netscape
                    pressedKeyCode = evt.which;
                } else {
                    // Microsoft
                    pressedKeyCode = window.event.keyCode;
                };
                if (pressedKeyCode == 13) {

                    // if the Qty is valid, then form is submitted
                    if(validate(inputField)) {

						//Get the , and . from the input field                    
	                    var input = inputField.value;
						var output = "";

						output = splitInput(input, "");
						output = splitInput(output, " ");
						output = splitInput(output, ",");
						output = splitInput(output, ".");
						document.getElementById(field).value=output;
						
						 //submit it to create the sub-items
	                    document.currentForm.action = action;
		    		    document.currentForm.submit();
		    		} else {
		    		    alert(errorMsg);
						document.getElementById(field).value="";
						document.getElementById(field).focus();
		    		}
                };
                return true;
            }
/*-------------------------------- function to set fokus --------------------------------*/       
        
            function skipFocus(skip){
                document.getElementById(skip).focus();
            }
        
        
/*-- function to distribute the over-all quantity among all variants according to the spread profile --*/        
        
            function spreadCalc(param,size, field, errorMsg){
		
		if(validate(param)){
			
			var input = param.value;
			var total = "";

			total = splitInput(input, " ");
			total = splitInput(total, ",");
			total = splitInput(total, ".");
		
			var validCombSize = size;
			var spreadArray = new Array(validCombSize);
			var sortArray = new Array2D(2, validCombSize);
			var parSpread="";
			var spreadValue="";
			var check = 0;
			var max = 0;
			var maxPos = 0;
                
			tmpArray = new Array2D(2, validCombSize);
			for(var i=1; i<=validCombSize; i++) {
			    parSpread = document.getElementById("spread["+i+"]").value;
			    spreadValue = Math.round(parSpread / 100 * total);
			    spreadArray[i] = spreadValue;
			    sortArray[i] = spreadValue;
			    check = check + spreadValue;
			    sortArray.items[0][i] = i;
			    sortArray.items[1][i] = spreadValue;                  
			}

			for (var i = 2; i<=validCombSize; i++) {
				for (var j = validCombSize; j>=i; j--) {
					if (sortArray.items[1][j-1]<sortArray.items[1][j]) {
						x = sortArray.items[1][j-1];
						y = sortArray.items[0][j-1];
						    sortArray.items[1][j-1] = sortArray.items[1][j];
						    sortArray.items[0][j-1] = sortArray.items[0][j];
						sortArray.items[1][j] = x;
						sortArray.items[0][j] = y;
					    }
				}
			}


			if(check == total){
			    for(var i=1; i<=validCombSize; i++){
				    document.getElementById("qty["+i+"]").value = spreadArray[i];
			    }
			}
			else if(check < total){
				for(var i=1; i <= (total-check); i++){
					spreadArray[sortArray.items[0][i]]++;
				}
				for(var i=1; i<=validCombSize; i++){
				    document.getElementById("qty["+i+"]").value = spreadArray[i];
			    }
			}
			else if(check > total){
				for(var i=1; i <= (check-total); i++){
					spreadArray[sortArray.items[0][i]]--;
				}
				for(var i=1; i<=validCombSize; i++){
				    document.getElementById("qty["+i+"]").value = spreadArray[i];
			    }
			}
		}
		else{
			alert(errorMsg);
			document.getElementById(field).value="";
			document.getElementById(field).focus();
		}
            }
            
            // used for spread/distribute function
            function Array2D(x,y){
                this.items = new Array(x);
                for(i=0;i<x;i++){this.items[i] = new Array(y);}
            }
            

/*---------------- submit document using a certain action forward ---------------------*/            
           
            function gridSubmit(action) {
	    		document.currentForm.action = action;
	    		document.currentForm.submit();
		    }
		    
/*---------------------------------- validate input -------------------------------*/		    

			function validateIput(inputField, field, errorMsg) {
				
				if(validate(inputField)){
					var input = inputField.value;
					var output = "";
					
					output = splitInput(input, "");
					output = splitInput(output, " ");
					output = splitInput(output, ",");
					output = splitInput(output, ".");
					
					document.getElementById(field).value=output;
					document.getElementById(field).focus();
				}
				else{
					alert(errorMsg);
					document.getElementById(field).value="";
					document.getElementById(field).focus();
				}
			}
			
			
			function validate(input){
				var validChar = "0123456789 .,";
				var valid = true;
				
				for(var i=0; i<input.value.length; i++){
					if(validChar.indexOf(input.value.charAt(i)) != -1){
						valid = true;
					}
					else{
						valid = false;
						break;
					}
					
				}
				if(!valid){
					return false;
				}
				else{
					return true;
				}
			}
			
			function splitInput(input, sep) {
				var output = "";
				var inputFrag = input.split(sep);
				
				for(var i=0; i<inputFrag.length; i++){
					output = output + inputFrag[i];
				}
				return output;
			}