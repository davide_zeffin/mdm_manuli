/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       mfatab.inc.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/
    function onMfaTabChange(action, onlineEvaluate) {
        if (onlineEvaluate == 'true') {
            submitForm(action);
        }
        else {
            if(document.currentForm) {
                document.currentForm.action = action;
                document.currentForm.submit();
            }
        }
    }
