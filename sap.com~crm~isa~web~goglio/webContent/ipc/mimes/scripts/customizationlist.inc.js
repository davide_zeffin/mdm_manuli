/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       customizationlist.inc.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function expandInstanceInCustomizationList(loopedInstance, instanceId, groupName, characteristicName, action) {
        document.currentForm.action = action;
        document.getElementsByName(customizationListExpandedInstancesInputField)[0].value = loopedInstance;
	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
        document.getElementsByName(currentCharacteristicNameInputField)[0].value = characteristicName;
        sendSubmit();
    }
