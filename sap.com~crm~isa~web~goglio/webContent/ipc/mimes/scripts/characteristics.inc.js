/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       characteristics.inc.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function onCsticValueChange(instanceId, groupName, csticName, action) {
		document.getElementsByName(currentInstanceIdInputField)[0].value=instanceId;
		document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=groupName;
		document.getElementsByName(currentCharacteristicNameInputField)[0].value=csticName;
        document.currentForm.action = action + '#C_' + csticName;
        if (submit == 'true') {
            sendSubmit();
        }else {
            unsubmittedChanges = 'true';
        }
    }
    

    function getMessageCsticDetails(instId, charGroupName, charName, valueName, action) {
        document.getElementsByName(currentInstanceIdInputField)[0].value=instId;
        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=charGroupName;
        document.getElementsByName(currentCharacteristicNameInputField)[0].value=charName;
        document.getElementsByName(currentValueNameInputField)[0].value=valueName;
        document.currentForm.action = action + '#C_' + charName;
        sendSubmit();
    }

    function openDetail(url) {
        var sat = window.open( url, 'Detail',
                               'width=640,height=530,menubar=no,location=no,scrollbars=yes,resizable=yes' );
        sat.focus();
    }

    function onSetValueClick(action) {
        document.currentForm.action = action;
        sendSubmit();
    }
    function onFocus(csticName) {
		document.getElementsByName(currentCharacteristicNameInputField)[0].value=csticName;
	}
	
    function hoverEffectOver(hoverName) {
        var hoverLink = document.getElementsByName(hoverName)[0];
        if (hoverLink != null) {
            hoverLink.style.backgroundColor = "#FFFFDC";
        }
    }

    function hoverEffectOut(hoverName) {
        var hoverLink = document.getElementsByName(hoverName)[0];
        if (hoverLink != null) {
            hoverLink.style.backgroundColor = "";
        }
    }
	
    