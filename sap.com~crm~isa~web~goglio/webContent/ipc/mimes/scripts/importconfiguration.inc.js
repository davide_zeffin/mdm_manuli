/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"                       */
/*                                                    */
/* Document:       importconfiguration.inc.js         */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/*----------------------------------------------------*/

    function onUploadButtonClick(){
        document.importForm.submit();
    }
    
    /* this function is used to reset all paramters from the main frame
     * to ensure that imported configration can be started like any other
     * configuration 
     */
    function importFinished(url){
        if (opener){
            opener.document.currentForm.action = url;
            // reset form values 
            if (opener.document.forms.currentForm.cInstId){
                opener.document.forms.currentForm.cInstId.value = "";
            }
            if (opener.document.forms.currentForm.sInstId){
                opener.document.forms.currentForm.sInstId.value = "";
            }
            if (opener.document.forms.currentForm.cCharGroupName){
                opener.document.forms.currentForm.cCharGroupName.value = "";
            }
            if (opener.document.forms.currentForm.cScrollCharGroupName){
                opener.document.forms.currentForm.cScrollCharGroupName.value = "";
            }
            if (opener.document.forms.currentForm.sCharGroupName){
                opener.document.forms.currentForm.sCharGroupName.value = "";
            }
            if (opener.document.forms.currentForm.characteristicStatusChange){
                opener.document.forms.currentForm.characteristicStatusChange.value = "";
            }
            if (opener.document.forms.currentForm.cCharName){
                opener.document.forms.currentForm.cCharName.value = "";
            }
            opener.document.forms.currentForm.submit();
            window.close();
        }
    }