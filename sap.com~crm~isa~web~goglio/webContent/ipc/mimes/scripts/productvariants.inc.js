/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       productvariants.inc.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function changeToProductVariant(variantID, action) {
        document.getElementsByName(currentProductVariantIdInputField)[0].value = variantID;
        submitForm(action);
    }
    
    function compareProductVariants(action) {
        submitForm(action);
    }
