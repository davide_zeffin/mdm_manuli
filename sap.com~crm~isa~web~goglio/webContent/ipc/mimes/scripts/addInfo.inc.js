/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"                       */
/*                                                    */
/* Document:       addInfo.inc.js                     */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/   
   
    function onAddInfoClick(action, switchParamName) {
       	document.getElementsByName(switchParamName)[0].value = 'T';    
        document.currentForm.action = action;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    } 
    
   