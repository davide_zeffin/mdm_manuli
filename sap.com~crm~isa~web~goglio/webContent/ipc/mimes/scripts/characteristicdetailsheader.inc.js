/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"                       */
/*                                                    */
/* Document:       characteristicdetails.inc.js       */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function backToConfig(
        groupName,
        action
    ) {        
        document.currentForm.action = action;
        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
        sendSubmit();
    }    
