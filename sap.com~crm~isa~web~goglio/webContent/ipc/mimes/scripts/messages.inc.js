/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       messages.inc.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function openConflictSolver(action) {
        document.currentForm.action = action;
        sendSubmit();
    }

