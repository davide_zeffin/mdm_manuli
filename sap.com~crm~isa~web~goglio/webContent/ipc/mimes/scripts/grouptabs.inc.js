/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"                       */
/*                                                    */
/* Document:       grouptabs.inc.jsp                  */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function onGroupChange(
        instanceId,
        groupName,
        scrollGroupName,
        action
    ) {
        if (document.currentForm) {
            document.getElementsByName(currentInstanceIdInputField)[0].value=instanceId;
            document.currentForm.action = action;
            document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=groupName;
            document.getElementsByName(currentScrollCharacteristicGroupNameInputField)[0].value=scrollGroupName;
            
            document.body.style.cursor = "wait";
			calculateWindowSize();
		    showConfigSubmitInProgress(true);
	        try {
   	            document.currentForm.submit();
	        }
	        catch (e){
	            showConfigSubmitInProgress(false);
	            document.body.style.cursor = "default";
	        }
        }   
    }

    function onGroupScroll(
    	instanceId,
        selectedGroup,
        scrollGroupName,
        action
    ) {
        if (document.currentForm) {
            document.getElementsByName(currentInstanceIdInputField)[0].value=instanceId;        
            document.currentForm.action = action;
            document.getElementsByName(selectedCharacteristicGroupNameInputField)[0].value=selectedGroup;
            document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=selectedGroup;
            document.getElementsByName(currentScrollCharacteristicGroupNameInputField)[0].value=scrollGroupName;

            document.body.style.cursor = "wait";
			calculateWindowSize();
		    showConfigSubmitInProgress(true);
	        try {
   	            document.currentForm.submit();
	        }
	        catch (e){
	            showConfigSubmitInProgress(false);
	            document.body.style.cursor = "default";
	        }
        }   
    }

  