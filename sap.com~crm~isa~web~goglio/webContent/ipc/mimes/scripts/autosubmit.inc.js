/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"                       */
/*                                                    */
/* Document:       autosubmit.inc.js                  */
/* Description:    functions for automatic submit     */
/*                 when focus is lost to area out-    */
/*                 side of the config area.           */
/*                 Since this functionality is needed */
/*                 only in cases where the config area*/
/*                 is separate from the enclosing app-*/
/*                 lication (CRM online, MSA,         */
/*                 ERP, People Centric UI),           */
/*                 and not in ISA b2b, b2c, the       */
/*                 functions are not implemented here */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function configUILeft(evt) {
    }
    
    function configGridUiLeft(){
    }  