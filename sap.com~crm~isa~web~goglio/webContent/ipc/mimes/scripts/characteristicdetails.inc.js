/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"                       */
/*                                                    */
/* Document:       characteristicdetails.inc.js       */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

	function hoverEffectOver(hoverName) {
	    var hoverLink = document.getElementsByName(hoverName)[0];
	    if (hoverLink != null) {
	        hoverLink.style.backgroundColor = "#FFFFDC";
	    }
	}
	
	function hoverEffectOut(hoverName) {
	    var hoverLink = document.getElementsByName(hoverName)[0];
	    if (hoverLink != null) {
	        hoverLink.style.backgroundColor = "";
	    }
	}
