<%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.util.WebUtil"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
%><script type="text/javascript"><%
	%>var currentInstanceIdInputField = '<%= Constants.CURRENT_INSTANCE_ID %>';<%
	%>var instanceTreeStatusChangeInputField = '<%= Constants.INSTANCE_TREE_STATUS_CHANGE %>';<%
	%>var currentCharacteristicGroupNameInputField = '<%= Constants.CURRENT_CHARACTERISTIC_GROUP_NAME %>';<%
	%>var currentScrollCharacteristicGroupNameInputField = '<%= Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME %>';<%
	%>var selectedCharacteristicGroupNameInputField = '<%= Constants.SELECTED_CHARACTERISTIC_GROUP_NAME %>';<%
    %>var currentCharacteristicNameInputField = '<%= Constants.CURRENT_CHARACTERISTIC_NAME %>';<%
    %>var currentValueNameInputField = '<%= Constants.CURRENT_VALUE_NAME %>';<%
	%>var characteristicStatusChangeInputField = '<%= Constants.CHARACTERISTIC_STATUS_CHANGE %>';<%
	%>var currentConflictIdInputField = '<%= Constants.CURRENT_CONFLICT_ID %>';<%
	%>var currentConflictParticipantIdInputField = '<%= Constants.CURRENT_CONFLICT_PARTICIPANT_ID %>';<%
	%>var customizationListExpandedInstancesInputField = '<%= InternalRequestParameterConstants.CUSTOMIZATIONLIST_EXPANDED_INSTANCES %>';<%
	%>var currentProductVariantIdInputField = '<%= Constants.CURRENT_PRODUCT_VARIANT_ID %>';<%
	%>var parameterValidParamName = '<%= InternalRequestParameterConstants.PARAMETER_VALID %>';<%
	%>var comparisonShowAllInputField = '<%= Constants.COMPARISON_SHOW_ALL %>';<%	
	%>var comparisonFilterInputField = '<%= Constants.COMPARISON_FILTER_FLAG %>';<%	
	%>var techKeyInputField = '<%= InternalRequestParameterConstants.TECHKEY %>';<%		
	%>var defaultSetValuesAction = '<%= WebUtil.getAppsURL(pageContext,
												null,
												 "/ipc/dispatchSetValuesOnCaller.do",
												 null,
												 null,
												 false) %>';<%

	// controls whether export dialog has to be triggered
	%>var startExportFlag = 'false';<%	
    String startExport = (String) request.getAttribute(InternalRequestParameterConstants.EXPORT_START);
	if (startExport != null && startExport.equals(RequestParameterConstants.T)) {
	    %>startExportFlag = 'true';<%
	}
	// the URL for the export dialog (used in js-function startExport())
	%>var exportURL = '<%= WebUtil.getAppsURL(pageContext,
													null,
													 "/ipc/tiles/exportConfiguration.jsp",
													 null,
													 null,
													 false) %>';
	var posBehindFirstDot = location.hostname.indexOf(".")+1;
	if (posBehindFirstDot > 0) {
		document.domain = location.hostname.substr(posBehindFirstDot);
	}
	var acceptButtonAction = '<%= WebUtil.getAppsURL(pageContext,
                                                     null,
                                                     uiContext.getPropertyAsString(
                                                     RequestParameterConstants.APPLY_BUTTON_ACTION),
                                                     null,
                                                     null,
                                                     false) %>';	
	var saveUIModelAction = '<%= WebUtil.getAppsURL(pageContext, 
													null,
													"dynamicUIMaintenance/save.do",
													null,
													null,
													false) %>';	

</script><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/autosubmit.inc.js"/>" type="text/javascript"><%
%></script><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/jscripts.js"/>" type="text/javascript"><%
%></script><%
%>