
function changeShowAll(showAllFlag, action) {
    document.getElementsByName(comparisonShowAllInputField)[0].value = showAllFlag;
    submitForm(action);
}

function changeFilter(filterFlag, action) {
    document.getElementsByName(comparisonFilterInputField)[0].value = filterFlag;
    submitForm(action);
}

function applySnapshot(action) {
    // reset form values to avoid NullPointerException if
    // an instance is not existing in the snapshot we want to switch to
    if (document.currentForm.cInstId){
        document.currentForm.cInstId.value = "";
    }
    if (document.currentForm.sInstId){
        document.currentForm.sInstId.value = "";
    }
    if (document.currentForm.cCharGroupName){
        document.currentForm.cCharGroupName.value = "";
    }
    if (document.currentForm.cScrollCharGroupName){
        document.currentForm.cScrollCharGroupName.value = "";
    }
    if (document.currentForm.sCharGroupName){
        document.currentForm.sCharGroupName.value = "";
    }
    if (document.currentForm.characteristicStatusChange){
        document.currentForm.characteristicStatusChange.value = "";
    }
    if (document.currentForm.cCharName){
        document.currentForm.cCharName.value = "";
    }
    submitForm(action);
}