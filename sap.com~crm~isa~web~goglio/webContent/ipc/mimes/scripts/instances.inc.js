/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       instances.inc.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    /*
    /* par means local parameter, to avoid naming conflicts with global parameter
    */
    function onInstanceChange(
        parInstanceId,
        parInstanceTreeStatusChange,
        parGroupName,
        parAction
     ) {
        document.getElementsByName(currentInstanceIdInputField)[0].value=parInstanceId;
        document.getElementsByName(instanceTreeStatusChangeInputField)[0].value=parInstanceTreeStatusChange;
        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=parGroupName;
        document.currentForm.action = parAction;

        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    }

    function onInstanceExpandOrCollaps(
        parInstanceId,
        parInstanceTreeStatusChange,
        parGroupName,
        parAction
     ) {
        document.getElementsByName(currentInstanceIdInputField)[0].value=parInstanceId;
        document.getElementsByName(instanceTreeStatusChangeInputField)[0].value=parInstanceTreeStatusChange;
        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=parGroupName;
        document.currentForm.action = parAction;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    }
    