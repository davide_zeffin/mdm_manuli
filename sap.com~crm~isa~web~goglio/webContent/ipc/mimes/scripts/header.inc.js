/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       header.inc.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function onConflictButtonClick(action, csticName) {
        document.currentForm.action = action;
        document.getElementsByName(currentCharacteristicNameInputField)[0].value = csticName;
        sendSubmit();
    }
    function onCustomerButtonClick(action, target) {
        if (document.currentForm) {
            document.currentForm.action = action;
            document.currentForm.target = target;
            document.currentForm.submit();
        }
    }
    function onCustomizeButtonClick(action) {
        if (document.currentForm) {
            document.currentForm.action = action; 
            document.currentForm.submit();
        }
    }
   function onCheckButtonClick(action) {
        document.currentForm.action = action + '#C_' + document.getElementsByName(currentCharacteristicNameInputField)[0].value;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    }
    function onResetButtonClick(
        instanceId,
        csticName,
        groupName,
        action,
        warning) {
        if (showWarning(warning)) {
	        document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	        document.getElementsByName(currentCharacteristicNameInputField)[0].value = csticName;
	        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
	        document.currentForm.action = action;
	        sendSubmit();
	    }
    }
    function onBackButtonClick(
        instanceId,
        csticName,
        groupName,
        action,
        warning,
        showPopUp) {
        // show pop-up only if showPopUp=true
        if (showPopUp == 'true'){
	    	if (showWarning(warning)) {
	    		// submit only if ok was pressed
		        document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
		        document.getElementsByName(currentCharacteristicNameInputField)[0].value = csticName;
		        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
		        document.currentForm.action = action;    	
				sendSubmit();
	    	}
        }
        else {
	        document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	        document.getElementsByName(currentCharacteristicNameInputField)[0].value = csticName;
	        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
	        document.currentForm.action = action;    	
			sendSubmit();
        }
        
        
    }
    
    function onSettingsButtonClick(instanceId, groupName, action) {
	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	    document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
        document.currentForm.action = action;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    }	

function onShowConflictTraceButtonClick(instanceId, groupName, action) {
	document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
	document.currentForm.action = action;
	document.body.style.cursor = "wait";
	calculateWindowSize();
	showConfigSubmitInProgress(true);
	try {
		document.currentForm.submit();
	}
	catch (e){
		showConfigSubmitInProgress(false);
		document.body.style.cursor = "default";
	}
}	

    function onTakeSnapshotLinkClick(instanceId, groupName, action) {
       	document.getElementsByName(takeSnapshotFlagParamName)[0].value = 'T';
	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	    document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
        document.currentForm.action = action;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    }    

    function onCompareToStoredLinkClick(instanceId, groupName, action) {
       	document.getElementsByName(comparisonStoredFlagParamName)[0].value = 'T';    
	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	    document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
        document.currentForm.action = action;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    } 
    
    function onCompareToSnapshotLinkClick(instanceId, groupName, action) {
       	document.getElementsByName(comparisonSnapFlagParamName)[0].value = 'T';    
	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	    document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
        document.currentForm.action = action;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    } 
    
    function onPreviewButtonClick(action) {
       	document.getElementsByName(previewStoredFlagParamName)[0].value = 'T';    
        document.currentForm.action = action;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    } 
    
    function returnKeyForSearch(instanceId, groupName, event, action) {
    	if (event.keyCode == 13) {
    		onSearchSetButtonClick(instanceId, groupName, action);
    	}
    }
	function onSearchSetButtonClick(instanceId, groupName, action){
    	document.getElementsByName(searchSetFlagParamName)[0].value = 'T';
	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	    document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
        document.currentForm.action = action + '#C_' + document.getElementsByName(searchSetValueParamName)[0].value;
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
	}
	function onExportButtonClick(instanceId, groupName, action){
		document.getElementsByName(exportFlagParamName)[0].value = 'T';
	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	    document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;		
        document.currentForm.action = action;
  	    document.currentForm.submit();
    }
    function openImportDialog(url){
    	window.open(url,"ImportConfiguration", "height=300,width=300,status=yes,resizable=yes");
    }
    function openInfoPage(url){
    	window.open(url,"Info", "height=550,width=500,status=yes,resizable=yes,scrollbars=yes");
    }	    