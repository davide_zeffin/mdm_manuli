/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       jscripts.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

	var useNormalConfigProgressIndicator = true;
    var firstSubmit = 'true'; 
    var unsubmittedChanges = 'false';
    var docTop = 0;
	var docLeft = 0;
	var docHeight = 0;
	var docWidth = 0;

    /*
    The variable firstSubmit is true after the page is loaded.
    The function sendSubmit prevents two requests to be issued
    from the same form.
    */
    function sendSubmit() {
        if (firstSubmit == 'true') {   
            firstSubmit = 'false';
            document.body.style.cursor = "wait";
		    
			calculateWindowSize();
		    showConfigSubmitInProgress(true);
	        try {
                document.currentForm.submit();
	        }
	        catch (e) {
	            showConfigSubmitInProgress(false);
				firstSubmit = 'true';
	            document.body.style.cursor = "default";
	        }
        }
    }
    function submitForm(action) {
        document.currentForm.action = action;
        sendSubmit();
    }
    
    function submitChanges() {
        if (unsubmittedChanges == 'true') {
            sendSubmit();
        }
    }
    
    /* onAcceptButtonClick() is in jscripts.js since called from outside */
    /* and not only from the header. Needs to be present in settingsHeader and */  
    /* and comparisonHeader as well.                                           */
    function onAcceptButtonClick(action) {
        if (action != null) {
            document.currentForm.action = action;
        } else {
            document.currentForm.action = acceptButtonAction;    	
        }
		// sendSubmit();
		// Don't use sendSubmit() here because it sends the data only if the flag
		// "firstSubmit" is true. But in the online_evaluate=T case it could happen 
		// that the js-function onCsticValueChange() already set the flag to false. 
		// Then the click on the accept would not work. This happened if the user
		// entered a value in the input-field and did neither press enter nor leave
		// the field but directly click on the accept-button.
		// With the "document.currentForm.submit()" statement we force a submit of the data.
        document.body.style.cursor = "wait";
    	calculateWindowSize();
	    showConfigSubmitInProgress(true);
	    try {
   	    	document.currentForm.submit();
	    }
	    catch (e){
	    	showConfigSubmitInProgress(false);
	        document.body.style.cursor = "default";
	    }
    }  

    function onSaveUIModelButtonClick() {
        if (document.currentForm) {
            document.currentForm.action = saveUIModelAction; 
       		sendSubmit();
        }
    }    


    function openMimeInNewWindow(url) {
        var sat=window.open(url, 'MimeDetail',
                             'width=640,height=480,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
        sat.focus();
    }
    function openMimeInSameWindow(action, addURL, groupName, instanceId){
        document.currentForm.action = action + addURL;
	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	    document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;        
        sendSubmit();
    }
    function showWarning(text){
    	var execute = window.confirm(text);
    	return execute;
    }
    function returnKeyForCurrentForm(event, action, instanceId, groupName) {
    	if (event.keyCode == 13) {
    	    document.getElementsByName(currentInstanceIdInputField)[0].value = instanceId;
	    	document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value = groupName;
            document.currentForm.action = action + '#C_' + document.getElementsByName(currentCharacteristicNameInputField)[0].value;
            sendSubmit();
    	}
    }
    function returnKeyForCurrentFormDefault(event) {
    	if (event.keyCode == 13) {
            document.currentForm.action = defaultSetValuesAction;
            sendSubmit();
    	}
    }
    function startExport(){
        if (window.setElementFocus) {
            setElementFocus();
            
            /*Adjustment for autoscroll which takes the current cstic above the menu bar
            This code will scroll the screen up so that the current cstic is visible to the 
            user after refresh
            
            The height of the menu bar is 39px*/
            
            if( typeof( window.innerWidth ) == 'number' ) {
				//Non-IE
				docTop = self.pageYOffset;
				window.scroll(0,docTop-40);
			} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
				//IE 6+ in 'standards compliant mode'
				docTop = document.documentElement.scrollTop;
				window.scrollTo(0,docTop-40);
			} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
				//IE 4 compatible
				docTop = document.body.scrollTop;
				window.scrollTo(0,docTop-40);
			}
            
        }
    	if (startExportFlag == 'true') {
    		window.open(exportURL,"ExportConfiguration", "height=1,width=1,personalbar=no,location=no,menubar=no,toolbar=no");
    	}
    }
    
    function removeConflictParticipant(action, currentConflict, participant) {
        document.currentForm.action = action;
        document.getElementsByName(currentConflictIdInputField)[0].value = currentConflict;
        if (participant != '') {
            document.getElementsByName(currentConflictParticipantIdInputField)[0].value = participant;
        }
        sendSubmit();
    }

	function showConfigSubmitInProgress(show){
	  if(isNormalConfigProgressIndicator()){

	      var submitObject = document.getElementById("submitInProgress");
	      var submitObjectI = document.getElementById("submitInProgressI");
	      if (submitObject != null){
	          var submitWindowHeight = 80;
	          var submitWindowWidth = 150;
	          if (show == false){
	              submitObject.style.display = "none";
	              submitObjectI.style.display = "none";
	          }
	          else{
	              var height=docHeight;
	              var width=docWidth;
	              var left=docLeft;
	              var top=docTop;
	              left+=(width - submitWindowWidth)/2;
	              top+=(height - submitWindowHeight)/2;
	              submitObject.style.left = left + 'px';
	              submitObjectI.style.left = left + 'px';
	              submitObject.style.top = top + 'px';
	              submitObjectI.style.top = top + 'px';
	              submitObject.style.display = "block";
   	              submitObjectI.style.display = "block";
	          }
	      }
	   }
	   else{
	      if(show){
	        document.body.style.cursor = "wait";
	      }
	      else{
	        document.body.style.cursor = "default";
	      }
	   }
	}

	function isNormalConfigProgressIndicator(){
	  return useNormalConfigProgressIndicator ;
	}	
	
	function calculateWindowSize(){
	    // window size
	    if (self.innerHeight){ // all browsers except Explorer
	        docWidth = self.innerWidth;
	        docHeight = self.innerHeight;
	    }
	    else if (document.documentElement && document.documentElement.clientHeight){ // Explorer 6 
	        docWidth = document.documentElement.clientWidth;
	        docHeight = document.documentElement.clientHeight;
	    }
	    else if (document.body){ // other Explorers
	        docWidth = document.body.clientWidth;
	        docHeight = document.body.clientHeight;
	    }
	    
	    // scroll position
	    if (self.pageYOffset){  // all except Explorer
	        docLeft = self.pageXOffset;
	        docTop = self.pageYOffset;
	    }
	    else if (document.documentElement && document.documentElement.scrollTop){ // Explorer 6 
	        docLeft = document.documentElement.scrollLeft;
	        docTop = document.documentElement.scrollTop;
	    }
	    else if (document.body){ // other Explorers
	        docLeft = document.body.scrollLeft;
	        docTop = document.body.scrollTop;
	    }	
	}   
	
    function clearCharVals(action, instanceId, groupName, charName) {
        document.currentForm.action = action;
        document.getElementsByName(currentInstanceIdInputField)[0].value=instanceId;
        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=groupName;
        document.getElementsByName(currentCharacteristicNameInputField)[0].value = charName;
        sendSubmit();
    }
    
    function onCsticValueChange(instanceId, csticName, action) {
		document.getElementsByName(currentInstanceIdInputField)[0].value=instanceId;
		document.getElementsByName(currentCharacteristicNameInputField)[0].value=csticName;
        document.currentForm.action = action + '#C_' + csticName;
        sendSubmit();
    }    
    
    function getDetails(instId, charGroupName, charName, action) {
        document.getElementsByName(currentInstanceIdInputField)[0].value=instId;
        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=charGroupName;
        document.getElementsByName(currentCharacteristicNameInputField)[0].value=charName;
        document.currentForm.action = action + '#C_' + charName;
        sendSubmit();
    }
    
    function showAllOptions(instanceId, groupName, charName, techKey, action) {
        document.getElementsByName(currentInstanceIdInputField)[0].value=instanceId;
        document.getElementsByName(currentCharacteristicGroupNameInputField)[0].value=groupName;
        document.getElementsByName(currentCharacteristicNameInputField)[0].value=charName;
        document.getElementsByName(techKeyInputField)[0].value=techKey;
        document.currentForm.action = action + '#C_' + charName;
        sendSubmit();
    }
    
    function openDetail(url) {
        var sat = window.open( url, 'Detail',
                               'width=640,height=530,menubar=no,location=no,scrollbars=yes,resizable=yes' );
        sat.focus();
    }
    
    