/*----------------------------------------------------*/
/* Javascript for the "SAP IPC"               */
/*                                                    */
/* Document:       conflictdetails.inc.js                        */
/* Description:    utilities                          */
/* Version:        1.0                                */
/* Author:                                            */
/* Creation-Date:                                     */
/* Last-Update:                                       */
/* Parameter: The page using this Javascript needs    */
/*            to declare the following variables      */
/*----------------------------------------------------*/

    function toggleConflictExplanation(conflictLabelId, linkId, explTitle, explTitle2) {
        if (document.getElementsByName(conflictLabelId)[0].style.display=='none') {
            document.getElementsByName(conflictLabelId)[0].style.display='block';
            document.getElementsByName(linkId)[0].firstChild.nodeValue=explTitle;
        } else {
            document.getElementsByName(conflictLabelId)[0].style.display='none';
            document.getElementsByName(linkId)[0].firstChild.nodeValue=explTitle2;                
        }
    }
    