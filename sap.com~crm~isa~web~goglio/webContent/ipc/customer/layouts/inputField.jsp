<%--
/**
 * Base layout for characteristic values pages.
 * Display only input text field.
 **/
--%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/layouts/inputField.jsp"/><%
    String freeTextTile = CharacteristicValuesUI.getIncludeParams(pageContext).getFreeTextTile();
    Hashtable customerParams = CharacteristicValuesUI.getIncludeParams(pageContext).getCustomerParams();

%><table width = "100%"><%
    %><tr><%
        %><td class="layout"><%
        	%><jsp:include page="<%= freeTextTile %>" flush="true"/><%
        %></td><%
    %></tr><%
%></table>