<%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.CharacteristicDetailsRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDetailsUI" %><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><isa:moduleName name = "/ipc/components/characteristicDetails.jsp"/><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/characteristicdetails.inc.js"/>" type="text/javascript"><%
%></script><%
InstanceUIBean instance = (InstanceUIBean)request.getAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_INSTANCE);
CharacteristicUIBean characteristic = (CharacteristicUIBean) request.getAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_CHARACTERISTIC);
GroupUIBean characteristicGroup = (GroupUIBean) request.getAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP);
UIArea csticsArea = new UIArea("cstics", uiContext);

CharacteristicDetailsUI.include(
                                pageContext,
                                uiContext.getJSPInclude("tiles.characteristicDetails.jsp"),
                                uiContext,
                                instance,
                                characteristicGroup,
                                characteristic,
                                csticsArea,
                                customerParams
                                );
                                
%>
