<%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.core.SessionConst"%><%
%><%@ page import = "com.sap.isa.core.Constants"%><%
%><%@ page import = "com.sap.isa.core.UserSessionData"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.constants.CharacteristicsRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicGroupsUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicsUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.GroupTabsUI" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ taglib uri="/isa" prefix="isa"%><%
%><isa:moduleName name = "/ipc/components/characteristics.jsp"/><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
UserSessionData userData = (UserSessionData) session.getAttribute(SessionConst.USER_DATA);
UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
%><script type="text/javascript"><%
	%>var submit;<%
	if (uiContext.getOnlineEvaluate()) {
	    %>submit = 'true';<%
	}else {
		%>submit = 'false';<%
	}
	%>var currentCharacteristicNameInputField = '<%= Constants.CURRENT_CHARACTERISTIC_NAME %>';<%
	%>var characteristicStatusChangeInputField = '<%= Constants.CHARACTERISTIC_STATUS_CHANGE %>';<%
%></script><%
%><script src="<isa:mimeURL name ="/ipc/mimes/scripts/characteristics.inc.js"/>" type="text/javascript"><%
%></script><%
InstanceUIBean currentInstance = (InstanceUIBean) request.getAttribute(CharacteristicsRequestParameterConstants.CURRENT_INSTANCE);
GroupUIBean characteristicGroup = (GroupUIBean) request.getAttribute(CharacteristicsRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP);
GroupUIBean currentScrollGroup = (GroupUIBean) request.getAttribute(CharacteristicsRequestParameterConstants.CURRENT_SCROLL_CHARACTERISTIC_GROUP);
CharacteristicUIBean currentCharacteristic = (CharacteristicUIBean) request.getAttribute(CharacteristicsRequestParameterConstants.CURRENT_CHARACTERISTIC);
Hashtable customerParams = (Hashtable) request.getAttribute(InternalRequestParameterConstants.CUSTOMER_PARAMS);

//List groups = (List)request.getAttribute(CharacteristicsRequestParameterConstants.CHARACTERISTIC_GROUPS);
//to collapse and expand characteristics, the next input field will
//be filled with the name of the clicked characteristic
%><input type="hidden" name="<%= Constants.CHARACTERISTIC_STATUS_CHANGE %>" value=""/><%

List groups = currentInstance.getGroups();
UIArea csticsArea = new UIArea("cstics", uiContext);

//check if the model contains characteristic groups and the customizing is set to display groups
if (groups != null && !groups.isEmpty() && uiContext.getUseGroupInformation()) { // id=1
                    
    CharacteristicGroupsUI.include(pageContext,
	                               uiContext.getJSPInclude("tiles.characteristicGroup.jsp"),
                                   uiContext,
                                   currentInstance,
                                   characteristicGroup,
                                   customerParams);
}
else {
        //the following include contains the layout of the characteristics
        CharacteristicsUI.include(pageContext,
  	                              uiContext.getJSPInclude("tiles.characteristics.jsp"),
                                  uiContext,
                                  currentInstance,
                                  characteristicGroup,
                                  csticsArea,
                                  customerParams);
} // end if (id=1)
%>
