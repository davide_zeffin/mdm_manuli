<%--this include contains the UI of the status bar
    using the parameter "configuration" you can get all the
    information about the configuration.
    In this example, the long description of the root instance
    of the configuration is displayed.
--%><%
%><%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.*"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.StatusBarUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean"%><%
%><%@ page import = "com.customer.isa.ipc.ui.jsp.beans.CustomerConfigUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.MessageUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.util.*"%><%
%><%@ page import = "com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/statusbar.jsp"/><%
	BaseUI ui = new BaseUI(pageContext);
//read parameter from caller
StatusBarUI.IncludeParams includeParams = StatusBarUI.getIncludeParams(pageContext);
UIContext uiContext = includeParams.getIpcBaseUI();
ConfigUIBean configuration = includeParams.getConfiguration();
Hashtable customerParams = includeParams.getCustomerParams();

// using the attribute "rootInstance" you can get all the information about the root instance
// This attribute is defined in the file pageheader.jsp which you always should include in your global page.
InstanceUIBean rootInstance = configuration.getRootInstance();
String tableClass = "ipcStatusbar";
%>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.statusbar", null); 
%>
	<a 
		href="#statusbar_group-end"
		id="statusbar_group-begin"
		title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
<table class="<%= tableClass %>" 
<%
	if (ui.isAccessible())
	{
%>
	title="<isa:translate key="ipc.tt.statusbar"/>"
<%
	}
%>
	><%
    %><tr><%
        %><td colspan="1"><%
            %><table border="0" cellpadding="0" cellspacing="0" class="<%= tableClass %>"><%
                if (!uiContext.getHidePrices()) {
                %><colgroup><%
                    %><col width="25%"><%
					%><col width="35%"><%
                    %><col width="10%"><%
					%><col width="20%"><%
                    %><col width="20%"><%
                %></colgroup><%
                }
                else {
                %><colgroup><%
                    %><col width="30%"><%
					%><col width="40%"><%
					%><col width="20%"><%
                    %><col width="20%"><%
                %></colgroup><%
                }
                %><tr><%
                    // we show the language dependent name of the root instance
                    // feel free to show other data of the root instance using the
                    // methods of the API com.sap.spc.remote.client.object.Instance
                    %><td nowrap align="left"><%
                        %><%= rootInstance.getLanguageDependentName() %><%
                    %></td><%
					%><td align="left"><%
						%><%= rootInstance.getDescription() %><%
					%></td><%
                        if (!uiContext.getHidePrices()) {
                    %><td nowrap align="center"><%
                        %><isa:translate key="ipc.price_total"/>:&nbsp;<%= rootInstance.getPrice() %><%
                    %></td><%
                        }
//Shows the status light to display the availibility of the configuration
%><td align="center"><%
	%>Availability:<img alt="<isa:translate key="<%= ((CustomerConfigUIBean)configuration).getAvailibilityImage().getResourceKey() %>"/>" src="<isa:mimeURL name="<%= ((CustomerConfigUIBean)configuration).getAvailibilityImage().getImagePath() %>" />"><%
%></td><%
                    %><td align="right"><%
                        // we show the state of configuration as an image
                        // if the configuration is complete and consistent the traffic lights show green
                        // if the configuration is not complete, but consistent the traffic lights turn yellow
                        // if the configuration is not consistent the traffic lights turn red and yellow
                        if (uiContext.getShowStatusLights()) {
                        %><img alt="<isa:translate key="<%= configuration.getStatusImage().getResourceKey() %>"/>" src="<isa:mimeURL name="<%= configuration.getStatusImage().getImagePath() %>" />"><%
                        }
                        // or we show the configuration status using text
                        else {
                        %><isa:translate key="<%= configuration.getStatusText() %>"/><%
                        }
                    %></td><%
                %></tr><%
            %></table><%
        %></td><%
    %></tr><%
%></table><%
%>
<%
	if (ui.isAccessible())
	{
		String grpTxt = WebUtil.translate(pageContext, "ipc.statusbar", null); 
%>
	<a 
		href="#statusbar_group-begin"
		id="statusbar_group-end"
		title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"></a>
<%
	}
%>
