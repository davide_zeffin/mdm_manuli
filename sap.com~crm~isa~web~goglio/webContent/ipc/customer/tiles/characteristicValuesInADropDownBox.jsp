<%--
    THIS INCLUDE CONTAINS THE UI FOR THE MINIMIZED DISPLAY OF
    CHARACTERISTIC VALUES
    Display all values as text only, if the displaymode is switched on
--%>
<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants"%><%
%><%@ page import = "com.sap.isa.core.util.*"%><%
%><%@ page import = "java.util.Iterator"%><%
%><%@ page import = "java.util.List"%><%
%><%@ page import = "java.util.Hashtable"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.GroupUIBean"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.beans.ValueUIBean" %><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicValuesUI"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIContext"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIElement"%><%
%><%@ page import = "com.sap.isa.ipc.ui.jsp.uiclass.UIArea"%><%
%><%@ page import="com.sap.isa.ui.uiclass.BaseUI" %><%
%><%@ page import = "com.sap.isa.core.util.JspUtil"%><%
%><%@ page errorPage="/appbase/jspruntimeexception.jsp" %><%
%><isa:moduleName name = "/ipc/tiles/characteristicValuesInADropDownBox.jsp"/><% 

	BaseUI ui = new BaseUI(pageContext);
    boolean openSelect = false;
    boolean intervalValues = false;
    
    //read passed variables from request
    CharacteristicValuesUI.IncludeParams includeParams = CharacteristicValuesUI.getIncludeParams(pageContext);
    UIContext uiContext = includeParams.getIpcBaseUI();
    InstanceUIBean currentInstance = includeParams.getInstance();
    GroupUIBean currentGroup = includeParams.getGroup();
    CharacteristicUIBean currentCharacteristic = includeParams.getCurrentCharacteristic();
    List values = includeParams.getValues();
    UIArea csticsArea = includeParams.getUiArea();
    boolean readOnlyMode = includeParams.isReadOnlyMode();
    Hashtable customerParams = includeParams.getCustomerParams();

    if( currentCharacteristic.hasIntervalAsDomain() && currentCharacteristic.allowsMultipleValues()
            && currentCharacteristic.hasAssignedValues() )
        intervalValues = true;
    if( !uiContext.getPropertyAsBoolean( RequestParameterConstants.DISPLAY_MODE )
             && !currentCharacteristic.isReadOnly() && currentCharacteristic.isVisible() )  {
        openSelect = true;
        String currentCsticName = currentCharacteristic.getName();
        String name = "characteristic_" + currentCsticName;
        String instanceId = currentInstance.getId();
        String groupName = currentGroup.getName();
        StringBuffer actionBuffer = new StringBuffer().append(WebUtil.getAppsURL(
                                                              pageContext,
                                                              null,
                                                              "/ipc/dispatchSetValuesOnCaller.do",
                                                              null,
                                                              null,
                                                              false));
        //actionBuffer = actionBuffer.append("#C_").append(currentCsticName);
        // assemble onChange attribute
        StringBuffer onChangeBuffer = new StringBuffer().append("javascript:onCsticValueChange('");
		onChangeBuffer.append(instanceId);
		onChangeBuffer.append("','");
		onChangeBuffer.append(groupName);
		onChangeBuffer.append("','");
        onChangeBuffer.append(currentCsticName);
        onChangeBuffer.append("','");
        onChangeBuffer.append(actionBuffer);
        onChangeBuffer.append("');");
        String onChange = onChangeBuffer.toString();
        // assemble onkeypress attribute
        StringBuffer onKeypressBuffer = new StringBuffer().append("javascript:returnKeyForCurrentForm(event, '");
        onKeypressBuffer.append(actionBuffer);
        onKeypressBuffer.append("','");
		onKeypressBuffer.append(instanceId);
		onKeypressBuffer.append("','");
		onKeypressBuffer.append(groupName);        
        onKeypressBuffer.append("');");
        String onKeypress = onKeypressBuffer.toString();      
        String size = "";
        String multiple = "";
        if( currentCharacteristic.allowsMultipleValues() )  {
            int threshold = uiContext.getPropertyAsInteger(RequestParameterConstants.MULITIPLE_VALUES_THRESHOLD, 4);
            int isize = values.size() + 1;
            if( isize > threshold ) {
                isize = threshold;
            }
            size = String.valueOf( isize );
            multiple = "multiple";
        }
		if (ui.isAccessible()) {
			String csticName = currentCharacteristic.getDisplayName();
%><label for="<%= currentCsticName %>"><isa:translate key="ipc.acc.value" arg0="<%=csticName%>"/></label><%
		}
%><select id="<%= currentCsticName %>" name = "<%= name %>" size = "<%= size %>" <%= multiple %> onChange = "<%= onChange %>"<%
         %> onFocus="onFocus('<%= currentCsticName%>')"<%
         %> onkeypress="<%= onKeypress %>" tabindex="<%= csticsArea.getTabIndex()%>"><%
      //empty line in list in order to reset a value
    %><option value = ""></option><%
    } 
    for ( Iterator characteristicValuesIt = values.iterator(); characteristicValuesIt.hasNext(); ) {
        ValueUIBean characteristicValue = (ValueUIBean) characteristicValuesIt.next();
        if( openSelect && !characteristicValue.isAssignable() )
            continue;
        if( !intervalValues || characteristicValue.isAssignedByUser()) {

			if (openSelect) {
			    String selected = "";
			
			    if (characteristicValue.isAssigned()) {
			        selected = "selected";
			    }
			
			    String value = characteristicValue.getName(); //language independent name
			%><option <%= selected %> value = "<%= JspUtil.encodeHtml(value) %>"<%
			         %> ><%
			    %><%= characteristicValue.getDescription() %><%		
			    if (uiContext.getShowOneToOneConditions() && characteristicValue.getPrice() != null && !characteristicValue.getPrice().equals("")) {
			    %>&nbsp;(<isa:translate key = "ipc.surcharge"/>&nbsp;<%= characteristicValue.getPrice() %>)<%
			    }
			%></option><%
			
			}
			// read-only mode
			else if (characteristicValue.isAssigned()) {
                String cssClassSystemAssigned = "";
				if (ui.isAccessible()) {
					String csticName = currentCharacteristic.getDisplayName();
			%><isa:translate key="ipc.acc.value" arg0="<%=csticName%>"/>&nbsp;(<isa:translate key="ipc.readonly"/>)&nbsp;<%
				}
                // if the value is assigned by system we display it italic
                if (characteristicValue.isAssignedBySystem()) { // (id=2)
	                cssClassSystemAssigned = "systemAssigned";
    	        } // (id=2)
			%><span class="<%= cssClassSystemAssigned %>"><%= characteristicValue.getLanguageDependentName() %></span><br /><%
			}


        }
    }
    if( openSelect ) {
%></select><%
    }
%>