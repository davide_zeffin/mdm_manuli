<%@ taglib uri = "/isa" prefix = "isa"%><%
%><%@ page import = "java.util.Enumeration"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
    <title>Request Monitor</title>
</head>
<body>
<font size=-1>
<h3>Request Monitor</h3>
<table border=1>
    <tr>
        <td>
            AuthType: <%= request.getAuthType() %><br>
        </td>
        <td>
            CharacterEncoding: <%= request.getCharacterEncoding() %><br>
        </td>
        <td>
            ContentType: <%= request.getContentType() %><br>        
        </td>
    </tr>
    <tr>
        <td>
            Locale: <%= request.getLocale() %><br>        
        </td>
        <td>
            Method: <%= request.getMethod() %><br>                
        </td>
        <td>
            Protocol: <%= request.getProtocol() %><br>                
        </td>
    </tr>
    <tr>
        <td>
            Query String: <%= request.getQueryString() %><br>                
        </td>
        <td>
            Remote Address: <%= request.getRemoteAddr() %><br>                
        </td>
        <td>
            Requested Session ID: <%= request.getRequestedSessionId() %><br>        
        </td>
    </tr>
    <tr>
        <td>
            Requested URL: <%=  request.getRequestURL() %><br>        
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
<br><hr><br>
<b>Header</b><br>
<table border="1"><%
    Enumeration headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()){
        String headerName = (String) headerNames.nextElement();
        String header = (String) request.getHeader(headerName);
    %><tr>
        <td>
            <%= headerName %>
        </td>
        <td>
            <%= header %>
        </td><%
    %></tr><%
}
%>
</table>
<br>
<hr><%

Enumeration attributeNames = request.getAttributeNames();
%><b>Request Attributes:</b><br><%
while (attributeNames.hasMoreElements()){
    String attributeName = (String) attributeNames.nextElement();
    Object requestAttributeValue = request.getAttribute(attributeName);
    %><%= attributeName %>: <%= requestAttributeValue %><br><%
}
%><hr><%
Enumeration parameterNames = request.getParameterNames();
%><b>Request Parameters:</b><br><%
while (parameterNames.hasMoreElements()){
    String parameterName = (String) parameterNames.nextElement();
    String parameterValue = (String) request.getParameter(parameterName);
    %><%= parameterName %>: <%= parameterValue %><br><%
}
%>
</font>
</body>
</html>