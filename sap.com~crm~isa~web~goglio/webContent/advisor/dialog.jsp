<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.advisor.beans.*" %>

<jsp:useBean id="pageData" scope="request" type="com.sap.isa.advisor.beans.PageData" />
<jsp:useBean id="scenario" scope="request" type="java.lang.String" />

<isa:contentType />

<% PageSave savedPage = null;
   VerifyErrors errors = null;
if (request.getAttribute("savedData") != null) savedPage = (PageSave)request.getAttribute("savedData");
if (request.getAttribute("errorData") != null) errors = (VerifyErrors)request.getAttribute("errorData");
%>

<html>
  <head>
    <!-- Stylesheet -->

    <isa:includes/> 
   <!-- link href="<isa:mimeURL name="advisor/mimes/advisor.css" language=""/>" type="text/css" rel="stylesheet" -->
    <script language="JavaScript">
    function seeItems() {
      <% if (scenario.equalsIgnoreCase("b2c")) { %>
    	document.parentWindow.opener.parent.center.location="<isa:webappsURL name="/advisor/seeItems.do"/>";
      <% } else {%>
        document.parentWindow.opener.parent.work_history.form_input.location="<isa:webappsURL name="/advisor/seeItems.do"/>";
      <% } %>
    }
    function closeWin() {
      window.close();
    }
    </script>
  </head>
  <title>
  <isa:translate key="advisor.title"/>
  </title>

<body  class="advisor">
   <div><isa:moduleName name="advisor/dialog.jsp"/></div>
<center>
<table  border=0 class="boxout" cellpadding=1 cellspacing=0 width=98%>
<tr>
<td width=100% valign=top>
<table width=100% border=0 class="boxin" cellpadding=5 cellspacing=0>
<tr>
<td valign=top class="boxtitle"><isa:translate key="advisor.title"/></td>

 <!-- display close button -->
<td valign=top class="boxout" align=right nowrap>
<a class="navi" href="javascript:closeWin();">[ <isa:translate key="advisor.closeWin"/> ]</a></td>
</tr>
  <tr><td colspan=2 height=20>&nbsp;</td></tr>
<tr><td colspan=2 nowrap align=center>
   <% if (pageData.getQuestionsCount() > 0) { /*If we have any questions to display*/%>
<img src="<isa:mimeURL name="/advisor/mimes/mandatory.gif" language=""/>" width="13" height="12" border="0">:<isa:translate key="advisor.required"/>
    <% } else { %>
    &nbsp;
    <% } %>
</td></tr>
<tr>
<td colspan=2 valign=top>


   <% if (pageData.getRecommendationCount() > 0) { /*If we have any recommendations to make*/ %>
    <table>
    <tr>
    <td class="message">
     <isa:translate key="advisor.seeItems_left"/>
      <font class="matcheditem"><%=pageData.getRecommendationCount()%></font>
      <isa:translate key="advisor.seeItems_right"/>
   </td>
     </tr>
<tr>
<td align=center><input type="Button" class="button" value="<isa:translate key="advisor.seeItems"/>" onClick="javascript:seeItems();">
      </td>
    </tr>
    </table>
   <% } %>


   <% if (pageData.getQuestionsCount() > 0) { /*If we have any questions to display, ie if there is not the last step*/%>
    <form action="<isa:webappsURL name="/advisor/getData.do"/>" method="post">
   <center>
    <table width="90%">
      <%for (int i=0;i<pageData.getQuestionsCount();i++) {
        QuestionData question = pageData.getQuestion(i);
        QuestionSave savedQuestion = null;
        if (savedPage != null) savedQuestion = savedPage.getQuestion(i);
      %>
        <%if (errors != null && errors.getTyp(i) != null &&
              (errors.getTyp(i).equals(VerifyErrors.NO_MANDATORY_ANSWER) || errors.getTyp(i).equals(VerifyErrors.TOO_MANY_ANSWERS)) ) {
                                                                            /*If this question was wrongly answered */%>
          <tr>
          <td class="error">
                <% if (errors.getTyp(i).equals(VerifyErrors.NO_MANDATORY_ANSWER)) { %>
                  <isa:translate key="advisor.noMandatoryAnswer"/>
                <% } %>
                <% if (errors.getTyp(i).equals(VerifyErrors.TOO_MANY_ANSWERS)) { %>
                  <isa:translate key="advisor.tooManyAnswers"/>
                  <% if (question.getMaxCount() > 0) { %>
                    &nbsp;<isa:translate key="advisor.AnswersLimit_left"/>&nbsp;
                      <%= question.getMaxCount() %>
                    &nbsp;<isa:translate key="advisor.AnswersLimit_right"/>
                  <% } %>
                <% } %>

            </td>
          </tr>
        <% } /*End if this question was wrongly answered*/%>
        <% if (question.getMaxCount() > 1 && question.getMaxCount() < question.getAnswersCount()) {
                                                /*If there is a maxCount > 1 we should tell this to the user*/%>
                    <tr>
                      <td class="message">
                    <isa:translate key="advisor.AnswersLimitInfo_left"/>&nbsp;
                      <%= question.getMaxCount() %>
                    <isa:translate key="advisor.AnswersLimitInfo_right"/>

                      </td>
                    </tr>
        <% } /* End if maxCount > 1*/%>
      <tr>
       <td>
        <% if (question.isRequired()) { /*If the question is required, then we should indicate this by an icon, before the question*/%>
            <img src="<isa:mimeURL name="/advisor/mimes/mandatory.gif" language=""/>" width="13" height="12" border="0" alt="mandatory">
        <% } %>&nbsp;
       <b><%= question.getQuestionText() %></b>
       </td>
      </tr>
      <tr>
       <td>
      <%if (question.hasActivationList()) { /*I should create a table with activations in header*/ %>
        <table width="100%">

            <tr>
              <%int answerWidth = 30; %>
              <td width="<%=answerWidth%>%">
                &nbsp; <!-- this is the column for answers -->
              </td>
              <%int size = (100 - answerWidth)/(question.getActivationListCount()+1); %>
              <td width="<%=size%>%" align="center" valign=top>
                <isa:translate key="advisor.ActivationButton.noIdea"/>
              </td>
              <%for (int j=0;j<question.getActivationListCount();j++) {%>
              <td width="<%=size%>%" align="center" valign=top>
                <%= question.getActivation(j) %>
              </td>
              <% } %>
            </tr>

              <%for (int j=0;j<question.getAnswersCount();j++) {
                  String answer = question.getAnswer(j);
              %>
                <tr>
                  <td>
                  <!-- <input type="hidden" name="answer[<%=i%>]" value=""> -->
                  <%= answer %>
                  </td>
                <%for (int k=-1;k<question.getActivationListCount();k++) { %>
                  <td align="center">
                  <%if (k == -1) {%>
                    <input type="radio" class="radio" name="activ[<%=i%>][<%=j%>]" value="<%= k %>"
                    <%if (savedQuestion == null || savedQuestion.isActivated(j,k)) {%>
                      checked
                    <% } %>
                    >
                  <%} else { %>
                    <input type="radio" class="radio" name="activ[<%=i%>][<%=j%>]" value="<%= k %>"
                    <%if (savedQuestion != null && savedQuestion.isActivated(j,k)) {%>
                      checked
                    <% } %>
                    >
                  <% } %>
                  </td>
                <% } %>
                </tr>
              <% } %>

        </table>
        <% } else { /* has no activation list */ %>
          <table width="100%">
              <%for (int j=0;j<question.getAnswersCount();j++) {
                  String answer = question.getAnswer(j);
              %>
                <tr>
                  <td width="5%" align="center">
                    <% if (question.isSingleAnswer()) { %>
                      <input type="radio" class="radio" name="answer[<%=i%>]" value="<%= j %>"
                        <%if (savedQuestion != null && savedQuestion.containAnswer(j)) {%>
                          checked
                        <% } %>
                      >
                    <% } else {%>
                      <input type="Checkbox" class="checkbox" name="answer[<%=i%>]" value="<%= j %>"
                        <%if (savedQuestion != null && savedQuestion.containAnswer(j)) {%>
                          checked
                        <% } %>
                      >
                    <% } %>
                  </td>
                  <td width="90%">
                    <%= answer %>
                    <!-- <input type="hidden" name="activ[<%=i%>][<%=j%>]" value="-1"> -->
                  </td>
              <% } %>
          </table>
        <% } /* end if */%>
       </td>
      </tr>
      <% } /* end for looping through the questions*/ %>
    </table>
   </center>
<!-- Link to the previous states -->
<table width="100%" align=right>
  <tr>

    <td align=right>
 <input type="submit" class="button" value="<isa:translate key="advisor.Submit"/>" name="Sumbit">
    </td>
  </tr>

<tr>

          <td nowrap>
<hr size=1 shade width=100%>


<isa:translate key="advisor.page"/><%for (int i=1;i<pageData.getDialogIndex();i++) { %>
&nbsp;<a href="<isa:webappsURL name="/advisor/getNewState.do"/>?newStateID=<%=i%>"><%= i %></a>
<% } %>&nbsp;<%=pageData.getDialogIndex()%>


          </td>
        </tr>
      </table>


</form>
<% } else { /* end if no of questions > 0 */ %>
     <table>
    <tr>
      <td>
       <br>
      <isa:translate key="advisor.restart1"/>
      <a href="<isa:webappsURL name="/advisor/advisorStart.do"/>?restart=yes"><isa:translate key="advisor.restart2"/></a>

      </td>
    </tr>
    </table>
<% } %>


</td>
</tr>
<tr><td height=20 colspan=2>&nbsp;</td></tr>
</table>
</td></tr></table>
</center>






  </body>
</html>
