<%@ page language="java" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.advisor.AdvisorConstants" %>

<isa:contentType />

<html>
  <head>
    <!-- Stylesheet -->
   
     <isa:includes/> 
   <!-- link href="<isa:mimeURL name="advisor/mimes/advisor.css" language=""/>" type="text/css" rel="stylesheet" -->
    <script language="JavaScript1.2">

    function dummy()
    {
    }
    </script>
  <title>
  <isa:translate key="advisor.title"/>
  </title>
  </head>
 <body  class="advisor">
      <div><isa:moduleName name="advisor/error.jsp"/></div>
      <%String errCode=null;
        if (request.getAttribute(AdvisorConstants.ADV_ERROR_LEVEL) != null) {
        errCode = (String) request.getAttribute(AdvisorConstants.ADV_ERROR_LEVEL);
      %>

<center>
<table  border=0 class="boxout" cellpadding=1 cellspacing=0 width=98%> 
<tr>
<td width=100% valign=top>                                                                                                                                                                                                                                                                                                                         
<table width=100% border=0 class="boxin" cellpadding=5 cellspacing=0> 

<tr>                                                                                                                                                                                                                                                                                                                                    
<td valign=top class="boxtitle"><isa:translate key="advisor.title"/></td></tr>          
  <tr><td height=20>&nbsp;</td></tr>       
<tr>                                                                                                                                                            
<td>    <font class="error"> 

          <isa:translate key="advisor.error0"/><br>
          <!-- <%= errCode %> -->
      <%if (errCode.equals(AdvisorConstants.ADV_ERR_NO_BOM)) { %>
          <isa:translate key="advisor.error1"/>
      <% } %>
      <%if (errCode.equals(AdvisorConstants.ADV_ERR_NO_CAT)) { %>
          <isa:translate key="advisor.error2"/>
      <% } %>
      <%if (errCode.equals(AdvisorConstants.ADV_ERR_NO_BO)) { %>
          <isa:translate key="advisor.error3"/>
      <% } %>
      <%if (errCode.equals(AdvisorConstants.ADV_ERR_ON_BACKEND)) { %>
          <isa:translate key="advisor.error4"/>
      <% } %>
      <%if (errCode.equals(AdvisorConstants.ADV_ERR_GETTING_DATA)) { %>
          <isa:translate key="advisor.error5"/>
      <% } %>
      <%if (errCode.equals(AdvisorConstants.ADV_ERR_INCONSISTENT_STATE)) { %>
          <isa:translate key="advisor.error6"/>
      <% } %>
      <%if (errCode.equals(AdvisorConstants.ADV_NO_INSTANCE)) { %>
          <isa:translate key="advisor.error7"/>
      <% } %>
      <%} else { %>
          <isa:translate key="advisor.error"/>
      <% } %></font>

<br>
<br>
      <%if (errCode == null || !errCode.equals(AdvisorConstants.ADV_NO_INSTANCE)) { %>
      <isa:translate key="advisor.error1"/>
      <a href="<isa:webappsURL name="/advisor/advisorInit.do"/>"><isa:translate key="advisor.error2"/></a>
     
      <% } %>
  </td>                                                                                                                                                                                                                                                                                                 
</tr>                                                                                                                                                                                                                                                                                                                
<tr><td height=20>&nbsp;</td></tr>                                                                                                                                                                             
</table>                                                                                                                                                                      
   </td></tr></table>                                                                                                                                                                                                                                                                                               
</center>    
  


        
  </body>
</html>
