<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>SAP Internet Sales B2B - New Order</title>
    <isa:includes/>
    <!--
    <link href="css/b2b.css" type="text/css" rel="stylesheet">
    -->

	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>		
	
      <script src="jscript/new.js" type="text/javascript"></script>
	  
      <script src="jscript/reload.js" type="text/javascript"></script>	
	

  </head>
  <body>
    <div id="new-doc" class="module">
      
      <div class="module-name"><isa:moduleName name="editabledocnav.jsp" /></div>
      <form action="" name="newOrder" onsubmit="return load_new_doc();">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td>
                <div class="opener">Neues Dokument</div>
                <div>
                  <select class="submitDoc" name="docType">
                    <option value="not_possible">Dokumenttyp...</option>
                    <option value="not_possible">&nbsp;&nbsp;&nbsp;Auftrag</option>
                    <option value="not_possible">&nbsp;&nbsp;&nbsp;Auftrag mit Bestellvorlage</option>
                    <option value="not_possible">&nbsp;&nbsp;&nbsp;Angebot</option>
                    <option value="not_possible">&nbsp;&nbsp;&nbsp;Angebot mit Bestellvorlage</option>
                    <option value="not_possible">&nbsp;&nbsp;&nbsp;Bestellvorlage</option>
                  </select>&nbsp;&nbsp;<input class="submitDoc" type="submit" value="anlegen">
                </div>
              </td>
              <td class="actualDocTab">
                <div>[*] <var class="main">Auftrag</var>
                  <var>Neu</var><br>
                  <var>3086707</var><br>
                  <var>23.01.01</var></div>
              </td>
            </tr>
            <tr>
              <td class="docLine">&nbsp;</td>
              <td class="actualDocLine">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="actualButtonLine">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="actualButtonLine2"><br><br><br><br><br><br></td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </body>
</html>