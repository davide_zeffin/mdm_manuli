<%--
********************************************************************************
    File:         campaignEnrollmentMulti.inc.jsp
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      05.12.2006 Nikolaus
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/12/05 $
********************************************************************************
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib prefix="isacore" uri="/isacore" %>

<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.CampaignDetailUI" %>
<%@ page import="com.sap.ecommerce.businessobject.campaign.CampaignEnrollment" %>
<%@ page import="com.sap.ecommerce.businessobject.campaign.CampaignEnrollmentList" %>

<div class="module-name"><isa:moduleName name="b2b/campaignEnrollmentMulti.inc.jsp" /></div>

<%-- CampaignEnrollmentList enrollmList = ui.getEnrollmList();
--%>

<%-- if (enrollmList != null) { --%> 
<% String[] rowValues = {"odd", "even"};
   JspUtil.Alternator oddEven = new JspUtil.Alternator(rowValues);
   String oddEvenStr = ""; %>
<div class="document-items">

   <% String tableTitle = WebUtil.translate(pageContext, "ecom.camp.enrollment.table", null);
      if (ui.isAccessible()) { %>
         <a id="enrollmTableBegin" href="#enrollmTableEnd" title="<isa:translate key="access.table.begin" arg0="<%= tableTitle %>" arg1="<%=Integer.toString(ui.getRowNumber())%>" arg2="<%=Integer.toString(ui.getColNumber())%>" arg3="<%=Integer.toString(ui.getFirstBPIndex())%>" arg4="<%=Integer.toString(ui.getLastBPIndex())%>"/>"></a>
   <% } %>
   
  <table class="itemlist" id="enrollmTable" summary="<%= tableTitle %>">
    <thead>
      <tr>
        <th id="col_1" scope="col" title="<isa:translate key='ecom.camp.enrolleeName'/>"><isa:translate key='ecom.camp.enrolleeName'/></th>
        <th id="col_2" scope="col" title="<isa:translate key='ecom.camp.enrolleeId'/>"><isa:translate key='ecom.camp.enrolleeId'/></th>
        <th id="col_3" scope="col" title="<isa:translate key='ecom.camp.firstEnrolledByDate'/>"><isa:translate key='ecom.camp.firstEnrolledByDate'/></th>
        <th id="col_4" scope="col" title="<isa:translate key='ecom.camp.lastChangedByDate'/>"><isa:translate key='ecom.camp.lastChangedByDate'/></th>
        <th id="col_5" scope="col" title="<isa:translate key='ecom.camp.enrollment.enroll'/>"><isa:translate key='ecom.camp.enrollment.enroll'/>
             <br>
             <input type="checkbox" id="enrollall" name="enrollall" value="" onclick="javascript:enrollAllChanged(<%= ui.getRowNumber() %>)" 
                    <% if (ui.areAllBpOnPageEnrolled()) { %>
                           checked="checked" 
                    <% } %>
                    <% if (ui.isDisplayOnly()) { %>
                           disabled="disabled" 
                    <% } %>
             >
        </th>
      </tr>
    </thead>
    
    <tbody>
      <% int i = -1;%>
      <isa:iterate id="enrollm" name="enrollmListPage" type="com.sap.ecommerce.businessobject.campaign.CampaignEnrollment">      
          <% if (i % 2 == 0) {
                 oddEvenStr = "even";
             }
             else {
                 oddEvenStr = "odd";
             }
             i++;
          %>
          <tr class="<%= oddEvenStr %>">
            <%-- Enrollee TechKey --%>
            <div style="display: none"> <%-- make hidden fields invisible because of IE bug --%>
                <input type="hidden" name="enrolleeGUID[<%= i %>]" value="<%= enrollm.getEnrolleeGuid() %>">
            </div>
            
            <td headers="col_1">
                <%-- Enrollee Name --%>
                <div class="value">
                    <p><span><%= enrollm.getEnrolleeDescription() %></span></p>
                </div>
            </td>    

            <td headers="col_2">
                <%-- Business Partner Id --%>
                <div class="value">
                    <p><span><%= enrollm.getEnrolleeID() %></span></p>
                </div>
            </td>

            <%-- First Enrolled By / Date --%>
            <td headers="col_3">
             <% if (!enrollm.isFirstEnrolledInternally()) { %>
                    <%-- First Enrolled By Contact --%>
                    <div class="contact">
                      <%= enrollm.getFirstEnrolledByContact() %>
                    </div>    
                    <%-- First Enrolled By SoldTo --%>
                    <div class="soldto">
                      <%= enrollm.getFirstEnrolledBySoldTo() %>
                    </div>    
             <% } 
                else { %>
                    <div class="soldto">
		              <isa:translate key="ecom.camp.enrolledInternally"/>
		            </div>    
             <% } %>   
                <%-- First Enrolled Date --%>
                <div class="enrolldate">
                  <%= enrollm.getFirstEnrolledDate() %>
                </div>
             </td>
             
            <%-- Last Changed By / Date --%>
            <td headers="col_4">
             <% if (enrollm.isChangedAfterFirstEnrollment()) { %>
	             <% if (!enrollm.isLastChangedInternally()) { %>
	                    <%-- Last Changed By Contact--%>
		                <div class="contact">
		                  <%= enrollm.getLastChangedByContact() %>
		                </div>    
		                <%-- Last Changed By SoldTo--%>
		                <div class="soldto">
		                  <%= enrollm.getLastChangedBySoldTo() %>
		                </div>    
	             <% } 
	                else { %>
	                    <div class="soldto">
			              <isa:translate key="ecom.camp.changedInternally"/>
			            </div>    
	             <% } %>   
	                <%-- Last Changed Date --%>
	                <div class="enrolldate">
	                  <%= enrollm.getLastChangedDate() %>
	                </div>  
             <% } %>  
            </td>
            
            <%-- Enrollment Status --%>
            <td headers="col_5" class="checkbox">
                <div>
                    <input type="checkbox" name="isEnrolled[<%= i %>]" id="isEnrolled[<%= i %>]"
                           onclick="javascript:enableSubmit()" 
                        <% if (enrollm.isEnrolled()) { %>
                           checked="checked" 
                        <% } %>
                        <% if (ui.isDisplayOnly()) { %>
                           disabled="disabled" 
                        <% } %>
                    >
                </div>
            </td>
          </tr>
        </isa:iterate>
      </tbody>
    </table>
    
   <% if (ui.isAccessible()) {%>
       <a id="enrollmTableEnd" href="#enrollmTableBegin" title="<isa:translate key="access.table.end" arg0="<%= tableTitle %>"/>"></a>
   <% } %>
   
   <%-- Enrollment Status --%>
   <%@ include file="pageLinksEnrollm.inc.jsp" %>

</div> <%-- document-items --%>
<%-- } --%>