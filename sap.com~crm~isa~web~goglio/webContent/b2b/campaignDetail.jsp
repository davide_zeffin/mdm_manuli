<%--
********************************************************************************
    File:         campaignDetail.inc.jsp
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      27.11.2006
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/11/27 $
********************************************************************************
--%>
<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib prefix="isacore" uri="/isacore" %>

<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.CampaignDetailUI" %>
<%@ page import="com.sap.isa.core.util.Message" %>

<% CampaignDetailUI ui = new CampaignDetailUI(pageContext); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>
        <isa:translate key="ecom.camp.enrollment.title"/>
    </title>
    
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/campaign_enrollment.js" ) %>"
        type="text/javascript">
    </script>

   <isa:stylesheets/>
    
  </head>
  
  <body class="campaign">

	<div class="module-name"><isa:moduleName name="b2b/campaignDetail.jsp" /></div>
    <h1>
      <span><isa:translate key="ecom.camp.enrollment.title"/>
	        <%= ui.getCampaignId() %> 
	  </span>
	</h1>
    <div id="document">
      <form name="enrollmentForm" method="post" action='<isa:webappsURL name="/b2b/updatecampaign.do"/>'>
        <div class="document-header">

		<% if (ui.isAccessible) { %>
		  <a id="campaign-header-begin" href="#campaign-header-end" title="<isa:translate key="ecom.acc.sec.start.campaignHeader"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
		<% } %>
		
		<%-- Campaign Header Data --%>
		<input type="hidden" name="forward" value="displaycampaign"/>
		<input type="hidden" name="campaignpage" value="<%= ui.getCurrPage() %>"/>
		
		<table class="layout" DataTable="0" width="100%">
		  <tbody>
		    <tr>
		      <td class="col1" width="22%">
		        <%-- Campaign Id --%>
		        <div class="identifier">
		          <p><span><isa:translate key="ecom.camp.campType"/></span></p>
		        </div>    
		      </td>
		      <td class="col2" width="22%">
		        <div class="value">
		          <p><span><%= ui.getCampTypeDescr() %></span></p>
		        </div>    
		      </td>
              <td class="col3" width="12%">
                &nbsp;
              </td>
              <td class="col4" width="22%">
                &nbsp;
              </td>
              <td class="col5" width="22%">
                &nbsp;
              </td>
		    </tr>
		    <tr>
		      <td class="col1">
		        <%-- Campaign Planned Start Date --%>
		        <div class="identifier">
		          <p><span><isa:translate key="ecom.camp.campPlannedStartDate"/></span></p>
		        </div>    
		      </td>
		      <td class="col2">
		        <div class="value">
		          <p><span><%= ui.getCampPlannedStartDate() %></span></p>
		        </div>    
		      </td>
              <td class="col3">
                &nbsp;
              </td>
              <td class="col4">
		        <%-- Campaign Enrollment Start Date --%>
		        <div class="identifier">
		          <p><span><isa:translate key="ecom.camp.campEnrollmStartDate"/></span></p>
		        </div>    
		      </td>
              <td class="col5">
		        <div class="value">
		          <p><span><%= ui.getCampEnrollmStartDate() %></span></p>
		        </div>    
		      </td>
		    </tr>
		    <tr>
		      <td class="col1">
		        <%-- Campaign Planned End Date --%>
		        <div class="identifier">
		          <p><span><isa:translate key="ecom.camp.campPlannedEndDate"/></span></p>
		        </div>    
		      </td>
		      <td class="col2">
		        <div class="value">
		          <p><span><%= ui.getCampPlannedEndDate() %></span></p>
		        </div>    
		      </td>
              <td class="col3">
                &nbsp;
              </td>
              <td class="col4">
		        <%-- Campaign Enrollment End Date --%>
		        <div class="identifier">
		          <p><span><isa:translate key="ecom.camp.campEnrollmEndDate"/></span></p>
		        </div>    
		      </td>
              <td class="col5">
		        <div class="value">
		          <p><span><%= ui.getCampEnrollmEndDate() %></span></p>
		        </div>    
		      </td>
		    </tr>
		    <%-- Campaign Short Description --%>
		    <tr>
		      <td colspan="5">
		        <div class="identifier">
		          <h1 class="group"><span><%= ui.getCampShortDescr() %></span></h1>
		        </div>    
		      </td>
		    </tr>
		    <%-- Campaign Long Description --%>
		    <% if (!ui.getCampLongDescr().equals("")) { %>
		      <tr>
		        <td colspan="5">
		          <div class="identifier-multiline">
		            <p><span><%= ui.getCampLongDescr() %></span></p>
		          </div>    
		        </td>
		      </tr>
		    <% } %>
		  </tbody>
		</table>
		
        <%-- Message list --%>
        <% MessageList msgList = ui.getCampMessageList();
           if (msgList != null && !msgList.isEmpty()) { %>
	           <isa:message id="messagetext" name="<%= ui.MESSAGE_LIST %>" ignoreNull="true" type="<%=Message.ERROR%>">
	             <div class="error">
	               <span><%=JspUtil.encodeHtml(messagetext)%></span>
	             </div>
	           </isa:message>
	           <isa:message id="messagetext" name="<%= ui.MESSAGE_LIST %>" ignoreNull="true" type="<%=Message.WARNING%>">
	             <div class="warn">
	               <span><%=JspUtil.encodeHtml(messagetext)%></span>
	             </div>
	           </isa:message>
	           <isa:message id="messagetext" name="<%= ui.MESSAGE_LIST %>" ignoreNull="true" type="<%=Message.INFO%>">
	             <div class="info">
	               <span><%=JspUtil.encodeHtml(messagetext)%></span>
	             </div>
	           </isa:message>
	           <isa:message id="messagetext" name="<%= ui.MESSAGE_LIST %>" ignoreNull="true" type="<%=Message.SUCCESS%>">
	             <div class="info">
	               <span><%=JspUtil.encodeHtml(messagetext)%></span>
	             </div>
	           </isa:message>
	     <% } %>
		<% if (ui.isAccessible) { %>
		     <a id="campaign-header-end" href="#campaign-header-begin" title="<isa:translate key="ecom.acc.sec.end.campaignHeader"/>"></a>
		<% } %>
	    </div> <%-- document=header --%>	
	    
        <%-- Campaign Enrollment data --%>
      <% if (ui.isEnrollmentAvailable()) { %>
         <% if (ui.isAccessible) { %>
		       <a id="enrollment-begin" href="#enrollment-end" title="<isa:translate key="ecom.acc.sec.start.campaignEnrollment"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
		 <% } %>
		
		  <%-- only one enrollee detected --%>
		  <% if (ui.isSingleEnrollment()) { %>
               <%@ include file="campaignEnrollmentSingle.inc.jsp" %>
		  <% } 
		     else { %>
               <%@ include file="campaignEnrollmentMulti.inc.jsp" %>
		  <% } %>
		
		  <% if (ui.isAccessible) { %>
		       <a id="enrollment-end" href="#enrollment-begin" title="<isa:translate key="ecom.acc.sec.end.campaignEnrollment"/>"></a>
		  <% } %>
      <% } %>
      
         <%-- Button list --%>
         <div id="buttons">
		   <ul class="buttons-1">
		      <li>
                <%-- Button Close --%>
		        <a href='<isa:webappsURL name="/b2b/closecampaign.do?deleteMsgs=true"/>' 
		           name="<isa:translate key="ecom.camp.button.close"/>"
		        ><isa:translate key="ecom.camp.button.close"/></a>
		      </li>
		   </ul>
<% if (ui.isEnrollmentButtonVisible()) { %>
		   <ul class="buttons-3">
                <%-- Button Enroll / Delete Enrollment of a business partner --%>
    <% if (ui.isSingleEnrollment()) { %>
             <li>
               <a href="#" onClick="enrollment_submit()" 
        <% if (ui.isBpEnrolled()) { %>
		          title="<isa:translate key="ecom.camp.button.delEnroll"/>"   
		       ><isa:translate key="ecom.camp.button.delEnroll"/></a>
        <% } 
           else { %>
		          title="<isa:translate key="ecom.camp.button.enroll"/>"   
		       ><isa:translate key="ecom.camp.button.enroll"/></a>
        <% } 
       } 
       else { %>
           <%-- SelectAllBPs / DeselectAllBPs button --%> 
           <%-- !!! This functionality is currently disabled; the resources are not created !!! --%>
        <% if (!ui.isDisplayOnly() && false) { %> 
             <li>
               <a id="selectAllButton" name="selectAllButton" 
           <% if (ui.areAllBpEnrolled()) { %> 
                  href="javascript:setEnrollmentFlagForAllBPs('false')" 
                  title="<isa:translate key="ecom.camp.button.deselectAllBPs"/>"   
               ><isa:translate key="ecom.camp.button.deselectAllBPs"/></a>
           <% } 
              else { %>
                  href="javascript:setEnrollmentFlagForAllBPs('true')" 
                  title="<isa:translate key="ecom.camp.button.selectAllBPs"/>"   
               ><isa:translate key="ecom.camp.button.selectAllBPs"/></a>
           <% } %>
		     </li>
        <% } %>
		     
             <%-- Submit button --%> 
             <li id="enrollmButtonStyle" 
        <% if (ui.isEnrollmentButtonDisabled()) { %> 
                 class="disabled"
        <% } %>
             >
               <a id="enrollButton" name="enrollButton" 
        <% if (ui.isEnrollmentButtonDisabled()) { %> 
                  href="#" 
        <% } 
           else { %>
                  href="javascript:enrollment_submit()" 
        <% } %>
                  title="<isa:translate key="ecom.camp.button.submit"/>"   
               ><isa:translate key="ecom.camp.button.submit"/></a>
    <% } %>
		     </li>
		   </ul>
<% } %>
		 </div>
      </form>
    </div> <%-- document --%>
  </body>
</html>