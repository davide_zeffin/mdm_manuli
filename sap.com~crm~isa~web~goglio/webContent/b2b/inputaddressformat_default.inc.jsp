<% if ( isPerson) {%>
    <!-- first name -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.fstName"/>
      </td>
      <td>
        <input class="textInput" type="Text" size="30" name="firstName" 
               value="<%= JspUtil.removeNull(addressFormular.getAddress().getFirstName())%>" >
      </td>
    </tr>
    <!-- last name -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.lstName"/>
      </td>
      <td>
        <input class="textInput" type="Text" size="30" name="lastName" 
               value="<%= JspUtil.removeNull(addressFormular.getAddress().getLastName())%>" >
      </td>
    </tr>
<% } else { %>
    <!-- name1 -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.company"/>
      </td>
      <td>
        <input class="textInput" type="Text" size="30" name="name1" 
                 value="<%= JspUtil.removeNull(addressFormular.getAddress().getName1())%>" >
      </td>
    </tr>
    <!-- name2 -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.conPers"/>
      </td>
      <td>
        <input class="textInput" type="Text" size="30" name="name2" 
                 value="<%= JspUtil.removeNull(addressFormular.getAddress().getName2())%>"  >
      </td>
    </tr>
<% } %>
    <!-- street and house number-->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.strHouse"/>
      </td>
      <td>
        <input class="textInput" type="Text" size="30" name="street" 
               value="<%= JspUtil.removeNull(addressFormular.getAddress().getStreet())%>">
        <input class="textInput" type="Text" size="10" name="houseNumber" 
               value="<%= JspUtil.removeNull(addressFormular.getAddress().getHouseNo())%>" >
      </td>
    </tr>
    <!-- postal code and city -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.zipCity"/>
      </td>
      <td>
        <input class="textInput" type="Text" size="10" name="postalCode" 
               value="<%= JspUtil.removeNull(addressFormular.getAddress().getPostlCod1())%>" >
        <input class="textInput" type="Text" size="30" name="city" 
               value="<%= JspUtil.removeNull(addressFormular.getAddress().getCity())%>" >
      </td>
    </tr>
    <!-- country -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.country"/>
      </td>
      <td>
        <% 
           // The result of the following decision will be passed as a parameter of the 'send_countryChange'
           // JavaScript function which is defined within the file 'addressdetails_maintenance.js'.
           String fromCountry = "F";
           if (listOfRegions != null) {
             fromCountry = "T";
           } %>
        <select name="country" onChange="send_countryChange('<%= fromCountry %>')" >
        <% // build up the selection options 
           String defaultCountryId =(String) addressFormular.getAddress().getCountry(); %>
        <isa:iterate id="country" name="<%= ActionConstants.RC_COUNTRY_LIST %>"
                     type= "com.sap.isa.core.util.table.ResultData" resetCursor="true">
        <% // Should the current country be marked as selected?
             String selected="";
             if  (country.getString("ID").equals(defaultCountryId)) {
                 selected="SELECTED";
             } %>

             <OPTION VALUE="<%= country.getString("ID") %>" <%= selected %>>
               <%= country.getString("DESCRIPTION") %>
             </OPTION>
        </isa:iterate>
        </select>
      <script language="JavaScript">
        <% index = 0; %>    
        <isa:iterate id="country" name="<%= ActionConstants.RC_COUNTRY_LIST %>"
             type= "com.sap.isa.core.util.table.ResultData" resetCursor="true">
          <% // fill a JavaScript array to remember which country has regions that has to be displayed
        
             String arrayValue = "F";
             if (country.getBoolean("REGION_FLAG") == true) {
                 arrayValue = "T"; 
             } %>

          toCountry[<%= index++ %>] = "<%= arrayValue %>";
        </isa:iterate>
      </script>

      </td>
    </tr>
<% if (listOfRegions != null) { %>
    <!-- region -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.region"/>
      </td>
      <td>
        <select name="region">
            <% // build up the selection options           
               String defaultRegionId =(String) addressFormular.getAddress().getRegion(); %>
            <isa:iterate id="region" name="<%= ActionConstants.RC_REGION_LIST %>"
                         type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
              <% // Should the current region be marked as selected?
                 String selected="";
                 if  (region.getString("ID").equals(defaultRegionId)) {
                     selected="SELECTED";
                 } %>
              
                 <option value="<%= region.getString("ID")%>" <%= selected %>>
                   <%= region.getString("DESCRIPTION")%>
                 </option>
            </isa:iterate>
          </select>
      </td>
    </tr>
<% }%>
<% 
   if (listOfCounties != null && listOfCounties.getNumRows() > 0) { %>
    <!-- county -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.county"/>
      </td>
      <td>
        <select name="taxJurisdictionCode">
            <% // build up the selection options           
               String defaultTaxJurCode = (String) addressFormular.getAddress().getTaxJurCode(); %>
            <isa:iterate id="county" name="<%= ActionConstants.RC_POSSIBLE_COUNTIES %>"
                         type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
              <% // Should the current county be marked as selected?
                 String selected="";
                 if  (county.getString("TXJCD").equals(defaultTaxJurCode)) {
                     selected="SELECTED";
                 } %>
            
                 <option value="<%= county.getString("TXJCD")%>" <%= selected %>>
                   <%= county.getString("COUNTY") %><%= county.getString("OUTOF_CITY") %>
                </option>
            </isa:iterate>
          </select>
      </td>
    </tr>
<% } %>
<!-- telephone number -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.tel"/>
      </td>
      <td>
        <input class="textInput" type="Text" size="30" name="telephoneNumber" 
             value="<%= JspUtil.removeNull(addressFormular.getAddress().getTel1Numbr())%>">
      </td>
    </tr>
<!-- fax number -->
    <tr class="odd">
      <td ><isa:translate key="userSettings.adrchange.fax"/>
      </td>
      <td>
        <input class="textInput" type="Text" size="30" name="faxNumber" 
             value="<%= JspUtil.removeNull(addressFormular.getAddress().getFaxNumber())%>" >
      </td>
    </tr>
<!-- hidden field: person -->
<% if (addressFormular.isPerson()) { %>
   <input type="hidden" name="person" value="true" >
<% }
 else { 
   // the empty string will be interpreted as 'false' by the request parser %>
   <input type="hidden" name="person" value="" >
<% } %>

<!-- hidden field: regionRequired -->
<% if (addressFormular.isRegionRequired()) { %>
   <input type="hidden" name="regionRequired" value="true" >
<% }
 else { 
   // the empty string will be interpreted as 'false' by the request parser %>
   <input type="hidden" name="regionRequired" value="" >
<% } %>

<!-- hidden fields: name1/name2 or firstName/lastName -->
<% if (isPerson) { %>
     <input type="hidden" name="name1" 
            value="<%= JspUtil.removeNull(addressFormular.getAddress().getName1())%>" >
     <input type="hidden" name="name2" 
            value="<%= JspUtil.removeNull(addressFormular.getAddress().getName2())%>" >
<% }
 else { %>
     <input type="hidden" name="firstName" 
            value="<%= JspUtil.removeNull(addressFormular.getAddress().getFirstName())%>" >
     <input type="hidden" name="lastName" 
            value="<%= JspUtil.removeNull(addressFormular.getAddress().getLastName())%>" >
<% } %>
