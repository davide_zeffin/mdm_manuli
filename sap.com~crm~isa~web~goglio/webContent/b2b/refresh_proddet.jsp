<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
  <head>
    <title>SAP Internet Sales B2B</title>
	
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
            type="text/javascript">
    </script>

  	<script type="text/javascript">
	<!--
	    function resize(direction, frameObj) {
	      if (direction == "min") {
	        if (frameObj == "history")
	          window.fourthFS.cols = '*,15,0';
	      }
	      else {
	        if (frameObj == "history")
	          window.fourthFS.cols = '*,15,95';
	      }
	    }
	    <%-- since minibasket is implemented "documents"-Frame has to be refreshed always 
	         when "positions"-Frame is refreshed --%>
	    documents().location.href = '<isa:webappsURL name="/b2b/updateworkareanav.do"/>' ;    
	    form_input().location.href = '<isa:webappsURL name="/b2b/fs_proddet.jsp"/>';
	//-->    
  	</script>

  </head>
  <body></body>
</html>
