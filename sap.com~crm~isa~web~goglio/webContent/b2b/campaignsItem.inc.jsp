<%--
********************************************************************************
    File:         campaignsItem.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.03.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/03/18 $
********************************************************************************
--%>
      <% if (ui.showCurrentItemCampaignData() && ui.isElementVisible("order.item.campaign", itemKey) && ui.isElementEnabled("order.item.campaign")) { %>
			 <tr>
				 <td class="identifier">
				  <% if (ui.isManualCampaignEntryAllowed() || ui.getDisplayNoItemCampaignEntries() > 0) { %>
                     <label for="campId[<%= ui.line %>][0]"><isa:translate key="b2b.order.disp.camp"/></label>
                  <% } %>
				 </td>
				 <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
				 <td class="campimg-1">
				     <a class="icon" href="javascript:toggleCampaignRows('itemcampicon<%= ui.line %>', 'itemcamp<%= ui.line %>_', '<%= ui.getDisplayNoItemCampaignEntries() %>')" ><img id="itemcampicon<%= ui.line %>" src=<%=WebUtil.getMimeURL(pageContext,"mimes/images/open.gif")%> alt="<isa:translate key="camp.toggle.exp"/>"  /></a>
			     </td>
			     <% } %>   
				 <td class="campvalue">
					  <% if (ui.isManualCampaignEntryAllowed() && ui.isItemMainItem() && ui.isElementEnabled("order.item.campaign", itemKey)) { %>
							<input type="text" class="textinput-large" maxlength="24" name="campId[<%= ui.line %>][0]" id="campId[<%= ui.line %>][0]"
									   value="<%= JspUtil.encodeHtml(ui.getItemCampaignId(0)) %>"/>		   
					  <% } else { %>
						 	<input type="hidden" class="textinput-large" name="campId[<%= ui.line %>][0]" id="campId[<%= ui.line %>][0]"
									   value="<%= JspUtil.encodeHtml(ui.getItemCampaignId(0)) %>"/>
							 <%= JspUtil.encodeHtml(ui.getItemCampaignId(0)) %>
				      <% } %>
				      <% if (ui.isItemCampaignManuEntered(0)) { %>
					      <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_manCampaignEntry.gif") %>" alt="<isa:translate key="b2b.disp.camp.man"/>"/>
					  <% } %>
				 </td>
                 <td class="campdesc">
                 <% if (ui.isItemCampaignValid(0) && ui.getItemCampaignGuid(0).length() > 0) { %>
                      <%= JspUtil.encodeHtml(ui.getItemCampaignDescs(0)) %>
                 <% } %> 
                 </td>
	             <td class="campdelete">
                 <% if (ui.getItemCampaignId(0) != null && ui.getItemCampaignId(0).length() > 0) { %> 
                      <input type="checkbox" name="delCampId[<%= ui.line %>][0]"/><img src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/trash_icon.gif")%>" alt="<isa:translate key="b2b.camp.delete"/>"/>
                 <% } %>  
                 </td> 
			 </tr>
			 <% if (!ui.isItemCampaignValid(0) || !ui.isItemCampaignKnown(0))  { %>
			 <tr>
				 <td class="identifier"></td>
				 <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
				 <td class="campimg-1"></td>
			     <% } %> 
			     <td colspan="2">
			      <div class="error">
                      <span>
                         <% if (!ui.isItemCampaignKnown(0)) { %>
                            <isa:translate key="camp.unknown" arg0="<%= ui.getItemCampaignId(0) %>"/>
                         <% }
                            else { %>
                            <isa:translate key="camp.invalid" arg0="<%= ui.getItemCampaignId(0) %>"/>
                         <% } %> 
                      </span>
                     </div>
			     </td>
			     <td class="campdelete">&nbsp;</td>   
			 </tr>
			 <% } %>
              <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
                  <% for (int iItemCamp = 1; iItemCamp < ui.getDisplayNoItemCampaignEntries(); iItemCamp++) { %>
                      <tr id="itemcamp<%= ui.line %>_<%= iItemCamp %>" style="display: none">
                          <td class="identifier"></td>
                        <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
				          <td class="campimg-1"></td>
			            <% } %>  
                          <td class="campvalue">
                               <% if (ui.isManualCampaignEntryAllowed() && ui.isItemMainItem() && ui.isElementEnabled("order.item.campaign", itemKey)) { %>
                                     <input type="text" class="textinput-large" maxlength="24" name="campId[<%= ui.line %>][<%= iItemCamp %>]" id="campId[<%= ui.line %>][<%= iItemCamp %>]"
                                                value="<%= JspUtil.encodeHtml(ui.getItemCampaignId(iItemCamp)) %>"/>		   
                               <% } else { %>
                                     <input type="hidden" class="textinput-large" name="campId[<%= ui.line %>][<%= iItemCamp %>]" id="campId[<%= ui.line %>][<%= iItemCamp %>]"
                                                value="<%= JspUtil.encodeHtml(ui.getItemCampaignId(iItemCamp)) %>"/>
                                      <%= JspUtil.encodeHtml(ui.getItemCampaignId(iItemCamp)) %>
                               <% } %>
                               <% if (ui.isItemCampaignManuEntered(iItemCamp)) { %>
                                   <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_manCampaignEntry.gif") %>" alt="<isa:translate key="b2b.disp.camp.man"/>"/>
                               <% } %>
                          </td>
                          <td class="campdesc">
                               <% if (ui.isItemCampaignValid(iItemCamp) && ui.getItemCampaignGuid(iItemCamp).length() > 0) { %>
                                    <%= JspUtil.encodeHtml(ui.getItemCampaignDescs(iItemCamp)) %>
                               <% } %> 
                          </td>
                          <td class="campdelete">
                          <% if (ui.getItemCampaignId(iItemCamp) != null && ui.getItemCampaignId(iItemCamp).length() > 0) { %> 
                              <input type="checkbox" name="delCampId[<%= ui.line %>][<%= iItemCamp %>]"/><img src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/trash_icon.gif")%>" alt="<isa:translate key="b2b.camp.delete"/>"/>
                          <% } %>  
                          </td>  
                      </tr>
                    <% if (!ui.isItemCampaignValid(iItemCamp)  || !ui.isItemCampaignKnown(iItemCamp)) { %>
			          <tr id="itemcamp<%= ui.line %>_<%= iItemCamp %>_err" style="display: none">
				          <td class="identifier"></td>
				          <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
				          <td class="campimg-1"></td>
			              <% } %> 
			              <td colspan="2">
			                 <div class="error">
                               <span>
                               <% if (!ui.isItemCampaignKnown(iItemCamp)) { %>
                                    <isa:translate key="camp.unknown" arg0="<%= ui.getItemCampaignId(iItemCamp) %>"/>
                               <% }
                                  else { %>
                                 <isa:translate key="camp.invalid" arg0="<%= ui.getItemCampaignId(iItemCamp) %>"/>
                               <% } %> 
                               </span>
                             </div>
			              </td> 
			              <td class="campdelete">&nbsp;</td>   
			          </tr>
			        <% } %>
                  <% } %>
                  <% if (ui.isManualCampaignEntryAllowed() && ui.isElementEnabled("order.item.campaign", itemKey)) { %>
                       <tr id="itemcamp<%= ui.line %>_<%= ui.getDisplayNoItemCampaignEntries() %>" style="display: none">
                       	  <td class="identifier"></td>
                       	<% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
				          <td class="campimg-1"></td>
			            <% } %>  
                          <td class="campvalue" colspan="<%= ui.getNonCampItemDetailColspan() + 1 %>">
                              <a href="#" onclick="submit_refresh();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.lnk.morecamps"/>" <% } %> ><isa:translate key="b2b.lnk.morecamps"/></a>
                          </td>
                       </tr>
                  <% } %>
              <% } %>
      <% } %>
