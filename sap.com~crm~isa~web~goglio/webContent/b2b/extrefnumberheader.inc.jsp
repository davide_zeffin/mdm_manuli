<%--
********************************************************************************
    File:         extrefnumberheader.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      04.04.2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/04/04 $
********************************************************************************
--%>

<% if (ui.isElementVisible("order.extRefNumbers") && ui.header.getAssignedExternalReferences() != null && ui.header.getAssignedExternalReferences().size() > 0) {
	  ExternalReference externalReference;
	  for (int i = 0; i < ui.header.getAssignedExternalReferences().size(); i++) {
		externalReference = (ExternalReference)ui.header.getAssignedExternalReferences().getExtRef(i);
%>		
        <tr>
           <td class="identifier">
              <label for="extRefNumberType"><%= externalReference.getDescription() %>:</label>
           </td>
           <td class="value"> 
              <% if (ui.isElementEnabled("order.extRefNumbers")) { %>
                  <input name="extReferenceNumber[<%= i %>]" id="extRefNum" maxlength="35" class="textinput-large" type="text" value="<%= JspUtil.encodeHtml(externalReference.getData()) %>"/>&nbsp;   
              <% } else { %>
                  <%= JspUtil.encodeHtml(externalReference.getData()) %>
              <% } %>
              <input type="hidden" name="extReferenceType[<%= i %>]" value="<%= JspUtil.encodeHtml(externalReference.getRefType()) %>"/>
              <input type="hidden" name="extReferenceTechKey[<%= i %>]" value="<%= externalReference.getTechKey() %>"/>
           </td>   
        </tr>
<%	  }
   } 
%>