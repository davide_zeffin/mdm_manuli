<%--
********************************************************************************
    File:         campaignsHeader.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.03.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/03/18 $
********************************************************************************
--%>
    <% if (ui.showCampaignData() && ui.isElementEnabled("order.campaign")) { %>
      <tr>
        <td class="identifier">
             <label for="headCampId"><isa:translate key="b2b.order.disp.camp"/></label>
        </td>
        <% if (ui.showHeaderToggleMultipleCampaignData()) { %>
        <td class="campimg-1">
             <a class="icon" href="javascript:toggleCampaignRows('headercampicon', 'headercamp_', '<%= ui.getDisplayNoHeaderCampaignEntries() %>')" ><img id="headercampicon" src=<%=WebUtil.getMimeURL(pageContext,"mimes/images/open.gif")%> alt="<isa:translate key="camp.toggle.exp"/>"  /></a>
        </td>
        <% } %>
        <td class="campvalue" style="white-space: nowrap"">
            <% if (ui.isManualCampaignEntryAllowed() && ui.isElementEnabled("order.campaign")) { %>
                  <input type="text" class="textinput-large" maxlength="24" name="headCampId[0]" id="headCampId[0]" 
                   value="<%= JspUtil.encodeHtml(ui.getHeaderCampaignId(0)) %>"/>
                  <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.order.disp.camp.hint"/>"/> 
            <% } else { %>
                  <input type="hidden" class="textinput-large" name="headCampId[0]" id="headCampId[0]" 
                   value="<%= JspUtil.encodeHtml(ui.getHeaderCampaignId(0)) %>"/>
                <%= JspUtil.encodeHtml(ui.getHeaderCampaignId(0)) %>
            <% } %>
             <input type="hidden" name="invHeadCampId[0]" id="invHeadCampId[0]" value="<%= (ui.isHeaderCampaignValid(0) ? "" : "X") %>"/>
        </td>
        <td class="campdesc">
           <% if (ui.isHeaderCampaignValid(0) && ui.getHeaderCampaignGuid(0).length() > 0) { %>
               <%= JspUtil.encodeHtml(ui.getHeaderCampaignDescs(0)) %>
           <% } %>
        </td>
      </tr>
      <% if (!ui.isHeaderCampaignValid(0) || !ui.isHeaderCampaignKnown(0)) { %>
      <tr>
        <td class="identifier"></td>
        <% if (ui.showHeaderToggleMultipleCampaignData()) { %>
        <td class="campimg-1"></td>
        <% } %> 
        <td colspan="2">
           <div class="error">
             <span> 
            <% if (!ui.isHeaderCampaignKnown(0)) { %>
               <isa:translate key="camp.unknown" arg0="<%= ui.getHeaderCampaignId(0) %>"/>
            <% }
               else { %>
               <isa:translate key="camp.invalid" arg0="<%= ui.getHeaderCampaignId(0) %>"/>
            <% } %> 
             </span>
           </div>
        </td>  
      </tr>
      <% } %>
      <% if (ui.showHeaderToggleMultipleCampaignData()) { %>
          <% for (int iHeadCamp = 1; iHeadCamp < ui.getDisplayNoHeaderCampaignEntries(); iHeadCamp++) { %>
              <tr id="headercamp_<%= iHeadCamp %>" style="display: none">
                  <td class="identifier"></td>
                  <% if (ui.showHeaderToggleMultipleCampaignData()) { %>
                  <td class="campimg-1"></td>
                  <% } %>
                  <td class="campvalue" style="white-space: nowrap">
                      <% if (ui.isManualCampaignEntryAllowed() && ui.isElementEnabled("order.campaign")) { %>
                            <input type="text" class="textinput-large" maxlength="24" name="headCampId[<%= iHeadCamp %>]" id="headCampId[<%= iHeadCamp %>]" 
                             value="<%= JspUtil.encodeHtml(ui.getHeaderCampaignId(iHeadCamp)) %>"/> 
                            <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.order.disp.camp.hint"/>"/> 
                      <% } else { %>
                            <input type="hidden" class="textinput-large" name="headCampId[<%= iHeadCamp %>]" id="headCampId[<%= iHeadCamp %>]" 
                             value="<%= JspUtil.encodeHtml(ui.getHeaderCampaignId(iHeadCamp)) %>"/>
                          <%= JspUtil.encodeHtml(ui.getHeaderCampaignId(iHeadCamp)) %>
                      <% } %>
                         <input type="hidden" name="invHeadCampId[<%= iHeadCamp %>]" id="invHeadCampId[<%= iHeadCamp %>]" value="<%= (ui.isHeaderCampaignValid(iHeadCamp)? "" : "X") %>"/>
                  </td>
                  <td class="campdesc">
                  <% if (ui.isHeaderCampaignValid(iHeadCamp) && ui.getHeaderCampaignGuid(iHeadCamp).length() > 0) { %>
                      <%= JspUtil.encodeHtml(ui.getHeaderCampaignDescs(iHeadCamp)) %>
                  <% } %>
                  </td>
              </tr>
            <% if (!ui.isHeaderCampaignValid(iHeadCamp) || !ui.isHeaderCampaignKnown(iHeadCamp)) { %>
              <tr id="headercamp_<%= iHeadCamp %>_err" style="display: none">
                  <td class="identifier"></td>
                  <% if (ui.showHeaderToggleMultipleCampaignData()) { %>
                  <td class="campimg-1"></td>
                  <% } %> 
                  <td colspan="2">
                      <div class="error">
                         <span>
                            <% if (!ui.isHeaderCampaignKnown(iHeadCamp)) { %>
                               <isa:translate key="camp.unknown" arg0="<%= ui.getHeaderCampaignId(iHeadCamp) %>"/>
                            <% }
                               else { %>
                               <isa:translate key="camp.invalid" arg0="<%= ui.getHeaderCampaignId(iHeadCamp) %>"/>
                            <% } %> 
                         </span>
                      </div>
                  </td>  
              </tr>
            <% } %>
          <% } %>
            <% if (ui.isManualCampaignEntryAllowed() && ui.isElementEnabled("order.campaign")) { %>
              <tr id="headercamp_<%= ui.getDisplayNoHeaderCampaignEntries() %>" style="display: none">
                 <td class="identifier"></td>
                 <% if (ui.showHeaderToggleMultipleCampaignData()) { %>
                 <td class="campimg-1"></td>
                 <% } %>
                 <td class="campvalue" style="white-space: nowrap"">
                       <a href="#" onclick="submit_refresh();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.lnk.morecamps"/>" <% } %> ><isa:translate key="b2b.lnk.morecamps"/></a>
                 </td>
              </tr>
            <% } %>
      <% } %>
   <% } %>