    <%--
********************************************************************************
    File:         extrefobjectitem.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      25.01.2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/01/25 $
********************************************************************************
--%>
    
   <% if (item.getExtRefObjectType() != null &&  item.getExtRefObjectType().length() > 0 && ui.isElementVisible("order.item.extRefObjects", itemKey)) { %>
       <tr>
          <td class="identifier">
              <label for="vinNum_<%= ui.line %>"><%= ui.header.getExtRefObjectTypeDesc() %>:</label>
          </td>
          <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
          <td class="campimg-1"></td>
          <% } %> 
          <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
              <% ExtRefObjectList itemExtRefObjects = item.getAssignedExtRefObjects();
           	     String itemOldVins = "";
                if (ui.isElementEnabled("order.item.extRefObjects", itemKey)) { %>
                   <input name="vinNum_<%= ui.line %>" id="vinNum_<%= ui.line %>" class="textinput-large" type="text" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.disp.diff.extrefobj"/><isa:translate key="b2b.acc.more.entries.tab.ret"/>" <% } %>
                      value="<%= JspUtil.encodeHtml(ui.getExtRefObjValue(itemExtRefObjects,0)) %>"/>
                <% } else { %>
                   <%= JspUtil.encodeHtml(ui.getExtRefObjValue(itemExtRefObjects,0)) %>
                <% } %>
	            <% for (int k = 0; k < itemExtRefObjects.size(); k++) { %>
		           <input type="hidden" name="itemVinKeys[<%= ui.line %>][<%= k %>]" value="<%= JspUtil.removeNull(itemExtRefObjects.getExtRefObject(k).getTechKey().getIdAsString()) %>"/>
	            <% } %>                 
	            <% itemOldVins = ui.getExtRefObjListAsString(itemExtRefObjects);%>
                <input type="hidden" id="addVinNum_<%= ui.line %>" name="addVinNum_<%= ui.line %>" value="<%= JspUtil.encodeHtml(itemOldVins) %>"/>
                <% if (ui.isBasket()) { %>
                   <a href="#" onclick="getVinPopupItem('<%= item.getTechKey() %>','<%= ui.line %>'); return false" title="<isa:translate key="b2b.order.add.extrefobj.desc"/>">
                <% } else { %>
                   <a href="#" onclick="getVinPopupItemOC('<%= item.getTechKey() %>','<%= ui.line %>'); return false" title="<isa:translate key="b2b.order.add.extrefobj.desc"/>">
                <% } %>
                <isa:translate key="b2b.order.add.extrefobj"/>                    
                </a>
             </td>
         </tr>
   <% } %>                 