<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<isa:stylesheets/>
		<script type="text/javascript">
		<!--
		  var img0   = "<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/left_arrow.gif") %>";
		  var img0_h = "<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/left_arrow_h.gif") %>";
		  var img1   = "<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/right_arrow.gif") %>";
		  var img1_h = "<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/right_arrow_h.gif") %>";
	  	//-->
		</script>	
		
		<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/closer.js" ) %>" type="text/javascript" language="JavaScript"></script>
		
	</head>
	<body class="closer" onload="chgArrow('left', '1');">

		<%-- Accesskey for the organizer section. It must be an resourcekey, so that it can be translated. --%>
		<a id="access-organizer-closer" href="#access-items" title="<isa:translate key='b2b.closer.organizer.access'/>" accesskey="<isa:translate key='b2b.closer.accesskey.key'/>"></a>
	
		<div id="leftArrow1">
            <%-- href and img must be in one row to avoid "framing" effects on mouse over !! --%>
	        <a href="#" onmouseover="overArrow('left', 'arrow_left1');" onmouseout="outArrow('left', 'arrow_left1');" onclick="parent.resize('close', 'organizer'); chgArrow('right', '1'); return false;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/left_arrow.gif") %>" width="7" height="13" border="0" alt="<isa:translate key='b2b.navigator.close'/>" name="arrow_left1" /></a>
	    </div>
		<div id="leftArrow2" style="display: none">
	        <a href="#" onmouseover="overArrow('left', 'arrow_left2');" onmouseout="outArrow('left', 'arrow_left2');" onclick="parent.resize('min', 'organizer'); chgArrow('right', '2'); return false;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/left_arrow.gif") %>" width="7" height="13" border="0" alt="<isa:translate key='b2b.navigator.col'/>" name="arrow_left2" /></a>
	    </div>
		<div id="rightArrow1">
	        <a href="#" onmouseover="overArrow('right', 'arrow_right1');" onmouseout="outArrow('right', 'arrow_right1');" onclick="parent.resize('open', 'organizer'); chgArrow('left', '1'); return false;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/right_arrow.gif") %>" width="7" height="13" border="0" alt="<isa:translate key='b2b.navigator.show'/>" name="arrow_right1" /></a>
	    </div>
		<div id="rightArrow2" style="display: none">
	        <a href="#" onmouseover="overArrow('right', 'arrow_right2');" onmouseout="outArrow('right', 'arrow_right2');" onclick="parent.resize('max', 'organizer'); chgArrow('left', '2'); return false;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/right_arrow.gif") %>" width="7" height="13" border="0" alt="<isa:translate key='b2b.navigator.exp'/>" name="arrow_right2" /></a>
	    </div>
	</body>
</html>