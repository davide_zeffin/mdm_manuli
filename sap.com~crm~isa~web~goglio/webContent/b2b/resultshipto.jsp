<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- resultshipto.jsp --%>
<%-- This JSP displays selected shiptos for the soldto,
       to select one and gives the result to the SearchShiptoAction.
       
--%>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.*" %>

<%-- import the requiered tag libraries --%>
<%@ taglib uri="/isa" prefix="isa" %>

<% String[] evenOdd = new String[] { "even", "odd" }; %>

<isa:contentType/>
<html>
<head><title><isa:translate key="resultshipto.title"/></title>
  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">
</head>
<body class="shipto">

<div id="resultshipto" class="module">
<div class="module-name"><isa:moduleName name="b2b/resultshipto.jsp" /></div>
<form method="post" action='<isa:webappsURL name="/b2b/readshipto.do" />'  >
<br><br><br><br>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr class="headLink">
    <td colspan="3"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="8" alt="" border="0"></td>
    <!-- <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd.gif") %>" width="1" height="8" alt="" border="0"></td> -->
  </tr>
  <tr class="bodylight">
    <td colspan="3"><br><br></td>
  </tr>
  <tr class="bodylight">
    <td class="opener">&nbsp;</td>
    <td class="opener"><isa:translate key="resultshipto.yourchoice"/></td>
    <td class="opener">&nbsp;</td>
    <!-- <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd.gif") %>" width="9" height="21" alt="" border="0"></td> -->
  </tr>
  <tr class="bodylight">
    <td colspan="2"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="10" alt="" border="0"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="bodylight">&nbsp;</td>
    <td class="bodylight" align="left"><isa:translate key="resultshipto.selectsoldtotitle"/><br><br></td>
    <td class="bodylight" width="5%">&nbsp;</td>
  </tr>
  <tr class="bodylight">
    <td width="10%">&nbsp;</td>
    <td colspan="1">
      <table class="start-list" border="0" cellspacing="0" cellpadding="3" align="left" >
        <tr>
         <th style="border-left: 1px solid #9b9b9b;"><isa:translate key="shipto.jsp.name"/></th>
         <th><isa:translate key="shipto.jsp.street"/></th>
         <th><isa:translate key="shipto.jsp.zip"/></th>
         <th><isa:translate key="shipto.jsp.city"/></th>
        </tr>
            
        <%-- iterate over the list of shiptos and render a table showing them --%>
        <% int line = 0; %>
        <isa:iterate id="shipto" name="<%= SearchShiptoAction.SHIPTORESULT %>"
                    type="com.sap.isa.core.util.table.ResultData">
         <tr class="<%= evenOdd[++line % 2]%>">
           <td>
            <a href="<isa:webappsURL name="/b2b/readshipto.do"/><%=  ReadShiptoAction.createRequest(shipto) %> "> 
              <%= shipto.getString("NAME1") %><%= shipto.getString("LASTNAME") %>
            </a>
           </td>
           <td><%=shipto.getString("STREET")%></td>
           <td><%= shipto.getString("POSTL_COD1") %></td>           
           <td><%= shipto.getString("CITY") %></td>
         </tr>
        </isa:iterate>
      </table>
      <br><br>
    </td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr class="bodylight">
    <td colspan="3"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="21" alt="" border="0"></td>
  </tr>
 <tr>
  </tr>
</table>

<table class="shipto" border="0" cellspacing="0" cellpadding="5" width="100%">
  <tr>
      <td align="right">

          
          &nbsp;&nbsp;&nbsp;
          <input class="blue" type="submit" name="<%=ReadShiptoAction.FORWARD_CANCEL%>" value="<isa:translate key="shipto.jsp.cancel"/>">
          
          

      </td>
  </tr>
</table>
  </form>
</div>
</body>
</html>