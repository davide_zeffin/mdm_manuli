<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore"  prefix="isacore" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<title>SAP Internet Sales B2B - User settings</title>
		<isa:stylesheets/>
	</head>
	
	<body class="header-body">
		<div id="header-appl">
			<div class="module-name"><isa:moduleName name="b2b/user_settings_header.jsp" /></div>
			
			<div class="header-logo">
			</div>
			
			<div class="header-applname">
				<isa:translate key="b2b.header.settings"/>
			</div>
			<form action="">
				
			<div id="header-nav-view">
				<ul>
					<li>
						<a href="<isa:webappsURL name="b2b/showpwchange.do"/>" target="settings_main">
							<span class="active"><isa:translate key="userSettings.jsp.pwchange"/></span>
						</a>
					</li>
						<isacore:ifShopProperty property = "backend" value = "<%=ShopData.BACKEND_CRM%>" >    
    					  <li>
	    					<a href="<isa:webappsURL name="b2b/showaddresschange.do"/>" target="settings_main">
	    						<span class="active"><isa:translate key="userSettings.jsp.addresschange"/></span>
	    					</a>
	    				  </li>
	    				</isacore:ifShopProperty>
	    				<isacore:ifShopProperty property = "userProfileAvailable" value = "true" >
	    				  <li>
	    					<a href="<isa:webappsURL name="b2b/showprofile.do"/>" target="settings_main">
	    						<span class="active"><isa:translate key="userSettings.jsp.profile"/></span>
	    					</a>
	    				  </li>
			    		</isacore:ifShopProperty>
					<%--I015190 - Required for Grid materials --%>
					<li>
						<a href="<isa:webappsURL name="b2b/showusdchange.do"/>" target="settings_main">
							<span class="active"><isa:translate key="userSettings.jsp.usdchange"/></span>
						</a>
					</li> 
					<li>
						<a href="javascript:parent.window.close()">
							<span class="active"><isa:translate key="userSettings.jsp.close"/>
						</a>
					</li>
				</ul>
			</div>
			<div id="header-extradiv1">
				<span></span>
			</div>
			<div id="header-extradiv2">
				<span></span>
			</div>
			<div id="header-extradiv3">
				<span></span>
			</div>
			<div id="header-extradiv4">
				<span></span>
			</div>
			<div id="header-extradiv5">
				<span></span>
			</div>
			<div id="header-extradiv6">
				<span></span>
			</div>	
			
			
			</form>
		</div>
  </body>
</html>
