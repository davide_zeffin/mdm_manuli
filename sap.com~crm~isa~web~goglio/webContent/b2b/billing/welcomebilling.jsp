<%--
********************************************************************************
    File:         welcome.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Created:      26.7.2004

    $Revision: #1 $
    $Date: 2004/07/26 $ (Last changed)
    $Change: 197212 $ (changelist)
    
********************************************************************************
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<% BaseUI ui = new BaseUI(pageContext); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title>Workarea</title>
  <isa:stylesheets />
</head>

<body class="nobilldocument">

  <p>&nbsp;</p>
  <h1><isa:translate key="status.bill.dt.invoices"/></h1>
  <p><isa:translate key="b2b.sbill.welc.1"/></p>
  <p><isa:translate key="b2b.sbill.welc.2"/></p>
  <p><isa:translate key="b2b.sbill.welc.3"/></p>

</body>

</html>

