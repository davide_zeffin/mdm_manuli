<%--
********************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      08.5.2001

    $Revision: #3 $
    $Date: 2002/06/10 $
********************************************************************************

 IMPORTANT:
       - Page only accessible by using 'b2b/billinglistselect.do'

--%>

<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<% DocumentListSelectorForm form =
    (DocumentListSelectorForm)pageContext.findAttribute("DocumentListSelectorForm");
%>
<% String docCount = (String)request.getAttribute(DocumentListAction.RK_DOCUMENT_COUNT);
   Shop shop = (Shop)request.getAttribute(DocumentListAction.RK_DLA_SHOP);

   // Determine selected document status index
   int documentStatusSelectedIndex = 0;
   if ( form.isDocumentStatus(DocumentListSelectorForm.OPEN)) {
       documentStatusSelectedIndex = 1;
   }
   if ( form.isDocumentStatus(DocumentListSelectorForm.COMPLETED)) {
       documentStatusSelectedIndex = 2;
   }

   String shopBackend = shop.getBackend();

   boolean backendR3ButNotPI = shopBackend.equals(Shop.BACKEND_R3) ||
                       shopBackend.equals(Shop.BACKEND_R3_40) || shopBackend.equals(Shop.BACKEND_R3_45) ||
                       shopBackend.equals(Shop.BACKEND_R3_46);

   boolean backendR3 = backendR3ButNotPI || shopBackend.equals(Shop.BACKEND_R3_PI);

   // Build 'since Date' selection box dynamically
   Calendar calendar = new GregorianCalendar();
   int actYear = calendar.get(Calendar.YEAR);
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Document List</title>
<isa:includes/>
<%--
<link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css")%>" type="text/css" rel="stylesheet">
--%>
<script type="text/javascript">
    var documentStatusSelectedIndex = <%=documentStatusSelectedIndex%>;
    var IE4 = IE5 = NS4 = NS6 = false;
    var IE4 = (document.all && !document.getElementById) ? true : false;
    var IE5 = (document.all && document.getElementById) ? true : false;
    var NS4 = (document.layers) ? true : false;
    var NS6 = (document.getElementById && !document.all) ? true : false;

    function setDocumentStatusIndex() {
      documentStatusSelectedIndex = document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.selectedIndex;
    }
    function loadContract(documentType) {
        if (documentType == "<%=DocumentListSelectorForm.CONTRACT%>"  ||
            documentType == "<%=DocumentListSelectorForm.ORDER%>"     ||
            documentType == "<%=DocumentListSelectorForm.QUOTATION%>" ||
            documentType == "<%=DocumentListSelectorForm.ORDERTEMPLATE%>" ||
            documentType == "<%=DocumentListSelectorForm.AUCTION%>") {
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.style.backgroundColor = "silver";}
            <%-- document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.style.backgroundColor = "silver";} --%>
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE_VALUE %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE_VALUE %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.style.backgroundColor = "silver";}
            if (documentType == "<%=DocumentListSelectorForm.CONTRACT%>") {
                document.location.href="<isa:webappsURL name="/b2b/contractsearch.do"/>";
            } else if (documentType == "<%=DocumentListSelectorForm.AUCTION%>") {
                document.location.href="<isa:webappsURL name="/auction/buyer/auctionmonitor.do"/>";
            }  else {
                document.location.href="<isa:webappsURL name="/b2b/documentlistselect.do"/>?<%= DocumentListSelectorForm.DOCUMENT_TYPE %>=" + documentType + "&<%=DocumentListSelectorForm.ATTRIBUTE%>=<%=DocumentListSelectorForm.NOT_SPECIFIED%>&<%=DocumentListBaseAction.RP_NO_SEARCH_RESULT%>=true" ;
            }
        }
    }
    function updateAttributeValue(attribute) {
        if (attribute == "<%=DocumentListSelectorForm.NOT_SPECIFIED%>" || attribute != "<%=form.getAttribute()%>") {
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.value = "";
        }
        if (attribute == "<%=DocumentListSelectorForm.NOT_SPECIFIED%>") {
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.style.backgroundColor = "silver";}
        }
        else {
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.disabled = false;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.style.backgroundColor = "white";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.period.options[0].selected=true;
        }
    }
    function setPeriod() {
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.disabled = false;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.style.backgroundColor = "white";}
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.disabled = true;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.style.backgroundColor = "silver";}
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.disabled = true;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.style.backgroundColor = "silver";}
    }
    function setDate() {
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.disabled = true;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.style.backgroundColor = "silver";}
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.disabled = false;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.style.backgroundColor = "white";}
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.disabled = false;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.style.backgroundColor = "white";}
    }
</script>
</head>
  <body class="organizer"
        onLoad="javascript:updateAttributeValue('<%=form.getAttribute()%>');
                           <%=form.isValiditySelection(form.PERIOD) ? "javascript:setPeriod()" : "javascript:setDate()"%>;
                           loadContract('<%=form.getDocumentType()%>')">
    <div id="organizer-search" class="module">
    <div class="module-name"><isa:moduleName name="b2b/billing/organizer-content-doc-search.jsp" /></div>

      <%-- Selection Screen Area --%>

      <%-- form action="organizer-content-doc-search_contract.html" --%>
      <form name="<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>" method="get" action='<isa:webappsURL name ="b2b/documentlistselect.do"/>' >
        <!--fieldset>
          <legend>Suchkriterien</legend-->
          <table cellspacing="0" cellpadding="1" width=100%>
            <tbody>
              <tr>
                <td><isa:translate key="b2b.status.shuffler.key1"/></td>
                <td><select id="doctype" class="bigCatalogInput" name="<%= DocumentListSelectorForm.DOCUMENT_TYPE %>"
                            onChange="loadContract(this.form.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.options[this.form.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.options.selectedIndex].value )">
                    <% if (shop.isQuotationAllowed()) {%>
                    <option value="<%=DocumentListSelectorForm.QUOTATION%>" <%=form.isDocumentType(form.QUOTATION) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val1"/></option>
                    <% } %>
                    <option value="<%= DocumentListSelectorForm.ORDER %>"<%=form.isDocumentType(form.ORDER) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val2"/></option>
                    <% if (shop.isTemplateAllowed()) {%>
                    <option value="<%=DocumentListSelectorForm.ORDERTEMPLATE%>"<%=form.isDocumentType(form.ORDERTEMPLATE) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val3"/></option>
                    <% } %>
                    <% if (shop.isContractAllowed()) {%>
                    <option value="<%=DocumentListSelectorForm.CONTRACT%>">&nbsp;<isa:translate key="b2b.status.shuffler.key1val4"/></option>
                    <% } %>
                    <option value="<%=DocumentListSelectorForm.INVOICE%>"<%=form.isDocumentType(form.INVOICE) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val5"/></option>
                    <option value="<%=DocumentListSelectorForm.CREDITMEMO%>"<%=form.isDocumentType(form.CREDITMEMO) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val6"/></option>
                    <option value="<%=DocumentListSelectorForm.DOWNPAYMENT%>"<%=form.isDocumentType(form.DOWNPAYMENT) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val7"/></option>
                    <% if (!backendR3 &&
                           (userSessionData.getAttribute(IsaCoreInitAction.SC_AUCTION_ENABLED) != null) &&
                           ((Boolean)userSessionData.getAttribute(IsaCoreInitAction.SC_AUCTION_ENABLED)).booleanValue()) { %>
                        <option value="<%=DocumentListSelectorForm.AUCTION%>">&nbsp;<isa:translate key="b2b.status.shuffler.key1val9"/></option>
                    <% } %>
                  </select></td>
              </tr>
<%--          STATUS selection for BillingDocuments is not yet supported
              <tr id="status">
                <td><isa:translate key="status.select.which"/></td>
                <td><select class="bigCatalogInput" name="<%= DocumentListSelectorForm.DOCUMENT_STATUS %>"
                                 onChange="setDocumentStatusIndex();" >
                    <option value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>"<%=form.isDocumentStatus(form.NOT_SPECIFIED) ? "selected" : ""%> ><isa:translate key="status.sales.select.type.noselect"/></option>
                    <option value="<%=DocumentListSelectorForm.OPEN%>" <%=form.isDocumentStatus(form.OPEN) ? "selected" : ""%> >&nbsp;<isa:translate key="status.sales.select.type.open"/></option>
                    <option value="<%=DocumentListSelectorForm.COMPLETED%>" <%=form.isDocumentStatus(form.COMPLETED) ? "selected" : ""%> >&nbsp;<isa:translate key="status.sales.select.type.completed"/></option>
                    <option value="status_egal">&nbsp;(ohne bes. Status)</option>
                  </select></td>
              </tr>
--%>
              <tr id="name">
                <td><isa:translate key="b2b.status.shuffler.key2"/></td>
                <td><select class="bigCatalogInput" name="<%= DocumentListSelectorForm.ATTRIBUTE %>"
                            onChange="updateAttributeValue(document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>[document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>.selectedIndex].value)">
                    <option value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>" <%=form.isAttribute(form.NOT_SPECIFIED) ? "selected" : ""%> ><isa:translate key="b2b.status.shuffler.key3val1"/></option>
                    <% if (backendR3) { %>
                    <option value="<%=DocumentListSelectorForm.PURCHASEORDERNUMBER%>" <%=form.isAttribute(form.PURCHASEORDERNUMBER) ? "selected" : ""%>>&nbsp;<isa:translate key="b2b.status.shuffler.key3val2ex"/></option>
                    <% } else { %>
                    <option value="<%=DocumentListSelectorForm.REFERENCEDOC%>" <%=form.isAttribute(form.REFERENCEDOC) ? "selected" : ""%>>&nbsp;<isa:translate key="b2b.status.shuffler.key3val2"/></option>
                    <option value="<%=DocumentListSelectorForm.OBJECTID%>" <%=form.isAttribute(form.OBJECTID) ? "selected" : ""%>>&nbsp;<isa:translate key="b2b.status.shuffler.key3val4"/></option>
                    <%-- <option value="merkmal_egal">&nbsp;(ohne bes. Merkmal)</option> --%>
                    <% } %>
                  </select></td>
              </tr>
              <tr id="name">
                <td><isa:translate key="b2b.status.shuffler.key4"/></td>
                <td><input type="text" class="bigCatalogInput" value="<%= form.getAttributeValue() %>" name="<%= DocumentListSelectorForm.ATTRIBUTE_VALUE %>"></td>
              </tr>
              <tr id="name">
                <% if (backendR3ButNotPI) { %>
                <td><isa:translate key="b2b.status.shuffler.key5_exact"/></td>
                <% } else { %>
                <td><isa:translate key="b2b.status.shuffler.key5"/>
                <% } %>
              </tr>
              <tr id="zeitraum">
                <td colspan="2"><isa:translate key="b2b.status.shuffler.key6"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="radio" name="<%= DocumentListSelectorForm.VALIDITYSELECTION %>" value="<%=form.PERIOD%>"
                           onClick="javascript:setPeriod()" <%=form.isValiditySelection(form.PERIOD) ? "checked" : ""%> >&nbsp;
                    <select name="<%= DocumentListSelectorForm.PERIOD %>" class="middleCatalogInput">
                       <option value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>" <%=form.isPeriod(form.NOT_SPECIFIED) ? "selected" : ""%> ><isa:translate key="b2b.status.shuffler.key6val1"/></option>
                       <option value="<%=DocumentListSelectorForm.TODAY%>" <%=form.isPeriod(form.TODAY) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val2"/></option>
                       <option value="<%=DocumentListSelectorForm.SINCE_YESTERDAY%>" <%=form.isPeriod(form.SINCE_YESTERDAY) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val3"/></option>
                       <option value="<%=DocumentListSelectorForm.LAST_WEEK%>" <%=form.isPeriod(form.LAST_WEEK) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val4"/></option>
                       <option value="<%=DocumentListSelectorForm.LAST_MONTH%>" <%=form.isPeriod(form.LAST_MONTH) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val5"/></option>
                       <option value="<%=DocumentListSelectorForm.LAST_YEAR%>" <%=form.isPeriod(form.LAST_YEAR) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val6"/></option>
                       <%-- <option value="zeitraum_egal">&nbsp;(ohne bes. Zeitraum)</option> --%>
                    </select></td>
              </tr>
              <tr id="monat" >
                <td><isa:translate key="b2b.status.shuffler.key7"/></td>
                <td><input type="radio" name="<%= DocumentListSelectorForm.VALIDITYSELECTION %>" value="<%=form.MONTH%>"
                           onClick="javascript:setDate()" <%=form.isValiditySelection(form.MONTH) ? "checked" : ""%> >&nbsp;
                    <select name="<%= DocumentListSelectorForm.MONTH %>">
                       <option value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>"><isa:translate key="b2b.status.shuffler.key7val1"/></option>
                       <option value="01" <%=form.isMonth("01") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val2"/></option>
                       <option value="02" <%=form.isMonth("02") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val3"/></option>
                       <option value="03" <%=form.isMonth("03") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val4"/></option>
                       <option value="04" <%=form.isMonth("04") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val5"/></option>
                       <option value="05" <%=form.isMonth("05") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val6"/></option>
                       <option value="06" <%=form.isMonth("06") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val7"/></option>
                       <option value="07" <%=form.isMonth("07") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val8"/></option>
                       <option value="08" <%=form.isMonth("08") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val9"/></option>
                       <option value="09" <%=form.isMonth("09") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val10"/></option>
                       <option value="10" <%=form.isMonth("10") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val11"/></option>
                       <option value="11" <%=form.isMonth("11") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val12"/></option>
                       <option value="12" <%=form.isMonth("12") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val13"/></option>
                       <%-- <option value="00" <%=form.isMonth("00") ? "selected" : ""%> >&nbsp;<isa:translate key="status.select.nomonth"/></option> --%>
                  </select>&nbsp;
                  <select name="<%= DocumentListSelectorForm.YEAR %>" value="<%= form.getYear() %>" class="yearInput" >
                       <option value="0000" <%=form.isYear("0000") ? "selected" : ""%>></option>
                       <option value="<%=actYear+1%>" <%=form.isYear(Integer.toString(actYear+1)) ? "selected" : ""%>><%=actYear+1%></option>
                       <% for (int cnt = 0; cnt < 3; cnt++) {%>
                         <option value="<%=actYear-cnt%>" <%=form.isYear(Integer.toString(actYear-cnt)) ? "selected" : ""%>><%=actYear-cnt%></option>
                       <% } %>
                  </select>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input class="green" name="ocdsbutton" type="submit" value="<isa:translate key="status.select.searching"/>"></td>
              </tr>
            </tbody>
          </table>
        <!--/fieldset-->
        <input type="hidden" name="<%= DocumentListSelectorForm.SETBILLINGSELECTIONSTART %>" value="true">
        <input type="hidden" name="<%= DocumentListSelectorForm.SETSALESSELECTIONSTART %>" value="false">
      </form>



  <%-- Document List Area --%>

  <% if ( docCount != null ) { %>

  <% String documentTypeKey = "status.bill.dt.".concat(form.getDocumentType().toLowerCase());
     if ( ! docCount.equals("1")) {
            documentTypeKey = documentTypeKey.concat("s");          // plural
     };
     String documentNameKey = "b2b.status.dn.".concat(form.getDocumentType().toLowerCase());
  %>

  <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tbody>
      <tr>
        <td colspan="2">
          <br><br>
          <var><div class="searchresult"><isa:translate key="status.select.searchresult01"/>&nbsp;&nbsp;<%= docCount %>&nbsp;<isa:translate key="<%= documentTypeKey %>"/>&nbsp;<isa:translate key="status.select.searchresult02"/></div></var>
          <br>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <table class="organizer-list" border="0" cellspacing="0" cellpadding="2" width="100%">

          <%-- Tableheader --%>

          <thead>
             <tr>
               <th><isa:translate key="<%=documentNameKey%>"/></th>
               <th width=100%><isa:translate key="status.billing.reference.2lines"/></th>
               <th><isa:translate key="status.billing.grossvalue.2lines"/></th>
             </tr>
           </thead>


          <%-- Tablerows --%>

           <% String documentStatusKey = "";
              String even_odd = "odd";
           %>
           <isa:iterate id="billingheader" name="<%= DocumentListAction.RK_BILLING_LIST %>"
                        type="com.sap.isa.businessobject.header.HeaderBillingDocument">
           <tbody>
             <% if (even_odd.equals("odd")) {
                  even_odd = "even";
                } else {
                  even_odd = "odd";
                }
             %>
             <tr class="<%= even_odd %>">
               <td><var><a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>?techkey=<%=billingheader.getBillingDocNo()%>&object_id=<%=billingheader.getBillingDocNo()%>&objects_origin=<%= billingheader.getBillingDocumentsOrigin() %>&billing=X&company.code=<%=billingheader.getCompanyCode()%>"
                                                                                               target="form_input">
                          <%=billingheader.getBillingDocNo()%>
                        </a>
                        &nbsp;
                        <% if (billingheader.isStatusCancelled()) { %>
                        <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/BillingDocCancelled.gif")%>" alt="<isa:translate key="status.bill.cancel.doc"/>" width="8" height="8" border="0">
                        <% } %>
                        <br>
                        <isa:translate key="status.billing.list.from"/>&nbsp;&nbsp;<%= billingheader.getCreatedAt()%><br>
                        <isa:translate key="status.billing.list.dueat"/>&nbsp;<%= billingheader.getDueAt()%>
                   </var>
               </td>
               <td><%= billingheader.getSalesDocNumber() %></td>
               <% String strSpanPre = "",
                         strSpanSuf = "";
                  if (billingheader.isReversedDocument()) {
                      strSpanPre = "<span style=\"color:#f00;\">";
                      strSpanSuf = "</span>";
                  }
               %>
               <td align="right" nowrap><%=strSpanPre%><%= billingheader.getGrossValue()%>&nbsp;<%= billingheader.getCurrency() %><%=strSpanSuf%></td>
             </tr>
           </tbody>
           </isa:iterate>
          </table></td>
      </tr>
    </tbody>
  </table>

  <% } // END nothingSelectedyet %>

</body>
</html>
