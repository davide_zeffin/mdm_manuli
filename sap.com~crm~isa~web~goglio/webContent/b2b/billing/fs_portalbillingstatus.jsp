<%--
********************************************************************************
    File:         fs_portalbillingstatus.jsp
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Created:      22.5.2004

    $Revision: #9 $
    $Date: 2004/08/11 $ (Last changed)
    $Change: 199535 $ (changelist)
    
********************************************************************************
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.SessionConst" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.isacore.DocumentHandler" %>
<%@ page import="com.sap.isa.isacore.ManagedDocument" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);
   // Check for the existence of the Document handler and create it, if
   // not present
   UserSessionData userSessionData = (UserSessionData) pageContext.getSession().getAttribute(SessionConst.USER_DATA);
   // Set Application Area to "separate billing display" (see also StartApplicationAction)
   userSessionData.setAttribute(ActionConstants.SC_APPLAREA, ActionConstants.APPLAREA_SEPBILL);
   DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(ActionConstantsBase.DOCUMENT_HANDLER_BILL);

        if (documentHandler == null) {
            DocumentHandler documentHandlerShop = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            documentHandler = new DocumentHandler(documentHandlerShop.getHistory());
            userSessionData.setAttribute(ActionConstantsBase.DOCUMENT_HANDLER_BILL,
                    documentHandler);
        }
   String formInputSrc = "/b2b/billing/welcomebilling.jsp";
   ManagedDocument mDoc = documentHandler.getManagedDocumentOnTop();
   if (mDoc != null) {
       //formInputSrc = (mDoc.getHref() + "?" + mDoc.getHrefParameter() +"&externalforward=billingrefresh");
       formInputSrc = ("/b2b/billingstatusdetail.do");
   }
   // Check for crossentry
   String docKey = ((request.getParameter("doc_key") != null &&  request.getParameter("doc_key").length() > 0) ? request.getParameter("doc_key") : null);
   String docId = request.getParameter("doc_id");
   String docType = request.getParameter("doc_type");
   String docOrigin = request.getParameter("objects_origin");
   if (docKey != null) {
       formInputSrc = ("/b2b/documentstatusdetailprepare.do?techkey=" + docKey + "&object_id=" + docId + "&objects_origin=" + docOrigin + "&billing=X");
   }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title></title>
</head>
<frameset cols="25%,67%,*" id="portalbillingstatusFS">
  <frame name="organizer_content" src="<isa:webappsURL name="/appbase/genericsearch.jsp?genericsearch.name=SearchCriteria_B2B_Billing&GSdocumenthandlernoadd=yes"/>" frameborder="0" marginwidth="0" marginheight="0">
  <frame name="form_input" src="<isa:webappsURL name="<%=formInputSrc%>"/>" frameborder="0" marginwidth="0" marginheight="0">
  <frame name="_history" src="<isa:webappsURL name="/b2b/history/updatehistory.do"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
    <noframes>
        <p>&nbsp;</p>
    </noframes>
</frameset>
</html>