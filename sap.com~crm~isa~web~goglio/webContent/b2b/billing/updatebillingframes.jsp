<%--
********************************************************************************
    File:         udpatebillingframes.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Created:      24.7.2004

    $Revision: #3 $
    $Date: 2004/07/26 $ (Last changed)
    $Change: 197212 $ (changelist)
    
********************************************************************************
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.SessionConst" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase" %>
<%@ page import="com.sap.isa.isacore.DocumentHandler" %>
<%@ page import="com.sap.isa.isacore.ManagedDocument" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% BaseUI ui = new BaseUI(pageContext);
   // Refresh the frames according to the current context
   UserSessionData userSessionData = (UserSessionData) pageContext.getSession().getAttribute(SessionConst.USER_DATA);
   DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(ActionConstantsBase.DOCUMENT_HANDLER_BILL);

   String formInputSrc = "/b2b/billing/welcomebilling.jsp";
   ManagedDocument mDoc = documentHandler.getManagedDocumentOnTop();
   if (mDoc != null) {
       //formInputSrc = (mDoc.getHref() + "?" + mDoc.getHrefParameter() +"&externalforward=billingrefresh");
       formInputSrc = ("/b2b/billingstatusdetail.do");
   }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title></title>
  <script type="text/javascript">
  <!--
    function updateBillingFrames() {
      // Refresh history screen
      parent._history.location.href="<isa:webappsURL name="/b2b/history/updatehistory.do"/>";
      parent.form_input.location.href="<isa:webappsURL name="<%=formInputSrc%>"/>";
    }
  -->
  </script>
</head>
<body onload="updateBillingFrames();"></body>
</html>