<%--
********************************************************************************
    File:        batch.jsp
    Copyright (c) 2001, SAP Germany, All rights reserved.

    Author:       SAP
    Created:      19.4.2001
    Version:      1.0

    $Revision: #53 $
    $Date: 2002/07/29 $
********************************************************************************
--%>

<%@ page import="java.util.*" %>
<%@ page import="java.lang.*"%>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.core.businessobject.management.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="checksession.inc" %>
<%@ include file="usersessiondata.inc" %>

<isa:contentType />
<isacore:browserType/>

<%  BaseUI ui = new BaseUI(pageContext);

    String batchselection = "";

    if ((userSessionData.getAttribute("batchselection") != null) &&
        (((String)userSessionData.getAttribute("batchselection")).length() > 0)) {
      batchselection = (String)userSessionData.getAttribute("batchselection");
    }

    String productId = "";

    if ((userSessionData.getAttribute("batchproductid") != null) &&
        (((String)userSessionData.getAttribute("batchproductid")).length() > 0)){
      productId = (String)userSessionData.getAttribute("batchproductid");
    }

    String product = "";
    if ((userSessionData.getAttribute("batchproduct") != null) &&
        (((String)userSessionData.getAttribute("batchproduct")).length() > 0)) {
      product = (String)userSessionData.getAttribute("batchproduct");
    }

	BatchCharList batchs =
            (BatchCharList) request.getAttribute(MaintainBasketBaseAction.RC_BATCHS);

	ItemSalesDoc item =
			(ItemSalesDoc)request.getAttribute(MaintainBasketBaseAction.RC_SINGLE_ITEM);

	BatchCharVals tempElement = new BatchCharVals();

	ArrayList flag = new ArrayList();

	int batchCountTotal=0;
	int batchCountCheckBox=0;

	boolean flagAddVal=true;
	String inputs="";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<head>
<title></title>
<isa:includes/>
    <!--
    <link href="<%= WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css") %>"
          type="text/css" rel="stylesheet">
          -->  
    <script type="text/javascript">
	  <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>
    <script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/table_highlight.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "auction/jscript/calendar.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/browsersniff.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
	        type="text/javascript">
    </script>

	<script type="text/javascript">

        function toggle(idName) {
          opera = false;
          // Opera V6 cannot change style.display, but it can disguise itself
          // as nearly every browser. With the userAgent property we find out
          // its true nature.
          if (navigator.userAgent.indexOf("Opera") != -1){
            opera = true;
          }
          if (document.all && !opera) {
            // the world of the IEs
            openIcon = document.all(idName + "open");
            closeIcon = document.all(idName + "close");
            target = document.all(idName);

            if (target.style.display == "none") {
                        target.style.display = "inline";
                        openIcon.style.display = "none";
                        closeIcon.style.display = "inline";
            }
            else {
                        target.style.display = "none";
                        openIcon.style.display = "inline";
                        closeIcon.style.display = "none";
            }
          }
          else if (document.getElement && !opera) {
            // the world of W3C without Opera
            openIcon = document.getElement(idName + "open");
            closeIcon = document.getElement(idName + "close");
            target = document.getElement(idName);

            if (target.style.display == "none") {
                        target.style.display = "table-row";
                        openIcon.style.display = "none";
                        closeIcon.style.display = "inline";
            }
            else {
                        target.style.display = "none";
                        openIcon.style.display = "inline";
                        closeIcon.style.display = "none";
            }
          }
          else {
             // handle NS4.7 and Opera
            alert("ServerRequest");
          }
        }

		function submit_selection(target,source1,source2,length,seperator,hidden){
			var i=0;
			document.forms["batch_positions"].elements[target].value="";
			while(document.all(source1 +  i + "]") != null){
				if(document.forms["batch_positions"].elements[source1 +  i + "]"].checked == true){
					document.forms["batch_positions"].elements[target].value = document.forms["batch_positions"].elements[target].value + document.forms["batch_positions"].elements[source1 +  i + "]"].value + seperator;
				}
				i++;
			}
			document.all(target).value = document.all(target).value.slice(0,document.all(target).value.length-1);
		}

		function submit_selection_plus_addVal(target,source1,source2,length,seperator,hidden){
			var i=0;
			document.forms["batch_positions"].elements[target].value="";
			while(document.all(source1 +  i + "]") != null){
				if(document.forms["batch_positions"].elements[source1 +  i + "]"].checked == true){
					document.forms["batch_positions"].elements[target].value = document.forms["batch_positions"].elements[target].value + document.forms["batch_positions"].elements[source1 +  i + "]"].value + seperator;
				}
				i++;
			}
				if(document.forms["batch_positions"].elements[source2].value.length != null){
					if(document.forms["batch_positions"].elements[source2].value.length > length){
						alert("Wert darf nicht l�nger als " + length + " Zeichen sein");
						document.forms["batch_positions"].elements[source2].value="";
					}
					else{
						document.forms["batch_positions"].elements[target].value = document.forms["batch_positions"].elements[target].value + document.forms["batch_positions"].elements[source2].value + seperator;
						document.forms["batch_positions"].elements[hidden].value= "";
						document.forms["batch_positions"].elements[hidden].value = document.forms["batch_positions"].elements[source2].value;
					}
				}
			document.all(target).value = document.all(target).value.slice(0,document.all(target).value.length-1);
		}

		function submit_AddVal(target,source, length){
			document.forms["batch_positions"].elements[target].value="";
			if(document.forms["batch_positions"].elements[source].value.length > length){
				alert("<isa:translate key="b2b.order.dt.batch.allowed.val1"/>" + length + "<isa:translate key="b2b.order.dt.batch.allowed.val2"/>");
			}
			else{
				document.forms["batch_positions"].elements[target].value = document.forms["batch_positions"].elements[source].value;
			}
		}

		function submit_inputs(target,source1,source2){
			document.forms["batch_positions"].elements[target].value="";

			if(document.forms["batch_positions"].elements[source2].value != null){
				if(document.forms["batch_positions"].elements[source1].value != "" && document.forms["batch_positions"].elements[source2].value != ""){
					document.forms["batch_positions"].elements[target].value = document.forms["batch_positions"].elements[source1].value + "-" + document.forms["batch_positions"].elements[source2].value;
				}
				else{
					if(document.forms["batch_positions"].elements[source2].value == ""){
						document.forms["batch_positions"].elements[target].value = document.forms["batch_positions"].elements[source1].value;
					}
					else{
						document.forms["batch_positions"].elements[target].value = document.forms["batch_positions"].elements[source2].value;
					}
				}
			}
			else{
				document.forms["batch_positions"].elements[target].value = document.forms["batch_positions"].elements[source1].value;
			}
		}

		function CBSelect(checkbox, val, sel){
			if(document.forms["batch_positions"].elements[checkbox].checked == true){
				document.forms["batch_positions"].elements[sel].value = document.forms["batch_positions"].elements[val].value;
			}
			else{
				document.forms["batch_positions"].elements[sel].value = "";
			}
		}
	</script>

</head>
<body class="workarea">
<div style="margin-left:10px">

<form action="<isa:webappsURL name="/b2b/maintainbatch.do"/>"
      name="batch_positions"
      method="post"
      target="_parent">

    <!-- Header Data -->
    <table border=0px cellspacing=5 cellpadding=0>
    	<tr>
    		<td class="vertical-align-middle">
            	<isa:translate key="b2b.order.display.productno"/>:
            </td>
            <td>
            	<b><%= JspUtil.encodeHtml(product) %></b>
            </td>
    	</tr>
    </table>
    <!-- Batch element info start -->

   <% for(int k=0; k<item.getBatchCharListJSP().size(); k++){ %>

		<input type="hidden" name="elementNameTest[<%= batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(k).getCharName()) %>">
		<input type="hidden" name="elementTxtTest[<%= batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(k).getCharTxt()) %>">
		<input type="hidden" name="elementAddValTest[<%= batchCountTotal %>]" value="<%= item.getBatchCharListJSP().get(k).getCharAddVal() %>">
		<input type="hidden" name="elementUnitTest[<%= batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(k).getCharUnit()) %>">
		<input type="hidden" name="elementUnitTExtTest[<%= batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(k).getCharUnitTExt()) %>">
		<input type="hidden" name="elementDataTypeTest[<%= batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(k).getCharDataType()) %>">

		<input type="hidden" name="elementCountCBsTest[<%= batchCountTotal %>]" value="<%= item.getBatchCharListJSP().get(k).getCharValNum() %>">
		
		<%for(int j=0; j<item.getBatchCharListJSP().get(k).getCharValNum(); j++){%>

			<input type="hidden" name="elementCharValTest[<%= batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(k).getCharVal(j))%>">
			<input type="hidden" name="elementCharValTxtTest[<%= batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(k).getCharValTxt(j))%>">
			<input type="hidden" name="elementCharValMaxTest[<%=batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(k).getCharValMax(j))%>">
		 <% batchCountCheckBox++; 
		  } %>

		<%if( item.getBatchCharListJSP().get(k).getCharAddVal() == true || (item.getBatchCharListJSP().get(k).getCharValNum() == 0 && item.getBatchCharListJSP().get(k).getCharDataType().equals("CHAR"))){ %>
			<input type="hidden" name="characteristicAddValTest[<%= batchCountTotal %>]" value="")
		<%}

		  if(item.getBatchCharListJSP().get(k).getCharValNum() == 0 && item.getBatchCharListJSP().get(k).getCharDataType().equals("NUM")){
		    String grpTxt = WebUtil.translate(pageContext, "b2b.batch.batch.info", new String[] { String.valueOf(JspUtil.encodeHtml(product)) }); 
			if(ui.isAccessible() && k==0) { %>
			    <a href="#group-end1" id= group-begin1" title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>">
				</a>
		 <% } %>
		 
			<input type="text" class="textInput" name="characteristicFromTest[<%= batchCountTotal %>]" value="" title="<%=grpTxt%>">
			<input type="text" class="textInput" name="characteristicToTest[<%= batchCountTotal %>]"  value="" title="<%=grpTxt%>">
			
		 <% if(ui.isAccessible() && k==0) { %>
			    <a href="#group-begin1" id="group-end1" title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>" >
			    </a>
		 <% } 
		  }
		  batchCountTotal++;
	  }%>

<!-- Batch element info end -->
    <!-- Body Data -->
    <div style="margin-top:20px;margin-bottom:5px;">
    	<isa:translate key="b2b.order.details.batch.values"/>
    </div>

     <%   // Data table preamble
		  String tableTitle = WebUtil.translate(pageContext, "b2b.order.details.batch.values", null);
		  if (ui.isAccessible())  {
			int colno = 3;
			int rowno = batchs.size();
			String firstRow ="0";
			String columnNumber = Integer.toString(colno);
			String rowNumber = Integer.toString(rowno);
			if(batchs.size()>0)
				firstRow = Integer.toString(1);
			String lastRow = Integer.toString(rowno);
     %>
			<a href="#tableend1"
				id="tablebegin1"
				title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>"
				arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>"></a>
      <% } %>

    <table class="poslist" border=0px cellspacing="0" cellpadding="3">
    <thead>
    	<tr>
    		<th class="poslist" scope="col">&nbsp;</th>
    		<th class="poslist" scope="col" id="desccol"><isa:translate key="b2b.order.details.batch.charname"/></th>
    		<th class="poslist" scope="col" id="valuecol"><isa:translate key="b2b.order.dt.batch.charchoosen"/></th>
    	</tr>
    </thead>
    <tbody>

   <% JspUtil.Alternator alternator =
            new JspUtil.Alternator(new String[] {"odd", "even" });

      String tag_style="";
      String tag_style_input="";
      String even_odd="";
      String tag_style_detail=""; 

      int countTotal=0;
      int countCheckBox=0;
      int countCBPerLine=0;
      int countFrom=0;
      int countTo=0; 

      for(int i=0; i<batchs.size(); i++){

    	 tempElement = null; 
    	 for(int j=0; j<item.getBatchCharListJSP().size(); j++){
    	     if(batchs.get(i).getCharName().equals(item.getBatchCharListJSP().get(j).getCharName())){
    		    tempElement = item.getBatchCharListJSP().get(j);
    		 }
    	 }

         even_odd = alternator.next(); // "even" or "odd"
         tag_style= "poslist-mainpos-"+even_odd;
         tag_style_input= "batch-"+even_odd;
         tag_style_detail = "poslist-detail-" + even_odd;

         countTotal++;

         int countFromPerLine=0;
         int countToPerLine=0;
         inputs = "";
    %>
    	<tr scope="row">
    		<td class="<%= tag_style %>" valign=top>
    			<input type="hidden" name="elemCount[<%= i %>]" value="<%= batchs.size() %>">
    			<a href='javascript: toggle("characteristics_<%= i %>")' class="iconlink">
                	<img id="characteristics_<%= i %>close" style="DISPLAY: none"
                             src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>"
                             alt="<isa:translate key="b2b.order.details.item.close"/>"
                             border="0" width="16" height="16" valign=top>
                    <img id="characteristics_<%= i %>open" style="DISPLAY: inline"
                             src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>"
                             alt="<isa:translate key="b2b.order.details.item.open"/>"
                              border="0" width="16" height="16" valign=top>
                 </a>
            </td>
            <td class="<%= tag_style %>" headers="desccol" id="batchrow">
				<a href='javascript: toggle("characteristics_<%= i %>")' >
                    <input type="hidden" name="elementName[<%= i %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharName()) %>">
                    <input type="hidden" name="elementTxt[<%= i %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharTxt()) %>">
                    <input type="hidden" name="elementAddVal[<%= i %>]" value="<%= batchs.get(i).getCharAddVal() %>">
                    <input type="hidden" name="elementUnit[<%= i %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharUnit()) %>">
                    <input type="hidden" name="elementUnitTExt[<%= i %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharUnitTExt()) %>">
                    <input type="hidden" name="elementDataType[<%= i %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharDataType()) %>">

                    <%if(batchs.get(i).getCharTxt() != ""){%>
                    	<%= JspUtil.encodeHtml(batchs.get(i).getCharTxt()) %>
                    <%}
                    else{%>
                    	<%= JspUtil.encodeHtml(batchs.get(i).getCharName()) %>
                	<%}%>
            	</a>
            </td>
            <td class="<%= tag_style %>" headers="valuecol">
            	<% String unavailable = WebUtil.translate(pageContext, "access.unavailable", null);
            	   if(tempElement != null) {
            	       for(int j=0; j<tempElement.getCharValNum(); j++){
            		       if(tempElement.getCharValMax(j).equals("")){
            			       inputs = inputs + JspUtil.encodeHtml(tempElement.getCharVal(j)) + ";";
            			   }
            			   else{
            			       inputs = inputs + JspUtil.encodeHtml(tempElement.getCharVal(j)) + "-" + JspUtil.encodeHtml(tempElement.getCharValMax(j));
            		       }
            		   }
            	  %>
					   <input type="text" class="<%= tag_style_input%>" name="batchElement[<%= i %>]"
					           id="batchElement[<%= i %>]" size="50" readonly="true" value="<%= inputs %>"
					     <% if(ui.isAccessible()) { %>
						       title="<isa:translate key="access.input" arg0="" arg1="<%=unavailable%>"/>"
					     <% } %>
					    >
            	<%} 
            	  else { %>
					<input type="text" class="<%= tag_style_input%>" name="batchElement[<%= i %>]"
					id="batchElement[<%= i %>]" size="50" readonly="true" value=""
					<% if(ui.isAccessible()) { %>
						title="<isa:translate key="access.input" arg0="" arg1="<%=unavailable%>"/>"
					<% } %>
					>
            	<% } %>
            </td>
         </tr>

	<% if (tempElement == null) { %>

         <tr id="characteristics_<%= i %>"
         	<% if (!browserType.isUnknown() && !browserType.isOpera() && !browserType.isNav4()) { %>
            style="display:none;">
            <% } 
               else { %>
            >
            <% } %>

            <td class="<%= tag_style %>">&nbsp;</td>
			<td class="<%= tag_style %>" colspan="2" align="right" headers="batchrow desccol valuecol">
				<table class="poslistdetail-<%= even_odd %>">
					<tr>
						<td style="margin-left:12pt">

						<% // Data table preamble
						   String tableTitle1 = WebUtil.translate(pageContext, "b2b.order.dt.batch.charchoosen", null);
						   if (ui.isAccessible()) {
							   int rowno1 = batchs.get(i).getCharValNum();
							   String firstRow1 = (rowno1 > 0) ? "1" : "0";
							   String rowNumber1 = Integer.toString(rowno1);
						%>
							<a href="#tableend2" id="tablebegin2"
								title="<isa:translate key="access.table.begin" arg0="<%=tableTitle1%>" arg1="<%=rowNumber1%>"
								arg2="1" arg3="<%=firstRow1%>" arg4="<%=rowNumber1%>"/>">
							</a>
					  <% } %>

    				  <% for(int j=0; j<batchs.get(i).getCharValNum(); j++) {
    					     if (batchs.get(i).getCharValCode(j).equals("1")) { %>
    							<table>
    								<tr>
    									<td>
    										<input type="hidden" name="elementCharValue[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j))%>">
    										<input type="hidden" name="elementCharValTxt[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharValTxt(j))%>">
											<input type="hidden" name="elementCharValMax[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharValMax(j))%>">

											<input type="hidden" name="characteristicSelected[<%= countCheckBox%>]" id="characteristicSelected[<%= countCheckBox%>]" value="">
									     <% if(batchs.get(i).getCharAddVal()) { %>
												<input type="checkbox" name="characteristic[<%= countCheckBox %>]" id="characteristic[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j)) %>"
													onClick='submit_selection_plus_addVal("batchElement[<%= i %>]","characteristic[","characteristicAddVal[<%= i %>]","<%= batchs.get(i).getCharNumberDigit()%>", ";","elementCharValTxtFake[<%= i %>]");
																	CBSelect("characteristic[<%= countCheckBox %>]", "elementCharValue[<%= countCheckBox %>]", "characteristicSelected[<%= countCheckBox%>]")'>
										 <% }
											else{ %>
												<input type="checkbox" name="characteristic[<%= countCheckBox %>]" id="characteristic[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j)) %>"
													onClick='submit_selection("batchElement[<%= i %>]","characteristic[","characteristicAddVal[<%= i %>]","<%= batchs.get(i).getCharNumberDigit()%>", ";","elementCharValTxtFake[<%= i %>]");
																	CBSelect("characteristic[<%= countCheckBox %>]", "elementCharValue[<%= countCheckBox %>]", "characteristicSelected[<%= countCheckBox%>]")'>
										 <% } %>
											<label for="characteristic[<%= countCheckBox %>]"><%= JspUtil.encodeHtml(batchs.get(i).getCharValTxt(j)) %></label>
										</td>
									</tr>
								</table>
							 <% countCheckBox++;
								countCBPerLine++;
    					     }
							 else { %>
								<table>
    								<tr>
    									<td>
    										<label for="characteristicFrom[<%= i %>]"><isa:translate key="b2b.order.details.batch.from"/></label>
    										<input type="text" class="textInput" name="characteristicFrom[<%= i %>]" id="characteristicFrom[<%= i %>]"
												onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]", "characteristicTo[<%= i %>]")'>(<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j))%>)
											<label for="characteristicTo[<%= i %>]"><isa:translate key="b2b.order.details.batch.to"/></label>
											<input type="text" class="textInput" name="characteristicTo[<%= i %>]" id="characteristicTo[<%= i %>]"
													onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]","characteristicTo[<%= i %>]")'>(<%= JspUtil.encodeHtml(batchs.get(i).getCharValMax(j))%>)
										  <% if (batchs.get(i).getCharUnitTExt() != ""){ %>
												<%= JspUtil.encodeHtml(batchs.get(i).getCharUnitTExt()) %>
										  <% }
											 else { %>
    											<%= JspUtil.encodeHtml(batchs.get(i).getCharUnit()) %>
    									  <% } %>
    										<input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
										<td>
									</tr>
								</table>
							<% }
							} %>

						 <% if (ui.isAccessible())  { /* Data Table Postamble */ %>
							  <a id="tableend2" href="#tablebegin2" title="<isa:translate key="access.table.end" arg0="<%=tableTitle1%>"/>"</a>
						 <% } /* Data Table Postamble ends */ %>
						 <input type="hidden" name="countCBPerLine[<%= i %>]" value="<%= countCBPerLine %>">
						 <% countCBPerLine= 0; %>
						</td>
					</tr>

					<tr>
						<td style="margin-left:12pt">
						 <% if( batchs.get(i).getCharAddVal() == true || (batchs.get(i).getCharValNum() == 0 && batchs.get(i).getCharDataType().equals("CHAR"))) { %>
							<table>
								<tr>
									<td>
										<input type="text" class="textInput" name="characteristicAddVal[<%= i %>]" id="characteristicAddVal[<%= i %>]"
											onChange='submit_selection_plus_addVal("batchElement[<%= i %>]","characteristic[","characteristicAddVal[<%= i %>]","<%= batchs.get(i).getCharNumberDigit()%>", ";","elementCharValTxtFake[<%= i %>]")'>
										<input type="hidden" name="elementCharValMaxFake[<%= i %>]" value="">
										<input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
									<td>
								</tr>
							</table>
						 <% } %>

						 <% if(batchs.get(i).getCharValNum() == 0 && batchs.get(i).getCharDataType().equals("NUM")){%>
							<table>
        						<tr>
        							<td>
        								<label for="characteristicFrom[<%= i %>]"><isa:translate key="b2b.order.details.batch.from"/></label>
	    								<input type="text" class="textInput" name="characteristicFrom[<%= i %>]" id="characteristicFrom[<%= i %>]"
												onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]", "characteristicTo[<%= i %>]")'>
										<label for="characteristicTo[<%= i %>]"><isa:translate key="b2b.order.details.batch.to"/></label>
										<input type="text" class="textInput" name="characteristicTo[<%= i %>]" id="characteristicTo[<%= i %>]"
													onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]","characteristicTo[<%= i %>]")'>
									 <% if (batchs.get(i).getCharUnitTExt() != "") { %>
											<%= JspUtil.encodeHtml(batchs.get(i).getCharUnitTExt()) %>
        							 <% }
        								else{ %>
        									<%= JspUtil.encodeHtml(batchs.get(i).getCharUnit()) %>
        							 <% } %>
	    								<input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
									<td>
								</tr>
							</table>
						<% } %>
						</td>
					</tr>
				</table>
          </td>
         </tr>
      <% } 
         else { %>
            <tr id="characteristics_<%= i %>"
 				<% if (!browserType.isUnknown() && !browserType.isOpera() && !browserType.isNav4()) { %>
    				style="display:none;">
    			<% } else { %>
   				      >
   			    <% } %>

				<td class="<%= tag_style %>">&nbsp;</td>
					<td class="<%= tag_style %>" colspan="2" align="right" headers="batchrow desccol valuecol">

						<table class="poslistdetail-<%= even_odd %>">
							<tr>
								<td style="margin-left:12pt">
        							<input type="hidden" name="countCBPerLine[<%= i %>]" value="<%= batchs.get(i).getCharValNum() %>">

										<%  // Data table preamble
											String tableTitle2 = WebUtil.translate(pageContext, "b2b.order.dt.batch.charchoosen", null);
											if (ui.isAccessible())  {
												int colno2 = 1;
												int rowno2 = batchs.get(i).getCharValNum();
												String firstRow2 ="0";
												String columnNumber2 = Integer.toString(colno2);
												String rowNumber2 = Integer.toString(rowno2);
												if(batchs.get(i).getCharValNum()>0)
													firstRow2 = Integer.toString(1);
												String lastRow2 = Integer.toString(rowno2);
										 %>
												<a href="#tableend3" id="tablebegin3"
													title="<isa:translate key="access.table.begin" arg0="<%=tableTitle2%>"
													arg1="<%=rowNumber2%>"	arg2="<%=columnNumber2%>" arg3="<%=firstRow2%>" arg4="<%=lastRow2%>"/>">
												</a>
										  <% } %>

            							  <% for(int j=0; j<batchs.get(i).getCharValNum(); j++) {
            								     boolean flagCB=false;
            								     if(batchs.get(i).getCharValCode(j) == "1") {
            									     for(int k=0; k<tempElement.getCharValNum(); k++) {
            										     if(batchs.get(i).getCharVal(j).equals(tempElement.getCharVal(k))) {
            											     flagCB = true;
            											     if(tempElement.getCharVal(k).equals(tempElement.getCharVal(0))) {
            												    flagAddVal=false;
            										          } 
            							   %>
        									 <table>
        										<tr>
        											<td>
        												<input type="hidden" name="elementCharValue[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j))%>">
                										<input type="hidden" name="elementCharValTxt[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharValTxt(j))%>">
	            										<input type="hidden" name="elementCharValMax[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharValMax(j))%>">

    	        										<input type="hidden" name="characteristicSelected[<%= countCheckBox%>]" id="characteristicSelected[<%= countCheckBox%>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j)) %>">
	    	    										<input type="checkbox" name="characteristic[<%= countCheckBox %>]" id="characteristic[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j)) %>" checked
    														onClick='submit_selection("batchElement[<%= i %>]","characteristic[","characteristicAddVal[<%= i %>]","<%= batchs.get(i).getCharNumberDigit()%>", ";","elementCharValTxtFake[<%= i %>]");
																			CBSelect("characteristic[<%= countCheckBox %>]", "elementCharValue[<%= countCheckBox %>]", "characteristicSelected[<%= countCheckBox%>]")'>
														<label for="characteristic[<%= countCheckBox %>]"> <%= JspUtil.encodeHtml(batchs.get(i).getCharValTxt(j)) %> </label>
													</td>
												</tr>
        									 </table>
        								<% }
        							   }
        							}
        							else{
                                        if(batchs.get(i).getCharDataType().equals("NUM")){
                							if(batchs.get(i).getCharName().equals(tempElement.getCharName())) { %>
                    							<table>
													<tr>
														<td>
                    										<label for="characteristicFrom[<%= i %>]"><isa:translate key="b2b.order.details.batch.from"/></label>
	                										<input type="text" class="textInput" name="characteristicFrom[<%= i %>]" id="characteristicFrom[<%= i %>]" value="<%= JspUtil.encodeHtml(tempElement.getCharVal(0))%>"
																onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]", "characteristicTo[<%= i %>]")'>
															<label for="characteristicTo[<%= i %>]"><isa:translate key="b2b.order.details.batch.to"/></label>
                											<input type="text" class="textInput" name="characteristicTo[<%= i %>]" id="characteristicTo[<%= i %>]" value="<%= JspUtil.encodeHtml(tempElement.getCharValMax(0))%>"
                												onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]","characteristicTo[<%= i %>]")'>
                										   <% if (batchs.get(i).getCharUnitTExt() != ""){ %>
                												<%= JspUtil.encodeHtml(batchs.get(i).getCharUnitTExt()) %>
		    											   <% }
															  else{ %>
																<%= JspUtil.encodeHtml(batchs.get(i).getCharUnit()) %>
														   <% } %>
                											<input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
                										<td>
    			    								</tr>
                								</table>
                						<% }	
                						   else { %>
                								<table>
		        								   <tr>
    												  <td>
    													  <label for="characteristicFrom[<%= i %>]"><isa:translate key="b2b.order.details.batch.from"/></label>
    													  <input type="text" class="textInput" name="characteristicFrom[<%= i %>]" id="characteristicFrom[<%= i %>]"
                    											onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]", "characteristicTo[<%= i %>]")'>
                    									  <label for="characteristicTo[<%= i %>]"><isa:translate key="b2b.order.details.batch.to"/></label>
			    										  <input type="text" class="textInput" name="characteristicTo[<%= i %>]" id="characteristicTo[<%= i %>]"
    															onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]","characteristicTo[<%= i %>]")'>
    												    <% if (batchs.get(i).getCharUnitTExt() != ""){ %>
    														  <%= JspUtil.encodeHtml(batchs.get(i).getCharUnitTExt()) %>
    													<% }
                    									   else{ %>
    	        											  <%= JspUtil.encodeHtml(batchs.get(i).getCharUnit()) %>
	            									    <% } %>
    													  <input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
												      <td>
												  </tr>
                							  </table>
                						<% }
			    					  } 
        						   }
                				   if(!flagCB){%>
            								<table>
        										<tr>
        										   <td>
        											  <input type="hidden" name="elementCharValue[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j))%>">
                									  <input type="hidden" name="elementCharValTxt[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharValTxt(j))%>">
	            									  <input type="hidden" name="elementCharValMax[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharValMax(j))%>">

    	        									  <input type="hidden" name="characteristicSelected[<%= countCheckBox%>]" id="characteristicSelected[<%= countCheckBox%>]" value="">
	    	    									  <input type="checkbox" name="characteristic[<%= countCheckBox %>]" id="characteristic[<%= countCheckBox %>]" value="<%= JspUtil.encodeHtml(batchs.get(i).getCharVal(j)) %>"
    														onClick='submit_selection("batchElement[<%= i %>]","characteristic[","characteristicAddVal[<%= i %>]","<%= batchs.get(i).getCharNumberDigit()%>", ";","elementCharValTxtFake[<%= i %>]");
    																		document.forms["batch_positions"].elements["characteristicSelected[<%= countCheckBox%>]"].value = this.value'>
    												  <label for="characteristic[<%= countCheckBox %>]"> <%= JspUtil.encodeHtml(batchs.get(i).getCharValTxt(j)) %></label>
    											  </td>
    										  </tr>
            							  </table>
                				<% }
        						   countCheckBox++;
        					  } %>

							  <% /* Data Table Postamble */
							     if (ui.isAccessible())  { %>
							 		  <a id="tableend3" href="#tablebegin3" title="<isa:translate key="access.table.end" arg0="<%=tableTitle2%>"/>"</a>
							  <% } /* Data Table Postamble ends */
							  %>
        						</td>
        					</tr>
        					<tr>
        						<td style="margin-left:12pt">
        						 <% if (batchs.get(i).getCharAddVal() == true || (batchs.get(i).getCharValNum() == 0 && batchs.get(i).getCharDataType().equals("CHAR"))) {
        							  if (batchs.get(i).getCharName().equals(tempElement.getCharName()) && flagAddVal == true) { %>
        							<table>
        								<tr>
        									<td>
        										<input type="text" class="textInput" name="characteristicAddVal[<%= i %>]" id="characteristicAddVal[<%= i %>]" value="<%= JspUtil.encodeHtml(tempElement.getCharVal(0))%>"
        											onChange='submit_selection("batchElement[<%= i %>]","characteristic[","characteristicAddVal[<%= i %>]","<%= batchs.get(i).getCharNumberDigit()%>", ";","elementCharValTxtFake[<%= i %>]")'>
        										<input type="hidden" name="elementCharValMaxFake[<%= i %>]" value="">
        										<input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
        									<td>
        								</tr>
        							</table>
        							<%}
        							  else{%>
        							<table>
        								<tr>
        									<td>
        										<input type="text" class="textInput" name="characteristicAddVal[<%= i %>]" id="characteristicAddVal[<%= i %>]"
        											onChange='submit_selection("batchElement[<%= i %>]","characteristic[","characteristicAddVal[<%= i %>]","<%= batchs.get(i).getCharNumberDigit()%>", ";","elementCharValTxtFake[<%= i %>]")'>
        										<input type="hidden" name="elementCharValMaxFake[<%= i %>]" value="">
        										<input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
        									<td>
        								</tr>
        							</table>
        						   <% }
        						   } %>

        						<% if(batchs.get(i).getCharValNum() == 0 && batchs.get(i).getCharDataType().equals("NUM")) {
        							  if(batchs.get(i).getCharName().equals(tempElement.getCharName())) { %>
        							<table>
        								<tr>
        									<td>
        										<label for="characteristicFrom[<%= i %>]"><isa:translate key="b2b.order.details.batch.from"/></label>
        										<input type="text" class="textInput" name="characteristicFrom[<%= i %>]" id="characteristicFrom[<%= i %>]" value="<%= JspUtil.encodeHtml(tempElement.getCharVal(0)) %>"
        											onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]", "characteristicTo[<%= i %>]")'>
        										<label for="characteristicTo[<%= i %>]"><isa:translate key="b2b.order.details.batch.to"/></label>
        										<input type="text" class="textInput" name="characteristicTo[<%= i %>]" id="characteristicTo[<%= i %>]" value="<%= JspUtil.encodeHtml(tempElement.getCharValMax(0)) %>"
        												onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]","characteristicTo[<%= i %>]")'>
        									 <% if (batchs.get(i).getCharUnitTExt() != "") { %>
        										  <%= JspUtil.encodeHtml(batchs.get(i).getCharUnitTExt()) %>
        									 <% }
        										else{ %>
        										  <%= JspUtil.encodeHtml(batchs.get(i).getCharUnit()) %>
        									 <% } %>
        										<input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
        									<td>
        								</tr>
        							</table>
        						 <% }
        							else{%>
        								<table>
        								<tr>
        									<td>
        										<label for="characteristicFrom[<%= i %>]"><isa:translate key="b2b.order.details.batch.from"/></label>
        										<input type="text" class="textInput" name="characteristicFrom[<%= i %>]" id="characteristicFrom[<%= i %>]"
        											onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]", "characteristicTo[<%= i %>]")'>
        										<label for="characteristicTo[<%= i %>]"><isa:translate key="b2b.order.details.batch.to"/></label>
        										<input type="text" class="textInput" name="characteristicTo[<%= i %>]" id="characteristicTo[<%= i %>]"
        												onChange='submit_inputs("batchElement[<%= i %>]", "characteristicFrom[<%= i %>]","characteristicTo[<%= i %>]")'>
        									  <% if (batchs.get(i).getCharUnitTExt() != ""){ %>
        											<%= JspUtil.encodeHtml(batchs.get(i).getCharUnitTExt()) %>
        									  <% }
        										 else { %>
        											<%= JspUtil.encodeHtml(batchs.get(i).getCharUnit()) %>
        									  <% } %>
        										<input type="hidden" name="elementCharValTxtFake[<%= i %>]" value="">
        									<td>
        								</tr>
        							</table>
        						 <% }
        						} %>
        						</td>
        					</tr>
        				</table>
  				  </td>
			 </tr>
          <% }
      } /* Top Level for */ %>
    </tbody>
    </table>

    <% if (ui.isAccessible()) { /* Data Table Postamble */ %>
			  <a id="tableend1" href="#tablebegin1" title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>"</a>
	<% } /* Data Table Postamble ends */ %>
	
    <input type="hidden" name="countTotal" value="<%= countTotal %>">
    <input type="hidden" name="itemGuid" value="<%= JspUtil.encodeHtml(batchselection) %>">
    <input type="hidden" name="productGuid" value="<%= JspUtil.encodeHtml(productId) %>">

	<input type="hidden" name="cancel" value="">
	<input type="hidden" name="setbatch" value="">

</form>
</div>
</body>
</html>
