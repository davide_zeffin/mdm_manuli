<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.isacore.action.InitQuickSearchAction" %>
<%@ page import="com.sap.isa.businessobject.IsaQuickSearch" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectBase" %>
<%@ page import="com.sap.isa.core.util.Message" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.FrameworkConfigManager" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<isa:contentType />

<%@ include file="/b2b/checksession.inc" %>

<% /* set the type of the search to perform */
   IsaQuickSearch iqs = (IsaQuickSearch) request.getAttribute(InitQuickSearchAction.QS_OBJECT); 
   ServletContext context = pageContext.getServletContext();
   String exactSearch = FrameworkConfigManager.Servlet.getInteractionConfigParameter(pageContext.getSession(),
			"ui", 
			"exact.search.isacore", 
			"exact.search.isacore.isa.sap.com");
   String maxHits = FrameworkConfigManager.Servlet.getInteractionConfigParameter(pageContext.getSession(),
			"ui", 
			"maxhits.search.isacore", 
			"maxhits.search.isacore.isa.sap.com");
   if ("true".equalsIgnoreCase(exactSearch)) {
	   iqs.setExactSearch(true);
   }
   else {
	   iqs.setExactSearch(false);
   }
   
	BaseUI ui = new BaseUI(pageContext);
   
   %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<title><isa:translate key="iqs.input.title"/></title>
	<isa:stylesheets/>
    
	<script type="text/javascript">
	<!--
		function openWin() {
			var sat = window.open('popup_artikel_to_order.html', 'artikel_to_order', 'width=300,height=220,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
			sat.focus();
		}
      
		function searchProducts() {
			document.productsearch.submit();
		}
	//-->
	</script>
  </head>
  <body class="document-search">
	 <div id="organizer-content">
		<div class="module-name">
			<isa:moduleName name="b2b/organizer-content-product-search.jsp" />
		</div>
		 <div class="filter">
			<div class="filter-title">
				<span><isa:translate key="iqs.input.header"/></span>
			</div>
			<form name="productsearch" method="post" action="<isa:webappsURL name="b2b/firstquicksearch.do" />" >
				<% if (ui.isAccessible) { %>
					<a href="#end-search" name="begin-search" title="<isa:translate key="b2b.organizer.acc.endsearch"/>" ></a>
				<% } %>
				<%-- Accesskey for the header section. It must be an resourcekey, so that it can be translated. --%>
				<div>
					<a id="organizer" href="#access-items" title="<isa:translate key="b2b.organizer.acc.accesskey"/>" accesskey="<isa:translate key="b2b.organizer.acc.accesskey.key"/>" ></a>
				</div>			
				<ul>
					<li>
						<span title="label shuffler">
							<select id="doctype" class="bigCatalogInput" name="attribute">
								<%-- we have to construct the key for the translate-tag dynamically --%>
								<%-- init the String-var for this here --%>
								<% String thePrefix = "iqs.attribute."; %>
								<% String theKey; %>

								<isa:iterate id="attributes" name="<%= InitQuickSearchAction.QS_COMMONATTRIBUTES_LIST %>"
									 type="com.sap.isa.catalog.impl.CatalogAttribute">
									<% String theDescription = InitQuickSearchAction.getAttributeDescription( pageContext, thePrefix, attributes);
									if (theDescription != null) { 
									%>
										<% if (null == iqs) { %>
											<option value="<%=attributes.getName() %>" <%=attributes.getName().equalsIgnoreCase("object_id") ? "selected" : "" %> ><%=theDescription %></option>
										<% } else { %>
											<% if (!iqs.getSearchAttribute().equals("")){ %>
												<option value="<%=attributes.getName() %>" <%=attributes.getName().equalsIgnoreCase(iqs.getSearchAttribute()) ? "selected" : "" %> ><%=theDescription %></option>
											<% } else { %>
												<option value="<%=attributes.getName() %>" <%=attributes.getName().equalsIgnoreCase("object_id") ? "selected" : "" %> ><%=theDescription %></option>
											<% } %>
										<% } %>
									<% } %>
								</isa:iterate>						
							</select>
						</span>
					</li>
					<li>
						<% if (null != iqs && null != iqs.getSearchexpr()) { %>
							<input name="searchexpr" type="text" class="input-1" value="<%=iqs.getSearchexpr()%>" />
						<% } else { %>
							<input name="searchexpr" type="text" class="input-1" value="" />
						<% } %>
					</li>
					<li>
						<%-- create a dropdown box to display the max. number of hits to return --%>
						<select name="displayHits" id="displayHits" style="width:50px" title="<isa:translate key="b2b.organizer.displayhits.title" />">
						  <%-- only hardcoded values here --%>
						  <% if (null != iqs) { 
							  String dispHits = iqs.getDisplayHitsAsString();
							  if ("0".equals(dispHits)) {
								  dispHits = "50";
							  } %>
							  <option value="5" <%="5".equals(dispHits) ? "selected=\"selected\"" : ""%> >5</option>
							  <option value="10" <%="10".equals(dispHits) ? "selected=\"selected\"" : ""%> >10</option>
							  <option value="25" <%="25".equals(dispHits) ? "selected=\"selected\"" : ""%> >25</option>
							  <option value="50" <%="50".equals(dispHits) ? "selected=\"selected\"" : ""%> >50</option>
							  <option value="100" <%="100".equals(dispHits) ? "selected=\"selected\"" : ""%> >100</option>
						  <% } else { %>
							  <option value="5">5</option>
							  <option value="10">10</option>
							  <option value="25">25</option>
							  <option value="50" selected>50</option>
							  <option value="100">100</option>
						  <% } %>
						</select><label for="displayHits" >&nbsp;<isa:translate key="iqs.input.displayhits"/></label>
					</li>
					<li>
						<a href="#" onclick="searchProducts();" title="<isa:translate key="iqs.input.submit" />: <isa:translate key="b2b.organizer.search.button" />" class="button"><isa:translate key="iqs.input.submit" /></a>
					</li>
					<%-- explanation of search functionality --%>
					<li>
						<div style="width:90%; white-space:normal;">
							<isa:translate key="iqs.description.row1" />
							<isa:translate key="iqs.description.row2" />
						</div>
					</li>
					<%-- print some error messages, if available --%>
					<isa:message id="errortext" name="<%=InitQuickSearchAction.QS_OBJECT%>" type="<%=Message.ERROR%>" >
						<li>
							<div class="error">
								<span>
									<%=errortext%>
								</span>
							</div>				
						</li>
					</isa:message>
					<isa:message id="infotext" name="<%=InitQuickSearchAction.QS_OBJECT%>" type="<%=Message.INFO%>" >
						<li>
							<div class="info">
								<span>
									<%=infotext%>
								</span>
							</div>
						</li>				
					</isa:message>
				</ul>
				<% if (ui.isAccessible) { %>
					<a href="#begin-search" name="end-search" title="<isa:translate key="b2b.organizer.acc.beginsearch"/>"></a>
				<% } %>

				<%-- hidden field containing the maximim number of hits to read from the catalog engine --%>
				<div>
					<input type="hidden" name="maxHits" value="<%=maxHits%>" />
				</div>
			</form>
		</div>
	  </div>
<%-- GREENORANGE BEGIN --%>
	<div id="zgo_sign">
		<b><font color="01663E">Fres-co Online Sales</font></b><br/>
		<b><font color="01663E">a Division of Goglio SpA</font></b><br/>
		Via Dell'Industria 7<br/>
		21020 Daverio (VA) - Italy<br/>
		Phone: +39 0332 940240 <br/>
		Fax: +39 0332 940716 <br/>
		P.I. IT00870210150 <br/>
		Capitale sociale Euro 10.449.000<br/>
		Registro Imprese Milano n.00870210150<br/>
		C.C.I.A.A. Milano Rea N. 1856<br/>
		(Mecc.Export MIO78374)<br/>
	</div>
<%-- GREENORANGE END --%>	  
  </body>
</html>
