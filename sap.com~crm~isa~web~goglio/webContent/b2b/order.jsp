<%--
********************************************************************************
    File:         order.jsp
    Copyright (c) 2001, SAP Germany, All rights reserved.

    Author:       SAP
    Created:      19.4.2001
    Version:      1.0

    $Revision: #53 $
    $Date: 2002/07/29 $  
********************************************************************************
--%>

<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.core.businessobject.management.*" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.BasketUI" %>
<%@ page import="com.sap.isa.helpvalues.*" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.*" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="checksession.inc" %>

<isa:contentType />
<isacore:browserType/>

<%
/*  this code is used by the modification examples
    String z_extHeaderData = "";

    if (request.getAttribute("Z_CustomHeaderField") != null)
    z_extHeaderData = (String)request.getAttribute("Z_CustomHeaderField");
*/
    BasketUI ui = new BasketUI(pageContext); 
    //inline display of product configuration will be restricted on values of identifying characteristics
    boolean showDetailView = false;
//  GREENORANGE INSERT BEGIN	
	FieldSupport field; // Oggetto per gli help values
	
	String z_extCurrency = (String)ui.header.getExtensionData("ZEXT_WAERK");
//	String z_extCurrency = ui.header.getCurrency();
	String z_extTermsPayment = ui.header.getPaymentTerms();
	String z_extInco1 = ui.header.getIncoTerms1();
	String z_extInco2 = ui.header.getIncoTerms2();	

	/*if (request.getAttribute("currency") != null){
		z_extCurrency = (String)request.getAttribute("currency");
		ui.header.setCurrency(z_extCurrency);
	}		
	if (request.getAttribute("paymentTerms") != null){
		z_extTermsPayment = (String)request.getAttribute("paymentTerms");
		ui.header.setPaymentTerms(z_extTermsPayment);
	}*/	
	UserSessionData userSessionData = UserSessionData.getUserSessionData(pageContext.getSession());
	String yourRefError = (String)userSessionData.getAttribute("yourRefError");

//	GREENORANGE INSERT END

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>
        <isa:translate key="b2b.order.display.pagetitle"/>
    </title>

   <isa:stylesheets/>
    
    <script type="text/javascript">
          <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
    </script>
    <%-- include calendar control --%>
    <%@ include file="/appbase/jscript/calendar.js.inc.jsp"%>

    <%DateFormat dateFormatter = new SimpleDateFormat(ui.getDefaultDateFormat());%>
            
    <%@ include file="/b2b/jscript/orderTools.inc.jsp" %>
    
    <script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/table_highlight.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "auction/jscript/calendar.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/browsersniff.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
          type="text/javascript"> 
    </script>  
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
          type="text/javascript">
    </script>
	<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderToolsRetkey.js") %>"
          type="text/javascript">
    </script>
   <% if(ui.getShop().isPricingCondsAvailable() == true) {%>
	 <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
		   type = "text/javascript">
	 </script>
<% } %>
    <script type="text/javascript">
          <%@ include file="/b2b/jscript/ExtRefObjTools.js"  %>
    </script>  

    <%-- javascript functions for batches - import only when batch flag on shop level is enabled --%>
    <isacore:ifShopProperty property = "batchAvailable" value = "true">
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/batchTools.js") %>"
              type="text/javascript">
      </script>
    </isacore:ifShopProperty>

    <script type="text/javascript">    	
    <!--
	<% if(response.getCharacterEncoding() != null) { %>
	contentType = "<%=response.getCharacterEncoding()%>";
	<%  }  %>

        function searchSoldTo() {
            parent.parent.organizer_nav.location.href="<isa:webappsURL name="/b2b/organizer-nav-customer-search.jsp"/>";
            parent.parent.organizer_content.location.href="<isa:webappsURL name="/b2b/preparebupasearch.do"/>";
        }
    //-->
    <!--	    
		<%=HelpValuesSearchUI.getJSPopupCoding(ui.HELP_VALUE_CUSTOMER_SEARCH, pageContext, ui.getCustomerSearchMapping("customerNumber")) %>			    
	//-->	
    </script>
    <%-- The javascript function openWin is used to display the values
    in an additional popup window --%>
    <script type="text/javascript">
    <!--
    	<%= HelpValuesSearchUI.getJSPopupCoding("IncoTerms1",pageContext,new Boolean(false)) %>
		<% 
		if (ui.isProductValuesSearchAvailable()) { %>
			<%=HelpValuesSearchUI.getJSPopupCoding("Product",pageContext,new Boolean(false)) %>
		<% } %>  	    
		<%=HelpValuesSearchUI.getJSPopupCoding("Customer",pageContext,new Boolean(false)) %>			    
	//-->			    
    </script>
<%-- End search --%>            
  </head>
  
  <body class="order" onfocus="selReqDelSelectWdwOpen();" onload="miRegisterObjects();" onkeypress="checkReturnKeyPressed(event);">
<%--
The body consists of two layers: 
 - the document layer, containing the header and the items of the document. It is scrollable.
 - the buttons layer, containig the buttons for the document. It stays at the bottom of the page.
--%>  
  <div class="module-name"><isa:moduleName name="b2b/order.jsp" /></div>
  
  <%-- Accesskey for the header section. --%>
  <a id="access-header" href="#access-items" title="<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>

  <h1><span><%= JspUtil.encodeHtml(ui.getHeaderDescription()) %>:&nbsp;<isa:translate key="b2b.docnav.new"/></span></h1>
  	
  <div id="document"> <%-- level: sub1 --%>
<%--
The document layer contains:
 - the document-header layer, which holds the header information of the document
 - the document-items layer, which contains all items
--%>
	<%-- Show accessibility messages --%>
	<% if (ui.isAccessible) { %>
		<%@ include file="/appbase/accessibilitymessages.inc.jsp"  %>
	<% } %>

    <%-- !!!! Do not change the name of the form, the JavaScript functions rely on them !!!! --%>
    <form action="<isa:webappsURL name="b2b/maintainbasket.do"/>"
          id="order_positions"
          name="order_positions"
          method="post">
    <div>
        <isacore:requestSerial mode="POST"/>
        <%-- store all businesspartner informations --%>
        <%= ui.storeBusinessPartners() %>
    </div>
  
    <div class="document-header"> <%-- level: sub2 --%>
    <%--
    The document-header layer contains:
     - the data table. It displays the title of the document and the standard input fields like <Your Reference:>, <Deliver To>, ...
     - the price table. It displays price information like <Total price net>, <Freight>, <Taxes> ...
     - the message table. It displays the textarea for the message entry <Your message to us>.
     - the error layer (optionally), if there are errors.
    --%>
    
        <div class="header-basic">  <%-- level: sub3 --%>
		<table class="layout">
		    <tr>
		        <td class="col1">
                    <%-- the header information for the basket --%>                   
		            <% if (ui.isAccessible) { %>
		                <a href="#end-table1" title="<isa:translate key="b2b.acc.header.dat.link"/>"></a> 
		            <% } %>		        
                    <table class="header-general" summary="<isa:translate key="b2b.acc.header.dat.sum"/>">
				        <% if (ui.isServiceRecall()) { %>
				           <%@ include file="/b2b/serviceRecallHeader.inc.jsp" %>	
				        <% } %>
				        <% if (ui.isSoldToVisible() && ui.isElementVisible("order.soldTo")) { %>
				        <tr>
				            <td class="identifier">
				                <label for="customerNumber"><isa:translate key="b2b.order.display.cnsmr.b2r.no"/></label>
				            </td>
				            <td class="value">
						    <% if (ui.isSoldToChangeable() && ui.isElementEnabled("order.soldTo")) { %>
								  <input type="text" class="textinput-large" type="text" name="customerNumber" id="customerNumber" value="<%= JspUtil.encodeHtml(ui.getSoldToId()) %>"/>
								  <%= HelpValuesSearchUI.getJSCallPopupCoding(ui.HELP_VALUE_CUSTOMER_SEARCH,"order_positions","",pageContext) %>
								  <input type="hidden" name="customerNumberOld" value="<%= JspUtil.encodeHtml(ui.getSoldToId()) %>"/>
								 
							   <% if (!ui.isBackendR3PI()){ %>   
								  <a href="#" class="icon" onclick="createSoldTo()">
									  <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_create.gif") %>" alt="<isa:translate key="b2b.order.display.cnsmr.b2r.add"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
								  </a>
				                <% } 
				                 }
				                 else { %>
				                  <strong><%= JspUtil.encodeHtml(ui.getSoldToId()) %></strong>
				                <% } %>
				            </td>
				        </tr>
				        <tr>
				            <td class="identifier">
				                <isa:translate key="b2b.order.consumer.b2r.name"/>
				            </td>
				            <td class="value">
						      <% 
						      if (ui.isSoldToChangeable()) { %>
				                <a href="#" onclick="showSoldToInSameFrame()"><%= JspUtil.encodeHtml(ui.getSoldToInfo()) %></a>
				              <%
						      }
						      else { %>  
				                <a href="#" onclick="showSoldTo('<%= JspUtil.removeNull(ui.getSoldToKey()) %>')"><%= JspUtil.encodeHtml(ui.getSoldToInfo()) %></a>
				              <%
						      } %>  
				            </td>
				        </tr>
				        <% } %>
			            <tr>
				           <td class="identifier">
				                <% if ((!ui.isOciTransfer() || !ui.isBasket()) && ui.isElementVisible("order.numberExt")) { %>
				                    <label for="poNumber"><isa:translate key="b2b.order.display.ponumber"/></label>
				                <% } %>
				                </td>
				                <td class="value">
				                  <% if (!ui.isOciTransfer() || !ui.isBasket() || ui.isElementVisible("order.numberExt")) { %>
  				                    <% if (ui.isElementEnabled("order.numberExt")) { %>					                
					                    <input type="text" class="textinput-large" maxlength="<%=(ui.isBackendR3()) ? 20 : 35%>"
					                           name="poNumber" id="poNumber"
					                           value="<%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %>"/>					                
					                <% } else { %>
					                  <%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %>
								    <% } %>
								  <% } else { %>
					                <input name="poNumber"  type="hidden" value="<%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %>"/>								   
								  <% } %>
								  <%-- GREENORANGE BEGIN --%>
								  <% if(yourRefError != null && yourRefError.length() > 0) { %>
									  <div class="error"><span><%= yourRefError %></span></div>
								  <% } %>	
								  <%-- GREENORANGE END --%>								  
				                </td>
				        </tr>						        
				        <tr>
				           <% if (ui.isElementVisible("order.description")) { %>
				                <td class="identifier">
				                    <% if (!ui.isBackendR3() && (!ui.isOciTransfer() || !ui.isBasket())) { %>
				                        <label for="headerDescription"><isa:translate key="b2b.order.display.poname"/></label>
				                    <% } %>
				                </td>
				            <% } %>
				            <td class="value">
				               <% if (!ui.isBackendR3() && (!ui.isOciTransfer() || !ui.isBasket()) ) { %>
    				               <% if (ui.isElementVisible("order.description")) { %>
				                          <% if (ui.isElementEnabled("order.description")) { %>
				                             <input type="text" class="textinput-large" maxlength="40"
				                                    name="headerDescription" id="headerDescription"
				                                    value="<%= JspUtil.encodeHtml(ui.header.getDescription()) %>"/>
				                        <% } else { %>
                                             <%= JspUtil.encodeHtml(ui.header.getDescription()) %>
								        <% } %>  
								    <% } else { %>
								       <input type="hidden" name="headerDescription" value="<%= JspUtil.encodeHtml(ui.header.getDescription()) %>"/>
								    <% } %>    
				               <% } %>
				            </td>
				        </tr>	
	            

						<%-- GREENORANGE INSERT BEGIN --%>	
						
              			<%-- Currency WAERK --%> 
						<tr>
							<td>
								<isa:translate key='customer.order.jsp.Currency'/>
							</td>
							<td>
		    					<%--input name="Z_CustomCurrency" id="Z_CustomCurrency" maxlength="3" class="textinput-small" type="text"  
		    					value="<%= z_extCurrency %>"/--%>
								
								<%--select class="select-large" name="Z_CustomCurrency" id="Z_CustomCurrency">
			                      	<option value="00">&nbsp;</option>	                       
									<isa:helpValues id="currencyList" name="Currency">
									    <option value="<%= currencyList.getValue("currency") %>">
			    					       <%= currencyList.getValueDescription("currency") %>
			        				    </option>
			   						 </isa:helpValues>
				                </select--%>
								<% 
									field = new FieldSupport(pageContext, "Currency", "order_positions", "currency", ""); 
									field.setListThreshold(101);
									if (z_extCurrency != null && z_extCurrency.length() > 0){
										field.setDefValue(z_extCurrency);
									}
								%>
								<%@ include file="/appbase/fieldwithhelpvalues.inc.jsp" %>
							</td>
				      	</tr>
						<%-- Currency WAERK --%>

              			<%-- Terms payment ZTERM --%> 
						<tr>
							<td>
								<isa:translate key="customer.order.jsp.TermsPayment"/>
							</td>
							<%--
		    					<td class="value" colspan="<%= ui.getNonCampHeaderDetailColspan() %>">
			    	   			<input name="Z_CustomTermsPayment" id="Z_CustomTermsPayment" maxlength="4" class="textinput-small" type="text" value=""/>
				       			<%= HelpValuesSearchUI.getJSCallPopupCoding("TermsPayment","order_header","",pageContext) %>
			                    <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<%= JspUtil.replaceSpecialCharacters(ui.header.getPaymentTermsDesc()) %>"/>
							--%>
							<td>						
		    					<%--input name="Z_CustomTermsPayment" id="Z_CustomTermsPayment" maxlength="4" class="textinput-small" type="text"  
		    					value="<%= z_extTermsPayment %>"/--%>
		    					<% 
									field = new FieldSupport(pageContext, "PaymentTerms", "order_positions", "paymentTerms", ""); 
									field.setListThreshold(51);
									if (z_extTermsPayment != null && z_extTermsPayment.length() > 0)
										field.setDefValue(z_extTermsPayment);
								%>
								<%@ include file="/appbase/fieldwithhelpvalues.inc.jsp" %>
							</td>
				      	</tr>
						<%-- Terms payment ZTERM --%>
						            
               			<%-- Incoterms 1 --%>
	    				<tr>
	    					<td class="identifier">
	    						<label for="incoterms1"><isa:translate key="b2b.order.disp.incoterms1"/></label>            
	    					</td>
		    				<td class="value" colspan="<%= ui.getNonCampHeaderDetailColspan() %>">
		    					<% 
									field = new FieldSupport(pageContext, "IncoTerms1", "order_positions", "incoterms1", ""); 
									field.setListThreshold(51);
									if (z_extInco1 != null && z_extInco1.length() > 0)
										field.setDefValue(z_extInco1);
								%>
								<%@ include file="/appbase/fieldwithhelpvalues.inc.jsp" %>
								
			    	   			<!-- <input name="incoterms1" id="incoterms1" maxlength="3" class="textinput-small" type="text" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.disp.incoterms"/><isa:translate key="b2b.acc.poss.entries.tab.ret"/>" <% } %>
			    	   	 			 value="<%=JspUtil.removeNull(ui.header.getIncoTerms1())%>"/>
				       			<%= HelpValuesSearchUI.getJSCallPopupCoding("IncoTerms1","order_positions","",pageContext) %> 
			                    <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<%= JspUtil.replaceSpecialCharacters(ui.header.getIncoTerms1Desc()) %>"/> -->
               			<%-- Incoterms 2 --%>
		    	      			<input name="incoterms2" id="incoterms2" maxlength="28" class="textinput-large" type="text" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.disp.incoterms2"/>" <% } %>
		    	   	        		 value="<%=JspUtil.removeNull(ui.header.getIncoTerms2())%>"/>
		    	       			<img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.order.disp.incoterms2"/>"/>
	    					</td> 
	    				</tr>
 						<%-- GREENORANGE INSERT END --%>	            
			            <%-- this code is used by the modification examples 
			            <tr>
			            <td>
			              <isa:translate key="mod.demo.b2b.z_customBasketHeaderField"/>
			            </td>
			            <td>
			              <input name="Z_CustomHeaderField" type="text" class="textInput" value="<%= z_extHeaderData %>"/>
			            </td>
			            </tr>
			            --%>
                    </table>
		            <% if (ui.isAccessible) { %>
		                <a name="end-table1" title="<isa:translate key="b2b.acc.header.dat.end"/>"></a>
		            <% } %>
                    <%-- End table header-general --%>
                </td>			            
                <td class="col2">              
			        <%-- Price --%>
					<% if (ui.isAccessible) { %>
					   <a href="#end-table2" title="<isa:translate key="b2b.acc.header.prc.link"/>"></a> 
					<% } %>
			        <table class="price-info" summary="<isa:translate key="b2b.acc.header.prc.sum"/>">
			            
			            <% if (ui.isElementVisible("order.priceNet")) { %>
				            <tr>
					            <% if (ui.isNetValueAvailable()) { %>
					                <td class="identifier" scope="row"><isa:translate key="b2b.order.display.pricenet"/></td>
					                <td class="value" ><%= JspUtil.removeNull(ui.getHeaderValue()) + "&nbsp;" + JspUtil.encodeHtml(ui.header.getCurrency())%>
					                </td>
					            <% } %>
			                </tr>
			            <% } %>	            
			            <% if (ui.isElementVisible("order.freight")) { %>
				            <tr>
					            <% if (ui.isFreightValueAvailable()) { %>
					                <td class="identifier" scope="row"><isa:translate key="b2b.order.display.freight"/></td>
					                <td class="value"><%= JspUtil.removeNull(ui.header.getFreightValue()) + "&nbsp;" + JspUtil.encodeHtml(ui.header.getCurrency())%></td>
					            <% } %>
			                </tr>
			            <% } %>		            
			            <% if (ui.isElementVisible("order.tax")) { %>    
				            <tr>
					            <% if (ui.isTaxValueAvailable()) { %>
					                <td class="identifier" scope="row"><isa:translate key="b2b.order.display.tax"/></td>
					                <td class="value"><%= JspUtil.removeNull(ui.header.getTaxValue()) + "&nbsp;" + JspUtil.encodeHtml(ui.header.getCurrency())%></td>
					            <% } %>
				            </tr>
				        <% } %>    
			            <% if (ui.isElementVisible("order.priceGross") && ui.isGrossValueAvailable()) { %>
				            <tr>
				                <td colspan="2" class="separator"></td>
				            </tr>
				            <tr>
				                <td class="identifier" scope="row"><isa:translate key="b2b.order.display.pricebrut"/></td>
				                <td class="value"><%= JspUtil.removeNull(ui.header.getGrossValue()) + "&nbsp;" + JspUtil.encodeHtml(ui.header.getCurrency())%></td>
				            </tr>
			            <% } %>
				        <% if (ui.isElementVisible("order.paymentterms") && ui.showHeaderPaymentTerms()) { %>
				            <tr>
				            	<td class="identifier-1" scope="row"><isa:translate key="b2b.order.disp.paymentterms"/></td>
				            	<td class="terms"><%=JspUtil.encodeHtml(ui.header.getPaymentTermsDesc()) %></td>
				            </tr>
				        <% } %>            
			        </table>
			        <% if (ui.isAccessible) { %>
					   <a name="end-table2" title="<isa:translate key="b2b.acc.header.prc.end"/>"></a>
					<% } %>
			        <%-- End Price--%> 
			    </td>
			</tr>
	    </table> <%-- class="layout"> --%>
	    </div> <%-- class="header-basic"> --%>
	    
	    <div class="header-itemdefault">  <%-- level: sub3 --%>
	        <h1 class="group"><span><isa:translate key="b2b.header.itemdefault"/></span></h1>
	        <div class="group">
            <%--Item defaults Data--%>
        	<% if (ui.isAccessible) { %>
		   	    <a name="#end-table4" title="<isa:translate key="b2b.acc.header.defdata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
		    <% } %>	        
	            <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>"> <%-- level: sub3 --%>               
			        <% if (ui.isElementVisible("order.deliverTo")) { %> 
			            <tr>
				            <% if (ui.isOciTransfer()  || !ui.isElementEnabled("order.deliverTo")) { %>
				                <td class="identifier"><label for="headerShipTo"><isa:translate key="b2b.order.display.soldto"/></label></td>
				                <td class="value" colspan="2">
				                    <%=ui.header.getShipTo().getShortAddress()%>
				                    <input type="hidden" name="headerShipTo" value="<%=ui.header.getShipTo().getTechKey()%>"/>
				                    <% if (ui.header.getShipTo().getHandle() != null) { %>
				                    	<input type="hidden" name="headerShipToHandle" value="<%=ui.header.getShipTo().getHandle()%>"/>
				                    <% } %>				                    	
                                    <a href="#" class="icon" onclick="showShipToWithTechKey(<%=ui.header.getShipTo().getTechKey()%>);" ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>"/></a>				                    
				                </td>
				            <% } else { %>
				               <% if (ui.getNumShipTos() > 0) { %>
					                <td class="identifier"><label for="headerShipTo"><isa:translate key="b2b.order.display.soldto"/></label></td>
		    		                <td class="value">
						                <select class="select-xlarge" name="headerShipTo" id="headerShipTo" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.shipto.title"/>" <% } %> >
					                        <isa:iterate id="shipTo"
					                             name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
					                             type="com.sap.isa.businessobject.ShipTo">
					                             <option <%= ui.getShipToSelected(shipTo) %> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
					                        </isa:iterate>
					                    </select>&nbsp;
					                    <a href="#" class="icon" onclick="showShipTo(document.order_positions.headerShipTo.selectedIndex);" ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>"/></a>
					                    <a href="#" class="icon" onclick="newShipTo();"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_new_green.gif") %>" alt="<isa:translate key="b2b.order.icon.newshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>"/></a>
					                </td>
				               <% } else { %>
				                  <td colspan=2>
				                    &nbsp;
				                 </td>
				               <% } %>
				            <% } %>   
			            </tr>
                    <% } %>
		            <% if (ui.isOciTransfer()) { %>
			            <tr>           
			                <td class="identifier" colspan=2>
			                      <input type="hidden" name="shipCond" value="<%= JspUtil.encodeHtml(ui.header.getShipCond())%>"/>
			                </td>
			            </tr>	                
		            <% }
		               if (ui.isElementVisible("order.shippingCondition")&& !ui.isOciTransfer()) { %>		            
					      <%@ include file="/b2b/shippingCondition.inc.jsp" %>
		            <% } 
					   if (ui.isElementVisible("order.deliveryPriority")){ %>		            					
                          <%@ include file="/b2b/deliveryPriorityHeader.inc.jsp" %>
		            <% } %>
		            <% if (ui.isElementVisible("order.reqDeliveryDate")){ %>                         	
			            <tr>
			                <td class="identifier">
			                  <% if (ui.isBasket() || ui.isQuotation()) { %>
			                        <label for="headerReqDeliveryDate"><isa:translate key="b2b.order.disp.reqdeliverydate"/></label>
			                  <% } %>
			                </td>
			                <td class="value">
				                <% if (ui.isElementEnabled("order.reqDeliveryDate") && (ui.isBasket() || ui.isQuotation())) { %>
					                  <input name="headerReqDeliveryDate" id="headerReqDeliveryDate" class="textinput-middle" type="text" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.disp.reqdelivdatehead2"/><isa:translate key="b2b.acc.reqdeldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
					                           value="<%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %>"
					                           onblur="checkReqDateChange();"/>				  
								    <% if (!ui.isAccessible) { %>
										   <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'headerReqDeliveryDate');">
					                          <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
										   </a>
								    <% } %>								
					            <% } else { %>
                                    <%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %>
        					        <input name="headerReqDeliveryDate" type="hidden" value="<%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %>" />
        					    <% } %>    
			                </td>
			            </tr>
			        <% } else { %>
			            <input name="headerReqDeliveryDate" type="hidden" value="<%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %>" />
			        <% } %>
		            <%-- Header Cancel date --%>
		            <% if (ui.isElementVisible("order.latestDeliveryDate")){ %>
			            <tr>
		    				<td class="identifier">
		       					<% if (ui.isBasket() || ui.isQuotation()) { %>
		             				<label for="headerCancelDate"><isa:translate key="b2b.order.grid.canceldate"/></label>
		       					<% } %>
							</td>
							<td class="value">
			   					<% if (ui.isBasket() || ui.isQuotation()) { %>
			   					      <% if (ui.isElementEnabled("order.latestDeliveryDate")){ %> 
				         			     <input name="headerlatestdlvdate" id="headerlatestdlvdate" class="textinput-middle" type="text" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.grid.canceldatehead2"/><isa:translate key="b2b.acc.canceldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
				                				value="<%= JspUtil.encodeHtml(ui.header.getLatestDlvDate()) %>"
				                				onblur="checkCancelDateChange();"/>
				    	  				 <% if (!ui.isAccessible) { %>
						  				  	   <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'headerlatestdlvdate');">
				                                    <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
									           </a>
						  				 <% } %>
			   					      <% } else { %>
    			   					      <input name="headerlatesdlvdate" type="hidden" value="<%= JspUtil.encodeHtml(ui.header.getLatestDlvDate()) %>" />
			      			  	      <% } %>
			      			  	<% } %>
							</td>
						</tr>
					<% } %>
					<%-- end of header cancelDate --%>		            		            	            	            
	            </table> <%-- end class="data" --%>
        	    <% if (ui.isAccessible) { %>
		   	        <a name="end-table4" title="<isa:translate key="b2b.acc.header.end.defdata"/>"></a>
		        <% } %>                                                
                <%-- End Item defaults Data--%>	            
	        </div> <%-- end class="group" --%>
	    </div> <%-- class="header-itemdefault"> --%>

        <%-- Additional Order Data --%>
        <% if (ui.isAccessible) { %>
		   	<a name="#end-table5" title="<isa:translate key="b2b.acc.header.adddata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
		<% } %>	    
	    <div class="header-additional">  <%-- level: sub3 --%>
            <h1 class="area">
                <a class="icon" id="toggleHeader" href="javascript:toggleHeader()" ><img id="addheadericon" src=<%=WebUtil.getMimeURL(pageContext,"mimes/images/open.gif")%> alt="<isa:translate key="b2b.order.adddata.icon.detail"/>"  /></a>
                <span><isa:translate key="b2b.header.adddata"/></span>
            </h1>	            
	        <div class="area" id="addheader" style="display: none">
	            <% if (ui.showCampaignData() || ui.showIncoterms()) { %>
                <div class="header-misc">
                    <h1 class="group"><span><isa:translate key="b2b.header.group.misc"/></span></h1>           
                    <div class="group">
                        <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>">   
							<%@ include file="/b2b/incoterms.inc.jsp" %>   
				            <%@ include file="/b2b/campaignsHeader.inc.jsp" %>
                        </table> <%-- end class="data" --%>
                    </div> <%-- end class="group" --%>                                       
                </div> <%-- end class="header-misc" --%>
                <% } %>
                <% if (ui.showExtRefData()) { %>
                <div class="header-misc">
                    <h1 class="group"><span><isa:translate key="b2b.header.group.extref"/></span></h1>
                    <div class="group">
                        <table class="data" summary="<isa:translate key="b2b.acc.header.extrefdata"/>">
          				    <% if (!ui.isServiceRecall()) { %>
				               <%@ include file="/b2b/extrefobjectheader.inc.jsp" %>	
				            <% } %>
				            <%@ include file="/b2b/extrefnumberheader.inc.jsp" %>  
                        </table> <%-- end class="data" --%>
                    </div> <%-- end class="group" --%> 
                </div> <%-- end class="header-misc" --%> 
                <% } %>
                <%-- Payment data --%>
                <%@ include file="/b2b/payment.inc.jsp" %> 
				<%-- end payment data --%>
				<div class="header-message">
                    <h1 class="group"><span><isa:translate key="b2b.header.group.message"/></span></h1>
                    <%-- Data Message --%>
                    <div class="group">
				        <% if (!ui.isOciTransfer() || ui.isElementVisible("order.comment")) { %>
					        <table class="message" summary="<isa:translate key="b2b.acc.header.msg.end"/>">
					            <tr>
					                <td class="identifier" ><label for="headerComment"><isa:translate key="b2b.order.details.note"/></label></td>
					                <td>
										<% if (ui.isElementEnabled("order.comment"))  { %>
										   <textarea id="headerComment" name="headerComment" rows="5" cols="80"><%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText(), new char[] {'\n'}):"" %></textarea>
									    <% } else { %>
										   <textarea id="headerComment" name="headerComment"  class="textarea-disabled"  rows="5" cols="80" readonly><%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText(), new char[] {'\n'}):"" %></textarea>
									    <% } %>
									</td>
					            </tr>
					        </table>
				        <% } %>
				    </div> <%-- end class="group" --%>
				    <%-- End Data Message --%>
				</div> <%-- end class="header-message" --%>
			</div> <%-- End class="area" --%>
		</div> <%-- class="header-additional"> --%>
		<% if (ui.isAccessible) { %>
		   	<a name="end-table5" title="<isa:translate key="b2b.acc.header.end.adddata"/>"></a>
		<% } %>                                                                                        
        <%-- End Additional Order Data--%>
        <%-- Document flow --%>
        <% if ( ui.isElementVisible("order.connectedDoc") && 
                ((ui.header.getPredecessorList() != null && ui.header.getPredecessorList().size() > 0) ||
                 (ui.header.getSuccessorList() != null && ui.header.getSuccessorList().size() > 0))) { %>
            <div class="header-docflow">  <%-- level: sub3 --%>
                <h1 class="group"><span><isa:translate key="b2b.header.group.docflow"/></span></h1>
                <div class="group">
                    <table class="data">
		                <%@ include file="/ecombase/connecteddocuments.inc.jsp" %>                
                    </table> <%-- end class="data" --%>
                </div> <%-- end class="group" --%>
            </div> <%-- class="header-docflow"> --%>
        <% } %>
        <%-- end document flow --%>
        
        <%-- Error --%>
        <%@ include file="/b2b/headErrMsg.inc.jsp" %>
        <%-- End Error --%>
    </div> <%-- End class="document-header" --%>
     
    <%-- the item information for the basket --%>
    <div class="document-items"> 
       <a id="access-items" href="#access-buttons" title= "<isa:translate key="b2b.acc.items.title"/>" accesskey="<isa:translate key="b2b.acc.items.key"/>"></a>
         <% if (ui.isAccessible) { %>
		    <a href="#end-table10" title="<isa:translate key="b2b.acc.items.link"/>"></a>
		 <% } %>
            <table class="itemlist" summary="<isa:translate key="b2b.acc.items.sum"/>">
               <tr>
                  <th class="opener">&nbsp;
                        <% int numberOfRows = ui.items.size() + ui.newpos; 
                           int startIndex   = 1; %>
                        <% if (numberOfRows > 0) {  %>                         
                          <a class="icon" id="toggleAllItems" href='javascript: toggleAllItemDetails("rowdetail_all","rowdetail_", <%= numberOfRows%>, <%= startIndex %>)' >
    						   <img class="img-1" id="rowdetail_allclose" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.close"/> <% } %>">
	    					   <img class="img-1" id="rowdetail_allopen" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.open"/> <% } %>">
		    			  </a>
		    			<% } %>              
                  </th>
                  <th class="item" scope="col"><isa:translate key="b2b.order.display.posno"/></th>
                  <th class="product" scope="col"><isa:translate key="b2b.order.display.productno"/></th>
                  <th class="qty" scope="col"><isa:translate key="b2b.order.display.quantity"/></th>
                  <th class="unit" scope="col"><isa:translate key="b2b.order.display.unit"/></th>
                  <th class="desc" scope="col"><isa:translate key="b2b.order.display.productname"/></th>
                  <% if (ui.isContractInfoAvailable()) { %>
                    <th class="ref-doc" scope="col"><isa:translate key="b2b.order.display.contract"/></th>
                  <% } %>
                  <% if (ui.isATPDisplayed()) { %>
                    <th class="qty-avail" scope="col"><isa:translate key="b2b.order.display.available"/></th>
                    <th class="date-on" scope="col"><isa:translate key="b2b.order.display.available.date"/></th>
                  <% } %>
                  <th class="price" scope="col"><isa:translate key="b2b.order.display.totalprice"/><br/><em><isa:translate key="b2b.order.details.price"/></em></th>
                  <th class="delete" scope="col"><img src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/trash_icon.gif")%>" alt="<isa:translate key="b2b.order.display.icon.delete"/>"/></th>
               </tr>
                
            <% String tag_style_detail = ""; 
			   String gridConfigType = ""; //that stores the product type to recognize grid products
            %>
            <isa:iterate id="item" name="<%= MaintainBasketBaseAction.RK_ITEMS %>"
                         type="com.sap.isa.businessobject.item.ItemSalesDoc">
            <% /* set the item in the ui class */
              ui.setItem(item);
              String itemKey = item.getTechKey().getIdAsString();
            /* ********** Check if the item is a BOM sub item to be supressed ****** */  
              if (!ui.isBOMSubItemToBeSuppressed()) { 
              tag_style_detail = "poslist-detail-" + ui.even_odd; 
              
            /* ********** Seperator between two items ********** */    
              if (!ui.itemHierarchy.isSubItem(item)) { %>
              <tr>
                <td colspan="11" class="separator"></td>
              </tr>
              <%gridConfigType = item.getConfigType();%>
           <% } %>
                       
           <% String substMsgColspan = "10"; %>
           <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>

               <tr  class="<%= ui.even_odd %>" id="row_<%= ui.line %>"
               <%
               	 // Hide the sub-items of the Grid Product
                 if (ItemSalesDoc.ITEM_CONFIGTYPE_GRID.equals(gridConfigType) &&
                     ui.itemHierarchy.isSubItem(item) ) {%>
               		style="display:none;"
               <%}%>
               >
                  <td class="opener">
                <% if (ui.showItemDetailButton()) { %>
                <%-- do not insert "details symbol" for configurable items on sub-levels --%>
                        <a class="icon" id="toggleItem_<%= ui.line %>" href='javascript: toggle("rowdetail_<%= ui.line %>")' >
						   <img class="img-1" id="rowdetail_<%= ui.line %>close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.acc" arg0="<%= item.getNumberInt()%>"/> <% } else { %><isa:translate key="b2b.order.details.item.close"/> <% } %>">
						   <img class="img-1" id="rowdetail_<%= ui.line %>open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.acc" arg0="<%= item.getNumberInt()%>"/> <% } else { %><isa:translate key="b2b.order.details.item.open"/> <% } %>">
						</a>
                <% } %>
                  </td>
                  <td <% if (ui.itemHierarchy.isSubItem(item)) { %> class="item-sub-<%= ui.getItemLevel() -1 %>" <% } else { %> class="" <% } %>>
                     <% if (ui.isElementVisible("order.item.posNo", itemKey)) { %>
                        <a href="#" name="item_<%=JspUtil.removeNull(item.getNumberInt())%>"></a>
                        <%= JspUtil.removeNull(item.getNumberInt()) %>
                        <input type="hidden" name="pos_no[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getNumberInt()) %>"/>
                     <% } %>
                  </td>
                  <td class="product">
		             <% if (ui.isElementVisible("order.item.product", itemKey)) { %>
						<% //The Grid product is non-editable after adding atleast one sub-item to it.
						   if (ui.isAuction() || !ui.isElementEnabled("order.item.product", itemKey) || ui.isItemFreeGoodExclusiveSubItem() ||
						       (ItemSalesDoc.ITEM_CONFIGTYPE_GRID.equals(gridConfigType) && ui.itemHierarchy.hasSubItems(item)) ){ %>
							<%= JspUtil.encodeHtml(ui.getProduct()) %><input type="hidden" name="product[<%= ui.line %>]" id="product[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getProduct()) %>" />
						<% } 
						   else { %>
							<input type="text" class="textinput-middle" name="product[<%= ui.line %>]" id="product[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getProduct()) %>"/>
							<% if (ui.isProductValuesSearchAvailable()) { %>
							  <%= HelpValuesSearchUI.getJSCallPopupCoding("Product","order_positions",""+ui.line,pageContext) %>
						    <% }   	    				
						   } %>
					  <% } %>
                  </td> 
                  <td class="qty">
                       <% if (ui.isElementVisible("order.item.qty", itemKey)) { %>
						   <%-- hidden field for oldquantity value --%>
						   <input type="hidden" name="oldquantity[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getOldQuantity()) %>"/>
	
						   <% //The Grid product's Quantity is non-editable after adding atleast one sub-item to it.
						   if ( ui.isAuction() || !ui.isElementEnabled("order.item.qty", itemKey) || ui.isItemFreeGoodExclusiveSubItem()  || 
								(ItemSalesDoc.ITEM_CONFIGTYPE_GRID.equals(gridConfigType) &&
								 ui.itemHierarchy.hasSubItems(item)) ){ %>
							<%= JspUtil.encodeHtml(item.getQuantity()) %><input type="hidden" name="quantity[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getQuantity()) %>"/>
							<% } else { %>
								<input type="text" class="textinput-small" name="quantity[<%= ui.line %>]" maxlength="8" value="<%= JspUtil.encodeHtml(item.getQuantity()) %>"/>
							<% } %>
					   <% } %>
                   </td>
                   <td class="unit">
                       <% if (ui.isElementVisible("order.item.unit", itemKey)) { %>
						  <% if (ui.units != null && !ui.isZeroProductId) { 
							   if (!ui.isAuction() && ui.isElementEnabled("order.item.unit", itemKey) && !ui.isItemFreeGoodExclusiveSubItem()){ 
								   if (ui.units.length==1)  { %>
									  <%= JspUtil.encodeHtml(ui.units[0]) %><input type="hidden" name="units[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(ui.units[0])%>"/>	
								   <% } else { %>
									  <select name="units[<%= ui.line %>]">
									    <% for (int j = 0; j < ui.units.length; j++) { %>
										   <option <%= ui.getItemUnitSelected(ui.units[j]) %> value="<%= JspUtil.encodeHtml(ui.units[j]) %>"><%= JspUtil.encodeHtml(ui.units[j]) %></option>
									    <% } %>
									  </select>
								   <% } %>
							   <% } else { %>
								   <%= JspUtil.encodeHtml(item.getUnit()) %><input type="hidden" name="units[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getUnit()) %>"/>
							   <% } %>
						  <% } else { %>
						     <%  if (ui.isElementEnabled("order.item.unit", itemKey)) { %>
	                            <input type="text" class="textinput-small" name="units[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getUnit()) %>"/>
	                         <% } else { %>
								<%= JspUtil.encodeHtml(item.getUnit()) %><input type="hidden" name="units[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getUnit()) %>"/>
	                         <% } %>
	                      <% } %>
	                   <% } %>
                   </td>
                   <td class="desc">
	                     <% if (ui.isElementVisible("order.item.description", itemKey)) { %>
	                        <% if (item.getDescription() != null && !"".equals(item.getDescription().trim())) { 
	                               if (ui.getShop().isInternalCatalogAvailable() && !TechKey.isEmpty(item.getProductId())) {%>
	                                 <a href="#" onclick="showproductincatalog('<%= item.getProductId().getIdAsString() %>');">
	                                    <%= JspUtil.encodeHtml(item.getDescription()).trim() %> 
	                                 </a>
	                            <% }
	                               else { %>
	                                  <%= JspUtil.encodeHtml(item.getDescription()).trim() %> 
	                           <% } %>
	                        <% } %>                            
	                    <% } %>
                        <%= ui.getSubstitutionReason() %>
                          <% if (ui.isCUAAvailable()) { %> 
                            &nbsp;<a href="#" class="icon" onclick="itemcua('<%= item.getProductId().getIdAsString() %>','<%=item.getTechKey().getIdAsString()%>', '<%= ui.header.getDocumentType() %>');">
                                     <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/cua_info.gif") %>" alt="<isa:translate key="b2b.order.display.icon.cuahint"/>"/>
                                  </a>
                          <% } %>
						 <% if (item.isConfigurable() && !(ui.itemHierarchy.isSubItem(item) && ui.isBackendR3()) && ui.isElementVisible("order.item.configuration", itemKey)) { 
								if (item.getConfigType() != null && 
									item.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)) {
										if (item.getExternalItem() != null) {%>
											<a href="#" onclick="griditemconfig('<%= item.getTechKey().getIdAsString() %>');"> 
												<img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_grid.gif") %>" 
												alt="<isa:translate key="b2b.order.display.grid"/>
												<% if (ui.isAccessible) { %><isa:translate key="b2b.order.display.grid"/> <% } %>" /> 
											</a>
									  <%}else{ %>
											<a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.grid.item.imp"/>');">
                                          		<img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_grid.gif") %>"
                                           		alt="<isa:translate key="b2b.order.display.grid"/>" />
                                        	</a>
									  <%}%>
	 					        <% }else {
	 					              if (ui.header.getIpcDocumentId() != null) { 
										  if (ui.isElementEnabled("order.item.configuration", itemKey)) { %>		 					                          
											<a href="#" class="icon" onclick="itemconfig('<%=item.getTechKey()%>');">
											  <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
											  alt="<isa:translate key="b2b.order.change.config.item"/>
											  <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.config.item"/> <% } %>" />
											</a>
									      <%}else{ %>	
											<a href="#" class="icon" onclick="itemconfig('<%=item.getTechKey()%>');">
											  <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
											  alt="<isa:translate key="status.sales.detail.config.item"/>
											  <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.config.item"/> <% } %>" />
											</a>									 	 
									      <%}%>									 	 
						 <% 	      } else { %>
						                <a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.config.imp"/>');">
                                          <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
                                           alt="<isa:translate key="status.sales.detail.config.item"/>" />
                                       </a>
						 <%         }
						       }
   							} %>
						<% if (ui.isItemCampaignAssignedAuto()) { %>
					      <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/camp_assigned.gif") %>" alt="<isa:translate key="b2b.disp.camp.ass"/>"/>
					    <% } %>
                        <%-- Batch Symbole --%>
                        <%-- hidden fields for batches - only when batch flag on shop level is enabled --%>
                        <isacore:ifShopProperty property = "batchAvailable" value = "true">
                            <% if (item.getBatchDedicated()) { %>
                                   &nbsp;
                                   <a href="#" onclick='javascript:openBatchDetails("rowdetail_<%=  ui.line %>"); setFocus("batchID[<%= ui.line%>]")'>
                                   <img id="batchDetails[<%= ui.line%>]" class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/batch2.gif") %>" alt="<isa:translate key="b2b.order.details.batch.dedicat"/> <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.button"/> <% } %>"/></a>
                            <% } %>
                        </isacore:ifShopProperty>
                        <%-- Batch Ende --%>
                   </td>
                  <% if (ui.isContractInfoAvailable()) { %>
                   <td class="ref-doc" id="ref-doc[<%= ui.line %>]">
                        <% if ((item.getContractId() != null) && !(item.getContractId().equals(""))) { %>    
                             <input type="hidden" name="detailContractKey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getContractKey().getIdAsString()) %>"/>
                             <input type="hidden" name="detailContractItemKey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getContractItemKey().getIdAsString()) %>"/>
                             <input type="hidden" name="contractRef[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getContractId()) %> / <%= JspUtil.removeNull(item.getContractItemId()) %>"/>                                              
                             <% if (ui.isElementVisible("order.item.contract", itemKey)) { %> 
                                <a href="<isa:webappsURL name="b2b/contractselect.do"/>?contractSelected=<%= item.getContractKey() %>"><%= JspUtil.encodeHtml(item.getContractId())%></a>&nbsp;/&nbsp;<%= JspUtil.encodeHtml(item.getContractItemId())%>
                                <% if (!ui.isBackendR3() && ui.isElementEnabled("order.item.contract", itemKey)) { %>
                                      <%--- deletion of contract reference --%>
                                      &nbsp; <a href='#' onclick="deletecontract('<%= ui.line %>');">
                                      <img src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/trash_icon.gif")%>" alt="<isa:translate key="b2b.order.icon.delete.cnt"/>"/>
                                <% } %>
                                </a>
                             <% } %>
                        <% } %>                                      
                   </td>
                  <% } %>
                  <% if (ui.isATPDisplayed()) { %>
	                   <td class="qty-avail">
	                       <% ArrayList aList = item.getScheduleLines();
	                          if (ui.showItemATPInfo() && ui.isElementVisible("order.item.schedulineQty", itemKey)) {
	                              for (int i = 0; i < aList.size(); i++) {
	                                  Schedline scheduleLine = (Schedline) aList.get(i); %>
	                               <%= JspUtil.removeNull(scheduleLine.getCommittedQuantity()) %><br/>
	                           <% }
	                           } %>
	                   </td>
	                   <td class="date-on">
	                      <% if (ui.showItemATPInfo()&& ui.isElementVisible("order.item.schedulineDate", itemKey)) {
	                             for (int i = 0; i < aList.size(); i++) {
	                               Schedline scheduleLine = (Schedline) aList.get(i); %>
	                               <%= JspUtil.removeNull(scheduleLine.getCommittedDate()) %><br/>
	                            <% }
	                         } %>
	                   </td>
                  <% } %>
                   <td class="price">
                   <% if (item.isPriceRelevant()) { 
                          if (!ui.isZeroProductId() && ui.isElementVisible("order.item.grossValue", itemKey)) { %>
                             <%= JspUtil.removeNull(ui.getItemValue()) + "&nbsp;" + JspUtil.encodeHtml(item.getCurrency()) %>
                          <% } 
                          if (ui.header.getIpcDocumentId() != null) { %>
                              <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
                              &nbsp;<a href="#" onclick="displayIpcPricingConds(  '<%= JspUtil.removeNull(ui.header.getIpcConnectionKey()) %>',
                                                                          '<%= JspUtil.removeNull(ui.header.getIpcDocumentId().getIdAsString()) %>',
                                                                          '<%= JspUtil.removeNull(ui.getIPCItemId(item)) %>'
                                                                          )"> <isa:translate key="b2b.order.display.ipcconds" /> </a>
                              </isacore:ifShopProperty>
                       <% } %>
                       <br/><em>
                       <% if (!ui.isZeroProductId() && ui.showNetPriceInfo() && ui.isElementVisible("order.item.netPrice", itemKey)) { %>
                          <%= JspUtil.removeNull(item.getNetPrice()) + "&nbsp;" + JspUtil.encodeHtml(item.getCurrency()) + "&nbsp;" + '/' + "&nbsp;" + JspUtil.encodeHtml(item.getNetQuantPriceUnit()) + "&nbsp;" + JspUtil.encodeHtml(item.getNetPriceUnit()) %>
                       <% } %>
					   </em>
                   <% } %>
                   </td>
                   <td class="delete">
                       <% if (!ui.isAuction() && item.isDeletable()) { %>
                          <input type="checkbox" name="delete[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"
                             <% if (ui.itemHierarchy.isSubItem(item) && item.isItemUsageATP()) { %>
                               onclick="if (document.forms['order_positions'].elements['quantity[<%= ui.line %>]'].value != 0) {document.forms['order_positions'].elements['quantity[<%= ui.line %>]'].value = 0;} else {document.forms['order_positions'].elements['quantity[<%= ui.line %>]'].value = document.forms['order_positions'].elements['oldquantity[<%= ui.line %>]'].value;}"
                             <% } %>
                           />
                       <% } %>
                        
                       <%-- start: hidden field section filled items --%>
                       <input type="hidden" name="itemTechKey[<%= ui.line %>]"       value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>
                       <input type="hidden" name="productkey[<%= ui.line %>]"        value="<%= JspUtil.removeNull(item.getProductId().getIdAsString()) %>"/>
                        
                       <%-- Hidden field optimization --%>
                       <% 
                       if(!(JspUtil.removeNull(item.getParentId().getIdAsString()).equals(""))) { %>
                          <input type="hidden" name="parentid[<%= ui.line %>]" value="<%= item.getParentId().getIdAsString() %>"/>
                       <% } 
                       if(!(item.getPcat()).equals("")) { %>
                           <input type="hidden" name="pcat[<%= ui.line %>]" value="<%= item.getPcat() %>"/>
                       <% } 
                       if(!(JspUtil.removeNull(item.getPcatVariant()).equals(""))) { %>
                           <input type="hidden" name="pcatVariant[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getPcatVariant()) %>"/>
                       <% }
                       if(!(JspUtil.removeNull(item.getPcatArea()).equals(""))) { %>
                           <input type="hidden" name="pcatArea[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getPcatArea()) %>"/>
                       <% }
                       if(item.isDataSetExternally()==true) { %>
                           <input type="hidden" name="setExt[<%= ui.line %>]" value="T"/>
                       <% } 
                       if(item.isConfigurable() ==true) { %>
                           <input type="hidden" name="configurable[<%= ui.line %>]" value="T"/>
                       <% } %>                           
                       <%-- end: hidden field section --%>
                   </td>
               </tr>

               <%-- Item Details filled items --%>
               <% if (ui.showItemDetailButton()) { %>
               <% tag_style_detail = ui.even_odd + "-detail"; %>
               <tr id="rowdetail_<%= ui.line %>" class="<%= tag_style_detail %>" <% if (ui.isToggleSupported()) { %> style="display:none;" <% } %> >
                 <td class="select">&nbsp;</td>
                 <td class="detail" colspan="<%= ui.getItemsColspan() %>">
                   <table class="item-detail">
                     <%@ include file="/b2b/proddetsysid_acc.inc.jsp" %>
                     <%-- display of product configuration --%>
                     <% if (ui.isElementVisible("order.item.configuration", itemKey)) { %>
                        <%@ include file="/ecombase/orderitemconfiginfo.inc.jsp" %>
                     <% } %>
                     <%-- Shipto --%>
                     <tr>
                       <% if (ui.isOciTransfer() || ui.getNumShipTos() == 0 || !ui.isElementVisible("order.item.deliverTo", itemKey)) { %>
                         <td class="identifier" colspan="<%= ui.getNonCampItemDetailColspan() + 1 %>">
                            <input type="hidden" id="customer[<%= ui.line %>]" name="customer[<%= ui.line %>]"/>
                         </td>
                       <% } else { %>
                         <td class="identifier">
                            <label for="customer[<%= ui.line %>]"><isa:translate key="b2b.order.display.soldto"/></label>
                         </td>
                         <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			             <td class="campimg-1"></td>
			             <% } %> 
                         <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">                           
                            <% if (! ui.isElementEnabled("order.item.deliverTo", itemKey)) { %>
                                <% if (item.getShipTo() != null) { %>
                                	<%= item.getShipTo().getShortAddress() %>
                                	<a href="#" class="icon" onclick="showShipToWithTechKey(<%= item.getShipTo().getTechKey() %>);" >
	                              	 <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
	                            	</a>
	                            <% } %>
                            <% } else { %>                           
	                            <select id="customer[<%= ui.line %>]" name="customer[<%= ui.line %>]" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.shipto.title"/>" <% } %> <%= (item.isItemUsageBOM())?"disabled='true'":"" %>>
	                               <isa:iterate id="shipTo"
	                                           name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
	                                           type="com.sap.isa.businessobject.ShipTo">
	                                        <option <%= ui.getItemShipToSelected(shipTo) %> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
	                                </isa:iterate>
	                            </select>&nbsp;
	                            <a href="#" class="icon" onclick="showShipTo(document.order_positions['customer[<%= ui.line %>]'].selectedIndex);" >
	                               <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" /></a>
	                            <% if (!item.isItemUsageBOM()) { %>
	                            <a href="#" class="icon" onclick="newShipToItem('<%= item.getTechKey() %>');">
	                               <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_new_green.gif") %>" alt="<isa:translate key="b2b.order.icon.newshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" /></a>
	                            <% } %>   
	                            <%//Multiple Shipto functionality available for ERP edition and for
	                              //GRID products only
	                              if (ui.isBackendR3() || ui.isBackendR3PI()) {
	                                  if (item.getConfigType() != null &&
								   	      item.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)) {%>
	                            	     <a href="#" class="icon" onclick="multipleShipTo('<%= item.getTechKey() %>');">
	                               	        <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_catalog_external.gif") %>" alt="<isa:translate key="b2b.order.icon.multishipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
	                               	     </a>
	                               <%    }
                                 }
                            } %>
                                 
                         </td>
                       <% } %>
                     </tr>
                     <%@ include file="/b2b/deliveryPriorityItem.inc.jsp" %> 
                     <%--  Request Delivery Date --%>     
                     <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey)) { %>
                        <tr>
	                       <td class="identifier">
						      <label for="reqdeliverydate[<%= ui.line %>]">
	                          <% if (ui.isBasket() || ui.isQuotation()) { %>
	                             <isa:translate key="b2b.order.reqdeliverydate"/>
	                          <% } %>
							  </label>
	                       </td>
                           <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			               <td class="campimg-1"></td>
			               <% } %> 
	                       <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
	                          <% if (ui.isBasket() || ui.isQuotation()) { %>
	                              <% if (!ui.isElementEnabled("order.item.reqDeliveryDate", itemKey)) { %>
	                                    <%= JspUtil.encodeHtml(item.getReqDeliveryDate()) %>
	                              <% } else { %>
			                            <input id="reqdeliverydate[<%= ui.line %>]" name="reqdeliverydate[<%= ui.line %>]" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.reqdeldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
			                                 type="text" class="textinput-middle" <%= (item.isItemUsageBOM())?"disabled='true'":"" %>
			                                 value="<%= JspUtil.encodeHtml(item.getReqDeliveryDate()) %>" />
								        <% if (!ui.isAccessible && !item.isItemUsageBOM()) { %>
			                              <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'reqdeliverydate[<%= ui.line %>]');">
					                         <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
										  </a>
									    <% } %>
									    <%//Multiple Requested Delv date functionality available for ERP edition 
										  //and for GRID products only
						  				  if (ui.isBackendR3() || ui.isBackendR3PI()) {
			                                  if ((item.getConfigType() != null &&
									  	   	       item.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)) &&
									  		       ui.itemHierarchy.hasSubItems(item)) {%>
			                           	         <a href="#" class="icon" onclick="multipleReqDlvDate('<%= item.getTechKey() %>');">
			                               	       <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/cua_info.gif") %>" alt="<isa:translate key="b2b.order.icon.multireqdate"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
			                                     </a>
			                                 <% }
						  				  }%>
					  			   <% } %>
					  			   
	                          <% } else { %>
	                               <input type="hidden" name="reqdeliverydate[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getReqDeliveryDate()) %>"/>                              
	                          <% } %>
	                       </td>
                         </tr>
	                 <% } %>
                     <%--  Cancel Date --%>
                     <% if (ui.isElementVisible("order.latestDeliveryDate")){ %>
                     <tr>
                       <td class="identifier">
					      <label for="latestdlvdate[<%= ui.line %>]">
                          <% if (ui.isBasket() || ui.isQuotation()) { %>
                             <isa:translate key="b2b.order.grid.canceldate"/>
                          <% } %>
						  </label>
                       </td>
                       <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			           <td class="campimg-1"></td>
			           <% } %> 
                       <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
                          <% if (ui.isBasket() || ui.isQuotation()) { %>
                             <% if (!ui.isElementEnabled("order.latestDeliveryDate")) { %>
                                <%= JspUtil.encodeHtml(item.getLatestDlvDate()) %>
                             <% } else { %>
	                            <input id="latestdlvdate[<%= ui.line %>]" name="latestdlvdate[<%= ui.line %>]" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.canceldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
	                                 type="text" class="textinput-middle" <%= (item.isItemUsageBOM())?"disabled='true'":"" %>
	                                 value="<%= JspUtil.encodeHtml(item.getLatestDlvDate()) %>" />
						        <% if (!ui.isAccessible && !item.isItemUsageBOM()) { %>
	                              <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'latestdlvdate[<%= ui.line %>]');">
			                         <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
								  </a>
						        <% } %>
						     <% } %>
                          <%} else { %>
                               <input type="hidden" name="latestdlvdate[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getLatestDlvDate()) %>"/>
                          <% } %>
                       </td>
                     </tr>
                     <% } %>
                     <%-- external reference objects --%>
                     <%@ include file="/b2b/extrefobjectitem.inc.jsp" %>                      
                     <%-- external reference numbers --%>
                     <%@ include file="/b2b/extrefnumberitem.inc.jsp" %>                      
                     <%-- campaigns --%>             
					 <%@ include file="/b2b/campaignsItem.inc.jsp" %>
					 <%-- payment terms --%>
			         <% if (ui.showItemPaymentTerms() && ui.isElementVisible("order.item.paymentterms", itemKey)) { %>
			            <tr>
			            	<td class="identifier" scope="row"><isa:translate key="b2b.order.disp.paymentterms"/></td>
                            <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			                <td class="campimg-1"></td>
			                <% } %> 
			            	<td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>"><%=JspUtil.encodeHtml(item.getPaymentTermsDesc()) %></td>
			            </tr>
			         <% } %>      					 
                     <%-- notes --%>
                     <% if (ui.isElementVisible("order.item.comment", itemKey)) { %>
	                     <tr class="message-data">
	                        <td class="identifier">
	                            <label for="comment[<%= ui.line %>]"><isa:translate key="b2b.order.details.note"/></label>
	                        </td>
                            <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			                <td class="campimg-1"></td>
			                <% } %> 
	                        <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
	                        <% if (ui.isElementEnabled("order.item.comment", itemKey)) { %>   
	                            <textarea id="comment[<%= ui.line %>]" name="comment[<%= ui.line %>]" cols="65" rows="2" ><%= JspUtil.encodeHtml(item.getText().getText(), new char[] {'\n'}) %></textarea>
	                        <% } else { %>
	                            <%= JspUtil.encodeHtml(item.getText().getText()) %>
	                        <% } %>
	                        </td>
	                     </tr>
                     <% } %>

                  <%--*********************** Start of Batch Input - Item Details ********--%>
                  <%-- detail fields for batches - only when batch flag on shop level is enabled --%>
                  <isacore:ifShopProperty property = "batchAvailable" value = "true">
                  <% if(item.getBatchDedicated()){%>
                     <tr>
                        <input type="hidden" name="batchDedicated[<%= ui.line %>]" value="<%= item.getBatchDedicated() %>"/>
                        <td class="identifier" colspan="<%= ui.getNonCampItemDetailColspan() + 2 %>"><isa:translate key="b2b.order.details.batch"/></td>
                     </tr>
                     <tr>
                        <% if (ui.isElementVisible("order.item.batchId", itemKey)) { %>
	                        <td class="identifier">
	                            <label for="batchID[<%= ui.line %>]"><isa:translate key="b2b.order.details.batch.number"/></label>
	                        </td>
	                        <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			                <td class="campimg-1"></td>
			                <% } %> 
	                        <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
	                            <input class="textInput-small" name="batchID[<%= ui.line %>]" id="batchID[<%= ui.line %>]" maxlength="12" value="<%= JspUtil.encodeHtml(item.getBatchID()) %>"/>
	                            <% if(item.getBatchOptionNum() != 0) { %>
	                                  <select name="batchOption[<%= ui.line %>]" onchange='javascript:document.forms["order_positions"].elements["batchID[<%= ui.line%>]"].value = this.value; this.value=""'>
	                                      <option value=""><isa:translate key="b2b.order.details.batch.list"/></option>
	                                  <% for (int i=0; i < item.getBatchOptionNum(); i++) { %>
	                                      <option value="<%= JspUtil.encodeHtml(item.getBatchOption(i)) %>"><%= JspUtil.encodeHtml(item.getBatchOption(i)) %> - <%= JspUtil.encodeHtml(item.getBatchOptionTxt(i)) %></option>
	                                  <% } %>
	                                  </select>
	                            <% } %>
	                        </td>
	                     <% } %>
                     </tr>
                     <tr>
                        <% if (ui.isElementVisible("order.item.batchChar", itemKey)) { %>
	                        <td class="identifier" colspan="<%= ui.getNonCampItemDetailColspan() + 2 %>">
	                        <% if(item.getBatchClassAssigned()) {%>
	                             <input type="hidden" name="batchClassAssigned[<%= ui.line %>]" value="<%= item.getBatchClassAssigned() %>"/>
	                             <input type="hidden" name="batchSelected[<%= ui.line %>]" value=""/>
	                             <a href="#" onclick='javascript:submit_batch_selection("<%= item.getTechKey().getIdAsString() %>", "<%= item.getProductId().getIdAsString() %>", "<%= JspUtil.encodeHtml(item.getProduct()) %>", "batchSelected[<%= ui.line %>]")'>
	                                 <isa:translate key="b2b.order.details.batch.values"/>
	                             </a>
								 <%if( item.getBatchCharListJSP().size() == 0) { %>
									 [<isa:translate key="b2b.order.dt.batch.values.false"/>]
								 <%}
								   else { %>
									 [<isa:translate key="b2b.order.dt.batch.values.true"/>]
								 <%}%>
	                             <%-- Batch element info start --%>
	                             <input type="hidden" name="elementCount[<%= ui.line %>]" value="<%= item.getBatchCharListJSP().size() %>"/>
	                         <% for(int i=0; i<item.getBatchCharListJSP().size(); i++) { %>
								 <input type="hidden" name="elementName[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharName()) %>"/>
								 <input type="hidden" name="elementTxt[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharTxt()) %>"/>
								 <input type="hidden" name="elementAddVal[<%= ui.batchCountTotal %>]" value="<%= item.getBatchCharListJSP().get(i).getCharAddVal() %>"/>
								 <input type="hidden" name="elementUnit[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharUnit()) %>"/>
								 <input type="hidden" name="elementUnitTExt[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharUnitTExt()) %>"/>
								 <input type="hidden" name="elementDataType[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharDataType()) %>"/>
								 <input type="hidden" name="elementCountCBs[<%= ui.batchCountTotal %>]" value="<%= item.getBatchCharListJSP().get(i).getCharValTxtNum() %>"/>
						       <% for (int j=0; j<item.getBatchCharListJSP().get(i).getCharValTxtNum(); j++) { %>
	                             <input type="hidden" name="elementCharVal[<%= ui.batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharVal(j)) %>"/>
	                             <input type="hidden" name="elementCharValTxt[<%= ui.batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharValTxt(j)) %>"/>
	                             <input type="hidden" name="elementCharValMax[<%=ui.batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharValMax(j)) %>"/>
	                             <% ui.batchCountCheckBox++;
	                              } %>
	                           <% if (item.getBatchCharListJSP().get(i).getCharAddVal() == true || (item.getBatchCharListJSP().get(i).getCharValTxtNum() == 0 && item.getBatchCharListJSP().get(i).getCharDataType().equals("CHAR"))) { %>
	                             <input type="hidden" name="characteristicAddVal[<%= ui.batchCountTotal %>]" value=""/>
	                           <% } %>
	            
	                           <% if (item.getBatchCharListJSP().get(i).getCharValTxtNum() == 0 && item.getBatchCharListJSP().get(i).getCharDataType().equals("NUM")) { %>
	                             <input type="text" class="textInput" name="characteristicFrom[<%= ui.batchCountTotal %>]" value=""/>
	                             <input type="text" class="textInput" name="characteristicTo[<%= ui.batchCountTotal %>]"  value=""/>
	                           <% } 
	                              ui.batchCountTotal++; 
	                           } %>
	                           <%-- Batch element info end --%>
	                        <% } %>
	                        </td>
	                     <% } %>
                      </tr>
                    <% } %>
                    </isacore:ifShopProperty>
                    <%--*******End of Batch Input - Item Details ***********--%>
                   </table>
                 </td>
              </tr>
              <%--   messages --%>
              <%@ include file="/b2b/itemErrMsg.inc.jsp" %>
          <% } %> <%-- ui.showItemDetailButton() --%>    
          <% } %> <%-- ui.isBOMSubItemToBeSuppressed() --%>	
             
         </isa:iterate>
        <%-- End document items--%>
        <%-- Add some empty lines for additional basket-data --%>
         <% int firstEmptyLine = ui.line + 1;
            if (!ui.isServiceRecall()) {
              for (int i = 0; i < ui.newpos; i++) { 
                 ui.setEmptyItem(); %>
             <%-- ********** Seperator between two items ********** --%>      
             <tr>
                <td colspan="11" class="separator"></td>
             </tr>
             <tr id="row_<%= ui.line %>" class="<%=ui.even_odd %>">
              <td class="opener">
                <% if (ui.isToggleSupported()) { %> 
                     <a id="toggleItem_<%= ui.line %>" href='javascript: toggle("rowdetail_<%= ui.line %>")' class="icon">
                        <img class="img-1" id="rowdetail_<%= ui.line %>close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.order.details.item.close"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.button"/> <% } %>"/>
						<img class="img-1" id="rowdetail_<%= ui.line %>open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.order.details.item.open"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.button"/> <% } %>"/></a>
                 <% } else {%>
                      &nbsp;
                 <% } %>
              </td>
              <td class="item">&nbsp;</td>
              <td class="product">
                 <% if (ui.isElementVisible("order.item.product") && ui.isElementEnabled("order.item.product")) { %>
	                 <input type="text" class="textinput-middle" name="product[<%= ui.line %>]" id="product[<%= ui.line %>]" value=""/>
					 <% if (ui.isProductValuesSearchAvailable()) { %>
					   <%= HelpValuesSearchUI.getJSCallPopupCoding("Product","order_positions",""+ui.line,pageContext) %>
		  			 <% } %> 
		  		  <% } %> 	    				                 
              </td>
              <td class="qty">
                 <% if (ui.isElementVisible("order.item.qty")&& ui.isElementEnabled("order.item.qty")) { %>
                    <input type="text" class="textinput-small" name="quantity[<%= ui.line %>]" maxlength="8" value=""/>
                 <% } %>
              </td>
              <td class="unit">
              <% if (!ui.isAuction() && ui.isElementVisible("order.item.unit") && ui.isElementEnabled("order.item.unit")) { %>
				 <input type="text" class="textinput-small" name="units[<%= ui.line %>]" value=""/>
              <% } %>
              </td>
              <td class="desc">&nbsp;</td>
          <% if (ui.isContractInfoAvailable()) { %>
              <td class="ref-doc">&nbsp;</td>
          <% } %>
          <% if (ui.isATPDisplayed()) { %>
              <td class="qty-avail">&nbsp;</td>
              <td class="date-on">&nbsp; </td>
          <% } %>
              <td class="price">&nbsp;</td>
              <td class="delete">&nbsp;</td>
          </tr>
          <%-- item detail empty lines --%>
          <% tag_style_detail = ui.even_odd + "-detail"; %>
          <tr id="rowdetail_<%= ui.line %>" class="<%= tag_style_detail %>" <% if (ui.isToggleSupported()) { %> style="display:none;" <% } %> >
                <td class="select">&nbsp;</td>
                <td class="detail" colspan="<%= ui.getItemsColspan() %>">
                   <%-- Details --%>
                   <table class="item-detail">                    
                      <%-- Shipto --%>
                      <tr>
                        <% if (ui.isOciTransfer() || ui.getNumShipTos() == 0 || !ui.isElementVisible("order.item.deliverTo")) { %>
                            <td class="value">
                                <input id="customer[<%= ui.line %>]" type="hidden" name="customer[<%= ui.line %>]"/>
                            </td>
                        <% } else { %>
                            <td class="identifier">
                                 <label for="customer[<%= ui.line %>]"><isa:translate key="b2b.order.display.soldto"/></label>
                            </td>
                            <% if (ui.isMultipleCampaignsAllowed()) { %>
                            <td class="campimg-1"></td>
                            <% } %>  
                            <td class="value">
                                <% if ( ui.isElementEnabled("order.item.deliverTo")) { %>                                                                
	                                <select id="customer[<%= ui.line %>]" name="customer[<%= ui.line %>]" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.shipto.title.emp"/>" <% } %> >
	                                    <isa:iterate id="shipTo"
	                                         name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
	                                         type="com.sap.isa.businessobject.ShipTo">
	                                       <option <%= ui.getShipToSelected(shipTo) %> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
	                                    </isa:iterate>
	                                </select>&nbsp;
	                                <a href="#" class="icon" onclick="showShipTo(document.order_positions['customer[<%= ui.line %>]'].selectedIndex);" >
	                                    <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
	                                </a>
	                            <% } else { %>
                                   <%= ui.header.getShipTo().getShortAddress() %>
                                   <a href="#" class="icon" onclick="showShipToWithTechKey(<%= ui.header.getShipTo().getTechKey() %>);" >
	                                  <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
	                               </a>	                                
	                            <% } %>
                            </td>
                        <% } %>
                      </tr>                      
					  <%@ include file="/b2b/deliveryPriorityNewItem.inc.jsp" %>
					  <%--  Request Delivery Date --%>
	                  <% if (ui.isElementVisible("order.item.reqDeliveryDate") && ui.isElementEnabled("order.item.reqDeliveryDate")) { %>
	                      <tr>
	                        <td class="identifier">
	                          <% if (ui.isBasket() || ui.isQuotation()) { %>
	                            <label for="reqdeliverydate[<%= ui.line %>]"><isa:translate key="b2b.order.reqdeliverydate"/></label>
	                          <% } %>
	                        </td>
                            <% if (ui.isMultipleCampaignsAllowed()) { %>
                            <td class="campimg-1"></td>
                            <% } %>  
	                        <td class="value">
	                          <% if (ui.isBasket() || ui.isQuotation()) { %>
	                             <input type="text" class="textinput-middle" id="reqdeliverydate[<%= ui.line %>]" name="reqdeliverydate[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %>" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.reqdeldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %> />
							   <% if (!ui.isAccessible) { %>
	                             <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'reqdeliverydate[<%= ui.line %>]');">
			                         <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
								 </a>
	                          <%  }
							     } %>
	                        </td>
	                      </tr>
	                    <% } %>
                      <%--  Cancel Date --%>
                      <% if (ui.isElementVisible("order.latestDeliveryDate") && ui.isElementEnabled("order.latestDeliveryDate")){ %>
                      <tr>
                        <td class="identifier">
                          <% if (ui.isBasket() || ui.isQuotation()) { %>
                            <label for="latestdlvdate[<%= ui.line %>]"><isa:translate key="b2b.order.grid.canceldate"/></label>
                          <% } %>
                        </td>
                        <% if (ui.isMultipleCampaignsAllowed()) { %>
                        <td class="campimg-1"></td>
                        <% } %>  
                        <td class="value">
                          <% if (ui.isBasket() || ui.isQuotation()) { %>
                             <input type="text" class="textinput-middle" id="latestdlvdate[<%= ui.line %>]" name="latestdlvdate[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(ui.header.getLatestDlvDate()) %>" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.canceldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %> />
						   <% if (!ui.isAccessible) { %>
                             <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'latestdlvdate[<%= ui.line %>]');">
		                         <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
							 </a>
                          <%  }
						     } %>
                        </td>
                      </tr>
                      <%}%>
                      <%-- external reference numbers --%>
                      <%@ include file="/b2b/extrefnumbernewitem.inc.jsp" %> 
                      <%-- campaigns --%>                        
					  <%@ include file="/b2b/campaignsNewItem.inc.jsp" %>
                      <%-- notes --%>
                      <% if (ui.isElementVisible("order.item.comment") && ui.isElementEnabled("order.item.comment")) { %>
	                      <tr class="message-data">
	                        <td class="identifier">
	                            <label for="comment[<%= ui.line %>]"><isa:translate key="b2b.order.details.note"/></label>
	                        </td>
                            <% if (ui.isMultipleCampaignsAllowed()) { %>
                            <td class="campimg-1"></td>
                            <% } %>  
	                        <td class="value">
	                            <textarea id="comment[<%= ui.line %>]" name="comment[<%= ui.line %>]" rows="2" cols="65"></textarea>
	                        </td>
	                      </tr>
	                  <% } %>
                    </table>
                    <% if (!ui.isOrderingOfNonCatalogProductsAllowed()) { %>
                           <input type="hidden" name="pcat[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(ui.getCatalog()) %>" />
                    <% } %>
                </td>
             </tr>
          <% } /* end for */
           } %>
    </table>
	<% if (ui.isAccessible) { %>
		  <a name="end-table10" title="<isa:translate key="b2b.acc.items.end"/>"></a>
	<% } %>

         <input type="hidden" name="sendpressed" value=""/>
         <input type="hidden" name="cancelpressed" value=""/>
         <input type="hidden" name="showproduct" value=""/>
         <input type="hidden" name="actualpos" value="1"/>
         <input type="hidden" name="deletekey" value=""/>
         <input type="hidden" name="refresh" value=""/>
         <input type="hidden" name="newpos" value=""/>
         <input type="hidden" name="newshipto" value=""/>
         <input type="hidden" name="processsoldto" value=""/>
         <input type="hidden" name="simulatepressed" value=""/>
         <input type="hidden" name="doctype" value="<%= ui.getDocType() %>"/>
         <input type="hidden" name="processtype" value=""/>
         <input type="hidden" name="configitemid" value=""/>

         <%-- hidden fields for grid configuratoin --%>
         <input type="hidden" name="configtype" value=""/>
         <input type="hidden" name="multipleshipto" value=""/>
         <input type="hidden" name="multiplereqdlvdate" value=""/>         

       <%-- Hidden field optimization --%>
       <%-- hidden fields for batches - only when batch product exists --%>
       <isacore:ifShopProperty property = "batchAvailable" value = "true">
         <input type="hidden" name="batchselection" value=""/>
         <input type="hidden" name="batchproductid" value=""/>
         <input type="hidden" name="batchproduct" value=""/>
       </isacore:ifShopProperty>
        
       <%-- hidden fields for UpSelling - only when UpSelling activated --%>
       <isacore:ifShopProperty property = "cuaAvailable" value = "true"/>
         <input type="hidden" name="cuaproductkey" value=""/>
         <input type="hidden" name="cuareplaceditem" value=""/>
         <input type="hidden" name="cuadocumenttype" value=""/>
         <input type="hidden" name="replaceditem" value=""/>
         <input type="hidden" name="replacewithproduct" value=""/>
         <input type="hidden" name="replacewithunit" value=""/>
         <input type="hidden" name="replacewithqunatity" value=""/>
       </isacore:ifShopProperty>
      </form>
      </div> <%-- document-items --%>
    </div> <%-- document --%>

    <%-- Buttons --%>
    <div id="buttons">
        <a id="access-buttons" href="#access-header" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>
    
        <ul class="buttons-1">
          <% if (!ui.isAuction() && !ui.isServiceRecall()) { %>
            <li>
                <select id="newposcount" size="1" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.newpos.qt"/>" <% } %> >
                  <option <%= ui.getNewPosSelected(5) %> value="5">5 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(10) %> value="10">10 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(15) %> value="15">15 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(20) %> value="20">20 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(25) %> value="25">25 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(30) %> value="30">30 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(40) %> value="40">40 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(50) %> value="50">50 <isa:translate key="b2b.newpos"/></option>
                </select>
              <% if (!ui.isAccessible) { %>
                <img class="icon" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.acc.buttons.newpos.qt"/>"/>
              <% } %>
            </li>
          <% } 
             if (!ui.isAuction()) { %>
            <li><a href="#" onclick="submit_refresh();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.upd"/>" <% } %> ><isa:translate key="b2b.order.display.submit.update"/></a></li>
          <% } %>
        </ul>
        <ul class="buttons-3">
            <li><a href="#" onclick="cancel_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cncl"/>" <% } %> ><isa:translate key="b2b.order.display.submit.cancel"/></a></li>
            <% if (ui.isOciTransfer() && ui.isBasket() && (!ui.isBackendR3())) { %>
                  <li><a href="#" onclick="send_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.snd"/>" <% } %> ><isa:translate key="b2b.ord.sub.send.oci"/></a></li>
            <% } 
               else %>
               <li>
               <% if (ui.isLateDecision && ui.isBasket()) {
                   if ((ui.getShop().isQuotationAllowed() && ui.getShop().isTemplateAllowed()) ||
                       (ui.getNumProcessTypesOrder() > 1 || ui.getNumProcessTypesQuotation() > 1)) { %>
                          <select id="simulSel" size="1" onchange="eval(document.getElementById('simulSel').options[document.getElementById('simulSel').selectedIndex].value);" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.sim"/>" <% } %> >
                             <option value="nothing"><isa:translate key="b2b.ordr.simul.b2r.sel"/></option> 
                             <% if (ui.getNumProcessTypesOrder() > 1 && ui.hasCreateOrderPermission()) { %>
                                <isa:iterate id="processTypesIterat" name="<%= ShopReadAction.PROCESS_TYPES %>" type="com.sap.isa.core.util.table.ResultData">                       
                                   <option value="submit_simulate('order', '<%= processTypesIterat.getString(1) %>')"><%= processTypesIterat.getString(2) %></option>
                                </isa:iterate>
                             <% } 
                                else if ( ui.hasCreateOrderPermission()) { %>
                                   <option value="submit_simulate('order','<%= ui.getProcessTypeOrder() %>')"><isa:translate key="b2b.ordr.simul.b2r.sel.ord"/></option>
                             <% } %>
                             <% if (ui.getShop().isQuotationAllowed()&& ui.hasCreateQuotPermission() ) { 
                                    if (ui.getNumProcessTypesQuotation() > 1) {%>
                                    <isa:iterate id="processTypesIterat" name="<%= (ui.getShop().isQuotationExtended()) ? ShopReadAction.QUOTATION_PROCESS_TYPES : ShopReadAction.PROCESS_TYPES %>" type="com.sap.isa.core.util.table.ResultData"> 
                                        <option value="submit_simulate('quotation', '<%= processTypesIterat.getString(1) %>')"><%= processTypesIterat.getString(2) %></option>
                                    </isa:iterate> 
                                 <% } 
                                    else { %>
                                      <option value="submit_simulate('quotation','<%= ui.getProcessTypeQuotation() %>')"><isa:translate key="b2b.ordr.simul.b2r.sel.quo"/></option>
                                 <% }
                                } %>
                             <% if (ui.getShop().isTemplateAllowed() && ui.hasCreateTempPermission() ) { %>
                             <option value="submit_simulate('ordertemplate')"><isa:translate key="b2b.ordr.simul.b2r.sel.tpl"/></option>
                             <% } %>
                          </select> 
                    <% } 
                       else { %>
                        <% if ( ui.hasCreateOrderPermission()) { %>
                           <li><a href="#" onclick="submit_simulate('order','<%= ui.getProcessTypeOrder() %>');" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.sim.ord"/>" <% } %> ><isa:translate key="b2b.ordr.disp.sub.simul.b2r.ordr"/></a></li>
                        <% }
                           if (ui.getShop().isQuotationAllowed() && ui.hasCreateQuotPermission()) { %>
                           <li><a href="#" onclick="submit_simulate('quotation','<%= ui.getProcessTypeQuotation() %>');" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.sim.quot"/>" <% } %> ><isa:translate key="b2b.order.disp.sub.simul.b2r.quo"/></a></li>
                        <% } 
                           if (ui.getShop().isTemplateAllowed() && ui.hasCreateTempPermission()) { %>
                           <li><a href="#" onclick="submit_simulate('ordertemplate');" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.sim.templ"/>" <% } %> ><isa:translate key="b2b.ordr.disp.sub.simul.b2r.templ"/></a></li>
                        <% }
                       } 
                   } 
                   else {
                       if (!ui.isBackendR3()) {
                           if (ui.isBasket() && (ui.getShop().isOrderSimulateAvailable() || ui.isExternalToOrder())
                                 && ui.hasCreateOrderPermission()) { %>
                               <li><a href="#" onclick="submit_simulate('order','<%= ui.getProcessTypeOrder() %>');" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.sim.ord"/>" <% } %> ><isa:translate key="b2b.order.disp.sub.simul"/></a></li>
                        <% } 
                           else { %>
                               <li><a href="#" onclick="send_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.snd"/>" <% } %> ><isa:translate key="<%= ui.getSendKey() %>"/></a></li>
                        <% } 
                       } 
                       else { // R/3 case %>                 
					        <li><a href="#" onclick="send_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.sim.ord"/>" <% } %> ><isa:translate key="<%= ui.getSendKey() %>"/></a></li>
                    <% } 
                   } %>
               </li>
        </ul>
    </div>
    <%-- End  Buttons--%>
    <div id="_isa_Calendar_Control" class="calendarcontrol" style="display:none;z-index:99999;width:250;height:200;position:absolute;">
    </div>
    <script language="Javascript">
       headerReqDelDateOld = '<%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate())%>';
    </script>
  </body>
</html>
