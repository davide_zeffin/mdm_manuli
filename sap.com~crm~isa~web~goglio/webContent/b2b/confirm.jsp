<%--
********************************************************************************
    File:         confirm.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:
    Created:      14.10.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/10/14 $
********************************************************************************
--%>

<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>


<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.businesspartner.backend.boi.PartnerFunctionData" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.ConfirmationUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uicontrol.UIController" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<isa:contentType />
<isacore:browserType/>

<%
    ConfirmationUI ui = new ConfirmationUI(pageContext);

    int contractcolumn = ui.isContractInfoAvailable() ? 1 : 0;

    boolean resetMiniBasket = true;

    if ((String)request.getAttribute(ShowOrderConfirmedAction.RK_RESET_MINBASKET) != null  &&
        ((String)request.getAttribute(ShowOrderConfirmedAction.RK_RESET_MINBASKET)).equals("TRUE")) {
         resetMiniBasket = false;
    }

    String inputValue = null;
    //inline display of product configuration: extended view
    boolean showDetailView = true;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="b2b.order.display.pagetitle"/></title>

    <isa:stylesheets/>

          <script type="text/javascript">
              <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
          </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
              type="text/javascript">
      </script>      
      <script type="text/javascript">

          // function for the onload()-event
          function loadPage() {
              <% // When ordering a quotation, the current basket is not to reset
               if (resetMiniBasket) {%>
              // sets minibasket information back to default (Transaction in Process)
              // This has to be done, because the Minibasket link (x Items of value y) suggest 
              // that if one clicks on it, the basket is displayed.
              // But this is not the case because the basket is no longer available.
                  isaTop().header.setBasketLinkText("<isa:translate key="b2b.header.minibasket.default"/>");
              <% } %>
          }

          function send_oci() {
              isaTop().location.href = "<isa:webappsURL name="/b2b/ocilinessend.do"/>";
          }

          function updateDocumentView() {
              location.href = "<isa:webappsURL name="b2b/updatedocumentview.do"/>";
          }
      </script>

  </head>

  <body class="confirmation" onload="loadPage();">
    <div class="module-name"><isa:moduleName name="b2b/confirm.jsp" /></div>
    <% if (!ui.isPrintOrderStatus) { %>
      <h1><span><isa:translate key="b2b.order.summary.confirm2.order"/></span></h1>
    <%
    }
    else {%>
      <h1><span><isa:translate key="b2b.order.summary.confirm2.order"/></span></h1>
    <%
    }%>

    <div id="document">
    <%-- Show accessibility messages --%>
        <% if (ui.isAccessible) { %>
                <%@ include file="/appbase/accessibilitymessages.inc.jsp"  %>
        <% } %>
      <div class="document-header">
      <%-- Header table --%>
      <a id="access-header" href="#access-items" title= "<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>
      <% if (ui.isAccessible) { %>
         <a href="#end-table1" title="<isa:translate key="ecm.acc.header.dat.sum"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="11"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
      <% } %>
        <div class="header-basic">
          <% if (!ui.isPrintOrderStatus) {
               if (ui.isOrderTemplate()) { %>
               <div>
               <% if (ui.isChangeMode()) { %>
                    <isa:translate key="b2b.ordtmpl.sum.conf.quotation"/>
               <% }
                  else { %>
                    <isa:translate key="b2b.ordertempl.sum.receipted"/>
               <% } %>
               </div>
            <% }
                           else if (ui.isQuotation()) {
                                if (ui.header.isQuotationExtended()) { %>
                        <div><isa:translate key="b2b.qt.summary.confirm.qt.x"/></div>
                        <div>
                        <% if (ui.isChangeMode()) { %>
                             <isa:translate key="b2b.qt.sum.changereceipted.x1"/>
                        <% }
                           else { %>
                             <isa:translate key="b2b.qt.summary.receipted.x1"/>
                        <% } %>
                             <isa:translate key="<%= ui.getDocumentStatusKey()%>"/>
                        <% if (!ui.header.isStatusCancelled()) { %>
                             <isa:translate key="b2b.qt.summary.receipted.x2"/></div>
                        <div><isa:translate key="b2b.quotation.summary.order.x1"/>
                             <isa:translate key="status.sales.status.released"/>
                             <isa:translate key="b2b.quotation.summary.order.x2"/></div>
                        <% } else { %>
                             </div>
                        <% } %>
                   <% } else { %>
                        <div>
                        <% if (ui.isChangeMode()) { %>
                             <isa:translate key="b2b.qt.sum.changereceipted"/>
                        <% }
                           else { %>
                             <isa:translate key="b2b.quotation.summary.receipted"/>
                        <% } %>
                        </div>
                        <div><isa:translate key="b2b.quotation.summary.order"/></div>
              <%      }
               }
                           else { // we assume it is an order %>
                   <div><isa:translate key="b2b.order.summary.thanks"/></div>
                   <div>
                <% if (ui.isChangeMode()) { %>
                        <isa:translate key="b2b.order.sum.changereceipted"/>
                <% }
                   else { %>
                        <isa:translate key="b2b.order.summary.receipted"/>
                <% } %>
                   </div>
            <% }
             } %>
        <div class="line"></div>
        <table class="layout">
                    <tr>
                        <td class="col1">
                            <table class="header-general" summary="<isa:translate key="ecm.acc.header.dat.sum"/>">
                        <tr class="title">
                                <td class="identifier"><%= ui.getHeaderDescription() %>:</td>
                                <%//NOTE 1081103 
                                  if ( ui.getDocDate() != null && ui.getDocDate().equals("") == false ) {%>
                                	<td class="value"><%= ui.getDocNumber() %>&nbsp;<isa:translate key="b2b.docnav.from"/>&nbsp;<%= ui.getDocDate() %></td>
                                <%}
                                  else{
                                    // if we don't have data the DocData we don't display the "from"-String %>
                                    <td class="value"><%= ui.getDocNumber() %></td>
                                <%} //END NOTE 1081103 %>
                        </tr>
                        <% if (ui.isServiceRecall()) { %>                         
                           <% if (ui.isElementVisible("order.recallId")){ %>
                              <tr>
                                 <td class="identifier"><isa:translate key="b2b.order.disp.recall"/></td>
                                 <td class="value"><%= ui.header.getRecallId() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getRecallDesc()) %></td>
                              </tr>
                           <% } %>
                           <% if (ui.isElementVisible("order.extRefObjects")){ %>
                              <% if (ui.header.getExtRefObjectType() != null &&  ui.header.getExtRefObjectType().length() > 0) { %>
                                 <tr>
                                    <td class="identifier"><%= JspUtil.encodeHtml(ui.header.getExtRefObjectTypeDesc()) %>:</td>
                                    <td class="value">
                                        <%  ExtRefObjectList extRefObjects = ui.header.getAssignedExtRefObjects();
                                        if ( extRefObjects != null && extRefObjects.size() > 0) {
                                            for (int j=0; j < extRefObjects.size(); j++) { %>
                                               <%= JspUtil.encodeHtml(extRefObjects.getExtRefObject(j).getData()) %>
                                               <% if (j < extRefObjects.size()-1) { %>;<% } %>
                                            <% }
                                        } %>
                                    </td>
                                 </tr>
                              <% } %>   
                           <% } %>
                        <% } %>
                        <%-- render the consumer, only in B2R scenario --%>
                        <% if (ui.isSoldToVisible() && ui.isElementVisible("order.soldTo")) { %>
	                        <tr  class="title">
	                            <td class="identifier"><isa:translate key="status.sales.consumer.name"/> (<isa:translate key="status.sales.consumer.no"/>):</td>
	                            <td class="value"><%= JspUtil.removeNull(ui.getSoldToName()) %> (<%= JspUtil.removeNull(ui.getSoldToId()) %>)</td>
	                        </tr>
                        <% } %>
                        <% if (ui.isCompPFReseller() && ui.isElementVisible("order.reseller")) { %>
                                    <%@ include file="/b2b/resellerinfo_acc.inc.jsp" %>
                        <% } %>
                        <% if (ui.isElementVisible("order.numberExt")) { %>
	                        <tr class="title">
	                            <td class="identifier"><isa:translate key="b2b.order.display.ponumber"/></td>
	                            <td class="value"><%= ui.header.getPurchaseOrderExt() %></td>
	                        </tr>
	                    <% } %>
                        <% if (!ui.isBackendR3() && ui.isElementVisible("order.description")) { %>
	                        <tr class="title">
	                            <td class="identifier"><isa:translate key="status.sales.description"/>:</td>
	                            <td class="value"><%= JspUtil.encodeHtml(ui.header.getDescription()) %></td>
	                        </tr>
                        <% } %>
                       <%-- Shipto --%>
                       <% if (ui.showHeaderShipTo() && ui.isElementVisible("order.deliverTo")) { %>
                          <tr>
                             <td><isa:translate key="status.sales.detail.deliveredto"/></td>
                            <% if (ui.isPickupFromVendor()) { %>
                              <td><isa:translate key="hom.jsp.CollectionFromVend"/></td>
                            <% }  else { %>
                              <td><%= JspUtil.encodeHtml(ui.header.getShipTo().getShortAddress()) %></td>
                            <% } %>
                          </tr>
                       <% } %>
                    </table>
                </td>
                <td class="col2">
                    <%-- Status --%>
                    <% if (!ui.isOrderTemplate()) { %>
                           <% if (ui.isAccessible) { %>
                              <a href="#end-table2" title="<isa:translate key="ecm.acc.head.status"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="2"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
                           <% } %>
                           <table class="status" summary="<isa:translate key="ecm.acc.head.status"/>">
                                <% if (ui.isElementVisible("order.status.overall")) { %>
	                                <tr>
	                                        <td class="identifier"><isa:translate key="status.sales.overallstatus"/>:</td>
	                                        <td class="value"><isa:translate key="<%= ui.getDocumentStatusKey() %>"/></td>
	                                </tr>
	                            <% } %> 
                                <% if (ui.isQuotation()) { %>
                                   <tr>
		                                <% if (ui.showValidTo()&& ui.isElementVisible("order.validTo")) { %>
		                                                    <td class="identifier"><isa:translate key="status.sales.detail.validTo"/>:</td>
		                                                    <td class="value"><%= ui.getHeaderValidTo() %></td>
		                                <% } %>
                                   </tr>
                                <% }
                                else if (!ui.isLargeDocument() &&
                                         ui.getDocumentDeliveryStatusKey() != null &&
                                         ui.getDocumentDeliveryStatusKey().length() > 0 &&
						                 ui.isElementVisible("order.status.delivery")) { %>
                                   <tr>
                                                   <td class="identifier"><isa:translate key="status.sales.deliverystatus"/>:</td>
                                                   <td class="value"><isa:translate key="<%= ui.getDocumentDeliveryStatusKey() %>"/></td>
                                   </tr>
                               <% } %>
                           </table>
                           <% if (ui.isAccessible) { %>
                               <a name="end-table2" title="<isa:translate key="ecm.acc.head.stat.end"/>"></a>
                           <% } %>
                    <% } %>
                    <%-- End Status --%>
                    <%-- table price --%>
                    <% if (ui.isAccessible) { %>
                        <a href="#end-table3" title="<isa:translate key="ecm.acc.header.prieces"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="4"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
                    <% } %>
                    <table class="price-info" summary="<isa:translate key="ecm.acc.header.prieces"/>">
                        <% if (ui.isNetValueAvailable() && ui.isElementVisible("order.priceNet")) { %>
                        <tr>
                            <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalnet"/>:</td>
                            <td class="value"><%= ui.getHeaderValue() %>&nbsp;<%= ui.header.getCurrency() %></td>
                        </tr>
                        <% } %>
                        <% if (ui.isFreightValueAvailable() && ui.isElementVisible("order.freight")) { %>
                        <tr>
                            <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalfreight"/>:</td>
                            <td class="value"><%= ui.header.getFreightValue() %>&nbsp;<%= ui.header.getCurrency() %></td>
                        </tr>
                        <% } %>	                        
                        <% if (ui.isTaxValueAvailable() && ui.isElementVisible("order.tax")) { %>
                        <tr>
                            <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totaltax"/>:</td>
                            <td class="value"><%= ui.header.getTaxValue() %>&nbsp;<%= ui.header.getCurrency() %></td>
                        </tr>
                        <% } %>
                        <% if (ui.isGrossValueAvailable()&& ui.isElementVisible("order.priceGross")) { %>
	                        <tr>
	                            <td colspan="2" class="separator"></td>
	                        </tr>                        
	                        <tr>
	                            <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalgross"/>:</td>
	                            <td class="value"><%= ui.header.getGrossValue() %>&nbsp;<%= ui.header.getCurrency() %></td>
	                        </tr>
                        <% } %>
                        <% if (ui.showHeaderPaymentTerms()&& ui.isElementVisible("order.paymentterms")) { %>
                        <tr>
                            <td class="identifier-1" scope="row"><isa:translate key="b2b.order.disp.paymentterms"/></td>
                            <td class="terms"><%= JspUtil.replaceSpecialCharacters(ui.header.getPaymentTermsDesc()) %></td>
                        </tr>
                        <% } %>
                    </table>
                    <% if (ui.isAccessible) { %>
                        <a name="end-table3" title="<isa:translate key="ecm.acc.head.prc.end"/>"></a>
                    <% } %>
                    <%-- end table price --%>
                </td>
            </tr>
        </table> <%-- class="layout"> --%>
    </div> <%-- class="header-basic"> --%>
    <%-- Item defaults Data
         We display only item defaults on header level,
         if we do not show them on item level. --%>
    <% if (ui.isElementVisible("order.shippingCondition")){ %>
        <div class="header-itemdefault">  <%-- level: sub3 --%>
            <h1 class="group"><span><isa:translate key="b2b.header.itemdefault"/></span></h1>
            <div class="group">
            <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>">
              <% if (ui.isElementVisible("order.shippingCondition")) { %>
                 <tr>
                     <td class="identifier"><isa:translate key="b2b.order.display.shipcond"/></td>
                     <td class="value">
                        <% if (!ui.isPrintOrderStatus) { %>
                            <isa:iterate id="shipCond" name="<%= MaintainBasketBaseAction.RC_SHIPCOND %>"  type="com.sap.isa.core.util.table.ResultData">
                              <% if (shipCond.getRowKey().toString().equals(ui.header.getShipCond())) { %>
                                <%= JspUtil.encodeHtml(shipCond.getString(1)) %>
                              <% } %>
                            </isa:iterate>
                        <% } else { %>
                           <%= JspUtil.encodeHtml(ui.header.getShipCond()) %>
                        <% } %>
                    </td>
                </tr>
              <% } %>
            </table> <%-- end class="data" --%>
        </div> <%-- end class="group" --%>
    </div> <%-- class="header-itemdefault"> --%>
    <% } %> <%-- End Item defaults Data --%>
    <%-- Additional Header Data--%>
    <% if (ui.isHeaderMiscDisplayed() || ui.isPaymentDataDisplayed() || ui.isHeaderMessageDisplayed()) { %>
    <div class="header-additional">  <%-- level: sub3 --%>
        <h1 class="area"><span><isa:translate key="b2b.header.adddata"/></span></h1>
        <div class="area" id="addheader">
            <% if(ui.isHeaderMiscDisplayed()) { %>
            <div class="header-misc">
                <h1 class="group"><span><isa:translate key="b2b.header.group.misc"/></span></h1>
                <div class="group">
                    <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>">
                        <% if ((ui.isElementVisible("order.incoTerms1") && ui.isElementVisible("order.incoTerms2")) && ui.header.getIncoTerms1Desc() != null && ui.header.getIncoTerms1Desc().length() > 0) { %>
                            <tr>
                                <td class="identifier"><isa:translate key="b2b.order.disp.incoterms1"/></td>
                                <td class="value"><%= JspUtil.replaceSpecialCharacters(ui.header.getIncoTerms1Desc()) %>&nbsp;<%=JspUtil.replaceSpecialCharacters(ui.header.getIncoTerms2()) %></td>
                            </tr>
                        <% } %>
                        <% if (ui.isHeaderCampaignListSet() && !ui.isOciTransfer() && ui.isElementVisible("order.campaign")) { 
                               for (int headCamp  = 0; headCamp < ui.getNoHeaderCampaignEntries(); headCamp++) { %>
                            <tr>
                                <td class="identifier"><% if (headCamp == 0) { %><isa:translate key="b2b.order.disp.camp"/><% } %></td>
                                <td class="value"><%= JspUtil.encodeHtml(ui.getHeaderCampaignId(headCamp)) %>; <%= JspUtil.encodeHtml(ui.getHeaderCampaignDescs(headCamp)) %></td>
                            </tr>
                            <% }
                           } %>
                    <% if (!ui.isServiceRecall() && ui.header.getExtRefObjectType() != null &&  ui.header.getExtRefObjectType().length() > 0  && ui.isElementVisible("order.extRefObjects")) { %>
                        <tr>
                            <td class="identifier"><%= JspUtil.encodeHtml(ui.header.getExtRefObjectTypeDesc()) %>:</td>
                            <td class="value">
                                <%  ExtRefObjectList extRefObjects = ui.header.getAssignedExtRefObjects();
                                    if ( extRefObjects != null && extRefObjects.size() > 0) {
                                              for (int j=0; j < extRefObjects.size(); j++) { %>
                                                     <%= JspUtil.encodeHtml(extRefObjects.getExtRefObject(j).getData()) %>
                                                     <% if (j < extRefObjects.size()-1) { %>;<% } %>
                                              <% }
                                     } %>
                            </td>
                        </tr>
                    <% } %>
                    <%-- external reference numbers --%>
                    <% if (ui.isElementVisible("order.extRefNumbers") && ui.header.getAssignedExternalReferences() != null && ui.header.getAssignedExternalReferences().size() > 0) {
	                    ExternalReference extRefHeader;
	                    for (int i = 0; i < ui.header.getAssignedExternalReferences().size(); i++) {
		                   extRefHeader = (ExternalReference)ui.header.getAssignedExternalReferences().getExtRef(i);
                    %>		
                          <tr>
                            <td class="identifier">
                              <label for="extRefNumberType"><%= extRefHeader.getDescription() %>:</label>
                            </td>
                            <td class="value"> 
                              <%= JspUtil.encodeHtml(extRefHeader.getData()) %>
                            </td>   
                          </tr>
                     <%	  }
                        } 
                     %>
                    </table> <%-- end class="data" --%>
                </div> <%-- end class="group" --%>
            </div> <%-- end class="header-misc" --%>
            <% } %>
            <%-- Payment data --%>
                <%@ include file="/ecombase/paymentinfo.inc.jsp" %>
            <%-- Data Message --%>
            <% if (ui.isHeaderMessageDisplayed() && ui.isElementVisible("order.comment") && ui.hasHeaderText()) { %>
              <div class="header-message">
                  <h1 class="group"><span><isa:translate key="b2b.header.group.message"/></span></h1>
                  <div class="group-confirm">
                      <table class="message" summary="<isa:translate key="ecm.acc.head.msg.d"/>">
                          <tr>
                              <td class="identifier"><label for="headerComment"><isa:translate key="b2b.order.details.note"/></label></td>
                              <td class="value"><%= (ui.hasHeaderText()) ? JspUtil.encodeHtml(ui.getHeaderText(), new char[] {'\n'}):"" %></td>
                          </tr>
                      </table>
                  </div> <%-- end class="group" --%>
              </div> <%-- end class="header-message" --%>
            <% } %>
            <%-- End Data Message --%>
        </div> <%-- End class="area" --%>
    </div> <%-- class="header-additional"> --%>
    <% } %>
    <% if (ui.isAccessible) { %>
       <a name="end-table1" title="<isa:translate key="ecm.acc.head.dat.end"/>"></a>
    <% } %>
    <%-- Document flow --%>
    <% if ( ui.isHeaderDocFlowAvailable()) { %>
        <div class="header-docflow">  <%-- level: sub3 --%>
            <h1 class="group"><span><isa:translate key="b2b.header.group.docflow"/></span></h1>
            <div class="group">
                <table class="data">
                                <%@ include file="/ecombase/connecteddocuments.inc.jsp" %>
                </table> <%-- end class="data" --%>
            </div> <%-- end class="group" --%>
        </div> <%-- class="header-docflow"> --%>
    <% } %>
    <%-- End Document flow --%>
    <%-- Error --%>
    <% if (ui.docMessageString.length() > 0) { %>
        <div class="error"><span><%= ui.docMessageString %></span></div>
    <% } %>
    </div> <%-- End  Document Header --%>

    <div class="document-items">

    <%-- Document Item Rows --%>
    <form id="positions" method="post" action="">
     <% if (ui.isAccessible) { %>
        <a href="#end-table5" title="<isa:translate key="ecm.acc.items.sum"/> <isa:translate key="ecm.acc.tab.size.info" arg0="7" arg1="<%=(String.valueOf(ui.items.size())) %>"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
     <% } %>
     <div>
     <input type="hidden" name="docnumber" value="<%=ui.header.getSalesDocNumber()%>" />
     </div>
     <table class="itemlist" summary="<isa:translate key="ecm.acc.items.sum"/>">
        <%-- Items Table Header --%>
        <tr>
            <th class="item" scope="col"><isa:translate key="status.sales.detail.itemposno"/></th>
            <th class="product" scope="col"><isa:translate key="b2b.order.display.itemdetails"/></th>
            <th class="qty" scope="col"><isa:translate key="b2b.conf.displ.quantity"/></th>
            <th class="price" scope="col"><isa:translate key="status.sales.detail.totalvalue"/><br /><em><isa:translate key="b2b.order.details.price"/></em></th>
         <% if (ui.showStatusColumn()) { %>
            <th class="status" scope="col">
              <% if (ui.isCustomerDocument()) { %>
              <isa:translate key="hom.jsp.procorder.customer.state"/>
              <% } %>
              <isa:translate key="status.sales.status"/>
            </th>
         <% } %>
        </tr>

        <%-- Items Table Positions --%>
        <isa:iterate id="item" name="<%= MaintainBasketBaseAction.RK_ITEMS %>"
                     type="com.sap.isa.businessobject.item.ItemSalesDoc">
              <% ui.setItem(item) ;
                 String itemKey = item.getTechKey().getIdAsString();
                 if ( !ui.isBOMSubItemToBeSuppressed() ) {
                   ConnectedDocumentItem connectedItem = (ConnectedDocumentItem)item.getPredecessor();
                   String substMsgColspan = ui.getItemsColspan();
               %>
               <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>
               <tr class="<%= ui.even_odd%>" id="row_<%= ui.line %>">                  
                  <td <% if (ui.itemHierarchy.isSubItem(item)) { %> class="item-sub-<%= ui.getItemLevel() -1 %>" <% } else { %> class="item" <% } %>>
                     <% if (ui.isElementVisible("order.item.posNo", itemKey)) { %>
                        <%= JspUtil.removeNull(item.getNumberInt()) %>
                     <% } %>
                  </td>
                  <td class="product">
                      <em>
                         <% if (ui.isElementVisible("order.item.product", itemKey)) { %>
                             <%= ui.getProduct() %> 
                         <% } %>
                         <% if (ui.isElementVisible("order.item.product", itemKey ) && ui.isElementVisible("order.item.description", itemKey ))  { %>
                              :
                         <% } %>
                         <% if (ui.isElementVisible("order.item.description", itemKey )) { %>
                            <%= JspUtil.encodeHtml(item.getDescription().trim()) %>
                         <% } %>
                      </em>
                      &nbsp;<%=ui.getSubstitutionReason()%>   
                      <%  if (ui.showItemDetailButton()) {%>
                        <table>
                          <%  if ( ui.isCustomerDocument() && connectedItem != null && ui.isElementVisible("order.item.pred.".concat(connectedItem.getDocType())) ) {%>
                            <tr>
                              <td><isa:translate key="hom.jsp.procorder.combinedorder"/></td>
                              <td>
                                <%=JspUtil.removeNull(connectedItem.getDocNumber()) %> / <%=JspUtil.removeNull(connectedItem.getPosNumber()) %>
                              </td>
                            </tr>
                          <% }%>
                          <tr>
		                       <%-- System product id --%>
		                       <% if (ui.isElementVisible("order.item.systemProductId", itemKey) && ui.isProductDeterminationInfoAvailable() ) { %>
		                          <tr>
		                            <td><isa:translate key="b2b.proddet.sysprodid"/></td>
		                            <td><%= item.getSystemProductId() %></td>
		                          </tr>
		                       <% } %>
		                       <%-- display of product configuration --%>
		                       <% if (ui.isElementVisible("order.item.configuration", itemKey)) { %>
     		                       <%@ include file="/ecombase/orderitemconfiginfoconfirm.inc.jsp" %>
	                           <% } %> 
		                       <%-- Shipto --%>
		                       <% if (ui.isElementVisible("order.item.deliverTo", itemKey) && ui.showItemShipTo()) { %>
		                          <tr>
		                             <td><isa:translate key="status.sales.detail.deliveredto"/></td>
		                              <td><%= JspUtil.encodeHtml(item.getShipTo().getShortAddress()) %></td>
		                          </tr>
		                       <% } %>
		                       <%-- Contract --%>
		                       <% if (ui.isElementVisible("order.item.contract", itemKey) && ui.isContractInfoAvailable() && !"".equals(item.getContractId()) ) { %>
		                          <tr>
		                              <td><isa:translate key="b2b.order.display.contract"/>:</td>
		                              <td><%= JspUtil.removeNull(item.getContractId()) %> / <%= JspUtil.removeNull(item.getContractItemId()) %>
		                                                </td>
		                          </tr>
		                       <% } %>
		                       <%-- Delivery Priority --%>
		                       <% if (ui.isElementVisible("order.item.deliveryPriority", itemKey) &&
		                              item.getDeliveryPriority() != null && item.getDeliveryPriority().length() > 0) { %>
		                          <tr>
		                            <td><isa:translate key="b2b.order.display.deliverPrio"/></td>
		                            <td ><%= (String) ui.getDeliveryPriorityDescription(item.getDeliveryPriority()) %></td>
		                          </tr>
		                       <% } %>
		                       <%-- Delivery date + schedule lines --%>
		                       <% if (!ui.header.isDocumentTypeOrderTemplate()) { %>
		                          <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey) &&  ui.isElementVisible("order.item.schedulineQty", itemKey) && ui.isElementVisible("order.item.schedulineDate", itemKey)) { %>
			                          <tr>
			                            <td><isa:translate key="b2b.conf.displ.reqdel"/></td>
			                            <td><%= item.getReqDeliveryDate() %><%= ui.getSchedulineData() %></td>
			                          </tr>
			                      <% } %>
		                          <%-- Cancel date --%>
		                          <% if (ui.isElementVisible("order.latestDeliveryDate") && item.getLatestDlvDate() != null && item.getLatestDlvDate().length() > 0) { %>
		                             <tr>
		                               <td><isa:translate key="b2b.order.grid.canceldate"/></td>
		                               <td><%= JspUtil.removeNull(item.getLatestDlvDate()) %></td>
		                             </tr>
		                          <% } %>
		                       <% } %>
		                       <%-- campaigns --%>
		                       <% if (ui.isElementVisible("order.item.campaign", itemKey) && ui.isItemCampaignListSet()) { 
                                     for (int itemCamp = 0; itemCamp < ui.getNoItemCampaignEntries(); itemCamp++) { %>
		                          <tr>
		                            <td><% if (itemCamp == 0) { %><isa:translate key="b2b.order.disp.camp"/><% } %></td>
		                            <td><%= JspUtil.encodeHtml(ui.getItemCampaignId(itemCamp)) %>;&nbsp;<%= JspUtil.encodeHtml(ui.getItemCampaignDescs(itemCamp)) %></td>
		                          </tr>
		                           <% }
		                          } %>
		                       <%-- external reference objects --%>
		                       <% if (ui.isElementVisible("order.item.extRefObjects", itemKey) && item.getExtRefObjectType() != null &&  item.getExtRefObjectType().length() > 0) { %>
		                          <tr>
		                            <td class="identifier"><%= JspUtil.encodeHtml(ui.header.getExtRefObjectTypeDesc()) %>:</td>
		                            <td class="value">
		                                <%  ExtRefObjectList itemExtRefObjects = item.getAssignedExtRefObjects(); %>
		                                <% if ( itemExtRefObjects != null && itemExtRefObjects.size() > 0) {
		                                        for (int j=0; j < itemExtRefObjects.size(); j++) { %>
		                                             <%= JspUtil.encodeHtml(itemExtRefObjects.getExtRefObject(j).getData()) %>
		                                             <% if (j < itemExtRefObjects.size()-1) { %>;<% } %>
		                                         <% } %>
		                                 <% } %>                    
		                            </td>
		                          </tr>
		                       <% } %>
		                       <%-- external reference numbers --%>
		                       <% if (item.getAssignedExternalReferences() != null && item.getAssignedExternalReferences().size() > 0 && ui.isElementVisible("order.item.extRefNumbers", itemKey)) {
	                                 ExternalReference extRefItem;
	                                 for (int i = 0; i < item.getAssignedExternalReferences().size(); i++) {
		                                extRefItem = (ExternalReference)item.getAssignedExternalReferences().getExtRef(i);
                               %>		
                                      <tr>
                                        <td class="identifier">
                                           <label for="itemExtRefNumberType"><%= extRefItem.getDescription() %>:</label>
                                        </td>
                                        <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
		                                  <td class="campimg-1"></td>
                             		    <% } %> 
                                        <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>"> 
                                          <%= JspUtil.encodeHtml(extRefItem.getData()) %>
                                       </td>   
                                      </tr>
                              <%	}
                                  } 
                               %>
		                       <%-- payment terms --%>
		                       <% if (ui.isElementVisible("order.item.paymentterms", itemKey) && ui.showItemPaymentTerms()) { %>
		                             <tr>
		                                  <td class="identifier" scope="row"><isa:translate key="b2b.order.disp.paymentterms"/></td>
		                                  <td class="value"><%=JspUtil.encodeHtml(item.getPaymentTermsDesc()) %></td>
		                             </tr>
		                        <% } %>
		                          <%-- notes --%>
		                        <% if (ui.isElementVisible("order.item.paymentterms", itemKey) && item.hasText()) { %>		                          
		                          <tr class="message-data">
		                            <td class="identifier"><isa:translate key="b2b.order.details.note"/></td>
		                            <td><%=JspUtil.encodeHtml(item.getText().getText(), new char[] {'\n'}) %></td>
		                          </tr>
		                        <% } %>
		                        <% if (ui.isElementVisible("order.item.batchId", itemKey) && item.getBatchID() != null && !item.getBatchID().equals("")) { %>
			                          <tr>
			                            <td><isa:translate key="b2b.order.details.batch.number"/></td>
			                            <td><%= JspUtil.replaceSpecialCharacters(item.getBatchID()) %></td>
			                          </tr>
			                    <% } %>
			                    <%-- Batch characteristics --%>
			                    <% if (ui.isElementVisible("order.item.batchChar", itemKey) && item.getBatchCharListJSP() != null && item.getBatchCharListJSP().size() != 0) {
			                            for(int i=0; i<item.getBatchCharListJSP().size(); i++) {
			                                inputValue = "";
			                                for(int j=0; j<item.getBatchCharListJSP().get(i).getCharValTxtNum(); j++) {
			                                    inputValue += item.getBatchCharListJSP().get(i).getCharValTxt(j) + ";";
			                                } %>
			                               <tr>
			                                <% if (item.getBatchCharListJSP().get(i).getCharTxt().equals("")) { %>
			                                   <td><%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharName()) %>:&nbsp;</td>
			                                <% } else { %>
			                                   <td><%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharTxt()) %>:&nbsp;</td>
			                                <% } %>
			                                   <td>&nbsp;<%= inputValue %></td>
			                               </tr>
			                         <% } %>
			                    <% } %>
                      </table>
                      <%
                        }%>
                  </td>
                  <td class="qty">
                     <% if (ui.isElementVisible("order.item.qty", itemKey) && ui.isElementVisible("order.item.unit", itemKey)) { %>
                        <%= item.getQuantity() %>&nbsp;<abbr title="<isa:translate key="status.sales.detail.itemunit"/>"><%= item.getUnit() %></abbr></td>
                     <% } %>
                  <td class="price">
                  <% if (  !ui.itemHierarchy.hasATPSubItem(item) && item.isPriceRelevant()) { %>                   
					<% if ( ui.isElementVisible("order.item.grossValue", itemKey)) { %><%= ui.getItemValue() %>&nbsp;<%= item.getCurrency() %><% } %>
                    <% if (ui.isElementVisible("order.item.netPrice", itemKey) && (item.getNetPriceUnit() != null) && (!item.getNetPriceUnit().equals(""))) { %>
                        <br/><em>
                            <%= JspUtil.removeNull(item.getNetPrice()) + "&nbsp;" + JspUtil.removeNull(item.getCurrency()) + "&nbsp;" + '/' + "&nbsp;" + JspUtil.removeNull(item.getNetQuantPriceUnit()) + ' ' + JspUtil.removeNull(item.getNetPriceUnit()) %>
                            </em>
                    <% }
                     } %>
                  </td>
                  <% if (ui.showStatusColumn()) { %>
                     <td class="status">
                         <% if (ui.isElementVisible("order.item.status", itemKey)) { %>
	                       <% if (ui.isCustomerDocument()) {
	                             Iterator extStatus = item.getExtendedStatus().getStatusIterator();
	                             request.setAttribute("EXTENDEDSTATUS",extStatus);%>
	                             <strong>
	                                <isa:iterate id="eStatus" name="EXTENDEDSTATUS" type="com.sap.isa.businessobject.ExtendedStatusListEntry">
	                                    <%= eStatus.isCurrentStatus() ? eStatus.getDescription() : ""%>
	                                </isa:iterate>
	                             </strong>
	                             <br>
	                       <% } %>
	                       <%= ui.getItemStatus() %>
                        <% } %>
                    </td>
                  <% } %>
                </tr>
                <% if (ui.itemMessageString.length() > 0) { %>
                <tr>
                  <td colspan="<%= ui.getItemsColspan() %>"><%= ui.itemMessageString %></td>
                </tr>
              <% } %>
                  <%-- information, that item is not confirmed related to the delivery date or item is delivery due --%>
<%--          <% if (item.isOrderDue()) { %>
                         <tr class="info" >
                           <td colspan="<%= ui.getItemsColspan() %>" > <isa:translate key="<%= ui.getItemBackOrderKey() %>"/>
                         </tr>
                   <% } %>
--%>
            <%-- ********** Seperator between two items ********** --%>
                <tr>
                   <td colspan="<%= ui.getItemsColspan() %>" class="separator"></td>
                </tr>
            <% } %>   <%-- isBOMSubItemToBeSuppressed() --%>
          </isa:iterate>
          <%-- delete individual UI elements of order items --%>
          <% 
		     ui.deleteIndividualUIElementsInGroup("order.item");  
          %>
     </table>
     <% if (ui.isAccessible) { %>
       <a name="#end-table5" title="<isa:translate key="ecm.acc.item.dat.end"/>"></a>
     <% } %>
    </form>
    </div>

    </div>

    <div id="buttons">
        <a id="access-buttons" href="#access-header" title="<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>
        <ul class="buttons-3">
        <% if (ui.isOciTransfer()) { %>
              <li><a href="#" onclick="updateDocumentView();" <% if (ui.isAccessible) { %> title="<isa:translate key="ecm.acc.button"/> <isa:translate key="b2b.order.summary.close"/>" <% } %> ><isa:translate key="b2b.order.summary.close"/></a></li>
              <li><a href="#" onclick="send_oci();" <% if (ui.isAccessible) { %> title="<isa:translate key="ecm.acc.button"/> <isa:translate key="b2b.ord.sub.send.oci"/>" <% } %> ><isa:translate key="b2b.ord.sub.send.oci"/></a></li>
        <% } else { %>
              <% if (!browserType.isUnknown() && !browserType.isNav4()) { %>
                    <li><a href="#" onclick="print();" <% if (ui.isAccessible) { %> title="<isa:translate key="ecm.acc.button"/>  <isa:translate key="b2b.order.summary.print"/>" <% } %> ><isa:translate key="b2b.order.summary.print"/></a></li>
              <% } %>
              <li><a href="#" onclick="updateDocumentView();" <% if (ui.isAccessible) { %> title="<isa:translate key="ecm.acc.button"/>  <isa:translate key="b2b.order.summary.close"/>" <% } %> ><isa:translate key="b2b.order.summary.close"/></a></li>
        <% } %>
        </ul>
    </div>

</body>
</html>