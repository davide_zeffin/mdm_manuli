<!-- get Reseller info and display it on ordersimulate.jsp -->
               <%
               
                BusinessPartnerManager bupaManager = ui.getBom().createBUPAManager();
                BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
               
               %>
                                             
                 
                 <tr>
                    <td align="right">
                        <isa:translate key="b2b.order.reseller.header"/>&nbsp;
                    </td>
                    <td class="orderHeadPrice">&nbsp;
                        <%= reseller.getAddress().getName1() %>
                    </td>
                  </tr>
                  <tr>
                    <td align="right">
                        <isa:translate key="b2b.order.reseller.address"/>&nbsp;
                    </td>
                    <td class="orderHeadPrice">&nbsp;
                        <%= reseller.getAddress().getStreet() %>&nbsp;
                        <%= reseller.getAddress().getHouseNo() %>
                    </td>
                  </tr>
                  <tr>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td class="orderHeadPrice">&nbsp;
                        <%= reseller.getAddress().getPostlCod1() %>&nbsp;
                        <%= reseller.getAddress().getCity() %>
                    </td>
                  </tr>
                  <tr>
                    <td align="right">
                       <isa:translate key="b2b.order.display.reseller.phone"/>&nbsp;
                    </td>
                    <td class="orderHeadPrice">&nbsp;
                        <%= reseller.getAddress().getTel1Numbr() %>
                    </td>
                  </tr>
                  <tr>
                    <td align="right">
                        <isa:translate key="b2b.order.display.reseller.mail"/>&nbsp;
                    </td>
                    <td class="orderHeadPrice">&nbsp;
                        <%= reseller.getAddress().getEMail() %>
                    </td>
                  </tr>
                