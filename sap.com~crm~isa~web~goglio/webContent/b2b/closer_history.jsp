<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.IsaBaseUI" %>

<%  IsaBaseUI ui = new IsaBaseUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<isa:stylesheets/>

		<script type="text/javascript">
		<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
		</script>
		
		<script type="text/javascript">
		<!--
			var img0   = "<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/left_arrow.gif") %>";
			var img0_h = "<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/left_arrow_h.gif") %>";
			var img1   = "<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/right_arrow.gif") %>";
			var img1_h = "<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/right_arrow_h.gif") %>";
		//-->
		</script>
			
		<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/closer.js" ) %>" type="text/javascript" language="JavaScript"></script>
  </head>
  
  <body class="closer" onload="chgArrow('right', '1');">
  
	<%-- Accesskey for the history section. It must be an resourcekey, so that it can be translated. --%>
	<a id="access-history-closer" href="#access-items" title="<isa:translate key='b2b.closer.history.access'/>" accesskey="<isa:translate key='b2b.closer.history.accesskey.key'/>"></a>

 <% if (ui.showCatalog()) { %>
	<div id="leftArrow1">
        <%-- href and img must be in one row to avoid "framing" effects on mouse over !! --%>
		<a href="#" onmouseover="overArrow('left', 'arrow_left1');" onmouseout="outArrow('left', 'arrow_left1');" onclick="parent.resizeHist('max', 'history'); chgArrow('right', '1'); return false;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/left_arrow.gif") %>" width="7" height="13" border="0" alt="<isa:translate key='b2b.history.show'/>" name="arrow_left1" /></a>
	</div>
	<div id="rightArrow1">
		<a href="#" onmouseover="overArrow('right', 'arrow_right1');" onmouseout="outArrow('right', 'arrow_right1');" onclick="parent.resizeHist('min', 'history'); chgArrow('left', '1'); return false;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/right_arrow.gif") %>" width="7" height="13" border="0" alt="<isa:translate key='b2b.history.close'/>" name="arrow_right1" /></a>
	</div>
 <% }
	else { %>
    <div id="leftArrow1">
        <%-- href and img must be in one row to avoid "framing" effects on mouse over !! --%>
		<a href="#" onmouseover="overArrow('left', 'arrow_left1');" onmouseout="outArrow('left', 'arrow_left1');" onclick="parent.resize('max', 'history'); chgArrow('right', '1'); return false;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/left_arrow.gif") %>" width="7" height="13" border="0" alt="<isa:translate key='b2b.history.show'/>" name="arrow_left1" /></a>
	</div>
	<div id="rightArrow1">
		<a href="#" onmouseover="overArrow('right', 'arrow_right1');" onmouseout="outArrow('right', 'arrow_right1');" onclick="parent.resize('min', 'history'); chgArrow('left', '1'); return false;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/right_arrow.gif") %>" width="7" height="13" border="0" alt="<isa:translate key='b2b.history.close'/>" name="arrow_right1" /></a>
	</div>
 <% } %> 

    <p>
    	<img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="1200" alt="" border="0">
    </p>
  </body>
</html>