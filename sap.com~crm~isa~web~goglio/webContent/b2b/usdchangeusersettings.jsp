<%-- This JSP displays the PasswordChange screen to enter a new password --%>

<%@ page import="com.sap.isa.isacore.action.b2b.USDChangeAction" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.*" %>
<%
	IsaBaseUI ui = new IsaBaseUI(pageContext);
%>

<%	String isPriceActive = "";
	String isStockActive = "";
	
	if(((String)request.getAttribute(USDChangeAction.GRID_PRICE)).equals("X")) isPriceActive="checked";
	if(((String)request.getAttribute(USDChangeAction.GRID_STOCK)).equals("X")) isStockActive="checked";
	
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<title><isa:translate key="b2b.usd.title"/></title>
		<isa:stylesheets/>
    	
		<script type="text/javascript">
			function send_transfer(cancel) {
				if (cancel == "X"){
					document.usersessiondatasettings.cancel.value="X";
				}else{
					alert("<isa:translate key="b2b.usd.saved"/>");
				}
				document.usersessiondatasettings.submit(); 
				return true;
			}
		</script>
    	
	</head>
  	
<body class="selection-shop">

<div class="selection">
	<div class="module-name"><isa:moduleName name="b2b/usdchangeusersettings.jsp" /></div>
	<h1><isa:translate key="b2b.usd.head"/></h1>

	<form method="post" 
		id="usersessiondatasettings"
		name="usersessiondatasettings"
		action='<isa:webappsURL name="/b2b/usdchangeusersettings.do"/>'>
		<div class="login">
		<% if (ui.isAccessible) { %>
			<a href="#end-login" name="start-login" title="<isa:translate key="b2b.usd.endusdchange"/>"></a>
		<% } %>
			<ul>
				<li class="label-input">
					<div>
						<label for="price"><isa:translate key="b2b.grid.checkbox.price"/></label><br/>
						<input type="checkbox" name="inclPrice" value="" <%=isPriceActive %>
						onClick="if (document.forms['usersessiondatasettings'].elements['inclPrice'].checked == true){  document.forms['usersessiondatasettings'].elements['gridprice'].value = 'X'; } else { document.forms['usersessiondatasettings'].elements['gridprice'].value = ''; }" />
					</div>
				</li>
				<li class="label-input">
					<div>
						<label for="stock"><isa:translate key="b2b.grid.checkbox.stock"/></label><br/>
						<input type="checkbox" name="inclStock" value="" <%=isStockActive%>
						onClick="if (document.forms['usersessiondatasettings'].elements['inclStock'].checked == true){  document.forms['usersessiondatasettings'].elements['gridstock'].value = 'X'; } else { document.forms['usersessiondatasettings'].elements['gridstock'].value = ''; }" />
					<div>
				</li>
				<li class="label-input">
					<div class="button1">
						<a href="#" onclick="javascript:send_transfer('');" class="button" name="" title="<isa:translate key="b2b.usd.submit"/>: <isa:translate key="b2b.usd.usdchange.title"/>">
							<isa:translate key="b2b.usd.submit"/>
						</a>
						<a href="#" onclick="javascript:send_transfer('X');" class="button" name="" title="<isa:translate key="b2b.login.pwchange.cancel"/>: <isa:translate key="b2b.usd.usdchange.title"/>">
							<isa:translate key="b2b.login.pwchange.cancel"/>
						</a>
					</div>
				</li>
			</ul>
			<% if (ui.isAccessible) { %>
				<a href="#start-login" name="end-login" title="<isa:translate key="b2b.usd.startusdchange"/>"></a>
			<% } %>	
		</div> <%-- login --%>
		
		<input type="hidden" name="cancel" value=""/>
		<input type="hidden" name="configtype" value=""/>
        <input type="hidden" name="gridprice" value="<%= JspUtil.encodeHtml((String)request.getAttribute(USDChangeAction.GRID_PRICE))%>"/>
        <input type="hidden" name="gridstock" value="<%= JspUtil.encodeHtml((String)request.getAttribute(USDChangeAction.GRID_STOCK))%>"/> 
	</form>

</div> <%-- selection --%>

</html>