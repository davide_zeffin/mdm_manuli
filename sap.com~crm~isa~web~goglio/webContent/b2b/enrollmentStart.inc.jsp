<%--
********************************************************************************
    File:         enrollmentStart.inc.jsp
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      23.11.2006
    Version:      1.0
********************************************************************************
--%>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.isacore.action.b2b.GetDealerFamilyAction" %>
<%@ page import="com.sap.isa.businesspartner.backend.boi.BusinessPartnerData" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>

<% String f_enrolleeId = "enrolleeId"; 
   String f_bpSelect = "bpSelect"; 
   String f_campaignId = "campaignId"; 
   field = ui.getSoldToFieldFromList(f_enrolleeId, "welcomeMktForm", "", "b2b.welcome.enrollment.myBpPrefix"); 
   field.setDefValue(ui.getEnrolleeId()); %>
<%-- field.setListThreshold(20); --%>

<script type="text/javascript">
<!--
      function start_enrollment_bp(currForm, f_campaignId) {
        var campaignId = document.forms[currForm].elements[f_campaignId].value;
        var reqP = "?campaignId="+campaignId+"&searchBpHier=false&campaignpage=1";
        var rewURL = rewriteURL("/b2b/searchcampaign.do"+reqP);
        displayBusy(); 
        parent.form_input.location.href=rewURL;
      }

      function start_enrollment(currForm, f_bpSelect, f_enrolleeId, f_campaignId) {
        var bpSelect = document.forms[currForm].elements[f_bpSelect].value;
        var searchBpHier = "";
        var enrolleeId = "";
        /* MY, ALL, SEL */
        switch (bpSelect) {
          case "MY":
            searchBpHier = "false";
            break;
          case "ALL":
            searchBpHier = "true";
            break;
          case "SEL":
            searchBpHier = "false";
            enrolleeId = document.forms[currForm].elements[f_enrolleeId].value;
            break;
        }
        var campaignId = document.forms[currForm].elements[f_campaignId].value;
        var reqP = "?campaignId="+campaignId+"&searchBpHier="+searchBpHier+"&enrolleeId="+enrolleeId;
        reqP += "&campaignpage=1";
        

        var rewURL = rewriteURL("/b2b/searchcampaign.do"+reqP);
        displayBusy(); 
        parent.form_input.location.href=rewURL;
      }

      function bpSelectChange(currForm, f_bpSelect, f_bpField) {
        var bpSelect = document.forms[currForm].elements[f_bpSelect].value;
        if (bpSelect == "SEL") {
          document.getElementById(f_bpField).style.visibility = "visible";
        }
        else {
          document.getElementById(f_bpField).style.visibility = "hidden";
        }
      }
//-->
</script>

<div class="module-name"><isa:moduleName name="b2b/enrollmentStart.inc.jsp" /></div>

<%-- Form is necessary for help values support --%>

<form name="welcomeMktForm" action="" method="get">

<% if (ui.isAccessible) { %>
  <a href="#end-campaign" title="<isa:translate key="ecm.acc.sec.campaign"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
<% } %>
    
  <div id="nodoc-campaign">
    <h1><span><isa:translate key="b2b.welcome.campaigns"/></span></h1>
            
    <ul>
      <li>
        &nbsp;
        <% if (ui.isBpDisplayed()) { %>
	        <div class="filter-1">
	          <label for="<%= f_bpSelect %>"><isa:translate key="b2b.welcome.enrollment.bpSelect"/></label>
	          <select name="<%= f_bpSelect %>" id="<%= f_bpSelect %>" size="1" 
	                  onchange='bpSelectChange("welcomeMktForm", "<%= f_bpSelect %>", "areaEnrolleeId")'>          
	            <option value="<%= CreateTransactionUI.SELBP_MY %>"
	              <% if (ui.isSearchForBpHier(CreateTransactionUI.SELBP_MY)) { %>
	                    selected="selected"
	              <% } %>
	              >
	              <isa:translate key="b2b.welcome.enrollment.bpSelect.my"/>
	            </option>
	            <option value="<%= CreateTransactionUI.SELBP_ALL %>"
	              <% if (ui.isSearchForBpHier(CreateTransactionUI.SELBP_ALL)) { %>
	                    selected="selected"
	              <% } %>
	              >
	              <isa:translate key="b2b.welcome.enrollment.bpSelect.all"/>
	            </option>
	            <option value="<%= CreateTransactionUI.SELBP_SEL %>"
	              <% if (ui.isSearchForBpHier(CreateTransactionUI.SELBP_SEL)) { %>
	                    selected="selected"
	              <% } %>
	              >
	              <isa:translate key="b2b.welcome.enrollment.bpSelect.another"/>
	            </option>
	          </select>
	        </div>
	
	        <%-- Business partner selection fields --%>
		    <div class="filter-2" id="areaEnrolleeId"
		    <% if (!ui.isSearchForBpHier(CreateTransactionUI.SELBP_SEL)) { %>
		          style="visibility:hidden"
		    <% } %>
		    >
		    <% if (!ui.getEnrolleeId().equals("")) { 
		          String enrolleeId = ui.getEnrolleeId(); 
		       } %>
		       <%@ include file="/appbase/fieldwithhelpvalues.inc.jsp" %>
		    </div>
        <% } %>
        
 	    <div class="filter-3">
          <%-- Campaign Id --%>
	 	  <div class="filter-4">
		    <label for="<%= f_campaignId %>"><isa:translate key="b2b.welcome.enrollment.campaignId"/></label>
		    <input type="text" onkeypress="return maskReturnKeyPressed(event);" id="<%= f_campaignId %>" name="<%= f_campaignId %>" size="24" maxlength="24" value=<%= ui.getCampaignId() %>>
	      </div>
	 
	 	  <%-- GO button --%>
	 	  <div class="buttons">
		 <% if (ui.isAccessible) { %> 
		    <label for="gobutton"><isa:translate key="gs.acc.but.start"/> </label>
		 <% } %>
		    <a class="button" name="gobutton" href="#" 
	     <% if (ui.isBpDisplayed()) { %>
		       onclick="start_enrollment('welcomeMktForm', '<%= f_bpSelect %>', '<%= f_enrolleeId %>', '<%= f_campaignId %>'); return false;" 
		 <% }
		    else { %>
		       onclick="start_enrollment_bp('welcomeMktForm', '<%= f_campaignId %>'); return false;" 
		 <% }
		    if (!ui.isAccessible) { %>
		       title="<isa:translate key="gs.acc.but.start"/>" 
		 <% } %> ><isa:translate key="gs.but.start"/></a>
		  </div>
	    </div>
 
        <%-- Message list --%>
        <% MessageList msgList = ui.getCampMessageList();
           if (msgList != null && !msgList.isEmpty()) { 
               pageContext.setAttribute(ui.MESSAGE_LIST, msgList); %>
               <br>
	           <isa:message id="messagetext" name="<%= ui.MESSAGE_LIST %>" ignoreNull="true">
	               <div class="error">
	                 <span><%=JspUtil.encodeHtml(messagetext)%></span>
	               </div>
	           </isa:message>
	     <% } %>
      </li>
    </ul>
  </div>
  
<% if (ui.isAccessible) { %>
  <a name="end-campaign" title="<isa:translate key="ecm.acc.sec.end.campaign"/>"></a>
<% } %>
</form>
