<%-- this includes only the fields --%>
<%-- any control logic must be added by the caller --%>
<%-- e.g. display - non-display; additional buttons --%>

<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase"%>
<%@ page import="com.sap.isa.payment.businessobject.*"%>
<%@ page import="com.sap.isa.payment.backend.boi.*"%>

<p>
<% if (paymentUI.isElementVisible(paymentUI.ONECARD_HOLDER) ||
       paymentUI.isElementVisible(paymentUI.CARD_HOLDER)) { %>
<isa:translate key="payment.card.holder.info"/><br /> 
<% } %>
<% if (paymentUI.isElementVisible(paymentUI.ONECARD_CVV) ||
       paymentUI.isElementVisible(paymentUI.CARD_CVV)) { %>
<isa:translate key="payment.card.cvv.info"/><br />
<% } %>
<% if (paymentUI.isElementVisible(paymentUI.ONECARD_SUFFIX) ||
       paymentUI.isElementVisible(paymentUI.CARD_SUFFIX)) { %>
<isa:translate key="payment.card.number.suffix.info"/><br />
<% } %>
<% if (paymentUI.isElementVisible(paymentUI.CARD_LIMITAMOUNT)) { %>
<isa:translate key="payment.card.limit.maxamount.inf"/>
<% } %>
</p>

<% paymentUI.setAllDisabledIfLargeDocumentCommands(); 
   paymentUI.resetSalesDocLastEnteredCVVS();
   String cvvVal = "";
		List payCards = new ArrayList();
		if (paymentUI.payment != null && paymentUI.payment.getPaymentMethods() != null && paymentUI.payment.getPaymentMethods().size() > 0) {
			List paymentMethods = paymentUI.payment.getPaymentMethods();
			Iterator iter = paymentMethods.iterator();			
			while (iter.hasNext()){					
			PaymentMethod payMethod = (PaymentMethod) iter.next();			
				if (payMethod != null){
				// if the payment method is Credit card, fill the import table accordingly
					if(payMethod.getPayTypeTechKey().equals(PaymentBaseTypeData.CARD_TECHKEY )) {                    		
						PaymentCCard payCCard = (PaymentCCard) payMethod;							
						payCards.add(payCCard);
					}
				}
			}
		} 
%>

<%-- Credit card details Start --%>
	<% if (paymentUI.oneCard) { %>
      <table 
	      class="list-simple" 
	      summary="<isa:translate key="payment.acc.maintenance"/>">	 
	    <% if (paymentUI.isElementVisible(paymentUI.ONECARD_TYPE)) { %>
	    <tr>
		<td class="identifier">
			<label for="nolog_cardtype[<%=paymentUI.oneLine%>]"><isa:translate key="payment.card.type"/>
			</label>
		</td>
		<td class="value">
			<% if (paymentUI.isMaintainable(paymentUI.ONECARD_TYPE)) { %>
			<select 
				name="nolog_cardtype[<%=paymentUI.oneLine%>]"
				id="nolog_cardtype[<%=paymentUI.oneLine%>]"
				<%=paymentUI.getAllDisabled()%>
			>
				<option 
					value="">
					<isa:translate 
						key="b2c.order.chckt.selectopt.sel"/>
				</option>
				<isa:iterate 
					id="cardType" 
					name="<%=PaymentActions.CARDTYPES%>" 
					type="com.sap.isa.core.util.table.ResultData">
					<option 
						<%=paymentUI.getCardTypeSelected(cardType.getRowKey().toString())%>
						value="<%=cardType.getRowKey()%>">
						<%= JspUtil.encodeHtml(cardType.getString(1)) %>
					</option>
				</isa:iterate>
			</select>
			<% } else { %>
			<% if (paymentUI.payment != null && payCards.size() > 0 && ((PaymentCCard) payCards.get(0)).getTypeDescription() != null) { %>
			<%= JspUtil.encodeHtml(((PaymentCCard) payCards.get(0)).getTypeDescription()) %>
			<input 
				type="hidden" 
			    name="nolog_cardtype[<%=paymentUI.oneLine%>]" 
			    id="nolog_cardtype[<%=paymentUI.oneLine%>]" 
			    value="<%= ((PaymentCCard) payCards.get(0)).getTypeTechKey() %>" />
			    <%= JspUtil.encodeHtml(((PaymentCCard) payCards.get(0)).getTypeDescription()) %>
			 <% } %>
			<% } %>				
		</td>
	</tr>
	<% } %>
	<% if (paymentUI.isElementVisible(paymentUI.ONECARD_HOLDER)) { %>
	<tr>
		<td class="identifier">
			<label 
				for="nolog_cardholder[<%=paymentUI.oneLine%>]">
				<isa:translate key="payment.card.holder"/>
			</label>
		</td>
		<td class="value">
			<input 
				class="textInput" 
				type="text" 
				<%=paymentUI.getFieldReadOnly(paymentUI.ONECARD_HOLDER)%>
				<%=paymentUI.getAllDisabled()%>
			    size="20" 
			    name="nolog_cardholder[<%=paymentUI.oneLine%>]" 
			    id="nolog_cardholder[<%=paymentUI.oneLine%>]" 
			    <% if (paymentUI.payment != null && payCards.size() > 0 && ((PaymentCCard) payCards.get(0)).getHolder() != null) { %>
			    value="<%=JspUtil.encodeHtml(((PaymentCCard) payCards.get(0)).getHolder())%>" />
			    <% } %>
		</td>
	</tr>
	<% } %>
	<% if (paymentUI.isElementVisible(paymentUI.ONECARD_NUMBER)) { %>
	<tr>
		<td class="identifier">
			<label 
				for="nolog_cardnumber[<%=paymentUI.oneLine%>]"> 
				<isa:translate 
					key="payment.card.number"/>
			</label>
		</td>
		<td class="value">
			<input 
				class="textInput" 
				type="text" 
				<%=paymentUI.getFieldReadOnly(paymentUI.ONECARD_NUMBER)%>
				<%=paymentUI.getAllDisabled()%>
			    size="16" 
			    name="nolog_cardno[<%=paymentUI.oneLine%>]" 
			    id="nolog_cardno[<%=paymentUI.oneLine%>]" 
			    <% if (paymentUI.payment != null && payCards.size() > 0 && ((PaymentCCard) payCards.get(0)).getNumber() != null) { %>
			    value="<%=((PaymentCCard) payCards.get(0)).getNumber()%>" />
			    <% } %>
		</td>
	</tr>
	<% } %>
	<% if (paymentUI.isElementVisible(paymentUI.ONECARD_CVV)) { %>
	<tr>
		<td class="identifier">
			<label 
				for="nolog_cardcvv[<%=paymentUI.oneLine%>]">
				<isa:translate 
					key="payment.card.cvv"/>
			</label>
		</td>
		<td class="value">
			<input 
				class="textInput" 
				type="text" 
				<%=paymentUI.getFieldReadOnly(paymentUI.ONECARD_CVV)%>
				<%=paymentUI.getAllDisabled()%>
			    size="6"
			    name="nolog_cardcvv[<%=paymentUI.oneLine%>]" 
			    id="nolog_cardcvv[<%=paymentUI.oneLine%>]" 
			    <% if (paymentUI.payment != null && payCards.size() > 0 && ((PaymentCCard) payCards.get(0)).getCVV() != null) { 
                       paymentUI.addSalesDocLastEnteredCVVS(0, ((PaymentCCard) payCards.get(0)).getCVV());
                 %>
			    value="<%=paymentUI.getCvvVal((PaymentCCard) (payCards.get(0)))%>" />
			    <% } %>
		</td>
	</tr>
	<% } %>
	<%-- Serialnumber for switch cards --%>
	<% if (paymentUI.isElementVisible(paymentUI.ONECARD_SUFFIX)) { %>
	<tr>
		<td class="identifier">
			<label 
				for="nolog_cardno_suffix[<%=paymentUI.oneLine%>]">
				<isa:translate 
					key="payment.card.number.suffix"/>
			</label>
		</td>
		<td class="value">
			<input 
				class="textInput" 
				type="text" 
				<%=paymentUI.getFieldReadOnly(paymentUI.ONECARD_SUFFIX)%>
				<%=paymentUI.getAllDisabled()%>
			    size="6"
			    name="nolog_cardno_suffix[<%=paymentUI.oneLine%>]" 
			    id="nolog_cardno_suffix[<%=paymentUI.oneLine%>]" 
			    <% if (paymentUI.payment != null && payCards.size() > 0 && ((PaymentCCard) payCards.get(0)).getNumberSuffix() != null) { %>
			    value="<%=((PaymentCCard) payCards.get(0)).getNumberSuffix()%>" />
			    <% } %>
		</td>
	</tr>
	<% } %>
    <%-- Serialnumber end --%>	
	<% if (paymentUI.isElementVisible(paymentUI.ONECARD_VALIDITY)) { %>       
	<tr>
		<td class="identifier">
			<isa:translate 
				key="payment.card.expd.txt"/>
		</td>
		<td class="value">
			<label 
				for="nolog_cardmonth[<%=paymentUI.oneLine%>]">
				<isa:translate 
					key="payment.card.expd.month"/>
			</label>
			<% if (paymentUI.isMaintainable(paymentUI.ONECARD_MONTH)) { %>
			<select 
				name="nolog_cardmonth[<%=paymentUI.oneLine%>]"
				id="nolog_cardmonth[<%=paymentUI.oneLine%>]"
				<%=paymentUI.getAllDisabled()%>>
				<isa:iterate 
					id="month" 
					name="<%=PaymentActions.CARDMONTHS%>" 
					type="java.lang.String">
					<option 
						<%=paymentUI.getCardMonthSelected(month)%>
						value="<%=month%>">
						<%=month%>
					</option>
				</isa:iterate>
			</select>
			<% } else { %>
			<% if (paymentUI.payment != null && payCards.size() > 0 && ((PaymentCCard) payCards.get(0)).getExpDateMonth() != null) { %>
            <%=((PaymentCCard) payCards.get(0)).getExpDateMonth()%>
			<input 
				type="hidden" 
			    size="2" 
			    name="nolog_cardmonth[<%=paymentUI.oneLine%>]"
			    id="nolog_cardmonth[<%=paymentUI.oneLine%>]"
			    value="<%=((PaymentCCard) payCards.get(0)).getExpDateMonth()%>"/>
			 <% } %>
			<% } %>
			<label 
				for="nolog_cardyear[<%=paymentUI.oneLine%>]">
				<isa:translate 
					key="payment.card.expd.year"/>
			</label>
			<% if (paymentUI.isMaintainable(paymentUI.ONECARD_YEAR)) { %>
			<select 
				name="nolog_cardyear[<%=paymentUI.oneLine%>]"
				id="nolog_cardyear[<%=paymentUI.oneLine%>]"
				<%=paymentUI.getAllDisabled()%>>
				<isa:iterate 
					id="year" 
					name="<%=PaymentActions.CARDYEARS%>" 
					type="java.lang.String">
					<option 
						<%=paymentUI.getCardYearSelected(year)%>
						value="<%=year%>">
						<%=year%>
					</option>
				</isa:iterate>
			</select>
			<% } else { %>
			<% if (paymentUI.payment != null && payCards.size() > 0 && ((PaymentCCard) payCards.get(0)).getExpDateYear() != null) { %>
			<%=((PaymentCCard) payCards.get(0)).getExpDateYear()%>
			<input 
				type="hidden" 
			     size="2" 
			     name="nolog_cardyear[<%=paymentUI.oneLine%>]" 
			     id="nolog_cardyear[<%=paymentUI.oneLine%>]" 
			     value="<%=((PaymentCCard) payCards.get(0)).getExpDateYear()%>"/>
			 <% } %>
			<% } %>
		</td>
	</tr>
	<% } %>
	</table>  
	<% } else { %>
             <table 
	               class="list-simple" 
	               summary="<isa:translate key="payment.acc.maintenance"/>">			
				<tr>
				    <% if (paymentUI.isElementVisible(paymentUI.CARD_TYPE)) { %>
					<th class="type" rowspan="2">
						<isa:translate key="payment.card.type"/>
					</th>
					<% } %>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_HOLDER)) { %>
					<th class="holder" rowspan="2">
						<isa:translate key="payment.card.holder"/>
					</th>
					<% } %>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_NUMBER)) { %>
					<th class="number" rowspan="2">
						<isa:translate key="payment.card.number"/><% if (paymentUI.isElementVisible(paymentUI.CARD_CVV)) { %>-<isa:translate key="payment.card.cvv"/><% } %><% if (paymentUI.isElementVisible(paymentUI.CARD_SUFFIX)) { %>-<isa:translate key="payment.card.number.suffix"/><% } %>	
					</th>
					<% } %>			
					<% if (paymentUI.isElementVisible(paymentUI.CARD_VALIDITY)) { %> 
					<th class="validity">
						<isa:translate key="payment.card.expd.txt"/>
					</th>
					<% } %>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_LIMITAMOUNT)) { %>
					<th class="amount" rowspan="2">
						<isa:translate key="payment.card.limit.maxamount"/>
					</th>
					<% } %>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_DELETEFLAG)) { %>
					<th class="flag" rowspan="2">
						<%--<isa:translate key="payment.card.chckb.delete.short"/> --%>
						<img src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/trash_icon.gif")%>" alt="<isa:translate key="payment.card.chckb.delete.short"/>" <% if (paymentUI.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>"  class="display-image"/> 						
					</th>
					<% } %>

				</tr>
				<tr>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_VALIDITY)) { %>
					<th class="validity">
					    <isa:translate key="payment.card.expd.txt2"/>					   
					</th>
					<% } %>
				</tr>				
				<% paymentUI.initLine(); %>
				<% if (paymentUI.paymentCardsExist()) { %>
				<isa:iterate 
					id="card" 
					name="<%=ActionConstantsBase.RC_PAYMENT_CARDS%>" 
					type="com.sap.isa.payment.businessobject.PaymentCCard">
					<tr>
						<% paymentUI.setPaymentCard((PaymentCCardData) card); %>
						<%-- credit card tech key--%>
						<% if (paymentUI.payment != null && card.getTechKey() != null) { %>						 
						<input
							type="hidden"
							name="nolog_cardtechkey[<%=paymentUI.line%>]"
							id="nolog_cardtechkey[<%=paymentUI.line%>]"
							value="<%=card.getTechKey() %>" />							
						<% } %>	
						<%-- credit card institutes--%>
						<% if (paymentUI.isElementVisible(paymentUI.CARD_TYPE)) { %>
						<td class="type">
							<% if (paymentUI.isMaintainable(paymentUI.CARD_TYPE) && paymentUI.isElementEnabled("payment.card.type", card.getTechKey().getIdAsString())) { %>
							<select 
								name="nolog_cardtype[<%=paymentUI.line%>]"
								id="nolog_cardtype[<%=paymentUI.line%>]"
								<%=paymentUI.getAllDisabled()%>>
								<option 
									value="">
									<isa:translate 
										key="b2c.order.chckt.selectopt.sel"/>
								</option>
								<isa:iterate 
									id="cardType" 
									name="<%=PaymentActions.CARDTYPES%>" 
									type="com.sap.isa.core.util.table.ResultData">
									<option 
										<%=paymentUI.getCardTypeSelected(cardType.getRowKey().toString())%>
										value="<%=cardType.getRowKey()%>">
										<%= JspUtil.encodeHtml(cardType.getString(1)) %>
									</option>
								</isa:iterate>
							</select>
							<% } else { %>
							<select 
								name="nolog_cardtype[<%=paymentUI.line%>]_disabled"
								disabled="disabled" >
								<option>
									<%= JspUtil.encodeHtml(card.getTypeDescription()) %>
								</option>								
							</select>
							<input
								type="hidden" 
							    name="nolog_cardtype[<%=paymentUI.line%>]"
							    id="nolog_cardtype[<%=paymentUI.line%>]"
							    value="<%= card.getTypeTechKey() %>" />
							<% } %>
							
						</td>
						<% } %>
						<%-- credit card owner --%>
						<% if (paymentUI.isElementVisible(paymentUI.CARD_HOLDER)) { %>
						<td class="holder">
							<% if (paymentUI.isElementEnabled("payment.card.holder", card.getTechKey().getIdAsString())) {%>
							<input 
								class="textInput" 
								type="text" 
								<%=paymentUI.getFieldReadOnly(paymentUI.CARD_HOLDER)%>
								<%=paymentUI.getAllDisabled()%>
							    name="nolog_cardholder[<%=paymentUI.line%>]"
							    id="nolog_cardholder[<%=paymentUI.line%>]"
							    value="<%= JspUtil.encodeHtml(card.getHolder()) %>" />
							 <% }else{ %>
							 	<%= JspUtil.encodeHtml(card.getHolder()) %><input type="hidden" name="nolog_cardholder[<%=paymentUI.line%>]" id="nolog_cardholder[<%=paymentUI.line%>]" value="<%= JspUtil.encodeHtml(card.getHolder()) %>" />
							 <% } %>
						</td>
						<% } %>
						<%-- credit card number --%>
						<% if (paymentUI.isElementVisible(paymentUI.CARD_NUMBER)) { %>
						<td class="number">
							<% if (paymentUI.isElementEnabled("payment.card.holder", card.getTechKey().getIdAsString())) {%>
							<input 
								class="cardnumber" 
								type="text" 
								<%=paymentUI.getFieldReadOnly(paymentUI.CARD_NUMBER)%>
								<%=paymentUI.getAllDisabled()%>
							    name="nolog_cardno[<%=paymentUI.line%>]"
							    id="nolog_cardno[<%=paymentUI.line%>]"
							    value="<%=card.getNumber()%>" />
							 <% }else{ %>
							 	<%= JspUtil.encodeHtml(card.getNumber()) %><input type="hidden" name="nolog_cardno[<%=paymentUI.line%>]" id="nolog_cardno[<%=paymentUI.line%>]" value="<%= JspUtil.encodeHtml(card.getNumber()) %>" />
							 <% } %>
							
						<%-- credit card cvv --%>
						<% if (paymentUI.isElementVisible(paymentUI.CARD_CVV)) { 
                               paymentUI.addSalesDocLastEnteredCVVS(paymentUI.line, card.getCVV());
                               
                         %>	
                         	<% if (paymentUI.isElementEnabled("payment.card.cvv", card.getTechKey().getIdAsString())) {%>						 
							-<input 
								class="cvv" 
								type="text" 
								maxlength="4"
								<%=paymentUI.getFieldReadOnly(paymentUI.CARD_CVV)%>
								<%=paymentUI.getAllDisabled()%>
							    name="nolog_cardcvv[<%=paymentUI.line%>]"
							    id="nolog_cardcvv[<%=paymentUI.line%>]"
							    value="<%= paymentUI.getCvvVal(card) %>" />	
							<% }else{ %>
							 	<%= JspUtil.encodeHtml(paymentUI.getCvvVal(card)) %><input type="hidden" name="nolog_cardcvv[<%=paymentUI.line%>]" id="nolog_cardcvv[<%=paymentUI.line%>]" value="<%= JspUtil.encodeHtml(paymentUI.getCvvVal(card)) %>" />
							 <% } %>  
						<% } %>
						
						<%-- credit card serial number --%>
						<% if (paymentUI.isElementVisible(paymentUI.CARD_SUFFIX)) { %>
							<% if (paymentUI.isElementEnabled("payment.card.number.suffix", card.getTechKey().getIdAsString())) {%>
							-<input 
								class="serial" 
								type="text" 
								<%=paymentUI.getFieldReadOnly(paymentUI.CARD_SUFFIX)%>
								<%=paymentUI.getAllDisabled()%>
							    name="nolog_cardno_suffix[<%=paymentUI.line%>]"
							    id="nolog_cardno_suffix[<%=paymentUI.line%>]"
							    value="<%=card.getNumberSuffix()%>" />
							 <% }else{ %>
							 	<%= JspUtil.encodeHtml(card.getNumberSuffix()) %><input type="hidden" name="nolog_cardno_suffix[<%=paymentUI.line%>]" id="nolog_cardno_suffix[<%=paymentUI.line%>]" value="<%= JspUtil.encodeHtml(card.getNumberSuffix()) %>" />
							 <% } %>					 
						<% } %>	
						</td>
						<% } %>					
						<%-- credit card month --%>
						<% if (paymentUI.isElementVisible(paymentUI.CARD_VALIDITY)) { %>       
						<td class="validity">		
							<% if (paymentUI.isMaintainable(paymentUI.CARD_MONTH) && paymentUI.isElementEnabled("payment.card.validity", card.getTechKey().getIdAsString())) { %>
							<select 
								name="nolog_cardmonth[<%=paymentUI.line%>]"
								id="nolog_cardmonth[<%=paymentUI.line%>]"
								<%=paymentUI.getAllDisabled()%>>
								<isa:iterate 
									id="month" 
									name="<%=PaymentActions.CARDMONTHS%>" 
									type="java.lang.String">
									<option 
										<%=paymentUI.getCardMonthSelected(month)%>
										value="<%=month%>">
										<%=month%>
									</option>
								</isa:iterate>
							</select>
							<% } else { %>	
							<select 
								name="nolog_cardmonth[<%=paymentUI.line%>]_disabled"
								disabled="disabled">
								<option> 
									<%=card.getExpDateMonth()%>
								<option>								
							</select>							
							<input 
								type="hidden" 
							    name="nolog_cardmonth[<%=paymentUI.line%>]"
							    id="nolog_cardmonth[<%=paymentUI.line%>]"
							    value="<%=card.getExpDateMonth()%>" />
							<% } %>	
						    <%-- credit card year --%>					 
							<% if (paymentUI.isMaintainable(paymentUI.CARD_YEAR) && paymentUI.isElementEnabled("payment.card.validity", card.getTechKey().getIdAsString())) { %>
							<select 
								name="nolog_cardyear[<%=paymentUI.line%>]"
								id="nolog_cardyear[<%=paymentUI.line%>]"
								<%=paymentUI.getAllDisabled()%>>
								<isa:iterate 
									id="year" 
									name="<%=PaymentActions.CARDYEARS%>" 
									type="java.lang.String">
									<option 
										<%=paymentUI.getCardYearSelected(year)%>
										value="<%=year%>">
										<%=paymentUI.getYearShort(year)%>
									</option>
								</isa:iterate>
							</select>
							<% } else { %>
							<select 
								name="nolog_cardyear[<%=paymentUI.line%>]_disabled"
								disabled="disabled" >
								<option> 
									<%=paymentUI.getYearShort(card.getExpDateYear())%>	
								<option>								
							</select>
							<input 								 
								type="hidden" 
							    name="nolog_cardyear[<%=paymentUI.line%>]"
							    id="nolog_cardyear[<%=paymentUI.line%>]"
							    value="<%=card.getExpDateYear()%>" />
							<% } %>                     					    
						</td>
						<% } %>
						<%-- credit card auth. limited --%>
						<%-- credit card auth. limit amount --%>
						<% if (paymentUI.isElementVisible(paymentUI.CARD_LIMITAMOUNT)) { %>
						<td class="amount">
							<% if (paymentUI.isElementEnabled("payment.card.limit.amount", card.getTechKey().getIdAsString())) {%>
							<input 
								class="textInput" 
								type="text"
								maxlength="12"
								<%=paymentUI.getFieldReadOnly(paymentUI.CARD_LIMITAMOUNT)%>
								<%=paymentUI.getAllDisabled()%>
							    name="authlimit[<%=paymentUI.line%>]"
							    id="authlimit[<%=paymentUI.line%>]"
							    value="<%=JspUtil.removeNull(card.getAuthLimit())%>"
							 />
							 <% }else{ %>
							 	<%= JspUtil.encodeHtml(card.getAuthLimit()) %><input type="hidden" name="authlimit[<%=paymentUI.line%>]" id="authlimit[<%=paymentUI.line%>]" value="<%= JspUtil.encodeHtml(card.getAuthLimit()) %>" />
							 <% } %>
						</td>
						<% } %>
						<%-- credit card delete flag --%>
						<% if (paymentUI.isElementVisible(paymentUI.CARD_DELETEFLAG)) { %>
						<td class="flag">
							<% if (paymentUI.isMaintainable(paymentUI.CARD_DELETEFLAG) && paymentUI.isElementEnabled("payment.card.chckb.delete", card.getTechKey().getIdAsString())) { %>
							<input 
								type="checkbox" 
								<%=paymentUI.getAllDisabled()%>
								name="deleteflag[<%=paymentUI.line%>]" 
								id="deleteflag[<%=paymentUI.line%>]" 
								value="delete" 
							/>
							<% } else {%>
							<input 
								type="checkbox" 
								<%=paymentUI.getAllDisabled()%>
								name="deleteflag[<%=paymentUI.line%>]" 
								id="deleteflag[<%=paymentUI.line%>]" 
								value="delete" 
								disabled="disabled"
							/>
							<% } %>
						</td> 
						<% } %>

					</tr>
					<% paymentUI.addLine(); %>
				</isa:iterate>
				<% } %>
				<% for (int i = 0; i < paymentUI.getNewLines(); i++) { %>
				<tr>
					<%-- credit card institutes--%>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_TYPE)) { %>
					<td class="type">
						<select 
							name="nolog_cardtype[<%=paymentUI.line%>]" 
							id="nolog_cardtype[<%=paymentUI.line%>]" 
							<%=paymentUI.getAllDisabled()%>
						>
							<option 
								value="">
								<isa:translate 
									key="b2c.order.chckt.selectopt.sel"/>
							</option>
							<isa:iterate 
								id="cardType" 
								name="<%=PaymentActions.CARDTYPES%>" 
								type="com.sap.isa.core.util.table.ResultData">
								<option 
									value="<%=cardType.getRowKey()%>">
									<%= JspUtil.encodeHtml(cardType.getString(1)) %>
								</option>
							</isa:iterate>
						</select>
					</td>
					<% } %>
					<%-- credit card owner --%>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_HOLDER)) { %>
					<td class="holder">
						<input 
							class="textInput" 
							type="text" 
							<%=paymentUI.getAllDisabled()%>
							name="nolog_cardholder[<%=paymentUI.line%>]" 
							id="nolog_cardholder[<%=paymentUI.line%>]" 
						/>
					</td>
					<% } %>
					<%-- credit card number --%>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_NUMBER)) { %>
					<td class="number">
						<input 
							class="cardnumber" 
							type="text" 
							<%=paymentUI.getAllDisabled()%>
							name="nolog_cardno[<%=paymentUI.line%>]" 
							id="nolog_cardno[<%=paymentUI.line%>]" 
						/>
					<%-- credit card cvv --%>
					 <% if (paymentUI.isElementVisible(paymentUI.CARD_CVV)) { %>
						-<input 
							class="cvv" 
							type="text"
							maxlength="4" 
							<%=paymentUI.getAllDisabled()%>
							name="nolog_cardcvv[<%=paymentUI.line%>]" 
							id="nolog_cardcvv[<%=paymentUI.line%>]" 
						/>
					<% } %>
					<%-- credit card serial number --%>
					   <% if (paymentUI.isElementVisible(paymentUI.CARD_SUFFIX)) { %>
						-<input 
							class="serial" 
							type="text" 
							<%=paymentUI.getAllDisabled()%>
							name="nolog_cardno_suffix[<%=paymentUI.line%>]" 
							id="nolog_cardno_suffix[<%=paymentUI.line%>]" 
						/>
					   <% } %>	
					</td>							
					<% } %>								
					<%-- credit card month --%>
					 <% if (paymentUI.isElementVisible(paymentUI.CARD_VALIDITY)) { %>      
					 <td class="validity">					 
					    <%-- credit card month --%>
						<select 
							name="nolog_cardmonth[<%=paymentUI.line%>]" 
							id="nolog_cardmonth[<%=paymentUI.line%>]" 
							<%=paymentUI.getAllDisabled()%>
						>
							<isa:iterate 
								id="month" 
								name="<%=PaymentActions.CARDMONTHS%>" 
								type="java.lang.String">
								<option 
									value="<%=month%>">
									<%=month%>
								</option>
							</isa:iterate>
						</select>
					    <%-- credit card year --%>
						<select 
							name="nolog_cardyear[<%=paymentUI.line%>]" 
							id="nolog_cardyear[<%=paymentUI.line%>]" 
							<%=paymentUI.getAllDisabled()%>
						>
							<isa:iterate 
								id="year" 
								name="<%=PaymentActions.CARDYEARS%>" 
								type="java.lang.String">
								<option 
									value="<%=year%>">
									<%=paymentUI.getYearShort(year)%>
								</option>
							</isa:iterate>
						</select>	
					</td>
					<% } %>
					<%-- credit card auth. limited --%>
					<%-- credit card auth. limit amount --%>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_LIMITAMOUNT)) { %>
					<td class="amount">
						<input 
							class="textInput" 
							type="text" 
							maxlength="12"
							<%=paymentUI.getFieldReadOnlyNewLine(paymentUI.CARD_LIMITAMOUNT)%>
							<%=paymentUI.getAllDisabled()%>
							name="authlimit[<%=paymentUI.line%>]" 
							id="authlimit[<%=paymentUI.line%>]" 
						/>
					</td>
					<% } %>
					<%-- credit card delete flag --%>
					<% if (paymentUI.isElementVisible(paymentUI.CARD_DELETEFLAG)) { %>
					<td class="flag">
						<input 
							type="checkbox" 
							<%=paymentUI.getAllDisabled()%>
							name="deleteflag[<%=paymentUI.line%>]" 
							id="deleteflag[<%=paymentUI.line%>]" 
							value="delete"
						/>
					</td>
					<% } %>

				</tr>
				<% paymentUI.addLine(); %>
				<% } %>
		    </table>		
	<% } %>

<%-- Credit card details End --%>
