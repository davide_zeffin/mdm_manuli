<%--
********************************************************************************
    File:         campaignsNewItem.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.03.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/03/18 $
********************************************************************************
--%>
	<% if (ui.isManualCampaignEntryAllowed() && ui.isElementVisible("order.item.campaign") && ui.isElementEnabled("order.item.campaign")) { %>
		  <tr>
			 <td class="identifier">
				 <label for="campId[<%= ui.line %>][0]"><isa:translate key="b2b.order.disp.camp"/></label>
			 </td>
		     <% if (ui.isMultipleCampaignsAllowed()) { %>
             <td class="campimg-1">
                 <a class="icon" href="javascript:toggleCampaignRows('itemcampicon<%= ui.line %>', 'itemcamp<%= ui.line %>_', '5')" ><img id="itemcampicon<%= ui.line %>" src=<%=WebUtil.getMimeURL(pageContext,"mimes/images/open.gif")%> alt="<isa:translate key="camp.toggle.exp"/>"  /></a>
             </td>
			 <% } %>   
			 <td class="value">
				 <input type="text" class="textinput-large" maxlength="24" name="campId[<%= ui.line %>][0]" id="campId[<%= ui.line %>][0]"
						   value=""/>
			 </td>
		  </tr>
   	   <% if (ui.isMultipleCampaignsAllowed()) {
              for (int iNewItemCamp = 1; iNewItemCamp < 5; iNewItemCamp++) { %>
              <tr id="itemcamp<%= ui.line %>_<%= iNewItemCamp %>" style="display: none">
                 <td class="identifier"></td>
                 <% if (ui.isMultipleCampaignsAllowed()) { %>
                 <td class="campimg-1"></td>
                 <% } %>  
                 <td class="value">
                     <input type="text" class="textinput-large" maxlength="24" name="campId[<%= ui.line %>][<%= iNewItemCamp %>]" id="campId[<%= ui.line %>][<%= iNewItemCamp %>]"
                               value=""/>
                 </td>
              </tr>
            <% } %>
              <tr id="itemcamp<%= ui.line %>_5" style="display: none">
                 <td class="identifier"></td>
                 <% if (ui.isMultipleCampaignsAllowed()) { %>
                 <td class="campimg-1"></td>
                 <% } %>  
                 <td class="value">
                     <a href="#" onclick="submit_refresh();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.lnk.morecamps"/>" <% } %> ><isa:translate key="b2b.lnk.morecamps"/></a>
                 </td>
              </tr>
        <% }
	     } %>
