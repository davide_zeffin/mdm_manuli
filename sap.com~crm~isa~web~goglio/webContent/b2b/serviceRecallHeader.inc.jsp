<%--
********************************************************************************
    File:         serviceRecallHeader.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      13.06.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/05/13 $
********************************************************************************
--%>
    
<% String extRefObjectType = "";
   if (ui.header.getExtRefObjectType() != null && ui.header.getExtRefObjectType().length() > 0) { 
	extRefObjectType = ui.header.getExtRefObjectTypeDesc();
   }
%>
	<tr>
	   <isa:translate key="b2b.recallorderinfo.msg" arg0="<%= extRefObjectType %>"/>
	</tr>     
	<tr>
	  <% if (ui.isElementVisible("order.recallId")){ %>
	    <td class="identifier">
	          <label for="headRecallId"><isa:translate key="b2b.order.disp.recall"/><br><isa:translate key="b2b.order.mandatory"/></label>
	    </td>
	  <% } %>
	  <% if (ui.isElementVisible("order.recallId")){ %>
		<td class="value">
		    <% if (ui.isElementEnabled("order.recallId")){ %>		    
			    <input type="text" class="textinput-middle" maxlength="10" name="headRecallId" id="headRecallId" value="<%= JspUtil.encodeHtml(ui.header.getRecallId()) %>"/>&nbsp;
			              <%= JspUtil.encodeHtml(ui.header.getRecallDesc()) %>
			<% } else { %>
			    <input type="hidden" maxlength="10" name="headRecallId" id="headRecallId" value="<%= JspUtil.encodeHtml(ui.header.getRecallId()) %>"/>&nbsp;
			              <%= JspUtil.encodeHtml(ui.header.getRecallDesc()) %>
			<% } %>
        </td>
      <% } %>		    	    
	</tr>
	
<%@ include file="/b2b/extrefobjectheader.inc.jsp" %>



