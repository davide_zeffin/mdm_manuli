<%--
********************************************************************************
    File:         incoterms.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.03.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/03/18 $
********************************************************************************
--%>
    <% if ((ui.isElementVisible("order.incoTerms1") || ui.isElementVisible("order.incoTerms2")) && ui.showIncoterms()) { %>
	    <tr>
	    	<td class="identifier">
	    		<label for="incoterms1"><isa:translate key="b2b.order.disp.incoterms1"/></label>            
	    	</td>
            <% if (ui.showHeaderToggleMultipleCampaignData()) { %>
			<td class="campimg-1"></td>
			<% } %>
	    	<td class="value" colspan="<%= ui.getNonCampHeaderDetailColspan() %>">
    	       <%-- Incoterms 1 --%>
    	       <% if (ui.isElementVisible("order.incoTerms1")) { %>
		    	   <% if (ui.isElementEnabled("order.incoTerms1")){ %>
			    	   <input name="incoterms1" id="incoterms1" maxlength="3" class="textinput-small" type="text" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.disp.incoterms"/><isa:translate key="b2b.acc.poss.entries.tab.ret"/>" <% } %>
			    	   	 value="<%= ui.header.getIncoTerms1() %>"/>
				       <%= HelpValuesSearchUI.getJSCallPopupCoding("IncoTerms1","order_positions","",pageContext) %>
				   <% } else { %>
				       <input name="incoterms1" id="incoterms1" type="hidden" value="<%= ui.header.getIncoTerms1() %>"/><%= ui.header.getIncoTerms1() %>
				   <% } %>
			   <% } else { %>
    			   <input name="incoterms1" id="incoterms1" maxlength="3" type="hidden" value="<%= ui.header.getIncoTerms1() %>"/>
    			   
	           <% } %>
	    	   <% if (ui.header.getIncoTerms1Desc() != null && ui.header.getIncoTerms1Desc().length() > 0) { %>
                     <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<%= JspUtil.replaceSpecialCharacters(ui.header.getIncoTerms1Desc()) %>"/>
               <% } %>
               <%-- Incoterms 2 --%>
   	    	   <% if (ui.isElementVisible("order.incoTerms2")) { %>
   	    	       <% if (ui.isElementEnabled("order.incoTerms2")) { %>
		    	      <input name="incoterms2" id="incoterms2" maxlength="28" class="textinput-large" type="text" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.disp.incoterms2"/>" <% } %>
		    	   	        value="<%= JspUtil.replaceSpecialCharacters(ui.header.getIncoTerms2()) %>"/>		    	       
		    	   <% } else { %>
    		    	   <input name="incoterms2" id="incoterms2" type="hidden" value="<%= ui.header.getIncoTerms2() %>"/><%= ui.header.getIncoTerms2() %>
	    	       <% } %>
		           <% if (!ui.isAccessible) { %>	
		    	       <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.order.disp.incoterms2"/>"/>
		    	   <% } %>
		    	<% } else { %>
		    	   <input name="incoterms2" id="incoterms2" readonly type="hidden" maxlength="28" type="hidden" value="<%= ui.header.getIncoTerms2() %>"/>
		    	   <%= ui.header.getIncoTerms2() %>
		    	<% } %>
	    	</td>
	    </tr>
    <% } %>