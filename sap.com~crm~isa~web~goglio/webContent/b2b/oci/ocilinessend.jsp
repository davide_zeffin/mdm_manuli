<%--
********************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      29.05.2001

    $Revision: #2 $
    $Date: 2004/05/05 $
********************************************************************************
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.businessobject.oci.*" %>
<%@ page import="com.sap.isa.isacore.action.oci.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.oci.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%@ include file="/b2b/checksession.inc" %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
    <head>
    	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="b2b.htmltitel.standard"/></title>

    	<isa:stylesheets/>
    
    </head>

    <body class="hints" onload="document.SUBMITFORM.submit()">
    <div class="module-name"><isa:moduleName name="b2b/oci/ocilinessend.jsp" /></div>
    <div id="nodoc-header">
        <div><span></span></div>
    </div>
    <% if(request.getAttribute(OciLinesSendAction.OCI_VERSION).equals(OciVersion.VERSION_2_0.toString())) { %>
        <p><span><isa:translate key="oci.xmlTransfer"/></span></p>
    <% } else { %>
        <p><span><isa:translate key="oci.htmlTransfer"/></span></p>
    <% } %>

    <%-- send list with oci lines --%>
    <form action="<%= request.getAttribute(OciLinesSendAction.OCI_HOOK_URL)%>" method="post" name="SUBMITFORM" target="<%=request.getAttribute(OciLinesSendAction.OCI_TARGET)%>">
        <div>
	<% String param = (String)request.getAttribute(OciLinesSendAction.OCI_OKCODE); %>
	<% if (param != null) { %>
    	    <input type="hidden" name="<%=OciLinesSendAction.OCI_OKCODE%>" value="<%=param%>"/>
	<% } %>
	<% param = (String)request.getAttribute(OciLinesSendAction.OCI_CALLER); %>
	<% if (param != null) { %>
    	    <input type="hidden" name="<%=OciLinesSendAction.OCI_CALLER%>" value="<%=param%>"/>
	<% } %>
	<% OciLineList ociLineList =
    	    (OciLineList)pageContext.findAttribute(OciLinesSendAction.OCI_LINE_LIST); %>
	    <% for (int i=0; i < ociLineList.size(); i++) { %><%=ociLineList.get(i)+"</input>\n"%><% } %>
	</div>
    </form>
  </body>
</html>