<%/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      12 May 2001

    $Revision: #2 $
    $Date: 2004/05/05 $
*****************************************************************************/%>
<%@ page import="com.sap.isa.isacore.action.oci.*" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%@ include file="/b2b/checksession.inc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
    <head>
    	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="b2b.htmltitel.standard"/></title>
	
	<script type="text/javascript">
	    <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>		
	
        <script src='<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>'
            type="text/javascript"></script>
	
        <script type="text/javascript">
	    isaTop().location.href = '<isa:webappsURL name="/b2b/ocilinessend.do"/>'
        </script>
    </head>
<body></body>
</html>