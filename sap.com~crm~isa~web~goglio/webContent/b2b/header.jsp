<%-- import the taglibs used on this page --%>

<%--
        HTML/Design changes by: michael.oertel@pixelpark.com
--%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.*" %>

<%@ page import="com.sap.isa.isacore.DocumentHandler" %>
<%@ page import="com.sap.isa.core.SessionConst" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%
        HeaderUI ui = new HeaderUI(pageContext);

       /* get user session data object */
       UserSessionData userSessionData =
          UserSessionData.getUserSessionData(request.getSession());

       DocumentHandler documentHandler = (DocumentHandler)
          userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
       String accessText = "";
       
//GREENORANGE BEGIN
	//get Internet Sales core functionality BOM
	BusinessObjectManager isaBOM =
		(BusinessObjectManager) userSessionData.getBOM(
			BusinessObjectManager.ISACORE_BOM);

	String current_user = isaBOM.getUser().getUserId().toUpperCase();	
	String USER_GUEST = (String)userSessionData.getAttribute("user_guest");
//GREENORANGE END
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
        <head>
                <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title>SAP Internet Sales B2B - Header</title>

                <isa:stylesheets/>

                <%
                if (ui.isAccessible())
                {
                %>
                <%@ include file="/appbase/jscript/accesskey.js.inc.jsp" %>
                <%
                }
                %>
                <script type="text/javascript">
                        <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
                </script>

                <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
                        type="text/javascript">
                </script>

                <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
                        type="text/javascript">
                </script>

                <script type="text/javascript">
                <!--
                        <%-- function busy(frameObject) --%>
                        <%@ include file="/b2b/jscript/busy.js" %>


                        <%-- flag indicates currently selected view; used to avoid unneccessary refreshments of navigator frame from e.g. history --%>
                        <%-- value: true = document view / false = catalog view --%>
                        var isDocumentView = <%= (documentHandler.isCatalogOnTop() ? "false" : "true") %>;
                        <%-- variable for user settings window --%>
                        var win_user_settings;

                        <%-- open new user settings window --%>
                        function userSettings() {
                                win_user_settings = window.open('<isa:webappsURL name="/b2b/fs_user_settings.jsp"/>', 'User_Settings', 'width=900,height=700,scrollbars=1,resizable=yes');
                                win_user_settings.focus();
                        }


                        function help() {
                                 win_help = window.open('<isa:webappsURL name="/ecall/jsp/customer/common/frameset_help.jsp"/>', 'Hilfe', 'width=700,height=550,scrollbars=yes,resizable=yes');
                                 win_help.focus();
                        }

                        function lwc() { <%-- Live Web Collaboration --%>
                                 win_lwc = window.open('<isa:webappsURL name="/ecall/jsp/customer/common/frameset_help.jsp?pageType=support"/>', 'lwc', 'width=700,height=550,scrollbars=yes,resizable=yes');
                                 win_lwc.focus();
                        }

                        function openStoreLoc() { <%-- Store Locator --%>
                                 win_storeloc = window.open('<isa:webappsURL name="/dealerlocator/searchdealer.do"/>?popupWin=true', 'storeLocator', 'width=700,height=600,scrollbars=yes,resizable=yes');
                                 win_storeloc.focus();
                        }


                        <%-- switch to catalog view --%>
                        function show_catalog_view() {
                                 isDocumentView = false;
                                 isaTop().parent.location.href="<isa:webappsURL name="/b2b/catalogstart.do"/>";
                        }

                        <%-- switch to document view --%>
                        function show_document_view() {
                                 isDocumentView = true;
                                 isaTop().parent.location.href="<isa:webappsURL name="/b2b/catalogend.do"/>";
                        }

                        function logoff() {

                                if(win_user_settings != null && win_user_settings.closed == false) {
                                        win_user_settings.close();
                                }
                                ret = true;
                                if ( parent.work_history != null && 
                                     parent.work_history.documents != null && 
                                     parent.work_history.documents.editableDoc==true) {
                                        ret=confirm('<isa:translate key="b2b.header.box"/>');
                                }
                                if (ret) {
                                    if ( parent.work_history != null && 
                                         parent.work_history.documents != null ) {
                                        busy(form_input());
                                    }
                                    isaTop().location.href="<isa:webappsURL name="/b2b/logoff.do"/>";
                                }
                        }

                        function switchAccessibility() {

                                if(win_user_settings != null && win_user_settings.closed == false) {
                                        win_user_settings.close();
                                }
                                ret = true;
                                if ( parent.work_history != null && 
                                     parent.work_history.documents != null && 
                                     parent.work_history.documents.editableDoc==true) {
                                        ret=confirm('<isa:translate key="b2b.header.box"/>');
                                }
                                if (ret) {
                                    <% if (ui.isAccessible()){ %>
                                        if (isaTop().parent != null)
                                        {
                                                isaTop().parent.location.href="<isa:webappsURL name="/base/switchaccessibility.do?sap-accessibility=NO"/>";
                                        } 
                                   <% }
                                      else { %>
                                        if (isaTop().parent != null)
                                        {
                                                isaTop().parent.location.href="<isa:webappsURL name="/base/switchaccessibility.do?sap-accessibility=X"/>";
                                        }
                                   <% } %>
                                }
                        }

                        function show_basket() {
                                 isDocumentView = true;
                                 // highlight_document_link();
                                 busy(form_input());
                                 isaTop().parent.location.href="<isa:webappsURL name="/b2b/catalogend.do"/>";
                                 //parent.organizer_nav.location.href="<isa:webappsURL name="/b2b/organizer-nav-doc-search.jsp"/>";
                                 //parent.organizer_content.location.href= "<isa:webappsURL name="/b2b/updateorganizercont.do"/>";                                 
                                 form_input().location.href="<isa:webappsURL name="/b2b/editabledocumentontop.do"/>";
                        }
                        
                        function show_basket_from_catalog() {
                                 isDocumentView = true;
                                 isaTop().parent.location.href="<isa:webappsURL name="/b2b/catalogend.do"/>";                                
                        }

                        function show_collective_order() {
                                 if (parent.work_history.documents.editableDoc==true) {
                                     alert('<isa:UCtranslate key="isacore.docnav.no.navigate"/>');
                                 } else {
                                     busy(form_input());
                                     form_input().location.href="<isa:webappsURL name="/b2b/createcollectiveorder.do"/>";
                                 }
                        }

                        function change() {
                                 <%-- does only work in IE --%>
                                 document.all.test.innerText="neu";
                        }

                        function openWinOCI( addURL ) {
                                  var url = addURL;
                                  var sat = window.open(url, 'OCICatalog', 'width=700,height=500,scrollbars=yes,resizable=yes');
                                  sat.focus();
                        }

                        <%-- set link text for Basket link --%>
                        function setBasketLinkText(text){
                          if (getElement("basketlinktext")) {
                            getElement("basketlinktext").firstChild.nodeValue = text;
                          }  
                          if (getElement("basketForm")) {
                            getElement("basketForm").miniBasket.value = text;
                          }      
                          if (getElement("basketpic")) {
                            getElement("basketpic").alt = text;
                          }  
                        }

                //-->
                </script>
  </head>

  <% User user = (User) request.getAttribute(BusinessObjectBase.CONTEXT_NAME); %>



<body class="header-body" onload="isaTop().headerPageloaded = true;">
<% if(ui.isPortal) { %>
        <div id="header-portal">
<% } else { %>
        <div id="header-appl">
<% } %>
                <div class="header-logo">
                </div>

                <div class="header-applname">
                   <isa:translate key="b2b.jsp.applicationName"/>
                </div>

                <div class="header-username">
                        <%= JspUtil.encodeHtml(user.getAddress().getTitle()) %>&nbsp;<%= JspUtil.encodeHtml(user.getAddress().getName()) %>
                </div>

                <%-- Accesskey for the header section. It must be an resourcekey, so that it can be translated. --%>
                <a id="access-header" href="#access-items" title="<isa:translate key="b2b.header.accesskey"/>" accesskey="<isa:translate key="b2b.header.accesskey.key"/>"></a>

                <%-- catalog document view ... --%>
                <% if(!ui.homActivated) { %>
                <%--hide "document" / "catalog" link in POM-scenario --%>
                        <% if (ui.isAccessible) { %>
                                <a href="#end-nav2" title="<isa:translate key="b2b.header.acc.endnav2"/>"></a>
                        <% } %>
						<% 
						accessText = WebUtil.translate(pageContext, "b2b.header.groupCatalog", null);
						if(ui.isAccessible())
						{
						%>
						<a 
							href="#b2b_header_groupCatalog_end"
							id="b2b_header_groupCatalog_begin"
							title="<isa:translate key="access.grp.begin" arg0="<%=accessText%>"/>" 
						></a>                    	
						<%
						}
						%>                
                        <div id="header-nav-view">
                                <ul>
		                                <%
										accessText = WebUtil.translate(pageContext, "b2b.header.mytransaction", null);
										%>
                                        <li class="first"><a href="#" onclick="show_document_view()" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                                                <span class="active"><isa:translate key="b2b.header.mytransaction"/></span></a></li>

                                        <%-- default setting 1st load - catalog view is inactive --%>
                                        <% if (ui.internalCatalogVisible) { %>
		                                <%
										accessText = WebUtil.translate(pageContext, ui.catalogLink, null);
										%>
                                                <li><a href="#" onclick="show_catalog_view()" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                                                        <span class="active"><isa:translate key="<%= ui.catalogLink %>"/></span></a></li>
                                        <% } %>

                                        <% if ((ui.ociEnable) && (!ui.internalCatalogVisible)) { %>
		                                <%
										accessText = WebUtil.translate(pageContext, "b2b.header.extern.catalog", null);
										%>
                                                <li><a href="#" onclick="openWinOCI('<%= JspUtil.encodeHtml(ui.externalCatalogURL) %>'); return false;" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                                                        <span class="active"><isa:translate key="b2b.header.extern.catalog"/></span></a></li>
                                        <% } %>
                                </ul>
                        </div>
						<%
						accessText = WebUtil.translate(pageContext, "b2b.header.groupCatalog", null); 
						if(ui.isAccessible())
						{
						%>
						<a 
							href="#b2b_header_groupCatalog_begin"
							id="b2b_header_groupCatalog_end"
							title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>" 
						></a>                    	
						<%
						}
						%>
                <% } %>

<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% if( !current_user.equalsIgnoreCase(USER_GUEST) ) { %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>					

                <%--... basket ... --%>
                <div id="header-nav-basket">
				  <%
        		  if (ui.isMiniBasketAvailable()) {%>
                  	<ul>
                                <% if (!ui.homActivated) {
										accessText = WebUtil.translate(pageContext, "b2b.header.minibasket.default", null); 
								%>
                                    <li class="first">
                                        <% if( ui.showCatalog()) { %>
                                            <a href="#" onclick="show_basket_from_catalog()" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_wk_blue_header.gif") %>" name="basketpic" alt="<isa:translate key="b2b.header.minibasket.default"/>" />
                                        <% }
                                           else { %>
                                            <a href="#" onclick="show_basket()" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_wk_blue_header.gif") %>" name="basketpic" alt="<isa:translate key="b2b.header.minibasket.default"/>" />
                                        <% } %>
                                            <span id="basketlinktext"><isa:translate key="b2b.header.minibasket.default"/></span></a>
                                    </li>
                                <% } 
                                   else { 
										accessText = WebUtil.translate(pageContext, "b2b.header.collorder.default", null);
										%>
                                        <li class="first">
                                                <a href="#" onclick="show_collective_order();"title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_wk_blue_header.gif") %>" name="basketpic" alt="<isa:translate key="b2b.header.collorder.default"/>" />
                                                <span id="basketlinktext"><isa:translate key="b2b.header.collorder.default"/></span></a>
                                        </li>
                                <% } %>
                        </ul>
                    <% } %>    
                </div>

<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
	<% } %>				
<%-- GREENORANGE END (showing only if user is not user_guest) --%>					

                <%-- ... functions ... --%>
				<% 
				accessText = WebUtil.translate(pageContext, "b2b.header.groupFunction", null);
				if(ui.isAccessible()) {
				%>
				<a 
					href="#b2b_header_groupFunction_end"
					id="b2b_header_groupFunction_begin"
					title="<isa:translate key="access.grp.begin" arg0="<%=accessText%>"/>" 
				></a>                    	
				<%
				}
				%>                
                <div id="header-nav-functions">
                        <ul>    
                                <li class="first">
                                    <a href="#" onclick="userSettings();" 
                                        <% if (ui.isAccessible()) { 
                                            accessText = WebUtil.translate(pageContext, "b2b.header.settings", null);
                                        %> 
                                        title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>"
                                        <% } %>
                                        >
                                        <span><isa:translate key="b2b.header.settings"/></span>
                                    </a>
                                </li>
                                <% if (ui.isStoreLocatorAvailable() && !ui.isPortal) {
								       accessText = WebUtil.translate(pageContext, "dealerlocator.jsp.title", null);
								%>
                                <li><a href="#" onclick="openStoreLoc();" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                                        <span><isa:translate key="dealerlocator.jsp.title"/></span></a></li>
                                <% } %>
                                <%
								accessText = WebUtil.translate(pageContext, "b2b.header.lwc", null);
								%>
                                <li><a href="#" onclick="lwc();" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                                        <span><isa:translate key="b2b.header.lwc"/></span></a></li>
                                <%
								accessText = WebUtil.translate(pageContext, "b2b.header.help", null);
								%>
                                <li><a href="#" onclick="help();" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                                        <span><isa:translate key="b2b.header.help"/></span></a></li>
                                <%
                            if(!ui.isPortal) {
                                String accessibilityMode = ui.isAccessible() ? "access.accessmode.off" : "access.accessmode.on";
                                accessibilityMode = WebUtil.translate(pageContext, accessibilityMode, null);
                                %>
                                <li><a href="#" onclick="switchAccessibility();" title="<isa:translate key="access.accessmode.switch"  arg0="<%=accessibilityMode%>"/>">
                                        <%--<span><isa:translate key="access.accessmode.switch"  arg0="<%=accessibilityMode%>"/></span></a></li>--%>
                                        <span><isa:translate key="access.accessmode.switch2"/></span></a></li>
                                        <%
					/*
                                        if (ui.isAccessible())
                                        {
				<li><a href="#" onclick="openAcccessKeysWindow();"><img src="WebUtil.getMimeURL(pageContext, "mimes/images/hint.gif")" alt="<isa:translate key="access.accesskey.help"/>" />
                                        <span><isa:translate key="access.accesskey.help"/></span></a></li>
                                        }
					*/
                                        %>
                                <%
								accessText = WebUtil.translate(pageContext, "b2b.header.logoff", null);
								%>
								
								<%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
								<% if( !current_user.equalsIgnoreCase(USER_GUEST) ) { %>				
								<%-- GREENORANGE END (showing only if user is not user_guest) --%>					
                                <li><a href="#" onclick="logoff();" title="<isa:translate key="access.link" arg0="<%=accessText%>" arg1=""/>">
                                        <span><isa:translate key="b2b.header.logoff"/></span></a></li>
                                <%-- GREENORANGE BEGIN (showing only if user is not user_guest) --%>
								<% }else{ %>
								<li><b><a href="<isa:webappsURL name="init.do"/>" target="_top" title="LogIn">
                                        <span>Log In</span></a></b></li>
								<% } %>
								<%-- GREENORANGE END (showing only if user is not user_guest) --%>					
                         <% } %>
                        </ul>
                </div>
				<%
				accessText = WebUtil.translate(pageContext, "b2b.header.groupFunction", null);
				if(ui.isAccessible())
				{
				%>
				<a 
					href="#b2b_header_groupFunction_begin"
					id="b2b_header_groupFunction_end"
					title="<isa:translate key="access.grp.end" arg0="<%=accessText%>"/>" 
				></a>                    	
				<%
				}
				%>                
                <%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
                <%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
                <%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
                                so make sure to test your work in these browsers. --%>
                <div id="header-extradiv1">
                        <span></span>
                </div>

                <div id="header-extradiv2">
                        <span></span>
                </div>

                <div id="header-extradiv3">
                        <span></span>
                </div>
                <div id="header-extradiv4">
                        <span></span>
                </div>
                <div id="header-extradiv5">
                        <span></span>
                </div>
                <div id="header-extradiv6">
                        <span></span>
                </div>
        </div>

        <%-- preloading busy-gif --%>
        <div>
                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/busy.gif") %>" alt="" style="display: none;" />
        </div>

        <form id="basketForm" action="javascript:show_basket();">
            <div>
                <input name="miniBasket" type="hidden" value="<isa:translate key="b2b.header.minibasket.default"/>" />
            </div>
        </form>
</body>

</html>