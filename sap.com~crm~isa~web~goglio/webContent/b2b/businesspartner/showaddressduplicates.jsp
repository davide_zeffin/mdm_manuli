<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.isacore.action.b2c.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.actionform.b2c.*" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>
<%@ taglib uri="/isa" prefix="isa"  %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.businesspartner.action.BupaConstants" %>
<%@ page import="com.sap.isa.businesspartner.backend.boi.BusinessPartnerData" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% // Are there error messages? An existing messageList instance within the request context indicates
   // an error, otherwise it wouldn't be here.
   boolean isErrorOccurred = false;
   if (request.getAttribute("messageList") != null) {
       isErrorOccurred = true;
   }

   // The address data are contained within the address formular. This object should never be 'null'.
   AddressFormular addressFormular = (AddressFormular) request.getAttribute("addressFormular");

   request.setAttribute("addressFormular", addressFormular);

   String[] evenOdd = new String[] { "even", "odd" };

   BaseUI ui = null;
   ui = new BaseUI(pageContext);

%>


<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

   <head>
	<title><isa:translate key="bupa.register.title"/></title>
    	<isa:includes/>
    	<!--
    	<link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">
    	-->

    	<!--<script
            src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/addressdetails_maintenance.js") %>"
            type="text/javascript">
    	</script>-->



    	<script  type="text/javascript">

		<%
			String selectBtnTxt = WebUtil.translate(pageContext, "bupa.register.selectButton", null);
			String saveBtnTxt = WebUtil.translate(pageContext, "bupa.register.saveButton", null);
			String cancelBtnTxt = WebUtil.translate(pageContext, "bupa.register.cancelButton", null);
			String unavailable = WebUtil.translate(pageContext, "access.unavailable", null);
		%>

	   var submitCounter = 0;
	   var buttonStatus = 0;

	   function send_save() {
	       if (submitCounter == 0) {

	           submitCounter++;
	           document.forms[0].saveClicked.value="true";
	           document.forms[0].submit();
	       }
	   }

	   // the user decides to cancel
	   function send_cancel() {
	       if (submitCounter == 0) {

	           submitCounter++;
	           document.forms[0].cancelClicked.value="true";
	           document.forms[0].submit();
	       }
	   }


	   function send_update() {

	           document.duplettencheck.selectbutton.disabled=true
	           document.duplettencheck.savebutton.disabled=false
			   <% if(ui.isAccessible()) {
               %>
			   		document.duplettencheck.selectbutton.title="<isa:translate key="access.button" arg0="<%=selectBtnTxt%>" arg1="<%=unavailable%>"/>";
	                document.duplettencheck.savebutton.title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>";

			   <% } %>
	   }

	  function send_update1() {

	           document.duplettencheck.selectbutton.disabled=false;
	           document.duplettencheck.savebutton.disabled=true;

			   <% if(ui.isAccessible()) {	%>
			   		document.duplettencheck.selectbutton.title="<isa:translate key="access.button" arg0="<%=selectBtnTxt%>" arg1=""/>";
	                document.duplettencheck.savebutton.title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1="<%=unavailable%>"/>";
			   <% } %>

	   }

	   function init_buttons() {

	           document.duplettencheck.selectbutton.disabled=true;
	           document.duplettencheck.savebutton.disabled=false;
			   <% if(ui.isAccessible()) {	    %>
			   		document.duplettencheck.selectbutton.title="<isa:translate key="access.button" arg0="<%=selectBtnTxt%>" arg1="<%=unavailable%>"/>";
	                document.duplettencheck.savebutton.title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>";

			   <% } %>

	   }

    	</script>
   </head>


   <body onLoad="init_buttons()" class="ship">

      <div class="module-name"><isa:moduleName name="b2b/businesspartner/showaddressduplicates.jsp" /></div>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr class="ship">
      		<td colspan="3"><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="10" alt="" border="0"></td>
  	  </tr>
          <tr class="ship">
    		<td class="opener" valign="middle">

                   <b><isa:translate key="bupa.register.titleduplicates"/></b>

   		</td>
    		<td><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2darkblue.gif") %>" width="8" height="17" alt="" border="0"></td>
    		<td><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="130" height="1" alt="" border="0"></td>
  	  </tr>
      </table>


      <table border="0" cellpadding="0" cellspacing="0" width="100%">
 	  <tr>
   	   <td>
             <form action="<isa:webappsURL name="b2b/bupacreationaddressduplicates.do"/>" name="duplettencheck" method="post" >
              <isacore:requestSerial mode="POST"/>

	   	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	   	  <tr>&nbsp;</tr>
	   	  <tr><isa:translate key="bupa.dupltab.info"/></tr>
	   	  <tr>&nbsp;</tr>
	   	</table>

   	        <table class="list" cellpadding="3" cellspacing="0" border="0" width="100%">

   	   	  <tr>
         	    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th><isa:translate key="bupa.duplicatestable.description"/></th>
                    <th><isa:translate key="bupa.duplicatestable.street"/></th>
                    <th><isa:translate key="bupa.duplicatestable.houseno"/></th>
                    <th><isa:translate key="bupa.duplicatestable.zip"/></th>
                    <th><isa:translate key="bupa.duplicatestable.city"/></th>
                    <th><isa:translate key="bupa.duplicatestable.country"/></th>
                    <th><isa:translate key="bupa.duplicatestable.region"/></th>
                    <th><isa:translate key="bupa.duplicatestable.phone"/></th>
                    <th><isa:translate key="bupa.duplicatestable.fax"/></th>
                    <th><isa:translate key="bupa.duplicatestable.email"/></th>
                  </tr>


   	          <tr>
   	            <td><input type="radio" name="<%= BupaConstants.CUSTOMERSELECTION %>" value="" checked
   	               onClick="send_update()" title="<isa:translate key="bupa.adddup.newbp.selector"/>"></td>
   	            <td></td>
   	           <% if ((addressFormular.getAddress().getLastName() != null) && (addressFormular.getAddress().getLastName().length() > 0)) { %>
   	            <td>
   	   	      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFirstName()))%>&nbsp;
   	   	      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getLastName()))%>
   	            </td>
   	           <% } else { %>
	            <td>
   	   	      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName1()))%>&nbsp;
   	   	      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName2()))%>
   	            </td>
   	          <% } %>

                    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getStreet()))%></td>
                    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getHouseNo()))%></td>
           	    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getPostlCod1()))%></td>
           	    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getCity()))%></td>
           	    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getCountry()))%></td>
           	    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getRegion()))%></td>
           	    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getTel1Numbr()))%></td>
   	   	    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFaxNumber()))%></td>
   	   	    <td><%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getEMail()))%></td>
   		  </tr>

   		  <tr>
   		    <td colspan="12">
   		      &nbsp;<br>
   		      <isa:translate key="bupa.duplicatestable.infotext1"/><br>
   	              <isa:translate key="bupa.duplicatestable.infotext2"/><br>
   	              <isa:translate key="bupa.duplicatestable.infotext3"/><br>
   	              &nbsp;
   	            </td>
   	          </tr>

			    <%
					String cols = "12";
					String rows = Integer.toString(
					   ((ResultData)pageContext.findAttribute(BupaConstants.DUPLICATESLIST)).getNumRows());
					String firstRow = Integer.toString(1);
					String tableTitle = WebUtil.translate(pageContext, "b2b.dupbp.table.title", null);
				%>
				<%
					if (ui.isAccessible())
					{
				%>
				    <tr>
				    <td>
					<a href="#tableend"
						id="tablebegin"
						title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rows%>" arg2="<%=cols%>" arg3="<%=firstRow%>" arg4="<%=rows%>"/>">
					</a>

					</td>
					</tr>
				<%
					}
				%>

   	          <tr>
                    <th></th>
                    <th scope="col"><isa:translate key="bupa.duplicatestable.soldto"/></th>
          	    <th scope="col"><isa:translate key="bupa.duplicatestable.description"/></th>
         	    <th scope="col"><isa:translate key="bupa.duplicatestable.street"/></th>
         	    <th scope="col"><isa:translate key="bupa.duplicatestable.houseno"/></th>
         	    <th scope="col"><isa:translate key="bupa.duplicatestable.zip"/></th>
         	    <th scope="col"><isa:translate key="bupa.duplicatestable.city"/></th>
         	    <th scope="col"><isa:translate key="bupa.duplicatestable.country"/></th>
            	    <th scope="col"><isa:translate key="bupa.duplicatestable.region"/></th>
         	    <th scope="col"><isa:translate key="bupa.duplicatestable.phone"/></th>
                    <th scope="col"><isa:translate key="bupa.duplicatestable.fax"/></th>
                    <th scope="col"><isa:translate key="bupa.duplicatestable.email"/></th>
                  </tr>

   		  <% int line = 0; %>
   		  <isa:iterate id="bupa" name="<%= BupaConstants.DUPLICATESLIST %>"
                    			type="com.sap.isa.core.util.table.ResultData">
         	  <tr class="<%= evenOdd[++line % 2]%>">

                    <td><input type="radio" name="<%= BupaConstants.CUSTOMERSELECTION %>" value="<%= bupa.getString(BusinessPartnerData.SOLDTO_TECHKEY) %>"
                      onClick="send_update1()" title="<isa:translate key="bupa.duptable.selector"/>"></td>
          	    <td><%= bupa.getString(BusinessPartnerData.SOLDTO) %></td>

                    <% if ((bupa.getString(BusinessPartnerData.LASTNAME) != null) && (addressFormular.getAddress().getLastName().length() > 0)) { %>

                    <td>
            		<%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.FIRSTNAME)) %>&nbsp;
            		<%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.LASTNAME)) %>
          	    </td>

                    <% } else { %>

          	    <td>
            		<%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.NAME1)) %>&nbsp;
                    </td>

         	    <% } %>

          	    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.STREET)) %></td>
          	    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.HOUSE_NO)) %></td>
           	    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.POSTL_COD1)) %></td>
          	    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.CITY)) %></td>
          	    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.COUNTRY)) %></td>
          	    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.REGION)) %></td>
          	    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.TEL1)) %>&nbsp;<%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.TELEXT)) %></td>
                    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.FAX1)) %>&nbsp;<%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.FAXEXT)) %></td>
                    <td><%= JspUtil.encodeHtml(bupa.getString(BusinessPartnerData.EMAIL)) %></td>
                  </tr>
                  </isa:iterate>

				 <%
					if (ui.isAccessible())
					{
			 	 %>
			 	    <tr>
				    <td>
					<a
						id="tableend"
						href="#tablebegin"
						title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>">
					</a>
					</td>
				    </tr>
                 <%
					}
			 	 %>

   	        </table>

<!-- Buttons -->
                <table width="100%" border="0">
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><br>
                      <p align="right">




                        <input type="Button" name="selectbutton" value="<isa:translate key="bupa.register.selectButton"/>"
						title="<isa:translate key="access.button" arg0="<%=selectBtnTxt%>" arg1=""/>"
                         onclick="send_save()" class="green">


                        <input type="Button" name="savebutton" value="<isa:translate key="bupa.register.saveButton"/>"
						title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>"
                         onclick="send_save()" class="green">

                        <input type="Button" name="cancelbutton" value="<isa:translate key="bupa.register.cancelButton"/>"
						title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>"
                         onclick="send_cancel()" class="green">

                    </td>
                    <td>&nbsp;</td>

	           </tr>
                </table>



   	      <!-- hidden fields -->

   	      <input type="hidden" name="cancelClicked"  value="">
   	      <input type="hidden" name="saveClicked"  value="">


   	     </form>
   	  </tr>
      </table>
   </body>
</html>