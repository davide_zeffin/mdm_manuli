<% if ( isPerson) {%>

 
    <%-- show errors concerning the lastName of the user --%>
    <% if (isErrorOccurred){ %>                  
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="lastName">
            <%addMessage(Message.ERROR, errortext);%>
          </isa:message>
    <% }%>



   
    <%-- show errors concerning the firstName of the user --%>
    
    
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="firstName">
            <%addMessage(Message.ERROR, errortext);%>
          </isa:message>
    <% }%>
   



<% } else { %>
   
    <%-- show errors concerning the company of the user --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="name1">
            <%addMessage(Message.ERROR, errortext);%>
          </isa:message>
    <% }%>

    
    <%-- show errors concerning the contact person of the user --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="name2">
            <%addMessage(Message.ERROR, errortext);%>
          </isa:message>
    <% }%>
<% } %>

<!-- street and house number-->
  
<%-- show errors concerning the street of the user --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="street">
        <%addMessage(Message.ERROR, errortext);%>
      </isa:message>

<%-- show errors concerning the house number of the user --%>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="houseNo">
       <%addMessage(Message.ERROR, errortext);%>
      </isa:message>
<% }%>

<!-- postal code and city -->
  
<%-- show errors concerning the postal code information of the user/company --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="postlCod1">
      <%addMessage(Message.ERROR, errortext);%>
      </isa:message>

<%-- show errors concerning the city information of the user/company --%>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="city">
       <%addMessage(Message.ERROR, errortext);%>
      </isa:message>
<% }%>

<!-- country -->
  
<%-- show errors concerning the country information of the user/company --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="country">
        <%addMessage(Message.ERROR, errortext);%>
      </isa:message>
<% }%>

	                 
    <%-- show errors concerning the region information of the user/company --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="region">
            <%addMessage(Message.ERROR, errortext);%>
          </isa:message>
    <% }%>
      
    <%-- show errors concerning the county information of the user/company --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="county">
            <%addMessage(Message.ERROR, errortext);%>
          </isa:message>
    <% }%>


<%-- show errors concerning the telephone number of the user --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="telephone1Number">
        <%addMessage(Message.ERROR, errortext);%>
      </isa:message>
<% }%>

<%-- show errors concerning the fax number of the user --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="faxNumber">
        <%addMessage(Message.ERROR, errortext);%>
      </isa:message>
<% }%>


  

<!-- are there messages concerning e-mail property? -->
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="eMail">
        <%addMessage(Message.ERROR, errortext);%>
      </isa:message>
<% }%>




