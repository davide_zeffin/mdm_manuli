<% if ( isPerson) {%>

<!-- last name -->
      <tr>
      <% if ( isChangeable.equalsIgnoreCase("X")) {%>  
        
        <td class="emphasize"><label for="lname"><isa:translate key="bupa.register.lastName.asterix"/></label></td>
        <td class="emphasize">
         
          <input class="textInput" type="Text" size="30" name="lastName" id="lname"
                 value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getLastName()))%>" >
        
        </td>   
        
         <% } else { %>
          
          <td class="emphasize"><isa:translate key="bupa.register.lastName"/></td>
          <td class="emphasize">
           <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getLastName()))%>
          </td> 
         <% } %>
        
        
      </tr>
    <%-- show errors concerning the lastName of the user --%>
    <% if (isErrorOccurred){ %>                  
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="lastName">
            <tr>
        
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
        
            </tr>
          </isa:message>
    <% }%>

    <!-- first name -->
      <tr>
        
        <td class="emphasize"><label for="fname"><isa:translate key="bupa.register.firstName"/></label></td>
        <td class="emphasize">
         <% if ( isChangeable.equalsIgnoreCase("X")) {%> 
          <input class="textInput" type="Text" size="30" name="firstName" id="fname"
                 value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFirstName()))%>" >
         <% } else { %>
          <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFirstName()))%>
         <% } %>
         </td> 
        
      </tr>
    <%-- show errors concerning the firstName of the user --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="firstName">
            <tr>
              
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
              
            </tr>
          </isa:message>
    <% }%>

    
<% } else { %>
    <!-- name1 -->
      <tr>
      <% if ( isChangeable.equalsIgnoreCase("X")) {%>   
        
        <td class="emphasize"><label for="company"><isa:translate key="bupa.register.company.asterix"/></label></td>
        <td class="emphasize">
         
          <input class="textInput" type="Text" size="30" name="name1" id="company"
                 value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName1()))%>" >
        </td>         
         <% } else { %>
        
          <td class="emphasize"><isa:translate key="bupa.register.company"/></td>
          <td class="emphasize">
           <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName1()))%>
          </td> 
         <% } %>
         
        
      </tr>
    <%-- show errors concerning the company of the user --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="name1">
            <tr>
        
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
        
            </tr>
          </isa:message>
    <% }%>

    <!-- name2 -->
      <tr>
        
        <td class="emphasize"><label for="cnctperson"><isa:translate key="bupa.register.contactPerson"/></label></td>
        <td class="emphasize">
         <% if ( isChangeable.equalsIgnoreCase("X")) {%>
          <input class="textInput" type="Text" size="30" name="name2" id="cnctperson"
                 value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName2()))%>"  >
         <% } else { %>
          <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName2()))%>
         <% } %>
        </td>
        
      </tr>
    <%-- show errors concerning the contact person of the user --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="name2">
            <tr>
        
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
        
            </tr>
          </isa:message>
    <% }%>
<% } %>

<!-- house number and street-->
  <tr>
  <% if ( isChangeable.equalsIgnoreCase("X")) {%> 
    
    <td class="emphasize"><label for="housestreet"><isa:translate key="bupa.register.NoSt.ax"/></label></td>
    <td class="emphasize" id="housestreet">
     
      <input class="textInput" type="Text" size="10" name="houseNumber" 
             value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getHouseNo()))%>" >
      <input class="textInput" type="Text" size="30" name="street" 
             value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getStreet()))%>">
    </td>
     
     <% } else { %>
    
     <td class="emphasize"><isa:translate key="bupa.register.HouseNoAndStreet"/></td>
     <td class="emphasize">
      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getHouseNo()))%>
      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getStreet()))%>
     </td>
     
     <% } %>
     
    
    
  </tr>
<%-- show errors concerning the street of the user --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="street">
        <tr>
    
          <td colspan="2" class="emphasize">
            <div class="error">
              <%=errortext%>
            </div>
          </td>
    
        </tr>
      </isa:message>
<%-- show errors concerning the house number of the user --%>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="houseNo">
        <tr>
    
          <td colspan="2" class="emphasize">
            <div class="error">
              <%=errortext%>
            </div>
          </td>
    
        </tr>
      </isa:message>
<% }%>

<% if ( isChangeable.equalsIgnoreCase("X")) {%>  

<% if ((listOfRegions != null)) { %>
<!-- city, region and postal code-->
      <tr>
    
        <td class="emphasize"><label for="ctyregpc"><isa:translate key="bupa.register.CtyRegPC.ax"/></label></td>
        <td class="emphasize" id="cityregpc">
          <!-- city -->
         
          <input class="textInput" type="Text" size="30" name="city" 
                 value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getCity()))%>" >
          <!-- region -->
          <select name="region">
            <% // build up the selection options           
               String defaultRegionId =(String) addressFormular.getAddress().getRegion(); %>
            <isa:iterate id="region" name="<%= BupaConstants.POSSIBLE_REGIONS %>"
                         type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
              <% // Should the current region be marked as selected?
                 String selected="";
                 if  (region.getString("ID").equals(defaultRegionId)) {
                     selected="SELECTED";
                 } %>
              
                 <option value="<%= region.getString("ID")%>" <%= selected %>>
                   <%= JspUtil.encodeHtml(region.getString("DESCRIPTION"))%>
                 </option>
            </isa:iterate>
          </select>
          <!-- postal code -->
          <input class="textInput" type="Text" size="10" name="postalCode" 
                 value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getPostlCod1()))%>" >
         
        </td>
    
      </tr>
    <%-- show errors concerning the postal code information of the user/company --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="postlCod1">
            <tr>
    
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
    
            </tr>
          </isa:message>
    <%-- show errors concerning the region information of the user/company --%>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="region">
            <tr>
    
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
    
            </tr>
          </isa:message>
    <%-- show errors concerning the city information of the user/company --%>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="city">
            <tr>
    
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
    
            </tr>
          </isa:message>
    <% }%>
<% }
   else { %> 			<!-- list of region != null -->
<!-- city and postal code-->
      <tr>
    
        <td class="emphasize"><label for="ctypc"><isa:translate key="bupa.register.CtyPCode.ax"/></label></td>
        <td class="emphasize" id="ctypc">
         
          <!-- city -->
          <input class="textInput" type="Text" size="30" name="city" 
                 value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getCity()))%>" >
          <!-- postal code -->
          <input class="textInput" type="Text" size="10" name="postalCode" 
                 value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getPostlCod1()))%>" >
         
          </td>
    
      </tr>
    <%-- show errors concerning the postal code information of the user/company --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="postlCod1">
            <tr>
    
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
    
            </tr>
          </isa:message>
    <%-- show errors concerning the city information of the user/company --%>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="city">
            <tr>
    
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
    
            </tr>
          </isa:message>
    <% }%>
<% } %>

<% } else { %>   <!-- isChangeable == X -->
   
   
   <% if ((addressFormular.getAddress().getRegion() != null) && (addressFormular.getAddress().getRegion().length() > 0)) { %>	
       <tr>		
   
        <td class="emphasize"><isa:translate key="bupa.register.CtyRegPC"/></td>
        <td class="emphasize">
   	<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getCity()))%>
   	<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getRegionText50()))%>        
   	<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getPostlCod1()))%>
	</td>
       </tr>
   <% } else { %> <!-- region !=null -->
       <tr>
        
        <td class="emphasize"><isa:translate key="bupa.register.CityAndPostalCode"/></td>
        <td class="emphasize">
        <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getCity()))%>
        <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getPostlCod1()))%>
        </td>
       </tr>

   <% } %>	<!-- region !=null -->


<% } %>  <!-- isChangeable == X -->


<% if ( isChangeable.equalsIgnoreCase("X")) {%>  

<% if (listOfCounties != null && listOfCounties.getNumRows() > 0){ %>
    <!-- county -->
      <tr>
   
        <td class="emphasize"><label for="county"><isa:translate key="bupa.register.county"/></label></td>
        <td class="emphasize">
          <select name="taxJurisdictionCode" id="county">
            <% // build up the selection options           
               String defaultTaxJurCode = (String) addressFormular.getAddress().getTaxJurCode(); %>
            <isa:iterate id="county" name="<%= BupaConstants.POSSIBLE_COUNTIES %>"
                         type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
              <% // Should the current county be marked as selected?
                 String selected="";
                 if  (county.getString("TXJCD").equals(defaultTaxJurCode)) {
                     selected="SELECTED";
                 } %>
            
                 <option value="<%= county.getString("TXJCD")%>" <%= selected %>>
                   <%= JspUtil.encodeHtml(county.getString("COUNTY")) %><%= JspUtil.encodeHtml(county.getString("OUTOF_CITY")) %>
                </option>
            </isa:iterate>
          </select>
        </td>
   
      </tr>

<% } %>     <!-- listofcounties != null -->


<% } else { %>   

<% if (((addressFormular.getAddress().getDistrict()) != null) && (addressFormular.getAddress().getDistrict().length() > 0)) { %>
       
       <tr>
   
        <td class="emphasize"><isa:translate key="bupa.register.county"/></td>
        <td class="emphasize">
        <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getDistrict()))%>        
        </td>
       </tr>  
   
<% } %>    

<% } %>


    <%-- show errors concerning the county information of the user/company --%>
    <% if (isErrorOccurred){ %>
          <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                       type="<%=Message.ERROR%>" property="county">
            <tr>
   
              <td colspan="2" class="emphasize">
                <div class="error">
                  <%=errortext%>
                </div>
              </td>
   
            </tr>
          </isa:message>
    <% }%>


<!-- country -->
  <tr>
  <% if ( isChangeable.equalsIgnoreCase("X")) {%>  
   
    <td class="emphasize"><label for="country"><isa:translate key="bupa.register.country.asterix"/></label></td>
    <td class="emphasize">
      <% 
         // The result of the following decision will be passed as a parameter of the 'send_countryChange'
         // JavaScript function which is defined within the file 'addressdetails_maintenance.js'.
         String fromCountry = "F";
         if (listOfRegions != null) {
             fromCountry = "T";
         } %>
      
     
      
      <select name="country" onChange="send_countryChange('<%= fromCountry %>')" id="country">
        <% // build up the selection options 
           String defaultCountryId =(String) addressFormular.getAddress().getCountry(); %>
        <isa:iterate id="country" name="<%= BupaConstants.POSSIBLE_COUNTRIES %>"
                     type= "com.sap.isa.core.util.table.ResultData" resetCursor="true">
          <% // Should the current country be marked as selected?
             String selected="";
             if  (country.getString("ID").equals(defaultCountryId)) {
                 selected="SELECTED";
             } %>

             <OPTION VALUE="<%= country.getString("ID") %>" <%= selected %>>
               <%= JspUtil.encodeHtml(country.getString("DESCRIPTION")) %>
             </OPTION>
        </isa:iterate>
      </select>
      <script language="JavaScript">
        <% index = 0; %>    
        <isa:iterate id="country" name="<%= BupaConstants.POSSIBLE_COUNTRIES %>"
             type= "com.sap.isa.core.util.table.ResultData" resetCursor="true">
          <% // fill a JavaScript array to remember which country has regions that has to be displayed
        
             String arrayValue = "F";
             if (country.getBoolean("REGION_FLAG") == true) {
                 arrayValue = "T"; 
             } %>

          toCountry[<%= index++ %>] = "<%= arrayValue %>";
        </isa:iterate>
      </script>
    </td>  
      
    <% } else { %>
   
     <td class="emphasize"><isa:translate key="bupa.register.country"/></td>
     <td class="emphasize">
     <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getCountryText()))%>
     </td>
    <% } %>
     
    </td>
   
  </tr>
<%-- show errors concerning the country information of the user/company --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="country">
        <tr>
   
          <td colspan="2" class="emphasize">
            <div class="error">
              <%=errortext%>
            </div>
          </td>
   
        </tr>
      </isa:message>
<% }%>

<!-- telephone number -->
  <tr>
   
    <td class="emphasize"><label for="telephone"><isa:translate key="bupa.register.telephone"/></label></td>
    <td class="emphasize">
     <% if ( isChangeable.equalsIgnoreCase("X")) {%>   
      <input class="textInput" type="Text" size="30" name="telephoneNumber" id="telephone"
             value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getTel1Numbr()))%>">
     <% } else { %>
      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getTel1Numbr()))%>
     <% } %>
     </td>
   
  </tr>
<%-- show errors concerning the telephone number of the user --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="telephone1Number">
        <tr>
   
          <td colspan="2" class="emphasize">
            <div class="error">
              <%=errortext%>
            </div>
          </td>
   
        </tr>
      </isa:message>
<% }%>

<isacore:ifShopProperty property = "backend" value = "<%=ShopData.BACKEND_R3_PI%>" >
<!-- telephone extension -->
  <tr>
    
    <td class="emphasize"><isa:translate key="bupa.register.telephext"/></td>
    <td class="emphasize">
      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getTel1Ext()))%>
     </td>
    
  </tr>
</isacore:ifShopProperty>

<!-- fax number -->
  <tr>
   
    <td class="emphasize"><label for="fax"><isa:translate key="bupa.register.fax"/></label></td>
    <td class="emphasize">
     <% if ( isChangeable.equalsIgnoreCase("X")) {%>   
      <input class="textInput" type="Text" size="30" name="faxNumber" id="fax"
             value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFaxNumber()))%>" >
     <% } else { %>
      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFaxNumber()))%>
     <% } %>
     </td>
   
  </tr>
<%-- show errors concerning the fax number of the user --%>
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="faxNumber">
        <tr>
   
          <td colspan="2" class="emphasize">
            <div class="error">
              <%=errortext%>
            </div>
          </td>
   
        </tr>
      </isa:message>
<% }%>

<!-- new  -->
<!-- e-mail Address -->
  <tr>
   
    <td class="emphasize"><label for="email"><isa:translate key="bupa.register.email"/></label></td>
    <td class="emphasize">
     <% if ( isChangeable.equalsIgnoreCase("X")) {%>   
      <input class="textInput" type="Text" size="30" name="email" id="email"
             value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getEMail()))%>" >
     <% } else { %>
      <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getEMail()))%>
     <% } %>
     </td>
   
  </tr>
  

<!-- are there messages concerning e-mail property? -->
<% if (isErrorOccurred){ %>
      <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                   type="<%=Message.ERROR%>" property="eMail">
        <tr>
   
          <td colspan="2" class="emphasize">
            <div class="error">
              <%=errortext%>
            </div>
          </td>
   
        </tr>
      </isa:message>
<% }%>

<!-- end: new -->



<!-- hidden field: person -->
<% if (addressFormular.isPerson()) { %>
   <input type="hidden" name="person" value="true" >
<% }
 else { 
   // the empty string will be interpreted as 'false' by the request parser %>
   <input type="hidden" name="person" value="" >
<% } %>

<!-- hidden field: regionRequired -->
<% if (addressFormular.isRegionRequired()) { %>
   <input type="hidden" name="regionRequired" value="true" >
<% }
 else { 
   // the empty string will be interpreted as 'false' by the request parser %>
   <input type="hidden" name="regionRequired" value="" >
<% } %>

<!-- hidden fields: name1/name2 or firstName/lastName -->
<% if (isPerson) { %>
     <input type="hidden" name="name1" 
            value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName1()))%>" >
     <input type="hidden" name="name2" 
            value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName2()))%>" >
<% }
 else { %>
     <input type="hidden" name="firstName" 
            value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFirstName()))%>" >
     <input type="hidden" name="lastName" 
            value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getLastName()))%>" >
<% } %>
