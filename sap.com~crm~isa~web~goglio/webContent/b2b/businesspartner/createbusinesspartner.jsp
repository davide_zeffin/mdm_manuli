<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.isacore.action.b2c.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.actionform.b2c.*" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ page import="com.sap.isa.businesspartner.action.BupaConstants" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>


<%@ taglib uri="/isa" prefix="isa"  %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<% // Are there error messages? An existing messageList instance within the request context indicates
   // an error, otherwise it wouldn't be here.
   boolean isErrorOccurred = false;
   if (request.getAttribute("messageList") != null) {
       isErrorOccurred = true;
   }

   // The address data are contained within the address formular. This object should never be 'null'.
   AddressFormular addressFormular = (AddressFormular) request.getAttribute("addressFormular");
   // flag that indicates whether the user represents a person or an organization
   boolean isPerson;

   // list of regions may be 'null'
   ResultData listOfRegions = (ResultData) request.getAttribute(BupaConstants.POSSIBLE_REGIONS);
   // list of counties may be 'null'
   ResultData listOfCounties = (ResultData) request.getAttribute(BupaConstants.POSSIBLE_COUNTIES);

   // the index variable will be used to fill a JavaScript array
   int index;

   //flag that indicates wheter address include is in input or display mode
   String isChangeable = (String) request.getAttribute(BupaConstants.IS_CHANGEABLE);

   BaseUI ui = null;
   ui = new BaseUI(pageContext);
   isPerson = addressFormular.isPerson();
%>



<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
   
  <head>    
     <title><isa:translate key="bupa.register.title"/></title>
    	<isa:includes/>
    	<!-- <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet"> -->

    	<!--<script  src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/addressdetails_maintenance.js") %>"
            type="text/javascript">
    	</script>-->

    	<!-- normally included in *.js file, but include doesn't work -->

    	<script type="text/javascript">

	   var submitCounter = 0;
	   var toTitle = new Array();
	   var toCountry = new Array();

	   function send_save() {
	       if (submitCounter == 0) {

	           submitCounter++;
	           document.forms[0].submit();
	       }
	   }

	   // the user decides to cancel
	   function send_cancel() {
	       if (submitCounter == 0) {

	           submitCounter++;
	           document.forms[0].cancelClicked.value="true";
	           document.forms[0].submit();
	       }
	   }



	   // the user selects another title
	   function send_titleChange(fromTitle) {
	       // remark: fromTitle should be equal to 'P' or 'O'
	       // P = title key is used for a person
	       // O = title key is used for an organization
	       // Possible title changes:
	       // (1) from P to O
	       // (2) from P to P
	       // (3) from O to O
	       // (4) from O to P
	       // there are 2 situations where a rebuild of this JSP must be forced: (1), (4)

	       // determine the titleElement, i.e. the select box which contains the possible titles
	       var titleKeyElement = document.forms[0].titleKey;

	       // determine the index of the selected option (i.e. the selected title)
	       var selectedIndex = titleKeyElement.selectedIndex;

	       // decide whether to send a request or not


	       if (fromTitle != toTitle[selectedIndex])

	       {
	         // debugging:
	         //alert("Title change form "+fromTitle+" to "+toTitle[selectedIndex]);
	         document.forms[0].titleChange.value="true";
	         document.forms[0].submit();
	       }
	   }

	   // the user selects another country
	   function send_countryChange(fromCountry) {
    		// remark: fromCountry should be equal to 'T' or 'F'
    		// T = country where regions are reqired
    		// F = country where regions are not required
    		// Possible country changes:
    		// (1) from F to F
    		// (2) from F to T
    		// (3) from T to F
    		// (4) from T to T
    		// there are 3 situations where a rebuild of this JSP must be forced: (2), (3), (4)


    		// determine the countryElement, i.e. the select box which contains the possible countries
    		var countryElement = document.forms[0].country;
    		// determine the index of the selected option (i.e. the selected country)
    		var selectedIndex = countryElement.selectedIndex;

 		// decide whether to send a request or not
    		if (fromCountry != "F" || toCountry[selectedIndex] != "F")
    		{
      		// at this point: F to F is not the case

      		// debugging:
      		// alert("Title change form "+fromCountry+" to "+toCountry[selectedIndex]);

      		if (toCountry[selectedIndex] == "F") {
          		document.forms[0].regionRequired.value=""; // the empty string will be interpreted as 'false'
                                                                   // by the request parser
      		}
      		else {
          		document.forms[0].regionRequired.value="true";
      		}

      			document.forms[0].countryChange.value="true";
      			document.forms[0].submit();
    		}
	}


 	</script>
   </head>


   <body class="order">
       <div class="module-name"><isa:moduleName name="b2b/businesspartner/createbusinesspartner.jsp" /></div>
  	  <%
	      if (isErrorOccurred){
		  if(ui.isAccessible()) {

	  %>
	  		  <%@ include file="/b2b/messages-declare.inc.jsp" %>
	 		  <%@ include file="/b2b/messages-begin.inc.jsp" %>

	           <!--general error messages -->
	 		  <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
	                       type="<%=Message.ERROR%>" property="">
	 		  	 <%addMessage(Message.ERROR, errortext);%>
	           </isa:message>

 			<%-- show errors concerning the title of the user --%>
           <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
	                         type="<%=Message.ERROR%>" property="titleKey">
           <%addMessage(Message.ERROR, errortext);	  %>
           </isa:message>

	 		  <% if (addressFormular.getAddressFormatId() == 0) { %>
	                <%@ include file="inputaddressformat_default_err.inc" %>
	           <% }
	              else { %>
	                <%@ include file="inputaddressformat_us_err.inc" %>
	           <% }

	 	  		if (!messages.isEmpty()){
	 	  %>
	 	  			<%@ include file="/b2b/messages-end.inc.jsp" %>
	 	  <%	} %>
      <%  } // if ui.isAccessible
		} // if there is error.
	  %>


      <div class="module-name"><isa:moduleName name="b2b/businesspartner/createbusinesspartner.jsp" /></div>

	  <%
	    String grpTxt = WebUtil.translate(pageContext, "bupa.register.title1", null);
	    if (ui.isAccessible()) 	{  %>
			<a	href="#group-end"
				id="group-begin"
				title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>"> </a>
	  <%} %>
	
	<div class="data">
	
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr class="ship">
      		<td colspan="3"><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="10" alt="" border="0"></td>
  	  </tr>
          <tr class="ship">
    		<td class="opener" valign="middle">

                   <div class="title"><isa:translate key="bupa.register.title1"/></div>

   		 </td>
        </tr>
      </table>
	</div>
	
	<div class="data">
	
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

 	  <tr>
   	   <td>
             <form action="<isa:webappsURL name="b2b/businesspartnercreate.do"/>" name="michasaveregistration" method="post" >
              <isacore:requestSerial mode="POST"/>
   	      <table class="list" width="100%" border="0" cellspacing="0" cellpadding="1">

         <!--general error messages -->


                    <% if (isErrorOccurred){ %>
                         <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                                      type="<%=Message.ERROR%>" property="">
                           <tr>

                             <td class="emphasize" colspan="2">
                               <div class="error">
                                 <%=errortext%>
                               </div>
                             </td>

                           </tr>
                         </isa:message>
                    <% }%>

		 <tr></tr>
		 <tr>

                    <td class="identifier" width="40%"><label for="formOfAddress">
					      <isa:translate key="bupa.register.formOfAddress"/></label></td>
                    <td class="value" width="60%">
                      <%
                         // The result of the following decision will be passed as a parameter of the
                         // 'send_tileChange' JavaScript function which is defined within the file
                         // 'addressdetails_maintenance.js'.
                         String fromTitle = "P";
                         if (!isPerson) {
                             fromTitle = "O";
                         } %>


                      <select name="titleKey" onChange="send_titleChange('<%= fromTitle %>')"
					                               id="formOfAddress" class="select-large">
                	<% // build up the selection options
                           String defaultTitleKey =(String) addressFormular.getAddress().getTitleKey(); %>
                        <isa:iterate id="formsOfAddress" name="<%= BupaConstants.FORMS_OF_ADDRESS %>"
                                     type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
                          <% // Should the current title be marked as selected?
                             String selected="";
                             if  (formsOfAddress.getString("ID").equals(defaultTitleKey)) {
                                 selected="SELECTED";
                             } %>

                             <option value="<%= formsOfAddress.getString("ID") %>" <%= selected %>>
                               <%= formsOfAddress.getString("DESCRIPTION") %>
                             </option>
                        </isa:iterate>
                      </select>
                      <script type="text/javascript">
                        <% index = 0; %>
                        <isa:iterate id="formsOfAddress" name="<%= BupaConstants.FORMS_OF_ADDRESS %>"
                                     type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
                        <% // fill a JavaScript array to remember which title key represents a person and
                           // which one represents an organization

                           String arrayValue = "P";
                           if (!formsOfAddress.getBoolean("FOR_PERSON")) {
                               arrayValue = "O";
                           } %>

                          toTitle[<%= index++ %>] = "<%= arrayValue %>";
                        </isa:iterate>
                      </script>
                   </td>


                 </tr>

           <%-- show errors concerning the title of the user --%>



                <% if (isErrorOccurred){ %>
                    <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                                 type="<%=Message.ERROR%>" property="titleKey">
                      <tr>

                        <td class="emphasize" colspan="2">
                          <div class="error">
                            <%=errortext%>
                          </div>
                        </td>

                      </tr>

                    </isa:message>
                <% }%>

<!-- include the specific address format -->
                  <% if (addressFormular.getAddressFormatId() == 0) { %>
                       <%@ include file="inputaddressformat_default.inc" %>
                  <% }
                     else { %>
                       <%@ include file="inputaddressformat_us.inc" %>
                  <% } %>



                </table>

		<table border="0" cellpadding="0" cellspacing="0" width="100%">

		<tr>&nbsp</tr>

		<tr>

                <isa:translate key="bupa.register.requiredinput"/>

                </tr>

		</table>

<!-- Buttons -->
                <table class="shipto" border="0" cellspacing="0" cellpadding="5" width="100%">
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><br>
                      <p align="right">

					     <%
							String saveBtnTxt = WebUtil.translate(pageContext, "bupa.register.saveButton", null);
							String cancelBtnTxt = WebUtil.translate(pageContext, "bupa.register.cancelButton", null);
					     %>
					    <input type="Button" value="<isa:translate key="bupa.register.saveButton"/>"
                         onclick="send_save()" title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>"
                         class="green">

              			<input type="Button" value="<isa:translate key="bupa.register.cancelButton"/>"
                         onclick="send_cancel()" title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>"
                         class="green">


                    </td>
                    <td>&nbsp;</td>

	           </tr>
                 </table>

   	      </table>
   	      <!-- hidden fields -->
   	      <input type="hidden" name="titleChange"   value="">
   	      <input type="hidden" name="countryChange"  value="">
   	      <input type="hidden" name="cancelClicked"  value="">

   	     </form>
   	  </tr>
      </table>
      
     </div>
     
		<%	if (ui.isAccessible()) {%>
			<a	href="#group-begin"
				id="group-end"
				title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"> </a>
		<%
			}
		%>

   </body>



</html>