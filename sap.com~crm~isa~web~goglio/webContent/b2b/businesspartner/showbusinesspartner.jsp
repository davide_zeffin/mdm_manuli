<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.businesspartner.action.BupaConstants" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>

<%-- import the taglibs used on this page --%>
<%@ taglib prefix="isa" uri="/isa" %>
<%@ taglib uri="/isacore"  prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<% // Are there error messages? An existing messageList instance within the request context indicates
   // an error, otherwise it wouldn't be here.
   boolean isErrorOccurred = false;
   if (request.getAttribute(BupaConstants.MESSAGE_LIST) != null) {
       isErrorOccurred = true;
   }

   // The address data are contained within the address formular. This object should never be 'null'.
   AddressFormular addressFormular = (AddressFormular) request.getAttribute("addressFormular");
   // flag that indicates whether the user represents a person or an organization
   boolean isPerson;

   // list of regions may be 'null'
   ResultData listOfRegions = (ResultData) request.getAttribute(BupaConstants.POSSIBLE_REGIONS);
   // list of counties may be 'null'
   ResultData listOfCounties = (ResultData) request.getAttribute(BupaConstants.POSSIBLE_COUNTIES);

   // the index variable will be used to fill a JavaScript array
   int index;

   //flag that indicates whether address include is in input or display mode
   String isChangeable = (String) request.getAttribute(BupaConstants.IS_CHANGEABLE);

   //flag that indicates whether address include is in input or display mode
   String inWindow = (String) request.getParameter(BupaConstants.DISPLAY_IN_WINDOW);

   if (inWindow == null) {
    inWindow = "false";
   }

   //flag that indicates whether address include is in input or display mode
   String readOnly = (String) request.getParameter(BupaConstants.READ_ONLY);

   if (readOnly == null) {
    readOnly = "false";
   }

   //flag that indicates whether creation success messages should be displayed;
   String showCreationSuccess = (String) request.getAttribute(BupaConstants.SHOW_CREATION_SUCCESS);

   //flag that indictes whether the select Button should be displayed or not
   String showSelectButton = (String) request.getAttribute(BupaConstants.SHOW_SELECT_BUTTON);

   //id of bupa
   String bupaid = (String) request.getAttribute(BupaConstants.PARTNER_ID);

    BaseUI ui = null;
   	ui = new BaseUI(pageContext);
	isPerson = addressFormular.isPerson();
%>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

  <head>
     <title><isa:translate key="bupa.personaldetails.title"/></title>
     <isa:includes/>
     <!--
     <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">
     -->
     <%-- include der java script methoden nicht vergessen! am besten zentral in einem file --%>
     <script type="text/javascript">

           var submitCounter = 0;

           function send_change() {
               if (submitCounter == 0) {
                   submitCounter++;
                   document.forms[0].changeClicked.value="true";
                   document.forms[0].submit();
               }
           }

           // the user decides to cancel
           function send_cancel() {
             <%
             if (inWindow.equals("true")) { %>
              window.close();
             <%
             }
             else {%>
               if (submitCounter == 0) {
                   submitCounter++;
                   document.forms[0].cancelClicked.value="true";
                   document.forms[0].submit();
               }
             <%
             }%>
           }

           // the user decides to cancel
           function send_select() {
               if (submitCounter == 0) {

                   submitCounter++;
                   document.forms[0].selectClicked.value="true";
                   document.forms[0].submit();
               }
           }

     </script>

  </head>

  <body class="ship">

     <div class="module-name"><isa:moduleName name="b2b/businesspartner/showbusinesspartner.jsp" /></div>
     <%

	    if (isErrorOccurred){
		  if(ui.isAccessible()) {
	  %>
	  	  <%@ include file="/b2b/messages-declare.inc.jsp" %>
		  <%@ include file="/b2b/messages-begin.inc.jsp" %>

          <%--general error messages --%>
		  <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
                      type="<%=Message.ERROR%>" property="">
		  	 <%addMessage(Message.ERROR, errortext);%>
          </isa:message>


		  <% if (addressFormular.getAddressFormatId() == 0) { %>
               <%@ include file="inputaddressformat_default_err.inc" %>
          <% }
             else { %>
               <%@ include file="inputaddressformat_us_err.inc" %>
          <% }

	  		if (!messages.isEmpty()){
	  %>
	  			<%@ include file="/b2b/messages-end.inc.jsp" %>
	  <%
			}
          } // if ui.isAccessible
		} // if there is error.
	  %>


	<%     String grpTxt = WebUtil.translate(pageContext, "bupa.register.titledetails", null);
	       if (ui.isAccessible()) 	{
	%>
		 <a
		 	href="#group-end"
		 	id="group-begin"
		 	title="<isa:translate key="access.grp.begin" arg0="<%=grpTxt%>"/>">
		 </a>
	<%     }  %>


     <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="ship">
                <td colspan="3"><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="10" alt="" border="0"></td>
          </tr>

          <tr class="ship">
                <td class="opener" valign="middle">

                   <b><isa:translate key="bupa.register.titledetails"/></b>

                </td>
                <td><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2darkblue.gif") %>" width="8" height="17" alt="" border="0"></td>
                <td><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="130" height="1" alt="" border="0"></td>
          </tr>
      </table>

  <table border="0" cellspacing="0" cellpadding="0" width="100%">
     <% if ((showCreationSuccess != null) && (showCreationSuccess.equalsIgnoreCase("X"))) { %>
     <tr><b><isa:translate key="bupa.creation.successmessage"/></b></tr>
     <% } %>
     <tr>
      <td>
       <form action="<isa:webappsURL name="b2b/businesspartnerdetailsdispatch.do"/>" name="michasaveregistration" method="post" >
        <isacore:requestSerial mode="POST"/>
        <table class="list" width="100%" border="0" cellspacing="0" cellpadding="1">


         <%--general error messages --%>


          <% if (isErrorOccurred){
		       if(!ui.isAccessible()) { %>
           <isa:message id="errortext" name="<%= BupaConstants.MESSAGE_LIST %>"
              type="<%=Message.ERROR%>" property="">
              <tr>

               <td class="emphasize" colspan="2">
               <div class="error">
               <%=errortext%>
               </div>
               </td>

              </tr>
             </isa:message>
           <% }
		    }
		   %>

           <tr></tr>


           <%-- display business partner id --%>

            <tr>

              <td class="emphasize" width="40%"><isa:translate key="bupa.register.id"/></td>
              <td class="emphasize" width="60%">
               <%= bupaid %>
              </td>
            </tr>

            <tr>

              <td class="emphasize" width="40%"><isa:translate key="bupa.register.formOfAddress"/></td>
              <td class="emphasize" width="60%">
                <%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getTitle()))%>
              </td>
            </tr>


           <%-- include the specific address format --%>
           <% if (addressFormular.getAddressFormatId() == 0) { %>
              <%@ include file="inputaddressformat_default.inc" %>
           <% }
              else { %>
           <%@ include file="inputaddressformat_us.inc" %>
           <% } %>
        </table>

<%-- Buttons --%>
        <table class="shipto" border="0" cellspacing="0" cellpadding="5" width="100%">
         <tr>
          <td>&nbsp;</td>
            <td colspan="2"><br>
              <p align="right">

<% if ((showSelectButton != null) && (showSelectButton.equalsIgnoreCase("X"))) { %>

     <%
		String selectBtnTxt = WebUtil.translate(pageContext, "bupa.register.selectButton", null);
     %>

      <input type="Button" value="<isa:translate key="bupa.register.selectButton"/>"
	  title="<isa:translate key="access.button" arg0="<%=selectBtnTxt%>" arg1=""/>"
                 onclick="send_select()" class="green">
<% } %>

<isacore:ifShopProperty property = "backend" value = "<%=ShopData.BACKEND_CRM%>" >
<% if (!(readOnly.equalsIgnoreCase("true"))) { %>

     <%
		String changeBtnTxt = WebUtil.translate(pageContext, "bupa.register.changeButton", null);
     %>

      <input type="Button" value="<isa:translate key="bupa.register.changeButton"/>"
	  title="<isa:translate key="access.button" arg0="<%=changeBtnTxt%>" arg1=""/>"
                 onclick="send_change()" class="green">
<% } %>
</isacore:ifShopProperty>

      <%
		String cancelBtnTxt = WebUtil.translate(pageContext, "bupa.register.closeButton", null);
     %>
              <input type="Button" value="<isa:translate key="bupa.register.closeButton"/>"
			  title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>"
                         onclick="send_cancel()" class="green">



              <%-- hidden fields --%>

              <% String guid = (String) request.getAttribute(BupaConstants.BUPA_TECHKEY); %>
              <input type="hidden" name="<%= BupaConstants.BUPA_TECHKEY %>" value="<%= guid %>" >
              <input type="hidden" name="changeClicked"   value="">
              <input type="hidden" name="cancelClicked"  value="">
              <input type="hidden" name="selectClicked"  value="">
              <input type="hidden" name="<%= BupaConstants.SHOW_SELECT_BUTTON %>"  value="<%= showSelectButton%>">
              <input type="hidden" name="<%= BupaConstants.DISPLAY_IN_WINDOW %>"  value="<%= inWindow %>">

          </td>

         </tr>
        </table>

       </form>
     </tr>
  </table>
  <%	if (ui.isAccessible()) { %>
     	  <a	href="#group-begin"
  		        id="group-end"
  		        title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"> </a>
  <%	} %>


  </body>
</html>
