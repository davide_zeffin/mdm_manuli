<%--
********************************************************************************
    File:         contract_details.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      19.4.2001
    Version:      1.0

    $Revision: #10 $
    $Date: 2001/05/22 $
********************************************************************************
--%>

<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ include file="checksession.inc" %>

<%

  boolean noorderdetails      = (request.getParameter("noorderdetails") != null);
  boolean nocontractdetails   = (request.getParameter("nocontractdetails") != null); 
  boolean nonotedetails       = (request.getParameter("nonotedetails") != null);
  boolean nosupplydetails     = (request.getParameter("nosupplydetails") != null);
  boolean nosimproductdetails = (request.getParameter("nosimproductdetails") != null);

%>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>SAP Internet Sales B2B</title>
    <isa:includes/>
    <!-- <link href="<isa:mimeURL name="mimes/shared/style/stylesheet.css" />"
          type="text/css" rel="stylesheet"> -->
		  
	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>			  
		  
      <script src="<isa:mimeURL name="b2b/jscript/order.js" />" 
              type="text/javascript"></script>	  
		  
  </head>
  <body class="workarea" onload="retrieveMainData()">
    <div id="new-doc" class="module">
      <div class="module-name">contrcat_details.jsp</div>
      <form action="" name="contract_details">
        <!-- die tabelle soll an dieser stelle eindeutiger zu sehen sein >> class="list" -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td colspan="5" align="left">
                <table border="0">
                  <tbody>
                    <tr valign="top">
                      <% if (!noorderdetails) { %>
                        <td class="DetailsTab"><a href="<isa:webappsURL name="b2b/order_details.jsp"/>" onclick="saveDetailData(); return true;"><isa:translate key="b2b.order.details.linkdetails"/></a></td>
                      <% } %>
                      <% if (!nocontractdetails) { %>
                        <td class="actualDetailsTab"><isa:translate key="b2b.order.details.linkcontract"/></td>
                      <% } %>                     
                      <% if (!nonotedetails) { %> 
                        <td class="DetailsTab"><a href="<isa:webappsURL name="b2b/notice_details.jsp"/>" onclick="saveDetailData(); return true;"><isa:translate key="b2b.order.details.linknote"/></a></td>
                      <% } %>
                      <td>&nbsp;</td>
                      <% if (!nosupplydetails) { %>
                        <td class="DetailsTab"><a href="<isa:webappsURL name="b2b/supply_details.jsp"/>" onclick="saveDetailData(); return true;"><isa:translate key="b2b.order.linkaccessories"/></a></td>
                      <% } %>
                      <% if (!nosimproductdetails) { %>
                        <td class="DetailsTab"><a href="<isa:webappsURL name="b2b/similarprod_details.jsp"/>" onclick="saveDetailData(); return true;"><isa:translate key="b2b.order.linkrecommendations"/></a></td>
                      <% } %>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td align="right"><div class="opener"><isa:translate key="b2b.order.details.header.cnt"/>&nbsp;&nbsp;&nbsp;<input type="text" name="position_number" value="1" readonly="readonly" size="3" class="detailCount"></div></td>
              <td>&nbsp;</td>
            </tr>
            <tr class="actualDetails">
              <td colspan="7">&nbsp;</td>
            </tr>
            <tr class="actualDetails">
              <td colspan="7">
                <table class="details" border="0" cellspacing="0" cellpadding="3">
                  <tr>
                    <th>Kontrakt/Position</th>
                    <th>G�ltig bis</th>
                    <th>Versand</th>
                    <th>Zielmenge</th>
                    <th>Abgerufene Menge</th>
                    <th>Zielwert</th>
                    <th>Abgerufener Wert</th>
                  </tr>
                  <tr>
                      <td><a href="#">123421/27</a></td>
                    <td>01.01.02</td>
                    <td>standard</td>
                    <td>5000 St�ck</td>
                    <td>270 St�ck</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr class="actualDetails">
              <td colspan="7">&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </body>
</html>