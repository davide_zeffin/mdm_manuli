<%--
********************************************************************************
    File:         shippingCondition.inc.jsp
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      29.12.2004
    Version:      1.0

    $Revision: #0 $
    $Date: 2004/12/29 $
********************************************************************************
--%>
<% if (ui.isElementVisible("order.shippingCondition")){ %>
    <tr>
       <td class="identifier"><label for="shipCond"><isa:translate key="b2b.order.display.shipcond"/></label></td>
       <td class="value">
          <% if (ui.isElementEnabled("order.shippingCondition"))  { %>
             <select class="select-large" name="shipCond" id="shipCond" >
                 <option value=""><isa:translate key="b2b.order.shipcondpleaseselect"/></option>
                 <isa:iterate id="shipCond"
                     name="<%= MaintainOrderBaseAction.RC_SHIPCOND %>"
                     type="com.sap.isa.core.util.table.ResultData">
                 <option <%= ui.getShipCondSelected(shipCond.getRowKey().toString()) %>  value="<%= shipCond.getRowKey() %>"><%= JspUtil.encodeHtml(shipCond.getString(1)) %></option>
                 </isa:iterate>
             </select>
          <% } else { %>
             <isa:iterate id="shipCond"
                 name="<%= MaintainOrderBaseAction.RC_SHIPCOND %>"
                 type="com.sap.isa.core.util.table.ResultData">
                <%   if (shipCond.getRowKey().toString().equals(ui.header.getShipCond())) {  %>
                   <%= JspUtil.encodeHtml(shipCond.getString(1)) %>
                   <input type="hidden" name="shipCond" id="shipCond" value="<%= shipCond.getRowKey().toString() %>">
                <%  } %>
             </isa:iterate>
          <% } %>
       </td>
    </tr>	                
<% } %>