<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<isa:contentType />

<% BaseUI ui = new BaseUI(pageContext);%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

  <head>
    <title><isa:translate key="b2b.prodrepl.helpwindow.titel"/></title>
    <isa:includes/>
    <!--
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>"
          type="text/css" rel="stylesheet">
          -->

  </head>
  <body class="workarea">
      <div class="module-name"><isa:moduleName name="b2b/productreplacementhelp.jsp" /></div>
	    <p>
      <isa:translate key="b2b.prodrepl.help1"/>
      <ul>
        <li><isa:translate key="b2b.prodrepl.help2.1prelink"/>
            <a href="#a1"><isa:translate key="b2b.prodrepl.help2.2link"/></a>
            <isa:translate key="b2b.prodrepl.help2.3postlink"/>
        <li><isa:translate key="b2b.prodrepl.help3.1prelink"/>
            <a href="#a2"><isa:translate key="b2b.prodrepl.help3.2link"/></a>
            <isa:translate key="b2b.prodrepl.help3.3postlink"/>
        <li><isa:translate key="b2b.prodrepl.help4.1prelink"/>
            <a href="#a3"><isa:translate key="b2b.prodrepl.help4.2link"/></a>
            <isa:translate key="b2b.prodrepl.help4.3postlink"/>
        <li><isa:translate key="b2b.prodrepl.help5.1prelink"/>
            <a href="#a4"><isa:translate key="b2b.prodrepl.help5.2link"/></a>
            <isa:translate key="b2b.prodrepl.help5.3postlink"/>
      </ul>
      <isa:translate key="b2b.prodrepl.help6"/>
      <p><a name="a1">
      <a name="a1"><b><isa:translate key="b2b.prodrepl.help7"/></b></a><br>
      <isa:translate key="b2b.prodrepl.help7a"/>
      <isa:translate key="b2b.prodrepl.help7b"/>
      <p>
      <a name="a2"><b><isa:translate key="b2b.prodrepl.help8"/></b></a><br>
      <isa:translate key="b2b.prodrepl.help8a"/>
      <isa:translate key="b2b.prodrepl.help8b"/>
      <p>
      <a name="a3"><b><isa:translate key="b2b.prodrepl.help9"/></b></a><br>
      <isa:translate key="b2b.prodrepl.help9a"/>
      <isa:translate key="b2b.prodrepl.help9b"/>
      <p>
      <a name="a4"><b><isa:translate key="b2b.prodrepl.help10"/></b></a><br>
      <isa:translate key="b2b.prodrepl.help10a"/>
      <isa:translate key="b2b.prodrepl.help10b"/>
      <p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;
  </body>
</html>