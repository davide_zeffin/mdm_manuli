<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"-->
<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<isa:contentType />

<html>
  <head>
    <title>SAPMarkets Internet Sales B2B - logoff</title>
    <isa:includes/>
    <!--
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>"
          type="text/css" rel="stylesheet">
     -->
     
	<script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComLogin.js") %>"
		type="text/javascript">
	</script>
     
    <script type="text/javascript">
     function loginOutOfIsaTop() {
  		loginInTopFrame("isaTop","<isacore:entryURL name="/b2b/init.do"/>");
     }
    </script>
  </head>
  
  <body class="hints">
    <div id="newdoc" class="module">
      <div class="module-name"><isa:moduleName name="b2b/logoff.jsp" /></div>
      <table border="0" cellpadding="0" cellspacing="0" width="380">
        <tbody>
          <tr>
            <td class="opener_blue" >&nbsp;&nbsp;&nbsp;<isa:translate key="b2b.logoff.visit"/>&nbsp;&nbsp;&nbsp;</td>
            <td style="vertical-align:middle;"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_blue.gif") %>" width="9" height="21" alt="" border="0"></td>
          </tr>
		  <tr>
		  	<td colspan="2">&nbsp;</td>
		  </tr>
		  <tr>
		  	<td colspan="2">
		  	<!-- <a href="<isacore:entryURL name="/b2b/init.do"/>" target="_parent"><isa:translate key="b2b.logoff.logon"/></a>. -->
		  	&nbsp;&nbsp;&nbsp;<a href="#" onclick="loginOutOfIsaTop();"><isa:translate key="b2b.logoff.logon"/></a>.
		  	
		    <%-- Include logic to invalidate the session. --%>
			<%@ include file="/user/logon/session_invalidate.inc" %>
			</td>
		  </tr>
        </tbody>
      </table>
    </div>
  </body>
</html>