<%--
********************************************************************************
    File:         deliveryPriorityHeader.inc.jsp
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      29.12.2004
    Version:      1.0

    $Revision: #0 $
    $Date: 2004/12/29 $
********************************************************************************
--%>
    <% if (ui.isElementVisible("order.deliveryPriority")){ %>
       <tr>               
	        <td class="identifier"><label for="deliveryPriority"><isa:translate key="b2b.order.display.deliverPrio"/></label></td>
	        <td class="value">	                             
              	<% if (ui.isElementEnabled("order.deliveryPriority")) { %>
	                <select class="select-large" name="deliveryPriority" id="deliveryPriority" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.deliv.prio.title"/>" <% } %> >
	                     <% if (!ui.isBackendR3()) { %>
	                      	<option value="00"
							   <% if (ui.header.getDeliveryPriority().equals("00")) { %>
	                      		   selected="selected"
	                      	   <% } %>
	                      	   >&nbsp;
	                      	</option>
	                     <% } %>	                       
	                     <isa:helpValues id="deliveryPriorityList" name="DeliveryPriority">
						    <option value="<%= deliveryPriorityList.getValue("deliveryPriority") %>"
						       <% if (ui.header.getDeliveryPriority().equals(deliveryPriorityList.getValue("deliveryPriority"))) { %>
								   selected="selected"
						       <% } %>
						       >
    					       <%= deliveryPriorityList.getValueDescription("deliveryPriority") %>
        				    </option>
   						 </isa:helpValues>
	                </select>
	            <% } else { %>
	            	<%= ui.getDeliveryPriorityDescription(ui.header.getDeliveryPriority()) %>	                 
	            <% } %>
	        </td>
       </tr>
    <% } %>
    