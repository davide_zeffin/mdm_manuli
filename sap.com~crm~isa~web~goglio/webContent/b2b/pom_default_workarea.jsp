<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"-->
<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.businessobject.oci.OciHelper" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<isa:contentType />

<%  boolean backendR3 = true; %>
<isacore:ifShopProperty property = "backend" value = "<%=ShopData.BACKEND_CRM%>">
  <% backendR3 = false; %>
</isacore:ifShopProperty>

<%
    // Check if oci transfer is required
    boolean ociTransfer =
        (((IsaCoreInitAction.StartupParameter)userSessionData.
            getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).
            getHookUrl().length() > 0);
%>

<html>
    <head>
        <title>SAPMarkets Internet Sales B2B - Default Workarea</title>
        <isa:includes/>
        <!--
        <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>"
              type="text/css" rel="stylesheet">
              -->
	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>	
			  
          <script type="text/javascript" language="javascript" src="jscript/reload.js"></script>
         
		  <script type="text/javascript" language="javascript" src="jscript/make_new_doc.js"></script>
				  
        <script type="text/javascript">
            function openWin( addURL ) {
                var url = addURL;
                var sat = window.open(url, 'OCICatalog', 'width=700,height=500,scrollbars=yes,resizable=yes');
                sat.focus();
            }
        </script>
    </head>

    <body class="hints">
        <div class="module-name"><isa:moduleName name="b2b/pom_default_workarea.jsp" /></div>
        <br><br>
        
        <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="iViewHeader" width="10"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headline_header_small.gif") %>" width="5" height="17" alt="" border="0"></td>
                <td class="iViewHeader"><isa:translate key="b2b.makenewdocstart.createnewdoc"/></td>
           </tr>
           <tr>
                <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="5" alt="" border="0"></td>
           </tr>
        </table>
            
        <table border="0" cellspacing="4">
            <tbody>
                <tr>
                    <td width="20" align="right"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/pfeil_rechts_mit_rand_blau.gif") %>" alt="" border="0"></td>
                    <td><isacore:ocitranslate key="b2b.makenewd.collo.1prelink"/>
                        <a href="#" onclick="create_collective_order(); return false;"><isacore:ocitranslate key="b2b.makenewd.collo.2link"/></a>
                        <isacore:ocitranslate key="b2b.makenewd.collo.3postlink"/>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>