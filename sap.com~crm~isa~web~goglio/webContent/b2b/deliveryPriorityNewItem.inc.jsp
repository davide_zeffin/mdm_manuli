<%--
********************************************************************************
    File:         deliveryPriorityNewItem.inc.jsp
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      29.12.2004
    Version:      1.0

    $Revision: #0 $
    $Date: 2004/12/29 $
********************************************************************************
--%>
   <% if ( ui.isElementVisible("order.item.deliveryPriority") && ui.isElementEnabled("order.item.deliveryPriority")){ %>
	 <tr>           
    	<td class="identifier"><label for="deliveryPriority[<%= ui.line %>]"><isa:translate key="b2b.order.display.deliverPrio"/></label></td>
    	<% if (ui.isMultipleCampaignsAllowed()) { %>
        <td class="campimg-1"></td>
        <% } %>  
    	<td class="value">
        	<select class="select-large" name="deliveryPriority[<%= ui.line %>]" id="deliveryPriority[<%= ui.line %>]" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.deliv.prio.title"/>" <% } %> >
        	 <% if (!ui.isBackendR3()) { %>
    			<option value="00" selected="selected">&nbsp;
                </option>
             <% } %>	                    	
          	<isa:helpValues id="deliveryPriorityList" name="DeliveryPriority">
				<option value="<%= deliveryPriorityList.getValue("deliveryPriority") %>">
					<%= deliveryPriorityList.getValueDescription("deliveryPriority") %>
				</option>
   						  	</isa:helpValues>
        	</select>
    	</td>
	 </tr>	                                
   <% } %>