<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.*" %>

<%@ include file="/b2b/usersessiondata.inc" %>

<%
    CreateTransactionUI ui = new CreateTransactionUI(pageContext);
%>
<isa:contentType />


<%  boolean backendR3 = true; %>
<isacore:ifShopProperty property = "backend" value = "<%=ShopData.BACKEND_CRM%>">
  <% backendR3 = false; %>
</isacore:ifShopProperty>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="b2b.htmltitel.standard"/></title>

        <isa:stylesheets/>

        <script type="text/javascript">
            <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
        </script>

        <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js" ) %>"
            type="text/javascript">
        </script>

        <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/make_new_doc.js" ) %>"
            type="text/javascript">
        </script>

        <script type="text/javascript">
        <!--
            function openWin( addURL ) {
                var url = addURL;
                var sat = window.open(url, 'OCICatalog', 'width=700,height=500,scrollbars=yes,resizable=yes');
                sat.focus();
            }
            //-->
        </script>

    </head>
    <body class="nodoc">
        <div class="module-name"><isa:moduleName name="b2b/pom_welcome.jsp" /></div>
        <% if (ui.welcome) { %>
          <div id="nodoc-first">
        <% } else { %>
          <div id="nodoc-default">
        <% } %>
            <div id="nodoc-header">
                <div><span></span></div>
            </div>
            <div id="nodoc-content">
                <% if (ui.isAccessible) { %>
                    <a href="#end-welcome" title="<isa:translate key="ecm.acc.sec.welcome"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
                <% } %>
                <div id="nodoc-welcome">
                    <h1><span><isa:translate key="b2b.make_new_doc_start.welcome"/></span></h1>
                    <p class="p1"><span><isa:translate key="b2b.welcomePom"/></span></p>
                    <p class="p2"><span><isacore:ocitranslate key="b2b.welcomePom.desc1"/></span></p>
                    <%
                    if (ui.hasCollOrderPermission) { %>
                      <p class="p3"><span><isa:translate key="b2b.welcomePom.desc2a"/></span></p>
                      <p class="p4"><span><isa:translate key="b2b.welcomePom.desc2b"/></span></p>
                    <%
                    } %>
                </div> <!-- nodoc-welcome -->
                <% if (ui.isAccessible) { %>
                    <a name="end-welcome" title="<isa:translate key="ecm.acc.sec.end.welcome"/>."></a>
                <% } %>
                <% if (ui.isAccessible) { %>
                    <a href="#end-transaction" title="<isa:translate key="ecm.acc.sec.transact"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
                <% } %>
                <div id="nodoc-transactions">
                <% if (ui.hasCollOrderPermission) { %>
                    <h1><span><isa:translate key="b2b.makenewdocstart.createnewdoc"/></span></h1>
                    <ul>
                      <li><isacore:ocitranslate key="b2b.makenewd.collo.1prelink"/>
                          <a href="#" onclick="create_collective_order(); return false;"><isa:translate key="b2b.makenewd.collo.2link"/></a>
                          <isa:translate key="b2b.makenewd.collo.3postlink"/>
                      </li>
                    </ul>
                <% } else { %>
                    <% if (!ui.welcome) { %>
                        <p>
                            <isacore:ocitranslate key="b2b.welcomePom.desc1"/>
                        </p>
                    <% } %>
                <% } %>
                </div> <!-- nodoc-transactions -->
                <% if (ui.isAccessible) { %>
                    <a name="end-transaction" title="<isa:translate key="ecm.acc.sec.end.transact"/>"></a>
                <% } %>
            </div> <!-- nodoc-content -->
        </div> <!-- nodoc-first -->
    </body>
</html>


