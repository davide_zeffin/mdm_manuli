<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); %>

<%@ taglib uri="/isa" prefix="isa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    	<title><isa:translate key="b2b.fs.title"/></title>

		<script type="text/javascript">
		<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
		</script>		
	
		<script src="<isa:mimeURL name="b2b/jscript/order.js" />"
		  type="text/javascript">
		</script>		
  	</head>
  	
  	<frameset rows="*,46" id="workareaFS" border="0" frameborder="0" framespacing="0">
	    <frame name="positions" src='<isa:webappsURL name="b2b/showbatch.do"/>' frameborder="0" title="<isa:translate key="b2b.frame.jsp.positions"/>" />
	    <frame name="closer_details" src='<isa:webappsURL name="b2b/closer_batch_details.jsp"/>' frameborder="0" scrolling="no" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.closerdetails"/>" />
    	<noframes>
      		<p>&nbsp;</p>
    	</noframes>
  	</frameset>
</html>