<%--
********************************************************************************
    File:         proddetdesc.inc
    Copyright (c) 2003, SAP AG
    Author:       SAP AG
    Created:      17.05.2003
    Version:      1.0
    
    Determine if productdetermination or substitution information should be displayed
    in the item description.
     
    Prerequisites: shop - must point to the current shop
                   item - must point to the current item 

    $Revision: #1 $
    $Date: 2003/05/17$
********************************************************************************
--%>
<%
  if (shop.isProductDeterminationInfoDisplayed()) {
      String substResDesc = shop.getSubstitutionReasonDesc(item.getSubstitutionReasonId());
      if (substResDesc.length() > 0) {
%>
   <br>(<isa:translate key="b2b.proddsubst.desc"/><%= JspUtil.encodeHtml(substResDesc) %>)
<%
      }
  }
%>