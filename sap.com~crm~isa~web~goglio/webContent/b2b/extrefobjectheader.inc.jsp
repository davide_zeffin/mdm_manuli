<%--
********************************************************************************
    File:         extrefobjectheader.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.03.2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/03/03 $
********************************************************************************
--%>
<% String oldVins = "";
   int h = 0;
   boolean isVinVisibleOnItemLevel = false; 
   boolean isVinVisible = false; %>     
<% if ( ui.isElementVisible("order.extRefObjects") && ui.header.getExtRefObjectType() != null && ui.header.getExtRefObjectType().length() > 0) { 
   isVinVisible = true; %>
   <tr>
        <td class="identifier">
          <label for="extRefObjectType"><%= ui.header.getExtRefObjectTypeDesc() %>:<% if (ui.isServiceRecall()) { %><br><isa:translate key="b2b.order.mandatory"/><% } %></label>
        </td>
        <td class="value">
          <% ExtRefObjectList extRefObjects = ui.header.getAssignedExtRefObjects();
             if (ui.isElementEnabled("order.extRefObjects")) { %>          
                 <input name="vinNum" id="vinNum" class="textinput-large" type="text" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.disp.vin"/><isa:translate key="b2b.acc.more.entries.tab.ret"/>" <% } %> 
                        value="<%= JspUtil.encodeHtml(ui.getExtRefObjValue(extRefObjects,0)) %>"/>&nbsp;                                                
             <% } else { %>
                 <%= JspUtil.encodeHtml(ui.getExtRefObjValue(extRefObjects,0)) %>
             <% } %>
             <% for (int j = 0; j < extRefObjects.size(); j++) { %>
                 <input type="hidden" name="headerVinKeys[<%= j %>]" value="<%= JspUtil.removeNull(extRefObjects.getExtRefObject(j).getTechKey().getIdAsString()) %>"/>
             <% } %>
             <% oldVins = ui.getExtRefObjListAsString(extRefObjects); %>
           	 <input type="hidden" id="addVinNum" name="addVinNum" value="<%= JspUtil.encodeHtml(oldVins) %>"/>
           	 <% if (ui.isBasket()) { %>           	                
                <a href="#" onclick="getVinPopup()" title="<isa:translate key="b2b.order.add.extrefobj.desc"/>">
             <% } else { %>
                 <a href="#" onclick="getVinPopupOC()" title="<isa:translate key="b2b.order.add.extrefobj.desc"/>">
             <% } %>
              <isa:translate key="b2b.order.add.extrefobj"/>
             </a>                  
             
        </td>
    </tr>
<% } %>
