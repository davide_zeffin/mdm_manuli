<%--
********************************************************************************
    File:         extrefnumbernewitem.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      04.04.2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/04/04 $
********************************************************************************
--%>
 
<% if (ui.header.getAssignedExternalReferences() != null && ui.header.getAssignedExternalReferences().size() > 0 
       && ui.isElementVisible("order.item.extRefNumbers") && ui.isElementEnabled("order.item.extRefNumbers") ) {
	  ExternalReference externalReference;
	  for (int j = 0; j < ui.header.getAssignedExternalReferences().size(); j++) {
		externalReference = (ExternalReference)ui.header.getAssignedExternalReferences().getExtRef(j);
%>		
        <tr>
           <td class="identifier">
              <label for="itemExtRefNumberType"><%= externalReference.getDescription() %>:</label>
           </td>
           <% if (ui.isMultipleCampaignsAllowed()) { %>
           <td class="campimg-1"></td>
           <% } %>  
           <td class="value"> 
              <input name="itemExtReferenceNumber[<%= ui.line %>][<%= j %>]" id="extRefNum" maxlength="35" class="textinput-large" type="text" value=""/>&nbsp;   
              <input type="hidden" name="itemExtReferenceType[<%= ui.line %>][<%= j %>]" value="<%= JspUtil.encodeHtml(externalReference.getRefType()) %>"/>
           </td>   
        </tr>
<%	  }
   } 
%>