<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>

<% String message = (String) request.getAttribute(IsaCoreBaseAction.RC_PRECONDITION_MESSAGE); %>

<html>
<head>
  <title><isa:translate key="preconditions.title.default"/></title>
</head>

<body>
    <div class="module-name"><isa:moduleName name="b2b/preconditionerror.jsp" /></div>

        <isa:translate key="<%= message %>"/>


</body>
</html>