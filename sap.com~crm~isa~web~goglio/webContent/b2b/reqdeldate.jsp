<% /** This JSP displays a screen to select, how the changed
       requested deliverydate on header level should be handeld
   **/
%>

<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
  <title>
        <isa:translate key="b2b.fs.title"/>
  </title>

  <isa:includes/>

  <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
              type="text/javascript">
  </script>

</head>

<body class="ship">

<div class="module">
  <div class="module-name"><isa:moduleName name="b2b/reqdeldate.jsp" /></div>

  <form name="Selection" >
  
    <table class="vertical-align-middle" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr><td colspan="3">&nbsp;</td></tr>
      <tr>
         <td>&nbsp;</td>
         <td colspan="2"><b><isa:translate key="b2b.reqdel.chg.header"/></b></td>
      </tr>
      <tr><td colspan="3">&nbsp;</td></tr>
      <tr>
         <td>&nbsp;</td>
         <td><input type="radio" name="selection" onclick="sel = updTypeOnlyNewPos;" checked="checked"></td>
         <td><isa:translate key="b2b.reqdel.chg.sel1"/></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td><input type="radio" name="selection" onclick="sel = updTypeAll;"></td>
         <td><isa:translate key="b2b.reqdel.chg.sel2"/></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td><input type="radio" name="selection" onclick="sel = updTypeEqToHeader;"></td>
         <td><isa:translate key="b2b.reqdel.chg.sel3"/></td>
      </tr>
      <tr><td colspan="3">&nbsp;</td></tr>
      <tr>
         <td>&nbsp;</td>
         <td align="right" colspan="2"><input class="green" type="button" onclick="window.close(); opener.updItemReqDeliveryDate(sel);" value="<isa:translate key="b2b.docnav.ok"/>">&nbsp;&nbsp;</td>
      </tr>
    </table>

  </form>
</div>

</body>
</html>


