<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); %>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	  <head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<title><isa:translate key="b2b.fs.title"/></title>
	
		<script src="<isa:webappsURL name="b2b/jscript/fs_startworkarea.js"/>"
                type="text/javascript">
        </script>
	  </head>

	  <frameset cols="*,15,95" id="fourthFS" border="0" frameborder="0" framespacing="0">
		<frameset rows="0,*" id="workFS" border="0" frameborder="0" framespacing="0">
		  <frame name="documents" src="<isa:webappsURL name="/b2b/updateworkareanav.do" />" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" />
		  <frame name="form_input" src="<isa:webappsURL name="/b2b/updatedocumentview.do?refresh=1" />" frameborder="0" scrolling="auto" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.forminput"/>" />
		</frameset>
		<frame name="closer_history" src="<isa:webappsURL name="/b2b/closer_history.jsp" />" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.closerhistory"/>" />
		<frame name="_history" src="<isa:webappsURL name="/b2b/history/updatehistory.do" />" frameborder="0" scrolling="auto" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.history"/>" />
		<noframes>
		  <p>&nbsp;</p>
		</noframes>        
	  </frameset>
</html>