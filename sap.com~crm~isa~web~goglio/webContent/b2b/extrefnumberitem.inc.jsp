<%--
********************************************************************************
    File:         extrefnumberitem.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      04.04.2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/04/04 $
********************************************************************************
--%>
 
<% if (item.getAssignedExternalReferences() != null && item.getAssignedExternalReferences().size() > 0 && ui.isElementVisible("order.item.extRefNumbers", itemKey)) {
	  ExternalReference externalReference;
	  for (int i = 0; i < item.getAssignedExternalReferences().size(); i++) {
		externalReference = (ExternalReference)item.getAssignedExternalReferences().getExtRef(i);
%>		
        <tr>
           <td class="identifier">
              <label for="itemExtRefNumberType"><%= externalReference.getDescription() %>:</label>
           </td>
           <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
		   <td class="campimg-1"></td>
		   <% } %> 
           <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>"> 
              <% if (ui.isElementEnabled("order.item.extRefNumbers", itemKey)) { %>
                  <input name="itemExtReferenceNumber[<%= ui.line %>][<%= i %>]" id="extRefNum" maxlength="35" class="textinput-large" type="text" value="<%= JspUtil.encodeHtml(externalReference.getData()) %>"/>&nbsp;   
              <% } else { %>
                  <%= JspUtil.encodeHtml(externalReference.getData()) %>
              <% } %>
              <input type="hidden" name="itemExtReferenceType[<%= ui.line %>][<%= i %>]" value="<%= JspUtil.encodeHtml(externalReference.getRefType()) %>"/>
              <input type="hidden" name="itemExtReferenceTechKey[<%= ui.line %>][<%= i %>]" value="<%= externalReference.getTechKey() %>"/>
           </td>   
        </tr>
<%	  }
   } 
%>