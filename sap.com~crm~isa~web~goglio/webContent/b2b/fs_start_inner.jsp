<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.isacore.action.IsaCoreInitAction" %>
<%@ include file="/b2b/usersessiondata.inc" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.HeaderUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<script type="text/javascript">
          <%@ include file="/b2b/jscript/fs_start_inner.js"  %>
</script>

<% 
	//Load UI-Class for Portal-Switch
	HeaderUI ui = new HeaderUI(pageContext);
%>	

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="b2b.fs.title"/></title>

 <% if( !ui.showCatalog() ) { %>
  	<script type="text/javascript">
  	<!--
        var workHistoryInterval; 

        workHistoryInterval = setInterval("loadPage()",500);
	//-->
  	</script>
 <% } %>

  </head>
  <% if (ui.isPortal) {%>
  <frameset rows="28,*" id="mainFS" border="0" frameborder="0" framespacing="0">
  <% } else{ %>
  <frameset rows="76,*" id="mainFS" border="0" frameborder="0" framespacing="0">
  <% } %>
  
    <frame name="header" src="<isa:webappsURL name="/b2b/header.do"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.header"/>" />

    <% if (!ui.showCatalog()) { %>
       <frameset cols="25%,15,*" id="secondFS" frameborder="0" framespacing="0">
         <frameset rows="53,*" id="thirdFS" frameborder="0" framespacing="0">
           <frame name="organizer_nav" src="<isa:webappsURL name="/b2b/updateorganizernav.do"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.organizernav"/>" />
           <frame name="organizer_content" src="<isa:webappsURL name="/b2b/updateorganizercont.do"/>" frameborder="0" scrolling="auto" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.organizercon"/>" />
         </frameset>
         <frame name="closer_organizer" src="<isa:webappsURL name="/b2b/closer_organizer.jsp"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.closerorganizer"/>" />
         <frame name="work_history" src="<isa:webappsURL name="/b2b/empty.jsp"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" />
       </frameset>
  <% }
     else { %>
       <frameset cols="*,15,95" id="secondFS" frameborder="0" framespacing="0">
          <frame name="form_input" src="<isa:webappsURL name="/b2b/showCatalog.do"/>" frameborder="0" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.catalog"/>" />
          <frame name="closer_history" src="<isa:webappsURL name="/b2b/closer_history.jsp" />" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.closerhistory"/>" />
          <frame name="work_history" src="<isa:webappsURL name="/b2b/history/updatehistory.do" />" frameborder="0" scrolling="auto" marginwidth="0" marginheight="0" title="<isa:translate key="b2b.frame.jsp.history"/>" />
       </frameset> 
  <% } %>  
    <noframes>
      <p>&nbsp;</p>
    </noframes>
  </frameset>
</html>