<%--
********************************************************************************
    File:         itemErrMsg.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.03.2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/03/11 $
********************************************************************************
--%>  
<%@ page import="com.sap.isa.core.taglib.MessageTag" %>
<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.isa.core.util.Message" %>

<%  ui.setItemCSSClass(ui.even_odd + "-error");

    boolean itemErrMsgExist = false;
    boolean itemWarnMsgExist = false;
    boolean itemInfoMsgExist = false;
    
    // read complete message list and check if any messages are available
    MessageList itemMsgList = MessageTag.getMessageListFromContext(pageContext, "item");
    if(itemMsgList != null) {
    	itemErrMsgExist = (itemMsgList.subList(Message.ERROR) == null) ? false : true;
    	itemWarnMsgExist = (itemMsgList.subList(Message.WARNING) == null) ? false : true;
    	itemInfoMsgExist = (itemMsgList.subList(Message.INFO) == null) ? false : true;
    }
    
    // only if some messages should be displayed, we write one <TR> and there all messages
    if (itemErrMsgExist || 
    		itemWarnMsgExist || 
    		itemInfoMsgExist ||
    		item.isBackordered()) {
%>
<tr class="<%=ui.even_odd %>"> 
    <td>&nbsp;</td>
    <td colspan="<%= ui.getItemsColspan() %>" class="item-msg-area">

        <%-- Error messages --%>
        <isa:message id="errortxt" name="item" type="<%=Message.ERROR%>" >
            <% if (errortxt != null && errortxt.length() > 0) { %>
                <div class="error-items">
                    <span>
                        <% if (ui.getFirstErroneousPos() + 1 == ui.line) { %>
                            <a name="firsterritem"></a>
                        <% } %>     
                        <%= errortxt %>
                        <% if (item.isProductAliasAvailable()) { %> 
                            <a href="#" onclick='submit_refresh();'>                
                                <isa:translate key="b2b.proddet.sel"/>
                             </a>
                        <% } %>
                        <% if (item.isDeterminedCampaignAvailable()) { %> 
                            <isa:translate key="b2b.campaign.sel.prelink"/>
                            <a href="#" onclick='submit_refresh();'> <isa:translate key="b2b.campaign.sel.link"/> </a>
                            <isa:translate key="b2b.campaign.sel.postlink"/>
                        <% } %>
                    </span>
                </div>
            <% } %>	          
        </isa:message>

        <%-- Warning messages --%>
        <isa:message id="warntxt" name="item" type="<%=Message.WARNING%>" >
            <% if (warntxt != null && warntxt.length() > 0) { %>
                <div class="warn-items">
                    <span>   
                        <%= warntxt %>
                    </span>
                </div>
            <% } %>	          
        </isa:message>      

        <%-- Info messages --%>
        <isa:message id="infotxt" name="item" type="<%=Message.INFO%>" >
            <% if (infotxt != null && infotxt.length() > 0) { %>
                <div class="info-items">
                    <span>   
                        <%= infotxt %>
                    </span>
                </div>
            <% } %>	          
        </isa:message>  
 
<%-- information, that item is not confirmed related to the delivery date --%>
<%      if (item.isBackordered()) { %>
        <div class="info-items">
            <span><isa:translate key="b2b.ordr.item.backorder"/></span>
        </div>
<%      } %>

    </td>
</tr>

<%  } %>


