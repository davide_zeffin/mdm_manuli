<%--
********************************************************************************
    File:         determination.jsp
    Copyright (c) 2001, SAP Germany, All rights reserved.

    Author:       SAP
    Created:      15.5.2003
    Version:      1.0

    $Revision: #1 $
    $Date: 2003/05/15 $
********************************************************************************
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.core.businessobject.management.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.DeterminationUI" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="checksession.inc" %>
<%@ include file="usersessiondata.inc" %>

<isa:contentType />
<isacore:browserType/>

<%  DeterminationUI ui = new DeterminationUI(pageContext);
                        
    // line item related variables
    int line = 0;
    AlternativProductList altProducts; 
    AlternativProduct altProduct;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">

  <head>
   <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>
        <isa:translate key="b2b.order.display.pagetitle"/>
    </title>
    
    <isa:stylesheets/>

      <script type="text/javascript">
          <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
      </script>

      <script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/table_highlight.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/browsersniff.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
          type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
          type="text/javascript">
      </script>
	  
	  <script type="text/javascript">
		  function send_confirm() {
			document.forms["order_positions"].sendpressed.value="true";
			document.forms["order_positions"].submit();
			return true;
		  }
		  
		  function cancel_confirm() {
			document.forms["order_positions"].target = "form_input";
			document.forms["order_positions"].cancelpressed.value="true";
			document.forms["order_positions"].submit();
			return true;
		  }
      </script>
      
  </head>
  <body class="determination">
  <div class="module-name"><isa:moduleName name="b2b/determination.jsp" /></div>
  <h1><span><%= JspUtil.encodeHtml(ui.getHeaderDescription()) %>:&nbsp;
     <% if (ManagedDocumentDetermination.DOC_MODE_NEW.equals(ui.getDocMode())) { %>
       <isa:translate key="b2b.docnav.new"/>
     <% }
        else { %>
       <%= ui.getDocNumber() %>
    <% } %></span></h1>
  <div id="document">
      <div class="document-header">
      <%-- !!!! Do not change the name of the form, the JavaScript functions rely on them !!!! --%>
      <form action="<isa:webappsURL name="b2b/determination.do"/>"
            name="order_positions"
            method="post">
      <a id="access-header" href="#access-items" title="<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>
      <isacore:requestSerial mode="POST"/>

      <%-- Header table--%>  
      <a id="access-header" href="#access-items" title= "<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>
      <div class="header-basic">
      <table class="layout">
	    <tr>
		    <td class="col1">
		      <% if (ui.isAccessible) { %>
                  <a href="#end-table1" title="<isa:translate key="b2b.acc.header.dat.link"/>"></a>
              <% } %>
		      <table class="header-general" summary="<isa:translate key="b2b.acc.header.dat.sum"/>">                            
              <%-- render the consumer, only in B2R scenario --%>
              <% if (ui.isSoldToVisible()) { %>
              <tr>
                  <td class="identifier"><isa:translate key="b2b.order.display.cnsmr.b2r.name"/> (<isa:translate key="b2b.order.display.cnsmr.b2r.no"/>)</td>
                  <td class="value"><%=JspUtil.encodeHtml(ui.getSoldToName()) %> (<%= JspUtil.removeNull(ui.getSoldToId()) %>)</td>
              </tr>
             <% } %>
              <tr>
                  <td class="identifier"><% if (!ui.isOciTransfer() || !ui.isBasket()) { %><isa:translate key="b2b.order.display.ponumber"/><% } %></td>
                  <td class="value"><% if (!ui.isOciTransfer() || !ui.isBasket()) { %><%= JspUtil.encodeHtml(JspUtil.removeNull(ui.header.getPurchaseOrderExt())) %><% } %></td>
              </tr>
              <tr>
                  <td class="identifier"><% if (!ui.isBackendR3() && (!ui.isOciTransfer() || !ui.isBasket())) { %> <isa:translate key="b2b.order.display.poname"/><% } %></td>
                  <td class="value"><% if (!ui.isBackendR3() && (!ui.isOciTransfer() || !ui.isBasket())) { %> <%= JspUtil.encodeHtml(JspUtil.removeNull(ui.header.getDescription())) %> <% } %></td>
              </tr>
              </table>
              <% if (ui.isAccessible) { %>
                  <a name="#end-table1" title="<isa:translate key="b2b.acc.header.dat.end"/>"></a>
              <% } %>
          </td>			            
          <td class="col2">
          </td>
      </tr>
     </table> <%-- class="layout" --%>
	 </div> <%-- class="header-basic" --%>
     <div class="error"><span><isa:translate key="b2b.proddet.note"/></span></div>
     </div> <%-- document-header --%>
     
     <div class="document-items"> 
     <a id="access-items" href="#access-buttons" title= "<isa:translate key="b2b.acc.items.title"/>" accesskey="<isa:translate key="b2b.acc.items.key"/>"></a>
     <%-- the alternative product information for the items  --%>
     <isa:iterate id="item" name="<%= MaintainBasketBaseAction.RK_ITEMS %>" type="com.sap.isa.businessobject.item.ItemSalesDoc">
      <% ui.setItem(item);
         
         if (ui.isDeterminationNecessary()) {
             String desc = "(" + JspUtil.encodeHtml(item.getDescription()) + ")";
             if (ui.isAccessible) { %> 
                <a href="#end-table<%=line + 2 %>" title="<isa:translate key="b2b.acc.items.link.det"/>"></a>
          <% } %>
           <table class="itemlist" summary="<isa:translate key="b2b.acc.items.title"/>">
             <tr>
               <td colspan="4">
                 <div class="info"><span>
                 <% if (ui.isProdSubst()) { %>
                      <isa:translate key="b2b.prodsubst.msg" arg0="<%= item.getNumberInt() %>" arg1="<%= JspUtil.encodeHtml(item.getProduct()) %>" arg2="<%= desc %>" />
                 <% }
                    else if (ui.isProdDet()) { %>
                      <isa:translate key="b2b.proddet.msg" arg0="<%=item.getNumberInt()%>" arg1="<%=JspUtil.encodeHtml(item.getProduct())%>"/>
                 <% } 
                    else if (ui.isCampDet()) { %>
                      <isa:translate key="b2b.campdet.msg" arg0="<%= item.getNumberInt() %>" arg1="<%= JspUtil.encodeHtml(item.getProduct()) %>" arg2="<%= desc %>" />
                 <% } %>
                 </span></div>
               </td>
             </tr>
             <tr>
                <td colspan="4" class="separator"></td>
             </tr>
             <input type="hidden" name="itemTechKey[<%= line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>
         <% if (ui.showPostPoneOption()) { %>
             <tr>
                 <td class="select"><input type="radio" name="alt[<%= line %>]" value="<%= DeterminationAction.NO_SELECTION%>"/></td>
                 <td class="desc" colspan="3"><isa:translate key="b2b.proddet.noprod"/></td>
             </tr>
             <tr>
                <td colspan="4" class="separator"></td>
             </tr>
         <% }    
            if (ui.isCampDet()) { %>
             <tr>
                 <td class="select"><input type="radio" name="alt[<%= line %>]" value="<%= DeterminationAction.ASSIGN_NO_CAMPAIGN%>"/></td>
                 <td class="desc" colspan="3"><isa:translate key="b2b.det.nocamp"/></td>
             </tr>
			 <tr>
                <td colspan="4" class="separator"></td>
             </tr>
          <% } 
             if (ui.isProdDet()) { %>
             <tr>
                 <td class="desc" colspan="4">&nbsp;&nbsp;<isa:translate key="b2b.proddet.listed" arg0="<%= JspUtil.encodeHtml(item.getProduct()) %>"/></td>
             </tr>
             <tr>
                <td colspan="4" class="separator"></td>
             </tr>
          <% } 
             for (int j=0; j < ui.getDetListSize(); j++) { 
          %>
             <tr>
                 <td class="select"><input type="radio" name="alt[<%=line%>]" value="<%=j%>"/></td>
              <% if (ui.isProdSubst()) { %>
                 <td class="desc"><%= ui.getSystemProductId(j)%></td>
                 <td class="desc"><%= JspUtil.encodeHtml(ui.getDescription(j))%></td>
                 <td class="desc"><%= JspUtil.encodeHtml(ui.getSubstitutionReasonDesc(j)) %></td>
              <% } 
                 else if (ui.isProdDet()) { %>
                 <td class="desc" colspan="3"><isa:translate key="b2b.proddet.for" arg0="<%= JspUtil.encodeHtml(ui.getproductIdTypeDesc(j)) %>" arg1="<%= JspUtil.encodeHtml(ui.getIdAndDesc(j)) %>"/></td>
              <% } 
                 else if (ui.isCampDet()) {%>
                 <td class="desc"><%= ui.getItemDetCampaignId(j)%></td>
                 <td class="desc"><%= JspUtil.encodeHtml(ui.getItemDetCampaignDesc(j))%></td>
                 <td class="desc"><%= JspUtil.encodeHtml(ui.getItemDetCampaignTypeDesc(j)) %></td>
              <% } %>
             </tr>
             <tr>
                <td colspan="4" class="separator"></td>
             </tr>
           <% } 
              line++; %>
            </table>
           <% if (ui.isAccessible) { %>
		    <a name="end-table<%=line + 2 %>" title="<isa:translate key="b2b.acc.items.end.det"/>"></a>
	       <% } %>
           <br/>
        <% } %>  
      </isa:iterate>
     <input type="hidden" name="sendpressed" value=""/>
     <input type="hidden" name="cancelpressed" value=""/>
   </form>
  </div> <%-- document-items --%> 
 </div> <%-- document --%>
 
     <%-- Buttons --%>
 <div id="buttons">
    <a id="access-buttons" href="#access-header" title="<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>
    <ul class="buttons-3">
        <li><a href="#" onclick="cancel_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cncl"/>" <% } %> ><isa:translate key="b2b.order.display.submit.cancel"/></a></li>
        <li><a href="#" onclick="send_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.ok"/>" <% } %> ><isa:translate key="b2b.docnav.ok"/></a></li>
    </ul>
 </div>
 <%-- End  Buttons--%>
</body>
</html>
