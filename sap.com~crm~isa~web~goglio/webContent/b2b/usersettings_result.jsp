<%-- usersettings_result.jsp --%>

<%-- This JSP displays the AddressChange confirmation screen --%>

<%@ taglib prefix="isa" uri="/isa" %>

<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.IsaCoreInitAction" %>

<%  
      UserSessionData userSessionData =
              UserSessionData.getUserSessionData(pageContext.getSession());

    // Are we running in a portal?
    // check if portal is given as an startupParameter

    IsaCoreInitAction.StartupParameter startupParameter =
        (IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
    boolean portal = false;

    if (startupParameter != null) {
        String portalParameter = startupParameter.getPortal();
        if (portalParameter.length() > 0) {
            if (portalParameter.equals("YES")) {
                portal = true;
            }
        }
    }
%>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <title><isa:translate key="userSettings.jsp.title"/></title>
    <isa:stylesheets/>
  </head>
  
<body class="popup">
<div class="module-name"><isa:moduleName name="b2b/usersettings_result.jsp" /></div>

<div class="content">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td>
        <isa:message id="errortext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
                     type="<%=Message.ERROR%>" >
          <div class="error">
            <span><%=errortext%></span>
          </div>			
        </isa:message>
        <isa:message id="infotext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
                     type="<%=Message.INFO%>" >
          <div class="info">
            <span><%=infotext%></span>
          </div>			
        </isa:message>
      </td>
    </tr>
    <tr>
      <td>
        <br />
<% if (portal) { %>
        <input class="green" type="button" value="<isa:translate key="userSettings.jsp.close"/>"
               name="backward" onclick="location.href='<isa:webappsURL name="/b2b/showaddresschange.do"/>'" />
<% } else { %>
        <input class="green" type="button" value="<isa:translate key="userSettings.jsp.close"/>"
               name="backward" onclick="location.href='<isa:webappsURL name="/b2b/user_settings_info.jsp"/>'" />
<% } %>
      <br /><br />
      </td>
    </tr>
  </tbody>
</table>
</div>   

</body>
</html>