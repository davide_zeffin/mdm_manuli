<%-- Include to display bestseller -- %>
<%-- The Javascript library "b2b/jscript/ipc_pricinganalysis.jsp" must be loaded --%>

<%-- use this include only as a dynamic include --%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.BestsellerUI" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<% ProductListUI ui = new BestsellerUI(pageContext); %>

<div class="bestseller">
  <div class="module-name"><isa:moduleName name="b2b/marketing/bestseller.inc.jsp" /></div>
  <%@ include file="/b2b/marketing/productlist.inc.jsp" %>

</div>

