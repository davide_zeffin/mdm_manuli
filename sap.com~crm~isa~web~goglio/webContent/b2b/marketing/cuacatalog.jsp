<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowCUAAction" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.CuaUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.catalog.uiclass.ProductsUI"%>


<% CuaUI ui = new CuaUI(pageContext); 
   ui.setTarget("form_input"); %>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.sap.isa.catalog.actions.ActionConstants"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%= ui.getLanguage() %>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="cua.jsp.title"/></title>
    
    <isa:stylesheets/>
	<script type="text/javascript">
		<!--
    	function goBack() {
    	 <% if (ui.getItem().getItemKey().getParentCatalog().getSavedItemList() != null) {
              if (ui.IsItemListAreaQuery()) { %>
                  parent.location.href="<isa:webappsURL name="/catalog/ProductsContainer.jsp?toNext=toItemPage"/>";
           <% } 
              else { %>
                  parent.location.href="<isa:webappsURL name="/catalog/setSavedItemList.do"/>";
           <% } 
            } 
            else {
            	if (ui.IsItemListQuery()) { %>
					parent.location.href="<isa:webappsURL name="/catalog/ProductsContainer.jsp?toNext=toItemPage_catalog"/><%=ProductsUI.determineScenario(request, true)%>";
			 <% } 
				else { %>
                    parent.location.href="<isa:webappsURL name="/catalog/ProductsContainer.jsp"><isa:param name="<%=ActionConstants.RESET_SEARCH%>" value="true"/></isa:webappsURL>";
         <%    }
            } %>
        }
		//-->
	</script>
	
   <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
      <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' type = "text/javascript"></script>
   </isacore:ifShopProperty>
  
</head>

<% if ( request.getParameter("inFrame")!= null && request.getParameter("inFrame").equals("yes") ) {%>
 <body class="organizerCatalog">
<%@ include file="../../catalog/cuaHeader.inc.jsp"%>
<% } else {%>
 <body class="organizerCatalog">
<% } %>
    <div class="module-name"><isa:moduleName name="b2b/marketing/cuacatalog.jsp" /></div>

<%
ui.setMethod(CuaUI.METHOD_ACTION);
ui.setDisplayType((String) request.getAttribute(ShowCUAAction.RC_CUATYPE)); %>

<%@ include file="cualist.jsp"%>

<% if ( request.getParameter("inFrame")== null || !request.getParameter("inFrame").equals("yes") ) {%>
  <p>
  <%
  String backBtnTxt = WebUtil.translate(pageContext, "error.jsp.back", null); %>
  <input class="green" type="button" onclick="goBack()" title="<isa:translate key="access.button" arg0="<%=backBtnTxt%>" arg1=""/>" value="<isa:translate key="error.jsp.back"/>">
<% } %>

</body>
</html>
