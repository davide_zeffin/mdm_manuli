<%-- Inlcude to display a marketing product list. either bestseller or recommendations --%>
<%-- ProductListUI ui must be define outside  --%>

<%-- The Javascript library "b2b/jscript/ipc_pricinganalysis.jsp" must be loaded --%>


<%-- use this include only as a static include --%>

<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference" %>

<% if (ui.isAccessible()) { %>
  <%@page import="com.sap.isa.catalog.actions.ActionConstants"%>
<%@page import="com.sap.isa.core.util.WebUtil"%>
<%@page import="com.sap.isa.core.util.JspUtil"%>
<a href="#groupcualist-end"
     id="groupcualist-begin"
     title="<isa:translate key="access.grp.begin" arg0="<%=ui.getTitle()%>"/>"></a>
<%
}%>

  <h1><%=ui.getTitle()%></h1>
  <div class="product-recommendations">

<%

/*String detailScenario = (String)request.getAttribute(ActionConstants.RA_DETAILSCENARIO);
if (detailScenario==null) {
	detailScenario="";
}*/

/* Check, if the productlist contains products */
if (ui.getProductList().size() > 0) {
%>
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <% int line = 0; %>
        <isa:iterate id="product" name="<%= ProductListUI.PC_PRODUCT_LIST %>"
                     type="com.sap.isa.businessobject.Product">

          <%
          if (line < 4) {
              line++;
              
              // prepare the product detail link
              String productDetailsLink = WebUtil.getAppsURL(pageContext, Boolean.valueOf(request.isSecure()), "b2b/productdetail.do", null, null, false)
              								+ ShowProductDetailAction.createDetailRequest(product,JspUtil.encodeURL(ui.getScenario()));
              %>
          <td>
            <table  border="0" cellpadding="4" cellspacing="0" width="100%">
              <tbody>
                <tr>
                  <td>
                    <%
                    if (product.getThumb().length() > 0) { %>
                      <img src="<%=product.getThumb() %>" alt="<%=product.getPicture() %>"/>
                    <%
                    }
                    else { %>
                      <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/small.gif" ) %>" alt="Thumbnail"/>
                  <%
                  }%>
                  </td>
                  <td width="100%">
                    <a href="<%=productDetailsLink %>">
                      <%=product.getId() %></a> <br/>
                    <a href="<%=productDetailsLink %>">
                      <%=JspUtil.encodeHtml(product.getDescription()) %>
                    </a> <br>
                    <%
                    if (ui.isListPriceAvailable()) { %>
                      <br/>
                      <isa:translate key="catalog.isa.price"/>&nbsp;<%=product.getListPrice() %>
                    <%
                    }
                    if (ui.isCustomerSpecificPriceAvailable()) { %>
                      <br/>
                      <isa:translate key="catalog.isa.IPCPrice"/>&nbsp;<%=product.getCustomerSpecificPrice() %>

                      <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
                        <%
                        IPCItemReference genIpcItemRef = product.getIPCItemReference();
						RFCIPCItemReference ipcItemRef = (RFCIPCItemReference)genIpcItemRef;
                        if (ipcItemRef != null) { %>
                          &nbsp;
                          <a href="#" onclick="displayIpcPricingConds('<%= JspUtil.removeNull(ipcItemRef.getConnectionKey()) %>',
                                                                      '<%= JspUtil.removeNull(ipcItemRef.getDocumentId()) %>',
                                                                      '<%= JspUtil.removeNull(ipcItemRef.getItemId()) %>'
                                                                      )"><isa:translate key="b2b.order.display.ipcconds"/></a>
                        <%
                        } %>
                      </isacore:ifShopProperty>

                      <br/>
                    <%
                    }%>

                    <a href="<isa:webappsURL name="b2b/addproducttodocument.do"/><%=GetTransferItemFromProductAction.createAddRequest(product,1) %>"
                     ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_wk_blue.gif" ) %>" height="16" width="16"
                           alt="<isa:translate key="product.jsp.addToBasket"/>"
                           title="<isa:translate key="product.jsp.addToBasket"/>" 
                           border="0"/></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
          <%
          }%>
        </isa:iterate>
        </tr>
     </tbody>
  </table>
<%
}
else { %>
  <div class="info">
      <span><%=ui.getNotFoundMessage()%></span>
  </div>
<%
}%>
</div>

<%
if (ui.isAccessible()) { %>
  <a href="#groupcualist-begin"
     id="groupcualist-end"
     title="<isa:translate key="access.grp.end" arg0="<%=ui.getTitle()%>"/>"></a>
<%
}%>

