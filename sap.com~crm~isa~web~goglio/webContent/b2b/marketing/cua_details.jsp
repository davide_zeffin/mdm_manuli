<%--
********************************************************************************
    File:         cua_details.jsp
    Copyright (c) 2001, SAP Germany, All rights reserved.
    Created:      June 2001
    Version:      1.0

    $Revision: #01 $
    $Date: 2002/06/21 $
********************************************************************************
--%>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>

<%@ page import="com.sap.isa.isacore.action.marketing.ShowCUAAction" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.CuaUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>


<% CuaUI ui = new CuaUI(pageContext); %>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="b2b.order.details.cua.title"/></title>
    <isa:stylesheets/>

    <script type="text/javascript">
    <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
    </script>
    <script src="<isa:mimeURL name="b2b/jscript/openwin.js" />"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
        type="text/javascript">
    </script>
    <script type="text/javascript">
    <!--
        function goBack() {
            getElement("updatedoc").submit();
        }
    //-->
    </script>

    <%@ include file="/b2b/marketing/cualistHead.inc.jsp" %>
</head>

<body class="organizerCatalog">
  <div class="module-name"><isa:moduleName name="b2b/marketing/cua_details.jsp" /></div>

  <isacore:ifShopProperty property = "accessoriesAvailable" value ="true" >
    <h1><isa:translate key="b2b.order.details.header.supply"/></h1>

    <%
    ui.setMethod(CuaUI.METHOD_ACTION);
    ui.setDisplayType(ShowCUAAction.ACCESSORY);
    ui.setTitle("b2b.order.details.header.supply"); %>
    <%@ include file="/b2b/marketing/cualist.inc.jsp" %>
  </isacore:ifShopProperty>


  <isacore:ifShopProperty property = "alternativeAvailable" value ="true">
    <h1><isa:translate key="b2b.order.header.upselling"/></h1>

    <%
    ui.setReplaceActionMethod();
    ui.setDisplayType(ShowCUAAction.ALTERNATIVES);
    ui.setTitle("b2b.order.header.upselling"); %>
    <%@ include file="/b2b/marketing/cualist.inc.jsp" %>
  </isacore:ifShopProperty>

  <isacore:ifShopProperty property = "crossSellingAvailable" value ="true">
    <h1><isa:translate key="b2b.order.details.header.similar"/></h1>

    <%
    ui.setMethod(CuaUI.METHOD_ACTION);
    ui.setDisplayType(ShowCUAAction.CROSSSELLING);
    ui.setTitle("b2b.order.details.header.similar"); %>
    <%@ include file="/b2b/marketing/cualist.inc.jsp" %>
  </isacore:ifShopProperty>


  <isacore:ifShopProperty property = "remanufactureAvailable" value ="true">
    <%
    ui.setReplaceActionMethod();
    ui.setDisplayType(ShowCUAAction.REMANUFACTURED);
    ui.setTitle("marketing.cua.remanufacture");
    if (ui.getProductList().size() > 0) { %>

      <h1><isa:translate key="marketing.cua.remanufacture"/></h1>
      <%@ include file="/b2b/marketing/cualist.inc.jsp" %>
    <%
    }%>
  </isacore:ifShopProperty>

  <isacore:ifShopProperty property = "newPartAvailable" value ="true">
    <%
    ui.setReplaceActionMethod();
    ui.setDisplayType(ShowCUAAction.NEWPART);
    ui.setTitle("marketing.cua.newpart");
    if (ui.getProductList().size() > 0) { %>

      <h1><isa:translate key="marketing.cua.newpart"/></h1>
      <%@ include file="/b2b/marketing/cualist.inc.jsp" %>
    <%
    }%>
  </isacore:ifShopProperty>


  <form id="updatedoc" action="<isa:webappsURL name="b2b/updatedocumentview.do"/>" >
    <%
    String backBtnTxt = WebUtil.translate(pageContext, "error.jsp.back", null); %>
    <div  id="buttons">
        <ul class="buttons-1">
            <li>
            <a href="#" onclick="goBack();" <% if (ui.isAccessible) { %> title="<isa:translate key="access.button" arg0="<%= backBtnTxt %>" arg1=""/>" <% } %> ><isa:translate key="b2b.login.pwchange.back"/></a>
            </li>
        </ul>
    </div>
  </form>

</body>
</html>
