<%-- Include for cua functionality --%>

<%-- !! use this include only as a static include !! --%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.businessobject.marketing.CUAList" %>
<%@ page import="com.sap.isa.isacore.action.marketing.ShowCUAAction" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.isacore.action.GetTransferItemFromProductAction" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>
 

<script type="text/javascript">
  <!--
  function openWin( addURL ) {

    var url = '<isa:webappsURL name="b2b/productdetail.do"/>' + addURL;

    var sat = window.open(url, 'artikel_to_order', 'width=350,height=370,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
    sat.focus();
  }


  function replaceItem(product, unit, quantity) {
    // get references to the documents
    var mainDocument = parent.positions.document;

    mainDocument.forms["order_positions"].replaceditem.value        = parent.detailsData.getDetailsItemTechKey();

    if (mainDocument.forms["order_positions"].orderchange) {
      mainDocument.forms["order_positions"].replacetype.value       = parent.detailsData.getDetailsItemReplaceType();
    }
    mainDocument.forms["order_positions"].replacewithproduct.value  = product;
    mainDocument.forms["order_positions"].replacewithunit.value     = unit;
    mainDocument.forms["order_positions"].replacewithqunatity.value = quantity;

    mainDocument.forms["order_positions"].submit();
    return true;
  }
  //-->
</script>

 
<% UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
	BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
	if(bom.getShop().isPricingCondsAvailable() == true) {%>
	  <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
			type = "text/javascript">
	  </script>
 <% } %>
<%
boolean productFound = false;
%>

