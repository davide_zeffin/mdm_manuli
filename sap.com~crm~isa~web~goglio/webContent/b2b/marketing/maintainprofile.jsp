<%--
    Display an marketing profile as an questionary.

    The questionary consists of questions and every question consists of answers.
    The iterate tag is used to loop over questions and answers.

    To every question you found an info message, which gives a short instruction
    how to anwser the questions. The info text will be created dynamic in the
    ShowProfileAction and used only resources with key messages.info.* .
    If there some errors occurs while the user maintains the profile the
    errors will display with the message tag. (Here only resources with key
    message.usererror.* will be used).
--%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.marketing.*" %>
<%@ page import="com.sap.isa.isacore.action.marketing.*" %>
<%-- @ page import="com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager,com.sap.isa.pers.obj.PersSessionData" --%>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.*" %>

<% int attributeCount = 0;
   String[] evenOdd = new String[] { "even", "odd" };
   BaseUI profileui = new BaseUI(pageContext); %>


<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%= profileui.getLanguage() %>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="marketing.jsp.ProfileTitle"/></title>
    <isa:stylesheets/>

    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
          type="text/javascript">
    </script>
    <script type="text/javascript">
    <!--
    function saveProfile() {
        getElement("questionary").SaveMktProfile.value = "true";
        getElement("questionary").submit();
    }
    //-->
  <% if (!profileui.isPortal()) { %>
  <!--
    function cancelProfile() {
        getElement("questionary").CancelMktProfile.value = "true";
        getElement("questionary").submit();
    }
    //-->
  <% } %>
  <!--
    function moreInputFields(hiddenFieldName) {
        getElement("questionary").elements[hiddenFieldName].value = "<isa:translate key="marketing.jsp.ChangeFields"/>";
        getElement("questionary").SaveMktProfile.value = "";
  //-->
  <% if (!profileui.isPortal()) { %>
  <!--
        getElement("questionary").CancelMktProfile.value = "";
  //-->
  <% } %>
  <!--
        getElement("questionary").submit();
    }
    //-->
    </script>
</head>
<%--
    HttpSession theSession = request.getSession();
    PersSessionData persData =(PersSessionData ) session.getAttribute("PERSSESSIONDATA");

    String url = null;
    if(persData != null)  {
          Object[] objs = (Object[])persData.get("incentive");
          if(objs != null && objs.length > 0 && objs[0] instanceof String)
                url = (String)objs[0];
          objs = null;
          persData.set("incentive" , objs);
    }

    if(url != null)  {
        url = WebUtil.getAppsURL(pageContext , Boolean.FALSE , url , null , null , true);
    %>

      <script language="Javascript1.2">
         var win_user_settings = window.open('<%=url%>', 'Coupon', 'width=550,height=450,scrollbars=yes,resizable=yes');
         win_user_settings.focus();
      </script>

    <%}
--%>

<body class="profile">
  <div class="module-name"><isa:moduleName name="b2b/marketing/maintainprofile.jsp" /></div>
  <h1><span><isa:translate key="marketing.jsp.ChangeProfile"/></span></h1>
  <div id="questions">

  <%
  String profilegrpTxt = WebUtil.translate(pageContext, "marketing.jsp.ChangeProfile", null);
  if (profileui.isAccessible) { %>
    <a href="#groupprofile-end"
       id="groupprofile-begin"
       title="="<isa:translate key="access.grp.begin" arg0="<%=profilegrpTxt%>"/>"
       onkeypress="if(event.keyCode=='115') {document.getElementById('groupprofile-end').focus();}"
    ><img src="<%=WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" border="0" width="1" height="1"></a>
  <%
  }%>
  <% if (!profileui.isPortal()) { %>
      <form id="questionary" method="post" action="<isa:webappsURL name="b2b/maintainprofile.do"/>" >
  <% } else { %>
      <form id="questionary" method="post" action="<isa:webappsURL name="b2b/changemarketingprofile.do"/>" >
  <% } %>
    <jsp:useBean id="questionary"
                 class="com.sap.isa.businessobject.marketing.MktQuestionary"
                 scope="request" />
    <div>
        <input type="hidden" name="AttributeSetKey" value="<%= JspUtil.encodeHtml(questionary.getAttributeSetKey().toString()) %> " />
        <input type="hidden" name="ProfileKey" value="<%= JspUtil.encodeHtml(questionary.getTechKey().toString()) %> " />
        <input type="hidden" name="SaveMktProfile" value="" />
    <% if (!profileui.isPortal()) { %>
        <input type="hidden" name="CancelMktProfile" value="" />
    <% } %>
    </div>
    <%-- show errors concern the questionary --%>
    <div class="top-message">
      <isa:message id="errortext" name="questionary" type="<%=Message.ERROR%>">
          <div class="error">
          <% if (profileui.isAccessible) { %>
              <span title="<isa:translate key="access.messages.error" arg0="<%=errortext%>"/>"><%=errortext%></span>
          <% }
             else { %>
              <span><%=errortext%></span>
          <% } %>
          </div>
      </isa:message>
      <div class="info">
         <span><span style="color:red">(*)</span> <isa:translate key="marketing.jsp.required"/></span>
      </div>
      <% if (questionary.isSaved()) {%>
          <div class="info">
              <span><b><isa:translate key="marketing.jsp.saveConfirm"/></b></span>
          </div>
      <% } %>
    </div>
    <table class="list" border="1" cellspacing="0" cellpadding="3">

      <% int line = 0; %>
      <isa:iterate id="question" name="<%= ShowProfileAction.RK_MKTATTRIBUTE_LIST %>"
                   type="com.sap.isa.businessobject.marketing.MktQuestion">

          <tr class="<%= evenOdd[++line % 2]%>">
            <td>
                <% if (question.getAttribute().isRequired()) {%>
                  <span style="color:red">*</span>
                <% } %>
                <%= JspUtil.encodeHtml(question.getAttribute().getDescription()) %>
              <%
              /* Set the current Attribute to pageContext for the iterator tag */
              request.setAttribute("mktanswerlist", question);
              /* Index to Attribute */
              attributeCount++;
              %>
              <i>
                <%-- show infos about the unit or currency of an attribute --%>
                <isa:message id="infotext" name="mktanswerlist"
                    type="<%=Message.INFO%>" property="Unit">
                  <span><%=infotext%></span> <br/>
                </isa:message>
              </i>
            </td>
            <td>

            <%-- show errors concern the question --%>
              <isa:message id="errortext" name="mktanswerlist"
                  type="<%=Message.ERROR%>" property="">
                  <div class="error">
                  <% if (profileui.isAccessible) { %>
                      <span title="<isa:translate key="access.messages.info" arg0="<%=errortext%>"/>"><%=errortext%></span> <br/>
                  <% }
                     else { %>
                      <span><%=errortext%></span> <br/>
                  <% } %>
                  </div>
              </isa:message>

            <isa:message id="infotext" name="mktanswerlist"
                type="<%=Message.INFO%>" property="">
              <%
              if (profileui.isAccessible) { %>
                <span title="<isa:translate key="access.messages.info" arg0="<%=infotext%>"/>"><%=infotext%></span> <br/>
              <%
              }
              else { %>
                <span><%=infotext%></span> <br/>
              <%
              } %>
            </isa:message>

            <%--  save the id and the datatype of the attribute in hidden fields  --%>
            <input type="hidden" name="Attribute[<%= attributeCount %>]" value="<%= question.getAttributeKey().toString() %>" />
            <input type="hidden" name="ChosenValues[<%= attributeCount %>]" value="<%= question.getTechKey().toString() %>" />
            <input type="hidden" name="DataType[<%= attributeCount %>]"  value="<%= question.getAttribute().getDataType() %>" />

                        <% int j = 0; %>
            <isa:iterate id="answer" name="mktanswerlist"
                         type="com.sap.isa.businessobject.marketing.MktAnswer">
           <% j++; %>
            <%
              switch(question.getRenderType()){
                case MktQuestion.PREDEFINED_VALUES:
                  /*  *** CASE 1:  CHARACTERISTICS WITH LIST OF PREDEFINED VALUES *** */
                  String selectionType;
                  String checked = "";

                  if (question.getAttribute().isMultiple()) {
                    selectionType = "checkbox";
                  }
                  else {
                    selectionType = "radio";
                  }

                  if ( answer.isChosen()) {
                    checked = "checked";
                  }

                  %>

                  <input type="<%= selectionType %>"
                         name="<%= question.getAttributeKey().toString() %>"
                         value="<%= answer.getTechKey().toString() %>"
                         id="<%= question.getAttributeKey().toString() %>_<%= j %>"
                         <% if (checked.length() > 0) { %>
                             checked="<%= checked %>"
                         <% } %>
                         />
                         <label for="<%= question.getAttributeKey().toString() %>_<%= j %>"> <%= JspUtil.encodeHtml(answer.getDescription()) %> </label> <br/>
                  <%
                  break;
                case MktQuestion.NO_PREDEFINED_VALUES_SINGLE:
                  /* *** CASE 2:  CHARACTERISTICS WITHOUT LIST OF PREDEFINED VALUES, SINGLE-VALUED *** */
                  %>

                  <input type="text" class="textInput"
                         name="<%= question.getAttributeKey().toString() %>"
                         value="<%= JspUtil.encodeHtml(answer.getTechKey().toString()) %>"
                         size="<%= question.getVisibleLength() %>"
                         maxlength="<%= question.getLength() %>" />
                  <br/>

                  <%
                  break;
                case MktQuestion.NO_PREDEFINED_VALUES_MULTIPLE:
                  /*** CASE 2b:  CHARACTERISTICS WITHOUT LIST OF PREDEFINED VALUES, MULTIPLE-VALUED ***/

                %>
                  <input type="text" class="textInput"
                         name="<%= question.getAttributeKey().toString() %>"
                         value="<%= JspUtil.encodeHtml(answer.getTechKey().toString()) %>"
                         size="<%= question.getVisibleLength() %>"
                         maxlength="<%= question.getLength() %>" />
                  <br/>


                <%
                  break;
              }
              request.setAttribute("answer", answer);
              %>

              <%-- show errors concern the answer --%>
              <% if (answer.getTechKey().toString().length() > 0) { %>
                  <isa:message id="errortext" name="mktanswerlist" type="<%=Message.ERROR%>"
                               property="<%= answer.getTechKey().toString() %>" >
                  <div class="error">
                    <%
                    if (profileui.isAccessible) {%>
                      <span title="<isa:translate key="access.messages.error" arg0="<%=errortext%>"/>"><%=errortext%></span> <br/>
                    <%
                    }
                    else { %>
                      <span><%=errortext%></span><br/>
                    <%
                    }%>
                    </div>
                  </isa:message>
               <%
               }%>

            </isa:iterate>

            <% if (question.getRenderType() == MktQuestion.NO_PREDEFINED_VALUES_MULTIPLE) {
            %>
              <%-- add a button to add more lines for free entries --%>
              <label for="CountofFields<%= question.getAttributeKey().toString() %>"><isa:translate key="marketing.jsp.MoreEntries"/></label>
              <select name="CountofFields<%= question.getAttributeKey().toString() %>" id="CountofFields<%= question.getAttributeKey().toString() %>">
                <option selected="selected" value="1"> 1</option>
                <option value="2"> 2</option>
                <option value="3"> 3</option>
                <option value="4"> 4</option>
              </select>

              <%
              String moreBtnTxt = WebUtil.translate(pageContext, "marketing.jsp.ChangeFields", null);
              String hiddenFieldName = "More" + question.getAttributeKey().toString(); %>
              <div>
                  <input type="hidden" name="<%=hiddenFieldName%>" value="" />
              </div>
              <a href="#" class="button" onclick="moreInputFields('<%=hiddenFieldName%>');" title="<isa:translate key="access.button" arg0="<%=moreBtnTxt%>" arg1=""/>" ><isa:translate key="marketing.jsp.ChangeFields"/></a>
            <%
              }
            %>
            </td>
          </tr>
      </isa:iterate>
    </table>
        <%
        String saveBtnTxt = WebUtil.translate(pageContext, "marketing.jsp.SaveProfile", null);
        String cancelBtnTxt = WebUtil.translate(pageContext, "marketing.jsp.Cancel", null);
        %>
  </form>
</div>
<div id="buttons">
    <ul class="buttons-1">
        <li><a href="#" onclick="saveProfile();" title="<isa:translate key="access.button" arg0="<%=saveBtnTxt%>" arg1=""/>" ><isa:translate key="marketing.jsp.SaveProfile"/></a></li>
    <% if (!profileui.isPortal()) { %>
        <li><a href="#" onclick="cancelProfile();" title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>" ><isa:translate key="marketing.jsp.Cancel"/></a></li>
    <% } %>
    </ul>
</div>

  <%
  if (profileui.isAccessible) { %>
    <a href="#groupprofile-begin"
       id="groupprofile-end"
       title="="<isa:translate key="access.grp.end" arg0="<%=profilegrpTxt%>"/>"
    ><img src="<%=WebUtil.getMimeURL(pageContext, "b2c/mimes/images/spacer.gif") %>" border="0" width="1" height="1"
          id="groupprofileend"
          alt="="<isa:translate key="access.grp.end" arg0="<%=profilegrpTxt%>"/>"></a>
  <%
  }%>

</body>
</html>