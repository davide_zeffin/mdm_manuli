<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib prefix="isacore" uri="/isacore" %>

<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title><isa:translate key="bestseller.jsp.title"/></title>
  <isa:stylesheets/>
    <% UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
     BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
     if(bom.getShop().isPricingCondsAvailable() == true) {%>
	   <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
			 type = "text/javascript">
	   </script>
  <% } %>
</head>
<body class="bestseller">
  <div class="module-name"><isa:moduleName name="b2b/marketing/bestseller.jsp" /></div>

  <% String pageName = "bestseller.inc.jsp"; %>
  <jsp:include page="<%=pageName%>" flush="true" />

</body>
</html>