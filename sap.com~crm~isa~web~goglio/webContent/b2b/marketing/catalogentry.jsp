<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.isacore.action.marketing.*" %>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<% String pageName = "";
   ProductList productList;
   String numColumns = "4";
%>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>SAPMarkets Internet Sales B2B - Catalog</title>
    <isa:stylesheets/>
   <% UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
     BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
     if(bom.getShop().isPricingCondsAvailable() == true) {%>
	   <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
			 type = "text/javascript">
	   </script>
  <% } %>
</head>

  <body class="catalogEntry">
    <div class="catalogEntry">
      <div class="module-name"><isa:moduleName name="b2b/marketing/catalogentry.jsp" /></div>
      <h1><isa:translate key="catalogentry.jsp.header"/></h1>
      <p class="product-recommendations" ><isa:translate key="catalogentry.jsp.info"/></p>

      <%-- the productlists are only avaiable when the corresponding marketing functions are available --%>
      <%
      productList = (ProductList) request.getAttribute(ShowRecommendationAction.RC_RECOMMENDATION);
      if (productList != null) { %>
        <% pageName = "recommendation.inc.jsp"; %>
        <jsp:include page="<%=pageName%>" flush="true" />
        <%
        /* Check, if the productlist contains products */
        if (productList.size() > 4) {
        %>
        <p class="product-recommendations">
          <a href="<isa:webappsURL name="/b2b/recommendation.do"/>">
            <isa:translate key="catalogentry.jsp.moreRecommend"/>
          </a>
        </p>
        <%
        }
      }

      productList = (ProductList) request.getAttribute(ShowBestsellerAction.RC_BESTSELLER);
      if (productList != null) { %>
        <% pageName = "bestseller.inc.jsp"; %>
        <jsp:include page="<%=pageName%>" flush="true" />
        <%
        /* Check, if the productlist contains products */
        if (productList.size() > 4) { %>
          <p class="product-recommendations">
            <a href="<isa:webappsURL name="/b2b/bestseller.do"/>">
              <isa:translate key="catalogentry.jsp.moreBestseller"/>
            </a>
          </p>
        <%
        }
      }%>
    </div>
  </body>
</html>