<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=JspUtil.getLanguage(pageContext)%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="marketing.jsp.ProfileTitle"/></title>
    <isa:stylesheets/>
    <body class="profile">
        <div class="module-name"><isa:moduleName name="b2b/marketing/saveprofile.jsp" /></div>
        <div id="questions">
            <span><b><isa:translate key="marketing.jsp.saveConfirm"/></b></span>
        </div>
    </body>
</html>