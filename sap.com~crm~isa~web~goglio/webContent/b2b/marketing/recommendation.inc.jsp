<%-- Include to display personal recommendations -- %>
<%-- The Javascript library "b2b/jscript/ipc_pricinganalysis.jsp" must be loaded --%>

<%-- use this include only as a dynamic include --%>

<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.ProductListUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.marketing.RecommendationUI" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>


<% ProductListUI ui = new RecommendationUI(pageContext); %>
<div class="recommendation">
  <div class="module-name"><isa:moduleName name="b2b/marketing/recommendation.inc.jsp" /></div>

  <%@ include file="/b2b/marketing/productlist.inc.jsp" %>
</div>

