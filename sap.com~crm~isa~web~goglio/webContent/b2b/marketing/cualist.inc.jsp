
<%-- !!! The following variables must be defined: !!!! --%>
<%-- CuaUI ui --%>
<%-- boolean productFound --%>

<%-- !! use this include only as a static include !! --%>


<%@ page import="com.sap.isa.catalog.webcatalog.WebCatItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>
<%@ page import="com.sap.spc.remote.client.object.IPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ taglib uri="/isacore" prefix="isacore" %>


<%
if (ui.isAccessible()) { %>
  <%@page import="com.sap.isa.catalog.actions.ActionConstants"%>
<a href="#groupcualist-end"
     id="groupcualist-begin"
     title="<isa:translate key="access.grp.begin" arg0="<%=ui.getTitle()%>"/>"></a>
<%
}%>

  <div class="product-recommendations">
  <table border="0" cellpadding="4" cellspacing="0">
    <tr>
<%
/* Check, if the productlist contains products */
productFound = false;
if (ui.getProductList().size() > 0) {
  %>

  <% int line = 0; %>
  <isa:iterate id="product" name="<%= ProductListUI.PC_PRODUCT_LIST %>"
  type="com.sap.isa.businessobject.marketing.CUAProduct">
  <%
  if (ShowCUAAction.isProductVisible(product.getCuaType(),ui.getDisplayType())) {
    productFound = true;
    line++;
    if (line  % 3 == 0) { %>
      <!-- start a new line -->
      </tr>
      <tr>
      <%}%>

      <td>
      <% if (product.getThumb().length() > 0) { %>
        <img alt="<%=product.getThumb() %>" src="<%=product.getThumb() %>" />  </td>
      <%} else { %>
        <img alt="Thumbnail" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/small.gif" ) %>" />
      <%}%>
        </td>
        <td>
      <a href="#" onclick="openWin('<%= JspUtil.encodeHtml(ShowProductDetailAction.createDetailRequest(product,ActionConstants.DS_IN_WINDOW)) %>') " >
      <%=product.getId() %>
      </a> <br/>
      <a href="#" onclick="openWin('<%= JspUtil.encodeHtml(ShowProductDetailAction.createDetailRequest(product,ActionConstants.DS_IN_WINDOW)) %>') " >
      <%=product.getDescription() %>
      </a><br/>

      <%
      if (ui.isListPriceAvailable()) { %>
        <br/>
        <isa:translate key="catalog.isa.price"/>&nbsp;<%=product.getListPrice() %>
      <%
      }
      if (ui.isCustomerSpecificPriceAvailable()) { %>
        <br/>
        <isa:translate key="catalog.isa.IPCPrice"/>&nbsp;<%=product.getCustomerSpecificPrice() %>

        <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
          <%
          IPCItemReference genIpcItemRef = product.getIPCItemReference();
		  RFCIPCItemReference ipcItemRef = (RFCIPCItemReference)genIpcItemRef;
          if (ipcItemRef != null) { %>
            &nbsp;
            <a href="#" onclick="displayIpcPricingConds('<%= JspUtil.removeNull(ipcItemRef.getConnectionKey()) %>',
                                                        '<%= JspUtil.removeNull(ipcItemRef.getDocumentId()) %>',
                                                        '<%= JspUtil.removeNull(ipcItemRef.getItemId()) %>'
                                                        )"><isa:translate key="b2b.order.display.ipcconds" /></a>
          <%
          } %>
        </isacore:ifShopProperty>

        <br/>
      <%
      }%>

      <br/>

      <% String localDetailScenario = "&" + ActionConstants.RA_DETAILSCENARIO + "=" + ui.getDetailScenario();
      	if (ui.getMethod().equals(CuaUI.METHOD_JAVASCRIPT)) { %>
        <%-- will be used in the order details --%>
          <a href="#" onclick="addToDocument('<%=product.getId()%>', '<%=product.getUnit()%>','1');" <%=ui.getTargetAttribute() %>>
            <isa:translate key="product.jsp.addToBasket"/>
          </a>
      <% }
         else if (ui.getMethod().equals(CuaUI.METHOD_SIMPLE_REPLACE)) { %>
      <%-- will be used in the order details --%>
          <a href="#" onclick="replaceItem('<%=product.getId()%>', '<%=product.getUnit()%>','1');" <%=ui.getTargetAttribute() %>>
            <isa:translate key="product.jsp.replace"/>
          </a>
      <% }
         else if (ui.getMethod().equals(CuaUI.METHOD_ACTION_REPLACE)) {
         /* therefore the request parameter "cuareplaceditem"  must be set */
          String replacedItem = request.getParameter("cuareplaceditem");
          if (replacedItem == null) {
            replacedItem = "";
          }
          %>
          <a href="<isa:webappsURL name="b2b/addproducttodocument.do"/><%= GetTransferItemFromProductAction.createReplaceRequest(product,replacedItem,1,"addtodocument") + localDetailScenario %>" <%=ui.getTargetAttribute() %>>
          <isa:translate key="product.jsp.replace"/></a>
      <% }
        else {%>
        <!-- IZ: <%= ui.getDetailScenario() %> -->
          <a href="<isa:webappsURL name="b2b/addproducttodocument.do"/><%= GetTransferItemFromProductAction.createAddRequest(product,1,"addtodocument") + localDetailScenario %>" <%=ui.getTargetAttribute() %>>
          <isa:translate key="product.jsp.addToBasket"/></a>
      <% } %>
        </td>
    <%} /* if type */ %>
    </isa:iterate>
  <%
  } /*if size */
if (!productFound) {
%>
    <td><br/><br/><isa:translate key="cua.jsp.notFound"/></td>
<%}%>
    </tr>
</table>
</div>  
<%
if (ui.isAccessible()) { %>
  <a href="#groupcualist-begin"
     id="groupcualist-end"
     title="<isa:translate key="access.grp.end" arg0="<%=ui.getTitle()%>"/>"></a>
<%
}%>

