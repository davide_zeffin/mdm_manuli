<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); %>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    	<title><isa:translate key="b2b.fs.title"/></title>
  	</head>

	<frameset rows="100%" id="isaTopFS" border="0" frameborder="0" framespacing="0">
    	<frame name="isaTop" src="<isa:webappsURL name="/b2b/fs_start_inner.jsp"/>" frameborder="0" scrolling="no" marginwidth="0" marginheight="0"/>

    	<noframes>
        	<p>&nbsp;</p>
    	</noframes>
	</frameset>
</html>