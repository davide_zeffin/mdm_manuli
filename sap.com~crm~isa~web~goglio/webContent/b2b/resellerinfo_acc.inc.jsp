<!-- get Reseller info and display it on ordersimulate.jsp -->
             <%
               BusinessPartnerManager bupaManager = ui.getBom().createBUPAManager();
               BusinessPartner reseller = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.RESELLER);
             %>

             <tr>
                <td class="identifier"><isa:translate key="b2b.order.reseller.header"/>&nbsp;</td>
                <td class="value"><%= JspUtil.encodeHtml(reseller.getAddress().getName1()) %></td>
              </tr>
              <tr>
                <td class="identifier"><isa:translate key="b2b.order.reseller.address"/>&nbsp;</td>
                <td class="value"><%= JspUtil.encodeHtml(reseller.getAddress().getStreet()) %>&nbsp;<%= JspUtil.encodeHtml(reseller.getAddress().getHouseNo()) %></td>
              </tr>
              <tr>
                <td class="identifier">&nbsp;</td>
                <td class="value"><%= JspUtil.encodeHtml(reseller.getAddress().getPostlCod1()) %>&nbsp;<%= JspUtil.encodeHtml(reseller.getAddress().getCity()) %></td>
              </tr>
              <tr>
                <td class="identifier"><isa:translate key="b2b.order.display.reseller.phone"/>&nbsp;</td>
                <td class="value"><%= JspUtil.encodeHtml(reseller.getAddress().getTel1Numbr()) %></td>
              </tr>
              <tr>
                <td class="identifier"><isa:translate key="b2b.order.display.reseller.mail"/>&nbsp;</td>
                <td class="value"><%= JspUtil.encodeHtml(reseller.getAddress().getEMail()) %></td>
              </tr>
                