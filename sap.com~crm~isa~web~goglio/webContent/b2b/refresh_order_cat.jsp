<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>SAP Internet Sales B2B</title>
	
	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>	
	
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
            type="text/javascript">
    </script>	

  	<script type="text/javascript">
	  <!--
		function show_document_view() {
           isDocumentView = true;
           isaTop().parent.location.href="<isa:webappsURL name="/b2b/catalogend.do"/>";
        }
	  //-->
       show_document_view();
  	</script>
  </head>
  <body></body>
</html>
