<%-- import the taglibs used on this page --%>

<%--
     HTML/Design changes by: michael.oertel@pixelpark.com
--%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.*" %>

<% 
        HeaderUI ui = new HeaderUI(pageContext);
        

       /* get user session data object */
       UserSessionData userSessionData =
          UserSessionData.getUserSessionData(request.getSession());

       DocumentHandler documentHandler = (DocumentHandler)
          userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"-->
<html>
  <head>
    <title>SAP Internet Sales B2B - Header</title>
    <isa:includes/>
    <!-- 
    <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>"
          type="text/css" rel="stylesheet">
          -->
        <script type="text/javascript">
        <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
        </script>                                 
                  
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
              type="text/javascript">
      </script>           

    <script type="text/javascript">

        // function busy(frameObject) 
        <%@ include file="/b2b/jscript/busy.js" %>


        //function help() {
        //    win_help = window.open('../ecall/jsp/customer/common/frameset_help.html?SUPPORTTYPE=\"PRODUCT CATALOG\"', "", 'width=550,height=450,scrollbars=yes,resizable=yes');
        //    win_help.focus();
        //}
	
        function help() {
            win_help = window.open('<isa:webappsURL name="/ecall/jsp/customer/common/frameset_help.jsp"/>', '', 'width=700,height=550,scrollbars=yes,resizable=yes');
            win_help.focus();
        }

        function contact() {
            win_contact = window.open('http://www.sap.de/aboutus/aboutus.asp', '', 'width=800,height=550,scrollbars=yes,resizable=yes');
            win_contact.focus();
        }
        
                function lwc() { /* Live Web Collaboration */
                win_lwc = window.open('<isa:webappsURL name="/ecall/jsp/customer/common/frameset_help_support.jsp"/>', 'lwc', 'width=700,height=550,scrollbars=yes,resizable=yes');
                win_lwc.focus();
                }


                // switch to catalog view
                function show_catalog_view() {
                        highlight_catalog_link();
                        busy(form_input());
                        parent.organizer_nav.location.href="<isa:webappsURL name="/b2b/organizer-nav-catalog.jsp"/>";
                        parent.organizer_content.location.href="<isa:webappsURL name="/catalog/showLastVisited.do"/>";
                }
                
                // switch to document view
                function show_document_view() {
                        highlight_document_link();
                        <% if (documentHandler.getOrganizerParameter("docsearchversion").equals("NEW")) { %>
                         var screenName = "<%= documentHandler.getOrganizerParameter("genericsearch.name") %>"
                         var vHref = "<isa:webappsURL name="/appbase/genericsearch.jsp?genericsearch.name="/>" + screenName;
                        <% } else { %>
                         var vHref = "<isa:webappsURL name="/b2b/documentlistselect.do"/>";
                        <% } %>
                        parent.organizer_content.location.href=vHref;
                        busy(form_input());
                        parent.organizer_nav.location.href="<isa:webappsURL name="/b2b/organizer-nav-doc-search.jsp"/>";        
                        form_input().location.href="<isa:webappsURL name="/b2b/catalogend.do"/>";
                }

                function show_basket() {
                     <% if (documentHandler.getOrganizerParameter("docsearchversion").equals("NEW")) { %>
                      var screenName = "<%= documentHandler.getOrganizerParameter("genericsearch.name") %>"
                      var vHref = "<isa:webappsURL name="/appbase/genericsearch.jsp?genericsearch.name="/>" + screenName;
                     <% } else { %>
                      var vHref = "<isa:webappsURL name="/b2b/documentlistselect.do"/>";
                     <% } %>
                    highlight_document_link();
                    parent.organizer_content.location.href=vHref;
                    busy(form_input());
                    parent.organizer_nav.location.href="<isa:webappsURL name="/b2b/organizer-nav-doc-search.jsp"/>";    
                        form_input().location.href="<isa:webappsURL name="/b2b/editabledocumentontop.do"/>";
                }
                
                function highlight_catalog_link() {
                    // changes link representations for catalog/document-view when catalog link is clicked 
                        if (document.imgcatview) {
                                document.links[3].className="viewSelectionActive";
                        document.imgcatview.src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_catalog_active.gif") %>";
                    }
                    if (document.imgdocview) {
                        document.links[1].className="viewSelectionInactive";
                        document.imgdocview.src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_transactions_inactive.gif") %>";
                    }
                }
                
                function highlight_document_link() {
                    // changes link representations for catalog/document-view when document link is clicked 
                        if (document.imgdocview) {
                                document.links[1].className="viewSelectionActive";
                        document.imgdocview.src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_transactions_active.gif") %>";
                    }
                    if (document.imgcatview) {
                        document.links[3].className="viewSelectionInactive";
                        document.imgcatview.src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_catalog_inactive.gif") %>";   
                    }
                }
        
        function openWin( addURL ) {
             var url = addURL;
             var sat = window.open(url, 'OCICatalog', 'width=700,height=500,scrollbars=yes,resizable=yes');
             sat.focus();
        }
        
        function show_collective_order() {
    	     busy(form_input());
    	     form_input().location.href="<isa:webappsURL name="/b2b/createcollectiveorder.do"/>";
	}
	
    </script>
  </head>

  <% User user = (User) request.getAttribute(BusinessObjectBase.CONTEXT_NAME); %>

  <body class="header">
    <div id="meta-navigation" class="module">
      <div class="module-name"><isa:moduleName name="b2b/headerportal.jsp" /></div>
          <!-- outer table for blue link line width 100%  -->
                
          <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" width="100%">
            <tbody>
                  <tr>
                    <%
                    if (!ui.homActivated) {%>
                      <td class="vertical-align-bottom" align="left" rowspan="2"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headline_header_large.gif")%>" width="5" height="20" alt="" border="0"></td><%}
                else {%>   
                  <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="1" alt="" border="0"></td>
                <% } %>
                <td class="vertical-align-bottom"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="3" alt="" border="0"></td>
                <td><img style="display: none;" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/busy.gif") %>"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="3" alt="" border="0"></td>
                  </tr>
                  <tr>
                    <td colspan="2">      
                  <!-- inner table with links for switching document/portal-view, align left-->
                      <table border="0" cellspacing="0" cellpadding="0" align="left" bgcolor="#FFFFFF">
                        <tbody>
                          <tr>
                        <%
                        if (ui.homActivated) { %>
                          <!-- hide "document" / "catalog" link in POM-scenario -->
                          <td class="vertical-align-middle" colpspan="4">
                            <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="1" alt="" border="0">
                          </td>
                                
                        <%
                        }
                        else { %>
                          <!-- show "documents"/"catalog" link -->
                          <td class="vertical-align-middle">
                            <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="16" alt="" border="0">
                            <a class="icon" href="#" onclick="show_document_view()">
                               <img name="imgdocview" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_transactions_active.gif") %>" alt="" border="0" height="14" width="17">
                            </a>
                          </td>
                          <td class="vertical-align-middle">
                            <a class="viewSelectionActive" name="linkdocview" href="#" onclick="show_document_view()"><isa:translate key="b2b.header.mytransaction"/></a>&nbsp;&nbsp;&nbsp;
                          </td>
                          <% if (ui.internalCatalogVisible) { %>        
                          <td class="vertical-align-middle">
                            <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="5" height="16" alt="" border="0">
                            <a class="icon" href="#" onclick="show_catalog_view()">
                              <img name="imgcatview" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_catalog_inactive.gif") %>" alt="" border="0" height="14" width="17">
                            </a>
                          </td>
                          <td class="vertical-align-middle">
                            <a class="viewSelectionInactive" name="linkcatview" href="#" onclick="show_catalog_view();"><isa:translate key="<%=ui.catalogLink %>"/></a>&nbsp;&nbsp;&nbsp;
                          </td>
                          <% } %>
                          <% if ((ui.ociEnable) &&(!ui.internalCatalogVisible)) { %>    
                          <td class="vertical-align-middle">
                             <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="5" height="16" alt="" border="0">
                             <a class="icon" href="#" onclick="openWin('<%=ui.externalCatalogURL %>'); return false;">
                               <img name="imgcatview" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_catalog_external.gif") %>" alt="" border="0" height="14" width="17">
                              </a>
                           </td>
                           <td class="vertical-align-middle">
                              <a class="viewSelectionInactive" name="linkcatview" href="#" onclick="openWin('<%=ui.externalCatalogURL %>'); return false;"><isa:translate key="b2b.header.extern.catalog"/></a>&nbsp;&nbsp;&nbsp;
                           </td> 
                          <% } %>
                        <%
                        } %>
                              
                      </tr>
                    </tbody>
                  </table>
                        
                  <!-- inner table for cells with links, align right-->
                  <table border="0" cellspacing="0" cellpadding="0" align="right" bgcolor="#FFFFFF">
                        <tbody>  
                      <tr> 
				  	  <%
        		  	  if (ui.isMiniBasketAvailable()) {%>                                
	                      <% if (!ui.homActivated) { %>          
	                      <!-- standard B2B scenario - minibasket as button -->
	                          <td class="vertical-align-middle" align="right">
	                            <a class="icon" href="#" onclick="show_basket()"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_wk_blue_header.gif") %>" alt="" border="0" height="16" width="20"></a>  
	                          </td>
	                          <td class="vertical-align-middle">
	                            <form name="basketForm" action="javascript:show_basket();">
	                              <input class="green" name="miniBasket" type="submit" value="<isa:translate key="b2b.header.minibasket.default"/>"></input>&nbsp;&nbsp;&nbsp;
	                            </form>
	                          </td>
	                      <% } else { %>
	                      <!-- HOM scenario - minibasket displays collective order -->
	                          <td class="vertical-align-middle" align="right">
	                            <a class="icon" href="#" onclick="show_collective_order();"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_wk_blue_header.gif") %>" alt="" border="0" height="16" width="20"></a>  
	                          </td>
	                          <td class="vertical-align-middle">
	                            <form name="basketForm" action="javascript:show_collective_order();">
	                              <input class="green" name="miniBasket" type="submit" value="<isa:translate key="b2b.header.collorder.default"/>"></input>&nbsp;&nbsp;&nbsp;
	                            </form>
	                          </td>
	                      <% } %>
                      <% } %>
                        <td class="vertical-align-middle">
                          &nbsp;&nbsp;
                          <a class="icon" href="#" onclick="lwc();">
                            <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_contact.gif") %>" alt="" border="0" height="14" width="17">
                          </a>
                        </td>
                        <td class="vertical-align-middle">
                          <a href="#" onclick="lwc();"><isa:translate key="b2b.header.lwc"/></a>&nbsp;&nbsp;&nbsp;
                        </td>
                             
                        <td class="vertical-align-middle">
                          &nbsp;&nbsp;
                          <a class="icon" href="#" onclick="help();">
                            <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_help.gif") %>" alt="" border="0" height="14" width="17">
                          </a>
                        </td>
                        <td class="vertical-align-middle">
                          <a href="#" onclick="help();"><isa:translate key="b2b.header.help"/></a>&nbsp;&nbsp;&nbsp;
                        </td>
                      </tr>
                      
                      <!-- table row for spacer line -->
                          <tr>
                            <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="3" alt="" border="0"></td>
                          </tr>  
                      
                    </tbody>
                  </table>
                        
                    </td>
              </tr>
           </tbody>
         </table>
         
         <table class="LineNavTabDoc" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>