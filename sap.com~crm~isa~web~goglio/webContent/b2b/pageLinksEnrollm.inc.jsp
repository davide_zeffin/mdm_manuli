<% if (ui.isPageLinkDisplayed()) {
        int[] pageLinks = ui.getPaginationLinks(); %>
    
    <div class="pagelinks">    
    <% if (ui.showPrevLink()) { %>
           <span>&nbsp&nbsp&nbsp</span>
              <a href="javascript:setPage(<%= ui.getCurrPage()-1 %>)" title="<%= ui.getCurrPage()-1 %>: <isa:translate key="catalog.isa.previouspage"/> (<isa:translate key="catalog.isa.page"/> <%= ui.getCurrPage()-1 %>)" tabindex= 0> <isa:translate key="catalog.isa.previous"/> </a>
           <span>&nbsp&nbsp&nbsp</span>
     <% }
        for (int j=0; j < pageLinks.length; j++) { %>
              <span>&nbsp&nbsp&nbsp</span>
        <% if (pageLinks[j] == ui.getCurrPage() || ui.getCurrPage() == 0) { %>
              <span> <%= ui.getCurrPage() + ((ui.getCurrPage() != 0) ? 0 : 1) %>&nbsp&nbsp&nbsp</span>
        <% } 
           else { %>
              <a href="javascript:setPage(<%= pageLinks[j] %>)" title="<%= pageLinks[j] %>: <isa:translate key="catalog.isa.page"/> <%= pageLinks[j] %>" tabindex= 0> <%= pageLinks[j] %></a>
              <span>&nbsp&nbsp&nbsp</span>
        <% }
        }
        if (ui.showNextLink()) { %>
           <span>&nbsp&nbsp&nbsp</span>
             <a href="javascript:setPage(<%= ui.getCurrPage()+1 %>)" title="<%= ui.getCurrPage()+1 %>: <isa:translate key="catalog.isa.nextpage"/> (<isa:translate key="catalog.isa.page"/> <%= ui.getCurrPage()+1 %>)" tabindex= 0> <isa:translate key="catalog.isa.next"/> </a>
           <span>&nbsp&nbsp&nbsp</span>
     <% }%>
     </div>    
<% } %>