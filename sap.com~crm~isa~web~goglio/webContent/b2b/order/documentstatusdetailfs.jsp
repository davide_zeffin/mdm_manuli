<%--
********************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Ralf Witt
    Created:      08.5.2001

    $Revision: #2 $
    $Date: 2002/01/22 $
********************************************************************************
--%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head>
    <title><isa:translate key="b2b.fs.title"/></title>

  </head>
  <frameset rows="*,46" id="workareaFS" border="0" frameborder="0" framespacing="0">
    <frame name="positions"
           src='<isa:webappsURL name="b2b/orderstatusdetail.do"/>'
           frameborder="0">
    <frame name="closer_details"
           src='<isa:webappsURL name="b2b/empty.jsp"/>'
           frameborder="0">
    <noframes>
      <body>&nbsp;</body>
    </noframes>
  </frameset>
</html>