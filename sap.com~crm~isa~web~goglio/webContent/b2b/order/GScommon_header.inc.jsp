<%--
********************************************************************************
    File:         GSpredoc_header.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Created:      05.04.2005
    Version:      1.0
********************************************************************************
--%>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>
<% GenericSearchUIData ui = (GenericSearchUIData)request.getAttribute(GenericSearchUIData.RC_UICLASSINSTANCE); %>
<% if ("predoc".equals(request.getParameter(GenericSearchUIData.RC_UI_INCLUDESTOSHOW))) { %>
    <script>
      function createFromPredecessor(params) {
          var request = "<isa:webappsURL name="b2b/createfrompredecessor.do"/>";
          request = request + "?" + params;
          var myTop = form_input();
          if (typeof isaTop == 'function' && isaTop().header) {
             isaTop().header.busy(parent.form_input)
          }
          myTop.location.href = request;
          
      }
    </script>
    <% // Show content for processing of PREDECESSOR DOCUMENTS %>
    <% String title = WebUtil.translate(pageContext, "b2b.selectpredecessordoc.ot", null);
       if ("QUOTATION".equals(ui.getSearchRequestMemory().get("rc_documenttypes"))) {
           title = WebUtil.translate(pageContext, "b2b.selectpredecessordocument.qt", null);
       }
    %>
    <div class="document-search-workarea"> <%-- Will be closed in file GSbcommon_buttons.inc.jsp --%>
    <h1><%= title %></h1>
    <div id="document"> <%-- Will be closed in file GSbcommon_buttons.inc.jsp --%>
<% } %>