<%--
********************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      08.5.2001

    $Revision: #1 $
    $Date: 2001/05/08 $
********************************************************************************
--%>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ taglib uri="/isa" prefix="isa" %>

<% BaseUI ui = new BaseUI(pageContext);%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

  <head>
    <title>SAP Internet Sales B2B</title>
  </head>
  <frameset rows="*,23" border="0" frameborder="0" framespacing="0">

    <frame name="batchs" src='<isa:webappsURL name="b2b/showorderbatch.do"/>' frameborder="0">
    <frame name="batchs_details" src='<isa:webappsURL name="b2b/order/closer_batchorder_details.jsp"/>' frameborder="0" scrolling="no" marginwidth="0" marginheight="0">

    <noframes>
      <body>&nbsp;</body>
    </noframes>
  </frameset>
</html>