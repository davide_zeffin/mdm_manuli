<%--
********************************************************************************
    File:         GSbackorder_header.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Created:      18.03.2005
    Version:      1.0
********************************************************************************
--%>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ taglib uri="/isa" prefix="isa" %>
<% GenericSearchUIData ui = (GenericSearchUIData)request.getAttribute(GenericSearchUIData.RC_UICLASSINSTANCE); %>
 <h1><isa:translate key="gs.show.me.backorders"/></h1>
 <div id="document"> <%-- Will be closed in file GSbackorder_buttons.inc.jsp --%>