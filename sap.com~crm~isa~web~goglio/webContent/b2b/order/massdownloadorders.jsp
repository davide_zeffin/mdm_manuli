<%--
********************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      08.5.2001

    $Revision: #5 $
    $Date: 2002/06/10 $
********************************************************************************

 IMPORTANT:
       - Page only accessible by using 'b2b/b2b/documentlistselect.do'

--%>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.isacore.actionform.order.orderdownload.*" %>
<%@ page import="com.sap.isa.isacore.MassOrderDownloadBean" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>


<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>
<%@ taglib uri="/isa" prefix="isa" %>

	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>

  <script src="<isa:mimeURL name="b2b/jscript/frames.js"/>"
            type="text/javascript"></script>


<% BaseUI ui = new BaseUI(pageContext);%>

<%
  MassOrderDownloadBean mdb = (MassOrderDownloadBean) userSessionData.getAttribute("massdownloadbean");
  HashMap ordersMap = null;
  int docCount = -1;
  if(mdb!=null) {
    docCount=mdb.getSalesOrders().size();
    ordersMap=mdb.getSalesOrders();
  }

    boolean isBackendR3 = false;
    boolean isHOM = false;
%>

<isacore:ifShopProperty property = "backend" value ="<%= Shop.BACKEND_R3 %>">
    <% isBackendR3 = true; %>
</isacore:ifShopProperty>
<isacore:ifShopProperty property = "backend" value ="<%= Shop.BACKEND_R3_40 %>">
    <% isBackendR3 = true; %>
</isacore:ifShopProperty>
<isacore:ifShopProperty property = "backend" value ="<%= Shop.BACKEND_R3_45 %>">
    <% isBackendR3 = true; %>
</isacore:ifShopProperty>
<isacore:ifShopProperty property = "backend" value ="<%= Shop.BACKEND_R3_46 %>">
    <% isBackendR3 = true; %>
</isacore:ifShopProperty>
<isacore:ifShopProperty property = "backend" value ="<%= Shop.BACKEND_R3_PI %>">
    <% isBackendR3 = true; %>
</isacore:ifShopProperty>
<isacore:ifShopProperty property = "scenario" value ="<%= Shop.PARTNER_ORDER_MANAGEMENT %>">
    <% isHOM = true; %>
</isacore:ifShopProperty>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <title>Document List</title>
  <isa:includes/>
  <script type="text/javascript">

       function checkAll(){

           for (var i = 1; i <= <%= docCount %>; i++) {
              var sel = "select_[" + i + "]";
              document.forms["orderitemselect"].elements[sel].checked = document.forms["orderitemselect"].itemcheckbox0.checked;   
           }
        }

        function checkEvent(box) {
            if (box.checked == false) {
                document.forms["orderitemselect"].itemcheckbox0.checked=false;
            }
        }
        
        function download() {
             for (var i = 1; i <= <%= docCount %>; i++) {
              var sel = "select_[" + i + "]";
              var id = "orderId_[" + i + "]";

              if (document.forms["orderitemselect"].elements[sel].checked) {   
                  ordId = document.forms["orderitemselect"].elements[id].value;
                  
                  document.forms["orderdownload"].elements["orderGUIDS"].value =
                  document.forms["orderdownload"].elements["orderGUIDS"].value +
                  ";" +
                  document.forms["orderitemselect"].elements[sel].value;
              
                  document.forms["orderdownload"].elements["orderIDS"].value =
                  document.forms["orderdownload"].elements["orderIDS"].value +
                  ";" +
                  ordId ;
              }
            }
            
            document.forms["orderdownload"].submit();
        }
                
	    function remove(){
	        document.forms["orderitemselect"].operation.value='remove';
	    }
    </script>
  </head>
  <body class="workarea">
    <div id="organizer-search" class="module"></div>
    <div class="module-name"><isa:moduleName name="b2b/order/massdownloadorders.jsp" /></div>

    <!-- title of the mass download basket -->
    <b> <isa:translate key="ordr.downld.massdwnld.title"/> <br/>
    </b>
    <br/>
  <%-- Document List Area --%>
  
  <% if ( docCount > 0 ) { %>
  <form name="orderitemselect" method="post" action='<isa:webappsURL name ="/b2b/order/addOrderToMassDownloadList.do"/>'>
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tbody>
      <tr>
        <td>&nbsp;</td>
        <td>

		   <%
				  // Data table preamble
				  String tableTitle = WebUtil.translate(pageContext, "ordr.downld.massdwnld.title", null);
				  if (ui.isAccessible())
				  {

					int colno = 4;
					int rowno = docCount;
					String firstRow ="0";
					String columnNumber = Integer.toString(colno);
					String rowNumber = Integer.toString(rowno);
					firstRow = Integer.toString(1);
					String lastRow = Integer.toString(rowno);

		   %>
					<a href="#tableend"
						id="tablebegin"
						title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>"
						arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
					</a>
		   <%
				   }

				// Data table preamble ends
		   %>



        <table class="organizer-list" border="0" cellspacing="0" cellpadding="2" width="100%">

        <%-- Tableheader --%>
        <thead >
			<tr>
				<th align="center" scope="row">
					<isa:translate key="massdwld.del.btn"/>	<br/>
					<INPUT type="checkbox" id="checkbox0" onclick="checkAll();"
					    name="itemcheckbox0" title="<isa:translate key="b2b.orderdwld.checkall"/>" value="ON">&nbsp;
					    <label for="checkbox0"><isa:translate key="b2b.orderdwld.checkall"/></label>
				</th>
				<th align=left scope="row"><isa:translate key="status.sales.status"/></th>
				<th align=left scope="row"><isa:translate key="b2b.status.shuffler.key1val10"/>&nbsp;/<br>
					<isa:translate key="status.sales.date"/>
				</th>
				<th align=left scope="row"><isa:translate key="massdwld.descn"/></th>
			</tr>
        </thead>


          <%-- Tablerows --%>

           <% String documentStatusKey = "";
              String documentValidToKey = "";
              String documentModificationKey = "";
              String even_odd = "odd";
              int line = 0;
           %>
           <%--
	   <isa:iterate id="orderheader" name="<%= DocumentListAction.RK_ORDER_LIST %>"
                        type="com.sap.isa.businessobject.header.HeaderSalesDocument">
                  documentStatusKey = "status.sales.status.".concat(orderheader.getStatus());
	   --%>
    <%
	   Iterator iter = ordersMap.values().iterator();
	   while(iter.hasNext()) {
	      line++;
	      MassOrderDownloadBean.LeanSDObject orderheader = (MassOrderDownloadBean.LeanSDObject)iter.next();

	      documentStatusKey = "status.sales.status.".concat(orderheader.getStatus());
	      documentValidToKey = "status.sales.list.".concat("open").concat(".vT"); 
          String refDesSeparator = "/";
	  %>
           <tbody>
             <% if (even_odd.equals("odd")) {
                  even_odd = "even";
                } else {
                  even_odd = "odd";
                }
             %>
             <tr class="<%= even_odd %>">
	       <td align="center">
                    <input type="checkbox" name="select_[<%= line %>]" value="<%= JspUtil.removeNull(orderheader.getGUID()) %>" onclick="checkEvent(<%= line %>)">
	      </td>
	        <!-- status info -->
		<td><var>
			<% if( isHOM ) {
				HashMap map = (HashMap)userSessionData.getAttribute("userstatusmap");
			%>
			    <isa:translate key="<%= JspUtil.encodeHtml(documentStatusKey) %>"/>
			<% } else {%>
				<isa:translate key="<%= JspUtil.encodeHtml(documentStatusKey) %>"/>
			<% } %>
		</var></td>
	       <td><var>
                          <%= JspUtil.encodeHtml(orderheader.getSDNum()) %>
			&nbsp;<br>
                        <%= JspUtil.encodeHtml(orderheader.getDate())%>
                   </var></td>
	       <td><var><%= JspUtil.encodeHtml(orderheader.getDesc()) %>
                   </var></td>
            </tr>
          </tbody>
          
          <input type ="hidden" name="orderId_[<%= line %>]" value ="<%= orderheader.getSDNum()%>">
	<% } %>
	   <%--
	   </isa:iterate>
	   --%>

	  <%
		  // Data Table Postamble
		  if (ui.isAccessible())
		  {
	  %>
		  <a id="tableend"
			href="#tablebegin"
			title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>"</a>
	  <%
		  }
		  // Data Table Postamble ends
	  %>

          </table>

        </td>
        <td>&nbsp;</td>
      </tr>
    </tbody>
  </table>

   <br/>
    <table border="0" cellspacing="0" cellpadding="">
      <tbody>
	<!-- target set to _history only for showing the confirmation page afterwards.
	    Has no effect on the history page though -:)
	 -->
	  <tr>
	  <td colspan=1>
		&nbsp;&nbsp;&nbsp;
		<%
			if(ui.isAccessible()) {
				String delBtnTxt = WebUtil.translate(pageContext, "massdwld.del.btn", null);
		%>
				<input type="submit" name="delBtn" value="<isa:translate key="massdwld.del.btn"/>"
				title="<isa:translate key="access.button" arg0="<%=delBtnTxt%>" arg1=""/>"
				onClick="remove();" class="green">
		<%  } else { %>
				<input type="submit" name="delBtn" value="<isa:translate key="massdwld.del.btn"/>"
				onClick="remove();" class="green">
		<%  } %>

	  </td>
	    <input type="hidden" name="operation" value="">
	  </form>
	  <td colspan=3>&nbsp;
	  <label for="orderdwldformat">
	  <B> <isa:translate key="odrdwnld.select.format"/> </B></label>
	  </td>
  <form name="orderdownload" method="post" action='<isa:webappsURL name ="/b2b/orderDownload.do"/>'>
	  <td colspan=1>
	   &nbsp;&nbsp;

	    <select name="format" id="orderdwldformat">
	    <% if (!isBackendR3) { %>
	      <option  value="<%= OrderDownloadForm.PDF_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.pdf"/></option>
	    <% } %>
	      <option  value="<%= OrderDownloadForm.CSV_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.csv"/></option>
	      <option  value="<%= OrderDownloadForm.XML_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.xml"/></option>
	    </select>
	  </td>

	  	<%
			if(ui.isAccessible()) {
				String dwldBtnTxt = WebUtil.translate(pageContext, "massdwnld.startdownld.btn", null);
				String cancelBtnTxt = WebUtil.translate(pageContext, "b2b.order.display.submit.cancel", null);
		%>
				<td colspan=1>&nbsp;&nbsp;
				<input type="submit" name="orderdownloadbtn"
				title="<isa:translate key="access.button" arg0="<%=dwldBtnTxt%>" arg1=""/>"
				value="<isa:translate key="massdwnld.startdownld.btn"/>" class="green">
				</td>
				<!-- cancel btn -->
				<td colspan=1>&nbsp;&nbsp;
				<input type="submit" name="orderdownloadcnclbtn"
				title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>"
				value="<isa:translate key="b2b.order.display.submit.cancel"/>"
				onClick="form_input().location.href='<isa:webappsURL name ="/b2b/orderDownload.do?exit=true"/>';return false;" class="green">
	  			</td>
		<%  } 
		    else { %>
				<td colspan=1>&nbsp;&nbsp;
				<input type="button" name="orderdownloadbtn" onclick="download()"
				 value="<isa:translate key="massdwnld.startdownld.btn"/>" class="green">
			  	</td>
			  	<!-- cancel btn -->
			  	<td colspan=1>&nbsp;&nbsp;
				<input type="submit" name="orderdownloadcnclbtn"
				value="<isa:translate key="b2b.order.display.submit.cancel"/>"
				onClick="form_input().location.href='<isa:webappsURL name ="/b2b/orderDownload.do?exit=true"/>';return false;" class="green">
	  			</td>
		<% } %>

	  </tr>
	    <input type="hidden" name="orderIDS" value="" >
	    <input type="hidden" name="orderGUIDS" value="">
	    <!-- Field which distinguishes between the single order/mass download of orders -->
        </form>
      </tbody>
    </table>
  <% } // END nothingSelectedyet
  else {
  %>
<isa:translate key="massdwnld.noorders.msg1"/>

  <% } %>


</body>
</html>