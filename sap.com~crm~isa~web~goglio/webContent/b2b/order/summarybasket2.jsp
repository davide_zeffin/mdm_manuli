<%--
********************************************************************************
    File:         summarybasket2.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Steffen Mueller
    Created:      21.05.2001
    Version:      1.0

    $Revision: #0 $
    $Date: 2001/05/21 $
********************************************************************************
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>
<%--
<%@ include file="checksession.inc" %>
--%>

<%
    HeaderSalesDocument  header =
            (HeaderSalesDocument) request.getAttribute(MaintainBasketBaseAction.RK_HEADER);

    ItemList items =
            (ItemList) request.getAttribute(MaintainBasketBaseAction.RK_ITEMS);

    JspUtil.Alternator alternator =
            new JspUtil.Alternator(new String[] {"odd", "even" });

    String s = "";

    int newpos = 5;

    boolean contract = false;
    // Check for contracts
    Iterator it = items.iterator();

    while (it.hasNext()) {
      ItemSalesDoc item = (ItemSalesDoc) it.next();
      if (item.getContractId() != null && item.getContractId().length() > 0) {
        contract = true;
        break;
      }
    }

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>
        <isa:translate key="b2b.order.display.pagetitle"/>
    </title>
    <script type="text/javascript">

        function showShipTo(index) {
          var url = "<isa:webappsURL name="b2b/showshipto.do"/>?<%=ShowShipToAction.PARAM_SHIPTO_INDEX%>=" + index;
          var sat = window.open(url, 'sat_details', 'width=450,height=450,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
          sat.focus();
        }

    </script>

    <isa:includes/>
    <!--
    <link href="<%= WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css") %>"
          type="text/css" rel="stylesheet">
          -->
		  
	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>			  
		  
      <script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/table_highlight.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/order.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js") %>"
              type="text/javascript">
      </script>
				  

  </head>
  <body class="workarea">
    <div id="new-doc" class="module">
      <div class="module-name"><isa:moduleName name="b2b/order/summarybasket2.jsp" /></div>
      <%-- !!!! Do not change the name of the form, the JavaScript functions rely on them !!!! --%>
      <%-- <form action="<isa:webappsURL name="b2b/maintainbasket.do"/>"
            name="order_positions"
            method="post">   --%>
      <font size="+1"><isa:translate key="b2b.order.summary.confirm.order"/></font><br>
      <isa:translate key="b2b.order.summary.thanks"/><br>
      <isa:translate key="b2b.order.summary.receipted"/><br>
      <br>
      <font size="+1"><isa:translate key="b2b.order.summary.order"/>
      &nbsp; <%= JspUtil.removeNull(header.getSalesDocNumber()) %></font><br>
      <table>
        <tbody>
          <tr>
            <td colspan="10">&nbsp;</td>
          </tr>
          <tr>
            <td align="right">
                <isa:translate key="b2b.order.display.ponumber"/>
            </td>
            <td>
                <!--&nbsp; &nbsp; -->
                <%= JspUtil.removeNull(header.getPurchaseOrderExt()) %>
            </td>
            <td align="right">
                <isa:translate key="b2b.order.display.pricenet"/>
            </td>
            <td align="right">
                <%= JspUtil.removeNull(header.getNetValueWOFreight()) %>
            </td>
          </tr>
          <tr>
            <td align="right">
                <isa:translate key="b2b.order.display.poname"/>
            </td>
            <td>
                <!--&nbsp; &nbsp;-->
                <%= JspUtil.removeNull(header.getDescription()) %>
            </td>
            <td align="right">
                <isa:translate key="b2b.order.display.freight"/>
            </td>
            <td align="right">
                <%= JspUtil.removeNull(header.getFreightValue()) %>
            </td>
          </tr>
          <tr>
            <td align="right">
                <isa:translate key="b2b.order.display.shipto"/>
            </td>
            <td>
                <isa:iterate id="shipTo"
                             name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
                             type="com.sap.isa.businessobject.ShipTo">
                  <% String selected = "";
                     if (shipTo.equals(header.getShipTo())) { %>
                        <%= shipTo.getShortAddress() %>
                      <%  } %>
                </isa:iterate>
            </td>
            <td align="right">
                <isa:translate key="b2b.order.display.tax"/>
            </td>
              <td align="right">
                <%= JspUtil.removeNull(header.getTaxValue()) %>
              </td>
            </tr>
            <tr>
              <td align="right">
                <isa:translate key="b2b.order.display.shipcond"/>
              </td>
              <td>
                  <isa:iterate id="shipCond"
                             name="<%= MaintainBasketBaseAction.RC_SHIPCOND %>"
                             type="com.sap.isa.core.util.table.ResultData">
                    <% String selected = "";
                     if (shipCond.getRowKey().toString().equals(header.getShipCond())) { %>
                            <%= shipCond.getString(1) %>
                        <% } %>
                </isa:iterate>
              </td>
              <td align="right">
                <isa:translate key="b2b.order.display.pricebrut"/>
              </td>
              <td align="right">
                <%= JspUtil.removeNull(header.getGrossValue()) %>
              </td>
            </tr>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>
          </tbody>
        </table>
        <table class="list" border="0" cellspacing="0" cellpadding="3">
          <thead>
            <tr>
              <th>
                <isa:translate key="b2b.order.display.posno"/>
              </th>
              <th>
                <isa:translate key="b2b.order.display.productno"/>
              </th>
              <th>
                <isa:translate key="b2b.order.display.partnerproduct"/>
              </th>
              <th>
                <isa:translate key="b2b.order.display.quantity"/>
              </th>
              <th>
                <isa:translate key="b2b.order.display.unit"/>
              </th>
              <th>
                <isa:translate key="b2b.order.display.productname"/>
              </th>
              <% if (contract) { %>
                <th>
                  <isa:translate key="b2b.order.display.contract"/>
                </th>
              <% } %>
              <th>
                <isa:translate key="b2b.order.display.available"/>
              </th>
              <th>
                <isa:translate key="b2b.order.display.available.date"/>
              </th>
              <th>
                <isa:translate key="b2b.order.display.totalprice"/>
              </th>
            </tr>
          </thead>
          <tbody>
            <% int line = 0; %>
            <isa:iterate id="item" name="<%= MaintainBasketBaseAction.RK_ITEMS %>"
                         type="com.sap.isa.businessobject.item.ItemSalesDoc">
                <% MessageList messages = item.getMessageList();
                         /*  System.out.println(messages); */
                   String itemMessageString = "";
                   for (int i = 0; i < messages.size(); i++) {
                     itemMessageString += messages.get(i).getDescription();
                   }
                   line++;
                   String[] units = item.getPossibleUnits(); %>
                <tr id="row_<%= line %>" class="<%= alternator.next() %>">
                  <td rowspan="4">
                    <%= line %>
                  </td>
                  <td>
                    <%= JspUtil.removeNull(item.getProduct()) %>
                  </td>
                  <td>
                    <%= JspUtil.removeNull(item.getPartnerProduct()) %>
                  </td>
                  <td>
                    <%= JspUtil.removeNull(item.getQuantity()) %>
                  </td>
                  <td>
                    <%= JspUtil.removeNull(item.getUnit()) %>
                  </td>
                  <td>
                    <%= JspUtil.removeNull(item.getDescription()) %>
                  </td>
                  <% if (contract) { %>
                    <td>
                      <%= JspUtil.removeNull(item.getContractId()) %> / <%= JspUtil.removeNull(item.getContractItemId()) %>
                    </td>
                  <% } %>
                  <td>
                    <%= JspUtil.removeNull(item.getConfirmedQuantity()) %>
                  </td>
                  <td>
                    <%= JspUtil.removeNull(item.getConfirmedDeliveryDate()) %>
                  </td>
                  <td align="right">
                    <%= JspUtil.removeNull(item.getGrossValue()) %>
                  </td>
                </tr>
                <tr>    <!-- Additional row 1 -->
                  <td align="right">
                    <isa:translate key="b2b.order.disp.reqdeliverydate"/>
                  </td>
                  <td colspan="3">
                    <%= JspUtil.removeNull(item.getReqDeliveryDate()) %>
                  </td>
                  <td align="right" colspan="3">
                    <isa:translate key="b2b.order.details.price"/>
                  </td>
                  <td colspan="3">
                    <%= JspUtil.removeNull(item.getNetPrice()) %>
                  </td>
                </tr>
                <tr>    <!-- Additional row 2 -->
                  <td align="right">
                    <isa:translate key="b2b.order.display.shipto"/>
                  </td>
                  <td colspan="3">
                    <isa:iterate id="shipTo"
                             name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
                             type="com.sap.isa.businessobject.ShipTo">
                    <% String selected = "";
                        if (shipTo.equals(item.getShipTo())) { %>
                            <%= shipTo.getShortAddress() %>
                        <%  } %>
                    </isa:iterate>
                  </td>
                  <td colspan="3">
                    &nbsp;
                  </td>
                  <td colspan="3">
                    &nbsp;
                  </td>
                </tr>
                <tr>    <!-- Additional row 3 -->
                  <td align="right"><isa:translate key="b2b.order.details.note"/>&nbsp;
                  </td>
                  <td colspan="9">
                    <%= JspUtil.replaceSpecialCharacters(item.getText().getText()) %>
                  </td>
                </tr>
              <% if (itemMessageString.length() > 0) { %>
                <tr>
                  <td colspan="10">
                    <font color="red"><%= itemMessageString %></font><br>
                  </td>
                </tr>
              <% } %>
              </isa:iterate>

            <tr>
              <td colspan="10">&nbsp;</td>
            </tr>
          </tbody>
        </table>
        <br><br>
        <div align="right">
<!--          <input type="button" name="send"   value="<isa:translate key="b2b.order.display.submit.send"/>" onclick="send_confirm();">&nbsp;
          <input type="button" name="cancel" value="<isa:translate key="b2b.order.display.submit.cancel"/>" onclick="cancel_confirm();">
-->
        </div>
        <br><br>
      </form>
    </div>
  </body>
</html>