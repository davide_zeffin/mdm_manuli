<%--
********************************************************************************
    File:         order_change.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      3.5.2001
    Version:      1.0

    $Revision: #13 $
    $Date: 2002/08/05 $
********************************************************************************
--%>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.OrderChangeUI" %>

<%@ page import="com.sap.spc.remote.client.object.IPCItem" %>

<%@ page import="com.sap.isa.backend.boi.isacore.*"%>
<%@ page import="com.sap.isa.helpvalues.*" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.*" %>
<%@page import="java.text.DateFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="/b2b/checksession.inc" %>

<isacore:browserType/>
<%
    // Check if the product configuration was closed and we returned to the basket
    // In this case we must close this separate window of the configuration
      String exitconfig = (String) request.getAttribute(MaintainOrderBaseAction.RC_EXITCONFIG);
        if (exitconfig != null) { %>
           <html>
           <head>
           <script type="text/javascript">
             this.close();
           </script>
           </head>
           <body></body>
           </html>
<%         return;
        }
        %>
<%
  boolean leftColumn = false;
  String itemPredecessorTypeKey;
  String predecessorTextKey = "status.sales.predecessor";
  OrderChangeUI ui = new OrderChangeUI(pageContext);
  //inline display of product configuration will be restricted on values of identifying characteristics
  boolean showDetailView = false;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">


<isa:contentType />
<%--<html>--%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
<%-- Comment lines starting with "AS:" replace the previous lines, if the Accessibility Switch is set. --%>

  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>
        <isa:translate key="b2b.order.display.pagetitle"/>
    </title>
   <%-- <isa:includes/>  --%>
    <isa:stylesheets/>
    <script type="text/javascript">
    <!--
      <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
      //-->
    </script>

    <%-- include calendar control --%>
    <%@ include file="/appbase/jscript/calendar.js.inc.jsp"%>
    
    <%DateFormat dateFormatter = new SimpleDateFormat(ui.getDefaultDateFormat());%>

    <script src="<isa:mimeURL name="b2b/jscript/table_highlight.js" />"
            type="text/javascript">
    </script>

    <script src="<isa:mimeURL name="b2b/jscript/reload.js" />"
            type="text/javascript">
    </script>

    <script src="<isa:mimeURL name="b2b/jscript/frames.js" />"
            type="text/javascript">
    </script>

    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/browsersniff.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "auction/jscript/calendar.js") %>"
            type="text/javascript">
    </script>

 <% if(ui.getShop().isPricingCondsAvailable() == true) {%>
	   <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
			 type = "text/javascript">
	   </script>
  <% } %>

    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
          type="text/javascript">
    </script>
   <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderToolsRetkey.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
          type="text/javascript">
    </script>
        <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderToolsRetkey.js") %>"
          type="text/javascript">
    </script>
        <script type="text/javascript">
          <%@ include file="/b2b/jscript/ExtRefObjTools.js"  %>
    </script> 


    <%-- javascript functions for batches - only when batch flag on shop level is enabled --%>
    <isacore:ifShopProperty property = "batchAvailable" value = "true">
        <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/batchTools.js") %>"
                type="text/javascript">
        </script>
    </isacore:ifShopProperty>


    <%@ include file="/b2b/jscript/orderTools.inc.jsp"  %>

    <script type="text/javascript">

      // function for the onload()-event
      function loadPage() {
        <isacore:ifShopProperty property = "cuaAvailable" value = "true">
            document.forms["order_positions"].cuaproductkey.value="";
        </isacore:ifShopProperty>
        document.forms["order_positions"].showproduct.value="";
      }
     
     </script>
<%-- Search for additional fields --%>
    <%-- The javascript function openWin is used to display the values
    in an additional popup window --%>
    <script type="text/javascript">
    <!--
        <%=HelpValuesSearchUI.getJSPopupCoding("IncoTerms1",pageContext,new Boolean(false)) %>

        <%
        if (ui.isProductValuesSearchAvailable()) { %>
                <%=HelpValuesSearchUI.getJSPopupCoding("Product",pageContext,new Boolean(false)) %>
        <%
        }%>
        //-->
    </script>
<%-- End search --%>
</head>

<body class="orderchange" onload="loadPage();miRegisterObjects();" <% if (!ui.isHeaderChangeOnly()) { %> onfocus="selReqDelSelectWdwOpen();" onkeypress="checkReturnKeyPressed(event);"<% } %>>
    <div class="module-name"><isa:moduleName name="b2b/order/order_change.jsp" /></div>

    <h1><span><%= ui.getHeaderDescription() %>:&nbsp;<%= ui.getDocNumber() %>&nbsp;<isa:translate key="b2b.docnav.from"/>&nbsp;<%= ui.getDocDate() %></span></h1>
  <%--
    The body consists of two layers:
    - the document layer, containing the header and the items of the document. It is scrollable.
    - the buttons layer, containig the buttons for the document. It stays at the bottom of the page.
  --%>
        <%-- !!!! Do not change the name of the form, the JavaScript functions rely on them !!!! --%>
      
    <div id="document">
      <form action="<isa:webappsURL name="b2b/maintainorder.do"/>"
            id="order_positions"
            name="order_positions"
            method="post">
        <%-- Show accessibility messages --%>
        <% if (ui.isAccessible) { %>
                <%@ include file="/appbase/accessibilitymessages.inc.jsp"  %>
        <% } %>    
        <div class="document-header">
            <%-- Accesskey for the header section. --%>
            <a id="access-header" href="#access-items" title="<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>
            <%--
            The document layer contains:
            - the document-header layer, which holds the header information of the document
            - the document-items layer, which contains all items
            --%>

            <div>
                <isacore:requestSerial mode="POST"/>
                <%-- store all businesspartner informations --%>
                <%= ui.storeBusinessPartners() %>
            </div>
            <%--
            The document-header layer contains:
            - the data table. It displays the title of the document and the standard input fields like <Your Reference:>, <Deliver To>, ...
            - the price table. It displays price information like <Total price net>, <Freight>, <Taxes> ...
            - the message table. It displays the textarea for the message entry <Your message to us>.
            - the error layer (optionally), if there are errors.
            --%>
            <div class="header-basic">
            <table class="layout"> <%-- level: sub3 --%>
		        <tr>
		            <td class="col1">
                        <% if (ui.isAccessible) { %>
                            <a href="#end-table1" title="<isa:translate key="b2b.acc.header.dat.link"/>"></a>
            		    <% } %>		            
                        <table class="header-general" summary="<isa:translate key="b2b.acc.header.dat.sum"/>">   
                            <% if ( ui.isHeaderValidToAvailable() && ui.isElementVisible("order.validTo")) { %>
	                            <tr>
	                                <td class="identifier"><isa:translate key="status.sales.detail.validTo"/>:</td>
	                                <td class="value"><%= JspUtil.encodeHtml(ui.header.getValidTo())%></td>
	                            </tr>
                            <% } %>
				            <% if (ui.isServiceRecall()) { %>
				                <%@ include file="/b2b/serviceRecallHeader.inc.jsp" %>	
				            <% } %>                            
                            <% if (ui.isSoldToVisible() && ui.isElementVisible("order.soldTo")) { %>
                            <tr>
                                <td class="identifier"><isa:translate key="b2b.order.display.cnsmr.b2r.no"/>&nbsp;</td>
                                <td class="value"><%= JspUtil.removeNull(ui.getSoldToId()) %>&nbsp;<%= JspUtil.encodeHtml(ui.getSoldToName()) %>
                                <input type="hidden" name="customerNumberOld" value="<%= JspUtil.encodeHtml(ui.getSoldToId()) %>"/></td>
                            </tr>
                            <% } %>
                            <% if (ui.isElementVisible("order.numberExt")) { %>
	                            <tr>
	                                <td class="identifier"><isa:translate key="b2b.order.display.ponumber"/>&nbsp;</td>
	                                <td class="value">
	                                <% if (ui.isElementEnabled("order.numberExt"))  { %>
	                                    <input class="textinput-large" type="text" maxlength="<%=(ui.isBackendR3()) ? 20 : 35 %>" name="poNumber" id="poNumber" value="<%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %>"/>
	                                <% } else { %>
	                                    <%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %>
	                                    <input type="hidden" name="poNumber" id="poNumber" value="<%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %>"/>
	                                <% } %>
	                                </td>
	                            </tr>
                            <% } %>
                            <% if (!ui.isBackendR3() && ui.isElementVisible("order.description")) { %>
                            <tr>
                                <td class="identifier"> <isa:translate key="b2b.order.display.poname"/>&nbsp;</td>
                                <td class="value">
                                <% if (ui.isElementEnabled("order.description"))  { %>
                                    <input class="textinput-large" type="text" name="headerDescription" id="headerDescription" value="<%= JspUtil.encodeHtml(ui.header.getDescription()) %>"/>
                                <% } else { %>
                                    <%= JspUtil.encodeHtml(ui.header.getDescription()) %>
                                    <input type="hidden" name="headerDescription" id="headerDescription" value="<%= JspUtil.encodeHtml(ui.header.getDescription()) %>"/>
                                <% } %>
                                </td>
                            </tr>
                            <% } %>
                        </table>
                        <% if (ui.isAccessible) { %>
                            <a name="end-table1" title="<isa:translate key="b2b.acc.header.dat.end"/>"></a>
                        <% } %>
                        <%--End table header-general --%>
                    </td>			            
                    <td class="col2"> 
                        <%--Status--%>
                        <% if (ui.isAccessible) { %>
                            <a href="#end-table2" title="<isa:translate key="b2b.acc.header.stat.link"/>"></a>
                        <% } %>
                        <% if (!ui.header.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE) && ui.isElementVisible("order.status.overall")) { %>
	                        <table class="status" summary="<isa:translate key="b2b.acc.header.stat.sum"/>"> <%-- level: sub3 --%>
	                            <tr>
	                                <td class="identifier"><isa:translate key="status.sales.overallstatus"/>:</td>
	                                <td class="value" ><isa:translate key="<%= JspUtil.encodeHtml(ui.getDocumentStatusKey()) %>"/></td>
	                            </tr>
	                        </table>
                        <% } %>
                        <% if (ui.isAccessible) { %>
                            <a name="end-table2" title="<isa:translate key="b2b.acc.header.stat.end"/>"></a>
                        <% } %>
                        <%-- End Status --%>                                           
                        <%-- Price--%>
                        <% if (ui.isAccessible) { %>
                            <a href="#end-table3" title="<isa:translate key="b2b.acc.header.prc.link"/>"></a>
                        <% } %>
                        <table class="price-info" summary="<isa:translate key="b2b.acc.header.prc.sum"/>"> <%-- level: sub3 --%>
                            <% if (ui.isNetValueAvailable()  && ui.isElementVisible("order.priceNet")) { %>
	                            <tr>
	                                <td class="identifier" scope="row"><isa:translate key="b2b.order.display.pricenet"/></td>
	                                <td class="value"><%= JspUtil.removeNull(ui.getHeaderValue()) %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
	                            </tr>
                            <% } %>
                            <% if (ui.isFreightValueAvailable() && ui.isElementVisible("order.freight")) { %>
                            <tr>
                                <td class="identifier"  scope="row"><isa:translate key="b2b.order.display.freight"/></td>
                                <td class="value"><%= JspUtil.removeNull(ui.header.getFreightValue()) %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                            </tr>
                            <% } %>
                            <% if (ui.isTaxValueAvailable() && ui.isElementVisible("order.tax")) { %>
                            <tr>
                                <td class="identifier" scope="row"><isa:translate key="b2b.order.display.tax"/></td>
                                <td class="value"><%= JspUtil.removeNull(ui.header.getTaxValue()) %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                            </tr>
                            <% } %>
                            <% if (ui.isGrossValueAvailable()&& ui.isElementVisible("order.priceGross")) { %>
	                            <tr>
	                                <td colspan="2" class="separator"></td>
	                            </tr>
	                            <tr>
	                                <td class="identifier" scope="row"><isa:translate key="b2b.order.display.pricebrut"/></td>
	                                <td class="value"><%= JspUtil.removeNull(ui.header.getGrossValue()) %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
	                            </tr>
                            <% } %>
                            <% if (ui.showHeaderPaymentTerms() && ui.isElementVisible("order.paymentterms")) { %>
                            <tr>
                                <td class="identifier-1" scope="row"><isa:translate key="b2b.order.disp.paymentterms"/></td>
                                <td class="terms"><%=JspUtil.encodeHtml(ui.header.getPaymentTermsDesc()) %></td>
                            </tr>
                            <% } %>
                        </table>
                        <% if (ui.isAccessible) { %>
                            <a name="end-table3" title="<isa:translate key="b2b.acc.header.prc.end"/>"></a>
                        <% } %>
                        <%-- End Price --%>
                    </td>
                </tr>
            </table> <%-- class="layout"> --%>
            </div> <%-- class="header-basic"> --%>
            <% if (ui.isAccessible) { %>
                <a name="#end-table4" title="<isa:translate key="b2b.acc.header.alldata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
            <% } %>            
	        <div class="header-itemdefault">  <%-- level: sub3 --%>
	            <h1 class="group"><span><isa:translate key="b2b.header.itemdefault"/></span></h1>
	            <div class="group">
                    <%-- Item defaults Data--%>
                    <% if (ui.isAccessible) { %>
                        <a name="#end-table5" title="<isa:translate key="b2b.acc.header.defdata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
                    <% } %>
                    <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>">
                        <% if (ui.isElementVisible("order.deliverTo")) { %>
	                        <tr>
	                            <td class="identifier"><label for="headerShipTo"><isa:translate key="b2b.order.display.soldto"/></label></td>
	                            <td class="value">
	                            <% if (ui.isElementEnabled("order.deliverTo"))  { %>
				                    <% if (ui.header.getShipTo().getHandle() != null) { %>
				                    	<input type="hidden" name="headerShipToHandle" value="<%=ui.header.getShipTo().getHandle()%>"/>
				                    <% } %>	
				                    <select class="select-large" name="headerShipTo" id="headerShipTo" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.shipto.title"/>" <% } %> >
	                                    <isa:iterate id="shipTo"
	                                        name="<%= MaintainOrderBaseAction.SC_SHIPTOS %>"
	                                        type="com.sap.isa.businessobject.ShipTo">
	                                        <% if (shipTo.isActive()) { %>
	                                            <option <%= ui.getShipToSelected(shipTo) %> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
	                                        <% } else if (!shipTo.isActive() && shipTo.equals(ui.header.getShipTo())) { %>
	                                            <option value="">&nbsp;</option>
	                                        <% } %>
	                                    </isa:iterate>
	                                </select>&nbsp;
	                                <a class="icon" href="#" onclick="showShipTo(document.order_positions.headerShipTo.selectedIndex);" >
	                                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/> <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" class="display-image"/></a>&nbsp;
	                            <% } else { %>
	                                 <%= JspUtil.encodeHtml(ui.header.getShipToData().getShortAddress()) %>
	                                 <a class="icon" href="#" onclick="showShipToWithTechKey(<%= ui.header.getShipTo().getTechKey().getIdAsString()%> );" >
	                                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/> <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>"  class="display-image"/></a>&nbsp;
	                            <% } %>
	                            <% if (ui.isElementEnabled("order.deliverTo"))  { %>
	                                <a class="icon" href="#" onclick="newShipTo();">
	                                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_new_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.icon.newshipto"/> <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" class="display-image"/></a><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="35" height="1" alt="" />
	                            <% } %>
	                             </td>
	                         </tr>
	                     <% } %> 
		                 <% if (ui.isElementVisible("order.shippingCondition")) { %>		            
					           <%@ include file="/b2b/shippingCondition.inc.jsp" %>
		                 <% } 
					        if (ui.isElementVisible("order.deliveryPriority") && !ui.isBackendR3()) { %>		            					
                               <%@ include file="/b2b/deliveryPriorityHeader.inc.jsp" %>
		                 <% } %>                       
                         <tr>
                         <% if ( (! ui.header.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE) && ui.isElementVisible("order.reqDeliveryDate")) && (!ui.isAuction() ) && (!ui.isHeaderChangeOnly()) ) { %>     
                             <td class="identifier"><label for="headerReqDeliveryDate"><isa:translate key="b2b.order.disp.reqdeliverydate"/></label></td>
                             <td class="value">
                                 <% if (ui.isElementEnabled("order.reqDeliveryDate")) { %>
                                   <input class="textinput-middle" type="text" name="headerReqDeliveryDate" id="headerReqDeliveryDate" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.disp.reqdelivdatehead2"/><isa:translate key="b2b.acc.reqdeldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
                                     value="<%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %>"
                                     <% if (!ui.isHeaderChangeOnly()) { %>
                                         onblur="checkReqDateChange()"
                                     <% } %>
                                     />
                                     <% if (!ui.isAccessible) { %>
                                         <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'headerReqDeliveryDate');">
		                                    <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
							             </a>
                                         <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.order.disp.reqdelivdatehead2"/>"/>
                                     <% }
                                    } else { %>
                                     <%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %>
                                 <% } %>
                             </td>
                         <% }
                            else {  %>
                             <td colspan="2">
                                 <input name="headerReqDeliveryDate" type="hidden" value="<%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %>">
                             <td>
                         <% } %>
                         </tr>
                         <%-- Header Cancel date --%>
                         <% if (ui.isElementVisible("order.latestDeliveryDate")){ %>
                         <tr>
                         <% if ( ! ui.header.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE)) { %>
                             <td class="identifier"><label for="cancel_date"><isa:translate key="b2b.order.grid.canceldate"/></label></td>
                             <td class="value">
                                 <% if (ui.isElementEnabled("order.latestDeliveryDate")) { %>
                                 <input class="textinput-middle" type="text" name="headerlatestdlvdate" id="headerlatestdlvdate" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.order.grid.canceldatehead2"/><isa:translate key="b2b.acc.canceldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
                                     value="<%= JspUtil.encodeHtml(ui.header.getLatestDlvDate()) %>"
                                     <% if (!ui.isHeaderChangeOnly()) { %>
                                         onblur="checkCancelDateChange()"
                                     <% } %>
                                     />
                                     <% if (!ui.isAccessible) { %>
                                         <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'headerlatestdlvdate');">
		                                    <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
							             </a>
                                         <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.order.grid.canceldatehead2"/>"/>
                                     <% }
                                    } else { %>
                                     <%= JspUtil.encodeHtml(ui.header.getLatestDlvDate()) %>
                                 <% } %>
                             </td>
                         <% }
                            else {  %>
                             <td colspan="2">
                                 <input name="headerlatestdlvdate" type="hidden" value="<%= JspUtil.replaceSpecialCharacters(ui.header.getLatestDlvDate()) %>">
                             <td>
                         <% } %>
                     </tr> 
                     <% } %><%-- end of header cancelDate --%>
                 </table> <%-- end class="data" --%>
                 <% if (ui.isAccessible) { %>
                     <a name="end-table5" title="<isa:translate key="b2b.acc.header.end.defdata"/>"></a>
                 <% } %>                 
                 <%--End Item defaults Data--%>
	         </div> <%-- end class="group" --%>
	     </div> <%-- class="header-itemdefault"> --%> 
	     
	     <%-- Additional Header Data--%>
         <% if (ui.isAccessible) { %>
             <a name="#end-table6" title="<isa:translate key="b2b.acc.header.adddata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
         <% } %>
         <div class="header-additional">  <%-- level: sub3 --%>
            <h1 class="area">
                <a class="icon" href="javascript:toggleHeader()" ><img id="addheadericon" src="<%=WebUtil.getMimeURL(pageContext,"mimes/images/open.gif")%>" alt="<isa:translate key="b2b.order.adddata.icon.detail"/>"  /></a>
                <span><isa:translate key="b2b.header.adddata"/></span>
            </h1>	            
	        <div class="area" id="addheader" style="display: none">
                <div class="header-misc">
                    <h1 class="group"><span><isa:translate key="b2b.header.group.misc"/></span></h1>
                    <div class="group">
                        <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>">   
                            <% if (!ui.isBackendR3()) { %>
                               <%@ include file="/b2b/incoterms.inc.jsp" %>
                            <% } %>
				            <%@ include file="/b2b/campaignsHeader.inc.jsp" %>    
                        </table> <%-- end class="data" --%>
                    </div> <%-- end class="group" --%>
                </div> <%-- end class="header-misc" --%>
	            <% if ((ui.header.getAssignedExternalReferences() != null && ui.header.getAssignedExternalReferences().size() > 0)
                        || (ui.header.getExtRefObjectType() != null && ui.header.getExtRefObjectType().length() > 0)) { %>
                     <div class="header-misc">
                          <h1 class="group"><span><isa:translate key="b2b.header.group.extref"/></span></h1>
                              <div class="group">
                                  <table class="data" summary="<isa:translate key="b2b.acc.header.extrefdata"/>">
          		                       <% if (!ui.isServiceRecall()) { %>
				                          <%@ include file="/b2b/extrefobjectheader.inc.jsp" %>	
				                       <% } %>
				                       <%@ include file="/b2b/extrefnumberheader.inc.jsp" %>  
                                  </table> <%-- end class="data" --%>
                              </div> <%-- end class="group" --%> 
                     </div> <%-- end class="header-misc" --%> 
                <% } %>                               
                <%-- Payment data --%>
                <%@ include file="/b2b/payment.inc.jsp" %> 
				<%-- end payment data --%>
                <div class="header-message">
                    <h1 class="group"><span><isa:translate key="b2b.header.group.message"/></span></h1>
                    <%-- Data Message --%>
                    <div class="group">
                        <table class="message" summary="<isa:translate key="b2b.acc.header.msg.sum"/>">
                            <% if (ui.isElementVisible("order.comment")) { %>
	                            <tr>
	                                <td class="identifier" ><label for="headerComment"><isa:translate key="b2b.order.details.note"/></label></td>
	                                <td class="value">
	                                <% if (ui.isElementEnabled("order.comment"))  { %>
	                                    <textarea id="headerComment" name="headerComment" rows="5" cols="80"><%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText(), new char[] {'\n'}):"" %></textarea>
	                                <% } else { %>
	                                    <textarea id="headerComment" name="headerComment"  class="textarea-disabled"  rows="5" cols="80" readonly><%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText(), new char[] {'\n'}):"" %></textarea>
	                                <% } %>
	                                </td>
                                </tr>
                             <% } %>
                        </table>
                    </div> <%-- end class="group" --%>
				    <%-- End Data Message --%>
				</div> <%-- end class="header-message" --%>
			</div> <%-- End class="area" --%>
		</div> <%-- class="header-additional"> --%>
        <% if (ui.isAccessible) { %>
            <a name="end-table6" title="<isa:translate key="b2b.acc.header.end.adddata"/>"></a>
        <% } %>
        <%-- End Additional Header Data --%>
        <%-- Document flow --%>
        <% if (!ui.isHomActivated() && ui.isHeaderDocFlowAvailable()) { %>
            <div class="header-docflow">  <%-- level: sub3 --%>
                <h1 class="group"><span><isa:translate key="b2b.header.group.docflow"/></span></h1>
                <div class="group">
                    <table class="data">
		                <%@ include file="/ecombase/connecteddocuments.inc.jsp" %>                
                    </table> <%-- end class="data" --%>
                </div> <%-- end class="group" --%>
            </div> <%-- class="header-docflow"> --%>
        <% } %>               
        <%-- End Document flow --%>                 

        <%-- Error --%>
        <%@ include file="/b2b/headErrMsg.inc.jsp" %>
        <%-- End Error --%>
    </div> <%-- End  Document Header --%>

    <%-- the item information for the basket --%>
    <%  boolean row_1_isReqDeliveryDateChangeable = true;
        String tag_style_detail = "";
        String ipcItemId = "";
        String gridConfigType = "";

        if (ui.showItems()) { %>

   <%--Item List--%>
    <div class="document-items"> <%-- level: sub2 --%>
    <%--
    The document-items layer contains:
    - a table itemlist, which displays all items.
    --%>

    <%-- Accesskey for the items section. It must be an resourcekey, so that it can be translated. --%>
    <a id="access-items" href="#access-buttons" title= "<isa:translate key="b2b.acc.items.title"/>" accesskey="<isa:translate key="b2b.acc.items.key"/>"></a>

    <% if (ui.isAccessible) { %>
         <a href="#end-table10" title="<isa:translate key="b2b.acc.items.link"/>"></a>
    <% } %>
    <table class="itemlist" summary="<isa:translate key="b2b.acc.items.sum"/>"> <%-- level: sub3 --%>
           <tr>
              <th class="opener" scope="col">
              &nbsp;
              <% int numberOfRows = ui.items.size() + ui.newpos;
                             int startIndex   = 1; %>
                     <a class="icon" href='javascript: toggleAllItemDetails("rowdetail_all","rowdetail_", <%= numberOfRows%>, <%= startIndex %>)' >
                                   <img class="img-1" id="rowdetail_allclose" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.close"/> <% } %>"/>
                                           <img class="img-1" id="rowdetail_allopen" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.open"/> <% } %>"/>
                                        </a>
              </th>
              <th class="item" scope="col"><isa:translate key="b2b.order.display.posno"/></th>
              <th class="product" scope="col"><isa:translate key="b2b.order.display.productno"/></th>
              <th class="qty" scope="col"><isa:translate key="b2b.order.display.quantity"/></th>
              <th class="unit" scope="col"><isa:translate key="b2b.order.display.unit"/></th>
              <th class="desc" scope="col"><isa:translate key="b2b.order.display.productname"/></th>
              <% if (ui.isContractInfoAvailable()) { %>
                <th class="ref-doc" scope="col"><isa:translate key="b2b.order.display.contract"/></th>
              <% } %>
              <% if (! ui.header.isDocumentTypeOrderTemplate()
                    && ! ui.header.isDocumentTypeQuotation()) { %>
                <th class="qty-avail" scope="col"><isa:translate key="b2b.order.display.available"/></th>
                <th class="date-on" scope="col"><isa:translate key="b2b.order.display.available.date"/></th>
              <% } %>
              <th class="price" scope="col"><isa:translate key="b2b.order.display.totalprice"/>
                  <br></br><span class="slighttext"><isa:translate key="b2b.order.details.price"/></span>
              </th>
              <% if ( ! ui.header.isDocumentTypeOrderTemplate() && ! ui.header.isDocumentTypeQuotation()) {%>
                  <% if (ui.isHomActivated()) { %>
                     <th class="ref-doc" scope="col"><isa:translate key="<%= predecessorTextKey %>"/></th>
                  <% } else { %>
                      <th class="qty-avail"><isa:translate key="b2b.order.display.remainingqty"/></th>
                      <th class="qty-rest"><isa:translate key="b2b.order.display.calledquantity"/></th>
                  <% } %>
                  <th class="status"><isa:translate key="b2b.order.display.state"/></th>
              <% } %>
              <th class="delete"><isa:translate key="b2b.order.display.cancel"/></th>
            </tr>

         
            <isa:iterate id="item" name="<%= MaintainOrderBaseAction.RC_ITEMS %>"
                         type="com.sap.isa.businessobject.item.ItemSalesDoc">

              <%
              /* set the item in the ui class */
              ui.setItem(item);
              String itemKey = item.getTechKey().getIdAsString();        

              /* check if BOM sub item that has to be suppressed */
              if ( !ui.isBOMSubItemToBeSuppressed() ) {
              %>
              <%
                 //Get the configType for main Item only
                 if (!ui.itemHierarchy.isSubItem(item)) {
                        gridConfigType = item.getConfigType();
                 }
              %>

              <% String substMsgColspan = String.valueOf(ui.getItemsColspan()); %>
              <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>

              <tr class="<%= ui.even_odd %>" id="row_<%= ui.line %>"
              <%if (ItemSalesDoc.ITEM_CONFIGTYPE_GRID.equals(gridConfigType) &&
                     ui.itemHierarchy.isSubItem(item) ) {%>
                        style="display:none;"
              <%}%>
              >

              <td class="opener">
                <% if (ui.showItemDetailButton()) { %>
                <%-- do not insert "details symbol" for configurable items on sub-levels --%>
                  <a class="icon" href='javascript: toggle("rowdetail_<%= ui.line %>")'>
                    <img class="img-1" id="rowdetail_<%= ui.line %>close" style="DISPLAY: none"
                         src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>"
                         alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.acc" arg0="<%= item.getNumberInt()%>"/> <% } else { %><isa:translate key="b2b.order.details.item.close"/><% } %>"
                         width="16" height="16"/>
                    <img class="img-1" id="rowdetail_<%= ui.line %>open" style="DISPLAY: inline"
                         src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>"
                         alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.acc" arg0="<%= item.getNumberInt()%>"/> <% } else { %><isa:translate key="b2b.order.details.item.open"/><% } %>"
                         width="16" height="16"/>
                  </a>
                <%
                }
                else {%>
                   &nbsp;
                <%
                } %>
              </td>

              <%-- if sub-level, insert subpos symbols --%>
              <td <% if (ui.itemHierarchy.isSubItem(item)) { %> class="item-sub-<%= ui.getItemLevel() -1 %>" <% } else { %> class="" <% } %>>
                  <% if (ui.isElementVisible("order.item.posNo", itemKey)) { %>
                     <%= JspUtil.removeNull(item.getNumberInt()) %>
                     <input type="hidden" name="pos_no[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getNumberInt()) %>"/>
                  <% } %>
              </td>              
              <% boolean gridChangeable=true;
                //Used to disable the entry field for Grid quantity and product
                 if (ItemSalesDoc.ITEM_CONFIGTYPE_GRID.equals(gridConfigType) &&
                                        ui.itemHierarchy.hasSubItems(item)){
                                        gridChangeable = false;
                 }  %>
              <td class="product">
                <% if (ui.isElementVisible("order.item.product", itemKey)) { %>
	                <% if (ui.isElementEnabled("order.item.product", itemKey) && gridChangeable  && !ui.isItemFreeGoodExclusiveSubItem())  { %>
	                      <input type="text" class="textinput-middle" size="10" name="product[<%= ui.line %>]" id="product[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getProduct()) %>"/>
	                      <% if (ui.isProductValuesSearchAvailable() && ui.isZeroProductId()) { %>
	                           <%= HelpValuesSearchUI.getJSCallPopupCoding("Product","order_positions",""+ui.line,pageContext) %>
	                      <% }
	                } else { %>
	                      <%= JspUtil.encodeHtml(ui.getProduct()) %>
	                      <input type="hidden" name="product[<%= ui.line %>]" id="product[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getProduct()) %>"/>
	                <% } %>
	             <% } %>   
              </td>
              <td class="qty">
                    <% if (ui.isElementVisible("order.item.qty", itemKey)) { %>
	                    <% if (ui.isElementEnabled("order.item.qty", itemKey) && gridChangeable  && !ui.isItemFreeGoodExclusiveSubItem())  { %>
	                       <input type="text" class="textinput-small" name="quantity[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getQuantity()) %>"/>
	                    <% } else { %>
	                         <%= JspUtil.encodeHtml(item.getQuantity()) %><input type="hidden" name="quantity[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getQuantity()) %>"/>
	                     <% } %>
	                 <% } %>
              </td>
              <td class="unit">
                  <% if (ui.isElementVisible("order.item.unit", itemKey)) { %>  
                     <% if (ui.isElementEnabled("order.item.unit", itemKey) && !ui.isZeroProductId)  {
                          if (ui.units.length==1)  { %>
                            <input type="hidden" name="unit[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(ui.units[0])%>"/>
                            <div align="left">
                                <%= JspUtil.encodeHtml(ui.units[0]) %>
                            </div>
                          <% } else { %>
                            <select name="unit[<%= ui.line %>]">
                               <% for (int j = 0; j < ui.units.length; j++) { %>
                                   <option <%= ui.getItemUnitSelected(ui.units[j]) %> value="<%= JspUtil.encodeHtml(ui.units[j]) %>"> <%= JspUtil.encodeHtml(ui.units[j]) %></option>
                               <% } %>
                            </select>
                          <% } %>
                     <% } else {  %>
                          <% if (!ui.isZeroProductId) { %>
                               <input type="hidden" name="unit[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getUnit()) %>"/>
                               <div align="left">
                                   <%= JspUtil.encodeHtml(item.getUnit()) %>
                               </div>
                          <% } else { %> 
                                <% if (ui.units.length==1)  { %>
                                   <input type="text" class="textinput-small" name="unit[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getUnit()) %>"/>
                                <% } else { %>
                                   <select name="unit[<%= ui.line %>]">
                                      <% for (int j = 0; j < ui.units.length; j++) { %>
                                         <option <%= ui.getItemUnitSelected(ui.units[j]) %> value="<%= JspUtil.encodeHtml(ui.units[j]) %>"> <%= JspUtil.encodeHtml(ui.units[j]) %></option>
                                      <% } %>
                                   </select>
                                <% } %>
                          <% } %>
                     <% } %>
                  <% } %>
              </td>
              <td class="desc">
                    <% if (ui.isElementVisible("order.item.description", itemKey)) { %>            
	                    <% if (item.getDescription() != null && !"".equals(item.getDescription().trim())) {
	                           if (ui.getShop().isInternalCatalogAvailable() && !TechKey.isEmpty(item.getProductId())) {%>
	                              <a href="#" onclick="showproductincatalog('<%= item.getProductId().getIdAsString() %>');">
	                                  <%= JspUtil.encodeHtml(item.getDescription()).trim() %> 
	                              </a>
	                           <% } else { %>
	                             <div align="left">
	                                <%= JspUtil.encodeHtml(item.getDescription()).trim() %> 
	                             </div>
	                           <% } %>
	                    <% } %>
					<% } %>   
                    <%  if (ui.getSubstitutionReason() != null && ui.getSubstitutionReason().length() > 0) { %> 
                          <%=ui.getSubstitutionReason() %>&nbsp;
                    <% } %>
                    <% if (ui.isCUAAvailable()) {%>
                        <a href="#" onclick="itemcua('<%=item.getProductId().getIdAsString()%>','<%=item.isProductChangeable()?item.getTechKey().getIdAsString():""%>', '<%= ui.header.getDocumentType() %>');">
                          <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/cua_info.gif") %>" alt="<isa:translate key="b2b.order.display.icon.cuahint"/>" width="16" height="15"/>
                        </a>
                    <%
                    }
                    if (item.isConfigurable()&& ui.isElementVisible("order.item.configuration", itemKey)) {
                         String itemid = item.getTechKey().getIdAsString();
                         if (item.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)) {
								if (item.getExternalItem() != null) {%>
								   <a href="#" onclick="griditemconfig('<%= item.getTechKey().getIdAsString() %>');">
										   <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_grid.gif") %>"
										   alt="<isa:translate key="b2b.order.display.grid"/>
										   <% if (ui.isAccessible) { %><isa:translate key="b2b.order.display.grid"/> <% } %>" />
								   </a>
						 	  <%}else{ %>
								   <a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.grid.item.imp"/>');">
										   <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_grid.gif") %>"
										   alt="<isa:translate key="b2b.order.display.grid"/>" />
								   </a>
						 	  <%}%>
                         <%   }
                         else { 
							if (ui.header.getIpcDocumentId() != null) { 								
								if (ui.isElementEnabled("order.item.configuration", itemKey)) { %>		 					                          
									  <a href="#" class="icon" onclick="itemconfig('<%=item.getTechKey()%>');">
										<img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
										alt="<isa:translate key="b2b.order.change.config.item"/>
										<% if (ui.isAccessible) { %><isa:translate key="b2b.acc.config.item"/> <% } %>" />
									  </a>
								 <%}else{ %>	
									  <a href="#" class="icon" onclick="itemconfig('<%=item.getTechKey()%>');">
										<img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
										alt="<isa:translate key="status.sales.detail.config.item"/>
										<% if (ui.isAccessible) { %><isa:translate key="b2b.acc.config.item"/> <% } %>" />
									  </a>									 	 
								 <%}%>												
				         <%   } else { %>
						                <a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.config.imp"/>');">
                                          <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
                                           alt="<isa:translate key="status.sales.detail.config.item"/>" />
                                       </a>
						 <%   }                              
                        }
                    } %>
                    <% if (ui.isItemCampaignAssignedAuto()) { %>
                          <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/camp_assigned.gif") %>" alt="<isa:translate key="b2b.disp.camp.ass"/>"/>
                    <% } %>
                    <%-- hidden fields for batches - only when batch flag on shop level is enabled --%>
                    <isacore:ifShopProperty property = "batchAvailable" value = "true">
                      <%-- Batch Symbole --%>
                      <% if (item.getBatchDedicated()) { %>
                        &nbsp;
                        <a href="#" onClick='javascript:openBatchDetails("rowdetail_<%=  ui.line %>"); setFocus("batchID[<%= ui.line%>]")'>
                          <img id="batchDetails[<%= ui.line%>]"
                             src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/batch.gif") %>"
                             alt="<isa:translate key="b2b.order.details.batch.dedicated"/>"
                             width="16" height="16"/>
                         </a>
                      <% } %>
                    <%-- Batch Ende --%>
                    </isacore:ifShopProperty>

             </td>
             <% if (ui.isContractInfoAvailable()) { %>
                   <td class="ref-doc">
                        <% if ((item.getContractId() != null) && !(item.getContractId().equals(""))) { %>     
                            <% if (ui.isElementVisible("order.item.contract", itemKey)) { %> 
	                            <a href="<isa:webappsURL name="b2b/contractselect.do"/>?contractSelected=<%= item.getContractKey() %>"><%= JspUtil.encodeHtml(item.getContractId())%></a>&nbsp;/&nbsp;<%= JspUtil.encodeHtml(item.getContractItemId())%>
	                               <% if (!ui.isBackendR3()) { %>
	                                  <%--- deletion of contract reference --%>
	                                  &nbsp; <a href='#' onclick="deletecontract('<%= ui.line %>');">
	                                   <img src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/trash_icon.gif")%>" alt="<isa:translate key="b2b.order.icon.delete.cnt"/>"/>
	                               <% } %>
	                             </a> 
	                          <% } %>  
	                     <input type="hidden" name="detailContractKey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getContractKey().getIdAsString()) %>"/>
                         <input type="hidden" name="detailContractItemKey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getContractItemKey().getIdAsString()) %>"/>
                         <input type="hidden" name="contractRef[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(item.getContractId()) %> / <%= JspUtil.removeNull(item.getContractItemId()) %>"/>                       
                        <% } else { %>
                            &nbsp;
                        <% } %>
                     </td>
             <% } %>
             <% if (! ui.header.isDocumentTypeOrderTemplate()
                    && ! ui.header.isDocumentTypeQuotation()) { %>
                  <td class="qty-avail">
                     <% ArrayList aList = item.getScheduleLines();
                        if (ui.showItemATPInfo() && ui.isElementVisible("order.item.schedulineQty", itemKey)) {
                           for (int i = 0; i < aList.size(); i++) {
                               Schedline scheduleLine = (Schedline) aList.get(i); %>
                          <%= JspUtil.encodeHtml(scheduleLine.getCommittedQuantity()) %><br>
                          <%}
                        }%>
                  </td>
                  <td class="date-on">
                        <% if (ui.showItemATPInfo()&& ui.isElementVisible("order.item.schedulineDate", itemKey)) {
                              for (int i = 0; i < aList.size(); i++) {
                                Schedline scheduleLine = (Schedline) aList.get(i); %>
                            <%= JspUtil.encodeHtml(scheduleLine.getCommittedDate()) %><br>
                          <%}
                        }%>
                  </td>
             <% } %>

             <td class="price">
              <% if (item.isPriceRelevant()) {
                     if (!ui.isZeroProductId && ui.isElementVisible("order.item.grossValue", itemKey)) { %>
                          <%= JspUtil.encodeHtml(ui.getItemValue()) + "&nbsp;" + JspUtil.encodeHtml(item.getCurrency()) %>
                  <% } %>
                  <br><span class="slighttext">
                  <% if ((!ui.isZeroProductId) && ui.isElementVisible("order.item.netPrice", itemKey) && (item.getNetPriceUnit()!=null) && (!item.getNetPriceUnit().equals(""))) { %>
                           <%= JspUtil.encodeHtml(item.getNetPrice()) + "&nbsp;" + JspUtil.encodeHtml(item.getCurrency()) + "&nbsp;" + '/' + "&nbsp;" + JspUtil.encodeHtml(item.getNetQuantPriceUnit()) + "&nbsp;" + JspUtil.encodeHtml(item.getNetPriceUnit()) %></span>
                  <% }
                 } %>
             </td>
             <% if ( ! ui.header.isDocumentTypeOrderTemplate()
                    && ! ui.header.isDocumentTypeQuotation()) {%>
                   <% if ( ! item.isBusinessObjectTypeService() && !ui.isHomActivated() ) { %> <%--// Service positions does not have a remaining qty yet! --%>
                       <td class="qty-avail">
                           <% if (!ui.isZeroProductId && ui.isElementVisible("order.item.qtyDelivered", itemKey)) { %>
                              <%= JspUtil.encodeHtml(item.getDeliveredQuantity() ) %>
                           <% }
                           else { %>
                              &nbsp;
                           <% } %>
                       </td>
                       <td class="qty-rest">
                          <% if (!ui.isZeroProductId && ui.isElementVisible("order.item.qtyToDeliver", itemKey)) { %>
                             <%= JspUtil.encodeHtml(item.getQuantityToDeliver() ) %>
                          <% }
                          else { %>
                            &nbsp;
                          <% } %>
                       </td>
                   <% } else { %>
                      <td class="ref-doc">
                        <% if (ui.isHomActivated()) {
                             ConnectedDocumentItem connectedItem = (ConnectedDocumentItem)item.getPredecessor();  %>
                             <% if (connectedItem == null) { %> <%-- || !ui.isElementVisible("order.item.pred".concat(connectedItem.getDocType()))) { --%>
                                 -- / --
                             <% } else { %>
                                  <% itemPredecessorTypeKey = "status.sales.dt.".concat(connectedItem.getDocType()); %>
                                  <% if (connectedItem.isDisplayable()) { %>
                                         <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>?techkey=<%=connectedItem.getDocumentKey()%>&objects_origin=&object_id=<%= connectedItem.getDocNumber() %>&objecttype=<%= connectedItem.getDocType() %>">
                                         <isa:translate key="<%= itemPredecessorTypeKey %>"/> <%= connectedItem.getDocNumber() %></a>
                                  <% } else { %>
                                         <isa:translate key="<%= itemPredecessorTypeKey %>"/> <%= connectedItem.getDocNumber() %>
                                  <% } %>
                             <% } %>
                        <% } else { %>
                           <% if ( ui.isElementVisible("order.item.qtyToDeliver", itemKey)) { %>
                               <isa:translate key="status.sales.no.remain.qty1"/>&nbsp<isa:translate key="status.sales.no.remain.qty2"/>
                           <% } %>
                        <% } %>
                      </td>
                   <% } %>
                   <td class="status">
                        <% if ( ui.isElementVisible("order.item.status", itemKey)) { %>
                           <%= ui.getItemStatus() %>
                        <% } %>
                   </td>
                  <% } %>
                  <td class="delete">
                     <%
                           if (item.isDeletable()) { %>
                              <input type="checkbox" name="delete[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>
                           <%
                           } else if (item.isCancelable ()) { %>
                              <input type="checkbox" name="cancel[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>
                           <%
                             } else if (!ui.isElementVisible("order.item.status", itemKey) && (item.isItemUsageATP() || item.isItemUsageBOM())) { %>
                              <%= ui.getItemStatus() %>
                           <%
                             } else { %>
                         &nbsp;
                      <% } %>
                      <input type="hidden" name="replacetype[<%= ui.line %>]"  value="<%= ui.getItemReplaceType() %>"/>
                      <input type="hidden" name="itemTechKey[<%= ui.line %>]"  value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>
                      <input type="hidden" name="productkey[<%= ui.line %>]"  value="<%= JspUtil.removeNull(item.getProductId().getIdAsString()) %>"/>
                      <%-- hidden field optimization --%>
                   <% if(!(JspUtil.removeNull(item.getParentId().getIdAsString()).equals(""))) { %>
                           <input type="hidden" name="parentid[<%= ui.line %>]"  value="<%= JspUtil.removeNull(item.getParentId().getIdAsString()) %>"/>
                   <% }
                      if(!(JspUtil.removeNull(item.getPcat()).equals("")) && (ui.isItemMainItem()) ) { %>
                        <input type="hidden" name="pcat[<%= ui.line %>]" value="<%= item.getPcat() %>">
                   <% }
                      if(item.isDataSetExternally()==true) { %>
                        <input type="hidden" name="setExt[<%= ui.line %>]" value="<%= item.isDataSetExternally() ? "T" : "" %>"/>
                   <% } 
                      if(item.isConfigurable() ==true) { %>
                           <input type="hidden" name="configurable[<%= ui.line %>]" value="T"/>
                   <% } %>   
                      <%-- end: hidden field section filled items --%>
                 </td>
              </tr>

              <%-- Item Details --%>
              <% if (ui.showItemDetailButton()) { %>
              <% tag_style_detail = ui.even_odd + "-detail"; %>
              <tr class="<%= tag_style_detail %>" id="rowdetail_<%= ui.line %>" <% if (ui.isToggleSupported()) { %> style="display:none;"> <% } else { %> > <% } %>
                 <td class="select">&nbsp;</td>
                 <td class="detail" colspan="<%= ui.getItemsColspan() %>" >
                 <table class="item-detail">
                      <%@ include file="/b2b/proddetsysid_acc.inc.jsp" %>
                      <%-- display of product configuration --%>
                      <% if (ui.isElementVisible("order.item.configuration", itemKey)) { %>
                         <%@ include file="/ecombase/orderitemconfiginfo.inc.jsp" %>
                      <% } %>
                         <%-- Shipto --%>
                         <tr>
                           <% if (ui.isOciTransfer() || !ui.isElementVisible("order.item.deliverTo", itemKey)) { %>
                                <td class="identifier" colspan="<%= ui.getNonCampItemDetailColspan() + 1 %>">
                                    <input type="hidden" id="customer[<%= ui.line %>]" name="customer[<%= ui.line %>]"/>
                                </td>
                           <% } else { %>
							    <td class="identifier">
								   <label for="customer[<%= ui.line %>]"><isa:translate key="b2b.order.display.soldto"/></label>
								</td>
								<% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
								   <td class="campimg-1"></td>
								<% } %>
                                <% if (ui.isElementEnabled("order.item.deliverTo", itemKey)) { %>
                                   
                                  <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
                                        <select  id="customer[<%= ui.line %>]" name="customer[<%= ui.line %>]" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.shipto.title"/>" <% } %> <%= (item.isItemUsageBOM())?"disabled='true'":"" %>>
                                            <isa:iterate id="shipTo"
                                               name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
                                               type="com.sap.isa.businessobject.ShipTo">
                                            <option <%= ui.getItemShipToSelected(shipTo)%> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
                                            </isa:iterate>
                                        </select>&nbsp;
                                        <a class="icon" href="#" onclick="showShipTo(document.order_positions['customer[<%= ui.line %>]'].selectedIndex);" >
                                            <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" class="display-image"/></a>&nbsp;
                                     <% if (!item.isItemUsageBOM()) { %>
                                        <a class="icon" href="#" onclick="newShipToItem('<%= item.getTechKey() %>');">
                                            <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_new_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.icon.newshipto"/> <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" class="display-image"/></a><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="35" height="1" alt=""/>
                                     <% } %>
                                     <%//Multiple Shipto functionality available for ERP edition and for
			                           //GRID products only
                                       if ( ui.isBackendR3() &&
                                           (item.getConfigType() != null &&
							   	            item.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)) ){%>
                            	             <a href="#" class="icon" onclick="multipleShipTo('<%= item.getTechKey() %>');">
                               	                 <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_catalog_external.gif") %>" alt="<isa:translate key="b2b.order.icon.multishipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
                               	             </a>
                                    <% } %>
                                  </td>
                                <% } else { %>
                                    <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
                                    <% if (item.getShipTo() != null) { %>							            
                                        <input type="hidden" id="customer[<%= ui.line %>]" name="customer[<%= ui.line %>]" value="<%= item.getShipTo().getTechKey() %>"/> <%= JspUtil.encodeHtml(item.getShipTo().getShortAddress()) %> &nbsp;
                                          <a class="icon" href="#" onclick="showShipToWithTechKey('<%= item.getShipTo().getTechKey() %>');" >
                                             <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/> <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" class="display-image"/></a>&nbsp;
                                    <% } %>
                                    </td>
                                <% } %>
                           <% } %>
                         </tr>
                         <%-- Delivery Priority --%>
                         <%@ include file="/b2b/deliveryPriorityItem.inc.jsp" %>
                      <%--  Request Delivery Date --%>
                      <tr>
                         <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey)) { %>
                           <% if ( ! ui.header.isDocumentTypeOrderTemplate() && ! ui.header.isDocumentTypeQuotation()) { %>
                                 <td class="identifier"> <label for="reqdeliverydate[<%= ui.line %>]"><isa:translate key="b2b.order.reqdeliverydate"/></label></td>
                                 <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			                     <td class="campimg-1"></td>
			                     <% } %> 
                                 <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
                                   <% if (ui.isElementEnabled("order.item.reqDeliveryDate", itemKey))  { %>
                                        <input id="reqdeliverydate[<%= ui.line %>]" name="reqdeliverydate[<%= ui.line %>]"
                                             <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.reqdeldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
                                             type="text" class="textinput-middle" size ="15"
                                             value="<%= JspUtil.encodeHtml(item.getReqDeliveryDate()) %>"
                                             <%= (item.isItemUsageBOM())?"disabled='true'":"" %>/>
                                        <% if (!ui.isAccessible) { %>
                                           <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'reqdeliverydate[<%= ui.line %>]');">
		                                      <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
							               </a>
                                        <% } %>
                                        <%//Multiple Requested Delv date functionality available for ERP edition 
										  //and for GRID products only
			  				 			  if ( ui.isBackendR3() &&
                                  			  (item.getConfigType() != null &&
						  	   	       		   item.getConfigType().equals(ItemSalesDoc.ITEM_CONFIGTYPE_GRID)) &&
						  		               ui.itemHierarchy.hasSubItems(item)) {%>
                           	         			<a href="#" class="icon" onclick="multipleReqDlvDate('<%= item.getTechKey() %>');">
                               	       				<img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/cua_info.gif") %>" alt="<isa:translate key="b2b.order.icon.multireqdate"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
                                     			</a>
                            			<%}%>
                                   <% }
                                    else
                                      { %>
                                          <%= JspUtil.replaceSpecialCharacters(item.getReqDeliveryDate()) %>
                                          <input id="reqdeliverydate[<%= ui.line %>]" name="reqdeliverydate[<%= ui.line %>]"
                                                 <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.reqdeldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
                                                 type="hidden"
                                                 value="<%= JspUtil.encodeHtml(item.getReqDeliveryDate()) %>" />
                                   <% } %>

                                 </td>
                             <% }  %>
                           <% } %>
                         </tr>
                      <%-- Item Cancel date --%>
                      <% if (ui.isElementVisible("order.latestDeliveryDate")){ %>
                      <tr>
                           <% if ( ! ui.header.isDocumentTypeOrderTemplate() && ! ui.header.isDocumentTypeQuotation()) { %>
                                 <td class="identifier"> <label for="cancel_date_4"><isa:translate key="b2b.order.grid.canceldate"/></label></td>
                                 <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			                     <td class="campimg-1"></td>
			                     <% } %> 
                                 <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
                                   <% if (ui.isElementEnabled("order.latestDeliveryDate")) { %>
                                        <input id="latestdlvdate[<%= ui.line %>]" name="latestdlvdate[<%= ui.line %>]"
                                           <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.canceldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" 
                                           <% } %>
                                           type="text" class="textinput-middle" size ="15" value="<%= JspUtil.encodeHtml(item.getLatestDlvDate()) %>"
                                           <%= (item.isItemUsageBOM())? "disabled='true'" : "" %>/>
                                        <% if (!ui.isAccessible) { %>
                                           <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'latestdlvdate[<%= ui.line %>]');">
		                                       <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
							               </a>
                                        <% } %>
                                   <% }
                                      else
                                      { %>
                                         <%= JspUtil.encodeHtml(item.getLatestDlvDate()) %>
                                         <input id="latestdlvdate[<%= ui.line %>]" name="latestdlvdate[<%= ui.line %>]"
                                                                                        <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.canceldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
                                                type="hidden"
                                                value="<%= JspUtil.encodeHtml(item.getLatestDlvDate()) %>" />
                                   <% } %>
                                 </td>
                           <% }  %>
                         </tr>
                         <% } %>
                         <%-- external reference object --%>
                         <%@ include file="/b2b/extrefobjectitem.inc.jsp" %>   
                         <%-- external reference numbers --%>
                         <%@ include file="/b2b/extrefnumberitem.inc.jsp" %>                                                                                
                         <%-- campaigns --%>
                         <%@ include file="/b2b/campaignsItem.inc.jsp" %>
                         <%-- payment terms --%>
                         <% if (ui.showItemPaymentTerms() && ui.isElementVisible("order.item.paymentterms", itemKey)) { %>
			               <tr>
			            	  <td class="identifier" scope="row"><isa:translate key="b2b.order.disp.paymentterms"/></td>
			            	  <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
                              <td class="campimg-1"></td>
			                  <% } %> 
			            	  <td class="value" colspan="3"><%=JspUtil.encodeHtml(item.getPaymentTermsDesc()) %></td>
			               </tr>
			             <% } %>                                 
                         <%-- notes --%>
                         <% if (ui.isElementVisible("order.item.comment", itemKey) && !ui.isHeaderChangeOnly() ) { %>
                           <tr class="message-data">
                            <td class="identifier">
                                <isa:translate key="b2b.order.details.note"/>
                            </td>
                            <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			                <td class="campimg-1"></td>
			                <% } %>
                            <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
                               <% if (ui.isElementEnabled("order.item.comment", itemKey)) { %>	                            
	                                <textarea id="comment[<%= ui.line %>]" name="comment[<%= ui.line %>]" rows="2" cols="80" ><%= JspUtil.encodeHtml(item.getText().getText(), new char[] {'\n'}) %></textarea>           
                               <% } else { %>
                                    <%= JspUtil.encodeHtml(item.getText().getText(), new char[] {'\n'}) %>
                                    <input type="hidden" name="comment[<%= ui.line %>]" value="<%= item.getText() != null ? JspUtil.encodeHtml(item.getText().getText(), new char[] {'\n'}) : ""%>"/>
                               <% } %>
                            </td>
                          </tr>
                         <% } %>
                    <%-- hidden fields for batches - only when batch flag on shop level is enabled --%>
                    <isacore:ifShopProperty property = "batchAvailable" value = "true">
                    <%--*******************************************************************************************************************
                    **********************************       Start of Batch Input - Item Details      *************************************
                    ********************************************************************************************************************--%>
                    <% if(item.getBatchDedicated()){ %>
                      <tr>
                        <input type="hidden" name="batchDedicated[<%= ui.line %>]" value="<%= item.getBatchDedicated() %>"/>
                        <td class="identifier" colspan="<%= ui.getNonCampItemDetailColspan() + 2 %>"><isa:translate key="b2b.order.details.batch"/></td>
                      </tr>
                      <tr>
                         <% if (ui.isElementVisible("order.item.batchId", itemKey)) { %>                     
	                         <td class="identifier">
	                            <label for="batchID[<%= ui.line %>]"><isa:translate key="b2b.order.details.batch.number"/></label>
	                          </td>
	                          <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
			                  <td class="campimg-1"></td>
			                  <% } %> 
	                          <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
	                             <input class="textinput-small" name="batchID[<%= ui.line %>]" id="batchID[<%= ui.line %>]" size="12" value="<%= JspUtil.encodeHtml(item.getBatchID()) %>"/>
	                              <% if(item.getBatchOptionNum() != 0) {%>
	                                  <select size="1" style="border:0px" name="batchOption[<%= ui.line %>]" onchange='javascript:document.forms["order_positions"].elements["batchID[<%= ui.line%>]"].value = this.value; this.value=""'>
	                                                  <option value=""><isa:translate key="b2b.order.details.batch.list"/></option>
	                                          <% for(int i=0; i < item.getBatchOptionNum(); i++){ %>
	                                                  <option value="<%= JspUtil.encodeHtml(item.getBatchOption(i)) %>"><%= JspUtil.encodeHtml(item.getBatchOption(i)) %> - <%= JspUtil.encodeHtml(item.getBatchOptionTxt(i)) %></option>
	                                          <% } %>
	                                  </select>
	                              <% } %>
	                          </td>
	                       <% } %>
                      </tr>
                      <tr>
                         <% if (ui.isElementVisible("order.item.batchChar", itemKey)) { %>
	                         <td class="identifier" colspan="<%= ui.getNonCampItemDetailColspan() + 2 %>">
	                             <% if(item.getBatchClassAssigned()) {%>
	                                   <input type="hidden" name="batchClassAssigned[<%= ui.line %>]" value="<%= item.getBatchClassAssigned() %>"/>
	                                   <input type="hidden" name="batchSelected[<%= ui.line %>]" value=""/>
	                                   <a href="#" onClick='javascript:submit_batch_selection("<%= item.getTechKey().getIdAsString() %>", "<%= item.getProductId().getIdAsString() %>", "<%= JspUtil.encodeHtml(item.getProduct()) %>", "batchSelected[<%= ui.line %>]")'>
	                                       <isa:translate key="b2b.order.details.batch.values"/>
	                                   </a>
	                                   <%if( item.getBatchCharListJSP().size() == 0){%>
	                                       [<isa:translate key="b2b.order.dt.batch.values.false"/>]
	                                   <%}
	                                    else{%>
	                                       [<isa:translate key="b2b.order.dt.batch.values.true"/>]
	                                   <%}%>
	                              <%-- Batch element info start --%>
	                                   <input type="hidden" name="elementCount[<%= ui.line %>]" value="<%= item.getBatchCharListJSP().size() %>"/>
	                                   <%for(int i=0; i<item.getBatchCharListJSP().size(); i++){%>
	                                       <input type="hidden" name="elementName[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharName()) %>"/>
	                                       <input type="hidden" name="elementTxt[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharTxt()) %>"/>
	                                       <input type="hidden" name="elementAddVal[<%= ui.batchCountTotal %>]" value="<%= item.getBatchCharListJSP().get(i).getCharAddVal() %>"/>
	                                       <input type="hidden" name="elementUnit[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharUnit()) %>"/>
	                                       <input type="hidden" name="elementUnitTExt[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharUnitTExt()) %>"/>
	                                       <input type="hidden" name="elementDataType[<%= ui.batchCountTotal %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharDataType()) %>"/>
	                                       <input type="hidden" name="elementCountCBs[<%= ui.batchCountTotal %>]" value="<%= item.getBatchCharListJSP().get(i).getCharValTxtNum() %>"/>
	                                       <%for(int j=0; j<item.getBatchCharListJSP().get(i).getCharValTxtNum(); j++){%>
	                                          <input type="hidden" name="elementCharVal[<%= ui.batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharVal(j))%>"/>
	                                          <input type="hidden" name="elementCharValTxt[<%= ui.batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharValTxt(j))%>"/>
	                                          <input type="hidden" name="elementCharValMax[<%=ui.batchCountCheckBox %>]" value="<%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharValMax(j))%>"/>
	                                          <%ui.batchCountCheckBox++;%>
	                                   <%}%>
	
	                                  <%if( item.getBatchCharListJSP().get(i).getCharAddVal() == true || (item.getBatchCharListJSP().get(i).getCharValTxtNum() == 0 && item.getBatchCharListJSP().get(i).getCharDataType().equals("CHAR"))){%>
	                                          <input type="hidden" name="characteristicAddVal[<%= ui.batchCountTotal %>]" value=""/>
	                                  <%}%>
	
	                                  <%if(item.getBatchCharListJSP().get(i).getCharValTxtNum() == 0 && item.getBatchCharListJSP().get(i).getCharDataType().equals("NUM")){%>
	                                          <input type="text" class="textinput-small" name="characteristicFrom[<%= ui.batchCountTotal %>]" value=""/>
	                                          <input type="text" class="textInput-small" name="characteristicTo[<%= ui.batchCountTotal %>]"  value=""/>
	                                  <%}%>
	                                  <%ui.batchCountTotal++;%>
	                                                          <%}%>
	                             <%-- Batch element info end --%>
	
	                            <% } %>
	                            </td>
                            <% } %>
                      </tr>
                      <% } %>

                      <%--*******************************************************************************************************************
                          ***********************************       End of Batch Input - Item Details      **************************************
                          ********************************************************************************************************************--%>
                      </isacore:ifShopProperty>

                       </table>
                     </td>
                  </tr>
                  <%--   messages --%>
                  <%@ include file="/b2b/itemErrMsg.inc.jsp" %>
          <% } %>  <%-- ui.showItemDetailButton()) --%>
          <% } %>  <%-- ui.isBOMSubItemToBeSuppressed() --%>
          <%-- ********** Seperator between two items ********** --%>
          <tr>
            <td colspan="<%= ui.getItemsColspan() %>" class="separator"></td>
          </tr>
     </isa:iterate>
     <% } %> <%-- ui.showItems() --%>

     <%-- Add some empty lines for additional basket-data --%>
     <% if (!ui.isServiceRecall() && !ui.isHeaderChangeOnly()) {
          for (int i = 0; i < ui.newpos; i++) {
              ui.setEmptyItem(); %>
         <tr class="<%= ui.even_odd %>" id="row_<%= ui.line %>">
             <td class="opener">
                  <%  if (ui.isToggleSupported()) {  %>
                  <%-- do not insert "details symbol" for configurable items on sub-levels --%>
                       <a class="icon" href='javascript: toggle("rowdetail_<%= ui.line %>")'>
                            <img id="rowdetail_<%= ui.line %>close" style="DISPLAY: none"
                               src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>"
                               alt="<isa:translate key="b2b.order.details.item.close" />"
                               width="16" height="16"/>
                            <img id="rowdetail_<%= ui.line %>open" style="DISPLAY: inline"
                               src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>"
                               alt="<isa:translate key="b2b.order.details.item.open" />"
                               width="16" height="16"/>
                       </a>
                  <% } else {%>
                      &nbsp;
                  <% } %>
             </td>
             <td class="item">
                 &nbsp;
             </td>
             <td class="product">
                 <% if ( ui.isElementVisible("order.item.product") && ui.isElementEnabled("order.item.product")) { %>
	                 <input type="text" class="textinput-middle" size="10" name="product[<%= ui.line %>]" id="product[<%= ui.line %>]" value=""/>
	                 <% if (ui.isProductValuesSearchAvailable()) { %>
	                       <%= HelpValuesSearchUI.getJSCallPopupCoding("Product","order_positions",""+ui.line,pageContext) %>
	                 <% } %>
	              <% } %>
             </td>
             <td class="qty">
                 <% if (ui.isElementVisible("order.item.qty") && ui.isElementEnabled("order.item.qty")) { %>
                    <input type="text" class="textinput-small" size="5" name="quantity[<%= ui.line %>]" maxlength="8" value=""/>
                 <% } %>  
             </td>
             <td class="unit">
                 <% if (ui.isElementVisible("order.item.unit") && ui.isElementEnabled("order.item.unit")) { %>
                    <input type="text" class="textinput-small" name="unit[<%= ui.line %>]" value=""/>
                 <% } %>   
             </td>
             <td class="desc">
                 &nbsp;
             </td>
             <% if (ui.isContractInfoAvailable()) { %>
                 <td class="ref-doc">
                    <%= "" %>
                 </td>
             <% } %>
             <% // Note 1007752 Order_change.jsp colspan for empty rows not correct
             //if (!ui.isLateDecision && ! ui.header.isDocumentTypeOrderTemplate() && ! ui.header.isDocumentTypeQuotation()) { %>
             <% if (! ui.header.isDocumentTypeOrderTemplate() && ! ui.header.isDocumentTypeQuotation()) { %>
                 <td class="qty-avail">
                    &nbsp;
                 </td>
                 <td class="date-on">
                    &nbsp;
                 </td>
             <% } %>
             <td class="price" align="right">
                 &nbsp;
             </td>
             <% if ( ! ui.header.isDocumentTypeOrderTemplate() && ! ui.header.isDocumentTypeQuotation()) {%>
                 <% if (ui.isHomActivated()) { %>
                     <td class="ref-doc">&nbsp;
                     </td>
                 <% } else { %>
                     <td class="qty-avail">&nbsp;
                     </td>
                     <td class="qty-rest">&nbsp;
                     </td>
                 <% } %>
                 <td class="status">&nbsp;
                 </td>
             <% } %>
             <td class="delete">
                 &nbsp;
             </td>

         </tr>
         <%-- item detail --%>

       <% tag_style_detail = ui.even_odd + "-detail"; %>
         <tr id="rowdetail_<%= ui.line %>" class="<%= tag_style_detail %>" <% if (ui.isToggleSupported()) { %> style="display:none;"> <% } else { %> > <% } %>
            <td class="select">&nbsp;</td>
            <td class="detail" colspan="<%= ui.getItemsColspan() %>" >
            <%-- Details --%>
            <table class="item-detail">
             <%-- Shipto --%>
             <tr>
               <% if (ui.isOciTransfer() || !ui.isElementVisible("order.item.deliverTo")) { %>
                    <td class="identifier" colspan="<%= ui.getNonCampItemDetailColspan() + 1 %>">
                        <input type="hidden" id="customer[<%= ui.line %>]" name="customer[<%= ui.line %>]"/>
                    </td>
               <% } else { %>
				    <td class="identifier">
					   <label for="customer[<%= ui.line %>]"><isa:translate key="b2b.order.display.soldto"/></label>
					</td>
					<% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
					   <td class="campimg-1"></td>
					<% } %>
                    <% if (ui.isElementEnabled("order.item.deliverTo")) { %>                       
                      <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
                            <select  id="customer[<%= ui.line %>]" name="customer[<%= ui.line %>]" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.shipto.title.emp"/>" <% } %>>
                                <isa:iterate id="shipTo"
                                   name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
                                   type="com.sap.isa.businessobject.ShipTo">
                                <option <%= ui.getShipToSelected(shipTo)%> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
                                </isa:iterate>
                            </select>&nbsp;
                            <a class="icon" href="#" onclick="showShipTo(document.order_positions['customer[<%= ui.line %>]'].selectedIndex);" >
                                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" class="display-image"/></a>&nbsp;                         
                         
                      </td>
                    <% } else { %>
                       <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>">
					       <%= ui.header.getShipTo().getShortAddress() %>
                           <a href="#" class="icon" onclick="showShipToWithTechKey('<%= ui.header.getShipTo().getTechKey() %>');" >
	                           <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/><% if (ui.isAccessible) { %><isa:translate key="b2b.acc.open.window"/> <% } %>" />
	                       </a>	
                       </td>
                    <% } %>
               <% } %>
             </tr>
               <%-- Delivery Priority --%>
			       <%@ include file="/b2b/deliveryPriorityNewItem.inc.jsp" %>
			   <%-- Request Delivery Date --%>
               <% if (ui.isElementVisible ("order.item.reqDeliveryDate") && ui.isElementEnabled ("order.item.reqDeliveryDate")) { %>
	               <tr>
	                   <% if ( ! ui.header.isDocumentTypeOrderTemplate() && ! ui.header.isDocumentTypeQuotation()) { %>
	                        <td class="identifier"> <label for="reqdeliverydate[<%= ui.line %>]"><isa:translate key="b2b.order.reqdeliverydate"/></label></td>
                       <% if (ui.isMultipleCampaignsAllowed()) { %>
			                <td class="campimg-1"></td>
			           <% } %> 
	                        <td class="value">
	                            <input id="reqdeliverydate[<%= ui.line %>]" name="reqdeliverydate[<%= ui.line %>]"
	                                <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.reqdeldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
	                                   type="text" class="textinput-middle" size ="15"
	                            />
	                            <% if (!ui.isAccessible) { %>
	                                <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'reqdeliverydate[<%= ui.line %>]');">
			                            <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
								    </a>
	                            <% } %>
	                        </td>
	                   <% }  %>
	               </tr>
	           <% } %>
               <%-- Item Cancel Date for empty lines--%>
               <% if (ui.isElementVisible("order.latestDeliveryDate") && ui.isElementEnabled("order.latestDeliveryDate")){ %>
               <tr>
                   <% if ( ! ui.header.isDocumentTypeOrderTemplate() && ! ui.header.isDocumentTypeQuotation()) { %>
                        <td class="identifier"> <label for="cancel_date_4"><isa:translate key="b2b.order.grid.canceldate"/></label></td>
                       <% if (ui.isMultipleCampaignsAllowed()) { %>
			                <td class="campimg-1"></td>
			           <% } %> 
                        <td class="value">
                            <input id="latestdlvdate[<%= ui.line %>]" name="latestdlvdate[<%= ui.line %>]"
                                <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.canceldathead.form" arg0="<%= ui.getShop().getDateFormat() %>"/>" <% } %>
                                   type="text" class="textinput-middle" size ="15"
                            />
                            <% if (!ui.isAccessible) { %>
                                <a href="#" onClick="setDateField(document.order_positions['latestdlvdate[<%= ui.line %>]']);setDateFormat('<%=ui.getDateFormat()%>');isaTop().newWin = window.open('<isa:webappsURL name="b2b/calendar.jsp"/>','cal','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes'); isaTop().newWin.focus();">
                                           <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>" class="display-image"/>
                                </a>
                            <% } %>
                        </td>
                   <% }  %>
               </tr>
               <%}%>
               <%-- external reference numbers --%>
                    <%@ include file="/b2b/extrefnumbernewitem.inc.jsp" %>
               <%-- campaigns --%>
                   <%@ include file="/b2b/campaignsNewItem.inc.jsp" %> 
               <%-- notes --%>
               <% if (ui.isElementEnabled("order.item.comment") && ui.isElementEnabled("order.item.comment")) { %>
	               <tr class="message-data">
	                   <td class="identifier">
	                       <isa:translate key="b2b.order.details.note"/>
	                   </td>
	                <% if (ui.isMultipleCampaignsAllowed())  { %>
			           <td class="campimg-1"></td>
			        <% } %> 
	                   <td class="value">
	                         <textarea id="comment[<%= ui.line %>]" name="comment[<%= ui.line %>]" rows="2" cols="80" ></textarea>
	                   </td>
	                </tr>
	            <% } %>
           </table>
           <% if (!ui.isTransferCatalogKey()) { %>
                 <input type="hidden" name="pcat[<%= ui.line %>]"  value="<%= JspUtil.encodeHtml(ui.getCatalog()) %>"/>
           <% }
              if ((!ui.isOrderingOfNonCatalogProductsAllowed()) && (ui.isItemMainItem() ) ) { %>
                 <input type="hidden" name="pcat[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(ui.getShop().getCatalog()) %>">
           <% } %>
           </td>
         </tr>
         <%-- ********** Seperator between two items ********** --%>
         <tr>
            <td colspan="<%= ui.getItemsColspan() %>" class="separator"></td>
         </tr>
       <% } /* end for */
       } %>
       <tr>
       <td>
       <input type="hidden" name="sendpressed" value=""/>
       <input type="hidden" name="orderpressed" value=""/>

       <input type="hidden" name="cancelpressed" value=""/>
       <input type="hidden" name="showproduct" value=""/>
       <input type="hidden" name="actualpos" value="1"/>
       <input type="hidden" name="refresh" value=""/>
       <input type="hidden" name="newpos" value=""/>
       <input type="hidden" name="newshipto" value=""/>
       <input type="hidden" name="configitemid" value=""/>

	   <%-- hidden fields for grid configuratoin --%>
       <input type="hidden" name="configtype" value=""/>
       <input type="hidden" name="multipleshipto" value=""/>
       <input type="hidden" name="multiplereqdlvdate" value=""/>  
       
        <%-- Hidden field optimization --%>
        <%-- hidden fields for batches - only when batch product exists --%>
        <isacore:ifShopProperty property = "batchAvailable" value = "true">
          <input type="hidden" name="batchselection" value=""/>
          <input type="hidden" name="batchproductid" value=""/>
          <input type="hidden" name="batchproduct" value=""/>
        </isacore:ifShopProperty>

        <%-- hidden fields for UpSelling - only when UpSelling activated --%>
        <isacore:ifShopProperty property = "cuaAvailable" value = "true">
          <input type="hidden" name="cuaproductkey" value=""/>
          <input type="hidden" name="cuareplaceditem" value=""/>
          <input type="hidden" name="replaceditem" value=""/>
          <input type="hidden" name="cuadocumenttype" value=""/>
          <input type="hidden" name="replacewithproduct" value=""/>
          <input type="hidden" name="replacewithunit" value=""/>
          <input type="hidden" name="replacewithqunatity" value=""/>
          <input type="hidden" name="replacetype" value=""/>
        </isacore:ifShopProperty>

        </td>
        </tr>
        
        </table>
       </div><%-- document-items --%>
        </form>
    </div><%-- document --%>
 <%-- Buttons --%>
   <div id="buttons">
       <%-- Accesskey for the button section. It must be an resourcekey, so that it can be translated. --%>
       <a id="access-buttons" href="#access-header" title="<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>

      <ul class="buttons-1">
      <% if (!ui.isAuction() && !ui.isServiceRecall()) {
             if (!ui.isHeaderChangeOnly()) { %>
            <li>
                <select id="newposcount" size="1" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.newpos.qt"/>" <% } %> >
                  <option <%= ui.getNewPosSelected(5) %> value="5">5 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(10) %> value="10">10 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(15) %> value="15">15 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(20) %> value="20">20 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(25) %> value="25">25 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(30) %> value="30">30 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(40) %> value="40">40 <isa:translate key="b2b.newpos"/></option>
                  <option <%= ui.getNewPosSelected(50) %> value="50">50 <isa:translate key="b2b.newpos"/></option>
                </select>
            <% if (!ui.isAccessible) { %>
                <img class="icon" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.acc.buttons.newpos.qt"/>"/>
            <% } %>
            </li>
           <% } 
          } %>
          <li><a href="#" onclick="submit_refresh();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.upd"/>" <% } %> ><isa:translate key="b2b.order.display.submit.update"/></a></li>
      </ul>
      <ul class="buttons-3">
      <% if (!ui.isAuction()) { %> 
         <li><a href="#" onclick="cancel_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cncl"/>" <% } %>><isa:translate key="b2b.order.display.submit.close"/></a></li>
         <li><a href="#" <% if(ui.isHeaderChangeOnly()) {%>  onclick="send_confirm_header_only();" <% } else { %>  onclick="send_confirm();" <% } %> <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.save.ord"/>" <% } %> ><isa:translate key="b2b.order.change.submit.save"/></a></li>
         <%
      } else { %>
         <li><a href="#" onclick="cancel_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cncl"/>" <% } %>><isa:translate key="b2b.order.display.submit.cancel"/></a></li>
         <li><a href="#" <% if(ui.isHeaderChangeOnly()) {%>  onclick="send_confirm_header_only();" <% } else { %>  onclick="send_confirm();" <% } %> <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.save.ord"/>" <% } %> ><isa:translate key="b2b.order.change.submit.save"/></a></li>
      <%
      } %>      
         <%
         if (ui.isCollectiveOrder()) {%>
         <li><a href="#" onclick="order_confirm();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.collorder.send"/>" <% } %> ><isa:translate key="b2b.collorder.send"/></a></li>
         <%
         }%>
      </ul>
    </div>

    <%-- End  Buttons --%>
   <script type="text/javascript">
     headerReqDelDateOld = '<%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate())%>';
   </script>
   </body>
</html>

