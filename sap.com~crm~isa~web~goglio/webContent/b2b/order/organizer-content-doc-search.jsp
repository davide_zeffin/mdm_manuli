<%--
********************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      08.5.2001

    $Revision: #5 $
    $Date: 2002/06/10 $
********************************************************************************

 IMPORTANT:
       - Page only accessible by using 'b2b/b2b/documentlistselect.do'

--%>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.ContextConst" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.orderdownload.*" %>
<%@ page import="com.sap.isa.core.interaction.InteractionConfig" %>
<%@ page import="com.sap.isa.core.interaction.InteractionConfigContainer" %>

<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>
<%@ taglib uri="/isa" prefix="isa" %>


<% DocumentListSelectorForm form =
    (DocumentListSelectorForm)pageContext.findAttribute("DocumentListSelectorForm");
%>
<% String docCount = (String)request.getAttribute(DocumentListAction.RK_DOCUMENT_COUNT);
   String showRemoveMessage = (String)request.getAttribute(SalesDocumentRemoveAction.RK_REMOVEMESSGAGEFLAG);
   String doclistreload = (String)request.getAttribute(SalesDocumentRemoveAction.RK_DOCLISTRELOADFLAG);
   String removedobjectid = (String)request.getAttribute(SalesDocumentRemoveAction.RK_REMOVED_OBJECT);
   Shop shop = (Shop)request.getAttribute(DocumentListAction.RK_DLA_SHOP);

   // Determine selected document status index
   int documentStatusSelectedIndex = 0;
   if ( form.isDocumentStatus(DocumentListSelectorForm.OPEN)) {
       documentStatusSelectedIndex = 1;
   }
   if (shop.isQuotationExtended() &&
       form.getDocumentType().equals(DocumentListSelectorForm.QUOTATION)) {
       if (form.isDocumentStatus(DocumentListSelectorForm.RELEASED)) {
           documentStatusSelectedIndex = 2;
       }
       else if (form.isDocumentStatus(DocumentListSelectorForm.ACCEPTED)) {
           documentStatusSelectedIndex = 3;
       }
       else if (form.isDocumentStatus(DocumentListSelectorForm.COMPLETED)) {
           documentStatusSelectedIndex = 4;
       }
   }
   else if ( form.isDocumentStatus(DocumentListSelectorForm.COMPLETED)) {
       documentStatusSelectedIndex = 2;
   }

   boolean calledFromPortal = (((IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).getPortal().equals("YES") ? true : false);

   boolean backendR3ButNotPI = shop.getBackend().equals(Shop.BACKEND_R3) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_40) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_45) ||
                       shop.getBackend().equals(Shop.BACKEND_R3_46);

   boolean backendR3 = backendR3ButNotPI ||
                       shop.getBackend().equals(Shop.BACKEND_R3_PI);
   boolean isOrderDownloadRequired =  false;
   InteractionConfigContainer icc =  (InteractionConfigContainer)(userSessionData.getAttribute(SessionConst.INTERACTION_CONFIG));
   InteractionConfig uiInteractionConfig = icc.getConfig("ui");
   if (icc!=null && "true".equalsIgnoreCase(uiInteractionConfig.getValue("enable.orderdownload"))) {
       isOrderDownloadRequired = true;
   }
   if(isOrderDownloadRequired) {
    isOrderDownloadRequired = form.getDocumentType().equals(DocumentListSelectorForm.ORDER) &&
          docCount!=null && !docCount.equals("0");
   }
   Calendar calendar = new GregorianCalendar();
   int actYear = calendar.get(Calendar.YEAR);
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Document List</title>
  <isa:includes/>
  <!--
  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css")%>" type="text/css" rel="stylesheet">
  -->
  <script type="text/javascript">
    var documentStatusSelectedIndex = <%=documentStatusSelectedIndex%>;
    var IE4 = IE5 = NS4 = NS6 = false;
    var IE4 = (document.all && !document.getElementById) ? true : false;
    var IE5 = (document.all && document.getElementById) ? true : false;
    var NS4 = (document.layers) ? true : false;
    var NS6 = (document.getElementById && !document.all) ? true : false;

    function setDocumentStatusIndex() {
      documentStatusSelectedIndex = document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.selectedIndex;
    }
    function loadContract(documentType) {
        if (documentType == "<%=DocumentListSelectorForm.CONTRACT%>"   ||
            documentType == "<%=DocumentListSelectorForm.INVOICE%>"    ||
            documentType == "<%=DocumentListSelectorForm.CREDITMEMO%>" ||
            documentType == "<%=DocumentListSelectorForm.DOWNPAYMENT%>" ) {

            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE_VALUE %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE_VALUE %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.style.backgroundColor = "silver";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.ocdsbutton.disabled = true;
            if (documentType == "<%=DocumentListSelectorForm.CONTRACT%>") {
                document.location.href="<isa:webappsURL name="/b2b/contractsearch.do"/>";
            } else {
                document.location.href="<isa:webappsURL name="/b2b/billinglistselect.do"/>?<%= DocumentListSelectorForm.DOCUMENT_TYPE %>=" + documentType + "&<%=DocumentListSelectorForm.ATTRIBUTE%>=<%=DocumentListSelectorForm.NOT_SPECIFIED%>&<%=DocumentListBaseAction.RP_NO_SEARCH_RESULT%>=true";
            }

            <% if (shop.isQuotationExtended()) { %>
        } else if (documentType == "<%=DocumentListSelectorForm.QUOTATION%>" &&
                                 documentType != "<%=form.getDocumentType()%>" ||
                                 documentType != "<%=DocumentListSelectorForm.QUOTATION%>" &&
                                 "<%=form.getDocumentType()%>" == "<%=DocumentListSelectorForm.QUOTATION%>") {
                    document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.disabled = true;
                    if(!NS4){ document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.style.backgroundColor = "silver"; }
                    document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.disabled = true;
                    if(!NS4){ document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.style.backgroundColor = "silver"; }
                    document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>.disabled = true;
                    if(!NS4){ document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>.style.backgroundColor = "silver"; }
                    document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE_VALUE %>.disabled = true;
                    if(!NS4){ document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE_VALUE %>.style.backgroundColor = "silver"; }
                    document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.disabled = true;
                    if(!NS4){ document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.style.backgroundColor = "silver"; }
                    document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.disabled = true;
                    if(!NS4){ document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.style.backgroundColor = "silver"; }
                    document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.disabled = true;
                    if(!NS4){ document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.style.backgroundColor = "silver"; }
                    document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.ocdsbutton.disabled = true;
                    document.location.href="<isa:webappsURL name="/b2b/documentlistselect.do"/>?<%= DocumentListSelectorForm.DOCUMENT_TYPE %>=" + documentType + "&<%=DocumentListSelectorForm.ATTRIBUTE%>=<%=DocumentListSelectorForm.NOT_SPECIFIED%>&<%=DocumentListBaseAction.RP_NO_SEARCH_RESULT%>=true";
            <% } %>
        } else if (documentType == "<%=DocumentListSelectorForm.AUCTION%>") {
            document.location.href="<isa:webappsURL name="/auction/buyer/auctionmonitor.do"/>";
        } else {
            if (documentType == "<%=DocumentListSelectorForm.ORDERTEMPLATE%>") {
                document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.disabled = true;
                if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.style.backgroundColor = "silver";}
                document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.value = "";
            } else {
                document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.disabled = false;
                if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.style.backgroundColor = "white";}
                document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.DOCUMENT_STATUS %>.selectedIndex = documentStatusSelectedIndex;
            }
        }
    }
    function updateAttributeValue(attribute) {
        if (attribute == "<%=DocumentListSelectorForm.NOT_SPECIFIED%>" || attribute != "<%=form.getAttribute()%>") {
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.value = "";
        }
        if (attribute == "<%=DocumentListSelectorForm.NOT_SPECIFIED%>") {
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.disabled = true;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.style.backgroundColor = "silver";}
        }
        else {
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.disabled = false;
            if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.attributeValue.style.backgroundColor = "white";}
            document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.period.options[0].selected=true;
        }
    }
    function setPeriod() {
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.disabled = false;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.style.backgroundColor = "white";}
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.disabled = true;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.style.backgroundColor = "silver";}
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.disabled = true;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.style.backgroundColor = "silver";}
    }
    function setDate() {
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.disabled = true;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.PERIOD %>.style.backgroundColor = "silver";}
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.disabled = false;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.MONTH %>.style.backgroundColor = "white";}
        document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.disabled = false;
        if(!NS4){document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.YEAR %>.style.backgroundColor = "white";}
    }
    function initShuffler() {
        updateAttributeValue('<%=form.getAttribute()%>');
        <%=form.isValiditySelection(form.PERIOD) ? "setPeriod()" : "setDate()"%>;
        loadContract('<%=form.getDocumentType()%>');
    }
    function requestConfirmation(techkey, objecttype, removetype, objectid) {
        var confirmMsg = "";
        if (objecttype == "<%=DocumentListSelectorForm.QUOTATION%>") {
            confirmMsg = "<isa:translate key="b2b.stat.shuff.conf.remv.quot"/>";
	} else {
            confirmMsg = "<isa:translate key="b2b.stat.shuff.conf.remv.ordtmp"/>";
	}

        if ( confirm(confirmMsg) ) {
            var x="<isa:webappsURL name ="b2b/salesdocumentremove.do"/>?techkey=" + techkey + "&objecttype=" + objecttype +
	          "&removetype=" + removetype + "&object_id=" + objectid;
	    document.location.href="<isa:webappsURL name ="b2b/salesdocumentremove.do"/>?techkey=" + techkey + "&objecttype=" + objecttype +
	                           "&removetype=" + removetype + "&object_id=" + objectid;
        }
    }
    </script>
  </head>
  <body class="organizer" onLoad="initShuffler()" >
    <div id="organizer-search" class="module"></div>
    <div class="module-name"><isa:moduleName name="b2b/order/organizer-content-doc-search.jsp" /></div>

      <%-- Selection Screen Area --%>

      <!-- form action="organizer-content-doc-search_contract.html" -->
      <form name="<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>" method="get" action='<isa:webappsURL name ="b2b/documentlistselect.do"/>' >

        <%-- Because of problems with interaction between this page and the history.jsp
             the request parameter "nobean" is passed thru --%>
        <% if (request.getParameter("nobean") != null) { %>
           <input type="hidden" name="nobean" value="<%= request.getParameter("nobean") %>">
        <% } %>

        <!--fieldset>
          <legend>Suchkriterien</legend-->
          <table cellspacing="0" cellpadding="1" width="100%" border="0">
            <tbody>
              <tr>
                <td><isa:translate key="b2b.status.shuffler.key1"/></td>
                <td><select id="doctype" class="bigCatalogInput" name="<%= DocumentListSelectorForm.DOCUMENT_TYPE %>"
                            onChange="loadContract(this.form.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.options[this.form.<%= DocumentListSelectorForm.DOCUMENT_TYPE %>.options.selectedIndex].value)">
                    <% if (shop.isQuotationAllowed()) {%>
                    <option value="<%=DocumentListSelectorForm.QUOTATION%>" <%=form.isDocumentType(form.QUOTATION) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val1"/></option>
                    <% } %>
                    <option value="<%= DocumentListSelectorForm.ORDER %>"<%=form.isDocumentType(form.ORDER) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val2"/></option>
                    <% if (shop.isTemplateAllowed()) {%>
                    <option value="<%=DocumentListSelectorForm.ORDERTEMPLATE%>"<%=form.isDocumentType(form.ORDERTEMPLATE) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val3"/></option>
                    <% } %>
                    <% if (shop.isContractAllowed()) {%>
                    <option value="<%=DocumentListSelectorForm.CONTRACT%>">&nbsp;<isa:translate key="b2b.status.shuffler.key1val4"/></option>
                    <% } %>
                    <% if (shop.isBillingDocsEnabled()  &&  ! calledFromPortal) {%>
                    <option value="<%=DocumentListSelectorForm.INVOICE%>"<%=form.isDocumentType(form.INVOICE) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val5"/></option>
                    <option value="<%=DocumentListSelectorForm.CREDITMEMO%>"<%=form.isDocumentType(form.CREDITMEMO) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val6"/></option>
                    <option value="<%=DocumentListSelectorForm.DOWNPAYMENT%>"<%=form.isDocumentType(form.DOWNPAYMENT) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key1val7"/></option>
                    <% } %>
                    <% if (!backendR3 &&
                           (userSessionData.getAttribute(IsaCoreInitAction.SC_AUCTION_ENABLED) != null) &&
                           ((Boolean)userSessionData.getAttribute(IsaCoreInitAction.SC_AUCTION_ENABLED)).booleanValue()) { %>
                        <option value="<%=DocumentListSelectorForm.AUCTION%>">&nbsp;<isa:translate key="b2b.status.shuffler.key1val9"/></option>
                    <% } %>
                  </select></td>
              </tr>
              <isacore:ifShopProperty property = "soldtoSelectable" value ="true">
                <tr>
                  <td><isa:translate key="b2b.status.shuffler.key1a"/></td>
                  <td><input type="text" class="bigCatalogInput" value="<%= form.getSoldto() %>" name="<%= DocumentListSelectorForm.SOLDTO %>"></td>
                </tr>
              </isacore:ifShopProperty>
              <% if (backendR3ButNotPI) { %>
                 <input type="hidden" name="<%= DocumentListSelectorForm.DOCUMENT_STATUS %>" value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>">
              <% }
                 else { %>
              <tr id="status">
                <td><isa:translate key="b2b.status.shuffler.key2"/></td>
                <td><select class="bigCatalogInput" name="<%= DocumentListSelectorForm.DOCUMENT_STATUS %>"
                            onChange="setDocumentStatusIndex();">
                    <option value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>"<%=form.isDocumentStatus(form.NOT_SPECIFIED) ? "selected" : ""%> ><isa:translate key="b2b.status.shuffler.key2val3"/></option>
                    <% if (shop.isQuotationExtended() && form.getDocumentType().equals(DocumentListSelectorForm.QUOTATION)) {%>
                      <option value="<%=DocumentListSelectorForm.OPEN%>" <%=form.isDocumentStatus(form.OPEN) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key2val4"/></option>
                      <option value="<%=DocumentListSelectorForm.RELEASED%>" <%=form.isDocumentStatus(form.RELEASED) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key2val5"/></option>
                      <option value="<%=DocumentListSelectorForm.ACCEPTED%>" <%=form.isDocumentStatus(form.ACCEPTED) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key2val6"/></option>
                      <% if (!backendR3){ %>
                        <option value="<%=DocumentListSelectorForm.COMPLETED%>" <%=form.isDocumentStatus(form.COMPLETED) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key2val7"/></option>
                      <%}%>
                    <% } else { %>
                      <option value="<%=DocumentListSelectorForm.OPEN%>" <%=form.isDocumentStatus(form.OPEN) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key2val1"/></option>
                      <option value="<%=DocumentListSelectorForm.COMPLETED%>" <%=form.isDocumentStatus(form.COMPLETED) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key2val2"/></option>
                    <% } %>
                    <!-- <option value="status_egal">&nbsp;(ohne bes. Status)</option> -->
                  </select></td>
              </tr>
              <% } %>
              <tr id="name">
                <td><isa:translate key="b2b.status.shuffler.key3"/></td>
                <td><select class="bigCatalogInput" name="<%= DocumentListSelectorForm.ATTRIBUTE %>"
                            onChange="updateAttributeValue(document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>[document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.<%= DocumentListSelectorForm.ATTRIBUTE %>.selectedIndex].value)">
                    <option value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>" <%=form.isAttribute(form.NOT_SPECIFIED) ? "selected" : ""%> ><isa:translate key="b2b.status.shuffler.key3val1"/></option>
                    <% if (backendR3) { %>
                    <option value="<%=DocumentListSelectorForm.PURCHASEORDERNUMBER%>" <%=form.isAttribute(form.PURCHASEORDERNUMBER) ? "selected" : ""%>>&nbsp;<isa:translate key="b2b.status.shuffler.key3val2ex"/></option>
                    <% } else { %>
                    <option value="<%=DocumentListSelectorForm.DESCRIPTION%>" <%=form.isAttribute(form.DESCRIPTION) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key3val3"/></option>
                    <option value="<%=DocumentListSelectorForm.PURCHASEORDERNUMBER%>" <%=form.isAttribute(form.PURCHASEORDERNUMBER) ? "selected" : ""%>>&nbsp;<isa:translate key="b2b.status.shuffler.key3val2"/></option>
                    <option value="<%=DocumentListSelectorForm.OBJECTID%>" <%=form.isAttribute(form.OBJECTID) ? "selected" : ""%>>&nbsp;<isa:translate key="b2b.status.shuffler.key3val4"/></option>
                    <% } %>
                    <!-- <option value="merkmal_egal">&nbsp;(ohne bes. Merkmal)</option> -->
                  </select></td>
              </tr>
              <tr id="name">
                <td><isa:translate key="b2b.status.shuffler.key4"/></td>
                <td><input type="text" class="bigCatalogInput" value="<%= form.getAttributeValue() %>" name="<%= DocumentListSelectorForm.ATTRIBUTE_VALUE %>"></td>
              </tr>
              <tr id="name">
                <% if (backendR3ButNotPI) { %>
                <td><isa:translate key="b2b.status.shuffler.key5_exact"/></td>
                <% } else { %>
                <td><isa:translate key="b2b.status.shuffler.key5"/></td>
                <% } %>
              </tr>
              <tr id="zeitraum">
                <td colspan="2"><isa:translate key="b2b.status.shuffler.key6"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="radio" name="<%= DocumentListSelectorForm.VALIDITYSELECTION %>" value="<%=form.PERIOD%>"
                           onClick="javascript:setPeriod()" <%=form.isValiditySelection(form.PERIOD) ? "checked" : ""%> >&nbsp;
                    <select name="<%= DocumentListSelectorForm.PERIOD %>"  class="middleCatalogInput">
                       <option value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>" <%=form.isPeriod(form.NOT_SPECIFIED) ? "selected" : ""%> ><isa:translate key="b2b.status.shuffler.key6val1"/></option>
                       <option value="<%=DocumentListSelectorForm.TODAY%>" <%=form.isPeriod(form.TODAY) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val2"/></option>
                       <option value="<%=DocumentListSelectorForm.SINCE_YESTERDAY%>" <%=form.isPeriod(form.SINCE_YESTERDAY) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val3"/></option>
                       <option value="<%=DocumentListSelectorForm.LAST_WEEK%>" <%=form.isPeriod(form.LAST_WEEK) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val4"/></option>
                       <option value="<%=DocumentListSelectorForm.LAST_MONTH%>" <%=form.isPeriod(form.LAST_MONTH) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val5"/></option>
                       <option value="<%=DocumentListSelectorForm.LAST_YEAR%>" <%=form.isPeriod(form.LAST_YEAR) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key6val6"/></option>
                       <!-- <option value="zeitraum_egal">&nbsp;(ohne bes. Zeitraum)</option> -->
                    </select></td>
              </tr>
              <tr id="monat" >
                <td><isa:translate key="b2b.status.shuffler.key7"/></td>
                <td><input type="radio" name="<%= DocumentListSelectorForm.VALIDITYSELECTION %>" value="<%=form.MONTH%>"
                           onClick="javascript:setDate()" <%=form.isValiditySelection(form.MONTH) ? "checked" : ""%> >&nbsp;
                    <select name="<%= DocumentListSelectorForm.MONTH %>">
                       <option value="<%=DocumentListSelectorForm.NOT_SPECIFIED%>"><isa:translate key="b2b.status.shuffler.key7val1"/></option>
                       <option value="01" <%=form.isMonth("01") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val2"/></option>
                       <option value="02" <%=form.isMonth("02") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val3"/></option>
                       <option value="03" <%=form.isMonth("03") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val4"/></option>
                       <option value="04" <%=form.isMonth("04") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val5"/></option>
                       <option value="05" <%=form.isMonth("05") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val6"/></option>
                       <option value="06" <%=form.isMonth("06") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val7"/></option>
                       <option value="07" <%=form.isMonth("07") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val8"/></option>
                       <option value="08" <%=form.isMonth("08") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val9"/></option>
                       <option value="09" <%=form.isMonth("09") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val10"/></option>
                       <option value="10" <%=form.isMonth("10") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val11"/></option>
                       <option value="11" <%=form.isMonth("11") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val12"/></option>
                       <option value="12" <%=form.isMonth("12") ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.status.shuffler.key7val13"/></option>
                       <!-- <option value="00" <%=form.isMonth("00") ? "selected" : ""%> >&nbsp;<isa:translate key="status.select.nomonth"/></option> -->
                  </select>&nbsp;
                  <select name="<%= DocumentListSelectorForm.YEAR %>" value="<%= form.getYear() %>" class="yearInput" >
                       <option value="0000" <%=form.isYear("0000") ? "selected" : ""%>></option>
                       <option value="<%=actYear+1%>" <%=form.isYear(Integer.toString(actYear+1)) ? "selected" : ""%>><%=actYear+1%></option>
                       <% for (int cnt = 0; cnt < 3; cnt++) {%>
                         <option value="<%=actYear-cnt%>" <%=form.isYear(Integer.toString(actYear-cnt)) ? "selected" : ""%>><%=actYear-cnt%></option>
                       <% } %>
                  </select>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input name="ocdsbutton" type="submit" value="<isa:translate key="status.select.searching"/>" class="green"></td>
              </tr>
            </tbody>
          </table>
        <!--/fieldset-->
        <input type="hidden" name="<%= DocumentListSelectorForm.SETSALESSELECTIONSTART %>" value="true">
        <input type="hidden" name="<%= DocumentListSelectorForm.SETBILLINGSELECTIONSTART %>" value="false">
      </form>



  <%-- Document List Area --%>

  <% if ( docCount != null ) { %>

  <% String documentTypeKey = "status.sales.dt.".concat(form.getDocumentType().toLowerCase());
     if ( ! docCount.equals("1")) {
            documentTypeKey = documentTypeKey.concat("s");          // plural
     };
     String documentNameKey = "b2b.status.dn.".concat(form.getDocumentType().toLowerCase());
  %>

  <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tbody>
      <tr>
        <td colspan="2">
          <br><br>
          <var><div class="searchresult"><isa:translate key="status.select.searchresult01"/>&nbsp;&nbsp;<%= docCount %>&nbsp;<isa:translate key="<%= documentTypeKey %>"/>&nbsp;<isa:translate key="status.select.searchresult02"/></div></var>
          <br>
        </td>
      </tr>

      <% if ( ! docCount.equals("0")) { %>
      <tr>
        <td>&nbsp;</td>
        <td>
          <table class="organizer-list" border="0" cellspacing="0" cellpadding="2" width="100%">

          <%-- Tableheader --%>

          <thead>
             <tr>
               <% if ( ! form.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE) &&
                       ! backendR3ButNotPI)  { %>
               <th><isa:translate key="status.sales.status"/></th>
               <% } %>
               <th><isa:translate key="<%=documentNameKey%>"/>&nbsp;/<br><isa:translate key="status.sales.date"/></th>
               <th width="100%" <%
               if (form.getDocumentType().equals(DocumentListSelectorForm.ORDER))  { %>

               <% }
               %>><isa:translate key="status.sales.select.extOrderNr"/>
               <% if ( ! backendR3)  { %> &nbsp;/<br><isa:translate key="status.sales.select.descript"/></th>
               <% } %>
               <% if ( ! form.getDocumentType().equals(DocumentListSelectorForm.ORDER))  { %>
               <th></th>
               <% } %>
               <% if(isOrderDownloadRequired) {%>
                        <th>
                        <a href="javascript:document.orderdownloadallform.submit()">
                       <img src="<isa:webappsURL name ="/b2b/mimes/images/download.gif"/>" alt="" border="0" title="<isa:translate key="massdwnld.addallodrimg.ttp"/>"></td>
                 </a>
                </th>
                        <% } %>
             </tr>
           </thead>


          <%-- Tablerows --%>

           <% String documentStatusKey = "";
              String documentValidToKey = "";
              String documentModificationKey = "";
              String even_odd = "odd";
           %>
           <isa:iterate id="orderheader" name="<%= DocumentListAction.RK_ORDER_LIST %>"
                        type="com.sap.isa.businessobject.header.HeaderSalesDocument">
           <% if (shop.isQuotationExtended() &&
                  orderheader.isDocumentTypeQuotation() &&
                  (orderheader.getStatus().equals("completed") ||
                   orderheader.getStatus().equals("open"))) {
                  if (!orderheader.isQuotationExtended()) {
                      documentStatusKey = "status.sales.status.l.".concat(orderheader.getStatus());
                  } else {
                      documentStatusKey = "status.sales.status.x.".concat(orderheader.getStatus());
                  }
              } else {
                  documentStatusKey = "status.sales.status.".concat(orderheader.getStatus());
              } %>
           <% documentValidToKey = "status.sales.list.".concat(orderheader.getStatus()).concat(".vT"); %>
           <% String refDesSeparator = "/";
              if ( orderheader.getDescription().length() == 0
                  || orderheader.getPurchaseOrderExt().length() == 0 ) {
                refDesSeparator = "";
              } %>
           <tbody>
             <% if (even_odd.equals("odd")) {
                  even_odd = "even";
                } else {
                  even_odd = "odd";
                }
             %>
             <tr class="<%= even_odd %>">
               <% if ( ! form.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE) &&
                       ! backendR3ButNotPI)  { %>
                     <% if ( (form.getDocumentType().equals(DocumentListSelectorForm.QUOTATION)
                               && !orderheader.getValidTo().equals("00000000")
                               && ( orderheader.getStatus().equals(DocumentListSelectorForm.OPEN) && !orderheader.isQuotationExtended()
                                 || orderheader.getStatus().equals(DocumentListSelectorForm.RELEASED) && orderheader.isQuotationExtended() )
                               ) )  { %>
               <td><var><isa:translate key="<%= documentValidToKey %>"/>:<br><%= orderheader.getValidTo()%></var></td>
                     <% } else { %>
               <td><var><isa:translate key="<%= documentStatusKey %>"/></var></td>
                   <% } %>
               <% } %>
               <td><var><a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/><%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input">
                          <%= orderheader.getSalesDocNumber() %>
                        </a>&nbsp;<br>
                        <%= orderheader.getChangedAt() %>
                   </var></td>
               <td><var><a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/><%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input">
                          <%= orderheader.getPurchaseOrderExt() %>
                        </a>
                        &nbsp;<font><b><%=refDesSeparator%></b></font>
                        <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/><%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input">
                          <%= orderheader.getDescription() %>
                        </a>
                   </var></td>
               <% if(isOrderDownloadRequired) {%>
                     <td>
                       <!--
                        <input id="addordertodownloaddoc" name="addordertodownloaddocbtn" type="submit" class="green" value="download this one">
                        -->
                      <%
                      String url ="b2b/order/addOrderToMassDownloadList.do";
                      url+="?orderguid="+orderheader.getTechKey().getIdAsString()+"&operation=add&orderid="+orderheader.getSalesDocNumber()
                      +"&desc="+(orderheader.getDescription()==null?"":orderheader.getDescription())+"&status="+orderheader.getStatus();
                      %>
                      <a href="<isa:webappsURL name ="<%=url%>"/>" target="form_input">
                       <img src="<isa:webappsURL name ="/b2b/mimes/images/download.gif"/>" alt="" border="0" title="<isa:translate key="massdwnld.addorderimg.ttip"/>"></td>
                     </a>

                    </td>
                   <% } %>

               <% if (orderheader.isChangeable() && !(form.getDocumentType().equals(DocumentListSelectorForm.ORDER))) { %>
               <td>
                     <% documentModificationKey = "status.docmod.".concat(orderheader.getDocumentType()); %>
                     <% if (form.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE)) { %>
                     <a href="javascript:requestConfirmation('<%= orderheader.getTechKey() %>','<%=orderheader.getDocumentType()%>','<%=SalesDocumentRemoveAction.RK_REMOVETYPE_ORDERTEMPLATE%>','<%= orderheader.getSalesDocNumber() %>');">
                       <img src="<isa:webappsURL name ="/b2b/mimes/images/trash.gif"/>" alt="<isa:translate key="<%=documentModificationKey%>"/>" border="0"></td>
                     </a>
                     <% } %>
                     <% if ( form.getDocumentType().equals(DocumentListSelectorForm.QUOTATION) &&
                            !(orderheader.getStatus().equals(DocumentListSelectorForm.COMPLETED)) &&
                            !orderheader.isQuotationExtended() &&
                            ! backendR3ButNotPI) { %>
                     <a href="javascript:requestConfirmation('<%= orderheader.getTechKey() %>','<%=orderheader.getDocumentType()%>','<%=SalesDocumentRemoveAction.RK_REMOVETYPE_QUOTATION%>','<%= orderheader.getSalesDocNumber() %>');">
                       <img src="<isa:webappsURL name ="/b2b/mimes/images/trash.gif"/>" alt="<isa:translate key="<%=documentModificationKey%>"/>" border="0"></td>
                     </a>
                     <% } %>
               </td>
               <% } %>
             </tr>
           </tbody>
           </isa:iterate>
          </table></td>
        <td>&nbsp;</td>
      </tr>
     <% }  // ZeroDocumensSelected %>
    </tbody>
  </table>

  <% } // END nothingSelectedyet %>

<% if (showRemoveMessage != null && showRemoveMessage.equals("X")) { %>
  <FORM name="RemoveMessage" method="post" action="<isa:webappsURL name="/b2b/updatedocumentview.do"/>" target="form_input">
  </FORM>
  <SCRIPT LANGUAGE="JavaScript"> document.RemoveMessage.submit();
  </SCRIPT>
<% } %>

<% if (showRemoveMessage != null && showRemoveMessage.equals("H")) { %>
  <FORM name="RemoveMessage" method="post" action="<isa:webappsURL name="/b2b/history/updatehistory.do"/>" target="_history">
  </FORM>
  <SCRIPT LANGUAGE="JavaScript"> document.RemoveMessage.submit();
  </SCRIPT>
<% } %>

<% if (doclistreload != null && doclistreload.equals("X")) { %>
  <SCRIPT LANGUAGE="JavaScript"> document.<%= DocumentListSelectorForm.DOCUMENTSELECTFORM%>.submit();
  </SCRIPT>
<% } %> 
<% if (isOrderDownloadRequired) { %>
<form id="orderdownloadallform" name="orderdownloadallform" method="post" action="<isa:webappsURL name="b2b/order/addOrderToMassDownloadList.do"/>" target="form_input">
   <% int count = 0; %>
   <isa:iterate id="orderheader1" name="<%= DocumentListAction.RK_ORDER_LIST %>"
              type="com.sap.isa.businessobject.header.HeaderSalesDocument">
  <input type="hidden" name="orderid[<%= count %>]" value="<%=orderheader1.getSalesDocNumber()%>" />
  <input type="hidden" name="orderguid[<%= count %>]" value="<%=orderheader1.getTechKey().getIdAsString()%>" />
  <input type="hidden" name="status[<%= count %>]" value="<%=orderheader1.getStatus()%>" />
  <input type="hidden" name="desc[<%= count %>]" value="<%=orderheader1.getDescription()%>" />
   <% count++; %>
  </isa:iterate>
  <input type="hidden" name="operation" value="addall" />
</form>
<% } %>

</body>
</html>