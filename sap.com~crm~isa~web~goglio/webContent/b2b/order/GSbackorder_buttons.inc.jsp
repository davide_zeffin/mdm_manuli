<%--
********************************************************************************
    File:         GSbackorder_buttons.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Created:      03.03.2005
    Version:      1.0
********************************************************************************
--%>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ taglib uri="/isa" prefix="isa" %>
<% GenericSearchUIData ui = (GenericSearchUIData)request.getAttribute(GenericSearchUIData.RC_UICLASSINSTANCE); %>
<% if (ui.isResultListEnabled()) { %>
<script type="text/javascript">
  function closeDocument() {
      document.location.href = "<isa:webappsURL name ="b2b/genericsearchclose.do"/>"
  }
</script>
    </div> <%-- Closes <div id="document"> in file GSbackorder_header.inc.jsp --%>
    <div id="buttons">
            <a id="access-buttons" href="#access-header" title= "Navigation bar of the document. Click on this link to get to the header." accesskey="n">
                    <img src="/b2b/mimes/images/1X1.gif" alt="" style="display:none" width="1" height="1" />
            </a>            
            <ul class="buttons-3">
                                    <li>
                                            <a href="#" onclick="closeDocument();" title="<isa:translate key="gs.wl.backord.list.close"/> <isa:translate key="ecm.acc.button"/>"><isa:translate key="gs.wl.backord.list.close"/></a>
                                    </li>
            </ul>
    </div>

<% } %>