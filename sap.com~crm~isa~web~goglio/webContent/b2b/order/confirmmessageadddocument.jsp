<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ taglib uri="/WEB-INF/isa.tld" prefix="isa" %>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
<head>
  <isa:includes/>
  <!-- <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet"> -->

</head>
<body>
  <div class="module-name"><isa:moduleName name="b2b/order/confirmmessageadddocument.jsp" /></div>

    <div class="opener"><isa:translate key="product.jsp.confirmAddToBasket"/></div>

    <div align="center">
      <input type="button" onclick="parent.window.close();" value="<isa:translate key="error.jsp.back"/>" >
    </div>
</body>
</html>