<%--
********************************************************************************
    File:         payment_maintain.jsp
    Copyright (c) 2004, SAP Germany, All rights reserved.

    Author:       SAP
    Created:      14.12.2004
    Version:      1.0
********************************************************************************
--%>

<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.core.businessobject.management.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.PaymentUI" %>
<%@ page import="com.sap.isa.helpvalues.*" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.*" %>
<%@ page import="com.sap.isa.payment.businessobject.*"%>
<%@ page import="com.sap.isa.payment.backend.boi.*"%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%--
<%@ include file="checksession.inc" %>
--%>

<isa:contentType />
<isacore:browserType/>

<%
    PaymentUI paymentUI = new PaymentUI(pageContext);
    
    if (paymentUI.getTypes() == null) {
      if (!paymentUI.isBackendR3()) {   	
	    paymentUI.initTypes(true, false, true, PaymentBaseTypeData.INVOICE_TECHKEY);
      } else {
	    paymentUI.initTypes(true, true, true, PaymentBaseTypeData.INVOICE_TECHKEY);
      }	   
    } else {        
     if (!paymentUI.isBackendR3()) {   	
	   paymentUI.setCODAvailable(false);
     } 
    }
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=paymentUI.getLanguage()%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="payment.maintain.jsp.title"/></title>

   <isa:stylesheets/>
   
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
          type="text/javascript"> 
    </script>
 
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/payment.js")%>"
          type="text/javascript"> 
    </script>

</head>

<body class="payment">
   <div class="module-name"><isa:moduleName name="b2b/order/payment_maintain.jsp" /></div>
   <h1><span><isa:translate key="payment.type.txt"/></span></h1>  
   <form action="<isa:webappsURL name="b2b/maintainpayment.do"/>" 
         id="paymentform"
         method="post">
    <div id="document">  
     <div class="document-header">  
     <input type="hidden" name="payment_maintain" value="true" />    
     <div class="header-payment">      
       <% if (paymentUI.isGrossValueAvailable() &&
              paymentUI.getHeader() != null) { %> 
           <div class="payment-price">      
		    <p><isa:translate key="payment.maintain.order.amount"/> <em><%= JspUtil.removeNull(paymentUI.getHeader().getGrossValue()) + "&nbsp;" + JspUtil.removeNull(paymentUI.getHeader().getCurrency())%></em></p>
		    &nbsp;
		   </div>
	   <% } %>  
	   <div class="payment-select">     
          <%@ include file="/b2b/paymentTypeSelection.inc.jsp" %> 
       </div>
     &nbsp;
     &nbsp;
     <div class="payment-cards">   
       <!-- Payment cards Start -->   
        <% paymentUI.setAllDisabledByPayType(); %>   
        <%@ include file="/b2b/paymentCards.inc.jsp" %> 
       <!-- Payment cards End -->  
      </div>
    <%-- Message list --%>
<%  if ( paymentUI.hasMessages() ) { %>
        <isa:message id="messagetext" name="<%= PaymentActions.DOC_MESSAGES %>" ignoreNull="true">
          <div class="error"><span><%=JspUtil.encodeHtml(messagetext)%></span></div>
        </isa:message>
<%  } %>
    
   </div>
  </div>
  </div>
  <div id="buttons">
     <input type="hidden" name="<%=PaymentActions.BUTTON%>" value="" />
     <ul class="buttons-3">
     <li>
     <a href="#" onclick="perform_action_ccardcheck('<%=PaymentActions.UPDATE%>', '<isa:translate key="payment.ccardno.invalid"/>');" title="<isa:translate key="payment.button.update"/>"><isa:translate key="payment.button.update"/></a></li>
     <li>
     <a href="#" onclick="perform_action('<%=PaymentActions.CANCEL%>');" title="<isa:translate key="payment.button.cancel"/>"><isa:translate key="payment.button.cancel"/></a></li>
     <li>
     <% if (paymentUI.isOrderSimulation()) { %>
     <a href="#" onclick="perform_action_ccardcheck('<%=PaymentActions.SAVE%>','<isa:translate key="payment.ccardno.invalid"/>');" title="<isa:translate key="payment.button.checkout"/>"><isa:translate key="payment.button.checkout"/></a></li>         
     <% } else { %>
     <a href="#" onclick="perform_action_ccardcheck('<%=PaymentActions.SAVE%>','<isa:translate key="payment.ccardno.invalid"/>');" title="<isa:translate key="payment.button.save"/>"><isa:translate key="payment.button.save"/></a></li>         
     <% } %>
     </ul>
   </div>  
  </form> 
</body>
</html>