<%--
********************************************************************************
    File:         closer_batch_details.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      15.5.2001
    Version:      1.0

    $Revision: #19 $
    $Date: 2002/05/08 $
********************************************************************************
--%>

<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>



<%@ include file="../checksession.inc" %>
<%@ include file="../usersessiondata.inc" %>

<isa:contentType />
<% BaseUI ui = new BaseUI(pageContext);%>

<%  boolean backendR3 = true; %>
<isacore:ifShopProperty property = "backend" value = "<%=ShopData.BACKEND_CRM%>">
  <% backendR3 = false; %>
</isacore:ifShopProperty>

<%
    // Check if oci transfer is required
    boolean ociTransfer =
        (((IsaCoreInitAction.StartupParameter)userSessionData.
            getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).
            getHookUrl().length() > 0);

    // Get the information for which type of sales document we were called
    String docType = (String) userSessionData.getAttribute(MaintainBasketBaseAction.SC_DOCTYPE);

    String confirmKey = "b2b.ord.conf." + docType;
    String cancelKey = "";
    if (ociTransfer && "basket".equals(docType)) {
        cancelKey  = "b2b.ord.cncl.conf." + docType + ".oci";
        confirmKey += ".oci";
    }
    else {
        cancelKey  = "b2b.ord.cncl.conf." + docType;
        confirmKey += ".plain";
    }
    String alertBasketEmpty = "b2b.ordr.alr." + docType + ".empty";

    String sendKey = "b2b.ord.sub.send." + docType;

    boolean orderSimulate = false;
%>
  <isacore:ifShopProperty property="OrderSimulateAvailable" value ="true">
    <% orderSimulate = true; %>
  </isacore:ifShopProperty>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <title>SAP Internet Sales - Closer Details</title>
    <isa:includes/>
    <!-- <link href="<%= WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css") %>"
          type="text/css" rel="stylesheet"> -->

	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>

      <script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/closer.js") %>"
              type="text/javascript">
      </script>

      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js") %>"
              type="text/javascript">
      </script>

      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
              type="text/javascript">
      </script>


    <script type="text/javascript">

      function send_confirm() {
              parent.frames[0].document.forms["batch_positions"].setbatch.value="true";
              parent.frames[0].document.forms["batch_positions"].action="<isa:webappsURL name="/b2b/maintainorderbatch.do"/>";
              parent.frames[0].document.forms["batch_positions"].submit();
              return true;
      }



      function cancel_confirm() {
              parent.frames[0].document.forms["batch_positions"].cancel.value="true";
              parent.frames[0].document.forms["batch_positions"].action="<isa:webappsURL name="/b2b/maintainorderbatch.do"/>";
              parent.frames[0].document.forms["batch_positions"].submit();
              return true;
      }
    </script>

  </head>
 <body class="workarea">
    <form action="">
      <div id="new-doc" class="module">
        <div class="module-name"><isa:moduleName name="closer_batchorder_details.jsp" /></div>
        <table width="100%" border="0" cellpadding="3" cellspacing="0">
          <tr>
            <td class="scrollbarSpace" align="right">

			<%
				if(ui.isAccessible()) {
					String cancelBtnTxt = WebUtil.translate(pageContext, "b2b.order.display.submit.cancel", null);
					String sendBtnTxt = WebUtil.translate(pageContext, "b2b.ord.sub.send.oci", null);
			%>
              		<input class="green" type="button" name="cancel"
              		title="<isa:translate key="access.button" arg0="<%=cancelBtnTxt%>" arg1=""/>"
              		value="<isa:translate key="b2b.order.display.submit.cancel"/>" onclick="cancel_confirm();">

                  	<input class="green" type="button" name="send"
                  	title="<isa:translate key="access.button" arg0="<%=sendBtnTxt%>" arg1=""/>"
                  	value="<isa:translate key="b2b.ord.sub.send.oci"/>" onclick="send_confirm();">
			<%  } else { %>
					<input class="green" type="button" name="cancel"
					value="<isa:translate key="b2b.order.display.submit.cancel"/>" onclick="cancel_confirm();">

                  	<input class="green" type="button" name="send"
                  	value="<isa:translate key="b2b.ord.sub.send.oci"/>" onclick="send_confirm();">
			<%  } %>



            </td>
          </tr>
        </table>
      </div>
    </form>
  </body>
</html>