<%--
********************************************************************************
    File:         ordersimulate.jsp
    Copyright (c) 2002, SAP AG, Germany, All rights reserved.
    Author:
    Created:      21.02.2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2002/05/10 $
********************************************************************************
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.businesspartner.backend.boi.PartnerFunctionData" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.PaymentUI" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<isa:contentType />
<isacore:browserType/>

<%
    SimulationUI ui = new SimulationUI(pageContext);

	String inputValue = "";
	
    //inline display of product configuration will be restricted on values of identifying characteristics
    boolean showDetailView = false;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title><isa:translate key="b2b.order.simulate.pagetitle"/></title>

    <isa:stylesheets/>

	  <script type="text/javascript">
	      <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	  </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
              type="text/javascript">
      </script>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
              type="text/javascript">
      </script>
    <% if(ui.getShop().isPricingCondsAvailable() == true) {%>
	   <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
		   type = "text/javascript">
	 </script>
    <% } %>
      <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js") %>"
              type="text/javascript">
      </script>
      <script type="text/javascript">

            alreadySend = 0;

            function sendPressed() {
              if (alreadySend == 0) {
                 check = confirm('<isa:UCtranslate key="<%= ui.getConfirmKey() %>"/>');

                 if(check) {
                     alreadySend = 1;
                     location.href = '<isa:webappsURL name="/b2b/basketsimulationsend.do"/>';
                     return true;
                  }
                  else {
                     return false;
                  }
              }
            }

            function backPressed() {
                location.href = '<isa:webappsURL name="/b2b/basketcancelsimulate.do"/>';
                return true;
            }
            function ociPressed() {
                location.href = '<isa:webappsURL name="/b2b/ocisimulate.do"/>';
                return true;
            }


            function maintainPaymentPressed() {
               location.href = '<isa:webappsURL name="/b2b/maintainpaymentprepare.do"/>';
               return true;
            }
            
    </script>
  </head>

  <body class="ordersimulate">
    <div class="module-name"><isa:moduleName name="b2b/order/ordersimulate.jsp" /></div>

    <h1><span><%= ui.getHeaderDescription() %></span></h1>
  
    <div id="document">
    <% if (ui.isAccessible()) { 
         if (ui.docMessageString.length() > 0) { %>
            <%@ include file="/b2b/messages-declare.inc.jsp" %>
	 		<%@ include file="/b2b/messages-begin.inc.jsp" %>
	 		<% addMessage(Message.ERROR, ui.docMessageString); %>

	<%	    if (!messages.isEmpty()) { %>
				 <%@ include file="/b2b/messages-end.inc.jsp" %>
	<%	    } 
          } 
        } %>
         <div class="document-header">
         <a id="access-header" href="#access-items" title= "<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>
             <div class="header-basic">  <%-- level: sub3 --%>
		     <table class="layout">
		         <tr>
		             <td class="col1">
                         <% if (ui.isAccessible()) { %>
                             <a href="#end-table1" title="<isa:translate key="ecm.acc.header.dat.sum"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="11"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
                         <% } %>
                         <table class="header-general" summary="<isa:translate key="ecm.acc.header.dat.sum"/>">
                             <tr class="title">
 	                             <td colspan="2"><%= ui.getHeaderDescription2() %></td>
                             </tr>
                             <%-- render the consumer, only in B2R scenario --%>
                             <% if (ui.isSoldToVisible()) { %>
                             <tr>
                                 <td class="identifier"><isa:translate key="status.sales.consumer.name"/> (<isa:translate key="status.sales.consumer.no"/>): </td>
                                 <td class="value"><%= JspUtil.encodeHtml(ui.getSoldToName()) %> (<%= JspUtil.encodeHtml(ui.getSoldToId()) %>)</td>
                             </tr>
                             <% } %>
							 <% if (!ui.isOciTransfer()) { %>
                             <tr>
                                 <td class="identifier"><isa:translate key="status.sales.purchaseorderext"/>:</td>
                                 <td class="value"><%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %></td>
                             </tr>
                             <% } %>
                             <% if (!ui.isBackendR3()) { %>
                             <tr>
                                 <td class="identifier"><isa:translate key="status.sales.description"/>: </td>
                                 <td class="value"><%= JspUtil.encodeHtml(ui.header.getDescription()) %></td>
                             </tr>
                             <% } %>
                             <%-- GREENORANGE BEGIN --%>
                             <tr>
                             	<td class="identifier"><isa:translate key='customer.order.jsp.Currency'/></td>
	                            <td class="value"><%= JspUtil.encodeHtml((String)ui.header.getExtensionData("ZEXT_WAERK")) %></td>
	                     	</tr>
	                     	<tr>
	                     		<% String termsPay = ""; %>
	                     		<td class="identifier"><isa:translate key="customer.order.jsp.TermsPayment"/></td>
	                     		<isa:helpValues id="paymentTermsList" name="PaymentTerms">
									<% 
										if(paymentTermsList.getValue("paymentTerms").equals(ui.header.getPaymentTerms())){
											termsPay = 
												paymentTermsList.getValueDescription("paymentTerms");
										}
									%>
			   					</isa:helpValues>
			   					<td class="value"><%= JspUtil.encodeHtml(termsPay) %></td>
	                     	</tr>
							<tr>
                             	<td class="identifier"><isa:translate key="b2b.order.disp.incoterms1"/></td>
	                            <td class="value"><%= JspUtil.encodeHtml(ui.header.getIncoTerms1()) %>
	                            <% if (ui.header.getIncoTerms1().length() > 0 && ui.header.getIncoTerms1Desc().length() > 0) { %>
	                                <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/hint.gif") %>" alt="<%= JspUtil.encodeHtml(ui.header.getIncoTerms1Desc()) %>"/>
	                            <% } %>
	                            <%=JspUtil.encodeHtml(ui.header.getIncoTerms2()) %>
	                        	</td>
	                     	</tr>
	                     	<%-- GREENORANGE END --%>
                         </table>
                         <% if (ui.isAccessible()) { %>
                             <a name="end-table1" title="<isa:translate key="ecm.acc.head.dat.end"/>"></a>
                         <% } %>
                     </td>
                     <td class="col2"> 
                         <%-- table price --%>
                         <% if (ui.isAccessible()) { %>
                             <a href="#end-table2" title="<isa:translate key="ecm.acc.header.prieces"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="4"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
                         <% } %>
                         <table class="price-info" summary="<isa:translate key="ecm.acc.header.prieces"/>">
                             <tr>
                                 <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalnet"/>:</td>
                                 <td class="value"><%= ui.getHeaderValue() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                             </tr>
<%-- GREENORANGE BEGIN --%>
							<% if (ui.isElementVisible("order.freight")) { %>
                             <tr>
                                 <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalfreight"/>:</td>
                                 <td class="value"><%= ui.header.getFreightValue() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                             </tr>
                            <% } %>
<%-- GREENORANGE END --%>                             
                             <tr>
                                 <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totaltax"/>:</td>
                                 <td class="value"><%= ui.header.getTaxValue() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                             </tr>
                             <tr>
                                 <td colspan="2" class="separator"></td>
                             </tr>
                             <tr>
                                 <td class="identifier" scope="row"><isa:translate key="status.sales.detail.totalgross"/>:</td>
                                 <td class="value"><%= ui.header.getGrossValue() %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %></td>
                             </tr>
                             <% if (ui.header.getPaymentTermsDesc() != null && ui.header.getPaymentTermsDesc().length() > 0) { %>
                             <tr>
                                 <td class="identifier-1" scope="row"><isa:translate key="b2b.order.disp.paymentterms"/></td>
                                 <td class="terms"><%= JspUtil.encodeHtml(ui.header.getPaymentTermsDesc()) %></td>
                             </tr>
                             <% } %>
                         </table>
                         <% if (ui.isAccessible()) { %>
                             <a name="end-table2" title="<isa:translate key="ecm.acc.head.prc.end"/>"></a>
                         <% } %>
                         <%-- End table price --%>
                     </td>
                 </tr>
             </table> <%-- class="layout"> --%>
	         </div> <%-- class="header-basic"> --%>
	         <%-- All Data --%>  
			 <% if (!ui.isOciTransfer()) { %>
             <% if (ui.isAccessible()) { %>
	             <a href="#end-table3" title="<isa:translate key="b2b.acc.header.alldata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
             <% } %>	           
             <div class="header-itemdefault">  <%-- level: sub3 --%>
	             <h1 class="group"><span><isa:translate key="b2b.header.itemdefault"/></span></h1>
	             <div class="group">
                 <%--Item defaults Data--%>
                 <% if (ui.isAccessible()) { %>
		             <a href="#end-table4" title="<isa:translate key="b2b.acc.header.defdata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
	             <% } %>
                     <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>">
	                     <tr>
	                         <td class="identifier"><isa:translate key="status.sales.detail.deliveryaddr"/>:</td>
	                         <td class="value">
	                         <% if (ui.header.getShipTo() != null) { %>
	                         <%= JspUtil.encodeHtml(ui.header.getShipTo().getShortAddress())%>
	                         <% } %>
	                         </td>
	                     </tr>                     
                         <% if (ui.isElementVisible("order.shippingCondition")){ %>
	                     <tr>
	                         <td class="identifier"><% if (ui.header.getShipCond() != null) { %><isa:translate key="status.sales.detail.shippingcond"/>:<%}%></td>
	                         <td class="value">
	                         <% if (ui.header.getShipCond() != null) { %> 
	                              <isa:iterate id="shipCond"
                                     name="<%= MaintainOrderBaseAction.RC_SHIPCOND %>"
                                     type="com.sap.isa.core.util.table.ResultData">
                                     <% if (shipCond.getRowKey().toString().equals(ui.header.getShipCond())) {  %>
                                          <%= JspUtil.encodeHtml(shipCond.getString(1)) %>
                                     <% } %>
                                  </isa:iterate> 
                             <% } %>
                             </td>
	                     </tr>
	                     <% }
	                        if (ui.isElementVisible("order.deliveryPriority") &&
	                            ui.header.getDeliveryPriority() != null) { %>
	                     <tr>
	                         <td class="identifier"><isa:translate key="status.sales.detail.dlv.prio"/>:</td>
	                         <td class="value"><%= JspUtil.encodeHtml(ui.getDeliveryPriorityDescription(ui.header.getDeliveryPriority())) %> </td>
	                     </tr>
                         <% } %>
	                     <% if ( ! ui.header.isDocumentTypeOrderTemplate()) { %>
	                     <tr>
	                         <td class="identifier"><isa:translate key="b2b.order.disp.reqdeliverydate"/></td>
	                         <td class="value"><%= JspUtil.encodeHtml(ui.header.getReqDeliveryDate()) %></td>
	                     </tr>
	                     <%-- Latest Delivery Date for Header  --%>
						 <% if (ui.isElementVisible("order.latestDeliveryDate")){ %>
							   <tr>
								 <td class="identifier"><isa:translate key="b2b.order.grid.canceldate"/></td>
								 <td class="value"><%= JspUtil.encodeHtml(ui.header.getLatestDlvDate()) %></td>
						 		</tr>
	                     <% } %>
	                
	                     <% } %>
	                 </table>
                 <% if (ui.isAccessible()) { %>
		             <a name="end-table4" title="<isa:translate key="b2b.acc.header.end.defdata"/>"></a>
	             <% } %>
                 <%-- End Item defaults Data --%>
	             </div> <%-- end class="group" --%>
	         </div> <%-- class="header-itemdefault"> --%>    
	         
             <%-- Additional Header Data --%>
             <% if (ui.isAccessible()) { %>
		         <a href="#end-table5" title="<isa:translate key="b2b.acc.header.adddata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
	         <% } %>
	         <div class="header-additional">  <%-- level: sub3 --%>
               <h1 class="area">
                   <a class="icon" href="javascript:toggleHeader()" ><img id="addheadericon" src="<%=WebUtil.getMimeURL(pageContext,"mimes/images/open.gif")%>" alt="<isa:translate key="b2b.order.adddata.icon.detail"/>"  /></a>
                   <span><isa:translate key="b2b.header.adddata"/></span>
               </h1>	            
	           <div class="area" id="addheader" style="display: none">
                 <div class="header-misc">
                     <h1 class="group"><span><isa:translate key="b2b.header.group.misc"/></span></h1>
            
                     <div class="group">
                         <table class="data" summary="<isa:translate key="b2b.acc.header.adddata"/>">
                             <% if (ui.header.getIncoTerms1() != null) { %>
	                         <%-- GREENORANGE BEGIN --%>
	                         <%-- <tr>
	                             <td class="identifier"><isa:translate key="b2b.order.disp.incoterms1"/></td>
	                             <td class="value"><%= JspUtil.encodeHtml(ui.header.getIncoTerms1()) %>
	                             <% if (ui.header.getIncoTerms1().length() > 0 && ui.header.getIncoTerms1Desc().length() > 0) { %>
	                                 <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/hint.gif") %>" alt="<%= JspUtil.encodeHtml(ui.header.getIncoTerms1Desc()) %>"/>
	                             <% } %>
	                             <%=JspUtil.encodeHtml(ui.header.getIncoTerms2()) %>
	                             </td>
	                         </tr> --%>
	                         <%-- GREENORANGE END --%>
 	                         <% }
 	                         if (ui.isHeaderCampaignListSet()) { %>
	                         <tr>
	                             <td class="identifier"><isa:translate key="b2b.order.disp.camp"/></td>
	                             <td class="value"><%= JspUtil.encodeHtml(ui.getHeaderCampaignId(0)) %>
	                             <% if (!ui.isHeaderCampaignValid(0))  { %>
	                                 <img width="16" height="16" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/error.gif") %>" alt="<isa:translate key="b2b.disp.camp.inv"/>"/>
	                             <% } %>
	                             </td>
	                         </tr>
	                         <tr>
	                             <td class="identifier">&nbsp;</td>
	                             <td class="value"><%= JspUtil.encodeHtml(ui.getHeaderCampaignDescs(0)) %></td>
	                         </tr>
                             <% }
                                if (ui.header.getRecallId() != null && ui.header.getRecallId().length() > 0) { %>
                             <tr class="odd">
                                 <td class="identifier"><isa:translate key="b2b.order.disp.recall"/></td>
                                 <td class="value"><%= JspUtil.encodeHtml(ui.header.getRecallId()) %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getRecallDesc()) %></td>
                             </tr>
	       	   	             <% } %>
                         </table>
                       </div> <%-- end class="group" --%>
                       <%-- Payment data --%>
                       <%@ include file="/ecombase/paymentinfo.inc.jsp" %> 
				       <%-- end payment data --%>
				 <div class="header-message">
                     <h1 class="group"><span><isa:translate key="b2b.header.group.message"/></span></h1>
                     <%-- Data Message --%>
                     <div class="group-confirm">                       
                         <table class="message" summary="<isa:translate key="ecm.acc.head.msg.d"/>">
                             <tr>
                                 <td class="identifier"><label for="headerComment"><isa:translate key="b2b.order.details.note"/></label></td>
                                 <td class="value"><%= (ui.hasHeaderText()) ? JspUtil.encodeHtml(ui.header.getText().getText(), new char[] {'\n'}):"" %></td>
                             </tr>
                         </table>
                     </div> <%-- end class="group" --%>
				     <%-- End Data Message --%>
				 </div> <%-- end class="header-message" --%>
			   </div> <%-- end class="header-misc" --%>
			</div> <%-- End class="area" --%>
		 </div> <%-- End class="header-additional"> --%>
		 <% if (ui.isAccessible()) { %>
		     <a name="end-table5" title="<isa:translate key="b2b.acc.header.end.adddata"/>"></a>
		 <% } %>
         <%-- End Additional Header Data --%>
         <%-- Document flow --%>
         <% if ((ui.header.getPredecessorList() != null && ui.header.getPredecessorList().size() > 0) ||
                (ui.header.getSuccessorList() != null && ui.header.getSuccessorList().size() > 0)) { %>
             <div class="header-docflow">  <%-- level: sub3 --%>
                 <h1 class="group"><span><isa:translate key="b2b.header.group.docflow"/></span></h1>
                 <div class="group">
                     <table class="data">
			             <%@ include file="/ecombase/connecteddocuments.inc.jsp" %>                
                     </table> <%-- end class="data" --%>
                 </div> <%-- end class="group" --%>
             </div> <%-- class="header-docflow"> --%>
         <% } %>               
         <%-- End Document flow --%> 
         <%-- Error --%>                            
         <% if (ui.docMessageString.length() > 0) { %>
             <div class="error"><span><%= ui.docMessageString %></span></div>
         <% } %>                    
         <% if (ui.isAccessible()) { %>
	         <a name="end-table3" title="<isa:translate key="b2b.acc.header.end.alldata"/>"></a>
         <% } %>
         <%-- End All Data --%>                    
         
	 <% } %> <%-- end if (!ui.isOciTransfer()) --%>
	
     </div> <%-- End document-header --%>

    <div class="document-items">

    <%-- Document Item Rows --%>
    <form name="positions" method="post">
     <% if (ui.isAccessible) { %>
        <a href="#end-table6" title="<isa:translate key="ecm.acc.items.sum"/>
        <isa:translate key="ecm.acc.tab.size.info" arg0="7" arg1="<%=(String.valueOf(ui.items.size())) %>"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
     <% } %>
     <div>
         <input type="hidden" name="docnumber" value="<%=ui.header.getSalesDocNumber()%>" />
     </div>
     <table class="itemlist" summary="<isa:translate key="ecm.acc.items.sum"/>">
        <%-- Items Table Header --%>
        <tr>
            <th class="item" scope="col"><isa:translate key="status.sales.detail.itemposno"/></th>
            <th class="product" scope="col"><isa:translate key="status.sales.detail.productno"/></th>
            <th class="qty" scope="col"><isa:translate key="status.sales.detail.itemquantity"/></th>
            <th class="desc" scope="col"><isa:translate key="status.sales.detail.description"/></th>
            <% if (!ui.header.isDocumentTypeOrderTemplate() && !ui.header.isDocumentTypeQuotation()) {%>
             <th class="qty-avail" scope="col"><isa:translate key="status.sales.dt.confDelQty"/></th>
             <th class="date-on" scope="col"><isa:translate key="status.sales.detail.confDelDate"/></th>
            <% } %>
            <% if (ui.isContractInfoAvailable()) { %>
             <th class="item" scope="col"><isa:translate key="status.sales.detail.itemcontract"/></th>
            <% } %>
            <th class="price" scope="col"><isa:translate key="status.sales.detail.totalvalue"/><br /><em><isa:translate key="b2b.order.details.price"/></em></th>
        </tr>

        <%-- Items Table Positions --%>
          <isa:iterate id="item" name="<%= MaintainBasketBaseAction.RK_ITEMS %>"
                           type="com.sap.isa.businessobject.item.ItemSalesDoc">
              <% ui.setItem(item);
                 ConnectedDocumentItem connectedItem = (ConnectedDocumentItem)item.getPredecessor();
                 String substMsgColspan = ui.getItemsColspan();
               %>
               <% if (!ui.isBOMSubItemToBeSuppressed()) { %>
               <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>
               <tr class="<%= ui.even_odd%>" id="row_<%= ui.line %>" scope="row">
                  <td <% if (ui.itemHierarchy.isSubItem(item)) { %> class="item-sub-<%= ui.getItemLevel() -1 %>" <% } else { %> class="item" <% } %>>
                        <%= JspUtil.removeNull(item.getNumberInt()) %>
                  </td>
                  <td class="product"><%= JspUtil.encodeHtml(ui.getProduct()) %></td>
                  <td class="qty"><%= JspUtil.encodeHtml(item.getQuantity()) %>&nbsp;<abbr title="<isa:translate key="status.sales.detail.itemunit"/>"><%= JspUtil.encodeHtml(item.getUnit()) %></abbr></td>
                  <td class="desc"><%= JspUtil.encodeHtml(item.getDescription().trim()) %>&nbsp;<%= JspUtil.encodeHtml(ui.getSubstitutionReason())%>
                    <% if (ui.isItemCampaignAssignedAuto()) { %>
                         <br />
                         <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/camp_assigned.gif") %>" alt="<isa:translate key="b2b.disp.camp.ass"/>"/>
                    <% } %>
                  </td>
                  <% if (!ui.header.isDocumentTypeOrderTemplate() && !ui.header.isDocumentTypeQuotation()) { %>
                  <td class="qty-avail">
                      <% ArrayList aList = item.getScheduleLines();
                         if (ui.showItemATPInfo()) {
                            for (int i = 0; i < aList.size(); i++) {
                                Schedline scheduleLine = (Schedline) aList.get(i); %>
                                <%= JspUtil.removeNull(scheduleLine.getCommittedQuantity()) %><br />
                      <%    }
                         } %>
                  </td>
                  <td class="date-on">
                      <% if (ui.showItemATPInfo()) {
                            for (int i = 0; i < aList.size(); i++) {
                               Schedline scheduleLine = (Schedline) aList.get(i); %>
                               <%= JspUtil.removeNull(scheduleLine.getCommittedDate()) %><br />
                      <%    }
                         } %>
                  </td>
                  <% } %>
                  <% if (ui.isContractInfoAvailable()) { %>
                  <td class="item>"><% if ( (item.getContractId() != null) && !(item.getContractId().equals(""))) { %>
                  <a href="<isa:webappsURL name="b2b/contractselect.do"/>?contractSelected=<%=item.getContractKey()%>"><%=item.getContractId()%></a>&nbsp;/&nbsp;<%=item.getContractItemId()%>
                      <% } %>
                  </td>
                  <% } %>
                  <td class="price">
                  <% if (!ui.itemHierarchy.hasATPSubItem(item) && item.isPriceRelevant()) {
                        if (ui.showIPCpricing == true  &&  ui.header.getIpcDocumentId() != null) { %>
                        <a href="#" onclick="displayIpcPricingConds(  '<%= JspUtil.removeNull(ui.header.getIpcConnectionKey()) %>',
                                                                      '<%= JspUtil.removeNull(ui.header.getIpcDocumentId().getIdAsString()) %>',
                                                                      '<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>'
                                                                   )">
                    <% } %>
                    <%= ui.getItemValue() %>&nbsp;<%= JspUtil.encodeHtml(item.getCurrency()) %>
                    <% if (ui.showIPCpricing == true) { %>
                        </a>
                    <% }
                       if ((item.getNetPriceUnit()!=null) && (!item.getNetPriceUnit().equals(""))){%>
                        <br /><em>
                            <%= JspUtil.removeNull(item.getNetPrice()) + "&nbsp;" + JspUtil.encodeHtml(item.getCurrency()) + "&nbsp;" + '/' + "&nbsp;" + JspUtil.encodeHtml(item.getNetQuantPriceUnit()) + ' ' + JspUtil.encodeHtml(item.getNetPriceUnit()) %>
                            </em>
                    <% } %>
                    </td>
                  <% } else { %>&nbsp;
                    </td>
                  <% } %>
                </tr>

                  <%-- Item Details --%>
                  <% 
				  if (ui.showItemDetailButton()) {%>                  
                  <tr class="<%=ui.even_odd + "-detail"%>" id="rowdetail_<%= ui.line %>" headers="row_<%= ui.line %>">
                     <td class="select"></td>
                     <td class="detail" colspan="<%= ui.getItemsColspan() %>">
                       <table class="item-detail" summary="<isa:translate key="ecm.acc.item.detail.info"/>">
                         <% if (ui.isProductDeterminationInfoAvailable()){ %>{ %>
                         <tr>
                           <td class="identifier"><isa:translate key="b2b.proddet.sysprodid"/></td>
                           <td class="identifier"><%= JspUtil.encodeHtml(item.getSystemProductId()) %></td>
                         </tr>
                         <% } %>
                         <%-- display of product configuration --%>
                         <%@ include file="/ecombase/orderitemconfiginfo.inc.jsp" %>
                         <%-- Shipto --%>
                         <% if (item.getShipTo() != null  &&
                                ! item.getShipTo().getShortAddress().equals("")) { %>
                            <tr>
                              <td class="identifier"><isa:translate key="status.sales.detail.deliveryaddr"/>:</td>
                              <td class="value" colspan="5"><%= JspUtil.encodeHtml(item.getShipTo().getShortAddress()) %></td>
                            </tr>
                         <% } %>
                         <%-- Delivery Priority --%>
                         <% if (ui.isElementVisible("order.deliveryPriority") &&
                                item.getDeliveryPriority() != null &&
                         		item.getDeliveryPriority().length() > 0) { %>
                            <tr>
                              <td class="identifier"><isa:translate key="status.sales.detail.dlv.prio"/>:</td>
                              <td class="value" colspan="5"><%=JspUtil.encodeHtml((String) ui.getDeliveryPriorityDescription(item.getDeliveryPriority())) %>
                              </td>
                            </tr>
                         <% } %>
                         <%--  Request Delivery Date (for orders only)--%>
                         <% if (!ui.header.isDocumentTypeOrderTemplate()) { %>
                            <tr>
                              <td class="identifier"><isa:translate key="b2b.order.reqdeliverydate"/></td>
                              <td class="value" colspan="5"><%= JspUtil.encodeHtml(item.getReqDeliveryDate()) %></td>
                            </tr>
                            <%--  Latest Delivery Date at item level(for orders only)--%>
							<% if (ui.isElementVisible("order.latestDeliveryDate")){ %>
							 	<tr>
                              		<td class="identifier"><isa:translate key="b2b.order.grid.canceldate"/></td>
                              		<td class="value" colspan="5"><%= JspUtil.encodeHtml(item.getLatestDlvDate()) %></td>
                            	</tr>
                         <% } %>
                            
                         <% } %>
                      <%-- campaigns --%>
                      <% if (ui.isItemCampaignListSet()) { %>
                          <tr>
                            <td class="identifier"><isa:translate key="b2b.order.disp.camp"/></td>
                            <td class="value"><%= JspUtil.encodeHtml(ui.getItemCampaignId(0)) %>
                             <% if (!ui.isItemCampaignValid(0))  { %>
                                 <img  width="16" height="16" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/error.gif") %>" alt="<isa:translate key="b2b.disp.camp.inv"/>"/>
                             <% } %>
                             <% if (ui.isItemCampaignManuEntered(0)) { %>
                                 <img  width="16" height="16" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_manCampaignEntry.gif") %>" alt="<isa:translate key="b2b.disp.camp.man"/>"/>
                             <% } %>
                            </td>
                          </tr>
                          <tr>
                             <td class="identifier">&nbsp;</td>
                             <td class="value"><%= JspUtil.encodeHtml(ui.getItemCampaignDescs(0)) %></td>
                          </tr>
                      <% } %>
                      <%-- notes --%>
                      <% if (item.hasText()) { %>
                          <tr class="message-data">
                            <td class="identifier"><isa:translate key="b2b.order.details.note"/></td>
                            <td class="value"><%= JspUtil.encodeHtml(item.getText().getText(), new char[] {'\n'}) %></td>
                          </tr>
                      <% } %>                          
                      <% if(item.getBatchID() != null && !item.getBatchID().equals("")){ %>
                        <tr>
                          <td class="identifier">><isa:translate key="b2b.order.details.batch.number"/></td>
                          <td class="value"><%= JspUtil.encodeHtml(item.getBatchID()) %></td>
                        </tr>
                      <% } %>
                      <%-- Batch element info start --%>
                     <% if (item.getBatchCharListJSP() != null && item.getBatchCharListJSP().size() != 0) {
                            for(int i=0; i<item.getBatchCharListJSP().size(); i++) {
                                inputValue = "";
                                for(int j=0; j<item.getBatchCharListJSP().get(i).getCharValTxtNum(); j++) {
                                    inputValue += item.getBatchCharListJSP().get(i).getCharValTxt(j) + ";";
                                } %>
                               <tr class="odd">
                                <% if (item.getBatchCharListJSP().get(i).getCharTxt().equals("")) { %>
                                   <td class="identifier"><%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharName()) %>:&nbsp;</td>
                                <% } else { %>
                                   <td class="value"><%= JspUtil.encodeHtml(item.getBatchCharListJSP().get(i).getCharTxt()) %>:&nbsp;</td>
                                <% } %>
                                   <td>&nbsp;<%= JspUtil.encodeHtml(inputValue) %></td>
                               </tr>
                          <% }
                       } %>
                      <%-- Batch element info end --%>
                   </table>
                   <%--   messages --%>
                   <%@ include file="/b2b/itemErrMsg.inc.jsp" %>
            <%-- ********** Seperator between two items ********** --%>
            <tr>
               <td colspan="<%= ui.getItemsColspan() %>" class="separator"></td>
            </tr>
            <%
              } // ui.showItemDetailButton() %>
          <% } //  ui.isBOMSubItemToBeSuppressed() %>
          </isa:iterate>
     </table>
     <% if (ui.isAccessible()) { %>
       <a name="end-table6" title="<isa:translate key="ecm.acc.item.dat.end"/>"></a>
     <% } %>
    </form>
    </div>

    </div>

    <div id="buttons">
        <a id="access-buttons" href="#access-header" title="<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>
        <ul class="buttons-3">
            <li><a href="#" onclick="backPressed();" <% if (ui.isAccessible()) { %> title="<isa:translate key="b2b.acc.sim.button.back"/>" <% } %> ><isa:translate key="b2b.order.simulate.button.back"/></a></li>
            <% if (ui.isOciTransfer() && ui.isBackendR3()){ %>
            <li><a href="#" onclick="ociPressed();" <% if (ui.isAccessible()) { %> title="<isa:translate key="b2b.acc.buttons.snd"/>" <% } %> ><isa:translate key="b2b.ord.sub.send.oci"/></a></li>
            <% } %>
            <% PaymentUI paymentUI = new PaymentUI(pageContext, ui.header); %>            
            <% if (paymentUI.isPaymentMaintenanceAvailable())  { %>
              <li><a href="#" onclick="maintainPaymentPressed();" <% if (ui.isAccessible()) { %> title="<isa:translate key="payment.button.maintain"/>" <% } %> ><isa:translate key="payment.button.maintain"/></a></li>                   
            <% } %>
            <% if ( (!ui.isBackendR3()) || (!ui.isOciTransfer()) ) { %>
            <li><a href="#" onclick="sendPressed();" <% if (ui.isAccessible()) { %> title="<isa:translate key="<%= ui.getAccSendKey() %>"/>" <% } %> ><isa:translate key="<%= ui.getSendKey() %>"/></a></li>
            <% } %>
        </ul>
    </div>

</body>
</html>
