<% /** ToDo:

   **/
%>

<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>
<%

    HeaderSalesDocument  header =
            (HeaderSalesDocument) request.getAttribute(MaintainBasketBaseAction.RK_HEADER);

    ItemList items =
            (ItemList) request.getAttribute(MaintainBasketBaseAction.RK_ITEMS);

    JspUtil.Alternator alternator =
            new JspUtil.Alternator(new String[] {"odd", "even" });

    String s = "";

%>




<html>
<head><title>Summary of your shopping cart</title></head>



<body>
    <div class="module-name"><isa:moduleName name="b2b/order/summarybasket.jsp" /></div>

<H1> Summary of your shopping cart </H1>


<form method="get" action='<isa:webappsURL name="/b2b/summarybasket.do"/>'>

  <jsp:useBean id="bskt"
               class="com.sap.isa.businessobject.order.Basket"
               scope="request" />

<h3> Ihr Auftrag wurde gespeichert</h3>
        <table border="1" cellspacing="0" cellpadding="1" bgcolor="#6688FF" width="100%">
            <tr>
                <td align="right">
                    <isa:translate key="b2b.order.display.ponumber"/>
                </td>
                <td align="right" bgcolor="#FFFFFF">
                    <%= header.getSalesDocNumber() %>
                </td>
                <td align="right" >
                    Gesamtpreis netto:
                </td>
                <td align="right" bgcolor="#FFFFFF">
                    <%= header.getNetValueWOFreight() %>
                </td>
                <td align="right"  >
                       Fracht:
                </td>
                <td align="right" bgcolor="#FFFFFF">
                   <%= header.getFreightValue() %>
                </td>
                <td align="right" >
                   Steuern:
                </td>
                <td align="right" bgcolor="#FFFFFF">
                   <%= header.getTaxValue() %>
                </td>
                <td align="right" >
                   Gesamtpreis brutto:
               </td>
                <td align="right" bgcolor="#FFFFFF">
                    <%= header.getGrossValue() %>
                </td>
            </tr>
        </table>



  <br>

    <table border="0" cellspacing="20" cellpadding="0">
        <tr>
            <td>
                <input type="submit" name="Hugo" value="zum Anfang">
            </td>
            <td>
                <input type="submit" name="transfer" value="transfer_ebp">
            </td>
        </tr>
    </table>


</form>


</body>
</html>