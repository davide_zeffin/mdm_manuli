<%--
********************************************************************************
    File:         showextrefobj.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      19.01.2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/01/19 $
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.helpvalues.action.*" %>
<%@ page import="com.sap.isa.helpvalues.*" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.MaintainBasketBaseAction" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.ExternalReferenceObjectUI" %>
<%@ page import="com.sap.isa.businessobject.item.ItemList" %>
<%@ page import="com.sap.isa.businessobject.item.ItemSalesDoc" %>
<%@ page import="com.sap.isa.core.TechKey" %>

<% String[] evenOdd = new String[] { "even", "odd" }; %>

<%
    ExternalReferenceObjectUI ui = new ExternalReferenceObjectUI(pageContext);
    ItemList items = (ItemList) request.getAttribute(MaintainBasketBaseAction.RK_ITEMS);
    String itemKeyValue = ui.getItemKeyValue();
    ItemSalesDoc item = null;
    boolean changeable = true;
	if (ui.isHeader()) {
		  changeable = ui.isElementEnabled("order.extRefObjects");
	} else {
		if (itemKeyValue != "" ) {
			TechKey itemKey = new TechKey(itemKeyValue);
	        item = items.get(itemKey);
			changeable = ui.isElementEnabled("order.item.extRefObjects");    
		}
	}
%>

<isa:contentType />
<isacore:browserType/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
  <title><%= ui.header.getExtRefObjectTypeDesc() %></title>
  
  <isa:stylesheets/>
  <link type="text/css" rel="stylesheet" href="../mimes/shared/style/stylesheet.css" />  
    <script type="text/javascript">
       <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
          type="text/javascript">
    </script>  

  <script type="text/javascript">
      var oldVins;


      function readOldVins() {
          var vins="";
          var vin;
          var vin0;
          for (var i = 0; i < <%= ui.getCounter() %>; i++) {
              vin = getVin(i);
              if (vin.length > 0) {
                  vins = vins + getVin(i) + ";";
              }
          }
          oldVins = vins;
      }

      function cancelVin() {
        window.close()
      }

      function submit_newpos() {
	
	     document.forms['add_vin'].elements['newposvin'].value=
	     document.getElementById('newposvincount').options[document.getElementById('newposvincount').selectedIndex].value;
	     document.forms['add_vin'].submit();
	     return true;
      }
      
      function getVin(id) {
      	var vin;
      	var field;
      	var fieldName;
      	fieldName = "vinNumber_".concat(id.toString());
      	// get the Element
      	field = getElement(fieldName);
      	if (field != null) {
      	    vin = field.value;
      	} else {
      	    vin = "";
      	}
      	return vin;
      }
      
      function sendConfirm(msg) {
          var vins="";
          var vin;
          var vin0="";
          var doublet = false;
          for (var i = 0; i < <%= ui.getCounter() %>; i++) {
              vin = getVin(i);
              if (vin.length > 0) {
                  doublet = checkDuplicateVin(vin, vins);
              	  if (doublet == false) {
                      vins = vins + getVin(i) + ";";
                  } else {
                      var a = 1 + i;
                      alert(msg);
                      var focusString = "document.add_vin.vinNumber_" + i + ".focus()";
		          eval(focusString);
                      return false;
                  }
              } else
              {
              		vins = vins + " ;";
              }
          }
          <% if (ui.isHeader()) { %>
          	opener.document.forms['order_positions'].addVinNum.value=vins;
          <% } else { %>
          	opener.document.forms['order_positions'].addVinNum_<%= ui.getPosNum() %>.value=vins;
          <% } %>
          for (var k = 0; k < <%= ui.getCounter() %>; k++) {
              vin = getVin(k);
              if (vin.length > 0) {
                  vin0 = vin;
                  k = <%= ui.getCounter() %>;
              }
          }
          <% if (ui.isHeader()) { %>
          	opener.document.forms['order_positions'].vinNum.value=vin0;
          <% } else { %>
          	opener.document.forms['order_positions'].vinNum_<%= ui.getPosNum() %>.value=vin0;
          <% } %>
	  window.close();
      }
      
      function checkDuplicateVin(vin, vins) {
          var doub = false;
          var ende;
          var hit2;
          var hit = vins.indexOf(vin);
          if (hit == 0) {
              ende = vin.length; 
              hit2 = vins.indexOf(';');
              if (ende == hit2) {
                  doub = true;
              }
          } else if (hit > 0) {
              var look = vins.substring(hit - 1);
              hit2 = look.indexOf(';');
              if (0 == hit2) {
                  var end = look.substring(1);
                  var hit3 = end.indexOf(';');
                  if (vin.length == hit3) {
                      doub = true;
                  }
              }
          }
          return doub;
      }
      
  </script>
      
       
  </script>
  

</head>

<body class="order" onLoad="readOldVins()">

  <div class="module-name"><isa:moduleName name="b2b/showextrefobj.jsp" /></div>


  <form id="add_vin" name="add_vin" method="post" action="<isa:webappsURL name="/b2b/showextrefobj.do"/>">
  
  <div id="document"> 


      <div class="document-items"> <!-- level: sub2 -->
      <div class="info">
          <span><isa:translate key="b2b.extrefobj.info.msg" arg0="<%= String.valueOf(ui.getVinHits()) %>" arg1="<%= JspUtil.encodeHtml(ui.header.getExtRefObjectTypeDesc()) %>" /></span>
      </div>      
          <table class="itemlist">
            <%
            int line = 0;
            %>
	      <tr>
	      <% if (ui.isHeader()) { %>
		     <th><isa:translate key="<%= ui.getHead() %>" arg0="<%= JspUtil.encodeHtml(ui.header.getExtRefObjectTypeDesc()) %>"/></th>
		  <% } else { %>
		     <th><isa:translate key="<%= ui.getHead() %>" arg0="<%= ui.header.getExtRefObjectTypeDesc() %>" arg1="<%= JspUtil.encodeHtml(item.getNumberInt()) %>" /></th>
		  <% } %>
	      </tr>
	      <%  for (int j = 0; j < ui.getCounter(); j++) { %>
              <tr class="<%= evenOdd[++line % 2]%>">             
                   <% String vinValue="";
				      if (ui.isHeader()) {
					      vinValue=ui.getVinValue(j);
				      } else {
					      if (ui.isNewPos()) { 
						        vinValue=ui.getVinValue(j);
					      } else {
						        vinValue=ui.getVinValue(j, ui.getItemKeyValue());
					      }
					  } 
			          if (vinValue != null) vinValue = vinValue.trim(); %>  
                  <td align="middle" class="value">                   
                      <% if (changeable) { %>
                          <input name="vinNumber_<%= j %>" id="vinNumber_<%= j %>" class="textinput-large" type="text" value="<%= JspUtil.encodeHtml(vinValue) %>"/>
       	   	          <% } else { %>
       	   	              <%= vinValue %>
       	   	          <% } %>
       	   	      </td>
              </tr>
	      <% } %>
          </table>
	  <input type="hidden" name="loops" value="<%= ui.getCounter() %>"/>
	  <input type="hidden" name="newposvin" value=""/>
	  <input type="hidden" name="level" value="<%= ui.getHeader() %>"/>
	  <% if (ui.isHeader()) { %> 
	     <input type="hidden" name="itemkey" value=""/>
	     <input type="hidden" name="linenum" value=""/>
	  <% } else { %> 
	     <input type="hidden" name="itemkey" value="<%= ui.getItemKeyValue() %>"/>
	     <input type="hidden" name="linenum" value="<%= ui.getPosNum() %>"/>	   
	  <% } %>	  
	   
      </div> <%-- document-items --%>
    </div> <%-- document --%>
    <%-- Buttons --%>
    <div id="buttons">
        <a id="access-buttons" href="#access-header" title="<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>
    
        <ul class="buttons-1">
            <li>
                <select id="newposvincount" size="1" title="<isa:translate key="b2b.acc.buttons.newpos.qt"/>" >
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                  <option value="20">20</option>
                  <option value="25">25</option>
                  <option value="30">30</option>
                  <option value="40">40</option>
                  <option value="50">50</option>
                </select>
            </li>
            <li>
            <input class="head" type="button" onclick="submit_newpos();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.extrefobj"/>" <% } %> value="<isa:translate key="b2b.buttons.extrefobj"/>" />
            </li>
        </ul>
        <ul class="buttons-3">
           <li>
            <input class="head" type="button" onclick="cancelVin();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cncl"/>" <% } %> value="<isa:translate key="b2b.order.display.submit.cancel"/>" />
	   </li>
	   <li>
            <input class="head" type="button" onclick="sendConfirm('<isa:translate key="b2b.extrefobj.double"/><% if (ui.isAccessible) { %>&nbsp;<isa:translate key="b2b.acc.extrefobj.double"/> <% } %>');" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.extrefobj.snd"/>" <% } %> value="<isa:translate key="product.jsp.addToBasket"/>" />
           </li>
        </ul>
        
    </div>
    <%--End  Buttons--%>

  </form>

</body>
</html>