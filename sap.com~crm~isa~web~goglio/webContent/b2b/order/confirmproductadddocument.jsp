<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.order.AddToBasketAction" %>

<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@page import="com.sap.isa.catalog.actions.ActionConstants"%>
<%@page import="com.sap.isa.catalog.uiclass.ProductsUI"%>
<%@page import="com.sap.isa.core.util.JspUtil"%>
<%@page import="com.sap.isa.isacore.action.marketing.ShowBestsellerAction"%>
<%@page import="com.sap.isa.isacore.action.ShowProductListHelper"%>
<html>
<head>
  <isa:includes/>
  <!-- <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet"> -->
  <script type="text/javascript">
    function showMessage() {
      message = window.open('<isa:webappsURL name="/b2b/order/confirmmessageadddocument.jsp"/>', '', 'width=300,height=25');
      message.moveTo((screen.width-300)/2,(screen.height-25)/2);
      message.focus();
      }
   </script>

</head>
<body>
  <div class="module-name"><isa:moduleName name="b2b/order/confirmproductadddocument.jsp" /></div>

  <form method="get" action='<isa:webappsURL name="/b2b/afteraddtodocument.do"/>' >
    <!-- control the forward after the call with an hidden field -->
    <input type="hidden" name="<%=AddToBasketAction.FORWARD_NAME%>" value="<%=(String)request.getAttribute(AddToBasketAction.FORWARD_NAME)%>" />
    <input type="hidden" name="<%=ActionConstants.RA_DETAILSCENARIO%>" value="<%=ShowProductListHelper.getValueFromRequest(request, ActionConstants.RA_DETAILSCENARIO, ShowProductListHelper.ENCODE_TO_HTML)%>" />
  </form>
  <%-- navigate to the action --%>
  <script type="text/javascript">
      // showMessage();
      document.forms[0].submit();
      // alert('<isa:UCtranslate key="product.jsp.confirmAddToBasket"/>');
  </script>

</body>
</html>