<%--
********************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      08.5.2001

    $Revision: #5 $
    $Date: 2002/06/10 $
********************************************************************************

 IMPORTANT:
       - Page only accessible by using 'b2b/b2b/documentlistselect.do'

--%>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.isacore.actionform.order.orderdownload.*" %>
<%@ page import="com.sap.isa.isacore.MassOrderDownloadBean" %>
<%@ page import="com.sap.isa.isacore.DocumentHandler" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>
<%@ taglib uri="/isa" prefix="isa" %>

	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>

  <script src="<isa:mimeURL name="b2b/jscript/frames.js"/>"
            type="text/javascript"></script>

<% BaseUI ui = new BaseUI(pageContext);%>
<%
//  MassOrderDownloadBean mdb = (MassOrderDownloadBean) userSessionData.getAttribute("massdownloadbean");
  /**
   * Still the progress obj is null.Have to show the error message
   *
   */
  Set successOrdersSet = (Set)userSessionData.getAttribute("downloadsuccess");
  Set failureOrdersSet = (Set)userSessionData.getAttribute("downloadfailure");

  int successDocCount = -1;
  int failureDocCount = -1;
  if(successOrdersSet!=null)
    successDocCount=successOrdersSet.size();
  if(failureOrdersSet!=null)
    failureDocCount=failureOrdersSet.size();

%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<head>
  <title>Document List</title>
  <isa:includes/>

  </head>
  <body class="workarea">
    <div id="organizer-search" class="module"></div>
    <div class="module-name"><isa:moduleName name="b2b/order/orderdownloadconfirm.jsp" /></div>

  <%-- Document List Area --%>
           <%
	   String orderids = "";
           %>
  <b>    <isa:translate key="massdwnld.confirmmsg.title"/><br> </b>

  <% if ( successDocCount < 0  && failureDocCount < 0) { %>
    <isa:translate key="massdwnld.confirmmsg.error"/>

    <% } %>

  <% if ( successDocCount > 0 ) { %>
    <isa:translate key="massdwnld.cnfrmmsg.sucodrs"/><br/><br/>
  <table border="0" cellspacing="0" cellpadding="0" width="20%">
    <tbody>
      <tr>
        <td>&nbsp;</td>
        <td>
		   <%
				  // Data table preamble
				  String tableTitle = WebUtil.translate(pageContext, "massdwnld.confirmmsg.title", null);
				  if (ui.isAccessible())  {

					int colno = 2;
					int rowno = successDocCount;
					String firstRow ="0";
					String columnNumber = Integer.toString(colno);
					String rowNumber = Integer.toString(rowno);
					firstRow = Integer.toString(1);
					String lastRow = Integer.toString(rowno);

		   %>
					<a href="#tableend1"
						id="tablebegin1"
						title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>" arg1="<%=rowNumber%>"
						arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
					</a>
		   <%
				   }

				// Data table preamble ends
		   %>



          <table class="organizer-list" border="0" cellspacing="0" cellpadding="2" width="100%">

          <%-- Tableheader --%>
          <thead>
             <tr>
               <%--
	       <th>order&nbsp;/<br><isa:translate key="status.sales.date"/></th>
	       --%>
               <th scope="col"><isa:translate key="b2b.status.dn.order"/></th>
		<th scope="col">
	       <img src="<isa:webappsURL name ="/b2b/mimes/images/icon_success.gif"/>"
	           alt="<isa:translate key="b2b.orderdwld.success"/>" border="0">
		</th>
             </tr>
           </thead>


          <%-- Tablerows --%>

           <%
	   String documentStatusKey = "";
              String documentValidToKey = "";
              String documentModificationKey = "";
              String even_odd = "odd";
           %>
           <%--
	   <isa:iterate id="orderheader" name="<%= DocumentListAction.RK_ORDER_LIST %>"
                        type="com.sap.isa.businessobject.header.HeaderSalesDocument">
                  documentStatusKey = "status.sales.status.".concat(orderheader.getStatus());
	   --%>
           <%
	   Iterator iter = successOrdersSet.iterator();
	   while(iter.hasNext()) {
	       String orderid = (String)iter.next();
	   %>
           <% String refDesSeparator = "/";
	    %>
           <tbody>
             <% if (even_odd.equals("odd")) {
                  even_odd = "even";
                } else {
                  even_odd = "odd";
                }
             %>
             <tr class="<%= even_odd %>">
	        <!-- status info -->
	       <td><var> <%= JspUtil.encodeHtml(orderid) %>
                   </var></td>
	       <td>
	       <img src="<isa:webappsURL name ="/b2b/mimes/images/icon_success.gif"/>"
	          alt="<isa:translate key="b2b.orderdwld.success"/>" border="0">
	       </td>
            </tr>
          </tbody>
	   <%
	   }
	   %>
	   <%--
	   </isa:iterate>
	   --%>

	  <%
		  // Data Table Postamble
		  if (ui.isAccessible())  {
	  %>
		  <a id="tableend1"
			href="#tablebegin1"
			title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>">
		  </a>
	  <%
		  }
		  // Data Table Postamble ends
	  %>

          </table></td>
        <td>&nbsp;</td>
      </tr>
    </tbody>
  </table>
<% } %>

  <% if ( failureDocCount> 0 ) { %>
    <isa:translate key="massdwnld.cnfmmsg.failodrs"/><br/><br/>
   <%
		  // Data table preamble
		  String tableTitle1 = WebUtil.translate(pageContext, "massdwnld.cnfmmsg.failodrs", null);
		  if (ui.isAccessible())  {

			int colno = 2;
			int rowno = failureDocCount;
			String firstRow ="0";
			String columnNumber = Integer.toString(colno);
			String rowNumber = Integer.toString(rowno);
			firstRow = Integer.toString(1);
			String lastRow = Integer.toString(rowno);

   %>
			<a href="#tableend2"
				id="tablebegin2"
				title="<isa:translate key="access.table.begin" arg0="<%=tableTitle1%>" arg1="<%=rowNumber%>"
				arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
			</a>
   <%
		   }

		// Data table preamble ends
   %>


  <table border="0" cellspacing="0" cellpadding="0" width="20%">
    <tbody>
      <tr>
        <td>&nbsp;</td>
        <td>
          <table class="organizer-list" border="0" cellspacing="0" cellpadding="2" width="100%">

          <%-- Tableheader --%>
          <thead>
             <tr>
               <%--
	       <th>order&nbsp;/<br><isa:translate key="status.sales.date"/></th>
	       --%>
               <th scope="col"><isa:translate key="b2b.status.dn.order"/></th>
               <th scope="col">
	       <img src="<isa:webappsURL name ="/b2b/mimes/images/icon_failure.gif"/>"
	          alt="<isa:translate key="b2b.orderdwld.failure"/>" border="0">
	       </th>

             </tr>
           </thead>


          <%-- Tablerows --%>

           <%
	   String documentStatusKey = "";
              String documentValidToKey = "";
              String documentModificationKey = "";
              String even_odd = "odd";
           %>
           <%--
	   <isa:iterate id="orderheader" name="<%= DocumentListAction.RK_ORDER_LIST %>"
                        type="com.sap.isa.businessobject.header.HeaderSalesDocument">
                  documentStatusKey = "status.sales.status.".concat(orderheader.getStatus());
	   --%>
           <%
	   Iterator iter = failureOrdersSet.iterator();
	   while(iter.hasNext()) {
	       String orderid = (String)iter.next();
	   %>
           <tbody>
             <% if (even_odd.equals("odd")) {
                  even_odd = "even";
                } else {
                  even_odd = "odd";
                }
             %>
             <tr class="<%= even_odd %>">
	        <!-- status info -->
	       <td><var> <%= JspUtil.encodeHtml(orderid) %>
                   </var></td>
	       <td>
	       <img src="<isa:webappsURL name ="/b2b/mimes/images/icon_failure.gif"/>"
	         alt="<isa:translate key="b2b.orderdwld.failure"/>" border="0">
	       </td>

            </tr>
          </tbody>
	   <%
	   }
	   %>
	   <%--
	   </isa:iterate>
	   --%>

	  <%
		  // Data Table Postamble
		  if (ui.isAccessible()) {
	  %>
		  <a id="tableend2"
			href="#tablebegin2"
			title="<isa:translate key="access.table.end" arg0="<%=tableTitle1%>"/>"></a>
	  <%
		  }
		  // Data Table Postamble ends
	  %>

          </table></td>
        <td>&nbsp;</td>
      </tr>
    </tbody>
  </table>
<% } %>

	<!-- Release the doc handler now for the mass download stuff-->
	<br/>
	<isa:translate key="massdwnld.savemsg.ordrs"/>
	<br/><br/>
	<form name="orderdownload" method="post" action='<isa:webappsURL name ="/b2b/orderDownload.do?exit=true"/>'>
	<%
		if(ui.isAccessible()) {
			String dwldBtnTxt = WebUtil.translate(pageContext, "odrdwnld.orderdownload.btn", null);
			String closeBtnTxt = WebUtil.translate(pageContext, "massdwnld.closebtn.txt", null);
			String unavailable = WebUtil.translate(pageContext, "access.unavailable", null);

	%>
	<%		if(successDocCount > 0 ) { %>
				&nbsp;&nbsp; <input type="submit" name="orderdownloadbtn"
				value="<isa:translate key="odrdwnld.orderdownload.btn"/>"
				title="<isa:translate key="access.button" arg0="<%=dwldBtnTxt%>" arg1=""/>"
				onClick="form_input().location.href='<isa:webappsURL name ="/b2b/orderDownload.do?download=true"/>';
					document.orderdownload.orderdownloadbtn.disabled=true;
					document.orderdownload.orderdownloadbtn.title='<isa:translate key="access.button"
					arg0="<%=dwldBtnTxt%>" arg1="<%=unavailable%>"/>';return false;" class="green">
			<% } %>
			&nbsp;&nbsp;<input type="submit" name="closebtn"
			title="<isa:translate key="access.button" arg0="<%=closeBtnTxt%>" arg1=""/>"
			value="<isa:translate key="massdwnld.closebtn.txt"/>" class="green"/>

	<%  } else {  %>
			<% if(successDocCount > 0 ) { %>
				&nbsp;&nbsp; <input type="submit" name="orderdownloadbtn"
				value="<isa:translate key="odrdwnld.orderdownload.btn"/>"
				onClick="form_input().location.href='<isa:webappsURL name ="/b2b/orderDownload.do?download=true"/>';
					document.orderdownload.orderdownloadbtn.disabled=true;return false;" class="green">
			<% } %>
			&nbsp;&nbsp;<input type="submit" name="closebtn"
			value="<isa:translate key="massdwnld.closebtn.txt"/>" class="green"/>
	<%  } %>






	</form>
</body>
</html>