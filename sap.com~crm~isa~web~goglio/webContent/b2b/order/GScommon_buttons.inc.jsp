<%--
********************************************************************************
    File:         GScommon_buttons.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Created:      05.04.2005
    Version:      1.0
********************************************************************************
--%>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ taglib uri="/isa" prefix="isa" %>
<% GenericSearchUIData ui = (GenericSearchUIData)request.getAttribute(GenericSearchUIData.RC_UICLASSINSTANCE); %>
<% if (ui.isResultListEnabled()) { %>
<% if ("predoc".equals(request.getParameter(GenericSearchUIData.RC_UI_INCLUDESTOSHOW))) { %>
    <%-- Show content for processing of PREDECESSOR DOCUMENTS --%>
    <script type="text/javascript">
      var closeButtonPressed = false;
      function closeDocument() {
          if (closeButtonPressed == false) { <%-- Can be pressed only once --%>
              closeButtonPressed = true;
              document.location.href = "<isa:webappsURL name ="b2b/genericsearchclose.do"/>"
          }
      }
    </script>
           </div> <%-- Closes <div id="document"> in file GScommon_header.inc.jsp --%>
           <div id="buttons">
                   <a id="access-buttons" href="#access-header" title= "Navigation bar of the document. Click on this link to get to the header." accesskey="n">
                           <img src="/b2b/mimes/images/1X1.gif" alt="" style="display:none" width="1" height="1" />
                   </a>
                   <ul class="buttons-1">
                   </ul>
                   <ul class="buttons-3">
                                           <li>
                                                   <a href="#" onclick="closeDocument();" title="<isa:translate key="gs.wl.backord.list.close"/> <isa:translate key="ecm.acc.button"/>"><isa:translate key="gs.wl.backord.list.close"/></a>
                                           </li>
                   </ul>
           </div>
        </div> <%-- Closes <div class="document-search-workarea"> in file GScommon_header.inc.jsp --%>
    
    <% } %>
<% } %>