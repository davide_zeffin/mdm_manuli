<%--
********************************************************************************
    File:         close_config.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Dendl
    Created:      09.07.2001
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/05/23 $
********************************************************************************
--%>
<html>
   <head>
   <script type="text/javascript">
      window.close(self);
   </script>
   </head>
  <body>Configuration is finished. Please close the window
    <div class="module-name">b2b/order/close_config.jsp</div>
  </body>
</html>

