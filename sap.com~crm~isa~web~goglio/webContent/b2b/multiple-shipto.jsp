<% /** This JSP displays a screen with the list of available shiptos
	   and allows to select the shiptos for which the GRID items are copied to
   **/
%>
<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="java.util.ArrayList"%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ include file="usersessiondata.inc" %>

<%
	ArrayList presetShipTos = new ArrayList();

	if (userSessionData.getAttribute("multiShipTos") != null) {
		presetShipTos = (ArrayList) userSessionData.getAttribute("multiShipTos");
	}
%>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>
			<isa:translate key="b2b.fs.title"/>
		</title>
 <isa:stylesheets/>
 <isa:includes/>
 <script type="text/javascript">
	function save_shiptos() {
		document.multishipto.multishiptobut.value="X";
		document.multishipto.refresh.value="X";
		document.multishipto.target ='form_input';
		window.close();
		document.multishipto.submit();
		return true;
	}
	function return_to_basket() {
		window.close();
		document.multishipto.target='form_input';
		document.multishipto.submit();
		return true;
	}

	<!--To set/reset the value depending on check/uncheck of check box-->
	function setValue(line, techKey) {
	  shiptocheckbox = "mshipto["+line+"]";
	  shiptoValue    = "mshiptoval["+line+"]";
	  if (document.getElementById(shiptocheckbox).checked == true){
		 document.getElementById(shiptoValue).value = techKey;

	  }else{
		 document.getElementById(shiptoValue).value = "";
	  }
	}

 </script>
	</head>

	<body class="ship">
	<form name="multishipto"  action="<isa:webappsURL name="b2b/multipleshipto.do"/>" method="POST">
		<div id="document">
			<div class="module-name"><isa:moduleName name="b2b/multiple-shipto.jsp" /></div>

			<table class="itemlist">

			<%//=request.getParameter("itemguid")%>
			<% int line = 0;%>
			  <isa:iterate id="shipTo"
				 name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
				 type="com.sap.isa.businessobject.ShipTo">
				 <tr>
					<%
					  //Required to default the pre-checked check boxes
					  String mshipTo= "", isChecked="";
					  if(presetShipTos.contains(shipTo.getTechKey().toString())){
						  mshipTo = shipTo.getTechKey().toString();
						  isChecked = "checked";
					  }
					%>
					<td>
						<input type="checkbox" id="mshipto[<%= line %>]" name="mshipto[<%= line %>]" value="" <%=isChecked%> onclick="setValue('<%=line%>','<%=shipTo.getTechKey()%>');">
					</td>
					<td>
						<input type="hidden" id="mshiptoval[<%=line %>]" name="mshiptoval[<%=line %>]" value="<%=mshipTo%>">
						<%= shipTo.getShortAddress() %>
					</td>
				 </tr>
				 <%line++;%>
				</isa:iterate>
			</table>
			<input type="hidden" name="multishiptobut" value="">
			<input type="hidden" name="refresh" value="">
		</div>
		<div>
			<table class="itemlist">
				<tr>
					<td>
					<input class="green" type="button" name="ok" value="<isa:translate key="b2b.docnav.ok"/>" onclick="save_shiptos();">
					<input class="green" type="button" name="cancel" value="<isa:translate key="b2b.order.display.submit.cancel"/>" onclick="return_to_basket();">
					</td>
				</td>
			</table>
		</div>
	</form>
	</body>
</html>