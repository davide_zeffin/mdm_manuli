<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore"  prefix="isacore" %>

<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectBase" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); %>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  		<title><isa:translate key="userSettings.jsp.title"/></title>
  		<isa:stylesheets/>
	</head>

	<body class="nodoc">
		<div>
			<div class="module-name"><isa:moduleName name="b2b/user_settings_info.jsp" /></div>
			<form action="">
				<div id="nodoc-first">
					<div id="nodoc-welcome">
	                	<p class="p1"><span><isa:translate key="userSettings.jsp.info"/></span></p>
					</div>
					<ul>
						<li>
							<a href="<isa:webappsURL name="b2b/showpwchange.do"/>" ><isa:translate key="userSettings.jsp.pwchange"/></a>
						</li>
						<isacore:ifShopProperty property = "backend" value = "<%=ShopData.BACKEND_CRM%>" >    
						    <li>
		  						<a href="<isa:webappsURL name="b2b/showaddresschange.do"/>" ><isa:translate key="userSettings.jsp.addresschange"/></a>
							</li>
						</isacore:ifShopProperty>
						<isacore:ifShopProperty property = "userProfileAvailable" value = "true" >
						    <li>
								<a href="<isa:webappsURL name="b2b/showprofile.do"/>"><isa:translate key="userSettings.jsp.profile"/></a>
							</li>
						</isacore:ifShopProperty>
					<% /* d041773 Message 2347632 2006 these settings do not have an impact in the B2B
						<li>
							<a href="<isa:webappsURL name="b2b/showusdchange.do"/>"><isa:translate key="userSettings.jsp.usdchange"/></a>
						</li> */ %>
					</ul>
				</div>
				<div>
					<ul>
						<isa:message id="UserInfo"
		                    name="<%=BusinessObjectBase.CONTEXT_NAME%>"
		                    type="<%=Message.INFO%>"
		                    ignoreNull="true">
		                    <li>
		                     	<div class="info">
		                     		<span>
										<%=UserInfo%>
									</span>
								</div>
							</li>
						</isa:message>
		
						<isa:message id="UserError"
		               		name="<%=BusinessObjectBase.CONTEXT_NAME%>"
		               		type="<%=Message.ERROR%>"
		                	ignoreNull="true">
		                	<li>
			                	<div class="error">
			                		<span>
			           					<%=UserError%>
			           				</span>
			           			</div>
			           		</li>
						</isa:message>
					</ul>
				</div>
			</div>
		</form>
	</body>
</html>