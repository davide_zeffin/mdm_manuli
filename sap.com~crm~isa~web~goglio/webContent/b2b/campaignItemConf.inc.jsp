<%--
********************************************************************************
    File:         campaignsHeaderConf.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.03.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/03/18 $
********************************************************************************
--%>
       <% if (ui.isItemCampaignListSet()) { 
            for (int iItemCamp = 0; i < ui.getNoItemCampaignEntries(); iItemCamp++)%>
              <tr class="odd">
                <td class="identifier" align="right"><isa:translate key="b2b.order.disp.camp"/>&nbsp;</td>
                <td colspan="<%= 7 + contractcolumn %>" style="border-right: none;"><%= ui.getItemCampaignId(iItemCamp) %><br/><%= ui.getItemCampaignDescs(iItemCamp) %></td>
              </tr>
         <% }
          } %>
