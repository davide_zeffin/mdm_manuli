<% /** This JSP displays a screen to enter or change an shipto
       author : 
   **/
%>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>

<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ taglib uri="/isa" prefix="isa" %>

  <%
    // if the backend is R/3, e-mail is not supported    
    boolean backendR3    = false;
  %>

  <isacore:ifShopProperty property="Backend" value ="<%= Shop.BACKEND_R3 %>">
    <% backendR3 = true; %>
  </isacore:ifShopProperty>
   <isacore:ifShopProperty property="Backend" value ="<%= Shop.BACKEND_R3_40 %>">
    <% backendR3 = true; %>
  </isacore:ifShopProperty>
   <isacore:ifShopProperty property="Backend" value ="<%= Shop.BACKEND_R3_45 %>">
    <% backendR3 = true; %>
  </isacore:ifShopProperty>
   <isacore:ifShopProperty property="Backend" value ="<%= Shop.BACKEND_R3_46 %>">
    <% backendR3 = true; %>
  </isacore:ifShopProperty>
  <isacore:ifShopProperty property="Backend" value ="<%= Shop.BACKEND_R3_PI %>">
    <% backendR3 = true; %>
  </isacore:ifShopProperty>

  <tr>
    <td colspan="3">
    <div class="module-name"><isa:moduleName name="b2b/shiptodetails.jsp" /></div>
    </td>
  </tr>

  <% boolean isPerson = false;
     String name1 = JspUtil.encodeHtml(address.getName1());
     if( (name1 == null) || (name1.equals("")) ){
         name1 = JspUtil.encodeHtml(address.getLastName());
         isPerson = true;
     } 
     String name2 = JspUtil.encodeHtml(address.getName2());
     if( (name2 == null) || (name2.equals("")) ){
         name2 = JspUtil.encodeHtml(address.getFirstName());
         isPerson = true;
     }      
  %>  

  <tr>
    <td class="identifier" colspan="2">
    	<label for="name1">
    		<isa:translate key="shipto.jsp.name1"/><% if (disabled.length() == 0) {%> * <%}%>  			
    	</label></td>
    <td class="value">
    <% if (isPerson == false) { %>
      <input type="text" class="textinput-large" name="name1" id="name1" value="<%=JspUtil.removeNull(name1)%>" size="35" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="NAME1" >
      <div class="error">           
          <%=errortext%><br />
        </div>
      </isa:message>             
     <% } else if (isPerson == true) { %>
      <input type="text" class="textinput-large" name="lastName" id="lastName" value="<%=JspUtil.removeNull(name1)%>" size="35" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="LASTNAME" >
      <div class="error">           
          <%=errortext%><br />
        </div>
      </isa:message>                  
     <% } %>                      
    </td>
  </tr>
  <tr>
  	<td class="identifier" colspan="2">
  		<label for="name2">
  			<isa:translate key="shipto.jsp.name2"/><% if (disabled.length() == 0) {%>  <%}%>
  		</label></td>
  	<td class="value">
  	<% if (isPerson == false) { %>
      <input type="text" class="textinput-large" name="name2" id="name2" value="<%=JspUtil.removeNull(name2)%>" size="35" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="NAME2" >
      <div class="error">
          <%=errortext%><br />
        </div>
      </isa:message>             
       <% } else if (isPerson == true) { %>
      <input type="text" class="textinput-large" name="firstName" id="firstName" value="<%=JspUtil.removeNull(name2)%>" size="35" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="FIRSTNAME" > 
      <div class="error">
          <%=errortext%><br />
        </div>
      </isa:message>                   
       <% } %>                         
    </td>
  </tr>
  <tr>
    <td class="identifier" colspan="2">
       <label for="street"><isa:translate key="shipto.jsp.street"/></label> /
       <label for="houseNumber"><isa:translate key="shipto.jsp.houseno"/></label>
    </td>
    <td class="value">
      <input type="text" class="textinput-large" name="street" id="street" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getStreet()))%>" size="35" <%= disabled %> />
      <input type="text" class="textinput-small" name="houseNumber" id="houseNumber" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getHouseNo()))%>" size="7" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="STREET" >
        <div class="error">
          <%=JspUtil.removeNull(errortext)%><br />
        </div>
      </isa:message>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="houseNo" >
        <div class="error">
          <%=JspUtil.removeNull(errortext)%><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <tr>
    <td class="identifier" colspan="2">
    	<label for="postalCode"><isa:translate key="shipto.jsp.zip"/></label> / 
    	<label for="city"><isa:translate key="shipto.jsp.city"/><% if (disabled.length() == 0) {%> * <%}%></label></td>
    <td class="value">
      <input type="text" class="textinput-small" name="postalCode" id="postalCode" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getPostlCod1()))%>" size="7" maxlength="10" <%= disabled %> />
      <input type="text" class="textinput-large" name="city" id="city" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getCity()))%>" size="35" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="POSTL_COD1" >
        <div class="error">
          <%=JspUtil.removeNull(errortext)%><br />
        </div>
      </isa:message>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="CITY" >
        <div class="error">
          <%=errortext%><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <tr>
    <td class="identifier" colspan="2"><label for="country"><isa:translate key="shipto.jsp.country"/><% if (disabled.length() == 0) {%> * <%}%></label></td>
    <td class="value">
      <input type="hidden" name="RegionList" value="" />

      <select class="textinput" name="country" id="country" <%= disabled %> onchange="loadRegionList()">
        <isa:iterate id="country" name="<%= ActionConstants.RC_COUNTRY_LIST %>"
            type="com.sap.isa.core.util.table.ResultData"
            resetCursor="true">
            <option value="<%=JspUtil.encodeHtml(country.getString(Shop.ID))%>"
                <%= address.getCountry().equals(country.getString(Shop.ID))?"selected=\"selected\"":"" %> >
            <%=JspUtil.encodeHtml(country.getString(Shop.DESCRIPTION))%>
            </option>
        </isa:iterate>
      </select>

      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="COUNTRY" >
        <div class="error">
          <%=JspUtil.removeNull(errortext)%><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <% ResultData regionList = (ResultData) request.getAttribute(ActionConstants.RC_REGION_LIST);
    if (regionList != null && regionList.getNumRows() > 0) {
  %>
  <tr>
    <td class="identifier" colspan="2"><label for="region"><isa:translate key="shipto.jsp.region"/><% if (disabled.length() == 0) {%> * <%}%></label></td>
    <td class="value">
      <select class="textinput" name="region" id="region" <%= disabled %>>
  	    <option value=""></option>
        <isa:iterate id="region" name="<%= ActionConstants.RC_REGION_LIST %>"
            type="com.sap.isa.core.util.table.ResultData"
            resetCursor="true">
            <option value="<%=JspUtil.encodeHtml(region.getString(Shop.ID))%>"
                <%= address.getRegion().equals(region.getString(Shop.ID))?"selected=\"selected\"":"" %> >
            <%=JspUtil.encodeHtml(region.getString(Shop.DESCRIPTION))%>
            </option>
        </isa:iterate>
      </select>
      <%-- <input type="text" name="region" value="<%=address.getRegion()%>" size="35" <%= disabled %> /> --%>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="REGION" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>
  
  <% ResultData taxCodeList = (ResultData) request.getAttribute(ActionConstants.RC_TAXCODE_LIST);
    if ( taxCodeList!= null &&  taxCodeList.getNumRows() > 0) {
  %>
  <tr>
    <td class="identifier" colspan="2"><label for="taxJurisdictionCode"><isa:translate key="shipto.jsp.county"/><% if (disabled.length() == 0) {%> * <%}%></label></td>
    <td class="value">
      <select class="textinput" name="taxJurisdictionCode" id="taxJurisdictionCode" <%= disabled %>>
        <isa:iterate id="taxCode" name="<%= ActionConstants.RC_TAXCODE_LIST %>"
            type="com.sap.isa.core.util.table.ResultData"
            resetCursor="true">
            <option value="<%=JspUtil.encodeHtml(taxCode.getString(User.TXJCD))%>"
                <%= address.getTaxJurCode().equals(taxCode.getString(User.TXJCD))?"selected=\"selected\"":"" %> >
            <%=JspUtil.encodeHtml(taxCode.getString(User.COUNTY))%>
            </option>
        </isa:iterate>
      </select>

      <% if(addressFormular.isEditable()) { %>
      <a href="#" class="icon" onclick="loadRegionList()"
        ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/refresh.gif") %>" 
              alt="<isa:translate key="shipto.jsp.counties.refresh"/>" 
      /></a>
      <% } %>

      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="taxJurisdictionCode" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="county" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <%}%>
  <%}%>
  <% if(!backendR3) { %> <!-- Email not available in R/3, so only show it, if other Backends are used -->
  <tr>
    <td class="identifier" colspan="2"><label for="email"><isa:translate key="shipto.jsp.mail"/></label></td>
    <td class="value"><input type="text" class="textinput-large" name="email" id="email" value="<%=JspUtil.encodeHtml(address.getEMail())%>" size="35" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="E_MAIL" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <% } %>
  
  <tr>
    <td class="identifier" colspan="2"><label for="telephoneNumber"><isa:translate key="shipto.jsp.telephone"/></label></td>
    <td class="value"><input type="text" class="textinput-large" name="telephoneNumber" id="telephoneNumber" value="<%=JspUtil.encodeHtml(address.getTel1Numbr())%>" size="35" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="tel1Numbr" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <tr>
    <td class="identifier" colspan="2"><label for="faxNumber"><isa:translate key="shipto.jsp.fax"/></label></td>
    <td class="value"><input type="text" class="textinput-large" name="faxNumber" id="faxNumber" value="<%=JspUtil.encodeHtml(address.getFaxNumber())%>" size="35" <%= disabled %> />
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="faxNumber" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>