<%--
Handles minibasketupdate from the catalog
--%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.core.SessionConst" %>
<%@ page import="com.sap.isa.isacore.DocumentHandler" %>

<%@ include file="/b2b/usersessiondata.inc" %>

<%  boolean homActivated = false; 
    boolean updateMinibasket = true;
    
    DocumentHandler documentHandler = (DocumentHandler)
        userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

    // Get minibasket information from requestcontext
    String netValue = documentHandler.getMiniBsktNetValue();
    String itemSize = documentHandler.getMiniBsktItemSize();
    String currency = documentHandler.getMiniBsktCurrency();
    // the doctype of the editable document displayed in minibasket
    String mb_doctype = documentHandler.getMiniBsktDoctype();
    
    if (netValue == null || netValue.trim().length() == 0) {
        updateMinibasket = false;
    }

    if (itemSize == null || itemSize.trim().length() == 0) {
        updateMinibasket = false;
    }
%>

    <isacore:ifShopProperty property = "homActivated" value ="true">
	    <% homActivated = true; %>
    </isacore:ifShopProperty>

	<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js" ) %>"
			type="text/javascript">
	</script>	 

	<script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
			type="text/javascript">
	</script>
	  	
    <script type="text/javascript">
    // alert("netValue   <%= netValue%>   itemsize <%= itemSize%>   updateMinibasket  <%= updateMinibasket%>");
   <!--
        function updateMinibasket() {
      
           if (isaTop().header.getElement("basketForm")) {
              // header object minibasket available => set up to date minibasket info
           <% if ("negotiatedContract".equals(mb_doctype)) { %>
                //note: when a 'negotiated contract' is edited only itemsize should be displayed in minibasket
                isaTop().header.setBasketLinkText("<%=itemSize%> <isa:translate key="b2b.header.minibasket.ncontract"/>");
           <% }
              else {
                //note: used for all sales documents except 'negotiated contracts'
                if ("-1".equals(itemSize) && "-1".equals(netValue)) { %>
                  //true when no editable document is available or editable document is no sales document
                  // path should never be used in HOM scenario %>
                  isaTop().header.setBasketLinkText("<isa:translate key="b2b.header.minibasket.default"/>");
             <% }
                else {
                  if ("0".equals(itemSize) && "0".equals(netValue)) {
                    // different minibasket descriptions for hom and standard
                    if (homActivated) { %>
                        isaTop().header.setBasketLinkText("<isa:translate key="b2b.header.hom.minibasket.empty"/>");
                 <% } 
                    else { %>
                        isaTop().header.setBasketLinkText("<isa:translate key="b2b.header.minibasket.empty"/>");              
                <%  }
                  } 
                  else {
                    //same description for standard b2b and hom scenario %>
                    isaTop().header.setBasketLinkText("<%=itemSize%> <isa:translate key="b2b.header.minibasket.l1"/> <%= netValue %> <%= JspUtil.encodeHtml(currency) %>");
               <% } 
                }
              } %>
            }
            else {
              // header object minibasket not available => don?t set minibaket info via this frame.
              // in this case minibasket value is taken from header.jsp.
            } 
        }
        
        function loadPage() {
          <% if (updateMinibasket) { %>
            // refresh minibasket in header
            updateMinibasket();
          <% } %>
        }

    //-->
     </script>