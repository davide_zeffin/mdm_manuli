<%--
********************************************************************************
    File:         organizer_menu.jsp.inc
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      September 2002
    Version:      1.0

    $Revision: #12 $
    $Date: 2002/03/01 $
********************************************************************************
* This include handle the display of the navigation menu.
* The String activeView; must be defined outside the include to use this include
* It describes the active organizer.
* The include uses the java object TabStribHelper to diplay the menu entries.
* See class documentation for further hints.
********************************************************************************
--%>

<%-- must be defined:  String activeView; !! --%>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.DocumentHandler" %>
<%@ page import="com.sap.isa.isacore.TabStripHelper" %>
<%@ page import="com.sap.isa.isacore.TabStripHelper.TabButton" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.core.SessionConst" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.*" %>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>

<%
	/* get user session data object */
	UserSessionData userSessionData =
	UserSessionData.getUserSessionData(request.getSession());

	DocumentHandler documentHandler = (DocumentHandler)
    userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
    
    OrganizerMenuUI ui = new OrganizerMenuUI(pageContext);
    
    //Initialize boolean variables in UI-Class
	ui.setCustomerdocsearch(true);
	ui.setDocsearch((documentHandler.getOrganizerParameter("docsearchversion").equals("NEW") ? false : true));
	ui.setGenericdocsearch((documentHandler.getOrganizerParameter("docsearchversion").equals("NEW") ? true : false));
	ui.setProductsearch(true);
	ui.setCustomersearch(true);
	ui.setCatalog(true);
	ui.setCatalogonly(false);
	ui.setExternalCatalogVisible(false);

%>

    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
            type="text/javascript">
    </script>

    <script type="text/javascript">
    <!--
      function load_customerdocSearch() {
        var screenName = "<%= JspUtil.encodeHtml(documentHandler.getOrganizerParameter("documentsearch.name")) %>"
        parent.organizer_content.location.href=("<isa:webappsURL name="/appbase/genericsearch.jsp?genericsearch.name="/>" + screenName);
        <%-- parent.organizer_content.location.href="<isa:webappsURL name="/b2b/customerdocumentlist.do"/>"; --%>
      }

      function load_docSearch() {
		parent.organizer_content.location.href="<isa:webappsURL name="/b2b/documentlistselect.do"/>";
        <%-- isaTop().header.busy(form_input()); --%>
        <%-- form_input().location.href="<isa:webappsURL name="/b2b/catalogend.do"/>"; --%>
      }

      function load_genericdocSearch() {
        var screenName = "<%= JspUtil.encodeHtml(documentHandler.getOrganizerParameter("documentsearch.name")) %>"
        parent.organizer_content.location.href=("<isa:webappsURL name="/appbase/genericsearch.jsp?genericsearch.name="/>" + screenName);
        <%-- isaTop().header.busy(form_input()); --%>
        <%-- form_input().location.href="<isa:webappsURL name="/b2b/catalogend.do"/>"; --%>
      }

      function load_ProductSearch() {
        parent.organizer_content.location.href="<isa:webappsURL name="/b2b/initquicksearch.do"/>";
        <%-- isaTop().header.busy(form_input()); --%>
        <%-- form_input().location.href="<isa:webappsURL name="/b2b/catalogend.do"/>"; --%>
      }

      function load_CustomerSearch() {
		var screenName = "<%= JspUtil.encodeHtml(documentHandler.getOrganizerParameter("genericcustsearch.name")) %>"
		parent.organizer_content.location.href=("<isa:webappsURL name="/appbase/genericsearch.jsp?genericsearch.name="/>" + screenName);		
      }

      function load_catalog() {
          isaTop().header.busy(form_input());
          parent.organizer_content.location.href="<isa:webappsURL name="/catalog/showLastVisited.do"/>";
      }
    //-->
    </script>

	<isacore:ifShopProperty property="soldtoSelectable" value ="false">
    	<% 	ui.setCustomersearch(false);  %>
	</isacore:ifShopProperty>

	<isacore:ifShopProperty property="homActivated" value ="false">
		<% 	ui.setCustomerdocsearch(false); %>
	</isacore:ifShopProperty>

	<isacore:ifShopProperty property="homActivated" value ="true">
		<% 	ui.setCatalog(false);
		    ui.setGenericdocsearch(false); 
       		ui.setDocsearch(false); 
			ui.setProductsearch(false); %>
  	</isacore:ifShopProperty>
  
	<isacore:ifShopProperty property = "internalCatalogAvailable" value ="false">
		<% 	ui.setCatalog(false); 
			ui.setProductsearch(false); %>
	</isacore:ifShopProperty>  

	<isacore:ifShopProperty property = "ociAllowed" value ="true">
		<% 	ui.setExternalCatalogVisible(true); %>
	</isacore:ifShopProperty>  

<%
/* All menu entries are defined within this section */
	TabStripHelper tabStrip = ui.getTabStrip(activeView);
%> 
  
<%-- !!!!!!!!!!!!!!!!!!!! The Html Stuff starts here !!!!!!!!!!!!!!!!!!!!!!!!!! --%>

<%-- no HTML header only include --%>

<% if (ui.isAccessible) { %>
	<a href="#end-navigation" title="<isa:translate key="b2b.org_nav.acc.tabstart"/>"></a>
<% } %>

<%	tabStrip.setActiveButton(activeView);
	documentHandler.setActualOrganizer(activeView);
%>	
  <%@ include file="/appbase/tabstrip.inc.jsp" %>
<%	
	/* show catalog headline only */
    if (ui.getCatalogonly()) { %>
		<li class="active-first">
			<a href="#" <% if (ui.isAccessible) { %> title="<isa:translate key="access.tab.cat.selected"/>" <% } %> >
				<%if (ui.getExternalCatalogVisible()){%>
					<isa:translate key="b2b.make_new_doc_start.catalogs"/>
				<%}else{%>
					<isa:translate key="b2b.make_new_doc_start.catalog"/>
				<%}%>
			</a>
		</li>
<% 			
 	} 
%>
	</ul>
</div><%--navigator--%>

<% if (ui.isAccessible) { %>
	<a name="end-navigation" title="<isa:translate key="b2b.org_nav.acc.tabend"/>"></a>
<% } %>