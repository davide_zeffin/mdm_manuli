<%-- This JSP displays the PasswordChange screen to enter a new password --%>

<%@ page import="com.sap.isa.isacore.action.b2b.PWChangeAction" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.user.action.UserActions" %>
<%
	IsaBaseUI ui = new IsaBaseUI(pageContext);
	
	//Get the max password length, depending on the backend.
	int passwdLength = UserActions.getPasswordLength(pageContext);
%>


<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<title><isa:translate key="b2b.login.pwchange.title"/></title>
    	<isa:stylesheets/>

		<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/buttons.js") %>"
			type="text/javascript">
		</script>
    	
    	<script type="text/javascript">
    	<!--
    		function submitForm(iname, ivalue){
    			document.getElementById('dummy').name = iname;
    			document.getElementById('dummy').value = ivalue;
    			document.getElementById('changePW').submit();
    		}
            function return_event(){
                submitForm('<%=JspUtil.encodeHtml((String)request.getAttribute(PWChangeAction.ACTION_NAME))%>', '<isa:translate key="b2b.login.pwchange.submit"/>');
            }
    	//-->
    	</script>
    	
  	</head>
  	
<body class="usersettings">

<div class="module-name"><isa:moduleName name="b2b/pwchangeusersettings.jsp" /></div>

<h1><isa:translate key="b2b.login.pwchange.head"/></h1>
	
<div id="scrollable-selection">
<div class="selection">

	<form method="post" action='<isa:webappsURL name="/b2b/pwchangeusersettings.do"/>' id="changePW">
		<input type="hidden" id="dummy" />
		<input type="hidden" name="<%=PWChangeAction.FORWARD_NAME%>" value="<%=JspUtil.encodeHtml((String)request.getAttribute(PWChangeAction.FORWARD_NAME))%>" />
		<div class="login">
		<% if (ui.isAccessible) { %>
			<a href="#end-login" name="start-login" title="<isa:translate key="b2b.login.pwchange.endpwchange"/>"></a>
		<% } %>
			<ul>
				<isa:message id="errortext" name="<%=BusinessObjectBase.CONTEXT_NAME%>" type="<%=Message.ERROR%>" > 
					<li>
						<div class="error">	
							<span><%=errortext%></span>
						<div> 
					</li>
				</isa:message>
				<li class="label-input">
					<div>
						<label for="oldpw"><isa:translate key="b2b.login.pwchange.oldpwd"/></label><br/>
						<input type="password" id="oldpw" class="input-1" name="<%=PWChangeAction.PN_OLDPASSWORD%>" />
					</div>
				</li>
				<li class="label-input">
					<div>
						<label for="newpw"><isa:translate key="b2b.login.pwchange.passwd"/></label><br/>
						<input type="password" id="newpw" class="input-1" name="<%=PWChangeAction.PN_PASSWORD%>" maxlength="<%=passwdLength%>" />
					</div>
				</li>
				<li class="label-input">
					<div>
						<label for="reppw"><isa:translate key="b2b.login.pwchange.passwdverify"/></label><br/>
						<input type="password" id="reppw" class="input-1" name="<%=PWChangeAction.PN_PASSWORD_VERIFY%>" maxlength="<%=passwdLength%>" onkeypress="checkreturn(event)" />
					<div>
				</li>
				<li class="label-input">
					<div class="button1">
						<a href="#" onclick="submitForm('<%=JspUtil.encodeHtml((String)request.getAttribute(PWChangeAction.ACTION_NAME))%>', '<isa:translate key="b2b.login.pwchange.submit"/>');" class="button" name="<%=JspUtil.encodeHtml((String)request.getAttribute(PWChangeAction.ACTION_NAME))%>" title="<isa:translate key="b2b.login.pwchange.submit"/>: <isa:translate key="b2b.login.pwchange.title"/>">
							<isa:translate key="b2b.login.pwchange.submit"/>
						</a>
						&nbsp;
								
						<a href="#" onclick="submitForm('<%=PWChangeAction.ACTION_CANCEL%>', '<isa:translate key="b2b.login.pwchange.cancel"/>');" class="button" name="<%=PWChangeAction.ACTION_CANCEL%>" title="<isa:translate key="b2b.login.pwchange.cancel"/>: <isa:translate key="b2b.login.pwchange.cancel"/>" >
							<isa:translate key="b2b.login.pwchange.cancel"/>
						</a>
						<br/>
					</div>
				</li>
				<isa:message id="infotext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
		 				type="<%=Message.INFO%>" >
		 			<li>
		 				<div class="info">
							<span><%=infotext%></span>
		 				</div>			
		 			</li>
			   </isa:message>
			</ul>
			<% if (ui.isAccessible) { %>
				<a href="#start-login" name="end-login" title="<isa:translate key="b2b.login.pwchange.startpwchange"/>"></a>
			<% } %>	
		</div> <%-- login --%>
	</form>

</div> <%-- selection --%>
</div>

</html>