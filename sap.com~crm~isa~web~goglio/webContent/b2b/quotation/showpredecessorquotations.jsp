<%--
********************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      29.11.2001

    $Revision: #1 $
    $Date: 2001/11/29 $
********************************************************************************
--%>

<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.quotation.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ include file="/b2b/checksession.inc" %>
<%@ taglib uri="/isa" prefix="isa" %>

<% BaseUI ui = new BaseUI(pageContext);

   String docCount = (String) request.getAttribute(SelectPredecessorQuotationAction.RK_QUOTE_COUNT);
   String processType = request.getParameter(SelectPredecessorQuotationAction.RK_PROCESS_TYPE);

   String theme    = "",
          language = "";

    JspUtil.Alternator alternator =
            new JspUtil.Alternator(new String[] {"odd", "even" });

    String title      = null;
    String tableHeader = null;
    title = WebUtil.translate(pageContext, "b2b.selectpredecessordocument.qt", null);
    tableHeader = WebUtil.translate(pageContext, "b2b.selprddoc.numfound.qt", new String[] { String.valueOf(docCount) });
%>


<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

  <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
     <title><isa:translate key="b2b.selectpredecessordoc.titel"/></title>
     <isa:stylesheets/>
  <!--
  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css")%>" type="text/css" rel="stylesheet">
  -->
   <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
        type="text/javascript"> 
    </script>        
    <script type="text/javascript">
    <!--
        function goBack() {
            getElement("createfromtemplate").submit();
        }
    //-->
    </script>  
        
</head>
  <body class="showpredecessorquotations">
    
    <div class="module-name"><isa:moduleName name="b2b/quotation/showpredecessorquotations.jsp" /></div>


  <% if ( docCount != null ) { %>
  
      <h1><%= title %></h1>
	  <div class="predecessors-doc">
      <div class="opener"><%= tableHeader %></div>
               
       	<%
       	    // Data Table Preamble
			int colno = 4;
			String columnNumber = Integer.toString(colno);
			String rowNumber = docCount;
			String firstRow = Integer.toString(1);
			String lastRow = docCount;
			String thead = WebUtil.translate(pageContext, "b2b.quotation.summary.quotation",null);
		%>
		<%
			if (ui.isAccessible())
			{
		%>
			<a href="#tableend"
				id="tablebegin"
				title="<isa:translate key="access.table.begin" arg0="<%=thead%>"
				arg1="<%=rowNumber%>" arg2="<%=columnNumber%>" arg3="<%=firstRow%>" arg4="<%=lastRow%>"/>">
				</a>
		<%
			}
		%>

       <table class="filter-result" border="0" cellspacing="0" cellpadding="3">

              <%-- Tableheader --%>
              <thead>
                 <tr>
                   <th><isa:translate key="b2b.selectpredecessordoc.num"/></th>
                   <th><isa:translate key="b2b.selectpredecessordoc.refno"/></th>
                   <th><isa:translate key="b2b.selectpredecessordoc.name"/></th>
                   <th><isa:translate key="b2b.selectpredecessordoc.date"/></th>
                 </tr>
              </thead>
              <%-- Tablerows --%>
			  <tbody>
               <isa:iterate id="orderheader" name="<%= SelectPredecessorQuotationAction.RK_QUOTE_LIST %>"
                            type="com.sap.isa.businessobject.header.HeaderSalesDocument">
                 <tr class="<%= alternator.next() %>">
                   <% if(ui.isAccessible()) { %>
					   <td>
						 <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>
						 <%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input"
						   title="<%=thead%>:<%= orderheader.getSalesDocNumber()%>">
						   <%= orderheader.getSalesDocNumber() %>
						 </a>
					   </td>
					   <td>
						 <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>
						 <%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input"
						 title="<%=thead%>:<%= orderheader.getPurchaseOrderExt()%>">
						   <%= orderheader.getPurchaseOrderExt() %>
						 </a>
					   </td>
					   <td>
						 <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>
						  <%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input"
						  title="<%=thead%>:<%= orderheader.getDescription()%>">
						   <%= orderheader.getDescription() %>
						 </a>
					   </td>
					<% } else { %>
					   <td>
						 <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>
							<%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input">
						   <%= orderheader.getSalesDocNumber() %>
						 </a>
					   </td>
					   <td>
						 <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>
						 <%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input">
						   <%= orderheader.getPurchaseOrderExt() %>
						 </a>
					   </td>
					   <td>
						 <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>
						  <%= DocumentStatusDetailPrepareAction.createParameter(orderheader) %>" target="form_input">
						   <%= orderheader.getDescription() %>
						 </a>
					   </td>
					<% } %>
                   <td>
                     <%= orderheader.getChangedAt() %>
                   </td>
                 </tr>               
               </isa:iterate>
              </tbody>
           </table>

           <%
		   	// Data Table Postamble
		   	if (ui.isAccessible())
		   	{
		   %>
		   		<a id="tableend"
		   			href="#tablebegin"
		   			title="<isa:translate key="access.table.end"
		   				arg0="<%=thead%>"/>"></a>
		   <%
		   	}
		   %>
		         
      </div>      
      <%-- buttons --%>
       <form id="createfromtemplate" method="get" action="<isa:webappsURL name ="b2b/updatedocumentview.do"/>" >
           <div  id="buttons">
               <ul class="buttons-1">
                   <li>
                       <a href="#" onclick="goBack();" <% if (ui.isAccessible) { %> title="<isa:translate key="error.jsp.back"/>" <% } %> ><isa:translate key="error.jsp.back"/></a>
                   </li>
               </ul>
           </div>
       </form>
  <% } %>

</body>
</html>