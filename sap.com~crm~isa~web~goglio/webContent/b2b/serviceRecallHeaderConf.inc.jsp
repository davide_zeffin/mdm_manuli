<%--
********************************************************************************
    File:         serviceRecallHeaderConf.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      13.06.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/05/13 $
********************************************************************************
--%>
<% if (ui.header.getRecallId() != null && ui.header.getRecallId().length() > 0) { %>
    <tr class="odd">
        <td class="identifier"><isa:translate key="b2b.order.disp.recall"/></td>
        <td class="orderHeadPrice" style="border-right: none;"><%= JspUtil.encodeHtml(ui.header.getRecallId()) %>&nbsp;<%= JspUtil.encodeHtml(ui.header.getRecallDesc()) %></td>
    </tr>
<% } %>
