<%--
********************************************************************************
    File:         calendar.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Martin Schley
    Created:      3.8.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/06/07 $
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ include file="usersessiondata.inc" %>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
  <TITLE>Calendar&nbsp;&nbsp;&nbsp;</TITLE>
</HEAD>

<isa:includes/>
<!-- <link href="<%= WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css") %>"
          type="text/css" rel="stylesheet"> -->

<SCRIPT LANGUAGE="JavaScript">

    //newWin = window;

    // USE THE JAVASCRIPT-GENERATED DOCUMENTS (calDocTop, calDocBottom) IN THE FRAMESET

    calDocFrameset =
        "<FRAMESET ROWS='70,*' FRAMEBORDER='0' BORDER='0'>\n" +
        "  <FRAME NAME='topCalFrame' SRC='javascript:parent.opener.calDocTop' SCROLLING='no'>\n" +
        "  <FRAME NAME='bottomCalFrame' SRC='javascript:parent.opener.calDocBottom' SCROLLING='no'>\n" +
        "</FRAMESET>\n";

    document.write(calDocFrameset);

</SCRIPT>

</HTML>
