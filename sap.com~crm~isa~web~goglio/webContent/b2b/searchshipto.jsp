<% /** This JSP displays a screen to enter or change an shipto
       author : 
   **/
%>

<%@ page import="com.sap.isa.isacore.action.SearchShiptoAction" %>
<%@ page import="com.sap.isa.isacore.action.ShowShipToAction" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.TechKey" %>
<%@ include file="usersessiondata.inc" %>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>



<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <title>1<isa:translate key="shipto.jsp.title"/></title>

    <title>2<isa:translate key="shipto.jsp.title.display"/></title>


  <link href="<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css" ) %>" type="text/css" rel="stylesheet">

  <script type="text/javascript">
    function z_searchshiptos() {
       document.submit();
    }    
  </script>


<% ShiptoSearch search = (ShiptoSearch) request.getAttribute(SearchShiptoAction.PARAM_SHIPTOSEARCH);%>

<% String itemKey = (String) request.getParameter("SEARCHSHIPTOITEMKEY"); %>

<% userSessionData.setAttribute("SEARCHSHIPTOITEMKEY", itemKey);%> 

</head>

<body class="workarea">

<div id="searchshipto" class="module">
  <div class="module-name"><isa:moduleName name="b2b/searchshipto.jsp" /></div>

  <form method="post" action='<isa:webappsURL name="/b2b/searchshipto.do" />'  >

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="ship">
      <td colspan="3"><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="10" alt="" border="0"></td>
  </tr>
  <tr class="ship">
    <td class="opener" valign="middle">


            <b><isa:translate key="shiptosearch.jsp.title"/></b>

   </td>
    <td><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headLineEnd_white2darkblue.gif") %>" width="8" height="17" alt="" border="0"></td>
    <td><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="130" height="1" alt="" border="0"></td>
  </tr>
</table>

<table class="list" width="100%" border="0" cellspacing="0" cellpadding="1">



	<tr>
  		<td class="odd" colspan="2"><isa:translate key="shiptosearch.shiptopartnerid"/></td>
  		<td class="even">

		<input type="text" class="textInput" name="partnerId" value="<%= JspUtil.removeNull(request.getAttribute(SearchShiptoAction.PARAM_SHIPTO_BPARTNERID))%>" size="35" >

  		</td>
	</tr>

  <tr>
    <td class="odd" colspan="2"><isa:translate key="shipto.jsp.name"/></td>
    <td class="even">

      <input type="text" class="textInput" name="name1" value="<%= JspUtil.removeNull(request.getAttribute(SearchShiptoAction.PARAM_NAME1))%>" size="35" >

    </td>
  </tr>

  <tr>
    <td class="odd" colspan="2"></td>
    <td class="even">

      <input type="text" class="textInput" name="name2" value="<%= JspUtil.removeNull(request.getAttribute(SearchShiptoAction.PARAM_NAME2))%>" size="35" >

    </td>
  </tr>
<%--
  <tr>
    <td class="odd" colspan="2"><isa:translate key="shipto.jsp.street"/></td>
    <td class="even">

      <input type="text" class="textInput" name="street" value="<%= JspUtil.removeNull(request.getAttribute(SearchShiptoAction.PARAM_STREET))%>" size="35" >

    </td>
  </tr>

  <tr>
    <td class="odd" colspan="2"><isa:translate key="shipto.jsp.zip"/></td>
    <td class="even">

      <input type="text" class="textInput" name="zipCode" value="<%= JspUtil.removeNull(request.getAttribute(SearchShiptoAction.PARAM_ZIPCODE))%>" size="35" >

    </td>
  </tr>

  <tr>
  	<td class="odd" colspan="2"><isa:translate key="shipto.jsp.city"/></td>
    <td class="even">

      <input type="text" class="textInput" name="city" value="<%= JspUtil.removeNull(request.getAttribute(SearchShiptoAction.PARAM_CITY))%>" size="35" >

    </td>
  </tr>
  <tr>
--%>  
    <td colspan="3" class "odd">
        <isa:message id="errortext" name="search" type="<%=Message.ERROR%>" property="" ignoreNull="true">
        <div class="error" >
          <br><%= JspUtil.removeNull(errortext) %>
        </isa:message>
        </div>
    </td>
   </tr>
   <tr>
     <td colspan="3" class "odd">
      <% if ((request.getAttribute(SearchShiptoAction.SHIPTORESULT) != null) &&
               (((ResultData)request.getAttribute(SearchShiptoAction.SHIPTORESULT)).getNumRows() == 0)) { %>
          <div class="error" >
          <isa:translate key="shiptosearch.jsp.noentry"/> 
          </div>
      <% } %>
    </td>
  </tr>
</table>


<table class="shipto" border="0" cellspacing="0" cellpadding="5" width="100%">
  <tr>
      <td align="right">

          <input class="green" type="submit" name="<%=SearchShiptoAction.PARAM_SEARCH%>" value="<isa:translate key="shiptosearch.jsp.search"/>">
          &nbsp;&nbsp;&nbsp;
          <input class="green" type="submit" name="<%=SearchShiptoAction.PARAM_CANCEL%>" value="<isa:translate key="shipto.jsp.cancel"/>">

          

      </td>
  </tr>
</table>
  </form>
</div>

</body>
</html>


