<%-- import the taglibs used on this page --%>

<%//@ page import="java.util.*" %>
<%//@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ taglib uri="/isa" prefix="isa" %>
<%
  String thisPage = "";
  String displayString = "";
  if ( request.getParameter("multipleshipto") != null &&
       request.getParameter("multipleshipto").length() > 0 ) {
	thisPage = "b2b/multiple-shipto.jsp";
	displayString = "Ship-tos";
  }
  if ( request.getParameter("multiplereqdlvdate") != null &&
       request.getParameter("multiplereqdlvdate").length() > 0 ) {
	thisPage = "b2b/multiple-reqdate.jsp";
	displayString = "Requested Delivery date";
  }
%>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
    	<title>
        	<isa:translate key="b2b.fs.title"/>
	    </title>
	<isa:stylesheets/>
	<script type="text/javascript">
		window.showModelessDialog('<isa:webappsURL name="<%=thisPage%>"/>','Multiple <%=displayString%>','dialogHeight: 300px; dialogWidth: 400px; dialogTop: 338px; dialogLeft: 300px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: Yes;');
	</script>

	</head>
	<body>
		Select the <%=displayString%> for which the item has to be copied.
	</body>
</html>