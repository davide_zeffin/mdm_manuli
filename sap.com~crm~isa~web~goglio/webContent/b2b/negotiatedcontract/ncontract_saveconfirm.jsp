<%--
********************************************************************************
    File:         ncontract_saveconfirm.jsp
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      17.01.2002
    Version:      1.0

    $Revision:$
    $Date:$
********************************************************************************
--%>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.businessobject.header.HeaderNegotiatedContract" %>
<%@ page import="com.sap.isa.isacore.action.negotiatedcontract.NegotiatedContractConstants" %>
<%@ page import="com.sap.isa.core.util.*" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="/b2b/usersessiondata.inc" %>
<%@ include file="/b2b/checksession.inc" %>

<isa:contentType />

<%
    BaseUI ui = new BaseUI(pageContext);
    HeaderNegotiatedContract header	= (HeaderNegotiatedContract) request.getAttribute(NegotiatedContractConstants.RC_HEADER);
    String docNumber = "";
    if (header != null) {
       docNumber = header.getSalesDocNumber();
    }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<HEAD>
	<isa:stylesheets/> <%-- Stylesheet --%>

    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
          type="text/javascript">
    </script>

    <script type="text/javascript">
      // function for the onload()-event

      function loadPage() {
        //sets minibasket information back to default after document has been saved
        isaTop().header.document.forms['basketForm'].miniBasket.value="<isa:translate key="b2b.header.minibasket.default"/>";
		isaTop().header.setBasketLinkText('<isa:translate key="b2b.header.minibasket.default"/>');
      }
    </script>
</HEAD>

<BODY class="message-workarea" onload="loadPage();">
    <div class="module-name"><isa:moduleName name="b2b/negotiatedcontract/ncontract_saveconfirm.jsp" /></div>
    <h1><span><isa:translate key="b2b.nct.title.conf.save"/></span></h1>
    <div id="document">
        <div class="document-header">
            <div class="header-basic">
               <p><isa:translate key="b2b.nct.confirm1.txt1" arg0="<%= docNumber %>" /></p>
      	       <p><isa:translate key="b2b.nct.confirm1.txt2"/></p>
      	       <p><isa:translate key="b2b.nct.confirm1.txt3"/></p>
      	          <UL>
			          <LI><A href='<isa:webappsURL name="b2b/updatedocumentview.do"/>'>
			                  <isa:translate key="b2b.nct.confirm1.txt4"/></A><br>
			          </LI>
        	          <LI><A href='<isa:webappsURL name="b2b/closenegotiatedcontract.do?history=true"/>'>
        	                  <isa:translate key="b2b.nct.confirm1.txt5"/></A>
        	          </LI>
	  		      </UL>
            </div> <%-- End header basic --%>
        </div> <%-- document header --%>
    </div> <%-- end document --%>
</BODY>

</HTML>