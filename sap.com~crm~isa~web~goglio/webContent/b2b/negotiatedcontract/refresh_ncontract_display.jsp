<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.negotiatedcontract.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%
        // find out, if someone pressed the refresh button of the browser
        String req;
        boolean refresh = false;

        req = (String) request.getParameter("refresh");
        if (req != null && req.equals("1")) {
            refresh = true;
        }
%>

<%
    BaseUI ui = new BaseUI(pageContext);
%>

<!DOCTYPE html PUBLIC "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">


  <head>
    <title>SAP Internet Sales B2B</title>

	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>

    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
            type="text/javascript">
    </script>

  <script type="text/javascript">


    function resize(direction, frameObj) {
      if (direction == "min") {
        if (frameObj == "history")
          window.fourthFS.cols = '*,15,0';
      }
      else {
        if (frameObj == "history")
          window.fourthFS.cols = '*,15,95';
      }
    }


    <% if (!refresh) { %>
    documents().location.href = '<isa:webappsURL name="/b2b/updateworkareanav.do"/>' ;
    getHistoryFrame().location.href = '<isa:webappsURL name="/b2b/history/updatehistory.do"/>' ;
    <% } %>
    form_input().location.href = '<isa:webappsURL name="/b2b/shownegotiatedcontract.do?modus=display"/>';

  </script>

  </head>
  <body></body>

</html>
