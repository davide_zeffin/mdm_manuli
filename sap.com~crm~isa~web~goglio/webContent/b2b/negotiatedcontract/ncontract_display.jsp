<%--
********************************************************************************
    File:         ncontract_display.jsp
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      20.01.2003
    Version:      1.0

    $Revision:$
    $Date:$
********************************************************************************
--%>
<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businessobject.negotiatedcontract.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.negotiatedcontract.NegotiatedContractConstants" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.negotiatedcontract.NegotiatedContractUI" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>


<%@ include file="/b2b/usersessiondata.inc" %>
<%@ include file="/b2b/checksession.inc" %>

<isacore:browserType/>
<isa:contentType />

<%

	NegotiatedContractUI ui = new NegotiatedContractUI(pageContext);

%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<title>
		<isa:translate key="b2b.order.display.pagetitle"/>
	</title>

	<isa:stylesheets/>


	<%-- include for locale calendar control --%>
	<%@ include file="/b2b/jscript/setLocaleCalendarSettings.jsp.inc" %>

	<script type="text/javascript">
	   <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>

	<script src='<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/table_highlight.js") %>'
			type="text/javascript">
	</script>
	<script src='<%=WebUtil.getMimeURL(pageContext,"b2b/jscript/reload.js") %>'
			type="text/javascript">
	</script>
	<script src='<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>'
			  type="text/javascript">
	</script>
	<script src='<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/browsersniff.js") %>'
			type="text/javascript">
	</script>
	<script src='<%=WebUtil.getMimeURL(pageContext, "auction/jscript/calendar.js") %>'
			type="text/javascript">
	</script>
	<script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
		  type="text/javascript">
	</script>
	<script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/table_highlight.js") %>"
		  type="text/javascript">
	</script>
	<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderToolsRetkey.js") %>"
		  type="text/javascript">
	</script>
	 <script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/negotiatedContracts.js") %>"
		  type="text/javascript">
	</script>
 
    <%@ include file="/b2b/jscript/negotiatedContracts.inc.jsp" %>

</head>

<body class="order" onload="loadPage();">
<%--
The body consists of two layers: 
 - the document layer, containing the header and the items of the document. It is scrollable.
 - the buttons layer, containig the buttons for the document. It stays at the bottom of the page.
--%>  
  <div class="module-name"><isa:moduleName name="b2b/negotiatedcontract/ncontract_display.jsp" /></div>
    
  <%-- Accesskey for the header section. --%>
  <a id="access-header" href="#access-items" title="<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>
  <%-- title --%>	   
  <h1><span>
      <% if (ui.getDocNumber() == null || ui.getDocNumber().equals("")) { %>
           <%= JspUtil.encodeHtml(ui.getHeaderDescription()) %><isa:translate key="b2b.docnav.new"/>
      <% } else { %>
           <%= JspUtil.encodeHtml(ui.getHeaderDescription()) %>: <%= JspUtil.encodeHtml(JspUtil.removeNull(ui.getDocNumber())) %>
      <% } %>
  </span></h1>
    
<%-- !!!! Do not change the name of the form, the JavaScript functions rely on them !!!! --%>
  <div id="document">
  <form name="negotiatedcontract_positions" method="post" action='<isa:webappsURL name="b2b/updatenegotiatedcontract.do" />'>    
	<div class="document-header">
	    <% // Error messages at the beginning in accessibility mode
		      if (ui.docMessageString.length() > 0 && ui.isAccessible()) { %>
				 <%@ include file="/b2b/messages-declare.inc.jsp" %>
				 <%@ include file="/b2b/messages-begin.inc.jsp" %>

	 	<%  	 addMessage(Message.ERROR, ui.docMessageString);
			  	 if (!messages.isEmpty()){
	 	%>
					<%@ include file="/b2b/messages-end.inc.jsp" %>
	 	<%       }
			  } // if there is error &&if ui.isAccessible
			// End of error messages for accessibility
		%>
	   <div class="header-basic">
		<%-- the header information for the basket --%>
           <%
             String grpTxt = ui.header.getProcessTypeDesc();
		     if (ui.isAccessible()) { %>
		        <a	href="#group-end" id="group-begin" title="<isa:translate key="access.grp.begin" arg0="<%=JspUtil.encodeHtml(grpTxt%>)"/>"></a>
		     <% }  %>
             <table class="layout">
                <tr>
		          <td class="col1">             
		       	       <% if (ui.isAccessible) { %>
                         <a href="#end-table1" title="<isa:translate key="ecm.acc.header.dat.sum"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="11"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
                       <% } %>
				    <table class="header-general" summary="<isa:translate key="ecm.acc.header.dat.sum"/>"> 
				      <%-- Your Reference: [input] --%>
				      <tr>
				         <td class="identifier" >
					        <isa:translate key="b2b.ncontract.header.reference"/>
				         </td>
				         <td class="value">
					        <%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %>
				         </td>
				      </tr>
				      <tr>
				      <%-- Description: [input] --%>
				      <tr>
				         <td class="identifier">
					        <isa:translate key="b2b.ncontract.header.description"/>
				         </td>
				         <td class="value">
					        <%= JspUtil.encodeHtml(ui.header.getDescription()) %>
				         </td>
				      </tr>
				      <%-- shipping condition: [input] --%>
				      <tr>
				          <td class="identifier"><label for="shipCond"><isa:translate key="b2b.ncontract.shipcond"/></label></td>
				          <td class="value">
                             <%= JspUtil.encodeHtml(ui.header.getShipCond()) %>
                          </td>
				      </tr>
                  </table>
                  <% if (ui.isAccessible) { %>
                    <a name="#end-table1" title="<isa:translate key="ecm.acc.head.dat.end"/>"></a>
                  <% } %>                
                </td>
		   <%-- End header data --%>

		   <%--Status--%>
           <td class="col2">
           <%--Status--%>
              <% if (ui.isAccessible) { %>
                 <a href="#end-table2" title="<isa:translate key="ecm.acc.head.status"/> <isa:translate key="ecm.acc.tab.size.info" arg0="2" arg1="2"/> <isa:translate key="ecm.acc.lnk.jump"/>"></a>
              <% } %>
              <table class="status" summary="<isa:translate key="ecm.acc.head.status"/>">
			    <tr>
				  <td class="identifier"><isa:translate key="b2b.nct.header.status"/>:</td>
				  <td class="value" ><isa:translate key="<%= JspUtil.encodeHtml(ui.getDocumentStatusKey()) %>"/></td>
			   </tr>
		      </table>
		   <%--End Status--%>
		   </td>
          </tr>
        </table><%-- layout --%>
      	</div> <%--header-basic--%>

	   <div class="header-itemdefault">  <%-- level: sub3 --%>
	        <div class="group">
            <%--Item defaults Data--%>
        	<% if (ui.isAccessible) { %>
		   	    <a name="#end-table4" title="<isa:translate key="b2b.acc.header.defdata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
		    <% } %>	        
	            <table class="data" summary="<isa:translate key="b2b.acc.header.defdata"/>"> <%-- level: sub3 --%>               
 				      <tr>
				       <%-- Contract period: From [input][calendar] To [input][calendar] --%>
				         <td class="identifier"> <isa:translate key="b2b.ncontract.header.period"/>&nbsp;<isa:translate key="b2b.ncontract.header.from"/></td>
				         <td class="value" >
						     <%= JspUtil.encodeHtml(JspUtil.replaceSpecialCharacters(ui.header.getContractStart())) %>
				         </td>
				      </tr>
				      <tr>
				         <td class="identifier">
					         <isa:translate key="b2b.ncontract.header.period"/>&nbsp;<isa:translate key="b2b.ncontract.header.to"/>
				         </td>
				         <td class="value">
						    <%= JspUtil.encodeHtml(JspUtil.replaceSpecialCharacters(ui.header.getContractEnd())) %>
				         </td>
				      </tr>        		            	            	            
	            </table> <%-- end class="data" --%>
        	    <% if (ui.isAccessible) { %>
		   	        <a name="end-table4" title="<isa:translate key="b2b.acc.header.end.defdata"/>"></a>
		        <% } %>                                                
                <%-- End Item defaults Data--%>	            
	        </div> <%-- end class="group" --%>
	    </div> <%-- class="header-itemdefault"> --%>  

       <%-- data messages--%>
        <div class="header-additional">
          <table class="message">
            <tr>
 	           <td class="identifier" >
 	             <% String titleTxt = WebUtil.translate(pageContext, "b2b.ncontract.header.history", null); %>
			     <span title="<isa:translate key="access.textarea.unavailable" arg0="<%=titleTxt%>"/>">
			     <label for="messagehistory">
	             <isa:translate key="b2b.ncontract.header.history"/></label></span>
	           </td>
               <td class="value">
                 <textarea readonly rows="2" cols="80" id="messagehistory" name="messagehistory" ><%= (ui.header.getTextHistory() != null) ? JspUtil.encodeHtml(ui.header.getTextHistory().getText(), new char[] {'\n'}):"" %></textarea>
               </td>
            </tr>
          </table> <%-- message --%>
        </div> <%-- header additional --%>

        <%-- Display error messages if existing --%>
        <% if(!ui.isAccessible()) { %>
            <%@ include file="/b2b/headErrMsg.inc.jsp" %>
        <% } %>
       </div> <%-- document-header --%>

	   <%-- the item information for the basket --%>
	   <div class="document-items">
          <a id="access-items" href="#access-buttons" title= "<isa:translate key="b2b.acc.items.title"/>" accesskey="<isa:translate key="b2b.acc.items.key"/>"></a>       
	   <%
	        // Data table preamble
            String tableTitle = WebUtil.translate(pageContext, "b2b.contract.item.table", null);
			if (ui.isAccessible())
			{

				int colno = 8;
				int rowno = 0;
				String firstRow ="0";
				// calculate the no of rows displayed.
	   %>
			   <isa:iterate id="item" name="<%= NegotiatedContractConstants.RC_ITEMS %>"
							   type="com.sap.isa.businessobject.item.ItemNegotiatedContract">
					<% rowno++; %>
			   </isa:iterate>

	   <%
				String columnNumber = Integer.toString(colno);
				String rowNumber = Integer.toString(rowno);
				if(rowno>0)
					firstRow = Integer.toString(1);
				String lastRow = Integer.toString(rowno);

	   %>
				<a href="#tableend"
					id="tablebegin"
					title="<isa:translate key="access.table.begin" arg0="<%=tableTitle%>"
					    arg1="<%=JspUtil.encodeHtml(rowNumber)%>" arg2="<%=JspUtil.encodeHtml(columnNumber)%>" arg3="<%=JspUtil.encodeHtml(firstRow)%>" arg4="<%=JspUtil.encodeHtml(lastRow)%>"/>"></a>
	   <%
			}

			// Data table preamble ends
	   %>


		  <table class="itemlist">
			   <tr>
				  <%-- column for position number --%>
				  <th class="item" id="itemcol" scope="col" rowspan="2"><isa:translate key="b2b.ncontract.pos.posno"/></th>
				  <%-- column for product number --%>
				  <th class="product" id="productcol" scope="col" rowspan="2"><isa:translate key="b2b.order.display.productno"/></th>
				  <% if (ui.contractType.equalsIgnoreCase(HeaderNegotiatedContract.QUANTITY_RELATED_CONTRACT) == true) { %>
					  <%-- column for target quantity and unit --%>
					  <th class="qty" id="targetcol" scope="col" colspan="2">
						  <isa:translate key="b2b.ncontract.pos.target"/>
					  </th>
				  <% } else {
					 if (ui.contractType.equalsIgnoreCase(HeaderNegotiatedContract.VALUE_RELATED_CONTRACT) == true) {  %>
						<%-- column for target value and currency --%>
						<th class="qty" id="targetcol" scope="col" rowspan="2">
							<isa:translate key="b2b.ncontract.pos.target"/><br>
							<isa:translate key="b2b.ncontract.pos.value"/>
						</th>
					 <% } else  { %>
						<th> nbsp; </th>
					 <% } %>
				  <% } %>

				  <%-- header for columns of price agreements --%>
				  <th class="desc" id="pricenegcol" scope="col" colspan="4">
					  <isa:translate key="b2b.ncontract.pos.priceagreement"/>
				  </th>
			   </tr>
			   <tr>
				  <% if (ui.contractType.equalsIgnoreCase(HeaderNegotiatedContract.QUANTITY_RELATED_CONTRACT) == true) { %>
				  <%-- column for target quantity and unit --%>
					  <th class="qty" id="targetqtycol" scope="col">
						 <isa:translate key="b2b.ncontract.pos.quantity"/>
					  </th>
					  <th class="unit" id="targetunitcol" scope="col">
						 <isa:translate key="b2b.ncontract.pos.unit"/>
					  </th>
				  <% } %>
				   <%-- 4 columns for price agreements: --%>
				  <%--    Rebate type --%>
				  <th class="price" id="pricenegrebatecol" scope="col">
					  <isa:translate key="b2b.ncontract.pos.rebatetype"/>
				  </th>
				  <%--    rebate input --%>
				  <th class="price" id="pricenegamountcol" scope="col">
					  <isa:translate key="b2b.ncontract.pos.rebate"/>
				  </th>
				  <%--    rebate quantity and unit --%>
				  <th class="qty" id="pricenegqtycol" scope="col">
					  <isa:translate key="b2b.ncontract.pos.quantity"/>
				  </th>
				  <th class="unit" id="pricenegunitcol" scope="col">
					 <isa:translate key="b2b.ncontract.pos.unit"/>
				  </th>
			   </tr>

			   <% String tag_style_detail = ""; %>
			   <isa:iterate id="item" name="<%= NegotiatedContractConstants.RC_ITEMS %>"
						   type="com.sap.isa.businessobject.item.ItemNegotiatedContract">
				   <% /* set the item in the ui class */
					  ui.setItem(item);
					  /* ********** Check if the item is a BOM sub item to be supressed ****** */  
					  if (!ui.isBOMSubItemToBeSuppressed()) { 
					    tag_style_detail = "poslist-detail-" + ui.even_odd;
				   %>
				   <tr>
					  <td colspan="11" class="separator"></td>
				   </tr>
				   
                  <% String substMsgColspan = "11"; %>
                  <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>

				  <tr  class="<%= ui.even_odd %>"  id="row_<%= ui.line %>" onkeypress="checkReturnKeyPressed(window.event);">
					 <%-- position number --%>
					 <td headers="itemcol">
					   <%= JspUtil.removeNull(item.getNumberInt()) %>
					 </td>
					 <td class="product" headers="productcol" scope="row" id="productrow">
						 <%= JspUtil.encodeHtml(item.getProduct()) %>
					 </td>
					 <% if (ui.contractType.equalsIgnoreCase(HeaderNegotiatedContract.QUANTITY_RELATED_CONTRACT) == true) { %>
						<%-- target quantity and unit --%>
						<td class="qty" headers="targetqtycol targetcol">
						   <%= JspUtil.encodeHtml(item.getQuantity()) %>
						</td>
						<td class="unit" headers="targetunitcol targetcol">
		                   <%= JspUtil.encodeHtml(item.getUnit()) %>
						</td>
					 <% }
					 else {
						 if (ui.contractType.equalsIgnoreCase(HeaderNegotiatedContract.VALUE_RELATED_CONTRACT) == true) {  %>
							<%-- target value and currency --%>
							<td class="price" headers="targetcol">
							   <%= JspUtil.encodeHtml(item.getUnit()) %>
							   <%= JspUtil.encodeHtml(item.getTargetValue()) %>>&nbsp;<%= JspUtil.encodeHtml(ui.header.getCurrency()) %>
							</td>
						 <% } else  { %>
							<td> nbsp; </td>
						 <% } %>
					 <% } %>
					 <%-- price agreements --%>
					 <%-- Rebate type --%>
					 <td headers="pricenegrebatecol pricenegcol">
						<% if ( item.getCondIdInquired() != null) { %>
		                    <%= JspUtil.encodeHtml(item.getCondInquiredDesc()) %>&nbsp;<isa:translate key="b2b.ncontract.pos.in" />&nbsp;<%= JspUtil.encodeHtml(item.getCondValueType(item.getCondTypeInquired()) )%>
						<% } %>
					 </td>
					<%-- rebate input --%>
					 <td class="qty" headers="pricenegamountcol pricenegcol">
					   <%  if ( (ui.condTypes != null) && (item.getCondTypeInquired() != null) ) { %>
							 <%= JspUtil.encodeHtml(item.getCondRateInquired()) %>
					   <% } else { %>
						  &nbsp;
					   <% } %>
					 </td>
					 <% if ( (ui.condTypes != null) && (item.getCondTypeInquired() != null) && !item.getCondValueType(item.getCondTypeInquired()).equals("%")) { %>
						<%-- rebate quantity --%>
						<td class="qty" headers="pricenegqtycol pricenegcol">
							  <%= JspUtil.encodeHtml(item.getCondPriceUnitInquired()) %>
						</td>
						<%-- rebate unit --%>
						<td class="unit" headers="pricenegunitcol pricenegcol">
                              <%= JspUtil.encodeHtml(ui.units[0]) %><input type="hidden" name="conditionUnit[<%= ui.line %>]" value="<%= JspUtil.encodeHtml(ui.units[0])%>" />
						</td>
					<% } else { %>
						<%-- no condition type selected or percentage rebate: no rebate quantity --%>
						<td>
						   &nbsp;
						</td>
						<td>
						<%-- ... and no rebate unit --%>
						   &nbsp;
						</td>
					<% } %>
				   </tr>

				   <%-- product description and pricing informations --%>
				   <% tag_style_detail = ui.even_odd + "-detail"; %>
				   <tr id="rowdetail_<%= ui.line %>"  class="<%= tag_style_detail %>" >
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td class="detail" colspan="7">
						<table class="item-detail" headers="productrow">
						 <tr>
							<%-- product description  --%>
							<td class="desc" scope="col" id="description">
							   <isa:translate key="b2b.ncontract.pos.desc"/>
							</td>
							<%-- normal price --%>
							<td  class="identifier" scope="row" id="normalprice">
							   <isa:translate key="b2b.ncontract.pos.normpric"/>&nbsp;
							</td>
							<td class ="price" headers="normalprice">
							   <% if (ui.itemNormalPriceExists()) { %>
								 <%= JspUtil.encodeHtml(item.getPriceNormal()) %>
								 &nbsp;<%= JspUtil.encodeHtml(item.getCurrency()) + " / " + JspUtil.encodeHtml(item.getPricingUnitPriceNormal()) + " " + JspUtil.encodeHtml(item.getUnitPriceNormal()) %>
							   <% } %>
							</td>
						 </tr>
						 <tr>
							<td  class="desc" headers="description">
							   <%= JspUtil.encodeHtml(item.getDescription()) %>
							</td>
							<%-- calculated price --%>
							<% if (item.getCondTypeInquired() != null ) { %>
							   <td  class="identifier" scope="row" id="calprice">
								  <isa:translate key="b2b.ncontract.pos.calprice"/>&nbsp;
							   </td>
							   <% if (ui.itemCalculatedPriceExists() ) { %>
								  <td  class="price" headers="calprice">
									<%= JspUtil.encodeHtml( item.getPriceInquired() ) %>
									&nbsp;<%= JspUtil.encodeHtml(item.getCurrency()) + " / " + JspUtil.encodeHtml(item.getPricingUnitPriceInquired()) + " " + JspUtil.encodeHtml(item.getUnitPriceInquired()) %>
								  </td>
							   <% } %>
							<% }  %>
						 </tr>
						 <%-- offered price --%>
						 <% if (ui.itemOfferedPriceExists() ) { %>
							<tr>
							   <td >&nbsp;</td>
							   <td class="identifier" scope="row" id="offerprice">
								 <isa:translate key="b2b.ncontract.pos.offprice"/>&nbsp;
							   </td>
							   <td  class="price" headers="offerprice">
								  <%= JspUtil.encodeHtml(item.getPriceOffered() ) %>
								  &nbsp;<%= JspUtil.encodeHtml(item.getCurrency()) + " / " + JspUtil.encodeHtml(item.getPricingUnitPriceOffered())  + " " + JspUtil.encodeHtml(item.getUnitPriceOffered()) %>
							   </td>
							</tr>
						  <% } %>
						</table>
					  </td>
					</tr>
                 <% }  // ui.isBOMSubItemToBeSuppressed() %> 
                 
			   </isa:iterate>  <%-- end of iterating through all positions --%>

			   <tr>
				  <td colspan="11" class="separator"></td>
			   </tr>
		  </table>
	  <%
		  // Data Table Postamble
		  if (ui.isAccessible())
		  {
	  %>
		  <a id="tableend"
			href="#tablebegin"
			title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>"</a>
	  <%
		  }
	  %>

	  </div> <%-- document-items --%>

	</form>
	</div> <%-- document --%>


	<%-- Buttons --%>
	<div id="buttons">
        <a id="access-buttons" href="#access-header" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>
 
		<ul class="buttons-3">

	         <%
	            if(ui.isAccessible()) {
					String rejectBtnTxt = WebUtil.translate(pageContext, "b2b.nct.button.reject", null);
					String acceptBtnTxt = WebUtil.translate(pageContext, "b2b.nct.button.accept", null);
					String changeBtnTxt = WebUtil.translate(pageContext, "b2b.nct.button.change", null);
					String closeBtnTxt = WebUtil.translate(pageContext, "b2b.nct.button.close", null);
			 %>
			 <% 	if ( ui.header.isStatusQuotation() ) { %>

						<li><a href="#" onclick="reject_confirm();"
							   title="<isa:translate key="access.button" arg0="<%=rejectBtnTxt%>" arg1=""/>">
							   <isa:translate key="b2b.nct.button.reject"/></a>
						</li>
						<li><a href="#" onclick="accept_confirm();"
							  title="<isa:translate key="access.button" arg0="<%=acceptBtnTxt%>" arg1=""/>">
							  <isa:translate key="b2b.nct.button.accept"/></a>
						</li>
			<% 		} %>
			<% 		if ( ui.header.isStatusQuotation() || ui.header.isStatusInProcess() ) { %>
					   <li><a href="#" onclick="change_ncontract();"
						  title="<isa:translate key="access.button" arg0="<%=changeBtnTxt%>" arg1=""/>">
						  <isa:translate key="b2b.nct.button.change"/></a>
					   </li>
			<% 		} %>
					<li><a href="#" onclick="close_ncontract();"
						  title="<isa:translate key="access.button" arg0="<%=closeBtnTxt%>" arg1=""/>">
						  <isa:translate key="b2b.nct.button.close"/></a>
					</li>
			<% } else { %>
			<% 		if ( ui.header.isStatusQuotation() ) { %>

							<li><a href="#" onclick="reject_confirm();">
								   <isa:translate key="b2b.nct.button.reject"/></a>
							</li>
							<li><a href="#" onclick="accept_confirm();">
								  <isa:translate key="b2b.nct.button.accept"/></a>
							</li>
			<% 		} %>
			<% 		if ( ui.header.isStatusQuotation() || ui.header.isStatusInProcess() ) { %>
						   <li><a href="#" onclick="change_ncontract();">
							  <isa:translate key="b2b.nct.button.change"/></a>
						   </li>
			<% 		} %>
					<li><a href="#" onclick="close_ncontract();">
						  <isa:translate key="b2b.nct.button.close"/></a>
					</li>

			<% } %>
	   </ul>
	</div>
	<%--End  Buttons--%>

    <%	if (ui.isAccessible()) { %>
			<a	href="#group-begin"
			id="group-end"
			title="<isa:translate key="access.grp.end" arg0="<%=grpTxt%>"/>"> </a>
	<%	} %>

  </body>
</html>






