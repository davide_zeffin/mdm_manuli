<%--
********************************************************************************
    File:         ncontract_confirm.jsp
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      16.10.2002
    Version:      1.0

    $Revision:$
    $Date:$
********************************************************************************
--%>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.businessobject.header.HeaderNegotiatedContract" %>
<%@ page import="com.sap.isa.isacore.action.negotiatedcontract.NegotiatedContractConstants" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>


<%@ include file="/b2b/usersessiondata.inc" %>
<%@ include file="/b2b/checksession.inc" %>

<%
    BaseUI ui = new BaseUI(pageContext);
    HeaderNegotiatedContract header	= (HeaderNegotiatedContract) request.getAttribute(NegotiatedContractConstants.RC_HEADER);
    String docNumber = "";
    if (header != null) {
       docNumber = header.getSalesDocNumber();
    }    
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <isa:stylesheets/> <%-- Stylesheet --%>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

    <title>
        <isa:translate key="b2b.order.display.pagetitle"/>
    </title>

	<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
          type="text/javascript">
    </script>

    <script type="text/javascript">
        // function for the onload()-event

        function loadPage() {
            //sets minibasket information back to default after document has been sent
            isaTop().header.document.forms['basketForm'].miniBasket.value="<isa:translate key="b2b.header.minibasket.default"/>";
			isaTop().header.setBasketLinkText('<isa:translate key="b2b.header.minibasket.default"/>');
        }
    </script>

</head>

<BODY class="message-workarea" onload="loadPage();">
    <div class="module-name"><isa:moduleName name="b2b/negotiatedcontract/ncontract_confirm.jsp" /></div>
    <h1><span><isa:translate key="b2b.nct.title.conf.send"/></span></h1>
    <div id="document">
        <div class="document-header">
             <div class="header-basic">
           	    <p class="p1"><span><isa:translate key="b2b.nct.confirm2.txt1" arg0="<%= docNumber %>" /></span></p>
           	 </div>
	            <%-- Send Button --%>
             <div class="buttons">
                  <a class="button" id="buttonsend" href="<isa:webappsURL name='b2b/updatedocumentview.do' />"> <isa:translate key="b2b.nct.confirm2.txt2"/></a>
             </div>
        </div> <%-- end document-head --%>
    </div> <%-- end document --%>
</body>
</html>

