<%--
********************************************************************************
    File:         busy.jsp
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Created:      24.04.2003
    Version:      1.0

    $Revision:$
    $Date:$
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%

    BaseUI ui = new BaseUI(pageContext);

%>


<!DOCTYPE html PUBLIC "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

  <head>
    <title><isa:translate key="b2b.fs.title"/></title>

	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>

  </head>

  <body>
  	<div id='busy'>
  	<img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/busy.gif") %>" alt="<isa:translate key="b2b.busy.message"/>" >
  	<p><isa:translate key="b2b.busy.message"/></p>
  	</div>
  	<script type="text/javascript"
  		noscript="<isa:UCtranslate key="b2b.busy.jsp.js"/>">
	  location.href='<isa:webappsURL name="b2b/shownegotiatedcontract.do"/>';
	</script>

  </body>
<%--
  <frameset rows="*,46" id="workareaFS" border="0" frameborder="0" framespacing="0">
    <frame name="positions" src='<isa:webappsURL name="b2b/shownegotiatedcontract.do"/>' frameborder="0">
    <!--<frame name="closer_details" src="b2b/negotiatedcontract/ncontract_changebuttons.jsp" frameborder="0" scrolling="no" marginwidth="0" marginheight="0">-->
	<frame name="closer_details" src='<isa:webappsURL name="b2b/negotiatedcontract/ncontract_changebuttons.jsp"/>' frameborder="0" scrolling="no" marginwidth="0" marginheight="0">
    <noframes>
      <body>&nbsp;</body>
    </noframes>
  </frameset>
--%>
</html>