<%--
********************************************************************************
    File:         determinesoldto.inc
    Copyright (c) 2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAPMarkets
    Created:      15.06.2002
    Version:      1.0

    $Revision: #1 $
    $Date: 2002/06/15 $
********************************************************************************
--%>
<%  
   com.sap.isa.businessobject.order.PartnerListEntry soldToEntry = header.getPartnerList().getSoldTo();
                         
   String soldToId = (soldToEntry != null) ? soldToEntry.getPartnerId(): "";
   String soldToName = "";
   TechKey soldToKey = (soldToEntry != null) ? soldToEntry.getPartnerTechKey():null;
                                              
   if (soldToKey != null && soldToKey.getIdAsString().length() > 0) {
       com.sap.isa.businesspartner.businessobject.BusinessPartner businessPartner = bom.createBUPAManager().getBusinessPartner(soldToKey);
       soldToName = businessPartner.getAddress().getName()  + " " + businessPartner.getAddress().getFirstName();
   }
   
   /* old version
   String soldToId = "";
   String soldToName = "";
   TechKey soldToKey = null;
                         
   if (bom.getUser().getSoldTo() != null) {
       soldToId = bom.getUser().getSoldTo().getId();
       Address addr = bom.getUser().getSoldTo().getAddress();
       if (addr != null) {
           soldToName = addr.getFirstName() + " " + addr.getName();
       }
       soldToKey = bom.getUser().getSoldTo().getTechKey();
   }
   */
%>




