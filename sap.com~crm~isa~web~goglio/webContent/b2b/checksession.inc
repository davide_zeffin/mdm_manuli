<%--
********************************************************************************
    File:         checksession.inc
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      24.4.2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2001/04/23 $
********************************************************************************
--%>

<%@ taglib uri="/isa" prefix="isa" %>

<%-- Check for an invalid session and forward to error page if session is invalid --%>
<% if (session.isNew()) { %>
    <% session.invalidate(); %>
    <jsp:forward page="invalidsession.jsp"/>
<% } %>
<%-- End of Check --%>