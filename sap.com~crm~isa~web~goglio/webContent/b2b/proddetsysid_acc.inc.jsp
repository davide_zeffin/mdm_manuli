<%--
********************************************************************************
    File:         proddetsysid_acc.inc.jsp
    Copyright (c) 2004, SAP AG
    Author:       SAP AG
    Created:      08.03.2004
    Version:      1.0

    Determine if system product id should be displayed in the item details

    Prerequisites: ui - must point to the OrderUI class
                   item - must point to the current item

    $Revision: #1 $
    $Date: 2004/03/08$
********************************************************************************
--%>
<% if (ui.isProductDeterminationInfoAvailable() && ui.isElementVisible("order.item.systemProductId", itemKey)) { %>
       <tr>
          <td class="identifier">
		    <label for="proddet_[<%= ui.line %>]">
              <isa:translate key="b2b.proddet.sysprodid"/>
            </label>
          </td>
          <% if (ui.showCurrentItemToggleMultipleCampaignData()) { %>
          <td class="campimg-1"></td>
          <% } %> 
          <td class="value" colspan="<%= ui.getNonCampItemDetailColspan() %>" id="proddet_[<%= ui.line %>]">
              <%= JspUtil.encodeHtml(item.getSystemProductId()) %>
          </td>
      </tr>
<% } %>