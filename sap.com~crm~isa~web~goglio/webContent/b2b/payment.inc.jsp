<%@ page import="com.sap.isa.isacore.uiclass.PaymentUI" %>

<% PaymentUI paymentUI = new PaymentUI(pageContext, ui.getSalesDoc()); 
   paymentUI.setShowCVVAsStars(true);
%>
           <%-- Payment card data --%>
           <% if (paymentUI.isManagedDocType() && paymentUI.isDirectPaymentAllowed() && paymentUI.enableMaintenance) { %>
             <div class="header-payment">
                <h1 class="group"><span><isa:translate key="payment.type.txt"/></span></h1>               
                <div class="group">	 
                <% if (paymentUI.isAccessible) { %>
                  <a name="#end-paymentmaintain" 
	                 title="<isa:translate key="payment.acc.maintenance"/>">
                  </a>
                <% } %>                
                <p><isa:translate key="payment.maintain.txt2"/></p> 
                <input type="hidden" name="payment_maintain" value="true" />          
 	            <%@ include file="/b2b/paymentCards.inc.jsp" %>  
 	            <% if (paymentUI.isAccessible) { %>
                  <a name="end-paymentmaintain" 
	                 title="<isa:translate key="payment.acc.maintenance.end"/>">
                  </a>
                <% } %>	       
	            </div>  
	         </div>    
	       <% } %>    	            
