<% /** This JSP displays a screen with the list of available shiptos
       and allows to select the shiptos for which the GRID items are copied to
   **/
%>
<%@ page errorPage="\appbase\jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="java.util.ArrayList"%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ include file="usersessiondata.inc" %>
<%
	ArrayList presetReqDates = new ArrayList();

	if (userSessionData.getAttribute("multiReqDates") != null) {
		presetReqDates = (ArrayList) userSessionData.getAttribute("multiReqDates");
	}
	int maxCount = 5, filledCount = presetReqDates.size();

	if ( maxCount <= filledCount){
		maxCount = maxCount + filledCount;
	}

%>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
    	<title>
        	<isa:translate key="b2b.fs.title"/>
	    </title>
 <isa:stylesheets/>
 <isa:includes/>
 
 
	<%-- include for locale calendar control --%>
	<%//@ include file="/b2b/jscript/setLocaleCalendarSettings.jsp.inc" %>
	
    <script src="<%=WebUtil.getMimeURL(pageContext, "auction/jscript/calendar.js") %>"
    	type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
          type="text/javascript">
    </script>
 <script type="text/javascript">
 	function save_shiptos() {
		document.multireqdate.multireqdatebut.value="X";
		document.multireqdate.refresh.value="X";
		document.multireqdate.target ='form_input';
		window.close();
		document.multireqdate.submit();
		return true;
 	}
 	function return_to_basket() {
 		window.close();
 		document.multireqdate.target='form_input';
 		document.multireqdate.submit();
 		return true;
 	}

 </script>
	</head>

	<body class="ship">
	<form name="multireqdate"  action="<isa:webappsURL name="b2b/multiplereqdate.do"/>" method="POST">
		<div id="document">
  			<div class="module-name"><isa:moduleName name="b2b/multiple-reqdate.jsp" /></div>
			<table class="itemlist">

				<%for (int i= 0; i < maxCount; i++){%>
					<tr>
						<td class="identifier">
						    <input id="mreqdate[<%=i%>]" name="mreqdate[<%=i%>]" type="text"  readonly class="textinput-middle" value="<%=((i < filledCount) ? presetReqDates.get(i) : "")%>" />
						
						    <a href="#" class="icon" onclick="setDateField(document.multireqdate['mreqdate[<%=i%>]']);setDateFormat('mm/dd/yyyy');isaTop().newWin = window.open('<isa:webappsURL name="b2b/calendar.jsp"/>','cal','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes'); isaTop().newWin.focus();">
                               <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>" />
                            </a>
						</td>
						<td class="identifier">
            				<input class="green" type="button" name="clear" value="<isa:translate key="dealerlocator.jsp.reset"/>" onclick="document.forms['multireqdate'].elements['mreqdate[<%=i%>]'].value=''">
            			</td>  			
				    </tr>
				<%}%>
			</table>
		</div>
		
			<input type="hidden" name="multireqdatebut" value="">
			<input type="hidden" name="refresh" value="">
		<div>
			<table class="itemlist">
				<tr>
					<td>				
		       		   <input class="green" type="button" name="ok" value="<isa:translate key="b2b.docnav.ok"/>" onclick="save_shiptos();">
				       <input class="green" type="button" name="cancel" value="<isa:translate key="b2b.order.display.submit.cancel"/>" onclick="return_to_basket();">
				    </td>
				</tr>
			</table>
		</div>
	</form>
	</body>
 </html>