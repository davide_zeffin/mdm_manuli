<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.CatalogStageListUI" %>
<%@ page import="com.sap.isa.isacore.action.ShopShowListAction" %>

<% CatalogStageListUI ui = new CatalogStageListUI(pageContext);
   
   ui.setTitle(WebUtil.translate(pageContext, "staging.jsp.title", null));
   ui.setTableTitle(WebUtil.translate(pageContext, "b2c.staginglist.jsp.table1", null));
   ui.setTableItems((com.sap.isa.core.util.table.ResultData) request.getAttribute(DetermineInactiveCatalogInfoAction.RK_INACTIVE_CATALOG_LIST));  
   String tableRowTitle[] = { WebUtil.translate(pageContext, "staging.jsp.version", null), 
                              WebUtil.translate(pageContext, "staging.jsp.status", null),
                              WebUtil.translate(pageContext, "staging.jsp.indexedOn", null),
                              WebUtil.translate(pageContext, "staging.jsp.indexedAt", null),
                              WebUtil.translate(pageContext, "staging.jsp.indexedBy", null) };
   ui.setTableRowTitle(tableRowTitle);
   ui.setDisplayShopList(false); %>
   
   <%@ include file="displayList.inc.jsp"%>