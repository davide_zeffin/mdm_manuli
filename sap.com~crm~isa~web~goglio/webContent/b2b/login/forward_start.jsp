<%@ page language="java" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); %>

<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    	<title></title>
    	<script language="JavaScript">
    	<!--
	      function loadStartPage() {
	        document.forms["startForm"].submit();
	        return true;
	      }
	    -->
    	</script>
	</head>
  	<body onload="loadStartPage();return true;">
		<form method="GET" action='<isa:webappsURL name="b2b/start.do"/>' name="startForm">
          <script type="text/javascript">
          <!--
            <%-- determine the browser version --%>
            document.write('<input type="hidden" name="browsername" value="' +  escape(navigator.userAgent.toLowerCase()) + '">');
            document.write('<input type="hidden" name="browsermajor" value="' + escape(parseInt(navigator.appVersion)) + '">');
            document.write('<input type="hidden" name="browserminor" value="' + escape(parseFloat(navigator.appVersion)) + '">');
           //-->
          </script>
        </form>
  </body>
</html>
