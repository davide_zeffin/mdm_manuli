 <%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.ShopShowListAction" %>
<%@ page import="com.sap.isa.catalog.actions.DetermineInactiveCatalogInfoAction" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%@ include file="/b2b/usersessiondata.inc" %>

<% String[] evenOdd = new String[] { "even", "odd" }; %>
 
   <isa:contentType/>
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
   <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
   <head>
      <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
         <title> <%= ui.getTitle() %> </title>
         <isa:stylesheets/>
   </head>

   <body class="shoplist">
   <div id="scrollable-selection">
   <%@ include file="/b2b/login/header.inc.jsp" %>
   <div class="module-name"><isa:moduleName name="b2b/login/shoplist.jsp" /></div>

   <div class="selection">
      <h1><%= ui.getTitle() %></h1>
      <% if (ui.isAccessible) { %>
            <a href="#end-table1" title="<isa:translate key="shop.jsp.acc.tabletext1"/> [2] <isa:translate key="shop.jsp.acc.tabletext2"/> [<%= ui.getTableTitleSize() %>] <isa:translate key="shop.jsp.acc.tabletext3"/>"></a>
      <% }
         /* IPC pricing date for staging */
         if(!ui.isDisplayShopList()) { %> 
             <form name="inactiveCatForm" action="<isa:webappsURL name="/app/readinactivecat.do"/>" method="post">
                <input type="hidden" name="techkey" value=""/>
                <script type="text/javascript">
                   <!-- 
                      function loadCatalog(techkey) {
                         document.inactiveCatForm.techkey.value = techkey;
                         document.inactiveCatForm.submit();
                      }
                   //-->
                </script>
             <% if (ui.showCatalogueStagingIPCDate()) { %>
                <%DateFormat dateFormatter = new SimpleDateFormat(ui.getDefaultDateFormat());%>
                <%@ include file="/appbase/jscript/calendar.js.inc.jsp"%>
             <br>
             <div class="login">
               <ul>
                 <li>
                   <p><span><isa:translate key="staging.jsp.infoText"/></span></p>
                   <label for="startDate" class="label-input"><isa:translate key="staging.jsp.iPCPricingDate"/></label>
                   <input name="startDate" id="startDate" type="text" size="24" value="<%=JspUtil.encodeHtml(ui.getToday())%>"/>
                   <% if (!ui.isAccessible) { %>
                      <a class="icon" href="#" onclick="openCalendarWindow('<%=ui.getDefaultDateFormat()%>', 'startDate');">
                        <img class="img" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>"/>
                      </a>
                   <% }%>
                  </li>       
                </ul>         
              </div>
       <% }
       } %>
       <div class="filter-result">
          <table summary="<%=ui.getTableTitle()%>">
             <tr>
            <% int index=0;
               for(index=0;index<ui.getTableTitleSize();index++) { %>
                   <th scope="col" tabindex="0"><%=ui.getTableRowTitle(index)%></th>
            <% } %>    
             </tr>
            <% int line = 0; 
               if (ui.isDisplayShopList()) { %>
                   <isa:iterate id="shop" name="<%= ShopShowListAction.RK_SHOP_LIST %>" type="com.sap.isa.core.util.table.ResultData">
                      <tr class="<%= evenOdd[++line % 2]%>">
                        <td>
                        <% if (!shop.getString(Shop.ID).equals("")) { %>
                             <a href="<isa:webappsURL name="/b2b/readshop.do"/>?shopId=<%= shop.getRowKey() %>" title="<%=shop.getString(Shop.ID) %>: <isa:translate key="shop.jsp.idtext"/>">
                                  <%=shop.getString(Shop.ID) %>
                             </a>
                        <% } 
                           else { %>
                             &nbsp;
                        <% } %>
                        </td>
                        <td>
                        <% if (!shop.getString(Shop.DESCRIPTION).equals("")) { %>
                              <a href="<isa:webappsURL name="/b2b/readshop.do"/>?shopId=<%= shop.getRowKey() %>">
                                   <%= JspUtil.encodeHtml(shop.getString(Shop.DESCRIPTION)) %>
                              </a>
                        <% } else { %>
                             &nbsp;
                        <% } %>
                        </td>
                      </tr>
                   </isa:iterate>
            <% } 
               else { %>
                   <isa:iterate id="inactiveCat" name="<%= DetermineInactiveCatalogInfoAction.RK_INACTIVE_CATALOG_LIST %>" type="com.sap.isa.core.util.table.ResultData"> 
                      <tr class="<%= evenOdd[++line % 2]%>">
                   <% String indexState = inactiveCat.getString("REPL_STATE");
                      String resKey = "shop.jsp.indexstate."+indexState; 
                      if (ui.isDisplayLink(indexState)) { %> 
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("VERSION"))%>"     tabindex="0"><%=inactiveCat.getString("VERSION")%></a></td>
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("REPL_STATE"))%>"  tabindex="0"><isa:translate key="<%=resKey%>"/></a></td>
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("REPL_TIME_IO"))%>" tabindex="0"><%=inactiveCat.getString("REPL_TIME_IO")%></a></td>
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("REPL_TIME_T"))%>" tabindex="0"><%=inactiveCat.getString("REPL_TIME_T")%></a></td>
                        <td scope="row"><a href="javascript:loadCatalog('<%=inactiveCat.getString("TECHKEY_C")%>')" title="<%=JspUtil.encodeHtml(inactiveCat.getString("REPL_BY"))%>" tabindex="0"><%=inactiveCat.getString("REPL_BY")%></a></td>
                   <% }
                      else { %>
                        <td scope="row"><%=inactiveCat.getString("VERSION")%></td>
                        <td scope="row"><isa:translate key="<%=resKey%>"/></td>
                        <td scope="row"><%=inactiveCat.getString("REPL_TIME_IO")%></td>
                        <td scope="row"><%=inactiveCat.getString("REPL_TIME_T")%></td>
                        <td scope="row"><%=inactiveCat.getString("REPL_BY")%></td>
                   <% } %>   
                      </tr>
                   </isa:iterate>
                 </form>
            <% } %>   

          </table>
       <% if (ui.isAccessible) { %>
             <a name="end-table1" title="<isa:translate key="shop.jsp.acc.tabletext4"/>"></a>
       <% } %>
      </div> <%-- filter-result --%>
   </div> <%-- selection --%>
   </div> <%-- scrollable selection --%>

</body>
</html>