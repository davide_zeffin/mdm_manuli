<%-- pwchange.jsp --%>

<%-- This JSP displays the PasswordChange screen to enter a new password --%>

<%@ page import="com.sap.isa.isacore.action.b2b.PWChangeAction" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.LoginUI" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>

<%
	LoginUI ui = new LoginUI(pageContext);

	//Get the max password length, depending on the backend.
	int passwdLength = ui.getPasswordLength();
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<isa:contentType/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  		<title><isa:translate key="b2b.login.pwchange.title"/></title>
		<isa:stylesheets/>
		
		<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/buttons.js") %>"
			type="text/javascript">
		</script>
		
		<script type="text/javascript">
		<!--
			function return_event(){
				document.getElementById("changepw").submit();
			}
		//-->
		</script>
		
	</head>

<body class="selection-shop">

<% if (ui.isPortal) { %>
	<div id="header-portal">
<% } else { %>
	<div id="header-appl">
<% } %> 

	<div class="header-logo">
	</div>
	
	<div class="header-applname">
		B2B <i>Shop International</i>
	</div>

	<div class="header-username">
	
	</div>

	<%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
	<%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
	<%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
		 so make sure to test your work in these browsers. --%>
	<div id="header-extradiv1">
		<span></span>
	</div>
	<div id="header-extradiv2">
		<span></span>
	</div>
	<div id="header-extradiv3">
		<span></span>
	</div>
	<div id="header-extradiv4">
		<span></span>
	</div>
	<div id="header-extradiv5">
		<span></span>
	</div>
	<div id="header-extradiv6">
		<span></span>
	</div>
</div> <%-- header-appl --%>	
	
<div class="selection">
	<div class="module-name"><isa:moduleName name="b2b/login/pwchange.jsp" /></div>
	<h1><isa:translate key="b2b.login.pwchange.head"/></h1>
	
	<form method="post" action="<isa:webappsURL name="/b2b/pwchange.do"/>" name="changepw">
	<%-- control the forward after the call with an hidden field --%>
		<input type="hidden" name="<%=PWChangeAction.FORWARD_NAME%>" value="<%=(String)request.getAttribute(PWChangeAction.FORWARD_NAME)%>" />
		<input type="hidden" name="<%=(String)request.getAttribute(PWChangeAction.ACTION_NAME)%>" value="<isa:translate key="b2b.login.pwchange.submit"/>" />
		<% if (ui.isAccessible) { %>
			<a href="#end-login" name="start-login" title="<isa:translate key="b2b.login.pwchange.endpwchange"/>"></a>
		<% } %>
			
		<div class="login">
			<ul>
				<li class="text">
					<span>
						<% if ( ((String) request.getAttribute(PWChangeAction.ACTION_NAME)).equals(PWChangeAction.ACTION_US)) { %>
							<isa:translate key="b2b.login.pwchange.pwlength"/>
						<% }
						   else { %>
						   <isa:translate key="b2b.login.pwchange.pwchang"/>
						<% } %>
					</span>
				</li>
				<li class="label-input">
					<div>
						<label for="oldpw"><isa:translate key="b2b.login.pwchange.oldpwd"/></label><br/>
						<input type="password" id="oldpw" class="input-1" name="<%=PWChangeAction.PN_OLDPASSWORD%>" onkeypress="checkreturn(event)" />
					</div>
				</li>
				<li class="label-input">
					<div>
						<label for="newpw"><isa:translate key="b2b.login.pwchange.passwd"/></label><br/>
						<input type="password" id="newpw" class="input-1" name="<%=PWChangeAction.PN_PASSWORD%>" maxlength="<%=passwdLength%>"  onkeypress="checkreturn(event)" />
					</div>
				</li>
				<li class="label-input">
					<div>
						<label for="reppw"><isa:translate key="b2b.login.pwchange.passwdverify"/></label><br/>
						<input type="password" id="reppw" class="input-1" name="<%=PWChangeAction.PN_PASSWORD_VERIFY%>" maxlength="<%=passwdLength%>" onkeypress="checkreturn(event)" />
					</div>
				</li>
				<li class="label-input">
					<div class="button1">
						<a href="#" onclick="document.changepw.submit();" title="<isa:translate key="b2b.login.pwchange.submit"/>: <isa:translate key="b2b.login.pwchange.title"/>" 
							class="button" name="<%=(String)request.getAttribute(PWChangeAction.ACTION_NAME)%>" ><isa:translate key="b2b.login.pwchange.submit"/></a>
						
						
						<a href="#" onclick="location.href='<isacore:entryURL name="/b2b/init.do"/>'" title="<isa:translate key="b2b.login.pwchange.cancel"/>: <isa:translate key="b2b.login.pwchange.cancel"/>" 
							class="button" name="backward" ><isa:translate key="b2b.login.pwchange.cancel"/></a>
					</div>
				</li>
				<li>
					<div>&nbsp;</div>
				</li>
				<isa:message id="errortext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
					 type="<%=Message.ERROR%>">
					<li>
						<div class="error">
							<span>
								<%=errortext%>
							</span>
						</div>
					</li>	
				</isa:message>
				<isa:message id="infotext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
					 type="<%=Message.INFO%>">
					<li>
						<div class="error">
							<span>
								<%=infotext%><br/>
							</span>
						</div>
					</li>
				</isa:message>
			</ul>
			<% if (ui.isAccessible) { %>
				<a href="#start-login" name="end-login" title="<isa:translate key="b2b.login.pwchange.startpwchange"/>"></a>
			<% } %>
		</div>
	</form>
</div> <%-- selection --%>
	
	

</body>
</html>