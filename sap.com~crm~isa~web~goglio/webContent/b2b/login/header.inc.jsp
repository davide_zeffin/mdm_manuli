



<%
if (ui.isPortal) { %>
  <div id="header-portal">
<%
} else { %>
  <div id="header-appl">
<%
}%>

  <div class="header-logo">
  </div>

  <div class="header-applname">
    <isa:translate key="b2b.jsp.applicationName"/>
  </div>

  <div class="header-username">
      <%
      User user = (User) request.getAttribute(BusinessObjectBase.CONTEXT_NAME);
      if (user != null) { %>
        <%=user.getAddress().getTitle()%>&nbsp;<%=user.getAddress().getName()%>
      <%
      }%>
  </div>

  <%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
  <%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
  <%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
           so make sure to test your work in these browsers. --%>
  <div id="header-extradiv1">
          <span></span>
  </div>
  <div id="header-extradiv2">
          <span></span>
  </div>
  <div id="header-extradiv3">
          <span></span>
  </div>
  <div id="header-extradiv4">
          <span></span>
  </div>
  <div id="header-extradiv5">
          <span></span>
  </div>
  <div id="header-extradiv6">
          <span></span>
  </div>

</div> <%-- header-appl --%>

