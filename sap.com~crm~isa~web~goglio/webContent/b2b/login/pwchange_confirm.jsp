<%-- pwchange_confirm.jsp --%>
<%-- This JSP displays the PasswordChange confirmation screen --%>

<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.PWChangeAction" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.LoginUI" %>

<%
	LoginUI ui = new LoginUI(pageContext);
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	  	<title><isa:translate key="b2b.login.pwchange.title"/></title>
		<isa:stylesheets/>
		
		<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/buttons.js") %>"
			type="text/javascript">
		</script>			
		
		<script language="JAVAScript">
		<!--
			function return_event(){
				document.getElementById("pwchange").submit();
			}
		//-->
		</script>
		
	</head>


<body class="selection-shop" onkeypress="checkreturn(event)">

<% if (ui.isPortal) { %>
	<div id="header-portal">
<% } else { %>
	<div id="header-appl">
<% } %> 

	<div class="header-logo">
	</div>
	
	<div class="header-applname">
		B2B <i>Shop International</i>
	</div>

	<div class="header-username">
	
	</div>

	<%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
	<%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
	<%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
		 so make sure to test your work in these browsers. --%>
	<div id="header-extradiv1">
		<span></span>
	</div>
	<div id="header-extradiv2">
		<span></span>
	</div>
	<div id="header-extradiv3">
		<span></span>
	</div>
	<div id="header-extradiv4">
		<span></span>
	</div>
	<div id="header-extradiv5">
		<span></span>
	</div>
	<div id="header-extradiv6">
		<span></span>
	</div>
</div> <%-- header-appl --%>

<div class="selection">
	<div class="module-name"><isa:moduleName name="b2b/login/pwchange_confirm.jsp" /></div>
	<h1><isa:translate key="b2b.login.pwchange.head"/></h1>

	<div class="login">
		<form method="post" action="<isa:webappsURL name="/b2b/pwchange.do"/>?<%=PWChangeAction.PN_ACTION_CONTINUE%>=X" name="pwchange">
			<ul>
				<isa:message id="errortext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
					type="<%=Message.ERROR%>" >
					<li>
						<div class="error">
							<span>
								<%=errortext%><br/>
							</span>
						</div>
					</li>
				</isa:message>
				<isa:message id="infotext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
					type="<%=Message.INFO%>" >
					<li>
						<div class="message">
							<span>
								<%=infotext%><br/>
							</span>
						</div>
					</li>
				</isa:message>
				<li class="label-input">
					<div class="button1">
						<a href="#" onclick="document.pwchange.submit();" class="button" title="<isa:translate key="b2b.login.pwchange.forward"/>: <isa:translate key="b2b.login.pwchange.forward.text"/>" ><isa:translate key="b2b.login.pwchange.forward"/></a>
					</div>
				</li>
			</ul>
		</form> 
	</div> <%-- login --%>

</div> <%-- selection --%>

</body>
</html>