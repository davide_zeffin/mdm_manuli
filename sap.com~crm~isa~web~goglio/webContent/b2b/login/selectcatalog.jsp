<% /** This JSP displays all catalogs for the chosen shop with their variants to the user,
           to select one and gives the result to the ReadCatalogAction.
           author : Martin Schley
   **/
%>
<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<%-- import the libs used on this page --%>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.SelectCatalogAction" %>
<%@ page import="com.sap.isa.isacore.action.b2b.ReadCatalogAction" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.lang.*" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%
String[] evenOdd = new String[] { "even", "odd" };

BaseUI ui = new BaseUI(pageContext);

// needed for Accessibility (number of rows)
ResultData rd = (ResultData) request.getAttribute(SelectCatalogAction.RK_CATALOG_LIST);

%>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title><isa:translate key="b2b.selectcatalog.title"/></title>
  <isa:stylesheets/>
</head>

<body class="shoplist">

  <div id="scrollable-selection">

  <%@ include file="/b2b/login/header.inc.jsp" %>
  
  <div class="module-name"><isa:moduleName name="b2b/login/selectcatalog.jsp" /></div>

  <div class="selection">
    <h1><isa:translate key="b2b.selectcatalog.title"/></h1>


    <div class="filter-result">
      <%
      if (ui.isAccessible) { %>
              <a href="#end-table1" title="<isa:translate key="shop.jsp.acc.tabletext1"/> [2] <isa:translate key="shop.jsp.acc.tabletext2"/> [<%= rd.getNumRows() %>] <isa:translate key="shop.jsp.acc.tabletext3"/>"></a>
      <%
      } %>
      <table summary="Table with all available catalogs. Please select one by activating the link.">
        <tr>
          <th scope="col"><isa:translate key="b2b.selectcatalog.catalogid"/></th>
          <th scope="col"><isa:translate key="b2b.selectcatalog.catalogdes"/></th>
        </tr>
        <%-- iteration over the table with catalogs --%>
        <% int line = 0; %>
        <isa:iterate id="catalog" name="<%= SelectCatalogAction.RK_CATALOG_LIST %>" type="com.sap.isa.core.util.table.ResultData">
          <tr class="<%= evenOdd[++line % 2]%>">
            <td><a href="<isa:webappsURL name="/b2b/readcatalog.do"/><%=ReadCatalogAction.createRequest(catalog) %>"><%=JspUtil.encodeHtml(catalog.getString(User.RD_CATALOG_KEY))%></a></td>
            <td><a href="<isa:webappsURL name="/b2b/readcatalog.do"/><%=ReadCatalogAction.createRequest(catalog) %> "><%=JspUtil.encodeHtml(catalog.getString(User.RD_CATALOG_DESCRIPTION))%></a></td>
          </tr>
        </isa:iterate>
      </table>
      <%
      if (ui.isAccessible) { %>
        <a name="end-table1" title="<isa:translate key="shop.jsp.acc.tabletext4"/>"></a>
      <%
      } %>
        </div> <%-- filter-result --%>
  </div> <%-- selection --%>
</div> <%-- scrollable-selection --%>

</body>
</html>