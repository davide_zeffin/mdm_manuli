<%-- selectsoldto.jsp --%>
<%--
        This JSP displays all soldtos for the user,
    to select one and gives the result to the ReadSoldToAction.
--%>

<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.SelectSoldToAction" %>
<%@ page import="com.sap.isa.isacore.action.b2b.ReadSoldToAction" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.LoginUI" %>

<%-- import the requiered tag libraries --%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ include file="/b2b/usersessiondata.inc" %>

<%

String[] evenOdd = new String[] { "even", "odd" };

LoginUI ui = new LoginUI(pageContext);

// Portal integration (remove header and adjust table width)
String topTableWidth = "70%";
if (ui.isPortal) {
  topTableWidth = "90%";
}

//needed for Accessibility (number of rows)
ResultData rd = (ResultData) request.getAttribute(SelectSoldToAction.RK_SOLDTO_LIST);

%>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
        <head>
                <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
                <title><isa:translate key="b2b.htmltitel.standard"/></title>
                <isa:stylesheets/>
        </head>

<body class="selectsoldto">
<div class="module-name"><isa:moduleName name="b2b/login/selectsoldto.jsp" /></div>
<div id="scrollable-selection">

  <%@ include file="/b2b/login/header.inc.jsp" %>


  <div class="selection">
        <h1>
                <% if (ui.isCompPFReseller()) { %>
                        <isa:translate key="b2b.login.selectreseller.title"/>
                <% } else { %>
                        <isa:translate key="b2b.login.selectsoldto.title"/>
                <% } %>
        </h1>

        <div class="filter-result">
                <% if (ui.isAccessible) { %>
                        <a href="#end-table1" title="<isa:translate key="shop.jsp.acc.tabletext1"/> [8] <isa:translate key="shop.jsp.acc.tabletext2"/> [<%= rd.getNumRows() %>] <isa:translate key="shop.jsp.acc.tabletext3"/>"></a>
                <% } %>
                <table summary="Table with all available shops. Please select one by activating the link.">
                        <tr>
                                <th scope="col">
                                        <%
                                                String linktitle = "";
                                                if (ui.isCompPFReseller()) {
                                                        linktitle = "b2b.login.selectreseller.id";
                                        %>
                                                        <isa:translate key="b2b.login.selectreseller.id"/>
                                        <% } else {
                                                        linktitle = "b2b.login.selectsoldto.soldto";
                                        %>
                                                        <isa:translate key="b2b.login.selectsoldto.soldto"/>
                                        <% } %>
                                </th>
                                <th scope="col"><isa:translate key="b2b.login.selectsoldto.name"/></th>
                                <th scope="col"><isa:translate key="b2b.login.selectsoldto.street"/></th>
                                <th scope="col"><isa:translate key="b2b.login.selectsoldto.houseno"/></th>
                                <th scope="col"><isa:translate key="b2b.login.selectsoldto.zip"/></th>
                                <th scope="col"><isa:translate key="b2b.login.selectsoldto.city"/></th>
                                <th scope="col"><isa:translate key="b2b.login.selectsoldto.region"/></th>
                                <th scope="col"><isa:translate key="b2b.login.selectsoldto.country"/></th>
                        </tr>
                        <%-- iterate over the list of soldtos and render a table showing them --%>
                        <% int line = 0; %>
                        <isa:iterate id="soldto" name="<%= SelectSoldToAction.RK_SOLDTO_LIST %>"
                                type="com.sap.isa.core.util.table.ResultData">
                                <tr class="<%= evenOdd[++line % 2]%>">
                                        <td>
                                                <a href="<isa:webappsURL name="/b2b/readsoldto.do"/><%=ReadSoldToAction.createRequest(soldto) %>"
                                                        title="<%= soldto.getString("SOLDTO") %>: <isa:translate key="shop.jsp.soldto.link"/>" <isa:translate key="<%= linktitle %>"/>.">
                                                        <%= soldto.getString("SOLDTO") %>
                                                </a>
                                        </td>
                                        <td><%= JspUtil.encodeHtml(soldto.getString("NAME1")) %><%= JspUtil.encodeHtml(soldto.getString("LASTNAME")) %></td>
                                        <td><%= JspUtil.encodeHtml(soldto.getString("STREET")) %></td>
                                        <td><%= JspUtil.encodeHtml(soldto.getString("HOUSE_NO")) %></td>
                                        <td><%= JspUtil.encodeHtml(soldto.getString("POSTL_COD1")) %></td>
                                        <td><%= JspUtil.encodeHtml(soldto.getString("CITY")) %></td>
                                        <td><%= JspUtil.encodeHtml(soldto.getString("REGIONTEXT50")) %></td>
                                        <td><%= JspUtil.encodeHtml(soldto.getString("COUNTRYTEXT")) %></td>
                                </tr>
                        </isa:iterate>
                </table>
                <% if (ui.isAccessible) { %>
                        <a name="end-table1" title="<isa:translate key="shop.jsp.acc.tabletext4"/>"></a>
                <% } %>
        </div> <%-- filter-result --%>
</div> <%-- selection --%>
</div> <%-- scrollable-selection --%>

</body>
</html>