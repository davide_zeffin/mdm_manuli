<% /** This JSP displays the login screen to enter the login data.After pressing Submit,
       the LoginParameter will be given to the LoginAction, which verifies the login in
       the CRM-System.Afterwards the User will either be logged on or informed by a Message,
       why he was not logged on the system.
       author : Martin Schley
   **/
%>
<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.LoginAction" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.LoginUI" %>

<%
	LoginUI ui = new LoginUI(pageContext);
%>


<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  		<title><isa:translate key="b2b.login.jsp.title"/></title>

		<isa:stylesheets/>
		
		<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/buttons.js") %>"
			type="text/javascript">
		</script>

		<script type="text/javascript">
		<!--
	  		var submitcount=0;
	  		function checkFields() { 
	  
			    if (submitcount == 0){
			        submitcount++;
			        return true;
			    }
			    else {
			        return false;
			    }
	  		}
	  		
	  		function startLogin(name, value){
				document.getElementById("hiddenfield").name = name;
				document.getElementById("hiddenfield").value = value;
				document.getElementById("login_form").submit();
	  		}
	  		
	  		function return_event(){
				startLogin('<%=LoginAction.PN_LOGIN%>', '<isa:translate key="b2b.login.jsp.login"/>');
	  		}
	  //-->
	  </script>
	
	</head>
	
<body class="login-body">

<% if (ui.isPortal) { %>
	<div id="header-portal">
<% } else { %>
	<div id="header-appl">
<% } %> 

	<div class="header-logo">
	</div>
	
	<div class="header-applname">
		B2B <i>Shop International</i>
	</div>

	<div class="header-username">
	
	</div>

	<%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
	<%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
	<%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
		 so make sure to test your work in these browsers. --%>
	<div id="header-extradiv1">
		<span></span>
	</div>
	<div id="header-extradiv2">
		<span></span>
	</div>
	<div id="header-extradiv3">
		<span></span>
	</div>
	<div id="header-extradiv4">
		<span></span>
	</div>
	<div id="header-extradiv5">
		<span></span>
	</div>
	<div id="header-extradiv6">
		<span></span>
	</div>
</div> <%-- header-appl --%>


<div class="selection">
	<div class="module-name"><isa:moduleName name="b2b/login/login.jsp" /></div>
	<%-- create a form to read username, password and locale --%>
	<% User user = (User) request.getAttribute(BusinessObjectBase.CONTEXT_NAME);
	   if (user == null) {
		user = new User();
	   }
	%>
	
	<script type="text/javascript">
		<!--
		  // determine the browser version
		  document.write('<input type="hidden" name="browsername" value="' +  escape(navigator.userAgent.toLowerCase()) + '">');
		  document.write('<input type="hidden" name="browsermajor" value="' + escape(parseInt(navigator.appVersion)) + '">');
		  document.write('<input type="hidden" name="browserminor" value="' + escape(parseFloat(navigator.appVersion)) + '">');
		//-->
	</script>
		
	<h1><isa:translate key="b2b.login.jsp.welcome"/></h1>
	
	<form method="post" action='<isa:webappsURL name="b2b/login.do"/>' id="login_form"
		onsubmit="return checkFields()">
			  
		<% if (ui.isAccessible) { %>
			<a href="#end-login" name="start-login" title="<isa:translate key="b2b.login.jsp.endlogin"/>"></a>
		<% } %>
		<div class="login">
			<input type="hidden" id="hiddenfield" />
			<ul>
				<isa:message id="UserInfo"
						name="<%=BusinessObjectBase.CONTEXT_NAME%>"
						type="<%=Message.INFO%>"
						ignoreNull="true">
					<li>
						<div class="info">
							<span>
								<%=UserInfo%>
							</span>
						</div>
					</li>
				</isa:message>
				<li class="label-input">
					<div>
						<label for="userid"><isa:translate key="b2b.login.jsp.userId"/></label><br/>
						<input class="input-1" type="text" id="userid" name="<%=LoginAction.PN_USERID%>" value="<%=JspUtil.replaceSpecialCharacters(user.getUserId())%>" onkeypress="checkreturn(event)" />
					</div>
				</li>
				<li class="label-input">
					<div>
						<label for="password"><isa:translate key="b2b.login.jsp.password"/></label><br/>
						<input class="input-1" type="password" id="password" name="<%=LoginAction.PN_PASSWORD%>" onkeypress="checkreturn(event)" />
					</div>
				</li>
				<li>
					<div class="button1">
						<a href="#" onclick="startLogin('<%=LoginAction.PN_LOGIN%>', '<isa:translate key="b2b.login.jsp.login"/>');" title="<isa:translate key="b2b.login.jsp.login"/>: <isa:translate key="b2b.login.jsp.logintitle"/>" 
							class="button" name="<%=LoginAction.PN_LOGIN%>" > <isa:translate key="b2b.login.jsp.login"/></a>
						<a href="#" onclick="startLogin('<%=LoginAction.PN_PASSWORD_CHANGE%>', '<isa:translate key="b2b.login.jsp.pwchange"/>');" title="<isa:translate key="b2b.login.jsp.pwchange"/>: <isa:translate key="b2b.login.jsp.cpwdtitle"/>" 
							class="button" name="<%=LoginAction.PN_PASSWORD_CHANGE%>" ><isa:translate key="b2b.login.jsp.pwchange"/></a>
					</div>
				</li>
				<li>
					<div>&nbsp;</div>
				</li>
				<isa:message id="UserError"
						name="<%=BusinessObjectBase.CONTEXT_NAME%>"
						type="<%=Message.ERROR%>"
						ignoreNull="true">
					<li>
						<div class="error">
							<span>
								<%=UserError%>
							</span>
						</div>
					</li>
				</isa:message>
			</ul>
			<% if (ui.isAccessible) { %>
				<a href="#start-login" name="end-login" title="<isa:translate key="b2b.login.jsp.beginlogin"/>"></a>
			<% } %>
		</div><%-- filter-result --%>
	</form>
</div> <%-- selection --%>

</body>
</html>
