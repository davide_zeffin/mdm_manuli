<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>

<%@ include file="/b2b/usersessiondata.inc" %>

<% // Portal integration (remove header)
   boolean calledFromPortal = (((IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).getPortal().equals("YES") ? true : false);
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />>
		<title><isa:translate key="b2b.htmltitel.standard"/></title>
		<isa:stylesheets/>
	</head>

	<body class="selection-shop">
		<div class="module-name"><isa:moduleName name="b2b/login/error_ume.jsp" /></div>
		<% if (calledFromPortal) { %>
			<div id="header-portal">
		<% } else { %>
			<div id="header-appl">
		<% } %> 

			<div class="header-logo">
			</div>

			<div class="header-applname">
				B2B <i>Shop International</i>
			</div>

			<div class="header-username">

			</div>

			<%-- These extra divs/spans may be used as catch-alls to add extra imagery. --%>
			<%-- Add a background image to each and use width and height to control sizing, place with absolute positioning --%>
			<%-- There's a rather nasty colour shift bug with transparent GIFs in Netscape 6/7 and Mozilla v1.0 up to v1.3 (in which it's fixed),
				 so make sure to test your work in these browsers. --%>
			<div id="header-extradiv1">
				<span></span>
			</div>
			<div id="header-extradiv2">
				<span></span>
			</div>
			<div id="header-extradiv3">
				<span></span>
			</div>
			<div id="header-extradiv4">
				<span></span>
			</div>
			<div id="header-extradiv5">
				<span></span>
			</div>
			<div id="header-extradiv6">
				<span></span>
			</div>
		</div> <%-- header-appl --%>
	<% } %>

	<div class="selection">
		<h1><isa:translate key="b2b.htmltitel.standard"/></h1>
		<div class="login">
			<ul>
				<li>
					<div class="info">
						<span><isa:translate key="user.ume.error.logon"/><br/></span>
					</div>
				</li>
				<isa:message id="errortext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
					type="<%=Message.ERROR%>" >
					<li>
						<div class="error">
							<span><%=errortext%><br/></span>
						</div>
					</li>
				</isa:message>
				<isa:message id="infotext" name="<%=BusinessObjectBase.CONTEXT_NAME%>"
					   type="<%=Message.INFO%>" >
					<li>
						<div class="info">
							<span><%=infotext%><br/></span>
						</div>
					</li>
				</isa:message>
				<li>
					<div class="button1">
						<a href="#" onClick="location.href='<isa:reentryURL name="/b2b/init.do"/>'" class="button">
							<isa:translate key="user.button.logon"/>
						</a>
					</div>
				</li>
			</ul>
		</div><%--login--%>
	</div><%--selection--%>

</body>
</html>