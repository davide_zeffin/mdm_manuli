<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.uiclass.ShopListUI" %>
<%@ page import="com.sap.isa.isacore.action.ShopShowListAction" %>

<% ShopListUI ui = new ShopListUI(pageContext);
   
   ui.setTitle(WebUtil.translate(pageContext, "shop.jsp.title", null));
   ui.setTableTitle(WebUtil.translate(pageContext, "b2c.shoplist.jsp.table1", null));
   ui.setTableItems((com.sap.isa.core.util.table.ResultData) request.getAttribute(ShopShowListAction.RK_SHOP_LIST));  
   String tableRowTitle[] = {WebUtil.translate(pageContext, "shop.jsp.id", null), WebUtil.translate(pageContext, "shop.jsp.description", null)};
   ui.setTableRowTitle(tableRowTitle);
   ui.setDisplayShopList(true); %>
   
   <%@ include file="displayList.inc.jsp"%>