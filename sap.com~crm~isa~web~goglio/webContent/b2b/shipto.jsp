<% /** This JSP displays a screen to enter or change an shipto
       author : Wolfgang Sattler
   **/
%>

<%@ page import="com.sap.isa.isacore.action.ShowShipToAction" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>
<%@ page import="com.sap.isa.businessobject.Address" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.businessobject.User" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.order.MaintainBasketNewShiptoAction"%>

<%  BaseUI ui = new BaseUI(pageContext); 
%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>


<%
  AddressFormular addressFormular = (AddressFormular) request.getAttribute(ActionConstants.RC_ADDRESS_FORMULAR);

  /* if the input fields should be disabled, the addressformular shouldn't not be editable. */

   String disabled = addressFormular.isEditable()?"":"disabled=\"disabled\"";

  /* read the address from the addressFormular */
  Address address  = addressFormular.getAddress();
%>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<%
		if (disabled.length() == 0) { %>
  			<title><isa:translate key="shipto.jsp.title"/></title>
<%
		}
		else {%>
    		<title><isa:translate key="shipto.jsp.title.display"/></title>
<%
		}%>

		<isa:stylesheets/>


  		<script type="text/javascript">
  		<!--
    		function loadRegionList() {
      			document.ShipTo.RegionList.value="load";
				document.ShipTo.submit();
			}
			
			function saveForm() {
             	document.ShipTo.save.value="true";
             	document.ShipTo.cancel.value="";
             	document.ShipTo.submit();  
             	return true;
            }
            
            function cancelForm() {
             	document.ShipTo.cancel.value="true";
             	document.ShipTo.save.value="";
             	document.ShipTo.submit();  
             	return true;
            }
			
   		//-->
  		</script>

	</head>

<body class="shipto">

 <div class="module-name"><isa:moduleName name="b2b/shipto.jsp" /></div>
 
 <form method="post" action='<isa:webappsURL name="b2b/maintainshipto.do"/>' name="ShipTo" >
 
<% if (disabled.length() != 0) {%>

 <div class="popup">  
 
 
 <% if(ui.isPortal) { %>
  <div id="header-portal">
 <% } else { %>
  <div id="header-appl">
 <% } %>
   
     <div class="header-logo">
     </div>

     <div class="header-applname">
       <isa:translate key="b2b.jsp.applicationName"/>
     </div>
     <div id="header-extradiv1">
        <span></span>
      </div>
        
   </div>
   
 <% } %> 
   
 <h1><span>
	 <% if (disabled.length() == 0) { %>
	    <isa:translate key="shipto.jsp.header"/>
	 <%} else { %>
	    <isa:translate key="shipto.jsp.header.display"/>
	 <%}%>
 </span></h1> 
 
<% if (disabled.length() == 0) {%>

 <div id="document">  
 
  <div class="document-header">  
   
<% } else { %>   

  <div class="content">
  
<% } %>
   
     <input type="hidden" name="<%=ShowShipToAction.PARAM_ITEMKEY  %>"
           value="<%=JspUtil.removeNull(request.getAttribute(ShowShipToAction.RC_ITEMKEY))%>" />

     <input type="hidden" name="<%=ShowShipToAction.PARAM_SHIPTO_KEY  %>"
           value="<%= JspUtil.removeNull(request.getAttribute(ShowShipToAction.RC_SHIPTO_KEY))%>" />

     <input type="hidden" name="<%=MaintainBasketNewShiptoAction.SHIPTO_HANDLE  %>"
           value="<%= JspUtil.removeNull(request.getAttribute(MaintainBasketNewShiptoAction.SHIPTO_HANDLE))%>" />           
           
   <div class="group">
   <table class="data" width="100%" border="0" cellspacing="0" cellpadding="1">
    <%-- include the address data --%>
    <% if (addressFormular.getAddressFormatId() == 0){ %>

      <%@ include file="shiptodetails.jsp" %>
    <%
    }
    else { %>
      <%@ include file="shiptodetails1.jsp" %>
    <%
    } %>

  <% if (disabled.length() == 0) {%>
    <tr>
      <td colspan="3" class="value">(*) <isa:translate key="shipto.jsp.rquiredFields"/></td>
    </tr>
  <% } %>
    <tr>
      <td colspan="3">
        <isa:message id="errortext" name="address" type="<%=Message.ERROR%>" property="" >
         <div class="error" >
           <span><%= JspUtil.removeNull(errortext) %></span>
         </div>
        </isa:message>        
       </td>
      </tr>
     </table>
    </div>
   </div>
  </div>
  
  <div id="buttons">
    <ul class="buttons-1">     
	  <% if (disabled.length() == 0) {
	  %>	      
	      <li>
	        <a href="#" onclick="saveForm();"><isa:translate key="shipto.jsp.save"/></a>
	        <input type="hidden" name="save" value="" />
	      </li>
	      <li>
	      	<a href="#" onclick="cancelForm();"><isa:translate key="shipto.jsp.cancel"/></a>
	        <input type="hidden" name="cancel" value="" />
	      </li>
	      
	  <%}
	    else {
	  %>
	      <li><a href="#" onclick="window.close();"><isa:translate key="shipto.jsp.close"/></a></li>
	  <%}%>
	  </ul>
     </div>
     
   </form>

 </div>
 
</body>
</html>