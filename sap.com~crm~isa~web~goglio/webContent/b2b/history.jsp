<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%--
Displays all elements of the history.
Precondition: Reference on the History object in the request constant "historylist".

Since there is the complete History Object with all HistoryItem objects in the
request context, you can easily display other data in the history.
See the documentation of HistoryItem, if you want to know which data are available.
--%>
<%@ page import="java.util.List" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.history.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.IsaCoreInitAction"%>
<%@ page import="com.sap.isa.isacore.action.IsaCoreInitAction.StartupParameter"%>

<%@ page import="com.sap.isa.isacore.uiclass.b2b.HistoryUI" %>

<%@ include file="/b2b/usersessiondata.inc" %>

<%-- import the required tag libraries --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<% 
        HistoryUI ui = new HistoryUI(pageContext);
        // Get Application Area (could be "separate billing display") (see also StartApplicationAction)
        boolean isAreaSepBilling = ( ActionConstants.APPLAREA_SEPBILL.equals(userSessionData.getAttribute(ActionConstants.SC_APPLAREA)) ? true : false );
        StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
%>

<isa:contentType />
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
        <head>
                <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
                <title><isa:translate key="b2b.htmltitel.standard"/></title>
                <isa:stylesheets/>
                          
                <script type="text/javascript">
                <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
                </script>       
                                  
                <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
                        type="text/javascript">
                </script>              
    
        <script type="text/javascript">
        <!--     
            function switch_to_documentView() {
                <% if ( ! isAreaSepBilling) { %>
                <%-- when a document from history is selected it has to be ensured that the header frame --%>
                isaTop().header.busy(parent.form_input);
                
                <%-- refresh navigator screen only when document view is not currently selected (isDocumentView=false) --%>
                if (!isaTop().header.isDocumentView) {
                   <%-- switches to document view => highlight document link. --%>
                   isaTop().parent.location.href="<isa:webappsURL name="/b2b/catalogend.do"/>";
                   <%-- isaTop().header.highlight_document_link(); --%>
                   <%-- organizer_nav().location.href = '<isa:webappsURL name="b2b/updateorganizernav.do"/>'; --%>
                   <%-- organizer_content().location.href = '<isa:webappsURL name="b2b/updateorganizercont.do"/>'; --%>
                } 
                <% } %>
            }
        //-->
        </script>
        
    </head>

    <body class="history">
                <% if (ui.isAccessible) { %>
                        <a id="access-history" href="#"  title="<isa:translate key="b2b.history.acc.lastdocs.title"/>" accesskey="<isa:translate key="b2b.history.accesskey"/>"></a>
                <% } %>
                <h1><span title="<isa:translate key="b2b.history.header.tooltip"/>"><isa:translate key="b2b.history.header.small"/></span></h1>
                
                <% if (request.getAttribute(UpdateHistoryAction.HISTORY_LIST) != null &&
                           !((List)request.getAttribute(UpdateHistoryAction.HISTORY_LIST)).isEmpty()) {
                %>
                <div class="history-items">
                <% if (ui.isAccessible) { %>
                        <a name="liststart" href="#listend" title="<isa:translate key="b2b.history.acc.beginlist"/>"></a>
                <% } %>
                        <ul>
                                <%-- loop over all entries in the history --%>
                                <isa:iterate id="historyItem" name="<%= UpdateHistoryAction.HISTORY_LIST %>"
                                         type="com.sap.isa.isacore.HistoryItem">                 
                                    <% ui.setHistoryItem(historyItem); %>
                                    <%   if ( ( ! startupParameter.isBillingIncluded() && 
                                                ( isAreaSepBilling  &&  ui.getHref().indexOf("billing=X") >  0)  
                                             || (!isAreaSepBilling  &&  ui.getHref().indexOf("billing=X") == -1) )
                                           || startupParameter.isBillingIncluded() ) { %>
                                         <% String urlTarget = " target=\"form_input\" "; // Show content in the middle frame! %>
                                        <li>
                                                <%  if (!ui.getHref().equals("")) { %>
                                                                <a class="historyHeaderLink" href='<isa:webappsURL name="<%= ui.getHref() %>"/>' onclick="switch_to_documentView()" <%=urlTarget%>>
                                                <% }
                                                        if ("basket".equals(ui.getDocType())) { %>
                                                                <isacore:ocitranslate key="<%= ui.getKey() %>"/>   <%-- display the type of the document in basket case--%>
                                                <%  }else { %>
                                                                <isa:translate key="<%= ui.getKey() %>"/>   <%-- display the type of the document in non basket case --%>
                                                <%  }
                                                        if (ui.getHref() != null) { %>
                                                                </a>
                                                <% } %> 
                                                
                                                <div>
                                                        <%-- display default information --%>
                                                        <a class="historyLink" href='<isa:webappsURL name="<%= ui.getHref() %>"/>' <%=urlTarget%>  
                                                                title='<isa:translate key="b2b.history.item.refno"/> <%= JspUtil.encodeHtml(ui.getTooltipRefNo()) %><isa:translate key="b2b.history.item.refname"/> <%= JspUtil.encodeHtml(ui.getTooltipRefName()) %>' onclick="switch_to_documentView();">
                                                                <span title="<isa:translate key="status.sales.list.documentno"/>"><%= JspUtil.encodeHtml(ui.getDocNumber()) %></span><br/>
                                                                <span title="<isa:translate key="gs.att.lbl.creation.date"/>"><%= JspUtil.encodeHtml(ui.getDate()) %></span><br/>
                                                                <% if(ui.getRefNo().length() > 0) { %>
                                                                        <span title="<isa:translate key="b2b.ncontract.header.reference"/>"><%= JspUtil.encodeHtml(ui.getRefNo()) %></span><br/>
                                                                <% } %>
                                                                <% if(ui.getRefName().length() > 0) { %>
                                                                        <span title="<isa:translate key="b2b.ncontract.header.description"/>"><%= JspUtil.encodeHtml(ui.getRefName()) %></span>
                                                                <% } %>
                                                        </a>
                                                </div>
                                        </li>
                                    <% } %>
                                </isa:iterate>
                        </ul>
                        <% if (ui.isAccessible) { %>
                                <a name="listend" href="#liststart" title="<isa:translate key="b2b.history.acc.endlist"/>"></a>
                        <% } %>
                </div>
                <% } %>
                
                <%-- If someone clicks on a document in the history, it might be the case,
                         that an operator has deleted this document in the backend. In this case,
                         the corresponding action should remove this document from the history,
                         set History.NODOCFOUND in the request context and refresh the history.
                         The JSP will then display a warning. --%>
                <%  if (userSessionData.getAttribute(History.NODOCFOUND) != null  &&  ((String)userSessionData.getAttribute(History.NODOCFOUND)).equals("true")) { 
                                userSessionData.removeAttribute(History.NODOCFOUND); %>
                <SCRIPT LANGUAGE="JavaScript">
                  organizer_content().location.reload();
                  alert("<isa:translate key="b2b.history.error.docnotfound"/>");
                </SCRIPT>
                <% } %>         
                
    </body>
</html>