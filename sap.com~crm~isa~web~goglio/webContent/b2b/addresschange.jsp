<%-- addresschange.jsp --%>

<%-- This JSP displays the AddressChange screen to change the current address data --%>

<%@ page import="com.sap.isa.isacore.action.b2b.ReadAddressDataAction" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.isacore.action.IsaCoreInitAction" %>
<%@ page import="com.sap.isa.isacore.AddressFormular" %>
<%@ page import="com.sap.isa.businessobject.BusinessObjectBase" %>
<%@ page import="com.sap.isa.businessobject.User" %>
<%@ page import="com.sap.isa.businessobject.Address" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.table.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ taglib uri="/isa" prefix="isa" %>


<% // The address data are contained within the address formular. This object should never be 'null'.
   AddressFormular addressFormular = (AddressFormular) request.getAttribute(ActionConstants.RC_ADDRESS_FORMULAR);
   // flag that indicates whether the user represents a person or an organization
   boolean isPerson;

   // list of regions may be 'null'
   ResultData listOfRegions = (ResultData) request.getAttribute(ActionConstants.RC_REGION_LIST);
   // list of counties may be 'null'
   ResultData listOfCounties = (ResultData) request.getAttribute(ActionConstants.RC_POSSIBLE_COUNTIES);

   // the index variable will be used to fill a JavaScript array
   int index;

   UserSessionData userSessionData =
              UserSessionData.getUserSessionData(pageContext.getSession());

   // Are we running in a portal?
   // check if portal is given as an startupParameter

   IsaCoreInitAction.StartupParameter startupParameter =
       (IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
   boolean portal = false;

   if (startupParameter != null) {
       String portalParameter = startupParameter.getPortal();
       if (portalParameter.length() > 0) {
           if (portalParameter.equals("YES")) {
               portal = true;
           }
       }
   }

   BaseUI ui = new BaseUI(pageContext);

%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<title><isa:translate key="userSettings.adrchange.title"/></title>
	<isa:stylesheets/>

	<script type="text/javascript">
	<!--
		<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	//-->
	</script>

   	<script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/addressdetails_maintenance.js") %>"
           type="text/javascript">
   	</script>

</head>


<body class="usersettings">
<div class="module-name"><isa:moduleName name="b2b/addresschange.jsp" /></div>

<h1><isa:translate key="userSettings.jsp.addresschange"/></h1>

<div id="scrollable-selection">
  <div class="selection">

	<a id="addrchange" href="#" title="<isa:translate key="b2b.userSettings.adrchange.acctext"/>" accesskey="<isa:translate key="b2b.userSettings.adrchange.acckey"/>" ></a>

	<div class="login">

	<div>
	  	<p class="p1"><span><isa:translate key="userSettings.adrchange.head"/></span></p>
	</div>

	<form action="<isa:webappsURL name="/b2b/changeAddressData.do"/>" name="personalDetail" method="post">
		<input type="hidden" name="soldtoChange" value="" />

		<% if (ui.isAccessible) { %>
			<a href="#end-change" name="start-change" title="<isa:translate key="b2b.userSettings.adrchange.beginchange"/>"></a>
		<% } %>

		<table border="0" cellspacing="0" cellpadding="4">
			<tr>
				<td class="identifier">
				<isa:translate key="userSettings.adrchange.formAdr"/>
				</td>
				<td class="value">
					<select name="titleKey">
	      				<% // build up the selection options
	         				String defaultTitleKey =(String) addressFormular.getAddress().getTitleKey();
	         				isPerson = addressFormular.isPerson(); %>

	         			<isa:iterate id="formsOfAddress" name="<%= ActionConstants.RC_FORMS_OF_ADDRESS %>"
	                    	type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
	         				<% 	// If the user represents a person, only forms of address for persons should
	            				// be displayed. If the user doesn't represents a person, only forms of address
	            				// for organisation should be displayed. So a title change will never be forced.
	            				if ((isPerson && formsOfAddress.getBoolean("FOR_PERSON")) ||
	                				(!isPerson && !formsOfAddress.getBoolean("FOR_PERSON"))) {
	              					// Should the current title be marked as selected?
	              					String selected="";
	              					if (formsOfAddress.getString("ID").equals(defaultTitleKey)) {
	                					selected="SELECTED";
	              					}
	          					%>
	          						<OPTION VALUE="<%= JspUtil.encodeHtml(formsOfAddress.getString("ID")) %>" <%= selected %>>
	                         			<%= JspUtil.encodeHtml(formsOfAddress.getString("DESCRIPTION")) %>
	          						</OPTION>
	         					<% } /* if ((isPerson...  */ %>
	          			</isa:iterate>
	        		</select>
				</td>
			</tr>
				<% if ( isPerson) {%>
					<%-- first name --%>
					<tr>
						<td class="identifier">
							<label for="firstName"><isa:translate key="userSettings.adrchange.fstName"/></label>
						</td>
						<td class="value">
							<input class="textinput-large" type="Text" size="30" name="firstName" id="firstName"
								value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFirstName())) %>" >
						</td>
					</tr>
	  				<%-- last name --%>
					<tr>
		  				<td class="identifier">
							<label for="lastName"><isa:translate key="userSettings.adrchange.lstName"/></label>
						</td>
						<td class="value">
							<input class="textinput-large" type="Text" size="30" name="lastName" id="lastName"
								value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getLastName())) %>" >
						</td>
					</tr>
				<% } else { %>
					<%-- name1 --%>
					<tr>
						<td class="identifier">
							<label for="name1"><isa:translate key="userSettings.adrchange.company"/></label>
						</td>
						<td class="value">
							<input class="textinput-large" type="Text" size="30" name="name1" id="name1"
								value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName1())) %>" >
						</td>
					</tr>
					<%-- name2 --%>
					<tr>
						<td class="identifier">
							<label for="name2"><isa:translate key="userSettings.adrchange.conPers"/></label>
						</td>
						<td class="value">
							<input class="textinput-large" type="Text" size="30" name="name2" id="name2"
								value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getName2())) %>"  >
						</td>
					</tr>
				<% } %>
			<tr>
				<td class="identifier">
					<label for="addresspartner"><isa:translate key="userSettings.adrchange.company"/></label>
				</td>
				<td class="value">
<% 		 			ResultData soldTos  = (ResultData) request.getAttribute(ReadAddressDataAction.RK_SOLDTO_LIST);
	    			String chosenSoldTo = "";

	    			if(soldTos != null){
	        			chosenSoldTo = JspUtil.encodeHtml(soldTos.getTable().getRow(1).getField("SOLDTO").getString());

	        			if(soldTos.getNumRows() == 1){
	            			String soldToDesc = JspUtil.encodeHtml(soldTos.getTable().getRow(1).getField("NAME1").getString());
%>
	      					<%= soldToDesc %>
	      					<input type="hidden" name="addresspartner" value="<%=chosenSoldTo%>" id="addresspartner" />
<%					 	} else{
	            			String addressPartner = JspUtil.encodeHtml((String)request.getAttribute(ReadAddressDataAction.RK_ADDRESSPARTNER));
%>
	      					<select size="1" name="addresspartner" onChange="send_SoldtoChange()" id="addresspartner">
								<isa:iterate id="soldTosList" name="<%= ReadAddressDataAction.RK_SOLDTO_LIST %>"
									type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
<%						          	String soldToKey  = soldTosList.getString("SOLDTO");
	            					String soldToDesc = JspUtil.encodeHtml(soldTosList.getString("NAME1"));
	            					String selected="";
	            					// Should the current sold-to be marked as selected?
	            					if ( soldToKey.equals(addressPartner) ) {
	                					selected="selected";
	            					}
%>
	        						<option value="<%=soldToKey%>" <%=selected%>><%=soldToDesc%></option>
	        					</isa:iterate>
	      					</select>
<%		      			} // end: else
	    			} // End: if(soldTos != null)
%>
				</td>
			</tr>
			<tr>
				<td class="identifier">
					<label for="addressKey"><isa:translate key="userSettings.adrchange.sad"/></label>
				</td>
				<td class="value">
<%  				ResultData shortAddresses = (ResultData) request.getAttribute(ReadAddressDataAction.RK_SHORTADDRESS);
    				if(shortAddresses != null){
        				if(shortAddresses.getNumRows() == 1){
           		 			String addressKey = shortAddresses.getTable().getRow(1).getField("KEY").getString();
            				String addressDesc = JspUtil.encodeHtml(shortAddresses.getTable().getRow(1).getField("DESC").getString());
%>
      						<%=addressDesc%>
      						<input type="hidden" name="<%=ReadAddressDataAction.ADDR_TECHKEY%>" value="<%=addressKey%>" id="addressKey" />
<%      				} else{
            				String selAddressKey = JspUtil.encodeHtml((String)request.getAttribute(ReadAddressDataAction.ADDR_TECHKEY));
%>
      						<select size="1" name="<%=ReadAddressDataAction.ADDR_TECHKEY%>" id="addressKey">
        						<isa:iterate id="shortAddressesList" name="<%= ReadAddressDataAction.RK_SHORTADDRESS %>"
                      				type="com.sap.isa.core.util.table.ResultData" resetCursor="true" >
<%          						String addressKey  = shortAddressesList.getString("KEY");
            						String addressDesc = JspUtil.encodeHtml(shortAddressesList.getString("DESC"));
            						String selected="";
            						// Should the current sold-to be marked as selected?
            						if ( addressKey.equals(selAddressKey) ) {
                						selected="selected";
            						}
%>
 	      	 						<option value="<%=addressKey%>" <%=selected%>><%=addressDesc%></option>
        						</isa:iterate>
      						</select>
<%      				} // end: else
    				} // End: if(soldTos != null)
%>
				</td>
			</tr>
			<%-- telephone number --%>
			<tr>
			 	<td class="identifier">
					<label for="telephoneNumber"><isa:translate key="userSettings.adrchange.tel"/></label>
				</td>
				<td class="value">
					<input class="textinput-large" type="Text" size="30" name="telephoneNumber" id="telephoneNumber"
           				value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getTel1Numbr())) %>">
				</td>
			</tr>
			<%-- fax number --%>
			<tr>
				<td class="identifier">
		    		<label for="faxNumber"><isa:translate key="userSettings.adrchange.fax"/></label>
		    	</td>
		    	<td class="value">
					<input class="textinput-large" type="Text" size="30" name="faxNumber" id="faxNumber"
						value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getFaxNumber())) %>" >
				</td>
			</tr>
			<%-- hidden field: person --%>
			<% if (addressFormular.isPerson()) { %>
				<input type="hidden" name="person" value="true" />
			<% } else {
				// the empty string will be interpreted as 'false' by the request parser %>
				<input type="hidden" name="person" value="" />
			<% } %>

			<%-- eMail --%>
			<tr>
				<td class="identifier">
					<label for="email"><isa:translate key="userSettings.adrchange.email"/></label>
				</td>
				<td class="value">
					<input class="textinput-large" type="Text" name="email" size="30" id="email"
						value="<%= JspUtil.encodeHtml(JspUtil.removeNull(addressFormular.getAddress().getEMail())) %>" >
				</td>
			</tr>

			<%-- show info an derror messages --%>
			<isa:message id="UserInfo"
				name="<%=BusinessObjectBase.CONTEXT_NAME%>"
				type="<%=Message.INFO%>"
				ignoreNull="true">

				<tr>
					<td>
						<div class="info">
							<span><%=UserInfo%></span>
						</div>
					</td>
				</td>
			</isa:message>
			<isa:message id="UserError"
				name="<%=BusinessObjectBase.CONTEXT_NAME%>"
				type="<%=Message.ERROR%>"
				ignoreNull="true">

				<tr>
					<td>
						<div class="error">
							<span>
								<%=UserError%>
							</span>
						</div>
					</tr>
				</tr>
			</isa:message>
		</table>	
				<%-- hidden fields --%>
		<input type="hidden" name="countryChange"  value="" />
		<input type="hidden" name="cancelClicked"  value="" />
	</div>
	</form>
			<%-- Buttons --%>
 	<div id="buttons">
    	<ul class="buttons-1">
			<li>
				<a href="#" onclick="send_save()"><isa:translate key="userSettings.adrchange.saveBtn"/></a>
			</li>
			<% if (!portal) { %>
				<li>
					<a href="#" onclick="send_cancel()"><isa:translate key="userSettings.adrchange.cancBtn"/></a>
				</li>
			<% } %>
		</ul>
	</div>
		<% if (ui.isAccessible) { %>
			<a href="#start-change" name="end-change" title="<isa:translate key="b2b.userSettings.adrchange.endchange"/>"></a>
		<% } %>

  </div>
</div>

</body>
</html>
