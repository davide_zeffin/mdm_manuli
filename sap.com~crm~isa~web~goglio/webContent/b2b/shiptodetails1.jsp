<% /** This JSP displays a screen to enter or change an shipto
       author : Wolfgang Sattler
   **/
%>

<%@ page import="com.sap.isa.core.util.*" %>
<%@ taglib uri="/isa" prefix="isa" %>

  <tr>
    <td class="odd" colspan="3">
    <div class="module-name"><isa:moduleName name="b2b/shiptodetails1.jsp" /></div>
    </td>
  </tr>

  <% boolean isPerson = false;
     String name1 = JspUtil.encodeHtml(address.getName1());
     if( (name1 == null) || (name1.equals("")) ){
         name1 = JspUtil.encodeHtml(address.getLastName());
		 isPerson = true;
     } 
     String name2 = JspUtil.encodeHtml(address.getName2());
     if( (name2 == null) || (name2.equals("")) ){
         name2 = JspUtil.encodeHtml(address.getFirstName());
		 isPerson = true;
     } 
  %>  

  <tr>
    <td class="odd" colspan="2"><isa:translate key="shipto.jsp.name"/><% if (disabled.length() == 0) {%> * <%}%></td>
    <td class="even">
      <% if (isPerson == false) { %>
      <input type="text" class="textInput" name="name1" value="<%=JspUtil.removeNull(name1)%>" size="35" <%= disabled %>>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="name1" >
       <div class="error"> 
          <%=errortext%><br />
        </div>
      </isa:message>            
        <% } else if (isPerson == true) { %>
      <input type="text" class="textInput" name="lastName" value="<%=JspUtil.removeNull(name1)%>" size="35" <%= disabled %>>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="lastname" > 
       <div class="error"> 
          <%=errortext%><br />
        </div>
      </isa:message>                   
        <% } %>              
      <% if (isPerson == false) { %>
      <input type="text" class="textInput" name="name2" value="<%=JspUtil.removeNull(name2)%>" size="35" <%= disabled %>>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="name2" >
        <div class="error">
          <%=errortext%><br />
        </div>
      </isa:message>            
        <% } else if (isPerson == true) { %>
      <input type="text" class="textInput" name="firstName" value="<%=JspUtil.removeNull(name2)%>" size="35" <%= disabled %>>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="firstname" >  
       <div class="error">
          <%=errortext%><br />
        </div>
      </isa:message>                  
        <% } %>             
    </td>
  </tr>
  <tr>
    <td class="odd" colspan="2">
      <isa:translate key="shipto.jsp.houseno"/> / <isa:translate key="shipto.jsp.street"/>
    </td>
    <td class="even">
      <input type="text" class="textInput" name="houseNumber" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getHouseNo()))%>" size="7" <%= disabled %>>
      <input type="text" class="textInput" name="street" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getStreet()))%>" size="35" <%= disabled %>>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="houseNo" >
        <div class="error">
          <%=JspUtil.removeNull(errortext)%><br />
        </div>  
      </isa:message>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="street" >
        <div class="error">           
          <%=JspUtil.removeNull(errortext)%><br />
        </div>  
      </isa:message>
    </td>
  </tr>
  <tr>
    <td class="odd" colspan="2">
    <isa:translate key="shipto.jsp.city"/> / <isa:translate key="shipto.jsp.zip"/> <% if (disabled.length() == 0) {%> * <%}%></td>
    <td class="even">
      <input type="text" class="textInput" name="city" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getCity()))%>" size="35" <%= disabled %>>
      <%
      ResultData regionList = (ResultData) request.getAttribute(ActionConstants.RC_REGION_LIST);
        if (regionList != null && regionList.getNumRows() > 0) {%>
        <select class="submitDoc" name="region" <%= disabled %>>
		  <option value="">
          <isa:iterate id="region" name="<%= ActionConstants.RC_REGION_LIST %>"
              type="com.sap.isa.core.util.table.ResultData">
              <option value="<%=JspUtil.encodeHtml(region.getString(Shop.ID))%>"
                  <%= address.getRegion().equals(region.getString(Shop.ID))?"selected":"" %> >
              <%=JspUtil.encodeHtml(region.getString(Shop.ID))%>
          </isa:iterate>
        </select>
      <%
      }%>
      <input type="text" class="textInput" name="postalCode" value="<%=JspUtil.encodeHtml(JspUtil.removeNull(address.getPostlCod1()))%>" size="7" maxlength="10" <%= disabled %>>

      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="city" >
        <div class="error">
          <%=errortext%><br />
        </div>
      </isa:message>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="region" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="postlCod1" >
        <div class="error">
          <%=JspUtil.removeNull(errortext)%><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <tr>
    <td class="odd" colspan="2"><isa:translate key="shipto.jsp.country"/><% if (disabled.length() == 0) {%> * <%}%></td>
    <td class="even">
      <input type="hidden" name="RegionList" value="">
      <select class="submitDoc" name="country", <%= disabled %> onchange="loadRegionList()">
        <isa:iterate id="country" name="<%= ActionConstants.RC_COUNTRY_LIST %>"
            type="com.sap.isa.core.util.table.ResultData"
            resetCursor="true">
            <option value="<%=JspUtil.encodeHtml(country.getString(Shop.ID))%>"
                <%= address.getCountry().equals(country.getString(Shop.ID))?"selected":"" %> >
            <%=JspUtil.encodeHtml(country.getString(Shop.DESCRIPTION))%>
        </isa:iterate>
      </select>

      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="country" >
        <div class="error">
          <%=JspUtil.removeNull(errortext)%><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <% ResultData taxCodeList = (ResultData) request.getAttribute(ActionConstants.RC_TAXCODE_LIST);
    if (regionList != null && regionList.getNumRows() > 0 && taxCodeList!= null &&  taxCodeList.getNumRows() > 0) {
  %>
  <tr>
    <td class="odd" colspan="2"><isa:translate key="shipto.jsp.county"/><% if (disabled.length() == 0) {%> * <%}%></td>
    <td class="even">
      <select class="submitDoc" name="taxJurisdictionCode" <%= disabled %>>
        <isa:iterate id="taxCode" name="<%= ActionConstants.RC_TAXCODE_LIST %>"
            type="com.sap.isa.core.util.table.ResultData"
            resetCursor="true">
            <option value="<%=JspUtil.encodeHtml(taxCode.getString(User.TXJCD))%>"
                <%= address.getTaxJurCode().equals(taxCode.getString(User.TXJCD))?"selected":"" %> >
            <%=JspUtil.encodeHtml(taxCode.getString(User.COUNTY))%>
        </isa:iterate>
      </select>

      <% if(addressFormular.isEditable()) { %>
      <a href="#" class="icon" onclick="loadRegionList()"
        ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/refresh.gif") %>" 
              alt="<isa:translate key="shipto.jsp.counties.refresh"/>" 
      /></a>
      <% } %>

      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="taxJurisdictionCode" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="county" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <%}%>

  <tr>
    <td class="odd" colspan="2"><isa:translate key="shipto.jsp.mail"/></td>
    <td class="even"><input type="text" class="textInput" name="email" value="<%=JspUtil.encodeHtml(address.getEMail())%>" size="35" <%= disabled %>>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="eMail" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <tr>
    <td class="odd" colspan="2"><isa:translate key="shipto.jsp.telephone"/></td>
    <td class="even"><input type="text" class="textInput" name="telephoneNumber" value="<%=JspUtil.encodeHtml(address.getTel1Numbr())%>" size="35" <%= disabled %>>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="tel1Numbr" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>
  <tr>
    <td class="odd" colspan="2">Fax</td>
    <td class="even"><input type="text" class="textInput" name="faxNumber" value="<%=JspUtil.encodeHtml(address.getFaxNumber())%>" size="35" <%= disabled %>>
      <isa:message id="errortext" name="address" type="<%=Message.ERROR%>"
                   property="faxNumber" >
        <div class="error">
          <%= JspUtil.removeNull(errortext) %><br />
        </div>
      </isa:message>
    </td>
  </tr>