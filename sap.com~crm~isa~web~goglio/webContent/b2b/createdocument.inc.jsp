<%--
********************************************************************************
    File:         createdocument.jsp
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:
    Created:      May 2001
    Version:      1.0

    $Revision: #23 $
    $Date: 2002/06/28 $
********************************************************************************
--%>
<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.helpvalues.*" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.GetDealerFamilyAction" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.*" %>
<%@ page import="com.sap.isa.businesspartner.backend.boi.*" %>

<%
    CreateTransactionUI ui = new CreateTransactionUI(pageContext);
    String dealerIdName = "orderDealer";
    FieldSupport field;
%>
    <title><isa:translate key="b2b.htmltitel.standard"/></title>

    <script type="text/javascript">
            <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/make_new_doc.js" ) %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js" ) %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
            type="text/javascript">
    </script>

    <script type="text/javascript">
        <!--
            function save_document() {
        		 
                displayBusy();

				var i = 0;
                var docType = "";
                var radioObject= document.getElementsByName("orderType");

                while(radioObject[i] != null) {
                   if (radioObject[i].checked == true) {
                    var docType = radioObject[i].value;
                   }
                   i++;
                }
				var dealeridName = "orderDealer";
                if (docType.substring(0,1) == "q") {
                  dealeridName = "quotationDealer";
                }
                else if (docType.substring(0,1) == "t"){
                  dealeridName = "templateDealer";
                }
                
				getElement("newOrder").soldToId.value = getSoldTo(dealeridName);
                getElement("newOrder").submit();            
            }

            function cancel_document() {
              getElement("newOrder").cancel.value="true";
              getElement("newOrder").submit();
            }
		//-->
    </script>

  <%-- display waiting screen --%>
  <%@ include file="/appbase/busy.inc.jsp" %>    
  
  <div id="page">

  <div class="module-name"><isa:moduleName name="b2b/createdocument.inc.jsp" /></div>

  <form id="newOrder" method="post" action="<isa:webappsURL name="/b2b/createdocument.do"/>">   
  <div>
    <input type="hidden" name="cancel" value="<isa:translate key="b2b.noDocument.cancel"/>" />
    <input type="hidden" name="soldToId" value="" />
  </div>
  <div id="nodoc-default">
     <div id="nodoc-header">
        <div><span></span></div>
     </div>
      <div id="nodoc-content">
        <div id="nodoc-transactions">
            <% if (ui.isAccessible) { %>
                <a href="#end-transaction" title="<isa:translate key="ecm.acc.sec.transact"/>.&nbsp;<isa:translate key="ecm.acc.lnk.jump"/>"></a>
            <% } %>
                <h1><span><isa:translate key="b2b.noDocument.title"/></span></h1>
                <%-- Check if Header button <minibasket> has been pressed --%>
                <p><span><isa:translate key="<%= ui.getInfoKeys()[0] %>"/></span></p>
                <p><span><isa:translate key="<%= ui.getInfoKeys()[1] %>"/></span></p>
                   <% if (ui.minibasket) { %>
                       <input type="hidden" name="requestSource" value="minibasket"/>
                   <% } else { %>
                       <input type="hidden" name="requestSource" value="default"/>
                   <% } %>

				 <% Object detailScenario = request.getParameter(ActionConstants.RA_DETAILSCENARIO) != null ? 
    										request.getParameter(ActionConstants.RA_DETAILSCENARIO) : 
    										request.getAttribute(ActionConstants.RA_DETAILSCENARIO);
			       if(detailScenario != null && !detailScenario.toString().equals("")) { %>
			           <input type="hidden" name="<%=ActionConstants.RA_DETAILSCENARIO%>" value="<%=JspUtil.encodeHtml(detailScenario)%>" >
		    	<% } %>
                      <%-- select a ordering sold to party --%>
                      <% if (ui.hasCreateOrderOBPermission) { %>
                      <p><span></span></p>
                          <% field = ui.getSoldToField(dealerIdName, "newOrder"); %>
                          <%@ include file="/appbase/fieldwithhelpvalues.inc.jsp" %>
                      <p><span></span></p>
                      <% } %>
                   <%-- end sold to selection --%>
                   <% int i = 0; %>
                   <% if (ui.numRowsOrder >= 1 && ui.hasCreateOrderPermission) { %>
                       <isa:iterate id="processTypesIterat"
                            name="<%= ui.PROCESS_TYPES %>"
                            type="com.sap.isa.core.util.table.ResultData">
                            <% String order = "o_".concat(processTypesIterat.getString(1)); %>
                            <% i++; %>
                            <p class="p1"><span><input type="radio" name="orderType" id="orderType_<%= i %>" value="<%= order %>"<% if ( i == 1) { %> checked="checked" <% } %>/>
                            <label for="orderType_<%= i %>"><isa:translate key="b2b.docnav.basket"/><% if (ui.numRowsOrder > 1) { %>(<%= processTypesIterat.getString(2) %>)<% } %>
                            </label></span></p>
                       </isa:iterate>
                   <% } %>
                   <%-- quotation is allowed --%>
                   <% if (ui.quotationAllowed) { %>
                       <% dealerIdName = "quotationDealer"; %>
                       <%-- select a ordering sold to party --%>
                       <% if (ui.hasCreateQuotOBPermission) { %>
					   <p class="p1"><span></span></p>
                           <% field = ui.getSoldToField(dealerIdName, "newOrder"); %>
                           <%@ include file="/appbase/fieldwithhelpvalues.inc.jsp" %>
                       <p class="p1"><span></span></p>
                       <% } %>
                       <%-- end sold to selection --%>
                       <% if (ui.numRowsQuotation >= 1) { %>
                           <% if (ui.numRowsOrder == 1) { %>
                               <p class="p1"><span></span></p>
                           <% } %>
                           <isa:iterate id="processTypesIterat"
                               name="<%= (ui.quotationExtended) ? ui.QUOTATION_PROCESS_TYPES : ui.PROCESS_TYPES %>"
                               type="com.sap.isa.core.util.table.ResultData">
                               <% String quotation = "q_".concat(processTypesIterat.getString(1)); %>
                               <% i++; %>
                               <p class="p1"><span><input type="radio" id="orderType_<%= i %>" name="orderType" value="<%= quotation %>" />
                               <label for="orderType_<%= i %>"><isa:translate key="b2b.docnav.quotation"/>
                               <% if (ui.numRowsQuotation > 1) { %>
                                   (<%= processTypesIterat.getString(2) %>)
                               <% } %>
                               </label></span></p>
                           </isa:iterate>
                       <% } %>
                   <% } %> <%-- End quotation is allowed --%>
                   <% if (ui.templateAllowed && ui.hasCreateTempPermission) { %>
                       <% dealerIdName = "templateDealer"; %>
                       <%-- select a ordering sold to party --%>
                       <% if (ui.hasCreateTempOBPermission) { %>
                       <p class="p1"><span></span></p>
                           <%  field = ui.getSoldToField(dealerIdName, "newOrder"); %>
                           <%@ include file="/appbase/fieldwithhelpvalues.inc.jsp" %>
                       <p class="p1"><span></span></p>
                       <% } %>
                       <%-- end sold to selection --%>
                       <% i++; %>
                       <p class="p1"><span><input type="radio" id="orderType_<%= i %>" name="orderType" value="template" /><label for="orderType_<%= i %>"><isa:translate key="b2b.docnav.ordertemplate"/></label></span></p>
                   <% } %>
            <% if (ui.isAccessible) { %>
                <a name="end-transaction" title="<isa:translate key="ecm.acc.sec.end.transact"/>"></a>
            <% } %>
        </div>
      </div>
  </div> <!-- End nodoc-first -->

    <%-- Buttons --%>
    <div id="buttons">
            <ul class="buttons-1">
                <li><a href="#" onclick="save_document();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cre"/>" <% } %> ><isa:translate key="b2b.noDocument.submit"/></a></li>
            </ul>
            <ul class="buttons-1">
                <li><a href="#" onclick="cancel_document();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cncl"/>" <% } %> ><isa:translate key="b2b.order.display.submit.cancel"/></a></li>
            </ul>
    </div>

  </form>
  </div> <%-- End page --%>
