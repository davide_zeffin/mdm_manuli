<%--
********************************************************************************
    File:         headErrMsg.inc.jsp
    Copyright (c) 2005, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.03.2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/03/11 $
********************************************************************************
--%>
<%@ page import="com.sap.isa.core.taglib.MessageTag" %>
<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.isa.core.util.Message" %>

<%  boolean headerErrMsgExist = false;
    boolean headerWarnMsgExist = false;
    boolean headerInfoMsgExist = false;
    
    // read complete message list and check if any messages are available
    MessageList headerMsgList = MessageTag.getMessageListFromContext(pageContext, MaintainOrderBaseAction.RC_MESSAGES);
    if(headerMsgList != null) {
    	headerErrMsgExist = (headerMsgList.subList(Message.ERROR) == null) ? false : true;
    	headerWarnMsgExist = (headerMsgList.subList(Message.WARNING) == null) ? false : true;
    	headerInfoMsgExist = (headerMsgList.subList(Message.INFO) == null) ? false : true;
    }
    
    // only if some messages should be displayed, we write a DIV area
    if (headerErrMsgExist || 
    		headerWarnMsgExist || 
    		headerInfoMsgExist) {
%>
    <div class="header-msg-area">
       <%-- error messages --%>
       <isa:message id="errortxt" name="<%=MaintainOrderBaseAction.RC_MESSAGES%>" type="<%= Message.ERROR %>" >
           <div class="error"><span><%= errortxt %></span></div>
       </isa:message>
       <% if (ui.hasErroneousPos()) { %>
           	<div class="error">
           	   <span><isa:translate key="b2b.oder.disp.itemerrors"/>
                     <a href="#firsterritem"><isa:translate key="b2b.oder.disp.firsterritem"/></a>
           	   </span>
           	</div>
       <% } %>   
       <%-- warning messages --%>    
       <isa:message id="warntxt" name="<%=MaintainOrderBaseAction.RC_MESSAGES%>" type="<%= Message.WARNING %>" >
           <div class="warn"><span><%= warntxt %></span></div>
       </isa:message>       
       <%-- info messages --%>
       <isa:message id="infotxt" name="<%=MaintainOrderBaseAction.RC_MESSAGES%>" type="<%= Message.INFO %>" >
           <div class="info"><span><%= infotxt %></span></div>
       </isa:message>
    </div>
<%  } %>
