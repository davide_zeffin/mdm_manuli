<%--
********************************************************************************
    File:        batch.jsp
    Copyright (c) 2001, SAP Germany, All rights reserved.

    Author:       SAP
    Created:      19.4.2001
    Version:      1.0

    $Revision: #53 $
    $Date: 2002/07/29 $
********************************************************************************
--%>

<%@ page import="java.util.*" %>
<%@ page import="java.lang.*"%>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.core.businessobject.management.*" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.BatchUI" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="checksession.inc" %>
<%@ include file="usersessiondata.inc" %>

<isa:contentType />
<isacore:browserType/>

<%  BatchUI ui = new BatchUI(pageContext);;

	String inputs="";
	String tableTitle = "";
	String tableTitle1 = "";
	String tableTitle2 = "";

    
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">

<head>
<title></title>
<isa:includes/>
    <!--
    <link href="<%= WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css") %>"
          type="text/css" rel="stylesheet">
          -->  
    <script type="text/javascript">
	  <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>
    <script src="<%= WebUtil.getMimeURL(pageContext, "b2b/jscript/table_highlight.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "auction/jscript/calendar.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/browsersniff.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
	        type="text/javascript">
    </script>

	<%-- JavaScript to toggle all characteristic values grouped together by 1 characteristic --%>
	<script type="text/javascript">
		function toggle(idName) {
		  opera = false;
		  // Opera V6 cannot change style.display, but it can disguise itself
		  // as nearly every browser. With the userAgent property we find out
		  // its true nature.
		  if (navigator.userAgent.indexOf("Opera") != -1){
		    opera = true;
		  }
		  if (document.all && !opera) {
		    // the world of the IEs
		    openIcon = document.getElementsByName(idName + "open")[0];
		    closeIcon = document.getElementsByName(idName + "close")[0];
		    target = document.getElementsByName(idName);

		    for(var i=0; i<target.length; i++){
			if (target[i].style.display == "none") {
					target[i].style.display = "inline";
					openIcon.style.display = "none";
					closeIcon.style.display = "inline";
			}
			    else {
					target[i].style.display = "none";
					openIcon.style.display = "inline";
					closeIcon.style.display = "none";
			    }
		    }
		  }
		  else if (document.getElement && !opera) {
		    // the world of W3C without Opera
		    openIcon = document.getElement(idName + "open");
		    closeIcon = document.getElement(idName + "close");
		    target = document.getElement(idName);

				for(var i=0; i<target.length; i++){
				if (target[i].style.display == "table-row") {
						target[i].style.display = "inline";
						openIcon.style.display = "none";
						closeIcon.style.display = "inline";
				}
				    else {
						target[i].style.display = "none";
						openIcon.style.display = "inline";
						closeIcon.style.display = "none";
				    }
			}
		  }
		  else {
		     // handle NS4.7 and Opera
		    alert("ServerRequest");
		  }
		}
	</script>
	
	<%-- JavaScript to refresh the overview, where the customer gets an aggregated view of his selected/entered values --%>
	<script type="text/javascript">
		function refreshOverview(dataType, digits, target,values, froms, tos, inputType, addValue){
			var result= "";

			var addValueArray = document.getElementsByName(addValue);
			for(var i=0; i<addValueArray.length; i++){
				if(addValueArray[i].value != null && addValueArray[i].value != ""){
					addValue = addValueArray[i].value;
					if(addValue.length > digits){
						alert("<%= WebUtil.translate(pageContext, "b2b.order.batch.input.length", null)%>");
					}
					else{
						result = result + ";" + addValueArray[i].value;
					}
				}
			}

			if(inputType == "1"){
				var valueArray = document.getElementsByName(values);
				for(var i=0; i<valueArray.length; i++){
					if(valueArray[i].checked == true) result = result + ";" + valueArray[i].value;
				}
			}
			else if(inputType == "3"){
				var fromArray = document.getElementsByName(froms);
				var toArray = document.getElementsByName(tos);
				for(var i=0; i<fromArray.length; i++){
					if(((fromArray[i].value != null && fromArray[i].value != "") && (toArray[i].value != null && toArray[i].value != "")) && dataType == "NUM"){
						if((fromArray[i].value*1) >= (toArray[i].value*1)){ 
							alert("<%= WebUtil.translate(pageContext, "b2b.order.batch.interval.chk", null)%>");
							fromArray[i].value = "";
							toArray[i].value = "";
						}
						else{
							if(fromArray[i].value != null && fromArray[i].value != "") result = result + ";" + fromArray[i].value;
							if(toArray[i].value != null && toArray[i].value != "") result = result + "-" + toArray[i].value;
						}
					}
					else if (((fromArray[i].value != null && fromArray[i].value == "") && (toArray[i].value != null && toArray[i].value != "")) && dataType == "NUM") {
							alert("<%= WebUtil.translate(pageContext, "b2b.order.batch.interval.chk", null)%>");						
							fromArray[i].value = "";
							toArray[i].value = "";
					}
					else{
						if(fromArray[i].value != null && fromArray[i].value != "") result = result + ";" + fromArray[i].value;
						if(toArray[i].value != null && toArray[i].value != "") result = result + "-" + toArray[i].value;
					}
				}
			}

			document.forms["batch_positions"].elements[target].value="";
			document.forms["batch_positions"].elements[target].value=result;
			document.all(target).value = document.all(target).value.slice(1,document.all(target).value.length);
			
		}
	</script>

</head>
<body class="workarea">
<div class="module-name"><isa:moduleName name="b2b/batch.jsp" /></div>
<div style="margin-left:10px">

<form action=""
      name="batch_positions"
      method="post"
      target="_parent">
      
      <!-- Header Data -->
    <table border=0px cellspacing=5 cellpadding=0>
    	<tr>
    		<td class="vertical-align-middle">
            	<isa:translate key="b2b.order.display.productno"/>:
            </td>
            <td>
            	<b><%= JspUtil.encodeHtml(ui.getProduct()) %></b>
            </td>
    	</tr>
    </table>

    <!-- Body Data -->
    <div style="margin-top:20px;margin-bottom:5px;margin-left:10px;">
    	<isa:translate key="b2b.order.details.batch.values"/>
    </div>

     <%   // Data table preamble
     if (ui.isAccessible()) {
     	String arg3 = ui.getBatchesSize() > 0 ? "1" : "0";
        String arg1 = Integer.toString(ui.getBatchesSize());
		String arg0 = WebUtil.translate(pageContext,"b2b.order.details.batch.values", null);
     %>
			<a href="#tableend1"
				id="tablebegin1"
				title="<isa:translate key="access.table.begin" 
				arg0="<%=arg0%>" 
				arg1="<%=arg1%>"
				arg2="3" 
				arg3="<%=arg3%>" 
				arg4="<%=arg1%>"/>"> </a>
  <% } %>
    <table class="poslist" border=0px cellspacing="0" cellpadding="3">
        <thead>
    	    <tr>
    	    	<th class="poslist" scope="col">&nbsp;</th>
    		    <th class="poslist" scope="col" id="desccol"><isa:translate key="b2b.order.details.batch.charname"/></th>
        		<th class="poslist" scope="col" id="valuecol"><isa:translate key="b2b.order.dt.batch.charchoosen"/></th>
        	</tr>
        </thead>
        <tbody>
		
		<%-- Define / Initialize different styles for table display --%> 
        <%	JspUtil.Alternator alternator = new JspUtil.Alternator(new String[] {"odd", "even" });
		    String tag_style="";
	        String tag_style_input="";
		    String even_odd="";
		 %>
	    
	    <%-- Start of table loop --%>
		<%	int mainTableRow = 0;
			String currentCharName = "";
			String currentCharTxt = "";
			
			/*	Loop over batch value table. In each row we compare the current characteristic and the previous one.
				If we reached a new characteristic all characteristic header information from the main table is read.
				Also see BatchUIInfoContainer.class for more detailed structure information.*/
					    
    		for(int i=0; i<ui.getNoOfEntries(); i++){
    			if(!currentCharName.equals(ui.getCharNameKey(i))){ //compare current characteristic with previous one
			    	currentCharName = ui.getCharNameKey(i);
			    	currentCharTxt = ui.getCharTxt(mainTableRow);
					even_odd = alternator.next(); // "even" or "odd"
					tag_style= "poslist-mainpos-"+even_odd;
					tag_style_input= "batch-"+even_odd;%>
					<%-- Start characteristic header row: containing detail icon, characteristic description, selected value overview --%>
			    	<tr scope="row">
			    		<td class="<%= JspUtil.encodeHtml(tag_style) %>" valign=top>
	    			        <a href='javascript: toggle("characteristics_<%= mainTableRow %>")' class="iconlink">
	                        	<img id="characteristics_<%= mainTableRow %>close" style="DISPLAY: none"
	                                 src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>"
	                                 alt="<isa:translate key="b2b.order.details.item.close"/>"
	                                 border="0" width="16" height="16" valign=top>
	                            <img id="characteristics_<%= mainTableRow %>open" style="DISPLAY: inline"
	                                 src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>"
	                                 alt="<isa:translate key="b2b.order.details.item.open"/>"
	                                 border="0" width="16" height="16" valign=top>
	                         </a>
	                    </td>
	                    <td class="<%= JspUtil.encodeHtml(tag_style) %>">
	    				<isa:translate key="b2b.order.details.batch.charname"/>: <%= JspUtil.encodeHtml(ui.getBatchDesc(mainTableRow))%>
	    				<%if(!ui.getCharUnit(mainTableRow).equals("")){%>
	    					&nbsp;(<%= JspUtil.encodeHtml(ui.getCharUnit(mainTableRow))%>)
	    				<%}%>
				    	</td>
				    	<td class="<%= tag_style %>" headers="valuecol">
							<input type="text" class="<%= JspUtil.encodeHtml(tag_style_input)%>" name="batchElement[<%= mainTableRow %>]"
						   		id="batchElement[<%= mainTableRow %>]" size="50" readonly="true" value="<%= JspUtil.encodeHtml(ui.getPreselection(mainTableRow))%>"
								<% if(ui.isAccessible()) { 
									String arg1 = WebUtil.translate(pageContext, "access.unavailable", null);%>
								title="<isa:translate key="access.input" arg0="" arg1="<%= arg1 %>"/>"
								<% } %>>
			            </td>
		            </tr>
		            <%-- End characteristic header row --%>
		            <%-- Start first value row for additional values --%> 
    				<%if(ui.getCharAddValSet(mainTableRow).equals("X")){ /* check if additional values are allowed --> free input fields */ %>
			            <tr id="characteristics_<%= mainTableRow %>" style="display:none;">
			            	<td class="<%= JspUtil.encodeHtml(tag_style) %>">&nbsp;
	    					</td>
		    				<td class="<%= JspUtil.encodeHtml(tag_style) %>">&nbsp;
		    				</td>
					    	<td class="<%= JspUtil.encodeHtml(tag_style) %>" align="right">
				            	<input id="addValue_<%= mainTableRow %>" name="add_value[<%= mainTableRow%>]" type="text" value="<%= JspUtil.encodeHtml(ui.getCharAddVal(mainTableRow))%>" onChange='refreshOverview("<%= JspUtil.encodeHtml(ui.getCharDataType(mainTableRow))%>", "<%= JspUtil.encodeHtml(ui.getCharNumberDigit(mainTableRow))%>", "batchElement[<%= mainTableRow %>]","values_<%= mainTableRow %>", "from_<%= mainTableRow %>", "to_<%= mainTableRow %>", "<%= ui.getInputType(i) %>","addValue_<%= mainTableRow %>")'/>
				            	<input type=hidden name="char_name_add_value[<%= mainTableRow %>]" value="<%= JspUtil.encodeHtml(currentCharName) %>"/>
							</td>
			            </tr>
			        <%}%> 
		    	<%-- End first value row for additional values --%>
		        <%    
	    			mainTableRow++;
    			}%>
    			
		    	<%-- Start value rows --%>
    			<tr id="characteristics_<%= mainTableRow-1 %>" style="display:none;">
    				<td class="<%= JspUtil.encodeHtml(tag_style) %>">&nbsp;
    				</td>
    				<td class="<%= JspUtil.encodeHtml(tag_style) %>">&nbsp;
    				</td>
			    	<td class="<%= JspUtil.encodeHtml(tag_style) %>" align="right">
	    			<%switch (ui.getInputType(i)){
    					case '1': /* type '1' for checkboxes */%>
					    	<%= JspUtil.encodeHtml(ui.getBatchValDesc(i))%>
			    			<input id="values_<%= mainTableRow-1 %>" name="values[<%= i %>]" type="checkbox" value="<%= JspUtil.encodeHtml(ui.getCharVal(i))%>" onClick='refreshOverview("<%= JspUtil.encodeHtml(ui.getCharDataType(mainTableRow-1))%>", "<%= JspUtil.encodeHtml(ui.getCharNumberDigit(mainTableRow-1))%>", "batchElement[<%= mainTableRow-1 %>]","values_<%= mainTableRow-1 %>", "", "", "<%= ui.getInputType(i) %>","addValue_<%= mainTableRow-1 %>")' <%= JspUtil.encodeHtml(ui.getSelected(i))%>/>
			    	<%		break;
		    			case '3': /* type '3' for range input fields */%>
    						<isa:translate key="b2b.order.details.batch.from"/>&nbsp;
    						<%if(ui.getCharValRangeMin(i) != null && !ui.getCharValRangeMin(i).equals("")){ /*if minimum values were maintained*/%>
    							(<%= JspUtil.encodeHtml(ui.getCharValRangeMin(i)) %>)&nbsp;
    						<%}%>
    						<input id="from_<%= mainTableRow-1 %>" name="values[<%= i %>]" type="text" class="textInput" value="<%= JspUtil.encodeHtml(ui.getCharVal(i))%>" onChange='refreshOverview("<%= JspUtil.encodeHtml(ui.getCharDataType(mainTableRow-1))%>", "<%= JspUtil.encodeHtml(ui.getCharNumberDigit(mainTableRow-1))%>", "batchElement[<%= mainTableRow-1 %>]", "", "from_<%= mainTableRow-1 %>", "to_<%= mainTableRow-1 %>", "<%= ui.getInputType(i) %>","addValue_<%= mainTableRow-1 %>")'/> 
							<isa:translate key="b2b.order.details.batch.to"/>&nbsp;
							<input id="to_<%= mainTableRow-1 %>" name="values_to[<%= i %>]" type="text" class="textInput" value="<%= JspUtil.encodeHtml(ui.getCharValMax(i))%>" onChange='refreshOverview("<%= JspUtil.encodeHtml(ui.getCharDataType(mainTableRow-1))%>", "<%= JspUtil.encodeHtml(ui.getCharNumberDigit(mainTableRow-1))%>", "batchElement[<%= mainTableRow-1 %>]", "", "from_<%= mainTableRow-1 %>", "to_<%= mainTableRow-1 %>", "<%= ui.getInputType(i) %>","addValue_<%= mainTableRow-1 %>")'/>
					    	<%if(ui.getCharValRangeMax(i) != null && !ui.getCharValRangeMax(i).equals("")){ /*if maximum values were maintained*/%>
    							(<%= JspUtil.encodeHtml(ui.getCharValRangeMax(i)) %>)&nbsp;
    						<%}%>
				    <%		break;
			    		default:%>
	    			<%}%>
	    				<%-- add characteritics header information as hidden fields --%>
	    				<input type=hidden name="char_name[<%= i %>]" value="<%= JspUtil.encodeHtml(currentCharName) %>"/>
		    			<input type=hidden name="char_txt[<%= i %>]" value="<%= JspUtil.encodeHtml(currentCharTxt) %>"/>
		    			<input type=hidden name="values_txt[<%= i %>]" value="<%= JspUtil.encodeHtml(ui.getCharValTxt(i)) %>"/>
			    	</td>
		    	</tr>
		    	<%-- End value rows --%>		    
		    <%}%>
		    <%-- End of table loop --%>
        </tbody>
    </table>
    
    <% if (ui.isAccessible()) { /* Data Table Postamble */ %>
			  <a id="tableend1" href="#tablebegin1" title="<isa:translate key="access.table.end" arg0="<%=tableTitle%>"/>"</a>
	<% } /* Data Table Postamble ends */ %>
    
    <%-- fields for item identification --%>
    <input type="hidden" name="itemGuid" value="<%= JspUtil.encodeHtml(ui.getItemGuid()) %>">
    
    <%-- fields needed for navigation --%>
    <input type="hidden" name="cancel" value="">
	<input type="hidden" name="setbatch" value="">
	<input type="hidden" name="orderchange" Value="<%= JspUtil.encodeHtml(ui.isOrderChange()) %>">
</form>
</div>
</body>
</html>
