<%--
********************************************************************************
    File:         campaignEnrollmentSingle.inc.jsp
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      04.12.2006
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/12/04 $
********************************************************************************
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib prefix="isacore" uri="/isacore" %>

<%@ page import="com.sap.isa.core.util.MessageList" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.util.JspUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.CampaignDetailUI" %>
<%@ page import="com.sap.ecommerce.businessobject.campaign.CampaignEnrollment" %>

<div class="module-name"><isa:moduleName name="b2b/campaignEnrollmentSingle.inc.jsp" /></div>

<% CampaignEnrollment campEnrollm = ui.getEnrollment();
%>

<% if (campEnrollm != null) { %>
<%-- Enrollment data for one single partner --%>
<div class="document-header">
  <%-- Enrollee TechKey --%>
  <div style="display: none"> <%-- make hidden fields invisible because of IE bug --%>
    <input type="hidden" name="enrollm[0].enrolleeGUID" value="<%= campEnrollm.getEnrolleeGuid() %>">
  </div>

  <table class="layout" DataTable="0" width="100%">
	<tbody>
	  <tr>
	    <td class="col1"  width="22%">
          <%-- Business Partner Name --%>
          <div class="identifier">
             <p><span><isa:translate key="ecom.camp.enrolleeName"/><isa:translate key="ecom.camp.label.colon"/></span></p>
          </div>
        </td>
	    <td class="col2" width="22%">
          <div class="value">
             <p><span><%= campEnrollm.getEnrolleeDescription() %></span></p>
          </div>    
        </td>
        <td class="col3" width="22%">&nbsp;</td>
        <td class="col4" width="22%">&nbsp;</td>
        <td class="col5" width="12%">&nbsp;</td>
      </tr>
	  <tr>
	    <td class="col1">
          <%-- Business Partner Id --%>
          <div class="identifier">
             <p><span><isa:translate key="ecom.camp.enrolleeId"/><isa:translate key="ecom.camp.label.colon"/></span></p>
          </div>    
        </td>
	    <td class="col2">
          <div class="value">
             <p><span><%= campEnrollm.getEnrolleeID() %></span></p>
          </div>    
        </td>
        <td class="col3">&nbsp;</td>
        <td class="col4">&nbsp;</td>
        <td class="col5">&nbsp;</td>
      </tr>
	  <tr>
	    <td class="col1">
          <%-- First Enrolled By / Date --%>
          <div class="identifier">
             <p><span><isa:translate key="ecom.camp.firstEnrolledByDate"/><isa:translate key="ecom.camp.label.colon"/></span></p>
          </div>    
        </td>
	    <td class="col2">
          <%-- First Enrolled By Contact --%>
          <div class="value">
             <p><span><%= campEnrollm.getFirstEnrolledByContact() %></span></p>
          </div>    
        </td>
 	    <td class="col3">
          <%-- First Enrolled By SoldTo --%>
          <div class="value">
             <p><span><%= campEnrollm.getFirstEnrolledBySoldTo() %></span></p>
          </div>    
        </td>
	    <td class="col4">
          <%-- First Enrolled Date --%>
          <div class="value">
             <p><span><%= campEnrollm.getFirstEnrolledDate() %></span></p>
          </div>    
        </td>
        <td class="col5">&nbsp;</td>
      </tr>
	  <tr>
	    <td class="col1">
          <%-- Last Changed By / Date --%>
          <div class="identifier">
             <p><span><isa:translate key="ecom.camp.lastChangedByDate"/><isa:translate key="ecom.camp.label.colon"/></span></p>
          </div>    
        </td>
     <% if (campEnrollm.isChangedAfterFirstEnrollment()) { %>
		    <td class="col2">
	          <%-- Last Changed By Contact--%>
	          <div class="value">
	             <p><span><%= campEnrollm.getLastChangedByContact() %></span></p>
	          </div>    
	        </td>
		    <td class="col3">
	          <%-- Last Changed By SoldTo--%>
	          <div class="value">
	             <p><span><%= campEnrollm.getLastChangedBySoldTo() %></span></p>
	          </div>    
	        </td>
		    <td class="col4">
	          <%-- Last Changed Date --%>
	          <div class="value">
	             <p><span><%= campEnrollm.getLastChangedDate() %></span></p>
	          </div>    
	        </td>
	        <td class="col5">&nbsp;</td>
     <% } 
        else { %>
	        <td colspan=4>&nbsp;</td>
     <% } %>
      </tr>
	  <tr>
	    <td class="col1">
          <%-- Enrollment Status --%>
          <div class="identifier">
             <p><span><isa:translate key="ecom.camp.enrollmStatus"/><isa:translate key="ecom.camp.label.colon"/></span></p>
          </div>    
        </td>
	    <td class="col2">
          <div class="value">
             <% if (campEnrollm.isEnrolled()) { %>
                  <p><span><isa:translate key="ecom.camp.enrollmStatus.enrolled"/></span></p>
             <% } 
                else { %>
                  <p><span><isa:translate key="ecom.camp.enrollmStatus.notEnrolled"/></span></p>
             <% } %>
          </div>    
        </td>
        <td class="col3">&nbsp;</td>
        <td class="col4">&nbsp;</td>
        <td class="col5">&nbsp;</td>
      </tr>
	</tbody>
  </table>
</div>


<% } %>