<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI ui = new BaseUI(pageContext); %>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<isa:contentType/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="b2b.fs.title"/></title>
    </head>
    <frameset rows="40,*" border="0" frameborder="0" framespacing="0">
        <frame name="settings_header" src="<isa:webappsURL name="/b2b/user_settings_header.jsp"/>" frameborder="0"  scrolling="no">
        <frame name="settings_main" src="<isa:webappsURL name="/b2b/showprofile.do"/>" frameborder="0" scrolling="auto">
        <noframes>
            <body>&nbsp;</body>
        </noframes>
    </frameset>
</html>
