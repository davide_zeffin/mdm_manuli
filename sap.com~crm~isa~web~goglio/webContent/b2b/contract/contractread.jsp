<%--
********************************************************************************
    Copyright (c) 2001, SAP Germany, All rights reserved.
    Author:       SAP
    Created:      23.4.2001

    $Revision: #1 $
    $Date: 2001/04/23 $
********************************************************************************
--%>
<%@ page import="com.sap.isa.core.ContextConst" %>
<%@ page import="com.sap.isa.businessobject.contract.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.spc.remote.client.object.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.ContractReadAction" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.contract.ContractUI" %>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>

<%@ page import="com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference" %>
<%@ page import="com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference" %>

<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="/b2b/checksession.inc" %>

 <isa:contentType />  
 <isacore:browserType/>
 
<% 
     ContractUI ui = new ContractUI(pageContext);
%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">

<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>SAP Internet Sales B2B - Contract List</title>

    <isa:stylesheets/> 
    
    <script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
    </script>		
   <script type="text/javascript" >
    function toggle(idName) {
		isaEasyToggle(idName);
	}
    function setPageCommand(command) {
    	getElement("<%=ContractReadAction.CONTRACT_PAGE_COMMAND%>").value = command;
        document.forms["contractitemsform"].submit();
    }
    function setPageSize(pageSize) {
    	getElement("<%=ContractReadAction.CONTRACT_PAGE_SIZE%>").value = pageSize;
    	document.forms["contractitemsform"].submit();
    }
    function expandItem(itemKey) {
    	document.forms["contractitemsform"].<%=ContractReadAction.CONTRACT_EXPAND_ITEM%>.value = itemKey;
        document.forms["contractitemsform"].submit();
    }
    function collapseItem(itemKey) {
    	document.forms["contractitemsform"].<%=ContractReadAction.CONTRACT_COLLAPSE_ITEM%>.value = itemKey;
        document.forms["contractitemsform"].submit();
    }
    function releaseItem(itemKey) {
        document.forms["contractitemsform"].<%=ContractReadAction.CONTRACT_RELEASE_ITEM%>.value = itemKey;
        document.forms["contractitemsform"].elements["requested["+itemKey+"]"].checked = true;          
        document.forms["contractitemsform"].<%=ContractReadAction.CONTRACT_RELEASE_COMMAND%>.value = "<%=ContractReadAction.CONTRACT_RELEASE%>";
        document.forms["contractitemsform"].target = "form_input";
        document.forms["contractitemsform"].submit();
    }
    function  configItem (itemKey, configFilter) {
        form_input().location.href = "<isa:webappsURL name='/b2b/contractinitconfiguration.do'/>?<%=ContractConfigurationAction.CONTRACT_CONFIGURE_ITEM_KEY%>=" + itemKey + "&<%=ContractConfigurationAction.CONTRACT_CONFIG_FILTER_ITEM%>=" + configFilter;
        return true;
    }
    function releaseContract() {
            if (checkMarked()) {
               document.forms["contractitemsform"].<%=ContractReadAction.CONTRACT_RELEASE_COMMAND%>.value = "<%=ContractReadAction.CONTRACT_RELEASE%>";
               document.forms["contractitemsform"].target = "form_input";
               document.forms["contractitemsform"].submit();
            }
    }
    function closeContract() {
    		form_input().location.href = '<isa:webappsURL name="b2b/contractclose.do"></isa:webappsURL>';
    }
    
    function itemCheckBoxChanged(itemKey) {
        var transBut = document.getElementById("LIreleasebutton");
        if (transBut == null) 
            return;
        checkbox = document.forms["contractitemsform"].elements["requested["+itemKey+"]"];
        transBut.className = "";
        if ( !checkMarked()) {
            transBut.className = "disabled";
        }
    }
   
    function checkMarked() {
        var inputs = getElementsByType('contractitemsform','checkbox');
        checkedItem = false;
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i] != null) {
                if (inputs[i].type == 'checkbox') {
                    if (inputs[i].checked) {
                        checkedItem = true;
                        return checkedItem;
                    }
                }
            }
        }    
        return checkedItem;
    }
  	   
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/getElementsByType.js") %>"
          type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
          type="text/javascript">
    </script>    
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
          type="text/javascript">
    </script>	
    <script src='<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/table_highlight.js") %>'
            type="text/javascript">
    </script>
    <script src='<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>'
            type="text/javascript">
    </script>
	<% UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
     BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
     if(bom.getShop().isPricingCondsAvailable() == true) {%>
	   <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
			 type = "text/javascript">
	   </script>
  <% } %>
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
          type="text/javascript"> 
    </script>
 
    <% boolean transferContract = true; %>   
</head>


<body class="order" >
  <%-- Title --%>
    <h1><span>
        <isa:translate key="b2b.contract.itemdetail.headerId"/>: <%= JspUtil.encodeHtml(ui.header.getId()) %>
    </span></h1>             
  <div id="document">
      <form action="<isa:webappsURL name="/b2b/contractread.do"/>"
          name="contractitemsform"
          id="contractitemsform"
          method="post">
 
   <% ContractItemList contractItemList =
            (ContractItemList)request.getAttribute(ContractReadAction.CONTRACT_ITEM_LIST); %>	  
    <input type="hidden"
           name="<%=ContractSelectAction.CONTRACT_SELECTED%>"
           id="<%=ContractSelectAction.CONTRACT_SELECTED%>"
           value="<%=  JspUtil.encodeHtml(contractItemList.getContractHeader().getKeyAsString()) %>">
    <input type="hidden"
           name="<%=ContractReadAction.CONTRACT_CURRENT_PAGE_NO%>"
           id="<%=ContractReadAction.CONTRACT_CURRENT_PAGE_NO%>"
           value="<%=contractItemList.getPageNumber()%>">
    <input type="hidden"
           name="<%=ContractReadAction.CONTRACT_PAGE_SIZE%>"
           id="<%=ContractReadAction.CONTRACT_PAGE_SIZE%>"
           value="<%=contractItemList.getPageSize()%>">
    <input type="hidden"
           name="<%=ContractReadAction.CONTRACT_LAST_PAGE_NO%>"
           id="<%=ContractReadAction.CONTRACT_LAST_PAGE_NO%>"
           value="<%=contractItemList.getLastPageNumber()%>">
    <input type="hidden"
           name="<%=ContractReadAction.CONTRACT_PAGE_COMMAND%>"
           id="<%=ContractReadAction.CONTRACT_PAGE_COMMAND%>">
    <input type="hidden"
           name="<%=ContractReadAction.CONTRACT_EXPAND_ITEM%>"
           id="<%=ContractReadAction.CONTRACT_EXPAND_ITEM%>">
    <input type="hidden"
           name="<%=ContractReadAction.CONTRACT_COLLAPSE_ITEM%>"
           id="<%=ContractReadAction.CONTRACT_COLLAPSE_ITEM%>">
    <input type="hidden"
           name="<%=ContractReadAction.CONTRACT_RELEASE_ITEM%>"
           id="<%=ContractReadAction.CONTRACT_RELEASE_ITEM%>">
    <input type="hidden"
           name="<%=ContractReadAction.CONTRACT_RELEASE_COMMAND%>"	
           id="<%=ContractReadAction.CONTRACT_RELEASE_COMMAND%>">  
    <div class="module-name"><isa:moduleName name="b2b/contract/contractread.jsp" /></div>
   
    <div class="document-header">
       <%-- Accesskey for the header section. --%>
       <a id="access-header" href="#access-items" title="<isa:translate key="b2b.acc.header.title"/>" accesskey="<isa:translate key="b2b.acc.header.key"/>"></a>
 
     <div class="header-basic">
          <%-- the header information for the basket --%>
          <% if (ui.isAccessible) { %>
	      <a href="#end-table1" title="<isa:translate key="b2b.acc.header.dat.link"/>"></a> 
          <% } %>
          <table class="data" summary="<isa:translate key="b2b.acc.header.dat.sum"/>">		    
            <tbody>                                         
                <% if ( ui.header.getExternalRefNo().length() != 0) { %>
                     <tr>
                            <td class="title">
                                <isa:translate key="b2b.contract.headerExtRefNo"/>
                            </td>
                            <td class ="value">
                                <%= JspUtil.encodeHtml(ui.header.getExternalRefNo()) %>
                            </td>
                        </tr>
                <% } %>
                <% if (ui.header.getDescription().length() != 0) { %>
                        <tr>
                            <td class="title">
                                <isa:translate key="b2b.contract.headerDescription"/>
                            </td>
                            <td class ="value">
                                <%= JspUtil.encodeHtml(ui.header.getDescription()) %>
                            </td>
                        </tr>
                <% } %>
                <% if (ui.header.getValidFromDate().length() != 0 ||
                       ui.header.getValidToDate().length() != 0) { %>
                     <tr>
                         <td class="identifier">
                             <isa:translate key="b2b.contract.headerValidity" />
                         </td>
                         <td class="value">
                             <%= JspUtil.encodeHtml(ui.header.getValidFromDate()) %>&nbsp;-&nbsp;<%= JspUtil.encodeHtml(ui.header.getValidToDate())%>
                         </td>
                     </tr>    
                <% } %>              
             
                <%-- cell containing a 1st table with buttons for paging --%>
                <% int pageNumber = ui.contractItemList.getPageNumber(); %>
                <% int lastPage = ui.contractItemList.getLastPageNumber(); %>
                <% if (ui.contractItemList.size()==0) { %>
                   <div class="error"><span><isa:translate key="b2b.contract.message.noPos"/></span></div>
		<% } %>
             <%--   <% if (lastPage > 1) { %>   --%>
                    <tr>
                        <td class="identifier">
                                  <isa:translate key="b2b.contract.pages"
                                       arg0='<%=Integer.toString(pageNumber)%>'
                                       arg1='<%=Integer.toString(lastPage)%>'/>
                        </td>
                        <% if (pageNumber > 1) { %>
                              <td>
                                  <a href="#" class="icon" onclick="setPageCommand('<%=ContractReadAction.CONTRACT_FIRST_PAGE%>')"  >
                                             <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/seitennav_links.gif") %>" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.cnt.page.first"/>" <% } %> />
                                  </a>
                              </td>
                        <% } %>                                        
                        <% if (pageNumber > 1) { %> 
                              <td>
                                  <a href="#" class="icon" onclick="setPageCommand('<%=ContractReadAction.CONTRACT_PREV_PAGE%>')" > 
                                            <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/seitennav_links_klein.gif") %>" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.cnt.page.prev"/>" <% } %>/>
                                  </a>
                              </td>
                        <% } %>
                        <% if (pageNumber < lastPage) { %>
                              <td>
                                   <a href="#" class="icon" onclick="setPageCommand('<%=ContractReadAction.CONTRACT_NEXT_PAGE%>')"  >
                                        <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/seitennav_rechts_klein.gif") %>" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.cnt.page.next"/>" <% } %> />
                                   </a>
                              </td>
                        <% } %>
                        <% if (pageNumber < lastPage) { %>
                              <td>
                                   <a href="#" class="icon" onclick="setPageCommand('<%=ContractReadAction.CONTRACT_LAST_PAGE%>')" > 
                                             <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/seitennav_rechts.gif") %>" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.cnt.page.last"/>" <% } %> />
                                   </a>
                              </td>
                        <% } %>
                    </tr>
             <%--   <% } %>  --%> <%-- paging buttons --%>   	     
            </tbody>
          </table> <%-- header --%>
	  <% if (ui.isAccessible) { %>
	        <a name="end-table1" title="<isa:translate key="b2b.acc.header.dat.end"/>"></a>
          <% } %>
    </div> <%-- header-basic --%>
    </div> <%-- document-header --%>
 
  <%-- the item information for the contract --%>
    <div class="document-items"> 
       <a id="access-items" href="#access-buttons" title= "<isa:translate key="b2b.acc.items.title"/>" accesskey="<isa:translate key="b2b.acc.items.key"/>"></a>
         <% if (ui.isAccessible) { %>
		    <a href="#end-table10" title="<isa:translate key="b2b.acc.items.link"/>"></a>
	 <% } %>
          <table class="itemlist" summary="<isa:translate key="b2b.acc.items.sum"/>">
             <% if (ui.contractItemList.size()!=0) { %>
                   <tr>
                       <th class="opener" rowspan="2">&nbsp;
                           <% int numberOfRows = ui.contractItemList.size(); 
                              int startIndex   = 1; %>                    
                              <a class="icon" href='javascript: toggleAllItemDetails("rowdetail_all","rowdetail_", <%= numberOfRows%>, <%= startIndex %>)' >
    						           <img class="img-1" id="rowdetail_allclose" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.close"/> <% } %>">
	    					           <img class="img-1" id="rowdetail_allopen" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.open"/> <% } %>">
                              </a>
                       </th>                            
                       <th class="item" rowspan="2"> <isa:translate key="b2b.contract.item.id"/></th>
                       <th class="qty" colspan="<%=transferContract?3:1%>"><isa:translate key="b2b.contract.item.transfer"/></th>
                       <th class="product" colspan="2"><isa:translate key="b2b.contract.item.product"/></th>
                       <th class="price" rowspan="2" ><isa:translate key="b2b.contract.item.price"/></th>
                       <th class="opener" rowspan="2" style="border-right: none;"><isa:translate key="b2b.contract.item.more.products"/></th>
                   </tr>
                   <tr>
                      <% if (transferContract){ %>
                            <th>&nbsp;</th>
                      <% } %>
                      <th class="qty" ><isa:translate key="b2b.contract.item.quantity"/></th>
                      <% if (transferContract){ %>
                           <th>&nbsp;</th>
                      <% } %>
                      <th class="product"><isa:translate key="b2b.contract.item.productId"/></th>
                      <th class="desc"><isa:translate key="b2b.contract.item.prodDescript"/></th>
                   </tr>
			       <% String tag_style_detail =""; 
			          IPCItemReference ipcItemRef = null; %>
                   <isa:iterate id="contractItem"
                                name="<%=ContractReadAction.CONTRACT_ITEM_LIST%>"
                                type="com.sap.isa.businessobject.contract.ContractItem"
                                ignoreNull="true">
                      <%-- set the item in the ui class --%>
                      <%  ui.setItem(contractItem);
					  /* ********** Check if the item is a BOM sub item to be supressed ****** */  
					  if (!ui.isBOMSubItemToBeSuppressed()) { 
                          tag_style_detail = "poslist-detail-" + ui.even_odd;   %>               
       					  <% String substMsgColspan = "6"; %>
       					  <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>			    
                          <tr class="<%= ui.even_odd %>" id="row_<%= ui.line %>">
     			          <% if (contractItem.isNewMainItem()) { %>
                                <td class="opener">
                                     <% if (ui.showItemDetailButton()) { %>
                                           <a class="icon" href="#" onclick='toggle("rowdetail_<%= ui.line %>")' >
		            			              <img class="img-1" id="rowdetail_<%= ui.line %>close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.acc" arg0="<%= contractItem.getId()%>"/> <% } else { %><isa:translate key="b2b.order.details.item.close"/> <% } %>"/>
					                          <img class="img-1" id="rowdetail_<%= ui.line %>open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.acc" arg0="<%= contractItem.getId()%>"/> <% } else { %><isa:translate key="b2b.order.details.item.open"/> <% } %>"/>
						                   </a>
                                       <% } %>
                                </td>
                                <td>
                                    <%= JspUtil.encodeHtml(contractItem.getId()) %>
                                </td>
				      <% } else { %>
				         <td>&nbsp;</td>
				         <td>&nbsp;</td>
				      <% } %>
                      <% if (contractItem.isAllProduct()) { %> <%--  all products are allowed --%>
                            <td colspan="7" align="left">
                                <isa:translate key="b2b.contract.message.allProducts"/> 
                            </td>
                      <% } else { %>
	                        <% if (transferContract ) { %>
                                 <% if (!contractItem.isStatusCompleted()) { %> 
                                      <td class="qty">
                                         <input type="checkbox"
                                             onclick="itemCheckBoxChanged('<%=contractItem.getKeyAsString()%>')"
                                             name="requested[<%=contractItem.getKeyAsString()%>]"
                                             id="requested[<%=contractItem.getKeyAsString()%>]"
                                             <%=contractItem.isRequested() ? "checked" : ""%> />                                      
                                      </td>
                                      <td class="qty">
	                                       <input type="text"
	                                              class="textinput-small"
	                                              name="quantity[<%=contractItem.getKeyAsString()%>]"
	                                              value="<%=JspUtil.encodeHtml(contractItem.getQuantity())%>"
	                                              size="8"
	                                              maxlength="8"/>&nbsp;<%=JspUtil.encodeHtml(contractItem.getUnit())%> 
	                                       <input type="hidden"
	                                                    name="unit[<%=contractItem.getKeyAsString()%>]"
	                                                    value="<%=JspUtil.encodeHtml(contractItem.getUnit())%>"/>   <%-- ????? --%>
                                      </td>
                                      <td>
		                                  <a href="#" onclick="releaseItem('<%=contractItem.getKeyAsString()%>')">
		                                           <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_wk_green.gif") %>" class="img-1" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.cnt.transfer"/>" <% } %> />
		                                      </a>
		                              </td>                                                                            	                                             
	                              <% } else { %>
	                                   <td>&nbsp;</td>	                                       
                                       <td class="qty">
                                            <%=JspUtil.encodeHtml(contractItem.getQuantity())%>&nbsp;<%=JspUtil.encodeHtml(contractItem.getUnit())%>
                                       </td>
 	                                   <td>&nbsp;</td>	                                       
                                  <% } %>                                        
                                  
                            <% } else { %>
                                     <td class="qty"><%=contractItem.getQuantity()%>&nbsp;<%=JspUtil.encodeHtml(contractItem.getUnit())%></td>
                            <% } %>
                            <td class="product"><%= JspUtil.encodeHtml(contractItem.getProductId()) %></td>
                            <td class="desc">
                                 <%= JspUtil.encodeHtml(contractItem.getProductDescription()) %>
                                 <% if (ui.configurable) { %>
                                        <br><a href="#" onclick="configItem('<%= ui.getItemKeyAsString(contractItem)%>','<%= contractItem.getConfigFilter() %>')"><isa:translate key="b2b.contract.item.config.view"/></a>                                    
                                 <% } %>
                            </td>  
					        <td class="price">
					            <%= JspUtil.encodeHtml(contractItem.getPrice()+ ' ' + contractItem.getCurrency() + ' ' + " / "  + ' ' + contractItem.getPriceUnit() + ' ' + contractItem.getPriceQuantUnit()) %>
					        </td>					      
 					        <%-- further products open/close--%>
                            <td class="opener">
                               <% if (contractItem.isExpanded()) { %>
                                     <a class="icon" href="#" onclick="collapseItem('<%=contractItem.getContractItemTechKey().getItemKey().getIdAsString()%>')">
                                        <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.acc.cnt.item.collaps" arg0="<%= contractItem.getId()%>" /> <% } else { %><isa:translate key="b2b.cnt.item.collaps" /> <% } %>">
                                     </a>
                               <% } else if (contractItem.isCollapsed()) { %>
                                        <a class="icon" href="javascript:expandItem('<%=contractItem.getContractItemTechKey().getItemKey().getIdAsString()%>')">
                                           <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.acc.cnt.item.expand" arg0="<%= contractItem.getId()%>" /><% } else { %> <isa:translate key="b2b.cnt.item.expand" /> <% } %>">
                                        </a>
                               <% } %>
                            </td>
                      <% } %> <%-- if all products --%>
				  
                      <%-- Item Details --%>
                      <% tag_style_detail = ui.even_odd + "-detail"; %>
                          <tr id="rowdetail_<%= ui.line %>" class="<%= tag_style_detail %>" <% if (ui.isToggleSupported()) { %> style="display:none;" <% } %> >
                              <td class="select">&nbsp;</td>
                              <td class="detail" colspan="<%=transferContract?9:7%>">
                                  <table class="item-detail"> 
                                      <% ContractAttributeList attributeList = (ContractAttributeList)pageContext.findAttribute(ContractSelectAction.CONTRACT_ATTRIBUTE_LIST); %>
					                  <% ContractAttributeValueMap attributeValueMap = (ContractAttributeValueMap)pageContext.findAttribute(ContractReadAction.CONTRACT_ATTRIBUTE_VALUE_MAP); %>
                                      <% ContractAttributeValueList attributeValueList = (ContractAttributeValueList)attributeValueMap.get(contractItem.getContractItemTechKey().getItemKey()); %>                                 
                                      <% if (attributeList == null || attributeList.isEmpty()) { %>
                                             <isa:translate key="b2b.contract.message.noDetails"/>
                                      <% } else { %>
                                           <% int size = attributeList.size(); 
					                        for (int i = 0; i < size; i++) { %>                                   
                                               <tr>
                                                  <td class="identifier">
                                                      <%=JspUtil.encodeHtml(attributeList.get(i).getDescription())%>:
                                                  </td>                                              
                                                  <td class="value">
                                                      <%=JspUtil.encodeHtml(attributeValueList.get(i).getValue())%> <%=JspUtil.encodeHtml(attributeValueList.get(i).getUnitValue())%>
                                                  </td>
                                               </tr>
                                              <% } %>
                                      <% } %>                                  
                                      <tr>
                                          <td colspan="9" align="right"></td>
                                      </tr>
                                  </table>
				              </td>
			              </tr>
    		           </tr>
                   <% }  // ui.isBOMSubItemToBeSuppressed() %>                                				    
			    </isa:iterate>				    
                    <% } %>
        </table>
             
        <% if (ui.isAccessible) { %>
		  <a name="end-table10" title="<isa:translate key="b2b.acc.items.end"/>"></a>
	<% } %>
        
       </div>
       </form>  
     </div>
 
    <%-- Buttons --%>
    <div id="buttons">
        <a id="access-buttons" href="#access-header" title="<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a> 
        <ul class="buttons-1">
            <li>
                <select name="pageselect" id="pageselectId" size="1" title="<isa:translate key="b2b.acc.buttons.entriesno"/>" >
                                        <option value="5"  <% if (ui.contractItemList.getPageSize() == 5)  { %> selected <% } %> >5</option>
                                        <option value="10" <% if (ui.contractItemList.getPageSize() == 10) { %> selected <% } %> >10</option>
                                        <option value="25" <% if (ui.contractItemList.getPageSize() == 25) { %> selected <% } %> >25</option>
                                        <option value="50" <% if (ui.contractItemList.getPageSize() == 50) { %> selected <% } %> >50</option>
                 </select>            
            </li>
            <li><a href="#" onclick="setPageSize(document.getElementById('pageselectId').value);" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.cnt.setpagesize"/>" <% } %> ><isa:translate key="b2b.contract.entriesPerPage"/></a></li>          
        </ul>  
        <ul class="buttons-2">
            <li id="LIreleasebutton" class="disabled" ><a href="#" id="releasebutton" name="releasebutton" onclick="releaseContract();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cnt.trans"/>" <% } %> ><isa:translate key="b2b.contract.addItems"/></a></li>
        </ul>  
        <ul class="buttons-3">
            <li><a href="#" onclick="closeContract();" <% if (ui.isAccessible) { %> title="<isa:translate key="b2b.acc.buttons.cnt.close"/>" <% } %> ><isa:translate key="b2b.contract.close"/></a></li>
        </ul>
    </div> 
 

</body>
</html>