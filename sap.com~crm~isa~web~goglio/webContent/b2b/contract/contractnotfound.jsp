
<%/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      17 May 2001

    $Revision: #1 $
    $Date: 2001/05/17 $
*****************************************************************************/%>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>


<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">

  <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

    <title>SAP Internet Sales B2B - Contract not found</title>
    <!-- <link href='<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css") %>'
          type="text/css"
          rel="stylesheet"> -->
  </head>
  <body class="workarea">
    <div id="new-doc" class="module">
      <div class="module-name"><isa:moduleName name="b2b/contract/contractnotfound.jsp" /></div>
      <font color=red size="3"><b><isa:translate key="error.jsp.head"/></b></font>
      <br>
      <p><b>
      <% if ((userSessionData.getAttribute(ShopReadAction.CONTRACT_NOT_ALLOWED) != null) &&
            ((Boolean)userSessionData.getAttribute(ShopReadAction.CONTRACT_NOT_ALLOWED)).booleanValue()) { %>
          <isa:translate key="b2b.contract.message.notallowed"/>
      <% } else { %>
          <isa:translate key="b2b.contract.message.chooseCnt"/>
      <% } %>
      </b>
    </div>
    <p>
    <form action='<isa:webappsURL name="/b2b/updatedocumentview.do"></isa:webappsURL>'
        target="form_input">
        <input type="submit" value="<isa:translate key="error.jsp.back"/>">
    </form>
  </body>
</html>
