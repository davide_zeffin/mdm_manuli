<%/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      28 March 2001

    $Revision: #1 $
    $Date: 2001/03/28 $
*****************************************************************************/%>
<%@ taglib uri="/isa" prefix="isa" %>

<%@ page import="com.sap.isa.businessobject.contract.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.DocumentListSelectorForm" %>
<%@ page import="com.sap.isa.isacore.actionform.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.action.negotiatedcontract.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ include file="/b2b/checksession.inc" %>
<%@ include file="/b2b/usersessiondata.inc" %>

<% boolean contractNegotiationAllowed = false; %>

<% if ((userSessionData.getAttribute(ShopReadAction.CONTRACT_NEGOTIATION_NOT_ALLOWED) == null) ||
      !((Boolean)userSessionData.getAttribute(ShopReadAction.CONTRACT_NEGOTIATION_NOT_ALLOWED)).booleanValue()) {
         contractNegotiationAllowed = true;
    } %>

<% ContractSearchForm form =
    (ContractSearchForm)pageContext.findAttribute("contractSearchForm"); %>
<% boolean calledFromPortal =
    (((IsaCoreInitAction.StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).getPortal().equals("YES") ? true : false);%>

<% String[] evenOdd = new String[] { "even", "odd" }; %>
<% boolean isBillingDocEnabled = true; %>
<isacore:ifShopProperty property="isBillingDocsEnabled" value ="false">
  <% isBillingDocEnabled = false; %>
</isacore:ifShopProperty>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<isa:contentType />
<html>
<head>
<title>SAP Internet Sales - Document Search</title>
<isa:includes/>
<!-- <link href='<%=WebUtil.getMimeURL(pageContext, "mimes/shared/style/stylesheet.css") %>'
type="text/css"
rel="stylesheet"> -->

	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>	

  <script src='<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js" ) %>' type="text/javascript">
  </script>


<script type="text/javascript">

  var IE4 = IE5 = NS4 = NS6 = false;
  var IE4 = (document.all && !document.getElementById) ? true : false;
  var IE5 = (document.all && document.getElementById) ? true : false;
  var NS4 = (document.layers) ? true : false;
  var NS6 = (document.getElementById && !document.all) ? true : false;

  function setPeriod() {
    document.selectform.period.disabled = false;
    if(!NS4){document.selectform.period.style.backgroundColor = "white";}
    document.selectform.month.disabled = true;
    if(!NS4){document.selectform.month.style.backgroundColor = "silver";}
    document.selectform.year.disabled = true;
    if(!NS4){document.selectform.year.style.backgroundColor = "silver";}
  }
  function setDate() {
    document.selectform.period.disabled = true;
    if(!NS4){document.selectform.period.style.backgroundColor = "silver";}
    document.selectform.month.disabled = false;
    if(!NS4){document.selectform.month.style.backgroundColor = "white";}
    document.selectform.year.disabled = false;
    if(!NS4){document.selectform.year.style.backgroundColor = "white";}
  }
  function submitForOtherDocument(documentType) {
    if (documentType == "<%=DocumentListSelectorForm.AUCTION%>") {
            document.location.href="<isa:webappsURL name="/auction/buyer/auctionmonitor.do"/>";
            return false;
    }
    if (document.selectform.<%=ContractSearchAction.DOCUMENT_TYPE%>[
        document.selectform.<%=ContractSearchAction.DOCUMENT_TYPE%>.selectedIndex].value
        != "<%=ContractSearchAction.CONTRACT%>") {
        document.selectform.period.disabled = true;
        if(!NS4){document.selectform.period.style.backgroundColor = "silver";}
        document.selectform.month.disabled = true;
        if(!NS4){document.selectform.month.style.backgroundColor = "silver";}
        document.selectform.year.disabled = true;
        if(!NS4){document.selectform.year.style.backgroundColor = "silver";}
        document.selectform.attributeValue.disabled = true;
        if(!NS4){document.selectform.attributeValue.style.backgroundColor = "silver";}
<!--        document.selectform.validitySelection.disabled = true; -->

        document.selectform.<%= ContractSearchAction.CONTRACT_SEARCH_BUTTON %>.disabled = true;
        document.selectform.submit();
    }
  }
  function updateAttributeValue(attribute) {
    if (attribute == "<%=form.NOT_SPECIFIED%>" || attribute != "<%=form.getAttribute()%>") {
        document.selectform.attributeValue.value = "";
    }
    if (attribute == "<%=form.NOT_SPECIFIED%>") {
        document.selectform.attributeValue.disabled = true;
        if(!NS4){document.selectform.attributeValue.style.backgroundColor = "silver";}
    }
    else {
        document.selectform.attributeValue.disabled = false;
        if(!NS4){document.selectform.attributeValue.style.backgroundColor = "white";}
    }
  }
</script>
</head>
<body class="organizer" onLoad="javascript:updateAttributeValue('<%=form.getAttribute()%>');<%=form.isValiditySelection(form.PERIOD) ? "javascript:setPeriod()" : "javascript:setDate()"%>">
<div id="organizer-search" class="module">
<div class="module-name"><isa:moduleName name="b2b/contract/contractsearch.jsp" /></div>
    <form name="selectform" method="post" action='<isa:webappsURL name="/b2b/contractsearch.do"></isa:webappsURL>'>
<table cellspacing="0" cellpadding="1" border="0" width="100%">
  <tbody>
      <%-- selection of document type --%>
      <tr>
          <td><isa:translate key="b2b.contract.shuffler.key1"/>&nbsp;</td>
          <td><select class="bigCatalogInput" name="<%=ContractSearchAction.DOCUMENT_TYPE%>"
                      onChange="submitForOtherDocument()">
                  <% if ((userSessionData.getAttribute(ShopReadAction.QUOTATION_NOT_ALLOWED) == null) ||
                        !((Boolean)userSessionData.getAttribute(ShopReadAction.QUOTATION_NOT_ALLOWED)).booleanValue()) { %>
                      <option value="<%=ContractSearchAction.QUOTATION%>">&nbsp;<isa:translate key="b2b.contract.shuffler.key1val2"/></option>
                  <% } %>
                  <option value="<%=ContractSearchAction.ORDER%>">&nbsp;<isa:translate key="b2b.contract.shuffler.key1val3"/></option>
                  <option value="<%=ContractSearchAction.ORDERTEMPLATE%>">&nbsp;<isa:translate key="b2b.contract.shuffler.key1val4"/></option>
                  <% if ((userSessionData.getAttribute(ShopReadAction.CONTRACT_NOT_ALLOWED) == null) ||
                        !((Boolean)userSessionData.getAttribute(ShopReadAction.CONTRACT_NOT_ALLOWED)).booleanValue()) { %>
                      <option value="<%=ContractSearchAction.CONTRACT%>" selected>&nbsp;<isa:translate key="b2b.contract.shuffler.key1val5"/></option>
                  <% } %>
                  <% if (isBillingDocEnabled && ! calledFromPortal) {%>
                  <option value="<%=ContractSearchAction.INVOICE%>">&nbsp;<isa:translate key="b2b.contract.shuffler.key1val6"/></option>
                  <option value="<%=ContractSearchAction.CREDITMEMO%>">&nbsp;<isa:translate key="b2b.contract.shuffler.key1val7"/></option>
                  <option value="<%=ContractSearchAction.DOWNPAYMENT%>">&nbsp;<isa:translate key="b2b.contract.shuffler.key1val8"/></option>
                  <% } %>
                  <% if ((userSessionData.getAttribute(IsaCoreInitAction.SC_AUCTION_ENABLED) != null) &&
                        ((Boolean)userSessionData.getAttribute(IsaCoreInitAction.SC_AUCTION_ENABLED)).booleanValue()) { %>
                      <option value="<%=ContractSearchAction.AUCTION%>">&nbsp;<isa:translate key="b2b.contract.shuffler.key1val9"/></option>
                  <% } %>
              </select>
          </td>
      </tr>
      <% if (contractNegotiationAllowed) { %>
      	<%-- selection of contract status --%>
      	<tr>
      		<td><isa:translate key="b2b.contract.shuffler.key7"/>&nbsp;</td>
            <td><select class="bigCatalogInput"
                        name="status"
            <!--            onChange="updateAttributeValue(document.selectform.attribute[document.selectform.attribute.selectedIndex].value)"> -->
                    <option value="<%=form.ALL_STATUS%>" <%=form.isStatus(form.ALL_STATUS) ? "selected" : ""%> ><isa:translate key="b2b.contract.shuffler.key7val1"/> 
                    <option value="<%=form.STATUS_IN_PROCESS%>" <%=form.isStatus(form.STATUS_IN_PROCESS) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key7val2"/> 
					<option value="<%=form.STATUS_SENT%>" <%=form.isStatus(form.STATUS_SENT) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key7val7"/>
					<option value="<%=form.STATUS_QUOTATION%>" <%=form.isStatus(form.STATUS_QUOTATION) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key7val4"/>
					<option value="<%=form.STATUS_ACCEPTED%>" <%=form.isStatus(form.STATUS_ACCEPTED) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key7val5"/>
                    <option value="<%=form.STATUS_RELEASED%>" <%=form.isStatus(form.STATUS_RELEASED) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key7val3"/>
                    <option value="<%=form.STATUS_REJECTED%>" <%=form.isStatus(form.STATUS_REJECTED) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key7val6"/>

                </select>
            &nbsp;</td>
        </tr>

      <% } %>


      <%-- selection of search attribute --%>
      <tr>
          <td><isa:translate key="b2b.contract.shuffler.key2"/>&nbsp;</td>
          <td><select class="bigCatalogInput"
                      name="attribute"
                      onChange="updateAttributeValue(document.selectform.attribute[document.selectform.attribute.selectedIndex].value)">
                  <option value="<%=form.NOT_SPECIFIED%>" <%=form.isAttribute(form.NOT_SPECIFIED) ? "selected" : ""%> ><isa:translate key="b2b.contract.shuffler.key2val1"/>
                  <option value="<%=form.ID%>" <%=form.isAttribute(form.ID) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key2val2"/>
                  <option value="<%=form.EXTERNAL_REF_NO%>" <%=form.isAttribute(form.EXTERNAL_REF_NO) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key2val3"/>
                  <option value="<%=form.DESCRIPTION%>" <%=form.isAttribute(form.DESCRIPTION) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key2val4"/>
              </select>
          &nbsp;</td>
      </tr>
      <tr>
          <td><isa:translate key="b2b.contract.shuffler.key3"/></td>
          <td><input type="text" class="bigCatalogInput"
                     name="attributeValue"
                     value="<%=form.getAttributeValue()%>"></td>
      </tr>
<%-- selections for validity dates --%>
            <tr><td colspan="2"><isa:translate key="b2b.contract.shuffler.key4"/></td>
            </tr>
 <%-- validity period --%>
      <tr>
          <td>&nbsp;</td>
          <td><input type="radio" name="validitySelection"
                     value="<%=form.PERIOD%>"
                     <%=form.isValiditySelection(form.PERIOD) ? "checked" : ""%>
                     onClick="javascript:setPeriod()">&nbsp;
              <select name="period" class="middleCatalogInput">
                  <option value="<%=form.TODAY%>" <%=form.isPeriod(form.TODAY) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key4val1"/>
                  <option value="<%=form.NEXT_WEEK%>" <%=form.isPeriod(form.NEXT_WEEK) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key4val2"/>
                  <option value="<%=form.NEXT_MONTH%>" <%=form.isPeriod(form.NEXT_MONTH) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key4val3"/>
                  <option value="<%=form.NEXT_YEAR%>" <%=form.isPeriod(form.NEXT_YEAR) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key4val4"/>
              </select>
          &nbsp;</td>
      </tr>
<%-- date --%>
      <tr>
          <td><isa:translate key="b2b.contract.shuffler.key5"/></td>
          <td><input type="radio" name="validitySelection"
                     value="<%=form.DATE%>"
                     <%=form.isValiditySelection(form.DATE) ? "checked" : ""%>
                     onClick="javascript:setDate()">&nbsp;
              <select name="month" class="yearInput">
                  <option value="<%=form.MONTH[0]%>" <%=form.isMonth(form.MONTH[0]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val1"/>
                  <option value="<%=form.MONTH[1]%>" <%=form.isMonth(form.MONTH[1]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val2"/>
                  <option value="<%=form.MONTH[2]%>" <%=form.isMonth(form.MONTH[2]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val3"/>
                  <option value="<%=form.MONTH[3]%>" <%=form.isMonth(form.MONTH[3]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val4"/>
                  <option value="<%=form.MONTH[4]%>" <%=form.isMonth(form.MONTH[4]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val5"/>
                  <option value="<%=form.MONTH[5]%>" <%=form.isMonth(form.MONTH[5]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val6"/>
                  <option value="<%=form.MONTH[6]%>" <%=form.isMonth(form.MONTH[6]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val7"/>
                  <option value="<%=form.MONTH[7]%>" <%=form.isMonth(form.MONTH[7]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val8"/>
                  <option value="<%=form.MONTH[8]%>" <%=form.isMonth(form.MONTH[8]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val9"/>
                  <option value="<%=form.MONTH[9]%>" <%=form.isMonth(form.MONTH[9]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val10"/>
                  <option value="<%=form.MONTH[10]%>" <%=form.isMonth(form.MONTH[10]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val11"/>
                  <option value="<%=form.MONTH[11]%>" <%=form.isMonth(form.MONTH[11]) ? "selected" : ""%> >&nbsp;<isa:translate key="b2b.contract.shuffler.key5val12"/>
              </select>&nbsp;
              <select name="year" class="yearInput">
                  <% for (int i = 0; i < form.YEAR.length; i++) { %>
                      <option value="<%=form.YEAR[i]%>" <%=form.isYear(form.YEAR[i]) ? "selected" : ""%> >&nbsp;<%=form.YEAR[i]%></option>
                  <% } %>
              </select>
              <isa:message id="validToError"
                           name="<%=ContractSearchAction.CONTRACT_HEADER_LIST%>"
                           type="<%=Message.ERROR%>"
                           property="validTo"
                           ignoreNull="true">
                  <br><%=validToError%>
              </isa:message>
          </td>
      </tr>
      <tr>
          <td><isa:translate key="b2b.contract.shuffler.key6"/></td>
          <td>
              <input class="green" type=submit
                     name="<%= ContractSearchAction.CONTRACT_SEARCH_BUTTON %>"
                     value='<isa:translate key="b2b.contract.shuffler.search"/>' >
          </td>
      </tr>
  </tbody>
</table>
</form>
<br><br>
<%-- if requested, table with results (contract headers) of selection --%>
<% if (form.isSelectRequested()) { %>
    <% ContractHeaderList headerList =
        (ContractHeaderList)pageContext.findAttribute(ContractSearchAction.CONTRACT_HEADER_LIST); %>
    <% if (headerList.isEmpty()) { %>
        <isa:message id="hits"
                     name="<%=ContractSearchAction.CONTRACT_HEADER_LIST%>"
                     type="<%=Message.INFO%>"
                     property="hits">
            <%=hits%>
        </isa:message>
    <% } else { %>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tbody>
    <tr>
      <td>
        <div class="searchresult"><isa:translate key="b2b.contract.hits.results"/><br>
            <span style="font-weight:normal">
                <isa:translate key="b2b.contract.hits.found"
                    arg0 = "<%=Integer.toString(headerList.size())%>"/>
            </span>
        </div>
      </td>
    </tr>
    <tr>
      <td>
      <% if (!contractNegotiationAllowed) { %>
         <table class="list" border="0" cellspacing="0" cellpadding="3" width="100%">
            <thead>
               <tr>
                 <th style="text-align: left"><isa:translate key="b2b.contract.hits.id"/><isa:translate key="b2b.contract.hits.separator"/><br><isa:translate key="b2b.contract.hits.validToDate"/></th>
                 <th style="text-align: left"><isa:translate key="b2b.contract.hits.externalRefNo"/><isa:translate key="b2b.contract.hits.separator"/><br><isa:translate key="b2b.contract.hits.description"/></th>
               </tr>
            </thead>
            <tbody>
               <% int line = 0; %>
               <isa:iterate id="contract" name="<%= ContractSearchAction.CONTRACT_HEADER_LIST %>" type="com.sap.isa.businessobject.contract.ContractHeader">
                   <tr class="<%= evenOdd[++line % 2]%>">
                      <td nowrap align="left"><a href='<isa:webappsURL name="b2b/contractselect.do"></isa:webappsURL>?<%=ContractSelectAction.CONTRACT_SELECTED%>=<%=contract.getTechKey().getIdAsString()%>'
                         target="form_input">
                         <%= contract.getId() %>
                         </a><isa:translate key="b2b.contract.hits.separator"/><br><%= contract.getValidToDate() %>
                      </td>
                      <td nowrap align="left"><%= contract.getExternalRefNo() %><isa:translate key="b2b.contract.hits.separator"/><br><%= contract.getDescription() %></td>
                   </tr>
               </isa:iterate>
            </tbody>
         </table>
      <% }
      else { %>
        <table class="list" border="0" cellspacing="0" cellpadding="3" width="100%">
          <thead>
            <tr>
                <th style="text-align: left"><isa:translate key="b2b.contract.hits.status"/></th>
                <th style="text-align: left"><isa:translate key="b2b.contract.hits.number"/><isa:translate key="b2b.contract.hits.separator"/><br><isa:translate key="b2b.contract.hits.period"/></th>
                <th style="text-align: left"><isa:translate key="b2b.contract.hits.externalRefNo"/><isa:translate key="b2b.contract.hits.separator"/><br><isa:translate key="b2b.contract.hits.description"/></th>
            </tr>
          </thead>
          <tbody>
              <% int line = 0; %>
              <isa:iterate id="contract" name="<%= ContractSearchAction.CONTRACT_HEADER_LIST %>" type="com.sap.isa.businessobject.contract.ContractHeader">
                 <tr class="<%= evenOdd[++line % 2]%>">
                    <% if (contract.isStatusReleased()) { %>
                         <td> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_led_green.gif")%>"
                         align="center"
                         alt="<isa:translate key="b2b.contract.shuffler.key7val3"/>">
                    </td>
                    <% } else  { %>
                       <% if (contract.isStatusRejected()) { %>
                          <td> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_led_red.gif")%>"
                                    align="center"
                                    alt="<isa:translate key="b2b.contract.shuffler.key7val6"/>" >
                       <% } else  { %>
                           <% if (contract.isStatusInProcess()) { %>
                              <td> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_led_yellow.gif")%>"
                                        align="center"
                                        alt="<isa:translate key="b2b.contract.shuffler.key7val2"/>" >
                           <% } else  { %>
                               <% if (contract.isStatusSent()) { %>
                                  <td> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_led_yellow.gif")%>"
                                            align="center"
                                            alt="<isa:translate key="b2b.contract.shuffler.key7val7"/>" >
                               <% } else  { %>
                                   <% if (contract.isStatusQuotation()) { %>
                                      <td> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_led_yellow.gif")%>"
                                      align="center"
                                      alt="<isa:translate key="b2b.contract.shuffler.key7val4"/>" >
                                   <% } else  { %>
                                       <% if (contract.isStatusAccepted()) { %>
                                          <td> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_led_yellow.gif")%>"
                                          align="center"
                                          alt="<isa:translate key="b2b.contract.shuffler.key7val5"/>" >
                                       <% } else  { %>
                                           <% if (contract.isStatusCompleted()) { %>
                                              <td> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_led_red.gif")%>"
                                              align="center"
                                              alt="<isa:translate key="b2b.contract.shuf.key7val8"/>" >
                                              <% } else  { %>
                                                  <td> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_led_red.gif")%>"
                                                    align="center"
                                                    alt="<isa:translate key="b2b.contract.shuffler.key7val2"/>" >
                                              <% } %>
                                       <% } %>
                                   <% } %>
                               <% } %>
                           <% } %>
                       <% } %>
                    <% } %>
                    <% if (contract.isStatusReleased() ) { %>
                        <td nowrap align="left"><a href='<isa:webappsURL name="b2b/contractselect.do"></isa:webappsURL>?<%=NegotiatedContractConstants.CONTRACT_SELECTED%>=<%=contract.getTechKey().getIdAsString()%>'
                            target="form_input">
                            <%= contract.getId() %>
                            </a><isa:translate key="b2b.contract.hits.separator"/><br><%= contract.getValidToDate() %>
                        </td>
                    <% } else  { %>
                        <td nowrap align="left"><a href='<isa:webappsURL name="b2b/preparenegotiatedcontract.do"></isa:webappsURL>?<%=NegotiatedContractConstants.CONTRACT_SELECTED%>=<%=contract.getTechKey().getIdAsString()%>'
                            target="form_input">
                            <%= contract.getId() %>
                            </a><isa:translate key="b2b.contract.hits.separator"/><br><%= contract.getValidToDate() %>
                    <% } %>
                    <td nowrap align="left"><%= contract.getExternalRefNo() %><isa:translate key="b2b.contract.hits.separator"/><br><%= contract.getDescription() %></td>
                 </tr>
              </isa:iterate>
          </tbody>
        </table>
      <% } %>
      </td>
    </tr>
  </tbody>
</table>
        <% } %>
    <% } %>
</div>
</body>
</html>