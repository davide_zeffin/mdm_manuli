<%/*****************************************************************************
    Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Hunsicker
    Created:      17 May 2001

    $Revision: #1 $
    $Date: 2001/05/17 $
*****************************************************************************/%>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>

<%@ include file="/b2b/checksession.inc" %>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
    <head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
      
	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>		
	
          <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
                  type="text/javascript">
          </script>
		  
        <%-- load error page in the form_input frame --%>
        <script type="text/javascript">

            form_input().location.href =
                '<isa:webappsURL name="/b2b/contract/contractnotfound.jsp"></isa:webappsURL>';
            getHistoryFrame().location.href =
                '<isa:webappsURL name="/b2b/history/updatehistory.do"></isa:webappsURL>';
        </script>
    </head>
    <body></body>
</html>
