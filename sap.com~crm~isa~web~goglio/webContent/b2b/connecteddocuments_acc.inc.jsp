<%--
********************************************************************************
    File:         connecteddocuments_acc.inc.jsp
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      08.03.2004
    Version:      1.0

    $Revision: #0 $
    $Date: 2004/04/08 $
********************************************************************************
--%>
<% List predecessorList = ui.header.getPredecessorList();
   ConnectedDocument predecessor;
   String predecessorHref;
   String predecessorTextSingle = "status.sales.predecessor";
   if (predecessorList.size() > 1) {
       predecessorText = predecessorText.concat("s");
   }
   if (!ui.isHomActivated()) {
      for (int i = 0; i < predecessorList.size(); i++) {
          predecessor = (ConnectedDocument)predecessorList.get(i);
          predecessorTypeKey = "status.sales.dt.".concat(predecessor.getDocType());
          if (predecessor.isDisplayable()) {
              predecessorHref = ("b2b/documentstatusdetailprepare.do?".concat("techkey=".concat(predecessor.getTechKey().getIdAsString()))).concat("&");
              predecessorHref = ((predecessorHref.concat("object_id=")).concat(predecessor.getDocNumber())).concat("&");
              predecessorHref = predecessorHref.concat("objects_origin=").concat("&").concat("objecttype=").concat(predecessor.getDocType());
          %>
          <tr>
              <% if (leftColumn) { %> <td>&nbsp;</td> <% } %>
              <td class="value"><% if (i == 0) { %><isa:translate key="<%= predecessorText %>"/>:<% } %></td>
              <td class="value"><a class="icon" href='<isa:webappsURL name="<%= predecessorHref %>"/>'><isa:translate key="<%= predecessorTypeKey %>"/> <%= predecessor.getDocNumber() %></a>
              </td>
          </tr>
          <%
          } else {
          %>
          <tr>
              <% if (leftColumn) { %> <td>&nbsp;</td> <% } %>
              <td class="value"><% if (i == 0) { %><isa:translate key="<%= predecessorText %>"/>:<% } %></td>
              <td class="value"><isa:translate key="<%= predecessorTypeKey %>"/> <%= predecessor.getDocNumber() %></td>
          </tr>
          <%
          }
      }
   }
   List successorList = ui.header.getSuccessorList();
   ConnectedDocument successor;
   String successorTypeKey;
   String successorHref;
   String successorText = "status.sales.successor";
   if (successorList.size() > 1) {
       successorText = successorText.concat("s");
   }
   for (int i = 0; i < successorList.size(); i++) {
       successor = (ConnectedDocument)successorList.get(i);
       successorTypeKey = "status.sales.dt.".concat(successor.getDocType());
       if (successor.isDisplayable()) {
           successorHref = ("b2b/documentstatusdetailprepare.do?".concat("techkey=".concat(successor.getTechKey().getIdAsString()))).concat("&");
           successorHref = ((successorHref.concat("object_id=")).concat(successor.getDocNumber())).concat("&");
//           successorHref = successorHref.concat("objects_origin=").concat("&").concat("objecttype=").concat(successor.getDocType());;
           successorHref = successorHref.concat("objects_origin=").concat(successor.getDocumentsOrigin()).concat("&").concat("objecttype=").concat(successor.getDocType());


//            add the &billing=X if needed
           if( "BILL".equals( successor.getAppTyp() ) ){
           successorHref = successorHref.concat("&billing=X");
           }
           %>
           <tr>           <% if (leftColumn) { %> <td>&nbsp;</td> <% } %>
           <td class="value"><% if (i == 0) { %><isa:translate key="<%= successorText %>"/>:<% } %></td>
           <td class="value"><a class="icon" href='<isa:webappsURL name="<%= successorHref %>"/>'><isa:translate key="<%= successorTypeKey %>"/> <%= successor.getDocNumber() %></a>
           </td>
       </tr>
       <%
       } else {
       %>
       <tr>
           <% if (leftColumn) { %> <td>&nbsp;</td> <% } %>
           <td class="value"><% if (i == 0) { %><isa:translate key="<%= successorText %>"/>:<% } %></td>
           <td class="value"><isa:translate key="<%= successorTypeKey %>"/> <%= successor.getDocNumber() %></td>
       </tr>
       <%
       }
   }
%>
