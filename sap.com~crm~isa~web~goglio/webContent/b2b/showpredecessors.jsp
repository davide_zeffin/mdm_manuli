<%--
********************************************************************************
    
    THIS JSP IS OBSOLET. The functionality had been replace by the use of the
    Generic Search Framework !!


    
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      08.06.2001

    $Revision: #1 $
    $Date: 2001/05/08 $
********************************************************************************
--%>

<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ include file="/b2b/checksession.inc" %>

<% BaseUI ui = new BaseUI(pageContext);

   String docCount = (String) request.getAttribute(DocumentListAction.RK_DOCUMENT_COUNT);

   String theme    = "",
          language = "";

    JspUtil.Alternator alternator =
            new JspUtil.Alternator(new String[] {"odd", "even" });


    String sourceType = request.getParameter(SelectPredecessorDocumentAction.RK_SOURCE);
    String targetType = request.getParameter(SelectPredecessorDocumentAction.RK_TARGET);
    String processType = request.getParameter(SelectPredecessorDocumentAction.RK_PROCESS_TYPE);
    String title      = null;
    String tableHeader = null;

    if ("ordertemplate".equals(sourceType)) {
        title = WebUtil.translate(pageContext, "b2b.selectpredecessordoc.ot", null);
        tableHeader = WebUtil.translate(pageContext, "b2b.selectpredecessordoc.numot", new String[] { String.valueOf(docCount) });
    }
    else if ("quotation".equals(sourceType)) {
        title = WebUtil.translate(pageContext, "b2b.selectpredecessordocument.qt", null);
        tableHeader = WebUtil.translate(pageContext, "b2b.selprddoc.numfound.qt", new String[] { String.valueOf(docCount) });
    }
%>


<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="b2b.selectpredecessordoc.titel"/></title>
        <isa:stylesheets/>
        <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js")%>"
            type="text/javascript"> 
        </script>        
        <script type="text/javascript">
        <!--
            function goBack() {
                getElement("createfromtemplate").submit();
            }
        //-->
        </script>        
    </head>
    <body class="showpredecessors">
    
        <div class="module-name"><isa:moduleName name="b2b/showpredecessors.jsp" /></div>
        <% if ( docCount != null ) { %>
            <h1><%= title %></h1>
			<div class="predecessors-doc">
            <div class="opener"><%= tableHeader %></div>
            <table class="filter-result" border="0" cellspacing="0" cellpadding="3">
              <%-- Tableheader --%>
              <thead>
                 <tr>
                   <th><isa:translate key="b2b.selectpredecessordoc.num"/></th>
                   <th><isa:translate key="b2b.selectpredecessordoc.refno"/></th>
                   <th><isa:translate key="b2b.selectpredecessordoc.name"/></th>
                   <th><isa:translate key="b2b.selectpredecessordoc.date"/></th>
                 </tr>
              </thead>
              <%-- Tablerows --%>
              <tbody>
               <isa:iterate id="orderheader" name="<%= SelectPredecessorDocumentAction.RK_ORDER_LIST %>"
                            type="com.sap.isa.businessobject.header.HeaderSalesDocument">
                 <tr class="<%= alternator.next() %>">
                   <td>
                     <a href="<isa:webappsURL name="b2b/createfrompredecessor.do">
                                  <isa:param name="source" value="<%= sourceType %>" />
                                  <isa:param name="target" value="<%= targetType %>" />
                                  <isa:param name="techkey" value="<%= orderheader.getTechKey().toString() %>" />
                                  <isa:param name="salesdocnumber" value="<%= orderheader.getSalesDocNumber() %>" />
                                  <isa:param name="processtype" value="<%= processType %>" />
                              </isa:webappsURL>" >    
                       <%= orderheader.getSalesDocNumber() %>
                     </a>
                   </td>
                   <td>
                     <a href="<isa:webappsURL name="b2b/createfrompredecessor.do">
                                  <isa:param name="source" value="<%= sourceType %>" />
                                  <isa:param name="target" value="<%= targetType %>" />
                                  <isa:param name="techkey" value="<%= orderheader.getTechKey().toString() %>" />
                                  <isa:param name="salesdocnumber" value="<%= orderheader.getSalesDocNumber() %>" />
                                  <isa:param name="processtype" value="<%= processType %>" />
                              </isa:webappsURL>" >
                       <%= orderheader.getPurchaseOrderExt() %>
                     </a>
                   </td>
                   <td>
                     <a href="<isa:webappsURL name="b2b/createfrompredecessor.do">
                                  <isa:param name="source" value="<%= sourceType %>" />
                                  <isa:param name="target" value="<%= targetType %>" />
                                  <isa:param name="techkey" value="<%= orderheader.getTechKey().toString() %>" />
                                  <isa:param name="salesdocnumber" value="<%= orderheader.getSalesDocNumber() %>" />
                                  <isa:param name="processtype" value="<%= processType %>" />
                              </isa:webappsURL>" >                                          
                       <%= orderheader.getDescription() %>
                     </a>
                   </td>
                   <td>
                     <%= orderheader.getChangedAt() %>
                   </td>
                 </tr>
               </isa:iterate>
               </tbody>
           </table>
           </div>
           <%-- buttons --%>
           <form id="createfromtemplate" method="get" action="<isa:webappsURL name ="b2b/updatedocumentview.do"/>" >
               <div  id="buttons">
                   <ul class="buttons-1">
                       <li>
                           <a href="#" onclick="goBack();" <% if (ui.isAccessible) { %> title="<isa:translate key="error.jsp.back"/>" <% } %> ><isa:translate key="error.jsp.back"/></a>
                       </li>
                   </ul>
               </div>
           </form>
  <% } %>

</body>
</html>