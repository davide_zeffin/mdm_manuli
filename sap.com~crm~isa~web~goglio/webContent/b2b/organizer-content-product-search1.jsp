<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>
<%@ page import="com.sap.isa.isacore.action.ShowProductDetailAction" %>
<%@ page import="com.sap.isa.businessobject.IsaQuickSearch" %>
<%@ page import="com.sap.isa.businessobject.ProductList" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.isacore.action.QuickSearchAction" %>
<%@ page import="com.sap.isa.isacore.action.InitQuickSearchAction" %>
<%@ page import="com.sap.isa.isacore.action.QuickSearchAddToBasketAction" %>
<%@ page import="com.sap.isa.isacore.action.PageQuickSearchAction" %>
<%@ page import="com.sap.isa.core.FrameworkConfigManager" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.IsaQuickSearchUI" %>
<%@ page import="com.sap.isa.businessobject.Shop" %>

<%@ include file="checksession.inc" %>
<%@ include file="usersessiondata.inc" %>

<isa:contentType />
<% 
    IsaQuickSearchUI ui = new IsaQuickSearchUI(pageContext); 
%>

<%@ include file="/b2b/checksession.inc" %>
<%
    ServletContext context = pageContext.getServletContext();

    String maxHits = FrameworkConfigManager.Servlet.getInteractionConfigParameter(pageContext.getSession(),
         "ui", 
         "maxhits.search.isacore", 
         "maxhits.search.isacore.isa.sap.com");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><isa:translate key="b2b.htmltitel.standard"/></title>
        <isa:stylesheets/>
		
        <script type="text/javascript">
            <%@ include file="/b2b/jscript/urlrewrite.inc" %>
        </script>

        <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js") %>"
            type="text/javascript">
        </script>
				
        <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/addtodocument.js") %>"
            type="text/javascript">
        </script>

        <script type="text/javascript">
        <!--
          function openWin( addURL ) {
            var url = '<isa:webappsURL name="b2b/productdetail.do"/>' + addURL;
            var sat = window.open(url, 'artikel_to_order', 'width=800,height=600,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
            sat.focus();
          }
	      
          function transferItem(productId, productGuid, unit, isR3) {
            if (getMyForm() == null || isR3 == 'true') {
              form_input().location.href="<isa:webappsURL name="/b2b/quicksearchaddtobasket.do"/>?<%=QuickSearchAddToBasketAction.RC_PRODUCT_GUID%>=" + productGuid + "&<%=QuickSearchAddToBasketAction.RC_PRODUCT_UNIT%>=" + unit;
            }
            else {
                writeProdNumber(productId);
            }
          }
	      
          function searchProducts(){
            document.productsearch.submit();
          }
	      
        //-->
        </script>
  </head>
  
  <body class="document-search" onload="setAddToDocumentErrorMessages('<isa:translate key="iqs.addtobasket.noopenorder"/>',
                                  '<isa:translate key="iqs.product.addtobasket.nopos"/>');" >
    <div id="organizer-content" class="module">
        <div class="module-name">
            <isa:moduleName name="b2b/organizer-content-product-search1.jsp" />
        </div>
		
        <% IsaQuickSearch iqs = (IsaQuickSearch) request.getAttribute(InitQuickSearchAction.QS_OBJECT); %>
			
        <div class="filter">
		
            <div class="filter-title">
                <span><isa:translate key="iqs.input.header"/></span>
            </div>
			
            <form name="productsearch" method="post" action="<isa:webappsURL name="b2b/nextquicksearch.do" />" >
            <% if (ui.isAccessible) { %>
                <a href="#end-search" name="begin-search" title="<isa:translate key="b2b.organizer.acc.endsearch"/>"></a>
            <% } %>
            <%-- Accesskey for the header section. It must be an resourcekey, so that it can be translated. --%>
            <div>
                <a id="organizer" href="#access-items" title="<isa:translate key="b2b.organizer.acc.accesskey"/>" accesskey="<isa:translate key="b2b.organizer.acc.accesskey.key"/>"></a>
            </div>
                <ul>
                    <li>
                        <span title="label shuffler">
                            <select id="doctype" class="bigCatalogInput" name="attribute">
                                <%-- we have to construct the key for the translate-tag dynamically --%>
                                <%-- init the String-var for this here --%>
                                <% String thePrefix = "iqs.attribute."; %>
                                <% String theKey; %>
                                <isa:iterate id="attributes" name="<%= InitQuickSearchAction.QS_COMMONATTRIBUTES_LIST %>"
                                         type="com.sap.isa.catalog.impl.CatalogAttribute">
			
                                    <% String theDescription = InitQuickSearchAction.getAttributeDescription( pageContext, thePrefix, attributes);
                                       if (theDescription != null) {
                                    %>
                                           <option value="<%=attributes.getName() %>" <%=attributes.getName().equalsIgnoreCase(iqs.getSearchAttribute()) ? "selected=\"selected\"" : "" %>><%=theDescription %></option>
                                    <% } %>
                                </isa:iterate>
                            </select>
                        </span>				
                    </li>
                    <li>
                        <span><input name="searchexpr" type="text" class="input-1" value="<%=iqs.getSearchexpr() %>"/></span>
                    </li>
                    <li>
                        <%-- create a dropdown box (I hate this word) to display the max. number of hits to return --%>
                          <select name="displayHits" style="width:50px" title="<isa:translate key="b2b.organizer.displayhits.title" />">
                              <%-- only hardcoded values here --%>
                              <option value="5" <%="5".equals(iqs.getDisplayHitsAsString()) ? "selected=\"selected\"" : ""%>>5</option>
                              <option value="10" <%="10".equals(iqs.getDisplayHitsAsString()) ? "selected=\"selected\"" : ""%>>10</option>
                              <option value="25" <%="25".equals(iqs.getDisplayHitsAsString()) ? "selected=\"selected\"" : ""%>>25</option>
                              <option value="50" <%="50".equals(iqs.getDisplayHitsAsString()) ? "selected=\"selected\"" : ""%>>50</option>
                              <option value="100" <%="100".equals(iqs.getDisplayHitsAsString()) ? "selected=\"selected\"" : ""%>>100</option>
                          </select><label for="displayHits" >&nbsp;<isa:translate key="iqs.input.displayhits"/></label>			
                    </li>
                    <li>
                        <a href="#" onclick="searchProducts();" title="<isa:translate key="iqs.input.submit" />: <isa:translate key="b2b.organizer.search.button" />" class="button"><isa:translate key="iqs.input.submit" /></a>
                    </li>
                    <%-- hidden field containing the maximim number of hits to read from the catalog engine --%>
                    <li>
                        <input type="hidden" name="maxHits" value="<%=maxHits%>" />
                    </li>
                </ul>
                <% if (ui.isAccessible) { %>
                    <a href="#begin-search" name="end-search" title="<isa:translate key="b2b.organizer.acc.beginsearch"/>"></a>
                <% } %>
            </form>
        </div>
		
        <div class="filter-result-msg" >
            <isa:translate key="iqs.result.header2" />
            <span style="font-weight:normal">
                <isa:translate key="iqs.result.numhits" arg0="<%=iqs.getTotalHitsAsString() %>" />
            </span>
        </div>

        <div class="filter-result">
            <%-- display the navigation line only if we found more than one page of results --%>
            <% if (iqs.getTotalPages() > 1) { %>
            <%  /* calculate the "navigation"-line */
                int totalPages = iqs.getTotalPages();
                int curPage    = iqs.getCurPage();

                String forward  = "b2b/pagequicksearch.do?QuickSearchPageCommand=forward&curPage=" + curPage;
                String backward = "b2b/pagequicksearch.do?QuickSearchPageCommand=backward&curPage=" + curPage;

                switch(totalPages) {
                case 1:
                   /* nothing */
                break;

                case 2:
                    if (curPage == 1) {
                    %>
                        <span class="search-no">&lt;</span>
                               | <span class="search-no">1</span>
                               | <a href="<isa:webappsURL name="b2b/pagequicksearch.do?QuickSearchPageCommand=2" />">2</a>
                               | <a href="<isa:webappsURL name="<%=forward%>" />">&gt;</a>
                    <%
                    }
                    else {
                    %>
                        <a href="<isa:webappsURL name="<%=backward%>" />">&lt;</a>
                               | <a href="<isa:webappsURL name="b2b/pagequicksearch.do?QuickSearchPageCommand=1" />">1</a>
                               | <span class="search-no">2</span>
                               | <span class="search-no">&gt;</span>
                    <%
                    }
                break;

                default:
                    if (curPage == 1) {
                    %>
                        <span class="search-no">&lt;</span>
                               | <span class="search-no">1</span>
                               | <a href="<isa:webappsURL name="b2b/pagequicksearch.do?QuickSearchPageCommand=2" />">2</a>
                               | <a href="<isa:webappsURL name="b2b/pagequicksearch.do?QuickSearchPageCommand=3" />">3</a>
                               | <a href="<isa:webappsURL name="<%=forward%>" />">&gt;</a>
                    <%
                    }
                    else if (curPage == totalPages) {
                        String link1 = "b2b/pagequicksearch.do?QuickSearchPageCommand=" + (curPage - 2);
                        String link2 = "b2b/pagequicksearch.do?QuickSearchPageCommand=" + (curPage - 1);
                    %>
                        <a href="<isa:webappsURL name="<%=backward%>" />">&lt;</a>
                                    | <a href="<isa:webappsURL name="<%=link1%>" />"><%=(curPage - 2) %></a>
                                    | <a href="<isa:webappsURL name="<%=link2%>" />"><%=(curPage - 1) %></a>
                                    | <span class="search-no"><%=curPage%></span>
                                    | <span class="search-no">&gt;</span>
                    <%
                    }
                    else {
                        String link1 = "b2b/pagequicksearch.do?QuickSearchPageCommand=" + (curPage - 1);
                        String link2 = "b2b/pagequicksearch.do?QuickSearchPageCommand=" + (curPage + 1);
                    %>
                        <a href="<isa:webappsURL name="<%=backward%>" />">&lt;</a>
                                    | <a href="<isa:webappsURL name="<%=link1%>" />"><%=(curPage - 1) %></a>
                                    | <span class="search-no"><%=curPage%></span>
                                    | <a href="<isa:webappsURL name="<%=link2%>" />"><%=(curPage + 1) %></a>
                                    | <a href="<isa:webappsURL name="<%=forward%>" />">&gt;</a>
                    <%
                    }
                break; /* enddefault */
                } /* endswitch() */
                %>
            <% } /* endif ( iqs.getTotalPages() > 1 ) */ %>		
		

            <% /* define some variables used for sorting the results */
               /* the columns to sort after are numbered, starting with 0 */
               int curPage    = iqs.getCurPage();
               int curSortCol = iqs.getSortCol();
		
               String sortFirstCol  = "b2b/pagequicksearch.do?QuickSearchPageCommand=sort&curPage=" + curPage
                                              + "&sortCol=" + 0 + "&searchexpr=" + iqs.getSearchexpr() + "&displayHits=" + iqs.getDisplayHits();
               String sortSecondCol = "b2b/pagequicksearch.do?QuickSearchPageCommand=sort&curPage=" + curPage
                                              + "&sortCol=" + 1 + "&searchexpr=" + iqs.getSearchexpr() + "&displayHits=" + iqs.getDisplayHits();
               String sortThirdCol  = "b2b/pagequicksearch.do?QuickSearchPageCommand=sort&curPage=" + curPage
                                              + "&sortCol=" + 2 + "&searchexpr=" + iqs.getSearchexpr() + "&displayHits=" + iqs.getDisplayHits();
            %>
              <% if (ui.isAccessible) { %>
                <a href="#end-table1" title="Table. [4] columns and [<%= iqs.getDisplayHitsAsString() %>] rows. To skip, activate the link."></a>
              <% } %>
              <table border="0" cellspacing="0" cellpadding="2" width="99%">
                  <tr>
                    <th><a href="<isa:webappsURL name="<%=sortFirstCol%>" />"><isa:translate key="isacore.productId"/></a><br/><a class="icon" href="<isa:webappsURL name="<%=sortFirstCol%>" />">
                      <% if (curSortCol == 0) { %>
                        <img class="display-image" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/arrow_down.gif") %>" width="9" height="5" alt="" border="0" />
                      <% } else { %>
                        <img class="display-image" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="9" height="5" alt="" border="0" />
                      <% } %>
                      </a></th>
                    <th><a href="<isa:webappsURL name="<%=sortSecondCol%>" />"><isa:translate key="isacore.productDescription"/></a><br/><a class="icon" href="<isa:webappsURL name="<%=sortSecondCol%>" />">
                      <% if (curSortCol == 1) { %>
                        <img class="display-image" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/arrow_down.gif") %>" width="9" height="5" alt="" border="0" />
                      <% } else { %>
                        <img class="display-image" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="9" height="5" alt="" border="0" />
                      <% } %>
                      </a></th>
                    <th><isa:translate key="isacore.price"/></th>
                    <th>&nbsp;</th>
                  </tr>

                  <% ProductList prodlist = (ProductList) request.getAttribute(QuickSearchAction.QS_RESULT_LIST); %>
			
                  <%-- use this flag to switch between the odd and even display --%>
                  <%-- modes of the lines in the result table                   --%>
                  <% int swap = 0; %>
			
                  <isa:iterate id="product" name="<%= QuickSearchAction.QS_RESULT_LIST %>"
                               type="com.sap.isa.businessobject.Product">
                      <% swap = 1 - swap; %>
                      <% if ( swap == 1 ) { %>
                      <tr class="even">
                      <% }
                         else { %>
                      <tr class="odd">
                      <% } %>
                        <td <% if (curSortCol == 0) { %> <% } %> >
                            <a href="#" onclick="openWin('<%=ShowProductDetailAction.createDetailRequest(product, ActionConstants.DS_IN_WINDOW) %>')"  title="<%=product.getId() %>: <isa:translate key="b2b.organizer.acc.detaillink" />">
                                <%=product.getId() %>
                            </a>
                        </td>
                        <td <% if (curSortCol == 1) { %> <% } %> >
                            <a href="#" onclick="openWin('<%=ShowProductDetailAction.createDetailRequest(product, ActionConstants.DS_IN_WINDOW) %>')"  title="<%=product.getDescription() %>: <isa:translate key="b2b.organizer.acc.detaillink" />">
                                <%=product.getDescription() %>
                            </a>
                        </td>
                        <td align="right" <% if (curSortCol == 2) { %> <% } %>>
                        <%if (ui.isPriceVisible()) { %>
                              <%=product.getItemPrice() %>
                              <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
                              </isacore:ifShopProperty>
                         <% } %>
                        </td>
                        <td>
                            <a href="#" onclick="transferItem('<%=product.getId()%>','<%=product.getCatalogItem().getProductID()%>', '<%=product.getCatalogItem().getUnit()%>', '<%=ui.isBackendR3()%>');" title="<%=product.getDescription() %>: <isa:translate key="b2b.organizer.acc.addtobasket" />">
                                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_wk_green.gif") %>" width="16" height="16" alt="<isa:translate key="catalog.isa.basket"/>" border="0" />
                            </a>
                        </td>
                      </tr>
                  </isa:iterate>
              </table>
             <% if (ui.isAccessible) { %>
                <a name="end-table1" title="End table"></a>
             <% } %>
             </div>
        </div>
    </div>  
<%-- GREENORANGE BEGIN --%>
	<div id="zgo_sign">
		<b><font color="01663E">Fres-co Online Sales</font></b><br/>
		<b><font color="01663E">a Division of Goglio SpA</font></b><br/>
		Via Dell'Industria 7<br/>
		21020 Daverio (VA) - Italy<br/>
		Phone: +39 0332 940240 <br/>
		Fax: +39 0332 940716 <br/>
		P.I. IT00870210150 <br/>
		Capitale sociale Euro 10.449.000<br/>
		Registro Imprese Milano n.00870210150<br/>
		C.C.I.A.A. Milano Rea N. 1856<br/>
		(Mecc.Export MIO78374)<br/>
	</div>
<%-- GREENORANGE END --%>	    
  </body>
</html>