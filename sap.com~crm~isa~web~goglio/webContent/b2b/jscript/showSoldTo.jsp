<%@ page import="com.sap.isa.businesspartner.action.BupaConstants" %>

<%@ taglib uri="/isa" prefix="isa" %>

    function showSoldTo(bupaKey) {
      var url = '<isa:webappsURL name="/b2b/businesspartnershow.do"/>?<%=BupaConstants.DISPLAY_IN_WINDOW%>=true&<%=BupaConstants.READ_ONLY%>=true&<%=BupaConstants.BUPA_TECHKEY %>=' + bupaKey ;
      var sat = window.open(url, 'sat_details', 'width=450,height=330,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
      sat.focus();
    }

