/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      29.4.2001
    Version:      1.0

    $Revision: #1 $
    $Date: 2001/04/29 $
*****************************************************************************/



    function toggle(idName) {
		isaEasyToggle(idName);
	}
	function setPageCommand(command) {
    	document.contractitemsform.<%=ContractReadAction.CONTRACT_PAGE_COMMAND%>.value = command;
        document.contractitemsform.submit();
    }
    function setPageSize(pageSize) {
    	document.contractitemsform.<%=ContractReadAction.CONTRACT_PAGE_SIZE%>.value = pageSize;
    	document.contractitemsform.submit();
    }
    function expandItem(itemKey) {
    	document.contractitemsform.<%=ContractReadAction.CONTRACT_EXPAND_ITEM%>.value = itemKey;
        document.contractitemsform.submit();
    }
    function collapseItem(itemKey) {
    	document.contractitemsform.<%=ContractReadAction.CONTRACT_COLLAPSE_ITEM%>.value = itemKey;
        document.contractitemsform.submit();
    }
    function releaseItem(itemKey) {
        contractitemsform.<%=ContractReadAction.CONTRACT_RELEASE_ITEM%>.value = itemKey;
        contractitemsform.elements["requested["+itemKey+"]"].checked = true;          
        contractitemsform.<%=ContractReadAction.CONTRACT_RELEASE_COMMAND%>.value = "<%=ContractReadAction.CONTRACT_RELEASE%>";
        contractitemsform.target = "form_input";
        contractitemsform.submit();
    }
    function configItem(itemKey) {
        form_input().location.href = "<isa:webappsURL name='/b2b/contractinitconfiguration.do'/>?<%=ContractConfigurationAction.CONTRACT_CONFIGURE_ITEM_KEY%>=" + itemKey;
        return true;
    }
    function releaseContract() {
        if (contractitemsform) {
        	document.contractitemsform.<%=ContractReadAction.CONTRACT_RELEASE_COMMAND%>.value = "<%=ContractReadAction.CONTRACT_RELEASE%>";
            contractitemsform.target = "form_input";
            contractitemsform.submit();
        }
    }
    function closeContract() {
    		form_input().location.href = '<isa:webappsURL name="b2b/contractclose.do"></isa:webappsURL>';
    }    