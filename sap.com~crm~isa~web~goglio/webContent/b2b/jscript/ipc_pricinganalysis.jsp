<%--
/*-----------------------------------------------------------------------------------------------*/
/* Javascript for the "SAP E-selling Project"                                                          */
/*                                                                                               */
/* Document:       ipc_pricinganalysis.jsp                                                       */
/* Description:    This document contains a function which is used to call the IPC               */ 
/*                 Pricing Analysis Tool.                                                        */
/* Version:        1.0                                                                           */
/* Author:         SAP AG                                                                     */
/* Creation-Date:  05.10.2005                                                                    */
/*                                                                                               */
/*                                                                               (c) SAP AG      */
/*-----------------------------------------------------------------------------------------------*/
--%>

<%@ taglib uri="/isa" prefix="isa" %>

function displayIpcPricingConds( connectionkey, docid, itmid ) {
    var urlforward = "<isa:webappsURL name='/b2b/preparepriceanalysisshow.do'/>?conKey=" + connectionkey + "&IPCdocID=" + docid + "&IPCitemID=" + itmid + "&mode=Display";
    var ipccondswinTest = window.open(urlforward, 'ipccondswin','width=650,height=420,menubar=no,locationbar=no,scrolling=auto,scrollbars=yes,resizable=yes');
    ipccondswinTest.focus();
    return true;
}
