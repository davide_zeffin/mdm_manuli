/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       orderTools.js                      */
/* Description:    useful functions for the order     */
/*                 handling                           */
/* some functions need the isaCore(ecom)base          */
/* functionality                                      */
/* Version:        1.0                                */
/* Author:         SAP                                */
/*----------------------------------------------------*/


/* create new header shipTo */
function newShipTo() {
    document.forms["order_positions"].newshipto.value="header";
    document.forms["order_positions"].submit();
    return true;
}

/* create new item shipTo */
function newShipToItem(itemKey) {
    if(itemKey != '') {
        document.forms["order_positions"].newshipto.value = itemKey;
        document.forms["order_positions"].submit();
        return true;
    }
    return false;
}

/* create soldTo in b2r basket - only used in b2r scenario */
function createSoldTo() {
    document.forms["order_positions"].processsoldto.value="createsoldto";
    document.forms["order_positions"].submit();
    return true;
}
        
/* show soldTo in b2r basket - only used in b2r scenario */    
function showSoldToInSameFrame() {
    document.forms["order_positions"].processsoldto.value="showsoldto";
    document.forms["order_positions"].submit();
    return true;
}

/* show cua details when clicked on cua icon in item details */
function itemcua(productId,replacedItem, documentType) {
    document.forms["order_positions"].cuaproductkey.value=productId;
    document.forms["order_positions"].cuareplaceditem.value=replacedItem;
    document.forms["order_positions"].cuadocumenttype.value=documentType;
    document.forms["order_positions"].submit();
    return true;
}

/* show configuration screen for configurable products */   
function itemconfig(itemId) {
    document.forms["order_positions"].configitemid.value=itemId;
    document.forms["order_positions"].submit();
    return true;
}
/* show grid entry screen for grid products */   
function griditemconfig(itemId) {
    document.forms["order_positions"].configtype.value="G";
    itemconfig(itemId);
}

/* Mutltiple Shipto functionality */
function multipleShipTo(itemId) {
    document.forms["order_positions"].configtype.value="G";
	document.forms["order_positions"].multipleshipto.value=itemId;
   	submit_refresh();
}

/* Mutltiple Requested delv date functionality */    	
function multipleReqDlvDate(itemId) {
    document.forms["order_positions"].configtype.value="G";
	document.forms["order_positions"].multiplereqdlvdate.value=itemId;
   	submit_refresh();
}

function showproductincatalog(productId) {
  document.forms["order_positions"].showproduct.value=productId;
  document.forms["order_positions"].submit();
  return true;
 }


function toggleAllItemDetails(id, idRow, number, startindex) {
	opera = false;
	if (navigator.userAgent.indexOf("Opera") != -1){
		opera = true;
	}
	if (document.all && !opera) {
    	var targetStyleDisplay = "none";
   		var openIconStyleDisplay = "inline";
   		var closerIconStyleDisplay = "none";
   		/* toggle icon in header line first */
   		openIconId = id + 'open';
   		openIcon = document.all(openIconId);
   		closerIconId = id + 'close';
   		closerIcon = document.all( closerIconId);
   		if(closerIcon.style.display == "none") {
       		targetStyleDisplay = "inline";
       		openIconStyleDisplay = "none";
       		openIcon.style.display = openIconStyleDisplay;
       		closerIconStyleDisplay = "inline";
       		closerIcon.style.display = closerIconStyleDisplay;
   		}
   		else {
       		openIcon.style.display = openIconStyleDisplay;
       		closerIcon.style.display = closerIconStyleDisplay;
   		}
   		/* adjust all rows */            
   		i = 0;
   		while(i < number) {
   		    rowNo = startindex + i;
       		openIconIdRow = idRow + rowNo + 'open'
       		openIconRow = document.all(openIconIdRow);
       		closerIconIdRow = idRow + rowNo + 'close';
       		closerIconRow = document.all( closerIconIdRow);
       		targetId = idRow + rowNo;
       		targetRow = document.all(targetId); 
       		if (targetRow != null) {
       		  targetRow.style.display = targetStyleDisplay;
       		}
       		if (openIconRow != null) { 
       		  openIconRow.style.display = openIconStyleDisplay;
       		}
       		if (closerIconRow != null) {
       		  closerIconRow.style.display = closerIconStyleDisplay;
       		}
       		i++;
   		}
   	}  
    else if (document.getElementById) {
    	// the world of W3C without Opera
		var targetStyleDisplay = "none";
		var openIconStyleDisplay = "inline";
		var closerIconStyleDisplay = "none";
		/* toggle icon in header line first */
		openIcon = document.getElementById(id + "open");
		closerIcon = document.getElementById(id + "close");
		target = document.getElementById(id);  			
		if(closerIcon.style.display == "none") {
			targetStyleDisplay = "table-row";
			openIconStyleDisplay = "none";
			openIcon.style.display = openIconStyleDisplay;
			closerIconStyleDisplay = "inline";
			closerIcon.style.display = closerIconStyleDisplay;
		}
		else {
			openIcon.style.display = openIconStyleDisplay;
			closerIcon.style.display = closerIconStyleDisplay;
		}
		/* adjust all rows */            
		i = 0;
		while(i < number) {
  		    rowNo = startindex + i;
			openIconRow = document.getElementById(idRow + rowNo + 'open');;
       		closerIconRow = document.getElementById(idRow + rowNo + 'close');
       		targetRow = document.getElementById(idRow + rowNo); 
       		if ( targetRow != null ) {      
       		  targetRow.style.display = targetStyleDisplay;
       		}
			if ( openIconRow != null ) {
       		  openIconRow.style.display = openIconStyleDisplay;
			}
			if ( closerIconRow != null ) {
       		  closerIconRow.style.display = closerIconStyleDisplay;
			}
       		i++;
   		}

    }
   	else {
    	// handle NS4.7 and Opera
        alert("ServerRequest");
    }                      
}

function toggle(idName) {
   isaEasyToggle(idName);
}

/* provides expand/collapse functionality for grid Product details */
function gridtoggle(idName,className) {

	toggle(idName);
		
	allTags = document.getElementsByTagName("TR");
	for (var i=0; i < allTags.length;i++){
   		if (allTags[i].className == className) {
   	   		if (allTags[i].style.display == "none" ) {
   		  		allTags[i].style.display = "inline";
   	   		}else{
   	      		allTags[i].style.display = "none";
   	   		}
   		}
   	}
}

function toggleremark() {
    openIcon = getElement("remark_open");
    closeIcon = getElement("remark_close");
    remarkarea = getElement("headerComment");

    if (openIcon.style.display == "") {
        openIcon.style.display = "none";
        closeIcon.style.display = "";
        remarkarea.rows=10;
    }
    else {
       openIcon.style.display = "";
       closeIcon.style.display = "none";
       remarkarea.rows=1;
    }
}

function openremark() {
    openIcon = getElement("remark_open");
    closeIcon = getElement("remark_close");
    remarkarea = getElement("headerComment");

    if(openIcon.style.display == "") {
        openIcon.style.display = "none";
        closeIcon.style.display = "";
        remarkarea.rows=10;
    }
}


function togglepayment() {
    isaToggle("payment_row", "payment_open", "payment_close");
}

function isBasketEmpty() {

	lineMax = 5;
	objDeleteBox = null;	
	emptyBasket = true;
	i = 1;
	
	while (( i <= lineMax ) && emptyBasket) {
		productString = "document.forms['order_positions'].elements['product[" + i + "]']";
		quantityString = "document.forms['order_positions'].elements['quantity[" + i + "]']";    	
		markedToDeleteString = "document.forms['order_positions'].elements['delete[" + i + "]'].checked";
		delString = "delete[" + i + "]";
		objDeleteBox = document.forms['order_positions'].elements[delString];
		if (objDeleteBox != null) {
			if ((eval(productString).value != "") && !(eval(markedToDeleteString)) && (eval(quantityString).value.replace(/0*/, "") != "")) {
				emptyBasket = false;
			}
		}
		else {
			if ((eval(productString).value != "") && (eval(quantityString).value.replace(/0*/, "") != "")) {
				emptyBasket = false;
			}		
		}
	i = i + 1;
	}
	
	return emptyBasket;
}

// scripts for buttons
var simulatePressed = false;

function submit_simulate(docType, processType) {
    // Multiple Invokation handling (note 905589)
    if ( ! miSetStatusUpdateRunning()) return false;

	if (simulatePressed) {
	  return true;
	}
	if (isBasketEmpty()) {
      return false ;
	}
	document.forms["order_positions"].simulatepressed.value=docType;
	document.forms["order_positions"].processtype.value=processType;
	document.forms["order_positions"].submit();
	simulatePressed = true;
    miSetStatusUpdateRunning(true); // note 908052
	return true;
}

function submit_newpos() {
    // Multiple Invokation handling (note 905589)
	if ( ! miSetStatusUpdateRunning(true)) return false;
	document.forms['order_positions'].elements['newpos'].value=
	document.getElementById('newposcount').options[document.getElementById('newposcount').selectedIndex].value;
	document.forms['order_positions'].submit();
	return true;
}

 function submit_refresh() {
    // Multiple Invokation handling (note 905589)
    if ( ! miSetStatusUpdateRunning(true)) return false;
 	if (document.getElementById('newposcount') != null) {
	    document.forms['order_positions'].elements['newpos'].value=
		document.getElementById('newposcount').options[document.getElementById('newposcount').selectedIndex].value;
	}	
	document.forms['order_positions'].elements['refresh'].value="true";
	disableEmptyInputFields();
	document.forms['order_positions'].submit();
	return true;
}

/* Processes the requested deliverydate on item level */
/* ----Begin---- */
var updTypeAll = 'all';
var updTypeEqToHeader = 'eq';
var updTypeOnlyNewPos = 'new';
var headerReqDelDateOld = '';
var sel = updTypeOnlyNewPos;
var selwdw = null;

function checkReqDateChange() {
    
    headerReqDelDate = document.forms['order_positions'].elements['headerReqDeliveryDate'].value;

    if (headerReqDelDate != headerReqDelDateOld && (selwdw == null || selwdw.closed)) {
       if (!isBasketEmpty()) { /* show selection screen only if there are old psoitions */
           checkRunning = true;
           var url = rewriteURL("b2b/reqdeldate.jsp");
           selwdw = window.open(url, 'selection', 'width=400,height=150,menubar=no,locationbar=no,scrolling=auto,resizable=yes,toolbar=no,dependent=yes,hotkeys=no');
           selwdw.moveBy(200,200);
           selwdw.focus(); 
       }
       else { /* if there are only new positions uopdate them implicitly */
           updItemReqDeliveryDate(updTypeOnlyNewPos);
       }
    }
    
    return true;
}

function selReqDelSelectWdwOpen() {
    if (selwdw != null) {
        isClosed = selwdw.closed;
        if (!isClosed) {
            selwdw.focus();
       }
    }
}

function updItemReqDeliveryDate(updType) {
    if (selwdw != null ) {
    selwdw.close();
    }
    selwdw = null;

    i = 1;
    headerReqDelDate = document.forms['order_positions'].elements['headerReqDeliveryDate'].value;
    
    while (i <= linecount) {
        productString = "document.forms['order_positions'].elements['product[" + i + "]']";
        reqDelString = "document.forms['order_positions'].elements['reqdeliverydate[" + i + "]']";
        reqDel = eval(reqDelString);
        
        if (reqDel) { 
            if ( (updType == updTypeAll ||
                 (updType == updTypeOnlyNewPos && eval(productString).value == "") || 
                 (updType == updTypeEqToHeader && reqDel.value == headerReqDelDateOld)) &&
                 (reqDel.type == "text" && !reqDel.disabled && !reqDel.readOnly) ) {

                reqDel.value = headerReqDelDate;
            }
        }
        
        i = i + 1;
    }
    
    headerReqDelDateOld = document.forms['order_positions'].elements['headerReqDeliveryDate'].value;
    
    return true
}
/* Processes the requested deliverydate on item level */
/* ----End---- */
    
    /* Processes the Cancel date on item level */
        var headerCancelDateOld = '';
        
        function checkCancelDateChange() {
        headerCancelDate = document.forms['order_positions'].elements['headerlatestdlvdate'].value;

        if (headerCancelDate != headerCancelDateOld && (selwdw == null || selwdw.closed)) {
           checkRunning = true;
           var url = rewriteURL("b2b/canceldate.jsp");
           selwdw = window.open(url, 'selection', 'width=400,height=150,menubar=no,locationbar=no,scrolling=auto,resizable=yes,toolbar=no,dependent=yes,hotkeys=no');
           selwdw.moveBy(200,200);
           selwdw.focus();
        }

        return true;
    }
    
	function updItemCancelDate(updType) {

        i = 1;
        headerCancelDate = document.forms['order_positions'].elements['headerlatestdlvdate'].value;

        while (i <= linecount) {
            productString = "document.forms['order_positions'].elements['product[" + i + "]']";
            cancelDateString = "document.forms['order_positions'].elements['latestdlvdate[" + i + "]']";
            cancelDate = eval(cancelDateString);

            if ( (updType == updTypeAll ||
                 (updType == updTypeOnlyNewPos && eval(productString).value == "") ||
                 (updType == updTypeEqToHeader && reqDel.value == headerCancelDateOld)) &&
                 (cancelDate.type == "text" && !cancelDate.disabled && !cancelDate.readOnly)
                ) {
                cancelDate.value = headerCancelDate;
            }

            i = i + 1;
        }

        headerCancelDateOld = document.forms['order_positions'].elements['headerlatestdlvdate'].value;

        return true
    }
    /* Processes the cancel date on item level */
    
  	function toggleHeader() {
  	    if (navigator.userAgent.indexOf("MSIE") != -1) {
  	    // the world of the IEs
            //alert("IE - Modus");
            icon = document.all("addheadericon");
    		target = document.all("addheader");
    		if (target.style.display == "none") {
    			target.style.display = "block";
                iconpath = icon.src.substring(0, icon.src.lastIndexOf("open.gif"));
    			icon.src=iconpath+"close.gif";
    		}else {
    			target.style.display = "none";
                iconpath = icon.src.substring(0, icon.src.lastIndexOf("close.gif"));
    			icon.src=iconpath+"open.gif";
    		}
    	} 
    	else {
    	  // the world of W3C
    	    //alert("W3C - Modus");
            icon = document.getElementById("addheadericon");
    		target = document.getElementById("addheader");
    		if (target.style.display == "none") {
    			target.style.display = "block";
                iconpath = icon.src.substring(0, icon.src.lastIndexOf("open.gif"));
    			icon.src=iconpath+"close.gif";
    		}else {
    			target.style.display = "none";
                iconpath = icon.src.substring(0, icon.src.lastIndexOf("close.gif"));
    			icon.src=iconpath+"open.gif";
    		}
    	} 
  	} 
  	
  	function deletecontract(line) {     
          document.forms["order_positions"].elements["contractRef["+line+"]"].value = "";
          document.forms["order_positions"].elements["detailContractKey["+line+"]"].value = "";
          document.forms["order_positions"].elements["detailContractItemKey["+line+"]"].value = ""; 
          submit_refresh();         
     } 
     
    /* ----End---- */