/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       new.js                             */
/* Description:    launches new order, templates etc. */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    03.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/
// needs frames.js. Please include in jsp.
// loads new order, templates etc.
function load_new_doc(msg) {
  var choice;

  for (i=0; i<document.newOrder.docType.length; i++) {
    if (document.newOrder.docType.options[i].selected == true) {
      choice = document.newOrder.docType.options[i].value;
    }
  }

  switch (choice) {
    case 'no_choice':
        break;

    case 'not_possible':
        alert(msg);
    	break;

    case 'order':
        location.href=rewriteURL("/b2b/createbasket.do");
    	break;

    case 'order_with_template':
        form_input().location.href=rewriteURL("/b2b/showpredecessordocs.do")+"?source=ordertemplate&target=order";
    	break;

    case 'order_with_quotation':
        form_input().location.href=rewriteURL("/b2b/showpredecessordocs.do")+"?source=quotation&target=order";
    	break;

    case 'order_quotation':
        form_input().location.href=rewriteURL("/b2b/showpredecessorquots.do");
    	break;

    case 'offer':
        location.href=rewriteURL("/b2b/createquotation.do");
    	break;

    case 'offer_with_template':
        form_input().location.href=rewriteURL("/b2b/showpredecessordocs.do")+"?source=ordertemplate&target=quotation";
    	break;

    case 'template':
        form_input().location.href=rewriteURL("/b2b/createordertemplate.do");
    	break;

    default:
        form_input().location.href=rewriteURL("/b2b/createnegotiatedcontract.do"+"?ncontracttransactiontype="+choice);
        break;
  }
}

// check if there is an editable document
// if there is an editable doc: display an alert box and return false
// else return true
// this function works especially for the onsubmit event
function editableDocumentAlert(msg) {
  if (editableDoc) {
    alert(msg);
    return false;
  }
  else return true;
}
