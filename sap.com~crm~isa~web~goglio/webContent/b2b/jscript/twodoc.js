/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       new.js                             */
/* Description:    launches new order, templates etc. */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    03.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/

// loads new order, templates etc.
function load_new_doc() {
  var choice, docLoc, formInputLoc, historyLoc;

  for(i=0; i<document.newOrder.docType.length; i++)
    if(document.newOrder.docType.options[i].selected == true)
      choice = document.newOrder.docType.options[i].value;

  switch (choice) {
    case 'no_choice':           alert('Bitte w�hlen Sie einen Dokumenttyp.');
                                flag = false; break;
    case 'not_possible':        docPopUp();
                                flag = false; break;
    case 'order':               docLoc = "onedocnav.jsp";
                                formInputLoc = "createbasket.do";
//                                formInputLoc = "empty.jsp";
                                historyLoc = "history_no_link.jsp";
                                flag = true; break;
    case 'order_with_template': //docLoc = "new_order_grey_with_order-template.html";
                                //docLoc = "new_order_grey.html";
                                //formInputLoc = "frameset_order_with_template.html";
                                //historyLoc = "history.html";
                                //flag = true; break;
                                alert("noch nicht realisiert"); flag = false; break;
    case 'offer':               alert("noch nicht realisiert"); flag = false; break;
    case 'offer_with_template': alert("noch nicht realisiert"); flag = false; break;
    case 'template':            alert("noch nicht realisiert"); flag = false; break;
    default: flag = false; break;
  }

  historyLoc = "history.html";

  if (flag) {
    if (formInputLoc != null) parent.parent.parent.work_history.location.href=formInputLoc;
    //if (historyLoc != null) parent.parent._history.location.href = historyLoc;
    if (docLoc != null) parent.documents.location.href = docLoc;
    return false; // normally return true
  }
  else
    return false;
}


function docPopUp() {
  alert('Sie k�nnen immer nur ein Dokument bearbeiten.\n\nSie haben bereits ein Dokument im Bearbeitungsmodus.\n\nBitte schlie�en Sie das Dokument, wenn Sie das aktuelle Dokument bearbeiten wollen.');
}
