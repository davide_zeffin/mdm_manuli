<%--
/*-----------------------------------------------------------------------------------------------*/
/* Javascript for the "SAP B2B Project"                                                          */
/*                                                                                               */
/* Document:       GSsendRequestAuction.jsp                                                      */
/* Description:    This document contains a function which is used to send a auction             */
/*                 specific request                                                              */ 
/* Version:        1.0                                                                           */
/* Author:         SAP AG                                                                        */
/* Creation-Date:  26.07.2004                                                                    */
/*                                                                                               */
/*                                                                               (c) SAP AG      */
/*-----------------------------------------------------------------------------------------------*/
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="java.lang.reflect.Method" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIFactory" %>
<%@ page import="com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI" %>

   function sendRequestAuction() {
     var request = "<isa:webappsURL name="/buyer/showauctions.do"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=<%--=ui.getScreenName()--%>&";
     request = request + "<%=GenericSearchUIData.RC_REQUESTSTARTED%>=true&";
     <%-- //request = request + "<%=GenericSearchUIData.RC_DATEFORMAT%>=<%-- =ui.getDateFormat()-%>&";
     //request = request + "<%=GenericSearchUIData.RC_NUMBERFORMAT%>=<%--=WebUtil.encodeURL(ui.getNumberFormat())-%>&"; 
     --%>
     for(var i = 0; i < scnElms.length; i ++) {
            var scnElmx = scnElms[i];
            if (! scnElmx) break;
    
            if (scnElmx.isVisible == true) {
                    <%-- // Add attribute and its value to the request, but only if attirubte has a NAME --%>
                    if (scnElmx.isRange == false) {
                            if ( document.getElementById(scnElmx.s_id) != null  &&
                                    document.getElementById(scnElmx.s_id).name != 'null') {
                                    request = request + ((document.getElementById(scnElmx.s_id).name) + "=" + (document.getElementById(scnElmx.s_id).value) + "&");
                            }
                    } else {
                            <%-- // For ranges add _LOW and _HIGH to the HTTP request parameter name --%>
                            var idLow = scnElmx.s_id  + "_low";
                            var idHigh = scnElmx.s_id + "_high";
                            if ( document.getElementById(idLow).name != 'null') {
                                    request = request + ((document.getElementById(idLow).name) + "=" + (document.getElementById(idLow).value) + "&");
                            }
                            if ( document.getElementById(idHigh).name != 'null') {
                                    request = request + ((document.getElementById(idHigh).name) + "=" + (document.getElementById(idHigh).value) + "&");
                            }
                    }
            }
    } <%-- end for --%>
     //alert("Request: " + request);
     parent.work_history.form_input.location.href = request;
   }

