/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       new.js                             */
/* Description:    launches new order, templates etc. */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    03.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/

// loads new order, templates etc.
function load_new_doc(msg) {
  var choice;

	alert(msg);
  for(i=0; i<document.newOrder.docType.length; i++)
    if(document.newOrder.docType.options[i].selected == true)
      choice = document.newOrder.docType.options[i].value;

  switch (choice) {
    case 'no_choice':           // alert('Bitte w�hlen Sie einen Dokumenttyp.');
                                break;
    case 'not_possible':        alert(msg);
                                break;
    case 'order':               parent.parent.parent.work_history.location.href=rewriteUrl("/b2b/createbasket.do");
                                break;
    case 'order_with_template': alert("noch nicht realisiert"); 
    														break;
    case 'offer':               alert("noch nicht realisiert"); 
    														break;
    case 'offer_with_template': alert("noch nicht realisiert"); 
    														break;
    case 'template':            alert("noch nicht realisiert"); 
    														break;
    default: 
    break;
  }
}
