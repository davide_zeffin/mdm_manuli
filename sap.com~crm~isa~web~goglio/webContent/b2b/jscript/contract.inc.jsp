/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP
    Created:      27.09.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/09/27 $
*****************************************************************************/

 <%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
 
 
 <%@ taglib uri="/isa" prefix="isa" %>
 
       function releaseItem(itemKey) {
          document.getElementById("contractitemsform").<%=ContractReadAction.CONTRACT_RELEASE_ITEM%>.value = itemKey;
          document.getElementById("contractitemsform").elements["requested["+itemKey+"]"].checked = true;          
	  document.getElementById("contractitemsform").<%=ContractReadAction.CONTRACT_RELEASE_COMMAND%>.value = "<%=ContractReadAction.CONTRACT_RELEASE%>";
          document.getElementById("contractitemsform").target = "form_input";
          document.getElementById("contractitemsform").submit();
       } 
       function releaseContract() {
          document.getElementById("contractitemsform").<%=ContractReadAction.CONTRACT_RELEASE_COMMAND%>.value = "<%=ContractReadAction.CONTRACT_RELEASE%>";
          document.getElementById("contractitemsform").target = "form_input";
          document.getElementById("contractitemsform").submit();
       }
       function closeContract() {
    	  location.href = '<isa:webappsURL name="b2b/contractclose.do"></isa:webappsURL>';
       }
       function expandItem(itemKey) {
     	  document.getElementById("contractitemsform").<%=ContractReadAction.CONTRACT_EXPAND_ITEM%>.value = itemKey;
          document.getElementById("contractitemsform").submit();
       }
       function collapseItem(itemKey) {
    	  document.getElementById("contractitemsform").<%=ContractReadAction.CONTRACT_COLLAPSE_ITEM%>.value = itemKey;
          document.getElementById("contractitemsform").submit();
      }       
      function setPageCommand(command) {
	  document.getElementById("contractitemsform").<%=ContractReadAction.CONTRACT_PAGE_COMMAND%>.value = command;
          document.getElementById("contractitemsform").submit();
      }
      function setPageSize(pageSize) {
    	  document.getElementById("contractitemsform").<%=ContractReadAction.CONTRACT_PAGE_SIZE%>.value = pageSize;
    	  document.getElementById("contractitemsform").submit();
      }  