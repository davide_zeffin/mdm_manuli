/*****************************************************************************
    File:         campaign_enrollment.js
    Copyright (c) 2006, SAP AG Europe GmbH, Germany, All rights reserved.
    Author:       SAG AG
    Created:      11.12.2006
    Version:      1.0 (

    $Revision: #1 $
    $Date: 2006/12/11 $
*****************************************************************************/


  function enrollment_submit() {
      document.forms["enrollmentForm"].target="form_input";
      document.forms["enrollmentForm"].elements["forward"].value="updateEnrollment";
      document.forms["enrollmentForm"].submit();
      return true;
  }

  function setPage(newPage) {
      document.forms["enrollmentForm"].target="form_input";
      document.forms["enrollmentForm"].elements["forward"].value="displaycampaign";
      document.forms["enrollmentForm"].elements["campaignpage"].value=newPage;
      document.forms["enrollmentForm"].submit();
      return true;
  }
  
  function enrollAllChanged(maxRows) {
      var isChecked = document.getElementById("enrollall").checked;
      var newValue = "";
      if (isChecked == true) {
           newValue = "on";
      }
      var i;
      for (i=0; i<maxRows; i++) {
          document.forms["enrollmentForm"].elements["isEnrolled["+i+"]"].checked = isChecked;
          document.forms["enrollmentForm"].elements["isEnrolled["+i+"]"].value = newValue;
      }
      document.getElementById("enrollall").value = newValue;
      enableSubmit();
  }

  function enableSubmit() {
      document.getElementById("enrollmButtonStyle").className = "button";
      document.getElementById("enrollButton").href = "javascript:enrollment_submit()";
  }

  // sets the enrollment flag for all bps
  function setEnrollmentFlagForAllBPs(flag) {
      document.forms["enrollmentForm"].target="form_input";
      var forward = "deselectAllBPs";
      if (flag == "true") { 
          forward = "selectAllBPs";
      }
      document.forms["enrollmentForm"].elements["forward"].value=forward;
      document.forms["enrollmentForm"].submit();
      return true;
  }

  