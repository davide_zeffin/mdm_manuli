<%--
/*-----------------------------------------------------------------------------------------------*/
/* Javascript for the "SAP B2B Project"                                                          */
/*                                                                                               */
/* Document:       billingbuttons.jsp                                                            */
/* Description:    This document contains functions which are used in the billing status detail  */
/* Version:        1.0                                                                           */
/* Author:         SAP AG                                                                        */
/* Creation-Date:  26.07.2004                                                                    */
/*                                                                                               */
/*                                                                               (c) SAP AG      */
/*-----------------------------------------------------------------------------------------------*/
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstants" %>
<%@ page import="com.sap.isa.core.UserSessionData" %>
<%@ page import="com.sap.isa.core.SessionConst" %>

    function closeDocument() {
         
        <% UserSessionData userSessionData = (UserSessionData) pageContext.getSession().getAttribute(SessionConst.USER_DATA); 
           if (ActionConstants.APPLAREA_SEPBILL.equals(userSessionData.getAttribute(ActionConstants.SC_APPLAREA))) { %>
            var myTop = window;
        <% } else { %>
            var myTop = form_input();
        <% } %>
        myTop.location.href = '<isa:webappsURL name="b2b/statusdocumentclose.do?"/>';

    }

