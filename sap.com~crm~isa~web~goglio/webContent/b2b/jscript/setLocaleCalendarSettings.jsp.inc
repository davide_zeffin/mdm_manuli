<%--
*********************************************************************
    File:         setLocaleCalendarSettings.jsp
    Copyright (c) 2003, SAP Retail
    Author:
    Created:      6.11.2003
    Version:      1.0

    $Revision: #1 $  
    $Date: 2003/11/06 $

*   This include handles the locale calendar settings for the
*   locale specific translations in calendar.jsp.
*   It should be included in all files using the calendar function
*   instead of implementing the code directly in the JSPs
**********************************************************************
--%>

<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>

<script type="text/javascript">
    <% if(response.getCharacterEncoding() != null) { %>
	 var contentType = "<%=response.getCharacterEncoding()%>";
    <%  }  %>
   
    //Locale specific settings for calendar...
    function setLocaleSpecificData(){
        var monthkeys = new Array(
            '<isa:translate key="eAuctions.calendar.jan"/>',
            '<isa:translate key="eAuctions.calendar.feb"/>',
            '<isa:translate key="eAuctions.calendar.mar"/>',
            '<isa:translate key="eAuctions.calendar.apr"/>',
            '<isa:translate key="eAuctions.calendar.may"/>',
            '<isa:translate key="eAuctions.calendar.jun"/>',
            '<isa:translate key="eAuctions.calendar.jul"/>',
            '<isa:translate key="eAuctions.calendar.aug"/>',
            '<isa:translate key="eAuctions.calendar.sep"/>',
            '<isa:translate key="eAuctions.calendar.oct"/>',
            '<isa:translate key="eAuctions.calendar.nov"/>',
            '<isa:translate key="eAuctions.calendar.dec"/>'
        );
        var weekkeys = new Array(
            '<isa:translate key="eAuctions.calendar.sunday"/>',
            '<isa:translate key="eAuctions.calendar.monday"/>',
            '<isa:translate key="eAuctions.calendar.tuesday"/>',
            '<isa:translate key="eAuctions.calendar.wednesday"/>',
            '<isa:translate key="eAuctions.calendar.thursday"/>',
            '<isa:translate key="eAuctions.calendar.friday"/>',
            '<isa:translate key="eAuctions.calendar.saturday"/>'
        );
        var todaybtn="<isa:translate key="eAuctions.calendar.today"/>";
        var okbtn="<isa:translate key="eAuctions.calendar.okbtn"/>";
        var cancelbtn="<isa:translate key="eAuctions.calendar.cancelbtn"/>";
        setLocaleSpecificKeys(monthkeys,weekkeys,todaybtn,okbtn,cancelbtn);
    }
</script>