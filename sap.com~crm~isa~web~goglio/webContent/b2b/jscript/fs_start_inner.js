/*****************************************************************************
    File:         fs_start_inner.js
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      Januar 2008
    Version:      1.0

    $Date: 2008/01/23 $
*****************************************************************************/

        var headerPageloaded = false;   

        var isFramesDefault = true;
        var lastDirection;  // only resize if direction changes

    	function resize(direction, frameObj) {
                //alert("Resize: " + direction + " FrObj: " + frameObj);
	      		if (direction == "min" && direction != lastDirection  &&  isFramesDefault == false) {
                   //setTimeout('document.getElementById("secondFS").cols = "90%,15,*";', 20); 
                   setTimeout('document.getElementById("secondFS").cols = "85%,15,*";', 40); 
                   setTimeout('document.getElementById("secondFS").cols = "80%,15,*";', 60); 
                   setTimeout('document.getElementById("secondFS").cols = "75%,15,*";', 80); 
                   setTimeout('document.getElementById("secondFS").cols = "70%,15,*";', 100); 
                   setTimeout('document.getElementById("secondFS").cols = "65%,15,*";', 120); 
                   setTimeout('document.getElementById("secondFS").cols = "60%,15,*";', 140); 
                   setTimeout('document.getElementById("secondFS").cols = "55%,15,*";', 160); 
                   setTimeout('document.getElementById("secondFS").cols = "50%,15,*";', 180); 
                   setTimeout('document.getElementById("secondFS").cols = "45%,15,*";', 200); 
                   setTimeout('document.getElementById("secondFS").cols = "40%,15,*";', 230); 
                   setTimeout('document.getElementById("secondFS").cols = "35%,15,*";', 270); 
                   setTimeout('document.getElementById("secondFS").cols = "30%,15,*";', 320); 
                   setTimeout('document.getElementById("secondFS").cols = "25%,15,*";', 380); // default navigator
                   isFramesDefault = true;
	    	  	} else if (direction == "max" && direction != lastDirection) {
                    setTimeout('document.getElementById("secondFS").cols = "35%,15,*";', 20);
                    setTimeout('document.getElementById("secondFS").cols = "40%,15,*";', 40); 
                    setTimeout('document.getElementById("secondFS").cols = "45%,15,*";', 60); 
                    setTimeout('document.getElementById("secondFS").cols = "50%,15,*";', 80); 
                    setTimeout('document.getElementById("secondFS").cols = "55%,15,*";', 100); 
                    setTimeout('document.getElementById("secondFS").cols = "60%,15,*";', 120); 
                    setTimeout('document.getElementById("secondFS").cols = "65%,15,*";', 140); 
                    setTimeout('document.getElementById("secondFS").cols = "70%,15,*";', 160); 
                    setTimeout('document.getElementById("secondFS").cols = "75%,15,*";', 180); 
                    setTimeout('document.getElementById("secondFS").cols = "80%,15,*";', 200); 
                    setTimeout('document.getElementById("secondFS").cols = "85%,15,*";', 230); 
                    setTimeout('document.getElementById("secondFS").cols = "*,15,95";', 300); // wide navigator
                    isFramesDefault = false;
        	    } else if (direction == "open" && direction != lastDirection) {
                    document.getElementById("secondFS").cols = "5%,15,*";  
                    setTimeout('document.getElementById("secondFS").cols = "10%,15,*";', 20); 
                    setTimeout('document.getElementById("secondFS").cols = "15%,15,*";', 40); 
                    setTimeout('document.getElementById("secondFS").cols = "20%,15,*";', 70); 
                    setTimeout('document.getElementById("secondFS").cols = "25%,15,*";', 110); // default navigator
                    isFramesDefault = true;
        	    } else if (direction == "close" && direction != lastDirection) {
        	        document.getElementById("secondFS").cols = "20%,15,*"; 
                    setTimeout('document.getElementById("secondFS").cols = "15%,15,*";', 20); 
                    setTimeout('document.getElementById("secondFS").cols = "10%,15,*";', 40); 
                    setTimeout('document.getElementById("secondFS").cols = "5%,15,*";', 70); 
                    setTimeout('document.getElementById("secondFS").cols = "0,15,*";', 110); // closed navigator
                    isFramesDefault = false;
        	    }
                lastDirection = direction;  // Store last direction command
                // Adjust the arrows accordingly
                adjustCloserArrows('Navigator',direction);
	    	}

            function adjustCloserArrows(location, direction) {
                var org_cont = window.frames.organizer_content;
                var clo_org = window.frames.closer_organizer;
                if (location == 'Navigator'  &&  org_cont.isExpandedResultlist) {
                    if (direction == 'min') {
                        org_cont.expandedResultlistHandler('gsbuttoncollapse', false);
                        clo_org.chgArrow('left', '1');
                        clo_org.chgArrow('right', '2');
                    }
                    if (direction == 'max') {
                        org_cont.expandedResultlistHandler('gsbuttonexpand', false);
                        clo_org.hideArrow('1');;
                        clo_org.chgArrow('left', '2');
                    }
                    if (direction == 'open') {
                        clo_org.chgArrow('left', '1');
                        clo_org.chgArrow('right', '2');
                    }
                    if (direction == 'close') {
                        clo_org.hideArrow('2');
                        clo_org.chgArrow('right', '1');
                    }
                }
            }
            
            
        // It is essential to have the Header page loaged BEFORE loading the work area !! (note 936980)
        function loadPage() {
            if (headerPageloaded == false) {
                // wait for header page being loaded
                return
            }
            clearInterval(workHistoryInterval);
            work_history.location.href="<isa:webappsURL name="/b2b/fs_startworkarea.jsp"/>";
        }
        
        function resizeHist(direction, frameObj) {
	        if (direction == "min") {
                 setTimeout('document.getElementById("secondFS").cols = "*,15,60";', 20);
                 setTimeout('document.getElementById("secondFS").cols = "*,15,30";', 40);
                 setTimeout('document.getElementById("secondFS").cols = "*,15, 0";', 70);
            }
            else {
                 setTimeout('document.getElementById("secondFS").cols = "*,15,30";', 20);
                 setTimeout('document.getElementById("secondFS").cols = "*,15,60";', 40);
                 setTimeout('document.getElementById("secondFS").cols = "*,15,95";', 70);
            }
        }

