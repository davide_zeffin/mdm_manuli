/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* for open/closer                                    */
/*                                                    */
/*                                                    */
/*----------------------------------------------------*/

// changes arrow-image
function chgArrow(dir, objNo) {
	if (dir == 'left') {
		document.getElementById( ("rightArrow"+objNo) ).style.display = 'none';
		document.getElementById( ("leftArrow"+objNo) ).style.display = 'block';    
	}
	else {
		document.getElementById(("leftArrow"+objNo)).style.display = 'none';
		document.getElementById(("rightArrow"+objNo)).style.display = 'block';
	}
}

function chgBottomArrow(dir) {
    if (dir == '0') {
    	document.getElementById("BottomArrow1").style.display = 'none';
    	document.getElementById("BottomArrow0").style.display = 'block';
    }
    else {
    	document.getElementById("BottomArrow0").style.display = 'none';
    	document.getElementById("BottomArrow1").style.display = 'block';
    }
}

// hide arrow object by given object number
function hideArrow(objNo) {
    document.getElementById( ("rightArrow"+objNo) ).style.display = 'none';
    document.getElementById( ("leftArrow"+objNo) ).style.display = 'none';    
}

function getImageWithName(imgName) {
 var imgWithName = null;
 for(var i = 0; i < document.images.length; i++) {
    if (imgName == document.images[i].name) {
       imgWithName = document.images[i];
    }
 }
 return imgWithName;
}

// highlights arrow-image (imgName is like "arrow_right1")
function overArrow(dir, imgName) {
 var imgWithName = getImageWithName(imgName);
 if (dir == 'left') {
        imgWithName.src = img0_h;
 } else {
        imgWithName.src = img1_h;
 }
}

function overBottomArrow(dir) {
  if (dir == '0')
    document.images["arrow_0"].src = source0_h;
  else
    document.images["arrow_1"].src = source1_h;
}

// resets arrow-image
function outArrow(dir, imgName) {
 var imgWithName = getImageWithName(imgName);
 if (dir == 'left') {
        imgWithName.src = img0;
 } else {
        imgWithName.src = img1;
 }
}

function outBottomArrow(dir) {
  if (dir == '0')
    document.images["arrow_0"].src = source0;
  else
    document.images["arrow_1"].src = source1;
}

function writeArrows(src0, src1, FirstOnTop, spacer) {
  if (document.all) {
    document.writeln("<div id=\"BottomArrow0\">");
    document.writeln("<a href=\"#\" onmouseover=\"overBottomArrow('0');\" onmouseout=\"outBottomArrow('0');\" onclick=\"parent.resize('min'); chgBottomArrow('1'); return false;\"><img src=\""+src0+"\" width=\"13\" height=\"7\" border=\"0\" alt=\"hide Details\" name=\"arrow_0\" /></a>");
    document.writeln("</div>");
    document.writeln("<div id=\"BottomArrow1\">");
    document.writeln("<a href=\"#\" onmouseover=\"overBottomArrow('1');\" onmouseout=\"outBottomArrow('1');\" onclick=\"parent.resize('max'); chgBottomArrow('0'); return false;\"><img src=\""+src1+"\" width=\"13\" height=\"7\" border=\"0\" alt=\"show Details\" name=\"arrow_1\" /></a>");
    document.writeln("</div>");
    chgBottomArrow(FirstOnTop);
  }
  else
    document.writeln("<img src=\"" + spacer + "\" height=\"7\" width=\"1\" border=\"0\">");
}