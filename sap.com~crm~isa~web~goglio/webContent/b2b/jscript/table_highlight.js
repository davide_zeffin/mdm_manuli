/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       table_highlight.js                 */
/* Description:    highlights a single table row      */
/* Version:        1.1                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    17.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/


// global variables
var oldRow;
var oldRowBgColor;

// change actual and old table_row
function changeRowStyle(id) {
  if (!document.layers) {
    if (oldRow != null)
      for (i=0; i<oldRow.length; i++) {
        oldRow[i].style.backgroundColor = oldRowBgColor;
        oldRow[i].style.borderTop = "1px solid #D6DFC6";
        oldRow[i].style.borderBottom = "1px solid #D6DFC6";
      }
  
    var actualRow = document.getElementById(id).getElementsByTagName("TD");
  
    for (i=0; i<actualRow.length; i++) {
      actualRow[i].style.backgroundColor = "#ffffff";
      actualRow[i].style.borderTop = "1px solid #ADC76B";
      actualRow[i].style.borderBottom = "1px solid #ADC76B";
    }
  
    if (document.getElementById(id).className == "odd")
      oldRowBgColor = "#C6D7A5";
    else
      oldRowBgColor = "#C6D7A5";
  
    oldRow = actualRow;
  }
}
