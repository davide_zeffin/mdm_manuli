/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       table_highlight_addon.js           */
/* Description:    highlights additional table rows   */
/* Version:        1.2                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  09.03.2001                         */
/* Last-Update:    14.12.2001 (Ralf Witt)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/


// global variables
var oldRow;
var oldRowBgColor;
var oldDlvRow;
var oldDlvRowBgColor;
var oldShiptoRow;
var oldShiptoRowBgColor;

// change actual and old table_row
function changeRowStyle(id) {
  if (!document.layers) {
    if (oldRow != null)
      for (i=0; i<oldRow.length; i++) {
        oldRow[i].style.backgroundColor = oldRowBgColor;
        oldRow[i].style.borderTop = "1px solid #D6DFC6";
        oldRow[i].style.borderBottom = "1px solid #D6DFC6";
      }
  
    var actualRow = document.getElementById(id).getElementsByTagName("TD");
  
    for (i=0; i<actualRow.length; i++) {
      actualRow[i].style.backgroundColor = "#ffffff";
      actualRow[i].style.borderTop = "1px solid #ADC76B";
      actualRow[i].style.borderBottom = "1px solid #ADC76B";
    }
  
    if (document.getElementById(id).className == "odd")
      oldRowBgColor = "#C6D7A5";
    else
      oldRowBgColor = "#C6D7A5";
  
    oldRow = actualRow;
//  Depending Deliverķes
    var idDlv = "dlv" + id;
    if (oldDlvRow != null)
      for (i=0; i<oldDlvRow.length; i++) {
        oldDlvRow[i].style.backgroundColor = oldDlvRowBgColor;
        oldDlvRow[i].style.borderTop = "1px solid #D6DFC6";
        oldDlvRow[i].style.borderBottom = "1px solid #D6DFC6";
      }
  
    if (document.getElementById(idDlv) != null) {

        var actualRow = document.getElementById(idDlv).getElementsByTagName("TD");
  
        for (i=0; i<actualRow.length; i++) {
          actualRow[i].style.backgroundColor = "#E0E0E0";
          actualRow[i].style.borderTop = "1px solid #ADC76B";
          actualRow[i].style.borderBottom = "1px solid #ADC76B";
        }
  
        if (document.getElementById(id).className == "odd")
          oldDlvRowBgColor = "#C6D7A5";
        else
          oldDlvRowBgColor = "#C6D7A5";
  
        oldDlvRow = actualRow;
    }
//  Depending ShipTo partner
    var idShipto = "shipto" + id;
    if (oldShiptoRow != null)
      for (i=0; i<oldShiptoRow.length; i++) {
        oldShiptoRow[i].style.backgroundColor = oldShiptoRowBgColor;
        oldShiptoRow[i].style.borderTop = "1px solid #D6DFC6";
        oldShiptoRow[i].style.borderBottom = "1px solid #D6DFC6";
      }
  
    if (document.getElementById(idShipto) != null) {

        var actualRow = document.getElementById(idShipto).getElementsByTagName("TD");
  
        for (i=0; i<actualRow.length; i++) {
          actualRow[i].style.backgroundColor = "#E0E0E0";
          actualRow[i].style.borderTop = "1px solid #ADC76B";
          actualRow[i].style.borderBottom = "1px solid #ADC76B";
        }
  
        if (document.getElementById(id).className == "odd")
          oldShiptoRowBgColor = "#C6D7A5";
        else
          oldShiptoRowBgColor = "#C6D7A5";
  
        oldShiptoRow = actualRow;
   }

  }
}
