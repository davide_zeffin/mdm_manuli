/*------------------------------------------------------*/
/*    File:         negotiatedContracts.js              */
/*    Copyright (c) 2004, SAP AG, All rights reserved.  */
/*    Author:       SAP AG                              */
/*    Created:      February 2004                       */
/*    Version:      1.0                                 */
/*                                                      */
/*    $Revision: #2 $                                   */
/*    $Date: 2003/10/15 $                               */
/*------------------------------------------------------*/
          
    // function for the onload()-event 
    function loadPage() {
       setLocaleSpecificData();
    }

    function update_negotiatedcontract() {
		document.forms['negotiatedcontract_positions'].elements['cancel'].value="";
		document.forms['negotiatedcontract_positions'].elements['refresh'].value="true";
		document.forms['negotiatedcontract_positions'].elements['newpos'].value="";
		document.forms['negotiatedcontract_positions'].elements['save'].value="";
		document.forms['negotiatedcontract_positions'].elements['send'].value="";
		document.forms['negotiatedcontract_positions'].submit();
		return true;
	}
	
	function save_negotiatedcontract() {
		document.forms['negotiatedcontract_positions'].elements['cancel'].value="";
		document.forms['negotiatedcontract_positions'].elements['save'].value="true";
		document.forms['negotiatedcontract_positions'].elements['refresh'].value="";
		document.forms['negotiatedcontract_positions'].elements['send'].value="";
	    document.forms['negotiatedcontract_positions'].submit();
	    return true;
	}
		  
