/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       orderToolsRetkey.js                */
/* Description:    useful functions for the order     */
/*                 return key handling                */
/* Version:        1.0                                */
/* Author:         SAP                                */
/*----------------------------------------------------*/

/* check if the return key was pressed  */
if (is_nav==true && is_major < 5) {
	// Only necessary for Netscape versions < 5 
	document.captureEvents(Event.KEYPRESS);
	document.onkeypress = checkReturnKeyPressed;
};

function checkReturnKeyPressed(evt, retPressedFunc){
    var targetName;
    var pressedKeyCode;
    var altKey;
    var zKeyCode;
    
    // determine event data, depending on the browser
    if (is_nav) {
        // Netscape 
        pressedKeyCode = evt.which;
        targetName = evt.target.name;
        zKeyCode = 90;
    }
    else {
        // Microsoft, Opera
        pressedKeyCode = window.event.keyCode;
        targetName = evt.srcElement.name;
        zKeyCode = 26;
    };
    
    if (is_nav==true && is_major < 5) {
        shiftKey = evt.modifiers & Event.SHIFT_MASK;
        ctrlKey = evt.modifiers & Event.CONTROL_MASK;
    }
    else {
        shiftKey = evt.shiftKey;
        ctrlKey = evt.ctrlKey;
    }
    
    // alert("sh " + shiftKey + " ctrl " + ctrlKey + " press " + pressedKeyCode + " targ " + targetName);
    
    // Checking the targetName is only necessary for Nestacpe versions < 5 due to
    // the fact, that in these versions the onKeyPress event can not directly be
    // attached to a table row. So we have to make sure, that the event originated
    // from a product[i], quantity[i], ... field in the order_postions form, because
    // we are only interested in events coming from there. 
    if (pressedKeyCode == 13 && typeof(targetName) != "undefined") { // Return
        if (targetName.indexOf('[') > 0 && targetName.indexOf('comment[') < 0) {
            if (retPressedFunc != null && retPressedFunc.length > 0) {
                // execute given function
                eval(retPressedFunc);
            }
            else {
                // Default - Initiate Refresh 
                submit_refresh();
            }
        }
        else if (targetName == 'headerReqDeliveryDate') {
            checkReqDateChange();
        }
    }
    else if (shiftKey && ctrlKey && pressedKeyCode == zKeyCode) { //Shift + Ctrl + z
        goToFirstEmptyPos();
    }
    return true;
}

function checkReturnKeyPressedSoldToId(evt, soldToId, dealerIdName){
    var targetName;
    var pressedKeyCode;
    var altKey;
    var zKeyCode;
    
    // determine event data, depending on the browser
    if (is_nav) {
        // Netscape 
        pressedKeyCode = evt.which;
        targetName = evt.target.name;
        zKeyCode = 90;
    }
    else {
        // Microsoft, Opera
        pressedKeyCode = window.event.keyCode;
        targetName = evt.srcElement.name;
        zKeyCode = 26;
    };
    
    if (is_nav==true && is_major < 5) {
        shiftKey = evt.modifiers & Event.SHIFT_MASK;
        ctrlKey = evt.modifiers & Event.CONTROL_MASK;
    }
    else {
        shiftKey = evt.shiftKey;
        ctrlKey = evt.ctrlKey;
    }
    
    // alert("sh " + shiftKey + " ctrl " + ctrlKey + " press " + pressedKeyCode + " targ " + targetName);
    
    // Checking the targetName is only necessary for Nestacpe versions < 5 due to
    // the fact, that in these versions the onKeyPress event can not directly be
    // attached to a table row. So we have to make sure, that the event originated
    // from a product[i], quantity[i], ... field in the order_postions form, because
    // we are only interested in events coming from there. 
    if (pressedKeyCode == 13) { // Return
        if (targetName = 'soldToId') {
            create_orderwtemp('', soldToId, dealerIdName);
        }
    }

    return true;
}

function maskReturnKeyPressed(evt) {
    var pressedKeyCode;
    
    // determine event data, depending on the browser
    if (is_nav) {
        // Netscape 
        pressedKeyCode = evt.which;
    }
    else {
        // Microsoft, Opera
        pressedKeyCode = window.event.keyCode;
    };
    
    // alert(" press " + pressedKeyCode);
    
    // mask the return key by returning false 
    if (pressedKeyCode == 13) { // Return
        return false;
    }
    
    return true;
}