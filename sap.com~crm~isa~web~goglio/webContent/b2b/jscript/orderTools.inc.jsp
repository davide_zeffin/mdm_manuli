<%--
    File:         orderTools.inc.jsp
    Copyright (c) 2004, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      February 2004
    Version:      1.0

    This file must be inculded in a jsp via the inculde tag

      ...
         @ include file="/b2b/jscript/urlrewrite.inc"
      ...

    otherwise it won't work correctly.

    $Revision: #2 $
    $Date: 2003/04/09 $
--%>

<%@ page import="com.sap.isa.isacore.action.b2b.contract.DetailContractReadAction" %>
<%@ page import="com.sap.isa.isacore.action.ShowShipToAction" %>
<%@ page import="com.sap.isa.businesspartner.action.*" %>

<script type="text/javascript">
<!--
    var sessionid = "<isa:webappsURL name=""/>";
	
	    function toggleCampaignRows(iconId, targetId, rows) {
        
  	    if (navigator.userAgent.indexOf("MSIE") != -1) {
  	    // the world of the IEs
            //alert("IE - Modus");
            icon = document.all(iconId); 
            
            for (i = 1; i <= rows; i++) {
                target = document.all(targetId + i);
    		    if (target.style.display == "none") {
    			    target.style.display = "block";
    		    }
                else {
    			    target.style.display = "none";
    		    }
    		    //handle error rows, if existing
    		    targetErr = document.all(targetId + i + "_err");
    		    
    		    if (targetErr != null) {
    		        if (targetErr.style.display == "none") {
    			        targetErr.style.display = "block";
    		        }
                    else {
    			        targetErr.style.display = "none";
    		        }
    		    }
            }
    	} 
    	else {
    	  // the world of W3C
    	    //alert("W3C - Modus");
            icon = document.getElementById(iconId);
            
            for (i = 1; i <= rows; i++) {
                target = document.getElementById(targetId + i);
    		    if (target.style.display == "none") {
    			    target.style.display = "table-row";
    		    }
                else {
    			    target.style.display = "none";
    		    }
                
               //handle error rows, if existing
    		   targetErr = document.getElementById(targetId + i + "_err");

    		   if (targetErr != null) {
    		       if (targetErr.style.display == "none") {
    			       targetErr.style.display = "table-row";
    		       }
                   else {
    			       targetErr.style.display = "none";
    		       }
    		    }
            }
    	} 
        
        if (icon.src.lastIndexOf("open.gif") > 0) {  
            iconpath = icon.src.substring(0, icon.src.lastIndexOf("open.gif"));
    		icon.src=iconpath + "close.gif";
    		icon.alt='<isa:UCtranslate key="camp.toggle.coll"/>';
    	}
        else {
            iconpath = icon.src.substring(0, icon.src.lastIndexOf("close.gif"));
    		icon.src=iconpath + "open.gif";
    		icon.alt='<isa:UCtranslate key="camp.toggle.exp"/>';
    	}
  	} 
    
    function showProductReplacementHelp() {
      window.open('<isa:webappsURL name="b2b/productreplacementhelp.jsp"/>',
                  '<isa:UCtranslate key="b2b.prodrepl.helpwindow.titel"/>',
                  'scrollbars=yes,height=360,width=370');
    }

    function showShipTo(index) {
      var url = "<isa:webappsURL name="b2b/showshipto.do"/>?<%=ShowShipToAction.PARAM_SHIPTO_INDEX%>=" + index;
      var sat = window.open(url, 'sat_details', 'width=450,height=450,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
      sat.focus();
      return true;
    }

    function showShipToWithTechKey(techKey) {
      var url = "<isa:webappsURL name="b2b/showshipto.do"/>?<%=ShowShipToAction.PARAM_SHIPTO_TECHKEY%>=" + techKey;
      var sat = window.open(url, 'sat_details', 'width=450,height=450,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
      sat.focus();
      return true;
    }

    function showSoldTo(bupaKey) {
        var url = '<isa:webappsURL name="/b2b/businesspartnershow.do"/>?<%=BupaConstants.DISPLAY_IN_WINDOW%>=true&amp;<%=BupaConstants.READ_ONLY%>=true&amp;<%=BupaConstants.BUPA_TECHKEY %>=' + bupaKey ;
        openWinExtended(url,450,310);
      }

    function contractInfo(contractKey, contractItemKey, contractId) {
      var url = "<isa:webappsURL name="/b2b/detailcontractread.do"/>?<%=DetailContractReadAction.DETAIL_CONTRACT_KEY%>=" + contractKey + "&<%=DetailContractReadAction.DETAIL_CONTRACT_ITEM_KEY%>=" + contractItemKey + "&<%=DetailContractReadAction.DETAIL_CONTRACT_ID%>=" + contractId;
      window.open( url, 'contractdetails', 'height=500, width=400, resizable=yes, scrollbars=yes');
    }
	
    <%-- number of ui.lines to check for optimization --%>
	var linecount = <%= ui.items.size()%> + <%= ui.newpos %>;
      
	<%-- scripts for buttons --%>
	function disableEmptyInputFields() {
	  
	  <% if (ui.isHeaderChangeOnly()) { %>
	      linecount = 0;
	  <% } %>
	  
	  <%-- header values for optimization comparison on item level --%>
	  <% if (ui.isBasket()) { %>
			 var headerRqDlvDt = document.forms['order_positions'].elements['headerReqDeliveryDate'].value;
	  <% } %>
	
	  <%-- check item comment and item delivery date for optimization --%>
	  for(var i =1; i <= (linecount); i++) {
		  <%-- check item product field: is it send at all? --%>
		  var productString = "document.forms['order_positions'].elements['product["+ i +"]']";
		  if (eval(productString) == undefined || eval(productString).value == "") {
            continue;
		  }
		  <%-- check item comment fields => disable when empty --%>
		  var elementString = "document.forms['order_positions'].elements['comment["+ i +"]']";
		  
		  var elementObject = eval(elementString);
		  if (elementObject != null && elementObject.value == "") {
			  elementToDisable = "comment["+ i +"]";
			  document.getElementById(elementToDisable).disabled=true;
		  }
		  
		  <% if (ui.isBasket()) { %>
				<%-- check item delivery date fields => disable when empty or equal to header delivery date --%>
				elementString = "document.forms['order_positions'].elements['reqdeliverydate[" + i + "]']";
				<%-- subitems can be displayed without req. delivery date --%>
		  		if (eval(elementString) == undefined || eval(elementString).value == "") {
            		continue;
		  		}
				
				if ((eval(elementString).value == "")||(eval(elementString).value == headerRqDlvDt) ) {
				  elementToDisable = "reqdeliverydate[" + i + "]";
				  document.getElementById(elementToDisable).disabled=true;
				}
		  <% } %>
		  
		  <%-- check item delivery adress fields => disable when empty or equal to header delivery adress
			   open for implementation
			   check item delete flag => disable when not set
			   open for implementation 
		   --%>
	  } <%-- for --%>
	}
	
	function send_confirm() {
		// Multiple Invokation handling (note 905589)
		if (isBasketEmpty()  &&  miSetStatusUpdateRunning()) { // note 908052
		  alert('<isa:UCtranslate key="<%= ui.getAlertBasketEmpty() %>"/>');
		  return false ;
		}
		
        send_confirm_header_only();
     }
        
    function send_confirm_header_only() {
        // Multiple Invokation handling (note 905589)
        if ( ! miSetStatusUpdateRunning()) return false;	
        check = confirm('<isa:UCtranslate key="<%= ui.getConfirmKey() %>"/>');
		
		if(check) {
		  document.forms["order_positions"].sendpressed.value="true";
		  disableEmptyInputFields();
		  document.forms["order_positions"].submit();
          miSetStatusUpdateRunning(true); // note 908052
		} 
		return check;
	}
	
	function cancel_confirm() {
        // Multiple Invokation handling (note 905589)
        if ( ! miSetStatusUpdateRunning()) return false;	
		check = confirm('<isa:UCtranslate key="<%= ui.getCancelKey() %>"/>');
		
		if(check) {
		  document.forms["order_positions"].target = "form_input";
		  document.forms["order_positions"].cancelpressed.value="true";
		  document.forms["order_positions"].submit();
          miSetStatusUpdateRunning(true); // note 908052
		}		
		return check;
	}

	function order_confirm() {
        // Multiple Invokation handling (note 905589)
        if ( ! miSetStatusUpdateRunning()) return false;
   		if (document.forms["order_positions"] != null) {
			
			if (isBasketEmpty()) {
				alert('<isa:UCtranslate key="b2b.ordr.alr.collect.empty"/>');	
			  	return false;	
			}

	       	check = confirm("<isa:UCtranslate key="b2b.collorder.send.confirm"/>");
    	  	if(check == true) {
        		document.forms["order_positions"].orderpressed.value="true";
            	disableEmptyInputFields();
	            document.forms["order_positions"].submit();
                miSetStatusUpdateRunning(true); // 908052
    	  	}
    	  	return check;
    	}  	
          
        return false;
      }

     
     function goToFirstEmptyPos() {
     
        <%-- first possible empty posittion --%>
  	    i = <%= ui.items.size() + 1 %>;
  	    
	    while (i <= linecount && !<%= ui.isServiceRecall() %>) {

			if (getElement("product[" + i + "]").value == "") {
			    getElement("product[" + i + "]").focus();
		        break;
			}
	        i = i + 1;
	    }
	    
	    <%-- goto the last position, if there are no empty positions anymore --%>
	    if (i > linecount) {
	        getElement("product[" + linecount + "]").focus();
	    }
     }
     // *** START multiple invokation handling (note (note 905589))
     var miAllObjs = null;
     // note 1101891 - begin
     var miAllImgObjs = null;
     // note 1101891 - end
     var miUpdateStarted = false;
     // function is called on onload event of HTML page
     function miRegisterObjects() {
       // all buttons are modelled as <a href...>
       miAllObjs = document.getElementsByTagName('a');
       // a few buttons are modelled as <img...>
       miAllImgObjs = document.getElementsByTagName('img');
     }
     
     function miSetStatusUpdateRunning(setLock) {
       if (miUpdateStarted) {
         return false;
       }
       if (setLock) {  // note 908052
         miUpdateStarted = true;
         for (var i = 0; i < miAllObjs.length; i++) {
           miAllObjs[i].title="<isa:UCtranslate key="b2b.ordr.mi.title.inpr"/>";
         // note 1101891 - begin
           // disable some buttons 
           var first11 = miAllObjs[i].id.substring(0,11);
           if (miAllObjs[i].id == "toggleAllItems" ||
               miAllObjs[i].id == "toggleHeader"   ||
               first11         == "toggleItem_" ) {
              miAllObjs[i].href="#";
           }
         }
         // change quick info of img tags
         for (var i = 0; i < miAllImgObjs.length; i++) {
           var first10 = miAllImgObjs[i].id.substring(0,10);
           if (miAllImgObjs[i].id == "addheadericon" ||
               first10            == "rowdetail_" ) {
              miAllImgObjs[i].title="<isa:UCtranslate key="b2b.ordr.mi.title.inpr"/>";
           }
         // note 1101891 - end
         }
       }
       return true;
     }
     // *** END multiple invokation handling
  //-->
  </script>