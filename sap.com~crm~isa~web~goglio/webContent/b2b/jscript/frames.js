/*****************************************************************************
    File:         frames.js
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      May 2001
    Version:      1.0

    $Revision: #14 $
    $Date: 2001/05/30 $
*****************************************************************************/

/**
 * returns an reference to the top frame
 *
 */
function isaTop() {

    var tmpWnd = window;
	var lastWnd = tmpWnd;    

	try {
	    while (tmpWnd.name != "isaTop" && tmpWnd.parent != tmpWnd) {
	         lastWnd = tmpWnd;
    	     tmpWnd = tmpWnd.parent;
	    }
	}    
	catch (e) {
	 // this can happen because of domain relaxation!
	 	tmpWnd = lastWnd;
	}    
    return tmpWnd;
}


/**
 *
 * returns an reference to the header frame
 *
 */
function header() {

    return isaTop().header;

}

/**
 *
 * returns an reference to the work_history frame
 *
 */
function work_history() {

    return isaTop().work_history;

}

/**
 *
 * returns an reference to the work_history frame
 *
 */
function form_input() {

    return isaTop().work_history.form_input;

}


/**
 *
 * returns an reference to the work_history frame
 *
 */
function getHistoryFrame() {

    return isaTop().work_history._history;

}


/**
 *
 * set the history frame
 *
 */
function setHistoryFrameUrl(url) {

    if (isaTop().work_history) {

        if (isaTop().work_history._history) {
            var myFrame = isaTop().work_history._history;
            myFrame.location.href = url;
        }else alert('no history');
    }
    else alert('no workhistory');
}


/**
 *
 * returns an reference to the work_history frame
 *
 */
function closer_history() {

    return isaTop().work_history.closer_history;

}

/**
 *
 * returns an reference to the work_history frame
 *
 */
function documents() {

    return isaTop().work_history.documents;

}


/**
 *
 * returns an reference to the organizer_nav frame
 *
 */
function organizer_nav() {

    return isaTop().organizer_nav;

}


/**
 *
 * returns an reference to the organizer_content frame
 *
 */
function organizer_content() {

    return isaTop().organizer_content;

}


