/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       batchTools.js                      */
/* Description:    excluded javascript functions for  */
/*                 batch handling on salesdocuments   */
/* Version:        1.0                                */
/* Author:         SAP                                */
/*----------------------------------------------------*/

/* Start Scripts for Batch */
function openBatchDetails(idName) {

  var target = getElement(idName);

  if (target && isToggleSupported) {
    if (target.style.display == "none") {
      target.style.display = "inline";
    }
  }
  else {
     // handle NS4.7
    alert("ServerRequest");
  }
}

function setFocus(element){
     document.forms["order_positions"].elements[element].focus();
}

function submit_batch_selection(itemID, productID, product, batchselected){
  document.forms["order_positions"].elements["batchselection"].value =  itemID;
  document.forms["order_positions"].elements["batchproductid"].value =  productID;
  document.forms["order_positions"].elements["batchproduct"].value =  product;
  document.forms["order_positions"].elements[batchselected].value =  "true";
  document.forms["order_positions"].submit();
  return true;
}

/* End Scripts for Batch */