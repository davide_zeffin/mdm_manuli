function busy(frameObject) {

    try {
        with(frameObject.document){
			// get all the stylesheets from the header
			var allStylesheets = this.window.document.styleSheets;

			writeln("<html><head>");
			// add the css sheets
			for(var i = 0; i < allStylesheets.length; i++) {
				writeln('<link type="text/css" rel="stylesheet" href="' + allStylesheets[i].href + '" />\n');
			}
			// add the rest
			writeln("</head>\n<body><div id='busy'>\n" +
            	"<img src='<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/busy.gif") %>' alt='<isa:UCtranslate key="b2b.busy.message"/>' />" +
            	"<p><isa:UCtranslate key="b2b.busy.message"/></p>" +
            	"</div></body></html>");
        }
        frameObject.document.close();
    }
    catch (e) {}
}
