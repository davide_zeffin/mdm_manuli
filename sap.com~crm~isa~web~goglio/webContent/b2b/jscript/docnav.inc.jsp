<%--
    File:         docnav.inc.jsp
    Copyright (c) 2005, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      July 2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/07/15 $
--%>

	  	<script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
			type="text/javascript">
	  	</script>
	  	
<script type="text/javascript">
   <!--
        function updateMinibasket() {
      
            if (isaTop().header.getElement("basketForm")) {
                
              // header object minibasket available => set up to date minibasket info
              <%
              
              if (mb_doctype.equals("negotiatedContract")) { %>
                //note: when a 'negotiated contract' is edited only itemsize should be displayed in minibasket
                isaTop().header.setBasketLinkText("<%=itemSize%> <isa:UCtranslate key="b2b.header.minibasket.ncontract"/>");
              <%
              }
              else {
                //note: used for all sales documents except 'negotiated contracts'
                if ((itemSize.equals("-1")) && (netValue.equals("-1"))) { 
                  //true when no editable document is available or editable document is no sales document
                  // path should never be used in HOM scenario %>
                  isaTop().header.setBasketLinkText("<isa:UCtranslate key="b2b.header.minibasket.default"/>");
                <%
                }
                else {
                  if ((itemSize.equals("0")) && (netValue.equals("0"))) {
                    // different minibasket descriptions for hom and standard
                    if (homActivated) { %>
                        isaTop().header.setBasketLinkText("<isa:UCtranslate key="b2b.header.hom.minibasket.empty"/>");
                    <%
                    } 
                    else { %>
                        isaTop().header.setBasketLinkText("<isa:UCtranslate key="b2b.header.minibasket.empty"/>");              
                    <%
                    }
                  } 
                  else {
                    //same description for standard b2b and hom scenario %>
                    isaTop().header.setBasketLinkText("<%=itemSize%> <isa:UCtranslate key="b2b.header.minibasket.l1"/> <%= netValue %> <%= JspUtil.encodeHtml(currency) %>");
                  <%
                  } 
                }
              }%>
            }
            else {
              // header object minibasket not available => don?t set minibaket info via this frame.
              // in this case minibasket value is taken from header.jsp.
            } 
        }
        
        function loadPage() {
            // refresh minibasket in header
            updateMinibasket();
        }
    //-->
  </script>

