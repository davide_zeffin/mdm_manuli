/*----------------------------------------------------*/
/* Javascript for the "Internet Sales"                */
/*                                                    */
/* Document:       changearrowimage                   */
/* Description:    Invert direction of Arrow          */
/* Version:        1.0                                */
/* Author:         Ralf Witt                          */
/* Creation-Date:  30.10.2001                         */
/* Last-Update:    30.10.2001                         */
/*                                                    */
/*                                    (c) SAP Markets */
/*----------------------------------------------------*/

/* Folloing function inverts direction of the delivery info Arrow
   With the first entry in function Arrow has to switch to close, since
   with building the page, arrow is set to open!! 
   
   Input: 
    - nameOfImage = name of the img - tag
    - idx         = index of the direction (to handle more than
                    one image)
*/
var direction = new Array(100);

var changearrowimage_open  = ""
var changearrowimage_close = ""
function changeArrowImage(nameOfImage, idx) {
    if (direction[idx] == 1 && direction[idx] != null) {
        document.images[nameOfImage].src = changearrowimage_open;
        direction[idx] = 0;         // hide delivery details
    } else {
        document.images[nameOfImage].src = changearrowimage_close;
        direction[idx] = 1;         // show delivery details
    }
}

function changeArrowImageInit(openImage, closeImage) {
    changearrowimage_open  = openImage;
    changearrowimage_close = closeImage;
}
