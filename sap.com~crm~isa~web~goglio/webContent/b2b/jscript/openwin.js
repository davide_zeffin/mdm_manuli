/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       openwin.js                         */
/* Description:    opens details window               */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  04.04.2001                         */
/* Last-Update:    04.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/

// loads new order, templates etc.
function openWin() {
  var sat = window.open('customer_address_details.html', 'sat_details', 'width=400,height=230,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
  sat.focus();
}



