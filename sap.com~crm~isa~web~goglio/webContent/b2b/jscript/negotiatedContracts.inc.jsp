<%--
    File:         negotiatedContracts.inc.jsp
    Copyright (c) 2005, SAP AG, All rights reserved.
    Author:       SAP AG
    Created:      May 2005
    Version:      1.0

    This file must be inculded in a jsp via the inculde tag

      ...
         @ include file="/b2b/jscript/negotiatedContract.inc.jsp"
      ...

    otherwise it won't work correctly.

    $Revision: #1 $
    $Date: 2005/05/31 $
--%>

<script type="text/javascript">
<!--

	   function cancel_confirm() {
	      check = confirm('<isa:UCtranslate key="b2b.nct.confirm.cancel"/>');
          if(check == true) {
	         form_input().location.href = "<isa:webappsURL name="/b2b/closenegotiatedcontract.do?history=true"/>";
             return true;
          }
          else {
  	         return false;
          }
       }
       
	function send_negotiatedcontract() {
		// find out if there are products in the contract	      
	    lineMax = 4;
	    emptyContract = true;
	    i = 1;
	
	    while (( i <= lineMax ) && (emptyContract)) {
	  	   productString = "document.forms['negotiatedcontract_positions'].elements['product[" + i + "]']";
	  	   deleteCheckBox = "document.forms['negotiatedcontract_positions'].elements['delete[" + i + "]']";
	 	   if ((eval(productString).value != "") && (eval(deleteCheckBox) == null || eval(deleteCheckBox).checked == false)) {
		      emptyContract = false;
	       }
	       i = i + 1;
		}
		
		if (emptyContract) {  // alert if there is no product in the contract
		   	alert('<isa:UCtranslate key="b2b.nct.alert.noproduct"/>');
		    return false;
		}
		
		check = confirm('<isa:UCtranslate key="b2b.nct.confirm.send"/>'); // ask, if you really want to send the document
		if (!check) {
		   return false; 
		}
		
		document.forms['negotiatedcontract_positions'].elements['send'].value="true";
		document.forms['negotiatedcontract_positions'].elements['save'].value="";
		document.forms['negotiatedcontract_positions'].elements['refresh'].value="";
		document.forms['negotiatedcontract_positions'].elements['cancel'].value="";
		document.forms['negotiatedcontract_positions'].submit();
		return true;
	}
	       
	  	function reject_confirm() {
		   check = confirm('<isa:UCtranslate key="b2b.nct.confirm.reject"/>');
		   if (check) {
			form_input().location.href = "<isa:webappsURL name='b2b/changenegotiatedcontractstatus.do?reject=true' />";
		   }
        }

	    function accept_confirm() {
		   check = confirm('<isa:UCtranslate key="b2b.nct.confirm.accept"/>');
		   if (check) {
			form_input().location.href = "<isa:webappsURL name='b2b/changenegotiatedcontractstatus.do?accept=true' />";
		   }
	    }

	    function change_ncontract() {
		   if (parent.documents.editableDocumentAlert('<isa:UCtranslate key="b2b.onedocnav.onlyonedoc"/>\n\n<isa:UCtranslate key="b2b.onedocnav.onlyonedoc2"/>\n\n<isa:UCtranslate key="b2b.onedocnav.onlyonedoc3"/>') ) {
		   <%-- there is no editable doc, so we can go into change mode --%>
		   form_input().location.href = "<isa:webappsURL name='b2b/changenegotiatedcontractstatus.do?change=true' />";
		   }
	    }

    	function close_ncontract() {
		    form_input().location.href = "<isa:webappsURL name='b2b/closenegotiatedcontract.do' />";
	    }


//-->
</script>
