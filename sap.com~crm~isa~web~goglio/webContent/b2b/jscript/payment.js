/*------------------------------------------------------*/
/*    File:         payment.js                          */
/*    Copyright (c) 2008, SAP AG, All rights reserved.  */
/*    Author:       SAP AG                              */
/*    Created:      August 2008                         */
/*    Version:      1.0                                 */
/*                                                      */
/*------------------------------------------------------*/


  function perform_action(selected_action) {
     getElement("paymentform").selectedaction.value=selected_action;
     getElement("paymentform").submit();
     return true;
   }
   
   function perform_action_ccardcheck(selected_action, error_text) {
     if ( getElement("paytype_3").checked==true && !check_credit_card_entry(error_text)) {
        alert(error_text);
        return false;
     }
     else {
     	getElement("paymentform").selectedaction.value=selected_action;
     	getElement("paymentform").submit();
        return true;
     }

   }

   function button_pressed(selected_action) {
    //  check_credit_card_entry(selected_action, update, save, error_text);
      getElement("paymentform").selectedaction.value=selected_action;
      location.href = '<isa:webappsURL name="/b2b/maintainpayment.do"/>';
      return true;
   }

   function check_credit_card_entry(error_text) {
 
      var ccard_no = getElement("nolog_cardno[0]");
      var ccard_entered = false;
      var cont = (ccard_no != undefined);
      var i = 0;

      while (cont) {
          if (ccard_no.value != "") {
              ccard_entered = true;
              break;
          }
          i++;
          ccard_no = getElement("nolog_cardno[" + i +"]");
          cont = (ccard_no != undefined);
      }

      return ccard_entered;
            
   }