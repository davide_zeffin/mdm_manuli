/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       send_confirm.js                    */
/* Description:    confirm.window with reload         */
/* Version:        1.0                                */
/* Author:         m_vogel@pixelpark.com              */
/* Creation-Date:  11.04.2001                         */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/


// confirm.window
  function send_confirm() {
    Check = confirm("Wollen Sie den Auftrag entg�ltig absenden?");
    if(Check == true) { 
      reloadFrame('frameset_order_confirm.html', 'open_order_with_open_order_grey.html');
    }
  }
