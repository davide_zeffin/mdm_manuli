/*-----------------------------------------------------------------------------------------------*/
/* Javascript for the "SAP DealerLocator Project"                                                          */
/*                                                                                               */
/* Document:       addressdetails_maintenance.js                                                 */
/* Description:    This document contains functions which will be used to search after a           */ 
/*                       dealer            */
/*                       particular JSPs (searchdealer.jsp, showdealer.jsp).                */ 
/* Version:        1.1                                                                           */
/* Author:         Thorsten Mohr                                                                 */
/* Creation-Date:  04.09.2001                                                                    */
/*                                                                                               */
/*                                                                               (c) SAP Markets */
/*-----------------------------------------------------------------------------------------------*/

// Array used to find information about the new selected form of address (a title change occurred).
// The JSP will fill this array with "P"s and/or "O"s.
// E.g. a "P" at toTitle[1] means that the 2nd title within the select box represents a person,
// an "O" at toTitle[3] means that the 4th title represents an organization.
var toTitle = new Array();
// Array used to find information about the new selected country (a country change occurred).
// The JSP will fill this array with "F"s and/or "T"s.
// E.g. a "T" at toCountry[0] means that the 1st country within the select box requires regions to be displayed,
// an "F" at toCountry[1] means that the 2nd country requires no regions to be displayed.
var toCountry = new Array();

// Only if the submit counter has a value equal 0, a submit can be send, otherwise it is not allowed and must be
// suppressed.
var submitCounter = 0;

// the user decides to reset 
function send_reset() {
    if (submitCounter == 0) {

        submitCounter++;
        document.forms[0].cancelClicked.value="true";
        document.forms[0].submit();
    }
}


// the user decides to search
function send_search() {
    if (submitCounter == 0) {   
        
        submitCounter++;
        document.forms[0].submit();
    }
}


// the user selects another title
function send_titleChange(fromTitle) {
    // remark: fromTitle should be equal to 'P' or 'O'
    // P = title key is used for a person
    // O = title key is used for an organization
    // Possible title changes:
    // (1) from P to O
    // (2) from P to P
    // (3) from O to O
    // (4) from O to P
    // there are 2 situations where a rebuild of this JSP must be forced: (1), (4)
    
    // determine the titleElement, i.e. the select box which contains the possible titles
    var titleKeyElement = document.forms[0].titleKey;
    // determine the index of the selected option (i.e. the selected title)
    var selectedIndex = titleKeyElement.selectedIndex;
    
    // decide whether to send a request or not
    if (fromTitle != toTitle[selectedIndex])
    {
      // debugging:
      // alert("Title change form "+fromTitle+" to "+toTitle[selectedIndex]);
      document.forms[0].titleChange.value="true";
      document.forms[0].submit();
    }
}


// the user selects another country
function send_countryChange(fromCountry) {
    // remark: fromCountry should be equal to 'T' or 'F'
    // T = country where regions are reqired
    // F = country where regions are not required
    // Possible country changes:
    // (1) from F to F
    // (2) from F to T
    // (3) from T to F
    // (4) from T to T
    // there are 3 situations where a rebuild of this JSP must be forced: (2), (3), (4)

    // determine the countryElement, i.e. the select box which contains the possible countries
    var countryElement = document.forms[0].dealerCountry;
    // determine the index of the selected option (i.e. the selected country)
    var selectedIndex = countryElement.selectedIndex;
  
    // decide whether to send a request or not
    if (fromCountry != "F" || toCountry[selectedIndex] != "F")
    {
      // at this point: F to F is not the case
      
      // debugging:
      // alert("Title change form "+fromCountry+" to "+toCountry[selectedIndex]);
      
      if (toCountry[selectedIndex] == "F") {
          document.forms[0].regionRequired.value=""; // the empty string will be interpreted as 'false'
                                                     // by the request parser
      }
      else {
          document.forms[0].regionRequired.value="true";
      }

      document.forms[0].countryChange.value="true";
      document.forms[0].submit();
    }
}
