/*****************************************************************************
    File:         addToDoument.js
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler, Alexander Staff
    Created:      June 2001
    Version:      1.0

    $Revision: #14 $
    $Date: 2001/05/30 $
*****************************************************************************/

      // a global variable to set the error message
      var addToDocumentErrorMessage1 = 'no order';
      var addToDocumentErrorMessage2 = 'no positions';

      function setAddToDocumentErrorMessages(message1, message2) {
        addToDocumentErrorMessage1 = message1;
        addToDocumentErrorMessage2 = message2;
      }

      // get the reference to the form "order_positions" in the frame "form_input" frame
      function getMyForm() {

          // from frames.js
          var varFrame   = form_input();
          var theForm;
          
          if (varFrame != null) {
              theForm = varFrame.document.order_positions;
              // return theForm here, either it is null i.e. not found
              // or != null, which means we found the form

			  // contract uses still the positions frame!!	                  
              if (theForm == null) { 
              	//note 1252474
              	//varFrame = varFrame.positions;
              	//if (varFrame != null) {
                  theForm = varFrame.document.negotiatedcontract_positions;
                //  }
              }
                  
              return theForm;
          }
          
          return theForm;
      }

      // gets the next free input field to store the product number in
      function getFirstFreeField() {

          var theField = null;
          var index    = 1;
          var myForm;

          myForm = getMyForm();
          
          if (myForm == null) {
              // no action, no suitable frame/input form found
              // ATTENTION : JSP-Tag inside HTML-comments
              alert(addToDocumentErrorMessage1);
              // MessageBox : Please create new order, you unholy user !!!
              return null;
          }
          
          // get the field
          theField = myForm.elements["product[" + index + "]"];
          
          while (theField != null) {

              // field found, check if it is empty
              if (theField.value == "") {
                  // empty field found
                  return theField;
              }
              else {
                  index++;
                  theField = myForm.elements["product[" + index + "]"];
              }
          }

          // no empty field found
          // ATTENTION : JSP-Tag inside HTML-comments
          alert(addToDocumentErrorMessage2);
          // MessageBox : Please create new input fields, you unholy user !!!
          return theField;

      }

      // sets the prodnumber in the first free input field 
      // sets the quantity to a default-value of 1
      function setFirstFreeFieldValue(prodnumber) {

          var theField = null;
          var index    = 1;
          var myForm;

          myForm = getMyForm();
          
          if (myForm == null) {
              // no action, no suitable frame/input form found
              // ATTENTION : JSP-Tag inside HTML-comments
              alert(addToDocumentErrorMessage1);
              // MessageBox : Please create new order, you unholy user !!!
              return null;
          }
          
          //is 1 and not 0 note 1252474
          //if (myForm.name=="negotiatedcontract_positions") {
          //	index = 0;
          //}
          
          // get the field
          theField = myForm.elements["product[" + index + "]"];
          
          while (theField != null) {

              // field found, check if it is empty
              if (theField.value == "") {
                  // empty field found
                  theField.value = prodnumber;
                          
                  if (myForm.name=="order_positions") {
                      // get the quantity field
                      theField = myForm.elements["quantity[" + index + "]"];

                      theField.value = "1";
                  
                } 
                if (myForm.elements["targetquantity[" + index + "]"]) {
                      // get the quantity field
                      myForm.elements["targetquantity[" + index + "]"].value = "1";
                  
                } 
                return theField;
                  
              }
              else {
                  index++;
                  theField = myForm.elements["product[" + index + "]"];
              }
          }

          // no empty field found
          // ATTENTION : JSP-Tag inside HTML-comments
          alert(addToDocumentErrorMessage2);
          // MessageBox : Please create new input fields, you unholy user !!!
          return theField;

      }      // write the product number in the first available input field in the order
    
      function writeProdNumber( prodNumber ) {
          var theField;

          theField = setFirstFreeFieldValue(prodNumber)
          
          // any return value here ??
      }

      // write the product number in the first available input field in the order
      function addToDocument( prodNumber, unit, quantity ) {
        writeProdNumber(prodNumber);
      }
