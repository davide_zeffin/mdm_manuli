/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       collapse.js                        */
/* Description:    Collapse and expand table rows     */
/* Version:        1.1                                */
/* Author:         Ralf Witt                          */
/* Creation-Date:  03.07.2001                         */
/* Last-Update:    13.11.2001                         */
/*                                                    */
/*                                                    */
/*                                    (c) SAP Markets */
/*----------------------------------------------------*/

function collapseDlvInfo(elemName) {
    var ende = 0;
    var dlvElem;
    var dlvElems = new Array(100);
    var dlvElemsCnt = 0;
    var elemNameFull;
    var delrow = 1;
      
    // First get object without name expanded
    dlvElem = document.getElementById(elemName);        
    if (dlvElem != null) {
        dlvElems[dlvElemsCnt++]=dlvElem;
    }
    // collect all belonging Objects (TR tags)
    while (ende == 0) {
        elemNameFull = elemName + "_" + delrow;
        dlvElem = document.getElementById(elemNameFull);
        if (dlvElem == null) {
            ende = 1;
        } else {
            dlvElems[dlvElemsCnt++]=dlvElem;
        }
        delrow++;
    }

    // Hide or unhide objects        
    for (var i = 0; i < dlvElemsCnt; i++) {
        if (dlvElems[i].style.display == "none") {
            dlvElems[i].style.display = "";
        } else {
            dlvElems[i].style.display = "none";
        }
    }
}
