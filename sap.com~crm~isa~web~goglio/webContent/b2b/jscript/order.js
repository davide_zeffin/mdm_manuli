/*****************************************************************************
    File:         order.js
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Stefan Heimann
    Created:      19.4.2001
    Version:      2.0 (completely rewritten)

    $Revision: #2 $
    $Date: 2002/02/12 $
*****************************************************************************/



// variables
var detailsData;              // variable for object
var detailsLoaded = false;    // variable "Details already loaded?"
var orderLoaded = false;      // variable "Order already loaded?"
var getDetailsInterval;       // variable to store the interval object
var dataFlag = false;         // flag for data-status
var updateTime = 100;         // update time of details frame (ms)
var noticeFlag = true;        // notice area enabled


// "constructor": object with all properties
function detailsObj(id, posno, reqdeliverydate, customer, comment, netprice, itemTechKey, productkey, detailContractKey, detailContractItemKey, contractRef, deliveryDateChangeable, shiptoChangeable) {
  (id != null)                     ? this.id = id                                         : this.id = "";
  (posno != null)                  ? this.posno = posno                                   : this.posno = "";
  (reqdeliverydate != null)        ? this.reqdeliverydate = reqdeliverydate               : this.reqdeliverydate = "";
  (customer != null)               ? this.customer = customer                             : this.customer = "";
  (comment != null)                ? this.comment = comment                               : this.comment = "";
  (netprice != null)               ? this.netprice = netprice                             : this.netprice = "";
  (itemTechKey != null)            ? this.itemTechKey = itemTechKey                       : this.itemTechKey = "";
  (productkey != null)             ? this.productkey = productkey                         : this.productkey = "";
  (detailContractKey != null)      ? this.detailContractKey = detailContractKey           : this.detailContractKey = "";
  (detailContractItemKey != null)  ? this.detailContractItemKey = detailContractItemKey   : this.detailContractItemKey = "";
  (contractRef != null)            ? this.contractRef = contractRef                       : this.contractRef = "";
  (deliveryDateChangeable != null) ? this.deliveryDateChangeable = deliveryDateChangeable : this.deliveryDateChangeable = true;
  (shiptoChangeable != null)       ? this.shiptoChangeable = shiptoChangeable             : this.shiptoChangeable = true;
}


// initialization of object
function initObj(intervalCheck, id, reqdeliverydate, customer, comment, netprice, itemTechKey, productkey, detailContractKey, detailContractItemKey, contractRef, deliveryDateChangeable, shiptoChangeable) {
  if (!detailsData && !intervalCheck) {
    posno = "10";
    detailsData = new detailsObj(id, posno, reqdeliverydate, customer, comment, netprice, itemTechKey, productkey, detailContractKey, detailContractItemKey, contractRef, deliveryDateChangeable, shiptoChangeable);
    dataFlag = true;
  }
  if (intervalCheck) {
    if (getDetailsInterval)  window.clearInterval(getDetailsInterval);
    getDetailsInterval = window.setInterval('startInterval()', updateTime);
  }
}


// watch function
function startInterval() {
  if (detailsLoaded && dataFlag) {
    window.clearInterval(getDetailsInterval);
    getDetailsInterval = window.setInterval('detailsData.getAllDetails()', updateTime);
  }
}


//-----------------------------------------------------------------------------
// set and get methods for the details-object
//-----------------------------------------------------------------------------

// set all details
detailsObj.prototype.setAllDetails = function(id, posno, reqdeliverydate, customer, comment, netprice, itemTechKey, productkey, detailContractKey, detailContractItemKey, contractRef, deliveryDateChangeable, shiptoChangeable) {
  dataFlag = false;
  if (id != null)                     this.setDetailsID(id);
  if (posno != null)                  this.setPosno(posno);
  if (reqdeliverydate != null)        this.setDetailsReqdeliverydate(reqdeliverydate);
  if (customer != null)               this.setDetailsCustomer(customer);
  if (comment != null)                this.setDetailsComment(comment);
  if (netprice != null)               this.setDetailsNetprice(netprice);
  if (itemTechKey != null)            this.setDetailsItemTechKey(itemTechKey);
  if (productkey != null)             this.setDetailsProductKey(productkey);
  if (detailContractKey != null)      this.setDetailsDetailContractKey(detailContractKey);
  if (detailContractItemKey != null)  this.setDetailsDetailContractItemKey(detailContractItemKey);
  if (contractRef != null)            this.setDetailsContractRef(contractRef);
  if (deliveryDateChangeable != null) this.setDetailsDeliveryDateChangeable(deliveryDateChangeable);
  if (shiptoChangeable != null)       this.setDetailsShipToChangeable(shiptoChangeable);
  dataFlag = true;
  startInterval();
}
// get all details
detailsObj.prototype.getAllDetails = function() {
  if (dataFlag == true) {

    window.clearInterval(getDetailsInterval);

    if (details.document.forms['position_details']) {
      actualForm = details.document.forms['position_details'];
      actualForm.elements['position_number'].value = this.getPosno();
      actualForm.elements['reqdeliverydate'].value = this.getDetailsReqdeliverydate();
      actualForm.elements['customer'].value = this.getDetailsCustomer();
      actualForm.elements['netprice'].value = this.getDetailsNetprice();
      actualForm.elements['itemTechKey'].value = this.getDetailsItemTechKey();
//      actualForm.elements['productkey'].value = this.getDetailsProductKey();
      actualForm.elements['detailContractKey'].value = this.getDetailsDetailContractKey();
      actualForm.elements['detailContractItemKey'].value = this.getDetailsDetailContractItemKey();
      actualForm.elements['reqdeliverydate'].disabled = !this.getDetailsDeliveryDateChangeable();
      actualForm.elements['customer'].disabled = !this.getDetailsShipToChangeable();
    }

    if (details.document.forms['order_change_details']) {
      actualForm = details.document.forms['order_change_details'];
      actualForm.elements['position_number'].value = this.getPosno();
      actualForm.elements['reqdeliverydate'].value = this.getDetailsReqdeliverydate();
      actualForm.elements['customer'].value = this.getDetailsCustomer();
      actualForm.elements['netprice'].value = this.getDetailsNetprice();
      actualForm.elements['itemTechKey'].value = this.getDetailsItemTechKey();
      actualForm.elements['detailContractKey'].value = this.getDetailsDetailContractKey();
      actualForm.elements['detailContractItemKey'].value = this.getDetailsDetailContractItemKey();
      actualForm.elements['reqdeliverydate'].disabled = !this.getDetailsDeliveryDateChangeable();
      actualForm.elements['customer'].disabled = !this.getDetailsShipToChangeable();
      
    }

    if (details.document.forms['notice_details']) {
      actualForm = details.document.forms['notice_details'];
      actualForm.elements['position_number'].value = this.getPosno();
      actualForm.elements['comment'].value = this.getDetailsComment();
      if (!getNoticeStatus())
        actualForm.elements["comment"].disabled = true;
    }

    if (details.document.forms['contract_details']) {
      actualForm = details.document.forms['contract_details'];
      if (actualForm.elements['contractRef'])
        actualForm.elements['contractRef'].value = this.getDetailsContractRef();
      actualForm.elements['position_number'].value = this.getPosno();
    }

    if (details.document.forms['position_accessories']) {
      actualForm = details.document.forms['position_accessories'];
      actualForm.elements['position_number'].value = this.getPosno();
    }

    if (details.document.forms['position_crossselling']) {
      actualForm = details.document.forms['position_crossselling'];
      actualForm.elements['position_number'].value = this.getPosno();
    }

    if (details.document.forms['position_upselling']) {
      actualForm = details.document.forms['position_upselling'];
      actualForm.elements['position_number'].value = this.getPosno();
    }
  }
}

//-----------------------------------------------------------------------------
// set details id
detailsObj.prototype.setDetailsID = function(id) {
  this.id = id;
}
// get details id
detailsObj.prototype.getDetailsID = function() {
  return this.id;
}
//-----------------------------------------------------------------------------
// set deliveryDateChangeable
detailsObj.prototype.setDetailsDeliveryDateChangeable = function(deliveryDateChangeable) {
  this.deliveryDateChangeable = deliveryDateChangeable;
}
// get details id
detailsObj.prototype.getDetailsDeliveryDateChangeable = function() {
  return this.deliveryDateChangeable;
}
//-----------------------------------------------------------------------------
// set deliveryDateChangeable
detailsObj.prototype.setDetailsShipToChangeable = function(shiptoChangeable) {
  this.shiptoChangeable = shiptoChangeable;
}
// get details id
detailsObj.prototype.getDetailsShipToChangeable = function() {
  return this.shiptoChangeable;
}
//-----------------------------------------------------------------------------
// set position number
detailsObj.prototype.setPosno = function(posno) {
  this.posno = posno;
}
// get position number
detailsObj.prototype.getPosno = function() {
  return this.posno;
}
//-----------------------------------------------------------------------------
// set details date
detailsObj.prototype.setDetailsReqdeliverydate = function(reqdeliverydate) {
  this.reqdeliverydate = reqdeliverydate;
  if (orderLoaded) {
    datePart = eval("positions.document.forms['order_positions'].elements['reqdeliverydate["+this.id+"]']");
    datePart.value = this.reqdeliverydate;
  }
}
// get details date
detailsObj.prototype.getDetailsReqdeliverydate = function() {
  return this.reqdeliverydate;
}
//-----------------------------------------------------------------------------
// set details customer
detailsObj.prototype.setDetailsCustomer = function(customer) {
  this.customer = customer;
  if (orderLoaded) {
    customerPart = eval("positions.document.forms['order_positions'].elements['customer["+this.id+"]']");
    customerPart.value = this.customer;
  }
}
// get details customer
detailsObj.prototype.getDetailsCustomer = function() {
  return this.customer;
}
//-----------------------------------------------------------------------------
// set details comment
detailsObj.prototype.setDetailsComment = function(comment) {
  this.comment = comment;
  commentPart = eval("positions.document.forms['order_positions'].elements['comment["+this.id+"]']");
  commentPart.value = comment;
}
// get details comment
detailsObj.prototype.getDetailsComment = function() {
  return this.comment;
}
//-----------------------------------------------------------------------------
// set details netprice
detailsObj.prototype.setDetailsNetprice = function(netprice) {
  this.netprice = netprice;
  if (orderLoaded) {
    netpricePart = eval("positions.document.forms['order_positions'].elements['netprice["+this.id+"]']");
    netpricePart.value = netprice;
  }
}
// get details netprice
detailsObj.prototype.getDetailsNetprice = function() {
  return this.netprice;
}
//-----------------------------------------------------------------------------
// set details itemTechKey
detailsObj.prototype.setDetailsItemTechKey = function(itemTechKey) {
  this.itemTechKey = itemTechKey;
  if (orderLoaded) {
    itemTechKeyPart = eval("positions.document.forms['order_positions'].elements['itemTechKey["+this.id+"]']");
    itemTechKeyPart.value = itemTechKey;
  }
}
// get details itemTechKey
detailsObj.prototype.getDetailsItemTechKey = function() {
  return this.itemTechKey;
}
//-----------------------------------------------------------------------------
// set details productkey
detailsObj.prototype.setDetailsProductKey = function(productkey) {
  this.productkey = productkey;
  if (orderLoaded) {
    productkeyPart = eval("positions.document.forms['order_positions'].elements['productkey["+this.id+"]']");
    productkeyPart.value = productkey;
  }
}
// get details productkey
detailsObj.prototype.getDetailsProductKey = function() {
  return this.productkey;
}
//-----------------------------------------------------------------------------
// set details detailContractKey
detailsObj.prototype.setDetailsDetailContractKey = function(detailContractKey) {
  this.detailContractKey = detailContractKey;
  if (orderLoaded) {
    detailContractKeyPart = eval("positions.document.forms['order_positions'].elements['detailContractKey["+this.id+"]']");
    detailContractKeyPart.value = detailContractKey;
  }
}
// get details detailContractKey
detailsObj.prototype.getDetailsDetailContractKey = function() {
  return this.detailContractKey;
}
//-----------------------------------------------------------------------------
// set details detailContractItemKey
detailsObj.prototype.setDetailsDetailContractItemKey = function(detailContractItemKey) {
  this.detailContractItemKey = detailContractItemKey;
  if (orderLoaded) {
    detailContractItemKeyPart = eval("positions.document.forms['order_positions'].elements['detailContractItemKey["+this.id+"]']");
    detailContractItemKeyPart.value = detailContractItemKey;
  }
}
// get details detailContractItemKey
detailsObj.prototype.getDetailsDetailContractItemKey = function() {
  return this.detailContractItemKey;
}
//-----------------------------------------------------------------------------
// set details contractRef
detailsObj.prototype.setDetailsContractRef = function(contractRef) {
  this.contractRef = contractRef;
  if (orderLoaded) {
    contractRefPart = eval("positions.document.forms['order_positions'].elements['contractRef["+this.id+"]']");
    
    if (contractRefPart != null) {
        contractRefPart.value = contractRef;
    }
  }
}
// get details contractRef
detailsObj.prototype.getDetailsContractRef = function() {
  return this.contractRef;
}

// get details ReplaceType
detailsObj.prototype.getDetailsItemReplaceType = function() {
  return positions.document.forms['order_positions'].elements['replacetype['+ this.id +']'].value;
}



// disable the notice textarea
function setNoticeStatus(status) {
  noticeFlag = status;
}

function getNoticeStatus() {
  return noticeFlag;
}



/**
 * Prepare the item for deletion. The deletion of items is done using a
 * normal hyperlink and not an submit button. This makes it necessary to
 * store the technical key of the item to be deleted in an hidden field
 * called delete and submit the form.
 *
 * @param techkey Technical key of the item to be deleted
 */
function deleteItem(techkey) {
    // get references to the documents
    var mainDocument = positions.document;

   // store the techkey in the hidden field
   mainDocument.forms['order_positions'].elements['deletekey'].value = techkey;

   // submit the form
   mainDocument.forms['order_positions'].submit();
}


/**
 * Refresh the data of all frames and submit the main form
 */
function refresh_submit() {
    positions.document.forms['order_positions'].submit();
    return true;
}