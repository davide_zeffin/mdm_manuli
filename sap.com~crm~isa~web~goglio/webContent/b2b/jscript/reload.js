/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       reload.js                          */
/* Description:    reloads new order, templates etc.  */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    03.04.2001 (S.Heimann)             */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/


// switches between order, templates
function reloadFrame(formInputLoc, docLoc) {
  if ((formInputLoc != null) && (formInputLoc != ''))
    parent.form_input.location.href = formInputLoc;
  if ((docLoc != null) && (docLoc != ''))
    parent.documents.location.href = docLoc;
}
