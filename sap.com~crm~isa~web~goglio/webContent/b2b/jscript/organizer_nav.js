/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       new.js                             */
/* Description:    launches new order and catalog     */
/* Version:        1.0                                */
/* Author:         stefan.heimann@pixelpark.com       */
/* Creation-Date:  29.03.2001                         */
/* Last-Update:    21.05.2001 (M.Schley SAP)          */
/*                                                    */
/*                           (c) Pixelpark AG, Berlin */
/*----------------------------------------------------*/

function load_docSearch() {
  parent.organizer_content.location.href=rewriteUrl("/b2b/documentlistselect.do");
//  parent.documents.location.href="open_order_grey_with_open_order2.html";
  parent.form_input.location.href=rewriteUrl("/b2b/empty.jsp");
}
function load_PruductSearch() {
  parent.organizer_content.location.href=rewriteUrl("/b2b/initquicksearch.do");
//  parent.documents.location.href="open_order_grey_with_open_order2.html";
  parent.form_input.location.href=rewriteUrl("/b2b/empty.jsp");
}
function load_catalog() {
  parent.organizer_content.location.href=rewriteUrl("/catalog/logon.do");
  //parent.form_input.location.href="catalog_entry.html";
  parent.parent.work_history.location.href=rewriteUrl("/b2b/catalogstart.do");
  //parent.documents.location.href="open_order_grey_with_open_order2_min.html";
}

function load_ProductSearch() {
  parent.organizer_content.location.href=rewriteUrl("/b2b/initquicksearch.do");
}
