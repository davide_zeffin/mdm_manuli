<%--
/*-----------------------------------------------------------------------------------------------*/
/* Javascript for the "SAP B2B Project"                                                          */
/*                                                                                               */
/* Document:       GSloadNewPage.jsp                                                             */
/* Description:    This document contains a function which is used to load a new Page            */ 
/* Version:        1.0                                                                           */
/* Author:         SAP AG                                                                        */
/* Creation-Date:  26.05.2004                                                                    */
/*                                                                                               */
/*                                                                               (c) SAP AG      */
/*-----------------------------------------------------------------------------------------------*/
--%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.core.SessionConst"%>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.isacore.DocumentHandler"%>
<%@ page import="com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIData" %>
<%@ page import="com.sap.isa.isacore.actionform.order.DocumentListSelectorForm" %>
<%@ page import="com.sap.isa.isacore.action.order.SalesDocumentRemoveAction" %>

<% UserSessionData uSD = UserSessionData.getUserSessionData(request.getSession());
   String showRemoveMessage = (String)uSD.getAttribute(SalesDocumentRemoveAction.RK_REMOVEMESSGAGEFLAG);
   DocumentHandler docHandler = (DocumentHandler)uSD.getAttribute(SessionConst.DOCUMENT_HANDLER);
   String searchName = docHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
   String salesDocSearch = docHandler.getOrganizerParameter("documentsearch.name");
   String initDocSearch = docHandler.getOrganizerParameter("initdocsearch.name");
%>

function GSloadNewPage(object) {
    var selDocType = "";
    var request = "";
    var searchScreen = "<%=searchName%>";
    if (object.type == 'select-one') {
        selDocType = object.options[object.selectedIndex].value;
    }
    if (selDocType == 'INVOICE'  ||
        selDocType == 'CREDITMEMO'  ||
        selDocType == 'DOWNPAYMENT') {
        if (searchScreen != 'SearchCriteria_B2B_Billing') { 
            // Load new page only if not already loaded
            request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Billing";
        }
    }
    if (selDocType == 'ORDER'      ||
        selDocType == 'ORDERTMP'   ||
        selDocType == 'INQUIRY'    ||
        selDocType == 'QUOTATION'   ) {
        if (searchScreen.indexOf('Sales') <= 0) {
            // Load new page only if not already loaded
            request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Sales";
        }
    }
    if (selDocType == 'ORDERITM'      ||
        selDocType == 'ORDTMPITM'   ||
        selDocType == 'QUOTITM'   ) {
        if (searchScreen.indexOf('Items') <= 0) {
            // Load new page only if not already loaded
            request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Items";
        }
    }
    if (selDocType == 'AUCTION') {
    	request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Auction";
    }
    if (selDocType == 'BACKORDER'  &&  searchScreen != 'SearchCriteria_B2B_Sales_Backorder') {
    	request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Sales_Backorder";
    }
    if (selDocType == 'CONTRACT'  &&  searchScreen != 'SearchCriteria_B2B_Contracts') {
	    request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Contracts";
    }
    if (request != "") {
        <%-- Add object attributes to request --%>
        request = request + "&" +object.name + "=" + object.value;
        parent.organizer_content.location.href = request;
    }
}

function GSloadNewPageR3(object) {
    var selDocType = "";
    var request = "";
    var searchScreen = "<%=searchName%>";
    if (object.type == 'select-one') {
        selDocType = object.options[object.selectedIndex].value;
    }
    if (selDocType == 'INVOICE'  ||
        selDocType == 'CREDITMEMO'  ||
        selDocType == 'DOWNPAYMENT') {
        // Load new page only if not already loaded
        if ('<%= initDocSearch %>' == 'SearchCriteria_B2B_Sales' && searchScreen != 'SearchCriteria_B2B_Billing' ) {         
        	request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Billing";
        }
        else if ('<%= initDocSearch %>' == 'SearchCriteria_BOB_Sales' && searchScreen != 'SearchCriteria_BOB_Billing' ) {
        	request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_BOB_Billing";
        }
        else if ('<%= initDocSearch %>' == 'SearchCriteria_Agent_Sales' && searchScreen != 'SearchCriteria_Agent_Billing' ) {
        	request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_Agent_Billing";
        }        
    }
    if (selDocType == 'ORDER'      ||
        selDocType == 'BACKORDER'  ||
        selDocType == 'INQUIRY'    ||
        selDocType == 'QUOTATION'  ||
        selDocType == 'CONTRACT' ) {
        if (searchScreen != '<%= initDocSearch %>') {
            // Load new page only if not already loaded
            request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=<%= initDocSearch %>";
        }
    }
    if (selDocType == 'AUCTION') {
        request = "<isa:webappsURL name="/auction/buyer/search.jsp"/>";
    }
    <%--if (selDocType == 'CONTRACT'  &&  searchScreen != 'SearchCriteria_B2B_Contracts') {
	    request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Contracts";
    }--%>
    if (selDocType == 'ORDERTMP'  &&  searchScreen != 'SearchCriteria_B2B_Ordertemplates') {
        if ('<%= initDocSearch %>' == 'SearchCriteria_BOB_Sales' || '<%= initDocSearch %>' == 'SearchCriteria_Agent_Sales'){           
	    request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_BOB_Ordertemplates";
	    }
	    else{
	    request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_B2B_Ordertemplates";
	    }
    }
    if (request != "") {
    	 <%-- Add object attributes to request --%>
        request = request + "&" +object.name + "=" + object.value;
        parent.organizer_content.location.href = request;
    }
}

function sendRequestAgent() {
    if ( getElementByPtyName('IRT_BDH_PAYER').value == '' )  {
        var oldstyle = getElementByPtyName('IRT_BDH_PAYER').style.borderColor;
        getElementByPtyName('IRT_BDH_PAYER').style.borderColor="red";
        alert( "<isa:UCtranslate key="b2b.gs.billing.requiredentry"/> " );
         getElementByPtyName('IRT_BDH_PAYER').style.borderColor = oldstyle;
    } else {
        sendRequest();
    }
}

function GScountryChange(object) {
    var countryID = getElementByPtyName('COUNTRY').value;

	// initialise request variable
    request = "";
    // set customer_selectby to ADDRESS
    request = "<isa:webappsURL name="/appbase/genericsearch.jsp"/>?<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=SearchCriteria_BOB_Customers&countryChange=true";
    request = request + ("&rc_custselectcountry=" + countryID);	
    parent.organizer_content.location.href = request;
    
}
function removeSalesDocument(techkey, objecttype, removetype, objectid) {
    var confirmMsg = "";
    var chnmodMsg = "";
    if (objecttype == "<%=DocumentListSelectorForm.QUOTATION%>") {
        confirmMsg = "<isa:UCtranslate key="b2b.stat.shuff.conf.remv.quot"/> " + objectid;
        chnmodMsg = "<isa:UCtranslate key="b2b.stat.shuff.remv.doc.chnmod.qt"/> " + objectid;
    } else {
        confirmMsg = "<isa:UCtranslate key="b2b.stat.shuff.conf.remv.ordtmp"/> " + objectid;
        chnmodMsg = "<isa:UCtranslate key="b2b.stat.shuff.remv.doc.chnmod.ot"/> " + objectid;
    }
    if (parent.work_history.documents.editableDoc == true &&  parent.work_history.documents.editableDocNo == objectid) {
        alert(chnmodMsg);
    } else {
        if ( confirm(confirmMsg) ) {
            var x="<isa:webappsURL name ="b2b/salesdocumentremove.do"/>?techkey=" + techkey + "&objecttype=" + objecttype +
                  "&removetype=" + removetype + "&object_id=" + objectid;
            document.location.href="<isa:webappsURL name ="b2b/salesdocumentremove.do"/>?techkey=" + techkey + "&objecttype=" + objecttype +
                                   "&removetype=" + removetype + "&object_id=" + objectid + "&<%=GenericSearchUIData.RC_SEARCHCRITERASCREENNAME%>=<%=searchName%>";
        }
    }
}

<% // Take care for the screen update after a document had been deleted
   if ("X".equals(showRemoveMessage)) { 
       uSD.removeAttribute(SalesDocumentRemoveAction.RK_REMOVEMESSGAGEFLAG);
%>
      findTargetLocation( null, "form_input").location.href = "<isa:webappsURL name="/b2b/updatedocumentview.do"/>";
<% } %>
<% if ("H".equals(showRemoveMessage)) { 
       uSD.removeAttribute(SalesDocumentRemoveAction.RK_REMOVEMESSGAGEFLAG);
%>
      findTargetLocation( null, "_history").location.href = "<isa:webappsURL name="/b2b/history/updatehistory.do"/>";
<% } %>

<%
  // Following takes care a defined function is called at least one time
  String visDocType = (String)uSD.getAttribute("genericsearch.onevisibledoctype");
  String funcToCall = (String)uSD.getAttribute("genericsearch.functiontocall");
  if (visDocType != null &&  funcToCall != null) {
        uSD.removeAttribute("genericsearch.onevisibledoctype");
        uSD.removeAttribute("genericsearch.functiontocall");
%>
  setTimeout("startOnPageLoaded('<%=visDocType%>', '<%=funcToCall%>')", 500);
  
  var alreadyStarted = false;
  function startOnPageLoaded(objID, funcToCall) {
      var obj = document.getElementById(objID);
      if (alreadyStarted != false  ||  obj == null) {
          return true;
      }
      clearTimeout("startOnPageLoaded('<%=visDocType%>', '<%=funcToCall%>')");
      alreadyStarted = true;
      <%=funcToCall.substring(0, funcToCall.indexOf("("))%>(obj);
  }
  
<% } %>
