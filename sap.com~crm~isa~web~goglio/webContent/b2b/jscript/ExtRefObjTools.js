<%--
/*----------------------------------------------------*/
/* Javascript for the "SAP B2B Project"               */
/*                                                    */
/* Document:       extRefObjTools.js                  */
/* Description:    useful functions for the vin popup */
/*                 handling                           */
/* some functions need the isaCore(ecom)base          */
/* functionality                                      */
/* Version:        1.0                                */
/* Author:         SAP                                */
/*----------------------------------------------------*/
--%>
<!--
        function getVinPopup() {
          var url = "";
          var url = rewriteURL("/b2b/showextrefobj.do")+"?level=1&vinvalues="+getVins();
          var sat = window.open(url, 'Fahrgestellnummern', 'width=450,height=450,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
          sat.focus();
        }
        
        function getVins() {
      	  var vinsH;
      	  var fieldH;
      	  var fieldH1;
      	  // get the Elements
      	  fieldH1 =getElement("vinNum");
      	  fieldH = getElement("addVinNum");
      	  if ((fieldH1 != null && fieldH1.value != '') && (fieldH != null && fieldH.value == '')) {
            vinsH = fieldH1.value;
      	  } else if (fieldH1 != null && fieldH != null) {	
      	    vinsH = getCheckedVins(fieldH1.value, fieldH.value);
      	  } else {
      	    vinsH = "";
      	  }
      	  return vinsH;
        }  
        
        function getCheckedVins(vin, vins) {
          var i = 0;
          var firstVIN;
          var checkedVINs;
          i = vins.indexOf(';');
          if (i != -1) {
            firstVIN = vins.substring(0, i);	
          }
          if (vin == firstVIN) {
            checkedVINs = vins;	
          } else {
            checkedVINs = vin.concat(vins.substring(i));
          }
          return checkedVINs;
        }
        
        function getVinPopupItem(itemKey, line) {
          var url = "";
          var url = rewriteURL("/b2b/showextrefobj.do")+"?level=0&itemkey="+itemKey+"&linenum="+line+"&vinvalues="+getVinsItem(line);
          var sat = window.open(url, 'Fahrgestellnummern', 'width=450,height=450,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
          sat.focus();
        
        } 
        
        function getVinPopupNewItem(line) {
          var url = "";
          var url = rewriteURL("/b2b/showextrefobj.do")+"?level=0&linenum="+line+"&vinvalues="+getVinsItem(line);
          var sat = window.open(url, 'Fahrgestellnummern', 'width=450,height=450,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
          sat.focus();        
        }
        
        function getVinsItem(line) {
      	  var vinsI;
      	  var fieldI;
      	  var fieldI1;
      	  var fieldname1;
      	  var fieldname;
      	  fieldname1 = "vinNum_" + line;
      	  fieldname = "addVinNum_" + line;
      	  // get the Elements
      	  fieldI1 = getElement(fieldname1);
      	  fieldI = getElement(fieldname);
      	  if ((fieldI1 != null && fieldI1.value != '') && (fieldI != null && fieldI.value == '')) {
            vinsI = fieldI1.value;
      	  } else if (fieldI1 != null && fieldI != null) {
      	    vinsI = getCheckedVins(fieldI1.value, fieldI.value);
      	  } else {
      	    vinsI = "";
      	  }
      	  return vinsI;
        }  
        
        function getVinPopupH(level, vins) {
          var url = "";
          var url = rewriteURL("/b2b/showextrefobjconfirm.do")+"?level="+level+"&savedvins="+vins;
          var sat = window.open(url, 'Fahrgestellnummern', 'width=450,height=450,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
          sat.focus();      
        } 
        
        function getVinPopupOC() {
          var url = "";
          var url = rewriteURL("/b2b/showextrefobjchange.do")+"?level=1&vinvalues="+getVins();
          var sat = window.open(url, 'Fahrgestellnummern', 'width=450,height=450,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
          sat.focus();
        }

        function getVinPopupItemOC(itemKey, line) {
          var url = "";
          var url = rewriteURL("/b2b/showextrefobjchange.do")+"?level=0&itemkey="+itemKey+"&linenum="+line+"&vinvalues="+getVinsItem(line);
          var sat = window.open(url, 'Fahrgestellnummern', 'width=450,height=450,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
          sat.focus();
        
        } 
        
        function getVinPopupNewItemOC(line) {
          var url = "";
          var url = rewriteURL("/b2b/showextrefobjchange.do")+"?level=0&linenum="+line+"&vinvalues="+getVinsItem(line);
          var sat = window.open(url, 'Fahrgestellnummern', 'width=450,height=450,menubar=no,locationbar=no,scrollbars=yes,resizable=yes');
          sat.focus();        
        } 
        
        function checkDuplicateVin(msg) {
          var vinV;
          var vinH;
          var fieldV;
          var fieldH;
          var doublet = false;
          fieldV = getElement("vinNum");
          fieldH = getElement("addVinNum");
          if ((fieldV != null && fieldV.value != '') && (fieldH != null && fieldH.value != '')) {
            vinV = fieldV.value;
            vinH = fieldH.value;
            doublet = checkDoubleVin(vinV, vinH);
            if (doublet == true) {
              alert(msg);
              var focusString = "document.order_positions.vinNum.focus()";
	      eval(focusString);
              return doublet;
            }	
          }
          return doublet;
        }
        
        function checkDoubVinItem(lines, msg) {
          var vinV;
          var vinH;
          var fieldV;
          var fieldH;
          var doublet = false;
          for (var i = 1; i <= lines; i++) {
      	      fieldNameV = "vinNum_" + i;
      	      fieldNameH = "addVinNum_" + i;
      	      // get the Elements
      	      fieldV = getElement(fieldNameV);
      	      fieldH = getElement(fieldNameH);
              if ((fieldV != null && fieldV.value != '') && (fieldH != null && fieldH.value != '')) {
                  vinV = fieldV.value;
                  vinH = fieldH.value;
                  doublet = checkDoubleVin(vinV, vinH);
                  if (doublet == true) {
                      alert(msg);
                      var focusString = "document.order_positions.vinNum_" + i + ".focus()";
	              eval(focusString);
                      return doublet;
                  }	
              }      	      
	  }
	  return doublet;
        }


        function checkDoubVinItemPos(line, msg) {
          var vinV;
          var vinH;
          var fieldV;
          var fieldH;
          var doublet = false;
          fieldNameV = "vinNum_" + line;
      	  fieldNameH = "addVinNum_" + line;
      	  // get the Elements
      	  fieldV = getElement(fieldNameV);
      	  fieldH = getElement(fieldNameH);
          if ((fieldV != null && fieldV.value != '') && (fieldH != null && fieldH.value != '')) {
              vinV = fieldV.value;
              vinH = fieldH.value;
              doublet = checkDoubleVin(vinV, vinH);
              if (doublet == true) {
                  alert(msg);
                  var focusString = "document.order_positions.vinNum_" + line + ".focus()";
	          eval(focusString);
                  return doublet;
              }	
          }      	      
	  return doublet;
        }
        
        function checkDoubleVin(vin, vins) {
          var doub = false;
          var ende;
          var hit2;
          var hit = vins.indexOf(vin);
	  if (hit > 0) {
              var look = vins.substring(hit - 1);
              hit2 = look.indexOf(';');
              if (0 == hit2) {
                  var end = look.substring(1);
                  var hit3 = end.indexOf(';');
                  if (vin.length == hit3) {
                      doub = true;
                  }
              }
          }
          return doub;
        }  
        
        function check_submit_refresh(lines, msg) { 
            var testH = false; 
            var testI = false;
            testH = checkDuplicateVin(msg);
            testI = checkDoubVinItem(lines, msg);
            if (testH == false && testI == false) {      
                submit_refresh(); 
            }
        }
        
        function checkVinToggle(line, msg) {
            var doubles = checkDoubVinItemPos(line, msg);
            if (doubles == false) {
                toggle("rowdetail_" + line);
            }
        }


	function check_send_confirm(lines, msg)  {
            var testH = false; 
            var testI = false;
            testH = checkDuplicateVin(msg);
            testI = checkDoubVinItem(lines, msg);
            if (testH == false && testI == false) {      
                send_confirm(); 
            }		
	}
	
	function check_send_confirm_header_only(msg) {
	    var testH = false; 
            testH = checkDuplicateVin(msg);
            if (testH == false) {      
                send_confirm_header_only(); 
            }	
	}            
//-->