<%--
********************************************************************************
    File:         determineprocesstypes.inc
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.11.2003
    Version:      1.0

    $Revision: #1 $
    $Date: 2003/11/25 $
********************************************************************************
--%>
<%  // Determination of the process types for orders, quotations and contracts

    ResultData processTypesOrder = (ResultData)userSessionData.getAttribute(ShopReadAction.PROCESS_TYPES);
    int numRowsOrder = 0;
    String processTypeOrder = null;
    String lastProcessTypeOrder = null;  
    if (processTypesOrder != null) {
	numRowsOrder =  processTypesOrder.getNumRows();
        if (numRowsOrder == 1) {
          boolean first = processTypesOrder.first(); 
          processTypeOrder = processTypesOrder.getString(1);
        }
        if (numRowsOrder > 1) {
          boolean last = processTypesOrder.last(); 
          lastProcessTypeOrder = processTypesOrder.getString(1); 
        }    
    }
    
    ResultData processTypesQuotation = (ResultData)userSessionData.getAttribute(ShopReadAction.QUOTATION_PROCESS_TYPES);    
    int numRowsQuotation = 0;
    String processTypeQuotation = null;
    String lastProcessTypeQuotation = null;    
    if (processTypesQuotation != null) {
	numRowsQuotation =  processTypesQuotation.getNumRows();
        if (numRowsQuotation == 1) {
          boolean first = processTypesQuotation.first(); 
          processTypeQuotation = processTypesQuotation.getString(1);
        }
        if (numRowsQuotation > 1) {
           boolean last = processTypesQuotation.last(); 
           lastProcessTypeQuotation = processTypesQuotation.getString(1); 
        }         
    }
     
    ResultData processTypesContract = (ResultData)userSessionData.getAttribute(ShopReadAction.CONTRACT_NEGOTIATION_PROCESS_TYPES);    
    int numRowsContract = 0;
    String processTypeContract = null;  
    String lastProcessTypeContract = null;  
    if (processTypesContract != null) {
        numRowsContract =  processTypesContract.getNumRows();
        if (numRowsContract == 1) {
            boolean first = processTypesContract.first(); 
            processTypeContract = processTypesContract.getString(1);
        }    
        if (numRowsContract > 1) {
           boolean last = processTypesContract.last(); 
           lastProcessTypeContract = processTypesContract.getString(1); 
        }           
    }       
    
%>




