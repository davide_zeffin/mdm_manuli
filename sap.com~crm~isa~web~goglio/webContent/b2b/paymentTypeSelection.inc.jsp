<%@ page import="com.sap.isa.isacore.action.PaymentActions" %>



    <script type="text/javascript">
    <!--

      // the user decides to pay by credit card
      function enableCreditCardInput(line_number) {
        for (i=0;i<line_number;i++) {
          <% // name in this case was the name of the frame itself (this.name)! So another variable-name is necessary! %>
          var nameX="nolog_cardtype["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
          nameX="nolog_cardholder["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
          nameX="nolog_cardno["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
          nameX="nolog_cardcvv["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
          nameX="nolog_cardno_suffix["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
          nameX="nolog_cardmonth["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
          nameX="nolog_cardyear["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
          nameX="authlimit["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
          nameX="deleteflag["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = false;
        }
      }

      // the user decides to pay not by credit card
      function disableCreditCardInput(line_number) {
        <% // name in this case was the name of the frame itself (this.name)! So another variable-name is necessary! %>      
        for (i=0;i<line_number;i++) {
          var nameX="nolog_cardtype["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
          nameX="nolog_cardholder["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
          nameX="nolog_cardno["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
          nameX="nolog_cardcvv["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
          nameX="nolog_cardno_suffix["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
          nameX="nolog_cardmonth["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
          nameX="nolog_cardyear["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
          nameX="authlimit["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
          nameX="deleteflag["+i+"]";
          if (getElement(nameX)) 
          getElement(nameX).disabled = true;
        }
      }
           
     //-->
    </script>

    
    
       <p><span><isa:translate key="payment.maintain.txt1"/></span></p>
       <!-- Payment type Start -->                         
                          <% if (paymentUI.isInvoiceAvailable()) { %>
                              <p>
                                <% if (paymentUI.getNumberOfTypes() > 1) { %>
                                <input type="radio"
                                       name="paytype"
                                       id="paytype_<%= paymentUI.getInvoiceTypeAsString() %>"
                                       value="<%= paymentUI.getInvoiceTypeAsString() %>"                                                     
                                       onclick="disableCreditCardInput(<%=paymentUI.getNumberOfLines()%>)"
                                       <%= paymentUI.getChecked(paymentUI.getInvoiceType()) %> 
                                 />
                                <% }
                                else { %>
                                <input type="hidden"
                                       name="paytype"
                                       value="<%= paymentUI.getInvoiceTypeAsString() %>" />
                                <% } %>
                                <label for="paytype_<%= paymentUI.getInvoiceTypeAsString() %>">
                                <isa:translate key="payment.type.invoice"/>
                                </label>
                              </p>
                          <%
                          }
                          if (paymentUI.isCODAvailable()) {
                          %>
                              <p>
                                <% if (paymentUI.getNumberOfTypes() > 1) { %>
                                <input type="radio"
                                       name="paytype"
                                       id="paytype_<%= paymentUI.getCODTypeAsString() %>"
                                       value="<%= paymentUI.getCODTypeAsString() %>"                                       
                                       onclick="disableCreditCardInput(<%=paymentUI.getNumberOfLines()%>)" 
                                       <%= paymentUI.getChecked(paymentUI.getCODType()) %> 
                                />
                                <% }
                                else { %>
                                <input type="hidden"
                                       name="paytype"
                                       value="<%= paymentUI.getCODTypeAsString() %>" />
                                <% } %>
                                <label for="paytype_<%= paymentUI.getCODTypeAsString() %>">
                                <isa:translate key="payment.type.cod"/>
                                </label>
                              </p>
                          <%
                          }
                          if (paymentUI.isCardAvailable()) {
                          %>
                              <p>
                                <% if (paymentUI.getNumberOfTypes() > 1) { %>
                                <input type="radio"
                                       name="paytype"
                                       id="paytype_<%= paymentUI.getCardTypeAsString() %>"
                                       value="<%= paymentUI.getCardTypeAsString() %>"                                       
                                       onclick="enableCreditCardInput(<%=paymentUI.getNumberOfLines()%>)"
                                       <%= paymentUI.getChecked(paymentUI.getCardType()) %> 
                                />
                                <% }
                                else { %>
                                <input type="hidden"
                                       name="paytype"
                                       value="<%= paymentUI.getCardTypeAsString() %>" />
                                <% } %>
                                <label for="paytype_<%= paymentUI.getCardTypeAsString() %>">
                                <isa:translate key="payment.type.paycard"/>
                                </label>
                              </p>
                          <% } %>                         
     <!-- Payment type End -->    
    