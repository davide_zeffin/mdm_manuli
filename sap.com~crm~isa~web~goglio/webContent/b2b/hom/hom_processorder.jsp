
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.businesspartner.action.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.order.hom.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.orderdownload.*" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.PaymentUI" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*"%>
<%@ page import="com.sap.isa.payment.businessobject.PaymentCCard"%>


<% ProcessCustomerOrderUI ui = new ProcessCustomerOrderUI(pageContext);
   HeaderSalesDocument  header = ui.header;
	ui.isB2BSupported=false;
   String itemStatusKey = "";
   //inline display of product configuration will be restricted on values of identifying characteristics
   boolean showDetailView = false;
%>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
<head>
  <title><isa:translate key="b2b.order.display.pagetitle"/></title>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

  <isa:stylesheets/>

  <script type="text/javascript">
          <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
  </script>

  <%-- include for locale calendar control --%>
  <%@ include file="../jscript/setLocaleCalendarSettings.jsp.inc" %>

  <script src="<isa:mimeURL name="b2b/jscript/table_highlight.js" />"
                  type="text/javascript">
  </script>
  <script src="<isa:mimeURL name="b2b/jscript/reload.js" />"
                  type="text/javascript">
  </script>
  <script src="<isa:mimeURL name="b2b/jscript/frames.js" />"
                  type="text/javascript">
  </script>
  <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/browsersniff.js") %>"
                  type="text/javascript">
  </script>
  <script src="<%=WebUtil.getMimeURL(pageContext, "auction/jscript/calendar.js") %>"
                  type="text/javascript">
  </script>
  
 <%if(ui.getShop().isPricingCondsAvailable() == true) {%>
	 <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
		   type = "text/javascript">
	 </script>
<% } %>
  
  <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
                  type="text/javascript">
  </script>

  <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
                  type="text/javascript">
  </script>

  <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderToolsRetkey.js") %>"
          type="text/javascript">
  </script>

  <%@ include file="/b2b/jscript/orderTools.inc.jsp" %>

  <script type="text/javascript">
  <!--
    function send_confirm_process() {
      check = confirm('<isa:UCtranslate key="<%= ui.getConfirmKey() %>"/>');

      if(check) {
        document.forms["order_positions"].sendpressed.value="true";
        document.forms["order_positions"].submit();
      }

      return check;
    }


    function showShipToForKey(techkey) {
          var url = "<isa:webappsURL name="b2b/showshipto.do"/>?<%=ShowShipToAction.PARAM_SHIPTO_TECHKEY%>=" + techkey;
          openWinExtended(url, '450','450');
          return true;
    }

    function showShipTo(index) {
          var url = "<isa:webappsURL name="b2b/showshipto.do"/>?<%=ShowShipToAction.PARAM_SHIPTO_INDEX%>=" + index;
          openWinExtended(url, '450','450');
          return true;
    }

    function showShipToForCollectiveOrder(index) {
          var url = "<isa:webappsURL name="/b2b/collectiveordershowshipto.do"/>?<%=ShowShipToAction.PARAM_SHIPTO_INDEX%>=" + index;
          openWinExtended(url, '450','450');
          return true;
    }


    function setDisableEnable(idName, actionActive, disable) {

      quantityActive = getElement(idName + "quantity" + actionActive);
      unitActive = getElement(idName + "unit");
      reqdeliverydateActive = getElement(idName + "reqdeliverydate");
      commentActive = getElement(idName + "comment");

      quantityActive.disabled=disable;
      reqdeliverydateActive.disabled=disable;
      commentActive.disabled=disable;
      if (unitActive != null) {
        unitActive.disabled=disable;
      }
    }


    var titleReorder = "<isa:UCtranslate key="hom.jsp.procorder.reorder.title" />";
    var titleForward = "<isa:UCtranslate key="hom.jsp.procorder.forward" />";


    function adjustStatus(line) {
      itemstatus = getElement("itemextstatus["+line+"]");
      tempitemstatus = getElement("tempitemextstatus_"+line);
      itemstatus.value = tempitemstatus.value;
    }

    function processItem(idName, actionActive, actionInActive, statusDescription, statusValue, line, isChangeable) {

      var openIconActive = idName + "open" + actionActive;
      var closeIconActive = idName + "close" + actionActive;


      var savedprod = getElement("savedproduct["+line+"]");
      var product = getElement("product["+line+"]");
      var itemstatus = getElement("itemextstatus["+line+"]");
      var targetActive = getElement(idName);

      var checkBoxName = "select_["+line+"]";


      // show the correct shipTo area
      hideElement(idName + "shipto" + actionInActive);
      showElement(idName + "shipto" + actionActive);

      // rename input field for the shipTo
      var shipToInput = getElement(idName + "customer" + actionInActive);
      shipToInput.name="customerxx["+line+"]";

      shipToInput = getElement(idName + "customer" + actionActive);
      shipToInput.name="customer["+line+"]";

      // show the correct quantity area
      hideElement(idName + "quantityArea" + actionInActive);
      showElement(idName + "quantityArea" + actionActive);

      // rename input field for the quantity
      var quantityInput = getElement(idName + "quantity" + actionInActive);
      quantityInput.name="quantityxx["+line+"]";

      quantityInput = getElement(idName + "quantity" + actionActive);
      quantityInput.name="quantity["+line+"]";

      titleId = idName + "title";
      if (actionActive == "Reorder") {
        setText(titleId, titleReorder);
      }
      else {
        setText(titleId, titleForward);
      }

      if (targetActive.style.display == "none") {
        showElement(idName);
        hideElement(openIconActive);
        showElement(closeIconActive);

        // hidden unallowed icons and show disabled icon
        hideElement(idName + "open" + actionInActive);
        showElement(idName + "open" + actionInActive + "Disabled");

        // hide check box
        hideElement(checkBoxName);

        var checkBox = getElement(checkBoxName);
        checkBox.checked=false;

        // adjust status
        showElement("itemstatus_"+line);
        hideElement("tempitemextstatus_"+line);

        setText("itemstatus_"+line, statusDescription);

        itemstatus.value = statusValue;

        product.value = savedprod.value;

        if (isChangeable) {
          setDisableEnable(idName, actionActive, false);
        }
        else {
          setDisableEnable(idName, actionActive, true);
        }
      }
      else {
        hideElement(idName);
        showElement(openIconActive);
        hideElement(closeIconActive);

        // hidden unallowed icons including disabled icon
        showElement(idName + "open" + actionInActive);
        hideElement(idName + "open" + actionInActive + "Disabled");

        // adjust status
        showElement("tempitemextstatus_"+line);
        hideElement("itemstatus_"+line);

        // show checkbox
        showElement(checkBoxName);

        product.value = "";

        setDisableEnable(idName, actionActive, true);

        adjustStatus(line);

      }
    }

    function toggleDetail(idName) {
      isaEasyToggle(idName);
    }


    function checkAll(){
      var newValue = document.forms["order_positions"].itemcheckbox0.checked;
      for(var i=0;i<document.forms["order_positions"].elements.length;i++) {
        var e = document.forms["order_positions"].elements[i];
        if(e.name.substring(0, 8)=='select_[' && !e.disabled)
          e.checked=newValue;
      }
    }

    function checkEvent(box) {
      if(box.checked == false) {
              document.forms["order_positions"].itemcheckbox0.checked=false;
      }
    }

    function showAll() {
      temp = "";
      for(i=0; i<document.forms["order_positions"].elements.length; ++i) {
              temp += document.forms["order_positions"].elements[i].name+ " = " +document.forms["order_positions"].elements[i].value+ "\n";
      }
      alert (temp);
    }

    function setStatusBox(idName, status, line) {
      savedprod = getElement("savedproduct["+line+"]");
      product = getElement("product["+line+"]");
      itemstatus = getElement("itemextstatus["+line+"]");
      target = getElement(idName);

      if (target.style.display == "inline") {
        for(i=0;i<target.length;++i) {
                if(target.options[i].value=="") {target.options[i].selected = true;}
        }
        product.value="";
        itemstatus.value = "";
        target.disabled = false;
      }
      else {
        for(i=0;i<target.length;++i) {
                if(target.options[i].value.substring(target.options[i].value.length-4,target.options[i].value.length) == status) {target.options[i].selected = true;}
        }
        product.value = savedprod.value;
        itemstatus.value = target.value;
        target.disabled = true;
      }
    }

    function newitemconfig(itemId) {
      document.forms["order_positions"].confignewitemid.value=itemId;
      document.forms["order_positions"].submit();
      return true;
    }


    <%-- needed for Order Download --%>
    function prepareDownload() {
      // document.forms["orderdownload"].orderIDS.value = document.order_positions.salesdocnumber.value;
      // document.forms["orderdownload"].orderGUIDS.value = document.order_positions.techkey.value;
      // document.forms["orderdownload"].submit();
      return true;
    }

    // function for the onload()-event
    function loadPage() {
          setLocaleSpecificData();
    }

    function set_state() {

      var newValue = document.forms['changeState'].elements['newstatus'].value;
      var selInd   = document.forms['changeState'].elements['newstatus'].selectedIndex;
      var newValueDescription = document.forms['changeState'].elements['newstatus'].options[selInd].text;
      var mainDocument = document;

      if (newValue!="") {

        var line  = 1;

        var tmpStatus = mainDocument.all("tempitemextstatus_" +line);
        var allchanged = true;

        while (tmpStatus != null) {

          var checkBoxElement =  mainDocument.all("select_["+line+"]");
          if (checkBoxElement.checked) {
            // check, if value is allowed!
            var found = false;
            var currentStatusDescription = "";
            for(j=0;j<tmpStatus.length;j++) {
              if(tmpStatus.options[j].value == newValue) {
                found = true;
                checkBoxElement.checked = false;
                tmpStatus.value = newValue;
                itemstatus = mainDocument.all("itemextstatus["+line+"]");
                itemstatus.value = newValue;
              }
              // Because the current status value is always empty, we compare with the description
              if (tmpStatus.options[j].value.length == 0) { 
                  currentStatusDescription = tmpStatus.options[j].text;
                  if (newValueDescription == currentStatusDescription) {
                      found = true;
                      checkBoxElement.checked = false;
                      tmpStatus.value = "";
                      itemstatus = mainDocument.all("itemextstatus["+line+"]");
                      itemstatus.value = "";
                  }
              }
            }
            if (!found) {
              allchanged = false;
            }
          }
          // next item
          line += 1;
          tmpStatus = mainDocument.all("tempitemextstatus_" +line);
        }

        mainDocument.all("checkbox0").checked=false;
        if (!allchanged) {
          alert("<isa:UCtranslate key="hom.jsp.stateChange.notAllowed"/>");
        }
      }
    }

  //-->

  </script>

</head>

<body class="order" onload="loadPage();miRegisterObjects();">

  <div class="module-name"><isa:moduleName name="b2b/hom/hom_processorder.jsp" /></div>

  <%--
    The body consists of two layers:
    - the document layer, containing the header and the items of the document. It is scrollable.
    - the buttons layer, containig the buttons for the document. It stays at the bottom of the page.
  --%>

    <%-- Document information --%>
    <h1><isa:translate key="hom.jsp.gen.edit.customer.edit"/>&nbsp;<isa:translate key="hom.jsp.gen.edit.customer.order"/>
         <%= JspUtil.removeNull(header.getSalesDocNumber()) %>&nbsp;<isa:translate key="hom.jsp.gen.edit.cust.order.date"/>&nbsp;<%= header.getChangedAt()%>&nbsp;</h1>

    <div id="document">
    
    <%-- !!!! Do not change the name of the form, the JavaScript functions rely on them !!!! --%>
    <form action="<isa:webappsURL name="b2b/maintainprocesscustomerorder.do"/>"
        id="order_positions"
        name="order_positions"
        method="post">    
    
  <%-- Show accessibility messages --%>
  <%
  if (ui.isAccessible) { %>
    <%@ include file="/appbase/accessibilitymessages.inc.jsp"  %>
  <%
  } %>    
    
    <%--
        The document layer contains:
        - the document-header layer, which holds the header information of the document
        - the document-items layer, which contains all items
    --%>
      
      <div class="document-header">
        <%-- Accesskey for the header section. --%>
        <a href="#" title="<isa:translate key="hom.jsp.prcorder.pagebegin"/>" accesskey="<isa:translate key="hom.jsp.prcorder.pagekey"/>"></a>

        <div>
          <input type="hidden" name="headerReqDeliveryDate" value="<%= JspUtil.replaceSpecialCharacters(ui.header.getReqDeliveryDate()) %>"/>
          <isacore:requestSerial mode="POST"/>
        </div>


        <div class="header-basic">
          <%--
            The document-header layer contains:
            - the data table. It displays the title of the document and the standard input fields like <Your Reference:>, <Deliver To>, ...
            - the price table. It displays price information like <Total price net>, <Freight>, <Taxes> ...
            - the message table. It displays the textarea for the message entry <Your message to us>.
            - the error layer (optionally), if there are errors.
          --%>
        <%
		String prcorder_table_data_title = WebUtil.translate(pageContext, "b2b.hom.prcorder.tbldata", null);
        // Accessibility requirement
		if (ui.isAccessible()) 
		{
			int prcorder_table_data_col_nro = 0;
			if ( ui.isHeaderValidToAvailable() ) 
				prcorder_table_data_col_nro = 2;
			int prcorder_table_data_row_nro = 6;
				if (ui.paytypeIsCard)
				prcorder_table_data_row_nro++;
	        String prcorder_table_data_col_cnt = Integer.toString(prcorder_table_data_col_nro);
	        String prcorder_table_data_row_cnt = Integer.toString(prcorder_table_data_col_nro);
        %>
          <a id="prcorder_table_data_begin"
             href="#prcorder_table_data_end"
             title="<isa:translate key="access.table.begin" arg0="<%=prcorder_table_data_title%>" arg1="<%=prcorder_table_data_row_cnt%>" arg2="<%=prcorder_table_data_col_cnt%>" arg3="1" arg4="<%=prcorder_table_data_row_cnt%>"/>">
          </a>
        <%
        }
        %>
          <table class="layout"> <%-- level: sub3 --%>
            <tr>
              <td class="col1">

                  <table class="header-general" summary="<%=prcorder_table_data_title%>"><%-- level: sub3 --%>

                    <% if ( ui.isHeaderValidToAvailable() && ui.isElementVisible("order.validTo")) { %>
                       <tr>
                          <td class="identifier"><isa:translate key="status.sales.detail.validTo"/>:</td>
                          <td class="value"><%= ui.header.getValidTo()%></td>
                       </tr>
                    <% } %>

                    <%-- Customer Info --%>
              <%
				String soldName[] = {JspUtil.encodeHtml(ui.getSoldToName())};
				String showSold = WebUtil.translate(pageContext, "hom.jsp.orderdtl.showSold", soldName);
              %>
                    <% if ( ui.isElementVisible("order.soldTo") ) { %>
	                    <tr>
	                      <td class="identifier"><strong><isa:translate key="hom.jsp.processorder.customer"/></strong></td>
	                      <td class="value">
	                        <a title="<isa:translate key="access.link" arg0="<%=showSold%>" arg1=""/>" href="#" onclick="javascript:showSoldTo('<%=ui.getSoldToKey().getIdAsString()%>')"><%= JspUtil.removeNull(ui.getSoldToId()) %></a>
	                          <%= JspUtil.encodeHtml(ui.getSoldToName()) %>
	                      </td>
	                    </tr>
	                <% } %>

                    <%-- Reference Number --%>
	                <% if ( ui.isElementVisible("order.numberExt") ) { %>    
	                    <tr>
	                       <td class="identifier"><isa:translate key="hom.jsp.processorder.ref.short"/>&nbsp;</td>
	                       <td class="value"><%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %></td>
	                    </tr>
                    <% } %>
                    
                    <%-- Description --%>
	                <% if ( ui.isElementVisible("order.description") ) { %>    
	                    <tr>
	                       <td class="identifier"><isa:translate key="hom.jsp.processorder.description"/></td>
	                       <td class="value"><%= JspUtil.encodeHtml(ui.header.getDescription()) %></td>
	                    </tr>
                    <% } %>

                    <%-- Shipping conditions --%>
	                <% if (ui.isElementVisible("order.shippingCondition")) { %>
	                    <tr>
	                      <td class="identifier"><isa:translate key="b2b.order.display.shipcond"/></td>
	                      <td class="value">
	                        <isa:iterate id="shipCond" name="<%= MaintainOrderBaseAction.RC_SHIPCOND %>"
	                                                 type="com.sap.isa.core.util.table.ResultData">
	                              <%
	                              if (shipCond.getRowKey().toString().equals(ui.header.getShipCond())) {  %>
	                                               <%= JspUtil.encodeHtml(shipCond.getString(1)) %>
	                              <%
	                              } %>
	                        </isa:iterate>
	                      </td>
	                    </tr>
                    <% } %>
                    
                    <%-- Ship To --%>
	                <% if (ui.isElementVisible("order.deliverTo")) { %>    
	                    <tr>
	                      <td class="identifier"><isa:translate key="b2b.order.display.soldto"/></td>
	                      <td class="value">
	                        <%
	                        if (ui.isCollectionFromVendor) { %>
	                          <isa:translate key="hom.jsp.CollectionFromVend"/>
	                        <%
	                        }
	                        else { %>
	                          <a title="<isa:translate key="hom.jsp.prcorder.showShip"/>" class="icon" href="#" onclick="showShipToForKey('<%=ui.header.getShipTo()!=null?ui.header.getShipTo().getTechKey().getIdAsString():""%>');" ><%= ui.header.getShipTo()!=null ? JspUtil.encodeHtml(ui.header.getShipTo().getShortAddress()):""%></a>
	                        <%
	                        } %>
	                      </td>
	                    </tr>
                    <% } %>
                    
                    <%-- ui.payment infos start --%>
	                <% if (ui.isElementVisible(PaymentUI.PAYMENT_TYPE)) { %> 
	                    <tr>
	                      <td class="identifier"><isa:translate key="b2b.order.details.paymenttype"/></td>
	                      <td class="value">
	                         <isa:translate key="<%= ui.paymentKey %>"/>
	                            <%
	                            if (ui.paytypeIsCard) { %>
	                                <a href="javascript: togglepayment()"
	                                       title="<isa:translate key="b2b.order.icon.newshipto"/>">
	                                <img id="payment_close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.creditcard.hidedetails"/>" width="16" height="16"/><img id="payment_open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.creditcard.showdetails"/>" width="16" height="16"/></a>
	                            <%
	                            } %>
	                      </td>
	                    </tr>
                    <% } %>	                    
                    <%
                    if (ui.paytypeIsCard) { %>
                      <tr id="payment_row" style="DISPLAY:none"> <%-- style="DISPLAY:none" --%>
                        <td>&nbsp;</td>
                        <td>
                          <table>
                             <% if (ui.isElementVisible(PaymentUI.ONECARD_TYPE)) { %>
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.credCardInst"/>&nbsp;</span></td>
	                              <%// list of all credit card institutes
	                                ResultData listOfCardInst = (ResultData) request.getAttribute(MaintainOrderBaseAction.RC_CARDTYPE);
	                              %>
	                              <td><span><%= (((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getTypeDescription()) %></span></td>
	                            </tr>
	                         <% } %>
                             <% if (ui.isElementVisible(PaymentUI.ONECARD_HOLDER)) { %>	                                            
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.creditCardOwner"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getHolder())%></span></td>
	                            </tr>
	                         <% } %>
                             <% if (ui.isElementVisible(PaymentUI.ONECARD_NUMBER)) { %>	                      	                      
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.creditCardNumber"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getNumber())  %></span></td>
	                            </tr>
	                         <% } %>   
	                         <% if (ui.isElementVisible(PaymentUI.ONECARD_SUFFIX)) { %>	                                            
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.swtchCardSrNo"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getNumberSuffix()) %></span></td>
	                            </tr>
	                         <% } %>   
	                         <% if (ui.isElementVisible(PaymentUI.ONECARD_VALIDITY)) { %>	                      	                      
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.CCValidity"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getExpDateMonth() + "/" + ((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getExpDateYear()) %></span></td>
	                            </tr>
	                         <% } %>  
	                         <% if (ui.isElementVisible(PaymentUI.CARD_CVV)) { %>	                      	                      
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.crdtCardCVV"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getCVVAsStars()) %></span></td>	                      
	                            </tr>
	                         <% } %>                          
                          </table>
                        </td>
                      </tr>
                    <%
                    } %>

                  </table>
                  <% if (ui.isAccessible()) { %>
                     <a id="prcorder_table_data_end" href="#prcorder_table_data_begin" title="<isa:translate key="access.table.end" arg0="<%=prcorder_table_data_title%>"/>"></a>
                  <% } %>

                <%--End table data--%>
              </td>
              <td class="col2">
	              <%--Status Table--%>
                  <% if ( ui.isElementVisible("order.status.overall")) { %>      
				        <%
						String prcorder_tblstatus_title = WebUtil.translate(pageContext, "b2b.hom.prcorder.tblstatus", null);
				        // Accessibility requirement
						if (ui.isAccessible()) 
						{
					        int prcorder_tblstatus_col_nro = 2;
							int prcorder_tblstatus_row_nro = 1;
					        String prcorder_tblstatus_col_cnt = Integer.toString(prcorder_tblstatus_col_nro);
					        String prcorder_tblstatus_row_cnt = Integer.toString(prcorder_tblstatus_col_nro);
				        %>
				          <a id="prcorder_tblstatus_begin"
				             href="#prcorder_tblstatus_end"
				             title="<isa:translate key="access.table.begin" arg0="<%=prcorder_tblstatus_title%>" arg1="<%=prcorder_tblstatus_row_cnt%>" arg2="<%=prcorder_tblstatus_col_cnt%>" arg3="1" arg4="<%=prcorder_tblstatus_row_cnt%>"/>">
				          </a>
				        <%
				        }
				        %>                  
	                  <table class="status" summary="<%=prcorder_tblstatus_title%>"> <%-- level: sub3 --%>
	                      <tr>
	                         <td class="identifier"><isa:translate key="hom.jsp.procorder.customer.state"/>:</td>
	                         <td class="value"><isa:translate key="<%= ui.getDocumentStatusKey() %>"/></td>
	                      </tr>
	                  </table>
	
	                 <% if (ui.isAccessible()) { %>
	                     <a id="prcorder_tblstatus_end" href="#prcorder_tblstatus_begin" title="<isa:translate key="access.table.end" arg0="<%=prcorder_tblstatus_title%>"/>"></a>
	                  <% } %>
   	              <% } %>
                  <%--End Status--%>



                  <%--Price Table--%>
			        <%
					String prcorder_tblprice_title = WebUtil.translate(pageContext, "b2b.hom.prcorder.tblprice", null);
			        // Accessibility requirement
					if (ui.isAccessible()) 
					{
				        int prcorder_tblprice_col_nro = 2;
						int prcorder_tblprice_row_nro = 0;						
						if (ui.isNetValueAvailable())
							prcorder_tblprice_row_nro++;
						if (ui.isFreightValueAvailable())
							prcorder_tblprice_row_nro++;
						if (ui.isTaxValueAvailable())
							prcorder_tblprice_row_nro++;
						if (ui.isGrossValueAvailable())
							prcorder_tblprice_row_nro++;
				        String prcorder_tblprice_col_cnt = Integer.toString(prcorder_tblprice_col_nro);
				        String prcorder_tblprice_row_cnt = Integer.toString(prcorder_tblprice_col_nro);
			        %>
			          <a id="prcorder_tblprice_begin"
			             href="#prcorder_tblprice_end"
			             title="<isa:translate key="access.table.begin" arg0="<%=prcorder_tblprice_title%>" arg1="<%=prcorder_tblprice_row_cnt%>" arg2="<%=prcorder_tblprice_col_cnt%>" arg3="1" arg4="<%=prcorder_tblprice_row_cnt%>"/>">
			          </a>
			        <%
			        }
			        %>                  
                  <table class="price-info" summary="<%=prcorder_tblprice_title%>"> <%-- level: sub3 --%>
                    <% if (ui.isNetValueAvailable() && ui.isElementVisible("order.netValueWOFreight")) { %>
                      <tr>
                       <td class="identifier" scope="row"><isa:translate key="b2b.order.display.pricenet"/></td>
                       <td class="value"><%= JspUtil.removeNull(ui.header.getNetValueWOFreight()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %>&nbsp;</td>
                      </tr>
                    <% } %>
                    <% if (ui.isFreightValueAvailable() && ui.isElementVisible("order.freight")) { %>
                      <tr>
                        <td class="identifier"  scope="row"><isa:translate key="b2b.order.display.freight"/>&nbsp;</td>
                        <td class="value"><%= JspUtil.removeNull(ui.header.getFreightValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %></td>
                      </tr>
                    <% } %>
                    <% if (ui.isTaxValueAvailable() && ui.isElementVisible("order.tax")) { %>
                      <tr>
                        <td class="identifier" scope="row"><isa:translate key="b2b.order.display.tax"/>&nbsp;</td>
                        <td class="value"><%= JspUtil.removeNull(ui.header.getTaxValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %></td>
                      </tr>
                    <% } %>
                    <% if (ui.isGrossValueAvailable() && ui.isElementVisible("order.priceGross")) { %>
                      <tr>
                        <td colspan="2" class="separator"></td>
                      </tr>
                      <tr>
                        <td class="identifier" scope="row"><isa:translate key="b2b.order.display.pricebrut"/>&nbsp;</td>
                        <td class="value"><%= JspUtil.removeNull(ui.header.getGrossValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %></td>
                      </tr>
                    <% } %>
                  </table>

                  <% if (ui.isAccessible()) { %>
                     <a id="prcorder_tblprice_end" href="#prcorder_tblprice_begin" title="<isa:translate key="access.table.end" arg0="<%=prcorder_tblprice_title%>"/>"></a>
                  <% } %>
                  <%--End Price--%>
              </td>
            </tr>
          </table> <%-- class="layout"> --%>
        </div> <%--header-basic--%>


        <%--Data Message--%>
	    <% if (ui.isElementVisible("order.comment")) { %>
		    <%
			String prcorder_tblmessage_title = WebUtil.translate(pageContext, "b2b.hom.prcorder.tblmessage", null);
		    // Accessibility requirement
			if (ui.isAccessible()) 
			{
		        int prcorder_tblmessage_col_nro = 2;
				int prcorder_tblmessage_row_nro = 1;
		        String prcorder_tblmessage_col_cnt = Integer.toString(prcorder_tblmessage_col_nro);
		        String prcorder_tblmessage_row_cnt = Integer.toString(prcorder_tblmessage_col_nro);
		    %>
		      <a id="prcorder_tblmessage_begin"
		         href="#prcorder_tblmessage_end"
		         title="<isa:translate key="access.table.begin" arg0="<%=prcorder_tblmessage_title%>" arg1="<%=prcorder_tblmessage_row_cnt%>" arg2="<%=prcorder_tblmessage_col_cnt%>" arg3="1" arg4="<%=prcorder_tblmessage_row_cnt%>"/>">
		      </a>
		    <%
		    }
		    %>  
	        <table class="message" summary="<isa:translate key="b2b.acc.header.msg.sum"/>">
	          <tr>
	            <td class="identifier"><isa:translate key="hom.jsp.proc.customer.message"/></td>
	            <td class="value">
	              <textarea id="headerComment" rows="1" cols="80" onfocus="openremark()" name="headerComment" disabled="disabled" title="<%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText()):"" %>"><%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText()):"" %></textarea>
	              <a class="icon" title="<isa:translate key="hom.jsp.orderdtl.toggleMsg"/>" href="javascript: toggleremark()">
	                 <img id="remark_close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.remark.showdetails"/>" width="16" height="16"/>
	                 <img id="remark_open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.remark.hidedetails"/>" width="16" height="16"/></a>
	            </td>
	          </tr>
	        </table>
	        <%--End Data Message--%>
	        <% if (ui.isAccessible()) { %>
	          <a id="prcorder_tblmessage_end" href="#prcorder_tblmessage_begin" title="<isa:translate key="access.table.end" arg0="<%=prcorder_tblmessage_title%>"/>"></a>
	        <% } %>
	    <% } %>

        <%--Error--%>
        <% if (ui.docMessageString.length() > 0) { %>
            <div class="error">
               <span><%= JspUtil.encodeHtml(ui.docMessageString) %></span>
            </div>
        <% } %>

      </div> <%--End  Document Header--%>


      <%--Item List--%>
      <div class="document-items"> <%-- level: sub2 --%>
        <%--
        The document-items layer contains:
        - a table itemlist, which displays all items.
        --%>


        <%
        ItemList items = (ItemList) request.getAttribute(MaintainOrderBaseAction.RC_ITEMS);
        // Accessibility requirement
        int prcorder_tblitems_col_nro = 10;
        String prcorder_tblitems_col_cnt = Integer.toString(prcorder_tblitems_col_nro);
        String prcorder_tblitems_row_cnt = Integer.toString(items.size());
        String prcorder_tblitems_title = WebUtil.translate(pageContext, "hom.jsp.prcorder.tblitems", null);
        %>

        <%
        if (ui.isAccessible()) { %>
	      <a id="prcorder_tblitems_begin"
	         href="#prcorder_tblitems_end"
	         title="<isa:translate key="access.table.begin" arg0="<%=prcorder_tblitems_title%>" arg1="<%=prcorder_tblitems_row_cnt%>" arg2="<%=prcorder_tblitems_col_cnt%>" arg3="1" arg4="<%=prcorder_tblitems_row_cnt%>"/>">
	      </a>
        <%
        }%>

        <%-- the item information for the basket --%>
        <table class="itemlist" summary="<%=prcorder_tblitems_title%>">
          <thead>
            <tr>
              <th class="opener" scope="col" rowspan="2">&nbsp;
                 <% int numberOfRows = ui.items.size() + ui.newpos;
                    int startIndex   = 1; %>
                 <a class="icon" href='javascript: toggleAllItemDetails("rowdetail_all","rowdetail_", <%= numberOfRows%>, <%= startIndex %>)' >
                                           <img class="img-1" id="rowdetail_allclose" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.close"/> <% } %>"/>
                                           <img class="img-1" id="rowdetail_allopen" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.open"/> <% } %>"/>
                                 </a>
              </th>
              <th class="check" scope='col' rowspan="2">
                <input type="checkbox" id="checkbox0" onclick="checkAll();" name="itemcheckbox0" title="<isa:translate key="hom.jsp.prcorderr.markall"/>" value="ON"/>&nbsp;
              </th>
              <th class="item" scope='col' rowspan="2">
                <isa:translate key="b2b.order.display.posno"/>
              </th>
              <th class="product" scope='col'  rowspan="2">
                <isa:translate key="b2b.order.display.productno"/>
              </th>
              <th class="qty" scope='col'  rowspan="2">
                <isa:translate key="b2b.order.display.quantity"/>
              </th>
              <th class="desc" scope='col'  rowspan="2">
                <isa:translate key="b2b.order.display.productname"/>
              </th>
              <th class="price" scope='col'  rowspan="2">
                <isa:translate key="b2b.order.display.totalprice"/>
                <isa:translate key="b2b.order.details.price"/>
              </th>
              <th class="date-on" scope='col'  rowspan="2">
                <isa:translate key="hom.jsp.procorder.deliverydate" />
              </th>
              <th class="ref-doc" scope='col'  rowspan="2">
                <isa:translate key="hom.jsp.procorder.combinedorder" />
              </th>
              <th class="status"scope='col'  rowspan="2">
                 <isa:translate key="hom.jsp.processorder.itemstatus" />
              </th>
            </tr>
          </thead>

          <tbody>
            <%
            int margin = 0;
            String tag_style = "";
            String tag_style_detail = ""; %>

            <isa:iterate id="item" name="<%= MaintainOrderBaseAction.RC_ITEMS %>"
                         type="com.sap.isa.businessobject.item.ItemSalesDoc">

              <%
              /* set the item in the ui class */
              ui.setItem(item);
              String substMsgColspan = "10"; 
              String itemKey = item.getTechKey().getIdAsString(); %>

              <% if ( !ui.isBOMSubItemToBeSuppressed() ) { %>

              <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>

              <tr class="<%= ui.even_odd %>" id="row_<%= ui.line %>" onkeypress="checkReturnKeyPressed(event)" >

                <%
                tag_style = ui.getItemCSSClass();
                %>
                <td class="opener">
                
                <%-- some hidden fields --%>
                <input type="hidden" name="customerItemKey[<%= ui.line %>]"  value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>
                <input type="hidden" name="customerProduct[<%= ui.line %>]"  value="<%= JspUtil.encodeHtml(item.getProduct()) %>"/>
                <input type="hidden" name="customerQuantity[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getQuantity()) %>"/>
                <input type="hidden" name="customerUnit[<%= ui.line %>]"     value="<%= JspUtil.removeNull(item.getUnit()) %>"/>
                <input type="hidden" name="customerParentid[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getParentId().getIdAsString()) %>"/>                
                
                  <%
                  if (ui.showItemDetailButton()) { %>
                    <a href='javascript: toggleDetail("rowdetail_<%= ui.line %>");' class="iconlink"><img class="icon" id="rowdetail_<%= ui.line %>close" style="DISPLAY: none"
                       src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>"
                       alt="<isa:translate key="b2b.order.details.item.close" />"
                       width="16" height="16"/><img id="rowdetail_<%= ui.line %>open" style="DISPLAY: inline"
                         src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>"
                         alt="<isa:translate key="b2b.order.details.item.open" />"
                         width="16" height="16"/></a>
                    <% if (ui.showReorderIcon) { %>
                       <a href='javascript: processItem("rowdetailprocess_<%= ui.line %>", "Reorder", "Forward", "<%= JspUtil.encodeHtml(ui.getReorderStatusDescription()) %>", "<%= ui.getReorderStatusValue() %>", <%= ui.line %>, <%= ui.showReorderIcon%>);' class="iconlink"><img class="icon" id="rowdetailprocess_<%= ui.line %>closeReorder" style="DISPLAY: none"
                          src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hom_reorder_0.gif") %>" alt="<isa:translate key="hom.jsp.reorder.item.close" />" width="16" height="16"/><img id="rowdetailprocess_<%= ui.line %>openReorder" style="DISPLAY: inline"
                          src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hom_reorder_1.gif") %>" alt="<isa:translate key="hom.jsp.reorder.item.open" />" width="16" height="16"/></a><img id="rowdetailprocess_<%= ui.line %>openReorderDisabled" style="DISPLAY: none"
                          src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hom_reorder_disabled.gif") %>" alt="<isa:translate key="hom.jsp.reorder.item.open" />" width="16" height="16"/>
                    <%
                    }
                    if (ui.showForwardIcon) { %>
                      <a href='javascript: processItem("rowdetailprocess_<%= ui.line %>", "Forward", "Reorder", "<%= ui.getForwardStatusDescription() %>", "<%= ui.getForwardStatusValue() %>", <%= ui.line %>, <%= ui.showForwardIcon%>);' class="iconlink"><img class="icon" id="rowdetailprocess_<%= ui.line %>closeForward" style="DISPLAY: none"
                         src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hom_process_0.gif") %>" alt="<isa:translate key="hom.jsp.process.item.close" />" width="16" height="16"/><img id="rowdetailprocess_<%= ui.line %>openForward" style="DISPLAY: inline"
                         src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hom_process_1.gif") %>" alt="<isa:translate key="hom.jsp.process.item.open" />" width="16" height="16"/></a><img id="rowdetailprocess_<%= ui.line %>openForwardDisabled" style="DISPLAY: none"
                         src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hom_process_disabled.gif") %>" alt="<isa:translate key="hom.jsp.process.item.open" />" width="16" height="16"/>
                    <%
                    } %>
                  <%
                  }
                  else {%>
                    &nbsp;
                  <% } %>
                </td>

                <%-- Checkbox --%>
                <td class="check">
                  <%
                  if (!ui.isSubItem) { %>
                    <input type="checkbox" id="select_[<%= ui.line %>]" name="select_[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>" onclick="checkEvent(this)" <%= !ui.showcollectiveOrder?"":"disabled" %>/>
                  <%
                  }%>

                  <input type="hidden" name="replacetype[<%= ui.line %>]" value=""/>
                </td>

                <%-- if sub-level, insert subpos symbols --%>
                <td <% if (ui.itemHierarchy.isSubItem(item)) { %> class="item-sub-<%= ui.getItemLevel() -1 %>" <% } else { %> class="" <% } %>>

                    <% if (ui.isElementVisible("order.item.posNo", itemKey)) { %>
                        <%= JspUtil.removeNull(item.getNumberInt()) %>
                    <% } %>
                    <input type="hidden" name="pos_no[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getNumberInt()) %>"/>
                </td>

                <%
                String readonly = ui.itemMessageString.length() == 0?"readonly=\"readonly\"":"";
                %>

                <td class="product">
                    <% if (ui.isElementVisible("order.item.product", itemKey)) { %>
                        <%= JspUtil.encodeHtml(item.getProduct()) %>
					<% } %>
                </td>
                <td class="qty">
                    <% if (ui.isElementVisible("order.item.qty", itemKey)) { %>
                        <%= JspUtil.removeNull(item.getQuantity()) %>&nbsp;<%= JspUtil.removeNull(item.getUnit()) %>
                    <% } %>
                </td>

                <td class="desc">
                  <% if (ui.isElementVisible("order.item.description", itemKey)) { %>
                      <%= JspUtil.encodeHtml(item.getDescription()) %>
                  <% } %>                 
                  <%
                  if (item.isConfigurable() && ui.isElementVisible("order.item.configuration", itemKey)) {
					if (item.getExternalItem() != null) { %>
						<a href="#" class="icon" onclick="itemconfig('<%=item.getTechKey()%>');">
									 <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
									 alt="<isa:translate key="b2b.order.display.configure"/>
									 <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.config.item"/> <% } %>" />
						</a>
					<% } else { %>
							 <a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.config.imp"/>');">
									   <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
										alt="<isa:translate key="b2b.order.display.configure"/>" />
							 </a>       
					<% } %>                  	
                  <% } %>
                </td>

                <%-- Price --%>
                <td class="price">
                  <% if (ui.isElementVisible("order.item.netValueWOFreight", itemKey)) { %>
                      <%= JspUtil.removeNull(item.getNetValueWOFreight()) + "&nbsp;" + JspUtil.removeNull(item.getCurrency()) %>
                  <% } %>
                  <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
                    <%
                    if (ui.header.getIpcDocumentId() != null ) { %>
                          &nbsp;<a href="#" onclick="displayIpcPricingConds('<%= JspUtil.removeNull(ui.header.getIpcConnectionKey()) %>',
                                                                            '<%= JspUtil.removeNull(ui.header.getIpcDocumentId().getIdAsString()) %>',
                                                                            '<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>'
                                                                            )"><isa:translate key="b2b.order.display.ipcconds" /> </a>

                    <%
                    } %>
                  </isacore:ifShopProperty>
                  <% if (ui.isElementVisible("order.item.netPrice", itemKey)) { %>
                      <strong><span class="slighttext"><%= JspUtil.removeNull(item.getNetPrice()) + "&nbsp;" + JspUtil.removeNull(item.getCurrency()) %></span></strong>
                  <% } %>
                </td>

                <%-- DeliverDate --%>
                <td class="date-on">
                    <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey)) { %>
                        <%= JspUtil.encodeHtml(item.getReqDeliveryDate()) %>
                    <% } %>
                </td>

                <%-- Combined Order Referenz --%>
                <td class="ref-doc">
                  <%-- if (ui.isElementVisible("order.item.connectedItem", itemKey)) { --%>
	                  <%
	                  if (ui.connectedItem == null) { %>
	                    -- / --
	                  <%
	                  } else {
	                    if (ui.showcollectiveOrder) { %>
	                      <%=JspUtil.removeNull(ui.connectedItem.getDocNumber()) %> / <%=JspUtil.removeNull(ui.connectedItem.getPosNumber()) %>
	                    <%
	                    }
	                    else { %>
	                      <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>?techkey=<%=ui.connectedItem.getDocumentKey()%>&objects_origin=&object_id=&objecttype=order"><%=JspUtil.removeNull(ui.connectedItem.getDocNumber()) %> / <%=JspUtil.removeNull(ui.connectedItem.getPosNumber()) %></a>
	                    <%
	                    }
	                  } %>
	              <%-- } --%>
                </td>

                <%-- Status --%>
                <td class="status" style="vertical-align: middle">

                  <input type="hidden" id="itemextstatus[<%=ui.line%>]" name="itemextstatus[<%=ui.line%>]" value="<%=ui.getCurrentStatusValue()%>"/>
	              <% if (ui.isElementVisible("order.item.status", itemKey)) { %>  
                      <%-- This info is displayed, if item has status "reorder" or "forward" or is a sub item --%>
	                  <div id ="itemstatus_<%=ui.line%>" <%= ui.showcollectiveOrder||ui.isSubItem?"":"style='display:none;'" %> >
	                    <%= JspUtil.encodeHtml(ui.getCurrentStatusDescription()) %>
	                  </div>
	                  <%-- This info is displayed else --%>
	                  <% Iterator extStatus = (Iterator)item.getExtendedStatus().getStatusIterator();
	                     request.setAttribute("EXTENDEDSTATUS",extStatus);%>
	                  <select id="tempitemextstatus_<%=ui.line%>" name="tempitemextstatus[<%=ui.line%>]" onchange="adjustStatus(<%=ui.line%>)" <%= !(ui.showcollectiveOrder||ui.isSubItem)?"":"style='display:none;'" %>>
	                    <isa:iterate id="eStatus" name="EXTENDEDSTATUS" type="com.sap.isa.businessobject.ExtendedStatusListEntry">
	                          <%
	                          if (ui.isAllowedStatus(eStatus)) { %>
	                            <option value='<%= ui.getExtStatusValue(eStatus) %>' <%= eStatus.isCurrentStatus() ? "selected=\"selected\"" : ""%>><%= JspUtil.encodeHtml(eStatus.getDescription()) %></option>
	                          <%
	                          }%>
	                    </isa:iterate>
	                  </select>
                  <% } %>                  
                </td>

              </tr>

              <%-- Item Details --%>

              <% tag_style_detail = ui.even_odd + "-detail"; %>
              <tr class="<%= tag_style_detail %>" id="rowdetail_<%= ui.line %>" <% if (ui.isToggleSupported) {%> style="display:none;"<%}%>>
                <td class="select">&nbsp;</td>
                <td class="detail"
                  <% if (ui.isContractInfoAvailable()) { %>
                       colspan="14">
                  <% } else { %>
                       colspan="13">
                  <% } %>
                  <table class="item-detail">
                    <%-- display of product configuration --%>
                    <%-- ItemSalesDoc item = orderitem; --%>
                    <% if (ui.isElementVisible("order.item.configuration", itemKey)) { %>
                        <%@ include file="/ecombase/orderitemconfiginfo.inc.jsp" %>
                    <% } %> 
                    <%-- Status Customer --%>
                    <% if (ui.isElementVisible("order.item.status", itemKey)) { %>
	                    <tr>
	                      <td class="identifier"><isa:translate key="hom.jsp.procorder.customer.state" /></td>
	                      <td class="value"><% if (item.getStatus() != null) { %><% itemStatusKey = "status.sales.status.".concat(item.getStatus()); %><isa:translate key="<%= itemStatusKey %>" /><% } %></td>
	                    </tr>
	                <% } %>
                    <%
                    if (ui.isB2BSupported) { %>
                       <%-- ShipTo --%>
	                   <% if ( !ui.isElementVisible("order.item.deliverTo", itemKey)) { %>
	                       <tr>
	                         <td class="identifier"><isa:translate key="b2b.order.display.soldto" />&nbsp;</td>
	                         <td class="value"><a href="#" onclick="showShipToForKey(<%= item.getShipTo().getTechKey().getIdAsString() %>);"><% if (item.getShipTo() != null) { %><%= JspUtil.encodeHtml(item.getShipTo().getShortAddress())  %><% } %></a><img src="<%= WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="35" height="1" alt="" /></td>
	                       </tr>
	                   <% } %>
                       <%-- Notes --%>
                       <% if (ui.isElementVisible("order.item.comment", itemKey)) { %>
	                       <tr>
	                         <td class="identifier"><isa:translate key="hom.jsp.proc.customer.message" /></td>
	                         <td class="value"><%= JspUtil.replaceSpecialCharacters(item.getText().getText()) %></td>
	                       </tr>
                       <% } %>
                    <%
                    }%>
                  </table>
                </td>
              </tr>

              <%--   messages --%>
              <% ui.setItemCSSClass(ui.even_odd + "-error"); %>
              <% if (ui.itemMessageString.length() > 0) { %>
                <tr class="<%= ui.getItemCSSClass() %>" >
                   <td> &nbsp;</td>
                   <td
                    <% if (ui.isContractInfoAvailable()) { %>
                       colspan="14">
                    <% } else { %>
                       colspan="13">
                    <% } %>
                    <div class="error-items"><span>
                        <%= JspUtil.encodeHtml(ui.itemMessageString) %>
                        <% if (item.isProductAliasAvailable()) { %>
                              <a href="#" onClick='submit_refresh();'>
                                 <isa:translate key="b2b.proddet.sel"/>
                             </a>
                        <% } %>
                        <% if (item.isDeterminedCampaignAvailable()) { %>
                              <a href="#" onClick='submit_refresh();'>
                                  <isa:translate key="b2b.campaign.sel"/>
                              </a>
                        <% } %>

                    </span></div>
                   </td>
                </tr>
              <% } %>


              <%-- Item for collective order --%>
              <%
              if (ui.showcollectiveOrder) { %>
                <input type="hidden" id="product[<%= ui.line %>]" name="product[<%= ui.line %>]"  value="<%= JspUtil.encodeHtml(item.getProduct()) %>"/>
              <%
              }
              else { %>
                <input type="hidden" id="product[<%= ui.line %>]" name="product[<%= ui.line %>]"  value=""/>
              <%
              }

              if (ui.showcollectiveOrder || ui.showcollectiveOrderIcon) { %>
                <%
                if (ui.connectedItem != null) {  %>
                  <input type="hidden"   name="itemTechKey[<%= ui.line %>]"   value="<%= JspUtil.removeNull(ui.collectiveOrderItem.getTechKey().getIdAsString()) %>"/>
                  <input type="hidden"   name="productkey[<%= ui.line %>]"    value="<%= JspUtil.removeNull(ui.collectiveOrderItem.getProductId().getIdAsString()) %>"/>
                <%
                }
                else if (item.isConfigurable()) {%>
                  <input type="hidden"   name="productConfigurable[<%= ui.line %>]"    value="X"/>
                <%
                }%>

                <input type="hidden" id="savedproduct[<%= ui.line %>]"  name="savedproduct[<%= ui.line %>]"    value="<%= JspUtil.removeNull(item.getProduct()) %>"/>
                <%-- additional hidden field --%>

                <input type="hidden"   name="refItemKey[<%= ui.line %>]"      value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>

                <% tag_style_detail = "poslist-detail-" + ui.even_odd; %>

                <tr id="rowdetailprocess_<%= ui.line %>" <% if (!ui.showcollectiveOrder) { %>style="display:none;"><% } else { %>><% } %>

                  <td  class="select" colspan="2">
                    <b id="rowdetailprocess_<%= ui.line %>title"><%if (ui.isReorder){%><isa:translate key="hom.jsp.procorder.reorder.title" /><%}else{%><isa:translate key="hom.jsp.procorder.forward" /><%}%></b>&nbsp;
                  </td>
                  <td class="detail" colspan="13">
                    <table class="item-detail">
                      <tr>
                        <%-- Product ID --%>
                        <% if (ui.isElementVisible("order.item.product", itemKey)) { %>
                            <td class="identifier"> 
                                <isa:translate key="b2b.order.display.productno"/> 
                            </td>
                            <td class="value">                        
                                <%= JspUtil.encodeHtml(ui.collectiveOrderItem.getProduct()) %> 
                            </td>
                        <% } %>
                        <%-- Quantity --%>
	                    <% if (ui.isElementVisible("order.item.qty", itemKey)) { %>
	                        <td class="value">
                              <div id="rowdetailprocess_<%= ui.line %>quantityAreaReorder" <%= ui.isReorder?"":"style='display:none;'" %> >
	                            <input id="rowdetailprocess_<%= ui.line %>quantityReorder" type="text" class="textinput-small" size="3" maxlength="10" name="quantity<%= ui.isReorder?"":"xx"%>[<%= ui.line %>]" value="<%= JspUtil.removeNull(ui.collectiveOrderItem.getQuantity()) %>"/>&nbsp;
                              </div>
                              <div id="rowdetailprocess_<%= ui.line %>quantityAreaForward" <%= ui.isReorder?"style='display:none;'":"" %> >
	                            <%= JspUtil.removeNull(ui.collectiveOrderItem.getQuantity()) %>
	                            <input id="rowdetailprocess_<%= ui.line %>quantityForward" type="hidden" name="quantity<%= ui.isReorder?"xx":""%>[<%= ui.line %>]" value="<%= JspUtil.removeNull(ui.collectiveOrderItem.getQuantity()) %>"/>&nbsp;
                              </div>
	                        </td>
	                    <% } %>
	                    <% if (ui.isElementVisible("order.item.unit", itemKey)) { %>
	                        <td class="value">
	                        <%-- UOM --%>
	                        <%
	                        if (ui.units.length > 1) {%>
	                          <select id="rowdetailprocess_<%= ui.line %>unit" name="unit[<%= ui.line %>]">
	                            <%
	                            for (int j = 0; j < ui.units.length; j++) {
	
	                              String selected = ui.units[j].equals(ui.collectiveOrderItem.getUnit())?selected = "selected=\"selected\"":"";
	                              %>
	                              <option <%=selected %>><%= ui.units[j] %></option>
	                              <%
	                            } %>
	                          </select>
	                        <%
	                        }
	                        else {%>
	                          <%= ui.collectiveOrderItem.getUnit() %>
	                          <input type="hidden" name="unit[<%= ui.line %>]" value="<%= ui.collectiveOrderItem.getUnit() %>"/>
	                        <%
	                        }%>
	                        </td>
	                    <% } %>	                  
	                    <td class="value">
	                        <% if (ui.isElementVisible("order.item.description", itemKey)) { %>
	                            <%= JspUtil.encodeHtml(ui.collectiveOrderItem.getDescription()) %>
	                        <% } %>
	                        <% if (ui.connectedItem != null && ui.collectiveOrderItem.isConfigurable() && ui.isElementVisible("order.item.configuration", itemKey)) { %>
	                            &nbsp; <a href="#" onclick="newitemconfig('<%=ui.connectedItem.getTechKey().getIdAsString()%>');"> <isa:translate key="b2b.order.display.configure"/></a>
	                        <% } %>
	                    </td>
                      </tr>

                      <%-- BEGIN ConnectedOrder Prize --%>
                      <%
                      if (ui.connectedItem != null && (ui.isElementVisible("order.item.netValueWOFreight", itemKey) || ui.isElementVisible("order.item.netPrice", itemKey))) {  %>
                        <tr>
                           <td class="identifier">
                                    <isa:translate key="hom.jsp.procorder.reorder.price"/>
                           </td>
                           <td class="value">
                             <% if (ui.isElementVisible("order.item.netValueWOFreight", itemKey)) { %>
                                 <%= JspUtil.replaceSpecialCharacters(ui.collectiveOrderItem.getNetValueWOFreight()) + "&nbsp;" + JspUtil.replaceSpecialCharacters(ui.collectiveOrderItem.getCurrency()) %>
                                 &nbsp;/&nbsp;
                             <% } %> 
                             <% if (ui.isElementVisible("order.item.netPrice", itemKey)){%>
                                 <span class="slighttext">
                                     <%= JspUtil.replaceSpecialCharacters(ui.collectiveOrderItem.getNetPrice()) + "&nbsp;" + JspUtil.replaceSpecialCharacters(ui.collectiveOrderItem.getCurrency()) %>
                                 </span>
                             <% } %>
                           </td>
                        </tr>
                      <%
                      } %>
                      <%-- END Prize --%>

                      <%-- Delivery Date --%>
                      <tr>
	                     <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey)) { %>
	                        <td class="identifier" >
	                              <isa:translate key="hom.jsp.procorder.reorder.deldat"/>
	                        </td>
	                        <td class="value" colspan="3">
	                          <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey)) { %>
		                          <input id="rowdetailprocess_<%= ui.line %>reqdeliverydate" name="reqdeliverydate[<%= ui.line %>]"
		                                     type="text" class="textinput-middle" size ="15"
		                                     value="<%= JspUtil.replaceSpecialCharacters(ui.collectiveOrderItem.getReqDeliveryDate()) %>"/>
		                          <a href="#" onclick="setDateField(document.order_positions['reqdeliverydate[<%= ui.line %>]']);setDateFormat('<%=ui.getDateFormat()%>');isaTop().newWin = window.open('<isa:webappsURL name="b2b/calendar.jsp"/>','cal','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes'); isaTop().newWin.focus();">
		                                          <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>" class="display-image"/>
		                          </a>
		                       <% } else { %>
		                          <input id="rowdetailprocess_<%= ui.line %>reqdeliverydate" name="reqdeliverydate[<%= ui.line %>]"
		                                     type="hidden" value="<%= JspUtil.replaceSpecialCharacters(ui.collectiveOrderItem.getReqDeliveryDate()) %>"/>
		                       <% } %>
	                        </td>
	                     <% } %>
                      </tr>

                      <%-- Shipto --%>

                      <%-- first section for reorder --%>
                      <tr id="rowdetailprocess_<%= ui.line %>shiptoReorder" <%= ui.isReorder?"":"style='display:none;'" %> >
                        <% if (ui.isElementVisible("order.item.deliverTo", itemKey)) { %>
	                        <td class="identifier" >
	                              <isa:translate key="b2b.order.display.soldto"/>
	                        </td>
	                        <td class="value" colspan="3">
	                              <div class="vertical-align-middle">
	                                <% if ( ui.isElementEnabled("order.item.deliverTo", itemKey)) { %>
		                                <select id="rowdetailprocess_<%= ui.line %>customerReorder" name="customer<%= ui.isReorder?"":"xx"%>[<%= ui.line %>]">
		                                      <isa:iterate id="shipTo" name="<%= DarkMaintainShowCollectiveOrderAction.RC_SHIPTOS %>"
		                                                               type="com.sap.isa.businessobject.ShipTo">
		                                        <%
		                                        String selected = "";
		                                        if (ui.connectedItem != null && ui.collectiveOrderItem.getShipTo() != null && shipTo.getTechKey().equals(ui.collectiveOrderItem.getShipTo().getTechKey())) {
		                                              selected = " selected";
		                                        } %>
		
		                                        <option <%= selected %> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
		                                      </isa:iterate>
		                                </select>
		                            <% } else { %>
		                                <%= JspUtil.encodeHtml(ui.collectiveOrderItem.getShipTo().getShortAddress()) %>		     
		                            <% } %>
                                    &nbsp;                       
	                                <a class="icon" href="#" onclick="showShipToForCollectiveOrder(document.order_positions['customer[<%= ui.line %>]'].selectedIndex);" >
	                                      <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/>" class="display-image"/></a>&nbsp;
	                                <%
	                                if (ui.connectedItem != null && ui.isElementEnabled("order.item.deliverTo", itemKey)) {  %>
	                                      <a class="icon" href="#" onclick="newShipToItem('<%= ui.collectiveOrderItem.getTechKey() %>');">
	                                        <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_new_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.icon.newshipto"/>" class="display-image"/></a><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="35" height="1" alt="" />
	                                <%
	                                }%>
	                              </div>
	                        </td>
	                     <% } %>
                      </tr>

                      <%-- second section for forwarding --%>
                      <tr id="rowdetailprocess_<%= ui.line %>shiptoForward" <%= ui.isReorder?"style='display:none;'":"" %> >
	                     <% if (ui.isElementVisible("order.item.deliverTo", itemKey)) { %>   
	                        <td class="identifier" >
	                              <isa:translate key="b2b.order.display.soldto"/>
	                        </td>
	                        <td class="value" colspan="3">
	                              <div class="vertical-align-middle">
	                                <% ShipTo tmpShipTo = ui.shipTosCollectiveOrder[Integer.parseInt(ui.shipToCustomerIndex)]; %>
	                                <input type="hidden" id="rowdetailprocess_<%= ui.line %>customerForward" name="customer<%= ui.isReorder?"xx":""%>[<%= ui.line %>]" value="<%= tmpShipTo.getTechKey().getIdAsString() %>" />
	                                <var><a class="icon" href="#" onclick="showShipToForCollectiveOrder('<%=ui.shipToCustomerIndex%>');" ><%= JspUtil.encodeHtml(tmpShipTo.getShortAddress()) %></a></var>
	                              </div>
	                        </td>
	                     <% } %>
                      </tr>
                      <%-- end ship to --%>

                      <%-- notes --%>
                      <tr>
                         <% if (ui.isElementVisible("order.item.comment", itemKey)) { %>
	                        <td class="identifier">
	                                <isa:translate key="b2b.order.details.note"/>
	                        </td>
	
	                        <td class="value" colspan="3">
	                                <textarea id="rowdetailprocess_<%= ui.line %>comment" name="comment[<%= ui.line %>]" rows="2" cols="80" ><%= JspUtil.replaceSpecialCharacters(ui.collectiveOrderItem.getText().getText()) %></textarea>
	                        </td>
	                      <% } %>
                      </tr>
                      <%--   messages --%>
                      <%
                      if (ui.collectiveOrderItemMessage.length() > 0) { %>
                        <tr>
                          <td colspan="4">
                            <div class="error">
                              <span><%= JspUtil.encodeHtml(ui.collectiveOrderItemMessage) %></span>
                            </div>
                          </td>
                       </tr>
                      <%
                      } %>

                    </table>
                  </td>
                </tr>
              <%
              } /* end show collective order item */ %>
             <% }  // ui.isBOMSubItemToBeSuppressed()  %>

            </isa:iterate>
          </tbody>
        </table>
      </div>      <%-- document items --%>
      <div>
      <%
      if (ui.isAccessible()) {%>
        <a id="prcorder_tblitems_end" href="#prcorder_tblitems_begin" title="<isa:translate key="access.table.end" arg0="<%=prcorder_tblitems_title%>"/>"></a>
      <%
      }%>

      
      <input type="hidden" name="sendpressed" value=""/>
      <input type="hidden" name="cancelpressed" value=""/>
      <input type="hidden" name="actualpos" value="1"/>
      <input type="hidden" name="refresh" value=""/>
      <input type="hidden" name="setstate" value=""/>
      <input type="hidden" name="newpos" value=""/>
      <input type="hidden" name="newshipto" value=""/>
      <input type="hidden" name="configitemid" value=""/>
      <input type="hidden" name="confignewitemid" value=""/>
      </div>
    </form>      
    </div>  <%-- Document--%>


  <%--Buttons--%>
  <div id="buttons">
    <%-- Accesskey for the button section. It must be an resourcekey, so that it can be translated. --%>
    <a id="access-buttons" href="#access-header" title="<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>

    <form action="" name="changeState">
      <ul class="buttons-1">
          <li>
            <% ui.setExtendStatusIterator(); %>
            <span title="<isa:translate key="access.labelshuffler.title"/>" >
            <select name="newstatus">
              <option value="">&nbsp;</option>
              <isa:iterate id="uStatus"
                           name="<%=ProcessCustomerOrderUI.RC_USERSTATUSPROFILE%>"
                           type="com.sap.isa.core.util.table.ResultData">
                <%
                String businessProcess = uStatus.getString("BUSINESSPROCESS");
                if (!businessProcess.equals("PCHS") && !businessProcess.equals("FWRD") && !businessProcess.equals("DLVS") ) { %>
                  <option value="<%=uStatus.getRowKey()%>/<%=businessProcess%>"><%=uStatus.getString("DESCRIPTION_LONG")%></option>
                <%
                }%>
              </isa:iterate>
            </select>
            </span>
            <a href="#" onclick="set_state();" <% if (ui.isAccessible()) { %> title="<isa:translate key="hom.jsp.procordercloser.ok"/>" <% } %> ><isa:translate key="hom.jsp.procordercloser.ok"/></a>
          </li>
          <li><a href="#" onclick="submit_refresh();" <% if (ui.isAccessible()) { %> title="<isa:translate key="b2b.acc.buttons.upd"/>" <% } %> ><isa:translate key="b2b.order.display.submit.update"/></a></li>
      </ul>
    </form>
    <ul class="buttons-3">
       <li><a href="#" onclick="cancel_confirm();" <% if (ui.isAccessible()) { %> title="<isa:translate key="b2b.acc.buttons.cncl"/>" <% } %>><isa:translate key="b2b.order.display.submit.close"/></a></li>
       <li><a class="disabled" href="#" onclick="send_confirm_process();" <% if (ui.isAccessible()) { %> title="<isa:translate key="b2b.acc.buttons.save.ord"/>" <% } %> ><isa:translate key="b2b.order.change.submit.save"/></a></li>
    </ul>
  </div> 



  </body>
</html>

<% ui.items.clear(); %>
