<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.businesspartner.action.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.contract.*" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.isacore.actionform.order.orderdownload.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*"%>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.PaymentUI" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>
<%@ page import="com.sap.isa.payment.businessobject.PaymentCCard"%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ include file="/appbase/checksession.inc" %>


<%
  ChangeCustomerOrderUI ui = new ChangeCustomerOrderUI(pageContext);
  HeaderSalesDocument header = ui.header;

  String itemStatusKey = "";
  boolean isDetailSupported = true;
  
  //inline display of product configuration will be restricted on values of identifying characteristics
  boolean showDetailView = false;
%>

<% String accessText = ""; %>

<isa:contentType />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>
            <isa:translate key="b2b.order.display.pagetitle"/>
    </title>

    <isa:stylesheets/>

    <script type="text/javascript">
            <%@ include file="/b2b/jscript/urlrewrite.inc"  %>
    </script>

    <%-- include for locale calendar control --%>
    <%@ include file="../jscript/setLocaleCalendarSettings.jsp.inc" %>

    <script src="<isa:mimeURL name="b2b/jscript/table_highlight.js" />"
            type="text/javascript">
    </script>

    <script src="<isa:mimeURL name="b2b/jscript/reload.js" />"
            type="text/javascript">
    </script>

    <script src="<isa:mimeURL name="b2b/jscript/frames.js" />"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/browsersniff.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "auction/jscript/calendar.js") %>"
            type="text/javascript">
    </script>
    
	<% if(ui.getShop().isPricingCondsAvailable() == true) {%>
	 <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
		   type = "text/javascript">
	 </script>
   <% } %>
    <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
            type="text/javascript">
    </script>

    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
            type="text/javascript">
    </script>
    <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderToolsRetkey.js") %>"
            type="text/javascript">
    </script>

    <%@ include file="/b2b/jscript/orderTools.inc.jsp"  %>

    <script type="text/javascript">
    <!--

      function showShipTo(index) {
        var url = "<isa:webappsURL name="b2b/showshipto.do"/>?<%=ShowShipToAction.PARAM_SHIPTO_INDEX%>=" + index;
        openWinExtended(url, '450','450');
        return true;
      }

      <%-- function for the onload()-event --%>
      function loadPage() {
        setLocaleSpecificData();
        adjustShipToOnItems();
      }

      function adjustShipToOnItems() {
        <%
        if (!ui.isB2BSupported) { %>
           var lineShipTo;
           var actualForm = document.forms["order_positions"];
           var shipTokey  = actualForm.elements["headerShipTo"].value;

           var lineShipTo;
           var line = 1;
           while ( (lineShipTo = eval("actualForm.elements['customer["+line+"]']")) != null ) {
             lineShipTo.value = shipTokey;
             line++;
           }
        <%
        } %>
        return true;
      }
  //-->
  </script>

</head>

<body class="orderchange" onload="miRegisterObjects();">

  <div class="module-name"><isa:moduleName name="b2b/hom/hom_changeorder.jsp" /></div>
  <%--
    The body consists of two layers:
    - the document layer, containing the header and the items of the document. It is scrollable.
    - the buttons layer, containig the buttons for the document. It stays at the bottom of the page.
  --%>

   <%-- Dokument information --%>
   <h1><isa:translate key="hom.jsp.gen.edit.customer.chg"/>&nbsp;<isa:translate key="hom.jsp.gen.edit.customer.order"/>
       <%= ui.getDocNumber() %>&nbsp;<isa:translate key="hom.jsp.gen.edit.cust.order.date"/>&nbsp;<%= ui.getDocDate() %></h1>

     <%-- !!!! Do not change the name of the form, the JavaScript functions rely on them !!!! --%>

      <div id="document">

        <%-- Show accessibility messages --%>
        <% if (ui.isAccessible) { %>
                <%@ include file="/appbase/accessibilitymessages.inc.jsp"  %>
        <% } %>


      <%--
          The document layer contains:
          - the document-header layer, which holds the header information of the document
          - the document-items layer, which contains all items
      --%>

    <form action="<isa:webappsURL name="b2b/maintainorder.do"/>"
        name="order_positions"
        method="post">


      <div class="document-header">

        <%-- Accesskey for the header section. --%>
        <a href="#" title="<isa:translate key="hom.jsp.chgorder.pagebegin"/>"
                    accesskey="<isa:translate key="hom.jsp.chgorder.pagekey"/>"
                    ></a>


        <div>
          <isacore:requestSerial mode="POST"/>
          <%-- store all businesspartner informations --%>
          <%= ui.storeBusinessPartners() %>

          <input name="headerReqDeliveryDate" type="hidden"  value="<%= JspUtil.replaceSpecialCharacters(ui.header.getReqDeliveryDate()) %>"/>
        </div>



        <% if (ui.isAccessible()) { %>
           <a href="#end-table1" title="<isa:translate key="b2b.acc.header.dat.link"/>"></a>
        <% } %>

        <div class="header-basic">
          <%--
            The document-header layer contains:
            - the data table. It displays the title of the document and the standard input fields like <Your Reference:>, <Deliver To>, ...
            - the price table. It displays price information like <Total price net>, <Freight>, <Taxes> ...
            - the message table. It displays the textarea for the message entry <Your message to us>.
            - the error layer (optionally), if there are errors.
          --%>

          <table class="layout"> <%-- level: sub3 --%>
            <tr>
              <td class="col1">

                  <table class="header-general" summary="<isa:translate key="b2b.acc.header.dat.sum"/>"> <%-- level: sub3 --%>

                    <% if ( ui.isHeaderValidToAvailable() && ui.isElementVisible("order.validTo") ) { %>
                       <tr>
                          <td class="identifier"><isa:translate key="status.sales.detail.validTo"/>:</td>
                          <td class="value"><%= ui.header.getValidTo()%></td>
                       </tr>
                    <% } %>

                    <%-- Customer Info --%>
                    <% if ( ui.isElementVisible("order.soldTo") ) { %>
	                    <tr>
	                       <td class="identifier"><isa:translate key="hom.jsp.processorder.customer"/>&nbsp;</td>
	                       <td class="value">
	                          <a href="#" onclick="showSoldTo('<%= JspUtil.removeNull(ui.getSoldToKey()) %>')"><%= JspUtil.removeNull(ui.getSoldToId()) %></a>&nbsp;<%= JspUtil.encodeHtml(ui.getSoldToName()) %>
	                       </td>
	                    </tr>
                    <% } %>

                    <%-- Reference Number --%>
                    <% if ( ui.isElementVisible("order.numberExt") ) { %>
	                    <tr>
	                       <td class="identifier"><isa:translate key="hom.jsp.processorder.ref.short"/>&nbsp;</td>
	                       <td class="value"><%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %></td>
	                    </tr>
	                <% } %>

                    <%-- Description --%>
                    <% if ( ui.isElementVisible("order.description") ) { %>
	                    <tr>
	                       <td class="identifier">
	                          <% if (ui.isElementEnabled("order.description"))  { %>
	                          <label id="headerDescriptionLbl" for="headerDescription" title="<isa:translate key="hom.jsp.processorder.description"/>">
	                          <%
	                          }%>
	                            <isa:translate key="hom.jsp.processorder.description"/>&nbsp;
	                          <% if (ui.header.isDescriptionChangeable())  { %>
	                          </label>
	                          <%
	                          }%>
	                       </td>
	                       <td class="value">
	                          <% if (ui.isElementEnabled("order.description"))  { %>
	                     <input class="textinput-large" type="text" name="headerDescription" id="headerDescription" value="<%= JspUtil.encodeHtml(ui.header.getDescription()) %>"/>
	                          <% } else { %>
	                     <%= JspUtil.encodeHtml(ui.header.getDescription()) %>
	                          <% } %>
	                       </td>
	                    </tr>
                    <% } %>

                    <%-- Shipping conditions --%>
                    <% if (ui.isElementVisible("order.shippingCondition")) { %>
	                    <tr>
	                      <td class="identifier">
	                        <% if (ui.header.isShipCondChangeable()) { %>
	                          <label id="shipCondLbl" for="shipCond" title="<isa:translate key="b2b.order.display.shipcond"/>">
	                        <% } %>
	                            <isa:translate key="b2b.order.display.shipcond"/>&nbsp;
	                        <% if (ui.header.isShipCondChangeable())  { %>
	                             </label>
	                        <% } %>
	                      </td>
	                      <td class="value">
	                        <% if (ui.isElementEnabled("order.shippingCondition"))  { %>
	                            <select class="select-large" name="shipCond" id="shipCond">
	                                <option value=""><isa:translate key="b2b.order.shipcondpleaseselect"/></option>
	                                <isa:iterate id="shipCond"
	                                                       name="<%= MaintainOrderBaseAction.RC_SHIPCOND %>"
	                                                       type="com.sap.isa.core.util.table.ResultData">
	                                      <% String selected = "";
	                                       if (shipCond.getRowKey().toString().equals(ui.header.getShipCond())) {
											      selected = "selected=\"selected\"";
	                                                } %>
	                        <option <%= selected %> value="<%= shipCond.getRowKey() %>"><%= JspUtil.encodeHtml(shipCond.getString(1)) %></option>
	                              </isa:iterate>
	                    </select> <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="35" height="1" alt="" />
	                        <% } else { %>
	                            <isa:iterate id="shipCond"
	                                         name="<%= MaintainOrderBaseAction.RC_SHIPCOND %>"
	                                         type="com.sap.isa.core.util.table.ResultData">
	                                 <% if (shipCond.getRowKey().toString().equals(ui.header.getShipCond())) {  %>
	                                       <%= JspUtil.encodeHtml(shipCond.getString(1)) %>
	                                 <% } %>
	
	                            </isa:iterate>
	                        <% } %>
	                      </td>
	                    </tr>
	                <% } %>

                    <%-- Ship To --%>
                    <% if (ui.isElementVisible("order.deliverTo")) { %>
	                    <tr>
	                      <td class="identifier">
	                            <label id="headerShipToLbl" for="headerShipTo" title="<isa:translate key="b2b.order.display.soldto"/>">
	                                    <isa:translate key="b2b.order.display.soldto"/>&nbsp;
	                            </label>
	                      </td>
	                      <td>
	                        <% if (!ui.isElementEnabled("order.deliverTo")) { %>
	                            <td class="value" colspan="2">
				                    <input type="hidden" name="headerShipTo" value="<%=ui.header.getShipTo().getTechKey()%>"/>
				                </td>
	                        <% } else { %>
		                        <select class="select-large" name="headerShipTo" id="headerShipTo" onchange="adjustShipToOnItems()">
		                            <isa:iterate id="shipTo"
		                                    name="<%= MaintainOrderBaseAction.SC_SHIPTOS %>"
		                                    type="com.sap.isa.businessobject.ShipTo">
		                                    <% String selected = "";
		                                    if (shipTo.isActive()) {
		                                            if (shipTo.equals(ui.header.getShipTo())) {
														selected = "selected=\"selected\"";
		                                            } %>
		                                    <option <%= selected %> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(ui.getShipToDescription(shipTo)) %></option>
		                                       <% } else if (!shipTo.isActive() && shipTo.equals(ui.header.getShipTo())) { %>
		                                            <option value="">&nbsp;</option>
		                                       <% } %>
		                            </isa:iterate>
		                         </select>&nbsp;
		                            <a class="icon" href="#" onclick="showShipTo(document.order_positions.headerShipTo.selectedIndex);"
		                               title="<isa:translate key="b2b.order.showshipto"/>"
		                            ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" alt="<isa:translate key="b2b.order.showshipto"/>"/></a>&nbsp;
		                            <a class="icon" href="#" onclick="newShipTo();"
		                               title="<isa:translate key="b2b.order.icon.newshipto"/>"
		                            ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_new_green.gif") %>" alt="<isa:translate key="b2b.order.icon.newshipto"/>"/></a>
		                     <% } %>
	                      </td>
	                    </tr>
                    <% } %>

                    <%-- ui.payment infos start --%>
                    <% if (ui.isElementVisible(PaymentUI.PAYMENT_TYPE)) { %>
	                    <tr>
	                      <td class="identifier"><isa:translate key="b2b.order.details.paymenttype"/></td>
	                      <td class="value">
	                            <isa:translate key="<%= ui.paymentKey %>"/>
	                            <%
	                            if (ui.paytypeIsCard) { %>
	                                <a href="javascript: togglepayment()"
	                                       title="<isa:translate key="b2b.order.icon.newshipto"/>">
	                              <img id="payment_close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.creditcard.hidedetails"/>" width="16" height="16" /><img id="payment_open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.creditcard.showdetails"/>" width="16" height="16" /></a>
	                            <%
	                            } %>
	                      </td>
	                    </tr>
	                <% } %>
                    <%
                    if (ui.paytypeIsCard) { %>
                      <tr id="payment_row" style="DISPLAY:none"> <%-- style="DISPLAY:none" --%>
                        <td>&nbsp;</td>
                        <td >
                          <table>
                            <% if (ui.isElementVisible(PaymentUI.ONECARD_TYPE)) { %>
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.credCardInst"/>&nbsp;</span></td>
	                              <%// list of all credit card institutes
	                                ResultData listOfCardInst = (ResultData) request.getAttribute(MaintainOrderBaseAction.RC_CARDTYPE);
	                              %>
	                              <td><span><%= (((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getTypeDescription()) %></span></td>
	                            </tr>
	                        <% } %>
                            <% if (ui.isElementVisible(PaymentUI.ONECARD_HOLDER)) { %>
	                            <tr>                             
	                              <td><span><isa:translate key="b2b.payment.creditCardOwner"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getHolder()) %></span></td>
	                            </tr>
	                        <% } %>
                            <% if (ui.isElementVisible(PaymentUI.ONECARD_NUMBER)) { %>
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.creditCardNumber"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getNumber()) %></span></td>
	                            </tr>
	                        <% } %>
	                        <% if (ui.isElementVisible(PaymentUI.ONECARD_SUFFIX)) { %>
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.swtchCardSrNo"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getNumberSuffix()) %></span></td>
	                            </tr>
	                        <% } %>
                            <% if (ui.isElementVisible(PaymentUI.ONECARD_VALIDITY)) { %>
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.CCValidity"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getExpDateMonth() + "/" + ((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getExpDateYear()) %></span></td>
	                            </tr>
	                        <% } %>
	                        <% if (ui.isElementVisible(PaymentUI.CARD_CVV)) { %>
	                            <tr>
	                              <td><span><isa:translate key="b2b.payment.crdtCardCVV"/></span></td>
	                              <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getCVVAsStars()) %></span></td>
	                            </tr>
	                        <% } %>
                          </table>
                        </td>
                      </tr>
                    <%
                    } %>

                  </table>

                <% if (ui.isAccessible()) { %>
                   <a name="end-table1" title="<isa:translate key="b2b.acc.header.dat.end"/>"></a>
                <% } %>
                <%--End table data--%>
              </td>
              <td class="col2">
                <%--Status Table--%>
                <% if (ui.isAccessible()) { %>
                   <a href="#end-table2" title="<isa:translate key="b2b.acc.header.stat.link"/>"></a>
                <% } %>
                <% if (!ui.header.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE) && ui.isElementVisible("order.status.overall")) { %>
                    <table class="status" summary="<isa:translate key="b2b.acc.header.stat.sum"/>"> <%-- level: sub3 --%>
                        <tr>
                           <td class="identifier"><isa:translate key="status.sales.overallstatus"/>:</td>
                           <td class="value" ><isa:translate key="<%= ui.getDocumentStatusKey() %>"/></td>
                        </tr>
                   </table>
                <% } %>
                <% if (ui.isAccessible()) { %>
                    <a name="end-table2" title="<isa:translate key="b2b.acc.header.stat.end"/>"></a>
                <% } %>
                <%--End Status--%>


                <%--Price Table--%>
                <% if (ui.isAccessible()) { %>
                   <a href="#end-table3" title="<isa:translate key="b2b.acc.header.prc.link"/>"></a>
                <% } %>

                <table class="price-info" summary="Displays accumulated price information. 2 columns and 5 rows."> <%-- level: sub3 --%>
                  <% if (ui.isNetValueAvailable() && ui.isElementVisible("order.netValueWOFreight")) { %>
                    <tr>
                      <td class="identifier" scope="row"><isa:translate key="b2b.order.display.pricenet"/></td>
                      <td class="value"><%= JspUtil.removeNull(ui.header.getNetValueWOFreight()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %>&nbsp;</td>
                    </tr>
                  <% } %>
                  <% if (ui.isFreightValueAvailable() && ui.isElementVisible("order.freight")) { %>
                    <tr>
                      <td class="identifier"  scope="row"><isa:translate key="b2b.order.display.freight"/>&nbsp;</td>
                      <td class="value"><%= JspUtil.removeNull(ui.header.getFreightValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %></td>
                    </tr>
                  <% } %>
                  <% if (ui.isTaxValueAvailable() && ui.isElementVisible("order.tax")) { %>
                    <tr>
                      <td class="identifier" scope="row"><isa:translate key="b2b.order.display.tax"/>&nbsp;</td>
                      <td class="value"><%= JspUtil.removeNull(ui.header.getTaxValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %></td>
                    </tr>
                  <% } %>
                  <% if (ui.isGrossValueAvailable() && ui.isElementVisible("order.priceGross")) { %>
                    <tr>
                      <td colspan="2" class="separator"></td>
                    </tr>
                    <tr>
                      <td class="identifier" scope="row"><isa:translate key="b2b.order.display.pricebrut"/>&nbsp;</td>
                      <td class="value"><%= JspUtil.removeNull(ui.header.getGrossValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %></td>
                    </tr>
                  <% } %>
                </table>
                <% if (ui.isAccessible()) { %>
                           <a name="end-table3" title="<isa:translate key="b2b.acc.header.prc.end"/>"></a>
                <% } %>
                <%--End Price--%>
              </td>
            </tr>
          </table> <%-- class="layout"> --%>
        </div> <%--header-basic--%>

        <%--Data Message--%>
        <table class="message" summary="<isa:translate key="b2b.acc.header.msg.sum"/>">
          <% if (ui.isElementVisible("order.comment")) { %>
	          <tr>
	            <td class="identifier">
	              <label id="headerCommentLbl" for="headerComment" title="<isa:translate key="hom.jsp.proc.customer.message"/>">
	                <isa:translate key="hom.jsp.proc.customer.message"/>&nbsp;
	              </label>
	            </td>
	            <td class="value">
	              <%
	              if (ui.isElementEnabled("order.comment"))  { %>
	                <textarea id="headerComment" name="headerComment" rows="1" cols="60" onfocus="openremark()"><%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText()):"" %></textarea>
	              <%
	              } else { %>
	                <textarea id="headerComment" name="headerComment"  class="ui.getItemReadOnly()"  rows="1" cols="60" onfocus="openremark()" ui.getItemReadOnly()><%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText()):"" %></textarea>
	              <% } %>
	              <a class="icon" href="javascript: toggleremark()">
	                <img id="remark_close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.remark.showdetails"/>" width="16" height="16"/>
	                <img id="remark_open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.remark.hidedetails"/>" width="16" height="16"/></a>
	
	            </td>
	          </tr>
	       <% } %>
        </table>
        <%--End Data Message--%>
        <% if (ui.isAccessible()) { %>
          <a name="end-table4" title="<isa:translate key="b2b.acc.header.alldata"/>&nbsp;<isa:translate key="b2b.acc.header.jump"/>"></a>
        <% } %>


        <%--Error--%>
        <% if (ui.docMessageString.length() > 0) { %>
            <div class="error">
               <span><%= JspUtil.encodeHtml(ui.docMessageString) %></span>
            </div>
        <% } %>

      </div> <%--End  Document Header--%>


      <%--Item List--%>
      <div class="document-items"> <%-- level: sub2 --%>
        <%--
        The document-items layer contains:
        - a table itemlist, which displays all items.
        --%>

        <%-- Accesskey for the items section. It must be an resourcekey, so that it can be translated. --%>
        <a id="access-items" href="#access-buttons" title= "<isa:translate key="b2b.acc.items.title"/>" accesskey="<isa:translate key="b2b.acc.items.key"/>"></a>


        <%-- the item information for the basket --%>
        <%
        ItemList items = (ItemList) request.getAttribute(MaintainOrderBaseAction.RC_ITEMS);
        // Accessibility requirement
        int table1_col_nro = 9;
        if (ui.isContractInfoAvailable()) {
                table1_col_nro = table1_col_nro + 1;
        }
        if ( ! ui.header.isDocumentTypeOrderTemplate() &&
                         ! ui.header.isDocumentTypeQuotation()) {
                table1_col_nro = table1_col_nro + 2;
        }
        String table1_col_cnt = Integer.toString(table1_col_nro);
        String table1_row_cnt = Integer.toString(items.size());
        String table1_title = WebUtil.translate(pageContext, "hom.jsp.chgorder.table1", null);
        %>
        <%
        if (ui.isAccessible()) { %>
          <a id="beginTable1"
             href="#endTable1"
             title="<isa:translate key="access.table.size" arg0="<%=table1_col_cnt%>" arg1="<%=table1_row_cnt%>" arg2=""/>"
                ></a>
        <%
        } %>

        <table class="itemlist" summary="<%=table1_title%>">
            <tr>
              <%
              if (isDetailSupported) {%>
                <th class="opener">
                   &nbsp;
                   <% int numberOfRows = ui.items.size() + ui.newpos;
                      int startIndex   = 1; %>
                   <a class="icon" href='javascript: toggleAllItemDetails("rowdetail_all","rowdetail_", <%= numberOfRows%>, <%= startIndex %>)' >
                                           <img class="img-1" id="rowdetail_allclose" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.close"/> <% } %>"/>
                                           <img class="img-1" id="rowdetail_allopen" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.open"/> <% } %>"/>
                                   </a>
                </th>
              <%
              } %>
              <th class="item" scope="col">
                    <isa:translate key="b2b.order.display.posno"/>
              </th>
              <th class="product" scope="col">
                    <isa:translate key="b2b.order.display.productno"/>
              </th>
              <th class="qty" scope="col">
                    <isa:translate key="b2b.order.display.quantity"/>
              </th>
              <th class="unit" scope="col">
                    <isa:translate key="b2b.order.display.unit"/>
              </th>
              <th class="desc" scope="col">
                    <isa:translate key="b2b.order.display.productname"/>
              </th>
              <% if (ui.isContractInfoAvailable()) { %>
                <th class="ref-doc" scope="col">
                  <isa:translate key="b2b.order.display.ui.isContractInfoAvailable()"/>
                </th>
              <% } %>

              <th class="price" scope="col">
                <isa:translate key="b2b.order.display.totalprice"/>
                <br/><span class="slighttext"><isa:translate key="b2b.order.details.price"/></span>
              </th>
              <th class="qty-avail" scope="col" style="text-align:center">
                  <isa:translate key="hom.jsp.procorder.combinedorder" />
              </th>
              <th class="price"  scope="col" >
                  <isa:translate key="b2b.order.display.state"/>
              </th>

              <th class="delete" scope="col">
                    <isa:translate key="b2b.order.display.cancel"/>
              </th>
            </tr>
            <%
            int margin = 0;
            String tag_style = "";
            String tag_style_detail = ""; %>
            <isa:iterate id="item" name="<%= MaintainOrderBaseAction.RC_ITEMS %>"
                                     type="com.sap.isa.businessobject.item.ItemSalesDoc">
              <%
              /* set the item in the ui class */
              ui.setItem(item); 
              String itemKey = item.getTechKey().getIdAsString(); %>

              <% if ( !ui.isBOMSubItemToBeSuppressed() ) { %>
              <input type="hidden"   name="itemTechKey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>
              <input type="hidden"   name="productkey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getProductId().getIdAsString()) %>"/>
              <input type="hidden"   name="parentid[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getParentId().getIdAsString()) %>"/>
              <% String substMsgColspan = "10"; %>
              <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>

              <tr class="<%= ui.even_odd %>" id="row_<%= ui.line %>" onkeypress="checkReturnKeyPressed(event)" >
                <%
                tag_style = ui.getItemCSSClass();
                if (isDetailSupported) {%>
                  <td class="opener">
                    <% if (ui.showItemDetailButton()) { %> <%-- do not insert "details symbol" for configurable ui.items on sub-levels --%>
                      <a href='javascript: toggle("rowdetail_<%= ui.line %>")'
                        ><img class="icon" id="rowdetail_<%= ui.line %>close" style="DISPLAY: none"
                                 src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>"
                                 alt="<isa:translate key="b2b.order.details.item.close" />"
                                       width="16" height="16" 
                        /><img id="rowdetail_<%= ui.line %>open" style="DISPLAY: inline"
                                 src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>"
                                 alt="<isa:translate key="b2b.order.details.item.open" />"
                                       width="16" height="16" /></a>
                    <%
                    } else {%>
                      &nbsp;
                    <% } %>
                  </td>
                <%
                } %>

                <%-- if sub-level, insert subpos symbols --%>
                <td <% if (ui.itemHierarchy.isSubItem(item)) { %> class="item-sub-<%= ui.getItemLevel() -1 %>" <% } else { %> class="" <% } %>>
                    <% if (ui.isElementVisible("order.item.posNo", itemKey)) { %>
                       <%= JspUtil.removeNull(item.getNumberInt()) %>
                    <% } %>
                    <input type="hidden" name="pos_no[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getNumberInt()) %>"/>
                </td>

                <%-- Product id --%>
                <td class="product">
                  <% if (ui.isElementVisible("order.item.product", itemKey)) { %>
	                  <%
	                  if (ui.isElementEnabled("order.item.product", itemKey))  { %>
	                    <input type="text" class="textinput-middle" size="10" name="product[<%= ui.line %>]" <%= ui.getItemReadOnly() %> value="<%= JspUtil.encodeHtml(item.getProduct()) %>"/>
	                  <%
	                  } else { %>
	                    <%= JspUtil.removeNull(item.getProduct()) %>
	                    <input type="hidden" name="product[<%= ui.line %>]" <%= ui.getItemReadOnly() %> value="<%= JspUtil.encodeHtml(item.getProduct()) %>"/>
	                  <%
	                  } 
                  } else { %>
                    <input type="hidden" name="product[<%= ui.line %>]" <%= ui.getItemReadOnly() %> value="<%= JspUtil.encodeHtml(item.getProduct()) %>"/> 
                  <% } %>
                </td>

                <%-- Quantity --%>
                <td class="qty">
                  <% if (ui.isElementVisible("order.item.qty", itemKey)) { %>
	                  <% if (ui.isElementEnabled("order.item.qty", itemKey) && ui.statusOpen)  { %>
	                    <input type="text" class="textinput-small" size="3" maxlength="10" name="quantity[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getQuantity()) %>"/>
	                  <% } else { %>
	                    <%= JspUtil.removeNull(item.getQuantity()) %><input type="hidden" name="quantity[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getQuantity()) %>"/>
	                  <% } 
                  } else { %>
                     <input type="hidden" name="quantity[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getQuantity()) %>"/>
                  <% } %>
                </td>

                <%-- Unit --%>
                <td class="unit">
                  <% if (ui.isElementVisible("order.item.unit", itemKey)) { %>
	                  <%  if (ui.isElementEnabled("order.item.unit", itemKey) && ui.statusOpen)  { %>
	                    <% if (ui.units.length==1)  { %>
	                         <input type="hidden" name="unit[<%= ui.line %>]" value="<%= ui.units[0]%>"/>
	                         <div>
	                             <%= ui.units[0] %>
	                         </div>
	                    <% } else { %>
	                         <select name="unit[<%= ui.line %>]">
	                         <% for (int j = 0; j < ui.units.length; j++) { %>
	                              <option <%= ui.getItemUnitSelected(ui.units[j]) %> value="<%= ui.units[j] %>"> <%= ui.units[j] %></option>
	                         <% } %>
	                         </select>
	                    <% }
	                  } else { %>
	                    <input type="hidden" name="unit[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getUnit()) %>" />
	                    <%= JspUtil.removeNull(item.getUnit()) %>
	                  <% } %>
	              <% } else { %>   
	                 <input type="hidden" name="unit[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getUnit()) %>" />
	              <% } %>
                </td>

                <%-- Description --%>
                <td class="desc">
                  <% if (ui.isElementVisible("order.item.description", itemKey)) { %>
                     <%= JspUtil.encodeHtml(item.getDescription()) %>
                  <% } %>

                  <% if (item.isConfigurable() && ui.isElementVisible("order.item.configuration", itemKey)) { 
                  	   if (item.getExternalItem() != null) { %>
                           <a href="#" class="icon" onclick="itemconfig('<%=item.getTechKey()%>');">
                                        <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
                                        alt="<isa:translate key="status.sales.detail.config.item"/>
                                        <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.config.item"/> <% } %>" />
                           </a>
                       <% } else { %>
  				                <a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.config.imp"/>');">
                                          <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
                                           alt="<isa:translate key="status.sales.detail.config.item"/>" />
                                </a>       
                       <% } %>
                    <% } %>
                </td>

                <%-- Contract --%>
                <% if (ui.isContractInfoAvailable()) { %>
                  <td class="ref-doc" >
                    <% if ((item.getContractId() != null) && !(item.getContractId().equals("")) && ui.isElementVisible("order.item.contract", itemKey)) { %>
                      <a href="#" onclick="contractInfo('<%= JspUtil.removeNull(item.getContractKey().getIdAsString())%>', '<%= JspUtil.removeNull(item.getContractItemKey().getIdAsString())%>', '<%= JspUtil.removeNull(item.getContractId())%>');">
                      <%= JspUtil.removeNull(item.getContractId()) %> / <%= JspUtil.removeNull(item.getContractItemId()) %></a>
                    <% } else { %>
                            &nbsp;
                    <% } %>
                    <input type="hidden" name="detailContractKey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getContractKey().getIdAsString()) %>"/>
                    <input type="hidden" name="detailContractItemKey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getContractItemKey().getIdAsString()) %>"/>
                    <input type="hidden" name="contractRef[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getContractId()) %> / <%= JspUtil.removeNull(item.getContractItemId()) %>"/>
                  </td>
                <%
                } %>

                <td class="price">
                  <% if (ui.isElementVisible("order.item.netValueWOFreight", itemKey)) { %>
                     <%= JspUtil.removeNull(item.getNetValueWOFreight()) + "&nbsp;" + JspUtil.removeNull(item.getCurrency()) %>
                  <% } %>
                  <isacore:ifShopProperty property = "pricingCondsAvailable" value ="true">
                    <%
                    if (ui.header.getIpcDocumentId() != null ) { %>
                       &nbsp;<a href="#" onclick="displayIpcPricingConds(  '<%= JspUtil.removeNull(ui.header.getIpcConnectionKey()) %>',
                                                                           '<%= JspUtil.removeNull(ui.header.getIpcDocumentId().getIdAsString()) %>',
                                                                           '<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>'
                                                                           )"> <isa:translate key="b2b.order.display.ipcconds" /> </a>

                    <% } %>
                  </isacore:ifShopProperty>
                  <% if ( ui.isElementVisible("order.item.netPrice", itemKey)) { %>
                     <br/><span class="slighttext"><%= JspUtil.removeNull(item.getNetPrice()) + "&nbsp;" + JspUtil.removeNull(item.getCurrency()) + "&nbsp;" + '/' + "&nbsp;" + JspUtil.removeNull(item.getNetQuantPriceUnit()) + ' ' + JspUtil.removeNull(item.getNetPriceUnit()) %></span>
                  <% } %>
                </td>


                <%-- Combined Order Referenz --%>
                <td class="ref-doc">
	               <%-- if (ui.isElementVisible("order.item.connectedItem", itemKey)) { --%>
	                  <%
	                  ConnectedDocumentItemData connectedItem= item.getSucessor();
	                  if (connectedItem == null) { %>
	                    -- / --
	                  <%
	                  } else { %>
	                    <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>?techkey=<%=connectedItem.getDocumentKey()%>&objects_origin=&object_id=&objecttype=order"><%=JspUtil.removeNull(connectedItem.getDocNumber()) %> / <%=JspUtil.removeNull(connectedItem.getPosNumber()) %></a>
	                  <%
	                  } %> 
	            <%--   } --%>
                </td>

                <%-- Status --%>
                <%
                Iterator extStatus = (Iterator)item.getExtendedStatus().getStatusIterator();
                request.setAttribute("EXTENDEDSTATUS",extStatus); %>
                <td class="<%= tag_style %>" style="vertical-align: middle">
                  <% if (ui.isElementVisible("order.item.status", itemKey)) { %>
	                  <isa:iterate id="eStatus" name="EXTENDEDSTATUS" type="com.sap.isa.businessobject.ExtendedStatusListEntry">
	                    <%= eStatus.isCurrentStatus() ? eStatus.getDescription() : ""%>
	                  </isa:iterate>
	              <% } %>
                </td>

                <%-- Mark for deletion --%>
                <td class="delete" align="center">

                  <% String replaceType = "";
                  if (!ui.itemHierarchy.isSubItem(item)) {
                    if (item.isDeletable()) {
                          replaceType = item.isProductChangeable() ? "productchangeable" : "deletable"; %>
                          <input type="checkbox" name="delete[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>" />
                          <%
                    }
                    else if (item.isCancelable ()) {
                          replaceType = "cancelable"; %>
                          <input type="checkbox" name="cancel[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>" />
                    <%
                    }
                  } else { %>
                    &nbsp;
                  <%
                  } %>

                <input type="hidden" name="replacetype[<%= ui.line %>]" value="<%= replaceType %>"/>
                <input type="hidden" name="itemTechKey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getTechKey().getIdAsString()) %>"/>
                <input type="hidden" name="productkey[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getProductId().getIdAsString()) %>"/>
                <input type="hidden" name="parentid[<%= ui.line %>]" value="<%= JspUtil.removeNull(item.getParentId().getIdAsString()) %>"/>
                </td>
              </tr>


              <%-- Item Details --%>
              <% tag_style_detail = ui.even_odd + "-detail"; %>

              <tr class="<%= tag_style_detail %>" id="rowdetail_<%= ui.line %>" <% if (ui.isToggleSupported) {%> style="display:none;"<%}%>>
                <td class="select">&nbsp;</td>
                <td class="detail"
                  <% if (ui.isContractInfoAvailable()) { %>
                       colspan="14">
                  <% } else { %>
                       colspan="13">
                  <% } %>
                  <table class="item-detail">
                   <%-- display of product configuration --%>
                    <% if (ui.isElementVisible("order.item.configuration", itemKey)) { %>
                       <%@ include file="/ecombase/orderitemconfiginfo.inc.jsp" %>
                    <% } %>
                    <%--  Request Delivery Date --%>
                    <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey)) { %>
	                    <tr>
	                      <td class="identifier">
	                        <label for="reqdeliverydate_<%= ui.line %>"><isa:translate key="b2b.order.reqdeliverydate"/></label>
	                      </td>
	                      <td class="value">
	                        <input name="reqdeliverydate[<%= ui.line %>]"
	                               id="reqdeliverydate_<%= ui.line %>"
	                               type="text" class="textinput-middle" size ="15"
	                                 value="<%= JspUtil.replaceSpecialCharacters(item.getReqDeliveryDate()) %>" />
	                        <a href="#" onclick="setDateField(document.order_positions['reqdeliverydate[<%= ui.line %>]']);setDateFormat('<%=ui.getDateFormat()%>');isaTop().newWin = window.open('<isa:webappsURL name="b2b/calendar.jsp"/>','cal','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes'); isaTop().newWin.focus();"
	                          ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>" class="display-image" /></a>
	                      </td>
	                    </tr>
	                <% } else { %>
	                  <input name="reqdeliverydate[<%= ui.line %>]" type="hidden" value "<%= JspUtil.replaceSpecialCharacters(item.getReqDeliveryDate()) %>" />
	                <% } %>
                    <%
                    if (ui.isB2BSupported) { %>
                      <%-- Shipto --%>
                      <% if ( !ui.isElementVisible("order.item.deliverTo", itemKey)) { %>
                          <input type="hidden" name="customer[<%= ui.line %>]" value=""/>
                      <% } else { %>
	                      <tr>
	                        <td class="identifier">
	                          <isa:translate key="b2b.order.display.soldto"/>&nbsp;
	                        </td>
	                        <% if (! ui.isElementEnabled("order.item.deliverTo", itemKey)) { %>
	                           <%= item.getShipTo().getShortAddress() %>
                                <a class="icon" href="#" onclick="showShipTo(document.order_positions['customer[<%= ui.line %>]'].selectedIndex);" >
		                           <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/>" class="display-image" /></a>
	                        <% } else { %>
		                        <td class="value">
		                          <select name="customer[<%= ui.line %>]">
		                            <isa:iterate id="shipTo"
		                                                         name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
		                                                         type="com.sap.isa.businessobject.ShipTo">
		                                  <% String selected = "";
		                                        if (shipTo.equals(item.getShipTo())) {
		                                                selected = "selected=\"selected\"";
		                                   } %>
		
		                                  <option <%= selected %> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
		                            </isa:iterate>
		                          </select>&nbsp;
		                          <a class="icon" href="#" onclick="showShipTo(document.order_positions['customer[<%= ui.line %>]'].selectedIndex);" >
		                                                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/>" class="display-image" /></a>&nbsp;
		                          <a class="icon" href="#" onclick="newShipToItem('<%= item.getTechKey() %>');">
		                                <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_new_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.icon.newshipto"/>" class="display-image" /></a><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="35" height="1" alt="" />
		                        </td>
		                     <% } %>
	                      </tr>
	                  <% } %>
                      <%-- Notes --%>
                      <% if (ui.isElementVisible("order.item.comment", itemKey)) { %>
	                      <tr>
	                         <td class="identifier">
	                                 <isa:translate key="b2b.order.details.note"/>
	                         </td>
	
	                         <td class="value">
	                            <% if (ui.isElementEnabled("order.item.comment", itemKey)) { %>
	                                 <textarea id="comment[<%= ui.line %>]" name="comment[<%= ui.line %>]" rows="2" cols="80" ><%= JspUtil.replaceSpecialCharacters(item.getText().getText()) %></textarea>
	                            <% } else { %>
	                               <%= JspUtil.encodeHtml(item.getText().getText()) %>
	                            <% } %>
	                         </td>
	                      </tr>
	                   <% } %>
                    <%
                    }
                    else {%>
                     <input type="hidden" name="customer[<%= ui.line %>]" value=""/>
                    <%
                    }%>
                  </table>
                </td>
              </tr>

              <%--   messages --%>
              <%
              ui.setItemCSSClass(ui.even_odd + "-error");
              if (ui.itemMessageString.length() > 0) { %>
                <tr class="<%= ui.getItemCSSClass() %>">
                  <td >&nbsp;</td>
                  <td colspan="10">
                    <div class="error-items">
                      <span>
                        <%= JspUtil.encodeHtml(ui.itemMessageString) %>
                      </span>
                    </div>
                  </td>
              </tr>
              <% } %>
             <% } //  ui.isBOMSubItemToBeSuppressed() %>
            </isa:iterate>

            <%-- Start Code empty lines --%>
            <%-- Add some empty lines for additional basket-data --%>
            <%
            for (int i = 0; i < ui.newpos; i++) { %>
              <% ui.line++; %>
              <% ui.even_odd = ui.alternator.next();
                 tag_style= "poslist-mainpos-"+ui.even_odd; %>
              <tr class="<%= ui.even_odd %>" id="row_<%= ui.line %>" onkeypress="checkReturnKeyPressed(event)">
                 <%  
                 if (isDetailSupported) {%>
                        <td class="<%= tag_style %>">
                          <% if (ui.isToggleSupported) { %> <%-- do not insert "details symbol" for configurable ui.items on sub-levels --%>
                            <a href='javascript: toggle("rowdetail_<%= ui.line %>")' class="iconlink"
                              ><img id="rowdetail_<%= ui.line %>close" style="DISPLAY: none"
                                   src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>"
                                   alt="<isa:translate key="b2b.order.details.item.close" />"
                                   width="16" height="16" 
                              /><img id="rowdetail_<%= ui.line %>open" style="DISPLAY: inline"
                                   src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>"
                                   alt="<isa:translate key="b2b.order.details.item.open" />"
                                   width="16" height="16" /></a>
                          <%
                          } else {%>
                            &nbsp;
                          <% } %>
                        </td>
                    <% } %>
                <td class="item" colspan="1">
                  &nbsp;
                </td>
                <td class="product">
                  <% if (ui.isElementEnabled("order.item.product")) { %>
                     <input type="text" class="textinput-middle" size="10" name="product[<%= ui.line %>]" value="" />
                  <% } %>
                </td>

                <td class="qty">
                  <% if (ui.isElementEnabled("order.item.qty")) { %>
                     <input type="text" class="textinput-small" size="5" name="quantity[<%= ui.line %>]" maxlength="8" value="" />
                  <% } %>
                </td>

                <%-- unit --%>
                <td class="unit">
                      &nbsp;
                </td>

                <td class="desc">
                  &nbsp;
                </td>
                <% if (ui.isContractInfoAvailable()) { %>
                  <td class="ref-doc">
                     &nbsp;
                  </td>
                <% } %>
                <td class="price">
                  <%-- total price --%>
                  &nbsp;
                </td>
                <td class="qty-avail" scope="col"">
                  <%-- reorder --%>
                  &nbsp;
                </td>
                <td  class="price">
                      &nbsp;
                </td>
                <td class="delete">
                  <%-- deletion --%>
                  &nbsp;
                </td>

              <%-- item detail --%>
              <% tag_style_detail = ui.even_odd + "-detail"; %>
              <tr id="rowdetail_<%= ui.line %>" class="<%= tag_style_detail %>"
                <% if (ui.isToggleSupported) { %>
                  style="display:none;"
                <% } %>>

                <td class="select">&nbsp;</td>
                <td class="detail"
                  <% if (ui.isContractInfoAvailable()) { %>
                    colspan="11">
                  <% } else { %>
                    colspan="10">
                  <% } %>

                  <%-- Details --%>
                  <table class="item-detail">
                    <%--  Request Delivery Date --%>
                    <% if (ui.isElementEnabled("order.item.reqDeliveryDate")) { %>
	                    <tr>
	                      <td class="identifier">
	                              <isa:translate key="b2b.order.reqdeliverydate"/>
	                      </td>
	                      <td class="value">
	                        <input name="reqdeliverydate[<%= ui.line %>]"
	                                 type="text" class="textInput" size ="15" />
	                        <a href="#" onclick="setDateField(document.order_positions['reqdeliverydate[<%= ui.line %>]']);setDateFormat('<%=ui.getDateFormat()%>');isaTop().newWin = window.open('<isa:webappsURL name="b2b/calendar.jsp"/>','cal','width=300,height=370,screenX=200,screenY=300,titlebar=yes,resizable=yes'); isaTop().newWin.focus();"
	                          ><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/calendar.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.shipto.icon.calendar"/>" class="display-image" /></a>
	                      </td>
	                    </tr>
	                <% } %>

                    <% if (ui.isB2BSupported) { %>
                      <%-- Shipto --%>
                      <tr>
                          <% if ( !ui.isElementVisible("order.item.deliverTo")) { %>
                              <input type="hidden" name="customer[<%= ui.line %>]" value=""/>
                          <% } else { %>
	                          <td class="identifier">
	                            <isa:translate key="b2b.order.display.soldto"/>
	                          </td>
	                          <td class="value">
                                 <% if (ui.isElementEnabled("order.item.deliverTo")) { %>                          
		                            <div class="vertical-align-middle">
		                              <select name="customer[<%= ui.line %>]">
		                                      <isa:iterate id="shipTo"
		                                              name="<%= MaintainBasketBaseAction.SC_SHIPTOS %>"
		                                              type="com.sap.isa.businessobject.ShipTo">
		                                      <% String selected = ""; %>
		
		                                      <option <%= selected %> value="<%= shipTo.getTechKey() %>"><%= JspUtil.encodeHtml(shipTo.getShortAddress()) %></option>
		                                      </isa:iterate>
		                              </select>&nbsp;
		                              <a class="icon" href="#" onclick="showShipTo(document.order_positions['customer[<%= ui.line %>]'].selectedIndex);" >
		                                      <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/>" class="display-image" /></a>
		                            </div>
		                          <% } %>
	                          </td>
	                       <% } %>
                      </tr>

                      <%-- notes --%>
                      <% if (ui.isElementEnabled("order.item.comment")) { %>
	                      <tr>
	                        <td class "identifier">
	                          <isa:translate key="b2b.order.details.note"/>
	                        </td>
	                        <td class="value">
	                          <textarea id="comment[<%= ui.line %>]" name="comment[<%= ui.line %>]" rows="2" cols="80" ></textarea>
	                        </td>
	                      </tr>
	                   <% } %>
                    <%
                    }
                    else {%>
                      <input type="hidden" name="customer[<%= ui.line %>]" value=""/>
                    <%
                    }%>

                  </table>
                </td>
              </tr>

              <input type="hidden" name="pos_no[<%= ui.line %>]"            value="10" />
              <input type="hidden" name="netprice[<%= ui.line %>]"          value="" />
              <input type="hidden" name="itemTechKey[<%= ui.line %>]"       value="" />
              <input type="hidden" name="productkey[<%= ui.line %>]"        value="" />
              <% if (ui.isTransferCatalogKey()) { %>
                      <input type="hidden" name="pcat[<%= ui.line %>]"          value="<%= ui.getCatalog() %>" />
              <% } %>
              <input type="hidden" name="pcatVariant[<%= ui.line %>]"       value="" />
              <input type="hidden" name="pcatArea[<%= ui.line %>]"          value="" />
            <% }  /* end for */ %>

            <%-- Ende Code empty lines --%>
        </table>

        <%
        if (ui.isAccessible()) { %>
          <a id="endTable1"
             href="#beginTable1"
             title="<isa:translate key="access.table.exit"/>"
           ></a>
        <%
        } %>



        <input type="hidden" name="sendpressed" value="" />
        <input type="hidden" name="cancelpressed" value="" />
        <input type="hidden" name="changestatuspressed" value="" />
        <input type="hidden" name="actualpos" value="1" />
        <input type="hidden" name="refresh" value="" />
        <input type="hidden" name="newpos" value="" />
        <input type="hidden" name="newshipto" value="" />
        <input type="hidden" name="configitemid" value="" />

      
      </div> <%--End  Document Items--%>
    </form>
  </div> <%--End  Document--%>


  <%--Buttons--%>
  <div id="buttons">
    <%-- Accesskey for the button section. It must be an resourcekey, so that it can be translated. --%>
    <a id="access-buttons" href="#access-header" title="<isa:translate key="b2b.acc.buttons.sum"/>" accesskey="<isa:translate key="b2b.acc.buttons.key"/>"></a>

    <ul class="buttons-1">
       <% if (!ui.isAuction() && !ui.isServiceRecall()) {
          if (!ui.isHeaderChangeOnly()) { %>
             <li>
                <select id="newposcount" size="1" title="<isa:translate key="b2b.acc.buttons.newpos.qt"/>">
                             <option value="" >0 <isa:translate key="b2b.newpos"/></option>
                             <option value="1">1 <isa:translate key="b2b.newpos"/></option>
                             <option value="2">2 <isa:translate key="b2b.newpos"/></option>
                             <option value="5">5 <isa:translate key="b2b.newpos"/></option>
                </select>
             </li>
             <li>
                <img class="icon" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/hint.gif") %>" alt="<isa:translate key="b2b.acc.buttons.newpos.qt"/>"/>
             </li>
         <% } %>
       <% } %>
    </ul>
    <ul class="buttons-2">
        <li><a href="#" onclick="submit_refresh();" <% if (ui.isAccessible()) { %> title="<isa:translate key="b2b.acc.buttons.upd"/>" <% } %> ><isa:translate key="b2b.order.display.submit.update"/></a></li>
    </ul>
    <ul class="buttons-3">
       <li><a href="#" onclick="cancel_confirm();" <% if (ui.isAccessible()) { %> title="<isa:translate key="b2b.acc.buttons.cncl"/>" <% } %>><isa:translate key="b2b.order.display.submit.close"/></a></li>
       <li><a href="#"  onclick="send_confirm();" <% if (ui.isAccessible()) { %> title="<isa:translate key="b2b.acc.buttons.save.ord"/>" <% } %> ><isa:translate key="b2b.order.change.submit.save"/></a></li>
    </ul>
  </div>


</body>
</html>

<% ui.items.clear(); %>
