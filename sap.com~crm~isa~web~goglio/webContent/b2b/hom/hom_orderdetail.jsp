<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page errorPage="/appbase/jspruntimeexception.jsp" %>

<%@ page import="com.sap.isa.core.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="com.sap.isa.isacore.*" %>
<%@ page import="com.sap.isa.isacore.action.ActionConstantsBase" %>
<%@ page import="com.sap.isa.isacore.action.ShowShipToAction" %>
<%@ page import="com.sap.isa.businessobject.order.*" %>
<%@ page import="com.sap.isa.businessobject.item.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.order.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.businessobject.*" %>
<%@ page import="com.sap.isa.businessobject.header.*" %>
<%@ page import="com.sap.isa.businesspartner.businessobject.*" %>
<%@ page import="com.sap.isa.businesspartner.action.*" %>
<%@ page import="com.sap.isa.isacore.actionform.order.orderdownload.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.b2b.order.*" %>
<%@ page import="com.sap.isa.isacore.uiclass.PaymentUI" %>
<%@ page import="com.sap.isa.core.UserSessionData"%>
<%@ page import="com.sap.isa.businessobject.BusinessObjectManager"%>
<%@ page import="com.sap.isa.payment.businessobject.PaymentCCard"%>
<%@ page import="com.sap.isa.backend.boi.isacore.*"%>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>



<% CustomerOrderStatusUI ui = new CustomerOrderStatusUI(pageContext);
   String ZERO = "00000000000000000000000000000000";
   //inline display of product configuration will be restricted on values of identifying characteristics
   boolean showDetailView = false;
%>

<isa:contentType />


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
<head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <title>Sales Document Status</title>

  <isa:stylesheets />
  
<% UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
	BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
	if(bom.getShop().isPricingCondsAvailable() == true) {%>
	  <script src='<isa:webappsURL name="appbase/jscript/ipc_pricinganalysis.jsp"/>' 
			type = "text/javascript">
	  </script>
 <% } %>

  <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/reload.js")%>"
          type="text/javascript">
  </script>
  <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/openwin.js")%>"
          type="text/javascript">
  </script>
  <script src="<isa:mimeURL name="b2b/jscript/frames.js" />"
          type="text/javascript">
  </script>
  <script src="<isa:mimeURL name="b2b/jscript/getElementsByType.js" />"
          type="text/javascript">
  </script>

  <script src="<%=WebUtil.getMimeURL(pageContext, "mimes/jscript/EComBase.js") %>"
          type="text/javascript">
  </script>

  <script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/orderTools.js") %>"
          type="text/javascript">
  </script>


  <script type="text/javascript">
  <!--
    var noflash = 'X';

    function markAll() {
        var inputs = getElementsByType('order_positions','checkbox');
        for (var i=0; i<inputs.length;i++) {
          if (inputs[i].type == 'checkbox')
            inputs[i].checked = true;
       }
    }

    function checkMarked() {
        // NO NS4 support : var inputs = document.getElementsByTagName('input');
        var inputs = getElementsByType('order_positions','checkbox');
        checkedItem = false;
        for (var i=0; i<=inputs.length;i++) {
          if ((inputs[i] != null) && (inputs[i].type == 'checkbox') && (inputs[i].checked))
            checkedItem = true;
        }
        return checkedItem;
    }

    function trackwin(sURL) {
        tw = window.open(sURL,"Tracking","width=670,height=600,left=400,top=100,resizable=yes,scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no");
        tw.focus();
    }

    function itemconfig(itemId) {
        form_input().location.href = "<isa:webappsURL name='/b2b/inititemconfiguration.do'/>?<%=ItemConfigurationBaseAction.PARAM_ITEMID%>=" + itemId;
        return true;
    }


    function closeDocument() {
            document.location.href = '<isa:webappsURL name="b2b/statusdocumentclose.do"/>';
    }

    function print() {
      document.location.href = '<isa:webappsURL name="b2b/statusdocumentprint.do"/>';
    }


    function processDocument() {
      document.forms["documentprocess"].submit();
      return true;
    }


    function changeDocument() {
      document.forms["documentchange"].submit();
      return true;
    }

    function showShipTo(index) {
      var url = "<isa:webappsURL name="b2b/showshipto.do"/>?<%=ShowShipToAction.PARAM_SHIPTO_INDEX%>=" + index;
      var sat = window.open(url, 'sat_details', 'width=450,height=450,menubar=no,locationbar=no,scrolling=auto,resizable=yes');
      sat.focus();
      return true;
    }

    <%-- needed for Order Download --%>
    function prepareDownload() {
      document.forms["orderdownload"].orderIDS.value = document.forms["order_positions"].salesdocnumber.value;
      document.forms["orderdownload"].orderGUIDS.value = document.forms["order_positions"].techkey.value;
      document.forms["orderdownload"].submit();
      return true;
    }


  <%@ include file="/b2b/jscript/showSoldTo.jsp"  %>
  //-->
  </script>

</head>

<body class="orderstatus">

  <div class="module-name"><isa:moduleName name="b2b/hom/hom_orderdetail.jsp" /></div>

  <%-- Show accessibility messages --%>
  <%
  if (ui.isAccessible) { %>
    <%@ include file="/appbase/accessibilitymessages.inc.jsp"  %>
  <%
  } %>


  <%-- Order Number and Date --%>
  <h1><isa:translate key="hom.jsp.gen.edit.customer.view"/>&nbsp;<isa:translate key="hom.jsp.gen.edit.customer.order"/>
      <%= JspUtil.removeNull(ui.getDocNumber()) %>&nbsp;<isa:translate key="hom.jsp.gen.edit.cust.order.date"/>&nbsp;<%= ui.getDocDate()%>&nbsp;</h1>

  <div id="document">

    <div class="document-header">

      <a href="#" title="<isa:translate key="hom.jsp.orderdtl.pagebegin"/>" accesskey="<isa:translate key="hom.jsp.orderdtl.pagekey"/>"></a>

      <%-- Document Header Area --%>
      <div class="header-basic">
        <table class="layout"> <%-- level: sub3 --%>
          <tr>
            <td class="col1">


              <%
                String documentDeliveryStatusKey = "status.sales.status.".concat(ui.header.getDeliveryStatus());
				String soldName[] = {JspUtil.encodeHtml(ui.getSoldToName())};
				String showSold = WebUtil.translate(pageContext, "hom.jsp.orderdtl.showSold", soldName);
              %>
              <table class="header-general" summary="<isa:translate key="ecm.acc.header.dat.sum"/>">


                <%-- BuyerInfo  --%>
                <% if ( ui.isElementVisible("order.soldTo") ) { %>
	                <tr>
	                  <td class="identifier"><isa:translate key="hom.jsp.processorder.customer"/></td>
	                  <td class="value">
	                      <a title="<isa:translate key="access.link" arg0="<%=showSold%>" arg1=""/>" href="#" onclick="showSoldTo('<%=ui.getSoldToKey().getIdAsString()%>')"><b><%= JspUtil.removeNull(ui.getSoldToId()) %></b></a>&nbsp;<span><%= JspUtil.encodeHtml(ui.getSoldToName()) %></span>
	                  </td>
	                </tr>
	            <% } %>

                <%-- Reference Number --%>
                <% if ( ui.isElementVisible("order.numberExt") ) { %>
	                <tr>
	                  <td class="identifier"><isa:translate key="hom.jsp.processorder.reference"/></td>
	                  <td class="value"><%= JspUtil.encodeHtml(ui.header.getPurchaseOrderExt()) %></td>
	                </tr>
	            <% } %>

                <%-- Description --%>
                <% if ( ui.isElementVisible("order.description") ) { %>
	                <tr>
	                  <td class="identifier"><isa:translate key="hom.jsp.processorder.description"/></td>
	                  <td class="value"><%= JspUtil.encodeHtml(ui.header.getDescription()) %></td>
	                </tr>
	            <% } %>


                <%-- Shipconditions --%>
	            <% if (ui.isElementVisible("order.shippingCondition")) { %>    
	                <tr>
	                  <td class="identifier"><isa:translate key="b2b.order.display.shipcond"/></td>
	                  <td class="value"><%= JspUtil.encodeHtml(ui.header.getShipCond()) %></td>
	                </tr>
	            <% } %>

                <%-- Ship To --%>
                <% if (ui.isElementVisible("order.deliverTo")) { %>
	                <tr>
	                  <td class="identifier"><isa:translate key="b2b.order.display.soldto"/></td>
	                  <td class="value">
	                    <%
	                    if (ui.header.getShipTo() != null) {
	                      if (ui.isPickupFromVendor) { %>
	                        <isa:translate key="hom.jsp.CollectionFromVend"/>
	                      <%
	                      }
	                      else { 
							String showShip = WebUtil.translate(pageContext, "hom.jsp.orderdtl.showShip", null);
	                      %>
	                        <a title="<isa:translate key="access.link" arg0="<%=showShip%>" arg1=""/>" class="icon" href="#" onclick="showShipTo('<%=ui.header.getShipTo().getTechKey().getIdAsString()%>');" ><%= JspUtil.encodeHtml(ui.header.getShipTo().getShortAddress()) %></a>
	                      <%
	                      }
	                    } %>
	                  </td>
	                </tr>
	            <% } %>


                <%-- Payment infos --%>
	            <% if (ui.isElementVisible(PaymentUI.PAYMENT_TYPE)) { %>    
	                <tr>
	                   <td class="identifier"><isa:translate key="b2b.order.details.paymenttype"/></td>
	                   <td class="value">
	                      <isa:translate key="<%= ui.paymentKey %>"/>
	                      <%
	                      if (ui.paytypeIsCard) { 
							String togglePay = WebUtil.translate(pageContext, "hom.jsp.orderdtl.togglePay", null);
						  %>
	                        <a title="<isa:translate key="access.link" arg0="<%=togglePay%>" arg1=""/>" href="javascript: togglepayment()"><img id="payment_close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.creditcard.hidedetails"/>" width="16" height="16"/><img id="payment_open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.creditcard.showdetails"/>" width="16" height="16"/></a>
	                      <%
	                      } %>
	                   </td>
	                </tr>
	            <% } %>

                <%-- Payment infos: Additional information for credit card --%>
                <%
                if (ui.paytypeIsCard) { %>
                  <tr id="payment_row" style="DISPLAY:none"> <%-- style="DISPLAY:none" --%>
                    <td>&nbsp;</td>
                    <td>
                      <table>
                        <% if (ui.isElementVisible(PaymentUI.ONECARD_TYPE)) { %>
	                        <tr>                          
	                          <td><span><isa:translate key="b2b.payment.credCardInst"/>&nbsp;</span></td>
	                          <%// list of all credit card institutes
	                            ResultData listOfCardInst = (ResultData) request.getAttribute(MaintainOrderBaseAction.RC_CARDTYPE);
	                          %>
	                          <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getTypeDescription()) %></span></td>
	                        </tr>
	                    <% } %>
                        <% if (ui.isElementVisible(PaymentUI.ONECARD_HOLDER)) { %>
	                        <tr>
	                          <td><span><isa:translate key="b2b.payment.creditCardOwner"/></span></td>
	                          <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getHolder()) %></span></td>
	                        </tr>
	                    <% } %>
                        <% if (ui.isElementVisible(PaymentUI.ONECARD_NUMBER)) { %>
	                        <tr>
	                          <td><span><isa:translate key="b2b.payment.creditCardNumber"/></span></td>
	                          <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getNumber()) %></span></td>
	                        </tr>
	                     <% } %>   
	                     <% if (ui.isElementVisible(PaymentUI.ONECARD_SUFFIX)) { %>   
	                        <tr>
	                          <td><span><isa:translate key="b2b.payment.swtchCardSrNo"/></span></td>
	                          <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getNumberSuffix()) %></span></td>
	                        </tr>
	                     <% } %>   
	                     <% if (ui.isElementVisible(PaymentUI.ONECARD_VALIDITY)) { %>   
	                        <tr>
	                          <td><span><isa:translate key="b2b.payment.CCValidity"/></span></td>
	                          <td><span><%= JspUtil.removeNull(((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getExpDateMonth() + "/" + ((PaymentCCard) ui.payment.getPaymentMethods().get(0)).getExpDateYear()) %></span></td>
	                        </tr>
	                      <% } %>  
	                      <% if (ui.isElementVisible(PaymentUI.CARD_CVV)) { %>  
	                        <tr>
	                          <td><span><isa:translate key="b2b.payment.crdtCardCVV"/></span></td>
	                          <td><%-- = JspUtil.removeNull(ui.payment.maskStringWithStars(ui.payment.getCardCVV(), 2)) --%></td>
	                        </tr>
	                      <% } %>  
                      </table>
                    </td>
                  </tr>
                <%
                } %>

              </table>
            </td>
            <td class="col2">

              <%-- Header Status Table--%>
              <% if (!ui.header.getDocumentType().equals(DocumentListSelectorForm.ORDERTEMPLATE) && ui.isElementVisible("order.status.overall")) { %>
	              <table class="status" summary="<isa:translate key="ecm.acc.head.status"/>">
	                <tr>
	                  <td class="identifier"><isa:translate key="hom.jsp.procorder.customer.state"/></td>
	                  <td class="identifier"><isa:translate key="<%= ui.getDocumentStatusKey() %>"/></td>
	                </tr>
	              </table>
	          <% } %>


              <%-- Price Table--%>
              <table class="price-info" summary="<isa:translate key="ecm.acc.header.prieces"/>">

                <%-- Net Value --%>
                <% if (ui.isElementVisible("order.netValueWOFreight")) { %>
	                <tr>
	                  <td class="identifier"><isa:translate key="b2b.order.display.pricenet"/></td>
	                  <td class="value">
	                    <%= JspUtil.removeNull(ui.header.getNetValueWOFreight())%> &nbsp; <%= JspUtil.removeNull(ui.header.getCurrency()) %>
	                  </td>
	                </tr>
                <% } %>
                <%-- Freight --%>
	            <% if (ui.isElementVisible("order.freight")) { %> 
	                <tr>
	                  <td class="identifier"><isa:translate key="b2b.order.display.freight"/></td>
	                  <td class="value">
	                    <%= JspUtil.removeNull(ui.header.getFreightValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %>
	                  </td>
	                </tr>
                <% } %>
                <%-- Tax --%>
	            <% if (ui.isElementVisible("order.tax")) { %>   
                    <tr>
	                  <td class="identifier"><isa:translate key="b2b.order.display.tax"/></td>
	                  <td class="value">
	                    <%= JspUtil.removeNull(ui.header.getTaxValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %>
	                  </td>
	                </tr>
                <% } %>

                <%-- Total Prices --%>
	            <% if (ui.isElementVisible("order.priceGross")) { %>
	                <tr>
	                  <td class="identifier"><isa:translate key="b2b.order.display.pricebrut"/></td>
	                  <td class="value">
	                    <%= JspUtil.removeNull(ui.header.getGrossValue()) %>&nbsp;<%= JspUtil.removeNull(ui.header.getCurrency()) %>
	                  </td>
	                </tr>
	            <% } %>
              </table>
            </td>
          </tr>
        </table> <%-- class="layout"> --%>
      </div><%--header-basic--%>

      <%-- Customer Message --%>
	  <% if (ui.isElementVisible("order.comment")) { %>
	      <table class="message" summary="<isa:translate key="ecm.acc.head.msg.d"/>">
	        <tr>
	          <td class="identifier"><label for="headerComment"><isa:translate key="hom.jsp.proc.customer.message"/></label></td>
	          <td class="value">
	            <textarea id="headerComment" rows="1" cols="80" onfocus="openremark()" name="headerComment"  disabled="disabled" title="<%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText()):"" %>"><%= (ui.header.getText() != null) ? JspUtil.encodeHtml(ui.header.getText().getText()):"" %></textarea>
	            <a title="<isa:translate key="hom.jsp.orderdtl.toggleMsg"/>" href="javascript: toggleremark()"><img id="remark_close" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>" alt="<isa:translate key="b2b.remark.showdetails"/>" width="16" height="16"/><img id="remark_open" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>" alt="<isa:translate key="b2b.remark.hidedetails"/>" width="16" height="16"/></a>
	          </td>
	        </tr>
	      </table>
	  <% } %>
    </div> <%-- document-header--%>


    <%-- Document Item Rows --%>
    <div class="document-items">

      <% String header_rowspan="2";%>

      <% String documentItemStatusKey = ""; %>
      <% String documentItemStatusNameKey = "status.sales.detail.sn.".concat(ui.header.getDocumentType()); %>
      <form name="order_positions" method="post" action='<isa:webappsURL name ="/b2b/documentstatusaddtobasket.do"/>'>

        <%
        int item_colspan = 9;

        // Accessibility requirement
        int b2b_hom_orderdetail_table1_col_nro = 8;
        if (ui.isContractInfoAvailable()) {
          b2b_hom_orderdetail_table1_col_nro = b2b_hom_orderdetail_table1_col_nro + 1;
        }
        String b2b_hom_orderdetail_table1_col_cnt = Integer.toString(b2b_hom_orderdetail_table1_col_nro);
        String b2b_hom_orderdetail_table1_row_cnt = Integer.toString(ui.items.size());
        String b2b_hom_orderdetail_table1_title = WebUtil.translate(pageContext, "hom.jsp.orderdtl.table1", null);

        if (ui.isAccessible()) 
        {
        %>
         <div>
          <a id="b2b_hom_orderdetail_table1_begin"
             href="#b2b_hom_orderdetail_table1_end"
			 title="<isa:translate key="access.table.begin" arg0="<%=b2b_hom_orderdetail_table1_title%>" arg1="<%=b2b_hom_orderdetail_table1_row_cnt%>" arg2="<%=b2b_hom_orderdetail_table1_col_cnt%>" arg3="1" arg4="<%=b2b_hom_orderdetail_table1_row_cnt%>"/>">
          </a>
         </div>
        <%
        }
        %>
        <table id="itemlist" class="itemlist" border="1" cellspacing="0" cellpadding="3" summary="<%=b2b_hom_orderdetail_table1_title%>">

          <tr>
            <%
            if (ui.showItemDetailButton() ) {
              item_colspan++; %>
              <th class="opener" scope='col' >
                  <% int numberOfRows = ui.items.size() + ui.newpos;
                     int startIndex   = 1; %>
                  <a class="icon" href='javascript: toggleAllItemDetails("rowdetail_all","rowdetail_", <%= numberOfRows%>, <%= startIndex %>)' >
                         <img class="img-1" id="rowdetail_allclose" style="DISPLAY: none" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_close_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.close.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.close"/> <% } %>">
                                     <img class="img-1" id="rowdetail_allopen" style="DISPLAY: inline" src="<%=WebUtil.getMimeURL(pageContext,"b2b/mimes/images/icon_open_green.gif")%>" alt="<% if (ui.isAccessible) { %><isa:translate key="b2b.ord.det.item.open.all.acc" /> <% } else { %><isa:translate key="b2b.order.details.item.all.open"/> <% } %>">
                                  </a>

              </th>
            <%
            }%>
            <th class="item"  scope="col"><isa:translate key="status.sales.detail.itemposno"/></th>
            <th class="product" scope="col"><isa:translate key="status.sales.detail.productno"/></th>
            <th class="poslist" scope="col"><isa:translate key="status.sales.detail.itemquantity"/></th>
            <th class="poslist" style="text-align:center"><isa:translate key="status.sales.detail.description"/></th>
            <% if (ui.isContractInfoAvailable()) { %>
              <th class="item" scope="col" ><isa:translate key="status.sales.detail.itemcontract"/></th>
            <% } %>
            <th class="price" scope="col" ><isa:translate key="status.sales.detail.totalvalue"/> <br/>
                <em><isa:translate key="b2b.order.details.price"/></em>
            </th>
            <th class="" scope="col" ><isa:translate key="hom.jsp.procorder.deliverydate"/></th>

            <th class="ref-doc" scope="col"><isa:translate key="hom.jsp.procorder.combinedorder"/></th>

            <th class="status" scope="col"><isa:translate key="hom.jsp.procorder.customer.state"/></th>
            <th class="status" scope="col"><isa:translate key="hom.jsp.processorder.itemstatus"/></th>
          </tr>


          <%
          String tag_style = "";
          String tag_style_detail = "";
          int lineIndex = 1;
          int margin = 0;
          String rowname = "";
          boolean followingSubItems = false;


          String textarea_cols = "80";
          if (ui.isContractInfoAvailable()) {
              item_colspan++;
          }

          String subinfos_colspan = "" + item_colspan ;
          String substMsgColspan = subinfos_colspan;
          ui.setItemsToIterate();

          %>


          <isa:iterate id="orderitem" name="<%= ActionConstantsBase.RK_SALES_DOC_STATUS_DETAIL_ITEMITERATOR %>"
                       type="com.sap.isa.businessobject.item.ItemSalesDoc">

            <%
            documentItemStatusKey = "status.sales.status.".concat(orderitem.getStatus());

            /* set the item in the ui class */
            ui.setItem(orderitem);
            ItemSalesDoc item; 
            String itemKey = orderitem.getTechKey().getIdAsString(); %>

            <% if ( !ui.isBOMSubItemToBeSuppressed() ) { %>

            <%@ include file="/ecombase/prodsubstmsg.inc.jsp" %>


            <% if (ui.isProdSubstitution() == true) {
                  followingSubItems = true;
               }
             %>

            <tr class="<%= ui.even_odd%>" id="row_<%=lineIndex%>">
              <%
              if (ui.showItemDetailButton()) { %>
                <td class="opener">
	              <%
	              if ((ui.isB2BSupported || orderitem.isConfigurable())) { %>
                  <a href='javascript: toggle("rowdetail_<%= lineIndex %>")' class="iconlink" onFocus="if(this.blur)this.blur()">
                    <img id="rowdetail_<%= lineIndex %>close" style="DISPLAY: none"
                         src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_close_green.gif") %>"
                         alt="<isa:translate key="b2b.order.details.item.close" />"
                         border="0" width="16" height="16">
                    <img id="rowdetail_<%= lineIndex %>open" style="DISPLAY: inline"
                         src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_open_green.gif") %>"
                         alt="<isa:translate key="b2b.order.details.item.open" />"
                         border="0" width="16" height="16">
                  </a>
	              <%
	              } %>
                 &nbsp;
                </td>
              <%
              } %>
              <td <% if (ui.itemHierarchy.isSubItem(orderitem)) { %> class="item-sub-<%= ui.getItemLevel() -1 %>" <% } else { %> class="item" <% } %>>
                <% if (ui.isElementVisible("order.item.posNo", itemKey)) { %>
                   <%= JspUtil.removeNull(orderitem.getNumberInt()) %>
                <% } %>

                <%-- Construct Hidden Fields For Communication With Closer Panel --%>
                <%
                if (orderitem.getText() != null) { %>
                  <input name="comment[<%=ui.line%>]" type="hidden" value="<%= JspUtil.encodeHtml(orderitem.getText().getText()) %>"/>
                <%
                } else { %>
                  <input name="comment[<%=ui.line%>]" type="hidden" value=""/>
                <%
                } %>

              </td>

              <%-- Product Id --%>
              <td class="product">
                 <% if (ui.isElementVisible("order.item.product", itemKey)) { %>
                    <%= JspUtil.encodeHtml(orderitem.getProduct()) %>
                 <% } %>
              </td>

              <%-- Quantity --%>
              <td class="qty">
                 <% if (ui.isElementVisible("order.item.qty", itemKey)) { %>
                    <%= orderitem.getQuantity() %>&nbsp;<abbr title="<isa:translate key="status.sales.detail.itemunit"/>"><%= orderitem.getUnit() %></abbr>
                 <% } %>
              </td>

              <%-- Description --%>
              <td class="desc">
                  <% if (ui.isElementVisible("order.item.description", itemKey)) { %>
                     <%= JspUtil.encodeHtml(orderitem.getDescription().trim()) %>
                  <% } %>
                  <% if (orderitem.isConfigurable() && ui.isElementVisible("order.item.configuration", itemKey)) { 
                       if (orderitem.getExternalItem() != null) { %>
                           <a href="#" class="icon" onclick="itemconfig('<%=orderitem.getTechKey()%>');">
                                        <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
                                        alt="<isa:translate key="status.sales.detail.config.item"/>
                                        <% if (ui.isAccessible) { %><isa:translate key="b2b.acc.config.item"/> <% } %>" />
                           </a>
                        <% } else { %>
  				                <a href="#" class="icon" onclick="alert('<isa:translate key="status.sales.detail.config.imp"/>');">
                                          <img class="img-1" src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_config.gif") %>" 
                                           alt="<isa:translate key="status.sales.detail.config.item"/>" />
                                </a>       
                        <% } %>
                   <% } %>
                  <% } %>
              </td>

              <%-- Contract Information --%>
              <% if (ui.isContractInfoAvailable()) { %>
                <td class="item"><% if ( (orderitem.getContractId() != null) && !(orderitem.getContractId().equals("")) && ui.isElementVisible("order.item.contract", itemKey)) { %>
                  <a href="<isa:webappsURL name="b2b/contractselect.do"/>?contractSelected=<%=orderitem.getContractKey()%>"><%=orderitem.getContractId()%></a>&nbsp;/&nbsp;<%=orderitem.getContractItemId()%>
                  <% } %>
                </td>
              <% } %>

              <%-- Price --%>
              <td class="price">
              <% if (!ui.itemHierarchy.hasSubItems(orderitem) && orderitem.isPriceRelevant() ) { %>
                <% if (ui.showIPCpricing == true  &&  ui.header.getIpcDocumentId() != null) { %>
                    <a href="#" onclick="displayIpcPricingConds(  '<%= JspUtil.removeNull(ui.header.getIpcConnectionKey()) %>',
                                                                  '<%= JspUtil.removeNull(ui.header.getIpcDocumentId().getIdAsString()) %>',
                                                                  '<%= JspUtil.removeNull(orderitem.getTechKey().getIdAsString()) %>'
                                                               )">
                <% } %>
                <% if (ui.isElementVisible("order.item.netValueWOFreight", itemKey)) { %>
                   <%= JspUtil.removeNull(orderitem.getNetValueWOFreight()) %>&nbsp;<%= orderitem.getCurrency() %>
                <% } %>
                <% if (ui.showIPCpricing == true) { %>
                    </a>
                <% } %>
                <% if ((orderitem.getNetPriceUnit()!=null) && (!orderitem.getNetPriceUnit().equals("")) && ui.isElementVisible("order.item.netPrice", itemKey)){%>
                    <br /><em>
                        <%= JspUtil.removeNull(orderitem.getNetPrice()) + "&nbsp;" + JspUtil.removeNull(orderitem.getCurrency()) + "&nbsp;" + '/' + "&nbsp;" + JspUtil.removeNull(orderitem.getNetQuantPriceUnit()) + ' ' + JspUtil.removeNull(orderitem.getNetPriceUnit()) %>
                        </em>
                <% } %>
                </td>
              <% } else { %>
                &nbsp; </td>
              <% } %>

              <%-- Requested delivery date --%>
              <td class="req-Date">
                 <% if (ui.isElementVisible("order.item.reqDeliveryDate", itemKey)) { %>   
                    <%= JspUtil.replaceSpecialCharacters(orderitem.getReqDeliveryDate()) %>
                 <% } %>
              </td>

              <%-- Reference items --%>
              <td class="ref-doc">
	              
	        <%--     if (ui.isElementVisible("order.item.connectedItem", itemKey)) {    --%>               
	            <%  if (ui.connectedItem == null) {%>
	                  -- / --
	                <%
	                }
	                else if (ui.hasDispOrderPermission) { %>
	                  <% if (ui.isCollectiveOrderActive()) { %>
	                      <a href="<isa:webappsURL name="b2b/createcollectiveorder.do"/>"><%=JspUtil.removeNull(ui.connectedItem.getDocNumber()) %> / <%=JspUtil.removeNull(ui.connectedItem.getPosNumber()) %> </a>
	                  <% } else { %>
	                      <a href="<isa:webappsURL name="b2b/documentstatusdetailprepare.do"/>?techkey=<%=ui.connectedItem.getDocumentKey()%>&objects_origin=&object_id=&objecttype=order"><%=JspUtil.removeNull(ui.connectedItem.getDocNumber()) %> / <%=JspUtil.removeNull(ui.connectedItem.getPosNumber()) %></a>
	                  <% } %>
	                <%
	                }
	                else { %>
	                  <%=JspUtil.removeNull(ui.connectedItem.getDocNumber()) %> / <%=JspUtil.removeNull(ui.connectedItem.getPosNumber()) %>
	                <%
	                } %> 
	          <%--   } --%>
              </td>

              <%
              if (!ui.header.isDocumentTypeOrderTemplate() &&
                        ! (ui.header.isDocumentTypeQuotation()  && ! ui.header.isQuotationExtended())) {
                if (ui.itemHierarchy.hasSubItems(orderitem) || !ui.isElementVisible("order.item.status", itemKey)) { %>

                  <td class="status">&nbsp;</td>
                   <% } else { %>
                   <%-- Status and price will only be displayed on Subitems for product replacement --%>
                  <td class="status"><isa:translate key="<%= documentItemStatusKey %>"/></td>
                <%
                }
              }


              Iterator extStatus = orderitem.getExtendedStatus().getStatusIterator();
              request.setAttribute("EXTENDEDSTATUS",extStatus);%>
              <td class="status">
                <% if (ui.isElementVisible("order.item.status", itemKey)) { %>
                   <isa:iterate id="eStatus" name="EXTENDEDSTATUS" type="com.sap.isa.businessobject.ExtendedStatusListEntry">
                     <%= eStatus.isCurrentStatus() ? eStatus.getDescription() : ""%>
                   </isa:iterate>
                <% } %>
              </td>
            </tr>

   		    <%
    		if (ui.isB2BSupported || orderitem.isConfigurable()) { %>

              <%--  Item Details in expandable area --%>
              <tr class="<%=ui.even_odd + "-detail"%>" id="rowdetail_<%= lineIndex %>" <%= ui.isB2BSupported && !ui.isToggleSupported?"":"style='display:none;'"%> >
                <td class="select">&nbsp;</td>
                <td class="detail" colspan="<%=""+(item_colspan-1)%>">
                   <table class="item-detail" summary="<isa:translate key="ecm.acc.item.detail.info"/>">
                     <%-- display of product configuration --%>
                     <% item = orderitem; %>
                     <% if (ui.isElementVisible("order.item.configuration", itemKey)) { %>
                        <%@ include file="/ecombase/orderitemconfiginfo.inc.jsp" %>
                     <% } %>
            		
            		 <%
            		 if (ui.isB2BSupported) { %>
                         <%-- Shipto --%>
		                 <% if ( !ui.isElementVisible("order.item.deliverTo", itemKey)) { %>
		                     <tr>
		                       <td class="identifier"><isa:translate key="status.sales.detail.deliveryaddr"/>:</td>
		                       <td class="value" colspan="5">
		                         <% if (  orderitem.getShipTo() != null  &&
		                                ! orderitem.getShipTo().getShortAddress().equals("")) { %>
		                             &nbsp;<%= JspUtil.encodeHtml(orderitem.getShipTo().getShortAddress()) %>
		                                   <a class="icon" href="#" onclick="showShipTo('<%=orderitem.getShipTo().getTechKey().getIdAsString()%>');" >
		                                     <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/icon_shipad_detail_green.gif") %>" width="16" height="16" alt="<isa:translate key="b2b.order.showshipto"/>" border="0" class="display-image"></a>&nbsp;
		                         <% } %>
		                       </td>
		                    </tr>
		                <% } %>
	                    <%-- Notes --%>
		                <% if (ui.isElementVisible("order.item.comment", itemKey)) { %>
		                    <tr class="message-data">
		                       <td class="identifier"><isa:translate key="hom.jsp.proc.customer.message"/></td>
		                       <td class="value">
		                           <% if (orderitem.getText() != null) { %>
		                           <textarea name="comment[<%= lineIndex %>]" rows="2" cols="<%=textarea_cols%>" disabled><%= JspUtil.replaceSpecialCharacters(orderitem.getText().getText()) %></textarea>
		                           <% } else {%>
		                           <textarea name="comment[<%= lineIndex %>]" rows="2" cols="<%=textarea_cols%>" disabled></textarea>
		                           <% } %>
		                       </td>
		                    </tr>
		                <% } %>
		            <%
		            } %>
                  </table>

        		 <%
        		 if (ui.isB2BSupported && ui.isElementVisible("order.item.delivery.pos", itemKey)) { %>

                  <%-- Deliver infos --%>
                  <table class="item-info" summary="Delivery information for the item.">
                    <% int dlvRowCnt = 1; %>
                    <isa:iterate id="orderitemdel" name="orderitem"
                                  type="com.sap.isa.businessobject.item.ItemDelivery">
                      <tr>
                        <%
                        if (dlvRowCnt == 1) { %>
                          <td class="icon" rowspan="<%= String.valueOf(orderitem.getNoOfDeliveries())%>"> </td>
                        <% } %>
                        <% if (ui.isElementVisible("order.item.delivery.pos", itemKey)) { %>
	                        <td class="identifier-1"><isa:translate key="status.sales.detail.deliveryNr"/>:</td>
	                        <td class="value-1"><%= orderitemdel.getObjectId() %>&nbsp;/&nbsp;<%= orderitemdel.getDeliveryPosition() %></td>
	                    <% } %>
	                    <% if (ui.isElementVisible("order.item.delivery.date", itemKey)) { %>
	                        <td class="identifier-2"><isa:translate key="status.sales.date"/>:</td>
	                        <td class="value-2"><%= orderitemdel.getDeliveryDate() %></td>
	                    <% } %>
	                    <% if (ui.isElementVisible("order.item.delivery.qty", itemKey)) { %>
	                        <td class="identifier-3"><isa:translate key="status.sales.detail.deliveryQty"/>:</td>
	                        <td class="value-3"><%= orderitemdel.getQuantity() %><abbr title="<isa:translate key="status.sales.detail.itemquantity"/>"><%= orderitemdel.getUnitOfMeasurement() %></abbr></td>
	                    <% } %>
	                    <% if ( ui.isElementVisible("order.item.delivery.tracking", itemKey) && ! orderitemdel.getTrackingURL().equals("")) { %>
	                          <td class="identifier-4">
	                            <a href="#" onclick="trackwin('<%= orderitemdel.getTrackingURL() %>'); return false;">
	                              <img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/truck.gif")%>" alt="<isa:translate key="status.sales.detail.del.tracking"/>" width="17" height="17" border="0"></a>
	                          </td>
	                    <% } %>
                      </tr>
                      <% dlvRowCnt++; %>
                    </isa:iterate>
                  </table>
  		          <%
			      } %>
                </td>
              </tr>
            <%
            } %>
            <%-- ********** Seperator between two items ********** --%>
            <tr>
             <td colspan="<%= subinfos_colspan %>" class="separator" ></td>
            </tr>
            <% lineIndex++; %>          <%-- RowLine counter --%>
         <%--  <% } %>   ui.isBOMSubItemToBeSuppressed() --%>
          </isa:iterate>
        </table> <%-- itemlist --%>

        <%
        if (ui.isAccessible()) { %>
           <div>
			<a id="b2b_hom_orderdetail_table1_end" href="#b2b_hom_orderdetail_table1_begin" title="<isa:translate key="access.table.end" arg0="<%=b2b_hom_orderdetail_table1_title%>"/>"></a>
		   </div>
        <%
        } %>

        <div>
        <%-- ADDITIONAL HIDDEN FIELDS FOR ORDER DOWNLOAD --%>
        <input type="hidden" name="salesdocnumber" value="<%=ui.header.getSalesDocNumber()%>"/>
        <input type="hidden" name="techkey" value="<%= ui.header.getTechKey().getIdAsString()%>"/>
        </div>

      </form>
    </div> <%-- Document Item Rows --%>

  </div> <%-- Document --%>

    <div id="buttons">

    <% if (ui.isOrderDownloadRequired()) { %>
      <form name="orderdownload" method="post" action='<isa:webappsURL name ="/b2b/orderDownload.do"/>'>
        <div>
        <input type="hidden" name="orderIDS" value="<%=ui.header.getSalesDocNumber()%>" />
        <input type="hidden" name="orderGUIDS" value="<%= ui.header.getTechKey().getIdAsString()%>"/>
        <%-- Field which distinguishes between the single order/mass download of orders --%>
        <input type="hidden" name="orderDetail" value="X"/>
        </div>
        <ul class="buttons-1">
          <li>
            <%
            if (ui.isAccessible()) { %>
              <strong><label for="format"><isa:translate key="odrdwnld.odrdetail.title"/></label></strong>
            <%
            } %>
            <% String ordDownLoadBtnTxt = WebUtil.translate(pageContext, "odrdwnld.odrdetail.btn", null); 
               String ordDownLoadPicTxt = WebUtil.translate(pageContext, "massdwnld.addorderimg.ttip", null); %>
            <select name="format" id="format">
                    <option  value="<%= ActionConstantsBase.ORDDOWN_PDF_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.pdf"/></option>
                    <option  value="<%= ActionConstantsBase.ORDDOWN_CSV_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.csv"/></option>
                    <option  value="<%= ActionConstantsBase.ORDDOWN_XML_FORMAT_STR %>">  <isa:translate key="odrdwnld.option.xml"/></option>
            </select>
          </li>
          <li>
            <a href="#" onclick="prepareDownload()" name="orderdownloadbtn" id="orderdownloadbtn" title="<isa:translate key="access.button" arg0="<%=ordDownLoadBtnTxt%>" arg1=""/>" >
                    <isa:translate key="odrdwnld.odrdetail.btn"/>
            </a>
          </li>
          <li>
            <%
            String url ="b2b/order/addOrderToMassDownloadList.do";
            url+="?orderguid="+ui.header.getTechKey().getIdAsString()+"&amp;operation=add&amp;orderid="+ui.header.getSalesDocNumber()
            +"&amp;desc="+(ui.header.getDescription()==null?"":ui.header.getDescription())+"&amp;status="+ui.header.getStatus()+"&amp;orderdate="+ui.getDocDate();
            %>
            <a href="<isa:webappsURL name ="<%=url%>"/>" target="form_input" title="<isa:translate key="access.button" arg0="<%=ordDownLoadPicTxt%>" arg1=""/>" >
              <img src="<isa:webappsURL name ="/b2b/mimes/images/download.gif"/>" alt="" title="<isa:translate key="massdwnld.addorderimg.ttip"/>" border="0"/>
            </a>
          </li>
        </ul>
      </form>
    <% } %>

      <form name="documentprocess" method="get" action='<isa:webappsURL name ="/b2b/processcustomerorder.do"/>'
            onsubmit="return parent.parent.documents.editableDocumentAlert('<isa:UCtranslate key="b2b.onedocnav.onlyonedoc"/>\n\n<isa:UCtranslate key="b2b.onedocnav.onlyonedoc2"/>\n\n<isa:UCtranslate key="b2b.onedocnav.onlyonedoc3"/>')" >
      </form>
      <form name="documentchange" method="get" action='<isa:webappsURL name ="/b2b/changeorder.do"/>'
            onsubmit="return parent.parent.documents.editableDocumentAlert('<isa:UCtranslate key="b2b.onedocnav.onlyonedoc"/>\n\n<isa:UCtranslate key="b2b.onedocnav.onlyonedoc2"/>\n\n<isa:UCtranslate key="b2b.onedocnav.onlyonedoc3"/>')" >
      </form>
      <%
      String processBtnTxt = WebUtil.translate(pageContext, "hom.detail.btn.process", null);
      String changeBtnTxt = WebUtil.translate(pageContext, "hom.detail.btn.change", null);
      String printBtnTxt = WebUtil.translate(pageContext, "status.sales.detail.button.print", null);
      String closeBtnTxt = WebUtil.translate(pageContext, "status.sales.detail.button.close", null);
      %>

        <%-- Accesskey for the button section. It must be an resourcekey, so that it can be translated. --%>
        <a href="#" title= "Main functions of the document." accesskey="b">
                <img src="<%=WebUtil.getMimeURL(pageContext, "mimes/images/1X1.gif")%>" alt="" style="display:none" width="1" height="1" />
        </a>

        <ul class="buttons-2">
          <%
          if (ui.isProcessButtonAvailable()) { %>
            <li><a href="Javascript:processDocument();" title="<isa:translate key="access.button" arg0="<%=processBtnTxt%>" arg1=""/>" >
              <%= processBtnTxt %></a></li>
          <%
          }
          if (ui.isChangeDataButtonAvailable()) { %>
            <li><a href="Javascript:changeDocument();" title="<isa:translate key="access.button" arg0="<%=changeBtnTxt%>" arg1=""/>" >
            <%=changeBtnTxt %></a></li>
          <%
          }%>
        </ul>

        <ul class="buttons-3">
            <li><a href="Javascript:print();" title="<isa:translate key="access.button" arg0="<%=printBtnTxt%>" arg1=""/>" >
              <%= printBtnTxt %></a></li>
            <li><a href="Javascript:closeDocument();" title="<isa:translate key="access.button" arg0="<%=closeBtnTxt%>" arg1=""/>" >
              <%= closeBtnTxt %></a></li>
        </ul>
   </div>



  </body>
</html>
