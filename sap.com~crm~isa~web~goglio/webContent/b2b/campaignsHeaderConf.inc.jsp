<%--
********************************************************************************
    File:         campaignsHeaderConf.inc.jsp
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.03.2004
    Version:      1.0

    $Revision: #1 $
    $Date: 2004/03/18 $
********************************************************************************
--%>
       <% if (ui.isHeaderCampaignListSet()) { %>
            <tr class="odd">
                <td class="identifier"><isa:translate key="b2b.order.disp.camp"/></td>
                <td class="orderHeadPrice" style="border-right: none;"><%= JspUtil.encodeHtml(ui.getHeaderCampaignId(0)) %><br/><%= JspUtil.encodeHtml(ui.getHeaderCampaignDescs(0)) %></td>
            </tr>
       <% } %>
