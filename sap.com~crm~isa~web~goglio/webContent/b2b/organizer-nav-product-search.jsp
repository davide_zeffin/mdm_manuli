<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%  BaseUI baseUi = new BaseUI(pageContext); %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=baseUi.getLanguage()%>">
	<head>
		<title>SAP Internet Sales - Document Search</title>
		<isa:stylesheets/>
	</head>
	<body class="document-search" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">
		<div class="module-name"><isa:moduleName name="b2b/organizer-nav-product-search.jsp" /></div>
		<% String activeView ="productsearch"; %>
		<%@ include file="organizer_menu.jsp.inc" %>
		<br/>
	</body>
</html>