<%--
********************************************************************************
    File:         determineprocesstypes.inc
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      25.11.2003
    Version:      1.0

    $Revision: #1 $
    $Date: 2003/11/25 $
********************************************************************************
--%>
<%  // Get the information for which type of sales document we were called
    String docType = (String) userSessionData.getAttribute(MaintainBasketBaseAction.SC_DOCTYPE);
 
    // get the process type description
    ResultData processTypes = null;
    String processTypes;
    String processTypeDescription = "";
    if (MaintainBasketBaseAction.DOCTYPE_BASKET.equals(docType)) {
        processTypes = (ResultData)userSessionData.getAttribute(ShopReadAction.PROCESS_TYPES);

    } else {
        if (MaintainBasketBaseAction.DOCTYPE_QUOTATION.equals(docType)) {
           processTypes = (ResultData)userSessionData.getAttribute(ShopReadAction.QUOTATION_PROCESS_TYPES);        
        }
    }
    // get process type of document
    HeaderSalesDocument header = null;
    DocumentHandler documentHandler = (DocumentHandler)
                userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
    ManagedDocument mdoc = documentHandler.getManagedDocumentOnTop();
    DocumentState document = mdoc.getDocument(); 
    if (document instanceof Basket) {
        header =
            (HeaderSalesDocument) request.getAttribute(MaintainBasketBaseAction.RK_HEADER);
        processType = header.getProcessType();        
    } else if (document instanceof Order) {
        header = 
    else if (document instanceof OrderStatus) {  
        header = orderStatus.getOrderHeader();  
    }  
                 
    if (processTypes != null) {
        if (processTypes.first()) {
    	    do  {
    	       if (processTypes.getString(1).equalsIgnoreCase(processType)) {
    	          processTypeDescription = processTypes.getString(2); 
    	          break;
    	       }
    	    } while(processTypes.next());
    	}
    }   
%>




