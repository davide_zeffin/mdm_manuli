<%-- import the taglibs used on this page --%>
<%@ taglib uri="/isa" prefix="isa" %>
<%@ taglib uri="/isacore" prefix="isacore" %>

<%@ page import="com.sap.isa.core.util.WebUtil" %>
<%@ page import="com.sap.isa.isacore.action.b2b.*" %>
<%@ page import="com.sap.isa.isacore.action.order.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sap.isa.core.util.*" %>
<%@ page import="com.sap.isa.isacore.action.*" %>
<%@ page import="com.sap.isa.backend.boi.isacore.*" %>
<%@ page import="com.sap.isa.core.util.table.ResultData" %>
<%@ page import="com.sap.isa.ui.uiclass.BaseUI" %>

<%@ include file="/b2b/usersessiondata.inc" %>

<isa:contentType />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%  BaseUI ui = new BaseUI(pageContext);

	boolean quotationExtended = false; 
    boolean homActivated = false;
%>

<isacore:ifShopProperty property = "QuotationExtended" value ="true">
  <% quotationExtended = true; %>
</isacore:ifShopProperty>
<%  boolean backendR3 = true; %>
<isacore:ifShopProperty property = "backend" value = "<%=ShopData.BACKEND_CRM%>">
  <% backendR3 = false; %>
</isacore:ifShopProperty>

<isacore:ifShopProperty property = "homActivated" value ="true">
  <% homActivated = true; %>
</isacore:ifShopProperty>

<%
	// Check if oci transfer is required
	boolean ociTransfer =
		(((IsaCoreInitAction.StartupParameter)userSessionData.
			getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER)).
			getHookUrl().length() > 0);


	// Get minibasket information from requestcontext
	String netValue = (String) request.getAttribute(WorkareaNavAction.RK_MB_NETVALUE);
	String itemSize = (String) request.getAttribute(WorkareaNavAction.RK_MB_ITEMSIZE);
	String currency = (String) request.getAttribute(WorkareaNavAction.RK_MB_CURRENCY);
	// the doctype of the editable document displayed in minibasket
	String mb_doctype = (String) request.getAttribute(WorkareaNavAction.RK_MB_DOCTYPE);

%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<%=ui.getLanguage()%>">
  <head>
    <title>SAP Internet Sales B2B - New Order</title>
	<isa:stylesheets/>

	<script type="text/javascript">
	<%@ include file="/b2b/jscript/urlrewrite.inc"  %>
	</script>	
		  
	<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/docnav.js" ) %>"
		type="text/javascript">
	</script>
	  
	<script src="<%=WebUtil.getMimeURL(pageContext, "b2b/jscript/frames.js" ) %>"
		type="text/javascript">
	</script>

	<script type="text/javascript">
	<!--
		<%-- describes, if we have an editable document or not --%>
	    <%-- there are two documents, for this we must have an editable doc --%>
		var editableDoc = true;
        <%-- Editable Doc is always DOCNUMBER1 ! --%>
        var editableDocNo = "<%=(String)request.getAttribute(WorkareaNavAction.DOCNUMBER1)%>";		
	//-->
	</script>

    <%-- scripts for minibasket --%>
    <%@ include file="/b2b/jscript/docnav.inc.jsp"  %>
    <%-- end scripts for minibasket --%>

  </head>

  <body class="docnav" onload="loadPage();" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">

      <div id="new-doc" class="module">

         <div class="module-name"><isa:moduleName name="b2b/twodocnav.jsp" /></div>
      </div>

    <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td colspan="2">
                  <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="historyHeader3" colspan="2"><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/spacer.gif") %>" width="1" height="40" alt="" border="0"></td>
                    </tr>
                    <tr width="100%">
                      <td><img src="<%=WebUtil.getMimeURL(pageContext, "b2b/mimes/images/headline_header_small.gif") %>" width="100%" height="2" alt="" border="0"></td>
                    </tr>
                  </table>
                </td>
            </tr>
        </tbody>
    </table>
  </body>
  
</html>
