<%--
********************************************************************************
    File:         usersessiondata.inc
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits
    Created:      28.5.2001
    Version:      1.0

    $Revision: #32 $
    $Date: 2001/05/23 $
********************************************************************************
--%>

<%@ page import="com.sap.isa.core.*" %>

<%
    UserSessionData userSessionData =
            UserSessionData.getUserSessionData(pageContext.getSession());
%>
