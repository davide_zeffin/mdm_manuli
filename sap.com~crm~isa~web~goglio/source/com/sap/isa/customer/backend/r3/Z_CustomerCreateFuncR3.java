/*****************************************************************************
  Copyright (c) 2006, SAP AG, All rights reserved.

  Example bases on ISA 5.0
*****************************************************************************/
package com.sap.isa.customer.backend.r3;


// jco imports
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.customer.backend.boi.Z_CustomerCreateFuncBackend;
import com.sap.mw.jco.JCO;



/**
 * Template for backend object in customer projects
 * Concrete implementation of a backend object
 * This implemenation demonstrates how a backend object
 * is used to communicate with the R3 system
 */
public class Z_CustomerCreateFuncR3
        extends ISAR3BackendObject
        implements Z_CustomerCreateFuncBackend {

	private static final String functionName = "ZGO_CUSTOMERS_CREATE";
  	// initialize logging
  	private static IsaLocation log = IsaLocation.
        getInstance(Z_CustomerCreateFuncR3.class.getName());

		public JCO.Structure getImportStructure(){
			try{	
				// get Java representation of function module 	
				JCO.Function func = getDefaultJCoConnection().getJCoFunction(functionName);
				JCO.Structure customer = func.getImportParameterList().getStructure("CUSTOMER_CREATE");
				
				return customer;
				
			} catch (BackendException bex) {
				// The following key has to be added to WEB-INF/classes/ISAResources.properties 
				// in order to see the exception correctly e.g.
				// mycomp.error.getshopdesc=an error has occured while getting shop description 
				log.debug("mycomp.error.getshopdesc", bex);
			}
			return null;
		}
		
		/**
		 * Communicates with an SAP R3 system in order to 
		 * create a new Customer.
		 */
		public String createCustomer(JCO.Structure importCustomer, String notes) {

			try {
				// get reference Customer
				R3BackendData backendData = getOrCreateBackendData();
				String refcustomer = backendData.getRefCustomer();
			
				// get Java representation of function module 	
				JCO.Function func = getDefaultJCoConnection().getJCoFunction(functionName);
							 
				JCO.Structure customer = func.getImportParameterList().getStructure("CUSTOMER_CREATE");
				
				customer.copyFrom(importCustomer);
				
				//	provide import parameters:
				//	customer data,
				func.getImportParameterList().setValue(customer, "CUSTOMER_CREATE");

				// reference customer.
				func.getImportParameterList().setValue(refcustomer, "REFCUSTOMER");
				
				// notes
				func.getImportParameterList().setValue(notes, "NOTES");
				
				// execute function
				getDefaultJCoConnection().execute(func);
				
				// get result table
				JCO.Structure result = func.getExportParameterList().getStructure("CUSTOMER_RESULT");
				return result.getField("KUNNR").getValue().toString();
				
			} catch (BackendException bex) {
				// The following key has to be added to WEB-INF/classes/ISAResources.properties 
				// in order to see the exception correctly e.g.
				// mycomp.error.getshopdesc=an error has occured while getting shop description 
				log.debug("mycomp.error.getshopdesc", bex);
			}
			return null;
			 
		}


}