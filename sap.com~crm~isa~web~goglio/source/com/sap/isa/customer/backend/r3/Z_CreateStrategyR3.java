/*
 * Created on 23-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.backend.r3;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategy;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * @author SAP
 *
 */
public class Z_CreateStrategyR3 extends CreateStrategyR3PI{
	
	private static IsaLocation log = IsaLocation.getInstance(
	Z_CreateStrategyR3.class.getName());

	/**
	 * Creates a new CreateStrategyR3PI object.
	 * 
	 * @param backendData the R/3 specific customizing
	 * @param writeStrategy group of algorithms for writing
	 * @param readStrategy group of algorithms for reading
	 * @throws BackendException exception from R/3
	 */
	public Z_CreateStrategyR3(R3BackendData backendData, 
							  WriteStrategy writeStrategy, 
							  ReadStrategy readStrategy)
					   throws BackendException {
		super(backendData, writeStrategy, readStrategy);
	}
	
	
	/**
	  * This method can be used to set additional RFC tables or table fields before the
	  * create RFC SD_SALESDOCUMENT_CREATE is called. Set delivery block ID into RFC
	  * header table.
	  * 
	  * 
	  * @param document the ISA sales document
	  * @param sdSalesDocCreate RFC that is called for creating the document
	  */
	 protected void performCustExitBeforeR3Call(SalesDocumentData document, 
												JCO.Function sdSalesDocCreate) {

      
		String extCurrency = (String) document.getHeaderData().getExtensionData("ZEXT_WAERK");
		String extPmntTrms = document.getHeaderData().getPaymentTerms();//.getExtensionData("ZEXT_ZTERM");
		String extIncoterms1 = document.getHeaderData().getIncoTerms1();//.getExtensionData("ZEXT_INCO1");
		String extIncoterms2 = document.getHeaderData().getIncoTerms2();//.getExtensionData("ZEXT_INCO2");
	
																	
		if (log.isDebugEnabled())
			log.debug("performCustExitBeforeR3Call, extCurrency is: " + extCurrency);
		//now put it into RFC header structure
		sdSalesDocCreate.getImportParameterList().getStructure("SALES_HEADER_IN").setValue(extCurrency,"H_CURR");		
		sdSalesDocCreate.getImportParameterList().getStructure("SALES_HEADER_IN").setValue(extCurrency,"CURRENCY");		
		sdSalesDocCreate.getImportParameterList().getStructure("SALES_HEADER_IN").setValue(extIncoterms1,"INCOTERMS1");		
		sdSalesDocCreate.getImportParameterList().getStructure("SALES_HEADER_IN").setValue(extIncoterms2,"INCOTERMS2");		
		sdSalesDocCreate.getImportParameterList().getStructure("SALES_HEADER_IN").setValue(extPmntTrms,"PMNTTRMS");		

	 }	

}
