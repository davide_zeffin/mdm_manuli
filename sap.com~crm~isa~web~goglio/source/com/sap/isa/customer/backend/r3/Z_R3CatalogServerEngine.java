/*
 * Created on 21-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.backend.r3;

import java.util.Iterator;

import com.sap.isa.backend.r3.catalog.R3CatalogServerEngine;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.customer.action.Z_SaveUser;
import com.sap.mw.jco.JCO;
/**
 * @author greenorange
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Z_R3CatalogServerEngine extends R3CatalogServerEngine {
	
	private boolean addAttribute(JCO.Table attributes, Catalog catalog, CatalogItem theItem, String attr){
		String area = theItem.getGuid().substring(0, 10);
		String item = theItem.getGuid().substring(10, 20);
		
		if (attributes.getField("AREA").getString().equals(area)
			&& attributes.getField("ITEM").getString().equals(item)
			&& attributes.getField("ATTR_NAME").getString().equals(attr)) {

			catalog.createAttributeInternal().setNameInternal(attr);
			CatalogAttributeValue theAttrValue2 =
				theItem.createAttributeValueInternal(attr);
			theAttrValue2.setAsStringInternal(
				attributes.getField("ATTR_VALUE").getString());
				
			return true;
		}
		return false;
	}

	protected void performCustomerExitAfterCatalogRead(
		JCoConnection connection,
		Catalog catalog) {
			
		String user = Z_SaveUser.getUser();
		Z_SaveUser.setUser(null);
		
		JCO.Function funcGetDocs = null;
		JCO.ParameterList importParaGetDoc = null;
		JCO.Structure retStructure;
		JCO.Table attributes, prices;

		String area, item;

		// get catalog name and variant out of its GUID
		String catalogGuid = catalog.getGuid();
		int index = catalogGuid.lastIndexOf("_");
		String catalogName = catalogGuid.substring(0, index);
		String catalogVariant =
			catalogGuid.substring(index + 1, catalogGuid.length());

			 
 		//now call the function module in R/3
		try {
			funcGetDocs =
				connection.getJCoFunction("ZGO_ISA_READ_CATALOG_COMPLETE");
			importParaGetDoc = funcGetDocs.getImportParameterList();
			importParaGetDoc.setValue(user.toUpperCase() , "USER"); catalog.getDescription();
			importParaGetDoc.setValue(catalogName, "CATALOG");
			importParaGetDoc.setValue(catalogVariant, "VARIANT");
			importParaGetDoc.setValue("X", "WITH_ATTRIBUTES");
			importParaGetDoc.setValue("X", "WITH_PRICES");
			//log.debug("theItem.getGuid(): " + theItem.getGuid());
			//log.debug("theCategory.getGuid(): " + theCategory.getGuid());

			//fire the RFC
			connection.execute(funcGetDocs);

		} catch (BackendException be) {
			//log.error("Error when getting JCO.Function BAPI_ADV_MED_LAYOBJ_DOCS");
		}

		//		error-handling
		retStructure =
			funcGetDocs.getExportParameterList().getStructure("RETURN");

		if (!(retStructure.getString("TYPE").equals("")
			|| retStructure.getString("TYPE").equals("S"))) {
			//log.error("An error occured when retrieving the documents from R/3: " + retStructure.getString("MESSAGE"));
		}

		// get the attribute table
		attributes = funcGetDocs.getTableParameterList().getTable("ATTRIBUTES");
		// get the price table
		prices = funcGetDocs.getTableParameterList().getTable("PRICES");

		Iterator categoryIter = null;

		try {
			categoryIter = catalog.getCategories();
		} catch (CatalogException ce) {
			return;
		}

		CatalogCategory theCategory = null;
		
		// price attribute for the item
		CatalogAttributeValue catalogPriceAttribute;

		//loop through the categories
		if (categoryIter != null) {
			while (categoryIter.hasNext()) {
				theCategory = (CatalogCategory) categoryIter.next();
				Iterator itemIter = null;
				try {
					itemIter = theCategory.getItems();
				} catch (CatalogException ce) {
					return;
				}
				CatalogItem theItem = null;
				//for each category, loop through its items
				if (itemIter != null) {
					while (itemIter.hasNext()) {
						theItem = (CatalogItem) itemIter.next();
						
						// get price attribute
						catalogPriceAttribute = 
							(CatalogAttributeValue)theItem.
								getAttributeValue("ATTRNAME_PRICE");
						
						area = theItem.getGuid().substring(0, 10);
						item = theItem.getGuid().substring(10, 20);
							
						// updating price
						if (prices.getNumRows() > 0) {
							prices.firstRow();
							do {
								if (prices.getField("AREA").getString().equals(area)
									&& prices.getField("ITEM").getString().equals(item))
								{
									catalogPriceAttribute.setAsStringInternal(
										prices.getField("CONDVAL").getString());			
								}
							} while (prices.nextRow());
						}
						
						// adding item's attributes
						if (attributes.getNumRows() > 0) {
							attributes.firstRow();
							do {
								if (addAttribute(attributes, catalog, theItem, "Z_DESCRIZIONE_ISAD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_DESCRIZIONE_ISAV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_COD_VALVOLAD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_COD_VALVOLAV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_DESCR_COLORED"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_DESCR_COLOREV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_FAMMATER_RESD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_FAMMATER_RESV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_GR_CADAUNO_TOTD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_GR_CADAUNO_TOTV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_IMBALLO_ESTERNO_NUM_PFD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_IMBALLO_ESTERNO_NUM_PFV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_IMBALLO_INTERNO_NUM_PFD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_IMBALLO_INTERNO_NUM_PFV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_RIENT_PFD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_RIENT_PFV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_PZ_CONTAINER_20D"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_PZ_CONTAINER_20V"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_PZ_CONTAINER_40D"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_PZ_CONTAINER_40V"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_LARGHEZZA_PF_INTD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_LARGHEZZA_PF_INTV"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_LUNGHEZZA_PF_INTD"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "Z_LUNGHEZZA_PF_INTV"))
									continue;
									
								// Price/PC
								if (addAttribute(attributes, catalog, theItem, "PRICE_D"))
									continue;
								if (addAttribute(attributes, catalog, theItem, "PRICE_V"))
									continue;
								
								/*if (attributes.getField("AREA").getString().equals(area)
									&& attributes.getField("ITEM").getString().equals(item)
									&& attributes.getField("ATTR_NAME").getString().equals("Z_DESCRIZIONE_ISAD")) {

									catalog.createAttributeInternal().setNameInternal("Z_DESCRIZIONE_ISAD");
									CatalogAttributeValue theAttrValue2 =
										theItem.createAttributeValueInternal(
											"Z_DESCRIZIONE_ISAD");
									theAttrValue2.setAsStringInternal(
										attributes.getField("ATTR_VALUE").getString());
									continue;
								}
								if (attributes.getField("AREA").getString().equals(area)
									&& attributes.getField("ITEM").getString().equals(item)
									&& attributes.getField("ATTR_NAME").getString().equals("Z_DESCRIZIONE_ISAV")) {

									catalog.createAttributeInternal().setNameInternal("Z_DESCRIZIONE_ISAV");
									CatalogAttributeValue theAttrValue2 =
										theItem.createAttributeValueInternal(
											"Z_DESCRIZIONE_ISAV");
									theAttrValue2.setAsStringInternal(
										attributes.getField("ATTR_VALUE").getString());
									continue;
								}
								
								if (attributes.getField("AREA").getString().equals(area)
									&& attributes.getField("ITEM").getString().equals(item)
									&& attributes.getField("ATTR_NAME").getString().equals("Z_COD_VALVOLAD")) {
			
									catalog.createAttributeInternal().setNameInternal("Z_COD_VALVOLAD");
									CatalogAttributeValue theAttrValue2 =
										theItem.createAttributeValueInternal(
											"Z_COD_VALVOLAD");
									theAttrValue2.setAsStringInternal(
										attributes.getField("ATTR_VALUE").getString());
									continue;
								}
								if (attributes.getField("AREA").getString().equals(area)
									&& attributes.getField("ITEM").getString().equals(item)
									&& attributes.getField("ATTR_NAME").getString().equals("Z_COD_VALVOLAV")) {
			
									catalog.createAttributeInternal().setNameInternal("Z_COD_VALVOLAV");
									CatalogAttributeValue theAttrValue2 =
										theItem.createAttributeValueInternal(
											"Z_COD_VALVOLAV");
									theAttrValue2.setAsStringInternal(
										attributes.getField("ATTR_VALUE").getString());
									continue;
								}*/

								/*if (attributes.getField("AREA").getString().equals(area) && 
									attributes.getField("ITEM").getString().equals(item) && 
									attributes.getField("ATTR_NAME").getString().equals("CH0032D")) {
								
									catalog.createAttributeInternal().setNameInternal("CH0032D");
									CatalogAttributeValue theAttrValue2 =
										theItem.createAttributeValueInternal("CH0032D");
									theAttrValue2.setAsStringInternal(
										attributes.getField("ATTR_VALUE").getString());
								}
								if (attributes.getField("AREA").getString().equals(area) && 
									attributes.getField("ITEM").getString().equals(item) && 
									attributes.getField("ATTR_NAME").getString().equals("CH0032V")) {
								
									catalog.createAttributeInternal().setNameInternal("CH0032V");
									CatalogAttributeValue theAttrValue2 =
										theItem.createAttributeValueInternal("CH0032V");
									theAttrValue2.setAsStringInternal(
										attributes.getField("ATTR_VALUE").getString());
									break;
								}*/
							} while (attributes.nextRow());
						}
					}
				}
			}
		}
	}

}
