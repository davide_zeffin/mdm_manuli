/*
 * Created on 29-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.backend.r3;

import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.customer.backend.boi.Z_CustomerGetDetailFuncBackend;
import com.sap.mw.jco.JCO;

/**
 * @author greenorange
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Z_CustomerGetDetailFuncR3
	extends BackendBusinessObjectBaseSAP
	implements Z_CustomerGetDetailFuncBackend {

		private static final String functionName = "ZGO_ISA_USERGETDEFAULTS";
			// initialize logging
			private static IsaLocation log = IsaLocation.
				getInstance(Z_CustomerGetDetailFuncR3.class.getName());
		
		public JCO.Table getCustomerDefaults(String user, Shop shop){
			try {
				// get Java representation of function module 	
				JCO.Function func = getDefaultJCoConnection().getJCoFunction(functionName);
				
				func.getImportParameterList().setValue(user.toUpperCase(), "USER");
				func.getImportParameterList().setValue(shop.getSalesOrganisation(), "SALESORG");
				func.getImportParameterList().setValue(shop.getDistributionChannel(), "DISTRCHANN");
				func.getImportParameterList().setValue(shop.getDivision(), "DIVISION");
				// execute function
				getDefaultJCoConnection().execute(func);
				
				// get result table
				JCO.Table result = func.getTableParameterList().getTable("DEFAULTS");
				return result;
				
			} catch (BackendException bex) {
				// The following key has to be added to WEB-INF/classes/ISAResources.properties 
				// in order to see the exception correctly e.g.
				// mycomp.error.getshopdesc=an error has occured while getting shop description 
				log.debug("mycomp.error.getshopdesc", bex);
			}
			return null;
		}
}
