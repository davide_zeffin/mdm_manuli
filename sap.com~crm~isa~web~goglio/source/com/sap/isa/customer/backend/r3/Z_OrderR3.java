/*
 * Created on 23-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.backend.r3;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.util.Properties;

import com.sap.isa.backend.r3.salesdocument.OrderR3;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;


/**
 * @author SAP
 *
 */
public class Z_OrderR3 extends OrderR3 {
	private static IsaLocation log = IsaLocation.getInstance(Z_OrderR3.class.getName());

	/**
	 * Initializes Business Object. This method might be overriden in  customer projects
	 * to register customer specific functionality, it tells the order objects which
	 * algorithms to use. Registers the strategies for reading,  writing, creating and
	 * displaying an order. The specific class of the  strategies depends on the R/3
	 * release and the plug in availability.
	 *
	 * @param props                 a set of properties which may be useful to initialize
	 *        the object
	 * @param params                an object which wraps parameters
	 * @exception BackendException  exception from backend
	 */
	public void initBackendObject(Properties props,
		BackendBusinessObjectParams params) throws BackendException {
		if (log.isDebugEnabled()) {
			log.debug("init begin z_orderr3");
		}

		super.initBackendObject(props, params);

		R3BackendData backendData = getOrCreateBackendData();

		createStrategy = new Z_CreateStrategyR3(backendData, writeStrategy,	readStrategy);
		//detailStrategy = new Z_DetailStrategyR3(backendData, readStrategy);
	}

	/**
	 * Simulate order in backend (including check of credit card data). Calls methods for
	 * simulating the document in R/3 from the create strategy that was registered in
	 * <code> initBackendObject </code> method.
	 *
	 * <br> Set the delivery block long text.
	 *
	 * @param ordr                  the order data
	 * @exception BackendException  exception from R/3
	 */
//	public void simulateInBackend(OrderData ordr) throws BackendException {
//		super.simulateInBackend(ordr);
//
//		setDeliveryBlockText(ordr, getContext());
//
//		//now update the internal storage
//		setDirtyForInternalStorage(true);
//		checkInternalStorage(ordr);
//	}
//
//	/**
//	 * Saves the order in backend (after authorizatiion check for credit card). Calls
//	 * methods for creating the document in R/3 from the create strategy that was
//	 * registered in <code> initBackendObject </code> method.
//	 * <br> Set the delivery block long text.
//	 *
//	 * @param ordr                  the order data
//	 * @exception BackendException  exception from R/3
//	 */
//	public void saveInBackend(OrderData ordr) throws BackendException {
//		super.saveInBackend(ordr);
//		setDeliveryBlockText(ordr, getContext());
//	}
//
//	/**
//	 * Set the language dependent text for the delivery block.
//	 * @param ordr the current order
//	 */
//	public static void setDeliveryBlockText(SalesDocumentData ordr,
//		BackendContext context) {
//		HeaderData header = ordr.getHeaderData();
//		
//		String extCurrencyId = (String) header.getExtensionData("ZEXT_WAERK");
//
//		//String deliveryBlockId = (String) header.getExtensionData(DeliveryBlock.EXT_KEY_DELIVBLOCK_HEADER);
//
//		if (log.isDebugEnabled()) {
//			log.debug("deliveryBlockId is: " + extCurrencyId);
//		}
//
////		String deliveryBlockText = DeliveryBlock.getText((ExtendedResultSet) context.getAttribute(
////					DeliveryBlock.BACKENDCONTEXT_DELIVBLOCK_LIST),
////				deliveryBlockId);
//
//		header.addExtensionData("USD","");
//	}
//
//	/**
//	 * For new documents, this methods updates the internal storage. If payment data is
//	 * available (B2C), a simulate-create call is done since there is no way to update
//	 * the ISA representation without having the document in R/3.
//	 *
//	 * <br> For existing documents, method
//	 * updates the document in the internal storage. In contrast to Internet Sales CRM,
//	 * the representation of the document in R/3 is not updated here, but a simulate
//	 * call is done to get necessary R/3 information. The document will be written to
//	 * R/3 if a save is done.
//	 * <br> Set delivery block text.
//	 *
//	 * @param posd                  sales document data
//	 * @param usr                   user
//	 * @param shop                  the shop (holding sales area data)
//	 * @exception BackendException  Exception from backend
//	 */
//	public void updateInBackend(SalesDocumentData posd, UserData usr,
//		ShopData shop) throws BackendException {
//		super.updateInBackend(posd, shop);
//
//		//fetch the delivery id from the R/3 version of the document
//		if (isExistsInR3()) {
//			posd.getHeaderData().addExtensionData(DeliveryBlock.EXT_KEY_DELIVBLOCK_HEADER,
//				getDocumentR3Status().getHeader().getExtensionData(DeliveryBlock.EXT_KEY_DELIVBLOCK_HEADER));
//
//			if (log.isDebugEnabled()) {
//				log.debug(
//					"fetched delivery block from R/3 version of document: " +
//					posd.getHeaderData().getExtensionData(DeliveryBlock.EXT_KEY_DELIVBLOCK_HEADER));
//			}
//
//			setDeliveryBlockText(posd, getContext());
//
//			//now update the internal storage
//			setDirtyForInternalStorage(true);
//			checkInternalStorage(posd);
//		}
//	}
//
//	/**
//	 * Read the whole document from R/3. <br> Set delivery block text.
//	 *
//	 * @param posd                  the sales document that gets the data including
//	 *        header, items, deliveries, schedule lines.
//	 * @exception BackendException exception from R/3
//	 */
//	protected void getDetailFromBackend(SalesDocumentData posd)
//		throws BackendException {
//		super.getDetailFromBackend(posd);
//
//		setDeliveryBlockText(posd, getContext());
//
//		//now update the internal storage
//		setDirtyForInternalStorage(true);
//		checkInternalStorage(posd);
//	}
}