/*****************************************************************************
  Copyright (c) 2006, SAP AG, All rights reserved.

  Example bases on ISA 5.0
*****************************************************************************/
package com.sap.isa.customer.backend.boi;

// jco imports
import com.sap.mw.jco.JCO.Structure;

/**
 * Interface used to communicate with a backend object
 * The purpose of this interface is to hide backend implementation details
 * from the business objects
 */
public interface Z_CustomerCreateFuncBackend {

  public Structure getImportStructure();
  public String createCustomer(Structure s, String n);

}