/*
 * Created on 29-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.backend.boi;

import com.sap.isa.businessobject.Shop;
import com.sap.mw.jco.JCO;

/**
 * @author greenorange
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface Z_CustomerGetDetailFuncBackend {
	public JCO.Table getCustomerDefaults(String user, Shop shop);
}
