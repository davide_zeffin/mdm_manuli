/*****************************************************************************
  Copyright (c) 2006, SAP AG, All rights reserved.

  Example bases on ISA 5.0
*****************************************************************************/
package com.sap.isa.customer.businessobject;

// Internet Sales imports
import com.sap.isa.core.businessobject.BOBase;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.customer.backend.boi.Z_CustomerCreateFuncBackend;
import com.sap.mw.jco.JCO.Structure;

/**
 * Template for business object in customer projects
 */
public class Z_CustomerCreateFunc extends BOBase implements BackendAware {

	// initialize logging
	private static IsaLocation log =
		IsaLocation.getInstance(Z_CustomerCreateFunc.class.getName());

	private BackendObjectManager bem;
	private Z_CustomerCreateFuncBackend backendCustom;

	/**
	 * Returns a reference to the backend object. The backend object 
	 * is instantiated by the framework. 
	 * @return a reference to the backend object
	 */
	private Z_CustomerCreateFuncBackend getCustomBackend() {
		if (backendCustom == null) {
			//create new backend object
			try {
				backendCustom =
				// the backend object is registered in customer version
				// of backendobject-config.xml using the 'Z_Custom' type
	 			(Z_CustomerCreateFuncBackend) bem.createBackendBusinessObject("Z_CustomerCreate");
			} catch (BackendException bex) {
				// The following key has to be added to WEB-INF/classes/ISAResources.properties 
				// in order to see the exception correctly e.g.
				// mycomp.error.getbo=an error has occured while creating backend object 
				log.debug("mycomp.error.getbo", bex);
			}
		}
		return backendCustom;
	}

	/**
	 * This method is needed when a business objecs has a corresponding
	 * backend object.
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}

	public Structure getImportStructure(){
		return getCustomBackend().getImportStructure();
	}
	
	public String createCustomer(Structure customer, String notes){
		return getCustomBackend().createCustomer(customer, notes);
	}

}
