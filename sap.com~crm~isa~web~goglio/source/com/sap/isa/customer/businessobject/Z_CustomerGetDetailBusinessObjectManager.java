/*
 * Created on 29-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.businessobject;

import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.management.BOManager;
import com.sap.isa.core.businessobject.management.DefaultBusinessObjectManager;

/**
 * @author greenorange
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Z_CustomerGetDetailBusinessObjectManager
	extends DefaultBusinessObjectManager
	implements BOManager, BackendAware {

	//		key used for the business object in customer version of bom-config.xml
	public static final String CUSTOM_BOM = "Z_CUSTOMERGETDETAIL-BOM";

	// reference to business object 
	private Z_CustomerGetDetailFunc mCustomBO;

	/**
	 * constructor
	 */
	public Z_CustomerGetDetailBusinessObjectManager() {
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.businessobject.management.BOManager#release()
	 */
	public void release() {

	}

	/**
	* Returns custom business object
	*/
	public Z_CustomerGetDetailFunc getCustomBO() {
		if (mCustomBO == null) {
			mCustomBO = new Z_CustomerGetDetailFunc();
			assignBackendObjectManager(mCustomBO);
		}
		return mCustomBO;
	}
}
