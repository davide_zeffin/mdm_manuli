/*****************************************************************************
  Copyright (c) 2006, SAP AG, All rights reserved.

  Example bases on ISA 5.0
*****************************************************************************/
package com.sap.isa.customer.businessobject;

// Internet Sales imports
import com.sap.isa.core.businessobject.management.BOManager;
import com.sap.isa.core.businessobject.management.DefaultBusinessObjectManager;
import com.sap.isa.core.businessobject.BackendAware;

/**
 * Template for a custom BusinessObjectManager in customer projects
 */
public class Z_CustomerCreateBusinessObjectManager
	extends DefaultBusinessObjectManager
	implements BOManager, BackendAware {
	
	// key used for the business object in customer version of bom-config.xml
	public static final String CUSTOM_BOM = "Z_CUSTOMERCREATE-BOM";

	// reference to business object 
	private Z_CustomerCreateFunc mCustomBO;

	/**
	 * constructor
	 */
	public Z_CustomerCreateBusinessObjectManager() {
	}

	/**
	 * Method is called by the framework before the session is invalidated.
	 * The implemenation of this method should free any allocated resources
	 */
	public void release() {
	}

	/**
	 * Returns custom business object
	 */
	public Z_CustomerCreateFunc getCustomBO() {
		if (mCustomBO == null) {
			mCustomBO = new Z_CustomerCreateFunc();
			assignBackendObjectManager(mCustomBO);
		}
		return mCustomBO;
	}
}