/*
 * Created on 29-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.businessobject;

import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.businessobject.BOBase;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.customer.backend.boi.Z_CustomerGetDetailFuncBackend;
import com.sap.mw.jco.JCO;

/**
 * @author greenorange
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Z_CustomerGetDetailFunc extends BOBase implements BackendAware {

	// initialize logging
	private static IsaLocation log =
		IsaLocation.getInstance(Z_CustomerCreateFunc.class.getName());

	private BackendObjectManager bem;
	private Z_CustomerGetDetailFuncBackend backendCustom;

	/**
	 * Returns a reference to the backend object. The backend object 
	 * is instantiated by the framework. 
	 * @return a reference to the backend object
	 */
	private Z_CustomerGetDetailFuncBackend getCustomBackend() {
		if (backendCustom == null) {
			//create new backend object
			try {
				backendCustom =
					// the backend object is registered in customer version
					// of backendobject-config.xml using the 'Z_CustomerGetDetail' type
					(Z_CustomerGetDetailFuncBackend) bem.createBackendBusinessObject(
						"Z_CustomerGetDetail");
			} catch (BackendException bex) {
				// The following key has to be added to WEB-INF/classes/ISAResources.properties 
				// in order to see the exception correctly e.g.
				// mycomp.error.getbo=an error has occured while creating backend object 
				log.debug("mycomp.error.getbo", bex);
			}
		}
		return backendCustom;
	}

	/**
	 * This method is needed when a business objecs has a corresponding
	 * backend object.
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}

	public JCO.Table getCustomerDefaults(String user, Shop shop) {
		return getCustomBackend().getCustomerDefaults(user, shop);
	}

}
