/*
 * Created on Apr 6, 2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.action;

// internet sales imports
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.customer.businessobject.Z_CustomerCreateBusinessObjectManager;
import com.sap.isa.isacore.AddressFormular;
import com.sap.mw.jco.JCO.Structure;

/**
 * This action acts as a template for customer extensions
 */
public class Z_CustomerCreateAction extends BaseAction {

	/**
   	* This method is called by the ISA Framework when the
   	* action is executed
   	*/
  	public ActionForward doPerform( ActionMapping mapping,
	  							  	ActionForm form,
	  							  	HttpServletRequest request,
								  	HttpServletResponse response)
		throws ServletException	
	{
		// Captcha test passed, remove error display
		request.setAttribute("captcha_error", "false");
		
		// get user session data object
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(request.getSession());

		// getting custom BOM
		Z_CustomerCreateBusinessObjectManager myBOM =
			(Z_CustomerCreateBusinessObjectManager)userSessionData.
				getBOM(Z_CustomerCreateBusinessObjectManager.CUSTOM_BOM);
				
		BusinessObjectManager bom = 
			(BusinessObjectManager)userSessionData.
				getBOM(BusinessObjectManager.ISACORE_BOM);				

		RequestParser requestParser = new RequestParser(request);
		AddressFormular addressFormular = new AddressFormular(requestParser);
		Address address = addressFormular.getAddress();
		
		Structure customer = myBOM.getCustomBO().getImportStructure();	
		String kunnr = null;
		Shop shop = bom.getShop();
		
		if (shop == null) {
				   log.exiting();
	//			   throw new PanicException("shop.notFound");
		}

		// set the correct address format
		addressFormular.setAddressFormatId(shop.getAddressFormat());
		
		customer.setValue(address.getTitle(), "TITLE_MEDI");
		customer.setValue(address.getName1(), "NAME1");
		customer.setValue(address.getName2(), "NAME2");
		customer.setValue(address.getCoName(), "CONTACT");					// **** INTERLOCUTORE
		customer.setValue(address.getStreet(), "STRAS");			
		customer.setValue(address.getPostlCod1(), "PSTLZ");
		customer.setValue(address.getCity(), "ORT01");
		customer.setValue(address.getCountry(), "LAND1");
		customer.setValue(address.getRegion(), "REGIO");
		customer.setValue(address.getEMail(), "SMTP_ADDR");
		customer.setValue(address.getTel1Numbr(), "TEL_NUMBER");
		customer.setValue(address.getFaxNumber(), "FAX_NUMBER");
		customer.setValue(address.getTel1Ext(), "MOB_NUMBER");
		customer.setValue(address.getStrSuppl1(), "STCD1");
		
		String notes = address.getStrSuppl2();
		
		kunnr = myBOM.getCustomBO().createCustomer(customer, notes);
		
		try{
			addressFormular.addToRequest(request,shop);
		}catch(CommunicationException e){
			// something...
			// provare a cambiare classe base
		}
		if (kunnr != null && kunnr.length() > 0){
			String args[] = {kunnr};
			address.addMessage(new Message(Message.SUCCESS, "customer.confirmcreateuser.jsp.confirm", args, null));
			return mapping.findForward("success");
			//WebUtil.translate()
		}else{
			address.addMessage(new Message(Message.ERROR, "customer.createuser.jsp.error"));
			return mapping.findForward("failure");
		}
	}
}


