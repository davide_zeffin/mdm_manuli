/*
 * Created on Apr 6, 2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.action;

// internet sales imports
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.DataValidator;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.AddressFormular;

/**
 * This action acts as a template for customer extensions
 */
public class Z_preCustomerCreateAction extends BaseAction {

	/**
		* This method is called by the ISA Framework when the
		* action is executed
		*/
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException 
	{
		// get user session data object
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(request.getSession());

		BusinessObjectManager bom =
			(BusinessObjectManager) userSessionData.getBOM(
				BusinessObjectManager.ISACORE_BOM);

		RequestParser requestParser = new RequestParser(request);
		AddressFormular addressFormular = new AddressFormular(requestParser);
		Shop shop = bom.getShop();

		if (shop == null){
			log.exiting();
			// throw new PanicException("shop.notFound");
		}
		try {
			addressFormular.addToRequest(request, shop);
		} catch (CommunicationException e) {
			// something...
			// provare a cambiare classe base
		}
		
		if (requestParser.getParameter("RegionList")
			.getValue().getString().equals("load")) 
		{
			// Changed country, load region
			return mapping.findForward("loadregion");
		} 
		else if(requestParser.getParameter("RegionList")
				.getValue().getString().equals("clear"))
		{
			// Clear address fields
			addressFormular.setAddress(new Address());
			return mapping.findForward("loadregion");
		}else{
			// check mandatory fields

			Address address = addressFormular.getAddress();

			if (address.getName1().length() <= 0 ||
				address.getStreet().length() <= 0 ||
				address.getPostlCod1().length() <= 0 ||
				address.getCity().length() <= 0 ||
				address.getCountry().length() <= 0 ||
				address.getEMail().length() <= 0 ||
				address.getTel1Numbr().length() <= 0)
			{
				address.addMessage(new Message(Message.ERROR, "customer.createuser.jsp.mandatory"));
			}else{
				if (DataValidator.isInt(address.getPostlCod1()) == false){
					address.addMessage(new Message(Message.ERROR, "customer.createuser.jsp.only_digits", null, "POSTL_COD1"));
					address.setPostlCod1("");
				}
				if (DataValidator.isEmail(address.getEMail()) == false){
					address.addMessage(new Message(Message.ERROR, "customer.createuser.jsp.email_error", null, "E_MAIL"));
				}
				if (DataValidator.isInt(address.getTel1Numbr()) == false){
					address.addMessage(new Message(Message.ERROR, "customer.createuser.jsp.only_digits", null, "TEL1NUMBR"));
					address.setTel1Numbr("");
				}
				if (address.getFaxNumber().length() > 0 &&
					DataValidator.isInt(address.getFaxNumber()) == false){
					address.addMessage(new Message(Message.ERROR, "customer.createuser.jsp.only_digits", null, "FAXNUMBR"));
					address.setFaxNumber("");
				}
				if (address.getTel1Ext().length() > 0 &&
					DataValidator.isInt(address.getTel1Ext()) == false){
					address.addMessage(new Message(Message.ERROR, "customer.createuser.jsp.only_digits", null, "TEL1EXT"));
					address.setTel1Ext("");
				}
			}			
				
			if (address.getMessageList().size() == 0){
				
				// Check if GTC is accepted
				if(requestParser.getParameter("GTC")
				.getValue().getBoolean())
				{
					request.setAttribute("captcha_error", "true");
					return mapping.findForward("success");
				}else{
					address.addMessage(new Message(Message.ERROR, "customer.createuser.jsp.GTC_error"));
					return mapping.findForward("failure");
				}				
			}else{
				return mapping.findForward("failure");
			}
		}
	}
}
