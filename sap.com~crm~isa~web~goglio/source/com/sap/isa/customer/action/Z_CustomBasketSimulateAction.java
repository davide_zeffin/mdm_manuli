/*
 * Created on 20-mag-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.businessevent.BusinessEventHandler;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.order.MaintainBasketSimulateAction;
/**
 * @author greenorange
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Z_CustomBasketSimulateAction
	extends MaintainBasketSimulateAction {

	/**
	 * 
	 */
	public Z_CustomBasketSimulateAction() {
		super();
	}

	protected String basketPerform(
		HttpServletRequest request,
		HttpServletResponse response,
		HttpSession session,
		UserSessionData userSessionData,
		RequestParser parser,
		BusinessObjectManager bom,
		IsaLocation log,
		IsaCoreInitAction.StartupParameter startupParameter,
		BusinessEventHandler eventHandler,
		boolean multipleInvocation,
		boolean browserBack,
		Shop shop,
		SalesDocument preOrderSalesDocument,
		boolean auction,
		DocumentState targetDocument,
		DocumentHandler documentHandler)
		throws CommunicationException {

		String poNumber = request.getParameter("poNumber");
		
		if (poNumber.length() <= 0) {
			preOrderSalesDocument.setInvalid();

			userSessionData.setAttribute(
				"yourRefError",
				WebUtil.translate(
					userSessionData.getLocale(),
					"customer.order.jsp.yourRefError",
					null));
		}

		return super.basketPerform(
			request,
			response,
			session,
			userSessionData,
			parser,
			bom,
			log,
			startupParameter,
			eventHandler,
			multipleInvocation,
			browserBack,
			shop,
			preOrderSalesDocument,
			auction,
			targetDocument,
			documentHandler);
	}

}
