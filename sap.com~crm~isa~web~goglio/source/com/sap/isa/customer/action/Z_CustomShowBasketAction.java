/*
 * Created on 23-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.action;
// internet sales imports
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.customer.businessobject.Z_CustomerGetDetailBusinessObjectManager;
import com.sap.mw.jco.JCO;

public class Z_CustomShowBasketAction extends BaseAction {
	
	/**
	 * This method is called by the ISA Framework when the
	 * action is executed
	 * @param mapping The ActionMapping used to select this instance
	 * @param action The optional ActionForm bean for this request (if any)
	 * @param request The HTTP request we are processing
	 * @param response The HTTP response we are creating
	 *
	 * @return <code>ActionForward</code> describing where the control should
	 *         be forwarded
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException {
		// get user session data object. This is used to cummunicate with the
		// session context
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(request.getSession());

		// Avoid reading again user's defaults parameter
		// if update was clicked
		if (userSessionData.getAttribute("refresh") != null)
			return mapping.findForward("success");
						
		// get Internet Sales core functionality BOM
		BusinessObjectManager isaBOM =
			(BusinessObjectManager) userSessionData.getBOM(
				BusinessObjectManager.ISACORE_BOM);

		Z_CustomerGetDetailBusinessObjectManager myBOM =
			(Z_CustomerGetDetailBusinessObjectManager) userSessionData.getBOM(
				Z_CustomerGetDetailBusinessObjectManager.CUSTOM_BOM);

		// get extended basket header data from the extension data container
		// associated with the Basket Business Object
		/*		String extBasketHeaderCurrency = 
					(isaBOM.getBasket().getExtensionData("ZEXT_WAERK") != null)
						? (String) isaBOM.getBasket().getExtensionData("ZEXT_WAERK")
						: isaBOM.getShop().getCurrency();
				
				String extBasketHeaderTermsPayment = 
					(String) isaBOM.getBasket().getExtensionData("ZEXT_ZTERM");
		
				// set extended data in request attribute. This attribute
				// is retrieved in the order.jsp
				if (extBasketHeaderCurrency != null)
					request.setAttribute("currency", extBasketHeaderCurrency);
				if (extBasketHeaderTermsPayment != null)
					request.setAttribute("paymentTerms", extBasketHeaderTermsPayment);
		*/
			
		HeaderSalesDocument basketHeader = isaBOM.getBasket().getHeader();

		// Read user's default parameter
		JCO.Table defaults = 
			myBOM.getCustomBO().getCustomerDefaults(
				isaBOM.getUser().getUserId(),
				isaBOM.getShop());

		String field;
		
		if (defaults != null && defaults.getNumRows() > 0) {
			defaults.firstRow();
			do {
				field = defaults.getField("ENTRY_KEY").getString();

				if (field.equals("CURRENCY"))
//					basketHeader.setCurrency(
//						defaults.getField("ENTRY_VALUE").getString());
					basketHeader.addExtensionData("ZEXT_WAERK",
						defaults.getField("ENTRY_VALUE").getString());
						
				else if (field.equals("PAYMENTTERMS"))
					basketHeader.setPaymentTerms(
						defaults.getField("ENTRY_VALUE").getString());
						
				else if (field.equals("INCOTERMS1"))
					basketHeader.setIncoTerms1(
						defaults.getField("ENTRY_VALUE").getString());
						
				else if (field.equals("INCOTERMS2"))
					basketHeader.setIncoTerms2(
						defaults.getField("ENTRY_VALUE").getString());

			} while (defaults.nextRow());
		}
		
//
//		if (basketHeader.getCurrency() == null)
//			basketHeader.setCurrency(isaBOM.getShop().getCurrency());
//
//		if (basketHeader.getPaymentTerms() == null)
//			basketHeader.setPaymentTerms("0001");

		return mapping.findForward("success");
	}
}
