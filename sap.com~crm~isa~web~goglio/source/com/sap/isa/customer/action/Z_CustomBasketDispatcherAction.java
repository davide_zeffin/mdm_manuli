/*
 * Created on 23-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.action;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.order.MaintainBasketDispatcherAction;

/**
 * @author greenorange
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Z_CustomBasketDispatcherAction
	extends MaintainBasketDispatcherAction {
		
	protected void parseRequest(
		RequestParser parser,
		UserSessionData userSessionData,
		SalesDocument salesDocument,
		IsaLocation log)
		throws CommunicationException {

		// first call the super class to parse the standard parameters
		super.parseRequest(parser, userSessionData, salesDocument, log);

		// get the sales document header and change the data
		HeaderSalesDocument header = salesDocument.getHeader();

		// get extended basket header data from the http-request
		String extBasketHeaderDataWaerk =
			parser.getParameter("currency").getValue().getString().toUpperCase();
		header.addExtensionData("ZEXT_WAERK", extBasketHeaderDataWaerk);
		//header.setCurrency(extBasketHeaderDataWaerk);

		String extBasketHeaderDataZterm =
			parser.getParameter("paymentTerms").getValue().getString().toUpperCase();
		//header.addExtensionData("ZEXT_ZTERM", extBasketHeaderDataZterm);
		header.setPaymentTerms(extBasketHeaderDataZterm);

		String extBasketHeaderDataInco1 =
			parser.getParameter("incoterms1").getValue().getString().toUpperCase();
		//header.addExtensionData("ZEXT_INCO1", extBasketHeaderDataInco1);
		header.setIncoTerms1(extBasketHeaderDataInco1);

		String extBasketHeaderDataInco2 =
			parser.getParameter("incoterms2").getValue().getString().toUpperCase();
		//header.addExtensionData("ZEXT_INCO2", extBasketHeaderDataInco2);
		header.setIncoTerms2(extBasketHeaderDataInco2);

	}
}
