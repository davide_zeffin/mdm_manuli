/*
 * Created on 27-mag-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;

/**
 * @author greenorange
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Z_SaveUser extends BaseAction {

	static String user = null;
	
	public static String getUser(){
		return user;
	}
	
	public static void setUser(String u){
		user = u;
	}
	
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(request.getSession());
		
//		get Internet Sales core functionality BOM
		BusinessObjectManager isaBOM =
			(BusinessObjectManager) userSessionData.getBOM(
				BusinessObjectManager.ISACORE_BOM);
				
		while(user != null){
			try{
				Thread.sleep(500);
			}catch(InterruptedException e){
			}
		}
			
		user = isaBOM.getUser().getUserId();
//		User user = (User) isaBOM.getUser();
//		user.addExtensionData("ID", user.getUserId());
		
		return mapping.findForward("success");
	}

}
