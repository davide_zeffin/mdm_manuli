/*
 * Created on Apr 8, 2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.catalog.cache.CatalogCache;
import com.sap.isa.catalog.cache.CatalogCacheException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.AddressFormular;

/**
 * @author Owner
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class zgo_UserAction extends com.sap.isa.user.action.UserBaseAction {
	/** Request parameter name for new user .*/
		private static IsaLocation log = IsaLocation.getInstance(
		zgo_UserAction.class.getName());
	
		public static final String PN_NEW_USERID = "newuser";
		public ActionForward ecomPerform(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
			throws IOException, ServletException, CommunicationException {

			log.debug("init begin GREENORANGE");
			String extNewUser = request.getParameter("zgo_newuser");
			System.out.println("GREENORANGE: " + extNewUser);
			
			BusinessObjectManager BOM =
			   (BusinessObjectManager)userSessionData.
				   getBOM(BusinessObjectManager.ISACORE_BOM);
			   
			
			if (extNewUser != null) {					
				Shop shop = BOM.getShop();
				
				if(shop == null){
					// Create TechKey for the selected shop
					TechKey shopKey = new TechKey("GOGLIO");
					shop = BOM.createShop(shopKey); // May throw CommunicationException						
				}
				ResultData titleList = shop.getTitleList();
				request.setAttribute("titleList", titleList);
				AddressFormular addressFormular =
							new AddressFormular(shop.getAddressFormat());
				
				Address address = new Address();
				
				// get the default country for the given shop
				String defaultCountryId = shop.getDefaultCountry();
				
				if(defaultCountryId != null && defaultCountryId.length() > 0){
					// default country
					address.setCountry(defaultCountryId);
					
					if(shop.getRegionList(defaultCountryId) != null){
						// get first region
						String region = shop.getRegionList(defaultCountryId)
										.getTable().getRow(1)
										.getField(Shop.ID).getString();
						// set region
						address.setRegion(region);
					}
				}
				
				// default title: company
				address.setTitleKey("0003");
						
				addressFormular.setAddress(address);
				addressFormular.addToRequest(request, shop);
				
				return mapping.findForward("zgo_newuser");
			} else {
				//BOM.getCatalogConfiguration().getId();
				try{
					CatalogCache.getInstance().clear();//removeInstance(keyToRemove);
				}catch(CatalogCacheException e){
				}
				
				// saving in the session the user_guest (in upper case!)
				userSessionData.setAttribute("user_guest", "GUEST");
				return mapping.findForward("success");
			}
		}
	}
