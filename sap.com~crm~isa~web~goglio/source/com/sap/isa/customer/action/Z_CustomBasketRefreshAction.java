/*
 * Created on 23-apr-2010
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.customer.action;

// internet sales imports
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Z_CustomBasketRefreshAction extends BaseAction {
	/**
	 * This method is called by the ISA Framework when the
	 * action is executed
	 * @param mapping The ActionMapping used to select this instance
	 * @param action The optional ActionForm bean for this request (if any)
	 * @param request The HTTP request we are processing
	 * @param response The HTTP response we are creating
	 *
	 * @return <code>ActionForward</code> describing where the control should
	 *         be forwarded
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException {
		// get user session data object. This is used to communicate with the
		// session context
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(request.getSession());

		// get Internet Sales core functionality BOM
		BusinessObjectManager isaBOM =
			(BusinessObjectManager) userSessionData.getBOM(
				BusinessObjectManager.ISACORE_BOM);

		HeaderSalesDocument basketHeader = isaBOM.getBasket().getHeader();

		String  curr = request.getParameter("currency"),
				payTerms = request.getParameter("paymentTerms"),
				inco1 = request.getParameter("incoterms1"),
				inco2 = request.getParameter("incoterms2");
		
		if(curr != null && curr.length() > 0)
			basketHeader.addExtensionData("ZEXT_WAERK",
				curr.toUpperCase());
			
		if(payTerms != null && payTerms.length() > 0)
			basketHeader.setPaymentTerms(
				payTerms.toUpperCase());

		if(inco1 != null && inco1.length() > 0)			
			basketHeader.setIncoTerms1(
				inco1.toUpperCase());
			
		if(inco2 != null && inco2.length() > 0)			
			basketHeader.setIncoTerms2(
				inco2.toUpperCase());

		// get extended basket header data from the http-request
		//	  String extBasketHeaderCurrency = request.getParameter("currency").toUpperCase();
		//	  String extBasketHeaderTermsPayment = request.getParameter("paymentTerms").toUpperCase();

		//	  request.setAttribute("ZEXT_WAERK",extBasketHeaderCurrency);
		//	  request.setAttribute("ZEXT_ZTERM",extBasketHeaderTermsPayment);
		// set extended data as extended data of the Basket Business Object
		//	  basket.addExtensionData("ZEXT_WAERK", extBasketHeaderCurrency);
		//	  basket.addExtensionData("ZEXT_ZTERM", extBasketHeaderTermsPayment);

		//	  log.debug("GREENORANGE");
		// entry for test purposes
		userSessionData.setAttribute("refresh", "true");

		return mapping.findForward("success");
	}

}
