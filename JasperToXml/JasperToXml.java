import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlWriter;

class ConvertJasperJrxml {

	public static String sourcePath, destinationPath, xml;
	public static JasperDesign jd = new JasperDesign();
	
	public static void main(String[] args) {

//		sourcePath = "C:\\Job\\JAVA\\MDMPortal\\reports\\MDMJasper\\ManuliExtCrimping.jasper";
//		destinationPath = "C:\\Job\\JAVA\\MDMPortal\\reports\\MDMJasper\\ManuliExtCrimping1.jrxml";
		sourcePath = "C:\\Job\\ManuliIntCrimping.jasper";
		destinationPath = "C:\\Job\\ManuliIntCrimping.jrxml";
		try {
			JasperReport report =
				(JasperReport) JRLoader.loadObject(sourcePath);
			JRXmlWriter.writeReport(report, destinationPath, "UTF-8");
		} catch (JRException e) {
			e.printStackTrace();
		}
	}
}
