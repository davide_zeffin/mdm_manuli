// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package com.sap.tut.wd.tree;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateTreeView).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.util.StringTokenizer;

import com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDTreeNodeType;
import com.sap.tc.webdynpro.services.sal.localization.api.IWDResourceHandler;
import com.sap.tc.webdynpro.services.sal.localization.api.WDResourceHandler;
import com.sap.tut.wd.tree.wdp.IPrivateTreeView;
//@@end

//@@begin documentation
//@@end

public class TreeView
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(TreeView.class);

  static 
  {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see com.sap.tut.wd.tree.wdp.IPrivateTreeView for more details
   */
  private final IPrivateTreeView wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see com.sap.tut.wd.tree.wdp.IPrivateTreeView.IContextNode for more details.
   */
  private final IPrivateTreeView.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public TreeView(IPrivateTreeView wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
  /** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()

    //===== STEP 1: Load all initial Data from Filesystem.properties ==========
    resourceHandlerForTree = WDResourceHandler.createResourceHandlerForCurrentSession();

    // Load the resource bundle "Filesystem.properties" located 
    // in the package "com.sap.tut.wd.tree.resources"
    // for locale set in the resource handler. 
    resourceHandlerForTree.loadResourceBundle(
      "com.sap.tut.wd.tree.resources.Filesystem",
      this.getClass().getClassLoader());

    // get all Drives    
    String drives = resourceHandlerForTree.getString("Drives");

    StringTokenizer strTokenizer = new StringTokenizer(drives, ";");

    //===== STEP 2: Create context node elements of the type IFolderContentNode =========
    IPrivateTreeView.IFolderContentNode rootFolderContentNode = wdContext.nodeFolderContent();
    IPrivateTreeView.IFolderContentElement rootFolderContentElement;

    //	begin populating context node 'FolderContent'
    String aDriveToken;
    while (strTokenizer.hasMoreTokens()) {
      aDriveToken = strTokenizer.nextToken();

      // instantiate the new context node element of type 'IFolderContentElement'
      rootFolderContentElement = rootFolderContentNode.createFolderContentElement();

      //set contained context value attributes
      rootFolderContentElement.setText(aDriveToken);
      rootFolderContentElement.setHasChildren(true);
      rootFolderContentElement.setIconSource("~sapicons/s_clofol.gif");
      rootFolderContentElement.setIgnoreAction(true);
      // add root folder element to root folder node			
      rootFolderContentNode.addElement(rootFolderContentElement);

      // if last folder node element, fill its non-singleton child node (storing its entries)
      // with children (folder node elements) and expand the node ('Drive D' in tree).
      if (!strTokenizer.hasMoreTokens()) {
        addChildren(rootFolderContentElement);
        rootFolderContentElement.setIsExpanded(true);
      } else {
        rootFolderContentElement.setIsExpanded(false);
      }
    }
    
    //@@end
  }

  //@@begin javadoc:wdDoExit()
  /** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
  /**
   * Hook method called to modify a view just before rendering.
   * This method conceptually belongs to the view itself, not to the
   * controller (cf. MVC pattern).
   * It is made static to discourage a way of programming that
   * routinely stores references to UI elements in instance fields
   * for access by the view controller's event handlers, and so on.
   * The Web Dynpro programming model recommends that UI elements can
   * only be accessed by code executed within the call to this hook method.
   *
   * @param wdThis Generated private interface of the view's controller, as
   *        provided by Web Dynpro. Provides access to the view controller's
   *        outgoing controller usages, etc.
   * @param wdContext Generated interface of the view's context, as provided
   *        by Web Dynpro. Provides access to the view's data.
   * @param view The view's generic API, as provided by Web Dynpro.
   *        Provides access to UI elements.
   * @param firstTime Indicates whether the hook is called for the first time
   *        during the lifetime of the view.
   */
  //@@end
  public static void wdDoModifyView(IPrivateTreeView wdThis, IPrivateTreeView.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView
    if (firstTime) {
      IWDTreeNodeType treeNode = (IWDTreeNodeType) view.getElement("TheNode");
      /* The following line is necessary to create parameter mapping from parameter "path" to parameter "selectedElement".
       * Parameter "path" is of type string and contains the string representation of the tree element (its corresponding context element to be exact) 
       * that raised the onAction event. Parameter "selectedElement" is of type IWDNodeElement (or extends it) and is defined as parameter in the event handler 
       * that handles the onAction. The parameter mapping defined here translates the String "path" into the corresponding context element that then can 
       * be accessed within the event handler
       */
      treeNode.mappingOfOnAction().addSourceMapping("path", "selectedElement");
      /* The following line is necessary to create parameter mapping from parameter "path" to parameter "element".
       * Parameter "path" is of type string and contains the string representation of the tree element (its corresponding context element to be exact) 
       * that raised the onLoadChildren event. Parameter "element" is of type IWDNodeElement (or extends it) and is defined as parameter in the event handler 
       * that handles the onLoadChildren. The parameter mapping defined here translates the String "path" into the corresponding context element that then can 
       * be accessed within the event handler
       */
      treeNode.mappingOfOnLoadChildren().addSourceMapping("path", "element");
    }
    //@@end
  }

  //@@begin javadoc:onActionSelect(ServerEvent)
  /** Declared validating event handler. */
  //@@end
  public void onActionSelect(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent, com.sap.tut.wd.tree.wdp.IPrivateTreeView.IFolderContentElement selectedElement )
  {
    //@@begin onActionSelect(ServerEvent)
    wdContext.currentContextElement().setTextOfSelectedNode(selectedElement.getText());
    //@@end
  }

  //@@begin javadoc:onActionLoadChildren(ServerEvent)
  /** Declared validating event handler. */
  //@@end
  public void onActionLoadChildren(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent, com.sap.tut.wd.tree.wdp.IPrivateTreeView.IFolderContentElement element )
  {
    //@@begin onActionLoadChildren(ServerEvent)
    addChildren(element);
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others

  // store resource handler for property resource file in private member variable
  private IWDResourceHandler resourceHandlerForTree = null;
  
  /**
   * Adds child elements to given parent element. The parent element corresponds to a folder. 
   * The folder content (folder elements) is stored as a list of context elements of type
   * IFolderContentElement within the non-singleton node 'ChildNode' under the parent node.   
   */
  private void addChildren(IPrivateTreeView.IFolderContentElement parent) {
    IPrivateTreeView.IFolderContentNode folderContentNode = parent.nodeChildNode();
    IPrivateTreeView.IFolderContentElement folderContentElement;

    // read entries (folder content) for given key (folder) in resource bundle    
    String entries = resourceHandlerForTree.getString(parent.getText());
    StringTokenizer strTokenizer = new StringTokenizer(entries, ";");
    String anEntryToken;

    // if there is no entry in Filesystem.properties for given
    // key (parent.getText()) then the key is returned.   
    if (entries.equals(parent.getText())) {
      folderContentElement = folderContentNode.createFolderContentElement();
      folderContentElement.setText("Empty Folder");
      folderContentElement.setHasChildren(false);
      folderContentElement.setIgnoreAction(true);
      folderContentNode.addElement(folderContentElement);
    } else {
      // populate non-singleton child node with node elements (folder content elements)
      while (strTokenizer.hasMoreTokens()) {
        anEntryToken = strTokenizer.nextToken();
        folderContentElement = folderContentNode.createFolderContentElement();

        if (anEntryToken.indexOf(".") != -1) {
          // entryToken is a file
          folderContentElement.setHasChildren(false);
          folderContentElement.setIgnoreAction(false);
          folderContentElement.setIconSource("~sapicons/s_b_crea.gif");
        } else {
          // entryToken is a folder
          folderContentElement.setHasChildren(true);
          folderContentElement.setIgnoreAction(true);
          folderContentElement.setIconSource("~sapicons/s_clofol.gif");
        }
        folderContentElement.setText(anEntryToken);
        folderContentElement.setIsExpanded(false);
        folderContentNode.addElement(folderContentElement);
      }
    }
  }

  //@@end
}
