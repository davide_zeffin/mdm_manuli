<?xml version="1.0" encoding="UTF-8"?>
<!-- MetaDataAPI generated on: Tuesday, August 17, 2004 11:00:21 AM CEST -->
<Controller xmlns="http://xml.sap.com/2002/10/metamodel/webdynpro" xmlns:IDX="urn:sap.com:WebDynpro.Controller:2.0" mmRelease="6.30" mmVersion="2.0" mmTimestamp="1092733221282" name="Main" type="view" package="com.sap.tc.webdynpro.tutorials.valuehelp" masterLanguage="en">
	<AppClass.CodeBody>
		<Core.Text><![CDATA[// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package com.sap.tc.webdynpro.tutorials.valuehelp;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateMain).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import com.sap.dictionary.runtime.ISimpleTypeModifiable;
import com.sap.tc.webdynpro.progmodel.api.IWDAttributeInfo;
import com.sap.tc.webdynpro.services.sal.localization.api.WDResourceHandler;
import com.sap.tc.webdynpro.tutorials.valuehelp.wdp.IPrivateMain;
import com.sap.typeservices.IModifiableSimpleValueSet;
//@@end

//@@begin documentation
//@@end

public class Main
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(Main.class);

  static 
  {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see com.sap.tc.webdynpro.tutorials.valuehelp.wdp.IPrivateMain for more details
   */
  private final IPrivateMain wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see com.sap.tc.webdynpro.tutorials.valuehelp.wdp.IPrivateMain.IContextNode for more details.
   */
  private final IPrivateMain.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public Main(IPrivateMain wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
  /** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()

    //Access interface ISimpleTypeModifiable for modifying the attribute's datatype
    IWDAttributeInfo attributeInfo =
      wdContext.getNodeInfo().getAttribute(IPrivateMain.IContextElement.COUNTRY);
    ISimpleTypeModifiable countryType = attributeInfo.getModifiableSimpleType();

    // Set field label and populate valueset
    countryType.setFieldLabel("Country");
    IModifiableSimpleValueSet valueSet =
      countryType.getSVServices().getModifiableSimpleValueSet();
    for (int i = 0; i < 40; i++) {
      valueSet.put("Key_" + i, "Country " + i);
    }

    // initialize context value attribute 'Country'
    wdContext.currentContextElement().setCountry("Key_0");
    //@@end
  }

  //@@begin javadoc:wdDoExit()
  /** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
  /**
   * Hook method called to modify a view just before rendering.
   * This method conceptually belongs to the view itself, not to the
   * controller (cf. MVC pattern).
   * It is made static in order to discourage a way of programming that
   * routinely stores references to UI elements in instance fields
   * for access by the view controller's event handlers etc.
   * The Web Dynpro programming model recommends to restrict access to
   * UI elements to code executed within the call to this hook method!
   *
   * @param wdThis generated private interface of the view's controller as
   *        provided by Web Dynpro; provides access to the view controller's
   *        outgoing controller usages etc.
   * @param wdContext generated interface of the view's context as provided
   *        by Web Dynpro; provides access to the view's data
   * @param view the view's generic API as provided by Web Dynpro;
   *        provides access to UI elements
   * @param firstTime indicates whether the hook is called for the first time
   *        during the lifetime of the view
   */
  //@@end
  public static void wdDoModifyView(IPrivateMain wdThis, IPrivateMain.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView
    //@@end
  }

  //@@begin javadoc:onActionSave(ServerEvent)
  /** declared validating event handler */
  //@@end
  public void onActionSave(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionSave(ServerEvent)
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
}
]]></Core.Text>
	</AppClass.CodeBody>
	<Controller.Parent>
		<Core.Reference package="com.sap.tc.webdynpro.tutorials.valuehelp" name="Valuehelp" type="Component"/>
	</Controller.Parent>
	<Controller.Actions>
		<Action name="Save">
			<IncomingEvent.EventHandler>
				<Core.Reference path="EventHandler:onActionSave"/>
			</IncomingEvent.EventHandler>
		</Action>
	</Controller.Actions>
	<Controller.Context>
		<ContextValueNode cardinality="_1_1" name="Context" selection="_1_1">
			<ContextNode.Attributes>
				<ContextValueAttribute name="Color">
					<ContextValueAttribute.Type>
						<Core.ForeignReference modelName="DtDictionary" package="com.sap.tc.webdynpro.tutorials.valuehelp.simpletypes" name="Color" type="DtSimpleType"/>
					</ContextValueAttribute.Type>
				</ContextValueAttribute>
				<ContextValueAttribute name="Country">
					<ContextValueAttribute.Type>
						<Core.ForeignReference modelName="DtDictionary" package="com.sap.dictionary" name="string" type="DtSimpleType"/>
					</ContextValueAttribute.Type>
				</ContextValueAttribute>
			</ContextNode.Attributes>
		</ContextValueNode>
	</Controller.Context>
	<Controller.EventHandlers>
		<ControllerEventHandler name="onActionSave">
		</ControllerEventHandler>
	</Controller.EventHandlers>
	<Controller.View>
		<Core.Reference package="com.sap.tc.webdynpro.tutorials.valuehelp" name="Main" type="View"/>
	</Controller.View>
</Controller>
