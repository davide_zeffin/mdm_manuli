package com.sap.engine.examples.util;

/**
 * @author 1079
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class QuickCarRentalException extends Exception {

	/**
	 * Constructor for QuickCarRentalException.
	 */
	public QuickCarRentalException() {
		super();
	}

	/**
	 * Constructor for QuickCarRentalException.
	 * @param s
	 */
	public QuickCarRentalException(String s) {
		super(s);
	}

}
