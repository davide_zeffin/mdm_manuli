package com.sap.engine.examples.util;

/**
 * @author 1079
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class Constants {
  	public static final java.text.SimpleDateFormat FORMATTER = new java.text.SimpleDateFormat("dd.MM.yyyy");
	
	public static final java.lang.String STATUS_ACTIVE = "ACTIVE";
  	public static final java.lang.String STATUS_CANCELLED = "CANCELLED";
  	
  	public static final int ACTION_SAVE = 0;
  	public static final int ACTION_CANCEL = 1;
  	
  	public static final String RESERVATIONS = "rent.car.customer.reservations";	
	public static final String CLIENT_MESSAGE = "rent.car.customer.message";
	
	public static final String[] LOCATION =	{"Heidelberg","Frankfurt","Kassel","Berlin","Mannheim","Leipzig","Hamburg" };
	public static final String[] VEHICLE_TYPE ={"Economy","Compact","Intermediate","Full Size","Premium","Luxury","Mini Van" };	
}
