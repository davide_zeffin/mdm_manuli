package com.sap.engine.examples.util;

import java.io.Serializable;

/**
 * @author 1079
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class QuickBookingModel implements Serializable {
	private java.lang.String bookingId;
	private java.lang.String vehicleType;
	private java.lang.String dateFrom;
	private java.lang.String dateTo;
	private java.lang.String pickupLocation;
	private java.lang.String dropoffLocation;
	private java.lang.String price;

	/**
	 * Returns the bookingId.
	 * @return java.lang.String
	 */
	public java.lang.String getBookingId() {
		return bookingId;
	}

	/**
	 * Returns the dateFrom.
	 * @return java.lang.String
	 */
	public java.lang.String getDateFrom() {
		return dateFrom;
	}

	/**
	 * Returns the dateTo.
	 * @return java.lang.String
	 */
	public java.lang.String getDateTo() {
		return dateTo;
	}

	/**
	 * Returns the dropoffLocation.
	 * @return java.lang.String
	 */
	public java.lang.String getDropoffLocation() {
		return dropoffLocation;
	}

	/**
	 * Returns the pickupLocation.
	 * @return java.lang.String
	 */
	public java.lang.String getPickupLocation() {
		return pickupLocation;
	}

	/**
	 * Returns the price.
	 * @return java.lang.String
	 */
	public java.lang.String getPrice() {
		return price;
	}

	/**
	 * Returns the vehicleType.
	 * @return java.lang.String
	 */
	public java.lang.String getVehicleType() {
		return vehicleType;
	}

	/**
	 * Sets the bookingId.
	 * @param bookingId The bookingId to set
	 */
	public void setBookingId(java.lang.String bookingId) {
		this.bookingId = bookingId;
	}

	/**
	 * Sets the dateFrom.
	 * @param dateFrom The dateFrom to set
	 */
	public void setDateFrom(java.lang.String dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * Sets the dateTo.
	 * @param dateTo The dateTo to set
	 */
	public void setDateTo(java.lang.String dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * Sets the dropoffLocation.
	 * @param dropoffLocation The dropoffLocation to set
	 */
	public void setDropoffLocation(java.lang.String dropoffLocation) {
		this.dropoffLocation = dropoffLocation;
	}

	/**
	 * Sets the pickupLocation.
	 * @param pickupLocation The pickupLocation to set
	 */
	public void setPickupLocation(java.lang.String pickupLocation) {
		this.pickupLocation = pickupLocation;
	}

	/**
	 * Sets the price.
	 * @param price The price to set
	 */
	public void setPrice(java.lang.String price) {
		this.price = price;
	}

	/**
	 * Sets the vehicleType.
	 * @param vehicleType The vehicleType to set
	 */
	public void setVehicleType(java.lang.String vehicleType) {
		this.vehicleType = vehicleType;
	}

}
