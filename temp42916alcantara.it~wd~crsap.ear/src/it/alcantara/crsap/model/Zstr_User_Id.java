// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap.model;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Zstr_User_Id" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Zstr_User_Id extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Zstr_User_Id.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.alcantara.crsap.model.CRSAPArchive.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Zstr_User_Id () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.alcantara.crsap.model.CRSAPArchive.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.alcantara.crsap.model.CRSAPArchive.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Zstr_User_Id.class,
                       "ZSTR_USER_ID",
                       PROXYTYPE_STRUCTURE, 
                       "Zstr_User_Id", 
                       "it.alcantara.crsap.model" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Zstr_User_Id (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zstr_User_Id (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zstr_User_Id (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.alcantara.crsap.model.CRSAPArchive 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Zstr_User_Id ( it.alcantara.crsap.model.CRSAPArchive modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public CRSAPArchive modelInstance() {
    return (CRSAPArchive)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.alcantara.crsap.model.CRSAPArchive.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> User_Id
   * **************************************************************************/
  /** getter for ModelAttribute -> User_Id 
   *  @return value of ModelAttribute User_Id */
  public java.lang.String getUser_Id() {
    return super.getAttributeValueAsString("User_Id");
  }
 
  /** setter for ModelAttribute -> User_Id 
   *  @param value new value for ModelAttribute User_Id */
  public void setUser_Id(java.lang.String value) {
    super.setAttributeValueAsString("User_Id", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
  public static class Zstr_User_Id_List extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCList implements java.io.Serializable , java.util.List   {

    protected Class _associatedModelClazz() { return _associatedModelClazz; }
	protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){ return DEFAULT_SCOPE; }

    private static final long serialVersionUID = 60422512L ;
    
    public Zstr_User_Id_List () {
      super(createElementProperties(it.alcantara.crsap.model.Zstr_User_Id.class, new it.alcantara.crsap.model.Zstr_User_Id() ));
    }

    /** Constructor for specific scope
     *  use this Constructor, if this List should belong to a model instance
     *  of a different scope than the default model scope.
     *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     */
    public Zstr_User_Id_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
      this();
      maintainInScope(scope);
    }
  
    /** Constructor for specific modelInstanceId
     *  use this Constructor, if this List should belong to a different model instance
     *  than the default model instance.
     *  The default modelInstance has no id (null). 
     * 
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zstr_User_Id_List (String modelInstanceId) {
      this();
      maintainInInstanceId(modelInstanceId);
    }
  
    /** Constructor for specific scope and modelInstanceId
     *  use this Constructor, of this List should belong to a differnt scope
     *  and a different model instance than the default model scope and model instance.
     *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
     *  The default modelInstance has no id (null). 
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zstr_User_Id_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
      this();
      maintainInScopeAndInstanceId(scope, modelInstanceId);
    }
  
    /** Constructor for specific model Instance
     *  use this Constructor, if this instance should belong to a specific instance
     *  of the model it.alcantara.crsap.model.CRSAPArchive 
     *  passed as a parameter to this constructor.
     * 
     *  @param modelInstance the DynamicRFCModel referring to the modelInstance
     */
    public Zstr_User_Id_List ( it.alcantara.crsap.model.CRSAPArchive modelInstance) {
      this();
      maintainInModel(modelInstance);
    }

    public it.alcantara.crsap.model.Zstr_User_Id[] toArrayZstr_User_Id() {
      return (it.alcantara.crsap.model.Zstr_User_Id[])super.toArray(new it.alcantara.crsap.model.Zstr_User_Id[] {});
    }
    
    public int lastIndexOfZstr_User_Id(it.alcantara.crsap.model.Zstr_User_Id item) {
      return super.lastIndexOf(item);
    }
    
    public int indexOfZstr_User_Id(it.alcantara.crsap.model.Zstr_User_Id item) {
      return super.indexOf(item);
    }
    
    public Zstr_User_Id_List subListZstr_User_Id(int fromIndex, int toIndex) {
      return (Zstr_User_Id_List)super.subList(fromIndex, toIndex);
    }
    
    public void addAllZstr_User_Id(Zstr_User_Id_List item) {
      super.addAll(item);
    }
    
    public void addZstr_User_Id(it.alcantara.crsap.model.Zstr_User_Id item) {
      super.add(item);
    }
    
    public boolean removeZstr_User_Id(it.alcantara.crsap.model.Zstr_User_Id item) {
      return super.remove(item);
    }
    
    public it.alcantara.crsap.model.Zstr_User_Id getZstr_User_Id(int index) {
      return (it.alcantara.crsap.model.Zstr_User_Id)super.get(index);
    }
    
    public boolean containsAllZstr_User_Id(Zstr_User_Id_List item) {
      return super.containsAll(item);
    }
    
    public void addZstr_User_Id(int index, it.alcantara.crsap.model.Zstr_User_Id item) {
      super.add(index, item);
    }
    
    public boolean containsZstr_User_Id(it.alcantara.crsap.model.Zstr_User_Id item) {
      return super.contains(item);
    }
        
    public void addAllZstr_User_Id(int index, Zstr_User_Id_List item) {
      super.addAll(index, item);
    }
    
    public it.alcantara.crsap.model.Zstr_User_Id setZstr_User_Id(int index, it.alcantara.crsap.model.Zstr_User_Id item) {
      return (it.alcantara.crsap.model.Zstr_User_Id)super.set(index, item);
    }
    
    public it.alcantara.crsap.model.Zstr_User_Id removeZstr_User_Id(int index) {
      return (it.alcantara.crsap.model.Zstr_User_Id)super.remove(index);
    }
  }
  
}
