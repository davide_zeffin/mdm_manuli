// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap.model;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Zcrsap_Attachment" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Zcrsap_Attachment extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Zcrsap_Attachment.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.alcantara.crsap.model.CRSAPArchive.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Zcrsap_Attachment () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.alcantara.crsap.model.CRSAPArchive.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.alcantara.crsap.model.CRSAPArchive.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Zcrsap_Attachment.class,
                       "ZCRSAP_ATTACHMENT",
                       PROXYTYPE_STRUCTURE, 
                       "Zcrsap_Attachment", 
                       "it.alcantara.crsap.model" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Zcrsap_Attachment (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_Attachment (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_Attachment (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.alcantara.crsap.model.CRSAPArchive 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Zcrsap_Attachment ( it.alcantara.crsap.model.CRSAPArchive modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public CRSAPArchive modelInstance() {
    return (CRSAPArchive)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.alcantara.crsap.model.CRSAPArchive.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> Binary_File
   * **************************************************************************/
  /** getter for ModelAttribute -> Binary_File 
   *  @return value of ModelAttribute Binary_File */
  public java.lang.String getBinary_File() {
    return super.getAttributeValueAsString("Binary_File");
  }
 
  /** setter for ModelAttribute -> Binary_File 
   *  @param value new value for ModelAttribute Binary_File */
  public void setBinary_File(java.lang.String value) {
    super.setAttributeValueAsString("Binary_File", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> File_Name
   * **************************************************************************/
  /** getter for ModelAttribute -> File_Name 
   *  @return value of ModelAttribute File_Name */
  public java.lang.String getFile_Name() {
    return super.getAttributeValueAsString("File_Name");
  }
 
  /** setter for ModelAttribute -> File_Name 
   *  @param value new value for ModelAttribute File_Name */
  public void setFile_Name(java.lang.String value) {
    super.setAttributeValueAsString("File_Name", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
  public static class Zcrsap_Attachment_List extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCList implements java.io.Serializable , java.util.List   {

    protected Class _associatedModelClazz() { return _associatedModelClazz; }
	protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){ return DEFAULT_SCOPE; }

    private static final long serialVersionUID = 60422512L ;
    
    public Zcrsap_Attachment_List () {
      super(createElementProperties(it.alcantara.crsap.model.Zcrsap_Attachment.class, new it.alcantara.crsap.model.Zcrsap_Attachment() ));
    }

    /** Constructor for specific scope
     *  use this Constructor, if this List should belong to a model instance
     *  of a different scope than the default model scope.
     *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     */
    public Zcrsap_Attachment_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
      this();
      maintainInScope(scope);
    }
  
    /** Constructor for specific modelInstanceId
     *  use this Constructor, if this List should belong to a different model instance
     *  than the default model instance.
     *  The default modelInstance has no id (null). 
     * 
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zcrsap_Attachment_List (String modelInstanceId) {
      this();
      maintainInInstanceId(modelInstanceId);
    }
  
    /** Constructor for specific scope and modelInstanceId
     *  use this Constructor, of this List should belong to a differnt scope
     *  and a different model instance than the default model scope and model instance.
     *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
     *  The default modelInstance has no id (null). 
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zcrsap_Attachment_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
      this();
      maintainInScopeAndInstanceId(scope, modelInstanceId);
    }
  
    /** Constructor for specific model Instance
     *  use this Constructor, if this instance should belong to a specific instance
     *  of the model it.alcantara.crsap.model.CRSAPArchive 
     *  passed as a parameter to this constructor.
     * 
     *  @param modelInstance the DynamicRFCModel referring to the modelInstance
     */
    public Zcrsap_Attachment_List ( it.alcantara.crsap.model.CRSAPArchive modelInstance) {
      this();
      maintainInModel(modelInstance);
    }

    public it.alcantara.crsap.model.Zcrsap_Attachment[] toArrayZcrsap_Attachment() {
      return (it.alcantara.crsap.model.Zcrsap_Attachment[])super.toArray(new it.alcantara.crsap.model.Zcrsap_Attachment[] {});
    }
    
    public int lastIndexOfZcrsap_Attachment(it.alcantara.crsap.model.Zcrsap_Attachment item) {
      return super.lastIndexOf(item);
    }
    
    public int indexOfZcrsap_Attachment(it.alcantara.crsap.model.Zcrsap_Attachment item) {
      return super.indexOf(item);
    }
    
    public Zcrsap_Attachment_List subListZcrsap_Attachment(int fromIndex, int toIndex) {
      return (Zcrsap_Attachment_List)super.subList(fromIndex, toIndex);
    }
    
    public void addAllZcrsap_Attachment(Zcrsap_Attachment_List item) {
      super.addAll(item);
    }
    
    public void addZcrsap_Attachment(it.alcantara.crsap.model.Zcrsap_Attachment item) {
      super.add(item);
    }
    
    public boolean removeZcrsap_Attachment(it.alcantara.crsap.model.Zcrsap_Attachment item) {
      return super.remove(item);
    }
    
    public it.alcantara.crsap.model.Zcrsap_Attachment getZcrsap_Attachment(int index) {
      return (it.alcantara.crsap.model.Zcrsap_Attachment)super.get(index);
    }
    
    public boolean containsAllZcrsap_Attachment(Zcrsap_Attachment_List item) {
      return super.containsAll(item);
    }
    
    public void addZcrsap_Attachment(int index, it.alcantara.crsap.model.Zcrsap_Attachment item) {
      super.add(index, item);
    }
    
    public boolean containsZcrsap_Attachment(it.alcantara.crsap.model.Zcrsap_Attachment item) {
      return super.contains(item);
    }
        
    public void addAllZcrsap_Attachment(int index, Zcrsap_Attachment_List item) {
      super.addAll(index, item);
    }
    
    public it.alcantara.crsap.model.Zcrsap_Attachment setZcrsap_Attachment(int index, it.alcantara.crsap.model.Zcrsap_Attachment item) {
      return (it.alcantara.crsap.model.Zcrsap_Attachment)super.set(index, item);
    }
    
    public it.alcantara.crsap.model.Zcrsap_Attachment removeZcrsap_Attachment(int index) {
      return (it.alcantara.crsap.model.Zcrsap_Attachment)super.remove(index);
    }
  }
  
}
