// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap.model;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Zcrsap_Create_Input" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Zcrsap_Create_Input extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClassExecutable implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Zcrsap_Create_Input.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.alcantara.crsap.model.CRSAPArchive.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Zcrsap_Create_Input () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.alcantara.crsap.model.CRSAPArchive.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.alcantara.crsap.model.CRSAPArchive.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Zcrsap_Create_Input.class,
                       "ZCRSAP_CREATE",
                       PROXYTYPE_INPUT, 
                       "Zcrsap_Create_Input", 
                       "it.alcantara.crsap.model" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Zcrsap_Create_Input (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_Create_Input (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.alcantara.crsap.model.CRSAPArchive.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_Create_Input (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.alcantara.crsap.model.CRSAPArchive 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Zcrsap_Create_Input ( it.alcantara.crsap.model.CRSAPArchive modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public CRSAPArchive modelInstance() {
    return (CRSAPArchive)associatedModel();
  }

  /**
   * Returns the result from the last RFC call 
   */
  public it.alcantara.crsap.model.Zcrsap_Create_Output getOutput() {
    return (it.alcantara.crsap.model.Zcrsap_Create_Output)_outputAdapter();
  }

  /**
   * Hook method that executes the RFC call to the backend by calling the 
   * corresponding send method in the model class.</p>
   *
   * The result can be retrieved using the method "getOutput()", giving access to the
   * default 1:1 relation role "Output" which all executable ModelClasses do implement.</p>
   */
  protected final void doExecute() 
    throws com.sap.aii.proxy.framework.core.SystemFaultException,
           com.sap.aii.proxy.framework.core.ApplicationFaultException,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException {
    _outputAdapter( modelInstance().zcrsap_Create(this));
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.alcantara.crsap.model.CRSAPArchive.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /****************************************************************************
   *  1:n Relation -> Approvatori
   ***************************************************************************/

  /** getter for 1:n Relation Role -> Approvatori 
   *  @return java.util.List containing elements of 1:n Relation Role Approvatori */
  public java.util.List getApprovatori() {
    return (java.util.List)getRelatedModelObjects("Approvatori");
  }

  /** setter for 1:n Relation Role -> Approvatori 
   *  @param newList java.util.List replaces previous List of elements of 1:n Relation Role Approvatori */
  public void setApprovatori(com.sap.aii.proxy.framework.core.AbstractList list) {
    setRelatedModelObjects("Approvatori", list);
  }

  /** adds an element to the 1:n Relation Role -> Approvatori 
   *  @param o object to be added to List of elements of 1:n Relation Role Approvatori
   *  @return true if element was added */
  public boolean addApprovatori(it.alcantara.crsap.model.Zstr_User_Id o) {
    return addRelatedModelObject("Approvatori", o);
  }

  /** removes the given element from the 1:n Relation Role -> Approvatori 
   *  @param o object to be removed from List of elements of 1:n Relation Role Approvatori 
   *  @return true if element existed and was removed */
  public boolean removeApprovatori(it.alcantara.crsap.model.Zstr_User_Id o)  {
    return removeRelatedModelObject("Approvatori", o);
  }
  
  /****************************************************************************
   *  1:n Relation -> Approvatori_Finali
   ***************************************************************************/

  /** getter for 1:n Relation Role -> Approvatori_Finali 
   *  @return java.util.List containing elements of 1:n Relation Role Approvatori_Finali */
  public java.util.List getApprovatori_Finali() {
    return (java.util.List)getRelatedModelObjects("Approvatori_Finali");
  }

  /** setter for 1:n Relation Role -> Approvatori_Finali 
   *  @param newList java.util.List replaces previous List of elements of 1:n Relation Role Approvatori_Finali */
  public void setApprovatori_Finali(com.sap.aii.proxy.framework.core.AbstractList list) {
    setRelatedModelObjects("Approvatori_Finali", list);
  }

  /** adds an element to the 1:n Relation Role -> Approvatori_Finali 
   *  @param o object to be added to List of elements of 1:n Relation Role Approvatori_Finali
   *  @return true if element was added */
  public boolean addApprovatori_Finali(it.alcantara.crsap.model.Zstr_User_Id o) {
    return addRelatedModelObject("Approvatori_Finali", o);
  }

  /** removes the given element from the 1:n Relation Role -> Approvatori_Finali 
   *  @param o object to be removed from List of elements of 1:n Relation Role Approvatori_Finali 
   *  @return true if element existed and was removed */
  public boolean removeApprovatori_Finali(it.alcantara.crsap.model.Zstr_User_Id o)  {
    return removeRelatedModelObject("Approvatori_Finali", o);
  }
  
  /****************************************************************************
   *  1:n Relation -> Attach_Files
   ***************************************************************************/

  /** getter for 1:n Relation Role -> Attach_Files 
   *  @return java.util.List containing elements of 1:n Relation Role Attach_Files */
  public java.util.List getAttach_Files() {
    return (java.util.List)getRelatedModelObjects("Attach_Files");
  }

  /** setter for 1:n Relation Role -> Attach_Files 
   *  @param newList java.util.List replaces previous List of elements of 1:n Relation Role Attach_Files */
  public void setAttach_Files(com.sap.aii.proxy.framework.core.AbstractList list) {
    setRelatedModelObjects("Attach_Files", list);
  }

  /** adds an element to the 1:n Relation Role -> Attach_Files 
   *  @param o object to be added to List of elements of 1:n Relation Role Attach_Files
   *  @return true if element was added */
  public boolean addAttach_Files(it.alcantara.crsap.model.Zcrsap_Attachment o) {
    return addRelatedModelObject("Attach_Files", o);
  }

  /** removes the given element from the 1:n Relation Role -> Attach_Files 
   *  @param o object to be removed from List of elements of 1:n Relation Role Attach_Files 
   *  @return true if element existed and was removed */
  public boolean removeAttach_Files(it.alcantara.crsap.model.Zcrsap_Attachment o)  {
    return removeRelatedModelObject("Attach_Files", o);
  }
  
  /****************************************************************************
   *  1:n Relation -> Grupppo_It
   ***************************************************************************/

  /** getter for 1:n Relation Role -> Grupppo_It 
   *  @return java.util.List containing elements of 1:n Relation Role Grupppo_It */
  public java.util.List getGrupppo_It() {
    return (java.util.List)getRelatedModelObjects("Grupppo_It");
  }

  /** setter for 1:n Relation Role -> Grupppo_It 
   *  @param newList java.util.List replaces previous List of elements of 1:n Relation Role Grupppo_It */
  public void setGrupppo_It(com.sap.aii.proxy.framework.core.AbstractList list) {
    setRelatedModelObjects("Grupppo_It", list);
  }

  /** adds an element to the 1:n Relation Role -> Grupppo_It 
   *  @param o object to be added to List of elements of 1:n Relation Role Grupppo_It
   *  @return true if element was added */
  public boolean addGrupppo_It(it.alcantara.crsap.model.Zstr_User_Id o) {
    return addRelatedModelObject("Grupppo_It", o);
  }

  /** removes the given element from the 1:n Relation Role -> Grupppo_It 
   *  @param o object to be removed from List of elements of 1:n Relation Role Grupppo_It 
   *  @return true if element existed and was removed */
  public boolean removeGrupppo_It(it.alcantara.crsap.model.Zstr_User_Id o)  {
    return removeRelatedModelObject("Grupppo_It", o);
  }
  
  /****************************************************************************
   *  1:n Relation -> History_Action
   ***************************************************************************/

  /** getter for 1:n Relation Role -> History_Action 
   *  @return java.util.List containing elements of 1:n Relation Role History_Action */
  public java.util.List getHistory_Action() {
    return (java.util.List)getRelatedModelObjects("History_Action");
  }

  /** setter for 1:n Relation Role -> History_Action 
   *  @param newList java.util.List replaces previous List of elements of 1:n Relation Role History_Action */
  public void setHistory_Action(com.sap.aii.proxy.framework.core.AbstractList list) {
    setRelatedModelObjects("History_Action", list);
  }

  /** adds an element to the 1:n Relation Role -> History_Action 
   *  @param o object to be added to List of elements of 1:n Relation Role History_Action
   *  @return true if element was added */
  public boolean addHistory_Action(it.alcantara.crsap.model.Zcrsap_History o) {
    return addRelatedModelObject("History_Action", o);
  }

  /** removes the given element from the 1:n Relation Role -> History_Action 
   *  @param o object to be removed from List of elements of 1:n Relation Role History_Action 
   *  @return true if element existed and was removed */
  public boolean removeHistory_Action(it.alcantara.crsap.model.Zcrsap_History o)  {
    return removeRelatedModelObject("History_Action", o);
  }
  
  /****************************************************************************
   *  1:n Relation -> Richiedenti
   ***************************************************************************/

  /** getter for 1:n Relation Role -> Richiedenti 
   *  @return java.util.List containing elements of 1:n Relation Role Richiedenti */
  public java.util.List getRichiedenti() {
    return (java.util.List)getRelatedModelObjects("Richiedenti");
  }

  /** setter for 1:n Relation Role -> Richiedenti 
   *  @param newList java.util.List replaces previous List of elements of 1:n Relation Role Richiedenti */
  public void setRichiedenti(com.sap.aii.proxy.framework.core.AbstractList list) {
    setRelatedModelObjects("Richiedenti", list);
  }

  /** adds an element to the 1:n Relation Role -> Richiedenti 
   *  @param o object to be added to List of elements of 1:n Relation Role Richiedenti
   *  @return true if element was added */
  public boolean addRichiedenti(it.alcantara.crsap.model.Zstr_User_Id o) {
    return addRelatedModelObject("Richiedenti", o);
  }

  /** removes the given element from the 1:n Relation Role -> Richiedenti 
   *  @param o object to be removed from List of elements of 1:n Relation Role Richiedenti 
   *  @return true if element existed and was removed */
  public boolean removeRichiedenti(it.alcantara.crsap.model.Zstr_User_Id o)  {
    return removeRelatedModelObject("Richiedenti", o);
  }
  
  /****************************************************************************
   * 1:1 Relation -> Cr_Attributes
   ***************************************************************************/
  /** getter for ModelAttribute -> Cr_Attributes 
   *  @return value of ModelAttribute Cr_Attributes */
  public it.alcantara.crsap.model.Zcrsap_Attributes getCr_Attributes() {
    return (it.alcantara.crsap.model.Zcrsap_Attributes)getRelatedModelObject("Cr_Attributes");
  }
  
  /** setter for 1:1 Relation Role -> Cr_Attributes 
   *  @param value new value for 1:1 Relation Role  Cr_Attributes */
  public void setCr_Attributes(it.alcantara.crsap.model.Zcrsap_Attributes value) {
    setRelatedModelObject("Cr_Attributes",value);
  }
  
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
}
