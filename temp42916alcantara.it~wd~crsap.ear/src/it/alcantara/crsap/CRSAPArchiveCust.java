// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateCRSAPArchiveCust).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import it.alcantara.crsap.model.Zcrsap_Attributes;
import it.alcantara.crsap.model.Zcrsap_Create_Input;
import it.alcantara.crsap.model.Zcrsap_History;
import it.alcantara.crsap.model.Zstr_User_Id;
import it.alcantara.crsap.wdp.IPrivateCRSAPArchiveCust;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IAccettazioneListElement;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IAccettazioneListNode;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IActionPerformedElement;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IActionPerformedNode;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IApprovalListElement;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IApprovalListNode;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.ICRAttributesElement;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.ICr_AttributesElement;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IITListElement;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IITListNode;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IReaderListElement;
import it.alcantara.crsap.wdp.IPublicCRSAPArchiveCust.IReaderListNode;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException;
import com.sap.tc.webdynpro.progmodel.api.IWDMessageManager;
//@@end

//@@begin documentation
//@@end

public class CRSAPArchiveCust
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(CRSAPArchiveCust.class);

  static 
  {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see it.alcantara.crsap.wdp.IPrivateCRSAPArchiveCust for more details
   */
  private final IPrivateCRSAPArchiveCust wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see it.alcantara.crsap.wdp.IPrivateCRSAPArchiveCust.IContextNode for more details.
   */
  private final IPrivateCRSAPArchiveCust.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public CRSAPArchiveCust(IPrivateCRSAPArchiveCust wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
  /** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
    //$$begin Service Controller(1623252611)
    wdContext.nodeZcrsap_Create_Input().bind(new Zcrsap_Create_Input());
	wdContext.currentZcrsap_Create_InputElement().modelObject().setCr_Attributes( new Zcrsap_Attributes());
	
    
    //$$end
    //@@end
  }

  //@@begin javadoc:wdDoExit()
  /** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:executeZcrsap_Create_Input()
  /** Declared method. */
  //@@end
  public short executeZcrsap_Create_Input( )
  {
    //@@begin executeZcrsap_Create_Input()
    //$$begin Service Controller(-932645630)
    IWDMessageManager manager = wdComponentAPI.getMessageManager();
    short success = 0;
	short error = 1;
    // To reflush old date create a new input Element.
	wdContext.nodeZcrsap_Create_Input().bind((Collection)null);
	wdContext.nodeZcrsap_Create_Input().bind(new Zcrsap_Create_Input());
	wdContext.currentZcrsap_Create_InputElement().modelObject().setCr_Attributes( new Zcrsap_Attributes());

    // Retrieve Current Zcrsap_Create_Input Element.
    // It's root context node for RFC Calling.
	crsapCreateInput = wdContext.currentZcrsap_Create_InputElement().modelObject();
    
	try
	{
	    // Fill RFC Import Structure Fields
	    retrieveCRSapAttributes();
	    retrieveGruppoItUsers();
	    retrieveRichiedentiUsers();
		retrieveApprovatoriUsers();
		retrieveApprovatoriFinaliUsers();
		retrieveHistoryAction();
		
		crsapCreateInput.execute();
      	//wdContext.currentZcrsap_Create_InputElement().modelObject().execute();
      	wdContext.nodeOutput().invalidate();
      	
		byte[] crsapId = wdContext.currentOutputElement().getReq_Id();
		
		if (crsapId == null || crsapId.length != 16)
		{
			manager.reportException("Impossibile salvare la CR nel Back End", false);
			return error;
		}
    }
    catch(WDDynamicRFCExecuteException e)
    {
      manager.reportException(e.getMessage(), false);
      return error;
    }
	catch(ParseException e)
	{
	  manager.reportException(e.getMessage(), false);
	  return error;
	}
    
    return success;
    
    //$$end
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  
  
  	private Zcrsap_Create_Input crsapCreateInput = null;
	
  	/************************************
  	** Retrieve Richiedenti Users from 
  	** ReaderList Local Context
  	** And fill Richiedenti Context Fill
  	** in RFC Input Context.
  	************************************/
	private void retrieveRichiedentiUsers()
		throws NullPointerException
	{
		IReaderListNode readerNode = wdContext.nodeReaderList();
		//Zcrsap_Create_Input inputElement = wdContext.currentZcrsap_Create_InputElement().modelObject();
		
		for ( int k = 0; k < readerNode.size(); k++)
		{
			IReaderListElement readerElement = readerNode.getReaderListElementAt(k);
			String readerLogonID = readerElement.getReaderID();
			
			Zstr_User_Id richiedente = new Zstr_User_Id();
			richiedente.setUser_Id(readerLogonID);
			crsapCreateInput.addRichiedenti(richiedente);
		}
	}
  
	/************************************
	** Retrieve Approvatori Users from 
	** ApprovalList Local Context
	** And fill Approvatori Context Fill
	** in RFC Input Context.
	************************************/
  	private void retrieveApprovatoriUsers()
		throws NullPointerException
  	{
  		IApprovalListNode approvalNode = wdContext.nodeApprovalList();
  		
  		for ( int k = 0; k < approvalNode.size(); k++ )
  		{
			IApprovalListElement approvalListElement = approvalNode.getApprovalListElementAt(k);
			String approvalID = approvalListElement.getApprovalID();
			
			Zstr_User_Id approvatore = new Zstr_User_Id();
			approvatore.setUser_Id(approvalID);
			crsapCreateInput.addApprovatori(approvatore);
  		}
  	}
  	
	/************************************
	** Retrieve Approvatori Finali Users from 
	** AccettazioneList Local Context
	** And fill Approvatori Finali Context Fill
	** in RFC Input Context.
	************************************/
	private void retrieveApprovatoriFinaliUsers()
		throws NullPointerException
	{
		IAccettazioneListNode accettazioneNode = wdContext.nodeAccettazioneList();
  		
		for ( int k = 0; k < accettazioneNode.size(); k++ )
		{
			IAccettazioneListElement accettazioneListElement = accettazioneNode.getAccettazioneListElementAt(k);
			String accettazioneID = accettazioneListElement.getAccettazioneID();
			
			Zstr_User_Id accettatore = new Zstr_User_Id();
			accettatore.setUser_Id(accettazioneID);
			crsapCreateInput.addApprovatori_Finali(accettatore);
		}
	}
	
	/************************************
	** Retrieve It Groups Users from 
	** ItList Local Context
	** And fill Gruppo IT Context Fill
	** in RFC Input Context.
	************************************/  	
	private void retrieveGruppoItUsers()
		throws NullPointerException
	{
		/******************************************
		** Template To Load Data

		Zstr_User_Id user1 = new Zstr_User_Id();
		user1.setUser_Id("User1-Lamb");
	
		Zstr_User_Id user2 = new Zstr_User_Id();
		user2.setUser_Id("USer2-Lamb");
	 
		wdContext.currentZcrsap_Create_InputElement().modelObject().addApprovatori( user1);
		wdContext.currentZcrsap_Create_InputElement().modelObject().addApprovatori( user2);
		*****************************************/
		IITListNode itListNode = wdContext.nodeITList();
  		
		for ( int k = 0; k < itListNode.size(); k++ )
		{
			IITListElement itListElement = itListNode.getITListElementAt(k);
			String itID = itListElement.getITID();
			
			Zstr_User_Id itUser = new Zstr_User_Id();
			itUser.setUser_Id(itID);
			crsapCreateInput.addGrupppo_It(itUser);
		}
	}
  	
	private void retrieveCRSapAttributes()
	{
		ICRAttributesElement contextCRAttributes =
	  		wdContext.currentCRAttributesElement();
	  		
		ICr_AttributesElement rfcCRAttributes = 
			wdContext.currentCr_AttributesElement();
		
		String alternativaSviluppo = contextCRAttributes.getAlternativaSviluppo();
		String analisiCosti = contextCRAttributes.getAnalisiCosti();
		String codiceCR = contextCRAttributes.getCodiceCR();
		float costoTotale = contextCRAttributes.getCostoTotale();
		short crStatus = contextCRAttributes.getCRStatus();
		Date dataRichiesta = contextCRAttributes.getDataRichiesta();
		String descrizione = contextCRAttributes.getDescrizione();
		String impatto = contextCRAttributes.getImpatto();
		String priorita = contextCRAttributes.getPriorita();
		String responsabileSior = contextCRAttributes.getResponsabileSIOR();
		String richiedente = contextCRAttributes.getRichiedente();
		String riferimentoTicket = contextCRAttributes.getRiferimentoTicket();
		String risorseAssegnate = contextCRAttributes.getRisorseAssegnate();
		String societaConsulenza = contextCRAttributes.getSocietaConsulenza();
		String titolo = contextCRAttributes.getTitolo();
		int totaleGiornate = contextCRAttributes.getTotaleGiornateSviluppo();
		String soluzioneProposta = contextCRAttributes.getSoluzioneProposta();
		String soluzioneImplementata = contextCRAttributes.getSoluzioneImplementata();
		
		// Set Attribute 
		if ( crStatus == 0 )
			crStatus = 2;
			
		rfcCRAttributes.setAlternativasvil(alternativaSviluppo);
		rfcCRAttributes.setAnalisicosti(analisiCosti);
		rfcCRAttributes.setCodicecr(codiceCR);
		rfcCRAttributes.setCostototale(costoTotale);
		rfcCRAttributes.setTot_Gior_Svil(totaleGiornate);
		rfcCRAttributes.setCrstatus(crStatus);
		rfcCRAttributes.setDatarichiesta(new java.sql.Date(dataRichiesta.getTime()));
		rfcCRAttributes.setDescrizione(descrizione);
		rfcCRAttributes.setImpatto(impatto);
		rfcCRAttributes.setPriorita(priorita);
		rfcCRAttributes.setResponsabilesior(responsabileSior);
		rfcCRAttributes.setRichiedente(richiedente);
		rfcCRAttributes.setRif_Ticket(riferimentoTicket);
		rfcCRAttributes.setRisorseassegnate(risorseAssegnate);
		rfcCRAttributes.setSocieta_Cons(societaConsulenza);
		rfcCRAttributes.setSol_Proposta(soluzioneProposta);
		rfcCRAttributes.setTitolo(titolo);
		rfcCRAttributes.setSoluzioneimp(soluzioneImplementata);
  	
	}
    
	private void retrieveHistoryAction()
		throws NullPointerException, ParseException
	{
		IActionPerformedNode actionPerformedNode = 
			wdContext.nodeActionPerformed();
				  	
		for ( int k = 0; k < actionPerformedNode.size(); k++ )
		{
			IActionPerformedElement actionPerformedElement = 
				actionPerformedNode.getActionPerformedElementAt(k);
				
			String action = actionPerformedElement.getAction();
		
			DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy");
			String dateString = actionPerformedElement.getActionDate();
			Date date = df.parse(dateString);
			String owner = actionPerformedElement.getActionOwner();
				
			Zcrsap_History crsapHistory = new Zcrsap_History();
			crsapHistory.setAction_Desc(action);
			crsapHistory.setUser_Id(owner);
			crsapHistory.setData( new java.sql.Date(date.getTime()));
				
			crsapCreateInput.addHistory_Action(crsapHistory);	
		}
	}
  
  //@@end
}
