// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateChangeRequestSAP).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import it.alcantara.crsap.wdp.IPrivateChangeRequestSAP;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAP.IActionPerformedElement;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAP.IActionPerformedNode;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAP.ICRAttributesElement;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAP.IControllerlLoadImgElement;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAP.IITListElement;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAP.ILocalUserListElement;
import it.alcantara.crsap.wdp.IPublicChangeRequestSAP.ILocalUserListNode;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;

import com.sap.caf.eu.gp.context.api.GPContextFactory;
import com.sap.caf.eu.gp.context.api.IGPUserContext;
import com.sap.caf.eu.gp.exception.api.GPInvocationException;
import com.sap.caf.eu.gp.process.api.GPProcessFactory;
import com.sap.caf.eu.gp.process.api.IGPProcess;
import com.sap.caf.eu.gp.process.api.IGPProcessInstance;
import com.sap.caf.eu.gp.process.api.IGPProcessRoleInstance;
import com.sap.caf.eu.gp.process.api.IGPRoleInfo;
import com.sap.caf.eu.gp.process.rt.api.IGPProcessRoleInstanceList;
import com.sap.caf.eu.gp.process.rt.api.IGPRuntimeManager;
import com.sap.caf.eu.gp.structure.api.GPStructureFactory;
import com.sap.caf.eu.gp.structure.api.IGPAttributeInfo;
import com.sap.caf.eu.gp.structure.api.IGPStructure;
import com.sap.caf.eu.gp.structure.api.IGPStructureInfo;
import com.sap.security.api.IGroup;
import com.sap.security.api.IGroupFactory;
import com.sap.security.api.IGroupSearchFilter;
import com.sap.security.api.ISearchAttribute;
import com.sap.security.api.ISearchResult;
import com.sap.security.api.IUser;
import com.sap.security.api.IUserAccount;
import com.sap.security.api.IUserFactory;
import com.sap.security.api.IUserSearchFilter;
import com.sap.security.api.UMFactory;
import com.sap.tc.webdynpro.progmodel.api.IWDMessageManager;
import com.sap.tc.webdynpro.progmodel.api.IWDNode;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeElement;
import com.sap.tc.webdynpro.progmodel.api.WDResourceFactory;
import com.sap.tc.webdynpro.services.sal.datatransport.api.IWDResource;
import com.sap.tc.webdynpro.services.sal.um.api.IWDClientUser;
import com.sap.tc.webdynpro.services.sal.um.api.WDClientUser;
import com.sap.tc.webdynpro.services.sal.url.api.WDWebResourceType;
import com.sapportals.wcm.repository.IResource;
import com.sapportals.wcm.repository.IResourceFactory;
import com.sapportals.wcm.repository.ResourceContext;
import com.sapportals.wcm.repository.ResourceFactory;
import com.sapportals.wcm.util.uri.RID;
import com.sapportals.wcm.util.usermanagement.WPUMFactory;

//@@end

//@@begin documentation
//@@end

public class ChangeRequestSAP
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(ChangeRequestSAP.class);

  static 
  {
    //@@begin id
		String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see it.alcantara.crsap.wdp.IPrivateChangeRequestSAP for more details
   */
  private final IPrivateChangeRequestSAP wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see it.alcantara.crsap.wdp.IPrivateChangeRequestSAP.IContextNode for more details.
   */
  private final IPrivateChangeRequestSAP.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public ChangeRequestSAP(IPrivateChangeRequestSAP wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
	/** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()

		messageManager = wdComponentAPI.getMessageManager();

		/**************************
		** Load KM Values
		**************************/
		int returnValue = 0;
		manageKM = new ManageKM(KM_FILE_NAME);
		manageKM.init();

		/**************************
		** Retrieve Current user
		** logonID
		**************************/
		try {
			IResourceFactory factory = ResourceFactory.getInstance();
			IUser user = WDClientUser.getCurrentUser().getSAPUser();

			IUserAccount[] userAccounts = user.getUserAccounts();
			currentUserLogonID = userAccounts[0].getLogonUid();
		} catch (Exception e) {
			messageManager.reportException(
				"ChangeRequestSAP::wdDoInit Unable to " + "find Current logon ID. Exception = " + e,
				false);
		}
    //@@end
  }

  //@@begin javadoc:wdDoExit()
	/** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoPostProcessing()
	/**
	 * Hook called to handle data retrieval errors before rendering.
	 *
	 * After doModifyView(), the Web Dynpro Framework gets all context data needed
	 * for rendering by validating the contexts (which in turn calls the supply
	 * functions and supplying relation roles). In this hook, the application
	 * should handle the errors which occurred during validation of the contexts.
	 * 
	 * Using preorder depth-first traversal, this hook is called for all component
	 * controllers starting with the current root component.
	 *
	 * Permitted operations:
	 * - Flushing model queue
	 * - Creating messages
	 * - Reading context and model data
	 *
	 * Forbidden operations: 
	 * - Invalidating model data
	 * - Manipulating the context
	 * - Firing outbound plugs
	 * - Creating components
	 * - ...   
	 *
	 * @param isCurrentRoot true if this is the root of the current request
	 */
  //@@end
  public void wdDoPostProcessing(boolean isCurrentRoot)
  {
    //@@begin wdDoPostProcessing()
    //@@end
  }

  //@@begin javadoc:wdDoBeforeNavigation()
	/**
	 * Hook before the navigation phase starts.
	 *
	 * This hook allows you to flush the model queue and handle any
	 * errors that occur. Firing outbound plugs is allowed in this hook.
	 *
	 * Using preorder depth-first traversal, this hook is called for all component
	 * controllers starting with the current root component.
	 *
	 * @param isCurrentRoot true if this is the root of the current request
	 */
  //@@end
  public void wdDoBeforeNavigation(boolean isCurrentRoot)
  {
    //@@begin wdDoBeforeNavigation()
    //@@end
  }
  
  //@@begin javadoc:wdDoApplicationStateChange()
	/**
	 * Hook that informs the application about a state change.
	 * <p>
	 * This hook is called e.g. to tell the application that will be
	 * <ul>
	 *  <li>left via a suspend plug and therefore should go into a suspend/sleep
	 *      mode with minimal need of resources. errors that occur. Firing 
	 *      outbound plugs is allowed in this hook.
	 *  <li>left due to a timeout and could write it's state to a data base if the 
	 *      user comes back later on
	 * </ul>
	 *
	 * The concrete reason is available via IWDApplicationStateChangeInfo
	 * <p>
	 * <b>Important</b>: This hook is called for the top level component only!
	 *
	 * @param stateChangeInfo contains the information about the nature of the state change
	 * @param stateChangeReturn allows the application to ask for a different state change. 
	 *        The framework is allowed to ignore it considering i.e. the current resources situation.
	 */
  //@@end
  public void wdDoApplicationStateChange(com.sap.tc.webdynpro.progmodel.api.IWDApplicationStateChangeInfo stateChangeInfo, com.sap.tc.webdynpro.progmodel.api.IWDApplicationStateChangeReturn stateChangeReturn)
  {
    //@@begin wdDoApplicationStateChange()
    //@@end
  }

  //@@begin javadoc:complete()
	/** Declared method. */
  //@@end
  public void complete( )
  {
    //@@begin complete()

		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		/*
			if ( wdContext.currentCRAttributesElement().getCRStatus() == (short)7 )
			{
				wdThis.wdGetChangeRequestSAPInterfaceController().complete();
				return;
			}
		*/

		String resultStateValue = wdContext.currentResultStateElement().getResultStateValue();

		if (resultStateValue != null && resultStateValue.equals(RESULT_STATE_RIFIUTATO)) {
			wdContext.currentCRAttributesElement().setCRStatus((short)7);
			wdThis.wdGetChangeRequestSAPInterfaceController().complete();
			return;
		}

		try {
			String currentUserID = retrieveCurrentUserID();
			updateUserList(currentUserID);
			retrieveNextStatus();
			wdThis.wdGetChangeRequestSAPInterfaceController().complete();
		} catch (Exception e) {
			manager.reportException("Exception in ChangeRequestSAP::complete " + "= " + e, false);
		}

    //@@end
  }

  //@@begin javadoc:populateLocalUserList()
	/****************************
	** Declared method. 
	** Populate LocalUserListContex
	** depending on Stauts Value.
	****************************/
  //@@end
  public void populateLocalUserList( com.sap.tc.webdynpro.progmodel.api.IWDNode contextNode, java.lang.String attributeName )
  {
    //@@begin populateLocalUserList()

		ILocalUserListNode localNode = wdContext.nodeLocalUserList();
		localNode.bind((Collection) null);

		int size = contextNode.size();

		for (int k = 0; k < size; k++) {
			IWDNodeElement element = contextNode.getElementAt(k);
			String attributeValue = (String)element.getAttributeValue(attributeName);

			ILocalUserListElement localElement = localNode.createLocalUserListElement();

			localElement.setLocalUserID(attributeValue);
			localNode.addElement(localElement);
		}

    //@@end
  }

  //@@begin javadoc:startGuidedProcedure()
	/************************************ 
	** Retrun Value. 
	** 0 <=> OK
	** 1 <=> KO
	************************************/
  //@@end
  public int startGuidedProcedure( )
  {
    //@@begin startGuidedProcedure()

		int returnValue = 0;
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		try {
			checkRequiredFields();
			String processId = manageKM.getGPProcessID();

			/**********************************
			** Set Next Status = 2
			**********************************/
			wdContext.currentCRAttributesElement().setCRStatus((short) (2));

			IWDClientUser wdUser = WDClientUser.getCurrentUser();
			IUser user = wdUser.getSAPUser();

			IGPUserContext userContext = GPContextFactory.getContextManager().createUserContext(user);

			// Retrieve Process Design.
			// From This Object it's possible
			// to retrive the GP Design Time Configuration
			IGPProcess process = GPProcessFactory.getDesigntimeManager().getActiveTemplate(processId, userContext);

			// Retrieve GP Run Time Manager
			IGPRuntimeManager runtimeManager = GPProcessFactory.getRuntimeManager();

			// Retrieve ProcessRoleInstanceList
			IGPProcessRoleInstanceList processRoleInstanceList = manageRoleList(process, runtimeManager);

			// Retrieve Input Structure Parameters
			IGPStructure igpStructure = manageInputStructure(process);

			/**************************************
			** TO TEST		
			** testRole(processRoleInstanceList);
			** printStructureInfo(igpStructure.getStructureInfo());
			** printStructure(igpStructure);
			**************************************/
			String title = "CRSAPChangeManagement: " + wdContext.currentCRAttributesElement().getCodiceCR();

			/************************************
			** Attach Manage
			************************************/

			// Start Guided Procedure
			IGPProcessInstance igpProcessInstance =
				runtimeManager.startProcess(process, title, title, user, processRoleInstanceList, igpStructure, user);

			/*mx -> Gestione attachment vecchia
			String processInstanceId = igpProcessInstance.getID();
			
			IGPRuntimeAttachmentList attachmentList = 
				runtimeManager.getAttachmentList(processInstanceId,null, user);
			
			// Get Unique ID
			String userIniqueID = user.getUniqueID();
			int size = wdContext.nodeControllerlLoadImg().size();
			
			for ( int j = 0; j < size; j ++)
			{
				IControllerlLoadImgElement element = 
					wdContext.nodeControllerlLoadImg().getControllerlLoadImgElementAt(j);
					
				byte data[] = element.getFData();
				String fileName = element.getFName();
				String displayName[] = fileName.split("\\.");
				attachmentList.add(displayName[0], fileName, IGPAttachment.TYPE_FILE, userIniqueID, data);
				//attachmentList.add(displayName[0], fileName, IGPAttachment.TYPE_URL, userIniqueID, data);
			}
			
			runtimeManager.persistAttachmentList(processInstanceId,null,attachmentList,user);
			*/
			/*OLD INIT
			if (wdContext.currentCRAttributesElement() != null && 
					wdContext.currentCRAttributesElement().getFDataImg() != null )
			{
				byte[] file = wdContext.currentControllerlLoadImgElement().getFData();
				String fileName = wdContext.currentControllerlLoadImgElement().getFName();
				String displayName[] = fileName.split("\\.");
				//messageManager.reportSuccess("Add File Name = [" + fileName + "]");
				//attachmentList.add("Esempio", "AllegatoGPTest.doc", IGPAttachment.TYPE_FILE, userIniqueID, file);
				attachmentList.add(displayName[0], fileName, IGPAttachment.TYPE_FILE, userIniqueID, file);
				runtimeManager.persistAttachmentList(processInstanceId,null,attachmentList,user);
			}
			else
			{
				//manager.reportException("File or Current Context is null", false);
				//return 0;
			}
			OLD END */

		} catch (Exception e) {
			manager.reportException("Impossibile " + "Iniziare il processo di valutazione: " + e.getMessage(), false);

			/*****************************
			** Delete The last Row inserted
			** into Action Performed Node
			******************************/
			deleteLastActionPerformed();

			//printStackTrace(e);
			returnValue = 1;
			return returnValue;
		}

		return returnValue;

    //@@end
  }

  //@@begin javadoc:loadUserListDefaultValues()
	/** Declared method. */
  //@@end
  public void loadUserListDefaultValues( )
  {
    //@@begin loadUserListDefaultValues()

		/*******************************************
		** Clean all UserList Values.
		*******************************************/
		wdContext.nodeITList().bind((Collection) null);
		wdContext.nodeAccettazioneList().bind((Collection) null);
		wdContext.nodeReaderList().bind((Collection) null);
		wdContext.nodeApprovalList().bind((Collection) null);

		/*******************************************
		** Manager It Group.
		** From ManageKM retrieves IT Group.
		** All user in the IT Group are inserted
		** into ITList.
		*******************************************/
		String groupItName = manageKM.getGroupItName();

		try {

			if (groupItName == null) {
				throw new Exception(
					"ChangeRequestSAP::loadUserListDefaultValues "
						+ "Exception = "
						+ " Unable to retrieve ITGroupName from KM!!!!");
			}

			IUserFactory userFactory = UMFactory.getUserFactory();
			IUserSearchFilter userFilter = userFactory.getUserSearchFilter();

			IGroupFactory groupFactory = UMFactory.getGroupFactory();
			IGroupSearchFilter groupSearchFilter = groupFactory.getGroupSearchFilter();
			groupSearchFilter.setDisplayName(groupItName, ISearchAttribute.LIKE_OPERATOR, false);
			ISearchResult searchResult = groupFactory.searchGroups(groupSearchFilter);

			if (searchResult.getState() == ISearchResult.SEARCH_RESULT_OK) {
				while (searchResult.hasNext()) {
					String groupID = (String)searchResult.next();
					IGroup group = groupFactory.getGroup(groupID);
					Iterator iter = group.getMembers(true);

					while (iter.hasNext()) {
						String userID = (String)iter.next();
						IUser user = userFactory.getUser(userID);
						IUserAccount[] userAccounts = user.getUserAccounts();

						if (userAccounts != null) {
							IITListElement itListElement = wdContext.nodeITList().createITListElement();

							itListElement.setITID(userAccounts[0].getLogonUid());
							wdContext.nodeITList().addElement(itListElement);

						}

					}
				}
			}

		} catch (Exception e) {
			messageManager.reportException("ChangeRequestSAP::loadUserListDefaultValues " + "Exception = " + e, false);
		}

    //@@end
  }

  //@@begin javadoc:getGPProcessID()
	/** Declared method. */
  //@@end
  public java.lang.String getGPProcessID( )
  {
    //@@begin getGPProcessID()

		return manageKM.getGPProcessID();

    //@@end
  }

  //@@begin javadoc:getGroupItName()
	/** Declared method. */
  //@@end
  public java.lang.String getGroupItName( )
  {
    //@@begin getGroupItName()

		return manageKM.getGroupItName();

    //@@end
  }

  //@@begin javadoc:getSapModules()
	/** Declared method. */
  //@@end
  public java.util.HashSet getSapModules( )
  {
    //@@begin getSapModules()

		return manageKM.getSapModules();

    //@@end
  }

  //@@begin javadoc:getPriority()
	/** Declared method. */
  //@@end
  public java.util.HashSet getPriority( )
  {
    //@@begin getPriority()
		return manageKM.getPriority();
    //@@end
  }

  //@@begin javadoc:getCurrentUserLogonID()
	/** Declared method. */
  //@@end
  public java.lang.String getCurrentUserLogonID( )
  {
    //@@begin getCurrentUserLogonID()
		return currentUserLogonID;
    //@@end
  }

  //@@begin javadoc:UpdateActionPerformed()
	/******************************* 
	** Method used to Update 
	** ActionPermormed Table.
	** Input = actionMsg = Action Performed Name
	*******************************/
  //@@end
  public void UpdateActionPerformed( java.lang.String actionMsg )
  {
    //@@begin UpdateActionPerformed()

		Date date = new Date();

		IActionPerformedElement actionPerformedElement = wdContext.nodeActionPerformed().createActionPerformedElement();

		actionPerformedElement.setActionDate(date.toString());
		actionPerformedElement.setActionOwner(currentUserLogonID);
		actionPerformedElement.setAction(actionMsg);

		wdContext.nodeActionPerformed().addElement(actionPerformedElement);

    //@@end
  }

  //@@begin javadoc:getActionPerformedMsg()
	/*********************************************
	** Retrieve Message for Action Performed 
	** StatusCod: Status of GP.
	** actionType: true if approval false otherwise
	*********************************************/
  //@@end
  public java.lang.String getActionPerformedMsg( short statusCod, boolean actionType )
  {
    //@@begin getActionPerformedMsg()

		switch (statusCod) {
			case 0 :
				if (actionType == true)
					return "Inserimento nuova Change Request";
				else
					return "";
			case 2 :
				if (actionType == true)
					return "Change Request Approvata dal Revisore";
				else
					return "Change Request Rifiutata dal Revisore";
			case 3 :
				if (actionType == true)
					return "Change Request Approvata dall'Approvatore";
				else
					return "Change Request Rifiutata dall'Approvatore";

			case 4 :
				if (actionType == true)
					return "Change Request Approvata dall'Accettatore";
				else
					return "Change Request Rifiutata dall'Accettatore";
			case 5 :
				if (actionType == true)
					return "Change Request Avanzata in fase di TEST";
				else
					return "";
			case 6 :
				if (actionType == true)
					return "Change Request Avanzata in Produzione";
				else
					return "";
			case 7 :
				if (actionType == true)
					return "Change Request Modificata";
				else
					return "";
			default :
				return "Caso non gestito ......";
		}

    //@@end
  }

  //@@begin javadoc:getNextCounter()
	/** Declared method. */
  //@@end
  public java.lang.String getNextCounter( )
  {
    //@@begin getNextCounter()
		//mx -> Chiamata a RFC che ritorna progressivo
		wdThis.wdGetCRSAPCounterCustController().executeZcrsap_Counter_Get_Next_Input();
		if (wdContext.currentOutputElement() != null) {
			return wdContext.currentOutputElement().getE_Zcrsap_Cnt();
		} else {
			//mx -> Caso che non dovrebbe presentarsi...
			return "CNT_PROB";
		}
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others

	/***************************
	** Private Fields
	****************************/
	private ManageKM manageKM;
	private IWDMessageManager messageManager;
	private String currentUserLogonID;

	/***************************
	** Private Methods
	****************************/

	private void deleteLastActionPerformed() {
		IActionPerformedNode actionPerformedNode = wdContext.nodeActionPerformed();

		int size = actionPerformedNode.size();

		IActionPerformedElement actionPerformedElement = actionPerformedNode.getActionPerformedElementAt(size - 1);

		if (actionPerformedElement != null) {
			actionPerformedNode.removeElement(actionPerformedElement);
		}

	}

	private void printStructure(IGPStructure igpStructure) {
		IGPStructureInfo igpStructureInfo = igpStructure.getStructureInfo();

		messageManager.reportSuccess(
			"call printStructure for IGPStructure = [ " + igpStructureInfo.getTechName() + "]");

		Collection subStructuresInfo = igpStructureInfo.getStructures();
		Iterator iter = subStructuresInfo.iterator();

		while (iter.hasNext()) {
			IGPStructureInfo subStructureInfo = (IGPStructureInfo)iter.next();

			int multiplicity = subStructureInfo.getMultiplicity();
			String subStructureName = subStructureInfo.getTechName();

			messageManager.reportSuccess(
				"Structure subStructureName " + subStructureName + "with multiplicity = [" + multiplicity + "]");

			//if (multiplicity == IGPStructureInfo.MULITIPLICITY_0_N || 
			//	multiplicity == IGPStructureInfo.MULITIPLICITY_1_N )
			if (subStructureName.equals("InputOutputReaderComments.4")
				|| subStructureName.equals("InputOutputActionPerformed.3")) {
				try {

					//messageManager.reportSuccess("Find all SubStructure = [" + subStructureName + 
					//	"] Instance for Structure = [" + igpStructureInfo.getTechName() + "]");

					Collection subStructure = igpStructure.getStructures(subStructureName);

					Iterator subStructureIter = subStructure.iterator();

					while (subStructureIter.hasNext()) {
						IGPStructure subStructureItem = (IGPStructure)subStructureIter.next();
						printStructure(subStructureItem);
					}
				} catch (Exception exception) {
					messageManager.reportException("printStructure Exception = " + exception, false);
				}

			} else {
				try {
					//messageManager.reportSuccess("Find SubStructure = [" + subStructureName + 
					//	"] from Structure = [" + igpStructureInfo.getTechName() + "]");

					printStructure(igpStructure.getStructure(subStructureName));
				} catch (Exception exception) {
					messageManager.reportException("printStructure Exception = " + exception, false);
				}

			}

		}

		Collection attributes = igpStructureInfo.getAttributes();
		Iterator iterator = attributes.iterator();

		//messageManager.reportSuccess(" Number of Attributes for IGPStructure = [ " + 
		//	igpStructureInfo.getTechName() + "] = [" + attributes.size() + "]");

		while (iterator.hasNext()) {
			IGPAttributeInfo igpAttributeInfo = (IGPAttributeInfo)iterator.next();
			String attributeName = igpAttributeInfo.getTechName();
			int attributeType = igpAttributeInfo.getType();
			printAttribute(igpStructure, attributeName, attributeType);
		}

	}

	private void testRole(IGPProcessRoleInstanceList processRoleInstanceList) {
		Enumeration enumeration = processRoleInstanceList.getElements();

		while (enumeration.hasMoreElements()) {
			IGPProcessRoleInstance roleInstance = (IGPProcessRoleInstance)enumeration.nextElement();
			//messageManager.reportSuccess("Role Instance = " + roleInstance.toString());

			Enumeration users = roleInstance.getUsers();

			while (users.hasMoreElements()) {
				IUser user_local = (IUser)users.nextElement();
				//messageManager.reportSuccess("User = " + user_local.getUid());
			}
		}

	}

	private void printAttribute(IGPStructure igpStructure, String attributeName, int attributeType) {
		//messageManager.reportSuccess("Print Attribute = [" + attributeName 
		//	+ "] for Structure = [" + 
		//	igpStructure.getStructureInfo().getTechName() + "]");
		try {
			switch (attributeType) {
				case IGPAttributeInfo.BASE_BOOLEAN :
					boolean attributeValue = igpStructure.getAttributeAsBoolean(attributeName);
					//messageManager.reportSuccess("Attribute " + attributeName + 
					//	" Value = [" + attributeValue + "]");
					break;
				case IGPAttributeInfo.BASE_DATE :
					Date attributeDateValue = igpStructure.getAttributeAsDate(attributeName);
					//messageManager.reportSuccess("Attribute " + attributeName + 
					//	" Value = [" + attributeDateValue + "]");
					break;
				case IGPAttributeInfo.BASE_DECIMAL :
					BigDecimal attributeDecimalValue = igpStructure.getAttributeAsBigDecimal(attributeName);
					//messageManager.reportSuccess("Attribute " + attributeName + 
					//" Value = [" + attributeDecimalValue + "]");
					break;
				case IGPAttributeInfo.BASE_DOUBLE :
					double attributedoubleValue = igpStructure.getAttributeAsDouble(attributeName);
					//messageManager.reportSuccess("Attribute " + attributeName + 
					//	" Value = [" + attributedoubleValue + "]");
					break;
				case IGPAttributeInfo.BASE_FLOAT :
					double attributedoubleValue2 = igpStructure.getAttributeAsDouble(attributeName);
					//messageManager.reportSuccess("Attribute " + attributeName + 
					//	" Value = [" + attributedoubleValue2  + "]");
					break;
				case IGPAttributeInfo.BASE_SIGNED_INT :
					int attributeIntValue = igpStructure.getAttributeAsInt(attributeName);
					//messageManager.reportSuccess("Attribute " + attributeName + 
					//	" Value = [" + attributeIntValue  + "]");
					break;
				case IGPAttributeInfo.BASE_STRING :
					String attributeStringValue = igpStructure.getAttributeAsString(attributeName);
					//messageManager.reportSuccess("Attribute " + attributeName + 
					//	" Value = [" + attributeStringValue + "]");
					break;
				default :
					messageManager.reportException(
						"printAtribute " + "Unknown info Type = [" + attributeType + "]",
						false);
			}
		} catch (Exception exception) {
			messageManager.reportException("printAttribute Exception = " + exception, false);
		}

	}

	/********************************
	** Check if all requiered fields
	** are set.
	** RoleGruop.
	*********************************/
	private void checkRequiredFields() throws Exception {
		int accettazioneSize = wdContext.nodeAccettazioneList().size();
		int approvalSize = wdContext.nodeApprovalList().size();
		int itSize = wdContext.nodeITList().size();
		int readerSize = wdContext.nodeReaderList().size();

		if (accettazioneSize == 0 || approvalSize == 0 || itSize == 0 || readerSize == 0) {
			throw new Exception("Si deve assegnare almeno un utente a " + "tutti i ruoli specificati");
		}

		String codiceCR = wdContext.currentCRAttributesElement().getCodiceCR();

		if (codiceCR == null || codiceCR.length() == 0) {
			throw new Exception("Si deve selezionare un modulo SAP");
		}

	}

	private IGPStructure manageInputStructure(IGPProcess process) throws Exception {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		IGPStructureInfo structureInfo = null;
		IGPStructure igpStructure = null;

		structureInfo = process.getInputParameters();
		igpStructure = GPStructureFactory.getStructure(structureInfo);

		manageStructure(igpStructure);

		return igpStructure;

	}

	/********************************
		** Define Input Structure
		** with context values
		*********************************/
	private void manageStructure(IGPStructure igpStructure) throws Exception {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		IGPStructureInfo structureInfo = igpStructure.getStructureInfo();

		String structureInfoTechnicalName = structureInfo.getTechName();
		String structureInfoName = retrieveInfoName(structureInfoTechnicalName);

		//manager.reportSuccess("manageStructure structureInfoTechnicalName = " +
		//	"[" + structureInfoTechnicalName + "]" );

		/****************************************
		** Retrieve Attributes From root
		** InputParameter of GP Process
		****************************************/
		Collection attributes = structureInfo.getAttributes();
		Iterator attributesIter = attributes.iterator();

		while (attributesIter.hasNext()) {
			IGPAttributeInfo attributeInfo = (IGPAttributeInfo)attributesIter.next();
			String attributeInfoTechName = attributeInfo.getTechName();
			String attributeInfoName = retrieveInfoName(attributeInfoTechName);

			if (attributeInfoName.equals("crsapIDInput")) {
				byte[] crsapid = wdContext.currentResultStateElement().getCrsapID();
				igpStructure.setAttributeValue(attributeInfoTechName, crsapid);
			}
		}

		IWDNode contextNode = null;
		Collection structures = structureInfo.getStructures();

		if (structures == null || structures.size() == 0) {
			throw new Exception("Unable to Find Sub Structures for Structure Info = [" + structureInfoName + "]");
		}

		Iterator iter = structures.iterator();

		while (iter.hasNext()) {
			IGPStructureInfo subStructureInfo = (IGPStructureInfo)iter.next();

			String subStructureInfoTechnicalName = subStructureInfo.getTechName();
			String subStructureInfoName = retrieveInfoName(subStructureInfoTechnicalName);

			if (subStructureInfoName.equals("InputOuputApprovalList")) {
				//messageManager.reportSuccess("Add SubStructure = [" + subStructureInfoTechnicalName +
				//	"] to Structure = [" + igpStructure.getStructureInfo().getTechName() + "]");

				IGPStructure igpSubStructure = igpStructure.addStructure(subStructureInfoTechnicalName);

				contextNode = wdContext.nodeApprovalList();
				manageUserIGPStruct(contextNode, igpSubStructure);
			} else if (subStructureInfoName.equals("InputOutputAccettatoreList")) {
				//messageManager.reportSuccess("Add SubStructure = [" + subStructureInfoTechnicalName +
				//	"] to Structure = [" + igpStructure.getStructureInfo().getTechName() + "]");

				IGPStructure igpSubStructure = igpStructure.addStructure(subStructureInfoTechnicalName);

				contextNode = wdContext.nodeAccettazioneList();
				manageUserIGPStruct(contextNode, igpSubStructure);
			} else if (subStructureInfoName.equals("InputOuputReaderList")) {
				//messageManager.reportSuccess("Add SubStructure = [" + subStructureInfoTechnicalName +
				//	"] to Structure = [" + igpStructure.getStructureInfo().getTechName() + "]");

				IGPStructure igpSubStructure = igpStructure.addStructure(subStructureInfoTechnicalName);

				contextNode = wdContext.nodeReaderList();
				manageUserIGPStruct(contextNode, igpSubStructure);
			} else if (subStructureInfoName.equals("InputOutputITList")) {
				//messageManager.reportSuccess("Add SubStructure = [" + subStructureInfoTechnicalName +
				//	"] to Structure = [" + igpStructure.getStructureInfo().getTechName() + "]");

				IGPStructure igpSubStructure = igpStructure.addStructure(subStructureInfoTechnicalName);

				contextNode = wdContext.nodeITList();
				manageUserIGPStruct(contextNode, igpSubStructure);
			} else if (subStructureInfoName.equals("InputUpdateCRAttributes")) {
				//messageManager.reportSuccess("Add SubStructure = [" + subStructureInfoTechnicalName +
				//	"] to Structure = [" + igpStructure.getStructureInfo().getTechName() + "]");

				IGPStructure igpSubStructure = igpStructure.addStructure(subStructureInfoTechnicalName);

				contextNode = wdContext.nodeCRAttributes();
				manageCRAttributes(contextNode, igpSubStructure);
			} else if (subStructureInfoName.equals("InputOutputActionPerformed")) {
				contextNode = wdContext.nodeActionPerformed();
				manageStructureList(igpStructure, subStructureInfo, contextNode);
			} else if (subStructureInfoName.equals("InputOutputReaderComments")) {
				contextNode = wdContext.nodeReaderComments();
				manageStructureList(igpStructure, subStructureInfo, contextNode);

			} else if (subStructureInfoName.equals("SendMailInputStructure")) {
				IGPStructure sendMailStructure = igpStructure.addStructure("SendMailInputStructure");
				IGPStructureInfo sendMailStructureInfo = sendMailStructure.getStructureInfo();
				manageSendMailStructure(sendMailStructure, sendMailStructureInfo);
				/*
				sendMailStrucuture.setAttributeValue("SmtpServer", "10.4.0.41");
				sendMailStrucuture.setAttributeValue("SmtpPort", 25);
				sendMailStrucuture.setAttributeValue("SmtpBody", "Body");
				sendMailStrucuture.setAttributeValue("SmtpObject", "Oggetto");
				*/
			} else {
				messageManager.reportWarning(subStructureInfoName + "not Found in Context ...");
				/*
				throw new Exception("ChangeRequestSAP::retrieveContextNode " +
					" Invalid IGP Info Structure Name = [" + subStructureInfoName + "]");
				*/
			}

		}

	}

	private void manageSendMailStructure(IGPStructure sendMailStructure, IGPStructureInfo sendMailStructureInfo)
		throws GPInvocationException {

		/************************************
		** Retrive CR Attributes
		** to set in Body Mail
		*************************************/
		ICRAttributesElement attributesElement = wdContext.currentCRAttributesElement();

		String titolo = (attributesElement.getTitolo() != null) ? attributesElement.getTitolo() : "";
		String codiceCR = (attributesElement.getCodiceCR() != null) ? attributesElement.getCodiceCR() : "";
		String richiedente = (attributesElement.getRichiedente() != null) ? attributesElement.getRichiedente() : "";
		String analisiCosti = (attributesElement.getAnalisiCosti() != null) ? attributesElement.getAnalisiCosti() : "";
		float costoTotale = attributesElement.getCostoTotale();
		Date dataRichiesta = attributesElement.getDataRichiesta();
		String descrizione = (attributesElement.getDescrizione() != null) ? attributesElement.getDescrizione() : "";
		String impatto = (attributesElement.getImpatto() != null) ? attributesElement.getImpatto() : "";
		String priorita = (attributesElement.getPriorita() != null) ? attributesElement.getPriorita() : "";

		String responsabileSior =
			(attributesElement.getResponsabileSIOR() != null) ? attributesElement.getResponsabileSIOR() : "";

		String riferimentoTicket =
			(attributesElement.getRiferimentoTicket() != null) ? attributesElement.getRiferimentoTicket() : "";

		String risorseAssegnate =
			(attributesElement.getRisorseAssegnate() != null) ? attributesElement.getRisorseAssegnate() : "";

		String societaConsulenza =
			(attributesElement.getSocietaConsulenza() != null) ? attributesElement.getSocietaConsulenza() : "";

		String soluzioneImplementata =
			(attributesElement.getSoluzioneImplementata() != null) ? attributesElement.getSoluzioneImplementata() : "";

		String soluzioneProposta =
			(attributesElement.getSoluzioneProposta() != null) ? attributesElement.getSoluzioneProposta() : "";

		try {
			IWDClientUser wdUser = WDClientUser.getCurrentUser();
			IUser user = wdUser.getSAPUser();
			richiedente = user.getDisplayName();
		} catch (Exception e) {}

		Collection attibutesInfoSet = sendMailStructureInfo.getAttributes();
		Iterator iter = attibutesInfoSet.iterator();

		while (iter.hasNext()) {
			IGPAttributeInfo igpAttributeInfo = (IGPAttributeInfo)iter.next();
			String attributeTechnicalName = igpAttributeInfo.getTechName();
			String attributeName = retriveAttributeName(attributeTechnicalName);

			if (attributeName.equals("SmtpPort")) {
				sendMailStructure.setAttributeValue(attributeTechnicalName, 25);
			} else if (attributeName.equals("SmtpBody")) {
				//sendMailStructure.setAttributeValue(attributeTechnicalName, "E' presente un nuovo Task a suo Carico nella sua Intranet");
				sendMailStructure.setAttributeValue(
					attributeTechnicalName,
					"RICHIESTA CRSAP \n\n"
						+ "E' presente una nuova richiesta di CR SAP da autorizzare nella sua "
						+ "\n"
						+ "INTRANET. \n\n"
						+ "Titolo: "
						+ titolo
						+ "\n"
						+ "CodiceCR: "
						+ codiceCR
						+ "\n"
						+ "Richiedente: "
						+ richiedente
						+ "\n"
						+ "AnalisiCosti: "
						+ analisiCosti
						+ "\n"
						+ "Costo Totale:"
						+ costoTotale
						+ " €\n"
						+ "Data Richiesta: "
						+ dataRichiesta
						+ "\n"
						+ "Descrizione: "
						+ descrizione
						+ "\n"
						+ "Impatto: "
						+ impatto
						+ "\n"
						+ "Priorità: "
						+ priorita
						+ "\n"
						+ "Responsabile Sior: "
						+ responsabileSior
						+ "\n"
						+ "Riferimento Ticket: "
						+ riferimentoTicket
						+ "\n"
						+ "Risorse Assegnate: "
						+ risorseAssegnate
						+ "\n"
						+ "Società di Consulenza: "
						+ societaConsulenza
						+ "\n"
						+ "Soluzione Implementata: "
						+ soluzioneImplementata
						+ "\n"
						+ "Soluzione Proposta: "
						+ soluzioneProposta
						+ "\n"
						+ "\n\n"
						+ "Distinti saluti");
			} else if (attributeName.equals("SmtpServer")) {
				sendMailStructure.setAttributeValue(attributeTechnicalName, "10.4.0.41");
			} else if (attributeName.equals("SmtpObject")) {
				sendMailStructure.setAttributeValue(attributeTechnicalName, "Approvazione CR SAP");
			} else {
				messageManager.reportWarning(
					"ChangeRequestSAP::manageSendMailStructure Attribute = " + attributeName + " not managed!!!!!");
			}

		}

	}

	private void manageStructureList(
		IGPStructure igpStructure,
		IGPStructureInfo subStructureInfo,
		IWDNode contextNode)
		throws Exception {

		int size = contextNode.size();

		for (int k = 0; k < size; k++) {
			String subStructureInfoTechnicalName = subStructureInfo.getTechName();
			String subStructureInfoName = retrieveInfoName(subStructureInfoTechnicalName);
			IWDNodeElement nodeElement = contextNode.getElementAt(k);
			IGPStructure igpSubStructure = igpStructure.addStructure(subStructureInfoTechnicalName);
			setIGPAttribute(igpSubStructure, subStructureInfo, nodeElement);
		}
	}

	private void setIGPAttribute(
		IGPStructure igpStructure,
		IGPStructureInfo igpStructureInfo,
		IWDNodeElement nodeElement)
		throws Exception {
		Collection collection = igpStructureInfo.getAttributes();
		Iterator iter = collection.iterator();

		while (iter.hasNext()) {
			IGPAttributeInfo igpAttributeInfo = (IGPAttributeInfo)iter.next();
			int attributeInfoType = igpAttributeInfo.getType();
			String attributeTechnicalName = igpAttributeInfo.getTechName();
			String attributeName = retriveAttributeName(attributeTechnicalName);

			Object attributeValue = nodeElement.getAttributeValue(attributeName);

			switch (attributeInfoType) {
				case IGPAttributeInfo.BASE_DATE :
					Date date_value = (Date)attributeValue;
					igpStructure.setAttributeValue(attributeTechnicalName, date_value);
					break;
				case IGPAttributeInfo.BASE_STRING :
					String str_value = (String)attributeValue;
					igpStructure.setAttributeValue(attributeTechnicalName, str_value);
					break;
				case IGPAttributeInfo.BASE_FLOAT :
					Float float_value = (Float)attributeValue;
					igpStructure.setAttributeValue(attributeTechnicalName, float_value);
					break;
				case IGPAttributeInfo.BASE_SIGNED_SHORT :
					Short short_value = (Short)attributeValue;
					igpStructure.setAttributeValue(attributeTechnicalName, short_value);
					break;
				case IGPAttributeInfo.BASE_SIGNED_INT :
					Integer integer_value = (Integer)attributeValue;
					igpStructure.setAttributeValue(attributeTechnicalName, integer_value);
					break;
				case IGPAttributeInfo.BASE_BOOLEAN :
					Boolean boolean_value = (Boolean)attributeValue;
					igpStructure.setAttributeValue(attributeTechnicalName, boolean_value);
					break;
				case IGPAttributeInfo.BASE_BASE64BINARY :
					byte[] byte_value = (byte[])attributeValue;
					igpStructure.setAttributeValue(attributeTechnicalName, byte_value);
					break;
				default :
					throw new Exception("Invalid attributeInfoType = [" + attributeInfoType + "]");
			}

		}

	}

	private void manageCRAttributes(IWDNode attributeNode, IGPStructure igpSubStructure) throws Exception {
		//messageManager.reportSuccess("Set Attribute to [" + igpSubStructure.getStructureInfo().getTechName() +
		//	"] with Context Node = [" + attributeNode.getNodeInfo().getName() + "]");

		if (attributeNode == null) {
			throw new Exception("CRAttributes Context Node is null");
		}

		IGPStructureInfo igpSubStructureInfo = igpSubStructure.getStructureInfo();
		IWDNodeElement crAttributeElement = attributeNode.getCurrentElement();
		Collection attributes = igpSubStructureInfo.getAttributes();

		Iterator attributes_iter = attributes.iterator();

		while (attributes_iter.hasNext()) {
			IGPAttributeInfo attributeInfo = (IGPAttributeInfo)attributes_iter.next();

			int attributeInfoType = attributeInfo.getType();
			String attributeTechnicalName = attributeInfo.getTechName();
			String attributeName = retriveAttributeName(attributeTechnicalName);

			Object attributeValue = crAttributeElement.getAttributeValue(attributeName);

			switch (attributeInfoType) {
				case IGPAttributeInfo.BASE_DATE :
					Date date_value = (Date)attributeValue;
					igpSubStructure.setAttributeValue(attributeTechnicalName, date_value);
					break;
				case IGPAttributeInfo.BASE_STRING :
					String str_value = (String)attributeValue;
					igpSubStructure.setAttributeValue(attributeTechnicalName, str_value);
					break;
				case IGPAttributeInfo.BASE_FLOAT :
					Float float_value = (Float)attributeValue;
					igpSubStructure.setAttributeValue(attributeTechnicalName, float_value);
					break;
				case IGPAttributeInfo.BASE_SIGNED_SHORT :
					Short short_value = (Short)attributeValue;
					igpSubStructure.setAttributeValue(attributeTechnicalName, short_value);
					break;
				case IGPAttributeInfo.BASE_SIGNED_INT :
					Integer integer_value = (Integer)attributeValue;
					igpSubStructure.setAttributeValue(attributeTechnicalName, integer_value);
					break;
				case IGPAttributeInfo.BASE_BOOLEAN :
					Boolean boolean_value = (Boolean)attributeValue;
					igpSubStructure.setAttributeValue(attributeTechnicalName, boolean_value);
					break;
				case IGPAttributeInfo.BASE_BASE64BINARY :
					byte[] byte_value = (byte[])attributeValue;
					igpSubStructure.setAttributeValue(attributeTechnicalName, byte_value);
					break;
				default :
					throw new Exception("Invalid attributeInfoType = [" + attributeInfoType + "]");
			}
		}
	}

	private String retrieveInfoName(String technicalName) {
		String[] names = technicalName.split("\\.");
		return names[0];
	}

	private String retriveAttributeName(String attributeTechnicalName) {
		String[] names = attributeTechnicalName.split("\\.");
		return names[0];
	}

	private void manageUserIGPStruct(IWDNode contextNode, IGPStructure subStructure) throws Exception {
		//messageManager.reportSuccess("manageUserIGPStruct contextNode = [" + contextNode.getNodeInfo().getName() + 
		//	"] and Structure = [" + subStructure.getStructureInfo().getTechName() + "]");
		IGPStructureInfo structureInfo = subStructure.getStructureInfo();

		Collection collection = structureInfo.getAttributes();
		Iterator iter = collection.iterator();

		while (iter.hasNext()) {
			IGPAttributeInfo attributeInfo = (IGPAttributeInfo)iter.next();
			String attributeName = attributeInfo.getTechName();
			//messageManager.reportSuccess("Call setIGPAttributeValue for " +
			//	"Attribute = " + attributeName);
			setIGPAttributeValue(subStructure, contextNode, attributeInfo);
		}

	}

	/*********************************
	** Find Attribute Name corresponding
	** to attributeInfo from contextNode
	** and set it into IGPStructure
	*********************************/

	private void setIGPAttributeValue(IGPStructure igpStructure, IWDNode contextNode, IGPAttributeInfo attributeInfo)
		throws Exception {

		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		//manager.reportSuccess("setIGPAttributeValue igpStructureName = [" + 
		//	igpStructure.getStructureInfo().getTechName() + "] contextNodeName = [" +
		//	contextNode.getNodeInfo().getName() + "] attribute Name = [" + 
		//	attributeInfo.getTechName() + "]" );

		int attributeInfoType = attributeInfo.getType();
		int attributeMultiplicity = attributeInfo.getMultiplicity();
		String technicalName = attributeInfo.getTechName();

		String[] names = technicalName.split("\\.");
		String name = names[0];

		Collection attributeValues = retrieveNodeAttributes(contextNode, name);

		Object object = attributeValues.toArray()[0];

		if (attributeMultiplicity == IGPAttributeInfo.MULITIPLICITY_0_N
			|| attributeMultiplicity == IGPAttributeInfo.MULITIPLICITY_1_N) {
			//manager.reportSuccess("SetAttribute Values ");
			igpStructure.setAttributeValues(technicalName, attributeValues);
		} else {
			throw new Exception(
				"Processing Error: " + "Multiplicity is not correct for Attribute = [" + technicalName + "] ");
		}

	}

	/****************************
	** From ContextNode retrieve
	** all Elements and from every 
	** Element retrieve attribute value
	** from attributeName
	****************************/
	private Collection retrieveNodeAttributes(IWDNode contextNode, String attributeName) {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		//manager.reportSuccess("ChangeRequestSAP::retriveNodeAttributes ContextNode = [" +
		//	contextNode.getNodeInfo().getName() + "] attributeName = [" +
		//	attributeName + "]");

		/************************
		** NON COERENZA ...!!!!!
		** TO BE IMP !!!!!
		***********************/
		if (contextNode.getNodeInfo().getName().equals("AccettazioneList"))
			attributeName = "AccettazioneID";

		ArrayList arrayList = new ArrayList();

		int size = contextNode.size();

		for (int k = 0; k < size; k++) {
			IWDNodeElement nodeElement = contextNode.getElementAt(k);

			Object value = nodeElement.getAttributeValue(attributeName);

			//manager.reportSuccess("Insert AttributeElement = [" + 
			//	value.toString() + "]"  );

			arrayList.add(value);

		}

		return arrayList;
	}

	/******************************************
	** From IGP StructureInfo Technical Name
	** retrieve the corrisponding context node
	******************************************/
	private IWDNode retrieveContextNode(String strucuteInfoTechnicalName) {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		//manager.reportSuccess("Retrieve Context Node Corresponding with IGPStructure = " +
		//	"[" + strucuteInfoTechnicalName + "]");

		// The technical name can have the 
		// following sintax: 
		// InputITList.3
		IWDNode contextNode = null;
		String[] names = strucuteInfoTechnicalName.split("\\.");
		String name = names[0];

		//manager.reportSuccess("IGPStructure Name = [" + name  + "]");

		if (name.equals("InputOuputApprovalList")) {
			contextNode = wdContext.nodeApprovalList();
		} else if (name.equals("InputOutputAccettatoreList")) {
			contextNode = wdContext.nodeAccettazioneList();
		} else if (name.equals("InputOuputReaderList")) {
			contextNode = wdContext.nodeReaderList();
		} else if (name.equals("InputOutputITList")) {
			contextNode = wdContext.nodeITList();
		} else if (name.equals("InputUpdateCRAttributes")) {
			contextNode = wdContext.nodeCRAttributes();
		} else if (name.equals("InputOutputActionPerformed")) {
			contextNode = wdContext.nodeActionPerformed();
		} else if (name.equals("InputOutputReaderComments")) {
			contextNode = wdContext.nodeReaderComments();
		} else {
			manager.reportException(
				"ChangeRequestSAP::retrieveContextNode " + " Invalid IGP Info Structure Name",
				false);
			return null;
		}

		return contextNode;

	}

	private void printStructureInfo(IGPStructureInfo structureInfo) {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		String structureTecnicalName = structureInfo.getTechName();
		//manager.reportSuccess("Structure Tecnical Name = [" 
		//	+ structureTecnicalName + "]");

		Collection structures = structureInfo.getStructures();
		Iterator struct_iter = structures.iterator();

		while (struct_iter.hasNext()) {
			IGPStructureInfo structure = (IGPStructureInfo)struct_iter.next();

			printStructureInfo(structure);
		}

		Collection attributes = structureInfo.getAttributes();
		Iterator attribute_iter = attributes.iterator();

		while (attribute_iter.hasNext()) {
			IGPAttributeInfo attributeInfo = (IGPAttributeInfo)attribute_iter.next();

			String tecnicalName = attributeInfo.getTechName();
			int type = attributeInfo.getType();

			//manager.reportSuccess("Attribute Tecnical Name = [" + tecnicalName + 
			//	"] of type = [" + type + "]"); 
		}

	}

	private IGPProcessRoleInstanceList manageRoleList(IGPProcess process, IGPRuntimeManager runtimeManager)
		throws Exception {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		IGPRoleInfo roleInfo = null;
		IGPProcessRoleInstanceList processRoleInstanceList = null;
		String roleName;

		// Retrive Role Number
		int roleNumber = process.getRoleInfoCount();

		try {
			processRoleInstanceList = runtimeManager.createProcessRoleInstanceList();

			for (int k = 0; k < roleNumber; k++) {
				roleInfo = process.getRoleInfo(k);
				roleName = roleInfo.getRoleName();
				// TO BE IMP !!!!!!!
				if (roleName.equals("ProcessorManage")
					|| roleName.equals("role.owner")
					|| roleName.equals("role.administrator")
					|| roleName.equals("role.overseer")) {
					manageRoleInstance(roleName, processRoleInstanceList);
				}
			}
			/*
			String adminRole = process.getAdminRole().getRoleName();
			manageRoleInstance(adminRole,processRoleInstanceList);
			
			String ownerRole = process.getOwnerRole().getRoleName();
			manageRoleInstance(ownerRole,processRoleInstanceList);
			
			String overseerRole = process.getOverseerRole().getRoleName();
			manageRoleInstance(overseerRole,processRoleInstanceList);
			*/
		} catch (Exception exception) {
			manager.reportException("Exception in ChangeRequestSAP::manageRole = " + exception, false);
		}

		return processRoleInstanceList;

	}

	private void manageRoleInstance(String roleName, IGPProcessRoleInstanceList processRoleInstanceList)
		throws Exception {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		IGPProcessRoleInstance processRoleInstance = processRoleInstanceList.createProcessRoleInstance(roleName);

		ArrayList arrayList = retrieveUsersbyRole(roleName);

		if (arrayList == null || arrayList.size() == 0) {
			throw new Exception("Unable to Find users for Role = " + "[" + roleName + "]");
		}

		Iterator iter = arrayList.iterator();

		while (iter.hasNext()) {
			IUser iuser = (IUser)iter.next();
			//manager.reportSuccess("Add User = [" + iuser.getDisplayName() + 
			//	"] to RoleName = [" + roleName + "]");

			processRoleInstance.addUser(iuser);
			processRoleInstanceList.addProcessRoleInstance(processRoleInstance);

		}

	}

	/**********************************
	** Retrieve All Users
	** for a specific User
	**********************************/
	private ArrayList retrieveUsersbyRole(String roleName) {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		ArrayList arrayList = new ArrayList();

		/************************
		** To Test Ony
		** TO BE IMPLEMENTED
		************************/
		try {
			IUser iuser = WDClientUser.getCurrentUser().getSAPUser();
			arrayList.add(iuser);
		} catch (Exception e) {
			manager.reportException("ChangeRequestSAP::retrieveUsersbyRole " + "Exception = " + e, false);
		}

		return arrayList;
	}

	private String retrieveCurrentUserID() throws Exception {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		IWDClientUser clientUser = WDClientUser.getCurrentUser();
		IUserAccount[] userAccounts = clientUser.getSAPUser().getUserAccounts();
		IUserAccount iuserAccount = null;
		String logonID = null;

		for (int k = 0; k < userAccounts.length; k++) {
			iuserAccount = userAccounts[k];
			logonID = iuserAccount.getLogonUid();

			//manager.reportSuccess("Find Current UniqueId = [" + 
			//	logonID + "]");

		}

		return logonID;
	}

	/***************************************
	** Method updateUserList
	** It's used to manage the loop block
	** in GP.
	** When a user manager it's own task
	** it's removed from user list
	***************************************/

	private void updateUserList(String currentUserID) throws Exception {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		ICRAttributesElement crAttributesElement = wdContext.currentCRAttributesElement();

		if (crAttributesElement == null) {
			throw new Exception("ChangeRequestSAP::updateUserList " + " Context CRAttributesElement is null !!!!!");
		}

		short crStatus = crAttributesElement.getCRStatus();
		//manager.reportSuccess("CRSTATUS = [" + crStatus + "]");
		IWDNode node;

		switch (crStatus) {
			case 1 :
				throw new Exception("PROCESSING ERROR: " + "call updateUserList with status = 1");
			case 2 :
				node = wdContext.nodeReaderList();
				updateUserList(node, "ReaderID", currentUserID);
				break;
			case 3 :
				node = wdContext.nodeApprovalList();
				updateUserList(node, "ApprovalID", currentUserID);
				break;
			case 4 :
				node = wdContext.nodeAccettazioneList();
				updateUserList(node, "AccettazioneID", currentUserID);
				break;
			case 5 :
				break;
			case 6 :
				break;
			case 7 :
				/******************************
				** Update Status. It's not
				** necessary the update user
				** process. 
				***************************/
				break;
			default :
				throw new Exception("PROCESSING ERRORE: " + "Status = [" + crStatus + " IS INVALID");
		}

	}

	private void updateUserList(IWDNode node, String attributeName, String currentUserID) throws Exception {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();

		int size = node.size();
		String attributeValue;
		boolean found = false;

		//manager.reportSuccess("node = " + node.toString());
		//manager.reportSuccess("node Size = " + size);
		//manager.reportSuccess("CurrentUserID = " + currentUserID);
		//manager.reportSuccess("Attribute Name = " + attributeName);

		for (int j = size - 1; j >= 0; j--) {
			IWDNodeElement element = node.getElementAt(j);
			attributeValue = (String)element.getAttributeValue(attributeName);

			if (attributeValue == null)
				throw new Exception("Invalid Attribute = [" + attributeName + "] for node = [" + node + "]");

			if (attributeValue.equals(currentUserID)) {
				node.removeElement(element);
				found = true;
				break;
			}
		}

		if (!found) {
			throw new Exception("PROCESSING ERROR: It's not possibile to find" + " any user");
		}

	}

	private void retrieveNextStatus() throws Exception {

		ICRAttributesElement crAttributesElement = wdContext.currentCRAttributesElement();

		short crStatus = crAttributesElement.getCRStatus();
		IWDNode node;

		switch (crStatus) {
			case 1 :
				throw new Exception("PROCESSING ERRORE: " + "call updateUserList with status = 1");
			case 2 :
				node = wdContext.nodeReaderList();

				if (node.size() == 0)
					crAttributesElement.setCRStatus((short)3);

				break;
			case 3 :
				node = wdContext.nodeApprovalList();

				if (node.size() == 0)
					crAttributesElement.setCRStatus((short)4);

				break;
			case 4 :
				node = wdContext.nodeAccettazioneList();

				if (node.size() == 0)
					crAttributesElement.setCRStatus((short)5);

				break;
			case 5 :
				crAttributesElement.setCRStatus((short)6);
				break;
			case 6 :
				break;
			case 7 :
				/****************************
				** Update Status.
				** the next Status is 2 <=> 
				** Revisione
				****************************/
				crAttributesElement.setCRStatus((short)2);
				break;
			default :
				throw new Exception("PROCESSING ERRORE: " + "Status = [" + crStatus + " IS INVALID");
		}
	}

	private void printStackTrace(Exception exception) {
		IWDMessageManager manager = wdComponentAPI.getMessageManager();
		String stackTraceStr = new String();

		StackTraceElement[] stackTraceElements = exception.getStackTrace();

		for (int j = 0; j < stackTraceElements.length; j++) {
			stackTraceStr = stackTraceStr + stackTraceElements[j];
		}

		manager.reportException("Exception adding attribute, exception = " + exception, false);

		manager.reportException("Exception adding attribute, stackTrace = " + stackTraceStr, false);

	}

	// Possible Result State and Result State Desc of CO.
	private static final String RESULT_STATE_DA_RIVEDERE = "DaRivedere";
	private static final String RESULT_STATE_DA_RIVEDERE_DESC = "Da Rivedere";
	private static final String RESULT_STATE_APPROVATO = "Approvato";
	private static final String RESULT_STATE_APPROVATO_DESC = "Approvato";
	private static final String RESULT_STATE_RIFIUTATO = "Rifiutato";
	private static final String RESULT_STATE_RIFIUTATO_DESC = "Rifiutato";

	// KM Constant Definition
	private static final String KM_FILE_NAME = "/documents/IDProcessiGP/ChangeRequestSAPConf.txt";

	/*******************************
	** Class to Manager KM Values
	*******************************/
	protected class ManageKM {
		private String kmFileName;
		private IWDMessageManager messageManager = wdComponentAPI.getMessageManager();

		private HashSet sapModules = new HashSet();
		private HashSet priority = new HashSet();
		private String gpProcessID = new String();
		private String groupItName = new String();

		private final static String REG_EXP = "=";

		// KM Keys
		private static final String KM_KEY_SAP_MODULE = "SAP_MODULE";
		private static final String KM_KEY_IT_GROUP_NAME = "IT_GROUP_NAME";
		private static final String KM_KEY_PROCESS_ID = "PROCESS_ID";
		private static final String KM_KEY_PRIORITY = "PRIORITY";

		/*************************************
		** Public Interface
		*************************************/
		public ManageKM(String fileName) {
			kmFileName = fileName;
		}

		public String getGPProcessID() {
			return gpProcessID;
		}

		public String getGroupItName() {
			return groupItName;
		}

		public HashSet getSapModules() {
			return sapModules;
		}

		public HashSet getPriority() {
			return priority;
		}

		public void init() {
			try {
				IResourceFactory factory = ResourceFactory.getInstance();
				IUser user = WDClientUser.getCurrentUser().getSAPUser();
				ResourceContext resourceContext = new ResourceContext(WPUMFactory.getUserFactory().getEP5User(user));

				IResource resource = factory.getResource(RID.getRID(kmFileName), resourceContext);

				InputStream inputStream = resource.getContent().getInputStream();

				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String line;

				while ((line = bufferedReader.readLine()) != null) {
					parseLine(line);
				}
			} catch (Exception e) {
				messageManager.reportException("Exception in ManageKM::init = " + e, false);
			}
		}

		/***************************************
		** Private Function
		****************************************/
		private void parseLine(String line) {
			line = line.trim();
			String[] values = line.split(REG_EXP);

			String key = values[0].trim();
			String value = values[1].trim();

			//messageManager.reportSuccess("key = [" + key + "]");
			//messageManager.reportSuccess("value = [" + value + "]");

			if (key.equals(KM_KEY_PROCESS_ID)) {
				gpProcessID = value;
			} else if (key.equals(KM_KEY_IT_GROUP_NAME)) {
				groupItName = value;
			} else if (key.equals(KM_KEY_SAP_MODULE)) {
				sapModules.add(value);
			} else if (key.equals(KM_KEY_PRIORITY)) {
				priority.add(value);
			} else {
				messageManager.reportException("Key = [" + key + " in invalid Key", false);
			}

		}

	}

  //@@end
}

// ---- content of obsolete user coding area(s) ----
//@@begin obsolete:javadoc:loadKMValues()
//  /** Declared method. */
//@@end
//@@begin obsolete:loadAccettatoriDefaultValues()
//	// Accettazione
//	wdContext.nodeAccettazioneList().bind((Collection)null);
//	
//	IAccettazioneListElement listElement = 
//		wdContext.nodeAccettazioneList().createAccettazioneListElement();
//  		
//	listElement.setAccettazioneID("lsofia");
//	wdContext.nodeAccettazioneList().addElement(listElement);
//	
//@@end
//@@begin obsolete:javadoc:loadApprovatoriDefualtValues()
//  /** Declared method. */
//@@end
//@@begin obsolete:javadoc:loadAccettatoriDefaultValues()
//  /** Declared method. */
//@@end
//@@begin obsolete:loadKMValues()
//    
//    int returnValue = 0;
//	manageKM = new ManageKM(KM_FILE_NAME);
//	
//	try
//	{
//		manageKM.init();
//	}
//	catch ( Exception e)
//	{
//		return 1;
//	}
//    return returnValue;
//    
//@@end
//@@begin obsolete:loadApprovatoriDefualtValues()
//    
//	
//	// Approval
//	wdContext.nodeApprovalList().bind((Collection)null);
//	
//	IApprovalListElement approvalListElement = 
//		wdContext.nodeApprovalList().createApprovalListElement();
//	
//	approvalListElement.setApprovalID("lsofia");
//	wdContext.nodeApprovalList().addElement(approvalListElement);
//	
//@@end
//@@begin obsolete:loadRevisoriDefaultValues()
//    
//	// Revisori
//	
//	wdContext.nodeReaderList().bind((Collection)null);
//	
//	IReaderListElement readerListElement = 
//		wdContext.nodeReaderList().createReaderListElement();
//	
//	readerListElement.setReaderID("lsofia");
//	wdContext.nodeReaderList().addElement(readerListElement);
//    
//    
//@@end
//@@begin obsolete:javadoc:loadRevisoriDefaultValues()
//  /** Declared method. */
//@@end
