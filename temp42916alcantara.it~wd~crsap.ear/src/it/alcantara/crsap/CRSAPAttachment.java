// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.crsap;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateCRSAPAttachment).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import it.alcantara.crsap.wdp.IPrivateCRSAPAttachment;
import it.alcantara.crsap.wdp.IPublicCRSAPAttachment.IIt_FiletabElement;
import it.alcantara.model.addattach.Zcrsap_Add_Attachment_Input;
import it.alcantara.model.addattach.Zsattachment;
import it.alcantara.model.getattach.Zcrsap_Get_Attachment_Input;

import com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException;
import com.sap.tc.webdynpro.progmodel.api.IWDMessageManager;
//@@end

//@@begin documentation
//@@end

public class CRSAPAttachment
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(CRSAPAttachment.class);

  static 
  {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see it.alcantara.crsap.wdp.IPrivateCRSAPAttachment for more details
   */
  private final IPrivateCRSAPAttachment wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see it.alcantara.crsap.wdp.IPrivateCRSAPAttachment.IContextNode for more details.
   */
  private final IPrivateCRSAPAttachment.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public CRSAPAttachment(IPrivateCRSAPAttachment wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
  /** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
    //$$begin Service Controller(117027715)
    wdContext.nodeZcrsap_Get_Attachment_Input().bind(new Zcrsap_Get_Attachment_Input());
    //$$end
    //$$begin Service Controller(2070775267)
    wdContext.nodeZcrsap_Add_Attachment_Input().bind(new Zcrsap_Add_Attachment_Input());
    //$$end
    //@@end
  }

  //@@begin javadoc:wdDoExit()
  /** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:executeZcrsap_Add_Attachment_Input()
  /** Declared method. */
  //@@end
  public void executeZcrsap_Add_Attachment_Input( )
  {
    //@@begin executeZcrsap_Add_Attachment_Input()
    //$$begin Service Controller(1708830525)
    IWDMessageManager manager = wdComponentAPI.getMessageManager();
    try
    {
      wdContext.currentZcrsap_Add_Attachment_InputElement().modelObject().execute();
      wdContext.nodeOutput().invalidate();
    }
    catch(WDDynamicRFCExecuteException e)
    {
      manager.reportException(e.getMessage(), false);
    }
    //$$end
    //@@end
  }

  //@@begin javadoc:executeZcrsap_Get_Attachment_Input()
  /** Declared method. */
  //@@end
  public void executeZcrsap_Get_Attachment_Input( )
  {
    //@@begin executeZcrsap_Get_Attachment_Input()
    //$$begin Service Controller(680176603)
    IWDMessageManager manager = wdComponentAPI.getMessageManager();
    try
    {
      wdContext.currentZcrsap_Get_Attachment_InputElement().modelObject().execute();
      wdContext.nodeOutput_Get().invalidate();
    }
    catch(WDDynamicRFCExecuteException e)
    {
      manager.reportException(e.getMessage(), false);
    }
    //$$end
    //@@end
  }

  //@@begin javadoc:addAttachment()
  /** Declared method. */
  //@@end
  public void addAttachment( byte[] fileSource, java.lang.String fileName, java.lang.String fileType, byte[] crSapId )
  {
    //@@begin addAttachment()
    wdContext.currentZcrsap_Add_Attachment_InputElement().setI_Crsap_Id(crSapId);
    Zsattachment attachElement = new Zsattachment();
    attachElement.setFilecontent(fileSource);
    attachElement.setFilename(fileName);
    attachElement.setFiletype(fileType);
    wdContext.currentZcrsap_Add_Attachment_InputElement().modelObject().addIt_Filetab(attachElement);
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
}
