// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.model.update;

//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "UpdateCRSAP" RFC Adapter Model implementation
 * Copyright:    Copyright (c) 2001 - 2003. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class UpdateCRSAP extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel
{

  /**
   * Log version during <clinit>
   */
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final Object[][] WD_STATIC_TYPE_INFO = {
    { "it.alcantara.model.update.Zcrsap_Update_Output", "ZCRSAP_UPDATE",
      "output", new String[][] {
        { "Return_Code", "it.alcantara.model.update.types.Int_1" },
        { "Return_Msg", "it.alcantara.model.update.types.Char_25" },
      } },
    { "it.alcantara.model.update.Zcrsap_History", "ZCRSAP_HISTORY",
      "structure", 
      "it.alcantara.model.update.types.Zcrsap_History" },
    { "it.alcantara.model.update.Zcrsap_Update_Input", "ZCRSAP_UPDATE",
      "input", new String[][] {
        { "Action", "it.alcantara.model.update.types.Char1" },
        { "Analisi_Costi", "it.alcantara.model.update.types.Char_132" },
        { "Comment", "it.alcantara.model.update.types.Char_132" },
        { "Crsap_Id", "it.alcantara.model.update.types.Guid_16" },
        { "New_Status", "it.alcantara.model.update.types.Int_5" },
        { "Soluzione_Imp", "it.alcantara.model.update.types.Char_132" },
        { "User_Id", "it.alcantara.model.update.types.Zuser_Id" },
      } },
  };
  
  private static final com.sap.aii.proxy.framework.core.BaseProxyDescriptor staticDescriptor = 
    com.sap.aii.proxy.framework.core.BaseProxyDescriptorFactory.createNewBaseProxyDescriptor(
      "urn:sap-com:document:sap:rfc:functions:UpdateCRSAP.PortType",
      new java.lang.Object[][][] {
        {
          { "ZCRSAP_UPDATE",
            "zcrsap_Update",
            "ZCRSAP_UPDATE" },
          { "urn:sap-com:document:sap:rfc:functions:ZCRSAP_UPDATE.Input",
            "it.alcantara.model.update.Zcrsap_Update_Input" },
          { "urn:sap-com:document:sap:rfc:functions:ZCRSAP_UPDATE.Output",
            "it.alcantara.model.update.Zcrsap_Update_Output" },
          { "urn:sap-com:document:sap:rfc:functions:WDDynamicRFC_Fault.Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault" }
        },
      },
      com.sap.aii.proxy.framework.core.FactoryConstants.CONNECTION_TYPE_JCO,
      it.alcantara.model.update.UpdateCRSAP.class);

  public static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_MODELSCOPE =
        com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType.APPLICATION_SCOPE;
  public static final String DEFAULT_SYSTEMNAME = "ECC_ESS_MODELDATA";
  public static final String DEFAULT_RFCMETADATA_SYSTEMNAME = "ECC_ESS_METADATA";
  public static final String LOGICAL_DICTIONARY_NAME = "it.alcantara.model.update.types.UpdateCRSAP";
  public static final String LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME = "ECC_ESS_METADATA";
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057227900576L) ;
  static{
    com.sap.tc.webdynpro.progmodel.model.api.WDModelFactory.registerDefaultScopeForModel(it.alcantara.model.update.UpdateCRSAP.class, DEFAULT_MODELSCOPE);
  }

  /**
   * Class local cache for model metadata
   */
  private static final com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdModelMetadataCache =
  	new com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache(UpdateCRSAP.class);

  /**
   * Called by superclass to retrieve the model's metadata cache.
   */  
  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetInstanceMetadataCache() {
    return wdModelMetadataCache;
  }

  /**
   * Called to retrieve the model's metadata cache.
   */  
  static com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetStaticMetadataCache() {
    return wdModelMetadataCache;
  }
  
  /**
   * Constructor
   */
  public UpdateCRSAP() {
    this(DEFAULT_MODELSCOPE, null);
  }

  /**
   * Constructor
   * @param instanceId
   */
  public UpdateCRSAP( String instanceId) {
    this(DEFAULT_MODELSCOPE, instanceId);
  }

  /**
   * Constructor
   * @param scope
   */
  public UpdateCRSAP( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this(scope, null);
  }

  /**
   * Constructor
   * @param scope
   * @param instanceId
   */
  public UpdateCRSAP( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String instanceId) {
    super(WD_STATIC_TYPE_INFO, staticDescriptor, staticGenerationInfo, DEFAULT_SYSTEMNAME, LOGICAL_DICTIONARY_NAME, scope, instanceId, DEFAULT_RFCMETADATA_SYSTEMNAME, LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME);
  }

  public it.alcantara.model.update.Zcrsap_Update_Output zcrsap_Update(it.alcantara.model.update.Zcrsap_Update_Input input)
    throws com.sap.aii.proxy.framework.core.SystemFaultException,
           com.sap.aii.proxy.framework.core.ApplicationFaultException,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException {
    com.sap.aii.proxy.framework.core.BaseType result = null;
    prepareExecute();
    try {
      result = send$(input, "urn:sap-com:document:sap:rfc:functions", "UpdateCRSAP.PortType", "ZCRSAP_UPDATE", new it.alcantara.model.update.Zcrsap_Update_Output());
    } catch (com.sap.aii.proxy.framework.core.ApplicationFaultException e){
      if (e instanceof com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)
      	{ throw (com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)e;}
      throw createExceptionWrongExceptionType$(e);
    }
    if (result == null || (result instanceof it.alcantara.model.update.Zcrsap_Update_Output)) {
      return (it.alcantara.model.update.Zcrsap_Update_Output) result;
    } else { 
      throw createExceptionWrongType$(result);
    }
  }

}
