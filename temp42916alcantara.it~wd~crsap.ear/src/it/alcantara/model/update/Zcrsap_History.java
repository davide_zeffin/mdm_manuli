// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.model.update;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Zcrsap_History" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Zcrsap_History extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Zcrsap_History.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.alcantara.model.update.UpdateCRSAP.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.alcantara.model.update.UpdateCRSAP.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Zcrsap_History () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.alcantara.model.update.UpdateCRSAP.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.alcantara.model.update.UpdateCRSAP.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Zcrsap_History.class,
                       "ZCRSAP_HISTORY",
                       PROXYTYPE_STRUCTURE, 
                       "Zcrsap_History", 
                       "it.alcantara.model.update" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.alcantara.model.update.UpdateCRSAP.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Zcrsap_History (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_History (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.alcantara.model.update.UpdateCRSAP.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_History (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.alcantara.model.update.UpdateCRSAP 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Zcrsap_History ( it.alcantara.model.update.UpdateCRSAP modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public UpdateCRSAP modelInstance() {
    return (UpdateCRSAP)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.alcantara.model.update.UpdateCRSAP.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> Action_Desc
   * **************************************************************************/
  /** getter for ModelAttribute -> Action_Desc 
   *  @return value of ModelAttribute Action_Desc */
  public java.lang.String getAction_Desc() {
    return super.getAttributeValueAsString("Action_Desc");
  }
 
  /** setter for ModelAttribute -> Action_Desc 
   *  @param value new value for ModelAttribute Action_Desc */
  public void setAction_Desc(java.lang.String value) {
    super.setAttributeValueAsString("Action_Desc", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Data
   * **************************************************************************/
  /** getter for ModelAttribute -> Data 
   *  @return value of ModelAttribute Data */
  public java.sql.Date getData() {
    return super.getAttributeValueAsDate("Data");
  }
 
  /** setter for ModelAttribute -> Data 
   *  @param value new value for ModelAttribute Data */
  public void setData(java.sql.Date value) {
    super.setAttributeValueAsDate("Data", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> User_Id
   * **************************************************************************/
  /** getter for ModelAttribute -> User_Id 
   *  @return value of ModelAttribute User_Id */
  public java.lang.String getUser_Id() {
    return super.getAttributeValueAsString("User_Id");
  }
 
  /** setter for ModelAttribute -> User_Id 
   *  @param value new value for ModelAttribute User_Id */
  public void setUser_Id(java.lang.String value) {
    super.setAttributeValueAsString("User_Id", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
  public static class Zcrsap_History_List extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCList implements java.io.Serializable , java.util.List   {

    protected Class _associatedModelClazz() { return _associatedModelClazz; }
	protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){ return DEFAULT_SCOPE; }

    private static final long serialVersionUID = 60422512L ;
    
    public Zcrsap_History_List () {
      super(createElementProperties(it.alcantara.model.update.Zcrsap_History.class, new it.alcantara.model.update.Zcrsap_History() ));
    }

    /** Constructor for specific scope
     *  use this Constructor, if this List should belong to a model instance
     *  of a different scope than the default model scope.
     *  The default model scope can be found in it.alcantara.model.update.UpdateCRSAP.DEFAULT_MODELSCOPE
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     */
    public Zcrsap_History_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
      this();
      maintainInScope(scope);
    }
  
    /** Constructor for specific modelInstanceId
     *  use this Constructor, if this List should belong to a different model instance
     *  than the default model instance.
     *  The default modelInstance has no id (null). 
     * 
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zcrsap_History_List (String modelInstanceId) {
      this();
      maintainInInstanceId(modelInstanceId);
    }
  
    /** Constructor for specific scope and modelInstanceId
     *  use this Constructor, of this List should belong to a differnt scope
     *  and a different model instance than the default model scope and model instance.
     *  The default model scope can be found in it.alcantara.model.update.UpdateCRSAP.DEFAULT_MODELSCOPE
     *  The default modelInstance has no id (null). 
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zcrsap_History_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
      this();
      maintainInScopeAndInstanceId(scope, modelInstanceId);
    }
  
    /** Constructor for specific model Instance
     *  use this Constructor, if this instance should belong to a specific instance
     *  of the model it.alcantara.model.update.UpdateCRSAP 
     *  passed as a parameter to this constructor.
     * 
     *  @param modelInstance the DynamicRFCModel referring to the modelInstance
     */
    public Zcrsap_History_List ( it.alcantara.model.update.UpdateCRSAP modelInstance) {
      this();
      maintainInModel(modelInstance);
    }

    public it.alcantara.model.update.Zcrsap_History[] toArrayZcrsap_History() {
      return (it.alcantara.model.update.Zcrsap_History[])super.toArray(new it.alcantara.model.update.Zcrsap_History[] {});
    }
    
    public int lastIndexOfZcrsap_History(it.alcantara.model.update.Zcrsap_History item) {
      return super.lastIndexOf(item);
    }
    
    public int indexOfZcrsap_History(it.alcantara.model.update.Zcrsap_History item) {
      return super.indexOf(item);
    }
    
    public Zcrsap_History_List subListZcrsap_History(int fromIndex, int toIndex) {
      return (Zcrsap_History_List)super.subList(fromIndex, toIndex);
    }
    
    public void addAllZcrsap_History(Zcrsap_History_List item) {
      super.addAll(item);
    }
    
    public void addZcrsap_History(it.alcantara.model.update.Zcrsap_History item) {
      super.add(item);
    }
    
    public boolean removeZcrsap_History(it.alcantara.model.update.Zcrsap_History item) {
      return super.remove(item);
    }
    
    public it.alcantara.model.update.Zcrsap_History getZcrsap_History(int index) {
      return (it.alcantara.model.update.Zcrsap_History)super.get(index);
    }
    
    public boolean containsAllZcrsap_History(Zcrsap_History_List item) {
      return super.containsAll(item);
    }
    
    public void addZcrsap_History(int index, it.alcantara.model.update.Zcrsap_History item) {
      super.add(index, item);
    }
    
    public boolean containsZcrsap_History(it.alcantara.model.update.Zcrsap_History item) {
      return super.contains(item);
    }
        
    public void addAllZcrsap_History(int index, Zcrsap_History_List item) {
      super.addAll(index, item);
    }
    
    public it.alcantara.model.update.Zcrsap_History setZcrsap_History(int index, it.alcantara.model.update.Zcrsap_History item) {
      return (it.alcantara.model.update.Zcrsap_History)super.set(index, item);
    }
    
    public it.alcantara.model.update.Zcrsap_History removeZcrsap_History(int index) {
      return (it.alcantara.model.update.Zcrsap_History)super.remove(index);
    }
  }
  
}
