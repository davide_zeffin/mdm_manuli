// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.model.update;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Zcrsap_Update_Output" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Zcrsap_Update_Output extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Zcrsap_Update_Output.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.alcantara.model.update.UpdateCRSAP.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.alcantara.model.update.UpdateCRSAP.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Zcrsap_Update_Output () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.alcantara.model.update.UpdateCRSAP.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.alcantara.model.update.UpdateCRSAP.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Zcrsap_Update_Output.class,
                       "ZCRSAP_UPDATE",
                       PROXYTYPE_OUTPUT, 
                       "Zcrsap_Update_Output", 
                       "it.alcantara.model.update" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.alcantara.model.update.UpdateCRSAP.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Zcrsap_Update_Output (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_Update_Output (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.alcantara.model.update.UpdateCRSAP.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zcrsap_Update_Output (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.alcantara.model.update.UpdateCRSAP 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Zcrsap_Update_Output ( it.alcantara.model.update.UpdateCRSAP modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public UpdateCRSAP modelInstance() {
    return (UpdateCRSAP)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.alcantara.model.update.UpdateCRSAP.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /****************************************************************************
   *  1:n Relation -> History_Action
   ***************************************************************************/

  /** getter for 1:n Relation Role -> History_Action 
   *  @return java.util.List containing elements of 1:n Relation Role History_Action */
  public java.util.List getHistory_Action() {
    return (java.util.List)getRelatedModelObjects("History_Action");
  }

  /** setter for 1:n Relation Role -> History_Action 
   *  @param newList java.util.List replaces previous List of elements of 1:n Relation Role History_Action */
  public void setHistory_Action(com.sap.aii.proxy.framework.core.AbstractList list) {
    setRelatedModelObjects("History_Action", list);
  }

  /** adds an element to the 1:n Relation Role -> History_Action 
   *  @param o object to be added to List of elements of 1:n Relation Role History_Action
   *  @return true if element was added */
  public boolean addHistory_Action(it.alcantara.model.update.Zcrsap_History o) {
    return addRelatedModelObject("History_Action", o);
  }

  /** removes the given element from the 1:n Relation Role -> History_Action 
   *  @param o object to be removed from List of elements of 1:n Relation Role History_Action 
   *  @return true if element existed and was removed */
  public boolean removeHistory_Action(it.alcantara.model.update.Zcrsap_History o)  {
    return removeRelatedModelObject("History_Action", o);
  }
  
  /* ***************************************************************************
   *  ModelAttribute -> Return_Code
   * **************************************************************************/
  /** getter for ModelAttribute -> Return_Code 
   *  @return value of ModelAttribute Return_Code */
  public short getReturn_Code() {
    return super.getAttributeValueAsShort("Return_Code");
  }
 
  /** setter for ModelAttribute -> Return_Code 
   *  @param value new value for ModelAttribute Return_Code */
  public void setReturn_Code(short value) {
    super.setAttributeValueAsShort("Return_Code", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Return_Msg
   * **************************************************************************/
  /** getter for ModelAttribute -> Return_Msg 
   *  @return value of ModelAttribute Return_Msg */
  public java.lang.String getReturn_Msg() {
    return super.getAttributeValueAsString("Return_Msg");
  }
 
  /** setter for ModelAttribute -> Return_Msg 
   *  @param value new value for ModelAttribute Return_Msg */
  public void setReturn_Msg(java.lang.String value) {
    super.setAttributeValueAsString("Return_Msg", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
}
