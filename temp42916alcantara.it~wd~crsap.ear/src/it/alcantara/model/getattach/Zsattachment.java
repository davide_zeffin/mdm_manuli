// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.model.getattach;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Zsattachment" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Zsattachment extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Zsattachment.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.alcantara.model.getattach.CRSAPGetAttach.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Zsattachment () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.alcantara.model.getattach.CRSAPGetAttach.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Zsattachment.class,
                       "ZSATTACHMENT",
                       PROXYTYPE_STRUCTURE, 
                       "Zsattachment", 
                       "it.alcantara.model.getattach" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Zsattachment (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zsattachment (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Zsattachment (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.alcantara.model.getattach.CRSAPGetAttach 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Zsattachment ( it.alcantara.model.getattach.CRSAPGetAttach modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public CRSAPGetAttach modelInstance() {
    return (CRSAPGetAttach)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.alcantara.model.getattach.CRSAPGetAttach.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> Filecontent
   * **************************************************************************/
  /** getter for ModelAttribute -> Filecontent 
   *  @return value of ModelAttribute Filecontent */
  public byte[] getFilecontent() {
    return (byte[])super.getAttributeValue("Filecontent");
  }
 
  /** setter for ModelAttribute -> Filecontent 
   *  @param value new value for ModelAttribute Filecontent */
  public void setFilecontent(byte[] value) {
    super.setAttributeValue("Filecontent", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Filename
   * **************************************************************************/
  /** getter for ModelAttribute -> Filename 
   *  @return value of ModelAttribute Filename */
  public java.lang.String getFilename() {
    return super.getAttributeValueAsString("Filename");
  }
 
  /** setter for ModelAttribute -> Filename 
   *  @param value new value for ModelAttribute Filename */
  public void setFilename(java.lang.String value) {
    super.setAttributeValueAsString("Filename", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Filetype
   * **************************************************************************/
  /** getter for ModelAttribute -> Filetype 
   *  @return value of ModelAttribute Filetype */
  public java.lang.String getFiletype() {
    return super.getAttributeValueAsString("Filetype");
  }
 
  /** setter for ModelAttribute -> Filetype 
   *  @param value new value for ModelAttribute Filetype */
  public void setFiletype(java.lang.String value) {
    super.setAttributeValueAsString("Filetype", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
  public static class Zsattachment_List extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCList implements java.io.Serializable , java.util.List   {

    protected Class _associatedModelClazz() { return _associatedModelClazz; }
	protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){ return DEFAULT_SCOPE; }

    private static final long serialVersionUID = 60422512L ;
    
    public Zsattachment_List () {
      super(createElementProperties(it.alcantara.model.getattach.Zsattachment.class, new it.alcantara.model.getattach.Zsattachment() ));
    }

    /** Constructor for specific scope
     *  use this Constructor, if this List should belong to a model instance
     *  of a different scope than the default model scope.
     *  The default model scope can be found in it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     */
    public Zsattachment_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
      this();
      maintainInScope(scope);
    }
  
    /** Constructor for specific modelInstanceId
     *  use this Constructor, if this List should belong to a different model instance
     *  than the default model instance.
     *  The default modelInstance has no id (null). 
     * 
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zsattachment_List (String modelInstanceId) {
      this();
      maintainInInstanceId(modelInstanceId);
    }
  
    /** Constructor for specific scope and modelInstanceId
     *  use this Constructor, of this List should belong to a differnt scope
     *  and a different model instance than the default model scope and model instance.
     *  The default model scope can be found in it.alcantara.model.getattach.CRSAPGetAttach.DEFAULT_MODELSCOPE
     *  The default modelInstance has no id (null). 
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Zsattachment_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
      this();
      maintainInScopeAndInstanceId(scope, modelInstanceId);
    }
  
    /** Constructor for specific model Instance
     *  use this Constructor, if this instance should belong to a specific instance
     *  of the model it.alcantara.model.getattach.CRSAPGetAttach 
     *  passed as a parameter to this constructor.
     * 
     *  @param modelInstance the DynamicRFCModel referring to the modelInstance
     */
    public Zsattachment_List ( it.alcantara.model.getattach.CRSAPGetAttach modelInstance) {
      this();
      maintainInModel(modelInstance);
    }

    public it.alcantara.model.getattach.Zsattachment[] toArrayZsattachment() {
      return (it.alcantara.model.getattach.Zsattachment[])super.toArray(new it.alcantara.model.getattach.Zsattachment[] {});
    }
    
    public int lastIndexOfZsattachment(it.alcantara.model.getattach.Zsattachment item) {
      return super.lastIndexOf(item);
    }
    
    public int indexOfZsattachment(it.alcantara.model.getattach.Zsattachment item) {
      return super.indexOf(item);
    }
    
    public Zsattachment_List subListZsattachment(int fromIndex, int toIndex) {
      return (Zsattachment_List)super.subList(fromIndex, toIndex);
    }
    
    public void addAllZsattachment(Zsattachment_List item) {
      super.addAll(item);
    }
    
    public void addZsattachment(it.alcantara.model.getattach.Zsattachment item) {
      super.add(item);
    }
    
    public boolean removeZsattachment(it.alcantara.model.getattach.Zsattachment item) {
      return super.remove(item);
    }
    
    public it.alcantara.model.getattach.Zsattachment getZsattachment(int index) {
      return (it.alcantara.model.getattach.Zsattachment)super.get(index);
    }
    
    public boolean containsAllZsattachment(Zsattachment_List item) {
      return super.containsAll(item);
    }
    
    public void addZsattachment(int index, it.alcantara.model.getattach.Zsattachment item) {
      super.add(index, item);
    }
    
    public boolean containsZsattachment(it.alcantara.model.getattach.Zsattachment item) {
      return super.contains(item);
    }
        
    public void addAllZsattachment(int index, Zsattachment_List item) {
      super.addAll(index, item);
    }
    
    public it.alcantara.model.getattach.Zsattachment setZsattachment(int index, it.alcantara.model.getattach.Zsattachment item) {
      return (it.alcantara.model.getattach.Zsattachment)super.set(index, item);
    }
    
    public it.alcantara.model.getattach.Zsattachment removeZsattachment(int index) {
      return (it.alcantara.model.getattach.Zsattachment)super.remove(index);
    }
  }
  
}
