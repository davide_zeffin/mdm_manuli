// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.alcantara.model.getattach;

//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "CRSAPGetAttach" RFC Adapter Model implementation
 * Copyright:    Copyright (c) 2001 - 2003. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class CRSAPGetAttach extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel
{

  /**
   * Log version during <clinit>
   */
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final Object[][] WD_STATIC_TYPE_INFO = {
    { "it.alcantara.model.getattach.Zcrsap_Get_Attachment_Output", "ZCRSAP_GET_ATTACHMENT",
      "output", new String[][] {
      } },
    { "it.alcantara.model.getattach.Zcrsap_Get_Attachment_Input", "ZCRSAP_GET_ATTACHMENT",
      "input", new String[][] {
        { "I_Crsap_Id", "it.alcantara.model.getattach.types.Guid_16" },
      } },
    { "it.alcantara.model.getattach.Bapiret2", "BAPIRET2",
      "structure", 
      "it.alcantara.model.getattach.types.Bapiret2" },
    { "it.alcantara.model.getattach.Zsattachment", "ZSATTACHMENT",
      "structure", 
      "it.alcantara.model.getattach.types.Zsattachment" },
  };
  
  private static final com.sap.aii.proxy.framework.core.BaseProxyDescriptor staticDescriptor = 
    com.sap.aii.proxy.framework.core.BaseProxyDescriptorFactory.createNewBaseProxyDescriptor(
      "urn:sap-com:document:sap:rfc:functions:CRSAPGetAttach.PortType",
      new java.lang.Object[][][] {
        {
          { "ZCRSAP_GET_ATTACHMENT",
            "zcrsap_Get_Attachment",
            "ZCRSAP_GET_ATTACHMENT" },
          { "urn:sap-com:document:sap:rfc:functions:ZCRSAP_GET_ATTACHMENT.Input",
            "it.alcantara.model.getattach.Zcrsap_Get_Attachment_Input" },
          { "urn:sap-com:document:sap:rfc:functions:ZCRSAP_GET_ATTACHMENT.Output",
            "it.alcantara.model.getattach.Zcrsap_Get_Attachment_Output" },
          { "urn:sap-com:document:sap:rfc:functions:WDDynamicRFC_Fault.Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault" }
        },
      },
      com.sap.aii.proxy.framework.core.FactoryConstants.CONNECTION_TYPE_JCO,
      it.alcantara.model.getattach.CRSAPGetAttach.class);

  public static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_MODELSCOPE =
        com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType.APPLICATION_SCOPE;
  public static final String DEFAULT_SYSTEMNAME = "ECC_ESS_MODELDATA";
  public static final String DEFAULT_RFCMETADATA_SYSTEMNAME = "ECC_ESS_METADATA";
  public static final String LOGICAL_DICTIONARY_NAME = "it.alcantara.model.getattach.types.CRSAPGetAttach";
  public static final String LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME = "ECC_ESS_METADATA";
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057227900576L) ;
  static{
    com.sap.tc.webdynpro.progmodel.model.api.WDModelFactory.registerDefaultScopeForModel(it.alcantara.model.getattach.CRSAPGetAttach.class, DEFAULT_MODELSCOPE);
  }

  /**
   * Class local cache for model metadata
   */
  private static final com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdModelMetadataCache =
  	new com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache(CRSAPGetAttach.class);

  /**
   * Called by superclass to retrieve the model's metadata cache.
   */  
  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetInstanceMetadataCache() {
    return wdModelMetadataCache;
  }

  /**
   * Called to retrieve the model's metadata cache.
   */  
  static com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetStaticMetadataCache() {
    return wdModelMetadataCache;
  }
  
  /**
   * Constructor
   */
  public CRSAPGetAttach() {
    this(DEFAULT_MODELSCOPE, null);
  }

  /**
   * Constructor
   * @param instanceId
   */
  public CRSAPGetAttach( String instanceId) {
    this(DEFAULT_MODELSCOPE, instanceId);
  }

  /**
   * Constructor
   * @param scope
   */
  public CRSAPGetAttach( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this(scope, null);
  }

  /**
   * Constructor
   * @param scope
   * @param instanceId
   */
  public CRSAPGetAttach( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String instanceId) {
    super(WD_STATIC_TYPE_INFO, staticDescriptor, staticGenerationInfo, DEFAULT_SYSTEMNAME, LOGICAL_DICTIONARY_NAME, scope, instanceId, DEFAULT_RFCMETADATA_SYSTEMNAME, LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME);
  }

  public it.alcantara.model.getattach.Zcrsap_Get_Attachment_Output zcrsap_Get_Attachment(it.alcantara.model.getattach.Zcrsap_Get_Attachment_Input input)
    throws com.sap.aii.proxy.framework.core.SystemFaultException,
           com.sap.aii.proxy.framework.core.ApplicationFaultException,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException {
    com.sap.aii.proxy.framework.core.BaseType result = null;
    prepareExecute();
    try {
      result = send$(input, "urn:sap-com:document:sap:rfc:functions", "CRSAPGetAttach.PortType", "ZCRSAP_GET_ATTACHMENT", new it.alcantara.model.getattach.Zcrsap_Get_Attachment_Output());
    } catch (com.sap.aii.proxy.framework.core.ApplicationFaultException e){
      if (e instanceof com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)
      	{ throw (com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)e;}
      throw createExceptionWrongExceptionType$(e);
    }
    if (result == null || (result instanceof it.alcantara.model.getattach.Zcrsap_Get_Attachment_Output)) {
      return (it.alcantara.model.getattach.Zcrsap_Get_Attachment_Output) result;
    } else { 
      throw createExceptionWrongType$(result);
    }
  }

}
