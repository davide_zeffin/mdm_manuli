package com.sap.tut.wd.carrental.model.proxies;

// Import libraries
import javax.xml.rpc.holders.*;
import java.rmi.RemoteException;
import javax.xml.rpc.encoding.*;
import javax.xml.namespace.QName;
import com.sap.engine.services.webservices.jaxrpc.wsdl2java.ServiceParam;
import com.sap.engine.services.webservices.jaxrpc.wsdl2java.PropertyContext;
import com.sap.engine.services.webservices.jaxrpc.exceptions.TypeMappingException;
import com.sap.engine.services.webservices.jaxrpc.exceptions.accessors.XmlSerializationResourceAccessor;
import com.sap.exception.BaseRuntimeException;

public class Config1BindingStub extends com.sap.engine.services.webservices.jaxrpc.wsdl2java.BaseGeneratedStub implements com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument {

  // Proxy variables
  private com.sap.engine.services.webservices.jaxrpc.encoding.TypeMappingRegistryImpl typeRegistry;

  public Config1BindingStub() {
    super();
    super._setEndpoint("http://localhost:50000/QuickCarRentalService/Config1?style=document");
    this.transportBinding = new com.sap.engine.services.webservices.jaxrpc.wsdl2java.soapbinding.MimeHttpBinding();
    try {
      this.typeRegistry = new com.sap.engine.services.webservices.jaxrpc.encoding.TypeMappingRegistryImpl();
      this.typeRegistry.fromXML(this.getClass().getClassLoader().getResourceAsStream("com/sap/tut/wd/carrental/model/proxies/types.xml"),this.getClass().getClassLoader());
    } catch (java.lang.Exception e) {
      throw new BaseRuntimeException(XmlSerializationResourceAccessor.getResourceAccessor(),TypeMappingException.COMMON_EXCEPTION,e);
    }
    this.featureConfiguration.setProperty("typeMapping",this.typeRegistry.getDefaultTypeMapping());
    this.localProtocols.put("cancelBooking",new com.sap.engine.services.webservices.jaxrpc.wsdl2java.ProtocolList());
    this.localFeatures.put("cancelBooking",new PropertyContext());
    this.localProtocols.put("saveBooking",new com.sap.engine.services.webservices.jaxrpc.wsdl2java.ProtocolList());
    this.localFeatures.put("saveBooking",new PropertyContext());
    this.localProtocols.put("viewActiveBookings",new com.sap.engine.services.webservices.jaxrpc.wsdl2java.ProtocolList());
    this.localFeatures.put("viewActiveBookings",new PropertyContext());
  }

  public com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBookingResponse cancelBooking(com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBooking parameters) throws java.rmi.RemoteException,com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException {
    try {
      // Operation input params initialization
      this.inputParams = new ServiceParam[1];
      this.inputParams[0] = new ServiceParam();
      this.inputParams[0].isElement = true;
      this.inputParams[0].schemaName = new QName("urn:QuickCarRentalServiceVi","cancelBooking");
      this.inputParams[0].name = "parameters";
      this.inputParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBooking.class;
      this.inputParams[0].content = parameters;
      // Operation output params initialization
      this.outputParams = new ServiceParam[1];
      this.outputParams[0] = new ServiceParam();
      this.outputParams[0].isElement = true;
      this.outputParams[0].schemaName = new QName("urn:QuickCarRentalServiceVi","cancelBookingResponse");
      this.outputParams[0].name = "parameters";
      this.outputParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBookingResponse.class;
      // Operation faults initialization
      this.faultParams = new ServiceParam[1];
      this.faultParams[0] = new ServiceParam();
      this.faultParams[0].isElement = true;
      this.faultParams[0].schemaName = new QName("urn:QuickCarRentalServiceWsd/QuickCarRentalServiceVi","cancelBooking_com.sap.engine.examples.util.QuickCarRentalException");
      this.faultParams[0].name = "errorPart";
      this.faultParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException.class;
      com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBookingResponse parametersTemp;
      this.transportBinding.setTypeMappingRegistry(this.typeRegistry);
      this.transportBinding.startOperation(this.inputParams,this.outputParams,this.faultParams);
      // Binding Context initialization
      this.bindingConfiguration.clear();
      bindingConfiguration.setProperty("soapAction","");
      bindingConfiguration.setProperty("style","document");
      bindingConfiguration.setProperty("transport","http://schemas.xmlsoap.org/soap/http");
      PropertyContext bindingConfigurationX;
      bindingConfigurationX = bindingConfiguration.getSubContext("output");
      bindingConfigurationX.setProperty("operationName","cancelBooking");
      bindingConfigurationX.setProperty("use","literal");
      bindingConfigurationX = bindingConfiguration.getSubContext("input");
      bindingConfigurationX.setProperty("operationName","cancelBooking");
      bindingConfigurationX.setProperty("use","literal");
      bindingConfigurationX.setProperty("parts","parameters");
      super._fillEndpoint(bindingConfiguration);
      _buildOperationContext("cancelBooking",this.transportBinding);
      this.transportBinding.call(this.stubConfiguration,this.globalProtocols,_getOperationProtocols("cancelBooking"));
      if (this.faultParams[0].content != null) {
        throw (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException) this.faultParams[0].content;
      }
      parametersTemp = (com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBookingResponse) this.outputParams[0].content;
      return parametersTemp;
    } catch (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException e) {
      throw e;
    } catch (javax.xml.rpc.soap.SOAPFaultException e) {
      throw e;
    } catch (java.lang.Exception e) {
      throw new RemoteException("Service call exception",e);
    }
  }

  public java.lang.String cancelBooking(java.lang.String bookingId)  throws java.rmi.RemoteException,com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException {
    com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBooking _request_variable = new com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBooking();
    _request_variable.setBookingId(bookingId);
    com.sap.tut.wd.carrental.model.proxies.types.p1.CancelBookingResponse _result_variable = cancelBooking(_request_variable);
    return _result_variable.getResponse();
  }

  public com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBookingResponse saveBooking(com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBooking parameters) throws java.rmi.RemoteException,com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException {
    try {
      // Operation input params initialization
      this.inputParams = new ServiceParam[1];
      this.inputParams[0] = new ServiceParam();
      this.inputParams[0].isElement = true;
      this.inputParams[0].schemaName = new QName("urn:QuickCarRentalServiceVi","saveBooking");
      this.inputParams[0].name = "parameters";
      this.inputParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBooking.class;
      this.inputParams[0].content = parameters;
      // Operation output params initialization
      this.outputParams = new ServiceParam[1];
      this.outputParams[0] = new ServiceParam();
      this.outputParams[0].isElement = true;
      this.outputParams[0].schemaName = new QName("urn:QuickCarRentalServiceVi","saveBookingResponse");
      this.outputParams[0].name = "parameters";
      this.outputParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBookingResponse.class;
      // Operation faults initialization
      this.faultParams = new ServiceParam[1];
      this.faultParams[0] = new ServiceParam();
      this.faultParams[0].isElement = true;
      this.faultParams[0].schemaName = new QName("urn:QuickCarRentalServiceWsd/QuickCarRentalServiceVi","saveBooking_com.sap.engine.examples.util.QuickCarRentalException");
      this.faultParams[0].name = "errorPart";
      this.faultParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException.class;
      com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBookingResponse parametersTemp;
      this.transportBinding.setTypeMappingRegistry(this.typeRegistry);
      this.transportBinding.startOperation(this.inputParams,this.outputParams,this.faultParams);
      // Binding Context initialization
      this.bindingConfiguration.clear();
      bindingConfiguration.setProperty("soapAction","");
      bindingConfiguration.setProperty("style","document");
      bindingConfiguration.setProperty("transport","http://schemas.xmlsoap.org/soap/http");
      PropertyContext bindingConfigurationX;
      bindingConfigurationX = bindingConfiguration.getSubContext("output");
      bindingConfigurationX.setProperty("operationName","saveBooking");
      bindingConfigurationX.setProperty("use","literal");
      bindingConfigurationX = bindingConfiguration.getSubContext("input");
      bindingConfigurationX.setProperty("operationName","saveBooking");
      bindingConfigurationX.setProperty("use","literal");
      bindingConfigurationX.setProperty("parts","parameters");
      super._fillEndpoint(bindingConfiguration);
      _buildOperationContext("saveBooking",this.transportBinding);
      this.transportBinding.call(this.stubConfiguration,this.globalProtocols,_getOperationProtocols("saveBooking"));
      if (this.faultParams[0].content != null) {
        throw (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException) this.faultParams[0].content;
      }
      parametersTemp = (com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBookingResponse) this.outputParams[0].content;
      return parametersTemp;
    } catch (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException e) {
      throw e;
    } catch (javax.xml.rpc.soap.SOAPFaultException e) {
      throw e;
    } catch (java.lang.Exception e) {
      throw new RemoteException("Service call exception",e);
    }
  }

  public com.sap.tut.wd.carrental.model.proxies.types.QuickBookingModel saveBooking(java.lang.String vehicleTypeId, java.lang.String dateFromString, java.lang.String dateToString, java.lang.String pickupLocation, java.lang.String dropoffLocation)  throws java.rmi.RemoteException,com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException {
    com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBooking _request_variable = new com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBooking();
    _request_variable.setVehicleTypeId(vehicleTypeId);
    _request_variable.setDateFromString(dateFromString);
    _request_variable.setDateToString(dateToString);
    _request_variable.setPickupLocation(pickupLocation);
    _request_variable.setDropoffLocation(dropoffLocation);
    com.sap.tut.wd.carrental.model.proxies.types.p1.SaveBookingResponse _result_variable = saveBooking(_request_variable);
    return _result_variable.getResponse();
  }

  public com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookingsResponse viewActiveBookings(com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookings parameters) throws java.rmi.RemoteException,com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException {
    try {
      // Operation input params initialization
      this.inputParams = new ServiceParam[1];
      this.inputParams[0] = new ServiceParam();
      this.inputParams[0].isElement = true;
      this.inputParams[0].schemaName = new QName("urn:QuickCarRentalServiceVi","viewActiveBookings");
      this.inputParams[0].name = "parameters";
      this.inputParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookings.class;
      this.inputParams[0].content = parameters;
      // Operation output params initialization
      this.outputParams = new ServiceParam[1];
      this.outputParams[0] = new ServiceParam();
      this.outputParams[0].isElement = true;
      this.outputParams[0].schemaName = new QName("urn:QuickCarRentalServiceVi","viewActiveBookingsResponse");
      this.outputParams[0].name = "parameters";
      this.outputParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookingsResponse.class;
      // Operation faults initialization
      this.faultParams = new ServiceParam[1];
      this.faultParams[0] = new ServiceParam();
      this.faultParams[0].isElement = true;
      this.faultParams[0].schemaName = new QName("urn:QuickCarRentalServiceWsd/QuickCarRentalServiceVi","viewActiveBookings_com.sap.engine.examples.util.QuickCarRentalException");
      this.faultParams[0].name = "errorPart";
      this.faultParams[0].contentClass = com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException.class;
      com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookingsResponse parametersTemp;
      this.transportBinding.setTypeMappingRegistry(this.typeRegistry);
      this.transportBinding.startOperation(this.inputParams,this.outputParams,this.faultParams);
      // Binding Context initialization
      this.bindingConfiguration.clear();
      bindingConfiguration.setProperty("soapAction","");
      bindingConfiguration.setProperty("style","document");
      bindingConfiguration.setProperty("transport","http://schemas.xmlsoap.org/soap/http");
      PropertyContext bindingConfigurationX;
      bindingConfigurationX = bindingConfiguration.getSubContext("output");
      bindingConfigurationX.setProperty("operationName","viewActiveBookings");
      bindingConfigurationX.setProperty("use","literal");
      bindingConfigurationX = bindingConfiguration.getSubContext("input");
      bindingConfigurationX.setProperty("operationName","viewActiveBookings");
      bindingConfigurationX.setProperty("use","literal");
      bindingConfigurationX.setProperty("parts","parameters");
      super._fillEndpoint(bindingConfiguration);
      _buildOperationContext("viewActiveBookings",this.transportBinding);
      this.transportBinding.call(this.stubConfiguration,this.globalProtocols,_getOperationProtocols("viewActiveBookings"));
      if (this.faultParams[0].content != null) {
        throw (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException) this.faultParams[0].content;
      }
      parametersTemp = (com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookingsResponse) this.outputParams[0].content;
      return parametersTemp;
    } catch (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException e) {
      throw e;
    } catch (javax.xml.rpc.soap.SOAPFaultException e) {
      throw e;
    } catch (java.lang.Exception e) {
      throw new RemoteException("Service call exception",e);
    }
  }

  public com.sap.tut.wd.carrental.model.proxies.types.QuickBookingModel[] viewActiveBookings()  throws java.rmi.RemoteException,com.sap.tut.wd.carrental.model.proxies.QuickCarRentalException {
    com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookings _request_variable = new com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookings();
    com.sap.tut.wd.carrental.model.proxies.types.p1.ViewActiveBookingsResponse _result_variable = viewActiveBookings(_request_variable);
    return _result_variable.getResponse();
  }


}
