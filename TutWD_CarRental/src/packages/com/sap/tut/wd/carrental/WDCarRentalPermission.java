/*
 * Created on 24.02.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.tut.wd.carrental;

import com.sap.security.api.permissions.NamePermission;

/**
 * @author SAP AG
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class WDCarRentalPermission extends NamePermission {

	/**
	 * @param name
	 */
	public WDCarRentalPermission(String name) {
		super(name);
	}

	/**
	 * @param name
	 * @param action
	 */
	public WDCarRentalPermission(String name, String action) {
		super(name, action);
	}

}
