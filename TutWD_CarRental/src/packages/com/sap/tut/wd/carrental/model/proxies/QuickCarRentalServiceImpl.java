package com.sap.tut.wd.carrental.model.proxies;

public class QuickCarRentalServiceImpl extends com.sap.engine.services.webservices.jaxrpc.wsdl2java.ServiceBase implements com.sap.tut.wd.carrental.model.proxies.QuickCarRentalService {

  public QuickCarRentalServiceImpl() throws java.lang.Exception {
    super();
    java.io.InputStream input;
    input = this.getClass().getClassLoader().getResourceAsStream("com/sap/tut/wd/carrental/model/proxies/protocols.txt");
    loadProtocolsFromPropertyFile(input);
    init(this.getClass().getClassLoader().getResourceAsStream("com/sap/tut/wd/carrental/model/proxies/lports_1.xml"));
  }

  public com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument getLogicalPort(String portName) throws javax.xml.rpc.ServiceException {
    return (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument) getPort(new javax.xml.namespace.QName(null,portName),null);
  }
  public com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument getLogicalPort() throws javax.xml.rpc.ServiceException {
    return (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument) getLogicalPort(com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument.class);
  }
  public com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument getConfig1PortDocument() throws javax.xml.rpc.ServiceException {
    return (com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument) super.getPort(new javax.xml.namespace.QName(null,"Config1Port_Document"),com.sap.tut.wd.carrental.model.proxies.QuickCarRentalServiceViDocument.class);
  }


  public java.rmi.Remote getLogicalPort(String portName, Class seiClass) throws javax.xml.rpc.ServiceException {
    return super.getPort(new javax.xml.namespace.QName(null,portName),seiClass);
  }

  public java.rmi.Remote getLogicalPort(Class seiClass) throws javax.xml.rpc.ServiceException {
    return super.getLogicalPort(seiClass);
  }

}
