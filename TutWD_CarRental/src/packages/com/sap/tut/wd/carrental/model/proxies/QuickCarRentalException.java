package com.sap.tut.wd.carrental.model.proxies;

public class QuickCarRentalException extends com.sap.engine.services.webservices.jaxrpc.wsdl2java.ProxyException {
  private com.sap.tut.wd.carrental.model.proxies.types.QuickCarRentalException errorPart;

  public void init(com.sap.tut.wd.carrental.model.proxies.types.QuickCarRentalException errorPart) {
    this.errorPart=errorPart;
  }

  public QuickCarRentalException() {
    this.errorPart= new com.sap.tut.wd.carrental.model.proxies.types.QuickCarRentalException();
  }

  public Class getContentClass() {
    return com.sap.tut.wd.carrental.model.proxies.types.QuickCarRentalException.class;
  }
  public QuickCarRentalException(java.lang.String message) {
    this.errorPart= new com.sap.tut.wd.carrental.model.proxies.types.QuickCarRentalException();
    this.errorPart.setMessage(message);
  }

  public java.lang.String getMessageInternal() {
    return this.errorPart.getMessage();
  }
}

