package com.tods.xi;

public class LookupRFCException extends RuntimeException {

	public LookupRFCException() {
		super();
	}

	public LookupRFCException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public LookupRFCException(String arg0) {
		super(arg0);
	}

	public LookupRFCException(Throwable arg0) {
		super(arg0);
	}



}
