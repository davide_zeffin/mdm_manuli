package com.tods.xi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.aii.mapping.lookup.Channel;
import com.sap.aii.mapping.lookup.LookupService;
import com.sap.aii.mapping.lookup.Payload;
import com.sap.aii.mapping.lookup.RfcAccessor;
import com.sap.aii.mapping.lookup.XmlPayload;

public class LookupRFC {

	public static String Lookup(String sender, String receiver, String table_name, String input_value)
	throws Exception{

		// RFC-XML message
		String m =
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
			"<p2:ZXI_READ_TABLES xmlns:p2=\"urn:sap-com:document:sap:rfc:functions\">" +
			"<SENDER>" + sender + "</SENDER>" +
			"<RECEIVER>" + receiver + "</RECEIVER>" +
			"<TABLE_NAME>" + table_name + "</TABLE_NAME>" +
			"<INPUT_VALUE>" + input_value + "</INPUT_VALUE>" +
			"</p2:ZXI_READ_TABLE>";

		String error_text = 
			"Tabella: " + table_name +
			". Nessuna conversione possibile per chiave:" +	sender + "," + receiver + "," + input_value;

		RfcAccessor accessor = null;
		ByteArrayOutputStream out = null;
		String content = null;

		// Determine Business system name
		// Business system should be defined with the same name of SAP system (XD1/XP1)
		String businessSystem = "INTEGRATION_SERVER_" + System.getProperty("SAPSYSTEMNAME");

		// 1. Determine a channel (Business system, Communication channel)
		Channel channel = LookupService.getChannel(businessSystem,"cc_Rfc_Rcv_XI_Self");

		// 2. Get a RFC accessor for a channel.
		accessor = LookupService.getRfcAccessor(channel);

		// 3. Create a xml input stream representing the function module request message.
		InputStream inputStream = new ByteArrayInputStream(m.getBytes());

		// 4. Create xml payload
		XmlPayload payload = LookupService.getXmlPayload(inputStream);

		// 5. Execute lookup.
		Payload result = accessor.call(payload);

		InputStream in = result.getContent();
		out = new ByteArrayOutputStream(1024);
		byte[] buffer = new byte[1024];
		for (int read = in.read(buffer); read > 0;
			read = in.read(buffer)) {
			out.write(buffer, 0, read);
		}
		content = out.toString();

		// close stream
		if (out!=null) out.close();

		//7. close the accessor in order to free resources.
		if (accessor!=null) accessor.close();

		// Parsing response
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    	// create DOM structure from input XML
    	DocumentBuilder builder = factory.newDocumentBuilder();

    	Document document = builder.parse(new ByteArrayInputStream(content.getBytes()));

    	// Get output value
    	NodeList list = document.getElementsByTagName("OUTPUT_VALUE");
    	Node node = list.item(0);

    	// if found, look for the value
    	if (node == null) throw new Exception(error_text);
    	node = node.getFirstChild();

    	if (node == null) throw new Exception(error_text);

	    return node.getNodeValue();
	}
}
