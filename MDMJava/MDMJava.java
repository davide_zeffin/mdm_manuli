//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import net.sf.jasperreports.engine.JRException;
//import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import net.sf.jasperreports.engine.export.JRPdfExporter;

import a2i.cache.CatalogCache;
import a2i.common.A2iResultSet;
import a2i.common.CatalogData;
import a2i.common.ResultSetDefinition;

import a2i.core.Measurement;
import a2i.core.StringException;
import a2i.generated.MeasurementManager;
import a2i.search.FreeFormParameter;
import a2i.search.FreeFormParameterField;
import a2i.search.FreeFormTableParameter;
import a2i.search.Search;

public class MDMJava
{


	private static Map parameters = new HashMap();
	private static CatalogData catalogData;
	private static CatalogCache catalogCache;
	private static MeasurementManager measurementManager;
	private static String JASPER_REPORT_FOLDER   = "C:/Job/ManuliJasper/";
	private static String JASPER_FILENAME = "ManuliCatalogueExtended";
	private static String CACHE_DIRECTORY = "C:\\Job\\ManuliJasper";
	private static String CACHE_SERVER = "10.194.72.24";
	private static int CACHE_PORT = 2345;
	private static String CACHE_USER = "Admin";
	private static String CACHE_PASSWORD = "admin";
	//	variabili per stampa Catalogue	
	private static String Name;
	private static String catalogue_subtitle;
	private static String imagePath;
	private static String key_performance;
	private static String main_application;
	private static String min_cont_serv_temp;
	private static String max_cont_serv_temp;
	private static String cont_serv_temp_far;
	private static String cont_serv_temp_cel;
	private static String max_operating_temp;
	private static String min_external_temp;
	private static String recommended_fluids;
	private static String refrigerants;
	private static String lubrificants;
	private static String tube;
	private static String reinforcement;
	private static String cover;
	private static String applicable_specs;
	private static String type_approvals;
	private static String other_technical_notes;
	private static String standard_Packaging;
	private static String catalogue_small_exclamation;
	private static String catalogue_big_exclamation;

	//	variabili per stampa Catalogue Extended	
	private static String hose_inside_diameter_min_mm;
	private static String hose_inside_diameter_min_inches;
	private static String hose_inside_diameter_max_mm;
	private static String hose_inside_diameter_max_inches;
	private static String reinforcement_outside_diameter_min_mm;
	private static String reinforcement_outside_diameter_min_inches;
	private static String reinforcement_outside_diameter_max_mm;
	private static String reinforcement_outside_diameter_max_inches;
	private static String hose_outside_diameter_min_mm;
	private static String hose_outside_diameter_min_inches;
	private static String hose_outside_diameter_max_mm;
	private static String hose_outside_diameter_max_inches;
	private static String hose_max_wall_thickness_differences_mm;
	private static String hose_max_wall_thickness_differences_inches;
	//	variabili per stampa Catalogue Extended	

	//	variabili per stampa Marcature
	private static String brand_pack;
	private static String brand_customer;
	private static String marking_technology;
	private static String brand_color;
	private static String brand_logo;
	private static String brand_notes;
	private static String brand_data_type;
	private static String department;
	private static String brand;
	//	variabili per stampa Marcature

	private static String paramTableHeader = "prod_hoses";
	private static String paramTableItems;
	private static String paramValue;
	private static String paramCatalog = "false";
	private static String paramCategory = "false";
	private static String paramBrand = "";
	private static String paramType = "3";

	private static String fld01String;
	private static String fld02String;
	private static float fld02Float;
	private static String fld03String;
	private static String fld04String;
	private static float fld05Float;
	private static float fld06Float;
	private static float fld07Float;
	private static float fld08Float;
	private static float fld09Float;
	private static float fld10Float;
	private static float fld11Float;
	private static float fld12Float;
	private static float fld13Float;
	private static float fld14Float;
	private static float fld15Float;
	private static float fld16Float;
	private static String fld17String;
	private static String fld18String;
	private static String fld19String;
	//	variabili per stampa Catalogue Extended	
	private static float fld20Float;
	private static float fld21Float;
	private static float fld22Float;
	private static float fld23Float;
	private static float fld24Float;
	private static float fld25Float;
	private static float fld26Float;
	private static float fld27Float;
	private static float fld28Float;
	private static float fld29Float;
	private static float fld30Float;
	private static float fld31Float;
	private static float fld32Float;
	private static float fld33Float;
	//	variabili per stampa Catalogue Extended	

	//	variabili per stampa Catalogue Marcature	
	private static String fld34String;
	private static String fld35String;
	private static String fld36String;
	private static String fld37String;
	private static String fld38String;
	private static String fld39String;
	private static String fld40String;
	private static String fld41String;
	private static String fld42String;
	private static String fld43String;
	//	variabili per stampa Catalogue Marcature	

	private static String footer_left;
	private static String footer_right;
	static String addrow = "";
	static Vector recs = new Vector();
	static Vector recsNew = new Vector();
	static String DynamicParameter;

	
	public static void main(String[] args) {
		catalogData = new CatalogData();
		//		inizializzazione variabili		
		recs = new Vector();
		recsNew = new Vector();
		DynamicParameter = null;

		mdmlogin();

		DynamicParameter =
			"stampa=1&table=prod_hoses_rec&Name=PUSHFIT&brand_pack=M099&IS_NOT_NULL(Tree)=&catalogo=true&category=true";

		try {
			//			DynamicParameter = request.getParameter("DynamicParameter");
			int indexType = DynamicParameter.indexOf("stampa=");
			int indexItems = DynamicParameter.indexOf("table=");
			int indexValue = DynamicParameter.indexOf("Name=");
			int indexBrand = DynamicParameter.indexOf("brand_pack=");
			int indexNull = DynamicParameter.indexOf("IS_NOT_NULL(Tree)=");
			int indexCatalog = DynamicParameter.indexOf("catalogo=");
			int indexCategory = DynamicParameter.indexOf("category=");
			//paramTableHeader.substring(0,11);
			paramType =
				DynamicParameter.substring(indexType + 7, indexItems - 1);
			paramTableItems =
				DynamicParameter.substring(indexItems + 6, indexValue - 1);
			paramValue =
				DynamicParameter.substring(indexValue + 5, indexBrand - 1);
			paramBrand =
				DynamicParameter.substring(indexBrand + 11, indexNull - 1);
			paramCatalog =
				DynamicParameter.substring(indexCatalog + 9, indexCategory - 1);
			paramCategory = DynamicParameter.substring(indexCategory + 9);

		} catch (Exception e) {
			System.out.print("Wrong parameters!!! " + e.getMessage());
		}

		mdmGetProdHoses();
		mdmGetProdHosesRec();
		mdmGetSortItems();
		mdmJSaddValues();
		mdmJSprintValues();
		mdmGetDataOutput();
		mdmlogout();
	}


//	-------------- Login MDM Server -------------------------------------------
public static void mdmJSprintValues() {
	//	rendering e generazione del file PDF
	JRBeanCollectionDataSource datasource =
		new JRBeanCollectionDataSource(recsNew);

	JasperPrint jp;
	if (paramType.equals("1")) {
		JASPER_FILENAME = "ManuliCatalogue";
	} else if (paramType.equals("2")) {
		JASPER_FILENAME = "ManuliCatalogueExtended";
	} else {
		JASPER_FILENAME = "ManuliCatalogueMarcature";

	}

	try {
		jp =
			JasperFillManager.fillReport(
				JASPER_REPORT_FOLDER + JASPER_FILENAME + ".jasper",
				parameters,
				datasource);
		//						 simpleDS);
		JasperExportManager.exportReportToPdfFile(
			jp,
			JASPER_REPORT_FOLDER + JASPER_FILENAME + ".pdf");
	} catch (JRException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}



}
	
	
	//	-------------- Login MDM Server -------------------------------------------
	public static void mdmlogin() {
		System.out.print("CACHE_SERVER: " + CACHE_SERVER + "\n");
		System.out.print("CACHE_PORT: " + CACHE_PORT + "\n");
		System.out.print("CACHE_USER: " + CACHE_USER + "\n");
		System.out.print("CACHE_PASSWORD: " + CACHE_PASSWORD + "\n");

		try {
			catalogData.Login(
				CACHE_SERVER,
				CACHE_PORT,
				CACHE_USER,
				CACHE_PASSWORD,
				"English [US]");
			measurementManager = catalogData.GetMeasurements();
			System.out.print("Ok login: " + "\n");
		} catch (Exception e) {
			System.out.print("Error login: " + e.getMessage());
		}
	}
	//	-------------- LogOut MDM Server ------------------------------------------
	public static void mdmlogout() {

		try {
			catalogData.Logout();
			System.out.print("Ok logout: " + "\n");
		} catch (Exception e) {
			System.out.print("Error Logout: " + e.getMessage());
		}

	}
	//	-------------- Retrieve data from prod_hoses ------------------------------
	public static void mdmGetProdHoses() {

		if (paramType.equals("1") || paramType.equals("2")) {

			// create a ResultSetDefinition on the prod_hoses table
			ResultSetDefinition rsd_prod_hoses;
			rsd_prod_hoses = new ResultSetDefinition(paramTableHeader);
			rsd_prod_hoses.AddField("Name");
			rsd_prod_hoses.AddField("catalogue_subtitle");
			rsd_prod_hoses.AddField("catalogue_image");
			rsd_prod_hoses.AddField("key_performance");

			rsd_prod_hoses.AddField("main_application");
			rsd_prod_hoses.AddField("min_cont_serv_temp");
			rsd_prod_hoses.AddField("max_cont_serv_temp");
			rsd_prod_hoses.AddField("max_operating_temp");
			rsd_prod_hoses.AddField("min_external_temp");
			rsd_prod_hoses.AddField("recommended_fluids");
			rsd_prod_hoses.AddField("refrigerants");
			rsd_prod_hoses.AddField("lubrificants");
			rsd_prod_hoses.AddField("tube");
			rsd_prod_hoses.AddField("reinforcement");
			rsd_prod_hoses.AddField("cover");
			rsd_prod_hoses.AddField("applicable_specs");
			rsd_prod_hoses.AddField("type_approvals");
			rsd_prod_hoses.AddField("other_technical_notes");
			rsd_prod_hoses.AddField("standard_Packaging");
			rsd_prod_hoses.AddField("catalogue_small_exclamation");
			rsd_prod_hoses.AddField("catalogue_big_exclamation");
			rsd_prod_hoses.AddField("tree");

			// create an empty Search object
			Search search_prod_hoses = new Search(rsd_prod_hoses.GetTable());

			// Filters		
			FreeFormTableParameter fftp_H =
				search_prod_hoses.GetParameters().NewFreeFormTableParameter(
					rsd_prod_hoses.GetTable());
			FreeFormParameterField ffpf_H_Key01 =
				fftp_H.GetFields().New("Name");
			ffpf_H_Key01.GetFreeForm().NewString(
				paramValue,
				FreeFormParameter.EqualToSearchType);
			FreeFormParameterField ffpf_H_Key02 =
				fftp_H.GetFields().New("tree");
			ffpf_H_Key02.GetFreeForm().NewString(
				"Yes",
				FreeFormParameter.NotNullSearchType);

			// Printout Fields 
			FreeFormParameterField ffpf_H_Field00 =
				fftp_H.GetFields().New("Name");
			FreeFormParameterField ffpf_H_Field01 =
				fftp_H.GetFields().New("catalogue_subtitle");
			FreeFormParameterField ffpf_H_Field02 =
				fftp_H.GetFields().New("catalogue_image");
			FreeFormParameterField ffpf_H_Field03 =
				fftp_H.GetFields().New("key_performance");
			FreeFormParameterField ffpf_H_Field04 =
				fftp_H.GetFields().New("main_application");
			FreeFormParameterField ffpf_H_Field05 =
				fftp_H.GetFields().New("min_cont_serv_temp");
			FreeFormParameterField ffpf_H_Field06 =
				fftp_H.GetFields().New("max_cont_serv_temp");
			FreeFormParameterField ffpf_H_Field07 =
				fftp_H.GetFields().New("max_operating_temp");
			FreeFormParameterField ffpf_H_Field08 =
				fftp_H.GetFields().New("min_external_temp");
			FreeFormParameterField ffpf_H_Field09 =
				fftp_H.GetFields().New("recommended_fluids");
			FreeFormParameterField ffpf_H_Field10 =
				fftp_H.GetFields().New("refrigerants");
			FreeFormParameterField ffpf_H_Field11 =
				fftp_H.GetFields().New("lubrificants");
			FreeFormParameterField ffpf_H_Field12 =
				fftp_H.GetFields().New("tube");
			FreeFormParameterField ffpf_H_Field13 =
				fftp_H.GetFields().New("reinforcement");
			FreeFormParameterField ffpf_H_Field14 =
				fftp_H.GetFields().New("cover");
			FreeFormParameterField ffpf_H_Field15 =
				fftp_H.GetFields().New("applicable_specs");
			FreeFormParameterField ffpf_H_Field16 =
				fftp_H.GetFields().New("type_approvals");
			FreeFormParameterField ffpf_H_Field17 =
				fftp_H.GetFields().New("other_technical_notes");
			FreeFormParameterField ffpf_H_Field18 =
				fftp_H.GetFields().New("standard_Packaging");
			FreeFormParameterField ffpf_H_Field19 =
				fftp_H.GetFields().New("catalogue_small_exclamation");
			FreeFormParameterField ffpf_H_Field20 =
				fftp_H.GetFields().New("catalogue_big_exclamation");
			FreeFormParameterField ffpf_H_Field21 =
				fftp_H.GetFields().New("tree");

			try {
				A2iResultSet rs_H =
					catalogData.GetResultSet(
						search_prod_hoses,
						rsd_prod_hoses,
						null,
						true,
						0);
				for (int i = 0; i < rs_H.GetRecordCount(); i++) {
					if (!rs_H.GetValueAt(i, "tree").IsNull()) {

						// H_Field00
						if (!rs_H.GetValueAt(i, "Name").IsNull()) {
							Name = rs_H.GetValueAt(i, "Name").GetStringValue();
						}else{
							Name = null;
						}
						// H_Field01
						if (!rs_H
							.GetValueAt(i, "catalogue_subtitle")
							.IsNull()) {
							catalogue_subtitle =
								rs_H
									.GetValueAt(i, "catalogue_subtitle")
									.GetStringValue();
						}else{
							catalogue_subtitle = null;
						}
						// H_Field02
						if (!rs_H.GetValueAt(i, "catalogue_image").IsNull()) {
							CatalogCache catalogCache = new CatalogCache();
							catalogCache.Init(
								"10.194.72.24",
								2345,
								"Admin",
								"admin",
								CACHE_DIRECTORY,
								"English [US]");

							Object id =
								rs_H.GetValueAt(i, "catalogue_image").GetData();
							int idx = id.hashCode();
							imagePath =
								catalogCache.GetImagePath(
									"Images",
									"Original",
									idx);
							catalogCache.Shutdown();
							System.out.print("Passato!!!!!!!!!" + imagePath + "\n");
						}else{
							System.out.print("Passato!!!!!!!!!" + imagePath + "\n");
							imagePath = null;
					}
						// H_Field03
						if (!rs_H.GetValueAt(i, "key_performance").IsNull()) {
							key_performance =
								rs_H
									.GetValueAt(i, "key_performance")
									.GetStringValue();
						}else{
							key_performance = null;
						}
						// H_Field04
						if (!rs_H.GetValueAt(i, "main_application").IsNull()) {
							main_application =
								rs_H
									.GetValueAt(i, "main_application")
									.GetStringValue();
						}else{
							main_application = null;
						}
						// H_Field05
						if (!rs_H
							.GetValueAt(i, "min_cont_serv_temp")
							.IsNull()) {
							min_cont_serv_temp =
								rs_H
									.GetValueAt(i, "min_cont_serv_temp")
									.GetStringValue();
						}else{
							min_cont_serv_temp = null;
						}
						// H_Field06
						if (!rs_H
							.GetValueAt(i, "max_cont_serv_temp")
							.IsNull()) {
							max_cont_serv_temp =
								rs_H
									.GetValueAt(i, "max_cont_serv_temp")
									.GetStringValue();
						}else{
							max_cont_serv_temp = null;
						}
						// H_Field07
						if (rs_H.GetValueAt(i, "max_operating_temp").IsNull()
							!= true) {
							max_operating_temp =
								rs_H
									.GetValueAt(i, "max_operating_temp")
									.GetStringValue();
						}else{
							max_operating_temp = null;
						}
						// H_Field08
						if (rs_H.GetValueAt(i, "min_external_temp").IsNull()
							!= true) {
							min_external_temp =
								rs_H
									.GetValueAt(i, "min_external_temp")
									.GetStringValue();
						}else{
							min_external_temp = null;
						}
						// H_Field09
						if (rs_H.GetValueAt(i, "recommended_fluids").IsNull()
							!= true) {
							recommended_fluids =
								rs_H
									.GetValueAt(i, "recommended_fluids")
									.GetStringValue();
						}else{
							recommended_fluids = null;
						}
						// H_Field10
						if (rs_H.GetValueAt(i, "refrigerants").IsNull()
							!= true) {
							refrigerants =
								rs_H
									.GetValueAt(i, "refrigerants")
									.GetStringValue();
						}else{
							refrigerants = null;
						}
						// H_Field11
						if (rs_H.GetValueAt(i, "lubrificants").IsNull()
							!= true) {
							lubrificants =
								rs_H
									.GetValueAt(i, "lubrificants")
									.GetStringValue();
						}else{
							lubrificants = null;
						}
						// H_Field12
						if (rs_H.GetValueAt(i, "tube").IsNull() != true) {
							tube = rs_H.GetValueAt(i, "tube").GetStringValue();
						}else{
							tube = null;
						}

						// H_Field13
						if (rs_H.GetValueAt(i, "reinforcement").IsNull()
							!= true) {
							reinforcement =
								rs_H
									.GetValueAt(i, "reinforcement")
									.GetStringValue();
						}else{
							reinforcement = null;
						}
						// H_Field14
						if (rs_H.GetValueAt(i, "cover").IsNull() != true) {
							cover =
								rs_H.GetValueAt(i, "cover").GetStringValue();
						}else{
							cover = null;
						}
						// H_Field15
						if (rs_H.GetValueAt(i, "applicable_specs").IsNull()
							!= true) {
							applicable_specs =
								rs_H
									.GetValueAt(i, "applicable_specs")
									.GetStringValue();
						}else{
							applicable_specs = null;
						}
						// H_Field16
						if (rs_H.GetValueAt(i, "type_approvals").IsNull()
							!= true) {
							type_approvals =
								rs_H
									.GetValueAt(i, "type_approvals")
									.GetStringValue();
						}else{
							type_approvals = null;
						}
						// H_Field17
						if (rs_H
							.GetValueAt(i, "other_technical_notes")
							.IsNull()
							!= true) {
							other_technical_notes =
								rs_H
									.GetValueAt(i, "other_technical_notes")
									.GetStringValue();
						}else{
							other_technical_notes = null;
						}
						// H_Field18
						if (rs_H.GetValueAt(i, "standard_Packaging").IsNull()
							!= true) {
							standard_Packaging =
								rs_H
									.GetValueAt(i, "standard_Packaging")
									.GetStringValue();
						}else{
							standard_Packaging = null;
						}
						// H_Field19
						if (rs_H
							.GetValueAt(i, "catalogue_small_exclamation")
							.IsNull()
							!= true) {
							catalogue_small_exclamation =
								rs_H
									.GetValueAt(
										i,
										"catalogue_small_exclamation")
									.GetStringValue();
						}else{
							catalogue_small_exclamation = null;
						}
						// H_Field20
						if (rs_H
							.GetValueAt(i, "catalogue_big_exclamation")
							.IsNull()
							!= true) {
							catalogue_big_exclamation =
								rs_H
									.GetValueAt(i, "catalogue_big_exclamation")
									.GetStringValue();
						}else{
							catalogue_big_exclamation = null;
						}
					}
				}

			} catch (StringException e) {
				e.printStackTrace();
			}
		}
	}
	//	-------------- Retrieve data from prod_hoses_rec --------------------------

	public static void mdmGetProdHosesRec() {

		//		  create a ResultSetDefinition on the prod_hoses_rec table
		ResultSetDefinition rsd_prod_hoses_rec;
		rsd_prod_hoses_rec = new ResultSetDefinition(paramTableItems);
		rsd_prod_hoses_rec.AddField("Name");
		rsd_prod_hoses_rec.AddField("line");
		rsd_prod_hoses_rec.AddField("nominal_size");
		rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_mm");
		rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_inch");
		rsd_prod_hoses_rec.AddField("outside_diameter_mm");
		rsd_prod_hoses_rec.AddField("outside_diameter_inch");
		rsd_prod_hoses_rec.AddField("Max_working_pressure_bar");
		rsd_prod_hoses_rec.AddField("max_working_pressure_psi");
		rsd_prod_hoses_rec.AddField("burst_pressure_bar");
		rsd_prod_hoses_rec.AddField("burst_pressure_psi");
		rsd_prod_hoses_rec.AddField("min_bend_radius_mm");
		rsd_prod_hoses_rec.AddField("min_bend_radius_inch");
		rsd_prod_hoses_rec.AddField("weight_g_m");
		rsd_prod_hoses_rec.AddField("weight_lbs_ft");
		rsd_prod_hoses_rec.AddField("standard_ferrule");
		rsd_prod_hoses_rec.AddField("standard_insert");
		rsd_prod_hoses_rec.AddField("standard_mf3000");

		//		  variabili per stampa Catalogue Extended	
		rsd_prod_hoses_rec.AddField("hose_inside_diameter_min_mm");
		rsd_prod_hoses_rec.AddField("hose_inside_diameter_min_inches");
		rsd_prod_hoses_rec.AddField("hose_inside_diameter_max_mm");
		rsd_prod_hoses_rec.AddField("hose_inside_diameter_max_inches");
		rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_min_mm");
		rsd_prod_hoses_rec.AddField(
			"reinforcement_outside_diameter_min_inches");
		rsd_prod_hoses_rec.AddField("reinforcement_outside_diameter_max_mm");
		rsd_prod_hoses_rec.AddField(
			"reinforcement_outside_diameter_max_inches");
		rsd_prod_hoses_rec.AddField("hose_outside_diameter_min_mm");
		rsd_prod_hoses_rec.AddField("hose_outside_diameter_min_inches");
		rsd_prod_hoses_rec.AddField("hose_outside_diameter_max_mm");
		rsd_prod_hoses_rec.AddField("hose_outside_diameter_max_inches");
		rsd_prod_hoses_rec.AddField("hose_max_wall_thickness_difference_mm");
		rsd_prod_hoses_rec.AddField(
			"hose_max_wall_thickness_difference_inches");
		//		  variabili per stampa Catalogue Extended	

		//		  variabili per stampa Marcature
		rsd_prod_hoses_rec.AddField("category");
		rsd_prod_hoses_rec.AddField("brand_pack");
		rsd_prod_hoses_rec.AddField("brand_customer");
		rsd_prod_hoses_rec.AddField("marking_technology");
		rsd_prod_hoses_rec.AddField("brand_color");
		rsd_prod_hoses_rec.AddField("brand_logo");
		rsd_prod_hoses_rec.AddField("brand_notes");
		rsd_prod_hoses_rec.AddField("brand_date_type");
		rsd_prod_hoses_rec.AddField("brand_customer");
		rsd_prod_hoses_rec.AddField("department");
		rsd_prod_hoses_rec.AddField("brand");
		//		  variabili per stampa Marcature

		rsd_prod_hoses_rec.AddField("category");
		rsd_prod_hoses_rec.AddField("Printable");
		rsd_prod_hoses_rec.AddField("public");
		rsd_prod_hoses_rec.AddField("Phase");

		// create an empty Search object
		Search search = new Search(rsd_prod_hoses_rec.GetTable());
		FreeFormTableParameter fftpNames =
			search.GetParameters().NewFreeFormTableParameter(
				rsd_prod_hoses_rec.GetTable());

		// Filters		
		if (paramType.equals("3")) {
			FreeFormParameterField ffpfKey01 =
				fftpNames.GetFields().New("brand_pack");
			ffpfKey01.GetFreeForm().NewString(
				paramBrand,
				FreeFormParameter.EqualToSearchType);
			if (paramCategory.equals("false")) {
				FreeFormParameterField ffpfKey02 =
					fftpNames.GetFields().New("category");
				ffpfKey02.GetFreeForm().NewString(
					paramValue,
					FreeFormParameter.EqualToSearchType);
			}
		} else {
			FreeFormParameterField ffpfKey01 =
				fftpNames.GetFields().New("category");
			ffpfKey01.GetFreeForm().NewString(
				paramValue,
				FreeFormParameter.EqualToSearchType);
			if (paramCatalog.equals("false")) {
				FreeFormParameterField ffpfKey03 =
					fftpNames.GetFields().New("catalogue");
				ffpfKey03.GetFreeForm().NewString(
					"YES",
					FreeFormParameter.SubstringSearchType);
			}

		}

		//			FreeFormParameterField ffpfKey03 = fftpNames.GetFields().New("public");
		//			ffpfKey03.GetFreeForm().NewString("YES", FreeFormParameter.SubstringSearchType);
		FreeFormParameterField ffpfKey04 = fftpNames.GetFields().New("Phase");
		ffpfKey04.GetFreeForm().NewString(
			"BLOCKED",
			FreeFormParameter.NotEqualToSearchType);

		//		  Printout Fields 
		FreeFormParameterField ffpfField00 = fftpNames.GetFields().New("Name");
		FreeFormParameterField ffpfField01 = fftpNames.GetFields().New("line");
		FreeFormParameterField ffpfField02 =
			fftpNames.GetFields().New("nominal_size");
		FreeFormParameterField ffpfField03 =
			fftpNames.GetFields().New("reinforcement_outside_diameter_mm");
		FreeFormParameterField ffpfField04 =
			fftpNames.GetFields().New("reinforcement_outside_diameter_inch");
		FreeFormParameterField ffpfField05 =
			fftpNames.GetFields().New("outside_diameter_mm");
		FreeFormParameterField ffpfField06 =
			fftpNames.GetFields().New("outside_diameter_inch");
		FreeFormParameterField ffpfField07 =
			fftpNames.GetFields().New("Max_working_pressure_bar");
		FreeFormParameterField ffpfField08 =
			fftpNames.GetFields().New("max_working_pressure_psi");
		FreeFormParameterField ffpfField09 =
			fftpNames.GetFields().New("burst_pressure_bar");
		FreeFormParameterField ffpfField10 =
			fftpNames.GetFields().New("burst_pressure_psi");
		FreeFormParameterField ffpfField11 =
			fftpNames.GetFields().New("min_bend_radius_mm");
		FreeFormParameterField ffpfField12 =
			fftpNames.GetFields().New("min_bend_radius_inch");
		FreeFormParameterField ffpfField13 =
			fftpNames.GetFields().New("weight_g_m");
		FreeFormParameterField ffpfField14 =
			fftpNames.GetFields().New("weight_lbs_ft");
		FreeFormParameterField ffpfField15 =
			fftpNames.GetFields().New("standard_ferrule");
		FreeFormParameterField ffpfField16 =
			fftpNames.GetFields().New("standard_insert");
		FreeFormParameterField ffpfField17 =
			fftpNames.GetFields().New("standard_mf3000");

		//		  variabili per stampa Catalogue Extended	
		FreeFormParameterField ffpfField18 =
			fftpNames.GetFields().New("hose_inside_diameter_min_mm");
		FreeFormParameterField ffpfField19 =
			fftpNames.GetFields().New("hose_inside_diameter_min_inches");
		FreeFormParameterField ffpfField20 =
			fftpNames.GetFields().New("hose_inside_diameter_max_mm");
		FreeFormParameterField ffpfField21 =
			fftpNames.GetFields().New("hose_inside_diameter_max_inches");
		FreeFormParameterField ffpfField22 =
			fftpNames.GetFields().New("reinforcement_outside_diameter_min_mm");
		FreeFormParameterField ffpfField23 =
			fftpNames.GetFields().New(
				"reinforcement_outside_diameter_min_inches");
		FreeFormParameterField ffpfField24 =
			fftpNames.GetFields().New("reinforcement_outside_diameter_max_mm");
		FreeFormParameterField ffpfField25 =
			fftpNames.GetFields().New(
				"reinforcement_outside_diameter_max_inches");
		FreeFormParameterField ffpfField26 =
			fftpNames.GetFields().New("hose_outside_diameter_min_mm");
		FreeFormParameterField ffpfField27 =
			fftpNames.GetFields().New("hose_outside_diameter_min_inches");
		FreeFormParameterField ffpfField28 =
			fftpNames.GetFields().New("hose_outside_diameter_max_mm");
		FreeFormParameterField ffpfField29 =
			fftpNames.GetFields().New("hose_outside_diameter_max_inches");
		FreeFormParameterField ffpfField30 =
			fftpNames.GetFields().New("hose_max_wall_thickness_difference_mm");
		FreeFormParameterField ffpfField31 =
			fftpNames.GetFields().New(
				"hose_max_wall_thickness_difference_inches");
		//		  variabili per stampa Catalogue Extended	

		//		  variabili per stampa Marcature
		FreeFormParameterField ffpfField32 =
			fftpNames.GetFields().New("category");
		FreeFormParameterField ffpfField33 =
			fftpNames.GetFields().New("brand_pack");
		FreeFormParameterField ffpfField34 =
			fftpNames.GetFields().New("brand_customer");
		FreeFormParameterField ffpfField35 =
			fftpNames.GetFields().New("marking_technology");
		FreeFormParameterField ffpfField36 =
			fftpNames.GetFields().New("brand_color");
		FreeFormParameterField ffpfField37 =
			fftpNames.GetFields().New("brand_logo");
		FreeFormParameterField ffpfField38 =
			fftpNames.GetFields().New("brand_notes");
		FreeFormParameterField ffpfField39 =
			fftpNames.GetFields().New("brand_date_type");
		FreeFormParameterField ffpfField40 =
			fftpNames.GetFields().New("brand_customer");
		FreeFormParameterField ffpfField41 =
			fftpNames.GetFields().New("department");
		FreeFormParameterField ffpfField42 = fftpNames.GetFields().New("brand");
		//		  variabili per stampa Marcature

		// Ordinamento records
		A2iResultSet rs;
		try {
			Object fields = null;
			Object locale = null;
			if (paramType.equals("3")) {
				rs =
					catalogData.GetResultSet(
						search,
						rsd_prod_hoses_rec,
						ffpfField32.GetName(),
						true,
						0);

			} else {
				rs =
					catalogData.GetResultSet(
						search,
						rsd_prod_hoses_rec,
						ffpfField01.GetName(),
						true,
						0);

			}
			//			Vector recs = new Vector();

			for (int i = 0; i < rs.GetRecordCount(); i++) {
				// Field01  PART.REF.
				fld01String = rs.GetValueAt(i, "line").GetStringValue();

				int idx1 =
					rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
						",",
						0);
				int idx2 =
					rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
						",",
						idx1 + 1);
				int idx3 =
					rs.GetValueAt(i, "nominal_size").GetStringValue().indexOf(
						",",
						idx2 + 1);
				int idx4 =
					rs.GetValueAt(i, "nominal_size").GetStringValue().length();

				// Field02 HOSE SIZE DN
				fld02String =
					rs
						.GetValueAt(i, "nominal_size")
						.GetStringValue()
						.substring(
						0,
						idx1);
				fld02Float = Integer.parseInt(fld02String);
				// Field03 HOSE SIZE dash
				fld03String =
					rs
						.GetValueAt(i, "nominal_size")
						.GetStringValue()
						.substring(
						idx1 + 1,
						idx2);
				// Field04 HOSE SIZE mm
				//fld04String = rs.GetValueAt(i, "nominal_size").GetStringValue().substring(idx2 + 1, idx3);
				// Field04 HOSE SIZE inch
				fld04String =
					rs
						.GetValueAt(i, "nominal_size")
						.GetStringValue()
						.substring(
						idx3 + 1,
						idx4);

				// Field05
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_mm")
					.IsNull()
					!= true) {
					Measurement fldMeas05 =
						rs
							.GetValueAt(i, "reinforcement_outside_diameter_mm")
							.GetMeasurement();
					String fld05String =
						measurementManager.GetString(
							fldMeas05.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld05String = fld05String.replaceAll(",", ".");
					fld05Float = Float.valueOf(fld05String).floatValue();
				} else {
					fld05Float = 0;

				}

				// Field06
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_inch")
					.IsNull()
					!= true) {
					Measurement fldMeas06 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_inch")
							.GetMeasurement();
					String fld06String =
						measurementManager.GetString(
							fldMeas06.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld06String = fld06String.replaceAll(",", ".");
					fld06Float = Float.valueOf(fld06String).floatValue();
				} else {
					fld06Float = 0;

				}
				// Field07
				if (rs.GetValueAt(i, "outside_diameter_mm").IsNull() != true) {
					Measurement fldMeas07 =
						rs
							.GetValueAt(i, "outside_diameter_mm")
							.GetMeasurement();
					String fld07String =
						measurementManager.GetString(
							fldMeas07.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("outside_diameter_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld07String = fld07String.replaceAll(",", ".");
					fld07Float = Float.valueOf(fld07String).floatValue();
				} else {
					fld07Float = 0;

				}
				// Field08
				if (rs.GetValueAt(i, "outside_diameter_inch").IsNull()
					!= true) {
					Measurement fldMeas08 =
						rs
							.GetValueAt(i, "outside_diameter_inch")
							.GetMeasurement();
					String fld08String =
						measurementManager.GetString(
							fldMeas08.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("outside_diameter_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld08String = fld08String.replaceAll(",", ".");
					fld08Float = Float.valueOf(fld08String).floatValue();
				} else {
					fld08Float = 0;

				}
				// Field09
				if (rs.GetValueAt(i, "Max_working_pressure_bar").IsNull()
					!= true) {
					Measurement fldMeas09 =
						rs
							.GetValueAt(i, "Max_working_pressure_bar")
							.GetMeasurement();
					fld09Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas09.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
				} else {
					fld09Float = 0;

				}
				// Field10
				if (rs.GetValueAt(i, "max_working_pressure_psi").IsNull()
					!= true) {
					Measurement fldMeas10 =
						rs
							.GetValueAt(i, "max_working_pressure_psi")
							.GetMeasurement();
					fld10Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas10.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
				} else {
					fld10Float = 0;

				}
				// Field11
				if (rs.GetValueAt(i, "burst_pressure_bar").IsNull() != true) {
					Measurement fldMeas11 =
						rs.GetValueAt(i, "burst_pressure_bar").GetMeasurement();
					fld11Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas11.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
				} else {
					fld11Float = 0;

				}
				// Field12
				if (rs.GetValueAt(i, "burst_pressure_psi").IsNull() != true) {
					Measurement fldMeas12 =
						rs.GetValueAt(i, "burst_pressure_psi").GetMeasurement();
					fld12Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas12.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
				} else {
					fld12Float = 0;

				}
				// Field13
				if (rs.GetValueAt(i, "min_bend_radius_mm").IsNull() != true) {
					Measurement fldMeas13 =
						rs.GetValueAt(i, "min_bend_radius_mm").GetMeasurement();
					fld13Float =
						Float
							.valueOf(
								measurementManager.GetString(
									fldMeas13.GetValue(),
									0,
									0,
									0,
									false,
									-1))
							.floatValue();
				} else {
					fld13Float = 0;

				}
				// Field14
				if (rs.GetValueAt(i, "min_bend_radius_inch").IsNull()
					!= true) {
					Measurement fldMeas14 =
						rs
							.GetValueAt(i, "min_bend_radius_inch")
							.GetMeasurement();
					String fld14String =
						measurementManager.GetString(
							fldMeas14.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("min_bend_radius_inch")
								.GetDecimalPlaces(),
							false,
							-1);
					fld14String = fld14String.replaceAll(",", ".");
					fld14Float = Float.valueOf(fld14String).floatValue();

					//				String fldMeas12String =	measurementManager.GetString(fldMeas12.GetValue(), fldMeas12.GetUnitID(),
					//																 rs.GetFieldAt("min_bend_radius_inch").GetDimensionID(),
					//																 rs.GetFieldAt("min_bend_radius_inch").GetDecimalPlaces(),
					//																 false, -1);
				} else {
					fld14Float = 0;

				}
				// Field15
				if (rs.GetValueAt(i, "weight_g_m").IsNull() != true) {
					fld15Float = rs.GetValueAt(i, "weight_g_m").GetFloatValue();
				} else {
					fld15Float = 0;

				}
				// Field16
				if (rs.GetValueAt(i, "weight_lbs_ft").IsNull() != true) {
					fld16Float =
						rs.GetValueAt(i, "weight_lbs_ft").GetFloatValue();
				} else {
					fld16Float = 0;

				}
				// Field17
				if (rs.GetValueAt(i, "standard_ferrule").IsNull() != true) {
					fld17String =
						rs.GetValueAt(i, "standard_ferrule").GetStringValue();

				} else {
					fld17String = null;

				}
				// Field18
				if (rs.GetValueAt(i, "standard_insert").IsNull() != true) {
					fld18String =
						rs.GetValueAt(i, "standard_insert").GetStringValue();
					int index = fld18String.indexOf(",");
					fld18String = fld18String.substring(0, index);
				} else {
					fld18String = null;

				}
				// Field19
				if (rs.GetValueAt(i, "standard_mf3000").IsNull() != true) {
					fld19String =
						rs.GetValueAt(i, "standard_mf3000").GetStringValue();
				} else {
					fld19String = null;

				}
				//				variabili per stampa Catalogue Extended	
				// Field20
				if (rs.GetValueAt(i, "hose_inside_diameter_min_mm").IsNull()
					!= true) {
					Measurement fldMeas20 =
						rs
							.GetValueAt(i, "hose_inside_diameter_min_mm")
							.GetMeasurement();
					String fld20String =
						measurementManager.GetString(
							fldMeas20.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_inside_diameter_min_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld20String = fld20String.replaceAll(",", ".");
					fld20Float = Float.valueOf(fld20String).floatValue();
				} else {
					fld20Float = 0;

				}
				// Field21
				if (rs
					.GetValueAt(i, "hose_inside_diameter_min_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas21 =
						rs
							.GetValueAt(i, "hose_inside_diameter_min_inches")
							.GetMeasurement();
					String fld21String =
						measurementManager.GetString(
							fldMeas21.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_inside_diameter_min_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld21String = fld21String.replaceAll(",", ".");
					fld21Float = Float.valueOf(fld21String).floatValue();
				} else {
					fld21Float = 0;

				}
				// Field22
				if (rs.GetValueAt(i, "hose_inside_diameter_max_mm").IsNull()
					!= true) {
					Measurement fldMeas22 =
						rs
							.GetValueAt(i, "hose_inside_diameter_max_mm")
							.GetMeasurement();
					String fld22String =
						measurementManager.GetString(
							fldMeas22.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_inside_diameter_max_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld22String = fld22String.replaceAll(",", ".");
					fld22Float = Float.valueOf(fld22String).floatValue();
				} else {
					fld22Float = 0;

				}
				// Field23
				if (rs
					.GetValueAt(i, "hose_inside_diameter_max_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas23 =
						rs
							.GetValueAt(i, "hose_inside_diameter_max_inches")
							.GetMeasurement();
					String fld23String =
						measurementManager.GetString(
							fldMeas23.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_inside_diameter_max_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld23String = fld23String.replaceAll(",", ".");
					fld23Float = Float.valueOf(fld23String).floatValue();
				} else {
					fld23Float = 0;

				}
				// Field24
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_min_mm")
					.IsNull()
					!= true) {
					Measurement fldMeas24 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_min_mm")
							.GetMeasurement();
					String fld24String =
						measurementManager.GetString(
							fldMeas24.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_min_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld24String = fld24String.replaceAll(",", ".");
					fld24Float = Float.valueOf(fld24String).floatValue();
				} else {
					fld24Float = 0;

				}
				// Field25
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_min_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas25 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_min_inches")
							.GetMeasurement();
					String fld25String =
						measurementManager.GetString(
							fldMeas25.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_min_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld25String = fld25String.replaceAll(",", ".");
					fld25Float = Float.valueOf(fld25String).floatValue();
				} else {
					fld25Float = 0;

				}
				// Field26
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_max_mm")
					.IsNull()
					!= true) {
					Measurement fldMeas26 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_max_mm")
							.GetMeasurement();
					String fld26String =
						measurementManager.GetString(
							fldMeas26.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_max_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld26String = fld26String.replaceAll(",", ".");
					fld26Float = Float.valueOf(fld26String).floatValue();
				} else {
					fld26Float = 0;

				}
				// Field27
				if (rs
					.GetValueAt(i, "reinforcement_outside_diameter_max_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas27 =
						rs
							.GetValueAt(
								i,
								"reinforcement_outside_diameter_max_inches")
							.GetMeasurement();
					String fld27String =
						measurementManager.GetString(
							fldMeas27.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("reinforcement_outside_diameter_max_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld27String = fld27String.replaceAll(",", ".");
					fld27Float = Float.valueOf(fld27String).floatValue();
				} else {
					fld27Float = 0;

				}
				// Field28
				if (rs.GetValueAt(i, "hose_outside_diameter_min_mm").IsNull()
					!= true) {
					Measurement fldMeas28 =
						rs
							.GetValueAt(i, "hose_outside_diameter_min_mm")
							.GetMeasurement();
					String fld28String =
						measurementManager.GetString(
							fldMeas28.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_outside_diameter_min_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld28String = fld28String.replaceAll(",", ".");
					fld28Float = Float.valueOf(fld28String).floatValue();
				} else {
					fld28Float = 0;

				}
				// Field29
				if (rs
					.GetValueAt(i, "hose_outside_diameter_min_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas29 =
						rs
							.GetValueAt(i, "hose_outside_diameter_min_inches")
							.GetMeasurement();
					String fld29String =
						measurementManager.GetString(
							fldMeas29.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_outside_diameter_min_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld29String = fld29String.replaceAll(",", ".");
					fld29Float = Float.valueOf(fld29String).floatValue();
				} else {
					fld29Float = 0;

				}
				// Field30
				if (rs.GetValueAt(i, "hose_outside_diameter_max_mm").IsNull()
					!= true) {
					Measurement fldMeas30 =
						rs
							.GetValueAt(i, "hose_outside_diameter_max_mm")
							.GetMeasurement();
					String fld30String =
						measurementManager.GetString(
							fldMeas30.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_outside_diameter_max_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld30String = fld30String.replaceAll(",", ".");
					fld30Float = Float.valueOf(fld30String).floatValue();
				} else {
					fld30Float = 0;

				}
				// Field31
				if (rs
					.GetValueAt(i, "hose_outside_diameter_max_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas31 =
						rs
							.GetValueAt(i, "hose_outside_diameter_max_inches")
							.GetMeasurement();
					String fld31String =
						measurementManager.GetString(
							fldMeas31.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_outside_diameter_max_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld31String = fld31String.replaceAll(",", ".");
					fld31Float = Float.valueOf(fld31String).floatValue();
				} else {
					fld31Float = 0;

				}
				// Field32
				if (rs
					.GetValueAt(i, "hose_max_wall_thickness_difference_mm")
					.IsNull()
					!= true) {
					Measurement fldMeas32 =
						rs
							.GetValueAt(
								i,
								"hose_max_wall_thickness_difference_mm")
							.GetMeasurement();
					String fld32String =
						measurementManager.GetString(
							fldMeas32.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_max_wall_thickness_difference_mm")
								.GetDecimalPlaces(),
							false,
							-1);
					fld32String = fld32String.replaceAll(",", ".");
					fld32Float = Float.valueOf(fld32String).floatValue();
				} else {
					fld32Float = 0;

				}
				// Field33
				if (rs
					.GetValueAt(i, "hose_max_wall_thickness_difference_inches")
					.IsNull()
					!= true) {
					Measurement fldMeas33 =
						rs
							.GetValueAt(
								i,
								"hose_max_wall_thickness_difference_inches")
							.GetMeasurement();
					String fld33String =
						measurementManager.GetString(
							fldMeas33.GetValue(),
							0,
							0,
							rs
								.GetFieldAt("hose_max_wall_thickness_difference_inches")
								.GetDecimalPlaces(),
							false,
							-1);
					fld33String = fld33String.replaceAll(",", ".");
					fld33Float = Float.valueOf(fld33String).floatValue();
				} else {
					fld33Float = 0;

				}

				//			variabili per stampa Catalogue Extended	

				//			variabili per stampa Catalogue Marcature	
				if (paramType.equals("3")) {

					// Field34
					if (rs.GetValueAt(i, "category").IsNull() != true) {
						fld34String =
							rs.GetValueAt(i, "category").GetStringValue();
					} else {
						fld34String = null;

					}
					// Field35
					if (rs.GetValueAt(i, "brand_pack").IsNull() != true) {
						fld35String =
							rs.GetValueAt(i, "brand_pack").GetStringValue();
					} else {
						fld35String = null;

					}
					// Field36
					if (rs.GetValueAt(i, "brand_customer").IsNull() != true) {
						fld36String =
							rs.GetValueAt(i, "brand_customer").GetStringValue();
					} else {
						fld36String = null;

					}
					// Field37
					if (rs.GetValueAt(i, "marking_technology").IsNull()
						!= true) {
						fld37String =
							rs
								.GetValueAt(i, "marking_technology")
								.GetStringValue();
					} else {
						fld37String = null;

					}
					// Field38
					if (rs.GetValueAt(i, "brand_color").IsNull() != true) {
						fld38String =
							rs.GetValueAt(i, "brand_color").GetStringValue();
					} else {
						fld38String = null;

					}
					// Field39
					if (paramType.equals("3")
						&& !rs.GetValueAt(i, "brand_logo").IsNull()) {
						CatalogCache catalogCache = new CatalogCache();
						catalogCache.Init(
							CACHE_SERVER,
							CACHE_PORT,
							CACHE_USER,
							CACHE_PASSWORD,
							CACHE_DIRECTORY,
							"English [US]");
						Object id = rs.GetValueAt(i, "brand_logo").GetData();
						int idx = id.hashCode();
						imagePath =
							CACHE_DIRECTORY
								+ "\\"
								+ catalogCache.GetImagePath(
									"Images",
									"Original",
									idx);
						catalogCache.Shutdown();
					} else {
						imagePath = null;

					}
					// Field40
					if (rs.GetValueAt(i, "brand_notes").IsNull() != true) {
						fld40String =
							rs.GetValueAt(i, "brand_notes").GetStringValue();
					} else {
						fld40String = null;

					}
					// Field41
					if (rs.GetValueAt(i, "brand_date_type").IsNull() != true) {
						fld41String =
							rs
								.GetValueAt(i, "brand_date_type")
								.GetStringValue();
					} else {
						fld41String = null;

					}
					// Field42
					if (rs.GetValueAt(i, "department").IsNull() != true) {
						fld42String =
							rs.GetValueAt(i, "department").GetStringValue();
					} else {
						fld42String = null;

					}
					// Field43
					if (rs.GetValueAt(i, "brand").IsNull() != true) {
						fld43String =
							rs.GetValueAt(i, "brand").GetStringValue();
					} else {
						fld43String = null;

					}
				}

				if (paramType.equals("3")) {
					recs.add(new prod_hoses_rec_marcature(fld34String,
					// marcatura
					fld37String, // Marking Technology
					imagePath, // Brand Logo
					fld38String, // Brand Color
					fld41String, // Brand Date type
					fld42String, // Department
					fld40String, // Brand Note
					fld01String, // Line
					fld02Float, // DN
					fld03String, // Dash
					fld04String, // inch
					fld43String)); // Brand

				} else {
					recs.add(
						new prod_hoses_rec(
							fld01String,
							fld02Float,
							fld03String,
							fld04String,
							fld05Float,
							fld06Float,
							fld07Float,
							fld08Float,
							fld09Float,
							fld10Float,
							fld11Float,
							fld12Float,
							fld13Float,
							fld14Float,
							fld15Float,
							fld16Float,
							fld17String,
							fld18String,
							fld19String,
							fld20Float,
							fld21Float,
							fld22Float,
							fld23Float,
							fld24Float,
							fld25Float,
							fld26Float,
							fld27Float,
							fld28Float,
							fld29Float,
							fld30Float,
							fld31Float,
							fld32Float,
							fld33Float));

				}

			}

		} catch (StringException e) {
			e.getStackTrace();
		}
	}

	//	-------------- Sort items  --------------------------------------------
	public static void mdmGetSortItems() {

		String field1 = null;
		String field8 = null;
		int i;
		if (paramType.equals("1") || paramType.equals("2")) {
			Collections.sort(recs, new lineOrder());
			for (i = 0; i < recs.size(); i++) {
				String fld01String = ((prod_hoses_rec)recs.get(i)).getField01();
				if (field1 == null || !field1.equals(fld01String)) {
					recsNew.add(recs.get(i));
				}
				field1 = fld01String;
			}

			Collections.sort(recsNew, new sizeOrder());
		} else {

			Collections.sort(recs, new lineOrderMarc());
			for (i = 0; i < recs.size(); i++) {
				String fld08String = ((prod_hoses_rec_marcature)recs.get(i)).getField08();
				if (field8 == null || !field8.equals(fld08String)) {
					recsNew.add(recs.get(i));
				}
				field8 = fld08String;
			}
			Collections.sort(recsNew, new sizeOrderMarc());

		}
	}
	//	-------------- add values to JasperReport -----------------------------
	public static void mdmJSaddValues() {

		if (paramType.equals("1") || paramType.equals("2")) {

			//			  TESTATA			
			parameters.put("home_dir", CACHE_DIRECTORY);
			parameters.put("printable_name", Name);
			parameters.put("catalogue_subtitle", catalogue_subtitle);
			if (imagePath != null) {
				parameters.put(
					"catalogue_image",
					CACHE_DIRECTORY + "\\" + imagePath);
			}

			//			  PIE' PAGINA CENTRALE			
			parameters.put("key_performance", key_performance);
			//			  PIE' PAGINA LATO SINISTRO	
			if (paramType.equals("1")) {
				addrow = "\n";
			} else {
				addrow = "";

			}

			int i = 100;
			footer_left = "footer_left" + Integer.toString(i);
			if (main_application != null) {
				parameters.put(footer_left, "MAIN APPLICATIONS");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(footer_left, main_application + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}

			if (min_cont_serv_temp != null && max_cont_serv_temp != null) {
				int idx1 = min_cont_serv_temp.indexOf(",", 0);
				int idx2 = max_cont_serv_temp.indexOf(",", 0);
				cont_serv_temp_cel =
					min_cont_serv_temp.substring(0, idx1)
						+ " / "
						+ max_cont_serv_temp.substring(0, idx2);
				cont_serv_temp_far =
					min_cont_serv_temp.substring(idx1 + 2)
						+ " / "
						+ max_cont_serv_temp.substring(idx2 + 1);

				parameters.put(
					footer_left,
					"CONTINUOUS SERVICE TEMPERATURE RANGE");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(
					footer_left,
					cont_serv_temp_far + "\n" + cont_serv_temp_cel + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}

			if (max_operating_temp != null) {
				parameters.put(footer_left, "MAX OPERATING TEMPERATURE");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(footer_left, max_operating_temp + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}
			if (min_external_temp != null) {
				parameters.put(footer_left, "MIN EXTERNAL TEMPERATURE");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(footer_left, min_external_temp + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}
			if (recommended_fluids != null) {
				parameters.put(footer_left, "RECOMMENDED FLUIDS");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(footer_left, recommended_fluids + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}
			if (refrigerants != null) {
				parameters.put(footer_left, "REFRIGERANTS");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(footer_left, refrigerants + addrow);
				++i;
				footer_left = "footer_left" + Integer.toString(i);
			}
			if (lubrificants != null) {
				parameters.put(footer_left, "LUBRIFICANTS");
				++i;
				footer_left = "footer_left" + Integer.toString(i);

				parameters.put(footer_left, lubrificants + addrow);
			}

			//			  PIE' PAGINA LATO DESTRO	
			i = 100;
			footer_right = "footer_right" + Integer.toString(i);
			if (tube != null) {
				parameters.put(footer_right, "TUBE");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, tube + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}
			if (reinforcement != null) {
				parameters.put(footer_right, "REINFORCEMENT");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, reinforcement + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}

			if (cover != null) {
				parameters.put(footer_right, "COVER");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, cover + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}
			if (applicable_specs != null) {
				parameters.put(footer_right, "APPLICABLE SPECS");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, applicable_specs + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}
			if (type_approvals != null) {
				parameters.put(footer_right, "TYPE APPROVALS");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, type_approvals + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}

			if (other_technical_notes != null) {
				parameters.put(footer_right, "OTHER TECHNICAL NOTES");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, other_technical_notes + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}
			if (standard_Packaging != null) {
				parameters.put(footer_right, "STANDARD PACKAGING");
				++i;
				footer_right = "footer_right" + Integer.toString(i);

				parameters.put(footer_right, standard_Packaging + addrow);
				++i;
				footer_right = "footer_right" + Integer.toString(i);
			}
			if (catalogue_small_exclamation != null) {
				parameters.put(
					"catalogue_small_exclamation_caption",
					"SMALL EXCLAMATION");
				parameters.put(
					"catalogue_small_exclamation",
					catalogue_small_exclamation);
			}
			if (catalogue_big_exclamation != null) {
				parameters.put(
					"catalogue_big_exclamation_caption",
					"BIG EXCLAMATION");
				parameters.put(
					"catalogue_big_exclamation",
					catalogue_big_exclamation);
			}

		} else {

			//				  TESTATA			
			parameters.put("home_dir", CACHE_DIRECTORY);
			parameters.put("brand_package", fld35String);
			parameters.put("customer", fld36String);

		}
	}
//	-------------- Login MDM Server -------------------------------------------
public static void mdmGetDataOutput() {

//		// ////////////////////////////////////////////////////////////		
		System.out.print("DynamicParameter = -" + DynamicParameter + "-\n");
		System.out.print("paramTableHeader = -" + paramTableHeader + "-\n");
		System.out.print("paramTableItems = -" + paramTableItems + "-\n");
		System.out.print("paramType = -" + paramType + "-\n");
		System.out.print("paramValue = -" + paramValue + "-\n");
		System.out.print("paramBrand = -" + paramBrand + "-\n");
		System.out.print("paramCatalog = -" + paramCatalog + "-\n");
		System.out.print("paramCategory = -" + paramCategory + "-\n");
		System.out.print("Name: " + Name + "-\n");

		System.out.print("\n" + "HEADER DATA" + "\n");
		System.out.print("catalogue_subtitle: " + catalogue_subtitle + "\n");
		System.out.print("catalogue_image: " + imagePath + "\n");
		System.out.print("key_performance: " + key_performance + "\n");
		System.out.print("main_application: " + main_application + "\n");
		System.out.print("min_cont_serv_temp: " + min_cont_serv_temp + "\n");
		System.out.print("max_cont_serv_temp: " + max_cont_serv_temp + "\n");
		System.out.print("max_operating_temp: " + max_operating_temp + "\n");
		System.out.print("min_external_temp: " + min_external_temp + "\n");
		System.out.print("recommended_fluids: " + recommended_fluids + "\n");
		System.out.print("refrigerants: " + refrigerants + "\n");
		System.out.print("lubrificants: " + lubrificants + "\n");
		System.out.print("reinforcement: " + reinforcement + "\n");
		System.out.print("cover: " + cover + "\n");
		System.out.print("applicable_specs: " + applicable_specs + "\n");
		System.out.print("type_approvals: " + type_approvals + "\n");
		System.out.print(
			"other_technical_notes: " + other_technical_notes + "\n");
		System.out.print("standard_Packaging: " + standard_Packaging + "\n");
		System.out.print(
			"catalogue_small_exclamation: "
				+ catalogue_small_exclamation
				+ "\n");
		System.out.print(
			"catalogue_big_exclamation: " + catalogue_big_exclamation + "\n");
		System.out.print("item rec   : " + recs.size() + "\n");
		System.out.print("item recnew: " + recsNew.size() + "\n");
		
		if (paramType.equals("1") || paramType.equals("2")) {
			for (int i = 0; i < recsNew.size(); i++) {
				System.out.print(
					"rec: " + i + " " 
						+ ((prod_hoses_rec)recsNew.get(i)).getField01() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField02() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField03() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField04() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField05() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField06() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField07() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField08() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField09() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField10() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField11() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField12() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField13() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField14() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField15() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField16() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField17() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField18() + "---" 
						+ ((prod_hoses_rec)recsNew.get(i)).getField19() + "---" 
						+ "\n");
			}
		} else {
			for (int i = 0; i < recsNew.size(); i++) {
				System.out.print(
				"rec: " + i + " " 
						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField01() + "---"
//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField02() + "---"
//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField03() + "---"
//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField04() + "---"
//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField05() + "---"
//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField06() + "---"
//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField07() + "---"
						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField08() + "---"
						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField09() + "---"
						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField10() + "---"
						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField11() + "---"
//						+ ((prod_hoses_rec_marcature)recsNew.get(i)).getField12() + "---"
						+ "\n");

			}

		}
	}


}

