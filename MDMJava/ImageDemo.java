import a2i.cache.CatalogCache;
import a2i.common.A2iResultSet;
import a2i.common.CatalogData;
import a2i.common.ResultSetDefinition;
//import a2i.core.A2iValueArray;
import a2i.core.StringException;
import a2i.search.Search;

/**
 * Demonstration - how to retrieve images from an MDM directory
 */
public class ImageDemo
{

	public static void main(String[] args)
	{
		// Define the cache path
		String CACHE_DIRECTORY = "C:\\temp";
		
		// log in to the repository
		CatalogData catalogData = new CatalogData();
		catalogData.Login("10.194.72.24",  2345, "Admin", "admin", "English [US]");
		
		// Init a CatalogCache object
		CatalogCache catalogCache = new CatalogCache();
		catalogCache.Init("10.194.72.24",  2345, "Admin", "admin", CACHE_DIRECTORY, "English [US]");
		
		Search search = new Search("prod_hoses");
		ResultSetDefinition rsd = new ResultSetDefinition("prod_hoses");
		rsd.AddField("catalogue_image");
		
		// Get the products and extract the first image from the first
		// record with non-null Image value.
		try
		{
			A2iResultSet rs = catalogData.GetResultSet(search, rsd, null, true, 0);
			for(int i = 0; i < rs.GetRecordCount(); i++)
			{
				if(!rs.GetValueAt(i, "catalogue_image").IsNull())
				{
					System.out.print( rs.GetValueAt(i, "catalogue_image").GetData() + "\nl");
//					A2iValueArray valueArray = rs.GetValueAt(i, "Data_ID").GetValueArray();
//					int id = valueArray.GetValueAt(0).GetIntValue();
					Object id = rs.GetValueAt(i, "catalogue_image").GetData();
					int idx = id.hashCode();
					String imagePath = catalogCache.GetImagePath("Images", "Original", idx);
					System.out.println("Your image is at: " + CACHE_DIRECTORY + "\\" + imagePath);
//					break;
				}
			}
		}
		catch (StringException e)
		{
			e.printStackTrace();
		}
		finally
		{
			// Remember to shut down the cache!
			catalogCache.Shutdown();
			try
			{
				// Remember to log out!
				catalogData.Logout();
			}
			catch (StringException e)
			{
				// do nothing
			}
		}
		
	}
}
