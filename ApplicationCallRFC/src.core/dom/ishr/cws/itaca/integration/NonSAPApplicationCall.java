package dom.ishr.cws.itaca.integration;
 
import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapportals.portal.prt.component.*;

import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import com.sap.mw.jco.JCO;
import com.sap.security.core.server.destinations.api.DestinationService;
import com.sap.security.core.server.destinations.api.RFCDestination;

public class NonSAPApplicationCall extends AbstractPortalComponent
{
    public void doContent(IPortalComponentRequest request, IPortalComponentResponse response)
    {
		HttpServletRequest req = request.getServletRequest();
		HttpServletResponse resp = request.getServletResponse(true);
		
		IPortalComponentContext componentContext = request.getComponentContext();
		IPortalComponentProfile profile = componentContext.getProfile();
		String appURL = profile.getProperty("nonSAPApplicationURL");
		
		boolean boolReceivedCookie = false;
		Cookie currentCookie = null;
		Cookie cookiesArrray[] = req.getCookies();
		int cookieArrayLength = cookiesArrray.length;
		for(int i=0;i<cookieArrayLength;i++)
		{
		currentCookie = cookiesArrray[i];
		String currentCookieName = currentCookie.getName();
//		  Get out of the loop when "MYSAPSSO2" cookie is obtained
		if(currentCookieName.equalsIgnoreCase("MYSAPSSO2"))
		{
			boolReceivedCookie = true;
		break;
		}
		}
		
		
		resp.addCookie(currentCookie);
		
		String nonSAPURL = null;
if (boolReceivedCookie)
{


// Insert for RFC Call

	try {
    	
	Context ctx = new InitialContext();
	DestinationService dstService = (DestinationService) ctx.lookup(DestinationService.JNDI_KEY);

	RFCDestination dst = (RFCDestination) dstService.getDestination("RFC", "SAP_ECC");
	Properties jcoProperties = dst.getJCoProperties();
	JCO.Client client = JCO.createClient(jcoProperties);

	JCO.Function functionCfgApiGetConfigInfo;
	JCO.Repository mRepository = new JCO.Repository("WASJCORep", client);
	functionCfgApiGetConfigInfo = mRepository.getFunctionTemplate("ZISH_GET_CWS_CONTEXT").getFunction();
	JCO.ParameterList im = functionCfgApiGetConfigInfo.getImportParameterList();
	JCO.ParameterList ex = functionCfgApiGetConfigInfo.getExportParameterList();
	JCO.ParameterList tbl = functionCfgApiGetConfigInfo.getTableParameterList();

	//No import parameters
	//im.setValue("X", "IGNORE_BUFFER");
	client.execute(functionCfgApiGetConfigInfo);

	//lista parametri
//	im.setValue(messageType, "MESSAGE_TYPE");
//	im.setValue("X", "IGNORE_BUFFER");
//	client.execute(functionCfgApiGetConfigInfo);

	// process export document
//	retVal = ex.getString("NUMBER");
		  //elaborazione export parameters

String URL = appURL;
String ipaddr = req.getRemoteAddr();
String PATNR = ex.getString("PATNR");
String REPARTO = ex.getString("REPARTO");
String FALNR = ex.getString("FALNR");
String ORDER_PLACER = ex.getString("ORDER_PLACER");

//if (ex.getString("URL")!= null)
//{
//	URL = ex.getString("URL");
//}

//if (ex.getString("ADDRSTR")!= null)
//{
//	ipaddr = ex.getString("ADDRSTR");
//}
			  
	String params = URL + "applicationID=SAP&command=START_APPLICATION&clientID=" + ipaddr
			 + "&Ward.Id=" + ex.getString("REPARTO") + "&Patient.Id.MRN=" + ex.getString("PATNR") + "&Encounter.Id.VisitNumber=" +
			 ex.getString("FALNR") + "&Observation.Id.Placer_Order_Number=" + ex.getString("ORDER_PLACER");
			 
//if	(PATNR == "XXXXXXXXXX")	
//{
//	params = params + "&Patient.Id.MRN=" + ex.getString("PATNR");
//	if	(ex.getString("FALNR")!= "")	
//	{
//		params = params + "&Encounter.Id.VisitNumber=" + ex.getString("FALNR");
//	}
//	if	(ex.getString("ORDER_PLACER")!= "")	
//	{
//		params = params + "&Observation.Id.Placer_Order_Number=" + ex.getString("ORDER_PLACER");
//	}
	
//}
//else	
//{
//	if	(REPARTO != "X")
//{
//	params = params + "&Ward.Id=" + ex.getString("REPARTO");		
//}
	
//}

//String params = URL + "applicationID=SAP&command=START_APPLICATION&clientID=" + ipaddr +
//				 "&Ward.Id=" + ex.getString("REPARTO") + "&Patient.Id.MRN=" + ex.getString("PATNR") + "&Encounter.Id.VisitNumber=" +
//				 ex.getString("FALNR") + "&Observation.Id.Placer_Order_Number=" + ex.getString("ORDER_PLACER");			 

			  
	client.disconnect();
//	response.write(params);
    appURL = params;
	

}
catch(Exception e){
//	return null;
}

// End Insert for RFC Call



	nonSAPURL = appURL;

}
else
{
	nonSAPURL = "http://www.google.it";
}
//		   e.g. “http://MyNonSAPApplication/Integrated/Login.aspx”
		try {
			resp.sendRedirect(nonSAPURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		



    }
}