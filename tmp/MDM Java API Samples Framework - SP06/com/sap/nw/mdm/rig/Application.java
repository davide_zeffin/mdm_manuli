/*
 * Created on Jun 6, 2007
 *
 */
package com.sap.nw.mdm.rig;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.repository.commands.GetRepositoryRegionListCommand;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.SessionManager;
import com.sap.mdm.session.SessionTypes;
import com.sap.mdm.session.UserSessionContext;

/**
 * This class is the starting point to execute all sample programs.
 * To see a description of the various programs you can execute, please have a look at the
 * documentation for the following classes.  There are static variables in each of these
 * classes that point to the various programs that can be executed with a description of
 * what the program does.
 * <ul>
 * <li>{@link com.sap.nw.mdm.rig.programs.data.blobs.BLOBDataProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.checkout_checkin_rollback.CheckOutCheckInRecordsProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.crud.CRUDDataProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.crud.bulk.BulkCRUDDataProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.keymapping.KeyMappingProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.search.SearchProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.search.attribute.AttributeSearchProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.search.field.FieldSearchProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.syndication.SyndicationProgram}
 * <li>{@link com.sap.nw.mdm.rig.programs.data.workflow.WorkflowProgram}
 * </ul>
 * 
 * @author Richard LeBlanc
 *
 */
public class Application {

	private Application() {
		
	}

	/**
	 * Starts the application and executes a program
	 * 
	 * @param args - not required
	 * 
	 */
	static public void main(String[] args) {
		
		Application app = new Application();

		Program program = null;
		
		/* 
		 * Simply uncomment the line that contains the program you wish to execute and run this class.
		 */
		
		/*
		 * Blob Programs
		 */
//		program = BLOBDataProgram.INSERT_IMAGE;
//		program = BLOBDataProgram.RETRIEVE_AND_WRITE_IMAGE_TO_FILE;
//		program = BLOBDataProgram.RETRIEVE_AND_WRITE_PDF_TO_FILE;

		
		/*
		 * Checkout/Checkin Data Programs
		 */
//		program = CheckOutCheckInRecordsProgram.CHECK_OUT_NEW_CHECK_IN;
//		program = CheckOutCheckInRecordsProgram.CHECK_OUT_NEW_ROLLBACK;
		
//		program = CheckOutCheckInRecordsProgram.CHECK_OUT_EXISTING_CHECK_IN;
//		program = CheckOutCheckInRecordsProgram.CHECK_OUT_EXISTING_ROLLBACK;

		/*
		 * Create Read Update Delete (CRUD) Data Programs
		 */
//		program = CRUDDataProgram.CRUD_HIERARCHY_TABLE;
//		program = CRUDDataProgram.CRUD_MAIN_TABLE;
//		program = CRUDDataProgram.CRUD_MAIN_TABLE_WITH_FLAT_AND_HIERARCHY_LOOKUP_FIELDS;
//		program = CRUDDataProgram.CRUD_MAIN_TABLE_WITH_QUALIFIED_LOOKUP_FIELD;
//		program = CRUDDataProgram.CRUD_MAIN_TABLE_WITH_TAXONOMY_LOOKUP_FIELD;
//		program = CRUDDataProgram.CRUD_TAXONOMY_TABLE_WITH_ATTRIBUTES;


		/*
		 * Bulk Create Read Update Delete (CRUD) data programs
		 * (many records at once)
		 */
//		program = BulkCRUDDataProgram.BULK_CRUD_MAIN_TABLE;


		/*
		 * KeyMapping Programs
		 */
//		program = KeyMappingProgram.RETRIEVE;
//		program = KeyMappingProgram.MODIFY;

	
		/*
		 * Search Programs
		 */
//		program = SearchProgram.DRILL_DOWN_SEARCH;
//		program = SearchProgram.KEYWORD;
//		program = SearchProgram.MASK;
//		program = SearchProgram.NAMED_SEARCH;
//		program = SearchProgram.QUALIFIER;
		
		

		/*
		 * Attribute Search Programs
		 */
//		program = AttributeSearchProgram.COUPLED_NUMERIC;
//		program = AttributeSearchProgram.NUMERIC;
//		program = AttributeSearchProgram.TEXT;


		/*
		 * Field Search Programs
		 */
//		program = FieldSearchProgram.BOOLEAN;
//		program = FieldSearchProgram.CURRENCY;
//		program = FieldSearchProgram.LITERAL_DATE;
//		program = FieldSearchProgram.LOOKUP;
//		program = FieldSearchProgram.TEXT;


		/*
		 * Syndication Programs
		 */
//		program = SyndicationProgram.SYNDICATE_PORT;


		/*
		 * Workflow Programs
		 */
//		program = WorkflowProgram.EXECUTE;


		
		

		//TODO enter MDS name
		String mdsName = "YMQN00430769A"; //the name of the Master Data Server
		String repositoryName = "Products"; //make sure this is the name you use when unarchiving 
											//the repository otherwise change it to reflect the name
											//of your repository
		String regionName = "English [US]";
		String userName = "Admin"; //there is an admin user with no password in the provided repository
		String password = ""; //there is an admin user with no password in the provided repository
		
		app.start(mdsName, repositoryName, regionName, userName, password, program);
		
	}
	
	/*
	 * Establishes a connection to the given server and logs in to the given repository
	 * with the given logon information and executes the given program
	 */
	private void start(String serverName, String repositoryName, String regionName, 
						String user, String password, Program program) {

		//Create a user session context
		UserSessionContext context = new UserSessionContext(serverName, repositoryName, regionName, user);
		
		//Get an instance of the session manager
		SessionManager sessionManager = SessionManager.getInstance();
		
		//Create a user session
		sessionManager.createSession(context, SessionTypes.USER_SESSION_TYPE, password);
		
		
		program.setContext(context);
		
		program.setLoginRegion(getRegion(context));
		
		program.setRepositorySchema(MetadataManager.getInstance().getRepositorySchema(context));
		
		program.setAttributeSchema(MetadataManager.getInstance().getAttributeSchema(context));
		
		//execute the program from the list above
		program.execute();
		
		
		//destroy the session and close the connection to the MDS
		sessionManager.destroySession(context, SessionTypes.USER_SESSION_TYPE);
			
	}
	
	private RegionProperties getRegion(UserSessionContext context) {
	
		RegionProperties[] regions = null;

		try {
	
			GetRepositoryRegionListCommand cmd = new GetRepositoryRegionListCommand(context);
		
			cmd.execute();

			regions = cmd.getRegions();
		
		} catch (SessionException e) {

			e.printStackTrace();

		} catch (ConnectionException e) {

			e.printStackTrace();

		} catch (CommandException e) {

			e.printStackTrace();

		}
	
	
		for(int i=0, j=regions.length; i<j; i++) {
		
			if(regions[i].getName().equals(context.getRegionName())) {
			
				return regions[i];
			
			}
												
		}
	
		return null;
												
	}

}