/*
 * Created on Oct 9, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.blobs;

import com.sap.mdm.data.Record;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.dao.BLOBDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * This class retrieves the first record from the Image table, gets the Image as a byte array
 * and writes it to a file.
 * 
 * @author Richard LeBlanc
 */
class RetrieveImageAndWriteToFileProgram extends BLOBDataProgram {

	/*
	 *  (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute()
	 */
	public void execute() {
		
		//get the table schema of the Images table
		TableSchema tableSchema = MetadataManager.getInstance().getRepositorySchema(context).getTableSchema(MDMAPISamples.Images.TABLE);
		
		//get the first record in the Images table
		Record record = getFirstRecord(tableSchema);
		
		//get the original name of the image when it was first uploaded
		String fileName = ((StringValue)record.getFieldValue(tableSchema.getFieldId(MDMAPISamples.Images.ORIGINAL_NAME))).getString();
		
		//get the image by passing the table id and record id of the Images table record
		byte[] bytes = BLOBDAO.getBlob(context, tableSchema.getTable().getId(), record.getId(), loginRegion);

		//write the image to a file
		writeToFile(bytes, fileName);

	}
	
}
