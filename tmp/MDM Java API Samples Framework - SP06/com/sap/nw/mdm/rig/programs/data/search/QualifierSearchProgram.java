/*
 * Created on Jun 29, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search;

import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.NumericSearchConstraint;
import com.sap.mdm.search.QualifierSearchDimension;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * Performs a search on a qualifier field 
 * 
 * @author Richard LeBlanc
 */
class QualifierSearchProgram extends SearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */		
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		TableSchema pricingTableSchema = MetadataManager.getInstance().getRepositorySchema(context).getTableSchema(MDMAPISamples.Pricing_Regions.TABLE);

		
		FieldId pricesFieldID = tableSchema.getFieldId(MDMAPISamples.Products.REGIONAL_LIST_PRICE);
		
		FieldId regionalListPriceFieldID = pricingTableSchema.getFieldId(MDMAPISamples.Pricing_Regions.LIST_PRICE);


		SearchDimension searchDimension = new QualifierSearchDimension(pricesFieldID, regionalListPriceFieldID);
		
		SearchConstraint searchConstraint = null;
		
		
		System.out.println("Products where List Price = 333");
		
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.LESS_THAN);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.LESS_THAN_OR_EQUAL_TO);
		searchConstraint = new NumericSearchConstraint(333, NumericSearchConstraint.EQUALS);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.NOT_EQUAL);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.GREATER_THAN);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.GREATER_THAN_OR_EQUAL_TO);

		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);

		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;

	}

}