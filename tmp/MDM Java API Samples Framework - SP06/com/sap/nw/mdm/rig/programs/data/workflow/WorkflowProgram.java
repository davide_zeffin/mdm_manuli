/*
 * Created on Aug 31, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.workflow;

import com.sap.nw.mdm.rig.Program;

/**
 * This is the basis for all workflow programs.
 * It contains a static variable that points to the available workflow program
 * that can be executed.
 * 
 * @author Richard LeBlanc
 */
abstract public class WorkflowProgram extends Program {

	/**
	 * Retrieves a workflow, creates a workflow job for it and , launches the workflow
	 * job.
	 */
	static public final WorkflowProgram EXECUTE = new ExecuteWorkflowProgram(); 

}
