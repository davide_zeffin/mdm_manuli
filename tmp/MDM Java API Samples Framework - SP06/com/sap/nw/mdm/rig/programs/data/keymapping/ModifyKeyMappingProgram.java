/*
 * Created on Jan 22, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.nw.mdm.rig.programs.data.keymapping;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.RecordKeyMapping;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.nw.mdm.rig.data.dao.KeyMappingDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * This program modifies the key mappings for the existing record
 * with Part Number 68899
 * 
 * @author Richard LeBlanc
 */
public class ModifyKeyMappingProgram extends KeyMappingProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute()
	 */
	public void execute() {
		
		TableId tableID = schema.getTableId(MDMAPISamples.Products.TABLE);
		
		FieldId fieldID = schema.getFieldId(MDMAPISamples.Products.TABLE, 
												MDMAPISamples.Products.PART_NUMBER);
		
		//get the product that has part number 68899
		Record record = getRecordByValue(context, tableID, fieldID, "68899");
			
		RecordId[] recordIDs = {record.getId()};
		
		//get the key mappings
		RecordKeyMapping[] recordKMs = KeyMappingDAO.getKeyMappings(context, tableID, recordIDs, false);

		recordKMs[0].addKey(schema.getRemoteSystemId("ERP"), "New Key");
		
		KeyMappingDAO.modifyKeyMappings(context, recordKMs);

	}

}
