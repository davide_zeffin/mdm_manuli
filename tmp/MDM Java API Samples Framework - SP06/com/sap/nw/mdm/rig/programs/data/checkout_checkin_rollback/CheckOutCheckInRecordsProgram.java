/*
 * Created on Sep 7, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.checkout_checkin_rollback;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.nw.mdm.rig.Program;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;

/**
 * This is an abstract class all Checkout/Checkin programs extend.
 * 
 * @author Richard LeBlanc
 */
abstract public class CheckOutCheckInRecordsProgram extends Program {
	/**
	 * This program creates a new checked out record, modifies it and checks it in.
	 * <P>
	 * If you debug this program and stop it in between, you can verify that the new record is checked out by looking at 
	 * the records in the Data Manager.
	 */
	static public final CheckOutCheckInRecordsProgram CHECK_OUT_NEW_CHECK_IN = new CheckOutNewRecordAndCheckInProgram();
	/**
	 * This program creates a new checked out record, modifies it and performs a rollback. 
	 * <P>
	 * If you debug this program and stop it in between, you can verify that the records are checked out by looking at 
	 * the records in the Data Manager.
	 */
	static public final CheckOutCheckInRecordsProgram CHECK_OUT_NEW_ROLLBACK = new CheckOutNewRecordAndRollbackProgram();
	/**
	 * This program checks out the first 10 records of the Products table, 
	 * modifies them and checks them back in.
	 * <P>
	 * If you debug this program and stop it in between, you can verify that the records are checked out by looking at 
	 * the records in the Data Manager.
	 */
	static public final CheckOutCheckInRecordsProgram CHECK_OUT_EXISTING_CHECK_IN = new CheckOutExistingRecordsAndCheckInProgram();
	/**
	 * This program checks out the first 10 records of the Products table, 
	 * modifies them and then performs a rollback.
	 * <P>
	 * If you debug this program and stop it in between, you can verify that the records are checked out by looking at 
	 * the records in the Data Manager.
	 */
	static public final CheckOutCheckInRecordsProgram CHECK_OUT_EXISTING_ROLLBACK = new CheckOutExistingRecordsAndRollbackProgram();
	
	/**
	 * Retrieves the record identified by the provided record id.
	 * 
	 * @param tableSchema - the table that contains the record
	 * @param recordID - the record identifier
	 */
	protected Record getRecordByID(TableSchema tableSchema, RecordId recordID) {
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());
		
		resultDefinition.setSelectFields(tableSchema.getFieldIds());
		
		RecordId[] recordIDs = {recordID};
		
		Record[] records = RecordDAO.getRecordsByID(context, resultDefinition, null, recordIDs);
		
		return records[0];
		
	}

}
