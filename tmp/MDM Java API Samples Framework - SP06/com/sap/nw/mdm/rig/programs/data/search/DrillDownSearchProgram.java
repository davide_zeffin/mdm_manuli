/*
 * Created on Oct 14, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.FieldSearchDimension;
import com.sap.mdm.search.PickListSearchConstraint;
import com.sap.mdm.search.Search;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.mdm.valuetypes.LookupValue;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * Performs a drill down search.
 * It first searches for Products where Category = "Screwdrivers".
 * Then it searches for Products where Category = "Screwdrivers" and Manufacturer = "Gothenberg Corporation".
 * And finally it searches for Products where Category = "Screwdrivers" 
 * and Manufacturer = "Gothenberg Corporation" and Distribution Area = "Area A".
 * 
 * @author Richard LeBlanc
 *
 */
class DrillDownSearchProgram extends SearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public void execute() {

		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);
		
		//placeholder for the results of our searches
		Record[] records = null;

		//create a result definition for the Products table and include all fields
		ResultDefinition resultDefinition = new ResultDefinition(productsTableSchema.getTable().getId());
		
		resultDefinition.setSelectFields(productsTableSchema.getDisplayFieldIds());
		
		/*
		 * The first search criteria is on the Category field
		 */
		 
		//metadata needed for Categories search
		TableSchema categoryTableSchema = schema.getTableSchema(MDMAPISamples.Categories.TABLE);
		
		FieldId categoryFieldID = categoryTableSchema.getFieldId(MDMAPISamples.Categories.CATEGORY); 
		
		
		//get all Category records
		records = getAllRecords(categoryTableSchema);
		
		
		System.out.println("Categories");
		
		//print the Categories
		RecordPrinter.print(context, records, loginRegion);
		
		
		System.out.println("");
		
				
		
		//Create a search for Products where Category = "Screwdrivers"
		Search search = getSearch(productsTableSchema);
		
		search.addSearchItem(getPickListSearchParameter(productsTableSchema.getFieldId(MDMAPISamples.Products.CATEGORY),
														categoryTableSchema, categoryFieldID, "Screwdrivers"));
		
		
		//get Products where Category = "Screwdriver"
		records = RecordDAO.getRecords(context, search, resultDefinition, null, null, 1000, 0);

		
		
		System.out.println("Products where Category = Screwdrivers");
		
		//print the Products records
		RecordPrinter.print(context, records, loginRegion);
		
		System.out.println("");
		
		
		
		/*
		 * The second search criteria is on the Manufacturer field
		 */		
		
		//metadata for Manufacturer search
		TableSchema manufacturerTableSchema = schema.getTableSchema(MDMAPISamples.Manufacturers.TABLE);
		
		FieldId manufacturerNameFieldID = manufacturerTableSchema.getFieldId(MDMAPISamples.Manufacturers.NAME);
		
		//the path from the Products search table to the Manufacturers result table
		//goes through the Manufacturer field of the Products table so this is what we need to specify  
		FieldId[] resultTablePathMfr = {productsTableSchema.getFieldId(MDMAPISamples.Products.MANUFACTURER)};
				
		//get the list of manufacturers that are referred to by Products records
		//that meet the search criteria created earlier.  In this case, where Products -> Category = "Screwdrivers"
		records = getRemainingRecords(search, manufacturerTableSchema, resultTablePathMfr);
		
		
		System.out.println("Manufacturers");
		
		//print the list of remaining manufacturers
		RecordPrinter.print(context, records, loginRegion);
		
		
		System.out.println("");
		
		
		
		//add the following criteria to the search - Products -> Manufacturer = 
		search.addSearchItem(getPickListSearchParameter(productsTableSchema.getFieldId(MDMAPISamples.Products.MANUFACTURER),
														manufacturerTableSchema, manufacturerNameFieldID,
														"Gothenberg Corporation")); 
	
		//get Products where Category = "Screwdriver" and Manufacturer = "Gothenberg Corporation"
		records = RecordDAO.getRecords(context, search, resultDefinition, null, null, 1000, 0);
		
		
		
		System.out.println("Products where Category = Screwdriver and Manufacturer = Gothenberg Corporation");
				
		//print the Products records
		RecordPrinter.print(context, records, loginRegion);
		
		System.out.println("");
		
		

		/*
		 * The third search criteria is on the Distribution Area field
		 */		
		
		//metadata for Distribution Area search
		TableSchema distributionAreasTableSchema = schema.getTableSchema(MDMAPISamples.Distribution_Areas.TABLE);
	
		FieldId distributionAreaNameFieldID = distributionAreasTableSchema.getFieldId(MDMAPISamples.Distribution_Areas.NAME);
	
		//the path from the Products search table to the Manufacturers result table
		//goes through the Manufacturer field of the Products table so this is what we need to specify  
		FieldId[] resultTablePathDA = {productsTableSchema.getFieldId(MDMAPISamples.Products.DISTRIBUTION_AREA)};
			
		//get the list of Distribution Areas that are referred to by Products records
		//that meet the search criteria created earlier.  
		//In this case, Products where Category = "Screwdrivers" and Manufacturer = "Gothenberg Corporation"
		records = getRemainingRecords(search, distributionAreasTableSchema, resultTablePathDA);
	
		
		System.out.println("Distribution Areas");
		
		//print the list of remaining distribution areas
		RecordPrinter.print(context, records, loginRegion);
	
		System.out.println("");
	
	
	
		//add the following criteria to the search - Products -> Manufacturer = 
		search.addSearchItem(getPickListSearchParameter(productsTableSchema.getFieldId(MDMAPISamples.Products.DISTRIBUTION_AREA),
														  distributionAreasTableSchema, distributionAreaNameFieldID,
														  "Area C")); 
	
		//get Products where Category = "Screwdriver" and Manufacturer = "Gothenberg Corporation"
		//and Distribution Area = "Area A"
		records = RecordDAO.getRecords(context, search, resultDefinition, null, null, 1000, 0);
	
	
		System.out.println("Products where Category = Screwdriver and Manufacturer = Gothenberg Corporation and Distribution Area = Area C");
		
		//print the Products records
		RecordPrinter.print(context, records, loginRegion);

		System.out.println("");			
		
	}

	/*
	 * Returns a search parameter
	 */
	private SearchParameter getPickListSearchParameter(FieldId parameterFieldID,
														TableSchema searchTableSchema, FieldId searchFieldID, 
														String searchValue) {
		
		ResultDefinition resultDefinition = new ResultDefinition(searchTableSchema.getTable().getId());
		
		Record record = RecordDAO.getRecordsByValue(context, searchFieldID, new StringValue[] {new StringValue(searchValue)}, 
									resultDefinition, null)[0];
		
		SearchDimension searchDimension = new FieldSearchDimension(parameterFieldID);
		
		SearchConstraint searchConstraint = new PickListSearchConstraint(new MdmValue[] {new LookupValue(record.getId())});
		
		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);
		
		return searchParameter;
		
	}
	
	/*
	 * Returns the list of remaining records in the resultTable that are refered to by Products records
	 * that meet the provided search criteria.
	 */
	private Record[] getRemainingRecords(Search search, TableSchema resultTableSchema, FieldId[] resultTablePath) {
		
		ResultDefinition resultDefinition = new ResultDefinition(resultTableSchema.getTable().getId());
		
		resultDefinition.setSelectFields(resultTableSchema.getDisplayFieldIds());
		
		
		Record[] records = RecordDAO.getRecords(context, search, resultDefinition, 
												resultTablePath,
												null, 1000, 0);
				
		return records;

	}


	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.mdm.schema.TableSchema)
	 */
	protected SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		throw new UnsupportedOperationException();
		
	}

}
