/*
 * Created on Jul 4, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud;

import java.util.ArrayList;
import java.util.List;

import com.sap.mdm.data.HierNode;
import com.sap.mdm.data.MdmValueTypeException;
import com.sap.mdm.data.Record;
import com.sap.mdm.data.RecordFactory;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.extension.schema.RepositorySchemaEx;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.schema.fields.LookupFieldProperties;
import com.sap.mdm.search.Search;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.Program;
import com.sap.nw.mdm.rig.data.dao.HierarchyDAO;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;

/**
 * This is an abstract class that contains helper methods that can
 * be used by all CRUD (Create Read Update Delete) data programs.
 * <P>
 * It also contains a set of static fields that represents each of the CRUD data programs that can be executed.
 * 
 * @author Richard LeBlanc
 */
abstract public class CRUDDataProgram extends Program {
	
	/**
	 * This CRUDDataProgram creates, reads, updates and deletes a main table record.
	 */
	static public final CRUDDataProgram CRUD_MAIN_TABLE = new CRUDMainTableRecordProgram();
	
	
	/**
	 * This CRUDDataProgram creates, reads, updates and deletes a hierarchy table record.
	 */
	static public final CRUDDataProgram CRUD_HIERARCHY_TABLE = new CRUDHierarchyTableRecordProgram();
	
	
	/**
	 * This CRUDDataProgram creates, reads, updates and deletes a taxonomy table record.
	 */
	static public final CRUDDataProgram CRUD_TAXONOMY_TABLE_WITH_ATTRIBUTES = new CRUDTaxonomyTableRecordWithAttributesProgram();
	
	
	/**
	 * This CRUDDataProgram creates, reads, updates and deletes a main table record that contains
	 * flat and hierarchy lookup fields.
	 */
	static public final CRUDDataProgram CRUD_MAIN_TABLE_WITH_FLAT_AND_HIERARCHY_LOOKUP_FIELDS = new CRUDMainTableRecordWithFlatAndHierarchyLookupFieldsProgram();
	
	
	/**
	 * This CRUDDataProgram creates, reads, updates and deletes a main table record that contains
	 * a qualified lookup field.
	 */
	static public final CRUDDataProgram CRUD_MAIN_TABLE_WITH_QUALIFIED_LOOKUP_FIELD = new CRUDMainTableRecordWithQualifiedLookupFieldProgram();
	
	
	/**
	 * This CRUDDataProgram creates, reads, updates and deletes a main table record that contains
	 * a taxonomy lookup field.
	 */
	static public final CRUDDataProgram CRUD_MAIN_TABLE_WITH_TAXONOMY_LOOKUP_FIELD = new CRUDMainTableRecordWithTaxonomyLookupFieldAndAttributesProgram();
	

	
	/**
	 * Gets the record identified by the given record id for the given table.
	 * 
	 * @param tableSchema - the table from which to retrieve the records
	 * @param recordID - the record id of the record to retrieve
	 * @return the record identified by the given record id for the given table
	 */
	protected Record getRecordByID(TableSchema tableSchema, RecordId recordID) {
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());
		
		resultDefinition.setSelectFields(tableSchema.getFieldIds());
		
		resultDefinition.setLoadAttributes(true);

		ResultDefinition[] supportingResultDefinitions = getSupportingResultDefinitions(tableSchema, false);
		
		RecordId[] recordIDs = {recordID};
		
		Record[] records = RecordDAO.getRecordsByID(context, resultDefinition, supportingResultDefinitions, recordIDs);
		
		return records[0];
		
	}
	
	
	/**
	 * Gets all the records identified by the given record ids for the given table.
	 * 
	 * @param tableSchema - the table from which to retrieve the records
	 * @param recordIDs - the record ids of the records to retrieve
	 * @return all the records identified by the given record ids for the given table
	 */
	protected Record[] getRecordsByID(TableSchema tableSchema, RecordId[] recordIDs) {
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());

		resultDefinition.setSelectFields(tableSchema.getFieldIds());

		
		ResultDefinition[] supportingResultDefinition = getSupportingResultDefinitions(tableSchema, true);

		
		Record[] records = RecordDAO.getRecordsByID(context, resultDefinition, supportingResultDefinition, recordIDs);

		return records;

	}
	
	/**
	 * Retrieves the first record that contains the given value in the given field
	 * in the given table.
	 * 
	 * @param tableSchema - the table from which to retrieve the record
	 * @param fieldID - the field to look at when searching for the value
	 * @param value - the value to search for
	 * @return the first record that contains the given value in the given field
	 * in the given table
	 */
	protected Record getRecordByValue(TableSchema tableSchema, FieldId fieldID, String value) {
		
		StringValue[] values = {new StringValue(value)};
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());

		resultDefinition.setSelectFields(tableSchema.getDisplayFieldIds());


		ResultDefinition[] supportingResultDefinitions = getSupportingResultDefinitions(tableSchema, true);
		
		
		Record record = RecordDAO.getRecordsByValue(context, fieldID, values, resultDefinition, supportingResultDefinitions)[0];
	
		return record;
	
	}
	
	/**
	 * Returns all the records from the given table.
	 * @param tableSchema - the table from which to retrieve the records
	 * @return all the records from the given table
	 */
	protected Record[] getAllRecords(TableSchema tableSchema) {
		
		Search search = new Search(tableSchema.getTable().getId());
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());

		resultDefinition.setSelectFields(tableSchema.getDisplayFieldIds());

		
		ResultDefinition[] supportingResultDefinitions = getSupportingResultDefinitions(tableSchema, true);


		return RecordDAO.getRecords(context, search, resultDefinition, null, supportingResultDefinitions, 1000, 0);
		
	}
	
	/**
	 * Retrieves the entire table as a hierarchy.
	 * 
	 * @param tableSchema - the table to retrieve (must be taxonomy or hierarchy table)
	 * @return the entire table as a hierarchy
	 */
	protected HierNode getHierarchyNode(TableSchema tableSchema) {
		
		Search search = new Search(tableSchema.getTable().getId());
	
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());
	
		resultDefinition.setSelectFields(tableSchema.getDisplayFieldIds());
		
		
		ResultDefinition[] supportingResultDefinitions = getSupportingResultDefinitions(tableSchema, true);
		
	
		return HierarchyDAO.getHierarchyNode(context, search, null, null, resultDefinition, supportingResultDefinitions);
	
	}
	
	/**
	 * Creates a record with the values provided in the FieldValuePair array
	 * 
	 * @param tableSchema - the table in which to create the record
	 * @param fieldValuePairs - the fields and their values
	 * @return the record id of the newly created record
	 */
	protected RecordId createRecord(TableSchema tableSchema, FieldValuePair[] fieldValuePairs) {
		
		Record record = RecordFactory.createEmptyRecord(tableSchema.getTable().getId());
	
		try {
		
			for(int i=0, j=fieldValuePairs.length; i<j; i++) {
				
				record.setFieldValue(fieldValuePairs[i].getField(), fieldValuePairs[i].getValue());	
				
			}
						
		} catch (IllegalArgumentException e) {
		
			e.printStackTrace();
		
		} catch (MdmValueTypeException e) {
		
			e.printStackTrace();
		
		}

		return RecordDAO.createRecord(context, record);

	}

	/**
	 * Updates the record with the values provided in the FieldValuePair array
	 * @param record - the record to update
	 * @param fieldValuePairs - the fields and their values
	 */
	protected void updateRecord(Record record, FieldValuePair[] fieldValuePairs) {
		
		try {
	
			for(int i=0, j=fieldValuePairs.length; i<j; i++) {
			
				record.setFieldValue(fieldValuePairs[i].getField(), fieldValuePairs[i].getValue());	
			
			}
					
		} catch (IllegalArgumentException e) {
	
			e.printStackTrace();
	
		} catch (MdmValueTypeException e) {
	
			e.printStackTrace();
	
		}

		RecordDAO.modifyRecord(context, record, false);

	}
	
	/**
	 * Deletes the given record.
	 * 
	 * @param tableId - the table from which to delete record
	 * @param record - the record to delete
	 */
	protected void deleteRecord(TableId tableId, Record record) {
		
		RecordDAO.deleteRecord(context, tableId, record);
		
	}

	/*
	 * Creates a ResultDefinition for every lookup field in the provided TableSchema
	 */
	private ResultDefinition[] getSupportingResultDefinitions(TableSchema tableSchema, boolean displayFieldsOnly) {
		
		List list = new ArrayList();
		
		ResultDefinition supportingResultDefinition = null;
		
		FieldProperties[] fields = tableSchema.getFields();
		
		LookupFieldProperties lookupField = null;
		
		TableSchema lookupTableSchema = null;
		
		FieldId[] lookupFieldIDs = null;
		
		RepositorySchemaEx schema = MetadataManager.getInstance().getRepositorySchema(context);
		
		for(int i=0, j=fields.length; i<j; i++){
		
			if(fields[i].isLookup()) {
				
				if(fields[i].getType() != FieldProperties.QUALIFIED_FLAT_LOOKUP_FIELD) {
				
					lookupField = (LookupFieldProperties)fields[i];
					
					lookupTableSchema = schema.getTableSchema(lookupField.getLookupTableId());
	
					if(displayFieldsOnly) {
			
						lookupFieldIDs = lookupTableSchema.getDisplayFieldIds();
			
					} else {
				
						lookupFieldIDs = lookupTableSchema.getFieldIds();
				
					} 
					
					supportingResultDefinition = new ResultDefinition(lookupField.getLookupTableId());
					
					supportingResultDefinition.setSelectFields(lookupFieldIDs);
					
					list.add(supportingResultDefinition);
					
				}

			}
			
		}
		
		ResultDefinition[] supportingResultDefinitions = (ResultDefinition[])list.toArray(new ResultDefinition[list.size()]);
		
		return supportingResultDefinitions;
		
	}
	
}
