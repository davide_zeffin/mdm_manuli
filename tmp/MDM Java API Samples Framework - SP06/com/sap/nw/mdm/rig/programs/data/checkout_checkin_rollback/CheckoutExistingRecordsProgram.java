/*
 * Created on Jul 4, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.checkout_checkin_rollback;

import com.sap.mdm.data.MdmValueTypeException;
import com.sap.mdm.data.MultipleRecordsResult;
import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.Search;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * This abstract class provides helper methods for subclasses
 * 
 * @author Richard LeBlanc
 */
abstract class CheckoutExistingRecordsProgram extends CheckOutCheckInRecordsProgram {
	
	/**
	 * Gets the first x number of records of the given table where x is the given
	 * number of recors
	 * 
	 * @param tableSchema - the table from which to retrieve the records
	 * @param numberOfRecords - the number of records to retrieve
	 * @return
	 */
	protected Record[] getFirstXRecords(TableSchema tableSchema, int numberOfRecords) {
		
		Search search = new Search(tableSchema.getTable().getId());
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());
		
		resultDefinition.setSelectFields(tableSchema.getDisplayFieldIds());
		
		return RecordDAO.getRecords(context, search, resultDefinition, null, null, numberOfRecords, 0);
		
	}
	
	/**
	 * Retrieves the records identified by the given record ids for the given table
	 * 
	 * @param tableSchema - the table from which to retrieve the records
	 * @param recordIDs - the record ids of the records to retrieve
	 * @return
	 */
	protected Record[] getRecordsByID(TableSchema tableSchema, RecordId[] recordIDs) {
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());
	
		resultDefinition.setSelectFields(tableSchema.getDisplayFieldIds());
	
		Record[] records = RecordDAO.getRecordsByID(context, resultDefinition, null, recordIDs);
	
		return records;
		
	}
	
	
	/**
	 * Modifies the product name and part number fields of the given records
	 * 
	 * @param records - the records to update
	 * @param productTableSchema - the table schema of the Products table
	 */
	protected void modifyRecords(Record[] records, TableSchema productsTableSchema) {
	
		Record currentRecord = null;
	
		FieldId productNameFieldID = productsTableSchema.getFieldId(MDMAPISamples.Products.PRODUCT_NAME);
		
		FieldId partNumberFieldID = productsTableSchema.getFieldId(MDMAPISamples.Products.PART_NUMBER);
	
		StringValue productNameValue = null;

		StringValue partNumberValue = null;

		//modify the records
		for(int i=0, j=records.length; i<j; i++) {

			try {
				//get the existing product name
				productNameValue = (StringValue)records[i].getFieldValue(productNameFieldID);
	
				//update the product name
				records[i].setFieldValue(productNameFieldID, 
												new StringValue(productNameValue.getString() + "_updated"));
	
				//get the existing part number
				partNumberValue = (StringValue)records[i].getFieldValue(partNumberFieldID);
	
				//update the part number
				records[i].setFieldValue(partNumberFieldID, 
												new StringValue(partNumberValue.getString() + "_updated"));
		
			} catch (IllegalArgumentException e) {

				e.printStackTrace();

			} catch (MdmValueTypeException e) {

				e.printStackTrace();

			}
			
		}
		
		//persist in MDM
		MultipleRecordsResult results = RecordDAO.modifyRecords(context, productsTableSchema.getTable().getId(), records);

		//Print error messages for records that weren't created
		printFailures(results);		

	}
	
	private void printFailures(MultipleRecordsResult results) {
		
		int[] failedRecords = results.getFailedRecords();
	
		for(int i=0, j=failedRecords.length; i<j; i++) {
		
			System.out.println(results.getFailedRecordMessage(i));
		
		}
	
	}

}
