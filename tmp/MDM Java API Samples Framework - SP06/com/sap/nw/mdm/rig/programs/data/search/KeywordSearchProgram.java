/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search;

import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.KeywordSearchConstraint;
import com.sap.mdm.search.KeywordSearchDimension;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;

/**
 * Performs a keyword search 
 *
 * @author Richard LeBlanc
 */
class KeywordSearchProgram extends SearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */		
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		SearchDimension searchDimension = new KeywordSearchDimension();
		
		SearchConstraint searchConstraint = null;

		System.out.println("Records where keyword = Screwdriver");

		searchConstraint = new KeywordSearchConstraint("Screwdriver", KeywordSearchConstraint.CONTAINS);
//		searchConstraint = new KeywordSearchConstraint(null, KeywordSearchConstraint.EQUALS);
//		searchConstraint = new KeywordSearchConstraint(null, KeywordSearchConstraint.PROGRESSIVE);
//		searchConstraint = new KeywordSearchConstraint(null, KeywordSearchConstraint.SOUNDS_LIKE);
//		searchConstraint = new KeywordSearchConstraint(null, KeywordSearchConstraint.STARTS_WITH);


		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);

		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;

	}

}
