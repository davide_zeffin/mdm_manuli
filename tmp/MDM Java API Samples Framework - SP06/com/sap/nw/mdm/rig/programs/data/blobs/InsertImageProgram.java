/*
 * Created on Dec 4, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.nw.mdm.rig.programs.data.blobs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.sap.mdm.blobs.ImageBlobRecord;
import com.sap.mdm.data.MdmValueTypeException;
import com.sap.mdm.data.RecordFactory;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.group.HierGroupNode;
import com.sap.mdm.group.commands.RetrieveGroupTreeCommand;
import com.sap.mdm.ids.GroupNodeId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.valuetypes.BinaryValue;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.dao.DataGroupDAO;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * This program inserts an image in the Image table.
 * Please make sure you have an image with the name image.jpg located in the C:\Temp folder
 * before executing this program.
 * 
 * @author Richard LeBlanc
 */
public class InsertImageProgram extends BLOBDataProgram{

	/* 
	 * Please make sure there's an image named image.jpg in the C:\Temp folder
	 * before executing this program
	 * 
	 * (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute(com.sap.mdm.session.SessionConnection)
	 */
	public void execute() {
		
		byte[] bytes = getBytes("C:\\Temp\\image.jpg");
		
		GroupNodeId dataGroupNodeID = getGroupNodeID("Product Images", RetrieveGroupTreeCommand.DATA_GROUP_TYPE);
		
		GroupNodeId dataLocationID = getGroupNodeID("Temp", RetrieveGroupTreeCommand.DATA_LOCATION_TYPE);
		
		TableId tableID = MetadataManager.getInstance().getRepositorySchema(this.context).getTableId(MDMAPISamples.Images.TABLE);
		
		ImageBlobRecord record = RecordFactory.createEmptyImageRecord(tableID);
		
		try {
		
			record.setBinary(new BinaryValue(bytes));
			
			record.setDataGroupId(dataGroupNodeID);
		
			record.setDataLocationId(dataLocationID);
			
			record.setOriginalName(new StringValue("image.jpg"));	
			
		} catch (IllegalArgumentException e) {
		
			e.printStackTrace();
			
		} catch (MdmValueTypeException e) {
			
			e.printStackTrace();
			
		}
		
		RecordDAO.createRecord(context, record);		

		
	}
	
	private byte[] getBytes(String fileName) {
		
		File file = new File(fileName);
		
		int length = new Long(file.length()).intValue();

		byte[] bytes = new byte[length];

		FileInputStream fis = null;

		try {

			fis = new FileInputStream(file);
	
			fis.read(bytes);
	
		} catch (FileNotFoundException e) {
	
			System.out.println("Please make sure there's an image named image.jpg in the " +
								"C:\\Temp folder before executing this program");
			
			e.printStackTrace();
			
	
	
		} catch (IOException e) {
	
			e.printStackTrace();
	
		}
		
		return bytes;
		
	}
	
	private GroupNodeId getGroupNodeID(String nodeName, int nodeType) {
		
		HierGroupNode tree = DataGroupDAO.getDataGroupNode(context, nodeType);
		
		HierGroupNode[] leafNodes = tree.getAllLeafs();

		HierGroupNode currentNode = null;

		GroupNodeId groupNodeID = null;
		
		for(int i=0, j=leafNodes.length; i<j; i++) {
	
			currentNode = leafNodes[i];
			
			if(currentNode.getName().equalsIgnoreCase(nodeName)) {

				groupNodeID = currentNode.getId();
		
				break;

			} 
					
		}
		
		return groupNodeID;
		
	}
	
}
