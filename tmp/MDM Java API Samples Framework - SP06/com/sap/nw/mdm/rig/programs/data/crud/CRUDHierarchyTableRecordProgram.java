/*
 * Created on Apr 24, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud;

import com.sap.mdm.data.HierNode;
import com.sap.mdm.data.MdmValueTypeException;
import com.sap.mdm.data.Record;
import com.sap.mdm.data.RecordFactory;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.dao.HierarchyDAO;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.HierarchyPrinter;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * Creates, reads, updates and deletes a parent and a child record in a hierarchy table.
 * 
 * @author Richard LeBlanc
 */
class CRUDHierarchyTableRecordProgram extends CRUDDataProgram{
	
	public void execute() {
		
		TableSchema manufacturersTableSchema = schema.getTableSchema(MDMAPISamples.Manufacturers.TABLE);
		
		
		//Create Parent
		RecordId parentManufacturerRecordID = createParentManufacturerRecord(manufacturersTableSchema);
		
		
		//Create Child
		RecordId childManufacturerRecordID = createChildManufacturerRecord(manufacturersTableSchema, parentManufacturerRecordID);
		
		
		//Read hierarchy
		HierNode manufacturerHierarchy = getHierarchyNode(manufacturersTableSchema);
		
		
		//Print hierarchy
		System.out.println("Manufacturer hierarchy with new manufacturers");
		
		HierarchyPrinter.print(manufacturerHierarchy);
		
		
		System.out.println(System.getProperty("line.separator") + 
							"-------------------------------------" +
							System.getProperty("line.separator"));
		
		
		//Read Parent
		Record parentRecord = getRecordByID(manufacturersTableSchema, parentManufacturerRecordID);
		
		
		//Update Parent
		updateParentManufacturerRecord(manufacturersTableSchema, parentRecord);
		
		
		//Read Child
		Record childRecord = getRecordByID(manufacturersTableSchema, childManufacturerRecordID);
		
		
		//Update Child
		updateChildManufacturerRecord(manufacturersTableSchema, childRecord);
				
		
		//Read hierarchy
		HierNode updatedManufacturerHierarchy = getHierarchyNode(manufacturersTableSchema);
		
		
		//Print hierarchy
		System.out.println("Updated manufacturer hierarchy with updated manufacturers");
		
		HierarchyPrinter.print(updatedManufacturerHierarchy);
		
		
		//Delete Child
		deleteRecord(manufacturersTableSchema.getTable().getId(), childRecord);
		
		
		//Delete Parent - you can't delete the parent if it still has children
		deleteRecord(manufacturersTableSchema.getTable().getId(), parentRecord);
		
		
		//Read hierarchy
		updatedManufacturerHierarchy = getHierarchyNode(manufacturersTableSchema);


		//Print hierarchy
		System.out.println("Updated manufacturer hierarchy after deleting added manufacturers");

		HierarchyPrinter.print(updatedManufacturerHierarchy);
		
		
	}

	/*
	 * Creates a parent record
	 */
	private RecordId createParentManufacturerRecord(TableSchema tableSchema) {
		
		Record record = RecordFactory.createEmptyRecord(tableSchema.getTable().getId());
		
		FieldId fieldID = tableSchema.getField(MDMAPISamples.Manufacturers.NAME).getId();
			
		MdmValue value = new StringValue("ACME");
			
		try {
		
			record.setFieldValue(fieldID, value);
			
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
			
		} catch (MdmValueTypeException e) {
			
			e.printStackTrace();
		}			
			
		return RecordDAO.createRecord(context, record);
	}
	
	
	/*
	 * Creates a child record
	 */
	private RecordId createChildManufacturerRecord(TableSchema tableSchema, RecordId parentRecordID) {
		
		Record record = RecordFactory.createEmptyRecord(tableSchema.getTable().getId());
		
		FieldId fieldID = tableSchema.getField(MDMAPISamples.Manufacturers.NAME).getId();
		
		MdmValue value = new StringValue("ACME Jr.");
	
		try {
			
			record.setFieldValue(fieldID, value);
			
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
			
		} catch (MdmValueTypeException e) {
			
			e.printStackTrace();
			
		}	

		return HierarchyDAO.createHierarchyNode(context, record, parentRecordID, 0);	

	}
	
	/*
	 * Updates a parent record
	 */
	private void updateParentManufacturerRecord(TableSchema tableSchema, Record record) {
		
		FieldId fieldID = tableSchema.getField(MDMAPISamples.Manufacturers.NAME).getId();
		
		MdmValue value = new StringValue("ACME - updated.");
		
		try {

			record.setFieldValue(fieldID, value);
			
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
			
		} catch (MdmValueTypeException e) {
			
			e.printStackTrace();
			
		}
		
		RecordDAO.modifyRecord(context, record, false);
		
	}
	
	/*
	 * Updates a child record
	 */
	private void updateChildManufacturerRecord(TableSchema tableSchema, Record record) {

		FieldId fieldID = tableSchema.getField(MDMAPISamples.Manufacturers.NAME).getId();
		
		MdmValue value = new StringValue("ACME Jr. - updated.");
		
		try {

			record.setFieldValue(fieldID, value);
	
		} catch (IllegalArgumentException e) {
	
			e.printStackTrace();
	
		} catch (MdmValueTypeException e) {
	
			e.printStackTrace();
	
		}

		RecordDAO.modifyRecord(context, record, false);		
		
	}
	
}
