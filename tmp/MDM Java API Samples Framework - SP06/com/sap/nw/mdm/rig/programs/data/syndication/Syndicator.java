/*
 * Created on Aug 22, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.syndication;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.commands.SyndicatePortCommand;
import com.sap.mdm.ids.PortId;
import com.sap.mdm.ids.RemoteSystemId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;

/**
 * Syndicates records.
 * 
 * @author Richard LeBlanc
 */
public class Syndicator {

	/**
	 * Syndicates records to the specified port.  The records to be syndicated are determined
	 * by the search criteria as defined in the Syndication Map specified for the provided port.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param portID - the port to syndicate the records to
	 * @param remoteSystemID - the remote system
	 * @param filePrefix - the file name to use 
	 */
	static public void syndicatePort(UserSessionContext context, PortId portID, 
										RemoteSystemId remoteSystemID, String filePrefix) {
		
		SyndicatePortCommand cmd = null;
		
		try {
		
			cmd = new SyndicatePortCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setFilePrefix(filePrefix);
		
		cmd.setPortId(portID);

		cmd.setRemoteSystemId(remoteSystemID);
		

		try {

			cmd.execute();

		} catch (CommandException e) {

			e.printStackTrace();

		}

	}

}
