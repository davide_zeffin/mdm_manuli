/*
 * Created on Jun 19, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search;


import java.util.ArrayList;
import java.util.List;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.data.SortDefinition;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.extension.schema.RepositorySchemaEx;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.schema.fields.LookupFieldProperties;
import com.sap.mdm.search.Search;
import com.sap.mdm.search.SearchParameter;
import com.sap.nw.mdm.rig.Program;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * An instance of this abstract class will peform some kind of search.
 * So far the abstract classes are only implementing the getSearchParameters(Repository) method,
 * the rest of the code is in this class.
 * 
 * The different types of search are defined by the static fields of this class.
 * <P>
 * Default implementations are provided for the following methods
 * <ul>
 * <li>getSearch(Repository) - creates a Search object for the main table
 * <li>getResultTablePath - returns null
 * <li>getResultDefinition(Repository) - creates a ResultDefinition that contains all the fields of the main table
 * <li>getSupportingResultDefinitions(Repository) - returns an array containing a result definition for 
 * each lookup field in the main table.
 * <li>getSortDefinition(Repository) - returns null
 * </ul>
 * 
 * All subclasses must implement the getSearchParameters(Repository) method.  
 * 
 * @author Richard LeBlanc
 */
abstract public class SearchProgram extends Program {
	
	/**
	 * Performs a keyword search
	 */
	static public SearchProgram KEYWORD = new KeywordSearchProgram();
	
	/**
	 * Performs a mask search
	 */
	static public SearchProgram MASK = new MaskSearchProgram();
	
	/**
	 * Performs a named search search
	 */
	static public SearchProgram NAMED_SEARCH = new NamedSearchSearchProgram();
	
	/**
	 * Performs a search on a qualifier field
	 */
	static public SearchProgram QUALIFIER = new QualifierSearchProgram();

	/**
	 * Performs a drill down search
	 */
	static public SearchProgram DRILL_DOWN_SEARCH = new DrillDownSearchProgram();


	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public void execute() {
		
		TableSchema tableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);
		
		
		Search search = getSearch(tableSchema);
		
		
		SearchParameter[] searchParameters = getSearchParameters(tableSchema);
		
		for(int i=0, j=searchParameters.length; i<j; i++) {
		
			search.addSearchItem(searchParameters[i]);	
			
		}
		
		
		ResultDefinition resultDefinition = getResultDefinition(tableSchema);
		
		SortDefinition sortDefinition = getSortDefinition(tableSchema);
		
		resultDefinition.setFieldSortingOrder(sortDefinition);
		
		FieldId[] resultTablePath = getResultTablePath(tableSchema);
		
		
		ResultDefinition[] supportingResultDefinitions = getSupportingResultDefinitions(tableSchema);		
		
		
		Record[] records = RecordDAO.getRecords(context, search, resultDefinition, resultTablePath, supportingResultDefinitions, 1000, 0);

		
		System.out.println("Number of records found = " + records.length);
				
		
		RecordPrinter.print(context, records, loginRegion);
		
	}
	
	/**
	 * Returns the <code>Search</code> which defines the search criteria that retrieved records must meet.
	 * 
	 * @param tableSchema - the search table
	 */
	protected Search getSearch(TableSchema tableSchema) {
		
		return new Search(tableSchema.getTable().getId());
		
	}
	
	
	/**
	 * Returns the search criteria to add to the search
	 * 
	 * @param tableSchema - the search table
	 */	
	abstract protected SearchParameter[] getSearchParameters(TableSchema tableSchema);
	
	
	/**
	 * Returns the <code>SortDefinition</code> which defines how the retrieved records should be sorted.
	 * 
	 * @param tableSchema - the result table
	 */
	protected SortDefinition getSortDefinition(TableSchema tableSchema) {
		
		return null;
		
	}
	
	
	/**
	 * Returns a <code>ResultDefinition</code> which defines the structure of the records being retrieved.
	 * 
	 * @param tableSchema - the result table
	 */
	protected ResultDefinition getResultDefinition(TableSchema tableSchema) {
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());

		resultDefinition.setSelectFields(tableSchema.getFieldIds());
		
		return resultDefinition;
		
	}
	
	
	/**
	 * Returns an array of field ids that point to the table from which to retrieve the results.
	 * 
	 * As an example, let's say we want to retrieve all Categories that are refered to by Products
	 * where the Product Name = "Acme". In order to have MDM return the correct results, 
	 * we must provide the path between the search table i.e. Products and the results 
	 * table i.e. Categories.  In this case there are two possibilities; the Category field 
	 * and the Alternate Category field.  This is important because the one you choose will have an
	 * impact on the results since the set of Categories that are referred to by the Category lookup
	 * field are different than those that are referred to by the Alternate Category lookup field.
	 * table.  So in this case, the FieldId array will contain either the Category field id or
	 * the Alternate Category field id.  You will find this functionality useful when you are performing
	 * a drill down search.
	 * 
	 * Products
	 *   Product Name
	 *   Category
	 *   Alternate Category
	 * 
	 * 
	 * Categories
	 *   Name   
	 * 
	 * 
	 * 
	 * @param tableSchema - the result table
	 */
	protected FieldId[] getResultTablePath(TableSchema tableSchema) {
		
		return null;
		
	}
	
	/**
	 * Returns an array of <code>ResultDefinition</code> which define which fields of the lookup 
	 * records to retrieve.
	 * <P>
	 * The value of a lookup field is the <code>RecordId</code> of the record being looked up. 
	 * So when retrieving a lookup field, it is possible to get information about the entire looked up record.
	 * This is done by defining a supporting <code>ResultDefinition</code> for those records.
	 *  
	 * @param tableSchema - the result table
	 */
	protected ResultDefinition[] getSupportingResultDefinitions(TableSchema tableSchema) {
		
		List resultDefinitionList = new ArrayList();
		
		ResultDefinition supportingResultDefinition = null;
		
		FieldProperties[] fields = tableSchema.getFields();
		
		LookupFieldProperties lookupField = null;
		
		RepositorySchemaEx schema = MetadataManager.getInstance().getRepositorySchema(context);
		
		TableSchema lookupTableSchema = null;
		
		for(int i=0, j=fields.length; i<j; i++){
		
			if(fields[i].getType() != FieldProperties.QUALIFIED_FLAT_LOOKUP_FIELD) {
				
				if(fields[i].isLookup()) {
					
					lookupField = (LookupFieldProperties)fields[i];
					
					lookupTableSchema = schema.getTableSchema(lookupField.getLookupTableId());
					
					supportingResultDefinition = new ResultDefinition(lookupField.getLookupTableId());
					
					supportingResultDefinition.setSelectFields(lookupTableSchema.getDisplayFieldIds());
					
					resultDefinitionList.add(supportingResultDefinition);
	
				}
				
			}
			
		}
		
		ResultDefinition[] supportingResultDefinitions = (ResultDefinition[])resultDefinitionList.toArray(new ResultDefinition[resultDefinitionList.size()]);
		
		return supportingResultDefinitions;
		
	}
		
	/**
	 * Returns all of the table's records.
	 * 
	 * @param tableSchema - the search/result table
	 * @return all of the table's records
	 */
	protected Record[] getAllRecords(TableSchema tableSchema) {
		
		Search search = new Search(tableSchema.getTable().getId());
	
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());

		resultDefinition.setSelectFields(tableSchema.getDisplayFieldIds());

		return RecordDAO.getRecords(context, search, resultDefinition, null, null, 1000, 0);
	
	}
	
}
