/*
 * Created on Apr 24, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud;

import com.sap.mdm.data.HierNode;
import com.sap.mdm.data.MultilingualString;
import com.sap.mdm.data.Record;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.CoupledAttributeProperties;
import com.sap.mdm.schema.MeasurementProperties;
import com.sap.mdm.schema.NumericAttributeProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.schema.TextAttributeProperties;
import com.sap.mdm.schema.TextAttributeValueProperties;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.dao.TaxonomyDAO;
import com.sap.nw.mdm.rig.data.util.HierarchyPrinter;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.TaxonomyRecordPrinter;
import com.sap.nw.mdm.rig.data.util.UOM;

/**
 * Creates, reads, updates and deletes a record in a taxonomy table.
 * It also creates attributes and links them to a record in a taxonomy table.
 * 
 * @author Richard LeBlanc
 */
class CRUDTaxonomyTableRecordWithAttributesProgram extends CRUDDataProgram {
	
	public void execute() {
		
		TableSchema categoryTableSchema = schema.getTableSchema(MDMAPISamples.Categories.TABLE);
		
		
		//Read Category hierarchy
		HierNode categoryHierarchy = getHierarchyNode(categoryTableSchema);


		//Print Category hierarchy
		System.out.println("Category hierarchy");		
		
		HierarchyPrinter.print(categoryHierarchy);
		
		
		System.out.println(System.getProperty("line.separator") + 
							  "-------------------------------------" +
							  System.getProperty("line.separator"));
		
		
		//Create the Attributes
		TextAttributeProperties textAttribute = createTextAttribute(categoryTableSchema.getTable().getId()); 
		
		NumericAttributeProperties numericAttribute = createNumericAttribute(categoryTableSchema.getTable().getId());
		
		CoupledAttributeProperties coupledNumericAttribute = createCoupledNumericAttribute(categoryTableSchema.getTable().getId());
		
		
		//create a field value pair for the category field
		FieldValuePair fieldValuePair = new FieldValuePair(categoryTableSchema.getFieldId(MDMAPISamples.Categories.CATEGORY),
															new StringValue("AAA New Category"));
															
		FieldValuePair[] fieldValuePairs = {fieldValuePair};
		
		
		//Create the Category
		RecordId categoryRecordID = createRecord(categoryTableSchema, fieldValuePairs);

		
		
		//Read Category
		Record categoryRecord = getRecordByID(categoryTableSchema, categoryRecordID);
		
		
		//Please note that you must retrieve the categoryRecord after each change to the record
		//to get the new record timestamp, otherwise the operation e.g. linking a new attribute, will fail.		
		
		
		//Link the text attribute to category
		TaxonomyDAO.linkAttribute(context, categoryTableSchema.getTable().getId(), 
									textAttribute.getId(), 
									categoryRecord.getId(), 
									categoryRecord.getChangeStamp());
		
		
		//Read Category
		categoryRecord = getRecordByID(categoryTableSchema, categoryRecordID);
		
		
		//Link the numeric attribute to category
		TaxonomyDAO.linkAttribute(context, categoryTableSchema.getTable().getId(), 
									numericAttribute.getId(), 
									categoryRecord.getId(), 
									categoryRecord.getChangeStamp());
									
		
		
		//Read Category
		categoryRecord = getRecordByID(categoryTableSchema, categoryRecordID);
		
		
		//Link couple numeric attribute to category
		TaxonomyDAO.linkAttribute(context,
									categoryTableSchema.getTable().getId(), 
									coupledNumericAttribute.getId(), 
									categoryRecord.getId(), 
									categoryRecord.getChangeStamp());
				
				
		
		//Read Category hierarchy
		categoryHierarchy = getHierarchyNode(categoryTableSchema);


		//Print Category hierarchy
		System.out.println("Updated category hierarchy");		
		
		HierarchyPrinter.print(categoryHierarchy);
		
		
		System.out.println(System.getProperty("line.separator") + 
							"-------------------------------------" +
							System.getProperty("line.separator"));
									
		
		//Print category record with attributes including valid values for text attribute
		System.out.println("Category record with text attribute values");
				
		TaxonomyRecordPrinter.print(context, categoryRecord, loginRegion);
	
	
		//Delete Attributes
		AttributeProperties[] attributes = {coupledNumericAttribute, numericAttribute, textAttribute};
		
		deleteAttributes(categoryTableSchema.getTable().getId(), attributes);
		
		
		//Read Category
		categoryRecord = getRecordByID(categoryTableSchema, categoryRecordID);
		
		
		//Delete Category
		deleteRecord(categoryTableSchema.getTable().getId(), categoryRecord);
		
		
		
		//Read Category hierarchy
		categoryHierarchy = getHierarchyNode(categoryTableSchema);
		
		
		//Print Category hierarchy
		System.out.println("Updated category hierarchy after deleting the category");		
	
		HierarchyPrinter.print(categoryHierarchy);
	
	
		System.out.println(System.getProperty("line.separator") + 
							  "-------------------------------------" +
							  System.getProperty("line.separator"));
								
	
	}
	
	/*
	 * Creates the Color text attribute with values: Blue and Red 
	 */
	private TextAttributeProperties createTextAttribute(TableId taxonomyTableID) {
		
		
		TextAttributeProperties textAttribute = new TextAttributeProperties();
				
		textAttribute.setTableId(taxonomyTableID);
		
		//an attribute's name is always multilingual
		//in this case we maintain the name for the given login language
		MultilingualString attributeName = new MultilingualString();

		attributeName.set(loginRegion.getRegionCode(), "Color");
		
		textAttribute.setName(attributeName);
		
		//an attribute's value is also multilingual
		//create the red attribute value
		MultilingualString blueAttributeValueString = new MultilingualString();
		
		blueAttributeValueString.set(loginRegion.getRegionCode(), "Blue");

		
		TextAttributeValueProperties blueTextAttributeValue = new TextAttributeValueProperties();
		
		blueTextAttributeValue.setName(blueAttributeValueString);
		
		
		//create the red attribute value
		MultilingualString redAttributeValueString = new MultilingualString();

		redAttributeValueString.set(loginRegion.getRegionCode(), "Red");
		
		
		TextAttributeValueProperties redTextAttributeValue = new TextAttributeValueProperties();

		redTextAttributeValue.setName(redAttributeValueString);


		//add the values to the attribute
		textAttribute.addTextAttributeValue(blueTextAttributeValue);

		textAttribute.addTextAttributeValue(redTextAttributeValue);

		
		return (TextAttributeProperties)TaxonomyDAO.createAttribute(context, textAttribute);
		
	}
	
	/*
	 * Creates a numeric attribute and assign the Length dimension and set feet as the
	 * default unit of measure.
	 */
	private NumericAttributeProperties createNumericAttribute(TableId taxonomyTableID) {
		
		//setting the name
		MultilingualString attributeName = new MultilingualString();
	
		attributeName.set(loginRegion.getRegionCode(), "Length");
		
		//setting the dimension and default unit of measure
		MeasurementProperties mp = new MeasurementProperties(UOM.getDimension("Length").getId(),
																UOM.getUnit("Length", "feet").getId(),
																Byte.parseByte("0"), false);
	

		NumericAttributeProperties numericAttribute = new NumericAttributeProperties();
			
		numericAttribute.setTableId(taxonomyTableID);
			
		numericAttribute.setName(attributeName);
			
		numericAttribute.setMeasurement(mp);
		
	
		return (NumericAttributeProperties)TaxonomyDAO.createAttribute(context, numericAttribute);
	
	}
	
	/*
	 * Create a coupled numeric attribute for HP @ RPM using Power as the dimension for the HP
	 * attribute using horsepower as the default unit of measure, and Velocity as the dimension for
	 * the RPM attribute using revolutions/minute as the default unit of measure.
	 */
	private CoupledAttributeProperties createCoupledNumericAttribute(TableId taxonomyTableID) {
		
		//setting the first part of the name
		MultilingualString attributeName = new MultilingualString();
		
		attributeName.set(loginRegion.getRegionCode(), "HP");
		
		
		MeasurementProperties measurement = new MeasurementProperties(UOM.getDimension("Power (Apparent)").getId(),
																						UOM.getUnit("Power (Apparent)", "horsepower").getId(),
																						Byte.parseByte("0"), false);
		
		//setting the second part of the name
		MultilingualString coupledAttributeName = new MultilingualString();

		coupledAttributeName.set(loginRegion.getRegionCode(), "RPM");


		MeasurementProperties coupledMeasurement = new MeasurementProperties(UOM.getDimension("Velocity (Angular)").getId(),
																		UOM.getUnit("Velocity (Angular)", "revolutions/minute").getId(),
																		Byte.parseByte("0"), false);


		CoupledAttributeProperties coupledAttribute = new CoupledAttributeProperties();
		
		coupledAttribute.setTableId(taxonomyTableID);
				
		coupledAttribute.setName(attributeName);
	
		coupledAttribute.setMeasurement(measurement);
		
		coupledAttribute.setCoupledDelimiter("@");

		coupledAttribute.setCoupledName(coupledAttributeName);
				
		coupledAttribute.setCoupledMeasurement(coupledMeasurement);
		
		return (CoupledAttributeProperties)TaxonomyDAO.createAttribute(context, coupledAttribute);

	}
	
	/*
	 * Delete the provided attributes in the taxonomy table
	 */
	private void deleteAttributes(TableId taxonomyTableID, AttributeProperties[] attributes) {
		
		for(int i=0, j=attributes.length; i<j; i++) {
		
			TaxonomyDAO.deleteAttribute(context, taxonomyTableID, attributes[i].getId());	
			
		}
		
		
	}
	
}
