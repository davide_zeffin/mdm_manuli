/*
 * Created on Apr 24, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud;

import java.util.GregorianCalendar;

import com.sap.mdm.data.Record;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.valuetypes.BooleanValue;
import com.sap.mdm.valuetypes.DateTimeValue;
import com.sap.mdm.valuetypes.MdmValueFactory;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * Creates, reads, updates and deletes a record in a main table.
 * 
 * @author Richard LeBlanc
 */
class CRUDMainTableRecordProgram extends CRUDDataProgram {
	
	public void execute() {
		
		//get metadata
		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);
		
		
		//get the field value pairs to create the record
		FieldValuePair[] fieldValuePairs = getFieldValuePairs(productsTableSchema);	
		
		
		//Create a product record and get the record id
		RecordId productRecordID = createRecord(productsTableSchema, fieldValuePairs); 
													
		
		//Read
		Record productRecord = getRecordByID(productsTableSchema, productRecordID);
		
		
		//Print
		System.out.println("New product record");	
		
		RecordPrinter.print(context, productRecord, loginRegion);
		
		
		System.out.println(System.getProperty("line.separator") + 
							"-------------------------------------" +
							System.getProperty("line.separator"));
		
		
		
		//get the updated field value pair array
		FieldValuePair[] updatedFieldValuePairs = getUpdatedFieldValuePairs(productsTableSchema);
		
		
		//Update
		updateRecord(productRecord, updatedFieldValuePairs);
		
		
		//Read
		Record updatedProductRecord = getRecordByID(productsTableSchema, productRecordID);
		
		
		//Print
		System.out.println("Updated product record");	
		
		RecordPrinter.print(context, updatedProductRecord, loginRegion);
		
		
		//Delete
		deleteRecord(productsTableSchema.getTable().getId(), updatedProductRecord);
		
	}
	
	private FieldValuePair[] getFieldValuePairs(TableSchema productsTableSchema) {
		
		FieldId productNameFieldID = productsTableSchema.getField(MDMAPISamples.Products.PRODUCT_NAME).getId();
			
		FieldId partNumberFieldID = productsTableSchema.getField(MDMAPISamples.Products.PART_NUMBER).getId();
			
		FieldId activeStockFieldID = productsTableSchema.getField(MDMAPISamples.Products.ACTIVE_STOCK).getId();

		FieldId approvalDateFieldID = productsTableSchema.getField(MDMAPISamples.Products.APPROVAL_DATE).getId();


		FieldValuePair productNameFieldValuePair = new FieldValuePair(productNameFieldID, new StringValue("AAAAA"));
	
		FieldValuePair partNumberFieldValuePair = new FieldValuePair(partNumberFieldID, new StringValue("a1b-2c/3d"));

		FieldValuePair activeStockFieldValuePair = new FieldValuePair(activeStockFieldID, MdmValueFactory.createBooleanValue(true));

		FieldValuePair approvalDateFieldValuePair = new FieldValuePair(approvalDateFieldID, new DateTimeValue(new GregorianCalendar(2007, 4, 30)));


		FieldValuePair[] fieldValuePairs = {productNameFieldValuePair,
											partNumberFieldValuePair,
											activeStockFieldValuePair,
											approvalDateFieldValuePair};


		return fieldValuePairs;

	}
	
	private FieldValuePair[] getUpdatedFieldValuePairs(TableSchema productsTableSchema) {
		
		FieldId productNameFieldID = productsTableSchema.getField(MDMAPISamples.Products.PRODUCT_NAME).getId();
			
		FieldId partNumberFieldID = productsTableSchema.getField(MDMAPISamples.Products.PART_NUMBER).getId();
			
		FieldId activeStockFieldID = productsTableSchema.getField(MDMAPISamples.Products.ACTIVE_STOCK).getId();

		FieldId approvalDateFieldID = productsTableSchema.getField(MDMAPISamples.Products.APPROVAL_DATE).getId();


		FieldValuePair productNameFieldValuePair = new FieldValuePair(productNameFieldID, new StringValue("AAAAA - updated"));
			
		FieldValuePair partNumberFieldValuePair = new FieldValuePair(partNumberFieldID, new StringValue("a1b-2c/3d - updated"));
		
		FieldValuePair activeStockFieldValuePair = new FieldValuePair(activeStockFieldID, new BooleanValue(false));
		
		FieldValuePair approvalDateFieldValuePair = new FieldValuePair(approvalDateFieldID, new DateTimeValue(new GregorianCalendar(2007, 06, 15)));


		FieldValuePair[] updatedFieldValuePairs = {productNameFieldValuePair,
													partNumberFieldValuePair,
													activeStockFieldValuePair,
													approvalDateFieldValuePair};



		return updatedFieldValuePairs;

	} 
	
}
