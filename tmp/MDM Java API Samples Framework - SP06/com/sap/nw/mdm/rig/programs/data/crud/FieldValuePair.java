/*
 * Created on Sep 7, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud;

import com.sap.mdm.ids.FieldId;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * Utility class that represents a field/value pair
 * where the field is a field id and the value is an MdmValue.
 * 
 * @author Richard LeBlanc
 *  
 */
class FieldValuePair {

	private FieldId fieldID;
	
	private MdmValue value;

	FieldValuePair(FieldId fieldID, MdmValue value) {
		
		this.fieldID = fieldID;
		
		this.value = value;
		
	}
	
	FieldId getField() {
		
		return fieldID;
		
	}
	
	MdmValue getValue() {
		
		return value;
		
	}
	
	void setValue(MdmValue value) {
		
		this.value = value;
		
	}

}
