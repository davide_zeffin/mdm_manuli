/*
 * Created on Jul 12, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.syndication;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.ids.PortId;
import com.sap.mdm.ids.RemoteSystemId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.repository.PortProperties;
import com.sap.mdm.repository.commands.GetPortListCommand;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;

/**
 * Syndicates records to a port.  The records to be syndicated are determined by
 * the Syndication Map defined in the port.
 * 
 * @author Richard LeBlanc
 */
class SyndicatePortProgram extends SyndicationProgram {

	private UserSessionContext context;

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public void execute() {
		
		PortId portID = getPortId("Products");
		
		RemoteSystemId remoteSystemID = MetadataManager.getInstance().getRepositorySchema(context).getRemoteSystemId("ERP");
		
		Syndicator.syndicatePort(context, portID, remoteSystemID, "Products");
		
	}
	
	/*
	 * Retrieves the port id for the given port code
	 */
	private PortId getPortId(String portCode) {
		
		GetPortListCommand cmd = null;
		
		try {
		
			cmd = new GetPortListCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		try {
			
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
			
		}
		
		PortProperties[] ports = cmd.getPorts();
		
		PortProperties port = null;
		
		for(int i=0, j=ports.length; i<j; i++) {
	
			if(ports[i].getCode().equals(portCode)) {
				
				port = ports[i];
				
			}
			
		}
		
		return port.getId();
		
	}
	
}
