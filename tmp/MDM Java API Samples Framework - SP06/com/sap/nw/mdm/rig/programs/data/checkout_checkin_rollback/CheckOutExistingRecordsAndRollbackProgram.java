/*
 * Created on Apr 26, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.checkout_checkin_rollback;

import com.sap.mdm.data.Record;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * This program checks out the first 10 records of the product table,
 * modifies them and performs a rollback of the changes.
 * 
 * @author Richard LeBlanc
 */
class CheckOutExistingRecordsAndRollbackProgram extends CheckoutExistingRecordsProgram {
	
	public void execute() {
		
		//get the metadata		
		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);


		//create an array to hold the record ids of the first 10
		//records in the product table
		RecordId[] productRecordIDs = new RecordId[10];
		
		
		//get the first 10 records of the product table
		Record[] productRecords = getFirstXRecords(productsTableSchema, 10);
		
		
		//print the records before they're checked out, modified and checked in
		System.out.println("Records before modifications");
		RecordPrinter.print(context, productRecords, loginRegion);
		System.out.println("");
		
		
		//go through each record and put the record id in an array
		//to be used to check out the records
		for(int i=0, j=productRecords.length; i<j; i++) {
			
			productRecordIDs[i] = productRecords[i].getId();
			
		}
		
		//A record checkout causes MDS to create a new version of the record with a new
		//record id.  It is this record id that is returned and must be used for the checkin.
		RecordId[] checkedOutProductRecordIDs = CheckOutCheckInManager.checkOutRecords(context, productsTableSchema.getTable().getId(), productRecordIDs);
		
		
		//get the new version of the record using the checkout record id(s)
		productRecords = getRecordsByID(productsTableSchema, checkedOutProductRecordIDs);
		
		//update the record
		modifyRecords(productRecords, productsTableSchema);
		
		
		//get the updated records
		Record[] updatedProductRecords = getRecordsByID(productsTableSchema, checkedOutProductRecordIDs);
		
		
		//print the records before they're rolled back
		System.out.println("Records after modifications");
		RecordPrinter.print(context, updatedProductRecords, loginRegion);
		
		
		//roll back the records
		CheckOutCheckInManager.rollbackRecords(context, productsTableSchema.getTable().getId(), checkedOutProductRecordIDs);

		
		//get the rolled back records using the original record ids since the checkout record ids
		//are temporary
		Record[] rolledBackProductRecords = getRecordsByID(productsTableSchema, productRecordIDs);
		
		
		//print the rolled back records
		System.out.println("Records after rollback");
		RecordPrinter.print(context, rolledBackProductRecords, loginRegion);
		
	}
	
}