/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.field;

import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.BooleanSearchConstraint;
import com.sap.mdm.search.FieldSearchDimension;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * Performs a search on a boolean field.
 * 
 * @author Richard LeBlanc
 */
class BooleanFieldSearchProgram extends FieldSearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		SearchDimension searchDimension = new FieldSearchDimension(tableSchema.getField(MDMAPISamples.Products.ACTIVE_STOCK).getId());
		 
		SearchConstraint searchConstraint = null; 
		
		
		System.out.println("Products where Active Stock = false");
		
//		searchConstraint = new BooleanSearchConstraint(true);
		searchConstraint = new BooleanSearchConstraint(false);
		
		
		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);
		
		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;
		
	}	

}
