/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.field;

import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.FieldSearchDimension;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.mdm.search.TextSearchConstraint;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * Performs a search on a Text field.
 * 
 * @author Richard LeBlanc
 */
class TextFieldSearchProgam extends FieldSearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */		
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		SearchDimension searchDimension = new FieldSearchDimension(tableSchema.getField(MDMAPISamples.Products.PRODUCT_NAME).getId());

		SearchConstraint searchConstraint = null;
		
		
		System.out.println("Products where Product Name contains the string 'Set'");
		
		
		searchConstraint = new TextSearchConstraint("Set", TextSearchConstraint.CONTAINS);
//		searchConstraint = new TextSearchConstraint("", TextSearchConstraint.ENDS_WITH);
//		searchConstraint = new TextSearchConstraint("", TextSearchConstraint.EQUALS);
//		searchConstraint = new TextSearchConstraint("", TextSearchConstraint.EXCLUDES);
//		searchConstraint = new TextSearchConstraint("", TextSearchConstraint.SOUNDS_LIKE);
//		searchConstraint = new TextSearchConstraint("", TextSearchConstraint.STARTS_WITH);


		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);

		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;

	}

}
