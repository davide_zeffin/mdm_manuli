/*
 * Created on Apr 24, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud;

import com.sap.mdm.data.HierNode;
import com.sap.mdm.data.Record;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.valuetypes.LookupValue;
import com.sap.mdm.valuetypes.MultiValue;
import com.sap.nw.mdm.rig.data.util.HierarchyPrinter;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * Creates, reads, updates and deletes a record in a main table that 
 * contains flat and hierarchy lookup fields.
 * 
 * 
 * @author Richard LeBlanc
 */
class CRUDMainTableRecordWithFlatAndHierarchyLookupFieldsProgram extends CRUDDataProgram {
	
	public void execute() {
		
		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);
		
		TableSchema regionsTableSchema = schema.getTableSchema(MDMAPISamples.Location_Regions.TABLE);
		
		TableSchema manufacturersTableSchema = schema.getTableSchema(MDMAPISamples.Manufacturers.TABLE);
	
		TableSchema distributorsTableSchema = schema.getTableSchema(MDMAPISamples.Distributors.TABLE);

		//When creating a record with lookup fields,
		//you need to present the user with the list of values the user can select from.
		//Here i am retrieving all the values for the various lookup fields and printing them out.

		//Get all regions and print them
		Record[] regions = getAllRecords(regionsTableSchema);
		
		System.out.println("Regions");
		RecordPrinter.print(context, regions, loginRegion);
		System.out.println("");
		
		//Get all manufacturers and print them
		//Please note that only leaf nodes can be assigned to a record
		HierNode manufacturers = getHierarchyNode(manufacturersTableSchema);
		
		System.out.println("Manufacturers");
		HierarchyPrinter.print(manufacturers);
		System.out.println("");
		
		//Get all regions and print them
		Record[] distributors = getAllRecords(distributorsTableSchema);
		
		System.out.println("Distributors");
		RecordPrinter.print(context, distributors, loginRegion);
		System.out.println("");
		
		
		
		FieldValuePair[] fieldValuePairs = getFieldValuePairs(productsTableSchema,
																regionsTableSchema,
																distributorsTableSchema,
																manufacturersTableSchema);
		
		//Create the record
		RecordId productRecordID = createRecord(productsTableSchema, fieldValuePairs);
		
		
		//Read
		Record productRecord = getRecordByID(productsTableSchema, productRecordID);
		
		
		//Print
		System.out.println("New product record");
		
		RecordPrinter.print(context, productRecord, loginRegion);
		
		
		System.out.println(System.getProperty("line.separator") + 
							"-------------------------------------" +
							System.getProperty("line.separator"));
		
		
		//Get the updated field/value pairs
		FieldValuePair[] updatedFieldValuePairs = getUpdatedFieldValuePairs(productsTableSchema,
																			regionsTableSchema,
																			distributorsTableSchema,
																			manufacturersTableSchema);
		
		//Update
		updateRecord(productRecord, updatedFieldValuePairs);
		
		
		//Read
		Record updatedProductRecord = getRecordByID(productsTableSchema, productRecordID);
		
		
		//Print
		System.out.println("Updated product record");
		
		RecordPrinter.print(context, updatedProductRecord, loginRegion);
		
		
		//Delete
		deleteRecord(productsTableSchema.getTable().getId(), updatedProductRecord);
		
	}
	
	/*
	 * Returns the field/value pairs for the record to be created
	 */
	private FieldValuePair[] getFieldValuePairs(TableSchema productsTableSchema,
												TableSchema regionsTableSchema,
												TableSchema distributorsTableSchema,
												TableSchema manufacturersTableSchema) {
		
		
		//get the field ids for all fields we want to update
		FieldId regionFieldID = productsTableSchema.getFieldId(MDMAPISamples.Products.REGION);

		FieldId distributorsFieldID = productsTableSchema.getFieldId(MDMAPISamples.Products.DISTRIBUTORS);
		
		FieldId manufacturerFieldID = productsTableSchema.getFieldId(MDMAPISamples.Products.MANUFACTURER);
		
		//get the field ids for the tables we're getting records from
		FieldId regionNameFieldID = regionsTableSchema.getFieldId(MDMAPISamples.Location_Regions.NAME);
		
		FieldId distributorNameFieldID = distributorsTableSchema.getFieldId(MDMAPISamples.Distributors.NAME);
		
		FieldId manufacturerNameFieldID = manufacturersTableSchema.getFieldId(MDMAPISamples.Manufacturers.NAME);
				
		
		//before we can create the field/value pairs for the lookup fields,
		//we must have the record ids of the records in the tables
		//the lookup fields point to
		
		
		//get the Los Angeles region record and create a lookup value with it's record id
		Record region = getRecordByValue(regionsTableSchema, regionNameFieldID, "Los Angeles");
		
		LookupValue regionValue = new LookupValue(region.getId());
		
		
		//the distributor lookup field is a multi-valued lookup field,
		//wich means it can point to more than on distributor record
		
		//get the distributor records for Roland and Markus 
		Record roland = getRecordByValue(distributorsTableSchema, distributorNameFieldID, "Roland");
		Record markus = getRecordByValue(distributorsTableSchema, distributorNameFieldID, "Markus");
		
		//create lookup values for both records
		LookupValue rolandValue = new LookupValue(roland.getId());
		LookupValue markusValue = new LookupValue(markus.getId());
		
		//create a multi value and add both lookup values
		MultiValue distributorValues = new MultiValue();
		
		distributorValues.addValue(rolandValue);
		distributorValues.addValue(markusValue);
		
		
		//get the Professional Tools manufacturer record and create a lookup value with it's
		//record id
		Record manufacturer = getRecordByValue(manufacturersTableSchema, manufacturerNameFieldID, "Professional Tools");

		LookupValue manufacturerValue = new LookupValue(manufacturer.getId());

		//create the field/value pairs for the lookup fields
		FieldValuePair regionFieldValuePair = new FieldValuePair(regionFieldID, regionValue);
		
		FieldValuePair distributorsFieldValuePair = new FieldValuePair(distributorsFieldID, distributorValues);
		
		FieldValuePair manufacturerFieldValuePair = new FieldValuePair(manufacturerFieldID, manufacturerValue);
		


		FieldValuePair[] fieldValuePairs = {regionFieldValuePair,
											distributorsFieldValuePair,
											manufacturerFieldValuePair};


		return fieldValuePairs;

	}

	/*
	 * Returns the field/value pairs for the record to be updated
	 */
	private FieldValuePair[] getUpdatedFieldValuePairs(TableSchema productsTableSchema,
												TableSchema regionsTableSchema,
												TableSchema distributorsTableSchema,
												TableSchema manufacturersTableSchema) {
		
		
		//get the field ids for all fields we want to update
		FieldId regionFieldID = productsTableSchema.getField(MDMAPISamples.Products.REGION).getId();

		FieldId distributorsFieldID = productsTableSchema.getField(MDMAPISamples.Products.DISTRIBUTORS).getId();
		
		FieldId manufacturerFieldID = productsTableSchema.getField(MDMAPISamples.Products.MANUFACTURER).getId();
		
		
		//get the field ids for the tables we're getting records from
		FieldId regionNameFieldID = regionsTableSchema.getFieldId(MDMAPISamples.Location_Regions.NAME);
		
		FieldId distributorNameFieldID = distributorsTableSchema.getFieldId(MDMAPISamples.Distributors.NAME);
		
		FieldId manufacturerNameFieldID = manufacturersTableSchema.getFieldId(MDMAPISamples.Manufacturers.NAME);
		
		
		
		//before we can create the field/value pairs for the lookup fields,
		//we must have the record ids of the records in the tables
		//the lookup fields point to
		
		
		//get the Walldorf region record and create a lookup value with it's record id
		Record region = getRecordByValue(regionsTableSchema, regionNameFieldID, "Walldorf");
		
		LookupValue regionValue = new LookupValue(region.getId());
		
		
		//the distributor lookup field is a multi-valued lookup field,
		//wich means it can point to more than on distributor record
		
		//get the distributor records for Diego and Thomas
		Record diego = getRecordByValue(distributorsTableSchema, distributorNameFieldID, "Diego");
		Record thomas = getRecordByValue(distributorsTableSchema, distributorNameFieldID, "Thomas");
		
		//create lookup values for both records
		LookupValue diegoValue = new LookupValue(diego.getId());
		LookupValue thomasValue = new LookupValue(thomas.getId());
		
		//create a multi value and add both lookup values
		MultiValue distributorValues = new MultiValue();
		
		distributorValues.addValue(diegoValue);
		distributorValues.addValue(thomasValue);
		
		
		//get the The Stronghand Corporation manufacturer record and create a lookup value with it's
		//record id
		Record manufacturer = getRecordByValue(manufacturersTableSchema, manufacturerNameFieldID, "The Stronghand Corporation");

		LookupValue manufacturerValue = new LookupValue(manufacturer.getId());

		//create the field/value pairs for the lookup fields
		FieldValuePair regionFieldValuePair = new FieldValuePair(regionFieldID, regionValue);
		
		FieldValuePair distributorsFieldValuePair = new FieldValuePair(distributorsFieldID, distributorValues);
		
		FieldValuePair manufacturerFieldValuePair = new FieldValuePair(manufacturerFieldID, manufacturerValue);
		


		FieldValuePair[] fieldValuePairs = {regionFieldValuePair,
											distributorsFieldValuePair,
											manufacturerFieldValuePair};


		return fieldValuePairs;

	} 
	
}
