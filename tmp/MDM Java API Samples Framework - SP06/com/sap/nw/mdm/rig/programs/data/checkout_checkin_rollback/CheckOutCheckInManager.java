/*
 * Created on Aug 30, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.checkout_checkin_rollback;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.commands.CheckinRecordsCommand;
import com.sap.mdm.data.commands.CheckoutRecordsAsNewCommand;
import com.sap.mdm.data.commands.CheckoutRecordsCommand;
import com.sap.mdm.data.commands.RollbackRecordsCommand;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;

/**
 * This class allows you to perform most checkout/checkin/rollback related functions.
 * 
 * @author Richard LeBlanc
 */
public class CheckOutCheckInManager {

	/**
	 * Returns a special "check out" record id for each record id provided.  
	 * These record ids aren't the same as those provided to perform the check out.
	 * It is these "check out" record ids that must be used when performing any subsequent modifications
	 * to those records as well as when calling <code>checkInRecords(UserSessionContext, TableId, RecordId)</code>.
	 * If you use the original record ids, the check in will fail.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table that contains the records to check out
	 * @param recordIDs - the records to check out
	 */
	static public RecordId[] checkOutRecords(UserSessionContext context, TableId tableID, RecordId[] recordIDs) {
		
		CheckoutRecordsCommand cmd = null;
		
		try {
		
			cmd = new CheckoutRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
			
		} catch (ConnectionException e) {
			
			e.printStackTrace();
			
		}
	
		cmd.setTableId(tableID);
	
		cmd.setRecordIds(recordIDs);
	
		cmd.setExclusive(true);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
		
		return cmd.getCheckOutRecordIds();
	
	}
	/**
	 * Creates a new record that is checked out.
	 * 
	 * Please note that this method only returns one record id.
	 * The API implementation of the CheckoutRecordsAsNewCommand hasn't been enabled 
	 * to create multiple checked out records.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table in which to create the checked out record
	 */
	static public RecordId[] checkOutRecordsAsNew(UserSessionContext context, TableId tableID) {
	
		CheckoutRecordsAsNewCommand cmd = null;
		
		try {
		
			cmd = new CheckoutRecordsAsNewCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
	
		cmd.setTableId(tableID);
	
		cmd.setExclusive(true);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
	
		return cmd.getCheckOutRecordIds();
	
	}
	/**
	 * Checks in the records for the given "check out" record ids. 
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table that contains the records to be checked in
	 * @param recordIDs - the special "check out" record ids of the records to be checked in
	 * @see CheckOutCheckInManager#checkOutRecords(UserSessionContext, TableId, RecordId[])
	 */
	static public void checkInRecords(UserSessionContext context, TableId tableID, RecordId[] recordIDs) {
	
		CheckinRecordsCommand cmd = null;
		
		try {
		
			cmd = new CheckinRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
	
		cmd.setTableId(tableID);
	
		cmd.setRecordIds(recordIDs);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
	
	}
	/**
	 * Performs a rollback of any changes made to the record after it was checked out.  
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table that contains the records to be checked in
	 * @param recordIDs - the special "check out" record ids of the records to be checked in
	 * @see CheckOutCheckInManager#checkOutRecords(UserSessionContext, TableId, RecordId[])
	 */
	static public void rollbackRecords(UserSessionContext context, TableId tableID, RecordId[] recordIDs) {
	
		RollbackRecordsCommand cmd = null;
		
		try {
		
			cmd = new RollbackRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
			
			e.printStackTrace();
			
		}
	
		cmd.setTableId(tableID);
	
		cmd.setRecordIds(recordIDs);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
	
	}
}
