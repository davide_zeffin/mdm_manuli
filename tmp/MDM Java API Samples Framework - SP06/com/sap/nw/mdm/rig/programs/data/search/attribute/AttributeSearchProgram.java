/*
 * Created on Jul 4, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.attribute;

import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.ids.AttributeId;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.Search;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.nw.mdm.rig.data.dao.AttributeDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.programs.data.search.SearchProgram;

/**
 * This is an abstract class that all Attribute searches extend.
 * 
 * Please note that the execute() method, which contains most of the logic,
 * is actually from the SearchProgram super class.  The AttributeSearchProgram subclasses
 * mostly just implement the getSearchParameters(Repository) method.
 *
 * Please note that all attribute searches can only be performed as a pick list/drill down search.
 * This means that you must select the attribute from the list of attributes that are referenced by main table records.
 * And once you've selected the attribute, you must select the value from the list of values that the selected attribute contains.
 * This behavior is identical to the drill down search in the Data Manager.
 * 
 * @author Richard LeBlanc
 */
abstract public class AttributeSearchProgram extends SearchProgram {
	
	/**
	 * Performs a lookup search on a Coupled Numeric attribute
	 */
	static public final AttributeSearchProgram COUPLED_NUMERIC = new CoupledNumericAttributeSearchProgram();
	
	
	/**
	 * Performs a lookup search on a Numeric attribute
	 */
	static public final AttributeSearchProgram NUMERIC = new NumericAttributeSearchProgram();
	
	
	/**
	 * Performs a lookup search on a Text attribute
	 */
	static public final AttributeSearchProgram TEXT = new TextAttributeSearchProgram();
	
	
	/**
	 * Returns the first instance of the given attribute type
	 * 
	 * @param repository - the repository on which to perform the task
	 * @param productsTableSchema - the product table schema
	 * @param attributeType - the type of attribute as defined in the AttributeProperties class
	 * 
	 */
	protected AttributeProperties getAttribute(TableSchema productsTableSchema, int attributeType) {
		
		Search search = new Search(productsTableSchema.getTable().getId());
		
		//get all attributes that are linked to a category that is refered to by existing product records
		AttributeProperties[] attributes = AttributeDAO.getAttributes(context, search, productsTableSchema.getFieldId(MDMAPISamples.Products.CATEGORY));

		AttributeProperties attribute = null;

		//go through the attributes and return the first instance of the provided type
		for(int i=0, j=attributes.length; i<j; i++) {

			if(attributes[i].getType() == attributeType) {
	
				attribute = attributes[i];
				
				break;
	
			}

		}
	
		return attribute;
	
	}
	
	/**
	 * Returns the first value from the list of all values that exist for the given attribute
	 * 
	 * @param repository - the repository on which to perform the task
	 * @param tableID - the main table id
	 * @param categoryFieldID - the taxonomy lookup field id
	 * @param attributeID - the attribute id
	 */
	protected MdmValue getValue(TableId tableID, 
								FieldId categoryFieldID, AttributeId attributeID) {
		
		Search search = new Search(tableID);
		
		MdmValue[] values = AttributeDAO.getAttributeValues(context, search, categoryFieldID, attributeID);
		
		return values[0];
		
	}

	/**
	 * Returns a <code>ResultDefinition</code> which defines the structure of the 
	 * records being retrieved and whether attributes are returned.
	 * <P>
	 * This method overrides the one from SearchProgram because we are including the attributes
	 * in the ResultDefinition by calling the setLoadAttributes() method and passing true. 
	 * 
	 * @param tableSchema - the result table
	 */	
	protected ResultDefinition getResultDefinition(TableSchema tableSchema) {
		
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());
		
		resultDefinition.setSelectFields(tableSchema.getFieldIds());
		
		resultDefinition.setLoadAttributes(true);
		
		return resultDefinition;
		
	}
	
}