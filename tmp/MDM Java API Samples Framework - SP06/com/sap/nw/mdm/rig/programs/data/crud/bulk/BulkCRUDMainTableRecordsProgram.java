/*
 * Created on Jun 14, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud.bulk;

import java.util.ArrayList;
import java.util.List;

import com.sap.mdm.data.HierNode;
import com.sap.mdm.data.MdmValueTypeException;
import com.sap.mdm.data.MultipleRecordsResult;
import com.sap.mdm.data.Record;
import com.sap.mdm.data.RecordFactory;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.Search;
import com.sap.mdm.valuetypes.LookupValue;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.dao.HierarchyDAO;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * Creates, reads, updates and deletes multiple records in a main table.
 * 
 * @author Richard LeBlanc
 */
class BulkCRUDMainTableRecordsProgram extends BulkCRUDDataProgram {

	TableSchema productsTableSchema;
	
	TableSchema manufacturersTableSchema;
	
	public void execute() {
		
		//get metadata
		productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);
				
		manufacturersTableSchema = schema.getTableSchema(MDMAPISamples.Manufacturers.TABLE);
		
		
		//Create the records
		MultipleRecordsResult results = createProductRecords();
		
		
		//Print error messages for records that weren't created
		printFailures(results);
		
		//Get the record ids of the successfully created records
		int[] successfulRecordIndices = results.getSucceededRecords();
		
		RecordId[] recordIDs = new RecordId[successfulRecordIndices.length];
		
		for(int i=0, j=successfulRecordIndices.length; i<j; i++) {
			
			recordIDs[i] = results.getSucceededRecord(successfulRecordIndices[i]);
			
		}
		
		//Read
		Record[] productRecords = getRecordsByID(recordIDs);
		
		
		//Print
		System.out.println("New product records");	
		
		RecordPrinter.print(context, productRecords, loginRegion);


		System.out.println(System.getProperty("line.separator") + 
							"-------------------------------------" +
							System.getProperty("line.separator"));
				
		
		//Update the successful records
		updateProductRecords(productRecords);
		
		
		//Read
		Record[] updatedProductRecords = getRecordsByID(recordIDs);
		
		
		//Print
		RecordPrinter.print(context, updatedProductRecords, loginRegion);
		
		
		//Delete the records
		deleteProductRecords(productsTableSchema.getTable().getId(), recordIDs);

	}
	
	/*
	 * Creates 10 new records
	 */
	private MultipleRecordsResult createProductRecords() {
			
		//Retrieve all the manufacturer records
		//Usually you would present the user with this list for selection
		//Remember that only leaf nodes can be selected
		HierNode manufacturers = getTree(manufacturersTableSchema);
	
		//Get the leaf nodes
		HierNode[] leafManufacturers = getLeafNodes(manufacturers);
	
		//Get the record id of the first leaf manufacturer record and use it
		//as the value for the Manufacturer hierarchy lookup field
		LookupValue manufacturer = new LookupValue(leafManufacturers[0].getId());
		
				
			
		Record[] newRecords = new Record[10];
		
		for(int i=0, j=newRecords.length; i<j; i++) {
			
			newRecords[i] = RecordFactory.createEmptyRecord(productsTableSchema.getTable().getId());
			
			try {
				
				newRecords[i].setFieldValue(productsTableSchema.getField(MDMAPISamples.Products.PART_NUMBER).getId(), 
											new StringValue("Part Number " + String.valueOf(i)));
											
				newRecords[i].setFieldValue(productsTableSchema.getField(MDMAPISamples.Products.MANUFACTURER).getId(), 
											manufacturer);
											
			} catch (IllegalArgumentException e) {
				
				e.printStackTrace();
				
			} catch (MdmValueTypeException e) {
				
				e.printStackTrace();
				
			}
			
		}
		
		return RecordDAO.createRecords(context, productsTableSchema.getTable().getId(), newRecords);
		
	}
	
	/*
	 * Updates the given records
	 */
	private void updateProductRecords(Record[] records) {
		
		//Retrieve all the manufacturer records
		//Usually you would present the user with this list for selection
		//Remember that only leaf nodes can be selected
		HierNode manufacturers = getTree(manufacturersTableSchema);
	
		//Get the leaf nodes
		HierNode[] leafManufacturers = getLeafNodes(manufacturers);
	
		//Get the record id of the first leaf manufacturer record and use it
		//as the value for the Manufacturer hierarchy lookup field
		LookupValue manufacturer = new LookupValue(leafManufacturers[1].getId());
	
		String oldValue = null;
		
	
		for(int i=0, j=records.length; i<j; i++) {
		
			try {
		
				oldValue = ((StringValue)records[i].getFieldValue(productsTableSchema.getFieldId(MDMAPISamples.Products.PART_NUMBER))).getString();
		
				records[i].setFieldValue(productsTableSchema.getField(MDMAPISamples.Products.PART_NUMBER).getId(), 
															new StringValue(oldValue + "_updated"));

				records[i].setFieldValue(productsTableSchema.getField(MDMAPISamples.Products.MANUFACTURER).getId(), 
											manufacturer);										
						
			} catch (IllegalArgumentException e) {
		
				e.printStackTrace();
		
			} catch (MdmValueTypeException e) {
		
				e.printStackTrace();
		
			}
		
		}	
	
		RecordDAO.modifyRecords(context, productsTableSchema.getTable().getId(), records);
	
	}
	
	/*
	 * Deletes the given records
	 */
	private void deleteProductRecords(TableId tableID, RecordId[] recordIDs) {
		
		RecordDAO.deleteRecords(context, tableID, recordIDs);
		
	}
	
	/*
	 * Prints the error messages for those records that weren't successfully created
	 */
	private void printFailures(MultipleRecordsResult results) {
		
		int[] failedRecords = results.getFailedRecords();
		
		for(int i=0, j=failedRecords.length; i<j; i++) {
			
			System.out.println(results.getFailedRecordMessage(i));
			
		}
		
	}
	
	private Record[] getRecordsByID(RecordId[] recordIDs) {
		
		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);
		
		TableId manufacturersTableID = schema.getTableId(MDMAPISamples.Manufacturers.TABLE);

		
		ResultDefinition resultDefinition = new ResultDefinition(productsTableSchema.getTable().getId());

		resultDefinition.addSelectField(productsTableSchema.getFieldId(MDMAPISamples.Products.PART_NUMBER));
	
		resultDefinition.addSelectField(productsTableSchema.getFieldId(MDMAPISamples.Products.MANUFACTURER));
	

		ResultDefinition supportingResultDefinition = new ResultDefinition(manufacturersTableID);
		
		ResultDefinition[] supportingResultDefinitions = {supportingResultDefinition};
		
		
		Record[] records = RecordDAO.getRecordsByID(context, resultDefinition, supportingResultDefinitions, recordIDs);

		return records;

	}
	
	/**
	 * Returns all records of the provided table as a hierarchy
	 * 
	 * @param tableSchema - the table containing the records
	 */
	private HierNode getTree(TableSchema tableSchema) {
	
		Search search = new Search(tableSchema.getTable().getId());

		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());

		resultDefinition.setSelectFields(tableSchema.getDisplayFieldIds());

		return HierarchyDAO.getHierarchyNode(context, search, null, null, resultDefinition, null);

	}

	/**
	 * Returns all the leaf nodes for the given tree.
	 *  
	 * @param tree - the tree containing the leaf nodes
	 */
	private HierNode[] getLeafNodes(HierNode tree) {
	
		List nodeList = new ArrayList();
	
		fillList(tree, nodeList);
	
		HierNode[] leafNodes = (HierNode[])nodeList.toArray(new HierNode[nodeList.size()]);
	
		return leafNodes;
	
	
	}

	/*
	 * A recursive function that fills the provided list with the leaf nodes
	 * contained in the node 
	 */
	private void fillList(HierNode node, List list) {
	
		if(node.isLeaf()) {

			list.add(node);
		
		} else {
		
			HierNode[] children = node.getChildren();

			if(children != null) {

				for(int i=0, j=children.length; i<j; i++) {

					fillList(children[i], list);

				}

			}	
		
		}
	
	}
	
}
