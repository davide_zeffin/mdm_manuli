/*
 * Created on Oct 9, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.blobs;

import com.sap.mdm.data.Record;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.dao.BLOBDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * This class retrieves the first record from the PDF table, gets the PDF document as a byte array
 * and writes it to a file.
 * 
 * @author Richard LeBlanc
 */
class RetrievePDFAndWriteToFileProgram extends BLOBDataProgram {

	/*
	 *  (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute()
	 */
	public void execute() {
		
		//get the table schema of the PDF table
		TableSchema tableSchema = MetadataManager.getInstance().getRepositorySchema(context).getTableSchema(MDMAPISamples.PDF.TABLE);
		
		//get the first record in the PDF table
		//note: this does not retrieve the PDF, only information about the PDF
		Record record = getFirstRecord(tableSchema);
		
		//get the original file name of the PDF when it was first imported in MDM
		String fileName = ((StringValue)record.getFieldValue(tableSchema.getFieldId(MDMAPISamples.Images.ORIGINAL_NAME))).getString();
		
		//get the PDF as a byte array by passing the table id and record id of the PDF table record 
		byte[] bytes = BLOBDAO.getBlob(context, tableSchema.getTable().getId(), record.getId(), loginRegion);

		//take the bytes and write the PDF file
		writeToFile(bytes, fileName);

	}
	
	

}
