/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search;

import com.sap.mdm.data.Record;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.MaskSearchDimension;
import com.sap.mdm.search.PickListSearchConstraint;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.mdm.valuetypes.LookupValue;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * Performs a mask search
 * 
 * @author Richard LeBlanc
 */
class MaskSearchProgram extends SearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */		
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		TableSchema masksTableSchema = MetadataManager.getInstance().getRepositorySchema(context).getTableSchema(MDMAPISamples.Masks.TABLE);


		//get all mask records
		Record[] maskRecords = getAllRecords(masksTableSchema);
	
		
		System.out.println("Records from Mask table");
		
		//print the mask records
		RecordPrinter.print(context, maskRecords, loginRegion);
	
		System.out.println("");
		
		
		System.out.println("Products contained in first mask record");
	
	
		//select the first one and create a lookup value
		MdmValue lookupValue = new LookupValue(maskRecords[0].getId());

		MdmValue[] values = {lookupValue};


		SearchDimension searchDimension = new MaskSearchDimension();

		SearchConstraint searchConstraint = new PickListSearchConstraint(values);


		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);

		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;

	}
	
}
