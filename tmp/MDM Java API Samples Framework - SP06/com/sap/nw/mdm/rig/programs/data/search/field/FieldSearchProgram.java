/*
 * Created on Jul 4, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.field;

import com.sap.nw.mdm.rig.programs.data.search.SearchProgram;

/**
 * Subclasses will peform some kind of field search.  Most of the logic is contained in the
 * SearchProgram superclass.  Classes extending this class will mostly override the 
 * getSearchParameters(Repository) method.
 * 
 * The different types of field searches are defined by the static fields of this class.
 * 
 * @author Richard LeBlanc
 */
abstract public class FieldSearchProgram extends SearchProgram {
	
	/**
	 * Performs a search on a Boolean field.
	 */
	static public FieldSearchProgram BOOLEAN = new BooleanFieldSearchProgram();
		
	/**
	 * Performs a search on an Integer field.
	 */
	static public FieldSearchProgram CURRENCY = new CurrencyFieldSearchProgram();

	/**
	 * Performs a search on a Literal Date field.
	 */
	static public FieldSearchProgram LITERAL_DATE = new LiteralDateFieldSearchProgram();

	/**
	 * Performs a search on a Lookup field.
	 */
	static public FieldSearchProgram LOOKUP = new LookupFieldSearchProgram();

	/**
	 * Performs a search on a Text field.
	 */
	static public FieldSearchProgram TEXT = new TextFieldSearchProgam();

}
