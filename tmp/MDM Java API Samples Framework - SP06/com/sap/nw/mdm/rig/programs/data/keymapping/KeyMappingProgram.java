/*
 * Created on Aug 20, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.keymapping;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.search.Search;
import com.sap.mdm.session.UserSessionContext;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.Program;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;

/**
 * An abstract class that identifies key mapping related programs.
 * It contains a set of static variables that point to these various programs.
 * 
 * @author Richard LeBlanc
 */
public abstract class KeyMappingProgram extends Program {

	/**
	 * Retrieves key mapping information for an existing record
	 */
	static public final KeyMappingProgram RETRIEVE = new RetrieveKeyMappingProgram();
	
	
	/**
	 * Updates the key mappings for an existing record
	 */
	static public final KeyMappingProgram MODIFY = new ModifyKeyMappingProgram();
	
	
	/**
	 * Gets the first record where the provided field/value are a match.
	 * 
	 * @param repository - the repository on which to perform the task
	 * @param tableID - the table to search
	 * @param fieldID - the field to search
	 * @param value - the value to search for
	 * @return the first record where the provided field/value are a match 
	 */
	protected Record getRecordByValue(UserSessionContext context, TableId tableID, FieldId fieldID, String value) {
		
		Search search = new Search(tableID);
		
		ResultDefinition resultDefinition = new ResultDefinition(tableID);
		
		StringValue[] values = {new StringValue(value)};
		
		Record[] records = RecordDAO.getRecordsByValue(context, fieldID, values, resultDefinition, null);
		
		return records[0];
		
	}

}
