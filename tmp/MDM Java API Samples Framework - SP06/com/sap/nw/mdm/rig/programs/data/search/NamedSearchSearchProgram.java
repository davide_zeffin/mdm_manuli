/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search;

import com.sap.mdm.data.Record;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.NamedSearchSearchDimension;
import com.sap.mdm.search.PickListSearchConstraint;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.mdm.valuetypes.LookupValue;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * Performs a named search search
 * 
 * @author Richard LeBlanc
 */
class NamedSearchSearchProgram extends SearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */		
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		TableSchema namedSearchesTableSchema = MetadataManager.getInstance().getRepositorySchema(context).getTableSchema(MDMAPISamples.NamedSearches.TABLE);


		//get all named search records
		Record[] namedSearchesRecords = getAllRecords(namedSearchesTableSchema);
	
		
		System.out.println("Named Searches records");
		
		RecordPrinter.print(context, namedSearchesRecords, loginRegion);
		
		System.out.println("");
		
		
		System.out.println("Products contained in first named search record");
		
	
		//select the first one and create a lookup value
		MdmValue lookupValue = new LookupValue(namedSearchesRecords[0].getId());

		
		MdmValue[] lookupValues = {lookupValue};


		SearchDimension searchDimension = new NamedSearchSearchDimension();

		SearchConstraint searchConstraint = new PickListSearchConstraint(lookupValues);


		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);

		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;

	}
	
}
