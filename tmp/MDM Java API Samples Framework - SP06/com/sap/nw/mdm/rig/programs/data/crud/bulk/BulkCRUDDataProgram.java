/*
 * Created on Jul 4, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud.bulk;

import com.sap.nw.mdm.rig.Program;

/**
 * 
 * This abstract class contains a set of static fields that represents each 
 * of the Bulk CRUD data programs that can be executed.
 * 
 * @author Richard LeBlanc
 */
abstract public class BulkCRUDDataProgram extends Program {
	
	/**
	 * This BuldCRUDDataProgram creates, reads, updates and deletes multiple records at a time 
	 */
	static public BulkCRUDDataProgram BULK_CRUD_MAIN_TABLE = new BulkCRUDMainTableRecordsProgram();  
	
}
