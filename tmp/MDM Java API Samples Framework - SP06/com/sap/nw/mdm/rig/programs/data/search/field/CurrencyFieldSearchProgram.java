/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.field;

import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.FieldSearchDimension;
import com.sap.mdm.search.NumericSearchConstraint;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * Performs a search on an Currency field.
 * 
 * @author Richard LeBlanc
 */
class CurrencyFieldSearchProgram extends FieldSearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */		
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		SearchDimension searchDimension = new FieldSearchDimension(tableSchema.getField(MDMAPISamples.Products.COST_PRICE).getId());

		SearchConstraint searchConstraint = null;

		
		System.out.println("Products where Cost Price >= 20");
		
		
		searchConstraint = new NumericSearchConstraint(20, NumericSearchConstraint.GREATER_THAN_OR_EQUAL_TO);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.GREATER_THAN);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.EQUALS);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.NOT_EQUAL);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.LESS_THAN);
//		searchConstraint = new NumericSearchConstraint(0, NumericSearchConstraint.LESS_THAN_OR_EQUAL_TO);

		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);

		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;

	}

}