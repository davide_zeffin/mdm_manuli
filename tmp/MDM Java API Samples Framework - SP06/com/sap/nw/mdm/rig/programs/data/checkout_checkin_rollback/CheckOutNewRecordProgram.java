/*
 * Created on Sep 7, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.nw.mdm.rig.programs.data.checkout_checkin_rollback;

import com.sap.mdm.data.MdmValueTypeException;
import com.sap.mdm.data.Record;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.valuetypes.StringValue;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * This abstract class provides a helper method for CheckOutNewRecordProgram subclasses.
 * 
 * @author Richard LeBlanc
 */
abstract class CheckOutNewRecordProgram extends CheckOutCheckInRecordsProgram{
	
	/**
	 * Sets the value of the product name and part number fields and returns the record.
	 * This doesn't persist anything in MDM.
	 * 
	 * @param productsTableSchema - the products table schema
	 * @param record - the record to modify
	 * @return
	 */
	protected Record modifyRecord(TableSchema productsTableSchema, Record record) {
		
		FieldId productNameFieldID = productsTableSchema.getFieldId(MDMAPISamples.Products.PRODUCT_NAME);
		FieldId partNumberFieldID = productsTableSchema.getFieldId(MDMAPISamples.Products.PART_NUMBER);
		
		try {
	
			record.setFieldValue(productNameFieldID, new StringValue("AAA Acme Hammer"));
	
			record.setFieldValue(partNumberFieldID, new StringValue("AAA123BB"));
	
		} catch (IllegalArgumentException e) {
	
			e.printStackTrace();
	
		} catch (MdmValueTypeException e) {
	
			e.printStackTrace();
	
		}
		
		return record;
		
	}

	

}
