/*
 * Created on Apr 24, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud;

import com.sap.mdm.data.HierNode;
import com.sap.mdm.data.MdmValueTypeException;
import com.sap.mdm.data.Record;
import com.sap.mdm.data.RecordFactory;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.extension.schema.AttributeSchema;
import com.sap.mdm.extension.schema.LinkedAttribute;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.CoupledAttributeProperties;
import com.sap.mdm.schema.NumericAttributeProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.schema.TextAttributeProperties;
import com.sap.mdm.valuetypes.CoupledMeasurementValue;
import com.sap.mdm.valuetypes.LookupValue;
import com.sap.mdm.valuetypes.MeasurementValue;
import com.sap.mdm.valuetypes.MultiValue;
import com.sap.mdm.valuetypes.TextAttributeValue;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.HierarchyPrinter;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;
import com.sap.nw.mdm.rig.data.util.TaxonomyRecordPrinter;

/**
 * Creates, reads, updates and deletes a record in a main table that contains a taxonomy lookup field.
 * 
 * 
 * @author Richard LeBlanc
 */
class CRUDMainTableRecordWithTaxonomyLookupFieldAndAttributesProgram extends CRUDDataProgram {

	public void execute() {
		
		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);
		
		TableSchema categoriesTableSchema = schema.getTableSchema(MDMAPISamples.Categories.TABLE);
		
		//get the category taxonomy lookup field id
		FieldId categoryFieldID = categoriesTableSchema.getFieldId(MDMAPISamples.Categories.CATEGORY);
		
		
		//When creating a record with a taxonomy lookup field,
		//the user must first be presented with a list of categories to choose from
		HierNode categories = getHierarchyNode(categoriesTableSchema);
		
		
		System.out.println("Taxonomy Table Hierarchy");
		
		//print the category hierarchy
		HierarchyPrinter.print(categories);
		
		
		//Get the category record for Power Drills
		Record categoryRecord = getRecordByValue(categoriesTableSchema, categoryFieldID, "Power Drills");
		
		
		//once the category is selected, all the attributes for that category
		//must be displayed, including possible values for a text attribute
		//and possibly units of measure for a numeric and couple numeric attribute
		//if a dimensions was assigned.  
		
		System.out.println("Category Record");
		
		//Print the category record including the attributes  
		TaxonomyRecordPrinter.print(context, categoryRecord, loginRegion);
		
		
		System.out.println(System.getProperty("line.separator") + 
									"-------------------------------------" +
									System.getProperty("line.separator"));
									
		
		//Create the product record
		RecordId productRecordID = createProductRecord(productsTableSchema,
														categoriesTableSchema,
														categoryRecord.getId());
				
		
		//Read
		Record productRecord = getRecordByID(productsTableSchema, productRecordID);
		
		
		//Print
		System.out.println("New product record");
		
		RecordPrinter.print(context, productRecord, loginRegion);
		
	
		System.out.println(System.getProperty("line.separator") + 
							"-------------------------------------" +
							System.getProperty("line.separator"));
		
				
		//Update
		updateCategoryAttributeValues(productsTableSchema, categoriesTableSchema, productRecord);
		
		
		//Read
		Record updatedProductRecord = getRecordByID(productsTableSchema, productRecordID);
		
		
		//Print
		System.out.println("Updated product record");
		
		RecordPrinter.print(context, updatedProductRecord, loginRegion);
		
		
		//Delete
		deleteRecord(productsTableSchema.getTable().getId(), updatedProductRecord);
		
	}
	
	/*
	 * Creates a record in the products table with an assigned category and attributes
	 */
	private RecordId createProductRecord(TableSchema productsTableSchema,
											TableSchema categoriesTableSchema,
											RecordId categoryRecordID) {
		
		Record record = RecordFactory.createEmptyRecord(productsTableSchema.getTable().getId());

		try {
			
			//Create a lookup value with the category record id
			LookupValue categoryValue = new LookupValue(categoryRecordID);
			
			//assign the lookup value to the category lookup field
			record.setFieldValue(productsTableSchema.getField(MDMAPISamples.Products.CATEGORY).getId(), categoryValue);
			
			
			AttributeSchema schema = MetadataManager.getInstance().getAttributeSchema(context);
			
			//get the category's linked attributes
			LinkedAttribute[] linkedAttributes = schema.getLinkedAttributes(categoriesTableSchema.getTable().getId(), categoryRecordID);
			
			AttributeProperties attribute = null;
			
			String attributeName = null;
			
			for(int i=0, j=linkedAttributes.length; i<j; i++) {
				
				attribute = linkedAttributes[i].getAttributeProperties();
				
				attributeName = attribute.getName().get(loginRegion.getRegionCode());
							
				System.out.println(attributeName);
				
				if(attributeName.equalsIgnoreCase("Input Voltage")) {
				
					CoupledAttributeProperties coupledAttribute = (CoupledAttributeProperties)attribute;
						
					MeasurementValue primaryValue = new MeasurementValue(115, coupledAttribute.getMeasurement().getDefaultUnitId());
	
					MeasurementValue secondaryValue = new MeasurementValue(60, coupledAttribute.getCoupledMeasurement().getDefaultUnitId());
		
			
					CoupledMeasurementValue coupledMeasurementValue = new CoupledMeasurementValue(primaryValue, secondaryValue);
			
					//a coupled numeric attribute is always multi valued
					MultiValue multiValue = new MultiValue();
		
					multiValue.addValue(coupledMeasurementValue);
			
		
					record.setAttributeValue(productsTableSchema.getField(MDMAPISamples.Products.CATEGORY).getId(), coupledAttribute.getId(), multiValue);	
					
				}
					
				if(attributeName.equalsIgnoreCase("Power Capacity")) {
				
					NumericAttributeProperties numericAttribute = (NumericAttributeProperties)attribute;
					
					//a numeric attribute always contains a measurement value
					//if a dimension hasn't been specified, provide a null unit id.
					MeasurementValue measurementValue = new MeasurementValue(300, numericAttribute.getMeasurement().getDefaultUnitId());
					
					record.setAttributeValue(productsTableSchema.getField(MDMAPISamples.Products.CATEGORY).getId(), attribute.getId(), measurementValue);
							
				}
				
				
				if(attributeName.equalsIgnoreCase("Type")) {
					
					TextAttributeProperties textAttribute = (TextAttributeProperties)attribute;
		
					//a text attribute behaves like a lookup
					//get the first valid attribute value
					TextAttributeValue textAttributeValue = new TextAttributeValue(textAttribute.getTextAttributeValue(0).getId());
					
					record.setAttributeValue(productsTableSchema.getField(MDMAPISamples.Products.CATEGORY).getId(), textAttribute.getId(), textAttributeValue);
						
				}
				
			}			
			
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
			
		} catch (MdmValueTypeException e) {
			
			e.printStackTrace();
			
		}
		
		return RecordDAO.createRecord(context, record);

	}
	
	/*
	 * Updates the values of the attributes for the given product record
	 */
	private void updateCategoryAttributeValues(TableSchema productsTableSchema,
											TableSchema categoriesTableSchema,
											Record record) {
		
		LookupValue categoryLookupValue = (LookupValue)record.getFieldValue(productsTableSchema.getFieldId(MDMAPISamples.Products.CATEGORY)); 
		
		RecordId categoryRecordID = categoryLookupValue.getLookupId();
		
		AttributeSchema schema = MetadataManager.getInstance().getAttributeSchema(context);
			
		//get the category's linked attributes
		LinkedAttribute[] linkedAttributes = schema.getLinkedAttributes(categoriesTableSchema.getTable().getId(), categoryRecordID);
		
		AttributeProperties attribute = null;
		
		String attributeName = null;
		
		try {
		
			for(int i=0, j=linkedAttributes.length; i<j; i++) {
				
				attribute = linkedAttributes[i].getAttributeProperties();
				
				attributeName = attribute.getName().get(loginRegion.getRegionCode());
							
				if(attributeName.equalsIgnoreCase("Input Voltage")) {
				
					CoupledAttributeProperties coupledAttribute = (CoupledAttributeProperties)attribute;
		
					MeasurementValue primaryValue = new MeasurementValue(220, coupledAttribute.getMeasurement().getDefaultUnitId());

					MeasurementValue secondaryValue = new MeasurementValue(50, coupledAttribute.getCoupledMeasurement().getDefaultUnitId());


					CoupledMeasurementValue coupledMeasurementValue = new CoupledMeasurementValue(primaryValue, secondaryValue);

					//a coupled numeric attribute is always multi valued
					MultiValue multiValue = new MultiValue();

					multiValue.addValue(coupledMeasurementValue);


					record.setAttributeValue(productsTableSchema.getField(MDMAPISamples.Products.CATEGORY).getId(), coupledAttribute.getId(), multiValue);	
	
				}
	
				if(attributeName.equalsIgnoreCase("Power Capacity")) {

					NumericAttributeProperties numericAttribute = (NumericAttributeProperties)attribute;
	
					//a numeric attribute always contains a measurement value
					//if a dimension hasn't been specified, provide a null unit id.
					MeasurementValue measurement = new MeasurementValue(320, numericAttribute.getMeasurement().getDefaultUnitId());
	
					record.setAttributeValue(productsTableSchema.getField(MDMAPISamples.Products.CATEGORY).getId(), numericAttribute.getId(), measurement);
			
				}


				if(attributeName.equalsIgnoreCase("Type")) {
	
					TextAttributeProperties textAttribute = (TextAttributeProperties)attribute;

					//a text attribute behaves like a lookup
					//get the first valid attribute value
					TextAttributeValue textAttributeValue = new TextAttributeValue(textAttribute.getTextAttributeValue(1).getId());
	
					record.setAttributeValue(productsTableSchema.getField(MDMAPISamples.Products.CATEGORY).getId(), textAttribute.getId(), textAttributeValue);
		
				}
	
			}
			
		} catch (IllegalArgumentException e) {
		
			e.printStackTrace();
		
		} catch (MdmValueTypeException e) {
		
			e.printStackTrace();
		
		}
	
		RecordDAO.modifyRecord(context, record, false);

	}	

	
	
}