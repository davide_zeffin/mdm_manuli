/*
 * Created on Apr 24, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.crud;

import com.sap.mdm.data.Record;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.valuetypes.DoubleValue;
import com.sap.mdm.valuetypes.IntegerValue;
import com.sap.mdm.valuetypes.QualifiedLookupValue;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * Creates, reads, updates and deletes a record in a main table that contains a 
 * qualified lookup field.
 * 
 * @author Richard LeBlanc
 */
class CRUDMainTableRecordWithQualifiedLookupFieldProgram extends CRUDDataProgram {
	
	public void execute() {
		
		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);
		
		TableSchema pricingRegionsTableSchema = schema.getTableSchema(MDMAPISamples.Pricing_Regions.TABLE);
		
		
		//When creating a record with lookup fields,
		//you need to present the user with the list of values the user can select from.
		//Here i am retrieving all the values for the various lookup fields and printing them out.

		//Get all pricing regions 
		Record[] pricingRegions = getAllRecords(pricingRegionsTableSchema);


		//and print them
		RecordPrinter.print(context, pricingRegions, loginRegion);

		
		//Get the field/value pairs
		FieldValuePair[] fieldValuePairs = {getFieldValuePair(productsTableSchema, pricingRegionsTableSchema)};

		
		//Create the record
		RecordId recordID = createRecord(productsTableSchema, fieldValuePairs);
		
		
		//Read
		Record productRecord = getRecordByID(productsTableSchema, recordID);
		
		
		//Print
		System.out.println("New product record");	
		
		RecordPrinter.print(context, productRecord, loginRegion);


		System.out.println(System.getProperty("line.separator") + 
							"-------------------------------------" +
							System.getProperty("line.separator"));
		
		
		//Get the updated field/value pairs
		FieldValuePair[] updatedFieldValuePairs = {getUpdatedFieldValuePair(productsTableSchema,
																			pricingRegionsTableSchema,
																			productRecord)};
		
		//Update
		updateRecord(productRecord, updatedFieldValuePairs);
		
		
		//Read
		Record updatedProductRecord = getRecordByID(productsTableSchema, recordID);
		
		
		//Print
		System.out.println("Updated product record");
		
		RecordPrinter.print(context, updatedProductRecord, loginRegion);
		
		
		//Delete
		deleteRecord(productsTableSchema.getTable().getId(), updatedProductRecord);
		
	}
	
	/*
	 * Returns the field/value pair for the qualified lookup field
	 */
	private FieldValuePair getFieldValuePair(TableSchema productTableSchema,
												TableSchema pricingRegionsTableSchema) {
		
		//before we can create the field/value pair for the qualified lookup field,
		//we must have the record id of the records in the qualified table
		//the qualified lookup field point to
		
		//get the field id for every field we want to update
		FieldId regionalListPriceFieldID = productTableSchema.getFieldId(MDMAPISamples.Products.REGIONAL_LIST_PRICE);
		
		
		
		Record areaA = getRecordByValue(pricingRegionsTableSchema,
										pricingRegionsTableSchema.getFieldId(MDMAPISamples.Pricing_Regions.NAME),
										"Area A");
														
		Record areaB = getRecordByValue(pricingRegionsTableSchema,
										pricingRegionsTableSchema.getFieldId(MDMAPISamples.Pricing_Regions.NAME),
										"Area B");
		
		QualifiedLookupValue qlv = new QualifiedLookupValue();
			
		//create first qualified record
		int linkID1 = qlv.createQualifiedLink(areaA.getId());
			
		qlv.setQualifierFieldValue(linkID1, pricingRegionsTableSchema.getField(MDMAPISamples.Pricing_Regions.LIST_PRICE).getId(), new DoubleValue(10));
		qlv.setQualifierFieldValue(linkID1, pricingRegionsTableSchema.getField(MDMAPISamples.Pricing_Regions.PACKAGE_QUANTITY).getId(), new IntegerValue(1));
			
		//create second qualified record
		int linkID2 = qlv.createQualifiedLink(areaB.getId());
			
		qlv.setQualifierFieldValue(linkID2, pricingRegionsTableSchema.getField(MDMAPISamples.Pricing_Regions.LIST_PRICE).getId(), new DoubleValue(50));			
		qlv.setQualifierFieldValue(linkID2, pricingRegionsTableSchema.getField(MDMAPISamples.Pricing_Regions.PACKAGE_QUANTITY).getId(), new IntegerValue(6));
			
		FieldValuePair fieldValuePair = new FieldValuePair(regionalListPriceFieldID, qlv);
		
		return fieldValuePair;		
			
	}
	
	/*
	 * Returns the updated field/value pair for the qualified lookup field
	 */
	private FieldValuePair getUpdatedFieldValuePair(TableSchema productTableSchema,
													TableSchema pricingRegionsTableSchema,
													Record record) {
		
		//before we can create the field/value pair for the qualified lookup field,
		//we must have the record id of the records in the qualified table
		//the qualified lookup field point to
		
		//get the field id for every field we want to update
		FieldId regionalListPriceFieldID = productTableSchema.getFieldId(MDMAPISamples.Products.REGIONAL_LIST_PRICE);
		
		QualifiedLookupValue qlv = (QualifiedLookupValue)record.getFieldValue(regionalListPriceFieldID);
		
		
		//update the price for the first qualified record
		qlv.setQualifierFieldValue(0, pricingRegionsTableSchema.getField(MDMAPISamples.Pricing_Regions.LIST_PRICE).getId(), new DoubleValue(12));
		qlv.setQualifierFieldValue(0, pricingRegionsTableSchema.getField(MDMAPISamples.Pricing_Regions.PACKAGE_QUANTITY).getId(), new IntegerValue(1));
			
		//update the price for the second qualified record
		qlv.setQualifierFieldValue(1, pricingRegionsTableSchema.getField(MDMAPISamples.Pricing_Regions.LIST_PRICE).getId(), new DoubleValue(60));			
		qlv.setQualifierFieldValue(1, pricingRegionsTableSchema.getField(MDMAPISamples.Pricing_Regions.PACKAGE_QUANTITY).getId(), new IntegerValue(6));
		
		FieldValuePair fieldValuePair = new FieldValuePair(regionalListPriceFieldID, qlv);
		
		return fieldValuePair;		
			
	}
	
	
	
	
}
