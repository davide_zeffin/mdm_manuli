/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.field;

import java.util.GregorianCalendar;

import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.DateTimeSearchConstraint;
import com.sap.mdm.search.FieldSearchDimension;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * Performs a search on a Literal Date field
 * 
 * @author Richard LeBlanc
 */
class LiteralDateFieldSearchProgram extends FieldSearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		GregorianCalendar calendar = null;
		
		//the month is a zero based index so January is 0, February is 1, etc...
		calendar = new GregorianCalendar(2000, 0, 1);
	
	
		SearchDimension searchDimension = new FieldSearchDimension(tableSchema.getField(MDMAPISamples.Products.APPROVAL_DATE).getId());

		SearchConstraint searchConstraint = null;
		
		
		System.out.println("Records where Approval Date <= January 1st 2000");
		
		
//		searchConstraint = new DateTimeSearchConstraint(calendar, DateTimeSearchConstraint.GREATER_THAN_OR_EQUAL_TO);
//		searchConstraint = new DateTimeSearchConstraint(calendar, DateTimeSearchConstraint.GREATER_THAN);
//		searchConstraint = new DateTimeSearchConstraint(calendar, DateTimeSearchConstraint.EQUALS);
//		searchConstraint = new DateTimeSearchConstraint(calendar, DateTimeSearchConstraint.NOT_EQUAL);
//		searchConstraint = new DateTimeSearchConstraint(calendar, DateTimeSearchConstraint.LESS_THAN);
		searchConstraint = new DateTimeSearchConstraint(calendar, DateTimeSearchConstraint.LESS_THAN_OR_EQUAL_TO);
		
	
	
		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);
	
		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;
	
	}

}
