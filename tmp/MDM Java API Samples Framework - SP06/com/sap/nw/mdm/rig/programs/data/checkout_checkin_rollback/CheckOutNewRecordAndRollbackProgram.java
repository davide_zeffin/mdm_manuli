/*
 * Created on Apr 26, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.checkout_checkin_rollback;

import com.sap.mdm.data.Record;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * This program creates a new checked out record, modifies it and then performs a rollback.
 * The end result is that no record is created.
 * 
 * @author Richard LeBlanc
 */
class CheckOutNewRecordAndRollbackProgram extends CheckOutNewRecordProgram {
	
	public void execute() {
		
		//get the metadata
		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);

		
		//get the new record id
		//please note that even though the method is called checkOutRecordsAsNew,
		//the current implementation on returns one record id
		//so there's only one record id returned in the array
		RecordId[] productRecordIDs = CheckOutCheckInManager.checkOutRecordsAsNew(context, productsTableSchema.getTable().getId());		

		//get the newly created empty record
		Record newProductRecord = getRecordByID(productsTableSchema, productRecordIDs[0]); 
		
		//populate the record
		modifyRecord(productsTableSchema, newProductRecord);
		
		
		//persist the record in MDM
		RecordDAO.modifyRecord(context, newProductRecord, false);
		
		
		//get the new checked out record
		newProductRecord = getRecordByID(productsTableSchema, productRecordIDs[0]);
		
		
		//print the record before it's rolled back
		System.out.println("New record");
		RecordPrinter.print(context, newProductRecord, loginRegion);
		
		
		//rollback the new record
		CheckOutCheckInManager.rollbackRecords(context, productsTableSchema.getTable().getId(), productRecordIDs);
		
	}
	
}	