/*
 * Created on Aug 20, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.keymapping;

import com.sap.mdm.data.KeyMapping;
import com.sap.mdm.data.Record;
import com.sap.mdm.data.RecordKeyMapping;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.RemoteSystemId;
import com.sap.mdm.ids.TableId;
import com.sap.nw.mdm.rig.data.dao.KeyMappingDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * Retrieves key mapping information.
 * 
 * @author Richard LeBlanc
 */
class RetrieveKeyMappingProgram extends KeyMappingProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public void execute() {
		
		TableId tableID = schema.getTableId(MDMAPISamples.Products.TABLE);
		
		FieldId fieldID = schema.getFieldId(MDMAPISamples.Products.TABLE, 
															MDMAPISamples.Products.PART_NUMBER);
		
		//get the product that has part number 68899
		Record record = getRecordByValue(context, tableID, fieldID, "68899");
			
		RecordId[] recordIDs = {record.getId()};
		
		//get the key mappings
		RecordKeyMapping[] recordKMs = KeyMappingDAO.getKeyMappings(context, tableID, recordIDs, false);
		
		RecordKeyMapping recordKM = null;
		
		RemoteSystemId[] remoteSystemIDs = null;
		
		String remoteSystemName = null;
		
		KeyMapping km = null;
		
		String[] keyValues = null;
		
		//loop through all key mappings for all records
		for(int i=0, j=recordKMs.length; i<j; i++) {
			
			//get the current record
			recordKM = recordKMs[i];
			
			//one record can have many key mappings i.e. remote system/remote key combinations
			//get all the remote systems for the current record
			remoteSystemIDs = recordKM.getRemoteSystems();
			
			//loop through all the remote systems
			for(int k=0, l=remoteSystemIDs.length; k<l; k++) {
				
				//get the name of the remote system
				remoteSystemName = schema.getRemoteSystem(remoteSystemIDs[k]).
									getName().get(loginRegion.getRegionCode());
				
				//get the key mappings for the current remote system
				km = recordKM.getKeyMapping(remoteSystemIDs[i]);
				
				//get all the remote keys for the current remote system
				keyValues = km.getKeys();
				
				//go through the remote keys for the current key mapping and print out 
				//the remote system name and the remote key 
				for(int m=0, n=keyValues.length; m<n; m++) {
					
					System.out.println(remoteSystemName + " " + keyValues[m]);		
									
				}
								
			}
			
		}
		
	}	

}
