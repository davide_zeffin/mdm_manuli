/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.field;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.schema.fields.LookupFieldProperties;
import com.sap.mdm.search.FieldSearchDimension;
import com.sap.mdm.search.PickListSearchConstraint;
import com.sap.mdm.search.Search;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.mdm.valuetypes.LookupValue;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * Performs a search on a Lookup field
 * 
 * @author Richard LeBlanc
 */
class LookupFieldSearchProgram extends FieldSearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearchParameters(com.sap.nw.mdm.rig.repository.Repository)
	 */		
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		LookupFieldProperties manufacturerLookupField = (LookupFieldProperties)tableSchema.getField(MDMAPISamples.Products.MANUFACTURER);
		
		MdmValue value = getFirstReferencedLookupValue(tableSchema, manufacturerLookupField);
		
		MdmValue[] searchValues = {value};	
				

		SearchDimension searchDimension = new FieldSearchDimension(tableSchema.getField(MDMAPISamples.Products.MANUFACTURER).getId());

		SearchConstraint searchConstraint = new PickListSearchConstraint(searchValues);


		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);

		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;

	}
	
	//returns the first record
	private LookupValue getFirstReferencedLookupValue(TableSchema tableSchema, LookupFieldProperties lookupField) {
		
		TableSchema manufacturerTableSchema = MetadataManager.getInstance().getRepositorySchema(context).getTableSchema(lookupField.getLookupTableId());
		
		//set the Products table as the search table
		Search search = new Search(tableSchema.getTable().getId());
		
		//set the Product Types table as the result definition table
		ResultDefinition resultDefinition = new ResultDefinition(manufacturerTableSchema.getTable().getId());
		
		//define the path to the result table using the lookup field ids 
		//e.g. the Manufacturer lookup field points to the Manufacturers table which is the one we want records from
		FieldId[] resultTablePath = {MetadataManager.getInstance().getRepositorySchema(context).getField(MDMAPISamples.Products.TABLE, MDMAPISamples.Products.MANUFACTURER).getId()};


		//get all Manufacturer records that are referred to by Producs records
		Record[] manufacturerRecords = RecordDAO.getRecords(context, search, resultDefinition, resultTablePath, null, 0, 0);
		
		//create a lookup value with the second Manufacturer record returned
		//the first record returned is for the null lookup value (as you see in the Data Manager)
		LookupValue lookupValue = new LookupValue(manufacturerRecords[1].getId());
				
		return lookupValue;

	}

}