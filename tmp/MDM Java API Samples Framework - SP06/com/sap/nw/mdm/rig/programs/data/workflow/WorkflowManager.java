/*
 * Created on Aug 31, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.workflow;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.ids.WorkflowId;
import com.sap.mdm.ids.WorkflowJobId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;
import com.sap.mdm.workflow.WorkflowActionMenuNode;
import com.sap.mdm.workflow.WorkflowProperties;
import com.sap.mdm.workflow.WorkflowTask;
import com.sap.mdm.workflow.WorkflowTaskAction;
import com.sap.mdm.workflow.commands.AddRecordsToWorkflowJobCommand;
import com.sap.mdm.workflow.commands.CreateWorkflowJobCommand;
import com.sap.mdm.workflow.commands.ExecuteWorkflowTaskActionCommand;
import com.sap.mdm.workflow.commands.RetrieveWorkflowTaskActionsCommand;
import com.sap.mdm.workflow.commands.RetrieveWorkflowTasksCommand;
import com.sap.mdm.workflow.commands.RetrieveWorkflowsCommand;

/**
 * This class provides basic workflow functionalities
 *
 * @author Richard LeBlanc
 */
public class WorkflowManager {

	/**
	 * Returns all workflow records linked to the given table.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table id of the table the workflow record is linked to
	 * @return all workflow records linked to the given table
	 */
	static public WorkflowProperties[] getWorkflows(UserSessionContext context, TableId tableID) {
		
		RetrieveWorkflowsCommand cmd = null;
		
		try {
		
			cmd = new RetrieveWorkflowsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setTableId(tableID);
		
		try {
			
			cmd.execute();
			
		} catch (CommandException e) {
			
			e.printStackTrace();
			
		}
		
		return cmd.getWorkflows();
		
	}
	
	/**
	 * Creates a new workflow job for the given workflow.
	 * The workflow job contains the given records.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param workflowID - the workflow for which to create a job
	 * @param recordIDs - the records to add to the workflow job
	 * @return workflow job id of the newly created job
	 */
	static public WorkflowJobId createWorkflowJob(UserSessionContext context, WorkflowId workflowID,
													RecordId[] recordIDs) {
		
		CreateWorkflowJobCommand cmd = null;
		
		try {
		
			cmd = new CreateWorkflowJobCommand(context);

		} catch (SessionException e) {

			e.printStackTrace();

		} catch (ConnectionException e) {

			e.printStackTrace();

		}

		cmd.setWorkflowId(workflowID);
		
		cmd.setRecordIds(recordIDs);
		
		try {
			
			cmd.execute();
			
		} catch (CommandException e) {
			
			e.printStackTrace();
			
		}
		
		return cmd.getWorkflowJobId();
		
	}
	
	/**
	 * Adds the given records to the given workflow job
	 * 
	 * @param context - the context used to connect to the repository
	 * @param workflowJobID - the workflow job for which to add the records
	 * @param recordIDs - the records to add
	 */
	static public void addRecordsToWorkflowJob(UserSessionContext context, WorkflowJobId workflowJobID,
												RecordId[] recordIDs) {
	
		AddRecordsToWorkflowJobCommand cmd = null;
		
		try {
		
			cmd = new AddRecordsToWorkflowJobCommand(context);
		
		} catch (SessionException e) {

			e.printStackTrace();

		} catch (ConnectionException e) {

			e.printStackTrace();

		}

		
		cmd.setWorkflowJobId(workflowJobID);
		
		cmd.setRecordIds(recordIDs);
		
		try {
			
			cmd.execute();
			
		} catch (CommandException e) {
			
			e.printStackTrace();
			
		}
		
	}
	
	/**
	 * Returns the workflow tasks for the given table.
	 * Which workflow tasks to return are controlled by the given workflow task type.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table for which to get the workflow tasks
	 * @param workflowTaskType - the type(s) of workflow tasks to retrieve
	 * @return workflow tasks for the given table
	 * @see com.sap.mdm.workflow.WorkflowTask.Type
	 */
	static public WorkflowTask[] getWorkflowTasks(UserSessionContext context, TableId tableID,
													int workflowTaskType) {
		
		RetrieveWorkflowTasksCommand cmd = null;
		
		try {
		
			cmd = new RetrieveWorkflowTasksCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}		
		
		
		cmd.setTableId(tableID);
		
		cmd.setTaskType(workflowTaskType);
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
			
		}
		
		return cmd.getWorkflowTasks();
		
	}
	
	/**
	 * Returns the possible actions e.g. Launch, Next Step, Delete, etc... for the given
	 * workflow task
	 * 
	 * @param context - the context used to connect to the repository
	 * @param workflowTask - the task for which to get the actions
	 * @return the possible actions to be taken for the workflow
	 */
	static public WorkflowTaskAction[] getWorkflowTaskActions(UserSessionContext context, WorkflowTask workflowTask) {
		
		RetrieveWorkflowTaskActionsCommand cmd = null;
		
		try {
		
			cmd = new RetrieveWorkflowTaskActionsCommand(context);
		
		} catch (SessionException e) {

			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}

		cmd.setWorkflowTask(workflowTask);
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
		
		}
		
		return cmd.getWorkflowTaskActions();
		
	}
	
	/**
	 * Executes the given workflow task action e.g. Launch, Next Step, Delete, etc...
	 * 
	 * Since certain workflow task actions can have more than one possible target (e.g. Next Step
	 * when the next step is a group step contain multiple process steps and can be executed
	 * randomely) you sometimes have to specify the target by providing a WorkflowActionMenuNode.
	 * 
	 * 
	 * @param context - the context used to connect to the repository
	 * @param workflowTaskAction - the workflow task action to execute
	 * @param workflowTaskActionParameter - (optional) the workflow task action parameter
	 */
	static public void executeWorkflowTaskAction(UserSessionContext context, 
													WorkflowTaskAction workflowTaskAction,
													WorkflowActionMenuNode workflowTaskActionParameter) {
		
		ExecuteWorkflowTaskActionCommand cmd = null;
		
		try {
		
			cmd = new ExecuteWorkflowTaskActionCommand(context);
		
		} catch (SessionException e) {

			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setAction(workflowTaskAction);
		
		if(workflowTaskActionParameter != null) {
			
			cmd.setActionParameter(workflowTaskActionParameter);
			
		}
		
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
			
		}
		
		String errorMessage = cmd.getErrorMessage();
		
		if(errorMessage != null) {
			
			System.out.println(errorMessage);
			
		}
		
	}
	
	/**
	 * Returns the name of the given workflow task status
	 * 
	 * @param workflowTaskStatus - the status for which the name is to be returned
	 * @return the name of the given workflow task status
	 */
	static public String getWorkflowTaskStatusName(int workflowTaskStatus) {
		
		String statusName = null;
		
		switch(workflowTaskStatus) {
			
			case WorkflowTask.Status.ACCEPT:
				
				statusName = "Accept";
				
				break;
				
			case WorkflowTask.Status.ASSIGN:
				
				statusName = "Assign";
				
				break;
							
			case WorkflowTask.Status.ASSUME:
				
				statusName = "Assume";
	
				break;
							
			case WorkflowTask.Status.AVAILABLE:
				
				statusName = "Available";
	
				break;
						
			case WorkflowTask.Status.COMPLETED:
				
				statusName = "Completed";
	
				break;
					
			case WorkflowTask.Status.COMPLETED_CHECHIN:
				
				statusName = "Completed Checkin";
	
				break;
				
			case WorkflowTask.Status.DELETE:
				
				statusName = "Delete";
	
				break;
							
			case WorkflowTask.Status.DONE:
				
				statusName = "Done";
	
				break;
							
			case WorkflowTask.Status.INIT:
				
				statusName = "Init";
	
				break;
							
			case WorkflowTask.Status.LAUNCH:
				
				statusName = "Launch";
	
				break;
							
			case WorkflowTask.Status.MERGE:
				
				statusName = "Merge";
	
				break;
							
			case WorkflowTask.Status.PERFORM:
				
				statusName = "Perform";
	
				break;
							
			case WorkflowTask.Status.RECEIVED:
				
				statusName = "Received";
	
				break;
							
			case WorkflowTask.Status.RECEIVED_SEND_NEXT:
				
				statusName = "Received Send Next";
	
				break;
							
			case WorkflowTask.Status.REMOVE_TRASH:
				
				statusName = "Remove Trash";
	
				break;
							
			case WorkflowTask.Status.REPEAT:
				
				statusName = "Repeat";
	
				break;
							
			case WorkflowTask.Status.SEND:
				
				statusName = "Send";
	
				break;
							
			case WorkflowTask.Status.SEND_NEXT_STEP:
				
				statusName = "Send Next Step";
	
				break;
							
			case WorkflowTask.Status.SEND_NEXT_USER:
				
				statusName = "Send Next User";
	
				break;
														
			case WorkflowTask.Status.SPLIT:
				
				statusName = "Split";
	
				break;
							
			case WorkflowTask.Status.TRASH:
				
				statusName = "Trash";
	
				break;
							
			case WorkflowTask.Status.UNACCEPT:
				
				statusName = "Unaccept";
	
				break;
							
			case WorkflowTask.Status.UNDO_DELETE:
				
				statusName = "Undo Delete";
	
				break;
							
			case WorkflowTask.Status.UNLAUNCHED:
				
				statusName = "Unlaunched";
	
				break;
							
			case WorkflowTask.Status.VALIDATE:
				
				statusName = "Validate";
	
				break;
		}
		
		return statusName;
		
	}

}
