/*
 * Created on Aug 31, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.workflow;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.ids.WorkflowJobId;
import com.sap.mdm.search.Search;
import com.sap.mdm.session.UserSessionContext;
import com.sap.mdm.workflow.WorkflowActionMenuNode;
import com.sap.mdm.workflow.WorkflowJob;
import com.sap.mdm.workflow.WorkflowProperties;
import com.sap.mdm.workflow.WorkflowTask;
import com.sap.mdm.workflow.WorkflowTaskAction;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * This program creates a new workflow job, adds a record to the job and then launches it.
 * 
 * @author Richard LeBlanc
 */
class ExecuteWorkflowProgram extends WorkflowProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public void execute() {
		
		TableId productTableID = schema.getTableId(MDMAPISamples.Products.TABLE);
		
		//Get the "New Workflow" record that is linked to the Products table
		WorkflowProperties workflow = getWorkflow(context, productTableID, "New Workflow");
		
		
		//get the record id of the first record of the product table and insert it in a RecordID array
		RecordId[] recordIDs = {getFirstRecordId(context, productTableID)};
		
		
		//create a workflow job for the "New Workflow" workflow
		//and add the first record of the product table to this workflow
		WorkflowJobId workflowJobID = WorkflowManager.createWorkflowJob(context, workflow.getId(), recordIDs);
		
		
		//get all workflow tasks for the Products table
		WorkflowTask[] workflowTasks = WorkflowManager.getWorkflowTasks(context, productTableID, WorkflowTask.Type.ALL);
		
		
		//print all workflow tasks for the Products table
		//this is what you see in the workflow tab of the Data Manager
		printWorkflowTasks(workflowTasks);
		
		
		//get the workflow task linked to the previously created job
		WorkflowTask myWorkflowTask = getWorkflowTask(workflowJobID, workflowTasks);		
		
		
		//get all the possible actions for the workflow task e.g. launch, next step, delete, etc...
		WorkflowTaskAction[] workflowTaskActions = WorkflowManager.getWorkflowTaskActions(context, myWorkflowTask);
		
		
		//print the enabled actions i.e. those that can be invoked
		printEnabledWorkflowTaskActions(workflowTaskActions);
		
		
		//get the Launch workflow task action
		WorkflowTaskAction launch = getWorkflowTaskAction(workflowTaskActions, WorkflowTaskAction.LAUNCH);
				
		
		//execute the Launch action to launch the workflow
		WorkflowManager.executeWorkflowTaskAction(context, launch, null);
		
		
		//get the updated workflow tasks
		workflowTasks = WorkflowManager.getWorkflowTasks(context, productTableID, WorkflowTask.Type.ALL);
		
		
		//print the updated tasks to get an updated view
		printWorkflowTasks(workflowTasks);
		
		
		//get my workflow task
		myWorkflowTask = getWorkflowTask(workflowJobID, workflowTasks);
		
		
		//get the updated workflow task actions
		workflowTaskActions = WorkflowManager.getWorkflowTaskActions(context, myWorkflowTask);
		
		
		//print the enabled actions
		printEnabledWorkflowTaskActions(workflowTaskActions);
		
		
		//get the Next Step workflow task
		WorkflowTaskAction nextStep = getWorkflowTaskAction(workflowTaskActions, WorkflowTaskAction.SEND_NEXT_STEP);
		
		
		//It is possible for a workflow task action to have more than one
		//selection e.g. Next User so we print out the possible selections
		printWorkflowTaskMenuNodes(nextStep.getActionParameters());
	
	
		//Get the Next Step -> Stop selection
		WorkflowActionMenuNode menuNode = getWorkflowActionMenuNode(nextStep.getActionParameters(), "Stop");		
	
		
		//execute the Send Next Step workflow task action and send it to the "Stop" step
		WorkflowManager.executeWorkflowTaskAction(context, nextStep, menuNode);
		
		
		//get the updated workflow tasks
		workflowTasks = WorkflowManager.getWorkflowTasks(context, productTableID, WorkflowTask.Type.ALL);
	
	
		//print the updated tasks to get an updated view
		printWorkflowTasks(workflowTasks);
	
	
		//get my workflow task
		myWorkflowTask = getWorkflowTask(workflowJobID, workflowTasks);
	
	
		//get the updated workflow task actions
		workflowTaskActions = WorkflowManager.getWorkflowTaskActions(context, myWorkflowTask);
	
	
		//print the enabled actions
		printEnabledWorkflowTaskActions(workflowTaskActions);
	
	
		//get the Delete workflow task
		WorkflowTaskAction delete = getWorkflowTaskAction(workflowTaskActions, WorkflowTaskAction.DELETE);
		
		
		//execute the Delete workflow task
		WorkflowManager.executeWorkflowTaskAction(context, delete, null);
		
	}
	
	/*
	 * Get the workflow record with the given name that is linked to the given table id
	 */
	private WorkflowProperties getWorkflow(UserSessionContext context, TableId tableID, String name) {
		
		WorkflowProperties[] workflows = WorkflowManager.getWorkflows(context, tableID);
		
		WorkflowProperties workflow = null;
		
		for(int i=0, j=workflows.length; i<j; i++) {
			
			if(workflows[i].getName().equalsIgnoreCase(name)) {
				
				workflow = workflows[i];
				
			}
			
		}
		
		return workflow;
		
	}
	
	/*
	 * Get the workflow task associated to the given workflow job
	 */
	private WorkflowTask getWorkflowTask(WorkflowJobId jobID, WorkflowTask[] tasks) {
		
		WorkflowTask task = null;
		
		//go through all the workflow tasks for the products table
		for(int i=0, j=tasks.length; i<j; i++) {

			//grab the workflow task for the job we created previously
			if(tasks[i].getJob().getId().getIdValue() == jobID.getIdValue()) {
		
				task = tasks[i];
		
			}			

  		}
  		
  		return task;
		
	}
	
	/*
	 * Get the workflow action
	 */
	private WorkflowTaskAction getWorkflowTaskAction(WorkflowTaskAction[] actions, int type) {
		
		WorkflowTaskAction action = null;
		
		//go through all the possible actions
		for(int i=0, j=actions.length; i<j; i++) {
			
			//get the Launch action
			if(actions[i].getType() == type) {
				
				action = actions[i];
	
			}
			
		}
		
		return action;
		
	}
	
	/*
	 * Get the WorkflowActionMenuNode identified by the given name
	 */
	private WorkflowActionMenuNode getWorkflowActionMenuNode(WorkflowActionMenuNode[] menuNodes, String name) {
		
		WorkflowActionMenuNode menuNode = null;
		
		for(int i=0, j=menuNodes.length; i<j; i++) {
			
			if(menuNodes[i].getName().equalsIgnoreCase(name)) {
				
				menuNode = menuNodes[i];
					
			}
			
		}
		
		return menuNode;
		
	}

	
	/*
	 * Print all the workflow tasks
	 */
	private void printWorkflowTasks(WorkflowTask[] workflowTasks) {
		WorkflowJob job = null;
		WorkflowTask task = null;
		
		//go through all the workflow tasks for the products table
		//this is what you see in the workflow tab
		for(int i=0, j=workflowTasks.length; i<j; i++) {

			task = workflowTasks[i];
			job = task.getJob();
			 
			//print the job name and the status
			System.out.println(job.getName() + "\t" +
								task.getStepName() + "\t" +
								task.getStepDescription() + "\t" +
								WorkflowManager.getWorkflowTaskStatusName(task.getStatus()) + "\t" +
								getUserNamesAsString(task) + "\t" +
								job.getRecords().length + "\t" +
								job.getId().getIdValue() + "\t" +
								task.getArriveTime() + "\t" +
								job.getCreateTime() + "\t" +
								task.getStartTime() + "\t" +
								task.getEndTime());
		
		}
	
		System.out.println("");
	
	}
	
	/*
	 * Print enabled workflow task actions
	 */
	private void printEnabledWorkflowTaskActions(WorkflowTaskAction[] actions) {
		
		System.out.println("Enabled task actions...");
		
		for(int i=0, j=actions.length; i<j; i++) {
		
			//verify if the action is enabled
			//depending on the status of the task, only certain actions are enabled
			if(actions[i].isEnable()) {

				System.out.println(actions[i].getTypeName());
				
	  		}
	  		
		}
		
		System.out.println("");
		
	}
	

	/*
	 * Print possible selections for a workflow task action
	 */
	private void printWorkflowTaskMenuNodes(WorkflowActionMenuNode[] menuNodes) {
		
		System.out.println("Possible selections ...");
		
		for(int i=0, j=menuNodes.length; i<j; i++) {
		
			System.out.println(menuNodes[i].getName());
				
		}
		
		System.out.println("");
		
	}
	
		
	
	
	/*
	 * returns all user names as a semi-colon delimited string
	 */
	private String getUserNamesAsString(WorkflowTask task) {
		
		StringBuffer sb = new StringBuffer();
		
		String[] userNames = task.getUserNames();
		
		for(int i=0, j=userNames.length; i<j; i++) {
			
			if(i>0) {
			
				sb.append(";");
			
			}
			
			sb.append(userNames[i]);
			
			
		}
		
		return sb.toString();
		
	}
	
	/*
	 * Gets the first record of the given table
	 */
	private RecordId getFirstRecordId(UserSessionContext context, TableId tableID) {
		
		Search search = new Search(tableID);
		
		ResultDefinition resultDefinition = new ResultDefinition(tableID);
		
		Record[] records = RecordDAO.getRecords(context, search, resultDefinition, null, null, 1, 0);
		
		return records[0].getId();
		
	}

}
