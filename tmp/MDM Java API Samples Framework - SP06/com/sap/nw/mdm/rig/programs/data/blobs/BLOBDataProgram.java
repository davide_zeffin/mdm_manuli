/*
 * Created on Oct 9, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.nw.mdm.rig.programs.data.blobs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.Search;
import com.sap.nw.mdm.rig.Program;
import com.sap.nw.mdm.rig.data.dao.RecordDAO;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;

/**
 * This is an abstract class that contains helper methods for BLOB programs.
 * It also contains a set of static fields that represents each of the BLOB programs that can be executed.
 * 
 * @author Richard LeBlanc
 */
public abstract class BLOBDataProgram extends Program {
	
	/**
	 * A BLOBProgram to retrieve an image and write it to a file
	 */
	static public final BLOBDataProgram RETRIEVE_AND_WRITE_IMAGE_TO_FILE = new RetrieveImageAndWriteToFileProgram();
	
	
	/**
	 * A BLOBProgram to retrieve a PDF and write it to a file
	 */
	static public final BLOBDataProgram RETRIEVE_AND_WRITE_PDF_TO_FILE = new RetrievePDFAndWriteToFileProgram();

	/**
	 * A BLOBProgram to insert an Image
	 */
	static public final BLOBDataProgram INSERT_IMAGE = new InsertImageProgram();

	/*
	 * Gets the first record in the table represented by the given table schema
	 */
	protected Record getFirstRecord(TableSchema tableSchema) {
		
		Search search = new Search(tableSchema.getTable().getId());
	
		ResultDefinition resultDefinition = new ResultDefinition(tableSchema.getTable().getId());
	
		//MDMAPISamples.Images.ORIGINAL_NAME points to the string "Original_Name" so it will
		//work for all BLOB tables e.g. Images, PDFs, Videos, etc...
		resultDefinition.addSelectField(tableSchema.getFieldId(MDMAPISamples.Images.ORIGINAL_NAME));
	
		Record[] records = RecordDAO.getRecords(context, search, resultDefinition, null, null, 1, 0);
	
		return records[0];
	
	}
	
	/*
	 * Writes the given byte array to a file with the given name
	 */
	protected void writeToFile(byte[] bytes, String fileName) {
		
		File f = new File("c:\\" + fileName);
	
		try {
	
			f.createNewFile();
		
			FileOutputStream fos = new FileOutputStream(f);
	
			fos.write(bytes);
	
		} catch (IOException e) {
	
			e.printStackTrace();
	
		}		
	
	}


}
