/*
 * Created on Jul 12, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.syndication;

import com.sap.nw.mdm.rig.Program;

/**
 * This is an identifying interface for all Syndication programs.
 * It contains a set of static variables that point to the various programs
 * that can be executed.
 * 
 * @author Richard LeBlanc
 */
abstract public class SyndicationProgram extends Program {

	/**
	 * Syndicates records to a port.  The records to be syndicated are determined in the
	 * Syndication Map defined in the port.
	 */
	public static final SyndicationProgram SYNDICATE_PORT = new SyndicatePortProgram();

}
