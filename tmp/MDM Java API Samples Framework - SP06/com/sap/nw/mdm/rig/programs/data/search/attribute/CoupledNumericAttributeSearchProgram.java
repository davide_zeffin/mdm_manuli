/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.attribute;

import com.sap.mdm.ids.AttributeId;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.AttributeSearchDimension;
import com.sap.mdm.search.PickListSearchConstraint;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;


/**
 * Performs a Coupled Numeric attribute search.
 * 
 * @author Richard LeBlanc
 */
class CoupledNumericAttributeSearchProgram extends AttributeSearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearch(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {

		FieldId categoryFieldID = tableSchema.getField(MDMAPISamples.Products.CATEGORY).getId();

		//get the first instance of a couple numeric attribute
		AttributeId coupledNumericAttributeID = getAttribute(tableSchema, AttributeProperties.COUPLED_TYPE).getId();
		
		//get the first value for the coupled numeric attribute
		MdmValue value = getValue(tableSchema.getTable().getId(),
												categoryFieldID, coupledNumericAttributeID);
		
		MdmValue[] searchValues = {value};
		
		//create search criteria for the coupled numeric attribute
		//remember that for attributes, you must specify an existing value since it
		//acts as a lookup/picklist search
		SearchDimension searchDimension = new AttributeSearchDimension(categoryFieldID, coupledNumericAttributeID);

		SearchConstraint searchConstraint = new PickListSearchConstraint(searchValues);

		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);
		
		
		SearchParameter[] searchParameters = {searchParameter};

		return searchParameters;
		
	}

}
