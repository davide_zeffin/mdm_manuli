/*
 * Created on Jun 11, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.search.attribute;

import com.sap.mdm.ids.AttributeId;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.search.AttributeSearchDimension;
import com.sap.mdm.search.PickListSearchConstraint;
import com.sap.mdm.search.SearchConstraint;
import com.sap.mdm.search.SearchDimension;
import com.sap.mdm.search.SearchParameter;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;


/**
 * Performs a Text attribute search.
 * 
 * @author Richard LeBlanc
 */
class TextAttributeSearchProgram extends AttributeSearchProgram {

	/* (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.programs.data.search.SearchProgram#getSearch(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public SearchParameter[] getSearchParameters(TableSchema tableSchema) {
		
		FieldId categoryFieldID = tableSchema.getFieldId(MDMAPISamples.Products.CATEGORY);
		
		//get the first instance of a text attribute
		AttributeId textAttributeID = getAttribute(tableSchema, AttributeProperties.TEXT_TYPE).getId();
		
		//get the first value for the text attribute
		MdmValue value = getValue(tableSchema.getTable().getId(),
									categoryFieldID, textAttributeID);
									
		MdmValue[] searchValues = {value};
		
		//create search criteria for the text attribute
		//remember that for attributes, you must specify an existing value since it
		//acts as a lookup/picklist search		
		SearchDimension searchDimension = new AttributeSearchDimension(categoryFieldID, textAttributeID);

		SearchConstraint searchConstraint = new PickListSearchConstraint(searchValues);		

		SearchParameter searchParameter = new SearchParameter(searchDimension, searchConstraint);
		
		
		SearchParameter[] searchParameters = {searchParameter};
		
		return searchParameters;
		
	}

}
