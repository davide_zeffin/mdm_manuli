/*
 * Created on Apr 26, 2007
 *
 */
package com.sap.nw.mdm.rig.programs.data.checkout_checkin_rollback;

import com.sap.mdm.data.Record;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.schema.TableSchema;
import com.sap.nw.mdm.rig.data.util.MDMAPISamples;
import com.sap.nw.mdm.rig.data.util.RecordPrinter;

/**
 * This program checks out the first 10 records of the product table,
 * modifies them and checks then back in.
 * 
 * @author Richard LeBlanc
 */
class CheckOutExistingRecordsAndCheckInProgram extends CheckoutExistingRecordsProgram {
	
	/*
	 *  (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.Program#execute(com.sap.nw.mdm.rig.repository.Repository)
	 */
	public void execute() {
		
		//get the metadata
		TableSchema productsTableSchema = schema.getTableSchema(MDMAPISamples.Products.TABLE);

		
		//prepare an empty array to put record ids
		RecordId[] productRecordIDs = new RecordId[10];
		
		
		//retrieves the first 10 records from the products table
		Record[] productRecords = getFirstXRecords(productsTableSchema, 10);
		
		
		//print the records before they're checked out, modified and checked in
		System.out.println("Records before modifications");
		RecordPrinter.print(context, productRecords, loginRegion);
		System.out.println("");
		
		
		//go through each record and put the record id in the array prepared earlier
		//the record ids will be used to check out the records
		for(int i=0, j=productRecords.length; i<j; i++) {
			
			productRecordIDs[i] = productRecords[i].getId();
			
		}
		
		
		//A record checkout causes MDS to create a new version of the record with a new
		//record id.  It is this record id that is returned and must be used for the checkin.
		RecordId[] checkedOutProductRecordIDs = CheckOutCheckInManager.checkOutRecords(context, productsTableSchema.getTable().getId(), productRecordIDs);
		
		
		//get the new version of the record(s) using the checkout record id(s)
		productRecords = getRecordsByID(productsTableSchema, checkedOutProductRecordIDs);
		
		
		//update the records
		modifyRecords(productRecords, productsTableSchema);		
		
		
		//checkin the records
		CheckOutCheckInManager.checkInRecords(context, productsTableSchema.getTable().getId(), checkedOutProductRecordIDs);

		
		//get the updated checked in records using the original record ids.
		//the checkout record ids are temporary
		Record[] updatedProductRecords = getRecordsByID(productsTableSchema, productRecordIDs);
		
		
		//print the records after they've been modified and checked in
		System.out.println("Records after modifications");
		RecordPrinter.print(context, updatedProductRecords, loginRegion);
		
	}
	
}