/*
 * Created on Jun 6, 2007
 *
 */
package com.sap.nw.mdm.rig;

import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.extension.schema.AttributeSchema;
import com.sap.mdm.extension.schema.RepositorySchemaEx;
import com.sap.mdm.session.UserSessionContext;

/**
 * Represents a sample program to be executed.
 * Classes that extend this one make various calls to the MDM Java API
 * to invoke all kinds of functionalities e.g. creating records, modifying attributes,
 * record checkout/checkin, search, etc... 
 * 
 * @author Richard LeBlanc
 */
abstract public class Program {
	
	/**
	 * Executes the program
	 */
	abstract public void execute();
	

	protected RegionProperties loginRegion;
	
	/**
	 * Sets the region that is used to determine the language in which
	 * to retrieve field & attribute names and other multilingual information
	 * 
	 * @param region - the login region
	 */	
	public void setLoginRegion(RegionProperties region) {
		
		loginRegion = region;
		
	}
	
	protected UserSessionContext context;
	
	/**
	 * Sets the context to use when executing a program which in turn
	 * executes the necessary commands.
	 * 
	 * @param context
	 */
	public void setContext(UserSessionContext context) {
		
		this.context = context;
		
	}
	
	protected RepositorySchemaEx schema;
	
	/**
	 * Sets the repository schema
	 * 
	 * @param schema
	 */
	public void setRepositorySchema(RepositorySchemaEx schema) {
		
		this.schema = schema;
		
	}
	
	protected AttributeSchema attributeSchema;
	
	/**
	 * Sets the attribute schema
	 * 
	 * @param schema
	 */
	public void setAttributeSchema(AttributeSchema schema) {
		
		attributeSchema = schema;
		
	}
	
 
}
