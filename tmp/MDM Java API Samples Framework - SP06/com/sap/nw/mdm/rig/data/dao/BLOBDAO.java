/*
 * Created on Aug 21, 2007
 */
package com.sap.nw.mdm.rig.data.dao;

import com.sap.mdm.blobs.commands.RetrieveBlobCommand;
import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;
import com.sap.mdm.valuetypes.BinaryValue;
import com.sap.mdm.valuetypes.MultiregionValue;

/**
 * Provides access to BLOBs from all object tables
 *   
 * @author Richard LeBlanc
 */
public class BLOBDAO {

	/**
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table id of blob table
	 * @param recordID - the record id in the blob table
	 * @param region - the region for which to return the blob
	 */
	static public byte[] getBlob(UserSessionContext context, TableId tableID, RecordId recordID, RegionProperties region) {
		
		RetrieveBlobCommand cmd = null;
		
		try {
		
			cmd = new RetrieveBlobCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}

		cmd.setTableId(tableID);
		
		cmd.setRecordId(recordID);
		
		try {
			
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
		
		}

		MultiregionValue multiRegionValue = (MultiregionValue)cmd.getBlob();
		
		BinaryValue binaryValue = (BinaryValue)multiRegionValue.getValue(region.getRegionCode());
		
		return binaryValue.getBytes();
		
	}

}
