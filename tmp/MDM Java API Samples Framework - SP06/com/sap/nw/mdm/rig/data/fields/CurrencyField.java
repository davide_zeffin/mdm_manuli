/*
 * Created on Jun 22, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import com.sap.mdm.data.MultilingualString;
import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.fields.CurrencyFieldProperties;
import com.sap.mdm.valuetypes.DoubleValue;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * Represents an MDM currency field.
 * 
 * @author Richard LeBlanc
 */
class CurrencyField extends Field {

	/**
	 * Return the currency symbol
	 */
	String getPrefix(FieldProperties field, String regionCode) {
		
		CurrencyFieldProperties currencyField = (CurrencyFieldProperties)field;
	
		MultilingualString multilingualString = currencyField.getSymbol();

		String symbol = null;

		if(multilingualString != null) {
	
			return multilingualString.get(regionCode);
	
		} else {
	
			return "";
		}

		
	}

	/**
	 * Returns the numerical value as a string
	 */
	String getValue(FieldProperties field, MdmValue value) {
	
		if(field.getType() == FieldProperties.CURRENCY_FIELD && 
			value.getType() == MdmValue.Type.DOUBLE) {
		
			
			DoubleValue doubleValue = (DoubleValue)value;
				
			return String.valueOf(doubleValue.getDouble());

		} else {
	
			throw new IllegalArgumentException();	
	
		}
	
	}

}
