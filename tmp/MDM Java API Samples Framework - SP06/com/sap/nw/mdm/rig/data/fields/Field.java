/*
 * Created on Jun 19, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import java.util.HashMap;
import java.util.Map;

import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * This abstract class represents a generic Field and provides a helper method to 
 * help retrieve field values. Depending on the type of field, 
 * it redirects the calls to one of the appropriate subclasses located in this
 * same package.
 * 
 * @author Richard LeBlanc
 */
abstract public class Field {

	/*
	 * Used to get the value of a boolean field
	 */
	private static final Field BOOLEAN_FIELD = new BooleanField();
	
	/*
	 * Used to get the value of a currency field
	 */
	private static final Field CURRENCY_FIELD = new CurrencyField();
	
	/*
	 * Used to get the value of a text field
	 */
	private static final Field FIXED_WIDTH_TEXT_FIELD = new FixedWidthTextField();
	
	/*
	 * Used to get the value of an integer field
	 */
	private static final Field INTEGER_FIELD = new IntegerField();
	
	/*
	 * Used to get the value of a large text field
	 */
	private static final Field LARGE_TEXT_FIELD = new LargeTextField();
	
	/*
	 * Used to get the value of a Date Literal field
	 */
	private static final Field LITERAL_DATE_FIELD = new LiteralDateField();
	
	/*
	 * Used to get the value of a Timestamp field
	 */
	private static final Field TIMESTAMP_FIELD = new TimeStampField();
	
	
	/*
	 * Used to get the value of a Userstamp field
	 */
	private static final Field USERSTAMP_FIELD = new UserStampField();
	
	//contains a mapping of the MDM field types to one of Field subclasses
	static private Map map = new HashMap();
	
	static {
		
		map.put(new Integer(FieldProperties.BOOLEAN_FIELD), BOOLEAN_FIELD);
		
		map.put(new Integer(FieldProperties.CURRENCY_FIELD), CURRENCY_FIELD);
		
		map.put(new Integer(FieldProperties.FIXED_WIDTH_TEXT_FIELD), FIXED_WIDTH_TEXT_FIELD);
		
		map.put(new Integer(FieldProperties.INTEGER_FIELD), INTEGER_FIELD);
		
		map.put(new Integer(FieldProperties.LARGE_TEXT_FIELD), LARGE_TEXT_FIELD);
		
		map.put(new Integer(FieldProperties.DATE_FIELD), LITERAL_DATE_FIELD);
				
		map.put(new Integer(FieldProperties.TIMESTAMP_FIELD), TIMESTAMP_FIELD);
		
		map.put(new Integer(FieldProperties.USERSTAMP_FIELD), USERSTAMP_FIELD);	
		
		
	}

	/**
	 * Returns the value of the field as a String.
	 * 
	 * @param field - the field whose value to retrieve
	 * @param value - the MdmValue instance
	 * @param regionCode - used to retrieve data in the appropriate language
	 */
	static public String valueOf(FieldProperties field, MdmValue value, String regionCode) {
		
		if(value == null || value.isNull()) {
			
			return "";
			
		}
		
		Field myField = (Field)map.get(new Integer(field.getType()));
		
		if(myField == null) {
			
			return "unsupported field type";
			
		} else {
		
			String result = myField.getPrefix(field, regionCode) + 
							myField.getValue(field, value) + 
							myField.getRegionalValue(field, value, regionCode) + 
							myField.getSuffix(field, regionCode);
		
			return result;
		
		}
				
	}
	
	String getPrefix(FieldProperties field, String regionCode){
		
		return "";
		
	}
	
	String getValue(FieldProperties field, MdmValue value) {
		
		return "";
		
	}
	
	String getRegionalValue(FieldProperties field, MdmValue value, String regionCode) {
		
		return "";
		
	}
	
	String getSuffix(FieldProperties field, String regionCode) {
		
		return "";
		
	}
}