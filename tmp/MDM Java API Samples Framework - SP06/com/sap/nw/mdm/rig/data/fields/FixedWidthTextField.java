/*
 * Created on Jun 22, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.fields.FixedWidthTextFieldProperties;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.mdm.valuetypes.MultiregionValue;
import com.sap.mdm.valuetypes.StringValue;

/**
 * Represents an MDM fixed width text field
 * 
 * @author Richard LeBlanc
 */
class FixedWidthTextField extends Field {

	/*
	 *  (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.data.fields.Field#getValue(com.sap.nw.mdm.rig.repository.Repository, com.sap.mdm.schema.FieldProperties, com.sap.mdm.valuetypes.MdmValue)
	 */
	String getRegionalValue(FieldProperties field, MdmValue value, String regionCode) {
		
		if(field instanceof FixedWidthTextFieldProperties && 
			(value instanceof StringValue || value instanceof MultiregionValue)) {
		
			FixedWidthTextFieldProperties fixedWidthTextField = (FixedWidthTextFieldProperties)field;
	
			String result = null;
			
			StringValue stringValue = null;
			
			if(value.isMultiregion()) {
		
				MultiregionValue multiRegionValue = (MultiregionValue)value;
				
				return ((StringValue)multiRegionValue.getValue(regionCode)).getString();
				
			} else {
				
				return ((StringValue)value).getString();
				
			}
			
		} else {
	
			throw new IllegalArgumentException();	
	
		}

	}

}
