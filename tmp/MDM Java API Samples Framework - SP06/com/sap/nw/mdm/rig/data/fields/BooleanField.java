/*
 * Created on Jun 21, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.fields.BooleanFieldProperties;
import com.sap.mdm.valuetypes.BooleanValue;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * Represents an MDM boolean field.
 * 
 * @author Richard LeBlanc
 */
class BooleanField extends Field{

	/**
	 * Returns the true of false value as defined in the MDM Console for the approprite login region.
	 * 
	 * @see com.sap.nw.mdm.rig.data.fields.Field#getValue(com.sap.nw.mdm.rig.repository.Repository, com.sap.mdm.schema.FieldProperties, com.sap.mdm.valuetypes.MdmValue)
	 */
	String getRegionalValue(FieldProperties field, MdmValue value, String regionCode) {
		
		if(field.getType() == FieldProperties.BOOLEAN_FIELD && 
			value.getType() == MdmValue.Type.BOOLEAN) {
		
			BooleanFieldProperties booleanField = (BooleanFieldProperties)field;
			
			BooleanValue booleanValue = (BooleanValue)value;	
		
			if(booleanValue.getBoolean()) {
			
				return booleanField.getTrueValue().get(regionCode);
					
			} else {
		
				return booleanField.getFalseValue().get(regionCode);
		
			}
			
		} else {
			
			throw new IllegalArgumentException();	
			
		}
		
	}

}
