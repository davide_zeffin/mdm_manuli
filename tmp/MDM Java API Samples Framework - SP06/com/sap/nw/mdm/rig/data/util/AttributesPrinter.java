/*
 * Created on Jan 11, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.nw.mdm.rig.data.util;

import java.util.Iterator;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.extension.schema.AttributeSchema;
import com.sap.mdm.extension.schema.LinkedAttribute;
import com.sap.mdm.ids.DimensionId;
import com.sap.mdm.ids.UnitId;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.CoupledAttributeProperties;
import com.sap.mdm.schema.Dimensions;
import com.sap.mdm.schema.NumericAttributeProperties;
import com.sap.mdm.schema.TextAttributeProperties;
import com.sap.mdm.schema.TextAttributeValueProperties;
import com.sap.mdm.schema.UnitProperties;
import com.sap.mdm.session.UserSessionContext;

/**
 * Prints the linked attributes of a Taxonomy record
 * 
 * @author Richard LeBlanc
 */
class AttributesPrinter {
	
	/**
	 * Prints the provided attributes as well as all the possible values for text attributes
	 * 
	 * @param context - the context used to connect to the repository  
	 * @param record - the record that contains the attributes
	 * @param region - the login region
	 */
	static public void print(UserSessionContext context, Record record, RegionProperties region) {
	
		AttributeSchema attributeSchema = MetadataManager.getInstance().getAttributeSchema(context);
	
		LinkedAttribute[] linkedAttributes = attributeSchema.getLinkedAttributes(record.getTable(), record.getId());
		
		LinkedAttribute linkedAttribute = null;
		TextAttributeProperties textAttribute = null;
		NumericAttributeProperties numericAttribute = null;
		CoupledAttributeProperties coupledNumericAttribute = null;

		TextAttributeValueProperties[] textAttributeValues = null;

		Dimensions dimensions = new Dimensions();
		DimensionId dimensionID = null;
		Iterator unitOfMeasureIDs = null;
		UnitProperties unitOfMeasure = null;

		for(int i=0, j=linkedAttributes.length; i<j; i++) {

			linkedAttribute = linkedAttributes[i];
		
			switch(linkedAttribute.getAttributeProperties().getType()) {
		

				//If the attribute is a text attribute,
				//the possible attribute values must also be displayed.
				case AttributeProperties.TEXT_TYPE: 

					textAttribute = (TextAttributeProperties)linkedAttribute.getAttributeProperties();
	
					//the attribute name is multilingual so we must provide a region code
					System.out.println("  " + textAttribute.getName().get(record.getDefaultRegionCode()));
	
					textAttributeValues = textAttribute.getTextAttributeValues();
	
					for(int k=0, l=textAttributeValues.length; k<l; k++) {
		
						System.out.println("\t" + textAttributeValues[k].getName().get(record.getDefaultRegionCode()));
		
					}
				
					break;
	
				//If the attribute is numeric or coupled numeric and has an 
				//assigned dimension, you must present all the possible units of measure
				case AttributeProperties.NUMERIC_TYPE:
	
					numericAttribute = (NumericAttributeProperties)linkedAttribute.getAttributeProperties();
	
					System.out.println("  " + numericAttribute.getName().get(record.getDefaultRegionCode()));
	
					dimensionID = numericAttribute.getMeasurement().getDimensionId();
	
					unitOfMeasureIDs = dimensions.getUnitIds(dimensionID);
	
					System.out.println("\t" + "Available Units of Measure");
	
					while(unitOfMeasureIDs.hasNext()) {
		
						unitOfMeasure = dimensions.getUnit(dimensionID, ((UnitId)unitOfMeasureIDs.next()));	

						System.out.println("\t" + unitOfMeasure.getName(region.getLocale()));
		
					}
				
					break;


				case AttributeProperties.COUPLED_TYPE:

					coupledNumericAttribute = (CoupledAttributeProperties)linkedAttribute.getAttributeProperties();
	
					System.out.println("  " + coupledNumericAttribute.getName().get(record.getDefaultRegionCode()) +
										" " + coupledNumericAttribute.getCoupledDelimiter() + " " +			
										coupledNumericAttribute.getCoupledName().get(record.getDefaultRegionCode()));
	
					dimensionID = coupledNumericAttribute.getMeasurement().getDimensionId();

					unitOfMeasureIDs = dimensions.getUnitIds(dimensionID);

					System.out.println("\t" + "Available Units of Measure for primary value");
		
					while(unitOfMeasureIDs.hasNext()) {

						unitOfMeasure = dimensions.getUnit(dimensionID, ((UnitId)unitOfMeasureIDs.next()));	

						System.out.println("\t  " + unitOfMeasure.getName(region.getLocale()));

					}

					System.out.println("");
	
					dimensionID = coupledNumericAttribute.getCoupledMeasurement().getDimensionId();

					unitOfMeasureIDs = dimensions.getUnitIds(dimensionID);

					System.out.println("\t" + "Available Units of Measure for secondary value");

					while(unitOfMeasureIDs.hasNext()) {

						unitOfMeasure = dimensions.getUnit(dimensionID, ((UnitId)unitOfMeasureIDs.next()));	

						System.out.println("\t  " + unitOfMeasure.getName(region.getLocale()));

					}
				
					break;
	
			}			

		}	
	
	}

}
