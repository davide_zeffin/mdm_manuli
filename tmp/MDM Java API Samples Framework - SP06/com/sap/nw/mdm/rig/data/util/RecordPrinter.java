/*
 * Created on Jun 19, 2007
 *
 */
package com.sap.nw.mdm.rig.data.util;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.extension.schema.AttributeSchema;
import com.sap.mdm.ids.AttributeId;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.schema.fields.LookupFieldProperties;
import com.sap.mdm.session.UserSessionContext;
import com.sap.mdm.valuetypes.QualifiedLinkValue;
import com.sap.mdm.valuetypes.QualifiedLookupValue;
import com.sap.nw.mdm.rig.data.attributes.Attribute;
import com.sap.nw.mdm.rig.data.fields.Field;

/**
 * Prints records to the console
 * 
 * @author Richard LeBlanc
 */
public class RecordPrinter {
	
	/** Prints a record
	 * 
	 * @param context - the context used to connect to the repository
	 * @param record - the record to be printed
	 * @param region - provides the language for multilingual fields
	 */
	static public void print(UserSessionContext context, Record record, RegionProperties region) {
		
		TableSchema tableSchema = record.getMetadata();
		
		FieldId[] fieldIDs = record.getFields();

		FieldId displayFieldID = null;

		FieldProperties field = null;

		AttributeId[] attributeIDs = null;
		
		
		Record[] lookupRecords = null;

		TableId lookupTableID = null;
		
		TableSchema lookupTableSchema = null;	
		
		FieldProperties lookupField = null;	
		
		
		for(int i=0, j=fieldIDs.length; i<j; i++) {
	
			field = tableSchema.getField(fieldIDs[i]);

			System.out.print(field.getName().get(record.getDefaultRegionCode()));
	
	
			//the value of a lookup field is the record id of the record in the lookup table
			//that it points to.  Therefore, i look for lookup records in order to display something.
			//Lookup records will only be retrieved if the setSupportingResultDefinitions(ResultDefinition[])
			//method was called before performing executing one of the Retrieve***Commands.
			if(field.isLookup() || field.isLargeObjectLookup()) {
				
				lookupRecords = record.findLookupRecords(fieldIDs[i]);
		
				if(lookupRecords != null) {

					System.out.print("\t");

					System.out.println(record.getLookupDisplayValue(fieldIDs[i]));

				} else {

					System.out.print("You must provide a supporting ResultDefinition for the lookup table");

					System.out.print(System.getProperty("line.separator"));

				}

				
				switch(field.getType()) {
					
					case FieldProperties.QUALIFIED_FLAT_LOOKUP_FIELD:
						
						System.out.print(System.getProperty("line.separator"));
						
						lookupTableSchema = MetadataManager.getInstance().getRepositorySchema(context)
															.getTableSchema(((LookupFieldProperties)field).getLookupTableId());
						
									
						QualifiedLookupValue qlv = (QualifiedLookupValue)record.getFieldValue(fieldIDs[i]);
					
						QualifiedLinkValue[] links = qlv.getQualifiedLinks();
	
						FieldId[] qualifiers = null;
	
	
						for(int k=0, l=links.length; k<l; k++) {
							
							qualifiers = links[k].getQualifierFields();
	
							for(int m=0, n=qualifiers.length; m<n; m++) {
		
								lookupField = lookupTableSchema.getField(qualifiers[m]);
				
								System.out.print("  " + lookupField.getName().get(record.getDefaultRegionCode()));
		
								System.out.print("\t");
		
								System.out.println(Field.valueOf(lookupField, links[k].getQualifierValue(qualifiers[m]), record.getDefaultRegionCode()));
		
							}
		
						}
						
						break;
						
					case FieldProperties.TAXONOMY_LOOKUP_FIELD:
						
						attributeIDs = record.getAttributes(fieldIDs[i]);
						
						if(attributeIDs.length > 0) {
						
							AttributeSchema attSchema = MetadataManager.getInstance().getAttributeSchema(context);
					
							lookupTableID = ((LookupFieldProperties)field).getLookupTableId();
	
							AttributeProperties attribute = null;
		
							for(int k=0, l=attributeIDs.length; k<l; k++) {
		
								attribute = attSchema.getAttribute(lookupTableID, attributeIDs[k]);
		
								System.out.print("  " + attribute.getName().get(record.getDefaultRegionCode()));
		
								System.out.print("\t");	
		
								System.out.print(Attribute.valueOf(region, attribute, 
													record.getAttributeValue(fieldIDs[i], attributeIDs[k])));
		
								System.out.println("");
		
							}				
						
						}
											
						break;
						
				}
						
			} else {
				
				System.out.print("\t");
			
				System.out.println(Field.valueOf(field, record.getFieldValue(fieldIDs[i]), record.getDefaultRegionCode()));
			
				
			}

		}
		
	}

	/**
	 * Prints records
	 * 
	 * @param context - the context used to connect to the repository
	 * @param records - the records to be printed
	 * @param region - provides the language for multilingual fields
	 */
	static public void print(UserSessionContext context, Record[] records, RegionProperties region) {
	
		for(int i=0, j=records.length; i<j; i++) {
		
			print(context, records[i], region);
		
			System.out.println("------------------------------------------------------------------");
		
		}
	
	}

}
