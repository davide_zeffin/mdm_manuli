/*
 * Created on Jun 22, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import java.text.DateFormat;
import java.util.GregorianCalendar;

import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.fields.LiteralDateFieldProperties;
import com.sap.mdm.valuetypes.DateTimeValue;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * Represents an MDM Date Literal field
 * 
 * @author Richard LeBlanc
 */
class LiteralDateField extends Field {

	String getValue(FieldProperties field, MdmValue value) {
	
		if(field.getType() == FieldProperties.DATE_FIELD && 
			value.getType() == MdmValue.Type.DATE_TIME) {
		
			LiteralDateFieldProperties literalDateField = (LiteralDateFieldProperties)field;
	
			DateTimeValue dateTimeValue = (DateTimeValue)value;
		
			GregorianCalendar gc = new GregorianCalendar(dateTimeValue.getYear(),
															dateTimeValue.getMonth(),
															dateTimeValue.getDay());
			
			DateFormat dateFormat = DateFormat.getDateInstance();
			
			return dateFormat.format(gc.getTime());

		} else {
	
			throw new IllegalArgumentException();	
	
		}
	
	}

}
