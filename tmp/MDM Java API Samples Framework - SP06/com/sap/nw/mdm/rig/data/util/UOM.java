/*
 * Created on Apr 30, 2007
 *
 */
package com.sap.nw.mdm.rig.data.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import com.sap.mdm.ids.DimensionId;
import com.sap.mdm.ids.UnitId;
import com.sap.mdm.schema.DimensionProperties;
import com.sap.mdm.schema.Dimensions;
import com.sap.mdm.schema.UnitProperties;

/**
 * A utility class that provides access to the dimensions and units of measure
 * that exist within MDM.
 * 
 * @author Richard LeBlanc
 */
public class UOM {

	static private final Locale LOCALE = Locale.US;
	
	static private Dimensions dimensions = new Dimensions();
	
	static private Map nameDimensionMap = new HashMap();
	
	static private Map idDimensionUnitMap = new HashMap();
	
	static private Map nameDimensionUnitMap = new HashMap();
	
	//this static block populates several maps so that dimensions and units of measures
	//can be retrieved using DimensionId and UnitId or the dimension and unit of measure names
	static {
		
		Map uCodeMap = null;
		
		Map uIdMap = null;


		DimensionProperties dimension = null;

		Iterator dimensionIterator = dimensions.getDimensionsIds();

		UnitProperties unit = null;

		Iterator unitIterator = null;

		while(dimensionIterator.hasNext()) {

			dimension = dimensions.getDimension((DimensionId)dimensionIterator.next());
			
			nameDimensionMap.put(dimension.getName(LOCALE), dimension);
			

			unitIterator = dimensions.getUnitIds(dimension.getId());

			if(unitIterator.hasNext()) {

				uIdMap = new HashMap();
				
				uCodeMap = new HashMap();

			}

			while(unitIterator.hasNext()) {
	
				unit = dimensions.getUnit(dimension.getId(), (UnitId)unitIterator.next());
	
				uIdMap.put(unit.getId(), unit);
				
				uCodeMap.put(unit.getName(LOCALE), unit);
				
			}

			idDimensionUnitMap.put(dimension.getId(), uIdMap);
			
			nameDimensionUnitMap.put(dimension.getName(LOCALE), uCodeMap);
		
		}

		
	}
	
	
	/**
	 * Returns the DimensionProperties for the given DimensionId
	 * 
	 * @param dimensionID - the dimension identifier
	 */
	static public DimensionProperties getDimension(DimensionId dimensionID) {
		
		return (DimensionProperties)dimensions.getDimension(dimensionID);
		
	}
	
	
	/**
	 * Returns the DimensionProperties for the given dimension name
	 * 
	 * @param dimensionName - the name of the dimension
	 */	
	static public DimensionProperties getDimension(String dimensionName) {
		
		return (DimensionProperties)nameDimensionMap.get(dimensionName);
		
	}
	
	
	/**
	 * Returns the UnitProperties for the given dimension and unit of measure identifiers.
	 * 
	 * @param dimensionID - dimension identifier
	 * @param unitID - unit of measure identifier
	 */
	static public UnitProperties getUnit(DimensionId dimensionID, UnitId unitID) {
	
		Map m = (Map)idDimensionUnitMap.get(dimensionID);
	
		UnitProperties unit = (UnitProperties)m.get(unitID);
		
		return unit;
	
	}
	
	
	/**
	 * Returns the UnitProperties for the given dimension and unit of measure names
	 * 
	 * @param dimensionName - the dimension name
	 * @param unitName - the unit of measure name 
	 */
	static public UnitProperties getUnit(String dimensionName, String unitName) {
	
		Map m = (Map)nameDimensionUnitMap.get(dimensionName);
	
		UnitProperties unit = (UnitProperties)m.get(unitName);
		
		return unit;
	
	}
		
}
