/*
 * Created on Jun 21, 2007
 *
 */
package com.sap.nw.mdm.rig.data.attributes;

import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.NumericAttributeProperties;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.mdm.valuetypes.MeasurementValue;
import com.sap.mdm.valuetypes.MultiValue;
import com.sap.nw.mdm.rig.data.util.UOM;

/**
 * This class represents a Numeric Attribute
 * 
 * @author Richard LeBlanc
 */
class NumericAttribute extends Attribute {

	/*
	 * Returns the value of the numeric attribute which might be multi-value and also include
	 * a unit of measure.
	 * 
	 * @see com.sap.nw.mdm.rig.data.attributes.Attribute#getValue(com.sap.nw.mdm.rig.repository.Repository, com.sap.mdm.schema.AttributeProperties, com.sap.mdm.valuetypes.MdmValue)
	 */
	String getValue(RegionProperties region, AttributeProperties attribute, MdmValue value) {

		if(attribute.getType() == AttributeProperties.NUMERIC_TYPE) {
		
			if(value.isMultivalue()) {
			
				StringBuffer stringBuffer = new StringBuffer();
				
				MdmValue[] values = ((MultiValue)value).getValues();
				
				for(int i=0, j=values.length; i<j; i++) {
					
					if(values[i].getType() == MdmValue.Type.MEASUREMENT) {
				
						if(i>0) {
							
							stringBuffer.append(";");
							
						}
						
						stringBuffer.append(getValue(region, (NumericAttributeProperties)attribute, (MeasurementValue)values[i]));
				
					}
					 
				}
				
				return stringBuffer.toString();
			
			} else {
				
				if(value.getType() == MdmValue.Type.MEASUREMENT) {
					
					return getValue(region, (NumericAttributeProperties)attribute, (MeasurementValue)value);
					
				} else {
					
					throw new IllegalArgumentException();
					
				}
				
			}			
		
		
		} else {
		
			throw new IllegalArgumentException();
			
		}
		
	}
	
	private String getValue(RegionProperties region, NumericAttributeProperties attribute, MeasurementValue value) {
		
		return value.getMagnitude() + " " + 
							UOM.getUnit(attribute.getMeasurement().getDimensionId(), 
										value.getUnitId()).getName(region.getLocale());
		
	}

}
