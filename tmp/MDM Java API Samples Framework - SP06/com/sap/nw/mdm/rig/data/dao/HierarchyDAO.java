/*
 * Created on Jun 1, 2007
 *
 */
package com.sap.nw.mdm.rig.data.dao;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.HierNode;
import com.sap.mdm.data.Record;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.data.commands.CreateRecordCommand;
import com.sap.mdm.data.commands.RetrieveLimitedHierTreeCommand;
import com.sap.mdm.extension.MetadataManager;
import com.sap.mdm.extension.schema.RepositorySchemaEx;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.schema.TableProperties;
import com.sap.mdm.search.Search;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;

/**
 * Provides access to records in much the same way as hierarchy mode in the Data Manager. 
 * This means that it provides functionality to maintain a hierarchy of records and retrieve records in a hierarchical structure.
 * Access is limited to records in Hierarchy and Taxonomy tables.
 * <P>
 * Please note that this class doesn't provide access to the attributes of a Taxonomy table.
 * For this please see {@link com.sap.nw.mdm.rig.data.dao.TaxonomyDAO TaxonomyDAO}.
 * 
 * @author Richard LeBlanc
 */
public class HierarchyDAO {
	
	private HierarchyDAO() {
		
	}
	
	/**
	 * Creates a record in a hierarchy table.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param record - the record to create
	 * @param parentRecordID - the record's parent record id.  
	 * If it is null, the record will be created as a child of the root node of the hierarchy table 
	 * @param position - defines in which position the record being created should be inserted
	 * (only applies if the parent record has many children) 
	 * 
	 * @return the record id of the new record
	 */
	static public RecordId createHierarchyNode(UserSessionContext context, 
												Record record, RecordId parentRecordID, int position) {
		
		CreateRecordCommand cmd = null;
		
		try {
		
			cmd = new CreateRecordCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setRecord(record);
		
		if(parentRecordID != null) {
			
			cmd.setParentRecordId(parentRecordID);
			
		} 
		
		cmd.setPosition(position);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
	
		return cmd.getRecord().getId();
		
	}
	
	/**
	 * Returns a hierarchy of records that meet the search criteria.
	 * <P>
	 * @param context - the context used to connect to the repository
	 * @param search - the search table and criteria e.g. product name contains "screwdriver"
	 * @param rootNodeRecordID - the node in the hierarchy where the retrieval begins
	 * @param resultTablePath - the table from which to retrieve the data (optional, will use the table from
	 * the search object if null is passed)  
	 * @param resultDefinition - the fields to retrieve (ignores the resultdefinition table when used in 
	 * conjunction with search)
	 * @param supportingResultDefinitions - the fields to retrieve for lookup records (getting category information
	 * for a product record) pass null if you don't wish to retrieve lookup records
	 */
	static public HierNode getHierarchyNode(UserSessionContext context, Search search, RecordId rootNodeRecordID, 
											FieldId[] resultTablePath,
											ResultDefinition resultDefinition, ResultDefinition[] supportingResultDefinitions) {

		RepositorySchemaEx schema = MetadataManager.getInstance().getRepositorySchema(context);
		
		TableProperties table = schema.getTable(resultDefinition.getTable());

		switch(table.getType()) {

			case TableProperties.HIERARCHY:
			case TableProperties.TAXONOMY:

				RetrieveLimitedHierTreeCommand cmd = null;
				
				try {
				
					cmd = new RetrieveLimitedHierTreeCommand(context);
				
				} catch (SessionException e) {
				
					e.printStackTrace();
					
				} catch (ConnectionException e) {

					e.printStackTrace();
					
				}

				if(rootNodeRecordID != null) {
					
					cmd.setRootNode(rootNodeRecordID);	
					
				}
								
				cmd.setSearch(search);

				cmd.setResultDefinition(resultDefinition);
				
				if(supportingResultDefinitions != null) {
					
					cmd.setSupportingResultDefinitions(supportingResultDefinitions);
					
				}
				
				if(resultTablePath != null) {
				
					cmd.setResultTablePath(resultTablePath);	
					
				}
				
				
				try {

					cmd.execute();

				} catch (CommandException e) {

					e.printStackTrace();

				}

				return cmd.getTree();

			default:

				throw new IllegalArgumentException("This method only works with Taxonomy and Hierarchy tables.");				

		}
		
	}
	
	
}
