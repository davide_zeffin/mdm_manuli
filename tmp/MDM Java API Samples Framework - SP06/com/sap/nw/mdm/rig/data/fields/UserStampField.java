/*
 * Created on Jun 22, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.fields.UserStampFieldProperties;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.mdm.valuetypes.StringValue;

/**
 * Represents an MDM user stamp field
 * 
 * 
 * @author Richard LeBlanc
 */
class UserStampField extends Field {

	String getValue(FieldProperties field, MdmValue value) {
	
		if(field.getType() == FieldProperties.USERSTAMP_FIELD && 
			value.getType() == MdmValue.Type.STRING) {
		
			UserStampFieldProperties userStampField = (UserStampFieldProperties)field;

			StringValue stringValue = (StringValue)value;
				
			return stringValue.getString();

		} else {
	
			throw new IllegalArgumentException();	
	
		}
	
	}

}
