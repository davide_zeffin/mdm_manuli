/*
 * Created on Jun 19, 2007
 *
 */
package com.sap.nw.mdm.rig.data.util;

import com.sap.mdm.data.HierNode;

/**
 * Prints a hierarchy of records to the console.
 * 
 * @author Richard LeBlanc
 */
public class HierarchyPrinter {
	
	/**
	 * Prints the display field of the record represented by the node as well 
	 * as all the node's descendants.
	 * 
	 * @param node - the node to print
	 */
	static public void print(HierNode node) {
	
		if(node != null) {
			
			System.out.println(node.getDisplayValue());
			
			print(node, 1);
						
		}
		
	}


	static private void print(HierNode node, int level) {
		
		if(!node.isRoot()) {
	
			for(int i=0, j=level; i<j; i++) {
	
				System.out.print("\t");
	
			}
	
			System.out.println(node.getDisplayValue());
	
		}
	
		HierNode[] children = node.getChildren();
	
		if(children != null) {
	
			level++;
	
			for(int i=0, j=children.length; i<j; i++) {
	
				print(children[i], level);
	
			}
	
		}
		
	}

}
