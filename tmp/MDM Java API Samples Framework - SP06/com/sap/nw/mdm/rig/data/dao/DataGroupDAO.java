/*
 * Created on Dec 4, 2007
 *
 */
package com.sap.nw.mdm.rig.data.dao;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.group.HierGroupNode;
import com.sap.mdm.group.commands.RetrieveGroupTreeCommand;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;

/**
 * Provides access to all kinds of group nodes e.g. Validation Group, Data Group & Data Location
 * 
 * @author Richard LeBlanc
 */
public class DataGroupDAO {

	/**
	 * Retrieves the hierarchy group nodes of the given type 
	 * 
	 * @param context - the context used to connect to the repository
	 * @param groupType - the type of group node to retrieve
	 */
	static public HierGroupNode getDataGroupNode(UserSessionContext context, int groupType) {
		
		RetrieveGroupTreeCommand cmd = null;
		
		try {
		
			cmd = new RetrieveGroupTreeCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
			
		} catch (ConnectionException e) {
			
			e.printStackTrace();
			
		}
			
		cmd.setGroupType(groupType);
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
			
		}
		
		return cmd.getGroupTree();
		
	}

}
