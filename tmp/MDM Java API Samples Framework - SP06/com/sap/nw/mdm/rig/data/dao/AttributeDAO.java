/*
 * Created on May 29, 2007
 *
 */
package com.sap.nw.mdm.rig.data.dao;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.commands.RetrieveLimitedAttributeValuesCommand;
import com.sap.mdm.data.commands.RetrieveLimitedAttributesCommand;
import com.sap.mdm.ids.AttributeId;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.search.Search;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * Provides access to attributes that are being referenced by main table records.
 * This is what you see in the search section of the Data Manager.
 * <P>
 * For access to all defined attributes regardless of usage, please have a look at 
 * {@link com.sap.nw.mdm.rig.data.dao.TaxonomyDAO TaxonomyDAO}.
 * 
 * @author Richard LeBlanc
 */
public class AttributeDAO {
	
	private AttributeDAO() {
				
	}
	
	/**
	 * Returns all attributes that are referenced by record(s) that meet the search criteria.
	 * <P>
	 * This method is used to support the drill-down search capabilitie you see in the Data Manager.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param search - the search criteria
	 * @param taxonomyFieldID - the field id of the taxonomy lookup field that contains the attributes
	 */
	static public AttributeProperties[] getAttributes(UserSessionContext context, Search search, 
														FieldId taxonomyFieldID) {
														
		RetrieveLimitedAttributesCommand cmd = null;
		
		try {
		
			cmd = new RetrieveLimitedAttributesCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
	
		cmd.setSearch(search);
	
		cmd.setTaxonomyFieldId(taxonomyFieldID);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
	
		return cmd.getAttributes();
	}

	/**
	 * Returns the list of attribute values for the given AttributeId which are refered to by records that meet 
	 * the search criteria.
	 * <P> 
	 * For example, if you look in the Data Manager, you will see that Products where the manufacturer 
	 * is ABC Hand Tool Corporation and the attribute is Finish have two values - Black Oxide and Cobalt.
	 * <P> 
	 * This method is used to support the drill-down search capabilitie you see in the Data Manager.
	 * <P> 
	 * @param context - the context used to connect to the repository
	 * @param search - the search criteria that defines for which records to return the attribute values
	 * @param taxonomyFieldID - the field id of the taxonomy lookup field that contains the attributes
	 * @param attributeID - the attribute whose values must be returned
	 */
	static public MdmValue[] getAttributeValues(UserSessionContext context, Search search,
												FieldId taxonomyFieldID, AttributeId attributeID) {
	
		RetrieveLimitedAttributeValuesCommand cmd = null;
		
		try {
		
			cmd = new RetrieveLimitedAttributeValuesCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
	
		cmd.setSearch(search);
	
		cmd.setTaxonomyFieldId(taxonomyFieldID);
	
		cmd.setAttributeId(attributeID);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
	
		return cmd.getAttributeValues();
	
	}
	
}
