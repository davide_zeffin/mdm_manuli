/*
 * Created on Aug 31, 2007
 *
 */
package com.sap.nw.mdm.rig.data.dao;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.RelationshipGroup;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.data.commands.ModifyRelationshipsCommand;
import com.sap.mdm.data.commands.RetrieveRelationshipsCommand;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.RelationshipId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;

/**
 * Provides access to all relationship data
 * 
 * @author Richard LeBlanc
 */
public class RelationshipDAO {

	/**
	 * Retrieves information about the anchor record as well as about related 
	 * child, parent or sibling records for the specified relationship.
	 * It can also retrieve information about the relationship at the same time.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param anchorRecordID - the record for which we want related records
	 * @param anchorResultDefinition - the fields to retrieve for the anchor record
	 * @param supportingAnchorResultDefinitions - the fields to retrieve for lookup records of
	 * the anchor record
	 * @param getChildrenOnly - whether to return only child records only
	 * @param loadRecords - whether to load the relationship record
	 * @param memberResultDefinition - the fields to retrieve for related records
	 * @param supportingMemberResultDefinitions - the fields to retrieve for lookup records of
	 * the related records
	 * @param relationshipID - the relationship to search for
	 * @return relationship information
	 */
	static public RelationshipGroup getRelationships(UserSessionContext context,
															RecordId anchorRecordID,
															ResultDefinition anchorResultDefinition,
															ResultDefinition[] supportingAnchorResultDefinitions,
															boolean getChildrenOnly,
															boolean loadRecords,
															ResultDefinition memberResultDefinition,
															ResultDefinition[] supportingMemberResultDefinitions,
															RelationshipId relationshipID) {
		
		RetrieveRelationshipsCommand cmd = null;
		
		try {
		
			cmd = new RetrieveRelationshipsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}

		cmd.setAnchorRecordId(anchorRecordID);
		
		cmd.setAnchorResultDefinition(anchorResultDefinition);
		
		cmd.setAnchorSupportingResultDefinitions(supportingAnchorResultDefinitions);
		
		cmd.setGetChildren(getChildrenOnly);
		
		cmd.setLoadRecords(loadRecords);
		
		cmd.setMemberResultDefinition(memberResultDefinition);
		
		cmd.setMemberSupportingResultDefinitions(supportingMemberResultDefinitions);
		
		cmd.setRelationshipId(relationshipID);		
		
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
		
		}
		
		return cmd.getRelationshipGroup();
		
	}
	
	/**
	 * Modifies the participants in the relationship.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param relationshipGroup - the relationship information
	 * @param replaceAll - whether to remove all current relationship information
	 * and replace it with the provided relationship information
	 */
	static public void modifyRelationships(UserSessionContext context,
											RelationshipGroup relationshipGroup,
											boolean replaceAll) {
												
		ModifyRelationshipsCommand cmd = null;
		
		try {
		
			cmd = new ModifyRelationshipsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
			
			e.printStackTrace();
			
		}

		cmd.setRelationshipGroup(relationshipGroup);
		
		cmd.setReplaceAll(replaceAll);
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
		
		}
							
	}
	
}
