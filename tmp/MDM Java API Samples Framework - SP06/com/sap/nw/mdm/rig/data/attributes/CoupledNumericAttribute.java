/*
 * Created on Jun 21, 2007
 *
 */
package com.sap.nw.mdm.rig.data.attributes;

import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.CoupledAttributeProperties;
import com.sap.mdm.valuetypes.CoupledMeasurementValue;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.mdm.valuetypes.MultiValue;
import com.sap.nw.mdm.rig.data.util.UOM;

/**
 * This class represents a CoupledNumeric attribute
 * 
 * @author Richard LeBlanc
 */
class CoupledNumericAttribute extends Attribute{

	/*
	 * Since Coupled Numeric attributes are always multi-valued, there could be more than one value.
	 * If there is more than one value, it returns all the values as a string where each value is delimited
	 * by a semi-colon.  If there is just one value, it returns that value as a string.
	 * 
	 * @see com.sap.nw.mdm.rig.data.attributes.Attribute#getValue(com.sap.nw.mdm.rig.repository.Repository, com.sap.mdm.schema.AttributeProperties, com.sap.mdm.valuetypes.MdmValue)
	 */
	String getValue(RegionProperties region, AttributeProperties attribute, MdmValue value) {

		if(attribute.getType() == AttributeProperties.COUPLED_TYPE) {
			
			if(value.isMultivalue()) {
			
				StringBuffer stringBuffer = new StringBuffer();
				
				MdmValue[] values = ((MultiValue)value).getValues();
				
				for(int i=0, j=values.length; i<j; i++) {
					
					if(values[i].getType() == MdmValue.Type.COUPLED_MEASUREMENT) {
				
						if(i>0) {
							
							stringBuffer.append(";");
							
						}
						
						stringBuffer.append(getValue(region, (CoupledAttributeProperties)attribute, (CoupledMeasurementValue)values[i]));
				
					}
					 
				}
				
				return stringBuffer.toString();
			
			} else {
				
				if(value.getType() == MdmValue.Type.COUPLED_MEASUREMENT) {
					
					return getValue(region, (CoupledAttributeProperties)attribute, (CoupledMeasurementValue)value);
					
				} else {
					
					throw new IllegalArgumentException();
					
				}
				
			}			
				
		} else {
			
			throw new IllegalArgumentException();
			
		}		
		
	}
	
	/*
	 * Returns the value, as a string, of the given CoupledMeasurementValue.
	 */
	private String getValue(RegionProperties region, CoupledAttributeProperties attribute, CoupledMeasurementValue value) {
		
		String primaryUnit = UOM.getUnit(attribute.getMeasurement().getDimensionId(), value.getPrimaryValue().getUnitId()).getName(region.getLocale());

		String secondaryUnit = UOM.getUnit(attribute.getCoupledMeasurement().getDimensionId(), value.getSecondaryValue().getUnitId()).getName(region.getLocale());

		return value.getPrimaryValue().getMagnitude() + " " + 

				primaryUnit + " " +
		
				attribute.getCoupledDelimiter() + " " +

				value.getSecondaryValue().getMagnitude() + " " +
		
				secondaryUnit;
		 
	}

}