/*
 * Created on Jun 19, 2007
 *
 */
package com.sap.nw.mdm.rig.data.attributes;

import java.util.HashMap;
import java.util.Map;

import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * This abstract class represents a generic Attribute and provides helper methods to 
 * help retrieve the attribute's value. Depending on the type of attribute, 
 * it redirects the calls to one of the appropriate subclasses located in this
 * same package.
 * 
 * 
 * @author Richard LeBlanc
 */
abstract public class Attribute {
	
	/*
	 * Used to get the name and value from a couple numeric attribute
	 */
	static final Attribute COUPLED_ATTRIBUTE = new CoupledNumericAttribute();
	
	/*
	 * Used to get the name and value from a numeric attribute
	 */
	static final Attribute NUMERIC_ATTRIBUTE = new NumericAttribute();
	
	/*
	 * Used to get the name and value from a text attribute
	 */
	static final Attribute TEXT_ATTRIBUTE = new TextAttribute();
	
	//contains a mapping of the MDM attribute type to one of my Attribute subclasses
	static private Map map = new HashMap(3);
	
	static {
		
		map.put(new Integer(AttributeProperties.COUPLED_TYPE), COUPLED_ATTRIBUTE);
		
		map.put(new Integer(AttributeProperties.NUMERIC_TYPE), NUMERIC_ATTRIBUTE);
		
		map.put(new Integer(AttributeProperties.TEXT_TYPE), TEXT_ATTRIBUTE);		
		
	}
	
	/**
	 * Returns the value of the given attribute as a String.
	 * 
	 * @param region - the region for which to provide the value, if the value is multilingual
	 * @param attribute - the attribute whose value to return
	 * @param attributeValue - the value that must be converted to a string
	 */
	static public String valueOf(RegionProperties region, AttributeProperties attribute, MdmValue attributeValue) {
		
		//get the appropriate Attribute subclass from the map
		Attribute myAttribute = (Attribute)map.get(new Integer(attribute.getType()));
		
		return myAttribute.getValue(region, attribute, attributeValue);		

		
	}
	
	
	/**
	 * Returns the value of the given MdmValue as a String.
	 * The AttributeProperties instance is also required because the type of attribute
	 * will affect the value that is returned.
	 *
	 * @param region - the region for which to provide the value, if the value is multilingual	 
	 * @param attribute - the attribute whose value to return
	 * @param value - the value that must be converted to a string
	 * @return
	 */
	abstract String getValue(RegionProperties region, AttributeProperties attribute, MdmValue value);
	
}
