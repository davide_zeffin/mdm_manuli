/*
 * Created on May 29, 2007
 *
 */
package com.sap.nw.mdm.rig.data.dao;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.KeyMapping;
import com.sap.mdm.data.MultipleRecordsResult;
import com.sap.mdm.data.Record;
import com.sap.mdm.data.RegionalLayer;
import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.data.commands.CreateRecordCommand;
import com.sap.mdm.data.commands.CreateRecordsCommand;
import com.sap.mdm.data.commands.DeleteRecordsCommand;
import com.sap.mdm.data.commands.ModifyRecordCommand;
import com.sap.mdm.data.commands.ModifyRecordsCommand;
import com.sap.mdm.data.commands.RetrieveLimitedRecordsCommand;
import com.sap.mdm.data.commands.RetrieveRecordsByIdCommand;
import com.sap.mdm.data.commands.RetrieveRecordsByRemoteKeyCommand;
import com.sap.mdm.data.commands.RetrieveRecordsByValueCommand;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.search.Search;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;
import com.sap.mdm.valuetypes.StringValue;

/**
 * Provides access to records in much the same way as record mode in the Data Manager.
 * This means it provides access to records in a tabular (rows and columns) fashion.
 * 
 * It also provides access to attributes that are being referenced by main table records.
 * For access to all defined attributes regardless of usage, please have a look at 
 * {@link com.sap.nw.mdm.rig.data.dao.TaxonomyDAO TaxonomyDAO}.
 * 
 * @author Richard LeBlanc
 */
public class RecordDAO {
	
	private RecordDAO() {
				
	}
	
	/**
	 * Creates a new record.  The table in which the record is to be added is defined in the given record.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param record - the record to create
	 * @return the record id of the newly created record
	 */
	static public RecordId createRecord(UserSessionContext context, Record record) {
		
		CreateRecordCommand cmd = null;
		
		try {
		
			cmd = new CreateRecordCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setRecord(record);

		try {

			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}

		return cmd.getRecord().getId();
		
	}
	
	
	/**
	 * Creates new records.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table in which to add the records
	 * @param records - the records to create
	 * @return a MultipleRecordsResult which provides information as to which records were successfully created and which weren't
	 */
	static public MultipleRecordsResult createRecords(UserSessionContext context, TableId tableID, Record[] records) {
		
		CreateRecordsCommand cmd = null;
		
		try {
		
			cmd = new CreateRecordsCommand(context);
			
		} catch (SessionException e) {
			
			e.printStackTrace();
			
		} catch (ConnectionException e) {
			
			e.printStackTrace();
			
		}
	
		cmd.setTableId(tableID);
	
		cmd.setRecords(records);
	
		try {
		
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
	
		return cmd.getResult();
	
	}
	
		
	/**
	 * Updates the given record.  The table in which the record is to be updated is defined in the given record.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param record - the record to update
	 * @param modifyAnyway - indicates whether the update will occur even if the record's timestamp is invalid i.e.
	 * the record has been modified between the time you retrieve it and this attempted update
	 */
	static public void modifyRecord(UserSessionContext context, Record record, boolean modifyAnyway) {
		
		ModifyRecordCommand cmd = null;
		
		try {
		
			cmd = new ModifyRecordCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
	
		cmd.setRecord(record);
		
		cmd.setModifyAnyway(modifyAnyway);
		
		try {

			cmd.execute();

		} catch (CommandException e) {

			e.printStackTrace();

		}
			
	}
	
	/**
	 * Updates the given records.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table in which to update the records
	 * @param records - the records to update
	 * @return a MultipleRecordsResult which provides information as to which records were successfully updated and which weren't
	 */
	static public MultipleRecordsResult modifyRecords(UserSessionContext context, TableId tableID, Record[] records) {
		
		ModifyRecordsCommand cmd = null;
		
		try {
		
			cmd = new ModifyRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}

		cmd.setTableId(tableID);
	
		cmd.setRecords(records);
	
		try {

			cmd.execute();

		} catch (CommandException e) {

			e.printStackTrace();

		}
	
		return cmd.getResult();
	
	}
	
	/**
	 * Deletes a record.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table that contains the record to delete
	 * @param recordID - the record id of the record to delete
	 */
	static public void deleteRecord(UserSessionContext context, TableId tableID, RecordId recordID) {
		
		DeleteRecordsCommand cmd = null;
		
		try {
		
			cmd = new DeleteRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.addRecord(recordID);
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
			
		}
		
	}
	
	/**
	 * Deletes a record.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table that contains the record to delete
	 * @param record - the record to delete
	 */
	static public void deleteRecord(UserSessionContext context, TableId tableID, Record record) {
		
		DeleteRecordsCommand cmd = null;
		
		try {
		
			cmd = new DeleteRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {

			e.printStackTrace();
			
		}
	
		cmd.setTable(tableID);
		
		cmd.addRecord(record);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
		
		}
	
	}
	
	/**
	 * Deletes records.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table that contains the record to delete
	 * @param recordIDs - the record ids of the records to delete
	 */
	static public void deleteRecords(UserSessionContext context, TableId tableID, RecordId[] recordIDs) {
		
		DeleteRecordsCommand cmd = null;
		
		try {
		
			cmd = new DeleteRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
	
		cmd.setTable(tableID);
		
		cmd.addRecords(recordIDs);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
		
		}
	
	}
	
	/**
	 * Deletes records.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table that contains the record to delete
	 * @param records - the records to delete
	 */
	static public void deleteRecords(UserSessionContext context, TableId tableID, Record[] records) {
		
		DeleteRecordsCommand cmd = null;
		
		try {
		
			cmd = new DeleteRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}

		cmd.setTable(tableID);

		cmd.addRecords(records);

		try {

			cmd.execute();

		} catch (CommandException e) {

			e.printStackTrace();
	
		}

	}
	
	
	
	/**
	 * Returns all records whose record id is provided.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param resultDefinition - the structure of the records i.e. fields and attributes
	 * @param supportingResultDefinitions - the fields to retrieve for lookup records e.g. getting category information
	 * for a product record
	 * @param recordIDs - the records to retrieve 
	 */
	static public Record[] getRecordsByID(UserSessionContext context,
											ResultDefinition resultDefinition, 
											ResultDefinition[] supportingResultDefinitions,
											RecordId[] recordIDs) {
		
		RetrieveRecordsByIdCommand cmd = null;
		
		try {
		
			cmd = new RetrieveRecordsByIdCommand(context);
		
		} catch (SessionException e) {
			
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setIds(recordIDs);
		
		cmd.setResultDefinition(resultDefinition);
		
		if(supportingResultDefinitions != null) {
			
			cmd.setSupportingResultDefinitions(supportingResultDefinitions);
			
		}
		

		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}

		return cmd.getRecords().getRecords();
		
	}
	
	
	
	/**
	 * Returns all records from the ResultDefinition table that meet the given search criteria
	 *
	 * @param context - the context used to connect to the repository
	 * @param search - the search table and criteria
	 * @param resultDefinition - the structure of the records i.e. fields and attributes(ignores the 
	 * resultdefinition table when used in conjunction with a search object)
	 * @param supportingResultDefinitions - the fields to retrieve for lookup records (getting category information
	 * for a product record) pass null if you don't wish to retrieve lookup records
	 * @param pageSize - how many records to retrieve for each call (The default is 1000)
	 * @param pageIndex - the page to retrieve where the number of pages = total records / page size (page index starts at 0)
	 */
	static public Record[] getRecords(UserSessionContext context,
										Search search,
										ResultDefinition resultDefinition,
										FieldId[] resultTablePath,
										ResultDefinition[] supportingResultDefinitions,
										int pageSize,
										int pageIndex) {
		
		RetrieveLimitedRecordsCommand cmd = null;
		
		try {
		
			cmd = new RetrieveLimitedRecordsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
			
			e.printStackTrace();
			
		}
	
		cmd.setSearch(search);
	
		cmd.setResultDefinition(resultDefinition);
		
		if(resultTablePath != null) {
			
			cmd.setResultTablePath(resultTablePath);
			
		}
		
		if(supportingResultDefinitions != null) {
			
			cmd.setSupportingResultDefinitions(supportingResultDefinitions);
			
		}
		
		if(pageSize > 0) {
			
			cmd.setPageSize(pageSize); 
		
			cmd.setPageIndex(pageIndex);
			
		}
		
		cmd.setRegionalLayer(RegionalLayer.ALL);
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
		
		}
	
		return cmd.getRecords().getRecords();		
	
	}
	
	/**
	 * Retrieves records where the specified field has one of the provided values.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param fieldID - the field to search
	 * @param values - the values to look for
	 * @param resultDefinition - the fields to retrieve
	 * @param supportingResultDefinitions - the fields to retrieve for lookup records (getting category information
	 * for a product record)
	 * @return records where the specified field has one of the provided values
	 */
	static public Record[] getRecordsByValue(UserSessionContext context,
												FieldId fieldID,
												StringValue[] values,
												ResultDefinition resultDefinition,
												ResultDefinition[] supportingResultDefinitions) {
		
		RetrieveRecordsByValueCommand cmd = null;
		
		try {
		
			cmd = new RetrieveRecordsByValueCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}

		cmd.setFieldId(fieldID);
		
		cmd.setFieldValues(values);

		cmd.setResultDefinition(resultDefinition);
	
		if(supportingResultDefinitions != null) {
		
			cmd.setSupportingResultDefinitions(supportingResultDefinitions);
		
		}
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}

		return cmd.getRecords().getRecords();		

	}
	
	/**
	 * Retrieve records that have a remote key specified in the provided KeyMapping object
	 * 
	 * @param context - the context used to connect to the repository
	 * @param keyMapping - contains the remote keys to search for
	 * @param resultDefinition - the fields to retrieve
	 * @param supportingResultDefinitions - the fields to retrieve for lookup records (getting category information
	 * for a product record)
	 * @return records that have a remote key specified in the provided KeyMapping object
	 */
	static public Record[] getRecordsByRemoteKey(UserSessionContext context,
													KeyMapping keyMapping,
													ResultDefinition resultDefinition,
													ResultDefinition[] supportingResultDefinitions) {
		
		RetrieveRecordsByRemoteKeyCommand cmd = null;
		
		try {
		
			cmd = new RetrieveRecordsByRemoteKeyCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {

			e.printStackTrace();

		}
		
		cmd.setRemoteKeys(keyMapping);
		
		cmd.setResultDefinition(resultDefinition);
		
		if(supportingResultDefinitions != null) {
			
			cmd.setSupportingResultDefinitions(supportingResultDefinitions);
			
		}
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
			
		}
		
		return cmd.getRecords().getRecords();
		
	}
	
}
