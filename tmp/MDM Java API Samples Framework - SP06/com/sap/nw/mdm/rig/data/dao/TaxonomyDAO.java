/*
 * Created on May 29, 2007
 *
 */
package com.sap.nw.mdm.rig.data.dao;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.AttributeLink;
import com.sap.mdm.data.commands.CreateAttributeCommand;
import com.sap.mdm.data.commands.CreateAttributeLinkCommand;
import com.sap.mdm.data.commands.DeleteAttributeCommand;
import com.sap.mdm.data.commands.DeleteAttributeLinkCommand;
import com.sap.mdm.data.commands.ModifyAttributeCommand;
import com.sap.mdm.ids.AttributeId;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;

/**
 * Provides the ability to manage attributes in much the same way as taxonomy mode in the Data Manager e.g
 * create/modify/delete attributes and link/unlink attributes to taxonomy table records.
 * 
 * @author Richard LeBlanc
 *
 */
public class TaxonomyDAO {

	private TaxonomyDAO() {
		
	}

	/**
	 * Creates a new attribute
	 * 
	 * @param context - the context used to connect to the repository
	 * @param attribute - the attribute to add
	 * @return the newly created attribute
	 */
	static public AttributeProperties createAttribute(UserSessionContext context, AttributeProperties attribute) {

		CreateAttributeCommand cmd = null;
		
		try {
		
			cmd = new CreateAttributeCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setAttribute(attribute);
	
		try {
	
			cmd.execute();
	
		} catch (CommandException e) {
	
			e.printStackTrace();
	
		}
		
		return cmd.getAttribute();
		
	}
	
	
	/**
	 * Modifies an existing attribute
	 * 
	 * @param context - the context used to connect to the repository
	 * @param attribute - the attribute to modify
	 * @return the newly modified attribute
	 */
	static public AttributeProperties modifyAttribute(UserSessionContext context, AttributeProperties attribute) {

		ModifyAttributeCommand cmd = null;
		
		try {
		
			cmd = new ModifyAttributeCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
	
		cmd.setAttribute(attribute);

		try {

			cmd.execute();

		} catch (CommandException e) {

			e.printStackTrace();

		}
	
		return cmd.getAttribute();
	
	}
		
		
	/**
	 * Links the given attribute to the given taxonomy record.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param taxonomyTableID - the taxonomy table to work with
	 * @param attributeID - the attribute to link to the taxonomy record
	 * @param taxonomyRecordID - the taxonomy record to which the attribute will be linked
	 * @param changeStamp - the change stamp of the taxonomy record
	 */
	static public void linkAttribute(UserSessionContext context,
										TableId taxonomyTableID, AttributeId attributeID,
										RecordId taxonomyRecordID, int changeStamp) {
		
		AttributeLink al = new AttributeLink(attributeID, AttributeLink.NORMAL_PRIORITY);
		
		
		CreateAttributeLinkCommand cmd = null;
		
		try {
		
			cmd = new CreateAttributeLinkCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}

		cmd.setTaxonomyTableId(taxonomyTableID);
		
		cmd.setAttributeLink(al);
		
		cmd.setRecordId(taxonomyRecordID);
		
		cmd.setChangeStamp(changeStamp);

		try {

			cmd.execute();

		} catch (CommandException e) {

			e.printStackTrace();

		}
	
	}
	
	/**
	 * Unlinks the given attribute to the given taxonomy record
	 * 
	 * @param context - the context used to connect to the repository
	 * @param taxonomyTableID - the taxonomy table to work with
	 * @param attributeID - the attribute to unlink
	 * @param taxonomyRecordID - the taxonomy record to remove the attribute from
	 * @param changeStamp - the change stamp of the taxonomy record
	 */
	static public void unLinkAttribute(UserSessionContext context,
											TableId taxonomyTableID, AttributeId attributeID,
											RecordId taxonomyRecordID, int changeStamp) {
		
		DeleteAttributeLinkCommand cmd = null;
		
		try {
		
			cmd = new DeleteAttributeLinkCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}

		cmd.setTaxonomyTableId(taxonomyTableID);
	
		cmd.setRecordId(taxonomyRecordID);
		
		cmd.setAttributeId(attributeID);
	
		cmd.setChangeStamp(changeStamp);

		try {

			cmd.execute();

		} catch (CommandException e) {

			e.printStackTrace();

		}

	}
	
	/**
	 * Deletes the given attribute from the taxonomy table.  This is only possible if no records
	 * have a reference to the attribute.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param taxonomyTableID - the taxonomy table where the attribute is located
	 * @param attributeID - the attribute to delete
	 */
	static public void deleteAttribute(UserSessionContext context, TableId taxonomyTableID, AttributeId attributeID) {
		
		DeleteAttributeCommand cmd = null;
		
		try {
		
			cmd = new DeleteAttributeCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setTaxonomyTableId(taxonomyTableID);
		
		cmd.setAttributeId(attributeID);
		
		try {
			
			cmd.execute();
			
		} catch (CommandException e) {
			
			e.printStackTrace();
			
		}
		
	}
	
}
