/*
 * Created on Jun 22, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.fields.LargeTextFieldProperties;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.mdm.valuetypes.MultiregionValue;
import com.sap.mdm.valuetypes.StringValue;

/**
 * Represents an MDM large text field
 * 
 * @author Richard LeBlanc
 */
class LargeTextField extends Field {

	/*
	 *  (non-Javadoc)
	 * @see com.sap.nw.mdm.rig.data.fields.Field#getValue(com.sap.nw.mdm.rig.repository.Repository, com.sap.mdm.schema.FieldProperties, com.sap.mdm.valuetypes.MdmValue)
	 */
	String getRegionalValue(FieldProperties field, MdmValue value, String regionCode) {
		
		if(field instanceof LargeTextFieldProperties && 
			(value instanceof StringValue || value instanceof MultiregionValue)) {
		
			LargeTextFieldProperties largeTextField = (LargeTextFieldProperties)field;
	
			StringValue stringValue = null;
			
			if(value.isMultiregion()) {
		
				MultiregionValue multiRegionValue = (MultiregionValue)value;
				
				return ((StringValue)multiRegionValue.getValue(regionCode)).getString();
				
			} else {
				
				return ((StringValue)value).getString();
				
			}
			
		} else {
	
			throw new IllegalArgumentException();	
	
		}

	}

}
