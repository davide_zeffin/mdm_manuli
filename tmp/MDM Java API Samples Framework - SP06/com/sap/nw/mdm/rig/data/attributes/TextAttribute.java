/*
 * Created on Jun 20, 2007
 *
 */
package com.sap.nw.mdm.rig.data.attributes;

import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.schema.AttributeProperties;
import com.sap.mdm.schema.TextAttributeProperties;
import com.sap.mdm.schema.TextAttributeValueProperties;
import com.sap.mdm.valuetypes.MdmValue;
import com.sap.mdm.valuetypes.MultiValue;
import com.sap.mdm.valuetypes.TextAttributeValue;

/**
 * This class represents a Text Attribute
 * 
 * @author Richard LeBlanc
 */
class TextAttribute extends Attribute {

	/*
	 * Returns the value of the attribute, which might be multi-valued, for the given region.
	 * 
	 * @see com.sap.nw.mdm.rig.data.attributes.Attribute#getValue(com.sap.nw.mdm.rig.repository.Repository, com.sap.mdm.schema.AttributeProperties, com.sap.mdm.valuetypes.MdmValue)
	 */
	String getValue(RegionProperties region, AttributeProperties attribute, MdmValue value) {

		if(attribute.getType() == TextAttributeProperties.TEXT_TYPE) {
		
			TextAttributeProperties textAttribute = (TextAttributeProperties)attribute;
			
			String result = null;
		
			if(value.isMultivalue()) {
				
				StringBuffer buffer = new StringBuffer();
				
				MultiValue multiValue = (MultiValue)value;
				
				for(int i=0, j=multiValue.getValuesCount(); i<j; i++) {
				
					buffer.append(getValue(region, textAttribute, (TextAttributeValue)multiValue.getValue(i)));
					
				}
				
				result = buffer.toString();			
				 
			} else {
				
				if(value.getType() == MdmValue.Type.TEXT_ATTRIBUTE) {
					
					result = getValue(region, textAttribute, (TextAttributeValue)value);
					
				} else {
					
					throw new IllegalArgumentException();
						
				}
				
			}
			
			return result;

		} else {

			throw new IllegalArgumentException();
	
		}

	}
	
	private String getValue(RegionProperties region, TextAttributeProperties attribute, TextAttributeValue value) {
		
		TextAttributeValueProperties[] values = attribute.getTextAttributeValues();
		
		String result = null;
		
		for(int i=0, j=values.length; i<j; i++) {
			
			if(values[i].getId().getIdValue() == value.getId().getIdValue()) {
					
				result =  values[i].getName().get(region.getRegionCode());
					
			}
			
		}
		
		return result;
		
	}

}
