/*
 * Created on Jun 19, 2007
 *
 */
package com.sap.nw.mdm.rig.data.util;

import com.sap.mdm.data.Record;
import com.sap.mdm.data.RegionProperties;
import com.sap.mdm.session.UserSessionContext;

/**
 * Prints a hierarchy of records to the console.
 * 
 * @author Richard LeBlanc
 */
public class TaxonomyRecordPrinter {
	
	/**
	 * Prints the display field of the record represented by the node as well 
	 * as all the node's descendants.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param record - the record to be printed
	 * @param region - provides the language for multilingual fields
	 */
	static public void print(UserSessionContext context, Record record, RegionProperties region) {
	
		RecordPrinter.print(context, record, region);
		
		AttributesPrinter.print(context, record, region);

	}

}
