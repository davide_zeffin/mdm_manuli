/*
 * Created on Aug 30, 2007
 *
 */
package com.sap.nw.mdm.rig.data.dao;

import com.sap.mdm.commands.CommandException;
import com.sap.mdm.data.RecordKeyMapping;
import com.sap.mdm.data.commands.ModifyRecordsKeyMappingsCommand;
import com.sap.mdm.data.commands.RetrieveRecordsKeyMappingsCommand;
import com.sap.mdm.ids.RecordId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.net.ConnectionException;
import com.sap.mdm.session.SessionException;
import com.sap.mdm.session.UserSessionContext;

/**
 * Provides access to all records' key mapping information.
 * 
 * @author Richard LeBlanc
 */
public class KeyMappingDAO {
	
	/**
	 * Returns key mappings for the provided record ids.
	 * 
	 * @param context - the context used to connect to the repository
	 * @param tableID - the table to get the key mapping from
	 * @param recordIDs - the record ids for which to get the key mappings
	 * @param defaultKeysOnly - whether to return the default remote key only
	 * @return returns the key mappings for the provided record ids
	 */
	static public RecordKeyMapping[] getKeyMappings(UserSessionContext context,
												TableId tableID,
												RecordId[] recordIDs,
												boolean defaultKeysOnly) {
		
		RetrieveRecordsKeyMappingsCommand cmd = null;
		
		try {
		
			cmd = new RetrieveRecordsKeyMappingsCommand(context);
		
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setDefaultKeysOnly(defaultKeysOnly);
		
		cmd.setTableId(tableID);
		
		cmd.setRecordIds(recordIDs);
		
		try {
			
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
		
		}
		
		RecordKeyMapping[] recordKMs = cmd.getKeyMappings();
		
		return recordKMs;
		
	}
	
	static public void modifyKeyMappings(UserSessionContext context, RecordKeyMapping[] recordKeyMappings) {
		
		ModifyRecordsKeyMappingsCommand cmd = null;
		
		try {
			
			cmd = new ModifyRecordsKeyMappingsCommand(context);
			
		} catch (SessionException e) {
		
			e.printStackTrace();
		
		} catch (ConnectionException e) {
		
			e.printStackTrace();
		
		}
		
		cmd.setKeyMappings(recordKeyMappings);
		
		try {
		
			cmd.execute();
		
		} catch (CommandException e) {
		
			e.printStackTrace();
		
		}
		
	}

}
