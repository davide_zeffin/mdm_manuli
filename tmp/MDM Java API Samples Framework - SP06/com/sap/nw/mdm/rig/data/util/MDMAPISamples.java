/*
 * Created on Jun 26, 2007
 *
 */
package com.sap.nw.mdm.rig.data.util;


/**
 * Represents the schema for the Products repository.
 *  
 * @author Richard LeBlanc
 */
public class MDMAPISamples {

	public class Products {
	   
		static public final String TABLE = "Products";
	   
		static public final String PRODUCT_NAME = "Product_Name";
		   
		static public final String PART_NUMBER = "Part_Number";
		
		static public final String CATEGORY = "Category";
		
		static public final String MANUFACTURER = "Manufacturer";
		
		static public final String DISTRIBUTION_AREA = "Distribution_Area";
		
		static public final String REMARKS = "Remarks";
		
		static public final String COST_PRICE = "Cost_Price";
		
		static public final String ACTIVE_STOCK = "Active_Stock";
		   
		static public final String LAST_UPDATED = "Last_Updated";
		
		static public final String UPDATED_BY = "Updated_By";
		
		static public final String APPROVAL_DATE = "Approval_Date";
		   
		static public final String IMAGES = "Images";
		
		static public final String CUSTOMER_NUMBERS = "Customer_Numbers";
		
		static public final String SPEC_SHEETS = "Spec_Sheets";
		
		static public final String FEATURES_AND_GUIDANCE = "Features_and_Guidance";
		
		static public final String REGIONAL_LIST_PRICE = "Regional_List_Price";
		   
		static public final String APPLICATION = "Application";
		
		static public final String REGION = "Region";
		  	   
		static public final String DESCRIPTION = "Description";
		
		static public final String DISTRIBUTORS = "Distributor";
				
	}
   
	public class Countries {
		
		static public final String TABLE = "Countries";
		
		static public final String NAME = "Name";
		
	}
	
	public class Distribution_Areas {
		
		static public final String TABLE = "Distribution_Areas";
		
		static public final String NAME = "Name";
		
		static public final String AREA_MANAGER = "Area_Manager";
		
	}
	
	public class Distributors {
		
		static public final String TABLE = "Distributors";
	
		static public final String NAME = "Name";
	
	}
	
	public class Location_Regions {
		
		static public final String TABLE = "Location_Regions";
	
		static public final String NAME = "Name";
	
		static public final String COUNTRY = "Country";
	
	}
	
	public class Non_SKU_Items {
		
		static public final String TABLE = "Non_SKU_Items";
	
		static public final String NAME = "Name";
	
	}
   
	public class Manufacturers {
	   
		static public final String TABLE = "Manufacturers";
	   
		static public final String NAME = "Name";
		
		static public final String LOGO = "Logo";
		
		static public final String SUBJECT = "Subject";
		
		static public final String URL = "URL";
	   
	}
   
	public class Categories {
	   
		static public final String TABLE = "Categories";
	   
		static public final String CATEGORY = "Category";
		
		static public final String DEFINITION = "Definition";
	   
	}

	public class Customers {
		
		static public final String TABLE = "Customers";
		
		static public final String NAME = "Name";
		
		static public final String CUSTOMER_PART_NUMBER = "Customer_Part_Number";
		
		static public final String CUSTOMER_PRICE = "Customer_Price";
		
	}

	public class Pricing_Regions {
	   
		static public final String TABLE = "Pricing_Regions";
	   
		static public final String NAME = "Name";
	   
		static public final String LIST_PRICE = "List_Price";
		
		static public final String PACKAGE_QUANTITY = "Package_Qty";
	   
	}
	
	public class Images {
	   
		static public final String TABLE = "Images";
   
		static public final String ORIGINAL_NAME = "Original_Name";
		
	}
	
	public class PDF {
		
		static public final String TABLE = "PDFs";
		
		static public final String ORIGINAL_NAME = "Original_Name";
		
	}
   
	public class TextBlocks {
   	
		static public final String TABLE = "Text Blocks";
	   
	}
   
	public class Masks {
	   
		static public final String TABLE = "Masks";
	   
		static public final String NAME = "Name";
	   
	}
	
	public class NamedSearches {
		
		static public final String TABLE = "Named_Searches";
		
		static public final String NAME = "Name";
		
	}

}
