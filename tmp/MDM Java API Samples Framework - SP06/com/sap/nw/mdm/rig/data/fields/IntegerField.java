/*
 * Created on Jun 22, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.fields.IntegerFieldProperties;
import com.sap.mdm.valuetypes.IntegerValue;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * Represents an MDM integer field 
 * 
 * @author Richard LeBlanc
 */
class IntegerField extends Field {

	String getValue(FieldProperties field, MdmValue value) {
	
		if(field.getType() == FieldProperties.INTEGER_FIELD && 
			value.getType() == MdmValue.Type.INTEGER) {
		
			IntegerFieldProperties integerField = (IntegerFieldProperties)field;
	
			IntegerValue integerValue = (IntegerValue)value;
				
			return String.valueOf(integerValue.getInt());

		} else {
	
			throw new IllegalArgumentException();	
	
		}
	
	}

}
