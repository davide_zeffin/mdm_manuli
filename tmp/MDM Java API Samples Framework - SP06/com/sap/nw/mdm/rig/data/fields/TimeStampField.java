/*
 * Created on Jun 22, 2007
 *
 */
package com.sap.nw.mdm.rig.data.fields;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.fields.TimeStampFieldProperties;
import com.sap.mdm.valuetypes.DateTimeValue;
import com.sap.mdm.valuetypes.MdmValue;

/**
 * Represents an MDM timestamp field
 * 
 * @author Richard LeBlanc
 */
class TimeStampField extends Field {

	String getValue(FieldProperties field, MdmValue value) {
	
		if(field.getType() == FieldProperties.TIMESTAMP_FIELD && 
			value.getType() == MdmValue.Type.DATE_TIME) {
		
			TimeStampFieldProperties timeStampField = (TimeStampFieldProperties)field;

			DateTimeValue dateTimeValue = (DateTimeValue)value;

			GregorianCalendar gc = new GregorianCalendar(dateTimeValue.getYear(),
															dateTimeValue.getMonth(),
															dateTimeValue.getDay(),
															dateTimeValue.getHour(),
															dateTimeValue.getMinute(),
															dateTimeValue.getSeconds());
																
			gc.set(Calendar.MILLISECOND, dateTimeValue.getMilliseconds());
				
			DateFormat dateFormat = DateFormat.getDateTimeInstance();
				
			return dateFormat.format(gc.getTime());

		} else {
	
			throw new IllegalArgumentException();	
	
		}
	
	}

}
