// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.wiit.model;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Bapisflist" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Bapisflist extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Bapisflist.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.wiit.model.Flights.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.wiit.model.Flights.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Bapisflist () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.wiit.model.Flights.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.wiit.model.Flights.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Bapisflist.class,
                       "BAPISFLIST",
                       PROXYTYPE_STRUCTURE, 
                       "Bapisflist", 
                       "it.wiit.model" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Bapisflist (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Bapisflist (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Bapisflist (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.wiit.model.Flights 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Bapisflist ( it.wiit.model.Flights modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public Flights modelInstance() {
    return (Flights)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.wiit.model.Flights.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> Airpfrom
   * **************************************************************************/
  /** getter for ModelAttribute -> Airpfrom 
   *  @return value of ModelAttribute Airpfrom */
  public java.lang.String getAirpfrom() {
    return super.getAttributeValueAsString("Airpfrom");
  }
 
  /** setter for ModelAttribute -> Airpfrom 
   *  @param value new value for ModelAttribute Airpfrom */
  public void setAirpfrom(java.lang.String value) {
    super.setAttributeValueAsString("Airpfrom", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Airpto
   * **************************************************************************/
  /** getter for ModelAttribute -> Airpto 
   *  @return value of ModelAttribute Airpto */
  public java.lang.String getAirpto() {
    return super.getAttributeValueAsString("Airpto");
  }
 
  /** setter for ModelAttribute -> Airpto 
   *  @param value new value for ModelAttribute Airpto */
  public void setAirpto(java.lang.String value) {
    super.setAttributeValueAsString("Airpto", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Carrid
   * **************************************************************************/
  /** getter for ModelAttribute -> Carrid 
   *  @return value of ModelAttribute Carrid */
  public java.lang.String getCarrid() {
    return super.getAttributeValueAsString("Carrid");
  }
 
  /** setter for ModelAttribute -> Carrid 
   *  @param value new value for ModelAttribute Carrid */
  public void setCarrid(java.lang.String value) {
    super.setAttributeValueAsString("Carrid", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Connid
   * **************************************************************************/
  /** getter for ModelAttribute -> Connid 
   *  @return value of ModelAttribute Connid */
  public java.lang.String getConnid() {
    return super.getAttributeValueAsString("Connid");
  }
 
  /** setter for ModelAttribute -> Connid 
   *  @param value new value for ModelAttribute Connid */
  public void setConnid(java.lang.String value) {
    super.setAttributeValueAsString("Connid", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Deptime
   * **************************************************************************/
  /** getter for ModelAttribute -> Deptime 
   *  @return value of ModelAttribute Deptime */
  public java.sql.Time getDeptime() {
    return super.getAttributeValueAsTime("Deptime");
  }
 
  /** setter for ModelAttribute -> Deptime 
   *  @param value new value for ModelAttribute Deptime */
  public void setDeptime(java.sql.Time value) {
    super.setAttributeValueAsTime("Deptime", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Fldate
   * **************************************************************************/
  /** getter for ModelAttribute -> Fldate 
   *  @return value of ModelAttribute Fldate */
  public java.sql.Date getFldate() {
    return super.getAttributeValueAsDate("Fldate");
  }
 
  /** setter for ModelAttribute -> Fldate 
   *  @param value new value for ModelAttribute Fldate */
  public void setFldate(java.sql.Date value) {
    super.setAttributeValueAsDate("Fldate", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Seatsmax
   * **************************************************************************/
  /** getter for ModelAttribute -> Seatsmax 
   *  @return value of ModelAttribute Seatsmax */
  public int getSeatsmax() {
    return super.getAttributeValueAsInt("Seatsmax");
  }
 
  /** setter for ModelAttribute -> Seatsmax 
   *  @param value new value for ModelAttribute Seatsmax */
  public void setSeatsmax(int value) {
    super.setAttributeValueAsInt("Seatsmax", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Seatsocc
   * **************************************************************************/
  /** getter for ModelAttribute -> Seatsocc 
   *  @return value of ModelAttribute Seatsocc */
  public int getSeatsocc() {
    return super.getAttributeValueAsInt("Seatsocc");
  }
 
  /** setter for ModelAttribute -> Seatsocc 
   *  @param value new value for ModelAttribute Seatsocc */
  public void setSeatsocc(int value) {
    super.setAttributeValueAsInt("Seatsocc", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
  public static class Bapisflist_List extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCList implements java.io.Serializable , java.util.List   {

    protected Class _associatedModelClazz() { return _associatedModelClazz; }
	protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){ return DEFAULT_SCOPE; }

    private static final long serialVersionUID = 60422512L ;
    
    public Bapisflist_List () {
      super(createElementProperties(it.wiit.model.Bapisflist.class, new it.wiit.model.Bapisflist() ));
    }

    /** Constructor for specific scope
     *  use this Constructor, if this List should belong to a model instance
     *  of a different scope than the default model scope.
     *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     */
    public Bapisflist_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
      this();
      maintainInScope(scope);
    }
  
    /** Constructor for specific modelInstanceId
     *  use this Constructor, if this List should belong to a different model instance
     *  than the default model instance.
     *  The default modelInstance has no id (null). 
     * 
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Bapisflist_List (String modelInstanceId) {
      this();
      maintainInInstanceId(modelInstanceId);
    }
  
    /** Constructor for specific scope and modelInstanceId
     *  use this Constructor, of this List should belong to a differnt scope
     *  and a different model instance than the default model scope and model instance.
     *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
     *  The default modelInstance has no id (null). 
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Bapisflist_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
      this();
      maintainInScopeAndInstanceId(scope, modelInstanceId);
    }
  
    /** Constructor for specific model Instance
     *  use this Constructor, if this instance should belong to a specific instance
     *  of the model it.wiit.model.Flights 
     *  passed as a parameter to this constructor.
     * 
     *  @param modelInstance the DynamicRFCModel referring to the modelInstance
     */
    public Bapisflist_List ( it.wiit.model.Flights modelInstance) {
      this();
      maintainInModel(modelInstance);
    }

    public it.wiit.model.Bapisflist[] toArrayBapisflist() {
      return (it.wiit.model.Bapisflist[])super.toArray(new it.wiit.model.Bapisflist[] {});
    }
    
    public int lastIndexOfBapisflist(it.wiit.model.Bapisflist item) {
      return super.lastIndexOf(item);
    }
    
    public int indexOfBapisflist(it.wiit.model.Bapisflist item) {
      return super.indexOf(item);
    }
    
    public Bapisflist_List subListBapisflist(int fromIndex, int toIndex) {
      return (Bapisflist_List)super.subList(fromIndex, toIndex);
    }
    
    public void addAllBapisflist(Bapisflist_List item) {
      super.addAll(item);
    }
    
    public void addBapisflist(it.wiit.model.Bapisflist item) {
      super.add(item);
    }
    
    public boolean removeBapisflist(it.wiit.model.Bapisflist item) {
      return super.remove(item);
    }
    
    public it.wiit.model.Bapisflist getBapisflist(int index) {
      return (it.wiit.model.Bapisflist)super.get(index);
    }
    
    public boolean containsAllBapisflist(Bapisflist_List item) {
      return super.containsAll(item);
    }
    
    public void addBapisflist(int index, it.wiit.model.Bapisflist item) {
      super.add(index, item);
    }
    
    public boolean containsBapisflist(it.wiit.model.Bapisflist item) {
      return super.contains(item);
    }
        
    public void addAllBapisflist(int index, Bapisflist_List item) {
      super.addAll(index, item);
    }
    
    public it.wiit.model.Bapisflist setBapisflist(int index, it.wiit.model.Bapisflist item) {
      return (it.wiit.model.Bapisflist)super.set(index, item);
    }
    
    public it.wiit.model.Bapisflist removeBapisflist(int index) {
      return (it.wiit.model.Bapisflist)super.remove(index);
    }
  }
  
}
