// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.wiit.model;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Bapisfdeta" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Bapisfdeta extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClass implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Bapisfdeta.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.wiit.model.Flights.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.wiit.model.Flights.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Bapisfdeta () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.wiit.model.Flights.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.wiit.model.Flights.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Bapisfdeta.class,
                       "BAPISFDETA",
                       PROXYTYPE_STRUCTURE, 
                       "Bapisfdeta", 
                       "it.wiit.model" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Bapisfdeta (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Bapisfdeta (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Bapisfdeta (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.wiit.model.Flights 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Bapisfdeta ( it.wiit.model.Flights modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public Flights modelInstance() {
    return (Flights)associatedModel();
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.wiit.model.Flights.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> Airpfrom
   * **************************************************************************/
  /** getter for ModelAttribute -> Airpfrom 
   *  @return value of ModelAttribute Airpfrom */
  public java.lang.String getAirpfrom() {
    return super.getAttributeValueAsString("Airpfrom");
  }
 
  /** setter for ModelAttribute -> Airpfrom 
   *  @param value new value for ModelAttribute Airpfrom */
  public void setAirpfrom(java.lang.String value) {
    super.setAttributeValueAsString("Airpfrom", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Airpto
   * **************************************************************************/
  /** getter for ModelAttribute -> Airpto 
   *  @return value of ModelAttribute Airpto */
  public java.lang.String getAirpto() {
    return super.getAttributeValueAsString("Airpto");
  }
 
  /** setter for ModelAttribute -> Airpto 
   *  @param value new value for ModelAttribute Airpto */
  public void setAirpto(java.lang.String value) {
    super.setAttributeValueAsString("Airpto", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Arrtime
   * **************************************************************************/
  /** getter for ModelAttribute -> Arrtime 
   *  @return value of ModelAttribute Arrtime */
  public java.sql.Time getArrtime() {
    return super.getAttributeValueAsTime("Arrtime");
  }
 
  /** setter for ModelAttribute -> Arrtime 
   *  @param value new value for ModelAttribute Arrtime */
  public void setArrtime(java.sql.Time value) {
    super.setAttributeValueAsTime("Arrtime", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Carrid
   * **************************************************************************/
  /** getter for ModelAttribute -> Carrid 
   *  @return value of ModelAttribute Carrid */
  public java.lang.String getCarrid() {
    return super.getAttributeValueAsString("Carrid");
  }
 
  /** setter for ModelAttribute -> Carrid 
   *  @param value new value for ModelAttribute Carrid */
  public void setCarrid(java.lang.String value) {
    super.setAttributeValueAsString("Carrid", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Cityfrom
   * **************************************************************************/
  /** getter for ModelAttribute -> Cityfrom 
   *  @return value of ModelAttribute Cityfrom */
  public java.lang.String getCityfrom() {
    return super.getAttributeValueAsString("Cityfrom");
  }
 
  /** setter for ModelAttribute -> Cityfrom 
   *  @param value new value for ModelAttribute Cityfrom */
  public void setCityfrom(java.lang.String value) {
    super.setAttributeValueAsString("Cityfrom", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Cityto
   * **************************************************************************/
  /** getter for ModelAttribute -> Cityto 
   *  @return value of ModelAttribute Cityto */
  public java.lang.String getCityto() {
    return super.getAttributeValueAsString("Cityto");
  }
 
  /** setter for ModelAttribute -> Cityto 
   *  @param value new value for ModelAttribute Cityto */
  public void setCityto(java.lang.String value) {
    super.setAttributeValueAsString("Cityto", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Connid
   * **************************************************************************/
  /** getter for ModelAttribute -> Connid 
   *  @return value of ModelAttribute Connid */
  public java.lang.String getConnid() {
    return super.getAttributeValueAsString("Connid");
  }
 
  /** setter for ModelAttribute -> Connid 
   *  @param value new value for ModelAttribute Connid */
  public void setConnid(java.lang.String value) {
    super.setAttributeValueAsString("Connid", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Countryfr
   * **************************************************************************/
  /** getter for ModelAttribute -> Countryfr 
   *  @return value of ModelAttribute Countryfr */
  public java.lang.String getCountryfr() {
    return super.getAttributeValueAsString("Countryfr");
  }
 
  /** setter for ModelAttribute -> Countryfr 
   *  @param value new value for ModelAttribute Countryfr */
  public void setCountryfr(java.lang.String value) {
    super.setAttributeValueAsString("Countryfr", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Countryto
   * **************************************************************************/
  /** getter for ModelAttribute -> Countryto 
   *  @return value of ModelAttribute Countryto */
  public java.lang.String getCountryto() {
    return super.getAttributeValueAsString("Countryto");
  }
 
  /** setter for ModelAttribute -> Countryto 
   *  @param value new value for ModelAttribute Countryto */
  public void setCountryto(java.lang.String value) {
    super.setAttributeValueAsString("Countryto", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Currency
   * **************************************************************************/
  /** getter for ModelAttribute -> Currency 
   *  @return value of ModelAttribute Currency */
  public java.lang.String getCurrency() {
    return super.getAttributeValueAsString("Currency");
  }
 
  /** setter for ModelAttribute -> Currency 
   *  @param value new value for ModelAttribute Currency */
  public void setCurrency(java.lang.String value) {
    super.setAttributeValueAsString("Currency", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Deptime
   * **************************************************************************/
  /** getter for ModelAttribute -> Deptime 
   *  @return value of ModelAttribute Deptime */
  public java.sql.Time getDeptime() {
    return super.getAttributeValueAsTime("Deptime");
  }
 
  /** setter for ModelAttribute -> Deptime 
   *  @param value new value for ModelAttribute Deptime */
  public void setDeptime(java.sql.Time value) {
    super.setAttributeValueAsTime("Deptime", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Distance
   * **************************************************************************/
  /** getter for ModelAttribute -> Distance 
   *  @return value of ModelAttribute Distance */
  public java.math.BigDecimal getDistance() {
    return super.getAttributeValueAsBigDecimal("Distance");
  }
 
  /** setter for ModelAttribute -> Distance 
   *  @param value new value for ModelAttribute Distance */
  public void setDistance(java.math.BigDecimal value) {
    super.setAttributeValueAsBigDecimal("Distance", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Distid
   * **************************************************************************/
  /** getter for ModelAttribute -> Distid 
   *  @return value of ModelAttribute Distid */
  public java.lang.String getDistid() {
    return super.getAttributeValueAsString("Distid");
  }
 
  /** setter for ModelAttribute -> Distid 
   *  @param value new value for ModelAttribute Distid */
  public void setDistid(java.lang.String value) {
    super.setAttributeValueAsString("Distid", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Fldate
   * **************************************************************************/
  /** getter for ModelAttribute -> Fldate 
   *  @return value of ModelAttribute Fldate */
  public java.sql.Date getFldate() {
    return super.getAttributeValueAsDate("Fldate");
  }
 
  /** setter for ModelAttribute -> Fldate 
   *  @param value new value for ModelAttribute Fldate */
  public void setFldate(java.sql.Date value) {
    super.setAttributeValueAsDate("Fldate", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Fltime
   * **************************************************************************/
  /** getter for ModelAttribute -> Fltime 
   *  @return value of ModelAttribute Fltime */
  public int getFltime() {
    return super.getAttributeValueAsInt("Fltime");
  }
 
  /** setter for ModelAttribute -> Fltime 
   *  @param value new value for ModelAttribute Fltime */
  public void setFltime(int value) {
    super.setAttributeValueAsInt("Fltime", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Fltype
   * **************************************************************************/
  /** getter for ModelAttribute -> Fltype 
   *  @return value of ModelAttribute Fltype */
  public boolean getFltype() {
    return super.getAttributeValueAsBoolean("Fltype");
  }
 
  /** setter for ModelAttribute -> Fltype 
   *  @param value new value for ModelAttribute Fltype */
  public void setFltype(boolean value) {
    super.setAttributeValueAsBoolean("Fltype", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Paymentsum
   * **************************************************************************/
  /** getter for ModelAttribute -> Paymentsum 
   *  @return value of ModelAttribute Paymentsum */
  public java.math.BigDecimal getPaymentsum() {
    return super.getAttributeValueAsBigDecimal("Paymentsum");
  }
 
  /** setter for ModelAttribute -> Paymentsum 
   *  @param value new value for ModelAttribute Paymentsum */
  public void setPaymentsum(java.math.BigDecimal value) {
    super.setAttributeValueAsBigDecimal("Paymentsum", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Planetype
   * **************************************************************************/
  /** getter for ModelAttribute -> Planetype 
   *  @return value of ModelAttribute Planetype */
  public java.lang.String getPlanetype() {
    return super.getAttributeValueAsString("Planetype");
  }
 
  /** setter for ModelAttribute -> Planetype 
   *  @param value new value for ModelAttribute Planetype */
  public void setPlanetype(java.lang.String value) {
    super.setAttributeValueAsString("Planetype", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Price
   * **************************************************************************/
  /** getter for ModelAttribute -> Price 
   *  @return value of ModelAttribute Price */
  public java.math.BigDecimal getPrice() {
    return super.getAttributeValueAsBigDecimal("Price");
  }
 
  /** setter for ModelAttribute -> Price 
   *  @param value new value for ModelAttribute Price */
  public void setPrice(java.math.BigDecimal value) {
    super.setAttributeValueAsBigDecimal("Price", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Seatsmax
   * **************************************************************************/
  /** getter for ModelAttribute -> Seatsmax 
   *  @return value of ModelAttribute Seatsmax */
  public int getSeatsmax() {
    return super.getAttributeValueAsInt("Seatsmax");
  }
 
  /** setter for ModelAttribute -> Seatsmax 
   *  @param value new value for ModelAttribute Seatsmax */
  public void setSeatsmax(int value) {
    super.setAttributeValueAsInt("Seatsmax", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Seatsocc
   * **************************************************************************/
  /** getter for ModelAttribute -> Seatsocc 
   *  @return value of ModelAttribute Seatsocc */
  public int getSeatsocc() {
    return super.getAttributeValueAsInt("Seatsocc");
  }
 
  /** setter for ModelAttribute -> Seatsocc 
   *  @param value new value for ModelAttribute Seatsocc */
  public void setSeatsocc(int value) {
    super.setAttributeValueAsInt("Seatsocc", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
  public static class Bapisfdeta_List extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCList implements java.io.Serializable , java.util.List   {

    protected Class _associatedModelClazz() { return _associatedModelClazz; }
	protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){ return DEFAULT_SCOPE; }

    private static final long serialVersionUID = 60422512L ;
    
    public Bapisfdeta_List () {
      super(createElementProperties(it.wiit.model.Bapisfdeta.class, new it.wiit.model.Bapisfdeta() ));
    }

    /** Constructor for specific scope
     *  use this Constructor, if this List should belong to a model instance
     *  of a different scope than the default model scope.
     *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     */
    public Bapisfdeta_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
      this();
      maintainInScope(scope);
    }
  
    /** Constructor for specific modelInstanceId
     *  use this Constructor, if this List should belong to a different model instance
     *  than the default model instance.
     *  The default modelInstance has no id (null). 
     * 
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Bapisfdeta_List (String modelInstanceId) {
      this();
      maintainInInstanceId(modelInstanceId);
    }
  
    /** Constructor for specific scope and modelInstanceId
     *  use this Constructor, of this List should belong to a differnt scope
     *  and a different model instance than the default model scope and model instance.
     *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
     *  The default modelInstance has no id (null). 
     * 
     *  @param scope the WDModelScopeType representing in which scope this List is to be maintained
     *  @param modelInstanceId a String representing the model instance this List should belong to
     */
    public Bapisfdeta_List (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
      this();
      maintainInScopeAndInstanceId(scope, modelInstanceId);
    }
  
    /** Constructor for specific model Instance
     *  use this Constructor, if this instance should belong to a specific instance
     *  of the model it.wiit.model.Flights 
     *  passed as a parameter to this constructor.
     * 
     *  @param modelInstance the DynamicRFCModel referring to the modelInstance
     */
    public Bapisfdeta_List ( it.wiit.model.Flights modelInstance) {
      this();
      maintainInModel(modelInstance);
    }

    public it.wiit.model.Bapisfdeta[] toArrayBapisfdeta() {
      return (it.wiit.model.Bapisfdeta[])super.toArray(new it.wiit.model.Bapisfdeta[] {});
    }
    
    public int lastIndexOfBapisfdeta(it.wiit.model.Bapisfdeta item) {
      return super.lastIndexOf(item);
    }
    
    public int indexOfBapisfdeta(it.wiit.model.Bapisfdeta item) {
      return super.indexOf(item);
    }
    
    public Bapisfdeta_List subListBapisfdeta(int fromIndex, int toIndex) {
      return (Bapisfdeta_List)super.subList(fromIndex, toIndex);
    }
    
    public void addAllBapisfdeta(Bapisfdeta_List item) {
      super.addAll(item);
    }
    
    public void addBapisfdeta(it.wiit.model.Bapisfdeta item) {
      super.add(item);
    }
    
    public boolean removeBapisfdeta(it.wiit.model.Bapisfdeta item) {
      return super.remove(item);
    }
    
    public it.wiit.model.Bapisfdeta getBapisfdeta(int index) {
      return (it.wiit.model.Bapisfdeta)super.get(index);
    }
    
    public boolean containsAllBapisfdeta(Bapisfdeta_List item) {
      return super.containsAll(item);
    }
    
    public void addBapisfdeta(int index, it.wiit.model.Bapisfdeta item) {
      super.add(index, item);
    }
    
    public boolean containsBapisfdeta(it.wiit.model.Bapisfdeta item) {
      return super.contains(item);
    }
        
    public void addAllBapisfdeta(int index, Bapisfdeta_List item) {
      super.addAll(index, item);
    }
    
    public it.wiit.model.Bapisfdeta setBapisfdeta(int index, it.wiit.model.Bapisfdeta item) {
      return (it.wiit.model.Bapisfdeta)super.set(index, item);
    }
    
    public it.wiit.model.Bapisfdeta removeBapisfdeta(int index) {
      return (it.wiit.model.Bapisfdeta)super.remove(index);
    }
  }
  
}
