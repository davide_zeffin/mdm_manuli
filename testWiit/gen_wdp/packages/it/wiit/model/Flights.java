// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.wiit.model;

//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Flights" RFC Adapter Model implementation
 * Copyright:    Copyright (c) 2001 - 2003. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Flights extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel
{

  /**
   * Log version during <clinit>
   */
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final Object[][] WD_STATIC_TYPE_INFO = {
    { "it.wiit.model.Bapisflist", "BAPISFLIST",
      "structure", 
      "it.wiit.model.types.Bapisflist" },
    { "it.wiit.model.Bapisfdeta", "BAPISFDETA",
      "structure", 
      "it.wiit.model.types.Bapisfdeta" },
    { "it.wiit.model.Bapi_Sflight_Getlist_Input", "BAPI_SFLIGHT_GETLIST",
      "input", new String[][] {
        { "Afternoon", "it.wiit.model.types.Char1" },
        { "Airlinecarrier", "it.wiit.model.types.S_Carr_Id" },
        { "Fromcity", "it.wiit.model.types.S_From_Cit" },
        { "Fromcountrykey", "it.wiit.model.types.Land1" },
        { "Maxread", "it.wiit.model.types.Sydbcnt" },
        { "Tocity", "it.wiit.model.types.S_To_City" },
        { "Tocountrykey", "it.wiit.model.types.Land1" },
      } },
    { "it.wiit.model.Bapi_Sflight_Getdetail_Output", "BAPI_SFLIGHT_GETDETAIL",
      "output", new String[][] {
      } },
    { "it.wiit.model.Bapi_Sflight_Getlist_Output", "BAPI_SFLIGHT_GETLIST",
      "output", new String[][] {
      } },
    { "it.wiit.model.Bapiret2", "BAPIRET2",
      "structure", 
      "it.wiit.model.types.Bapiret2" },
    { "it.wiit.model.Bapi_Sflight_Getdetail_Input", "BAPI_SFLIGHT_GETDETAIL",
      "input", new String[][] {
        { "Airlinecarrier", "it.wiit.model.types.S_Carr_Id" },
        { "Connectionnumber", "it.wiit.model.types.S_Conn_Id" },
        { "Dateofflight", "it.wiit.model.types.S_Date" },
      } },
  };
  
  private static final com.sap.aii.proxy.framework.core.BaseProxyDescriptor staticDescriptor = 
    com.sap.aii.proxy.framework.core.BaseProxyDescriptorFactory.createNewBaseProxyDescriptor(
      "urn:sap-com:document:sap:rfc:functions:Flights.PortType",
      new java.lang.Object[][][] {
        {
          { "BAPI_SFLIGHT_GETLIST",
            "bapi_Sflight_Getlist",
            "BAPI_SFLIGHT_GETLIST" },
          { "urn:sap-com:document:sap:rfc:functions:BAPI_SFLIGHT_GETLIST.Input",
            "it.wiit.model.Bapi_Sflight_Getlist_Input" },
          { "urn:sap-com:document:sap:rfc:functions:BAPI_SFLIGHT_GETLIST.Output",
            "it.wiit.model.Bapi_Sflight_Getlist_Output" },
          { "urn:sap-com:document:sap:rfc:functions:WDDynamicRFC_Fault.Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault" }
        },
        {
          { "BAPI_SFLIGHT_GETDETAIL",
            "bapi_Sflight_Getdetail",
            "BAPI_SFLIGHT_GETDETAIL" },
          { "urn:sap-com:document:sap:rfc:functions:BAPI_SFLIGHT_GETDETAIL.Input",
            "it.wiit.model.Bapi_Sflight_Getdetail_Input" },
          { "urn:sap-com:document:sap:rfc:functions:BAPI_SFLIGHT_GETDETAIL.Output",
            "it.wiit.model.Bapi_Sflight_Getdetail_Output" },
          { "urn:sap-com:document:sap:rfc:functions:WDDynamicRFC_Fault.Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception",
            "com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault" }
        },
      },
      com.sap.aii.proxy.framework.core.FactoryConstants.CONNECTION_TYPE_JCO,
      it.wiit.model.Flights.class);

  public static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_MODELSCOPE =
        com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType.APPLICATION_SCOPE;
  public static final String DEFAULT_SYSTEMNAME = "WD_MODELDATA_CORSO";
  public static final String DEFAULT_RFCMETADATA_SYSTEMNAME = "WD_RFC_METADATA_CORSO";
  public static final String LOGICAL_DICTIONARY_NAME = "it.wiit.model.types.Flights";
  public static final String LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME = "WD_RFC_METADATA_CORSO";
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057227900576L) ;
  static{
    com.sap.tc.webdynpro.progmodel.model.api.WDModelFactory.registerDefaultScopeForModel(it.wiit.model.Flights.class, DEFAULT_MODELSCOPE);
  }

  /**
   * Class local cache for model metadata
   */
  private static final com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdModelMetadataCache =
  	new com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache(Flights.class);

  /**
   * Called by superclass to retrieve the model's metadata cache.
   */  
  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetInstanceMetadataCache() {
    return wdModelMetadataCache;
  }

  /**
   * Called to retrieve the model's metadata cache.
   */  
  static com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetStaticMetadataCache() {
    return wdModelMetadataCache;
  }
  
  /**
   * Constructor
   */
  public Flights() {
    this(DEFAULT_MODELSCOPE, null);
  }

  /**
   * Constructor
   * @param instanceId
   */
  public Flights( String instanceId) {
    this(DEFAULT_MODELSCOPE, instanceId);
  }

  /**
   * Constructor
   * @param scope
   */
  public Flights( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this(scope, null);
  }

  /**
   * Constructor
   * @param scope
   * @param instanceId
   */
  public Flights( com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String instanceId) {
    super(WD_STATIC_TYPE_INFO, staticDescriptor, staticGenerationInfo, DEFAULT_SYSTEMNAME, LOGICAL_DICTIONARY_NAME, scope, instanceId, DEFAULT_RFCMETADATA_SYSTEMNAME, LOGICAL_DICTIONARY_DEFAULT_SYSTEM_NAME);
  }

  public it.wiit.model.Bapi_Sflight_Getlist_Output bapi_Sflight_Getlist(it.wiit.model.Bapi_Sflight_Getlist_Input input)
    throws com.sap.aii.proxy.framework.core.SystemFaultException,
           com.sap.aii.proxy.framework.core.ApplicationFaultException,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException {
    com.sap.aii.proxy.framework.core.BaseType result = null;
    prepareExecute();
    try {
      result = send$(input, "urn:sap-com:document:sap:rfc:functions", "Flights.PortType", "BAPI_SFLIGHT_GETLIST", new it.wiit.model.Bapi_Sflight_Getlist_Output());
    } catch (com.sap.aii.proxy.framework.core.ApplicationFaultException e){
      if (e instanceof com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)
      	{ throw (com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)e;}
      throw createExceptionWrongExceptionType$(e);
    }
    if (result == null || (result instanceof it.wiit.model.Bapi_Sflight_Getlist_Output)) {
      return (it.wiit.model.Bapi_Sflight_Getlist_Output) result;
    } else { 
      throw createExceptionWrongType$(result);
    }
  }

  public it.wiit.model.Bapi_Sflight_Getdetail_Output bapi_Sflight_Getdetail(it.wiit.model.Bapi_Sflight_Getdetail_Input input)
    throws com.sap.aii.proxy.framework.core.SystemFaultException,
           com.sap.aii.proxy.framework.core.ApplicationFaultException,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException {
    com.sap.aii.proxy.framework.core.BaseType result = null;
    prepareExecute();
    try {
      result = send$(input, "urn:sap-com:document:sap:rfc:functions", "Flights.PortType", "BAPI_SFLIGHT_GETDETAIL", new it.wiit.model.Bapi_Sflight_Getdetail_Output());
    } catch (com.sap.aii.proxy.framework.core.ApplicationFaultException e){
      if (e instanceof com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)
      	{ throw (com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception)e;}
      throw createExceptionWrongExceptionType$(e);
    }
    if (result == null || (result instanceof it.wiit.model.Bapi_Sflight_Getdetail_Output)) {
      return (it.wiit.model.Bapi_Sflight_Getdetail_Output) result;
    } else { 
      throw createExceptionWrongType$(result);
    }
  }

}
