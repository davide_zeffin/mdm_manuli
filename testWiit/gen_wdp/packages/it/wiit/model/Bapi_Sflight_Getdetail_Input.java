// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.wiit.model;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR java.lang.String).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.lang.String;
//@@end

//@@begin documentation
/**
 * Title:        Web Dynpro Version 6.30
 * Description:  "Bapi_Sflight_Getdetail_Input" RFC Adapter ModelClass implementation
 * Copyright:    Copyright (c) 2001 - 2002. All rights reserved.
 * Company:      SAP AG
 * @author       File created by Web Dynpro code generator
 */
//@@end

public class Bapi_Sflight_Getdetail_Input extends com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModelClassExecutable implements java.io.Serializable
{
  /** Logging location. */
  private static final com.sap.tc.logging.Location logger = com.sap.tc.logging.Location.getLocation(Bapi_Sflight_Getdetail_Input.class);
  static {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  private static final long serialVersionUID = 858420244L ;
  private static final com.sap.aii.proxy.framework.core.GenerationInfo staticGenerationInfo = new com.sap.aii.proxy.framework.core.GenerationInfo("2.0", 1057090423493L) ;

  private static final Class _associatedModelClazz = it.wiit.model.Flights.class;
  private static final com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType DEFAULT_SCOPE =
    it.wiit.model.Flights.DEFAULT_MODELSCOPE;

  /**  Simple Constructor  */
  public Bapi_Sflight_Getdetail_Input () {
    super( descriptor( com.sap.tc.webdynpro.modelimpl.dynamicrfc.RFCMetadataRepository.getSingleton(it.wiit.model.Flights.DEFAULT_RFCMETADATA_SYSTEMNAME),
                       it.wiit.model.Flights.wdGetStaticMetadataCache().getBaseTypeDescriptorMap(),
                       Bapi_Sflight_Getdetail_Input.class,
                       "BAPI_SFLIGHT_GETDETAIL",
                       PROXYTYPE_INPUT, 
                       "Bapi_Sflight_Getdetail_Input", 
                       "it.wiit.model" ), 
            staticGenerationInfo);
  }

  /** Constructor for specific scope
   *  use this Constructor, if this instance should belong to a model instance
   *  of a different scope than the default model scope.
   *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   */
  public Bapi_Sflight_Getdetail_Input (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope) {
    this();
    maintainInScope(scope);
  }

  /** Constructor for specific modelInstanceId
   *  use this Constructor, if this instance should belong to a different model instance
   *  than the default model instance.
   *  The default modelInstance has no id (null). 
   * 
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Bapi_Sflight_Getdetail_Input (String modelInstanceId) {
    this();
    maintainInInstanceId(modelInstanceId);
  }

  /** Constructor for specific scope and modelInstanceId
   *  use this Constructor, of this instance should belong to a differnt scope
   *  and a different model instance than the default model scope and model instance.
   *  The default model scope can be found in it.wiit.model.Flights.DEFAULT_MODELSCOPE
   *  The default modelInstance has no id (null). 
   * 
   *  @param scope the WDModelScopeType representing in which scope this instance is to be maintained
   *  @param modelInstanceId a String representing the model instance this modelClass instance should belong to
   */
  public Bapi_Sflight_Getdetail_Input (com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType scope, String modelInstanceId) {
    this();
    maintainInScopeAndInstanceId(scope, modelInstanceId);
  }

  /** Constructor for specific model Instance
   *  use this Constructor, if this instance should belong to a specific instance
   *  of the model it.wiit.model.Flights 
   *  passed as a parameter to this constructor.
   * 
   *  @param modelInstance the DynamicRFCModel referring to the modelInstance
   */
  public Bapi_Sflight_Getdetail_Input ( it.wiit.model.Flights modelInstance) {
    this();
    maintainInModel(modelInstance);
  }

  /**
   * Returns the modelInstance associated with this ModelClass 
   */
  public Flights modelInstance() {
    return (Flights)associatedModel();
  }

  /**
   * Returns the result from the last RFC call 
   */
  public it.wiit.model.Bapi_Sflight_Getdetail_Output getOutput() {
    return (it.wiit.model.Bapi_Sflight_Getdetail_Output)_outputAdapter();
  }

  /**
   * Hook method that executes the RFC call to the backend by calling the 
   * corresponding send method in the model class.</p>
   *
   * The result can be retrieved using the method "getOutput()", giving access to the
   * default 1:1 relation role "Output" which all executable ModelClasses do implement.</p>
   */
  protected final void doExecute() 
    throws com.sap.aii.proxy.framework.core.SystemFaultException,
           com.sap.aii.proxy.framework.core.ApplicationFaultException,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFC_Fault_Exception,
           com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException {
    _outputAdapter( modelInstance().bapi_Sflight_Getdetail(this));
  }

  protected com.sap.tc.webdynpro.modelimpl.dynamicrfc.DynamicRFCModel.MetadataCache wdGetMetadataCache() {
    return it.wiit.model.Flights.wdGetStaticMetadataCache();	
  }
  protected Class _associatedModelClazz() {
    return _associatedModelClazz;
  }
  protected com.sap.tc.webdynpro.progmodel.model.api.WDModelScopeType _defaultScope(){
    return DEFAULT_SCOPE;
  }

  /* ***************************************************************************
   *  ModelAttribute -> Airlinecarrier
   * **************************************************************************/
  /** getter for ModelAttribute -> Airlinecarrier 
   *  @return value of ModelAttribute Airlinecarrier */
  public java.lang.String getAirlinecarrier() {
    return super.getAttributeValueAsString("Airlinecarrier");
  }
 
  /** setter for ModelAttribute -> Airlinecarrier 
   *  @param value new value for ModelAttribute Airlinecarrier */
  public void setAirlinecarrier(java.lang.String value) {
    super.setAttributeValueAsString("Airlinecarrier", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Connectionnumber
   * **************************************************************************/
  /** getter for ModelAttribute -> Connectionnumber 
   *  @return value of ModelAttribute Connectionnumber */
  public java.lang.String getConnectionnumber() {
    return super.getAttributeValueAsString("Connectionnumber");
  }
 
  /** setter for ModelAttribute -> Connectionnumber 
   *  @param value new value for ModelAttribute Connectionnumber */
  public void setConnectionnumber(java.lang.String value) {
    super.setAttributeValueAsString("Connectionnumber", value);
  }
   
  /* ***************************************************************************
   *  ModelAttribute -> Dateofflight
   * **************************************************************************/
  /** getter for ModelAttribute -> Dateofflight 
   *  @return value of ModelAttribute Dateofflight */
  public java.sql.Date getDateofflight() {
    return super.getAttributeValueAsDate("Dateofflight");
  }
 
  /** setter for ModelAttribute -> Dateofflight 
   *  @param value new value for ModelAttribute Dateofflight */
  public void setDateofflight(java.sql.Date value) {
    super.setAttributeValueAsDate("Dateofflight", value);
  }
   
  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
  
}
