// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package it.wiit.comp;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateFirstView).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import com.sap.tc.webdynpro.modelimpl.dynamicrfc.WDDynamicRFCExecuteException;

import it.wiit.comp.wdp.IPrivateFirstView;
import it.wiit.model.Bapi_Sflight_Getlist_Input;
import it.wiit.model.Bapisflist;
//@@end

//@@begin documentation
//@@end

public class FirstView
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(FirstView.class);

  static 
  {
    //@@begin id
		String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see it.wiit.comp.wdp.IPrivateFirstView for more details
   */
  private final IPrivateFirstView wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see it.wiit.comp.wdp.IPrivateFirstView.IContextNode for more details.
   */
  private final IPrivateFirstView.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public FirstView(IPrivateFirstView wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
	/** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
	Bapi_Sflight_Getlist_Input input = new Bapi_Sflight_Getlist_Input();
	wdContext.nodeBapi_Sflight_Getlist_Input().bind(input);
    //@@end
  }

  //@@begin javadoc:wdDoExit()
	/** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
	/**
	 * Hook method called to modify a view just before rendering.
	 * This method conceptually belongs to the view itself, not to the
	 * controller (cf. MVC pattern).
	 * It is made static to discourage a way of programming that
	 * routinely stores references to UI elements in instance fields
	 * for access by the view controller's event handlers, and so on.
	 * The Web Dynpro programming model recommends that UI elements can
	 * only be accessed by code executed within the call to this hook method.
	 *
	 * @param wdThis Generated private interface of the view's controller, as
	 *        provided by Web Dynpro. Provides access to the view controller's
	 *        outgoing controller usages, etc.
	 * @param wdContext Generated interface of the view's context, as provided
	 *        by Web Dynpro. Provides access to the view's data.
	 * @param view The view's generic API, as provided by Web Dynpro.
	 *        Provides access to UI elements.
	 * @param firstTime Indicates whether the hook is called for the first time
	 *        during the lifetime of the view.
	 */
  //@@end
  public static void wdDoModifyView(IPrivateFirstView wdThis, IPrivateFirstView.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView
    //@@end
  }

  //@@begin javadoc:onPlugfromSecondView(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onPlugfromSecondView(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onPlugfromSecondView(ServerEvent)
    //@@end
  }

  //@@begin javadoc:onActiongoSecondView(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActiongoSecondView(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActiongoSecondView(ServerEvent)
	try {
		wdContext.nodeBapi_Sflight_Getlist_Input().currentBapi_Sflight_Getlist_InputElement().modelObject().execute();
		wdContext.nodeOutput().invalidate();
		wdThis.wdFirePlugToSecondView();
	} catch (WDDynamicRFCExecuteException e) {
	wdComponentAPI.getMessageManager().reportException(
		"Error in executing Flight Search." + e.getMessage(),true);

	}


	/*
	 * The following code section can be used for any Java code that is 
	 * not to be visible to other controllers/views or that contains constructs
	 * currently not supported directly by Web Dynpro (such as inner classes or
	 * member variables etc.). </p>
	 *
	 * Note: The content of this section is in no way managed/controlled
	 * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
	 */
	//@@begin others
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
}
