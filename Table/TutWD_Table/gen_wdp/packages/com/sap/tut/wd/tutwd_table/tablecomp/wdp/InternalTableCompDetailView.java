// ---------------------------------------------------------------------------
// This file has been generated by the Web Dynpro Code Generator
// DON'T MODIFY!!! CHANGES WILL BE LOST WHENEVER THE FILE GETS GENERATED AGAIN
// ---------------------------------------------------------------------------
package com.sap.tut.wd.tutwd_table.tablecomp.wdp;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.sap.tc.logging.Location;
import com.sap.tc.webdynpro.progmodel.api.*;
import com.sap.tc.webdynpro.progmodel.gci.*;
import com.sap.tc.webdynpro.progmodel.context.*;
import com.sap.tc.webdynpro.progmodel.gci.*;
import com.sap.tc.webdynpro.services.exceptions.WDRuntimeException;

public class InternalTableCompDetailView
  implements IPrivateTableCompDetailView, com.sap.tc.webdynpro.progmodel.gci.IGCIViewDelegate
{

  /**
   * Location for this controller.
   */
  private static final Location logger = Location.getLocation(InternalTableCompDetailView.class);

  /**
   * Framework implementation of Controller that delegates to this.
   */
  private final com.sap.tc.webdynpro.progmodel.gci.IGCIView wdAlterEgo;

  /**
   * Delegate that implements application defined logic
   */
  private final com.sap.tut.wd.tutwd_table.tablecomp.TableCompDetailView delegate;


  // ---- Context --------------------------------------------------------------

  IGCINodeInfo infoProducts;
  IGCINodeInfo infoContext;

  {

    infoProducts = GCIContext.createMappedNode("Products", null, true, true, true, ".TableComp.Products", true, true, "ddic:com.sap.tut.wd.tutwd_table.Product",
      new IGCIAttributeInfo[] {
        GCIContext.createMappedAttribute("ORDER_NUMBER", "ORDER_NUMBER", false),
        GCIContext.createMappedAttribute("QUANTITY", "QUANTITY", false),
        GCIContext.createMappedAttribute("SPECIAL_FEATURES", "SPECIAL_FEATURES", false),
        GCIContext.createMappedAttribute("TEXTILE_CATEGORY", "TEXTILE_CATEGORY", false),
        GCIContext.createMappedAttribute("ARTICLE", "ARTICLE", false),
        GCIContext.createMappedAttribute("COLOR", "COLOR", false),
        GCIContext.createMappedAttribute("PRICE", "PRICE", false),
        GCIContext.createMappedAttribute("SIZE", "SIZE", false),
      },
      (IGCINodeInfo[])null // no child nodes
    );

    infoContext = GCIContext.createNode("Context", null, true, true, false, true, false, true, null,
      (IGCIAttributeInfo[])null, // no attributes
      new IGCINodeInfo[] {
        infoProducts,
      }
    );

  }
  
  private IContextNode contextNode;

  private void wdInitContextNode() {
    contextNode = new IContextNode(this, infoContext, (Node)null);
  }

  public IWDNode wdGetRootNode() {
    return contextNode;
  }

  public IContextNode wdGetContext() {
    return contextNode;
  }


  // ---- Actions --------------------------------------------------------------

  /**
   * Create a new action for this controller. A unique name for the action is generated automatially.
   * @param eventHandler is the action's eventhandler with proper signature
   * @param text is the text displayed in the UI element triggering this action
   */
  public IWDAction wdCreateAction(WDActionEventHandler eventHandler, String text) {
    return wdAlterEgo.createAction(null, eventHandler, text, null);
  }

  /**
   * Create a new action with the given name for this controller
   * @param eventHandler is the action's eventhandler with proper signature
   * @param name is the action's name
   * @param text is the text displayed in the UI element triggering this action
   */
  public IWDAction wdCreateNamedAction(WDActionEventHandler eventHandler, String name, String text) {
    return wdAlterEgo.createAction(name, eventHandler, text, null);
  }

  // ---- Controller part ------------------------------------------------------

  /**
   * Creates a new instance of this controller.
   */
  public InternalTableCompDetailView(com.sap.tc.webdynpro.progmodel.gci.IGCIView alterEgo) {
    this.wdAlterEgo = alterEgo;
    wdInitContextNode();
    this.delegate = new com.sap.tut.wd.tutwd_table.tablecomp.TableCompDetailView((IPrivateTableCompDetailView) this);
  }

  /**
   * Returns the public API for this controller instance.
   */
  public com.sap.tc.webdynpro.progmodel.api.IWDViewController wdGetAPI() {
    return (com.sap.tc.webdynpro.progmodel.api.IWDViewController) wdAlterEgo;
  }

  /**
   * Hook method called to initialize view controller.
   */
  public void wdDoInit() {
    logger.pathT("entering: wdDoInit");
    delegate.wdDoInit();
    logger.pathT("exiting: wdDoInit"); 
  }

  /**
   * Hook method called to clean up view controller.
   */
  public void wdDoExit() {
    logger.pathT("entering: wdDoExit");
    delegate.wdDoExit();
    logger.pathT("exiting: wdDoExit"); 
  }

  public Object wdInvokeEventHandler(String handlerName, IWDCustomEvent event)
    throws NoSuchMethodException {
    
    logger.pathT("entering: wdInvokeEventHandler", new Object[] { handlerName } );
    try {
      throw new NoSuchMethodException("Eventhandler " + handlerName + " not found for event " + event.getName());
    } finally {
      logger.pathT("exiting: wdInvokeEventHandler"); 
    }
  }

  /** outgoing controller usage */
  public com.sap.tut.wd.tutwd_table.tablecomp.wdp.IPublicTableComp wdGetTableCompController() {
    return
      (com.sap.tut.wd.tutwd_table.tablecomp.wdp.IPublicTableComp)
        wdAlterEgo
          .getComponentGCI()
            .getPublicInterface("TableComp");
  }


  // ---- UI Tree ---------------------------------------------------------

  // create UI tree
  public com.sap.tc.webdynpro.progmodel.api.IWDViewElement wdCreateUITree()
  {

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDTransparentContainer _RootUIElementContainer
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDTransparentContainer)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDTransparentContainer.class,
          "RootUIElementContainer");  
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixLayout _RootUIElementContainer__Layout
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixLayout)
        _RootUIElementContainer.createLayout(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixLayout.class);
    _RootUIElementContainer__Layout.setStretchedHorizontally(false);
    _RootUIElementContainer.addChild(wdCreateUITreeForGroup());
    return (_RootUIElementContainer);
  }

  // create UI tree for Group
  private com.sap.tc.webdynpro.progmodel.api.IWDUIElement wdCreateUITreeForGroup()
  {

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDGroup _Group
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDGroup)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDGroup.class,
          "Group");  
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData _Group__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData)
        _Group.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData.class);
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixLayout _Group__Layout
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixLayout)
        _Group.createLayout(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixLayout.class);
    _Group__Layout.setStretchedHorizontally(false);
    _Group__Layout.setStretchedVertically(false);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel _ARTICLE_label
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel.class,
          "ARTICLE_label");  
    _ARTICLE_label.setLabelFor("ARTICLE");
    _ARTICLE_label.setText(wdAlterEgo.getLocalizedText("view.TableCompDetailView.Group.ARTICLE_label.text"));
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData _ARTICLE_label__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData)
        _ARTICLE_label.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData.class);
    _Group.addChild(_ARTICLE_label);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField _ARTICLE
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField.class,
          "ARTICLE");  
    _ARTICLE.setReadOnly(true);
    _ARTICLE.bindValue("Products.ARTICLE");
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData _ARTICLE__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData)
        _ARTICLE.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData.class);
    _Group.addChild(_ARTICLE);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel _COLOR_label
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel.class,
          "COLOR_label");  
    _COLOR_label.setLabelFor("COLOR");
    _COLOR_label.setText(wdAlterEgo.getLocalizedText("view.TableCompDetailView.Group.COLOR_label.text"));
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData _COLOR_label__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData)
        _COLOR_label.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData.class);
    _Group.addChild(_COLOR_label);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField _COLOR
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField.class,
          "COLOR");  
    _COLOR.setReadOnly(true);
    _COLOR.bindValue("Products.COLOR");
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData _COLOR__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData)
        _COLOR.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData.class);
    _Group.addChild(_COLOR);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel _ORDER_NUMBER_label
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel.class,
          "ORDER_NUMBER_label");  
    _ORDER_NUMBER_label.setLabelFor("ORDER_NUMBER");
    _ORDER_NUMBER_label.setText(wdAlterEgo.getLocalizedText("view.TableCompDetailView.Group.ORDER_NUMBER_label.text"));
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData _ORDER_NUMBER_label__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData)
        _ORDER_NUMBER_label.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData.class);
    _Group.addChild(_ORDER_NUMBER_label);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField _ORDER_NUMBER
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField.class,
          "ORDER_NUMBER");  
    _ORDER_NUMBER.setReadOnly(true);
    _ORDER_NUMBER.bindValue("Products.ORDER_NUMBER");
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData _ORDER_NUMBER__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData)
        _ORDER_NUMBER.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData.class);
    _Group.addChild(_ORDER_NUMBER);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel _PRICE_label
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel.class,
          "PRICE_label");  
    _PRICE_label.setLabelFor("PRICE");
    _PRICE_label.setText(wdAlterEgo.getLocalizedText("view.TableCompDetailView.Group.PRICE_label.text"));
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData _PRICE_label__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData)
        _PRICE_label.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData.class);
    _Group.addChild(_PRICE_label);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField _PRICE
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField.class,
          "PRICE");  
    _PRICE.setReadOnly(true);
    _PRICE.bindValue("Products.PRICE");
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData _PRICE__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData)
        _PRICE.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData.class);
    _Group.addChild(_PRICE);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel _SIZE_label
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel.class,
          "SIZE_label");  
    _SIZE_label.setLabelFor("SIZE");
    _SIZE_label.setText(wdAlterEgo.getLocalizedText("view.TableCompDetailView.Group.SIZE_label.text"));
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData _SIZE_label__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData)
        _SIZE_label.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData.class);
    _Group.addChild(_SIZE_label);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField _SIZE
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField.class,
          "SIZE");  
    _SIZE.setReadOnly(true);
    _SIZE.bindValue("Products.SIZE");
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData _SIZE__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData)
        _SIZE.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData.class);
    _Group.addChild(_SIZE);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel _SPECIAL_FEATURES_label
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel.class,
          "SPECIAL_FEATURES_label");  
    _SPECIAL_FEATURES_label.setLabelFor("SPECIAL_FEATURES");
    _SPECIAL_FEATURES_label.setText(wdAlterEgo.getLocalizedText("view.TableCompDetailView.Group.SPECIAL_FEATURES_label.text"));
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData _SPECIAL_FEATURES_label__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData)
        _SPECIAL_FEATURES_label.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData.class);
    _Group.addChild(_SPECIAL_FEATURES_label);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField _SPECIAL_FEATURES
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField.class,
          "SPECIAL_FEATURES");  
    _SPECIAL_FEATURES.setReadOnly(true);
    _SPECIAL_FEATURES.bindValue("Products.SPECIAL_FEATURES");
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData _SPECIAL_FEATURES__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData)
        _SPECIAL_FEATURES.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData.class);
    _Group.addChild(_SPECIAL_FEATURES);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel _TEXTILE_CATEGORY_label
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDLabel.class,
          "TEXTILE_CATEGORY_label");  
    _TEXTILE_CATEGORY_label.setLabelFor("TEXTILE_CATEGORY");
    _TEXTILE_CATEGORY_label.setText(wdAlterEgo.getLocalizedText("view.TableCompDetailView.Group.TEXTILE_CATEGORY_label.text"));
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData _TEXTILE_CATEGORY_label__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData)
        _TEXTILE_CATEGORY_label.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixHeadData.class);
    _Group.addChild(_TEXTILE_CATEGORY_label);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField _TEXTILE_CATEGORY
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDInputField.class,
          "TEXTILE_CATEGORY");  
    _TEXTILE_CATEGORY.setReadOnly(true);
    _TEXTILE_CATEGORY.bindValue("Products.TEXTILE_CATEGORY");
    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData _TEXTILE_CATEGORY__LayoutData
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData)
        _TEXTILE_CATEGORY.createLayoutData(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDMatrixData.class);
    _Group.addChild(_TEXTILE_CATEGORY);

    com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDCaption _Group_Header
      = (com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDCaption)
        wdAlterEgo.createElement(com.sap.tc.webdynpro.clientserver.uielib.standard.api.IWDCaption.class,
          "Group_Header");  
    _Group_Header.setText(wdAlterEgo.getLocalizedText("view.TableCompDetailView.Group.Header.text"));
    _Group.setHeader(_Group_Header);
    return (_Group);
  }
  

  /**
   * Hook method called to modify view before rendering. Access to UI elements
   * is via the given view API only!
   * 
   * @param firstTime indicates whether the hook is called for the first time
   * during the lifetime of this view
   */
  public void wdDoModifyView(IWDView view, boolean firstTime) {
    logger.pathT("entering: doModifyView");
    com.sap.tut.wd.tutwd_table.tablecomp.TableCompDetailView.wdDoModifyView((IPrivateTableCompDetailView) this, (IPrivateTableCompDetailView.IContextNode) this.wdGetContext(), view, firstTime);
    logger.pathT("exiting: doModifyView"); 
  }


}
