import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Esempio di utilizzo dei generic e delle collection.
 * @author Andrea Medeghini
 * @version 1.0
 */
public class MyMapExample {
	public static void main(String[] args) {
		// creo la mappa
		MyMap<PersonKey, Person> personMap = new MyMap<PersonKey, Person>();

		// popolo la mappa
		try {
			personMap.put(new Person("Rossi", "Mario", 22));
			personMap.put(new Person("Rossi", "Paolo", 28));
			personMap.put(new Person("Bianchi", "Luigi", 38));
		}
		catch (MyMapException e) {
			e.printStackTrace();
		}

		// creo alcuni matcher
		MyMatcher<PersonKey, Person> myMatcher1 = new MyMatcher<PersonKey, Person>() {
			public boolean match(Person obj) {
				return Pattern.matches("Rossi", obj.getSurname());
			}
		};
		MyMatcher<PersonKey, Person> myMatcher2 = new MyMatcher<PersonKey, Person>() {
			public boolean match(Person obj) {
				return Pattern.matches("\w*(chi)+", obj.getSurname());
			}
		};
		MyMatcher<PersonKey, Person> myMatcher3 = new MyMatcher<PersonKey, Person>() {
			public boolean match(Person obj) {
				return obj.getAge() < 30;
			}
		};
		MyMatcher<PersonKey, Person> myMatcher4 = new MyMatcher<PersonKey, Person>() {
			public boolean match(Person obj) {
				return obj.getAge() > 20;
			}
		};

		// creo alcuni extractor
		MyExtractor<PersonKey, Person, String> myExtractor4 = new MyExtractor<PersonKey, Person, String>() {
			public String extract(Person obj) {
				return obj.getName();
			}
		};
		MyExtractor<PersonKey, Person, String> myExtractor5 = new MyExtractor<PersonKey, Person, String>() {
			public String extract(Person obj) {
				return obj.getSurname();
			}
		};
		MyExtractor<PersonKey, Person, Integer> myExtractor6 = new MyExtractor<PersonKey, Person, Integer>() {
			public Integer extract(Person obj) {
				return obj.getAge();
			}
		};
		MyExtractor<PersonKey, Person, PersonKey> myExtractor7 = new MyExtractor<PersonKey, Person, PersonKey>() {
			public PersonKey extract(Person obj) {
				return obj.getKey();
			}
		};

		// eseguo una serie di ricerche e stampo i risultati
		print(personMap.findAll(new PersonComparator()));
		print(personMap.find(myMatcher1, new PersonComparator()));
		print(personMap.find(myMatcher2, new PersonComparator()));
		print(personMap.find(myMatcher3, new PersonComparator()));
		print(personMap.find(myMatcher4, myExtractor4, new StringComparator()));
		print(personMap.find(myMatcher4, myExtractor5, new StringComparator()));

		// se utilizzo un set come contenitore intermedio vengono eliminati i duplicati
		List<String> names = new LinkedList<String>(new HashSet<String>(personMap.find(myMatcher4, myExtractor5)));
		Collections.sort(names);
		print(names);

		// posso estrarre qualunque tipo di dato
		List<Integer> ages = new LinkedList<Integer>(personMap.find(myMatcher4, myExtractor6));
		Collections.sort(ages);
		print(ages);

		// posso estrarre qualunque tipo di dato nell'ordine desiderato
		List<PersonKey> keys = new LinkedList<PersonKey>(personMap.find(myMatcher4, myExtractor7, new PersonKeyComparator()));
		print(keys);

		// per finire qualche divagazione con le chiavi e i cloni
		if (keys.size() > 0) {
			Person p1 = personMap.findByKey(keys.get(0));
			if (p1 != null) {
				Person p2 = p1.clone();
				try {
					personMap.put(p2);
				}
				catch (MyMapException e) {
					e.printStackTrace(System.out);
				}
				print(personMap.findAll(new PersonComparator()));
				Person p3 = new Person(p1.getSurname(), p1.getName(), p1.getAge());
				try {
					personMap.put(p3);
				}
				catch (MyMapException e) {
					e.printStackTrace(System.out);
				}
				print(personMap.findAll(new PersonComparator()));
			}
		}
	}

	public static void print(Collection<?> objList) {
		for (Object obj : objList) {
			System.out.println(obj);
		}
		System.out.println();
	}

	public static interface MyKey {
		public String asString();
	}

	public static interface MyObj<K extends MyKey> {
		public K getKey();
	}

	public static interface MyMatcher<K extends MyKey, T extends MyObj<K>> {
		public boolean match(T obj);
	}

	public static interface MyExtractor<K extends MyKey, T extends MyObj<K>, E> {
		public E extract(T obj);
	}

	public static class MyMapException extends Exception {
		private static final long serialVersionUID = 1L;

		/**
		 * @param message
		 */
		public MyMapException(String message) {
			super(message);
		}
	}

	public static class MyMap<K extends MyKey, T extends MyObj<K>> {
		private Map<K, T> map = new HashMap<K, T>();

		public void put(T obj) throws MyMapException {
			if (map.containsKey(obj.getKey())) {
				throw new MyMapException("The map already contains the key " + obj.getKey());
			}
			map.put(obj.getKey(), obj);
		}

		public void remove(T obj) {
			map.remove(obj.getKey());
		}

		public void remove(K key) {
			map.remove(key);
		}

		public T findByKey(K key) {
			return map.get(key);
		}

		public Collection<T> findAll() {
			return map.values();
		}

		public Collection<T> find(MyMatcher<K, T> matcher) {
			Set<T> set = new HashSet<T>();
			for (T obj : map.values()) {
				if (matcher.match(obj)) {
					set.add(obj);
				}
			}
			return set;
		}

		public <E> Collection<E> find(MyMatcher<K, T> matcher, MyExtractor<K, T, E> extractor) {
			List<E> list = new LinkedList<E>();
			for (T obj : map.values()) {
				if (matcher.match(obj)) {
					list.add(extractor.extract(obj));
				}
			}
			return list;
		}

		public Collection<T> findAll(Comparator<T> comparator) {
			List<T> list = new LinkedList<T>(findAll());
			Collections.sort(list, comparator);
			return list;
		}

		public Collection<T> find(MyMatcher<K, T> matcher, Comparator<T> comparator) {
			List<T> list = new LinkedList<T>(find(matcher));
			Collections.sort(list, comparator);
			return list;
		}

		public <E> Collection<E> find(MyMatcher<K, T> matcher, MyExtractor<K, T, E> extractor, Comparator<E> comparator) {
			List<E> list = new LinkedList<E>(find(matcher, extractor));
			Collections.sort(list, comparator);
			return list;
		}
	}

	public static class Person implements MyObj<PersonKey>, Serializable, Cloneable {
		private static final long serialVersionUID = 1L;
		private PersonKey key;
		private String objText;
		private String surname;
		private String name;
		private int age;

		private Person(PersonKey key, String surname, String name, int age) {
			if (surname == null) {
				throw new IllegalArgumentException("surname is null");
			}
			if (name == null) {
				throw new IllegalArgumentException("name is null");
			}
			if (age < 18) {
				throw new IllegalArgumentException("age < 18");
			}
			this.surname = surname;
			this.name = name;
			this.age = age;
			this.key = key;
		}

		public Person(String surname, String name, int age) {
			this(new PersonKey(surname, name), surname, name, age);
		}

		public PersonKey getKey() {
			return key;
		}

		public String getSurname() {
			return surname;
		}

		public String getName() {
			return name;
		}

		public int getAge() {
			return age;
		}

		@Override
		public String toString() {
			// posso precalcolare il valore testuale dell'oggetto perchè i campi sono costanti
			if (objText == null) {
				StringBuilder builder = new StringBuilder();
				builder.append(surname);
				builder.append(" ");
				builder.append(name);
				builder.append(", age ");
				builder.append(age);
				objText = builder.toString();
			}
			return objText;
		}

		public Person clone() {
			return new Person(key, surname, name, age);
		}
	}

	public static class PersonKey implements MyKey, Serializable, Cloneable {
		private static final long serialVersionUID = 1L;
		private String keyText;

		public PersonKey(String surname, String name) {
			// posso precalcolare il valore testuale della chiave perchè i campi surname e name sono costanti
			keyText = surname.toLowerCase() + "." + name.toLowerCase();
		}

		public String asString() {
			return keyText + " (" + super.toString() + ")";
		}

		@Override
		public String toString() {
			return "key = " + asString();
		}
	}

	public static class PersonComparator implements Comparator<Person> {
		public int compare(Person p1, Person p2) {
			int value = 0;
			if (p1 != null && p2 != null) {
				value = p1.getSurname().compareTo(p2.getSurname());
				if (value == 0) {
					value = p1.getName().compareTo(p2.getName());
					if (value == 0) {
						value = p1.getAge() - p2.getAge();
					}
				}
			}
			return value;
		}
	}

	public static class PersonKeyComparator implements Comparator<PersonKey> {
		public int compare(PersonKey k1, PersonKey k2) {
			int value = 0;
			if (k1 != null && k2 != null) {
				value = k1.asString().compareTo(k2.asString());
			}
			return value * -1;
		}
	}

	public static class StringComparator implements Comparator<String> {
		public int compare(String s1, String s2) {
			int value = 0;
			if (s1 != null && s2 != null) {
				value = s1.compareTo(s2);
			}
			return value;
		}
	}
}
