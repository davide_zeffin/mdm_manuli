package com.sic.nw.xi.afw.modules;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.CreateException;

import com.sap.aii.af.mp.module.*;
import com.sap.aii.af.ra.ms.api.InvalidParamException;
import com.sap.aii.af.ra.ms.api.Message;
import com.sap.aii.af.ra.ms.api.Payload;
import com.sap.aii.af.service.auditlog.*;

import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.xpath.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.*;
import org.w3c.dom.traversal.NodeIterator;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

/**
 * @ejbLocal <{com.sic.nw.xi.afw.modules.DynamicQueueIDLocal}>
 * @ejbLocalHome <{com.sic.nw.xi.afw.modules.DynamicQueueIDLocalHome}>
 * @stateless 
 */
public class DynamicQueueIDBean implements SessionBean {

	public ModuleData process(ModuleContext moduleContext, ModuleData inputModuleData) throws ModuleException {

		// Basic instances
		Object obj = null;
		Message msg = null;
		AuditMessageKey amk = null;
		ModuleException mEx = null;
		try {
			obj = inputModuleData.getPrincipalData();
			msg = (Message) obj;
			amk = new AuditMessageKey(msg.getMessageId(), AuditDirection.OUTBOUND);
			mc = moduleContext;
		} catch (Exception e) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Error while creating basic instances (obj,msg,amk,mp)");
			throw mEx = new ModuleException(auditStr + "Error while creating basic instances (obj,msg,amk,mp)");
		}

		// Read module parameters to collect xpath expression(s)
		Vector xpvct = new Vector();
		String xpstr = null;
		int cnt = 0;
		do {
			xpstr = mpget("xpath." + (new Integer(cnt)));
			if (xpstr != null)
				xpvct.add(xpstr);
			cnt++;	
		} while (xpstr != null);
		if (xpvct.size() == 0) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.WARNING, auditStr + "No XPath expressions found. Nothing to do!");
			return inputModuleData;
		}

		// Parse input document
		Document doc = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		factory.setNamespaceAware(true);
		factory.setValidating(false);
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(new ByteArrayInputStream(msg.getDocument().getContent()));
		} catch (Exception e) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Couldn't parse input Document: " + e);
			return inputModuleData;
		}

		// Evaluate XPath expression(s) to get node values
		Vector valvct = new Vector();
		for (int i=0; i<xpvct.size(); i++) {
			NodeIterator nl = null;
			try {
				nl = XPathAPI.selectNodeIterator(doc, (String) xpvct.get(i));
			} catch (TransformerException e1) {
				e1.printStackTrace();
			}
			Node n;
			String val = null;
			while ((n = nl.nextNode())!= null) {
				val = n.getFirstChild().getNodeValue();
			}
			if (val!=null)
				valvct.add(val);
		}
		if (valvct.size()==0) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "No suitable values found for given XPath expression(s)");
			return inputModuleData;
		}
		
		// Build and feed queue id
		String sep = mpget("separator");
		String qid = new String();
		for (int i=0; i<valvct.size(); i++) {
			qid += (String)valvct.get(i);
			if (sep!=null && i != valvct.size()-1)               // the last one doesn't need a separator
				qid += sep; 
		}
		try {
			msg.setConversationId(qid);
		} catch (InvalidParamException e) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Oh Gosh, I couldn't set the ConversationID: " + e);
		}

		return inputModuleData;
	}

	private String mpget(String pname) {
		return mc.getContextData(pname);
	}

	public void ejbRemove() {
	}

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void setSessionContext(SessionContext context) {
		myContext = context;
	}

	public void ejbCreate() throws CreateException {
	}

	private ModuleContext mc;
	private final String auditStr = "com.sic.DynamicQueueID - ";
	private SessionContext myContext;
}
