package com.sic.nw.xi.afw.modules;

import com.sap.aii.af.mp.module.*;
import com.sap.aii.af.ra.ms.api.Message;
import com.sap.aii.af.ra.ms.api.Payload;
import com.sap.aii.af.service.auditlog.*;
import java.io.*;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.ejb.*;

public class ZipWriterAteneBean implements SessionBean {

	public ZipWriterAteneBean() {
	}

	public ModuleData process(ModuleContext moduleContext, ModuleData inputModuleData) throws ModuleException {
		Object obj = null;
		Message msg = null;
		Hashtable mp = null;
		AuditMessageKey amk = null;
		ModuleException mEx = null;
		try {
			obj = inputModuleData.getPrincipalData();
			msg = (Message) obj;
			amk = new AuditMessageKey(msg.getMessageId(), AuditDirection.INBOUND);
			mp = (Hashtable) inputModuleData.getSupplementalData("module.parameters");
			mc = moduleContext;
		} catch (Exception e) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Error while creating basic instances (obj,msg,amk,mp)");
			throw mEx = new ModuleException(auditStr + "Error while creating basic instances (obj,msg,amk,mp)");
		}
		String FILEEXT = "txt";
		String FILENAME = "NEGXXX.";
		String ls = System.getProperty("line.separator");
		String csv = new String(msg.getDocument().getContent());
		String lines[] = csv.split(ls);
		
		// Very bad trick to put the half symbol in place of a mapping fake
		String msgContent = new String(msg.getDocument().getContent());
		msgContent = msgContent.replaceAll("..HALF..","½");
		

		try {
			byte buf[] = new byte[1024];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);

			// Create the Zip entry
			ByteArrayInputStream bais = new ByteArrayInputStream(msgContent.getBytes());
			zos.putNextEntry(new ZipEntry(FILENAME + FILEEXT));
			int len;
			while ((len = bais.read(buf)) > 0)
				zos.write(buf, 0, len);
			zos.closeEntry();
			bais.close();

			// Close the zip file
			zos.close();
			// Set new payload
			Payload thePayload = msg.getDocument();
			thePayload.setContent(baos.toByteArray());

			// Play with file extension
			String dir = mpget("target.directory");
			File fdir = new File(dir);
			final String zipfn = FILENAME;
			FilenameFilter filter = new FilenameFilter() {

				public boolean accept(File dir, String name) {
					return name.startsWith(zipfn);
				}

			};
			String zipFiles[] = fdir.list(filter);
			String tentativeZipFileName = "";
			if (zipFiles.length == 0) {
				tentativeZipFileName = FILENAME + "zip";
			} else {
				int counter = zipFiles.length;
				DecimalFormat df = new DecimalFormat("000");
				String ext = df.format(new Long(counter));
				tentativeZipFileName = FILENAME + ext;
				for (File destZip = new File(dir + tentativeZipFileName); destZip.exists(); destZip = new File(dir + tentativeZipFileName)) {
					counter++;
					ext = df.format(new Long(counter));
					tentativeZipFileName = FILENAME + ext;
				}

			}
			msg.setMessageProperty("http://sap.com/xi/XI/System/File", "FileName", tentativeZipFileName);
			msg.setMessageProperty("http://sap.com/xi/XI/System/File", "Directory", dir);
		} catch (Exception e) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Error in ZIP creation: " + e);
			throw mEx = new ModuleException(auditStr + "Error in ZIP creation: " + e);
		}
		return inputModuleData;
	}

	private String mpget(String pname) {
		return mc.getContextData(pname);
	}

	public void ejbRemove() {
	}

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void setSessionContext(SessionContext context) {
		myContext = context;
	}

	public void ejbCreate() throws CreateException {
	}

	private ModuleContext mc;
	private final String auditStr = "com.sic.nw.xi.afw.modules.ZipWriterAtene - ";
	private SessionContext myContext;
}
