package com.sic.nw.xi.afw.modules;
import javax.ejb.EJBLocalHome;

import javax.ejb.CreateException;
public interface DynamicQueueIDLocalHome extends EJBLocalHome {

	/**
	 * Create Method.
	 */
	public DynamicQueueIDLocal create() throws CreateException;

}

