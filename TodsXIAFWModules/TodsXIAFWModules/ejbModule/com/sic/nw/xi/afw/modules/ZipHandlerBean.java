package com.sic.nw.xi.afw.modules;

import com.sap.aii.af.mp.module.*;
import com.sap.aii.af.ra.ms.api.InvalidParamException;
import com.sap.aii.af.ra.ms.api.Message;
import com.sap.aii.af.ra.ms.api.MessageDirection;
import com.sap.aii.af.ra.ms.api.Payload;
import com.sap.aii.af.service.auditlog.*;
import java.io.*;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.ejb.*;

public class ZipHandlerBean implements SessionBean {

	static final int BUFFER = 2048;

	public ZipHandlerBean() {
	}

	public ModuleData process(ModuleContext moduleContext, ModuleData inputModuleData) throws ModuleException {
		Object obj = null;
		Message msg = null;
		Hashtable mp = null;
		AuditMessageKey amk = null;
		ModuleException mEx = null;
		try {
			obj = inputModuleData.getPrincipalData();
			msg = (Message) obj;
			if (msg.getMessageDirection() == MessageDirection.INBOUND)
				amk = new AuditMessageKey(msg.getMessageId(), AuditDirection.INBOUND);
			else
				amk = new AuditMessageKey(msg.getMessageId(), AuditDirection.OUTBOUND);
			mp = (Hashtable) inputModuleData.getSupplementalData("module.parameters");
			mc = moduleContext;
		} catch (Exception e) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Error while creating basic instances (obj,msg,amk,mp)");
			throw mEx = new ModuleException(auditStr + "Error while creating basic instances (obj,msg,amk,mp)");
		}

		// ZIP FILE HANDLER - Common part
		String ls = System.getProperty("line.separator");
		String csv = null;

		// ZIP FILE - READ
		if (mpget("action") != null && mpget("action").equalsIgnoreCase("READ")) {
			Audit.addAuditLogEntry(amk, AuditLogStatus.SUCCESS, auditStr + "ZIP READ PROCESS STARTED");
			ZipInputStream zis = null;
			try {
				ByteArrayInputStream bais = new ByteArrayInputStream(msg.getDocument().getContent());
				zis = new ZipInputStream(bais);
			} catch (Exception e) {
				Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Error creating the ZipInputStream instance: " + e);
				throw mEx = new ModuleException(auditStr + "Error creating the ZipInputStream instance: " + e);
			}

			// Read each ZIP file entry, extract text content
			int currentByte;
			byte data[] = new byte[BUFFER];
			ByteArrayOutputStream baos = null;
			ZipEntry ze = null;
			try {
				ze = zis.getNextEntry();
				while (ze != null) {
					// Skip unneeded files (cdt)
					if (ze.getName().toUpperCase().indexOf(".CTD") == -1) {
						baos = new ByteArrayOutputStream();
						while ((currentByte = zis.read(data, 0, BUFFER)) != -1) {
							baos.write(data, 0, currentByte);
						}
						String fileText = new String(baos.toByteArray());
						// Remove double quotes
						// fileText = fileText.replaceAll("\"", "");
						// Prepend record name to each text file line (zip entry
						// name w/o extension)
						String fileLines[] = fileText.split("\n");
						fileText = new String();
						String recType = ze.getName().substring(0, ze.getName().indexOf(".")).toUpperCase();
						for (int i = 0; i < fileLines.length; i++) {
							fileText += recType + "," + fileLines[i] + ls;
						}
						Audit.addAuditLogEntry(amk, AuditLogStatus.SUCCESS, auditStr + "Found " + fileLines.length + " lines of record type " + recType);
						// Append to final string (the csv)
						csv += fileText;
					}
					ze = zis.getNextEntry();
				}
			} catch (Exception e) {
				Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Error parsing ZIP file content: " + e);
				throw mEx = new ModuleException(auditStr + "Error parsing ZIP file content: " + e);
			}

			// TODO: CSV special handling
			csv = csv.replace('½', 'M');

			// Insert csv as payload
			try {
				Payload thePayload = msg.getDocument();
				thePayload.setContent(csv.getBytes());
			} catch (InvalidParamException e) {
				Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Error inserting extracted csv file as payload: " + e);
				throw mEx = new ModuleException(auditStr + "Error inserting extracted csv file as payload: " + e);
			}

			// ZIP FILE - WRITE
		} else {
			Audit.addAuditLogEntry(amk, AuditLogStatus.SUCCESS, auditStr + "ZIP WRITE PROCESS STARTED");
			String RECNAMES[] = { "NGFILTRA", "NGMAARS", "NGMAMVS", "NGMASAS", "NGTAAS" };
			String CTDNAMES[] = { "NGMAARS.ctd", "NGMAMVS.ctd", "NGMASAS.ctd", "NGTAAS.ctd" };
			String FILEEXT = ".dat";
			String ZIPFILENAME = "NEG<negozio>.";
			csv = new String(msg.getDocument().getContent());
			StringBuffer reclines[] = { new StringBuffer(), new StringBuffer(), new StringBuffer(), new StringBuffer(), new StringBuffer()};
			String lines[] = csv.split(ls);
			String codNegozio = null;
			for (int i = 0; i < lines.length; i++) {
				String rectype = lines[i].split(",")[0].replaceAll("\"", "");
				for (int recindex = 0; recindex < reclines.length; recindex++) {
					String relevantFields = new String();
					if (rectype.equalsIgnoreCase(RECNAMES[recindex])) {
						relevantFields = lines[i].substring(lines[i].indexOf(",") + 1, lines[i].length());
						reclines[recindex].append(relevantFields + ls);
						if (codNegozio == null && rectype.equalsIgnoreCase(RECNAMES[1]))
							codNegozio = relevantFields.split(",")[0].replaceAll("\"", "");
					}
				}
			}
			Audit.addAuditLogEntry(amk, AuditLogStatus.SUCCESS, auditStr + lines.length + " processed.");

			try {
				byte buf[] = new byte[1024];
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ZipOutputStream zos = new ZipOutputStream(baos);
				for (int i = 0; i < reclines.length; i++) {
					ByteArrayInputStream bais = new ByteArrayInputStream(reclines[i].toString().getBytes());
					zos.putNextEntry(new ZipEntry(RECNAMES[i] + ".dat"));
					int len;
					while ((len = bais.read(buf)) > 0)
						zos.write(buf, 0, len);
					zos.closeEntry();
					bais.close();
				}
				Audit.addAuditLogEntry(amk, AuditLogStatus.SUCCESS, auditStr + "Main data files added to ZIP file");

				String ctddir = mpget("ctd.directory");
				if (ctddir != null && new File(ctddir) != null) {
					for (int i = 0; i < CTDNAMES.length; i++) {
						FileInputStream fis = new FileInputStream(ctddir + CTDNAMES[i]);
						zos.putNextEntry(new ZipEntry(CTDNAMES[i]));
						int len;
						while ((len = fis.read(buf)) > 0)
							zos.write(buf, 0, len);
						zos.closeEntry();
						fis.close();
					}

				}
				Audit.addAuditLogEntry(amk, AuditLogStatus.SUCCESS, auditStr + "CTD files addes to ZIP files");
				zos.close();
				Payload thePayload = msg.getDocument();
				thePayload.setContent(baos.toByteArray());
				ZIPFILENAME = ZIPFILENAME.replaceAll("<negozio>", codNegozio);
				String dir = mpget("target.directory");
				File fdir = new File(dir);
				final String zipfn = ZIPFILENAME;
				FilenameFilter filter = new FilenameFilter() {

					public boolean accept(File dir, String name) {
						return name.startsWith(zipfn);
					}

				};
				String zipFiles[] = fdir.list(filter);
				String tentativeZipFileName = "";
				if (zipFiles.length == 0) {
					tentativeZipFileName = ZIPFILENAME + "zip";
				} else {
					int counter = zipFiles.length;
					DecimalFormat df = new DecimalFormat("000");
					String ext = df.format(new Long(counter));
					tentativeZipFileName = ZIPFILENAME + ext;
					for (File destZip = new File(dir + tentativeZipFileName); destZip.exists(); destZip = new File(dir + tentativeZipFileName)) {
						counter++;
						ext = df.format(new Long(counter));
						tentativeZipFileName = ZIPFILENAME + ext;
					}

				}
				Audit.addAuditLogEntry(amk, AuditLogStatus.SUCCESS, auditStr + "Going to write file " + tentativeZipFileName + " in " + dir);
				msg.setMessageProperty("http://sap.com/xi/XI/System/File", "FileName", tentativeZipFileName);
				msg.setMessageProperty("http://sap.com/xi/XI/System/File", "Directory", dir);
			} catch (Exception e) {
				Audit.addAuditLogEntry(amk, AuditLogStatus.ERROR, auditStr + "Error in ZIP creation: " + e);
				throw mEx = new ModuleException(auditStr + "Error in ZIP creation: " + e);
			}

		}

		return inputModuleData;
	}

	private String mpget(String pname) {
		return mc.getContextData(pname);
	}

	public void ejbRemove() {
	}

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void setSessionContext(SessionContext context) {
		myContext = context;
	}

	public void ejbCreate() throws CreateException {
	}

	private ModuleContext mc;

	private final String auditStr = "com.sic.nw.xi.afw.modules.ZipHandler - ";

	private SessionContext myContext;
}
