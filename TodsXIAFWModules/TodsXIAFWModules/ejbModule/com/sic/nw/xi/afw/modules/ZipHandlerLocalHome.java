package com.sic.nw.xi.afw.modules;
import javax.ejb.EJBLocalHome;

import javax.ejb.CreateException;
public interface ZipHandlerLocalHome extends EJBLocalHome {

	/**
	 * Create Method.
	 */
	public ZipHandlerLocal create() throws CreateException;

}

