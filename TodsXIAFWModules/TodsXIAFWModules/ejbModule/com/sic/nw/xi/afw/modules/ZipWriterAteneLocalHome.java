package com.sic.nw.xi.afw.modules;
import javax.ejb.EJBLocalHome;

import javax.ejb.CreateException;
public interface ZipWriterAteneLocalHome extends EJBLocalHome {

	/**
	 * Create Method.
	 */
	public ZipWriterAteneLocal create() throws CreateException;

}

