// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package com.sap.sdn.samples.evspp.component;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateEVS_PP_DUO).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import java.util.Iterator;
import java.util.Map;

import com.sap.dictionary.runtime.DdCheckException;
import com.sap.dictionary.runtime.ISimpleType;
import com.sap.sdn.samples.evspp.component.wdp.IPrivateEVS_PP_DUO;
import com.sap.tc.webdynpro.services.exceptions.WDNonFatalRuntimeException;
import com.sap.typeservices.ISimpleValueSet;
//@@end

//@@begin documentation
//@@end

public class EVS_PP_DUO
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(EVS_PP_DUO.class);

  static 
  {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see com.sap.sdn.samples.evspp.component.wdp.IPrivateEVS_PP_DUO for more details
   */
  private final IPrivateEVS_PP_DUO wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see com.sap.sdn.samples.evspp.component.wdp.IPrivateEVS_PP_DUO.IContextNode for more details.
   */
  private final IPrivateEVS_PP_DUO.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public EVS_PP_DUO(IPrivateEVS_PP_DUO wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
  /** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
    //@@end
  }

  //@@begin javadoc:wdDoExit()
  /** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
  /**
   * Hook method called to modify a view just before rendering.
   * This method conceptually belongs to the view itself, not to the
   * controller (cf. MVC pattern).
   * It is made static to discourage a way of programming that
   * routinely stores references to UI elements in instance fields
   * for access by the view controller's event handlers, and so on.
   * The Web Dynpro programming model recommends that UI elements can
   * only be accessed by code executed within the call to this hook method.
   *
   * @param wdThis Generated private interface of the view's controller, as
   *        provided by Web Dynpro. Provides access to the view controller's
   *        outgoing controller usages, etc.
   * @param wdContext Generated interface of the view's context, as provided
   *        by Web Dynpro. Provides access to the view's data.
   * @param view The view's generic API, as provided by Web Dynpro.
   *        Provides access to UI elements.
   * @param firstTime Indicates whether the hook is called for the first time
   *        during the lifetime of the view.
   */
  //@@end
  public static void wdDoModifyView(IPrivateEVS_PP_DUO wdThis, IPrivateEVS_PP_DUO.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView
    //@@end
  }

  //@@begin javadoc:getColorCalc(IPrivateEVS_PP_DUO.IContextElement)
  /**
   *  Declared getter method for attribute ColorCalc of node Context
   *  @param element the element requested for the value
   *  @return the calculated value for attribute ColorCalc
   */
  //@@end
  public java.lang.String getColorCalc(IPrivateEVS_PP_DUO.IContextElement element)
  {
    //@@begin getColorCalc(IPrivateEVS_PP_DUO.IContextElement)
	if ( null == element.getColor() ) return null;
    
	final ISimpleValueSet values =
		element
			.node()
				.getNodeInfo()
					.getAttribute(IPrivateEVS_PP_DUO.IContextElement.COLOR)
						.getSimpleType()
							.getSVServices()
								.getValues();
    						
	return values.getText( element.getColor() );
    //@@end
  }

  //@@begin javadoc:setColorCalc(IPrivateEVS_PP_DUO.IContextElement, java.lang.String)
  /**
   *  Declared setter method for attribute ColorCalc of node Context
   *  @param element the element to change the value
   *  @param value the new value for attribute ColorCalc
   */
  //@@end
  public void setColorCalc(IPrivateEVS_PP_DUO.IContextElement element, java.lang.String value)
  {
    //@@begin setColorCalc(IPrivateEVS_PP_DUO.IContextElement, java.lang.String)
	if ( null == value )
	{
		element.setColor( null );
		return;
	}
	value = value.toUpperCase();
	final ISimpleType type = 
		element
			.node()
				.getNodeInfo()
					.getAttribute(IPrivateEVS_PP_DUO.IContextElement.COLOR)
						.getSimpleType();
 
	try
	{
		type.checkValid(value);
		element.setColor( value );
	}
	catch (final DdCheckException ex)
	{
		final ISimpleValueSet values = type.getSVServices().getValues();
		for (final Iterator i = values.entrySet().iterator(); i.hasNext(); )
		{
			final Map.Entry e = (Map.Entry)i.next();
			if ( value.equalsIgnoreCase( (String)e.getValue() ) )
			{
				element.setColor( (String)e.getKey() );
				return;
			}
		}
		throw new WDNonFatalRuntimeException(ex);
	}
    
    //@@end
  }

  //@@begin javadoc:onActionVoid(ServerEvent)
  /** Declared validating event handler. */
  //@@end
  public void onActionVoid(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionVoid(ServerEvent)
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
}
