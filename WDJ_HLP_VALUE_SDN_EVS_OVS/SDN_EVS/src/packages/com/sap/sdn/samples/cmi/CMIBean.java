/*
 * Created on 05.07.2005
 *
 */
package com.sap.sdn.samples.cmi;

import java.util.Collection;

import com.sap.tc.cmi.metadata.ICMIModelClassInfo;

import com.sap.tc.cmi.model.ICMIModel;
import com.sap.tc.cmi.model.ICMIModelClass;
import com.sap.tc.cmi.model.ICMIGenericModelClass;
/**
 * @author Valery_Silaev
 *
 */
abstract public class CMIBean implements ICMIGenericModelClass 
{
	public Object getAttributeValue(final String name)
	{
		throw new IllegalArgumentException("Unknown attribute: " + name);			
	}
	
	public void setAttributeValue(final String name, final Object value)
	{
		throw new IllegalArgumentException("Unknown attribute: " + name);			
	}
	
	public ICMIModelClass getRelatedModelObject(final String targetRoleName) { return illegal(targetRoleName); }
	public void setRelatedModelObject(final String targetRoleName, final ICMIModelClass o) { illegal(targetRoleName); }
	public Collection getRelatedModelObjects(final String targetRoleName) { illegal(targetRoleName); return null; }
	public void setRelatedModelObjects(final String targetRoleName, final Collection col) { illegal(targetRoleName); }
	public boolean addRelatedModelObject(final String targetRoleName, final ICMIModelClass o){ illegal(targetRoleName); return false; }
	public boolean removeRelatedModelObject(final String targetRoleName, final ICMIModelClass o) { illegal(targetRoleName); return false; }

	public ICMIModel associatedModel() { return null; }	

	final private static ICMIModelClass illegal(final String relation)
	{
		throw new IllegalArgumentException("No relation with name " + relation);
	}

}
