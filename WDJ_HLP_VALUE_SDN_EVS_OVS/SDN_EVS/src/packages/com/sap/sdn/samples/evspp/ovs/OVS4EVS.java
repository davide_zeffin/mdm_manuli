/*
 * Created on 09.03.2006
 *
 */
package com.sap.sdn.samples.evspp.ovs;

import java.util.Iterator;
import java.util.Map;

import com.sap.dictionary.runtime.DdCheckException;
import com.sap.dictionary.runtime.ISimpleType;

import com.sap.tc.webdynpro.progmodel.api.IWDAttributeInfo;
import com.sap.tc.webdynpro.progmodel.api.IWDNodeElement;
import com.sap.tc.webdynpro.progmodel.api.IWDOVSNotificationListener;
import com.sap.tc.webdynpro.progmodel.api.WDValueServices;

import com.sap.tc.webdynpro.services.exceptions.WDNonFatalRuntimeException;

import com.sap.typeservices.ISimpleValueSet;

/**
 * @author Valery_Silaev
 *
 */
public class OVS4EVS 
{
	
	public static interface IElementResolver
	{
		public IWDNodeElement resolve(IWDNodeElement elementOfProxyAttr);
	}
	
	public static interface IHelper
	{
		public String getDisplayText(IWDNodeElement el);
		public void applyValue(IWDNodeElement el, final Object value);
	}
	
	final public static IElementResolver SAME_NODE_ELEMENT = new IElementResolver()
	{
		public IWDNodeElement resolve(final IWDNodeElement elementOfProxyAttr)
		{
			return elementOfProxyAttr;
		}
	}; 
	
	final public static IElementResolver PARENT_NODE_ELEMENT = new IElementResolver()
	{
		public IWDNodeElement resolve(final IWDNodeElement elementOfProxyAttr)
		{
			return elementOfProxyAttr.node().getParentElement();
		}
	};
	

	public static IHelper bind(final IWDAttributeInfo proxyAttr, final IWDAttributeInfo realAttr)
	{
		return bind( proxyAttr, realAttr, null );
	}
	
	public static IHelper bind(final IWDAttributeInfo proxyAttr, final IWDAttributeInfo realAttr, IElementResolver resolver)
	{
		if ( null == proxyAttr ) 
			throw new IllegalArgumentException("proxyAttr parameter is null");
			
		if ( null == realAttr ) 
			throw new IllegalArgumentException("realAttr parameter is null");
		
		if ( !realAttr.hasSimpleType() )
			throw new IllegalArgumentException( realAttr + " is not of simple type");
			
		if ( null == resolver )
		{
			if ( proxyAttr.getNode().equals( realAttr.getNode() ) )
				resolver = SAME_NODE_ELEMENT;
			else if ( realAttr.getNode().equals( proxyAttr.getNode().getParent() ) )
				resolver = PARENT_NODE_ELEMENT;
			else
				throw new IllegalArgumentException
				(
					"Null element resolver supplied, and proxy / real attributes " +					"resides are neither in same node, nor proxy node direct child of real node"
				);
		}
			
		WDValueServices.addOVSExtension
		(
			EVSQuery.class.getName(), 
			new IWDAttributeInfo[]{ proxyAttr },
			new EVSQuery( realAttr.getSimpleType() ), 
			new EvsNotificationListener( realAttr, resolver )
		);			
		
		return new EvsHelper( realAttr, resolver );	
	}

	private static class EvsNotificationListener implements IWDOVSNotificationListener
	{
		final private String _realAttr;
		final private IElementResolver _resolver;
		
		EvsNotificationListener(final IWDAttributeInfo realAttr, final IElementResolver resolver) 
		{ 
			_realAttr = realAttr.getName();
			_resolver = resolver; 
		}
	  		
		public void onQuery(final Object query) {}
  	
		public void applyResult(final IWDNodeElement target, final Object result)
		{
			final EVSQuery.Result myResult = (EVSQuery.Result)result;
			_resolver.resolve(target).setAttributeValue( _realAttr, myResult.realKey() );
		}
  	
		public void applyInputValues(final IWDNodeElement target, final Object query)
		{
			final EVSQuery myQuery = (EVSQuery)query;
			myQuery.apply( _resolver.resolve(target).getAttributeValue( _realAttr ) );
	  }
	}
	
	private static class EvsHelper implements IHelper
	{
		final private ISimpleType      _type;
		final private ISimpleValueSet  _svs; 
		final private IElementResolver _resolver;
		final private String           _realAttr;
		
		EvsHelper(final IWDAttributeInfo realAttr, final IElementResolver resolver) 
		{ 
			_type     = realAttr.getSimpleType();
			_svs      = _type.getSVServices().getValues(); 
			_realAttr = realAttr.getName();
			_resolver = resolver;
		} 
		
		public String getDisplayText(final IWDNodeElement el) 
		{
			return _svs.getText
			(
				_resolver.resolve(el).getAttributeValue( _realAttr )
			); 
		}
		
		public void applyValue(final IWDNodeElement el, final Object value)
		{
			final IWDNodeElement real = _resolver.resolve(el);
			
			if ( null == value )
			{
				 real.setAttributeValue( _realAttr, null );
				 return;
			}
			
			try
			{
				_type.checkValid(value);
				real.setAttributeValue( _realAttr, value );
			}
			catch (final DdCheckException ex)
			{
				final String strValue = value.toString();
				for (final Iterator i = _svs.entrySet().iterator(); i.hasNext(); )
				{
					final Map.Entry e = (Map.Entry)i.next();
					if ( strValue.equalsIgnoreCase( (String)e.getValue() ) )
					{
						real.setAttributeValue( _realAttr, e.getKey() );
						return;
					}
				}
				throw new WDNonFatalRuntimeException(ex);
			}
			
		}
		
	}
	
}
