/*
 * Created on 09.03.2006
 *
 */
package com.sap.sdn.samples.evspp.ovs;

import java.util.Locale;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.sap.dictionary.runtime.ISimpleType;

import com.sap.sdn.samples.cmi.CMIBean;
import com.sap.sdn.samples.cmi.CMIInfo;
import com.sap.sdn.samples.cmi.DictionaryTypes;

import com.sap.tc.cmi.exception.CMIException;

import com.sap.tc.cmi.metadata.ICMIModelClassInfo;
import com.sap.tc.cmi.metadata.ICMIModelObjectCollectionInfo;

import com.sap.tc.cmi.model.ICMIQuery;

import com.sap.tc.webdynpro.basesrvc.util.StringUtil;

/**
 * @author Valery_Silaev
 *
 */
public class EVSQuery	extends
		CMIBean 
	implements 
		ICMIQuery 
{
	private Collection _result = null;
	
	private String _keyFilter = "*";
	private String _valFilter = "*";
	
	final private ISimpleType _type;	
	final private Map         _evs; 
	final private Comparator  _resultComparator;
	
	protected EVSQuery(final ISimpleType type)
	{
		this(type, (Map)type.getSVServices().getValues(), null );
	}
	
	protected EVSQuery(final ISimpleType type, final Map evs)
	{
		this(type, evs, null);
	}
	
	protected EVSQuery(final ISimpleType type, final Map evs, final Comparator resultComparator)
	{
		_type = type;
		_evs  = evs;
		_resultComparator = resultComparator;
	}
	
	protected void apply(final Object value)
	{
		invalidate();
		_valFilter = "*";
		_keyFilter = "*";
		if ( null == value ) return;
		
		_keyFilter = _type.format(value);
		_valFilter = (String)_evs.get( value );
	}
	
	public String getKeyFilter() { return _keyFilter; }
	public void setKeyFilter(final String value) 
	{ 
		_keyFilter = StringUtil.isEmpty(value) ? "*" : value; 
	}
	
	public String getValueFilter() { return _valFilter; }
	public void setValueFilter(final String value) 
	{ 
		_valFilter = StringUtil.isEmpty(value) ? "*" : value;
	}
	
	public Object getAttributeValue(final String name)
	{
		if ( "keyFilter".equals(name) ) return getKeyFilter();
		if ( "valueFilter".equals(name) ) return getValueFilter();
		return super.getAttributeValue(name);
	}
	
	public void setAttributeValue(final String name, final Object value)
	{
		if ( "keyFilter".equals(name) ) 
			setKeyFilter((String)value);
		else if ( "valueFilter".equals(name) ) 
			setValueFilter((String)value);
		else 
			super.setAttributeValue(name, value);			
	}


	public ICMIModelClassInfo associatedInputParameterInfo() 
	{
		return associatedModelClassInfo();
	}

	public ICMIModelObjectCollectionInfo associatedResultInfo() 
	{
		return RESULT_INFO;
	}

	public long countOf() { return -1; }

	public void invalidate() { _result = null; }

	public void execute() throws CMIException
	{
		final Pattern keyFilter = compile(_keyFilter);
		final Pattern valFilter = compile(_valFilter);
			
		final ArrayList result = new ArrayList( _evs.size() );
		for (final Iterator i = _evs.entrySet().iterator(); i.hasNext(); )
		{
			final Map.Entry e = (Map.Entry)i.next();
			final String key = _type.format( e.getKey() );
			if ( 
				!keyFilter.matcher( key ).matches() ||
				!valFilter.matcher( (String)e.getValue() ).matches()
			   ) continue;
			   
			result.add( new Result( e.getKey(), key, (String)e.getValue() ) );	
		}
		
		if ( null != _resultComparator )
			Collections.sort( result, _resultComparator);
			
		_result = result;
	}

	public Object getInputParameter() 
	{
		return this;
	}

	public Collection getResult() 
	{
		return _result;
	}

	public boolean isDirty() 
	{
		return null == _result;
	}

	public ICMIModelClassInfo associatedModelClassInfo()
	{
		return INPUT_INFO; 
	}
	
	final private static Pattern MATCH_ALL = Pattern.compile(".*");
	private static Pattern compile(final String mask) throws CMIException
	{
		if ( skipFilter(mask) ) return MATCH_ALL;
		else
			try 
			{ 
				return Pattern.compile( mask.replaceAll("(\\*|\\?)", "\\.$1"), Pattern.CASE_INSENSITIVE );
			}
			catch (final PatternSyntaxException ex)
			{
				throw new CMIException(ex);
			}
	}
	
	
		
	private static boolean skipFilter(final String mask)
	{
		return ( StringUtil.isEmpty(mask) || "*".equals(mask) ); 
	}
	
	public static class Result extends CMIBean
	{
		final private Object _realKey;		
		final private String _key;
		final private String _value;
		 
		Result(final Object realKey, final String key, final String value) 
		{
			_realKey = realKey;
			_key     = key;
			_value   = value;
		}
		
		public Object realKey() { return _realKey; }
	
		public String getKey() { return _key; }
		public String getValue() { return _value; }
	
		public Object getAttributeValue(final String name)
		{
			if ( "value".equals(name) ) return getValue();
			if ( "key".equals(name) ) return getKey();
			return super.getAttributeValue(name);
		}
	
		public void setAttributeValue(final String name, final Object value)
		{
			if ( "key".equals(name) || "value".equals(name) ) 
				throw new IllegalArgumentException("Property " + name + " is read-only");
			super.setAttributeValue(name, value);			
		}

		public ICMIModelClassInfo associatedModelClassInfo()
		{
			return RESULT_ELEMENT_INFO;
		}
	}
	
	final private static CMIInfo.GenericClassInfo INPUT_INFO 
		= new CMIInfo.GenericClassInfo( "EVSQuery", Locale.ENGLISH )
	{
		{
			property("keyFilter",  
					 "com.sap.sdn.samples.evspp.types.EVSKey", 
					 false );
			property("valueFilter",       
					 "com.sap.sdn.samples.evspp.types.EVSValue", 
					  false );
		}
	};
	
	final private static ICMIModelClassInfo RESULT_ELEMENT_INFO 
		= new CMIInfo.GenericClassInfo( INPUT_INFO.getName() + "_Result", Locale.ENGLISH ) 
	{
		{
			property("key", 
					 "com.sap.sdn.samples.evspp.types.EVSKey", 
					 true );				
			property("value",       
					 "com.sap.sdn.samples.evspp.types.EVSValue", 
					 true );
		}
	};
	
	final private static ICMIModelObjectCollectionInfo RESULT_INFO = new CMIInfo.BasicCollectionInfo
	(
		RESULT_ELEMENT_INFO.getName() + "_Collection", RESULT_ELEMENT_INFO 
	);

public static void main(final String[] args) throws Exception
{
	System.out.println("a*b??c*def".replaceAll("(\\*|\\?)", "\\.$1") );	
}
}
