/*
 * Created on 11.08.2005
 */
package com.sap.sdn.samples.cmi;

import java.lang.ref.SoftReference;

import java.util.Locale;

import java.util.Map;
import java.util.IdentityHashMap;
import java.util.Collections;
import java.util.WeakHashMap;

import com.sap.dictionary.runtime.IBroker;
import com.sap.dictionary.runtime.ISimpleType;
import com.sap.dictionary.runtime.DdDictionaryPool;
import com.sap.dictionary.runtime.DdException;
/**
 * @author Valery_Silaev
 *
 */
final public class DictionaryTypes 
{
	private DictionaryTypes(){}
	
	public static ISimpleType getType(final String typeName, final Locale l, final ClassLoader cl)
	{
		try
		{
			return (ISimpleType)getBroker(cl, l).getDataType("java", typeName);
		}
		catch (final DdException ex)
		{
			throw new RuntimeException(ex);
		}
	}
	
	public static String getTypeName(final Class clazz)
	{
		if (null == clazz)
			throw new IllegalArgumentException("Class parameter is null");
		return (String)TYPES_MAP.get( clazz );
	}
	
	public static ISimpleType cast(final Class type, final Locale locale)
	{	
		final String typeName = getTypeName(type);
		if (null == typeName)
			throw new IllegalArgumentException("Unnable to find corresponding DDIC type for class " + type.getName() );
		return getType( typeName, locale, DictionaryTypes.class.getClassLoader() );
	}
	
	private static synchronized IBroker getBroker(final ClassLoader cl, final Locale l) throws DdException
	{
		final BrokerKey     key = new BrokerKey(cl, l);
		final SoftReference ref = (SoftReference)BROKERS.get(key);
		
		IBroker broker = null != ref ? (IBroker)ref.get() : null;
		if( null == broker )
		{
			broker = DdDictionaryPool.getInstance().createBroker( cl, l);
			BROKERS.put( key, new SoftReference(broker) );
		}
		return broker;
	}
	
	
	final private static class BrokerKey
	{
		final private Locale      _l;
		final private ClassLoader _cl;
		final private int         _h;
		
		private BrokerKey(final ClassLoader cl, final Locale l) 
		{ 
			_l  = l; _cl = cl;
			_h  = 31 * cl.hashCode() + l.hashCode(); 
		}
		
		final public int hashCode() { return _h; }
		final public boolean equals(final Object o)
		{
			if ( o.getClass() != BrokerKey.class ) return false;
			final BrokerKey k = (BrokerKey)o;
			return k._cl == _cl && k._l.equals( _l );
		}
	}
	
	
	final private static Map BROKERS = new WeakHashMap();
	final private static Map TYPES_MAP;
	
	static 
	{
		final Map map = new IdentityHashMap(20);
		map.put( boolean.class,    "com.sap.dictionary.boolean" );
		map.put( Boolean.class,    "com.sap.dictionary.boolean" );
		
		map.put( short.class,      "com.sap.dictionary.short" );
		map.put( Short.class,      "com.sap.dictionary.short" );
		map.put( int.class,        "com.sap.dictionary.integer" );
		map.put( Integer.class,    "com.sap.dictionary.integer" );
		map.put( long.class,       "com.sap.dictionary.long" );
		map.put( Long.class,       "com.sap.dictionary.long" );
		map.put( float.class,      "com.sap.dictionary.float" );
		map.put( Float.class,      "com.sap.dictionary.float" );
		map.put( double.class,     "com.sap.dictionary.double" );
		map.put( Double.class,     "com.sap.dictionary.double" );
		
		map.put( java.math.BigDecimal.class, "com.sap.dictionary.decimal" );
		
		map.put( byte[].class,     "com.sap.dictionary.binary" );
		
		map.put( String.class,     "com.sap.dictionary.string" );
		
		map.put( java.sql.Time.class,       "com.sap.dictionary.time" );
		map.put( java.sql.Date.class,       "com.sap.dictionary.date" );
		map.put( java.sql.Timestamp.class,  "com.sap.dictionary.timestamp" );
		
		map.put( com.sap.tc.webdynpro.progmodel.api.WDVisibility.class, "com.sap.ide.webdynpro.uielementdefinitions.Visibility" );		
				
		TYPES_MAP = Collections.unmodifiableMap( map );

	}
}