/*
 * Created on 5-dic-2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.mdm.connection;
import javax.naming.*;

import com.sap.mdm.search.Search;
import com.sapportals.connector.connection.*;
import a2i.common.*;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MDMConnection {

	public static void main(String[] args) {
		
//		1.1 get the context : 
		Context ctx = new InitialContext();

//		1.2 lookup in the JDNI to get the connection factory class
		IConnectionFactory connectionFactory = (IConnectionFactory)ctx.lookup("deployedAdapters/MDMEFactory/shareable/MDMEFactory");

//		1.3 Get Connection Spec to set connection properties
		IConnectionSpec spec = connectionFactory.getConnectionSpec();

//		1.4 Set Connection Properties
		spec.setPropertyValue("UserName", "Administrator");
		spec.setPropertyValue("Password", "123456");
		spec.setPropertyValue("Server", "server1");
		spec.setPropertyValue("Port", "50000");
		spec.setPropertyValue("RepositoryLanguage", "Chinese [HK]");

//		1.5 Get the Connection
		IConnection connection = connectionFactory.getConnectionEx(spec);

//		Step 2 : obtain access to the native object

//		2.1 Retrieve Native inteface
		INative nativeInterface = connection.retrieveNative();

//		2.2 Get the CatalogData the physical connection
		CatalogData catalog = (CatalogData) nativeInterface.getNative(CatalogData.class.getName());

//		Step 3: Retrieve data from Catalog

//		3.1 Create ResultSetDefinition for products table
		ResultSetDefinition resultdefenition = new ResultSetDefinition("Categories");
		resultdefenition.AddField("Id");

//		3.2 Create Search object for Categories table
		Search search = new Search("Categories");

//		3.3 Get Data from repository.
		A2iResultSet rs = catalog.GetResultSet(search, resultdefenition, "Id", true, 0);

//		Once we have all the data in the result set, one can do manipulation on it and retrieve the data.

//		Step 4: Close the connection
		connection.close();

	
		
		
	}
}
