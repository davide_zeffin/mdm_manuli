package com.faber.lookUp;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import com.sap.aii.mapping.api.DynamicConfiguration;
import com.sap.aii.mapping.api.DynamicConfigurationKey;
import com.sap.aii.mapping.api.StreamTransformation;
import com.sap.aii.mapping.lookup.Channel;
import com.sap.aii.mapping.lookup.LookupService;
import com.sap.aii.mapping.lookup.Payload;
import com.sap.aii.mapping.lookup.RfcAccessor;
import com.sap.aii.mapping.lookup.XmlPayload;

public class RFCLookUpPdf implements StreamTransformation {

	static final String ENTRYNAME = "ServeltShouldProcessThis.xml";
	static final int BUFFER = 1024 * 1000;
	String ourSourceFileName;
	private Map param;

	public void setParameter(Map map) {
		param = map;
		if (param == null) {
			param = new HashMap();
		}
	}

	public static void main(String[] args) {

		try {
			FileInputStream fin = new FileInputStream(args[0]);
			FileOutputStream fout = new FileOutputStream(args[1]);
			RFCLookUpPdf mapping = new RFCLookUpPdf();
			mapping.execute(fin, fout);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void execute(InputStream inputstream, OutputStream outputstream) {

		DynamicConfiguration conf =
			(DynamicConfiguration) param.get("DynamicConfiguration");
		DynamicConfigurationKey KEY_FILENAME =
			DynamicConfigurationKey.create(
				"http://sap.com/xi/XI/System/File",
				"FileName");

		String OldValue = conf.get(KEY_FILENAME);
		int len = 0;
		byte buf[] = new byte[BUFFER];
		RfcAccessor accessor = null;
		ByteArrayOutputStream out = null;
		String upDate = "UPDATE";

		try {

			String content = "";
			//	   filling the string with our RFC-XML (with values
			String m =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><ns0:ZXI_FABERINVOICE xmlns:ns0=\"urn:sap-com:document:sap:rfc:functions\">"
					+ "<FILENAME>"
					+ OldValue
					+ "</FILENAME>"
					+ "<OPERATION>"
					+ upDate
					+ "</OPERATION>"
					+ "</ns0:ZXI_FABERINVOICE>";

			//	   1. Determine a channel (Business system, Communication channel)
			Channel channel =
				LookupService.getChannel("SAP_SVIL", "XI_RFC_LookUp");
			//	   2. Get a RFC accessor for a channel.
			accessor = LookupService.getRfcAccessor(channel);
			//	   3. Create a xml input stream representing the function module request message.
			InputStream inputStream = new ByteArrayInputStream(m.getBytes());
			//	   4. Create xml payload
			XmlPayload payload = LookupService.getXmlPayload(inputStream);
			//	   5. Execute lookup.
			Payload result = accessor.call(payload);
			InputStream in = result.getContent();
			out = new ByteArrayOutputStream(1024);
			byte[] buffer = new byte[1024];
			for (int read = in.read(buffer);
				read > 0;
				read = in.read(buffer)) {
				out.write(buffer, 0, read);
			}
			content = out.toString();

			BufferedInputStream bis = new BufferedInputStream(inputstream);
			bis.mark(0);
			bis.reset();
			while ((len = bis.read(buf)) > 0)
				outputstream.write(buf, 0, len);

		} catch (Exception e) {

		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {

				}
			}
			//	   7. close the accessor in order to free resources.
			if (accessor != null) {
				try {
					accessor.close();
				} catch (Exception e) {

				}
			}

		}
	}
}
