// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package com.manulirubber.mdm.crimping;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import com
	.manulirubber
	.mdm
	.crimping
	.wdp
	.IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView;
import com.sap.tc.webdynpro.clientserver.event.api.WDPortalEventing;
import com.sap.tc.webdynpro.clientserver.navigation.api.WDPortalNavigation;
import com
	.sap
	.tc
	.webdynpro
	.clientserver
	.navigation
	.api
	.WDPortalNavigationHistoryMode;
import com.sap.tc.webdynpro.clientserver.navigation.api.WDPortalNavigationMode;
//@@end

//@@begin documentation
//@@end

public class STATIC_MDM_EPFC_CRIMPING_RESTView
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(STATIC_MDM_EPFC_CRIMPING_RESTView.class);

  static 
  {
    //@@begin id
		String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see com.manulirubber.mdm.crimping.wdp.IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView for more details
   */
  private final IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see com.manulirubber.mdm.crimping.wdp.IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView.IContextNode for more details.
   */
  private final IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public STATIC_MDM_EPFC_CRIMPING_RESTView(IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
	/** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
		namespace = "urn:com.sap.pct.mdm.appl.masteriviews";
		eventname = "selectRecord";
		WDPortalEventing.subscribe(
			namespace,
			eventname,
			wdThis.wdGetCatchValueAction());
		wdContext.currentContextElement().setTitle_booklet(
			"CRIMPING CHART BOOKLET");
		wdContext.currentContextElement().setTitle_datasheet(
			"CRIMPING CHART - UPDATE");

		java.util.Date utilDate = new java.util.Date();
		//	java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		java.sql.Date sqlDate = java.sql.Date.valueOf("2015-06-10");
		wdContext.currentContextElement().setDate_pubblication(sqlDate);
    //@@end
  }

  //@@begin javadoc:wdDoExit()
	/** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
	/**
	 * Hook method called to modify a view just before rendering.
	 * This method conceptually belongs to the view itself, not to the
	 * controller (cf. MVC pattern).
	 * It is made static to discourage a way of programming that
	 * routinely stores references to UI elements in instance fields
	 * for access by the view controller's event handlers, and so on.
	 * The Web Dynpro programming model recommends that UI elements can
	 * only be accessed by code executed within the call to this hook method.
	 *
	 * @param wdThis Generated private interface of the view's controller, as
	 *        provided by Web Dynpro. Provides access to the view controller's
	 *        outgoing controller usages, etc.
	 * @param wdContext Generated interface of the view's context, as provided
	 *        by Web Dynpro. Provides access to the view's data.
	 * @param view The view's generic API, as provided by Web Dynpro.
	 *        Provides access to UI elements.
	 * @param firstTime Indicates whether the hook is called for the first time
	 *        during the lifetime of the view.
	 */
  //@@end
  public static void wdDoModifyView(IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView wdThis, IPrivateSTATIC_MDM_EPFC_CRIMPING_RESTView.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView
    //@@end
  }

  //@@begin javadoc:onActionCatchValue(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionCatchValue(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent, java.lang.String dataObject )
  {
    //@@begin onActionCatchValue(ServerEvent)

		int marked = dataObject.indexOf("=");
		String value = dataObject.substring(marked + 1);
		wdContext.currentContextElement().setCatchedValue(value);

    //@@end
  }

  //@@begin javadoc:onActionPrintCrimping(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionPrintCrimping(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionPrintCrimping(ServerEvent)
		String appParams =
			"stampa=8&table="
				+ wdContext.currentContextElement().getCatchedValue()
				+ "&crimping="
				+ wdContext.currentContextElement().getFlag_crimping()
				+ "&hosefamily="
				+ wdContext.currentContextElement().getFlag_family()
				+ "&restricted=N"
				+ "&title_booklet="
				+ wdContext.currentContextElement().getTitle_booklet()
				+ "&title_datasheet="
				+ wdContext.currentContextElement().getTitle_datasheet()
				+ "&date_pubblication="
				+ wdContext.currentContextElement().getDate_pubblication();

		WDPortalNavigation.navigateAbsolute(
			"ROLES://portal_content/com.manulirubber.manuli_0/com.manulirubber.catalogue.iviews/com.manulirubber.staticstampa",
			WDPortalNavigationMode.SHOW_EXTERNAL,
			WDPortalNavigationHistoryMode.ALLOW_DUPLICATIONS,
			appParams);

    //@@end
  }

  //@@begin javadoc:onActionPrintInternalCrimping(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionPrintInternalCrimping(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionPrintInternalCrimping(ServerEvent)
		String appParams =
			"stampa=9&table="
				+ wdContext.currentContextElement().getCatchedValue()
				+ "&crimping="
				+ wdContext.currentContextElement().getFlag_crimping()
				+ "&hosefamily="
				+ wdContext.currentContextElement().getFlag_family()
				+ "&restricted=N"
				+ "&title_booklet="
				+ wdContext.currentContextElement().getTitle_booklet()
				+ "&title_datasheet="
				+ wdContext.currentContextElement().getTitle_datasheet()
				+ "&date_pubblication="
				+ wdContext.currentContextElement().getDate_pubblication();

		WDPortalNavigation.navigateAbsolute(
			"ROLES://portal_content/com.manulirubber.manuli_0/com.manulirubber.catalogue.iviews/com.manulirubber.staticstampa",
			WDPortalNavigationMode.SHOW_EXTERNAL,
			WDPortalNavigationHistoryMode.ALLOW_DUPLICATIONS,
			appParams);

    //@@end
  }

  //@@begin javadoc:onActionReload(ServerEvent)
	/** Declared validating event handler. */
  //@@end
  public void onActionReload(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionReload(ServerEvent)
		WDPortalEventing.fire(
			"urn:com.sap.pct.mdm.appl.masteriviews",
			"updateSearch",
			"&Public=YES");
		WDPortalEventing.fire(
			"urn:com.sap.pct.mdm.appl.masteriviews",
			"getRecordByIds",
			"MDMSystemAlias=product_manuli_rec&MDMTableName=Crimp_Rec"
				+ "&id=200&id=201&id=202&id=203");
		//recordId
		//	WDPortalEventing.fire(
		//		 "urn:com.sap.pct.mdm.appl.masteriviews",
		//		 "updateSearch",
		//		 "MDMSystemAlias=prod_manuli_rec&MDMTableName=Crimp_Rec"
		//		 + "&Public=YES") ; //recordId
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
	private String namespace;
	private String eventname;
  //@@end
}
