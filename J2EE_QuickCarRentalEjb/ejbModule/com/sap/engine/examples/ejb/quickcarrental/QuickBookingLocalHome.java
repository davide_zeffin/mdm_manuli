package com.sap.engine.examples.ejb.quickcarrental;
import javax.ejb.EJBLocalHome;

import java.util.Date;
import javax.ejb.CreateException;
import javax.ejb.FinderException;
import java.util.Collection;
public interface QuickBookingLocalHome extends EJBLocalHome {

	/**
	 * Create Method.
	 */
	public QuickBookingLocal create(
		String bookingId,
		String vehicleTypeId,
		Date reservationDate,
		Date dateFrom,
		Date dateTo,
		String pickupLocation,
		String dropoffLocation,
		String status)
		throws CreateException;

	/**
	 * Find Method.
	 */
	public QuickBookingLocal findByPrimaryKey(String primKey)
		throws FinderException;


	/**
	 * Find Method.
	 */
	public Collection findByStatus(String status) throws FinderException;


}

