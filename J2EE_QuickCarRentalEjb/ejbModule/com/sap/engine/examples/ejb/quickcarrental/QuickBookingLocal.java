package com.sap.engine.examples.ejb.quickcarrental;
import javax.ejb.EJBLocalObject;

import java.util.Date;
public interface QuickBookingLocal extends EJBLocalObject {

	/**
	 * Business Method.
	 */
	public String getBookingId();


	/**
	 * Business Method.
	 */
	public Date getDateFrom();


	/**
	 * Business Method.
	 */
	public Date getDateTo();


	/**
	 * Business Method.
	 */
	public String getDropoffLocation();


	/**
	 * Business Method.
	 */
	public String getPickupLocation();


	/**
	 * Business Method.
	 */
	public Date getReservationDate();


	/**
	 * Business Method.
	 */
	public String getStatus();


	/**
	 * Business Method.
	 */
	public String getVehicleTypeId();


	/**
	 * Business Method.
	 */
	public void setBookingId(String bookingId);


	/**
	 * Business Method.
	 */
	public void setDateFrom(Date dateFrom);


	/**
	 * Business Method.
	 */
	public void setDateTo(Date dateTo);


	/**
	 * Business Method.
	 */
	public void setDropoffLocation(String dropoffLocation);


	/**
	 * Business Method.
	 */
	public void setPickupLocation(String pickupLocation);


	/**
	 * Business Method.
	 */
	public void setReservationDate(Date reservationDate);


	/**
	 * Business Method.
	 */
	public void setStatus(String status);


	/**
	 * Business Method.
	 */
	public void setVehicleTypeId(String vehicleTypeId);


}

