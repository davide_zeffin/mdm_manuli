/*
 * Created on 23.02.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.engine.examples.ejb.quickcarrental;

import com.sap.security.api.permissions.ActionPermission;

/**
 * @author SAP AG
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class QuickReservationEjbPermission extends ActionPermission {

	/**
	 * @param cartype
	 * @param action
	 */
	public QuickReservationEjbPermission(String cartype, String action) {
		super(cartype, action);
		
	}

}
