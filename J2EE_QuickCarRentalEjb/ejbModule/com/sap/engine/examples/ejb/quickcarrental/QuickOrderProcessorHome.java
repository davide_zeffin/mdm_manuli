package com.sap.engine.examples.ejb.quickcarrental;
import javax.ejb.EJBHome;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
public interface QuickOrderProcessorHome extends EJBHome {

	/**
	 * Create Method.
	 */
	public QuickOrderProcessor create()
		throws CreateException, RemoteException;

}

