package com.sap.engine.examples.ejb.quickcarrental;
import javax.ejb.EJBHome;

import java.util.Date;
import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.FinderException;
import java.util.Collection;
public interface QuickBookingHome extends EJBHome {

	/**
	 * Create Method.
	 */
	public QuickBooking create(
		String bookingId,
		String vehicleTypeId,
		Date reservationDate,
		Date dateFrom,
		Date dateTo,
		String pickupLocation,
		String dropoffLocation,
		String status)
		throws CreateException, RemoteException;

	/**
	 * Find Method.
	 */
	public QuickBooking findByPrimaryKey(String primKey)
		throws FinderException, RemoteException;

	/**
	 * Find Method.
	 */
	public Collection findByStatus(String status)
		throws FinderException, RemoteException;

}

