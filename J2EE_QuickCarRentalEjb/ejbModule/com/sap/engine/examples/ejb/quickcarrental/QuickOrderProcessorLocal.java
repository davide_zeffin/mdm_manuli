package com.sap.engine.examples.ejb.quickcarrental;
import javax.ejb.EJBLocalObject;

import com.sap.engine.examples.util.QuickBookingModel;
import com.sap.engine.examples.util.QuickCarRentalException;
public interface QuickOrderProcessorLocal extends EJBLocalObject {

	/**
	 * Business Method.
	 */
	public QuickBookingModel saveBooking (
		String vehicleTypeId,
		String dateFromString,
		String dateToString,
		String pickupLocation,
		String dropoffLocation)throws QuickCarRentalException;


	/**
	 * Business Method.
	 */
	public String cancelBooking(String bookingId)throws QuickCarRentalException;


	/**
	 * Business Method.
	 */
	public QuickBookingModel[] viewActiveBookings()throws QuickCarRentalException;


}

