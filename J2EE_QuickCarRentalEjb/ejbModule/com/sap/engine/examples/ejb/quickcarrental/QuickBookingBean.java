package com.sap.engine.examples.ejb.quickcarrental;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;
import javax.ejb.CreateException;

import java.util.Date;
/**
 * @ejbHome <{com.sap.engine.examples.ejb.quickcarrental.QuickBookingHome}>
 * @ejbLocal <{com.sap.engine.examples.ejb.quickcarrental.QuickBookingLocal}>
 * @ejbLocalHome <{com.sap.engine.examples.ejb.quickcarrental.QuickBookingLocalHome}>
 * @ejbPrimaryKey <{java.lang.String}>
 * @ejbRemote <{com.sap.engine.examples.ejb.quickcarrental.QuickBooking}>
 * @hasSimplePK 
 */
public abstract class QuickBookingBean implements EntityBean {

	public void ejbLoad() {
	}

	public void ejbStore() {
	}

	public void ejbRemove() throws RemoveException {
	}

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void setEntityContext(EntityContext context) {
		myContext = context;
	}

	public void unsetEntityContext() {
		myContext = null;
	}

	private EntityContext myContext;

	/**
	 * @primKeyField 
	 */
	public abstract String getBookingId();

	public abstract void setBookingId(String bookingId);

	public abstract Date getReservationDate();


	public abstract void setReservationDate(Date reservationDate);


	public abstract String getDropoffLocation();


	public abstract void setDropoffLocation(String dropoffLocation);


	public abstract Date getDateTo();


	public abstract void setDateTo(Date dateTo);


	public abstract Date getDateFrom();


	public abstract void setDateFrom(Date dateFrom);


	public abstract String getPickupLocation();


	public abstract void setPickupLocation(String pickupLocation);


	public abstract String getStatus();


	public abstract void setStatus(String status);


	public abstract String getVehicleTypeId();


	public abstract void setVehicleTypeId(String vehicleTypeId);


	/**
	 * Create Method.
	 */
	public String ejbCreate(
		String bookingId,
		String vehicleTypeId,
		Date reservationDate,
		Date dateFrom,
		Date dateTo,
		String pickupLocation,
		String dropoffLocation,
		String status)
		throws CreateException {
		setBookingId(bookingId);
		setReservationDate(reservationDate);
		setDateFrom(dateFrom);
		setDateTo(dateTo);
		setStatus(status);

		//Relations to other EJBs in extended Car Rental Example
		setVehicleTypeId(vehicleTypeId);
		setPickupLocation(pickupLocation);
		setDropoffLocation(dropoffLocation);

		return null;
	}

	/**
	 * Post Create Method.
	 */
	public void ejbPostCreate(
		String bookingId,
		String vehicleTypeId,
		Date reservationDate,
		Date dateFrom,
		Date dateTo,
		String pickupLocation,
		String dropoffLocation,
		String status) {
	
	}

}
