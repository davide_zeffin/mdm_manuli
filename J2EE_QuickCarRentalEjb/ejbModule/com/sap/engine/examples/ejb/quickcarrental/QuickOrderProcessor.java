package com.sap.engine.examples.ejb.quickcarrental;
import java.rmi.RemoteException;

import javax.ejb.EJBObject;

import com.sap.engine.examples.util.QuickBookingModel;
import com.sap.engine.examples.util.QuickCarRentalException;
public interface QuickOrderProcessor extends EJBObject {

	/**
	 * Business Method.
	 */
	public QuickBookingModel saveBooking(
		String vehicleTypeId,
		String dateFromString,
		String dateToString,
		String pickupLocation,
		String dropoffLocation)
		throws RemoteException,QuickCarRentalException;


	/**
	 * Business Method.
	 */
	public String cancelBooking(String bookingId) throws RemoteException,QuickCarRentalException;


	/**
	 * Business Method.
	 */
	public QuickBookingModel[] viewActiveBookings() throws RemoteException,QuickCarRentalException;


}

