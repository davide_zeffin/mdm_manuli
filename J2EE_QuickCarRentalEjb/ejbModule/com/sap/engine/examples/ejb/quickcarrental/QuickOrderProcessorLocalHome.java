package com.sap.engine.examples.ejb.quickcarrental;
import javax.ejb.EJBLocalHome;

import javax.ejb.CreateException;
public interface QuickOrderProcessorLocalHome extends EJBLocalHome {

	/**
	 * Create Method.
	 */
	public QuickOrderProcessorLocal create() throws CreateException;

}

