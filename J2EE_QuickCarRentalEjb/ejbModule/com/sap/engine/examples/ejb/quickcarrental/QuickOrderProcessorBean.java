package com.sap.engine.examples.ejb.quickcarrental;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sap.engine.examples.util.Constants;
import com.sap.engine.examples.util.QuickBookingModel;
import com.sap.engine.examples.util.QuickCarRentalException;

import com.sap.security.api.IUser;
import com.sap.security.api.UMException;
import com.sap.security.api.UMFactory;

import com.sap.engine.examples.ejb.quickcarrental.QuickReservationEjbPermission;
import java.security.AccessControlException;

/**
 * @ejbHome <{com.sap.engine.examples.ejb.quickcarrental.QuickOrderProcessorHome}>
 * @ejbLocal <{com.sap.engine.examples.ejb.quickcarrental.QuickOrderProcessorLocal}>
 * @ejbLocalHome <{com.sap.engine.examples.ejb.quickcarrental.QuickOrderProcessorLocalHome}>
 * @ejbRemote <{com.sap.engine.examples.ejb.quickcarrental.QuickOrderProcessor}>
 * @stateless 
 */
public class QuickOrderProcessorBean implements SessionBean {

	private QuickBookingLocalHome bookingHome;

	public void ejbRemove() {
	}

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void setSessionContext(SessionContext context) {
		myContext = context;
	}

	private SessionContext myContext;
	/**
	 * Business Method.
	 */

	public QuickBookingModel saveBooking(
		String vehicleTypeId,
		String dateFromString,
		String dateToString,
		String pickupLocation,
		String dropoffLocation)
		throws QuickCarRentalException {

		try {
			String username = myContext.getCallerPrincipal().getName();
			IUser user =
				UMFactory.getUserFactory().getUserByUniqueName(username);

			try {
				user.checkPermission(
					new QuickReservationEjbPermission(vehicleTypeId, "create"));
			} catch (AccessControlException e) {
				e.printStackTrace();
				throw new QuickCarRentalException(
					user.getLastName()
						+ " may not create reservations for "
						+ vehicleTypeId
						+ " car types.");
			}

		} catch (UMException e) {
			throw new QuickCarRentalException("Could not get user name.");
		}

		Date dateFrom = getDate(dateFromString);
		Date dateTo = getDate(dateToString);
		if (dateTo.before(dateFrom))
			throw new QuickCarRentalException("Pickup date has to be before dropoff date.");

		QuickBookingLocal booking = null;
		java.util.Date reservationDate = new java.util.Date();

		java.lang.String bookingId = generateId();
		try {
			booking =
				bookingHome.create(
					bookingId,
					vehicleTypeId,
					reservationDate,
					dateFrom,
					dateTo,
					pickupLocation,
					dropoffLocation,
					Constants.STATUS_ACTIVE);
		} catch (CreateException e) {
			e.printStackTrace();
			throw new QuickCarRentalException(e.getMessage());
		}

		return getBookingModel(booking);

	}

	/**
	 * Business Method.
	 */
	public String cancelBooking(String bookingId)
		throws QuickCarRentalException {

		try {
			String username = myContext.getCallerPrincipal().getName();
			IUser user =
				UMFactory.getUserFactory().getUserByUniqueName(username);

			String vehicleTypeId;

			try {
				QuickBookingLocal booking =
					bookingHome.findByPrimaryKey(bookingId);
				vehicleTypeId = booking.getVehicleTypeId();
				try {
					user.checkPermission(
						new QuickReservationEjbPermission(
							vehicleTypeId,
							"cancel"));
					booking.setStatus(Constants.STATUS_CANCELLED);
				} catch (AccessControlException e) {
					e.printStackTrace();
					throw new QuickCarRentalException(
						user.getLastName()
							+ " may not cancel reservations for "
							+ vehicleTypeId
							+ " car types.");
				}

			} catch (FinderException e) {
				e.printStackTrace();
				throw new QuickCarRentalException(e.getMessage());
			}
		} catch (UMException e) {
			throw new QuickCarRentalException("Could not get user name.");
		}

		return bookingId + " cancelled.";
	}

	/**
	 * Business Method.
	 */
	public QuickBookingModel[] viewActiveBookings()
		throws QuickCarRentalException {
		ArrayList bookings = new ArrayList();

		try {
			String username = myContext.getCallerPrincipal().getName();
			IUser user =
				UMFactory.getUserFactory().getUserByUniqueName(username);

			try {
				user.checkPermission(
					new QuickReservationEjbPermission("*", "view"));
			} catch (AccessControlException e) {
				e.printStackTrace();
				throw new QuickCarRentalException(
					user.getLastName() + " may not view reservations.");
			}
		} catch (UMException e) {
			throw new QuickCarRentalException("Could not get user name.");
		}

		try {

			Collection active =
				bookingHome.findByStatus(Constants.STATUS_ACTIVE);
			for (Iterator iterator = active.iterator(); iterator.hasNext();) {
				bookings.add(
					getBookingModel((QuickBookingLocal) iterator.next()));
			}

		} catch (FinderException e) {
			e.printStackTrace();
			throw new QuickCarRentalException(e.getMessage());
		}
		QuickBookingModel[] result = new QuickBookingModel[bookings.size()];
		bookings.toArray(result);
		return result;
	}

	/**
	 * Create Method.
	 */
	public void ejbCreate() throws CreateException {
		try {
			Context ctx = new InitialContext();
			bookingHome =
				(QuickBookingLocalHome) ctx.lookup(
					"java:comp/env/QuickCarRental/QuickBookingBean");

		} catch (NamingException e) {
			throw new CreateException(e.getMessage());
		}
	}

	private String generateId() {
		Date temp = new Date();
		String stemp = String.valueOf(temp.hashCode());
		if (stemp.length() < 10)
			return stemp.substring(1, stemp.length());
		else
			return stemp.substring(1, 9);
	}

	private QuickBookingModel getBookingModel(QuickBookingLocal booking) {
		QuickBookingModel data = new QuickBookingModel();
		data.setBookingId(booking.getBookingId());
		data.setPickupLocation(booking.getPickupLocation());
		data.setVehicleType(booking.getVehicleTypeId());
		data.setDateFrom(Constants.FORMATTER.format(booking.getDateFrom()));
		data.setDateTo(Constants.FORMATTER.format(booking.getDateTo()));
		data.setDropoffLocation(booking.getDropoffLocation());
		int days =
			(int) ((booking.getDateTo().getTime()
				- booking.getDateFrom().getTime())
				/ (1000 * 60 * 60 * 24));
		if (days == 0)
			days = 1;
		data.setPrice(Float.toString(40 * days) + " EUR");
		return data;
	}

	private Date getDate(java.lang.String dateString)
		throws QuickCarRentalException {

		try {
			return Constants.FORMATTER.parse(dateString);
		} catch (java.text.ParseException pe) {
			pe.printStackTrace();
			throw new QuickCarRentalException("The date has to be in format dd.mm.yyyy, for example 21.04.2004");
		}
	}

}
