package it.greenorange.mdm;


import java.io.FileOutputStream;
import java.io.IOException;

import a2i.common.A2iResultSet;
import a2i.common.CatalogData;
import a2i.common.ResultSetDefinition;
import a2i.core.StringException;
import a2i.search.FreeFormParameter;
import a2i.search.FreeFormParameterField;
import a2i.search.FreeFormTableParameter;
import a2i.search.Search;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
/*
 * Created on 20-mar-2013
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FreeFormSearchExamples {
	
	CatalogData catalog;
	
	public void login() {
		
		catalog = new CatalogData();
		
		catalog.Login("manepsvi.sapman.dc",  2345, "Admin", "admin", "English [US]");

	}
	
	public void logout() {
		
		try {
			catalog.Logout();
		} catch (StringException e) {
			System.out.println("Logout Failed!");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Search on one single field for one single value
	 */
	public void searchSingleFieldSimple() {
		
		//FIND(Name, "Contractor") on table Products
		// case sensitive substring search in the field "Name"
		
		System.out.println();
		System.out.println("Performing searchSingleFieldSimple()");
		System.out.println("FIND(CatHier.Name, \"TRACTOR/1\") on table Products");
		System.out.println("Expected Hits: 18");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Crimp_Rec");
		rsd.AddField("Family");
		rsd.AddField("version");
		rsd.AddField("Crimp_Version");

		Search search = new Search("Crimp_Rec");

		FreeFormTableParameter fftpNames = search.GetParameters().NewFreeFormTableParameter("Crimp_Rec");
				
		FreeFormParameterField ffpfName = fftpNames.GetFields().New("Family");
		FreeFormParameterField ffpfPart = fftpNames.GetFields().New("version");
		FreeFormParameterField ffpfDesc = fftpNames.GetFields().New("Crimp_Version");
		ffpfName.GetFreeForm().NewString("ADLER/1", FreeFormParameter.SubstringSearchType);

		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Family", true, 0);
			
			System.out.println("Found " + result.GetRecordCount() + " records.");
			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "CatHier").GetStringValue());
				System.out.println(result.GetValueAt(n, "version").GetStringValue() + "\t\t" + 
								 //  result.GetValueAt(n, "version").GetStringValue() + "\t\t" + 
								   result.GetValueAt(n, "Family").GetStringValue());
			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}

		// step 1: creation of a document-object
		Document document = new Document();
		try {
		 // step 2:
		 // we create a writer that listens to the document
		 // and directs a PDF-stream to a file
		 PdfWriter.getInstance(document,
		   new FileOutputStream("HelloWorld.pdf"));
		 // step 3: we open the document
		 document.open();
		 // step 4: we add a paragraph to the document
		 document.add(new Paragraph("Hello World"));
		} catch (DocumentException de) {
		 System.err.println(de.getMessage());
		} catch (IOException ioe) {
		 System.err.println(ioe.getMessage());
		}
		// step 5: we close the document
		document.close();

	}

	/**
	 * Search on one single field for 3 values, AND combination
	 */	
	public void searchSingleFieldAnd() {
		
		//FIND(Name, "Contractor") AND FIND(Name, "Blade") AND FIND(Name, "Standard") on table Products
		
		System.out.println();
		System.out.println("Performing searchSingleFieldAnd()");
		System.out.println("FIND(Name, \"Description\") AND FIND(Name, \"Blade\") AND FIND(Name, \"Standard\") on table Products");
		System.out.println("Expected Hits: 5");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		
		Search search = new Search("Products");

		FreeFormTableParameter fftpNames = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfName = fftpNames.GetFields().New("Name", FreeFormParameterField.SEARCH_OPERATOR_AND);
		ffpfName.GetFreeForm().NewString("Description", FreeFormParameter.SubstringSearchType);
		ffpfName.GetFreeForm().NewString("Blade", FreeFormParameter.SubstringSearchType);
		ffpfName.GetFreeForm().NewString("Standard", FreeFormParameter.SubstringSearchType);

		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}

	/**
	 * Search on one single field for 3 values, OR combination
	 */	
	public void searchSingleFieldOr() {
		
		//FIND(Name, "Contractor") OR FIND(Name, "Blade") OR FIND(Name, "Standard") on table Products
		
		System.out.println();
		System.out.println("Performing searchSingleFieldOr()");
		System.out.println("FIND(Name, \"Description\") OR FIND(Name, \"Blade\") OR FIND(Name, \"Standard\") on table Products");
		System.out.println("Expected Hits: 48");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		
		Search search = new Search("Products");

		FreeFormTableParameter fftpNames = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfName = fftpNames.GetFields().New("Name", FreeFormParameterField.SEARCH_OPERATOR_OR);
		ffpfName.GetFreeForm().NewString("Description", FreeFormParameter.SubstringSearchType);
		ffpfName.GetFreeForm().NewString("Blade", FreeFormParameter.SubstringSearchType);
		ffpfName.GetFreeForm().NewString("Standard", FreeFormParameter.SubstringSearchType);

		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}
	
	/**
	 * Search on one single field for 3 values, nested OR / AND combination
	 */		
	public void searchSingleFieldOrAnd() {
		
		//(FIND(Name, "Contractor") OR FIND(Name, "Blade")) AND FIND(Name, "Standard") on table Products
		
		System.out.println();
		System.out.println("Performing searchSingleFieldOrAnd()");
		System.out.println("(FIND(Name, \"Description\") OR FIND(Name, \"Blade\")) AND FIND(Name, \"Standard\") on table Products");
		System.out.println("Expected Hits: 11");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		
		Search search = new Search("Products");

		FreeFormTableParameter fftpNames = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfName1 = fftpNames.GetFields().New("Name", FreeFormParameterField.SEARCH_OPERATOR_OR);
		ffpfName1.GetFreeForm().NewString("Description", FreeFormParameter.SubstringSearchType);
		ffpfName1.GetFreeForm().NewString("Blade", FreeFormParameter.SubstringSearchType);
		
		FreeFormParameterField ffpfName2 = fftpNames.GetFields().New("Name");
		ffpfName2.GetFreeForm().NewString("Standard", FreeFormParameter.SubstringSearchType);

		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}


	/**
	 * Search on two fields for single values, AND combination
	 */	
	public void searchTwoFieldsAnd() {
		
		//FIND(Manufacturer, "Estwing") AND FIND(Name, "Polyurethane") on table Products
		
		System.out.println();
		System.out.println("Performing searchTwoFieldsAnd()");
		System.out.println("FIND(Manufacturer, \"Estwing\") AND FIND(Name, \"Polyurethane\") on table Products");
		System.out.println("Expected Hits: 3");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		rsd.AddField("Manufacturer");
		
		Search search = new Search("Products");

		FreeFormTableParameter fftpProducts = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfManu = fftpProducts.GetFields().New("Manufacturer");
		ffpfManu.GetFreeForm().NewString("Estwing", FreeFormParameter.SubstringSearchType);

		FreeFormParameterField ffpfName = fftpProducts.GetFields().New("Name");
		ffpfName.GetFreeForm().NewString("Polyurethane", FreeFormParameter.SubstringSearchType);
		
		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue() + "\t\t" + result.GetValueAt(n, "Manufacturer").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}

	/**
	 * Search on two fields for single values, OR combination
	 */	
	public void searchTwoFieldsOr() {
		
		//FIND(Manufacturer, "Estwing") OR FIND(Name, "Polyurethane") on table Products
		
		System.out.println();
		System.out.println("Performing searchTwoFieldsOr()");
		System.out.println("FIND(Manufacturer, \"Estwing\") OR FIND(Name, \"Polyurethane\") on table Products");
		System.out.println("Expected Hits: 67");
		System.out.println("=========================");
		
		ResultSetDefinition rsd = new ResultSetDefinition("Products");
		rsd.AddField("Name");
		rsd.AddField("Manufacturer");
		
		Search search = new Search("Products");

		search.SetSearchType(Search.GlobalOrSearchCombinationType);
		
		FreeFormTableParameter fftpProducts = search.GetParameters().NewFreeFormTableParameter("Products");
		
		FreeFormParameterField ffpfManu = fftpProducts.GetFields().New("Manufacturer");
		ffpfManu.GetFreeForm().NewString("Estwing", FreeFormParameter.SubstringSearchType);

		FreeFormParameterField ffpfName = fftpProducts.GetFields().New("Name");
		ffpfName.GetFreeForm().NewString("Polyurethane", FreeFormParameter.SubstringSearchType);
		
		search.SetSearchType(Search.GlobalOrSearchCombinationType);
		
		A2iResultSet result;
		try {
			result = catalog.GetResultSet(search, rsd, "Name", true, 0);

			System.out.println("Found " + result.GetRecordCount() + " records.");
//			for(int n=0; n<result.GetRecordCount(); n++) {
//				System.out.println(result.GetValueAt(n, "Name").GetStringValue() + "\t\t" + result.GetValueAt(n, "Manufacturer").GetStringValue());
//			}


		} catch (StringException e) {
			System.out.println("Search Failed!");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		
		FreeFormSearchExamples ffse = new FreeFormSearchExamples();
		
		ffse.login();
		
		ffse.searchSingleFieldSimple();
		
//		ffse.searchSingleFieldAnd();
//		
//		ffse.searchSingleFieldOr();
//		
//		ffse.searchSingleFieldOrAnd();
//		
//		ffse.searchTwoFieldsAnd();
//
//		ffse.searchTwoFieldsOr();		
//		
		ffse.logout();
		
		
	}
}
