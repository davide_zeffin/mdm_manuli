/*
 * Created on 26-ago-2013
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package it.greenorange.mdm;

import java.util.ArrayList;

import com.sap.mdm.data.ResultDefinition;
import com.sap.mdm.ids.FieldId;
import com.sap.mdm.ids.TableId;
import com.sap.mdm.schema.FieldProperties;
import com.sap.mdm.schema.TableSchema;
import com.sap.mdm.schema.fields.LookupFieldProperties;

/**
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MDMHier {
	MdmDataConnection connection = getConnection();
	TableId hierTableId = connection.getRepositorySchema().getTableId("HIERARCHY");
	TableId Hier_TId = repository_schema.getTableId("");
           
	java.util.List list = new ArrayList();
 
	ResultDefinition Supporting_result_dfn = null;
 
	FieldProperties[] Hier_Field_props =rep_schema.getTableSchema(Hier_TId).getFields();
 
	LookupFieldProperties lookup_field = null;
	TableSchema lookupTableSchema = null;
           
	FieldId[] lookupFieldIDs = null;
/* 
	for (int i = 0, j = Hier_Field_props.length; i < j; i++) {
 
	if (Hier_Field_props[i].isLookup()) {     
								  lookup_field = (LookupFieldProperties) Hier_Field_props<i>;
		 lookupTableSchema =repository_schema.getTableSchema(lookup_field.getLookupTableId());
								  lookupFieldIDs = lookupTableSchema.getFieldIds();
           
		 Supporting_result_dfn = new ResultDefinition(lookup_field.getLookupTableId());
           
		 Supporting_result_dfn.setSelectFields(lookupFieldIDs);
           
		 list.add(Supporting_result_dfn);
           
		 }
           
	}
                     
	com.sap.mdm.search.Search hier_search =new com.sap.mdm.search.Search(Hier_TId);
           
	ResultDefinition Hier_Resultdfn =     new ResultDefinition(Hier_TId);
	Hier_Resultdfn.setSelectFields(rep_schema.getTableSchema(Hier_TId).getDisplayFieldIds());
           
	ResultDefinition[] supportingResultDefinitions =
	(ResultDefinition[])list.toArray(new ResultDefinition [ list.size() ]);
           
	RetrieveLimitedHierTreeCommand retrieve_Hier_tree_cmd =
	new RetrieveLimitedHierTreeCommand(conn_acc);
           
	retrieve_Hier_tree_cmd.setResultDefinition(Hier_Resultdfn);
	retrieve_Hier_tree_cmd.setSession(Auth_User_session_cmd.getSession());
	retrieve_Hier_tree_cmd.setSearch(hier_search);
	retrieve_Hier_tree_cmd.setSupportingResultDefinitions(supportingResultDefinitions);
           
	try {
		 retrieve_Hier_tree_cmd.execute();
	} catch (CommandException e5) {
			  // TODO Auto-generated catch block
			  e5.printStackTrace();
			  }
           
	HierNode Hier_Node = retrieve_Hier_tree_cmd.getTree();
                               
	print(Hier_Node,1);
 
 
//	  method print()
	static private void print(HierNode node, int level) {
 
	if (!node.isRoot()) {
		 for (int i = 0, j = level; i < j; i++) {
			  System.out.print("\t");
				   }
 
		 System.out.println(node.getDisplayValue());
 
			  }
 
	HierNode[] children = node.getChildren();
 
	if (children != null) {
			  level++;
	for (int i = 0, j = children.length; i < j; i++) {
	print(children<i>, level);
	}
	}
	}
 
//	  end method print()
 * 
 * @author Administrator
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
}
